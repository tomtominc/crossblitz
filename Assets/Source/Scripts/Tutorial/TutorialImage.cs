using System;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace CrossBlitz.Tutorial
{
    public class TutorialImage : MonoBehaviour
    {
        public SpriteAnimation loading;
        public Image image;
        public Button nextButton;
        public event Action OnClicked;

        private void Start()
        {
            nextButton.onClick.RemoveAllListeners();
            nextButton.onClick.AddListener(OnNextButton);
        }

        public async void Load(string imageUrl)
        {
            loading.SetActive(true);
            loading.Play("idle");
            var alloc = await Addressables.LoadAssetAsync<Sprite>(imageUrl).Task;
            loading.SetActive(false);
            image.sprite = alloc;
        }

        private void OnNextButton()
        {
            OnClicked?.Invoke();
        }
    }
}