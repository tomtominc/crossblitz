using System.Threading.Tasks;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.Fables;
using CrossBlitz.Fables.Battle;
using CrossBlitz.InputAPI;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI.GameLogic;
using TakoBoyStudios.Events;
using UnityEngine;
using UnityEngine.SceneManagement;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Tutorial
{
    public class TutorialController : MonoBehaviour
    {
        private void Start()
        {
            // open the collection menu (change deck)
            // open deck recipe menu (another variant of open collection menu)
            // open the collection menu (at edit deck)
            // battles - with conditions

            Events.Subscribe(EventType.OnOpenCollectionMenu, OnOpenCollectionMenu);
            Events.Subscribe(EventType.OnStateChanged, OnStateChanged);
            Events.Subscribe(EventType.OnFableInputStateChanged, OnFablesInputStateChanged);

            SceneController.OnSceneLoaded += OnSceneLoaded;
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnOpenCollectionMenu, OnOpenCollectionMenu);
            Events.Unsubscribe(EventType.OnStateChanged, OnStateChanged);
            SceneController.OnSceneLoaded -= OnSceneLoaded;
        }

        private void OnFablesInputStateChanged(IMessage message)
        {
            if (message.Data is OnFableInputStateChangedEventArgs args)
            {
                TutorialData tutorial = null;

                switch (args.inputMode)
                {
                    case FablesInput.Mode.CardShop:
                        tutorial = Db.TutorialDatabase.GetByName("Fables - Cardinella's Card Shop");
                        break;
                    case FablesInput.Mode.ManaMeld:
                        tutorial = Db.TutorialDatabase.GetByName("Fables - Mawlder's Melding Shop");
                        break;
                    case FablesInput.Mode.RelicShop:
                        tutorial = Db.TutorialDatabase.GetByName("Fables - Scias' Relic Shop");
                        break;
                    case FablesInput.Mode.Explore:
                        if (FableController.PlayerIsInAFable())
                        {
                            tutorial = Db.TutorialDatabase.GetByName("Fables - Hex Map I");

                            if (App.BattleResults == null)
                            {
                                Debug.LogError("Battle Results are null!");
                            }

                            if (App.BattleResults != null && App.BattleResults.GetMatchResult() == BattleResults.Result.Won)
                            {
                                var chapter = App.FableData.GetCurrentChapter();
                                var battleResults = App.BattleResults;
                                var tile = Db.MapDatabase.GetTileByBattle(battleResults.BattleUid);
                                var isAfterCutscene = true;

                                if (chapter != null && battleResults != null && tile != null &&
                                    !string.IsNullOrEmpty(tile.outroCutsceneUid) &&
                                    !chapter.HasSeenCutscene(tile.outroCutsceneUid))
                                {
                                    isAfterCutscene = args.lastMode == FablesInput.Mode.Cutscene;
                                }

                                if (isAfterCutscene)
                                {
                                    if (App.TutorialData.HasSeenTutorial(tutorial.Uid))
                                    {
                                        tutorial = Db.TutorialDatabase.GetByName("Fables - Hex Map II");
                                    }

                                    if (App.TutorialData.HasSeenTutorial(tutorial.Uid))
                                    {
                                        tutorial = Db.TutorialDatabase.GetByName("Fables - Battle Info");
                                    }

                                    if (App.TutorialData.HasSeenTutorial(tutorial.Uid))
                                    {
                                        var battle = Db.BattleDatabase.GetBattleData(App.BattleResults.GetBattleUid());

                                        if (battle != null && battle.BattleName.Contains("1-14"))
                                        {
                                            tutorial = Db.TutorialDatabase.GetByName("Fables - Chapter Completion");
                                        }
                                    }
                                }
                            }
                        }

                        break;
                }

                if (tutorial != null && !App.TutorialData.HasSeenTutorial(tutorial.Uid))
                {
                    OpenTutorial(tutorial.Uid);
                }
            }
        }

        private void OnSceneLoaded(SceneModel sceneModel)
        {
            TutorialData tutorial = null;

            if (FableController.PlayerIsInAFable() && sceneModel.Scene.name == "LevelGrid")
            {
                tutorial = Db.TutorialDatabase.GetByName("Fables - Level Grid");
            }
            else if (FableController.PlayerIsInAFable() && sceneModel.Scene.name == "MatchFinished")
            {
                tutorial = Db.TutorialDatabase.GetByName("Fables - Results");
            }

            if (tutorial != null && !App.TutorialData.HasSeenTutorial(tutorial.Uid))
            {
                OpenTutorial(tutorial.Uid);
            }
        }

        private void OnStateChanged(IMessage message)
        {
            if (message.Data is OnStateChangedEventArgs args)
            {
                TutorialData tutorial = null;

                switch (args.state)
                {
                    case StateDefinition.GAME_RO_SHAM_BO when FableController.PlayerIsInAFable():
                    {
                        tutorial = Db.TutorialDatabase.GetByName("Battles - Turn Order");
                        break;
                    }
                    case StateDefinition.GAME_MULLIGAN when FableController.PlayerIsInAFable():
                    {
                        tutorial = Db.TutorialDatabase.GetByName("Battles - Starting Hand");
                        break;
                    }
                    case StateDefinition.GAME_PLAYER_START_TURN when FableController.PlayerIsInAFable() &&
                                                                     GameServer.State.GetTurnCount(GameServer
                                                                         .LocalPlayerTeam) < 1:
                    {
                        var battleData = Db.BattleDatabase.GetBattleData(GameServer.Mode.BattleUid);
                        var playerDeck = App.PlayerDecks.GetDeckBasedOnGameState();

                        if (battleData != null)
                        {
                            if ((battleData.Deck != null && battleData.Deck.GetRelicCount() > 0) ||
                                (playerDeck != null && playerDeck.GetRelicCount() > 0))
                            {
                                tutorial = Db.TutorialDatabase.GetByName("Battles - Relics");
                            }
                            else if ((battleData.Deck != null && battleData.Deck.GetHeroPowers().Count > 0) ||
                                     (playerDeck != null && playerDeck.GetHeroPowers().Count > 0))
                            {
                                tutorial = Db.TutorialDatabase.GetByName("Battles - Blitz Bursts");
                            }
                            else
                            {
                                tutorial = Db.TutorialDatabase.GetByName("Battles - Rules I");

                                if (App.TutorialData.HasSeenTutorial(tutorial.Uid))
                                {
                                    tutorial = Db.TutorialDatabase.GetByName("Battles - Reading Cards");
                                }

                                if (App.TutorialData.HasSeenTutorial(tutorial.Uid))
                                {
                                    tutorial = Db.TutorialDatabase.GetByName("Battles - Rules II");
                                }
                            }
                        }

                        break;
                    }
                }

                if (tutorial != null && !App.TutorialData.HasSeenTutorial(tutorial.Uid))
                {
                    OpenTutorial(tutorial.Uid);
                }
            }
        }

        private void OnOpenCollectionMenu(IMessage message)
        {
            if (!FableController.PlayerIsInAFable())
            {
                return;
            }

            if (message.Data is OnOpenCollectionMenuEventArgs args)
            {
                TutorialData tutorial = null;

                switch (args.deckEditSettings.OpenMode)
                {
                    case DeckOpenMode.Collection when args.deckEditSettings.MenuMode == CollectionMenuMode.Collection:
                    {
                        tutorial = Db.TutorialDatabase.GetByName("Cards Menu - Deck Building");
                        break;
                    }
                    case DeckOpenMode.Collection when args.deckEditSettings.MenuMode == CollectionMenuMode.Relics:
                    {
                        tutorial = Db.TutorialDatabase.GetByName("Cards Menu - Relics");
                        break;
                    }
                    case DeckOpenMode.DeckEditor when args.deckEditSettings.MenuMode == CollectionMenuMode.Collection:
                    {
                        tutorial = Db.TutorialDatabase.GetByName("Cards Menu - Editing a Deck");
                        break;
                    }
                    case DeckOpenMode.DeckEditor when args.deckEditSettings.MenuMode == CollectionMenuMode.Relics:
                    {
                        tutorial = Db.TutorialDatabase.GetByName("Cards Menu - Relics");
                        break;
                    }
                    case DeckOpenMode.DeckCreator_SelectDeck:
                    {
                        tutorial = Db.TutorialDatabase.GetByName("Cards Menu - Creating a Deck");
                        break;
                    }
                }

                if (tutorial != null && !App.TutorialData.HasSeenTutorial(tutorial.Uid))
                {
                    OpenTutorial(tutorial.Uid);
                }
            }
        }

        private async void OpenTutorial(string tutorialUid)
        {
            await Task.Delay(1000);

            var sceneModel = await SceneController.Instance.LoadScene(new SceneLoadParams
            {
                SceneToLoad = "TutorialPopup",
                LoadSceneMode = LoadSceneMode.Additive
            });

            var tutorialMenu = sceneModel.GetSceneData<TutorialMenu>();

            if (tutorialMenu == null)
            {
                Debug.LogError("Error!");
                return;
            }

            tutorialMenu.SetAsOverlay();
            tutorialMenu.Open(tutorialUid);

            App.TutorialData.OnTutorialShown(tutorialUid);
        }
    }
}