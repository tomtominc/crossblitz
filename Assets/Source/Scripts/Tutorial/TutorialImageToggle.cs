using System;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Tutorial
{
    public class TutorialImageToggle : MonoBehaviour
    {
        public event Action<int> OnClicked;
        public Toggle toggle;
        private int m_index;

        private void Start()
        {
            toggle.onValueChanged.RemoveAllListeners();
            toggle.onValueChanged.AddListener(OnToggled);
        }

        public void SetIndex( int index )
        {
            m_index = index;
        }

        private void OnToggled(bool isOn)
        {
            if (isOn)
            {
                OnClicked?.Invoke(m_index);
            }
        }
    }
}