using System.Collections.Generic;
using System.Threading.Tasks;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Databases;
using DG.Tweening;
using TakoBoyStudios;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Tutorial
{
    public class TutorialMenu : MonoBehaviour, ISceneConfiguration
    {
        private const float ImageDisplayTime = 7;

        public Canvas canvas;
        public GameObject disableAllOverlay;
        public CanvasGroup parentCanvas;
        public GameObject viewRules;

        public TextMeshProUGUI tutorialTitle;
        public TextMeshProUGUI tutorialSubHeader;
        public TextMeshProUGUI tutorialText;

        public TutorialImage tutorialImagePrefab;
        public RectTransform tutorialImageContainer;

        public TutorialImageToggle tutorialImageTogglePrefab;
        public RectTransform toggleContainer;
        public ToggleGroup toggleGroup;

        public Button nextButton;
        public Button previousButton;
        public TextMeshProUGUI pageNumber;

        public RectTransform container;
        public CanvasGroup background;
        public Button exitButton;

        private string m_currentTutorial;
        private int m_currentPage;
        private int m_currentImage;
        private int m_totalImages;
        private float m_imageStartX;
        private float m_imageTimer;
        private float m_easingTimer;
        private bool m_animateOut;
        private List<TutorialImageToggle> m_toggles;

        public void Open(string tutorialUid)
        {
            m_currentTutorial = tutorialUid;
            m_currentPage = 0;
            m_currentImage = 0;

            parentCanvas.alpha = 1;
            disableAllOverlay.SetActive(false);

            var tutorial = Db.TutorialDatabase.Get(m_currentTutorial);
            tutorialTitle.text = tutorial.title;

            nextButton.onClick.RemoveAllListeners();
            nextButton.onClick.AddListener(OnNextButton);

            previousButton.onClick.RemoveAllListeners();
            previousButton.onClick.AddListener(OnPreviousButton);

            exitButton.onClick.RemoveAllListeners();
            exitButton.onClick.AddListener(OnExit);

            background.alpha = 0;
            background.DOFade(1, 1f);

            // container.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            // container.DOScale(1, 0.4f).SetEase(Ease.OutBack);

            container.DOAnchorPosY(0, 0.8f).SetEase(Ease.OutBack);

            viewRules.SetActive(true);

            OpenPage(m_currentPage);
        }

        private void Update()
        {
            if (m_animateOut)
            {
                if (parentCanvas.alpha > 0)
                {
                    parentCanvas.alpha -= Time.deltaTime * 5;

                    if (parentCanvas.alpha <= 0)
                    {
                        m_animateOut = false;
                        CloseScene();
                    }
                }
            }

            if (m_totalImages <= 1)
            {
                return;
            }

            m_imageTimer += Time.deltaTime;

            if (m_imageTimer >= ImageDisplayTime)
            {
                IncrementImage();
            }

            //if (m_easingTimer < 1)
            {
                m_easingTimer = Mathf.MoveTowards( m_easingTimer, 1,  Time.deltaTime * 5);
                var targetX = -345f * m_currentImage - m_currentImage;
                var x = EasingFunction.EaseOutQuad(m_imageStartX, targetX, m_easingTimer);
                tutorialImageContainer.anchoredPosition = new Vector2(x, 0);
            }

            // tutorialImageContainer.anchoredPosition = Vector2.MoveTowards(tutorialImageContainer.anchoredPosition,
            //     new Vector2(targetX, 0), 3000 * Time.deltaTime);
        }

        private void OpenPage(int pageIndex)
        {
            m_currentPage = pageIndex;

            var tutorial = Db.TutorialDatabase.Get(m_currentTutorial);
            var page = tutorial.pages[m_currentPage];

            tutorialSubHeader.text = page.header;
            tutorialText.text = page.text;
            m_totalImages = page.images.Count;

            RefreshButtons();
            LoadTutorialImages();
            SetCurrentImage(0);
        }

        private void OnNextButton()
        {
            m_currentPage++;
            OpenPage(m_currentPage);
        }

        private void OnPreviousButton()
        {
            m_currentPage--;
            OpenPage(m_currentPage);
        }

        private void RefreshButtons()
        {
            var pages = GetMaxNumberOfPages();
            pages = pages <= 0 ? 1 : pages;

            pageNumber.text = $"{m_currentPage + 1}/{pages}";
            nextButton.SetActive(m_currentPage + 1 < GetMaxNumberOfPages());
            previousButton.SetActive(m_currentPage > 0);
        }

        private int GetMaxNumberOfPages()
        {
            var tutorial = Db.TutorialDatabase.Get(m_currentTutorial);
            return tutorial.pages.Count;
        }

        private void LoadTutorialImages()
        {
            m_toggles = new List<TutorialImageToggle>();

            tutorialImageContainer.DestroyChildren();
            toggleContainer.DestroyChildren();

            var tutorial = Db.TutorialDatabase.Get(m_currentTutorial);
            var page = tutorial.pages[m_currentPage];

            for (var i = 0; i < page.images.Count; i++)
            {
                var tutorialImage = Instantiate(tutorialImagePrefab, tutorialImageContainer, false);
                tutorialImage.OnClicked += OnTutorialImageClicked;
                tutorialImage.Load(page.images[i]);

                var toggle = Instantiate(tutorialImageTogglePrefab, toggleContainer, false);
                toggle.toggle.group = toggleGroup;
                toggle.toggle.SetIsOnWithoutNotify(i == 0);
                toggle.SetIndex(i);
                toggle.OnClicked += OnImageToggled;
                m_toggles.Add( toggle );
            }
        }

        private void OnTutorialImageClicked()
        {
            IncrementImage();
        }

        private void OnImageToggled(int index)
        {
            SetCurrentImage(index);
        }

        private void IncrementImage()
        {
            SetCurrentImage(m_currentImage+1);

            if (m_currentImage >= m_totalImages)
            {
                SetCurrentImage(0);
            }
        }

        public void SetCurrentImage(int currImage)
        {
            m_currentImage = currImage;
            m_imageTimer = 0;
            m_imageStartX = tutorialImageContainer.anchoredPosition.x;
            m_easingTimer = 0;

            if (m_toggles.Count > m_currentImage)
            {
                m_toggles[m_currentImage].toggle.isOn = true;
            }
        }

        private void OnExit()
        {
            CloseScene();

            disableAllOverlay.SetActive(true);
            background.DOFade(0, 0.5f);
            container.DOAnchorPosY(-360f, 0.5f).SetEase(Ease.InBack);
        }

        private async void CloseScene()
        {
            await Task.Delay(500);
            await SceneController.Instance.UnloadScene("TutorialPopup");
        }

        public Task OnCreated(SceneModel sceneModel)
        {
            return Task.CompletedTask;
        }

        public void OnFocused(bool hasFocus)
        {

        }

        public void OnSceneLoaded(SceneModel model)
        {

        }

        public void OnSceneUnLoaded(SceneModel model)
        {

        }

        public void SetAsOverlay()
        {
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;
        }
    }
}