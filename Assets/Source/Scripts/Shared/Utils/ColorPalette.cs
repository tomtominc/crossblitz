﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using CrossBlitz.Card;

public static class ColorPalette
{
    public static class CrossBlitz
    {
        public static class AbilityKeywordColor
        {
            public const string BATTLECRY = "#F49648";
            public const string MULTI_ATTACK = "#1BC0BC";
            public const string ENRAGE = "#DF2A2A";
            public const string PIERCE = "#fcbc3b";
            public const string SILENCE = "#c3d8e9";
            public const string THORNS = "#d2d174";
            public const string SPLASH = "#6678f4";
            public const string LIFESTEAL = "#e65151";
            public const string FREEZE = "#8debf6";
            public const string ARMOR_BODY = "#42d783";
            public const string FLYING = "#67c2da";
            public const string POISON = "#d067f4";
        }

        public static class ClassColor
        {
            public const string DEMON = "#A41B00";
            public const string MAGE = "#6400AB";
            public const string PIRATE = "#4B85B9";
            public const string DRAGON = "#2B3257";
            public const string CHAMPION = "#D1945A";
            public const string BEAST = "#8F4D2B";
            public const string BRAWLER = "#385AC6";
            public const string SPROUT_ELF = "#568E34";
            public const string GOLEM = "#788788";
            public const string PYRO = "#dd412c";
            public const string FROST = "#329ab3";
            public const string TERRA = "#137c54";
            public const string MAGITEK = "#ce931c";
            public const string CHAKRA = "#e0527a";
            public const string DRACONIC = "#aa29d6";
        }

        public static string GetClassColor(Class @class)
        {
            switch (@class)
            {
                case Class.Demon:
                    return ClassColor.DEMON;
                case Class.Mage:
                    return ClassColor.MAGE;
                case Class.Pirate:
                    return ClassColor.PIRATE;
                case Class.Dragon:
                    return ClassColor.DRAGON;
                case Class.Champion:
                    return ClassColor.CHAMPION;
                case Class.SproutElf:
                    return ClassColor.SPROUT_ELF;
                case Class.Brawler:
                    return ClassColor.BRAWLER;
                case Class.Golem:
                    return ClassColor.GOLEM;
                case Class.Pyro:
                    return ClassColor.PYRO;
                case Class.Frost:
                    return ClassColor.FROST;
                case Class.Terra:
                    return ClassColor.TERRA;
                case Class.Magitek:
                    return ClassColor.MAGITEK;
                case Class.Chakra:
                    return ClassColor.CHAKRA;
                case Class.Draconic:
                    return ClassColor.DRACONIC;
                default:
                    return FactionColor.ALL;
            }
        }

        public static string GetTypeFilterColor(TypeFilter filter)
        {
            switch (filter)
            {
                case TypeFilter.All:
                    return "#b29678";
                case TypeFilter.Minion:
                case TypeFilter.Melee:
                case TypeFilter.Arcane:
                case TypeFilter.Ranged:
                    return "#e13229";
                case TypeFilter.Spell:
                    return "#af31f2";
                case TypeFilter.Trick:
                    return "#619b45";
                case TypeFilter.Power:
                    return "#c050a2";
            }

            return "#b29678";
        }

        public static string GetRarityFilterColor(Rarity rarity)
        {
            switch (rarity)
            {
                case Rarity.All:
                    return "#AE977A";
                case Rarity.Common:
                    return "#8FAAB3";
                case Rarity.Rare:
                    return "#6380D4";
                case Rarity.Mythic:
                    return "#B466FF";
                case Rarity.Legendary:
                    return "#EFAC28";
            }

            return "#b29678";
        }

        public static string GetCardSetFilterColor(CardSet cardset)
        {
            switch (cardset)
            {
                //case CardSet.All:
                //    return "#AE977A";
                case CardSet.Core:
                    return "#8B6355";
                case CardSet.PiratesAhoy:
                    return "#AB3B5C";
                case CardSet.StarscapeTwilight:
                    return "#A220DF";
                case CardSet.PerilousPilfering:
                    return "#BC8138";
                case CardSet.MonksMantra:
                    return "#5F5CC7";
                case CardSet.SproutwoodGuardian:
                    return "#848315";
                default:
                    return "#AE977A";
            }

            return "#b29678";
        }

        public static class FactionColor
        {
            public const string WAR = "#b75252";
            public const string NATURE = "#7ea63f";
            public const string FORTUNE = "#efac28";
            public const string BALANCE = "#638ab6";
            public const string CHAOS = "#784b9b";
            public const string NEUTRAL = "#7a908a";
            public const string ALL = "#ae977a";

            public const string WAR_LIGHT = "#FCF2BF";
            public const string NATURE_LIGHT = "#E1EBC7";
            public const string FORTUNE_LIGHT = "#F4EFAE";
            public const string BALANCE_LIGHT = "#E1F8F1";
            public const string CHAOS_LIGHT = "#E0DFFF";
            public const string NEUTRAL_LIGHT = "#ffffff";
            public const string ALL_LIGHT = "#ffffff";
        }


        public static string GetFactionColor(Faction faction)
        {
            switch (faction)
            {
                case Faction.War:
                    return FactionColor.WAR;
                case Faction.Nature:
                    return FactionColor.NATURE;
                case Faction.Fortune:
                    return FactionColor.FORTUNE;
                case Faction.Balance:
                    return FactionColor.BALANCE;
                case Faction.Chaos:
                    return FactionColor.CHAOS;
                case Faction.Neutral:
                    return FactionColor.NEUTRAL;
                default:
                    return FactionColor.ALL;
            }
        }

        public static string GetFactionColorMedium(Faction faction)
        {
            switch (faction)
            {
                case Faction.War:
                    return "#eea65d";
                case Faction.Nature:
                    return FactionColor.NATURE_LIGHT;
                case Faction.Fortune:
                    return FactionColor.FORTUNE_LIGHT;
                case Faction.Balance:
                    return FactionColor.BALANCE_LIGHT;
                case Faction.Chaos:
                    return FactionColor.CHAOS_LIGHT;
                case Faction.Neutral:
                    return FactionColor.NEUTRAL_LIGHT;
                default:
                    return FactionColor.ALL_LIGHT;
            }
        }

        public static string GetFactionColorLight(Faction faction)
        {
            switch (faction)
            {
                case Faction.War:
                    return FactionColor.WAR_LIGHT;
                case Faction.Nature:
                    return FactionColor.NATURE_LIGHT;
                case Faction.Fortune:
                    return FactionColor.FORTUNE_LIGHT;
                case Faction.Balance:
                    return FactionColor.BALANCE_LIGHT;
                case Faction.Chaos:
                    return FactionColor.CHAOS_LIGHT;
                case Faction.Neutral:
                    return FactionColor.NEUTRAL_LIGHT;
                default:
                    return FactionColor.ALL_LIGHT;
            }
        }

    }
}
