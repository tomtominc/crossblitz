﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ionic.Zlib;
using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class Crypto
{
    private const string ENCRYPT_PASSWORD = "5uUdJ#jgh&*e+QVbX=NavP@M^ePMe@WGHe8@Uhec27matwe-r7LNNGwqn_VvkQ-kAWuTJFnemUEVrfNJ$6juL$kVj8%V7WTt%ADrH3#VEpBv9CyyATK$cQa_?&JWMN=WBGTQw_=RCpS4TR-CawmaZ+d8V-5CTy7mv&C-NqL6WFt_EPEx$x5r7R+*5DaMkJN+?^xJn9$h=pR@==Lq3G%Z_sHct%#HW_xm=JJdTwPdwAKjmbEeFQddX7JUjsw6&X$Q";
    private const string ENCRYPT_SALT = "=ru2ywWRbCMCKx!Qw!66FX^Ux2CRX8Bn$wwxN$r+4&k+Zhn952fvVgEFr_DZhtbydZNqnhrf6?jVh$E2q_uHg3r*mrq@wdN9v^PzMH$QFthhH4Mt-n4j64L5hhcMs=eduMKWC4U%p$-J2T24=rkAB5^AHyJwPcYMuqu*xujY7eChp!Hp2jX&Hb+BLp$$jBDfJzVNY=GE-=@6Q_5fJN?$Fhpfeb4WY-g+PZWpd5!!w6-&Qny6%^QCE!8$nnU5FBQV";
    private const string ENCRYPT_VI = "@VQch+XD#=Wt3j-K";

    public static string ToBinary(this string data)
    {
        StringBuilder sb = new StringBuilder();

        foreach (char c in data.ToCharArray())
        {
            sb.Append(Convert.ToString(c, 2).PadLeft(8, '0'));
        }
        return sb.ToString();
    }

    public static string FromBinary(this string data)
    {
        List<Byte> byteList = new List<Byte>();

        for (int i = 0; i < data.Length; i += 8)
        {
            byteList.Add(Convert.ToByte(data.Substring(i, 8), 2));
        }
        return Encoding.ASCII.GetString(byteList.ToArray());
    }

    public static string Zip(this string json)
    {
        byte[] arr = GZipStream.CompressString(json);
        return Convert.ToBase64String(arr);
    }

    public static string UnZip(this string zipped)
    {
        byte[] bytes = Convert.FromBase64String(zipped);
        return GZipStream.UncompressString(bytes);
    }

    public static string Encrypt(this string text)
    {
        if (string.IsNullOrEmpty(text))
        {
            return string.Empty;
        }

        text = text.Zip();

        byte[] plainTextBytes = Encoding.UTF8.GetBytes(text);
        byte[] keyBytes = new Rfc2898DeriveBytes(ENCRYPT_PASSWORD, Encoding.ASCII.GetBytes(ENCRYPT_SALT)).GetBytes(256 / 8);
        var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC };
        var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(ENCRYPT_VI));

        byte[] cipherTextBytes;

        using (var memoryStream = new MemoryStream())
        {
            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
            {
                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                cryptoStream.FlushFinalBlock();
                cipherTextBytes = memoryStream.ToArray();
                cryptoStream.Close();
            }
            memoryStream.Close();
        }
        return Convert.ToBase64String(cipherTextBytes);
    }

    public static string Decrypt(this string encryptedText)
    {
        if (string.IsNullOrEmpty(encryptedText))
        {
            return string.Empty;
        }

        try
        {
            byte[] cipherTextBytes = Convert.FromBase64String(encryptedText);
            byte[] keyBytes = new Rfc2898DeriveBytes(ENCRYPT_PASSWORD, Encoding.ASCII.GetBytes(ENCRYPT_SALT)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC };

            var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(ENCRYPT_VI));
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();

            encryptedText = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
            return encryptedText.UnZip();
        }
        catch
        {
            return string.Empty;
        }
    }

    public static string CompressThenEncrypt(this string text)
    {
        byte[] plainTextBytes = Encoding.UTF8.GetBytes(text);
        byte[] keyBytes = new Rfc2898DeriveBytes(ENCRYPT_PASSWORD, Encoding.ASCII.GetBytes(ENCRYPT_SALT)).GetBytes(128 / 8);
        var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC };
        var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(ENCRYPT_VI));

        byte[] cipherTextBytes;

        using (var memoryStream = new MemoryStream())
        {
            using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress))
            {
                using (var cryptoStream = new CryptoStream(gZipStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }

                gZipStream.Close();
            }

            memoryStream.Close();
        }

        return Convert.ToBase64String(cipherTextBytes);
    }

    public static string DecryptThenDecompress(this string encryptedText)
    {
        byte[] cipherTextBytes = Convert.FromBase64String(encryptedText);
        byte[] keyBytes = new Rfc2898DeriveBytes(ENCRYPT_PASSWORD, Encoding.ASCII.GetBytes(ENCRYPT_SALT)).GetBytes(128 / 8);
        var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };
        var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(ENCRYPT_VI));

        using (var memoryStream = new MemoryStream(cipherTextBytes))
        {
            using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
            {
                using (var gZipStream = new GZipStream(cryptoStream, CompressionMode.Decompress))

                {
                    byte[] plainTextBytes = new byte[cipherTextBytes.Length];
                    gZipStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                    //  gZipStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();

                    gZipStream.Close();
                }

                cryptoStream.Close();
            }
            memoryStream.Close();
        }


        return Convert.ToBase64String(cipherTextBytes);
    }
}
