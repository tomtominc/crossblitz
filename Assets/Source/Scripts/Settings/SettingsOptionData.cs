using System;
using System.Collections.Generic;
using Mono.CSharp;

namespace CrossBlitz.Settings
{
    public enum SettingOptionType
    {
        None,
        Button,
        Options,
    }

    public class SettingsOptionData
    {
        public SettingOptionType optionType;
        public string optionName;
        public MenuContext linkedMenu;
        public int currentOption;
        public List<string> options;
        public Action<SettingsOptionData> onOptionClicked;
        public Action<SettingsOptionData> onOptionToggleChanged;
        public bool lineBreak;

        public SettingsOptionData()
        {
            optionType = SettingOptionType.None;
            optionName = string.Empty;
            currentOption = 0;
            options = new List<string>();
            lineBreak = false;
        }
    }
}