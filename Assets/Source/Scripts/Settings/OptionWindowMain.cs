using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CrossBlitz.ViewAPI;
using UnityEngine.UI;
using MEC;
using DG.Tweening;
using CrossBlitz.AddressablesAPI;

namespace CrossBlitz
{
    [Obsolete("Use SettingsWindow")]
    public class OptionWindowMain : View
    {
        private CanvasGroup globalFadeGroup;
        public Transform optionsWindowTransform;
        public Button backButton;
        public bool onMainMenu = false;

        private void Awake()
        {
            globalFadeGroup = GetComponent<CanvasGroup>();

            backButton.RectTransform().anchoredPosition = new Vector2(-60f, 0f);
            optionsWindowTransform.RectTransform().anchoredPosition = new Vector2(0f, -60f);
            globalFadeGroup.alpha = 0;

            backButton.onClick.AddListener(OnBackButton);

            Open();
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public override async void Open()
        {
            optionsWindowTransform.RectTransform().DOAnchorPosY(0, 0.24f).SetEase(Ease.OutBack);
            globalFadeGroup.DOFade(1, 0.24f);

            backButton.RectTransform().DOAnchorPosX(0, 0.24f).SetEase(Ease.OutBack);
        }

        public async void CloseOptionsWindow()
        {
            backButton.RectTransform().DOAnchorPosX(-60f, 0.24f).SetEase(Ease.InBack);
            optionsWindowTransform.RectTransform().DOAnchorPosY(-60f, 0.24f).SetEase(Ease.InBack);
            globalFadeGroup.DOFade(0, 0.24f);

            await System.Threading.Tasks.Task.Delay(400);
            await SceneController.Instance.UnloadScene("OptionsWindow");
        }

        private void OnBackButton()
        {
            backButton.interactable = false;
            CloseOptionsWindow();
        }

    }
}
