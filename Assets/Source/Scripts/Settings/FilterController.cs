using System;
using UnityEngine;

namespace CrossBlitz.Settings
{
    public class FilterController : MonoBehaviour
    {
        private static FilterController m_instance;
        public static FilterController Instance
        {
            get
            {
                if (m_instance == null)
                {
                    m_instance = FindObjectOfType<FilterController>();
                }

                return m_instance;
            }
        }

        public CameraFilterPack_TV_SCANLINES scanlines;
        public CameraFilterPack_TV_ARCADE arcade;
        public CameraFilterPack_Color_Chromatic_Aberration chromaticAberration;
        public float filterIntensity;

        private void Update()
        {
            if (chromaticAberration.Offset != 0)
            {
                chromaticAberration.Offset = Mathf.MoveTowards(chromaticAberration.Offset, 0, Time.deltaTime * 0.1f);
            }
        }

        public static void HitWithChromaticAberration()
        {
            m_instance.chromaticAberration.enabled = true;
            m_instance.chromaticAberration.Offset = 0.01f;
        }
    }
}