using System;
using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.Databases;
using CrossBlitz.Hero;
using CrossBlitz.Items.Data;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using DG.Tweening;
using MEC;
using Source.Scripts.Card;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.Settings;

namespace CrossBlitz.OptionsManager
{

    public enum OptionTypeEnum
    {
        Default,
        Link,
        Arrow,
        Resume,
        MainMenu,
        QuitGame,
        Difficulty,
        GameSpeed,
        HealthAdj,
        PowerAdj,
        CtrlType,
        CtrlDetect,
        Resolution,
        WindowType,
        VSync,
        CRTFilter,
        MasterVol,
        MusicVol,
        SFXVol,
        Back,
        PlayTime,
        ResetData,
        CardsCollected
    };

    [Obsolete("Use SettingsOption")]
    public class OptionsWindowManager : MonoBehaviour,

        IPointerClickHandler,
        IPointerEnterHandler,
        IPointerExitHandler
    {

        public String optionName;
        public OptionTypeEnum OptionType = new OptionTypeEnum();
        public GameObject optionLink;
        public TextMeshProUGUI optionLabel;
        public TextMeshProUGUI optionValueLabel;
        public String[] optionValues;
        public Image image;
        public Button leftButton;
        public Button rightButton;

        private OptionWindowMain optionMain;
        private Transform optionMainTransform;
        private CanvasGroup optionMainCanvas;

        private Transform parentGameObject;
        private CanvasGroup parentCanvas;
        private CanvasGroup linkParentCanvas;

        private Color highlightColor = new Color32(231, 237, 181, 255);
        private Color defaultColor = new Color32(139, 99, 85, 255);
        private Color imageAlphaOn = new Color32(255, 255, 255, 255);
        private Color imageAlphaOff = new Color32(255, 255, 255, 0);

        private float optionSetting;
        private int optionIndexMax;
        private int optionIndexMin;
        private int optionIndexCurr;

        private Tweener _tweener;
        private bool hover = false;
        private int transitionDir = 1;
        private bool exitMenu = false;
        public bool onMainMenu = false;

        private void Start()
        {
            if (!App.Initialized)
            {
                App.Init();
            }

            parentGameObject = this.transform.parent.transform.parent;
            parentCanvas = parentGameObject.GetComponent<CanvasGroup>();

            optionMainTransform = parentGameObject.transform.parent.transform.parent;
            optionMain = optionMainTransform.GetComponent<OptionWindowMain>();
            optionMainCanvas = optionMainTransform.GetComponent<CanvasGroup>();
            onMainMenu = optionMain.onMainMenu;

            if (optionLink != null)
            {
                linkParentCanvas = optionLink.GetComponent<CanvasGroup>();
                if(OptionType == OptionTypeEnum.Back)
                {
                    transitionDir = -1;
                }
            }

            optionLabel.text = optionName;

            leftButton.onClick.AddListener(OnLeftButton);
            rightButton.onClick.AddListener(OnRightButton);

            if (optionValues.Length > 0)
            {
                optionIndexMax = optionValues.Length - 1;
                optionIndexMin = 0;
                optionIndexCurr = 0;
                optionValueLabel.text = optionValues[optionIndexCurr];
            }

            //if (!parentGameObject.IsActive())
            //{
            //    hover = false;
            //}

            InitOptionValue();
        }


        private void Update()
        {
            if (_tweener != null && _tweener.IsActive())
            {
                optionLabel.color = highlightColor;
                optionValueLabel.color = highlightColor;
                //image.enabled = true;
                image.color = imageAlphaOn;
            }
            else
            {
                if (!hover)
                {
                    optionLabel.color = defaultColor;
                    optionValueLabel.color = defaultColor;
                    //image.enabled = false;
                    image.color = imageAlphaOff;
                }
            }

            if (optionIndexCurr == optionIndexMin)
            {
                leftButton.interactable = false;
            }
            else
            {
                leftButton.interactable = true;
            }

            if (optionIndexCurr == optionIndexMax)
            {
                rightButton.interactable = false;
            }
            else
            {
                rightButton.interactable = true;
            }

            if(this.IsActive())
            {
                if (OptionType == OptionTypeEnum.Link)
                {
                    if (optionMain.onMainMenu && optionLink.name == "ReturnTitle")
                    {
                        this.SetActive(false);
                    }
                }
            }

            if(OptionType == OptionTypeEnum.PlayTime)
            {
                SetOptionValue();
            }

        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (exitMenu) return;

            switch (OptionType)
            {

                case OptionTypeEnum.Link:
                case OptionTypeEnum.Back:
                    Timing.RunCoroutine(AnimateTransition());
                    break;

                case OptionTypeEnum.Resume:
                    exitMenu = true;
                    optionMain.CloseOptionsWindow();
                    break;


                case OptionTypeEnum.ResetData:
                    exitMenu = true;
                    optionMain.CloseOptionsWindow();
                    SettingsManager.ResetAllSavedData();
                    break;


                case OptionTypeEnum.QuitGame:
                    optionLabel.color = defaultColor;
                    optionValueLabel.color = defaultColor;
                    image.color = imageAlphaOff;

                    SettingsManager.QuitGame();

                    break;

                default:
                    break;
            }
        }


        public void OnPointerEnter(PointerEventData eventData)
        {
            if (exitMenu) return;

            if (_tweener == null || !_tweener.IsActive())
            {
                _tweener = this.RectTransform().DOPunchAnchorPos(Vector2.down * 2f, 0.15f);
            }
            hover = true;

        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (exitMenu) return;

            _tweener.Kill(true);

            hover = false;

            optionLabel.color = defaultColor;
            optionValueLabel.color = defaultColor;
            image.color = imageAlphaOff;
        }

        private void OnRightButton()
        {
            if(optionIndexCurr < optionIndexMax)
            {
                optionIndexCurr += 1;
                this.RectTransform().DOPunchAnchorPos(Vector2.right * 2f, 0.15f);
                SetOptionValue();
            }
            else
            {
                this.RectTransform().DOShakePosition(0.15f);
            }

        }

        private void OnLeftButton()
        {
            if (optionIndexCurr > optionIndexMin)
            {
                optionIndexCurr -= 1;
                this.RectTransform().DOPunchAnchorPos(Vector2.left * 2f, 0.15f);
                SetOptionValue();
            }
            else
            {
                this.RectTransform().DOShakePosition(0.15f);
            }

        }

        private void InitOptionValue()
        {

            switch (OptionType)
            {
                //case OptionTypeEnum.Link:
                //    if (optionMain.onMainMenu && optionLink.name == "ReturnTitle")
                //    {
                //        this.SetActive(false);
                //    }
                //    break;

                case OptionTypeEnum.Difficulty:
                    optionValueLabel.text = optionValues[optionIndexCurr];
                    break;

                case OptionTypeEnum.GameSpeed:
                    optionValueLabel.text = "x" + optionValues[optionIndexCurr];
                    break;

                case OptionTypeEnum.HealthAdj:
                    optionValueLabel.text = "+" + optionValues[optionIndexCurr];
                    break;

                case OptionTypeEnum.PowerAdj:
                    optionValueLabel.text = "+" + optionValues[optionIndexCurr];
                    break;

                case OptionTypeEnum.CtrlType:
                    optionValueLabel.text = optionValues[optionIndexCurr];
                    break;

                case OptionTypeEnum.CtrlDetect:
                    optionValueLabel.text = optionValues[optionIndexCurr];
                    break;

                case OptionTypeEnum.Resolution:
                    optionIndexCurr = App.Settings.GetResolutionScale() - 1;
                    optionValueLabel.text = optionValues[optionIndexCurr];
                    break;

                case OptionTypeEnum.WindowType:
                    optionIndexCurr = ((int)App.Settings.GetScreenMode());
                    optionValueLabel.text = optionValues[optionIndexCurr];
                    break;

                case OptionTypeEnum.VSync:
                    optionValueLabel.text = optionValues[optionIndexCurr];
                    break;

                case OptionTypeEnum.CRTFilter:
                    optionValueLabel.text = optionValues[optionIndexCurr];
                    break;

                case OptionTypeEnum.MasterVol:
                    optionIndexCurr = Convert.ToInt32(App.Settings.GetMasterVolume() * 10);
                    optionValueLabel.text = optionValues[optionIndexCurr] + "%";
                    break;

                case OptionTypeEnum.MusicVol:
                    optionIndexCurr = Convert.ToInt32(App.Settings.GetMusicVolume() * 10);
                    optionValueLabel.text = optionValues[optionIndexCurr] + "%";
                    break;

                case OptionTypeEnum.SFXVol:
                    optionIndexCurr = Convert.ToInt32(App.Settings.GetSfxVolume() * 10) ;
                    optionValueLabel.text = optionValues[optionIndexCurr] + "%";
                    break;

                case OptionTypeEnum.PlayTime:
                    //var timePlayedLong = (long) App.AccountInfo.TimePlayedTotal;
                    var timePlayed = new TimeSpan(0,0,0, (int)App.AccountInfo.GetTimePlayedTotal());
                    optionValueLabel.text = timePlayed.ToString("g");
                    break;

                case OptionTypeEnum.CardsCollected:
                    var cardsOwned = 0;
                    var cardsTotal = Db.CardDatabase.GetCollectableCardCount();
                    for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
                    {
                        var amountOwned = App.Inventory.GetItemAmount(Db.CardDatabase.Cards[i].id);
                        if (amountOwned > 0)
                        {
                            cardsOwned++;
                        }
                    }

                    optionValueLabel.text = $"{cardsOwned} / {cardsTotal}";
                    break;
            }
        }

        private void SetOptionValue()
        {

            switch (OptionType)
            {
                case OptionTypeEnum.Difficulty:
                    optionValueLabel.text = optionValues[optionIndexCurr];
                    break;

                case OptionTypeEnum.GameSpeed:
                    optionValueLabel.text = "x" + optionValues[optionIndexCurr];
                    break;

                case OptionTypeEnum.HealthAdj:
                    optionValueLabel.text = "+" + optionValues[optionIndexCurr];
                    break;

                case OptionTypeEnum.PowerAdj:
                    optionValueLabel.text = "+" + optionValues[optionIndexCurr];
                    break;

                case OptionTypeEnum.CtrlType:
                    optionValueLabel.text = optionValues[optionIndexCurr];
                    break;

                case OptionTypeEnum.CtrlDetect:
                    optionValueLabel.text = optionValues[optionIndexCurr];
                    break;

                case OptionTypeEnum.Resolution:
                    optionValueLabel.text = optionValues[optionIndexCurr];
                    SettingsManager.SetResolution(optionIndexCurr+1);
                    break;

                case OptionTypeEnum.WindowType:

                    optionValueLabel.text = optionValues[optionIndexCurr];

                    FullScreenMode screenMode;

                    switch (optionIndexCurr)
                    {
                        case 0:
                            screenMode = FullScreenMode.ExclusiveFullScreen;
                            break;

                        case 1:
                            screenMode = FullScreenMode.FullScreenWindow;
                            break;

                        case 2:
                            screenMode = FullScreenMode.MaximizedWindow;
                            break;
                        case 3:
                            screenMode = FullScreenMode.Windowed;
                            break;

                        default:
                            screenMode = FullScreenMode.Windowed;
                            break;
                    }
                    SettingsManager.SetWindowMode(screenMode);
                    break;

                case OptionTypeEnum.VSync:
                    optionValueLabel.text = optionValues[optionIndexCurr];
                    break;

                case OptionTypeEnum.CRTFilter:
                    optionValueLabel.text = optionValues[optionIndexCurr];
                    break;

                case OptionTypeEnum.MasterVol:
                    optionValueLabel.text = optionValues[optionIndexCurr] + "%";
                    float masterVol = optionIndexCurr / 10f;
                    SettingsManager.SetMasterVolume(masterVol);
                    break;

                case OptionTypeEnum.MusicVol:
                    optionValueLabel.text = optionValues[optionIndexCurr] + "%";
                    float musicVol = optionIndexCurr/10f;
                    SettingsManager.SetMusicVolume(musicVol);
                    break;

                case OptionTypeEnum.SFXVol:
                    optionValueLabel.text = optionValues[optionIndexCurr] + "%";
                    float sfxVol = optionIndexCurr / 10f;
                    SettingsManager.SetSfxVolume(sfxVol);
                    break;

                case OptionTypeEnum.PlayTime:
                    optionValueLabel.text = App.AccountInfo.GetTimePlayedTotal().ToString("hh:mm:ss");
                    break;

                default:
                    break;
            }
        }

        private IEnumerator<float> AnimateTransition()
        {
            yield return Timing.WaitUntilDone(WindowFadeOut());
            yield return Timing.WaitUntilDone(WindowFadeIn());
        }

        private IEnumerator<float> WindowFadeOut()
        {
            // Animate
            parentCanvas.DOFade(0, 0.12f);
            parentCanvas.RectTransform().DOPunchAnchorPos(Vector2.right * 4f * transitionDir, 0.24f);
            yield return Timing.WaitForSeconds(0.12f);

            // Reset parameters
            optionLabel.color = defaultColor;
            optionValueLabel.color = defaultColor;
            image.color = imageAlphaOff;
            parentCanvas.alpha = 1;
            linkParentCanvas.alpha = 0;

            // Deactivate/Activate menus
            optionLink.SetActive(true);
            parentGameObject.SetActive(false);

            yield break;
        }

        private IEnumerator<float> WindowFadeIn()
        {

            optionLink.RectTransform().DOPunchAnchorPos(Vector2.left * 4f * transitionDir, 0.24f);
            yield return Timing.WaitForSeconds(0.12f);
            linkParentCanvas.DOFade(1, 0.12f);
            yield return Timing.WaitForSeconds(0.12f);

            yield break;
        }
    }
}
