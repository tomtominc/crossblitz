using Asyncoroutine;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Cursors;
using CrossBlitz.Databases;
using CrossBlitz.Fables;
using CrossBlitz.Fables.Battle;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.Transition;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace CrossBlitz.Settings
{
    public static class SettingsManager
    {
        public static void SetResolution(int scale)
        {
            if (!App.Initialized)
            {
                App.Init();
            }

            App.Settings.SetResolutionScale(scale);

            var resolution = new Vector2Int(640, 360) * App.Settings.GetResolutionScale();
            Screen.SetResolution(resolution.x, resolution.y, App.Settings.GetScreenMode());
            CrossBlitzInputModule.Instance.OnResolutionChanged();
        }

        public static void SetWindowMode(FullScreenMode screenMode)
        {
            if (!App.Initialized)
            {
                App.Init();
            }

            App.Settings.SetScreenMode(screenMode);

            var resolution = new Vector2Int(640, 360) * App.Settings.GetResolutionScale();
            Screen.SetResolution(resolution.x, resolution.y, App.Settings.GetScreenMode());
            CrossBlitzInputModule.Instance.OnResolutionChanged();
        }

        public static void SetFilterMode(CrtFilterOption filter)
        {
            if (!App.Initialized)
            {
                App.Init();
            }

            App.Settings.SetFilterOption(filter);

            switch (App.Settings.GetFilterOption())
            {
                case CrtFilterOption.Off:
                    FilterController.Instance.scanlines.enabled = false;
                    FilterController.Instance.arcade.enabled = false;
                    break;
                case CrtFilterOption.ScanLines:
                    FilterController.Instance.scanlines.enabled = true;
                    FilterController.Instance.arcade.enabled = false;
                    break;
                case CrtFilterOption.Arcade:
                    FilterController.Instance.scanlines.enabled = false;
                    FilterController.Instance.arcade.enabled = true;
                    break;
            }
        }

        public static void SetFilterIntensity(int intensity)
        {
            if (!App.Initialized)
            {
                App.Init();
            }

            App.Settings.SetFilterIntensity(intensity);
            FilterController.Instance.scanlines.Fade = (intensity+1) *.2f;
            FilterController.Instance.arcade.Fade = (intensity+1) * .2f;
        }

        public static void SetMasterVolume(float volume)
        {
            if (!App.Initialized)
            {
                App.Init();
            }

            App.Settings.SetMasterVolume(volume);
            AudioController.SetMasterVolume(App.Settings.GetMasterVolume());
        }

        public static void SetMusicVolume(float volume)
        {
            if (!App.Initialized)
            {
                App.Init();
            }

            App.Settings.SetMusicVolume(volume);
            AudioController.SetMusicVolume(App.Settings.GetMusicVolume());
        }

        public static void SetSfxVolume(float volume)
        {
            if (!App.Initialized)
            {
                App.Init();
            }

            App.Settings.SetSfxVolume(volume);
            AudioController.SetSoundVolume(App.Settings.GetSfxVolume());
        }

        public static void SetAmbienceVolume(float volume)
        {
            if (!App.Initialized)
            {
                App.Init();
            }

            App.Settings.SetAmbienceVolume(volume);
            AudioController.SetAmbienceVolume(App.Settings.GetAmbienceVolume());
        }

        public static async void ResetAllSavedData()
        {
            if (!App.Initialized)
            {
                App.Init();
            }

            App.ResetData();
            await SceneController.Instance.UnloadAllScenes();
            StateController.ChangeState(StateDefinition.INITIALIZE);
        }

        public static void ExportSaveData()
        {
//#if UNITY_EDITOR
            // string path = EditorUtility.OpenFolderPanel("Export Save Data", "", "");
            //
            // if(path != "")
            // {
            //     path += "/SaveFile.es3";
            //     ES3.CopyFile(Application.persistentDataPath + "/SaveFile.es3", path);
            // }
//#endif
        }

        public static async void ImportSaveData()
        {
#if UNITY_EDITOR
            // string sourcePath = EditorUtility.OpenFilePanel("Import Save Data", "", "es3");
            // string destPath = Application.persistentDataPath + "/SaveFile.es3";
            //
            // if (sourcePath != "")
            // {
            //     if(ES3.FileExists(destPath))
            //     {
            //         ES3.DeleteFile(destPath);
            //     }
            //
            //     ES3.CopyFile(sourcePath, destPath);
            //
            //     App.ResetGame();
            //     await SceneController.Instance.UnloadAllScenes();
            //     StateController.ChangeState(StateDefinition.INITIALIZE);
            // }
#endif
        }


        public static void UnlockAllCards()
        {
            var cardItems = new List<ItemValue>();

            for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
            {
                var card = Db.CardDatabase.Cards[i];
                var value = 0;

                switch (card.rarity)
                {
                    case Rarity.Common:
                        value = 8;
                        break;
                    case Rarity.Rare:
                        value = 4;
                        break;
                    case Rarity.Mythic:
                        value = 2;
                        break;
                    case Rarity.Legendary:
                        value = 1;
                        break;
                }

                cardItems.Add(new ItemValue { ItemUid = card.id, Count = value });

            }

            ClientInventory.GrantAndDisplayItems(cardItems, null, false);
        }


        public static void UnlimitedMoney()
        {

            var currentBook = App.FableData.GetCurrentBook();

            if (currentBook != null)
            {
                currentBook.AddDawnDollars(99999);
            }

        }

        public static void AllIngredients()
        {
            var ingredientsList = Crafting.GetIngredientsByFaction(Faction.Neutral);
            ingredientsList.AddRange(Crafting.GetIngredientsByFaction(Faction.Balance));
            ingredientsList.AddRange(Crafting.GetIngredientsByFaction(Faction.Chaos));
            ingredientsList.AddRange(Crafting.GetIngredientsByFaction(Faction.Fortune));
            ingredientsList.AddRange(Crafting.GetIngredientsByFaction(Faction.Nature));
            ingredientsList.AddRange(Crafting.GetIngredientsByFaction(Faction.War));

            var ingredientsAdded = new List<ItemValue>();

            for (var i = 0; i < ingredientsList.Count; i++)
            {
                var item = ingredientsList[i].Key;
                var value = 999;

                ingredientsAdded.Add(new ItemValue { ItemUid = item, Count = value });
            }

            ClientInventory.GrantAndDisplayItems(ingredientsAdded, null, false);
        }

        public static async void ReturnToMainMenu()
        {
            await TransitionController.TransitionIn(TransitionController.TransitionType.Fade);

            await System.Threading.Tasks.Task.Delay(200);

            AmbientController.Stop();
            FableController.SetChapter(string.Empty, -1, -1);
            App.BattleResults.ResetData();
            await SceneController.Instance.UnloadAllScenes();


            StateController.ChangeState(StateDefinition.MAIN_MENU);
        }

        public static void QuitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }
}