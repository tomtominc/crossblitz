using CrossBlitz.Audio;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace CrossBlitz.Settings
{
    public class SettingsOption : MonoBehaviour,
        IPointerEnterHandler,
        IPointerExitHandler
    {
        public RectTransform optionContainerRect;
        public CanvasGroup optionContainerCanvas;
        public Image optionContainer;
        public TextMeshProUGUI optionLabel;
        public Button optionButton;
        public string hoverSound = "Text-LetterB";

        [BoxGroup("Option Toggle")] public RectTransform optionToggleGroup;
        [BoxGroup("Option Toggle")] public TextMeshProUGUI optionToggleLabel;
        [BoxGroup("Option Toggle")] public Button optionToggleButtonLeft;
        [BoxGroup("Option Toggle")] public Button optionToggleButtonRight;
        [BoxGroup("Option Toggle")] public LayoutElement optionToggleSizer;


        private static Color highlightColor = new Color32(231, 237, 181, 255);
        private static Color defaultColor = new Color32(139, 99, 85, 255);
        private static Color imageAlphaOn = new Color32(255, 255, 255, 255);
        private static Color imageAlphaOff = new Color32(255, 255, 255, 0);

        private SettingsOptionData m_data;
        public SettingsOptionData Data => m_data;



        private void Start()
        {
            optionButton.onClick.AddListener(OptionButtonClicked);
            optionToggleButtonLeft.onClick.AddListener(OptionToggleButtonLeftClicked);
            optionToggleButtonRight.onClick.AddListener(OptionToggleButtonRightClicked);
        }


        public void SetData(SettingsOptionData data, int optionIndex)
        {
            m_data = data;
            UpdateView();

            optionContainerCanvas.alpha = 0;
            optionContainerRect.anchoredPosition = new Vector2(20, 0);

            optionContainerCanvas.DOFade(1, 0.12f)
                .SetDelay(optionIndex * 0.06f);

            optionContainerRect.DOAnchorPos(Vector2.zero, 0.24f)
                .SetEase(Ease.OutBack).SetDelay(optionIndex * 0.06f);
        }

        private void Update()
        {
            if (m_data != null && m_data.optionType == SettingOptionType.Options)
            {
                optionToggleSizer.minWidth = 275f - (optionLabel.RectTransform().rect.width + 128f);
            }
        }

        public void UpdateView()
        {
            if (m_data.lineBreak)
            {
                gameObject.RectTransform().SetHeight(12f);
                optionContainerRect.gameObject.SetActive(false);
            }

            optionLabel.text = m_data.optionName;

            switch (m_data.optionType)
            {
                case SettingOptionType.Button:
                    optionToggleGroup.SetActive(false);
                    optionToggleSizer.SetActive(false);
                    optionButton.SetActive(true);
                    break;
                case SettingOptionType.Options:
                    optionToggleGroup.SetActive(true);
                    optionToggleSizer.SetActive(true);
                    optionButton.SetActive(false);
                    RefreshOptionToggle();
                    break;
            }

        }

        private void OptionButtonClicked()
        {
            m_data.onOptionClicked?.Invoke(m_data);
        }

        private void OptionToggleButtonLeftClicked()
        {
            if (!DOTween.IsTweening(optionContainerRect))
            {
                optionContainerRect.DOPunchAnchorPos(Vector2.left * 4, 0.15f);
            }

            m_data.currentOption--;
            RefreshOptionToggle();
            m_data.onOptionToggleChanged?.Invoke(m_data);
        }

        private void OptionToggleButtonRightClicked()
        {
            if (!DOTween.IsTweening(optionContainerRect))
            {
                optionContainerRect.DOPunchAnchorPos(Vector2.right * 4, 0.15f);
            }

            m_data.currentOption++;
            RefreshOptionToggle();
            m_data.onOptionToggleChanged?.Invoke(m_data);
        }

        private void RefreshOptionToggle()
        {
            optionToggleLabel.text = m_data.options[m_data.currentOption];

            if (m_data.options.Count <= 1)
            {
                optionToggleButtonLeft.SetActive(false);
                optionToggleButtonRight.SetActive(false);
                return;
            }

            optionToggleButtonLeft.SetActive(true);
            optionToggleButtonRight.SetActive(true);

            optionToggleButtonLeft.interactable = m_data.currentOption > 0;
            optionToggleButtonRight.interactable = m_data.currentOption < m_data.options.Count - 1;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!DOTween.IsTweening(optionContainerRect))
            {
                optionContainerRect.DOPunchAnchorPos(Vector2.down * 2, 0.15f);
            }

            AudioController.PlaySound(hoverSound);
            optionLabel.color = highlightColor;
            optionToggleLabel.color = highlightColor;
            optionContainer.color = imageAlphaOn;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            optionLabel.color = defaultColor;
            optionToggleLabel.color = defaultColor;
            optionContainer.color = imageAlphaOff;
        }
    }
}