using System;
using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Cursors;
using CrossBlitz.Databases;
using CrossBlitz.Fables;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using CrossBlitz.Transition;
using CrossBlitz.ViewAPI;
using DG.Tweening;
using TMPro;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Settings
{
    public enum SettingsContext
    {
        Default,
        TitleScreen,
        Fables,
        Game,
    }

    public enum MenuContext
    {
        Home,
        GameSettings,
        GameOptions,
        Display,
        Audio,
        PlayerData,
        PlayerStatistics,
        CollectionMenuSettings,
        FablesSettings,
        ResetAllDataConfirmation,
        QuitGameConfirmation,
        DeveloperOptions,
        RestartBattleConfirmation,
        ForfeitMatchConfirmation,
        ReturnToMainMenuConfirmation,
        ReturnToChapterSelectConfirmation,
        DeveloperFables,
        DeveloperBattles,
        DeveloperInventory,
        BattlesSettings,
        PlayTimeStatistics,
        RedcroftPlayTime,
        RedcroftPlayTimeCh1,
        RedcroftPlayTimeCh2,
        RedcroftPlayTimeCh3,
        VioletPlayTime,
        VioletPlayTimeCh1,
        VioletPlayTimeCh2,
        VioletPlayTimeCh3,
        QuillPlayTime,
        QuillPlayTimeCh1,
        QuillPlayTimeCh2,
        QuillPlayTimeCh3,
        SetoPlayTime,
        SetoPlayTimeCh1,
        SetoPlayTimeCh2,
        SetoPlayTimeCh3,
        MereenaPlayTime,
        MereenaPlayTimeCh1,
        MereenaPlayTimeCh2,
        MereenaPlayTimeCh3,
    }

    public class SettingsWindow : View
    {
        public RectTransform window;
        public CanvasGroup canvasGroup;

        public TextMeshProUGUI title;
        public RectTransform optionsLayout;
        public SettingsOption optionPrefab;
        private SettingsContext m_context;
        private MenuContext m_menu;

        public event Action OnClose;

        private void Awake()
        {
            canvasGroup.alpha = 0;
            window.anchoredPosition = new Vector2(0, -60);
        }

        public void Initialize(SettingsContext context)
        {
            canvasGroup.alpha = 0;
            window.anchoredPosition = new Vector2(0, -60);

            m_context = context;
            m_menu = MenuContext.Home;

            if (!App.Initialized)
            {
                App.Init();
            }

            ConstructMenu();
            OpenMenu();
        }

        private void OpenMenu()
        {
            window.DOAnchorPos(Vector2.zero, 0.18f).SetEase(Ease.OutBack);
            canvasGroup.DOFade(1, 0.18f);
            AudioController.PlaySound("Hero_Appear-Whoosh");
        }

        private async void CloseMenu()
        {
            AudioController.PlaySound("Hero_Appear-Whoosh");

            window.DOAnchorPos(new Vector2(0, -60), 0.18f).SetEase(Ease.InBack);
            canvasGroup.DOFade(0, 0.18f);

            await System.Threading.Tasks.Task.Delay(400);

            OnClose?.Invoke();
            await SceneController.Instance.UnloadScene("OptionsWindow");

            CrossBlitzInputModule.Instance.OnResolutionChanged();
        }

        private void ConstructMenu()
        {
            switch (m_menu)
            {
                case MenuContext.Home:
                    ConstructDefaultMenu();
                    break;
                case MenuContext.GameSettings:
                    ConstructGameSettings();
                    break;
                case MenuContext.GameOptions:
                    ConstructGameOptions();
                    break;
                case MenuContext.Display:
                    ConstructDisplay();
                    break;
                case MenuContext.Audio:
                    ConstructAudio();
                    break;
                case MenuContext.PlayerData:
                    ConstructPlayerData();
                    break;
                case MenuContext.PlayerStatistics:
                    ConstructPlayerStatistics();
                    break;
                case MenuContext.PlayTimeStatistics:
                    ConstructPlayTimeStatistics();
                    break;
                case MenuContext.RedcroftPlayTime:
                    ConstructPlayTimeRedcroft();
                    break;
                case MenuContext.RedcroftPlayTimeCh1:
                    ConstructPlayTimeCharacter("Redcroft", 1, MenuContext.RedcroftPlayTime);
                    break;
                case MenuContext.RedcroftPlayTimeCh2:
                    ConstructPlayTimeCharacter("Redcroft", 2, MenuContext.RedcroftPlayTime);
                    break;
                case MenuContext.RedcroftPlayTimeCh3:
                    ConstructPlayTimeCharacter("Redcroft", 3, MenuContext.RedcroftPlayTime);
                    break;
                case MenuContext.VioletPlayTime:
                    ConstructPlayTimeViolet();
                    break;
                case MenuContext.VioletPlayTimeCh1:
                    ConstructPlayTimeCharacter("Violet", 1, MenuContext.VioletPlayTime);
                    break;
                case MenuContext.VioletPlayTimeCh2:
                    ConstructPlayTimeCharacter("Violet", 2, MenuContext.VioletPlayTime);
                    break;
                case MenuContext.VioletPlayTimeCh3:
                    ConstructPlayTimeCharacter("Violet", 3, MenuContext.VioletPlayTime);
                    break;
                case MenuContext.QuillPlayTime:
                    ConstructPlayTimeQuill();
                    break;
                case MenuContext.QuillPlayTimeCh1:
                    ConstructPlayTimeCharacter("Quill", 1, MenuContext.QuillPlayTime);
                    break;
                case MenuContext.QuillPlayTimeCh2:
                    ConstructPlayTimeCharacter("Quill", 2, MenuContext.QuillPlayTime);
                    break;
                case MenuContext.QuillPlayTimeCh3:
                    ConstructPlayTimeCharacter("Quill", 3, MenuContext.QuillPlayTime);
                    break;
                case MenuContext.SetoPlayTime:
                    ConstructPlayTimeSeto();
                    break;
                case MenuContext.SetoPlayTimeCh1:
                    ConstructPlayTimeCharacter("Seto", 1, MenuContext.SetoPlayTime);
                    break;
                case MenuContext.SetoPlayTimeCh2:
                    ConstructPlayTimeCharacter("Seto", 2, MenuContext.SetoPlayTime);
                    break;
                case MenuContext.SetoPlayTimeCh3:
                    ConstructPlayTimeCharacter("Seto", 3, MenuContext.SetoPlayTime);
                    break;
                case MenuContext.MereenaPlayTime:
                    ConstructPlayTimeMereena();
                    break;
                case MenuContext.MereenaPlayTimeCh1:
                    ConstructPlayTimeCharacter("Mereena", 1, MenuContext.MereenaPlayTime);
                    break;
                case MenuContext.MereenaPlayTimeCh2:
                    ConstructPlayTimeCharacter("Mereena", 1, MenuContext.MereenaPlayTime);
                    break;
                case MenuContext.MereenaPlayTimeCh3:
                    ConstructPlayTimeCharacter("Mereena", 1, MenuContext.MereenaPlayTime);
                    break;
                case MenuContext.BattlesSettings:
                    ConstructBattlesSettings();
                    break;
                case MenuContext.CollectionMenuSettings:
                    ConstructCollectionMenuSettings();
                    break;
                case MenuContext.FablesSettings:
                    ConstructFablesSettings();
                    break;
                case MenuContext.ResetAllDataConfirmation:
                    ConstructResetAllDataConfirmation();
                    break;
                case MenuContext.QuitGameConfirmation:
                    ConstructQuitGameConfirmation();
                    break;
                case MenuContext.DeveloperOptions:
                    ConstructDeveloperOptions();
                    break;
                case MenuContext.DeveloperFables:
                    ConstructDeveloperFablesOptions();
                    break;
                case MenuContext.DeveloperBattles:
                    ConstructDeveloperBattlesOptions();
                    break;
                case MenuContext.DeveloperInventory:
                    ConstructDeveloperInventoryOptions();
                    break;
                case MenuContext.RestartBattleConfirmation:
                    ConstructRestartBattleConfirmation();
                    break;
                case MenuContext.ForfeitMatchConfirmation:
                    ConstructForfeit();
                    break;
                case MenuContext.ReturnToMainMenuConfirmation:
                    ConstructReturnToMainMenuConfirmation();
                    break;
                case MenuContext.ReturnToChapterSelectConfirmation:
                    ConstructReturnToChapterSelectConfirmation();
                    break;
            }
        }

        private void SetTitle(string titleName)
        {
            title.text = titleName;
        }

        private void ConstructDefaultMenu()
        {
            SetTitle("OPTIONS");

            var options = new List<SettingsOptionData>();

            options.Add(new SettingsOptionData
            {
                optionName = "RESUME",
                optionType = SettingOptionType.Button,
                onOptionClicked = OnResume
            });

            if (m_context == SettingsContext.Game)
            {
                options.Add( new SettingsOptionData
                {
                    optionName = "FORFEIT BATTLE",
                    optionType = SettingOptionType.Button,
                    linkedMenu = MenuContext.ForfeitMatchConfirmation,
                    onOptionClicked = OnChangeMenu
                });

                options.Add( new SettingsOptionData
                {
                    optionName = "TUTORIALS",
                    optionType = SettingOptionType.Button,
                    onOptionClicked = OnTutorialsOpened
                });
            }

            options.Add( new SettingsOptionData
            {
                optionName = "SETTINGS",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.GameSettings,
                onOptionClicked = OnChangeMenu
            });


            if (m_context == SettingsContext.Fables)
            {
                options.Add( new SettingsOptionData
                {
                    optionName = "RETURN TO CHAPTER SELECT",
                    optionType = SettingOptionType.Button,
                    linkedMenu = MenuContext.ReturnToChapterSelectConfirmation,
                    onOptionClicked = OnChangeMenu
                });

                options.Add( new SettingsOptionData
                {
                    optionName = "RETURN TO MAIN MENU",
                    optionType = SettingOptionType.Button,
                    linkedMenu = MenuContext.ReturnToMainMenuConfirmation,
                    onOptionClicked = OnChangeMenu
                });
            }

            options.Add( new SettingsOptionData
            {
                optionName = "QUIT GAME",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.QuitGameConfirmation,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }

        private void ConstructGameSettings()
        {
            SetTitle("SETTINGS");

            var options = new List<SettingsOptionData>();

            options.Add(new SettingsOptionData
            {
                optionName = "GAME OPTIONS",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.GameOptions,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData
            {
                optionName = "DISPLAY",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.Display,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData
            {
                optionName = "AUDIO",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.Audio,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData
            {
                optionName = "PLAYER DATA",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.PlayerData,
                onOptionClicked = OnChangeMenu
            });

            options.Add( new SettingsOptionData
            {
                optionName = "DEVELOPER OPTIONS",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.DeveloperOptions,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add( new SettingsOptionData
            {
                optionName = "BACK",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.Home,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }

        private void ConstructGameOptions()
        {
            SetTitle("GAME OPTIONS");

            var options = new List<SettingsOptionData>();

            options.Add(new SettingsOptionData
            {
                optionName = "BATTLES",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.BattlesSettings,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData
            {
                optionName = "COLLECTION MENU",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.CollectionMenuSettings,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData
            {
                optionName = "FABLES",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.FablesSettings,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add( new SettingsOptionData
            {
                optionName = "BACK",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.GameSettings,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }

        private void ConstructBattlesSettings()
        {
            SetTitle("BATTLES");

            var options = new List<SettingsOptionData>();

            options.Add(new SettingsOptionData
            {
                optionName = "DAMAGE DISPLAY SPEED",
                optionType = SettingOptionType.Options,
                currentOption = App.Settings.GetDamageDisplaySpeed(),
                options = new List<string> { "SLOW", "MEDIUM", "FAST", "VERY FAST" },
                onOptionToggleChanged = OnDamageDisplaySpeedChanged
            });

            options.Add(new SettingsOptionData
            {
                optionName = "CARD DISPLAY SPEED",
                optionType = SettingOptionType.Options,
                currentOption = App.Settings.GetCardDisplaySpeed(),
                options = new List<string> { "CLICK", "SLOW", "MEDIUM", "FAST" },
                onOptionToggleChanged = OnCardDisplaySpeedChanged
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add(new SettingsOptionData
            {
                optionName = "BACK",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.GameOptions,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }

        private void ConstructCollectionMenuSettings()
        {
            SetTitle("COLLECTION MENU SETTINGS");

            var options = new List<SettingsOptionData>();

            options.Add(new SettingsOptionData
            {
                optionName = "NEW CARD ALERTS",
                optionType = SettingOptionType.Options,
                currentOption = App.Settings.GetNewCardNotificationsEnabled() ? 1 : 0,
                options = new List<string> { "OFF", "ON" },
                onOptionToggleChanged = OnNewCardNotificationsEnabledChanged
            });

            options.Add(new SettingsOptionData
            {
                optionName = "CARD SPAWN EFFECT",
                optionType = SettingOptionType.Options,
                currentOption = App.Settings.GetDeckEditorCardAnimationsEnabled() ? 1 : 0,
                options = new List<string> { "OFF", "ON" },
                onOptionToggleChanged = OnDeckEditorCardAnimationsEnabledChanged
            });

            options.Add(new SettingsOptionData
            {
                optionName = "ZOOMED CARD DELAY",
                optionType = SettingOptionType.Options,
                currentOption = App.Settings.GetZoomedCardPopupDelay(),
                options = new List<string> {"OFF", "LOW", "MED","HIGH", "NO SHOW" },
                onOptionToggleChanged = OnZoomedCardPopupDelayChanged
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add( new SettingsOptionData
            {
                optionName = "BACK",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.GameOptions,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }

        private void ConstructFablesSettings()
        {
            SetTitle("FABLES SETTINGS");

            var options = new List<SettingsOptionData>();

            options.Add(new SettingsOptionData
            {
                optionName = "SPEED-UP CHARACTER",
                optionType = SettingOptionType.Options,
                currentOption = App.Settings.GetFablesSpeedUpCharacter(),
                options = new List<string> { "HOLD", "ALWAYS" },
                onOptionToggleChanged = OnFablesSpeedUpCharacterChanged
            });

            options.Add(new SettingsOptionData
            {
                optionName = "SCREENSHAKE",
                optionType = SettingOptionType.Options,
                currentOption = App.Settings.GetScreenShake(),
                options = new List<string> { "OFF", "MILD", "DEFAULT" },
                onOptionToggleChanged = OnScreenShakeChanged
            });

            options.Add(new SettingsOptionData
            {
                optionName = "SCREENFLASH",
                optionType = SettingOptionType.Options,
                currentOption = App.Settings.GetScreenFlash(),
                options = new List<string> {"OFF", "ON"},
                onOptionToggleChanged = OnScreenFlashChanged
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add(new SettingsOptionData
            {
                optionName = "BACK",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.GameOptions,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }

        private void ConstructDisplay()
        {
            SetTitle("DISPLAY OPTIONS");

            var options = new List<SettingsOptionData>();

            options.Add(new SettingsOptionData
            {
                optionName = "RESOLUTION",
                optionType = SettingOptionType.Options,
                currentOption = App.Settings.GetResolutionScale()-1,
                options = new List<string> { "640 x 360", "1280 x 720", "1920 x 1080", "2560 x 1440", "3200 x 1800", "3840 x 2160" },
                onOptionToggleChanged = OnResolutionChanged
            });

            options.Add(new SettingsOptionData
            {
                optionName = "SCREEN MODE",
                optionType = SettingOptionType.Options,
                currentOption = (int)App.Settings.GetScreenMode(),
                options = new List<string> { "FULL SCREEN", "FULL WINDOWED", "BORDERLESS", "WINDOWED" },
                onOptionToggleChanged = OnScreenModeChanged
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add(new SettingsOptionData
            {
                optionName = "FILTER",
                optionType = SettingOptionType.Options,
                currentOption = (int)App.Settings.GetFilterOption(),
                options = new List<string> { "OFF", "SCANLINES","ARCADE" },
                onOptionToggleChanged = OnFilterModeChanged
            });

            options.Add(new SettingsOptionData
            {
                optionName = "FILTER INTENSITY",
                optionType = SettingOptionType.Options,
                currentOption = (int)App.Settings.GetFilterIntensity(),
                options = new List<string> { "1", "2", "3", "4", "5" },
                onOptionToggleChanged = OnFilterIntensityChanged
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add( new SettingsOptionData
            {
                optionName = "BACK",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.GameSettings,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }

        private void ConstructAudio()
        {
            SetTitle("AUDIO OPTIONS");

            var options = new List<SettingsOptionData>();

            options.Add(new SettingsOptionData
            {
                optionName = "MASTER VOLUME",
                optionType = SettingOptionType.Options,
                currentOption = (int)(App.Settings.GetMasterVolume() / 0.1F),
                options = new List<string> { "0%", "10%", "20%", "30%", "40%", "50%", "60%", "70%", "80%", "90%", "100%" },
                onOptionToggleChanged = OnMasterVolumeChanged
            });

            options.Add(new SettingsOptionData
            {
                optionName = "MUSIC VOLUME",
                optionType = SettingOptionType.Options,
                currentOption = (int)(App.Settings.GetMusicVolume() / 0.1F),
                options = new List<string> { "0%","10%","20%","30%","40%","50%","60%","70%","80%","90%","100%" },
                onOptionToggleChanged = OnMusicVolumeChanged
            });

            options.Add(new SettingsOptionData
            {
                optionName = "SFX VOLUME",
                optionType = SettingOptionType.Options,
                currentOption =(int)(App.Settings.GetSfxVolume() / 0.1F),
                options = new List<string> { "0%","10%","20%","30%","40%","50%","60%","70%","80%","90%","100%" },
                onOptionToggleChanged = OnSfxVolumeChanged
            });

            options.Add(new SettingsOptionData
            {
                optionName = "AMBIENCE VOLUME",
                optionType = SettingOptionType.Options,
                currentOption = (int)(App.Settings.GetAmbienceVolume() / 0.1F),
                options = new List<string> { "0%", "10%", "20%", "30%", "40%", "50%", "60%", "70%", "80%", "90%", "100%" },
                onOptionToggleChanged = OnAmbienceVolumeChanged
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add( new SettingsOptionData
            {
                optionName = "BACK",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.GameSettings,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }

        private void ConstructPlayerData()
        {
            SetTitle("PLAYER DATA OPTIONS");

            var options = new List<SettingsOptionData>();

            options.Add( new SettingsOptionData
            {
                optionName = "STATISTICS",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.PlayerStatistics,
                onOptionClicked = OnChangeMenu
            });

            #if UNITY_EDITOR

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add(new SettingsOptionData
            {
                optionName = "EXPORT SAVE DATA",
                optionType = SettingOptionType.Button,
                onOptionClicked = OnExportSaveData
            });

            options.Add(new SettingsOptionData
            {
                optionName = "IMPORT SAVE DATA",
                optionType = SettingOptionType.Button,
                onOptionClicked = OnImportSaveData
            });
            #endif

            options.Add( new SettingsOptionData
            {
                optionName = "RESET ALL DATA",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.ResetAllDataConfirmation,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add( new SettingsOptionData
            {
                optionName = "BACK",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.GameSettings,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }

        private void ConstructPlayerStatistics()
        {
            SetTitle("STATISTICS");

            var options = new List<SettingsOptionData>();

            var cardsOwned = 0;
            var cardsTotal = Db.CardDatabase.GetCollectableCardCount();
            for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
            {
                var amountOwned = App.Inventory.GetItemAmount(Db.CardDatabase.Cards[i].id);
                if (amountOwned > 0)
                {
                    cardsOwned++;
                }
            }

            options.Add(new SettingsOptionData
            {
                optionName = "CARDS COLLECTED",
                optionType = SettingOptionType.Options,
                currentOption = 0,
                options = new List<string> { $"{cardsOwned} / {cardsTotal}" },
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add(new SettingsOptionData
            {
                optionName = "PLAY TIME",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.PlayTimeStatistics,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add( new SettingsOptionData
            {
                optionName = "BACK",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.PlayerData,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }

        private void ConstructPlayTimeStatistics()
        {
            SetTitle("PLAY TIME STATISTICS");

            var options = new List<SettingsOptionData>();

            options.Add(new SettingsOptionData
            {
                optionName = "TOTAL PLAY TIME",
                optionType = SettingOptionType.Options,
                currentOption = 0,
                options = new List<string> { new TimeSpan(0, 0, 0, (int)App.AccountInfo.GetTimePlayedTotal()).ToString("g") },
            });


            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add(new SettingsOptionData
            {
                optionName = "REDCROFT",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.RedcroftPlayTime,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData
            {
                optionName = "VIOLET",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.VioletPlayTime,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData
            {
                optionName = "QUILL",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.QuillPlayTime,
                onOptionClicked = OnChangeMenu
            });

            //options.Add(new SettingsOptionData
            //{
            //    optionName = "SETO PLAY TIME",
            //    optionType = SettingOptionType.Button,
            //    linkedMenu = MenuContext.SetoPlayTime,
            //    onOptionClicked = OnChangeMenu
            //});

            //options.Add(new SettingsOptionData
            //{
            //    optionName = "MEREENA PLAY TIME",
            //    optionType = SettingOptionType.Button,
            //    linkedMenu = MenuContext.MereenaPlayTime,
            //    onOptionClicked = OnChangeMenu
            //});

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add(new SettingsOptionData
            {
                optionName = "BACK",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.PlayerStatistics,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }


        private void ConstructPlayTimeRedcroft()
        {
            SetTitle("REDCROFT PLAY TIME");

            var options = new List<SettingsOptionData>();

            options.Add(new SettingsOptionData
            {
                optionName = "CHAPTER 1 PLAY TIME",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.RedcroftPlayTimeCh1,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData
            {
                optionName = "CHAPTER 2 PLAY TIME",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.RedcroftPlayTimeCh2,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData
            {
                optionName = "CHAPTER 3 PLAY TIME",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.RedcroftPlayTimeCh3,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add(new SettingsOptionData
            {
                optionName = "BACK",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.PlayTimeStatistics,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }

        private void ConstructPlayTimeViolet()
        {
            SetTitle("VIOLET PLAY TIME");

            var options = new List<SettingsOptionData>();

            options.Add(new SettingsOptionData
            {
                optionName = "CHAPTER 1 PLAY TIME",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.VioletPlayTimeCh1,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData
            {
                optionName = "CHAPTER 2 PLAY TIME",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.VioletPlayTimeCh2,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData
            {
                optionName = "CHAPTER 3 PLAY TIME",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.VioletPlayTimeCh3,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add(new SettingsOptionData
            {
                optionName = "BACK",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.PlayTimeStatistics,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }

        private void ConstructPlayTimeQuill()
        {
            SetTitle("QUILL PLAY TIME");

            var options = new List<SettingsOptionData>();

            options.Add(new SettingsOptionData
            {
                optionName = "CHAPTER 1 PLAY TIME",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.QuillPlayTimeCh1,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData
            {
                optionName = "CHAPTER 2 PLAY TIME",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.QuillPlayTimeCh2,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData
            {
                optionName = "CHAPTER 3 PLAY TIME",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.QuillPlayTimeCh3,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add(new SettingsOptionData
            {
                optionName = "BACK",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.PlayTimeStatistics,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }

        private void ConstructPlayTimeSeto()
        {
            SetTitle("SETO PLAY TIME");

            var options = new List<SettingsOptionData>();

            options.Add(new SettingsOptionData
            {
                optionName = "CHAPTER 1 PLAY TIME",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.SetoPlayTimeCh1,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData
            {
                optionName = "CHAPTER 2 PLAY TIME",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.SetoPlayTimeCh2,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData
            {
                optionName = "CHAPTER 3 PLAY TIME",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.SetoPlayTimeCh3,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add(new SettingsOptionData
            {
                optionName = "BACK",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.PlayTimeStatistics,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }

        private void ConstructPlayTimeMereena()
        {
            SetTitle("MEREENA PLAY TIME");

            var options = new List<SettingsOptionData>();

            options.Add(new SettingsOptionData
            {
                optionName = "CHAPTER 1 PLAY TIME",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.MereenaPlayTimeCh1,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData
            {
                optionName = "CHAPTER 2 PLAY TIME",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.MereenaPlayTimeCh2,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData
            {
                optionName = "CHAPTER 3 PLAY TIME",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.MereenaPlayTimeCh3,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add(new SettingsOptionData
            {
                optionName = "BACK",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.PlayTimeStatistics,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }

        private void ConstructPlayTimeCharacter(String character, int chapter, MenuContext prevMenu)
        {

            var chapterPlayTime = App.FableData.GetChapterPlayTime(character, "1", chapter);

            SetTitle(character.ToUpper() + " CHAPTER " + chapter + " PLAY TIME");

            var options = new List<SettingsOptionData>();

            options.Add(new SettingsOptionData
            {
                optionName = "CHAPTER",
                optionType = SettingOptionType.Options,
                currentOption = 0,
                options = new List<string> { new TimeSpan(0, 0, 0, (int)chapterPlayTime.TotalChapterPlayTime).ToString("g") },
            });

            options.Add(new SettingsOptionData
            {
                optionName = "BATTLES",
                optionType = SettingOptionType.Options,
                currentOption = 0,
                options = new List<string> { new TimeSpan(0, 0, 0, (int)chapterPlayTime.TotalTimeInBattles).ToString("g") },
            });

            options.Add(new SettingsOptionData
            {
                optionName = "CUTSCENES",
                optionType = SettingOptionType.Options,
                currentOption = 0,
                options = new List<string> { new TimeSpan(0, 0, 0, (int)chapterPlayTime.TotalTimeWatchingCutscenes).ToString("g") },
            });

            options.Add(new SettingsOptionData
            {
                optionName = "DECK EDITING",
                optionType = SettingOptionType.Options,
                currentOption = 0,
                options = new List<string> { new TimeSpan(0, 0, 0, (int)chapterPlayTime.TotalTimeDeckEditing).ToString("g") },
            });

            options.Add(new SettingsOptionData
            {
                optionName = "MELDING",
                optionType = SettingOptionType.Options,
                currentOption = 0,
                options = new List<string> { new TimeSpan(0, 0, 0, (int)chapterPlayTime.TotalTimeMelding).ToString("g") },
            });

            options.Add(new SettingsOptionData
            {
                optionName = "SHOPS",
                optionType = SettingOptionType.Options,
                currentOption = 0,
                options = new List<string> { new TimeSpan(0, 0, 0, (int)chapterPlayTime.TotalTimeInShops).ToString("g") },
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add(new SettingsOptionData
            {
                optionName = "BACK",
                optionType = SettingOptionType.Button,
                linkedMenu = prevMenu,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }

        private void ConstructResetAllDataConfirmation()
        {
            SetTitle("RESET ALL DATA?");

            var options = new List<SettingsOptionData>();

            options.Add( new SettingsOptionData
            {
                optionName = "NO",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.PlayerData,
                onOptionClicked = OnChangeMenu
            });

            options.Add( new SettingsOptionData
            {
                optionName = "YES",
                optionType = SettingOptionType.Button,
                onOptionClicked = OnResetAllData
            });

            Construct(options);
        }

        private void ConstructQuitGameConfirmation()
        {
            SetTitle("QUIT GAME?");

            var options = new List<SettingsOptionData>();

            options.Add( new SettingsOptionData
            {
                optionName = "NO",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.Home,
                onOptionClicked = OnChangeMenu
            });

            options.Add( new SettingsOptionData
            {
                optionName = "YES",
                optionType = SettingOptionType.Button,
                onOptionClicked = OnQuitGame
            });

            Construct(options);
        }

        private void ConstructDeveloperOptions()
        {
            SetTitle("DEVELOPER OPTIONS");

            var options = new List<SettingsOptionData>();

            options.Add(new SettingsOptionData
            {
                optionName = "FABLES",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.DeveloperFables,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData
            {
                optionName = "BATTLES",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.DeveloperBattles,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData
            {
                optionName = "INVENTORY",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.DeveloperInventory,
                onOptionClicked = OnChangeMenu
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add( new SettingsOptionData
            {
                optionName = "BACK",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.GameSettings,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }

        private void ConstructDeveloperFablesOptions()
        {
            SetTitle("DEVELOPER FABLES");

            var options = new List<SettingsOptionData>();

            // options.Add( new SettingsOptionData
            // {
            //     optionName = "SKIP CUTSCENES",
            //     optionType = SettingOptionType.Options,
            //     currentOption = App.Settings.SkipCutscenes ? 1 : 0,
            //     options = new List<string> { "OFF", "ON" },
            //     onOptionToggleChanged = OnSkipCutscenesChanged
            // });

            options.Add(new SettingsOptionData
            {
                optionName = "UNLOCK ALL NODES",
                optionType = SettingOptionType.Options,
                currentOption = App.Settings.GetUnlockAllNodes() ? 1 : 0,
                options = new List<string> { "OFF", "ON" },
                onOptionToggleChanged = OnUnlockAllNodesChanged
            });

            options.Add(new SettingsOptionData
            {
                optionName = "SKIP BATTLES",
                optionType = SettingOptionType.Options,
                currentOption = App.Settings.GetSkipBattles() ? 1 : 0,
                options = new List<string> { "OFF", "ON" },
                onOptionToggleChanged = OnSkipBattlesChanged
            });

            options.Add(new SettingsOptionData
            {
                optionName = "NO BATTLE REWARDS",
                optionType = SettingOptionType.Options,
                currentOption = App.Settings.GetNoBattleRewards() ? 1 : 0,
                options = new List<string> { "OFF", "ON" },
                onOptionToggleChanged = OnNoBattleRewardsChanged
            });

            options.Add(new SettingsOptionData
            {
                optionName = "SHOW REWARDS",
                optionType = SettingOptionType.Options,
                currentOption = App.Settings.GetDisplayRewardsWhenSkippingBattles() ? 1 : 0,
                options = new List<string> { "OFF", "ON" },
                onOptionToggleChanged = OnDisplayBattleRewardsChanged
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add(new SettingsOptionData
            {
                optionName = "BACK",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.DeveloperOptions,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }

        private void ConstructDeveloperBattlesOptions()
        {
            SetTitle("DEVELOPER BATTLES");

            var options = new List<SettingsOptionData>();

            options.Add(new SettingsOptionData
            {
                optionName = "SHOW CARD HISTORY",
                optionType = SettingOptionType.Options,
                currentOption = App.Settings.GetShowDebugHistories() ? 1 : 0,
                options = new List<string> { "OFF", "ON" },
                onOptionToggleChanged = OnShowDebugHistoriesChanged
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add(new SettingsOptionData
            {
                optionName = "UNLIMITED PLAYER HP",
                optionType = SettingOptionType.Options,
                currentOption = App.Settings.GetUnlimitedPlayerHP() ? 1 : 0,
                options = new List<string> { "OFF", "ON" },
                onOptionToggleChanged = OnUnlimitedPlayerHPChanged
            });

            options.Add(new SettingsOptionData
            {
                optionName = "UNLIMITED CARD HP",
                optionType = SettingOptionType.Options,
                currentOption = App.Settings.GetUnlimitedCardHP() ? 1 : 0,
                options = new List<string> { "OFF", "ON" },
                onOptionToggleChanged = OnUnlimitedCardHPChanged
            });

            options.Add(new SettingsOptionData
            {
                optionName = "UNLIMITED MANA",
                optionType = SettingOptionType.Options,
                currentOption = App.Settings.GetUnlimitedMana() ? 1 : 0,
                options = new List<string> { "OFF", "ON" },
                onOptionToggleChanged = OnUnlimitedManaChanged
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add(new SettingsOptionData
            {
                optionName = "BACK",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.DeveloperOptions,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }

        private void ConstructDeveloperInventoryOptions()
        {
            SetTitle("DEVELOPER INVENTORY");

            var options = new List<SettingsOptionData>();

            options.Add(new SettingsOptionData
            {
                optionName = "UNLOCK ALL CARDS",
                optionType = SettingOptionType.Button,
                onOptionClicked = OnUnlockAllCards
            });

            options.Add(new SettingsOptionData
            {
                optionName = "UNLOCK ALL INGREDIENTS",
                optionType = SettingOptionType.Button,
                onOptionClicked = OnAllIngredients
            });

            options.Add(new SettingsOptionData
            {
                optionName = "UNLIMITED MONEY",
                optionType = SettingOptionType.Button,
                onOptionClicked = OnUnlimitedMoney
            });

            options.Add(new SettingsOptionData { lineBreak = true });

            options.Add(new SettingsOptionData
            {
                optionName = "BACK",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.DeveloperOptions,
                onOptionClicked = OnChangeMenu
            });

            Construct(options);
        }

        private void ConstructRestartBattleConfirmation()
        {
            SetTitle("RESTART BATTLE?");

            var options = new List<SettingsOptionData>();

            options.Add( new SettingsOptionData
            {
                optionName = "NO",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.Home,
                onOptionClicked = OnChangeMenu
            });

            options.Add( new SettingsOptionData
            {
                optionName = "YES",
                optionType = SettingOptionType.Button,
                onOptionClicked = OnRestartBattle
            });

            Construct(options);
        }

        private void ConstructForfeit()
        {
            SetTitle("FORFEIT THE MATCH?");

            var options = new List<SettingsOptionData>();

            options.Add( new SettingsOptionData
            {
                optionName = "NO",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.Home,
                onOptionClicked = OnChangeMenu
            });

            options.Add( new SettingsOptionData
            {
                optionName = "YES",
                optionType = SettingOptionType.Button,
                onOptionClicked = OnForfeitMatch
            });

            Construct(options);
        }

        private void ConstructReturnToMainMenuConfirmation()
        {
            SetTitle("RETURN TO THE MAIN MENU?");

            var options = new List<SettingsOptionData>();

            options.Add( new SettingsOptionData
            {
                optionName = "NO",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.Home,
                onOptionClicked = OnChangeMenu
            });

            options.Add( new SettingsOptionData
            {
                optionName = "YES",
                optionType = SettingOptionType.Button,
                onOptionClicked = OnReturnToMainMenu
            });

            Construct(options);
        }

        private void ConstructReturnToChapterSelectConfirmation()
        {
            SetTitle("RETURN TO THE CHAPTER SELECT?");

            var options = new List<SettingsOptionData>();

            options.Add( new SettingsOptionData
            {
                optionName = "NO",
                optionType = SettingOptionType.Button,
                linkedMenu = MenuContext.Home,
                onOptionClicked = OnChangeMenu
            });

            options.Add( new SettingsOptionData
            {
                optionName = "YES",
                optionType = SettingOptionType.Button,
                onOptionClicked = OnReturnToChapterSelect
            });

            Construct(options);
        }

        private void Construct(IList<SettingsOptionData> options)
        {
            optionsLayout.DestroyChildren();

            for (var i = 0; i < options.Count; i++)
            {
                var option = Instantiate(optionPrefab, optionsLayout, false);
                option.SetData(options[i], i);
            }
        }

        private void OnResume(SettingsOptionData option)
        {
            App.SaveAll();
            CloseMenu();
        }

        private void OnChangeMenu(SettingsOptionData option)
        {
            if (option.optionName == "BACK" || option.optionName == "NO")
            {
                AudioController.PlaySound("TMP-UI-SelectLockedItem");
            }
            else
            {
                AudioController.PlaySound("Battle_Whoosh-Physical 01");
            }

            m_menu = option.linkedMenu;
            ConstructMenu();
        }

        private void OnTutorialsOpened(SettingsOptionData option)
        {
            // do some tutorial shit here
        }

        private void OnResolutionChanged(SettingsOptionData option)
        {
            SettingsManager.SetResolution(option.currentOption + 1);
        }

        private void OnScreenModeChanged(SettingsOptionData option)
        {
            SettingsManager.SetWindowMode((FullScreenMode)option.currentOption);
        }

        private void OnFilterModeChanged(SettingsOptionData option)
        {
            SettingsManager.SetFilterMode((CrtFilterOption) option.currentOption);
        }

        private void OnFilterIntensityChanged(SettingsOptionData option)
        {
            SettingsManager.SetFilterIntensity(option.currentOption);
        }

        private void OnMasterVolumeChanged(SettingsOptionData option)
        {
            SettingsManager.SetMasterVolume(option.currentOption * 0.1f);
        }

        private void OnMusicVolumeChanged(SettingsOptionData option)
        {
            SettingsManager.SetMusicVolume(option.currentOption * 0.1f);
        }

        private void OnSfxVolumeChanged(SettingsOptionData option)
        {
            SettingsManager.SetSfxVolume(option.currentOption * 0.1f);
        }

        private void OnAmbienceVolumeChanged(SettingsOptionData option)
        {
            SettingsManager.SetAmbienceVolume(option.currentOption * 0.1f);
        }

        private void OnQuitGame(SettingsOptionData option)
        {
            SettingsManager.QuitGame();
        }

        private void OnResetAllData(SettingsOptionData option)
        {
            SettingsManager.ResetAllSavedData();
            CloseMenu();
        }

        private void OnSkipCutscenesChanged(SettingsOptionData option)
        {
            App.Settings.SetSkipCutscenes(option.currentOption != 0);
        }

        private void OnSkipBattlesChanged(SettingsOptionData option)
        {
            App.Settings.SetSkipBattles(option.currentOption != 0);
        }

        private void OnUnlockAllNodesChanged(SettingsOptionData option)
        {
            App.Settings.SetUnlockAllNodes(option.currentOption != 0);
        }

        private void OnUnlockAllCards(SettingsOptionData option)
        {
            SettingsManager.UnlockAllCards();
        }

        private void OnExportSaveData(SettingsOptionData option)
        {
            SettingsManager.ExportSaveData();
        }

        private void OnImportSaveData(SettingsOptionData option)
        {
            SettingsManager.ImportSaveData();
        }

        private void OnUnlimitedMoney(SettingsOptionData option)
        {
            SettingsManager.UnlimitedMoney();
        }
        private void OnAllIngredients(SettingsOptionData option)
        {
            SettingsManager.AllIngredients();
        }

        private void OnNoBattleRewardsChanged(SettingsOptionData option)
        {
            App.Settings.SetNoBattleRewards(option.currentOption != 0);
        }

        private void OnDisplayBattleRewardsChanged(SettingsOptionData option)
        {
            App.Settings.SetDisplayRewardsWhenSkippingBattles(option.currentOption != 0);
        }

        private void OnShowDebugHistoriesChanged(SettingsOptionData option)
        {
            App.Settings.SetShowDebugHistories(option.currentOption != 0);
        }

        private void OnUnlimitedPlayerHPChanged(SettingsOptionData option)
        {
            App.Settings.SetUnlimitedPlayerHP(option.currentOption != 0);
        }

        private void OnUnlimitedCardHPChanged(SettingsOptionData option)
        {
            App.Settings.SetUnlimitedCardHP(option.currentOption != 0);
        }

        private void OnUnlimitedManaChanged(SettingsOptionData option)
        {
            App.Settings.SetUnlimitedMana(option.currentOption != 0);
        }

        private void OnNewCardNotificationsEnabledChanged(SettingsOptionData option)
        {
            App.Settings.SetNewCardNotificationsEnabled (option.currentOption != 0);
        }
        private void OnDeckEditorCardAnimationsEnabledChanged(SettingsOptionData option)
        {
            App.Settings.SetDeckEditorCardAnimationsEnabled(option.currentOption != 0);
        }

        private void OnFablesSpeedUpCharacterChanged(SettingsOptionData option)
        {
            App.Settings.SetFablesSpeedUpCharacter(option.currentOption);
        }

        private void OnZoomedCardPopupDelayChanged(SettingsOptionData option)
        {
            App.Settings.SetZoomedCardPopupDelay(option.currentOption);
        }

        private void OnDamageDisplaySpeedChanged(SettingsOptionData option)
        {
            App.Settings.SetDamageDisplaySpeed(option.currentOption);
        }

        private void OnCardDisplaySpeedChanged(SettingsOptionData option)
        {
            App.Settings.SetCardDisplaySpeed(option.currentOption);
        }

        private void OnScreenShakeChanged(SettingsOptionData option)
        {
            App.Settings.SetScreenShake(option.currentOption);
        }

        private void OnScreenFlashChanged(SettingsOptionData option)
        {
            App.Settings.SetScreenFlash(option.currentOption);
        }

        private void OnRestartBattle(SettingsOptionData option)
        {
            if (GameServer.Mode == null)
            {
                Debug.LogError("You're not currently in a game!");
                return;
            }

            Events.Publish(null, EventType.StartBattle, new StartBattleEventArgs
            {
                gameMode = new GameMode
                {
                    GameType = GameServer.Mode.GameType,
                    ConnectionMode = GameServer.Mode.ConnectionMode,
                    SceneEnvironment = GameServer.Mode.SceneEnvironment,
                    SceneToReturnTo = GameServer.Mode.SceneToReturnTo,
                    ShowResultsAfterMatch = GameServer.Mode.ShowResultsAfterMatch,
                    BattleUid = GameServer.Mode.BattleUid,
                    DeckUid = GameServer.Mode.DeckUid
                }
            });
        }

        private void OnForfeitMatch(SettingsOptionData option)
        {
            // if we're already in the game finished state, just return
            if (StateController.CurrentState != StateDefinition.GAME_FINISHED)
            {
                Events.Publish(null, EventType.OnMatchLost, null);
            }

            CloseMenu();
        }

        private async void OnReturnToMainMenu(SettingsOptionData option)
        {
           SettingsManager.ReturnToMainMenu();
        }

        private async void OnReturnToChapterSelect(SettingsOptionData option)
        {


            await TransitionController.TransitionIn(TransitionController.TransitionType.Cross);

            await System.Threading.Tasks.Task.Delay(900);
            FableController.SetChapter(string.Empty, -1, -1);
            App.BattleResults.ResetData();
            AmbientController.Stop();
            await SceneController.Instance.UnloadAllScenes();

            StateController.ChangeState(StateDefinition.FABLES_CHAPTER_SELECT);
        }
    }
}