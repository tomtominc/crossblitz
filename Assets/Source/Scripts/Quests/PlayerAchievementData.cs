using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using GameDataEditor;

namespace CrossBlitz.Quests
{
    public class PlayerAchievements : ClientData
    {
        public List<PlayerAchievementData> Achievements;

        public override void Init()
        {
            Achievements = App.Load("PlayerQuestData.Achievements", new List<PlayerAchievementData>());

            //var quests = GDEDataManager.GetAllItems<GDEQuestData>();

            var achievements = Db.AchievementDatabase.achievements;

            // add new quests
            for (var i = 0; i < achievements.Count; i++)
            {
                var achievement = achievements[i];
                var playerAchievement = GetAchievement(achievement.Uid);

                if (playerAchievement == null)
                {
                    playerAchievement = new PlayerAchievementData();
                    playerAchievement.AchievementUid = achievement.Uid;
                    playerAchievement.CurrentScore = 0;
                    playerAchievement.TargetScore = achievement.targetScore;
                    Achievements.Add(playerAchievement);
                }

                playerAchievement.TargetScore = achievement.targetScore;
                dirty = true;
            }

            // remove old quests that dont exist anymore
            for (var i = Achievements.Count-1; i > -1; i--)
            {
                var playerAchievement = Achievements[i];
                var achievement = achievements.Find(q => q.Uid == playerAchievement.AchievementUid);

                if (achievement == null)
                {
                    Achievements.RemoveAt(i);
                    dirty = true;
                }
            }
        }

        public List<PlayerAchievementData> GetAchievements()
        {
            return Achievements;
        }
        public void SetAchievements(List<PlayerAchievementData> achievements)
        {
            Achievements = achievements;
            dirty = true;
        }

        public override void ResetData()
        {
            base.ResetData();

            Achievements = new List<PlayerAchievementData>();

            dirty = true;
        }

        public override void Save()
        {
            App.Save("PlayerQuestData.Quests", Achievements);
        }

        public PlayerAchievementData GetAchievement(string id)
        {
            return Achievements.Find(q => q.AchievementUid == id);
        }

        public bool IsComplete(string id)
        {
            var quest = GetAchievement(id);
            if (quest == null) return false;
            return quest.CurrentScore >= quest.TargetScore;
        }

        public bool IsClaimed(string id)
        {
            if (!IsComplete(id)) return false;

            var quest = GetAchievement(id);
            if (quest == null) return false;
            return quest.Claimed;
        }

        public string FormatDescription(string description, string questKey)
        {
            var quest = GetAchievement(questKey);
            if (quest == null) return description;
            description = description.Replace("{Count}", quest.TargetScore.ToString("N0"));
            return description;
        }

    }
    public class PlayerAchievementData
    {
        public string AchievementUid;
        public int CurrentScore;
        public int TargetScore;
        public bool Claimed;
    }
}