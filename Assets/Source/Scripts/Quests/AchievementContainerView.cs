using CrossBlitz.Databases;
using CrossBlitz.PlayFab.Authentication;
using GameDataEditor;
using TMPro;
using UnityEngine;

namespace CrossBlitz.Quests
{
    public class AchievementContainerView : MonoBehaviour
    {
        public bool viewOnly;
        public CanvasGroup canvas;
        public TextMeshProUGUI title;
        public TextMeshProUGUI description;
        public TextMeshProUGUI counter;
        public SpriteAnimation questBar;
        public GameObject completeView;
        public GameObject claimView;

        private string m_achievementId;

        public void SetAchievementId(string achievementId)
        {
            m_achievementId = achievementId;

            if (string.IsNullOrEmpty(m_achievementId))
            {
                completeView.SetActive(false);
                claimView.SetActive(false);
                canvas.alpha = 0;
                return;
            }

            var achievement = Db.AchievementDatabase.GetAchievement(achievementId);

            const float minBarSize = 6;
            const float maxBarSize = 140;

            title.text = achievement.name;
            description.text = App.Achievements.FormatDescription( achievement.description , achievement.Uid );

            completeView.SetActive(false);
            claimView.SetActive(false);
            counter.SetActive(false);

            if (App.Achievements.IsClaimed(m_achievementId))
            {
                completeView.SetActive(true);
                questBar.RectTransform().sizeDelta = new Vector2(maxBarSize, 14f);
                questBar.Play("complete");
            }
            else if (App.Achievements.IsComplete(m_achievementId))
            {
                claimView.SetActive(true);

                if (viewOnly)
                {
                    description.text = "go to the <color=#c44b51>quests screen</color> to claim this card back!";
                }

                questBar.RectTransform().sizeDelta = new Vector2(maxBarSize, 14f);
                questBar.Play("incomplete");
            }
            else
            {
                var playerAchievement = App.Achievements.GetAchievement(m_achievementId);
                counter.SetActive(true);
                counter.text = $"{playerAchievement.CurrentScore}/{playerAchievement.TargetScore}";

                var questBarSize = Mathf.Clamp((float) playerAchievement.CurrentScore / (float) playerAchievement.TargetScore, minBarSize, maxBarSize);
                questBar.RectTransform().sizeDelta = new Vector2(questBarSize, 14f);
                questBar.Play("incomplete");
                questBar.SetActive(playerAchievement.CurrentScore > 0);
            }
        }
    }
}