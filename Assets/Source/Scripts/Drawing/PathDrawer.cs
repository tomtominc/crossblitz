using System;
using System.Collections.Generic;
using CrossBlitz.Fables.Grid;
using CrossBlitz.InputAPI;
using Shapes;
using UnityEngine;

namespace CrossBlitz.Drawing
{
    [ExecuteAlways]
    public class PathDrawer : ImmediateModeShapeDrawer
    {
        public override void DrawShapes(Camera cam)
        {
            if (FablesMapController.Instance == null) return;
            if (FablesMapController.Instance.input == null) return;
            if (FablesMapController.Instance.input.currentMode != FablesInput.Mode.Editor) return;
            if (HexGridEditor.CurrentSettings.Tool != GridEditorTool.PathEditor) return;

            var room = FablesMapController.Instance.GetCurrentRoom();

            if (room == null) return;

            var hoverTile = HexGrid2D.GetTileFromMousePosition();

            using (Draw.Command(cam))
            {
                Draw.LineGeometry = LineGeometry.Flat2D;
                Draw.ThicknessSpace = ThicknessSpace.Meters;
                Draw.Thickness = 1f/32f;
                Draw.Matrix = transform.localToWorldMatrix;
                Draw.LineEndCaps = LineEndCap.None;
                Draw.FontSize = 2;

                for (var i = 0; i < room.Tiles.Count; i++)
                {
                    var hex1 = HexGrid2D.GetTile(room.Tiles[i].uid);
                    hex1.TileData.connectingPathRooms ??= new List<int>();
                    var color = Color.green;
                    var drawSelected = false;

                    if (HexGridEditor.CurrentSettings.PathMode == PathMode.Delete)
                    {
                        if (hoverTile != null && hoverTile == hex1) color = Color.red;
                    }
                    else if (HexGridEditor.CurrentSettings.PathMode == PathMode.Select || HexGridEditor.CurrentSettings.PathMode == PathMode.Add)
                    {
                        if (hoverTile != null && hoverTile == hex1)
                        {
                            color = Color.blue;
                        }

                        if (hex1.TileData.uid == HexGridEditor.CurrentSettings.CurrentPathTile)
                        {
                            drawSelected = true;
                        }
                    }

                    for (var j = 0; j < hex1.TileData.connectingPathRooms.Count; j++)
                    {
                        var hex2 = HexGrid2D.GetTile(hex1.TileData.connectingPathRooms[j]);
                        var half = new Vector2((hex1.Center.x + hex2.Center.x) / 2f,
                            (hex1.Center.y + hex2.Center.y) / 2f);
                        Draw.Line(hex1.Center, half, color);
                    }

                    if (drawSelected)
                    {
                        Draw.Text(hex1.Center, Quaternion.identity, "Selected", Color.red);
                    }

                    // using (var p = new PolygonPath())
                    // {
                    //     Draw.Polygon(p, Color.blue);
                    // }

                }
            }
        }
    }
}