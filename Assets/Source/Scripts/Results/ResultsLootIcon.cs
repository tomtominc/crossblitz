using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.Databases;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.Items.Data;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.Utils;
using DG.Tweening;
using GameDataEditor;
using MEC;
using TMPro;
using UnityEngine;

namespace CrossBlitz.Results
{
    public class ResultsLootIcon : MonoBehaviour, ICursorSelectable
    {
        public bool hasToAnimateIn = true;
        public RectTransform lootIconContainer;
        public SpriteAnimation newLootContainer;
        public SpriteAnimation newLootSparkles;
        public SpriteAnimation cardIcon;
        public RectTransform rarityIconShadow;
        public SpriteAnimation rarityIcon;
        public SpriteAnimation ingredientIcon;
        public RectTransform dawnDollarIcon;
        public TextMeshProUGUI amountLabel;
        public SpriteAnimation spawnPoof;
        public RectTransform zoomedCardParent;
        public RectTransform xIcon;

        private HoverCard m_hoverCard;

        private void Start()
        {
            lootIconContainer.SetActive(!hasToAnimateIn);
            spawnPoof.SetActive(false);
        }

        public void SetLoot(string itemClass, string itemId, int amount, RectTransform lootPreviewTarget, bool isNew = false, bool disableCount = false)
        {
            if (itemClass == ItemClassType.Card)
            {
                cardIcon.SetActive(true);
                rarityIconShadow.SetActive(false);
                rarityIcon.SetActive(false);
                ingredientIcon.SetActive(false);
                dawnDollarIcon.SetActive(false);
                amountLabel.SetActive(false);
                xIcon.SetActive(false);

                cardIcon.Play("card-recipe");
                m_hoverCard = gameObject.AddComponent<HoverCard>();
                m_hoverCard.Initialize(Db.CardDatabase.GetCard(itemId), zoomedCardParent, 0);

                Timing.RunCoroutine(UpdateNewCardStatus());

                newLootContainer.SetActive(true);
                newLootContainer.Play("glow");
                newLootSparkles.SetActive(true);

            }
            else if (itemClass == ItemClassType.Ingredient)
            {
                cardIcon.SetActive(false);
                rarityIconShadow.SetActive(false);
                ingredientIcon.SetActive(true);
                dawnDollarIcon.SetActive(false);
                amountLabel.SetActive(!disableCount);
                xIcon.SetActive(!disableCount);

                var ingredientData = Db.ItemDatabase.GetItem(itemId);

                ingredientIcon.Play(ingredientData.ItemImageUrl);
                amountLabel.text = $"{amount:N0}";

                m_hoverCard = gameObject.AddComponent<HoverCard>();

                var ingredientCardData = CardData.FromItem(ingredientData, amount);

                var charactersInAmount = amountLabel.text.Length - 1;
                xIcon.anchoredPosition += (charactersInAmount*6) * Vector2.left;
                amountLabel.RectTransform().anchoredPosition += (charactersInAmount*6) * Vector2.left;
                m_hoverCard.Initialize(ingredientCardData, zoomedCardParent, 0);
            }
            else if (itemClass == ItemClassType.Recipe)
            {
                cardIcon.SetActive(true);
                rarityIconShadow.SetActive(true);
                ingredientIcon.SetActive(false);
                dawnDollarIcon.SetActive(false);
                amountLabel.SetActive(false);
                xIcon.SetActive(false);

                var recipe = Db.DeckRecipeDatabase.GetRecipe(itemId);
                cardIcon.Play($"deck-recipe-{recipe.deckData.faction.ToString().ToLower()}");

                newLootContainer.SetActive(true);
                newLootContainer.Play("glow");
                newLootSparkles.SetActive(true);
            }
            else if (itemClass == ItemClassType.ManaShard)
            {
                cardIcon.SetActive(false);
                rarityIconShadow.SetActive(false);
                ingredientIcon.SetActive(true);
                dawnDollarIcon.SetActive(false);
                amountLabel.SetActive(!disableCount);
                xIcon.SetActive(!disableCount);

                ingredientIcon.Play("mana-shard");
                amountLabel.text = $"{amount:N0}";

                m_hoverCard = gameObject.AddComponent<HoverCard>();
                var manaShardsData = CardData.FromCurrency(Currency.MANA_SHARDS, amount);
                m_hoverCard.Initialize(manaShardsData, zoomedCardParent, 0);
            }
            else // dawn dollars
            {
                cardIcon.SetActive(false);
                rarityIconShadow.SetActive(false);
                ingredientIcon.SetActive(false);
                dawnDollarIcon.SetActive(true);
                amountLabel.SetActive(!disableCount);
                xIcon.SetActive(!disableCount);

                amountLabel.text = $"{amount:N0}";

                var charactersInAmount = amountLabel.text.Length - 1;
                xIcon.anchoredPosition += (charactersInAmount*6) * Vector2.left;
                amountLabel.RectTransform().anchoredPosition += (charactersInAmount*6) * Vector2.left;

                m_hoverCard = gameObject.AddComponent<HoverCard>();
                var dawnDollarsData = CardData.FromCurrency(Currency.DAWN_DOLLARS, amount);
                m_hoverCard.Initialize(dawnDollarsData, zoomedCardParent, 0);
            }

            zoomedCardParent.position = lootPreviewTarget.position;
        }

        public IEnumerator<float> UpdateNewCardStatus()
        {
            while (m_hoverCard.ZoomedCard == null)
            {
                yield return Timing.WaitForOneFrame;
            }

            var cardObtainedEffectComponent = m_hoverCard.ZoomedCard.AddCardComponent<CardObtainedEffectComponent>();
            cardObtainedEffectComponent.Initialize(m_hoverCard.ZoomedCard);
            cardObtainedEffectComponent.OnAfterCreation();

            while (cardObtainedEffectComponent.Effect == null)
            {
                yield return Timing.WaitForOneFrame;
            }

            var cardObtainedEffect = m_hoverCard.ZoomedCard.GetVfx<CardObtainedEffectComponent, CardObtainedEffect>();
            cardObtainedEffect.SetupForNewRecipe(true);
        }

        public IEnumerator<float> AnimateIn(RectTransform lootPreviewTarget)
        {
            lootIconContainer.SetActive(true);
            spawnPoof.SetActive(true);

            if (newLootSparkles.IsActive())
            {
                newLootSparkles.Play("sparkles");
            }

            lootIconContainer.anchoredPosition = new Vector2(0, -12f);
            lootIconContainer.DOAnchorPosY(0, 0.4f).SetEase(Ease.OutBack);

            lootIconContainer.localScale = new Vector3(1.4f, .7f, 1);
            lootIconContainer.DOScale(Vector3.one, 0.8f).SetEase(Ease.OutElastic);

            spawnPoof.Play("launch");

            while (spawnPoof.IsDone == false) yield return Timing.WaitForOneFrame;

            spawnPoof.SetActive(false);

            zoomedCardParent.position = lootPreviewTarget.position;
        }

        public void AnimateIn()
        {
            lootIconContainer.SetActive(true);
        }

        public bool Interactable => false;
        public bool Grabbable => false;
        public bool InfoOnly => true;
        public bool Loading => false;
    }
}