using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Asyncoroutine;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables;
using CrossBlitz.Fables.Cutscene;
using CrossBlitz.MainMenu;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.Transition;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using DG.Tweening;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;
using CrossBlitz.Fables.Cutscene.Data;

namespace CrossBlitz.Results
{

    public class ChapterResults
    {
        public bool Initialized;

        public String PlayTime;
        public int AccoladesComplete;
        public int AccoladesMax;

        public int ChapterNumber;
        public String CharacterName;
    }

    public class ChapterResultsScreen : MonoBehaviour, ISceneConfiguration
    {

        public ShakeContainer paperShakeContainer;

        public CanvasGroup resultsScreenCanvas;
        public RectTransform resultsScreenTransform;
        public CanvasGroup chapterContainerCanvas;
        public RectTransform chapterContainerTransform;
        public RectTransform heroQuoteContainer;
        public TextMeshProUGUI heroQuote;
        public SpriteAnimation heroQuoteContainerAnimator;

        public CanvasGroup contentCanvas;
        public RectTransform contentTransform;

        public CanvasGroup statsContentCanvas;
        public RectTransform statsContentTransform;


        public CanvasGroup mapContainerCanvas;
        public float fadeInTime;
        private float waitTime5f = 0.5f;

        public TextMeshProUGUI pageNumber;
        public TextMeshProUGUI chapterNameNumber;
        public TextMeshProUGUI chapterName;
        public TextMeshProUGUI chapterBannerNumber;
        public SpriteAnimation chapterBannerAnimation;

        public RectTransform chapterCompleteTransform;
        public CanvasGroup chapterCompleteCanvas;

        public RectTransform chapterCompleteStampTransform;
        public CanvasGroup chapterCompleteStampCanvas;

        public RectTransform overviewContainerTransform;
        public CanvasGroup overviewContainerCanvas;

        public RectTransform playTimeTransform;
        public CanvasGroup playTimeCanvas;
        public TextMeshProUGUI playTimeLabel;

        public RectTransform accoladesTransform;
        public CanvasGroup accoladesCanvas;
        public TextMeshProUGUI accoladesLabel;

        private List<GameObject> ChapterRelicSlots;

        public HeroCutscenePortraitAnimator heroCutscenePortraitAnimator;

        public HeroInfoWindow ChapterWindow;
        public HeroMainMenuSlot heroSlot;
        public SpriteAnimation ChapterCompleteStamp;
        public SpriteAnimation ScrollingSquares;
        public RawImage ScrollingSquaresRawImg;
        public RawImage[] ScrollingSquaresRaw;

        public CanvasGroup buttonDisplay;
        public CanvasGroup skipButtonDisplay;
        public CanvasGroup pageButtonDisplay;


        public Button nextChapterButton;
        public Button quitButton;
        public Button replayButton;
        public Button shareButton;
        public Button skipButton;
        public Button nextButton;
        public Button prevButton;

        private int currentPage = 1;

        public RectTransform map;

        private string m_heroId;

        private void Start()
        {
            if (App.ChapterResults == null || !App.ChapterResults.Initialized )
            {
                if (App.PlayerData == null)
                {
                    App.Init();
                }

                App.ChapterResults = new ChapterResults();
                App.ChapterResults.Initialized = true;

                App.ChapterResults.PlayTime = "02:19:29";
                App.ChapterResults.AccoladesComplete = 50;
                App.ChapterResults.AccoladesMax = 50;

                App.ChapterResults.ChapterNumber = 1;
                App.ChapterResults.CharacterName = "Violet";

            }


            var hero = Db.HeroDatabase.GetHero(App.ChapterResults.CharacterName);

            heroCutscenePortraitAnimator.SetHeroId(hero.id);

            switch (hero.faction.ToString().ToLower())
            {
                case "balance":
                    ScrollingSquaresRawImg.texture = ScrollingSquaresRaw[0].texture;
                    break;
                case "chaos":
                    ScrollingSquaresRawImg.texture = ScrollingSquaresRaw[1].texture;
                    break;
                case "fortune":
                    ScrollingSquaresRawImg.texture = ScrollingSquaresRaw[2].texture;
                    break;
                case "nature":
                    ScrollingSquaresRawImg.texture = ScrollingSquaresRaw[3].texture;
                    break;
                case "war":
                    ScrollingSquaresRawImg.texture = ScrollingSquaresRaw[4].texture;
                    break;
            }


            heroCutscenePortraitAnimator.SetHeroId(App.ChapterResults.CharacterName);
            ChapterCompleteStamp.Play(hero.faction.ToString().ToLower());
            ScrollingSquares.Play(hero.faction.ToString().ToLower());
            chapterBannerAnimation.Play(hero.faction.ToString().ToLower());


            heroQuoteContainer.localScale = Vector3.zero;

            buttonDisplay.alpha = 0;
            buttonDisplay.interactable = false;

            chapterContainerCanvas.alpha = 0;
            chapterCompleteCanvas.alpha = 0;
            chapterCompleteStampCanvas.alpha = 0;
            chapterCompleteStampTransform.localScale = new Vector3(3, 3, 3);

            overviewContainerCanvas.alpha = 0;
            playTimeCanvas.alpha = 0;
            accoladesCanvas.alpha = 0;

            chapterNameNumber.text = App.ChapterResults.CharacterName + " : CH. " + App.ChapterResults.ChapterNumber;
            chapterNameNumber.text = chapterNameNumber.text.ToUpper();

            var chapter = Db.FablesDatabase.GetChapterByChapterId($"{App.ChapterResults.CharacterName}-1-{App.ChapterResults.ChapterNumber}");

            if (chapter != null)
            {
                chapterName.text = chapter.ChapterName.ToUpper();
            }
            else
            {
                chapterName.text = "???";
            }

            chapterBannerNumber.text = $"CHAPTER {App.ChapterResults.ChapterNumber}";

            nextChapterButton.onClick.AddListener(OnNextChapterButton);
            quitButton.onClick.AddListener(OnQuitButton);
            replayButton.onClick.AddListener(OnReplayButton);
            shareButton.onClick.AddListener(OnShareButton);

           // AudioController.PlaySound("Battle Results-Loop");

            Timing.RunCoroutine(AnimateScreenIn(App.ChapterResults));
        }

        private IEnumerator<float> AnimateScreenIn(ChapterResults results)
        {
            //yield return Timing.WaitUntilDone(TransitionController.TransitionOut(TransitionController.TransitionType.Hex));
            yield return Timing.WaitUntilDone(TransitionController.TransitionOut(TransitionController.TransitionType.Fade));
            yield return Timing.WaitUntilDone(CreateOverview(results));
            yield return Timing.WaitUntilDone(ShowButtons());
        }

        private IEnumerator<float> ShowHeroQuote()
        {
            yield return Timing.WaitForSeconds(0.4f);

            var hero = Db.HeroDatabase.GetHero(App.ChapterResults.CharacterName);

            DialogueSnippetData snippet = null;

            var snippets = hero.dialogueSnippets.FindAll(d => d.Option == "Fable Results/Complete");
            if (snippets.Count > 0)
            {
                snippet = snippets[UnityEngine.Random.Range(0, snippets.Count)];
            }

            if (snippet != null)
            {
                if (!string.IsNullOrEmpty(snippet.SoundFx))
                {
                    AudioController.PlaySound(snippet.SoundFx);
                }

                heroCutscenePortraitAnimator.AnimateToEmotion(snippet.Emotion);

                heroQuoteContainerAnimator.GetComponent<CanvasGroup>().alpha = 1;
                heroQuoteContainerAnimator.Play("open");
                heroQuoteContainerAnimator.RectTransform().localScale = Vector3.one;

                yield return Timing.WaitForSeconds(0.12f);
                heroQuote.text = snippet.Dialogue;
            }


        }

        private IEnumerator<float> CreateOverview(ChapterResults results)
        {

            if (results.AccoladesMax == results.AccoladesComplete)
            {
                accoladesLabel.text = "<color=#768a51>" + results.AccoladesComplete.ToString() + "/" + results.AccoladesMax.ToString() + "</color>";
            }
            else
            {
                accoladesLabel.text = results.AccoladesComplete.ToString() + "<color=#768a51>/" + results.AccoladesMax.ToString() + "</color>";
            }
            playTimeLabel.text = results.PlayTime;


            yield return Timing.WaitForSeconds(fadeInTime * 1);

            chapterContainerCanvas.DOFade(1f, fadeInTime);
            chapterContainerTransform.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, fadeInTime);
            yield return Timing.WaitForSeconds(fadeInTime * 1);

            chapterCompleteCanvas.DOFade(1f, fadeInTime);
            chapterCompleteTransform.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, fadeInTime);
            yield return Timing.WaitForSeconds(fadeInTime * 2);

            chapterCompleteStampCanvas.DOFade(1f, fadeInTime);
            chapterCompleteStampTransform.DOScale(Vector3.one, 0.12f);
            yield return Timing.WaitForSeconds(.14f);

            resultsScreenTransform.DOPunchAnchorPos(Vector2.down * 3f, 0.25f);

            yield return Timing.WaitForSeconds(fadeInTime * 1f);

            overviewContainerCanvas.DOFade(1f, fadeInTime);
            overviewContainerTransform.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, fadeInTime);
            yield return Timing.WaitForSeconds(fadeInTime * 1);

            playTimeCanvas.DOFade(1f, fadeInTime);
            playTimeTransform.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, fadeInTime);
            yield return Timing.WaitForSeconds(fadeInTime * 1);

            accoladesCanvas.DOFade(1f, fadeInTime);
            accoladesTransform.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, fadeInTime);


            //Timing.RunCoroutine(AnimateOut(EventType.OnContinueFromMatchResults));
            //heroQuoteContainer.DOScale(Vector3.one, 0.24f).SetEase(Ease.OutBack);
           // yield return Timing.WaitForSeconds(0.12f);

            Timing.RunCoroutine(ShowHeroQuote());

            yield return Timing.WaitForSeconds(fadeInTime * 4);
        }


        private IEnumerator<float> ShowButtons()
        {
            if (App.ChapterResults.ChapterNumber >= 3)
            {
                nextChapterButton.interactable = false;
                nextChapterButton.SetActive(false);
            }

            buttonDisplay.interactable = true;
            buttonDisplay.DOFade(1, 0.24f);

            // get a buttons rect transform by:
            // continueButton.RectTransform()
            // allowing you to move the button in, in a cool way

            yield break;
        }



        private IEnumerator<float> AnimateOut(EventType type)
        {
            switch (type)
            {
                case  EventType.OnContinueFromMatchResults:
                    heroCutscenePortraitAnimator.AnimateToEmotion("joy");
                    break;
                case EventType.OnReplayMatch:
                    heroCutscenePortraitAnimator.AnimateToEmotion("angry");
                    break;
            }

            yield break;
        }


        private void OnQuitButton()
        {

            Timing.RunCoroutine(OnReturnToChapterSelect());
        }

        private IEnumerator<float> OnReturnToChapterSelect()
        {
            FableController.SetChapter(string.Empty, -1, -1);
            App.BattleResults.ResetData();
            yield return Timing.WaitUntilDone(TransitionController.TransitionIn(TransitionController.TransitionType.Cross));
            yield return Timing.WaitUntilDone(SceneController.Instance.UnloadAllScenes().AsCoroutine());

            StateController.ChangeState(StateDefinition.FABLES_CHAPTER_SELECT);

            //await SceneController.Instance.UnloadScene("ChapterFinished");

            // await TransitionController.TransitionIn(TransitionController.TransitionType.Fade);
        }

        private void OnNextChapterButton()
        {
            //Events.Publish(this, EventType.OnContinueFromMatchResults, new OnContinueFromMatchEventArgs());

            // Timing.RunCoroutine(AnimateOut(EventType.OnContinueFromMatchResults));
            // AudioController.PlaySound("UI-ColiseumStart-NoFX");
            // AudioController.StopPlaylist();

            FableController.SetChapter(App.ChapterResults.CharacterName, 1, App.ChapterResults.ChapterNumber + 1);
            App.BattleResults.ResetData();
            Events.Publish(this, EventType.OnPlayBook, null);

        }

        private void OnReplayButton()
        {
            Events.Publish(null, EventType.StartBattle, new StartBattleEventArgs
            {
                gameMode = new GameMode
                {
                    GameType = GameServer.Mode.GameType,
                    ConnectionMode = GameServer.Mode.ConnectionMode,
                    SceneEnvironment = GameServer.Mode.SceneEnvironment,
                    SceneToReturnTo = GameServer.Mode.SceneToReturnTo,
                    ShowResultsAfterMatch = GameServer.Mode.ShowResultsAfterMatch,
                    BattleUid = GameServer.Mode.BattleUid,
                    DeckUid = GameServer.Mode.DeckUid,
                }
            });

            Timing.RunCoroutine(AnimateOut(EventType.OnReplayMatch));
        }

        private void OnShareButton()
        {
            Events.Publish(this, EventType.OnShareBattleResults, new OnShareBattleResultsEventArgs());
        }




        public Task OnCreated(SceneModel sceneModel)
        {
            return Task.CompletedTask;
        }

        public void OnFocused(bool hasFocus)
        {

        }

        public void OnSceneLoaded(SceneModel model)
        {

        }

        public void OnSceneUnLoaded(SceneModel model)
        {

        }
    }
}