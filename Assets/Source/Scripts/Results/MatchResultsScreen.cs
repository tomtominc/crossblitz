using System.Collections.Generic;
using System.Threading.Tasks;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Accolades;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Cutscene;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.Items.Data;
using CrossBlitz.LevelGrid;
using CrossBlitz.MainMenu;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.UI;
using CrossBlitz.Universal;
using CrossBlitz.Utils;
using DG.Tweening;
using GameDataEditor;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;
using CrossBlitz.Transition;

namespace CrossBlitz.Results
{
    public class MatchResultsScreen : MonoBehaviour, ISceneConfiguration
    {
        public ShakeContainer paperShakeContainer;

        public Camera sceneCamera;
        public CanvasGroup resultsScreenCanvas;

        public RectTransform resultsScreenTransform;
        public CanvasGroup heroContainerCanvas;
        public RectTransform heroContainerTransform;
        public RectTransform heroQuoteContainer;
        public TextMeshProUGUI heroQuote;
        public SpriteAnimation heroQuoteContainerAnimator;
        public TextMeshProUGUI battleNameLabel;
        public SpriteAnimation BattleBannerAnimation;

        public CanvasGroup levelTreeContainerCanvas;
        public RectTransform levelTreeContainerTransform;
        public Button levelTreeButton;

        public CanvasGroup contentCanvas;
        public RectTransform contentTransform;

        public CanvasGroup statsContentCanvas;
        public RectTransform statsContentTransform;

        public CanvasGroup accoladesCanvas;
        public RectTransform accoladesTransform;
        public CanvasGroup lootCanvas;
        public RectTransform lootTransform;
        public RectTransform lootListTransform;
        public RectTransform lootZoomedPreviewTarget;
        public float fadeInTime;
        private float waitTime2f = 0.2f;
        private float waitTime5f = 0.5f;

        public RawImage ScrollingSquaresRawImg;
        public RawImage[] ScrollingSquaresRaw;

        public TextMeshProUGUI pageNumber;
        public RectTransform heroXpBannerTransform;
        public CanvasGroup heroXpBannerCanvas;
        public RectTransform heroXpBarTransform;
        public CanvasGroup heroXpBarCanvas;


        public HeroInfoWindow heroInfoWindow;
        public HeroMainMenuSlot heroSlot;
        public AnimatedSlider heroXpSlider;
        public ShakeContainer xpBarShakeContainer;
        public RectTransform xpBarTransform;
        public HeroCutscenePortraitAnimator heroCutscenePortraitAnimator;

        public NumberLabelFormatter heroXp;

        public RectTransform accoladeDisplayContainer;
        public AccoladesDisplay accoladeDisplayPrefab;

        public RectTransform battleStatDisplayContainer;
        public BattleStatsDisplay battleStatDisplayPrefab;

        public RectTransform ingredientsFoundList;
        public IngredientDisplay ingredientDisplayPrefab;

        public SpriteAnimation levelUpBadge;
        public SpriteAnimation levelUpBanner;

        public CanvasGroup buttonDisplay;
        public CanvasGroup pageButtonDisplay;

        public ResultsLootIcon resultsLootIconPrefab;

        public Button continueButton;
        public Button replayButton;
        public Button shareButton;
        public Button nextButton;
        public Button prevButton;

        private int currentPage = 1;
        private float lootPitch = 1f;
        private float levelPitch = 1f;

        private string m_heroId;
        private float m_originalCameraDepth;

        private void Start()
        {
            LevelTreeController.OnOpened += OnLevelTreeOpened;
            LevelTreeController.OnClosed += OnLevelTreeClosed;

            if (App.BattleResults == null || !App.BattleResults.GetInitialized())
            {
                if (App.PlayerData == null)
                {
                    App.Init();
                }
                App.BattleResults = new BattleResults();
                App.BattleResults.SetInitialized(true);
                App.BattleResults.SetDeckId(App.PlayerDecks.GetDeckBasedOnGameState().uid);
                App.BattleResults.SetHeroLevelBeforeBattleEnd(1);
                App.BattleResults.SetHeroLevelsGained(4);
                App.BattleResults.SetExperienceBeforeBattleEnd(0);
                App.BattleResults.SetExperienceGained(200);
                App.BattleResults.SetDawnDollarsAcquired(300);
                App.BattleResults.SetManaShardsAcquired(100);
                App.BattleResults.SetCompletedAccoladeIds(new List<string>());
                App.BattleResults.SetCardsRewarded(new List<string>
                {
                    GDEItemKeys.Card_SparkleFiendElda,
                    GDEItemKeys.Card_Grimclaw
                });
                App.BattleResults.SetChestsObtained(new List<ItemValue>());
                App.BattleResults.SetIngredientsEarned(new List<ItemValue>
                {
                    new ItemValue {ItemUid = GDEItemKeys.Item_Mushpop, Count = 40},
                    new ItemValue {ItemUid = GDEItemKeys.Item_LushLeaf, Count = 5}
                });
            }

            m_originalCameraDepth = sceneCamera.depth;

            heroQuoteContainer.localScale = Vector3.zero;

            buttonDisplay.alpha = 0;
            buttonDisplay.interactable = false;
            heroContainerCanvas.alpha = 0;
            levelTreeContainerCanvas.alpha = 0;
            accoladesCanvas.alpha = 0;
            lootCanvas.alpha = 0;
            pageNumber.text = currentPage + "/2";

            battleNameLabel.text = Db.BattleDatabase.GetBattleData(App.BattleResults.GetBattleUid()).BattleName.ToUpper();

            continueButton.onClick.AddListener(OnContinueButton);
            replayButton.onClick.AddListener(OnReplayButton);
            shareButton.onClick.AddListener(OnShareButton);
            nextButton.onClick.AddListener(OnNextButton);
            prevButton.onClick.AddListener(OnPrevButton);

            shareButton.SetActive(false);

            if (App.BattleResults.GetMatchResult() == BattleResults.Result.Won)
            {
                replayButton.SetActive(false);
            }

            if (App.PlayerData != null)
            {
                var deck = App.PlayerDecks.GetDeck(App.BattleResults.GetDeckId());
                var heroData = deck.GetHeroData();
                var hero = App.HeroData.GetHero(heroData.id);

                heroCutscenePortraitAnimator.SetHeroId(heroData.id);

                if (hero.level == 1)
                {
                    levelTreeButton.SetActive(false);
                }

                switch (heroData.faction.ToString().ToLower())
                {
                    case "balance":
                        ScrollingSquaresRawImg.texture = ScrollingSquaresRaw[0].texture;
                        break;
                    case "chaos":
                        ScrollingSquaresRawImg.texture = ScrollingSquaresRaw[1].texture;
                        break;
                    case "fortune":
                        ScrollingSquaresRawImg.texture = ScrollingSquaresRaw[2].texture;
                        break;
                    case "nature":
                        ScrollingSquaresRawImg.texture = ScrollingSquaresRaw[3].texture;
                        break;
                    case "war":
                        ScrollingSquaresRawImg.texture = ScrollingSquaresRaw[4].texture;
                        break;
                }

                BattleBannerAnimation.Play(heroData.faction.ToString().ToLower());

            }



            Timing.RunCoroutine(AnimateScreenIn(App.BattleResults));
        }

        private void OnDestroy()
        {
            LevelTreeController.OnOpened -= OnLevelTreeOpened;
            LevelTreeController.OnClosed -= OnLevelTreeClosed;
        }

        private void OnLevelTreeOpened(LevelGridMenu levelGridMenu)
        {
            sceneCamera.depth = -1;

            resultsScreenCanvas.interactable = false;
            resultsScreenCanvas.blocksRaycasts = false;

            buttonDisplay.SetActive(false);
        }

        private void OnLevelTreeClosed()
        {
            heroInfoWindow.RefreshLevelData();

            sceneCamera.depth = m_originalCameraDepth;

            resultsScreenCanvas.interactable = true;
            resultsScreenCanvas.blocksRaycasts = true;

            buttonDisplay.SetActive(true);
        }

        private IEnumerator<float> AnimateScreenIn(BattleResults results)
        {

            CreateBattleStats();
            ShowPageButtonsImmediate();

            //yield return Timing.WaitForSeconds(1f);
            yield return Timing.WaitUntilDone(TransitionController.TransitionOut(TransitionController.TransitionType.Fade));
            //yield return Timing.WaitUntilDone(TransitionController.TransitionOut(TransitionController.TransitionType.Hex));

            if (results.HeroLevelsGained > 0)
            {
                yield return Timing.WaitUntilDone(DisplayExperienceGain(results));
            }
            else
            {
                yield return Timing.WaitUntilDone(DisplayExperienceGain(results));
                //Timing.RunCoroutine(DisplayExperienceGain(results));
                //yield return Timing.WaitForSeconds(0.4f);
            }

            Timing.RunCoroutine(ShowHeroQuote());
            Timing.RunCoroutine(CreateAccolades(results));

            yield return Timing.WaitForSeconds(1);
            yield return Timing.WaitUntilDone(FillLoot(results));
            yield return Timing.WaitForSeconds(1);

            GrantLoot();

            yield return Timing.WaitUntilDone(ShowButtons());

        }

        private void GrantLoot()
        {
            var cardsRewarded = App.BattleResults.GetCardsRewarded();

            if (cardsRewarded?.Count > 0)
            {
                var bookData = App.FableData.GetCurrentBook();

                if (bookData != null)
                {
                    for (var i = 0; i < cardsRewarded.Count; i++)
                    {
                        bookData.AddRecipe(cardsRewarded[i]);
                    }
                }
                else
                {
                    Debug.LogError("Book data is null!!");
                }
            }

            if (App.BattleResults.GetIngredientsEarned()?.Count > 0)
            {
                App.Inventory.GrantItems(App.BattleResults.GetIngredientsEarned());
            }
        }

        private IEnumerator<float> ShowHeroQuote()
        {
            yield return Timing.WaitForSeconds(0.4f);

            var deck = App.PlayerDecks.GetDeck(App.BattleResults.GetDeckId());
            var hero = deck.GetHeroData();
            DialogueSnippetData snippet = null;

            switch (App.BattleResults.GetMatchResult())
            {
                case BattleResults.Result.Won:
                {
                    var snippets = hero.dialogueSnippets.FindAll(d => d.Option == "Match Results/Win");
                    if (snippets.Count > 0)
                    {
                        snippet = snippets[UnityEngine.Random.Range(0, snippets.Count)];
                    }

                    break;
                }
                case BattleResults.Result.Lost:
                {
                    var snippets = hero.dialogueSnippets.FindAll(d => d.Option == "Match Results/Lose");
                    if (snippets.Count > 0)
                    {
                        snippet = snippets[UnityEngine.Random.Range(0, snippets.Count)];
                    }

                    break;
                }
                case BattleResults.Result.Draw:
                {
                    var snippets = hero.dialogueSnippets.FindAll(d => d.Option == "Match Results/Tie");
                    if (snippets.Count > 0)
                    {
                        snippet = snippets[UnityEngine.Random.Range(0, snippets.Count)];
                    }

                    break;
                }
                default:
                {
                    var snippets = hero.dialogueSnippets.FindAll(d => d.Option == "Match Results/Win");
                    if (snippets.Count > 0)
                    {
                        snippet = snippets[UnityEngine.Random.Range(0, snippets.Count)];
                    }

                    break;
                }
            }

            if (snippet != null)
            {
                if (!string.IsNullOrEmpty(snippet.SoundFx))
                {
                    AudioController.PlaySound(snippet.SoundFx);
                }

                heroCutscenePortraitAnimator.AnimateToEmotion(snippet.Emotion);

                heroQuoteContainerAnimator.GetComponent<CanvasGroup>().alpha = 1;
                heroQuoteContainerAnimator.Play("open");
                heroQuoteContainerAnimator.RectTransform().localScale = Vector3.one;

                yield return Timing.WaitForSeconds(0.12f);
                heroQuote.text = snippet.Dialogue;
            }
        }

        private IEnumerator<float> DisplayExperienceGain(BattleResults results)
        {
            var deck = App.PlayerDecks.GetDeck(results.DeckId);
            var hero = deck.GetHeroData();

            m_heroId = hero.id;

            var currentLevel = results.HeroLevelBeforeBattleEnd + 1;

            // Wait before fading anything in
            yield return Timing.WaitForSeconds(fadeInTime * 2);

            heroContainerCanvas.DOFade(1f, fadeInTime);
            heroContainerTransform.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, fadeInTime);

            heroInfoWindow.SetHeroData(hero);
            heroSlot.SetHeroId(hero.id, results.HeroLevelBeforeBattleEnd);

            heroCutscenePortraitAnimator.SetHeroId(hero.id);

            // set this as the xp the player had before the battle.
            if (currentLevel == 31)
            {
                heroXpSlider.SetValues(1, 0, 1);
                heroXpSlider.capped = true;
                heroXpSlider.valueLabel.text = "MAX LEVEL!";
                heroXp.label.text = "";
            }
            else
            {
                heroXpSlider.SetValues(results.ExperienceBeforeBattleEnd, 0, Xp.GetXpForLevel(currentLevel));
                heroXpSlider.speed_denominator = 0.5f;
            }

            // Fade in the EXP Window
            yield return Timing.WaitForSeconds(fadeInTime * 2);

            // Fade in the EXP Recieved Banner
            heroXpBannerCanvas.DOFade(1f, fadeInTime);
            heroXpBannerTransform.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, fadeInTime);
            if (currentLevel != 31) heroXp.UpdateLabel(results.ExperienceGained);

            heroXp.soundOn = true;
            yield return Timing.WaitUntilTrue(() => heroXp.IsFinishedAnimating);
            heroXp.soundOn = false;
            yield return Timing.WaitForSeconds(waitTime2f);

            // Fade in the EXP BAR
            heroXpBarCanvas.DOFade(1f, fadeInTime);
            heroXpBarTransform.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, fadeInTime);
            yield return Timing.WaitForSeconds(fadeInTime);

            var targetValue = results.ExperienceBeforeBattleEnd + results.ExperienceGained;

            while (targetValue >= Xp.GetXpForLevel(currentLevel))
            {
                heroXpSlider.SetTargetValue( Xp.GetXpForLevel(currentLevel) );
                heroXpSlider.soundOn = true;

                while (Mathf.Abs(heroXpSlider.TargetValue - heroXpSlider.Value) > float.Epsilon)
                {
                    yield return Timing.WaitForOneFrame;
                }

                //  paperShakeContainer.Shake(5, 0.5f);

                xpBarTransform.RectTransform().DOPunchAnchorPos(Vector2.up * 3f, 0.5f);
                levelUpBanner.GetComponent<RectTransform>().DOPunchAnchorPos(Vector2.down * 2f, 0.5f);
                levelUpBadge.GetComponent<RectTransform>().DOPunchAnchorPos(Vector2.down * 2f, 0.5f);
                // xpBarShakeContainer.Shake(5, 4f);

                Events.Publish(this, EventType.OnHeroVisuallyLeveledUp, new OnHeroVisuallyLeveledUpEventArgs { heroId = hero.id, newLevel = currentLevel});

                AnimateLevelUp(currentLevel);

                AudioController.PlaySound("CB-Logo - CrossSpawn", "SFX", true).setPitch(levelPitch);
                levelPitch += 0.25f;

                targetValue -= Xp.GetXpForLevel(currentLevel);
                currentLevel++;

                if(currentLevel == 31)
                {
                    heroXpSlider.SetValues(1, 0, 1);
                    heroXpSlider.capped = true;
                    heroXpSlider.valueLabel.text = "MAX LEVEL!";
                    targetValue = 0;
                }
                else
                {
                    heroXpSlider.SetValues(0, 0, Xp.GetXpForLevel(currentLevel));
                    heroXpSlider.SetTargetValue(0);
                }
            }

            if (targetValue > 0)
            {
                heroXpSlider.SetTargetValue( targetValue );
                heroXpSlider.soundOn = true;

                while (Mathf.Abs(heroXpSlider.TargetValue - heroXpSlider.Value) > float.Epsilon)
                {
                    yield return Timing.WaitForOneFrame;
                }

                heroXpSlider.soundOn = false;
            }

            heroXpSlider.soundOn = false;

            // Wait a bit after completing the exp gain
            yield return Timing.WaitForSeconds(waitTime2f);

            // Fade in the Level Tree Button
            levelTreeContainerCanvas.DOFade(1f, fadeInTime);
            levelTreeContainerTransform.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, fadeInTime);
            yield return Timing.WaitForSeconds(fadeInTime * 2);

        }

        private void AnimateLevelUp(int newLevel)
        {
            if (!levelUpBadge.IsActive())
            {
                levelUpBadge.SetActive(true);
                levelUpBadge.Play("idle");

                levelUpBanner.SetActive(true);
                levelUpBanner.Play("spawn");
            }
        }

        private IEnumerator<float> CreateAccolades(BattleResults results)
        {
            var tile = Db.MapDatabase.GetTileByBattle(results.BattleUid);

            var battleData = string.IsNullOrEmpty(tile.battleUid) ?
                Db.BattleDatabase.GetBattleData("2ec69ab7-6656-4889-a0da-8bdf41a93d96") :
                Db.BattleDatabase.GetBattleData(tile.battleUid);

            var clientData = App.FableData.GetTile(tile.uid);

            accoladesCanvas.DOFade(1f, fadeInTime);
            accoladesTransform.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, fadeInTime);

            yield return Timing.WaitForSeconds(fadeInTime * 2);

            accoladeDisplayContainer.DestroyChildren();

            clientData.BattleData?.FixDependencies(battleData);

            for (var i = 0; i < battleData.Accolades.Count; i++)
            {
                var clientAccoladeData = clientData.BattleData?.Accolades[i];
                if (clientAccoladeData == null) continue;
                var accoladeDisplay = Instantiate(accoladeDisplayPrefab, accoladeDisplayContainer);
                accoladeDisplay.SetData( battleData.Accolades[i],
                    clientAccoladeData, clientAccoladeData.IsComplete &&
                    !App.BattleResults.GetCompletedAccoladeIds().Exists( aId => aId  == clientAccoladeData.AccoladeUid ));
                accoladeDisplay.FadeIn();
                yield return Timing.WaitForSeconds(waitTime5f);
            }
        }

        private IEnumerator<float> FillLoot(BattleResults results)
        {
            lootCanvas.DOFade(1f, fadeInTime);
            lootTransform.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, fadeInTime);

            yield return Timing.WaitForSeconds(fadeInTime*2);

            var lootIcons = new List<ResultsLootIcon>();

            if (results.DawnDollarsAcquired > 0)
            {
                var lootIcon = Instantiate(resultsLootIconPrefab, lootListTransform, false);
                lootIcon.SetLoot(string.Empty, string.Empty, results.DawnDollarsAcquired, lootZoomedPreviewTarget);
                lootIcons.Add(lootIcon);
            }

            if (results.CardsRewarded.Count > 0)
            {
                for (var i = 0; i < results.CardsRewarded.Count; i++)
                {
                    var cardItemCount = App.Inventory.GetItemAmount(results.CardsRewarded[i]);

                    var lootIcon = Instantiate(resultsLootIconPrefab, lootListTransform, false);
                    lootIcon.SetLoot(ItemClassType.Card, results.CardsRewarded[i], 1, lootZoomedPreviewTarget, cardItemCount <= 0);
                    lootIcons.Add(lootIcon);
                }
            }

            if (results.ManaShardsAcquired > 0)
            {
                var lootIcon = Instantiate(resultsLootIconPrefab, lootListTransform, false);
                lootIcon.SetLoot(ItemClassType.ManaShard, string.Empty, results.ManaShardsAcquired, lootZoomedPreviewTarget);
                lootIcons.Add(lootIcon);
            }

            if (results.IngredientsEarned.Count > 0)
            {
                for (var i = 0; i < results.IngredientsEarned.Count; i++)
                {
                    var lootIcon = Instantiate(resultsLootIconPrefab, lootListTransform, false);
                    lootIcon.SetLoot(ItemClassType.Ingredient, results.IngredientsEarned[i].ItemUid, results.IngredientsEarned[i].Count, lootZoomedPreviewTarget);
                    lootIcons.Add(lootIcon);
                }
            }

            yield return Timing.WaitForOneFrame;

            for (var i = 0; i < lootIcons.Count; i++)
            {
                AudioController.PlaySound("Text-LetterC", "SFX", true).setPitch(lootPitch);
                Timing.RunCoroutine(lootIcons[i].AnimateIn(lootZoomedPreviewTarget));
                yield return Timing.WaitForSeconds(0.08f);

                lootPitch += 0.1f;
            }

            // if (results.DawnDollarsAcquired > 0)
            // {
            //     dawnDollars.SetActive(true);
            //     dawnDollars.smoothTime = 0.25f;
            //     if (skipButtonPressed) dawnDollars.smoothTime = 0.1f;
            //
            //     dawnDollars.UpdateLabel(results.DawnDollarsAcquired);
            //
            //     yield return Timing.WaitForSeconds(0.05f);
            //
            //     dawnDollars.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, fadeInTime);
            //     dawnDollars.GetComponent<CanvasGroup>().DOFade(1f, fadeInTime);
            //     yield return Timing.WaitUntilTrue(() => dawnDollars.IsFinishedAnimating);
            //     yield return Timing.WaitForSeconds(waitTime2f);
            // }
            //
            // if (results.ManaShardsAcquired > 0)
            // {
            //     manaShards.SetActive(true);
            //     manaShards.smoothTime = 0.25f;
            //     if (skipButtonPressed) manaShards.smoothTime = 0.1f;
            //     manaShards.UpdateLabel(results.ManaShardsAcquired);
            //
            //     yield return Timing.WaitForSeconds(0.05f);
            //
            //     if (results.DawnDollarsAcquired > 0)
            //     {
            //         dawnDollars.RectTransform().DOPunchAnchorPos(Vector2.left * 4f, fadeInTime);
            //     }
            //
            //     manaShards.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, fadeInTime);
            //     manaShards.GetComponent<CanvasGroup>().DOFade(1f, fadeInTime);
            //     yield return Timing.WaitUntilTrue(() => manaShards.IsFinishedAnimating);
            //     yield return Timing.WaitForSeconds(waitTime2f);
            // }
            //
            // if (results.ChestsObtained.Count > 0)
            // {
            //     chests.SetActive(true);
            //     chests.smoothTime = 0.25f;
            //     if (skipButtonPressed) chests.smoothTime = 0.1f;
            //     chests.UpdateLabel(results.ChestsObtained.Count);
            //
            //     yield return Timing.WaitForSeconds(0.05f);
            //
            //     if (results.DawnDollarsAcquired > 0)
            //     {
            //         dawnDollars.RectTransform().DOPunchAnchorPos(Vector2.left * 4f, fadeInTime);
            //     }
            //     if (results.ManaShardsAcquired > 0)
            //     {
            //         manaShards.RectTransform().DOPunchAnchorPos(Vector2.left * 4f, fadeInTime);
            //     }
            //
            //     chests.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, fadeInTime);
            //     chests.GetComponent<CanvasGroup>().DOFade(1f, fadeInTime);
            //     yield return Timing.WaitUntilTrue(() => chests.IsFinishedAnimating);
            //     yield return Timing.WaitForSeconds(waitTime2f);
            // }
            //
            // if (results.CardsRewarded.Count > 0)
            // {
            //     cards.SetActive(true);
            //     cards.smoothTime = 0.25f;
            //
            //     if (skipButtonPressed)
            //     {
            //         cards.smoothTime = 0.1f;
            //     }
            //
            //     cards.UpdateLabel(results.CardsRewarded.Count);
            //
            //     yield return Timing.WaitForSeconds(0.05f);
            //
            //     if (results.DawnDollarsAcquired > 0)
            //     {
            //         dawnDollars.RectTransform().DOPunchAnchorPos(Vector2.left * 4f, fadeInTime);
            //     }
            //     if (results.ManaShardsAcquired > 0)
            //     {
            //         manaShards.RectTransform().DOPunchAnchorPos(Vector2.left * 4f, fadeInTime);
            //     }
            //     if (results.ChestsObtained.Count > 0)
            //     {
            //         chests.RectTransform().DOPunchAnchorPos(Vector2.left * 4f, fadeInTime);
            //     }
            //
            //     cards.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, fadeInTime);
            //     cards.GetComponent<CanvasGroup>().DOFade(1f, fadeInTime);
            //     yield return Timing.WaitUntilTrue(() => cards.IsFinishedAnimating);
            //     yield return Timing.WaitForSeconds(waitTime2f);
            // }
        }

        private IEnumerator<float> CreateIngredients(BattleResults results)
        {
            // ingredientCanvas.DOFade(1f, fadeInTime);
            // ingredientTransform.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, fadeInTime);

            yield return Timing.WaitForSeconds(fadeInTime * 2);

            List<IngredientDisplay> iGrid = new List<IngredientDisplay>();

            for (var i = 0; i < results.IngredientsEarned.Count; i++)
            {
                var ingredientValue = results.IngredientsEarned[i];
                var ingredient = Instantiate(ingredientDisplayPrefab, ingredientsFoundList, false);

                ingredient.SetItemId(ingredientValue.ItemUid, true, ingredientValue.Count);
                ingredient.GetComponent<CanvasGroup>().alpha = 0;
                ingredient.labelFormatter.smoothTime = 0.25f;
                iGrid.Add(ingredient);

                yield return Timing.WaitForSeconds(0.05f);

                ingredient.GetComponent<CanvasGroup>().DOFade(1f, fadeInTime);
                ingredient.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, fadeInTime);

                // Punch the prevously displayed ingredients
                if (iGrid.Count > 0)
                {
                    for (var x = 0; x < iGrid.Count-1; x++)
                    {
                        iGrid[x].RectTransform().DOPunchAnchorPos(Vector2.left * 4f, fadeInTime);
                    }
                }


                yield return Timing.WaitForSeconds(fadeInTime * 2);
                yield return Timing.WaitUntilTrue(() => ingredient.labelFormatter.IsFinishedAnimating);
            }

            // delete the list containing the old ingredients for reference purposes
            iGrid.Clear();
            yield break;
        }

        private void CreateBattleStats()
        {
            //var battleStats = new List<BattleStatsDisplay>();
            var index = 0;

            for (var i = 0; i < 13; i++)
            {
                var stat = string.Empty;
                var description = string.Empty;

                switch (i)
                {
                    case 0:
                        stat = App.BattleResults.GetStats().TurnsTaken.ToString("N0");
                        description = "TURNS TAKEN: ";
                        break;
                    case 1:
                        stat = App.BattleResults.GetStats().TotalClientHeroDamageTaken.ToString("N0");
                        description = "HERO DAMAGE TAKEN: ";
                        break;
                    case 2:
                        stat = App.BattleResults.GetStats().TotalClientMinionDamageTaken.ToString("N0");
                        description = "MINION DAMAGE TAKEN: ";
                        break;
                    case 3:
                        stat = App.BattleResults.GetStats().TotalClientMinionsDestroyed.ToString("N0");
                        description = "MINIONS SLAIN: ";
                        break;
                    case 4:
                        stat = App.BattleResults.GetStats().TotalOpponentMinionsDestroyed.ToString("N0");
                        description = "MINIONS LOST: ";
                        break;
                    case 5:
                        stat = App.BattleResults.GetStats().TotalClientCardsPlayed.ToString("N0");
                        description = "CARDS PLAYED: ";
                        break;
                    case 6:
                        stat = App.BattleResults.GetStats().TotalClientMinionCardsPlayed.ToString("N0");
                        description = "MINION CARDS PLAYED: ";
                        break;
                    case 7:
                        stat = App.BattleResults.GetStats().TotalClientSpellCardsPlayed.ToString("N0");
                        description = "SPELL CARDS PLAYED: ";
                        break;
                    case 8:
                        stat = App.BattleResults.GetStats().TotalClientTrickCardsPlayed.ToString("N0");
                        description = "TRICK CARDS PLAYED: ";
                        break;
                }

                //if (!string.IsNullOrEmpty(description) || !string.IsNullOrEmpty(stat))
                {
                    var battleStat = Instantiate(battleStatDisplayPrefab, battleStatDisplayContainer);
                    var statColor = index % 2 == 0 ? "#6e3e55".ToColor() : "#906a66".ToColor();
                    var showContainer = index % 2 == 0;

                    battleStat.SetData(description,stat,statColor,showContainer);
                    index++;
                }
            }
        }

        private void ShowPageButtonsImmediate()
        {
            pageButtonDisplay.interactable = true;
            pageButtonDisplay.DOFade(1, 0.24f);
            prevButton.enabled = true;
            nextButton.enabled = true;
        }
        private IEnumerator<float> ShowButtons()
        {
            buttonDisplay.interactable = true;
            buttonDisplay.DOFade(1, 0.24f);

            pageButtonDisplay.interactable = true;
            pageButtonDisplay.DOFade(1, 0.24f);
            prevButton.enabled = true;
            nextButton.enabled = true;

            // get a buttons rect transform by:
            // continueButton.RectTransform()
            // allowing you to move the button in, in a cool way

            yield break;
        }

        // private IEnumerator<float> ShowSkipButton()
        // {
        //     skipButtonDisplay.interactable = true;
        //     skipButtonDisplay.DOFade(1, 0.24f);
        //
        //     // get a buttons rect transform by:
        //     // continueButton.RectTransform()
        //     // allowing you to move the button in, in a cool way
        //
        //     yield break;
        // }


        private IEnumerator<float> AnimateOut(EventType type)
        {
            switch (type)
            {
                case  EventType.OnContinueFromMatchResults:
                    heroCutscenePortraitAnimator.AnimateToEmotion("joy");
                    break;
                case EventType.OnReplayMatch:
                    heroCutscenePortraitAnimator.AnimateToEmotion("angry");
                    break;
            }

            yield break;
        }

        private bool WaitUntilCurrencyComplete(int amt, int target)
        {
            return amt >= target;
        }

        private void OnContinueButton()
        {
            Events.Publish(this, EventType.OnContinueFromMatchResults, new OnContinueFromMatchEventArgs());

            //Timing.RunCoroutine(AnimateOut(EventType.OnContinueFromMatchResults));
        }

        private void OnReplayButton()
        {
            Events.Publish(null, EventType.StartBattle, new StartBattleEventArgs
            {
                gameMode =  App.BattleResults.GetLastGameMode()
            });

            //Timing.RunCoroutine(AnimateOut(EventType.OnReplayMatch));
        }

        private void OnShareButton()
        {
            Events.Publish(this, EventType.OnShareBattleResults, new OnShareBattleResultsEventArgs());
        }

        private void OnSkipButton()
        {
            waitTime2f = .1f;
            waitTime5f = .1f;
            fadeInTime = .1f;
            heroXpSlider.speed_denominator = 0.25f;
            heroXp.smoothTime = 0.1f;
            // cards.smoothTime = 0.1f;
            // chests.smoothTime = 0.1f;
            // manaShards.smoothTime = 0.1f;
            // dawnDollars.smoothTime = 0.1f;
            // skipButtonPressed = true;
            // skipButton.enabled = false;
            //skipButtonDisplay.DOFade(0, 0.24f);
            resultsScreenTransform.DOPunchAnchorPos(Vector2.right * 2f, 0.5f);
        }

        private void OnNextButton()
        {
            if (currentPage == 1)
            {

                currentPage = 2;
                nextButton.interactable = false;
                prevButton.interactable = true;

               // contentTransform.DOPunchAnchorPos(Vector2.right * 4f, 0.25f);
                contentCanvas.DOFade(0f, 0.125f);

               // statsContentTransform.DOPunchAnchorPos(Vector2.right * 4f, 0.25f);
                statsContentCanvas.DOFade(1f, 0.125f);


                pageNumber.text = currentPage.ToString() + "/2";

                resultsScreenTransform.DOPunchAnchorPos(Vector2.right * 3f, 0.25f);
            }
        }

        private void OnPrevButton()
        {
            if (currentPage == 2)
            {
                currentPage = 1;
                nextButton.interactable = true;
                prevButton.interactable = false;

               // contentTransform.DOPunchAnchorPos(Vector2.right * 4f, 0.25f);
                contentCanvas.DOFade(1f, 0.125f);

              //  statsContentTransform.DOPunchAnchorPos(Vector2.right * 4f, 0.25f);
                statsContentCanvas.DOFade(0f, 0.125f);

                pageNumber.text = currentPage.ToString() + "/2";

                resultsScreenTransform.DOPunchAnchorPos(Vector2.left * 3f, 0.25f);

            }
        }

        public Task OnCreated(SceneModel sceneModel)
        {
            return Task.CompletedTask;
        }

        public void OnFocused(bool hasFocus)
        {

        }

        public void OnSceneLoaded(SceneModel model)
        {

        }

        public void OnSceneUnLoaded(SceneModel model)
        {

        }
    }
}