using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace CrossBlitz.ClientAPI
{
    public class Watch
    {
        public bool IsRunning;
        public float Time;

        public void Start()
        {
            IsRunning = true;
        }

        public void Restart()
        {
            Time = 0;
        }

        public void Stop()
        {
            IsRunning = false;
        }
    }

    public static class Timers
    {
        private static Dictionary<string, Watch> Watches;

        public static void Init()
        {
            Watches = new Dictionary<string, Watch>();
        }

        public static void Start(string watchId)
        {
            var watch = GetWatch(watchId);

            if (!IsRunning(watchId))
            {
                watch.Start();
            }

            Debug.Log($"[TIMER START] {watchId}");
        }

        public static void Update()
        {
            foreach (var watch in Watches)
            {
                if (watch.Value.IsRunning)
                {
                    watch.Value.Time += Time.deltaTime;
                }
            }
        }

        public static void Restart(string watchId)
        {
            var watch = GetWatch(watchId);
            watch.Restart();
        }

        public static bool IsRunning(string watchId)
        {
            var watch = GetWatch(watchId);
            return watch.IsRunning;
        }

        public static void Stop(string watchId)
        {
            var watch = GetWatch(watchId);
            watch.Stop();

            //Debug.Log($"[TIMER STOP] {watchId}");
        }

        public static void StopAll(bool kill)
        {
            foreach (var watch in Watches)
            {
                Stop(watch.Key);
            }

            if (kill)
            {
                Watches = null;
            }
        }

        public static Watch GetWatch(string watchId)
        {
            if (Watches == null)
            {
                return null;
            }

            if (!Watches.ContainsKey(watchId))
            {
                Watches.Add(watchId, new Watch());
            }

            return Watches[watchId];
        }
    }
}