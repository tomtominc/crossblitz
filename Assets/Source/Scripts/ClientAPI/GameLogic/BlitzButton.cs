using System;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.ClientAPI
{
    public class BlitzButton : MonoBehaviour
    {
        public GameObject mulliganPhase;
        public Button blitzButton;
        public GameObject opponentButton;
        public GameObject combatButton;

        public SpriteAnimation buttonFlipAnimator;
        public SpriteAnimation backFrame;

        public RectTransform swordRect;

        public SpriteAnimation noActionsLeftOverlay;
        public SpriteAnimation noActionsLeftOutline;

        public RectTransform shakeContainer;

        private RectTransform m_rectTransform;
        private Vector2 m_swordInPosition;
        public event Action OnPressed;

        private void Awake()
        {
            m_rectTransform = transform as RectTransform;
            m_swordInPosition = swordRect.anchoredPosition;
            swordRect.anchoredPosition = new Vector2(swordRect.sizeDelta.x, m_swordInPosition.y);

            blitzButton.onClick.AddListener(Pressed);

            OnMulliganPhase();
        }

        private void Pressed()
        {
            OnPressed?.Invoke();
        }

        public void SetInteractable(bool interactable)
        {
            blitzButton.interactable = interactable;
        }

        public void ClientHasNoActionsLeft(bool e)
        {
            if (e)
            {
                noActionsLeftOverlay.SetActive(true);
                noActionsLeftOverlay.Play("turns-done-idle");
                noActionsLeftOutline.Play("spawn", () => { noActionsLeftOutline.Play("idle"); });
            }
            else
            {
                noActionsLeftOverlay.SetActive(false);
            }
        }

        // =====================================================================================================
        // FLIP TO DIFFERENT STATES
        // =====================================================================================================

        private void FlipButton(string flipAnim, Action callback)
        {
            buttonFlipAnimator.SetActive(true);
            buttonFlipAnimator.Play(flipAnim, callback);

            blitzButton.interactable = false;
            blitzButton.SetActive(false);
            opponentButton.SetActive(false);
            combatButton.SetActive(false);
            mulliganPhase.SetActive(false);

            m_rectTransform.DOPunchAnchorPos(Vector2.down * 4f, 0.12f);
        }

        [Sirenix.OdinInspector.Button]
        public void FlipToPlayerTurn()
        {
            AudioController.PlaySound("battle_blitz_ready", "BARDSFX"); // sword in
            FlipButton("action-to-blitz", OnPlayerButtonFlipped);
        }

        private void OnPlayerButtonFlipped()
        {
            buttonFlipAnimator.SetActive(false);
            blitzButton.SetActive(true);
            blitzButton.interactable = true;
            backFrame.Play("player");

            swordRect.DOAnchorPos(m_swordInPosition, 0.24f).SetEase(Ease.OutBack);
            shakeContainer.DOPunchAnchorPos(Vector3.left*10, 0.34f).SetDelay(0.12f);
        }

        [Sirenix.OdinInspector.Button]
        public void FlipToOpponentTurn()
        {
            AudioController.PlaySound("battle_blitz_activate_enemy", "BARDSFX"); // normal flip
            FlipButton("action-to-enemy", OnOpponentButtonFlipped);
        }

        private void OnOpponentButtonFlipped()
        {
            buttonFlipAnimator.SetActive(false);
            opponentButton.SetActive(true);
            backFrame.Play("enemy");
        }

        [Sirenix.OdinInspector.Button]
        public void FlipToCombatPhase(Team from)
        {
            swordRect.DOAnchorPos(new Vector2(swordRect.sizeDelta.x, m_swordInPosition.y), 0.5f)
                .SetEase(Ease.InBack);

            var flipAnim = from == Team.Client ? "blitz-to-action" : "enemy-to-action";

            if(flipAnim == "blitz-to-action")   AudioController.PlaySound("battle_blitz_activate_player", "BARDSFX"); // sword away
            else                                AudioController.PlaySound("battle_blitz_activate_enemy", "BARDSFX"); // normal flip

            FlipButton(flipAnim, OnCombatButtonFlipped);
        }

        private void OnCombatButtonFlipped()
        {
            buttonFlipAnimator.SetActive(false);
            combatButton.SetActive(true);
            backFrame.Play("action-phase");
        }

        public void OnMulliganPhase()
        {
            mulliganPhase.SetActive(true);
            buttonFlipAnimator.SetActive(false);
            blitzButton.SetActive(false);
            backFrame.Play("action-phase");
        }

    }
}