using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.GameVfx;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using MEC;
using QFSW.QC.Utilities;
using UnityEngine;

namespace CrossBlitz.ClientAPI
{
    public class CommandResolver
    {
        public const string ResolvingTag = "command-queue";

        public bool executing;
        public CommandType current;
        public List<CommandType> resolvedCommands;

        private Queue<CommandQueue> _queue;
        private List<Command> m_removals;
        public const float WaitForGenericTrigger = 0.4f;

        public bool Executing => executing;
        public int QueueCount;

        public CommandResolver()
        {
            current = CommandType.NULL;
            resolvedCommands = new List<CommandType>();
            _queue = new Queue<CommandQueue>();
        }

        public void Dispose()
        {
            Timing.KillCoroutines(ResolvingTag);

            resolvedCommands = null;
            _queue = null;
        }

        public void Execute(CommandQueue commandQueue)
        {
            commandQueue.ReorderAndSquashVisuals();

            var log = "QUEUING COMMANDS:\n";

            for (var i = 0; i < commandQueue.commands.Count; i++)
            {
                log += $"{commandQueue.commands[i].Type}\n";
            }

            _queue.Enqueue(commandQueue);

            if (!executing)
            {
                executing = true;
                Timing.RunCoroutine(ResolveCommands(), ResolvingTag);
            }
        }

        public void RemoveCommandFromResolve(Command command)
        {
            m_removals ??= new List<Command>();
            m_removals.Add(command);
        }

        private IEnumerator<float> ResolveCommands()
        {
            while (_queue.Count > 0)
            {
                var commandQueue = _queue.Dequeue();
                var queue = commandQueue.ToQueue();

                while (queue.Count > 0)
                {
                    var command = queue.Dequeue();

                    QueueCount = queue.Count;
                    current = command.Type;

                    if (command.Delay > 0)
                    {
                        yield return Timing.WaitForSeconds(command.Delay);
                    }

                    HandleRelicActivation(command);

                    yield return Timing.WaitUntilDone(command.Resolve(), ResolvingTag);

                    if (m_removals != null)
                    {
                        for (var i = 0; i < m_removals.Count; i++)
                        {
                            var removal = m_removals[i];

                            if (queue.Contains(removal))
                            {
                                var queueList = queue.ToList();
                                queueList.Remove(removal);
                                queue = new Queue<Command>(queueList);
                            }
                        }
                    }

                    resolvedCommands.Add(current);
                }
            }

            // make sure we're not "executing" anymore
            executing = false;
            current = CommandType.NULL;

            yield return Timing.WaitForSeconds(0.24f);

            if (executing) yield break;

            //EvaluateDeath();
            OnFinishedResolving();
        }

        private void HandleRelicActivation(Command command)
        {
            if (command != null && !string.IsNullOrEmpty(command.SourceUid) && GameServer.State != null)
            {
                var source = GameServer.State.GetCard(command.SourceUid);

                if (source != null && source.characterData != null && (source.characterData.type == CardType.Relic || source.characterData.type == CardType.Elder_Relic))
                {
                    GameplayScreen.Instance.heroSideBarContainer.TriggerRelicSlot(source.uid);
                }
            }
        }

        private void OnFinishedResolving()
        {
            GameplayScreen.Instance.Refresh();

            var matchFinished = GameServer.CheckForMatchFinished();

            if (matchFinished)
            {
                OnMatchFinished();
            }
        }

        private void OnMatchFinished()
        {
            _queue = new Queue<CommandQueue>();
            Debug.Log("Game ends!");
            // the game ended!
        }

        public void EvaluateDeath()
        {
            var targetCards = GameplayScreen.Instance.GetBoardCards();
            targetCards.RemoveAll(c => !c.State.IsDead());
            targetCards = targetCards.DistinctBy(c => c.State.uid).ToList();
            targetCards = targetCards.OrderBy(c => c != null && c.State.HasTrigger(TriggerType.DEATHRATTLE)).ToList();

            for (var i = 0; i < targetCards.Count; i++)
            {
                var targetCardObject = targetCards[i];
                if (targetCardObject == null) continue;
                var damageFx = targetCardObject.View.GetVfx<DamageEffectComponent, DamageEffect>();
                Timing.RunCoroutine(damageFx.EvaluateDeath(targetCardObject.State).CancelWith(targetCardObject.gameObject));
            }
        }

        public void CleanUpDestroyedMinions()
        {
            var visibleBoardCards = GameplayScreen.Instance.GetBoardCards();

            for (var i = 0; i < visibleBoardCards.Count; i++)
            {
                var boardCard = visibleBoardCards[i];

                if (boardCard != null && boardCard.State.GetHealth() <= 0)
                {
                    boardCard.UpdateViewFromState();

                    var death = boardCard.View.GetVfx<DeathEffectComponent, DeathEffect>();

                    if (!death.IsPlaying)
                    {
                        Timing.RunCoroutine(death.Play(null, null));
                    }
                }
                else if (boardCard != null)
                {
                    boardCard.UpdateViewFromState();
                }
            }
        }

        public bool HasUnresolvedCommands(GameCardState cardState)
        {
            var queueList = _queue.ToList();

            for (var i = 0; i < queueList.Count; i++)
            {
                for (var j = 0; j < queueList[i].commands.Count; j++)
                {
                    var command = queueList[i].commands[j];
                    if (command.SourceUid == cardState.uid || (command.TargetUids != null && command.TargetUids.Contains(cardState.uid))) return true;
                }
            }

            return false;
        }
    }
}
