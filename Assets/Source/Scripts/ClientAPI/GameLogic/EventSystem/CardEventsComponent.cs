using System;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using Source.Scripts.Card;
using UnityEngine;

namespace CrossBlitz.ClientAPI.GameLogic.EventSystem
{
    public class CardEventsComponent : MonoBehaviour, ICardComponent
    {
        public event Action<GameCardView> OnFrozen;
        public event Action<GameCardView> OnUnFrozen;
        public event Action<GameCardView> OnDamaged;
        public event Action<GameCardView> OnDeath;
        public event Action<GameCardView> OnLostBarrier;
        public event Action<GameCardView> OnSummoned;
        public event Action<GameCardView> OnSilenced;
        public event Action<GameCardView> OnTransformed;

        private CardView _card;
        private GameCardView _gameCard;
        public void Initialize(CardView card)
        {
            _card= card;
        }

        public void OnAfterCreation()
        {
            _gameCard = _card.GetCardComponent<GameCardView>();
        }

        public void Summoned()
        {
            OnSummoned?.Invoke(_gameCard);
        }

        public void Frozen()
        {
            OnFrozen?.Invoke(_gameCard);
        }

        public void UnFrozen()
        {
            OnUnFrozen?.Invoke(_gameCard);
        }

        public void Damaged()
        {
            OnDamaged?.Invoke(_gameCard);
        }

        public void LostBarrier()
        {
            OnLostBarrier?.Invoke(_gameCard);
        }

        public void Silence()
        {
            OnSilenced?.Invoke(_gameCard);
        }

        public void Transformed()
        {
            OnTransformed?.Invoke(_gameCard);
        }

        public void Die()
        {
            OnDeath?.Invoke(_gameCard);
        }
    }
}