using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Grid;
using CrossBlitz.InputAPI;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using TakoBoyStudios.Events;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CrossBlitz.ClientAPI.GameLogic.EventSystem
{
    public enum EventType
    {
        Null,
        OnHeroChanged,
        OnHeroLevelChanged,
        OnBookChanged,
        OnPlayBook,
        ChangeState,
        OnStateChanged,
        OnCardHoveredOver,
        ModifyCurrency,
        OnModifiedCurrency,
        ModifyItemUses,
        ModifyItemsUses,
        OnModifiedItemUses,
        OnMenuStatusChanged,
        OpenTileInfo,
        OpenChapterInfo,
        OnHexTilePointerEnter,
        OnHexTilePointerExit,
        OpenNodeMetadataEditor,
        OpenTileEditor,
        OpenItemEditor,
        OpenCardEditorInstance,
        OnFableTileDataChanged,
        OpenBookEditor,
        OpenCutsceneEditor,
        OpenHeroEditor,
        OpenDialogueEditor,
        OnError,
        OpenRoomEditor,
        OnNodeSelected,
        OnHeroVisuallyLeveledUp,
        OnContinueFromMatchResults,
        OnReplayMatch,
        OnShareBattleResults,
        OnPlayerEnteredMap,
        OnPlayerArrivedAtTile,
        OnPlayerExitTile,

        OnShakeCharacter,
        OnEnteredFableMap,
        OnFablesWeatherChanged,
        OnBounceCharacter,
        OnPlayerStartedWalkingFromTile,
        OnPlayerInvokedTile,
        OnHeroHealthChanged,
        OnHeroesReset,
        OnPlayerQuestsChanged,

        OnOpenCollectionMenu,
        OnFableInputStateChanged,

        // Game Events --

        StartBattle,
        OnStartMatch,
        OnMatchWon,
        OnMatchLost,
        OnMatchDraw,
        OnContinueFromMatchFinished,
        OnTurnStart,
        OnTurnEnd,
        OnMatchDisposed,
        OnCardDrawn,
        OnCardDiscarded,
        OnMinionSummoned,
        OnCardChangedLocation,
        OnCharacterDamaged,
        OnCharacterStatsModified,
        OnMinionDestroyed,
        OnCardPlayed,
        OnCardClicked,
        OnCardEnteredActiveBoardTarget,
        OnCardExitActiveBoardTarget,
        OnModifiedMana,
        OnCardAddedToDeck,
        OnMinionFrozen,
        OnCardTrackedAndAddedToHand,
        OnGainedArmor,
        OnAccoladeComplete,
        OnSpawnSaveIcon,
        OnBlitzBurstInstantFill,
        OnCharacterHealed,
        OnTrapTriggered,
        OnTrapSet
    }

    public static class Events
    {
        private static bool _init;
        private static Action<IMessage> OnHeroChanged;
        private static Action<IMessage> OnHeroLevelChanged;
        private static Action<IMessage> OnBookChanged;
        private static Action<IMessage> OnPlayBook;
        private static Action<IMessage> ChangeState;
        private static Action<IMessage> OnStateChanged;
        private static Action<IMessage> OnCardHoveredOver;
        private static Action<IMessage> ModifyCurrency;
        private static Action<IMessage> OnModifiedCurrency;
        private static Action<IMessage> ModifyItemUses;
        private static Action<IMessage> ModifyItemsUses;
        private static Action<IMessage> OnModifiedItemUses;
        private static Action<IMessage> OnMenuStatusChanged;
        private static Action<IMessage> OpenTileInfo;
        private static Action<IMessage> OnHexTilePointerEnter;
        private static Action<IMessage> OnHexTilePointerExit;
        private static Action<IMessage> OpenNodeMetadataEditor;
        private static Action<IMessage> OpenCardEditorInstance;
        private static Action<IMessage> OnError;
        private static Action<IMessage> OnFableTileDataChanged;
        private static Action<IMessage> OpenChapterInfo;
        private static Action<IMessage> OpenBookEditor;
        private static Action<IMessage> OpenTileEditor;
        private static Action<IMessage> OpenItemEditor;
        private static Action<IMessage> OpenCutsceneEditor;
        private static Action<IMessage> OpenHeroEditor;
        private static Action<IMessage> OpenDialogueEditor;
        private static Action<IMessage> OpenRoomEditor;
        private static Action<IMessage> OnNodeSelected;
        private static Action<IMessage> OnHeroVisuallyLeveledUp;
        private static Action<IMessage> OnContinueFromMatch;
        private static Action<IMessage> OnReplayMatch;
        private static Action<IMessage> OnShareBattleResults;
        private static Action<IMessage> OnPlayerEnteredMap;
        private static Action<IMessage> OnPlayerArrivedAtTile;
        private static Action<IMessage> OnPlayerExitTile;
        private static Action<IMessage> StartBattle;
        private static Action<IMessage> OnContinueFromMatchFinished;

        private static Action<IMessage> OnShakeCharacter;
        private static Action<IMessage> OnEnteredFableMap;
        private static Action<IMessage> OnFablesWeatherChanged;
        private static Action<IMessage> OnBounceCharacter;
        private static Action<IMessage> OnPlayerStartedWalkingFromTile;
        private static Action<IMessage> OnPlayerInvokedTile;
        private static Action<IMessage> OnHeroHealthChanged;
        private static Action<IMessage> OnHeroesReset;
        private static Action<IMessage> OnPlayerQuestsChanged;

        private static Action<IMessage> OnOpenCollectionMenu;
        private static Action<IMessage> OnFableInputStateChanged;

        // Game Events --
        private static Action<IMessage> OnStartMatch;
        private static Action<IMessage> OnMatchWon;
        private static Action<IMessage> OnMatchLost;
        private static Action<IMessage> OnMatchDraw;
        private static Action<IMessage> OnTurnStart;
        private static Action<IMessage> OnTurnEnd;
        private static Action<IMessage> OnMatchDisposed;
        private static Action<IMessage> OnCardDrawn;
        private static Action<IMessage> OnCardDiscarded;
        private static Action<IMessage> OnMinionSummoned;
        private static Action<IMessage> OnCardChangedLocation;
        private static Action<IMessage> OnCharacterDamaged;
        private static Action<IMessage> OnCharacterStatsModified;
        private static Action<IMessage> OnMinionDestroyed;
        private static Action<IMessage> OnCardPlayed;
        private static Action<IMessage> OnCardClicked;
        private static Action<IMessage> OnCardEnteredActiveBoardTarget;
        private static Action<IMessage> OnCardExitActiveBoardTarget;
        private static Action<IMessage> OnModifiedMana;
        private static Action<IMessage> OnCardAddedToDeck;
        private static Action<IMessage> OnMinionFrozen;
        private static Action<IMessage> OnCardTrackedAndAddedToHand;
        private static Action<IMessage> OnGainedArmor;
        private static Action<IMessage> OnAccoladeComplete;
        private static Action<IMessage> OnSpawnSaveIcon;
        private static Action<IMessage> OnBlitzBurstInstantFill;
        private static Action<IMessage> OnCharacterHealed;
        private static Action<IMessage> OnTrapTriggered;
        private static Action<IMessage> OnTrapSet;

        private static Dictionary<EventType, Action<IMessage>> EventMessages;

        private static void Init()
        {
            EventMessages = new Dictionary<EventType, Action<IMessage>>
            {
                {EventType.OnHeroChanged, OnHeroChanged},
                {EventType.OnHeroLevelChanged, OnHeroLevelChanged},
                {EventType.OnBookChanged, OnBookChanged},
                {EventType.OnPlayBook, OnPlayBook},
                {EventType.ChangeState, ChangeState},
                {EventType.OnStateChanged , OnStateChanged},
                {EventType.OnCardHoveredOver, OnCardHoveredOver},
                {EventType.ModifyCurrency,ModifyCurrency},
                {EventType.OnModifiedCurrency, OnModifiedCurrency},
                {EventType.ModifyItemUses,ModifyItemUses},
                {EventType.ModifyItemsUses,ModifyItemsUses},
                {EventType.OnModifiedItemUses, OnModifiedItemUses},
                {EventType.OnMenuStatusChanged, OnMenuStatusChanged},
                {EventType.OpenTileInfo, OpenTileInfo},
                {EventType.OnHexTilePointerEnter, OnHexTilePointerEnter },
                {EventType.OnHexTilePointerExit, OnHexTilePointerExit},
                {EventType.OpenNodeMetadataEditor, OpenNodeMetadataEditor },
                {EventType.OpenCardEditorInstance, OpenCardEditorInstance },
                {EventType.OpenItemEditor, OpenItemEditor},
                {EventType.OnError, OnError },
                {EventType.OnFableTileDataChanged, OnFableTileDataChanged },
                {EventType.OpenChapterInfo, OpenChapterInfo },
                {EventType.OpenBookEditor, OpenBookEditor },
                {EventType.OpenTileEditor, OpenTileEditor },
                {EventType.OpenCutsceneEditor, OpenCutsceneEditor},
                {EventType.OpenHeroEditor, OpenHeroEditor},
                {EventType.OpenDialogueEditor, OpenDialogueEditor},
                {EventType.OpenRoomEditor, OpenRoomEditor},
                {EventType.OnNodeSelected, OnNodeSelected},
                {EventType.OnHeroVisuallyLeveledUp, OnHeroVisuallyLeveledUp},
                {EventType.OnContinueFromMatchResults, OnContinueFromMatch},
                {EventType.OnReplayMatch, OnReplayMatch},
                {EventType.OnShareBattleResults, OnShareBattleResults},
                {EventType.StartBattle, StartBattle},
                {EventType.OnPlayerEnteredMap, OnPlayerEnteredMap},
                {EventType.OnPlayerArrivedAtTile, OnPlayerArrivedAtTile},
                {EventType.OnPlayerExitTile, OnPlayerExitTile},
                {EventType.OnMatchWon, OnMatchWon},
                {EventType.OnMatchLost, OnMatchLost},
                {EventType.OnMatchDraw, OnMatchDraw},
                {EventType.OnContinueFromMatchFinished, OnContinueFromMatchFinished},
                {EventType.OnTurnStart, OnTurnStart},
                {EventType.OnTurnEnd, OnTurnEnd},
                {EventType.OnMatchDisposed, OnMatchDisposed},
                {EventType.OnCardDrawn, OnCardDrawn},
                {EventType.OnMinionSummoned, OnMinionSummoned},
                {EventType.OnCardChangedLocation, OnCardChangedLocation},
                {EventType.OnCharacterDamaged, OnCharacterDamaged},
                {EventType.OnCharacterStatsModified, OnCharacterStatsModified},
                {EventType.OnMinionDestroyed, OnMinionDestroyed},
                {EventType.OnCardPlayed, OnCardPlayed},
                {EventType.OnCardClicked, OnCardClicked},
                {EventType.OnCardEnteredActiveBoardTarget, OnCardEnteredActiveBoardTarget},
                {EventType.OnCardExitActiveBoardTarget, OnCardExitActiveBoardTarget},
                {EventType.OnModifiedMana, OnModifiedMana},
                {EventType.OnCardAddedToDeck, OnCardAddedToDeck},
                {EventType.OnMinionFrozen, OnMinionFrozen},
                {EventType.OnShakeCharacter, OnShakeCharacter},
                {EventType.OnEnteredFableMap, OnEnteredFableMap},
                {EventType.OnFablesWeatherChanged, OnFablesWeatherChanged},
                {EventType.OnBounceCharacter, OnBounceCharacter},
                {EventType.OnPlayerStartedWalkingFromTile, OnPlayerStartedWalkingFromTile},
                {EventType.OnPlayerInvokedTile, OnPlayerInvokedTile},
                {EventType.OnHeroHealthChanged, OnHeroHealthChanged},
                {EventType.OnHeroesReset, OnHeroesReset},
                {EventType.OnPlayerQuestsChanged, OnPlayerQuestsChanged},
                {EventType.OnStartMatch, OnStartMatch},
                {EventType.OnCardTrackedAndAddedToHand, OnCardTrackedAndAddedToHand},
                {EventType.OnCardDiscarded, OnCardDiscarded},
                {EventType.OnGainedArmor , OnGainedArmor},
                {EventType.OnAccoladeComplete, OnAccoladeComplete},
                {EventType.OnSpawnSaveIcon, OnSpawnSaveIcon},
                {EventType.OnBlitzBurstInstantFill, OnBlitzBurstInstantFill},
                {EventType.OnCharacterHealed, OnCharacterHealed},
                {EventType.OnTrapTriggered , OnTrapTriggered},
                {EventType.OnTrapSet , OnTrapSet},
                {EventType.OnOpenCollectionMenu , OnOpenCollectionMenu},
                {EventType.OnFableInputStateChanged, OnFableInputStateChanged }
            };

            var eventTypes = (EventType[])Enum.GetValues(typeof(EventType));

            for (var i = 0; i < eventTypes.Length; i++)
            {
                if (eventTypes[i] == EventType.Null) continue;

                if (!EventMessages.ContainsKey(eventTypes[i]))
                {
                    Debug.LogError($"Event Type: {eventTypes[i]} was not added to the event dictionary, you'll need to add it for the event to work!");
                }
            }

            _init = true;
        }

        public static void Subscribe(EventType eventType, Action<IMessage> handler)
        {
            if (!_init) Init();
            EventMessages[eventType] += handler;
        }

        public static void Unsubscribe(EventType eventType, Action<IMessage> handler)
        {
            if (!_init) Init();
            if (!EventMessages.ContainsKey(eventType)) return;
            EventMessages[eventType] -= handler;
        }

        public static void Publish(object sender, EventType eventType, object data)
        {
            if (!Server.IsOnMainThread())
            {
                Debug.Log($"Not on the main thread, this publish call is invalid! {eventType}");
                return;
            }

            if (!_init) Init();

            Message lMessage = Message.allocate();

            lMessage.Sender = sender;
            lMessage.MessageType = eventType;
            lMessage.Data = data;

            EventMessages[eventType]?.Invoke(lMessage);

            lMessage.Release();
        }
    }

    public class OnHeroLevelChangedEventArgs
    {
        public string heroId;
        public int level;
    }

    public class OnBookChangedEventArgs
    {
        public string bookHero;
        public int chapterNumber;
    }

    public class StateChangeEventArgs
    {
        public StateDefinition state;
    }

    public class ModifyCurrencyEventArgs
    {
        public string CurrencyCode;
        public int Amount;
    }

    public class OnModifiedCurrencyEventArgs
    {
        public string CurrencyCode;
        public int AmountLeft;
    }

    public class ModifyItemUsagesEventArgs
    {
        public string ItemName;
        public int UseAmount;
    }

    public class OnModifiedItemUsesEventArgs
    {
        public List<string> ItemNames;
    }

    public class ModifyItemsUsagesEventArgs
    {
        public List<string> ItemNames;
        public List<int> UseAmounts;
    }

    public class OnMenuStatusChangedEventArgs
    {
        public enum MenuStatus
        {
            Opened,
            Closed,
        }

        public MenuStatus Status;
        public string MenuName;
    }

    public class OpenTileInfoEventArgs
    {
        public FableTileData tileData;
        public ClientTileData clientTileData;
    }

    public class OnHexTilePointerEnterEventArgs
    {
        public HexTile2D tile;
    }

    public class OnHexTilePointerExitEventArgs
    {
        public HexTile2D tile;
    }

    public class OpenNodeMetadataEditorEventArgs
    {
        public FableTileData tile;
    }

    public class OpenCardEditorInstanceEventArgs
    {
        public string cardId;
    }

    public class OnErrorEventArgs
    {
        public string errorTitle;
        public string errorMessage;
    }

    public class OnFableTileDataChanged
    {
        public int tileUid;
    }

    public class OnOpenChapterInfoEventArgs
    {
        public string chapterUid;
    }

    public class OpenTileEditorEventArgs
    {
        public int tileUid;
    }

    public class OpenCutsceneEditorEventArgs
    {
        public bool createNewCutscene;
        public string cutsceneUid;
    }

    public class OpenHeroEditorEventArgs
    {
        public string heroId;
    }

    public class OpenDialogueEditorEventArgs
    {
        public string dialogueUid;
    }

    public class OpenRoomEditorEventArgs
    {
        public string roomUid;
    }

    /// <summary>
    /// Sends when a node is selected and deselected, when deselected, the tileUid will be 0.
    /// </summary>
    public class OnNodeSelectedEventArgs
    {
        public int tileUid;
    }

    public class OnHeroVisuallyLeveledUpEventArgs
    {
        public string heroId;
        public int newLevel;
    }

    public class OnContinueFromMatchEventArgs
    {

    }

    public class OnReplayMatchEventArgs
    {

    }

    public class OnShareBattleResultsEventArgs
    {

    }

    public class OnPlayerEnteredMapEventArgs
    {
        public GridPlayer player;
        public HexTile2D tile;
    }

    public class OnPlayerArrivedAtTileEventArgs
    {
        public bool fromEnter;
        public GridPlayer player;
        public HexTile2D tile;
    }

    public class OnPlayerInvokedTileEventArgs
    {
        public HexTile2D tile;
    }

    public class OnPlayerExitTileEventArgs
    {
        public GridPlayer player;
        public HexTile2D tile;
    }

    public class StartBattleEventArgs
    {
        public GameMode gameMode;

        // for other modes we need to fill out other settings.
    }

    public class OnTurnStartEventArgs
    {
        public bool isClient;
        public int totalTurns;
    }

    public class OnTurnEndEventArgs
    {
        public bool isClient;
        public int totalTurns;
    }

    public class OnCardDrawnEventArgs
    {
        public Team team;
        public bool mulligan;
        public int drawCount;
    }

    public class OnCardDiscardedEventArgs
    {
        public Team team;
        public int discardCount;
        public List<string> discardedCards;
    }

    public class OnMinionSummonedEventArgs
    {
        public bool isClientCard;
        public SummonCommand.Method method;
        public string cardUid;
    }

    public class OnCardChangedLocationEventArgs
    {
        public string cardUid;
        public CardLocation oldLocation;
        public CardLocation newLocation;
    }

    public class OnCharacterDamagedEventArgs
    {
        public string characterUid;
        public string damageSourceUid;
        public bool isClientCharacter;
        public int damageAmount;
    }

    public class OnCharacterStatsModifiedEventArgs
    {
        public string characterUid;
        public string appliedBy;
        public bool isClientCharacter;
        public int powerAmount;
        public int healthAmount;
    }

    public class OnMinionDestroyedEventArgs
    {
        public string cardUid;
        public bool isClientCard;
    }

    public class OnCardPlayedEventArgs
    {
        public bool isClientCard;
        public string cardUid;
    }

    public class OnCardClickedEventArgs
    {
        public string cardUid;
        public PointerEventData.InputButton button;
    }

    public class OnCardEnteredActiveBoardTargetEventArgs
    {
        public string cardUid;
    }

    public class OnCardExitActiveBoardTargetEventArgs
    {
        public string cardUid;
    }

    public class OnModifiedManaEventArgs
    {
        public Team team;
        public int currentMana;
        public int totalMana;
        public bool goldMana;
        public bool newTurn;
    }

    public class OnCardAddedToDeckEventArgs
    {
        public Team teamOfDeck;
        public Team addedByTeam;
        public string cardUid;
    }

    public class OnMinionFrozenEventArgs
    {
        public string cardUid;
    }

    public class OnShakeCharacterEventArgs
    {
        public string characterId;
        public int magnitude;
        public float duration;
    }

    public class OnBounceCharacterEventArgs
    {
        public string characterId;
    }

    public class OnEnteredFableMapEventArgs
    {
        public FablesInput.Mode mode;
        public FableMapData mapData;
        public int currentRoomIndex;
    }

    public class OnHeroHealthChangedEventArgs
    {
        public string heroId;
        public int healthChange;
        public int totalHealth;
    }

    public class OnPlayerQuestsChangedEventArgs
    {
        public ClientChapterData readOnlyChapterData;
    }

    public class OnStartMatchEventArgs
    {

    }

    public class OnGainedArmorEventArgs
    {
        public Team team;
        public int armorGained;
    }

    public class OnAccoladeCompleteArgs
    {
        public string accoladeID;
    }

    public class OnSpawnSaveIconArgs
    {

    }

    // only works for clients right now
    public class OnCardTrackedAndAddedToHandEventArgs
    {
        public string cardUid;
        public Team team;
    }

    public class OnBlitzBurstInstantFillArgs
    {
        public string playerUid;
    }

    public class OnCharacterHealedEventArgs
    {
        public string sourceUid;
        public string healedId;
        public int amount;
    }

    public class OnTrapTriggeredEventArgs
    {
        public string sourceUid;
    }

    public class OnTrapSetEventArgs
    {
        public string sourceUid;
    }

    public class OnOpenCollectionMenuEventArgs
    {
        public DeckEditSettings deckEditSettings;
    }

    public class OnStateChangedEventArgs
    {
        public StateDefinition state;
    }

    public class OnFableInputStateChangedEventArgs
    {
        public FablesInput.Mode inputMode;
        public FablesInput.Mode lastMode;
    }
}