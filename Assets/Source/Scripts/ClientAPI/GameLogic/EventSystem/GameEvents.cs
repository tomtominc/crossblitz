namespace CrossBlitz.ClientAPI.GameLogic.EventSystem
{
    /// <summary>
    /// Visual events, these events will fire when something visually changes to the card
    /// </summary>
    public enum GameEvents
    {
        OnGameInitialized,
        OnMulliganStart,
        OnMulliganCardSetToBeSwapped,
        OnMulliganCardSetToBeKept,
        OnMulliganEnded,
        OnTurnStart,
        OnMinionPlayed,
        OnMinionSummoned,
        OnCharacterAttack,
        OnCharacterFrozen,
        OnCharacterUnFrozen,
        OnCharacterObtainedArmor,
        OnHealed,
        OnCharacterDamaged,
        OnDrawnCard,
        OnSpellCast,
        OnAbilityGiven,
        OnModifyCharacterHealth,
        OnModifyCharacterPower,
        OnModifyCharacterHealthAndPower,
        OnCharacterSilenced,
        OnCardChangedLocation,
        OnTurnEnd,
    }
}