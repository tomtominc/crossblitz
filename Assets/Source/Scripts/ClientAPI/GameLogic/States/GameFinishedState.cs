using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Fables;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.Transition;
using MEC;
using TakoBoyStudios.Events;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ClientAPI.States
{
    public class GameFinishedState : State
    {
        public override void Enter()
        {
            if (FableController.PlayerIsInAFable())
            {
                App.FableData.OnExitBattle();
            }

            Events.Subscribe(EventType.OnContinueFromMatchResults, LoadFable);
        }

        private void LoadFable(IMessage message)
        {
            Timing.RunCoroutine(GoToState(StateDefinition.FABLE_ENTER));
        }

        private IEnumerator<float> GoToState(StateDefinition state)
        {
            //yield return Timing.WaitUntilDone(TransitionController.TransitionIn(TransitionController.TransitionType.Hex));
            yield return Timing.WaitUntilDone(TransitionController.TransitionIn(TransitionController.TransitionType.Fade));

            if (SceneController.IsLoaded("MatchFinished"))
            {
                yield return Timing.WaitUntilDone(SceneController.Instance.UnloadScene("MatchFinished").AsCoroutine());
            }

            StateController.ChangeState(state);
        }

        public override void Exit()
        {
            Events.Unsubscribe(EventType.OnContinueFromMatchResults, LoadFable);
        }
    }
}