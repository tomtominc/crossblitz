using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.Developer;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.States
{
    public class GameStateTurnStart : State
    {
        public override void Enter()
        {
            // this state is only for the client player.
            if (!GameServer.State.IsClientsTurn())
            {
                // go to the opponents turn, something went wrong!
                GameServer.ChangeGameState(StateDefinition.GAME_OPPONENT_START_TURN);
                return;
            }

            GameServer.State.TurnStartTime = DateTime.UtcNow.Ticks;

            Timing.RunCoroutine(StartTurnAnimation());
        }

        private IEnumerator<float> StartTurnAnimation()
        {
            if (!GameServer.State.BattleEventsUpdater.IsPlaying)
            {
                yield return Timing.WaitUntilDone(GameplayScreen.Instance.OpenTurnStartBanner());
            }

            if (Server.CommandResolver.Executing)
            {
                yield return Timing.WaitForOneFrame;
            }

            var turnStartCommand = new TurnStartCommand
            {
                PlayerUid = GameServer.ClientPlayerState.Uid,
                Team = Team.Client
            };

            global::GameLogic.StartCommand(turnStartCommand);

            while (Server.CommandResolver.Executing || Server.WaitingForExecuteGameLogicRpc) yield return Timing.WaitForOneFrame;

            GameServer.ChangeGameState(StateDefinition.GAME_PLAYER_UPDATE_TURN);
        }
    }
}
