using CrossBlitz.ServerAPI.GameLogic;
using UnityEngine;

namespace CrossBlitz.ClientAPI.States
{
    public class GameStateRoShamBo : State
    {
        public override void Enter()
        {
            GameplayScreen.Instance.turnOrderOverlay.SetActive(true);
        }

        public override void Update()
        {
            if (GameplayScreen.Instance.turnOrderOverlay.Finished)
            {
                GameServer.ChangeGameState(StateDefinition.GAME_MULLIGAN);
                Object.Destroy(GameplayScreen.Instance.turnOrderOverlay.gameObject);
            }
        }
    }
}