using CrossBlitz.ClientAPI.GameLogic;

namespace CrossBlitz.ClientAPI.States
{
    public class GameStatePlayerTurnUpdate : State
    {
        public override void Enter()
        {

            GameplayScreen.Instance.PlayerStartedTurn();
        }
    }
}
