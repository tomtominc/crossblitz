using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using MEC;

namespace CrossBlitz.ClientAPI.States
{
    public class GameOpponentUpdateTurn : State
    {
        public override void Enter()
        {
            GameplayScreen.Instance.OpponentTurnStarted();

            if (GameServer.Mode.ConnectionMode == ConnectionMode.LOCAL)
            {
                //var battleData = Db.BattleDatabase.GetBattleData(GameServer.Mode.BattleUid);
                //Timing.RunCoroutine(AI.UpdateTurn(AIStrategy.DoubloonCheat));
            }
        }

        public override void Update()
        {

        }
    }
}
