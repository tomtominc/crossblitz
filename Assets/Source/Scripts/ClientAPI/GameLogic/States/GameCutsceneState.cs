using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.Transition;
using UnityEngine;

namespace CrossBlitz.ClientAPI.States
{
    public class GameCutsceneState : State
    {
        private StateDefinition m_nextState;
        public void SetNextGameState(StateDefinition nextState)
        {
            m_nextState = nextState;
        }
        public override void Enter()
        {

        }

        public override void Update()
        {
            if (GameServer.State.BattleEventsUpdater.IsPlaying)
            {
                return;
            }

            if (m_nextState == StateDefinition.NONE)
            {
                Debug.LogError("Next state is none!!! Set the next state before entering this.");
                return;
            }

            GameServer.ChangeGameState(m_nextState);
            m_nextState = StateDefinition.NONE;
        }
    }
}