using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Audio;
using CrossBlitz.Databases;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using CrossBlitz.Transition;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.States
{
    public class GameStateMatchmaking : State
    {
        private bool _gameReady;

        public override void Enter()
        {
            _gameReady = false;

            GameServer.OnCreatedGameSuccess += GameCreatedSuccess;
        }

        public void StartGame(GameMode gameMode, bool fromReload = false)
        {
            if (GameServer.IsRunning())
            {
                Timing.RunCoroutine(ReloadGame(gameMode));
                return;
            }

            Server.JoinOrCreateMatch(gameMode);
            Timing.RunCoroutine(LoadGame(fromReload));
        }

        private void GameCreatedSuccess()
        {
            GameServer.OnCreatedGameSuccess -= GameCreatedSuccess;

            _gameReady = true;
        }

        private IEnumerator<float> ReloadGame(GameMode gameMode)
        {
            yield return Timing.WaitUntilDone(TransitionController.TransitionIn(TransitionController.TransitionType.Fade));
            //yield return Timing.WaitUntilDone(TransitionController.TransitionIn(TransitionController.TransitionType.Hex));
            yield return Timing.WaitUntilDone(SceneController.Instance.UnloadAllScenes().AsCoroutine());
            GameServer.Kill();
            StartGame(gameMode, true);
        }

        private IEnumerator<float> LoadGame(bool fromReload)
        {
            if (!fromReload)
            {
                yield return Timing.WaitUntilDone(
                    TransitionController.TransitionIn(TransitionController.TransitionType.Cross));
                yield return Timing.WaitUntilDone(SceneController.Instance.UnloadAllScenes().AsCoroutine());
            }

            var battle = Db.BattleDatabase.GetBattleData(GameServer.Mode.BattleUid);

            if (!string.IsNullOrEmpty(battle.Music))
            {
                AudioController.PlaySong(battle.Music);
            }

            while (!_gameReady)
            {
                yield return Timing.WaitForOneFrame;
            }

            StateController.ChangeState(StateDefinition.GAME_INITIALIZE);
        }
    }
}
