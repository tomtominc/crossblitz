using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI.GameLogic;
using UnityEngine;

namespace CrossBlitz.ClientAPI.States
{
    public class GameStateStartMatch : State
    {
        private float m_delay;

        public override void Enter()
        {
            m_delay = 0.24f;

            // do relic animations and wait to start game!
            GameServer.State.InitializeRelics();

            // sets the "custom" mana amount for this battle.
            var battle = Db.BattleDatabase.GetBattleData(GameServer.Mode.BattleUid);

            if (App.Settings.GetUnlimitedMana())
            {
                GameServer.SetMana(Team.Client, 10, false, true);
            }

            if (battle.CustomBattleOptions != null && battle.CustomBattleOptions.Enabled)
            {
                if (battle.CustomBattleOptions.PlayerStartingMana > 1)
                {
                    GameServer.ClientPlayerState.totalMana = battle.CustomBattleOptions.PlayerStartingMana - 1;
                    GameServer.SetMana(Team.Client, GameServer.ClientPlayerState.totalMana, false, true);
                }

                if (battle.CustomBattleOptions.OpponentStartingMana > 1)
                {
                    GameServer.OpponentPlayerState.totalMana = battle.CustomBattleOptions.OpponentStartingMana - 1;
                    GameServer.SetMana(Team.Opponent, GameServer.OpponentPlayerState.totalMana, false, true);
                }
            }
        }

        public override void Update()
        {
            m_delay -= Time.deltaTime;

            if (m_delay > 0)
            {
                return;
            }

            // start the client or opponents first turn.
            if (GameServer.State.IsClientsTurn())
            {
                GameServer.ChangeGameState(StateDefinition.GAME_PLAYER_START_TURN);
            }
            // only start the enemy turn if there's no dialogue happening.
            else //if (!GameServer.State.BattleEventsUpdater.IsPlaying)
            {
                GameServer.ChangeGameState(StateDefinition.GAME_OPPONENT_START_TURN);
            }
        }
    }
}