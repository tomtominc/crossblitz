using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;

namespace CrossBlitz.ClientAPI.States
{
    public class GameCombatPhase : State
    {
        private bool m_endedState;
        public override void Enter()
        {
            m_endedState = false;
            GameplayScreen.Instance.blitzButton.FlipToCombatPhase(GameServer.State.CurrentTeamTurn());
        }

        public override void Update()
        {
            if (Server.CommandResolver.Executing)
            {
                return;
            }

            if (m_endedState)
            {
                return;
            }

            m_endedState = true;

            if (!GameServer.IsGamePendingFinished())
            {
                Timing.RunCoroutine(SwapTurns());
            }
            // else
            // {
            //     GameServer.ChangeGameState(StateDefinition.GAME_FINISHED);
            // }
        }

        private IEnumerator<float> SwapTurns()
        {
            yield return Timing.WaitForSeconds(1);

            GameServer.State.ChangeTurns();
            GameServer.ChangeGameState(GameServer.State.IsClientsTurn() ?
                StateDefinition.GAME_PLAYER_START_TURN :
                StateDefinition.GAME_OPPONENT_START_TURN);
        }

        public override void Exit()
        {

        }
    }
}