using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using MEC;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ClientAPI.States
{
    public class GameStateMulligan : State
    {
        private bool _waitingForServer;
        private bool m_gameStarted;

        public override void Enter()
        {
            m_gameStarted = false;

            if (GameServer.Mode.ConnectionMode == ConnectionMode.ONLINE)
            {
                HandleMulliganOnline();
            }
            else
            {
                HandleMulliganLocal();
            }
        }

        private void HandleMulliganOnline()
        {
            if (GameServer.IsClientGoingFirst())
            {
                _waitingForServer = false;

                var draw = new DrawCardCommand
                {
                    PlayerUid = GameServer.ClientPlayerState.Uid,
                    CardCountBeforeDraw = 0,
                    DrawCount = GameServer.Rules.StartingDrawCount(GameServer.ClientPlayerIndex),
                    Mulligan = true
                };

                global::GameLogic.StartCommand(draw);
            }
            else
            {
                _waitingForServer = true;
            }
        }

        private void HandleMulliganLocal()
        {
            _waitingForServer = false;

            var draw = new DrawCardCommand
            {
                PlayerUid = GameServer.ClientPlayerState.Uid,
                CardCountBeforeDraw = 0,
                DrawCount = GameServer.Rules.StartingDrawCount(GameServer.ClientPlayerIndex),
                Mulligan = true
            };

            global::GameLogic.StartCommand(draw);

            AI.DrawMulliganCards( GameServer.Mode.BattleUid );
        }

        public override void Update()
        {
            if (m_gameStarted)
            {
                return;
            }


            // this only applies if we're online
            if (_waitingForServer && GameServer.OpponentPlayerState.finishedDrawingInitialCards)
            {
                var draw = new DrawCardCommand
                {
                    PlayerUid = GameServer.ClientPlayerState.Uid,
                    CardCountBeforeDraw = 0,
                    DrawCount = GameServer.Rules.StartingDrawCount(GameServer.ClientPlayerIndex),
                    Mulligan = true
                };

                global::GameLogic.StartCommand(draw);
                _waitingForServer = false;
            }

            if (!GameplayScreen.Instance.mulliganView.IsActive())
            {
               Timing.RunCoroutine( StartGame() );
            }
        }

        private IEnumerator<float> StartGame()
        {
            m_gameStarted = true;

            AddCardsToHand();

            if (SummonCustomBoardCards())
            {
                yield return Timing.WaitForSeconds(1);
            }

            while (Server.CommandResolver.Executing)
            {
                yield return Timing.WaitForOneFrame;
            }

            Events.Publish(this, EventType.OnStartMatch, new OnStartMatchEventArgs());

            // if (GameServer.State.BattleEventsUpdater.IsPlaying)
            // {
            //     GameServer.ChangeGameState(StateDefinition.GAME_CUTSCENE);
            //     var cutsceneState = StateController.GetState<GameCutsceneState>(StateDefinition.GAME_CUTSCENE);
            //     cutsceneState.SetNextGameState(StateDefinition.GAME_START_GAME);
            // }
            // else
            {
                GameServer.ChangeGameState(StateDefinition.GAME_START_GAME);
            }

        }
        private void AddCardsToHand()
        {
            if (GameServer.IsRunning() && !string.IsNullOrEmpty(GameServer.Mode.BattleUid))
            {
                var battleData = Db.BattleDatabase.GetBattleData(GameServer.Mode.BattleUid);

                if (battleData.CustomBattleOptions.Enabled && battleData.CustomBattleOptions.PlayerAdditionalCards.Enabled)
                {
                    var addCardCommand = new AddCardToZoneCommand
                    {
                        PlayerUid = GameServer.ClientPlayerState.Uid,
                        SourceUid = GameServer.ClientPlayerState.heroUid,
                        locationToAddTo = CardLocation.Hand,
                        TargetUids = new List<string>()
                    };

                    for (var i = 0; i < battleData.CustomBattleOptions.PlayerAdditionalCards.OverrideDeck.cards.Count; i++)
                    {
                        var card = battleData.CustomBattleOptions.PlayerAdditionalCards.OverrideDeck.cards[i];

                        var createdCards = addCardCommand.CreateCardsFromFilter(new CardFilter
                        {
                            cardId = card.id
                        }, Faction.None, card.count, null,null, null, string.Empty, string.Empty);

                        addCardCommand.TargetUids.AddRange(createdCards);
                    }

                    global::GameLogic.StartCommand(addCardCommand);
                }

                // if (battleData.CustomBattleOptions.Enabled &&
                //     battleData.CustomBattleOptions.OpponentAdditionalCards.Enabled &&
                //     !battleData.CustomBattleOptions.OpponentAdditionalCards.Hidden)
                // {
                //     var addCardCommand = new AddCardToZoneCommand
                //     {
                //         PlayerUid = GameServer.OpponentPlayerState.Uid,
                //         SourceUid = GameServer.OpponentPlayerState.heroUid,
                //         locationToAddTo = CardLocation.Hand,
                //         TargetUids = new List<string>()
                //     };
                //
                //     for (var i = 0;
                //          i < battleData.CustomBattleOptions.OpponentAdditionalCards.OverrideDeck.cards.Count;
                //          i++)
                //     {
                //         var card = battleData.CustomBattleOptions.OpponentAdditionalCards.OverrideDeck.cards[i];
                //
                //         var createdCards = addCardCommand.CreateCardsFromFilter(new CardFilter
                //         {
                //             cardId = card.id
                //         }, Faction.None, card.count);
                //
                //         addCardCommand.TargetUids.AddRange(createdCards);
                //     }
                //
                //     global::GameLogic.StartCommand(addCardCommand);
                // }
            }
        }

        private bool SummonCustomBoardCards()
        {
            var isSummoningCards = false;

            if (GameServer.IsRunning() && !string.IsNullOrEmpty(GameServer.Mode.BattleUid))
            {
                var battleData = Db.BattleDatabase.GetBattleData(GameServer.Mode.BattleUid);

                if (battleData.CustomBattleOptions.Enabled && battleData.CustomBattleOptions?.GameBoardOptions?.Board != null)
                {
                    var playerState = GameServer.GetPlayerState(Team.Client);
                    var opponentState = GameServer.GetPlayerState(Team.Opponent);

                    var playerSummonCommand = new SummonCommand
                    {
                        SummonAllAtOnce = true,
                        PlayerUid = playerState.Uid,
                        SummonMethod = SummonCommand.Method.PlayedWithAbility,
                        TargetUids = new List<string>(),
                        Positions = new List<Vector2Int>()
                    };

                    var opponentSummonCommand = new SummonCommand
                    {
                        SummonAllAtOnce = true,
                        PlayerUid = opponentState.Uid,
                        SummonMethod = SummonCommand.Method.PlayedWithAbility,
                        TargetUids = new List<string>(),
                        Positions = new List<Vector2Int>()
                    };

                    isSummoningCards = battleData.CustomBattleOptions.GameBoardOptions.Board.Count > 0;

                    for (var i = 0; i < battleData.CustomBattleOptions.GameBoardOptions.Board.Count; i++)
                    {
                        var boardSpot = battleData.CustomBattleOptions.GameBoardOptions.Board[i];
                        var index = i>7? i-8 : i;
                        var position = new Vector2Int(index%4, index/4);

                        if (i > 7) position.y += 2;

                        var tile = GameplayScreen.Instance.gameBoard.GetTile(position);

                        if (tile == null)
                        {
                            Debug.LogError($"Couldn't find tile at {position}!");
                            continue;
                        }

                        if (!string.IsNullOrEmpty(boardSpot.CardId))
                        {
                            if (tile.State == null)
                            {
                                Debug.LogError($"Tile[{i}] is null!");
                                continue;
                            }

                            if (tile.State.Occupied)
                            {
                                Debug.LogError($"Tile[{i}] is occupied!");
                                continue;
                            }

                            var player = GameServer.GetPlayerState(i > 7 ? Team.Opponent : Team.Client);
                            var cardData = Db.CardDatabase.GetCard(boardSpot.CardId);

                            if (cardData == null)
                            {
                                cardData = Db.CardDatabase.GetCardByName(boardSpot.CardId);

                                if (cardData == null)
                                {
                                    Debug.LogError($"Card Data for {boardSpot.CardId} is null!");
                                    continue;
                                }
                            }

                            var card = player.CreateCardAtLocation(cardData.id, CardLocation.Board);

                            if (card.data == null)
                            {
                                Debug.LogError($"Card Data for card {boardSpot.CardId} is null.");
                                continue;
                            }

                            // card.AddAbilities(new List<Ability>
                            // {
                            //     new Ability { keyword =  AbilityKeyword.BIDE }
                            // });

                            if (i > 7)
                            {
                                opponentSummonCommand.TargetUids.Add(card.uid);
                                opponentSummonCommand.Positions.Add(tile.Position);
                            }
                            else
                            {
                                playerSummonCommand.TargetUids.Add(card.uid);
                                playerSummonCommand.Positions.Add(tile.Position);
                            }

                            // var summonCommand = new SummonCommand
                            // {
                            //     PlayerUid = player.Uid,
                            //     SummonMethod = SummonCommand.Method.PlayedWithAbility,
                            //     Position = tile.Position,
                            //     SourceUid = card.uid,
                            //     Selection = Vector2Int.zero,
                            // };
                        }
                    }

                    if (playerSummonCommand.TargetUids.Count > 0)
                    {
                        global::GameLogic.StartCommand(playerSummonCommand);
                    }

                    if (opponentSummonCommand.TargetUids.Count > 0)
                    {
                        global::GameLogic.StartCommand(opponentSummonCommand);
                    }
                }
            }

            return isSummoningCards;
        }
    }
}
