using System;
using System.Collections.Generic;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;

namespace CrossBlitz.ClientAPI.States
{
    public class GameOpponentStartTurn : State
    {
        public override void Enter()
        {
            if (GameServer.State.IsClientsTurn())
            {
                GameServer.ChangeGameState(StateDefinition.GAME_PLAYER_START_TURN);
            }
            else
            {
                GameServer.State.TurnStartTime = DateTime.UtcNow.Ticks;
                Timing.RunCoroutine(StartTurnAnimation());
            }
        }

        private IEnumerator<float> StartTurnAnimation()
        {
            if (!GameServer.State.BattleEventsUpdater.IsPlaying)
            {
                yield return Timing.WaitUntilDone(GameplayScreen.Instance.OpenTurnStartBanner());
            }

            if (Server.CommandResolver.Executing)
            {
                yield return Timing.WaitForOneFrame;
            }

            AI.StartTurn();
        }
    }
}
