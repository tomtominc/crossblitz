using System.Collections.Generic;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Fables;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using CrossBlitz.Transition;
using CrossBlitz.ViewAPI;
using CrossBlitz.ViewAPI.Vs;
using MEC;
using UnityEngine.SceneManagement;

namespace CrossBlitz.ClientAPI.States
{
    public class GameStateInitialize : State
    {
        private VsScreen _vsScreen;
        public override void Enter()
        {
            if (FableController.PlayerIsInAFable())
            {
                App.FableData.OnEnterBattle();
            }

            LoadGameScreen();
        }

        private async void LoadGameScreen()
        {
            var gameLoadParams = new SceneLoadParams
            {
                SceneToLoad = "Game",
                LoadSceneMode = LoadSceneMode.Additive,
                Params = null
            };

            var gameplayScene = await SceneController.Instance.LoadScene(gameLoadParams);

            var gameplayScreen = gameplayScene.GetView<GameplayScreen>("GameplayScreen");
            gameplayScreen.Open();

            _vsScreen = gameplayScene.GetView<VsScreen>("VsScreen");

            if (GameServer.Mode.GameType == GameType.COLISEUM)
            {
                _vsScreen.Open();
            }
            else
            {
                ViewController.Close(_vsScreen);
            }

            Timing.RunCoroutine(LoadGame());
        }

        private IEnumerator<float> LoadGame()
        {
            yield return Timing.WaitUntilDone(TransitionController.TransitionOut(TransitionController.TransitionType.Cross));

            if (GameServer.Mode.GameType == GameType.COLISEUM)
            {
                yield return Timing.WaitUntilDone(_vsScreen.PlayOpeningAnimation());
            }

            GameServer.ChangeGameState(StateDefinition.GAME_RO_SHAM_BO);
        }
    }
}
