using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asyncoroutine;
using BlendModes;
using CrossBlitz.AssetManagement;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.GameVfx;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.Hero;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.Utils;
using DG.Tweening;
using MEC;
using Newtonsoft.Json;
using Sirenix.OdinInspector;
using Source.Scripts.Card;
using UnityAsync;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngineInternal;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class GameCardView : MonoBehaviour,
        ICardComponent,
        IBeginDragHandler,
        IDragHandler,
        IEndDragHandler,
        // IPointerEnterHandler,
        // IPointerExitHandler,
        IPointerClickHandler,
        ICursorSelectable
    {
        public enum GameStateType
        {
            None,
            Drag,
            EndDrag,
            Summoning,
            WaitForSelection,
        }

        public static GameCardView Current;
        public static GameCardView Last;

        public const int StandardSortingView = 5;
        public const int BoardSortingOrder = 4;
        public const int HandSortingOrder = 120;
        public const int DragSortingOrder = HandSortingOrder + 10;
        public const float ShadowOffsetBoard = 2;
        public const float ShadowOffsetMin = 4;
        public const float ShadowOffsetMax = 10;

        private const float StandardMoveSpeed = 16;
        private const float OutlineWidth = 2;

        // NOTHING IN THIS SCRIPT SHOULD BE PUBLIC!

        private string _uid;
        private GameStateType _gameState;
        private RectTransform _moveTarget;
        private RectTransform _rectTransform;
        private RectTransform _object;
        private CardView _view;
        private Canvas _canvas;
        private CanvasGroup _canvasGroup;
        private CanvasGroup _objectCanvasGroup;
        private int _previousSortOrder;
        private Tweener _shadowAnimation;
        private Tweener _hitTween;
        private CardEventsComponent _events;
        private bool m_checkForDrawIntoHand;

        private Vector2 _cardLocalOriginPoint;
        private Vector2 _cardShadowLocalOriginPoint;

        private float _moveSpeed = StandardMoveSpeed;
        private bool _playSfx = false;

        public RectTransform Rect => _rectTransform;

        public bool CheckForDrawIntoHand
        {
            get => m_checkForDrawIntoHand;
            set => m_checkForDrawIntoHand = value;
        }

        // events


        public GameCardState State => GameServer.State.GetCard(_uid);
        public CardView View => _view;
        public CardEventsComponent Events => _events;

        public TileState TileState
        {
            get
            {
                if (State == null || State.location != CardLocation.Board)
                {
                    return null;
                }

                return GameServer.State.Board.GetTile(State.position);
            }
        }

        public Vector2 AnchoredPosition
        {
            get => _rectTransform.anchoredPosition;
            set => _rectTransform.anchoredPosition = value;
        }

        [Button("Print State Details")]
        public void PrintStateDetails()
        {
            var settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto
            };

            var json = JsonConvert.SerializeObject(State, settings);

            Debug.Log(json);
        }

        public Vector2Int BoardPosition
        {
            get
            {
                if (State == null)
                    return new Vector2Int(-1,-1);
                if (State.data == null)
                    return new Vector2Int(-1, -1);
                if (View.viewType != CardView.CardViewType.BOARD)
                    return new Vector2Int(-1, -1);

                var tile = GameServer.State.Board.GetTile(_uid);

                if (tile == null)
                    return new Vector2Int(-1, -1);

                return tile.Position;
            }
        }

        public Vector2Int LocalBoardPosition
        {
            get
            {
                if (State == null)
                    return new Vector2Int(-1,-1);
                if (State.data == null)
                    return new Vector2Int(-1, -1);
                if (View.viewType != CardView.CardViewType.BOARD)
                    return new Vector2Int(-1, -1);

                var tile = GameServer.State.Board.GetTile(_uid);

                if (tile == null)
                {
                    return new Vector2Int(-1, -1);
                }

                return tile.LocalPosition;
            }
        }

        public Vector2 WorldPosition
        {
            get => _rectTransform.GetPosition(CoordinateSystem.AsChildOfCanvas);
            set => _rectTransform.SetPosition(value, CoordinateSystem.AsChildOfCanvas);
        }

        public GameStateType GameState
        {
            get => _gameState;
            set => _gameState = value;
        }

        public CanvasGroup CanvasGroup => _canvasGroup;
        public CanvasGroup ObjectCanvasGroup => _objectCanvasGroup;

        // ==========================================================================
        // EVENTS TO HOOK TO - USE FOR ANYTHING
        // ==========================================================================

        public event Action<GameCardView> OnLeftClicked;
        public event Action<GameCardView> OnRightClicked;
        public event Action<GameCardView> OnDragStarted;
        public event Action<GameCardView> OnDragEnded;

        public void Initialize(CardView card)
        {
            _view = GetComponent<CardView>();
            _canvas = GetComponent<Canvas>();
            _canvasGroup = GetComponent<CanvasGroup>();
            _objectCanvasGroup = _view.cardObject.GetComponent<CanvasGroup>();

            _rectTransform = this.RectTransform();
            _object = _view.cardObject;
            _cardLocalOriginPoint = _object.anchoredPosition;
            _cardShadowLocalOriginPoint = _view.frontShadow.rectTransform.anchoredPosition;

            if (GameplayScreen.Instance != null)
            {
                GameplayScreen.Instance.OnScreenStateChanged += OnScreenStateChanged;
            }
        }

        private void OnDestroy()
        {
            GameplayScreen.Instance.OnScreenStateChanged -= OnScreenStateChanged;
        }

        private void OnScreenStateChanged(GameplayScreen.ScreenStateType screenState)
        {
            RefreshUsageGlow();
        }

        public void OnAfterCreation()
        {
            _events = View.GetCardComponent<CardEventsComponent>();
        }

        public void SetupForShowOnly(CardData cardData)
        {
            enabled = false;
            _view.SetCardData(cardData);
        }

        public void SetCardData(GameCardState state)
        {
            _uid = state.uid;
            _view.SetCardData(State.data);

            if (_view.frontShadow)
            {
                switch (_view.viewType)
                {
                    case CardView.CardViewType.STANDARD:
                        _view.frontShadowRect.anchoredPosition = new Vector2(ShadowOffsetMin,-ShadowOffsetMin);
                        break;
                    case CardView.CardViewType.BOARD:
                        _view.frontShadowRect.anchoredPosition = new Vector2(ShadowOffsetBoard,-ShadowOffsetBoard);
                        break;
                    case CardView.CardViewType.ZOOMED:
                        _view.frontShadowRect.anchoredPosition = new Vector2(ShadowOffsetMax,-ShadowOffsetMax);
                        break;
                }
            }

            if (_view.viewType == CardView.CardViewType.BOARD)
            {
                if (State.GetAbilities().Exists( ability => ability.keyword == AbilityKeyword.FLYING))
                {
                    SetSortingOrder(BoardSortingOrder+1);
                }
                else
                {
                    SetSortingOrder(BoardSortingOrder);
                }
            }

            UpdateViewFromState();
        }

        public void SetupForMulligan()
        {
            _view.SetInteractable(true);
            _view.cardOutline.SetOutline("b455a0".ToColor(), OutlineWidth);
        }

        public int GetSortOrder()
        {
            return _canvas.sortingOrder;
        }
        public void SetSortingOrder(int sortingOrder)
        {
            _view.SetSortOrder(sortingOrder);
        }

        /// <summary>
        /// Sets the move target of this card, the card will follow this target until
        /// you set the move target to null
        /// </summary>
        /// <param name="moveTarget">Target to follow, set null if you don't want the card to follow anymore.</param>
        /// <param name="offset"></param>
        public void SetCardMoveTarget(RectTransform moveTarget, float moveSpeed = StandardMoveSpeed, bool playSFX = true)
        {
            _moveTarget = moveTarget;
            _moveSpeed = moveSpeed;
            if(playSFX) AudioController.PlaySound("battle_card_fly", "BARDSFX", true, gameObject);
        }

        public void Disintegrate()
        {

        }

        public IEnumerator<float> TransformBoardCard()
        {
            GameServer.OnCardTransformed(State);
            Events.Transformed();

            yield return Timing.WaitForOneFrame;

            OnSummonedOnBoard(SummonCommand.Method.PlayedWithAbility);

            yield return Timing.WaitForOneFrame;

            var requiredComponents = GameVfxComponent.GetRequiredVfxComponents(State);
            var createdComponents = new List<ICardComponent>();

            for (var i = 0; i < requiredComponents.Count; i++)
            {
                var requiredComponent = gameObject.GetComponent(requiredComponents[i]);

                if (requiredComponent == null)
                {
                    var component = (ICardComponent) gameObject.AddComponent(requiredComponents[i]);
                    component.Initialize(View);
                    component.OnAfterCreation();

                    createdComponents.Add(component);
                }
            }

            while (createdComponents.Exists(c =>c is GameVfxComponent vfx && vfx.Effect == null))
            {
                yield return Timing.WaitForOneFrame;
            }

            yield return Timing.WaitForOneFrame;
        }

        public bool displayRealGameStatsOnZoomedView;

        public void UpdateViewFromState(bool refreshStats = false)
        {
            // if (AI.Thinking)
            // {
            //     return;
            // }

            if (refreshStats)
            {
                State.RefreshStats();
            }

            _view.SetGameState(State);

            if (View.viewType == CardView.CardViewType.BOARD || View.viewType == CardView.CardViewType.STANDARD )
            {
                var currentHealth = State.GetHealth();
                var currentPower = State.GetPower();
                var currentCost = State.GetCost();

                _view.healthValue.text = currentHealth.ToString();
                _view.powerValue.text = currentPower.ToString();

                if (State.GetDatabaseMaxHealth() > currentHealth)
                {
                    _view.healthValue.color = "EC3C3C".ToColor();
                }
                else if (State.GetDatabaseMaxHealth() < currentHealth)
                {
                    _view.healthValue.color = "BBE085".ToColor();
                }
                else
                {
                    _view.healthValue.color = Color.white;
                }

                if (State.GetDatabasePower() > currentPower)
                {
                    _view.powerValue.color = "EC3C3C".ToColor();
                }
                else if (State.GetDatabasePower() < currentPower)
                {
                    _view.powerValue.color = "BBE085".ToColor();
                }
                else
                {
                    _view.powerValue.color = Color.white;
                }

                if (_view.costValue)
                {
                    _view.costValue.text = currentCost.ToString();

                    if (State.GetDatabaseCost() < currentCost)
                    {
                        _view.costValue.color = "EC3C3C".ToColor();
                    }
                    else if (State.GetDatabaseCost() > currentCost)
                    {
                        _view.costValue.color = "BBE085".ToColor();
                    }
                    else
                    {
                        _view.costValue.color = Color.white;
                    }
                }
            }
            else if (View.viewType == CardView.CardViewType.ZOOMED)
            {
                if (displayRealGameStatsOnZoomedView)
                {
                    _view.healthValueSF.text = State.GetPendingHealth().ToString();
                    _view.powerValueSF.text = State.GetPendingPower().ToString();
                    _view.costValueSF.text = State.GetCost().ToString();
                    _view.descriptionLabel.text = _view.FormatDescription(State.data.description, State);
                }
                else
                {
                    _view.costValueSF.text = State.GetDatabaseCost().ToString();
                    _view.powerValueSF.text = State.GetDatabasePower().ToString();
                    _view.healthValueSF.text = State.GetDatabaseMaxHealth().ToString();
                }

            }

            if (View.viewType == CardView.CardViewType.BOARD)
            {
                if (State.location == CardLocation.Board)
                {
                    var isActiveNow = _view.attackBlockedIcon.IsActive();

                    switch (State.data.attackType)
                    {
                        case AttackType.Melee:
                            _view.attackBlockedIcon.SetActive(LocalBoardPosition.y != 0);
                            break;
                        case AttackType.Ranged:
                            _view.attackBlockedIcon.SetActive(LocalBoardPosition.y != 1);
                            break;
                        case AttackType.Arcane:
                            _view.attackBlockedIcon.SetActive(false);
                            break;
                        case AttackType.Immobile:
                            _view.attackBlockedIcon.SetActive(false);
                            break;
                    }

                    if (State.characterData.type == CardType.Trick)
                    {
                        _view.attackBlockedIcon.SetActive(false);
                    }

                    if (!isActiveNow && _view.attackBlockedIcon.IsActive())
                    {
                        _view.attackBlockedIcon.DOKill(true);
                        _view.attackBlockedIcon.DOPunchScale(Vector3.one, 0.5f)
                            .SetDelay(1);
                    }
                }

                _view.UpdateAbilitiesForBoardCardDisplay(State.GetAbilities());
            }

            RefreshUsageGlow();
        }

        public void RefreshUsageGlow()
        {
            if (View.viewType != CardView.CardViewType.STANDARD) return;

            if( CanBePlayed(out _, out _) )
            {
                if (State.ConditionalAbilitiesHaveBeenMet(State.uid))
                {
                    _view.EnableUsageGlow(State.IsCardUsableFromHand(), true);
                }
                else
                {
                    _view.EnableUsageGlow(State.IsCardUsableFromHand());
                }
            }
            else
            {
                _view.EnableUsageGlow(false);
            }
        }

        public async Task<T> GetVfx<T>() where T : GameVfxComponent, ICardEffectComponent
        {
            var effectComponent = GetComponent<T>();

            if (effectComponent == null || effectComponent.Effect==null || (MonoBehaviour)effectComponent.Effect == null)
            {
                if (effectComponent == null)
                {
                    effectComponent = View.AddCardComponent<T>();
                }
                else
                {
                    Destroy(effectComponent);
                    await Await.NextUpdate();
                    effectComponent = View.AddCardComponent<T>();
                }

                effectComponent.Initialize(_view);
                effectComponent.OnAfterCreation();
            }

            while ((MonoBehaviour)effectComponent.Effect==null)
            {
                await Await.NextUpdate();
            }

            return effectComponent;
        }

        [TypeInferenceRule(TypeInferenceRules.TypeOfFirstArgument)]
        public async Task<T> GetVfx<T>(T type) where T : GameVfxComponent, ICardEffectComponent
        {
            var effectComponent = GetComponent<T>();

            if (effectComponent == null || effectComponent.Effect == null)
            {
                effectComponent = View.AddCardComponent<T>();
                effectComponent.Initialize(_view);
                effectComponent.OnAfterCreation();
            }

            while (effectComponent.Effect == null)
            {
                //Debug.Log($"Loading Effect ({typeof(T)}).");
                await Await.NextUpdate();
            }

            return effectComponent;
        }

        public IEnumerator<float> AnimateEffect<T>(Command command) where T : GameVfxComponent, ICardEffectComponent
        {
            var getVfxTask = GetVfx<T>();
            yield return Timing.WaitUntilDone(getVfxTask.AsCoroutine());
            var effectComponent = getVfxTask.Result;

            PlayAbilityDialogue();

            // if (command == null) Debug.Log("Command is null");
            // else
            // {
            //     Debug.Log("Command is not null");
            //     Debug.Log("command.SourceUid" + ": " + command.SourceUid);
            // }

            yield return Timing.WaitUntilDone(effectComponent.Effect.Play(command,new VfxParameters{ dimScreen = true }), CommandResolver.ResolvingTag);
        }

        public IEnumerator<float> Flip(bool toBack = false)
        {
            AudioController.PlaySound("battle_card_flip", "BARDSFX", true, gameObject);
            yield return Timing.WaitUntilDone(_rectTransform.DOScaleX(0, 0.24f)
                .SetEase(Ease.InBack, 3f)
                .WaitForCompletion(true));
            _view.cardBackObject.SetActive(toBack);
            _view.cardObject.SetActive(!toBack);
            yield return Timing.WaitUntilDone(_rectTransform.DOScaleX(1, 0.24f)
                .SetEase(Ease.OutBack, 3f)
                .WaitForCompletion(true));
        }

        public IEnumerator<float> FlickerGlow()
        {
            _view.frontOverlayCanvas.SetActive(true);
            _view.frontOverlayCanvas.alpha = 0;

            if (_view.attackTypeIconOverlayCanvas)
            {
                _view.attackTypeIconOverlayCanvas.SetActive(true);
                _view.attackTypeIconOverlayCanvas.alpha = 0;
            }

            _view.frontOverlayCanvas.DOFade(1, 0.12f);
            if (_view.attackTypeIconOverlayCanvas) _view.attackTypeIconOverlayCanvas.DOFade(1, 0.12f);

            yield return Timing. WaitForSeconds(0.12f);

            _view.frontOverlay.image.color = "f8d3d9".ToColor();
            if (_view.attackTypeIconOverlayCanvas)_view.attackTypeIconOverlay.color = "f8d3d9".ToColor();

            yield return Timing. WaitForSeconds(0.04f);

            _view.RectTransform().
                DOScale(new Vector3(1.2f, 0.8f, 1f), 0.08f);

            _view.frontOverlay.image.color = "e698a4".ToColor();
            if (_view.attackTypeIconOverlayCanvas) _view.attackTypeIconOverlay.color = "e698a4".ToColor();

            yield return Timing.WaitForSeconds(0.04f);

            _view.frontOverlay.image.color = "b455a0".ToColor();
            if (_view.attackTypeIconOverlayCanvas) _view.attackTypeIconOverlay.color = "b455a0".ToColor();
        }

        private GameCardView boardCardCreatedForSummon;
        private IEnumerator<float> CreateBoardCardForSummonToUseLater(GameTile tile)
        {
            var getBoardCardPrefabTask = AddressableReferenceLoader.GetAsset("Card_Board", true);
            yield return Timing.WaitUntilDone(getBoardCardPrefabTask.AsCoroutine());

            var settings = new CreateCardFactorySettings
            {
                CardPrefab = getBoardCardPrefabTask.Result,
                CardState = State,
                Layout = tile.RectTransform(),
                StartsDisabled = true,
                AdditionalComponents = GameVfxComponent.GetRequiredVfxComponents(State)
            };

            var boardCard = CardFactory.Create(settings, null);

            while (boardCard.AllVfxComponentsLoaded() == false)
            {
                yield return Timing.WaitForOneFrame;
            }

            boardCardCreatedForSummon = boardCard.GetCardComponent<GameCardView>();
        }

        public IEnumerator<float> AnimateSummon(GameTile tile, SummonCommand.Method method, int overrideSortOrder = -1)
        {
            boardCardCreatedForSummon = null;
            Timing.RunCoroutine(CreateBoardCardForSummonToUseLater(tile), CommandResolver.ResolvingTag);

            GameState = GameStateType.Summoning;
            SetCardMoveTarget(tile.RectTransform(),16,false);

            if (overrideSortOrder > -1)
            {
                SetSortingOrder(overrideSortOrder);
            }

            while (Vector2.Distance(AnchoredPosition, _moveTarget.position) <= 0.1f)
                yield return Timing.WaitForOneFrame;

            yield return Timing.WaitUntilDone(FlickerGlow(), CommandResolver.ResolvingTag);

            AudioController.PlaySound("battle_minion_spawn", "BARDSFX", true, gameObject);

            yield return Timing. WaitForSeconds(0.04f);

            _view.frontOverlay.image.color = Color.white;
            if (_view.attackTypeIconOverlayCanvas) _view.attackTypeIconOverlay.color = Color.white;

            var useCardEffect = GameEffectsManager.Instance.GetEffect(GameEffectsManager.VFX.UseCard);

            useCardEffect.transform.SetParent(tile.RectTransform(),false);
            useCardEffect.RectTransform().anchoredPosition = Vector2.zero;
            useCardEffect.animations[0].SetActive(true);
            useCardEffect.images[0].SetActive(false);

            if (overrideSortOrder > -1)
            {
                useCardEffect.canvas.sortingOrder = overrideSortOrder;
            }

            var effectAnim = useCardEffect.animations[0];
            effectAnim.Play("use-minion");

            useCardEffect.destroyAfterPlaying = true;

            _view.cardObject.SetActive(false);

            while (effectAnim.CurrentFrame <= 8) yield return Timing.WaitForOneFrame;

            while (boardCardCreatedForSummon == null)
            {
                useCardEffect.images[0].SetActive(true);
                yield return Timing.WaitForOneFrame;
            }

            boardCardCreatedForSummon.View.GetVfx<AttackEffectComponent, AttackEffect>().SetupDirection(State.Team);

            if (overrideSortOrder > -1)
            {
                boardCardCreatedForSummon.SetSortingOrder( overrideSortOrder );
            }

            GameEffectsManager.Instance.RemoveEffect(useCardEffect);

            yield return Timing.WaitUntilDone(boardCardCreatedForSummon.AnimateSummonOnTile(tile, State, SummonMinionData.SummonVisualEffect.FromHand, method), CommandResolver.ResolvingTag);

            GameState = GameStateType.None;
        }

        public IEnumerator<float> AnimateUseSpell()
        {
            GameState = GameStateType.Summoning;

            AudioController.PlaySound("battle_spell_activate", "BARDSFX", true, gameObject);

            if (!State.data.usesTargetingEffect)
            {
                transform.SetParent(GameplayScreen.Instance.gameBoard.RectTransform(), true);

                yield return Timing.WaitUntilDone(FlickerGlow(), CommandResolver.ResolvingTag);
                yield return Timing.WaitForSeconds(0.04f);

                transform.eulerAngles = Vector3.zero;

                _view.frontOverlay.image.color = Color.white;
                if (_view.attackTypeIconOverlayCanvas) _view.attackTypeIconOverlay.color = Color.white;

                var useCardEffect = GameEffectsManager.Instance.GetEffect(GameEffectsManager.VFX.UseCard);

                useCardEffect.transform.SetParent(GameplayScreen.Instance.gameBoard.RectTransform(), false);
                useCardEffect.RectTransform().anchoredPosition = this.RectTransform().anchoredPosition;
                useCardEffect.animations[0].SetActive(true);
                useCardEffect.images[0].SetActive(false);

                var effectAnim = useCardEffect.animations[0];
                effectAnim.Play("use-spell");

                useCardEffect.destroyAfterPlaying = true;

                _view.cardObject.SetActive(false);

                while (effectAnim.CurrentFrame <= 8) yield return Timing.WaitForOneFrame;
            }
            else
            {
                // var spellTargetEffect = _view.GetVfx<SpellTargetComponent, SpellTargetEffect>();
                // spellTargetEffect.ShowEffect(false);
                var heroBattleView = GameplayScreen.Instance.heroSideBarContainer.GetHeroView(State.Team);
                heroBattleView.HideSpellTargetingEffect();

                yield return Timing.WaitForSeconds(0.06f);
            }

            Destroy(gameObject);
        }

        public IEnumerator<float> AnimateDestroyInHand()
        {
            _rectTransform.DOLocalMoveY(_rectTransform.transform.localPosition.y + 32, .5f);
            _view.TintOverTime(Color.white, BlendMode.Color, 0.24f, true);
            yield return Timing.WaitForSeconds(0.24f);
            _view.CanvasGroup.DOFade(0, 0.5f);
            yield return Timing.WaitForSeconds(0.5f);
            GameplayScreen.Instance.handView.RemoveCard(this, null);
            Destroy(gameObject);

        }

        public IEnumerator<float> AnimateSummonOnTile(GameTile tile, GameCardState state, SummonMinionData.SummonVisualEffect summonVisualEffect, SummonCommand.Method method, Action OnMinionFinishedSummon=null)
        {
            GameState = GameStateType.Summoning;

            SetCardData(state);

            //while (tile.HasAnotherCardObject(gameObject)) yield return Timing.WaitForOneFrame;

            tile.SetCard(this);

            _view.cardObjectCanvas.alpha = 0;

            if (State.characterData.type == CardType.Trick)
            {
                var trapCardEffect = GetVfx<TrapCardEffectComponents>();
                yield return Timing.WaitUntilDone(trapCardEffect.AsCoroutine());
                ((TrapCardEffect)trapCardEffect.Result.Effect).Hide();
            }

            switch (summonVisualEffect)
            {
                // case SummonMinionData.SummonVisualEffect.FromHand when  State.characterData.type == CardType.Trick:
                // {
                //     _view.SetActive(true);
                //     _view.cardObjectCanvas.alpha = 1;
                //     // this is handled below no need for any other effects/
                //     yield return Timing.WaitUntilDone(AnimateEffect<GolemBuildEffectComponent>(Command.Empty()));
                //     break;
                // }
                case SummonMinionData.SummonVisualEffect.FromAbility:
                {
                    yield return Timing.WaitUntilDone(AnimateEffect<SummonEffectComponent>(Command.Empty()), CommandResolver.ResolvingTag);
                    break;
                }
                case SummonMinionData.SummonVisualEffect.Blood:
                {
                    yield return Timing.WaitUntilDone(AnimateEffect<BloodSummonEffectComponent>(Command.Empty()), CommandResolver.ResolvingTag);
                    break;
                }
                case SummonMinionData.SummonVisualEffect.Soul:
                {
                    yield return Timing.WaitUntilDone(AnimateEffect<SoulSummonEffectComponent>(Command.Empty()), CommandResolver.ResolvingTag);
                    break;
                }
                case SummonMinionData.SummonVisualEffect.Overgrow:
                {
                    yield return Timing.WaitUntilDone(AnimateEffect<OvergrowSummonEffectComponent>(Command.Empty()), CommandResolver.ResolvingTag);
                    break;
                }
                case SummonMinionData.SummonVisualEffect.Slime:
                {
                    yield return Timing.WaitUntilDone(AnimateEffect<GreenSummonEffectComponent>(Command.Empty()), CommandResolver.ResolvingTag);
                    break;
                }
                case SummonMinionData.SummonVisualEffect.Fire:
                {
                    yield return Timing.WaitUntilDone(AnimateEffect<FireSummonEffectComponent>(Command.Empty()), CommandResolver.ResolvingTag);
                    break;
                }
                case SummonMinionData.SummonVisualEffect.BrigadeTrap:
                {
                    yield return Timing.WaitUntilDone(AnimateEffect<BrigadeTrapEffectComponent>(Command.Empty()), CommandResolver.ResolvingTag);
                    break;
                }
                case SummonMinionData.SummonVisualEffect.FromOutsideScreen:
                {
                    var offsetAmount = 250f;

                    if (tile.Position.y < 2) //client side, move from bottom!
                    {
                        AnchoredPosition = Vector2.down * offsetAmount;
                    }
                    else// opponent side, move from top of screen!
                    {
                        AnchoredPosition = Vector2.up * offsetAmount;
                    }


                    _view.SetActive(true);

                    while (_view.loadingData || _view.isDirty) yield return Timing.WaitForOneFrame;

                    _view.cardObjectCanvas.alpha = 1;

                    yield return Timing.WaitUntilDone(_rectTransform.DOAnchorPosY(0, 0.5f)
                        .SetEase(Ease.OutBack).WaitForCompletion(true));
                    break;
                }
            }

            if (summonVisualEffect != SummonMinionData.SummonVisualEffect.FromOutsideScreen && summonVisualEffect != SummonMinionData.SummonVisualEffect.BrigadeTrap)
            {
                _view.cardObjectCanvas.alpha = 1;
                _view.SetActive(true);
                _view.frontOverlayCanvas.alpha = 1;
                _view.frontOverlay.image.color = "b455a0".ToColor();
                _view.frontOverlayCanvas.SetActive(true);

                if (_view.attackTypeIconOverlayCanvas)
                {
                    _view.attackTypeIconOverlayCanvas.alpha = 1;
                    _view.attackTypeIconOverlay.color = "b455a0".ToColor();
                    _view.attackTypeIconOverlayCanvas.SetActive(true);
                }

                while (_view.loadingData || _view.isDirty) yield return Timing.WaitForOneFrame;

                _view.frontOverlayCanvas.DOFade(0, 0.16f)
                    .OnComplete(() => _view.frontOverlayCanvas.SetActive(false));

                if (_view.attackTypeIconOverlayCanvas)
                {
                    _view.attackTypeIconOverlayCanvas.DOFade(0, 0.16f)
                        .OnComplete(() => _view.attackTypeIconOverlayCanvas.SetActive(false));
                }

                AnchoredPosition = new Vector2(0, 4f);

                yield return Timing.WaitForSeconds(0.04f);

                AnchoredPosition = new Vector2(0, -5f);

                yield return Timing.WaitForSeconds(0.04f);

                AnchoredPosition = new Vector2(4, 2);

                yield return Timing.WaitForSeconds(0.04f);

                AnchoredPosition = new Vector2(-1, -2);

                yield return Timing.WaitForSeconds(0.04f);

                AnchoredPosition = new Vector2(0, 1);

                yield return Timing.WaitForSeconds(0.04f);

                AnchoredPosition = new Vector2(0, 0);
            }

            var hoverCard = GetComponent<HoverCard>();
            hoverCard.Initialize(_view);
            hoverCard.OnAfterCreation();

            PlaySummonDialogue();

            OnMinionFinishedSummon?.Invoke();
            OnSummonedOnBoard(method);

            GameState = GameStateType.None;
        }

        public async void OnSummonedOnBoard(SummonCommand.Method method)
        {
            if (State.location == CardLocation.Board)
            {

                if (State.data.type == CardType.Trick)
                {
                    GameLogic.EventSystem.Events.Publish(this, EventType.OnTrapSet, new OnTrapSetEventArgs
                    {
                        sourceUid = State.uid
                    });
                }
                else
                {
                    GameLogic.EventSystem.Events.Publish(this, EventType.OnMinionSummoned, new OnMinionSummonedEventArgs
                    {
                        isClientCard = State.Team == Team.Client,
                        cardUid = _uid,
                        method = method
                    });
                }

                Events.Summoned();

                await ShowAbilityEffect();
            }
        }

        public async Task ShowAbilityEffect()
        {
            if (State.IsSlow())
            {
                var summoningSicknessEffectComponent = await GetVfx<SummoningSicknessEffectComponent>();
                ((SummoningSicknessEffect)summoningSicknessEffectComponent.Effect).StartWait();
            }

            if (State.characterData.type == CardType.Trick)
            {
                var trapCardEffect = await GetVfx<TrapCardEffectComponents>();
                ((TrapCardEffect)trapCardEffect.Effect).Hide();
            }

            var abilities = State.GetAbilities();

            if (abilities == null || abilities.Count <= 0)
            {
                return;
            }

            for (var i = 0; i < abilities.Count; i++)
            {
                var ability = abilities[i];
                switch (ability.keyword)
                {
                    case AbilityKeyword.TOUGH:
                        var toughEffect = await GetVfx<ToughEffectComponent>();
                        Timing.RunCoroutine(toughEffect.Effect.Play(Command.Empty(), null), CommandResolver.ResolvingTag);
                        break;
                    case AbilityKeyword.BARRIER when State.HasStatusEffect(StatusEffect.Barrier):
                        var barrierEffect = await GetVfx<BarrierEffectComponent>();
                        Timing.RunCoroutine( ((BarrierEffect)barrierEffect.Effect).Spawn(), CommandResolver.ResolvingTag);
                        break;
                    case AbilityKeyword.THORNS:
                        var thornsEffect = await GetVfx<ThornsEffectComponent>();
                        Timing.RunCoroutine(((ThornsEffect)thornsEffect.Effect).Spawn(), CommandResolver.ResolvingTag);
                        break;
                    case AbilityKeyword.FLYING:
                        var flyingEffect = await GetVfx<FlyingEffectComponent>();
                        Timing.RunCoroutine(((FlyingEffect)flyingEffect.Effect).Spawn(), CommandResolver.ResolvingTag);
                        break;
                    case AbilityKeyword.LIFESTEAL:
                        var lifestealEffect = await GetVfx<LifestealEffectComponent>();
                        Timing.RunCoroutine(((LifestealEffect)lifestealEffect.Effect).Spawn(), CommandResolver.ResolvingTag);
                        break;
                    case AbilityKeyword.DUAL_STRIKE:
                        var dualStrikeEffect = await GetVfx<DualstrikeEffectComponent>();
                        Timing.RunCoroutine(((DualstrikeEffect)dualStrikeEffect.Effect).Spawn(), CommandResolver.ResolvingTag);
                        break;
                    case AbilityKeyword.GUARD:
                        var guardEffect = await GetVfx<GuardEffectComponent>();
                        Timing.RunCoroutine(((GuardEffect)guardEffect.Effect).Spawn(), CommandResolver.ResolvingTag);
                        break;
                    case AbilityKeyword.FLEETING:
                        var fleetingEffect = await GetVfx<FleetingEffectComponent>();
                        Timing.RunCoroutine(((FleetingEffect)fleetingEffect.Effect).Spawn(), CommandResolver.ResolvingTag);
                        break;
                }

                if (ability.IsRiff())
                {
                    var riffEffect = await GetVfx<RiffEffectComponent>();
                    ((RiffEffect)riffEffect.Effect).Spawn();
                }
            }
        }

        public IEnumerator<float> TransformToSmallCardView()
        {
            _view.frontOverlayCanvas.SetActive(true);
            _view.frontOverlayCanvas.alpha = 0;

            if (_view.attackTypeIconOverlayCanvas)
            {
                _view.attackTypeIconOverlayCanvas.SetActive(true);
                _view.attackTypeIconOverlayCanvas.alpha = 0;
            }

            _view.frontOverlayCanvas.DOFade(1, 0.12f);

            if (_view.attackTypeIconOverlayCanvas)
            {
                _view.attackTypeIconOverlayCanvas.DOFade(1, 0.12f);
            }

            yield return Timing. WaitForSeconds(0.12f);

            _view.frontOverlay.image.color = "f8d3d9".ToColor();
            if (_view.attackTypeIconOverlayCanvas)
            {
                _view.attackTypeIconOverlay.color = "f8d3d9".ToColor();
            }

            yield return Timing. WaitForSeconds(0.04f);

            _view.RectTransform().
                DOScale(new Vector3(1.2f, 0.8f, 1f), 0.08f);

            _view.frontOverlay.image.color = "e698a4".ToColor();
            if (_view.attackTypeIconOverlayCanvas) _view.attackTypeIconOverlay.color = "e698a4".ToColor();

            yield return Timing. WaitForSeconds(0.04f);

            _view.frontOverlay.image.color = "b455a0".ToColor();
            if (_view.attackTypeIconOverlayCanvas) _view.attackTypeIconOverlay.color = "b455a0".ToColor();

            yield return Timing. WaitForSeconds(0.04f);

            _view.RectTransform().
                DOScale(new Vector3(88f / 168f, 100f / 202f), 0.16f)
                .OnComplete(() => { _canvasGroup.DOFade(0f, 0.12f); });
        }

        public IEnumerator<float> FadeInFromTransformation()
        {
            _view.frontOverlayCanvas.DOFade(0, 0.24f)
                .OnComplete(()=> _view.frontOverlayCanvas.SetActive(false));
            if (_view.attackTypeIconOverlayCanvas)
            {
                _view.attackTypeIconOverlayCanvas.DOFade(0, 0.24f)
                    .OnComplete(() => _view.attackTypeIconOverlayCanvas.SetActive(false));
            }

            yield return Timing. WaitForSeconds(0.04f);

            _view.frontOverlay.image.color = "e698a4".ToColor();
            if (_view.attackTypeIconOverlayCanvas)_view.attackTypeIconOverlay.color = "e698a4".ToColor();

            yield return Timing. WaitForSeconds(0.04f);

            _view.frontOverlay.image.color = "f8d3d9".ToColor();
            if (_view.attackTypeIconOverlayCanvas)_view.attackTypeIconOverlay.color = "f8d3d9".ToColor();

            yield return Timing. WaitForSeconds(0.04f);

            _view.frontOverlay.image.color = Color.white;
            if (_view.attackTypeIconOverlayCanvas)_view.attackTypeIconOverlay.color = Color.white;
        }

        public IEnumerator<float> TransformToStandardCardViewFromBoardCard(CardView standardCardView)
        {
            View.Fade(0, 0.24f);
            standardCardView.Fade(1, 0.24f);

            yield return Timing.WaitForSeconds(0.24f);
        }

        public IEnumerator<float> TransformFromStandardToZoomedCardView()
        {
            var reference = AddressableReferenceLoader.GetAsset("Card_Zoomed", true);

            yield return Timing.WaitUntilDone(reference.AsCoroutine());

            var standardCardView = _view;
            var settings = new CreateCardFactorySettings
            {
                CardPrefab = reference.Result,
                Layout = standardCardView.RectTransform(),
                AdditionalComponents = new List<Type> {typeof(GameCardView), typeof(CardTransformComponent)}
            };

            var cardState = State;

            settings.CardState = cardState;
            settings.SortingOrder = HandSortingOrder + 20;

            var zoomedCardView = CardFactory.Create(settings, null);
            zoomedCardView.SetInteractable(false);
            zoomedCardView.RectTransform().anchoredPosition = Vector2.zero;

            if (standardCardView)
            {
                zoomedCardView.transform.localScale = new Vector3(88f / 168f, 100f / 202f, 1);
                zoomedCardView.transform.DOScale(Vector3.one, 0.24f).SetEase(Ease.InOutBack);
                zoomedCardView.CanvasGroup.alpha = 0;
                zoomedCardView.CanvasGroup.DOFade(1, 0.24f);

                standardCardView.SetSortingLayer(zoomedCardView.GetSortingLayer());
                standardCardView.SetSortOrder(zoomedCardView.GetSortOrder() - 1);
                standardCardView.cardObject.DOScale(new Vector3(168f / 88f, 202f / 100f, 1), 0.24f)
                    .SetEase(Ease.InOutBack);

                yield return Timing.WaitForSeconds(0.24f);

                standardCardView.cardObject.SetActive(false);

                yield return Timing.WaitForSeconds(OpponentPlayedCardPreview.PlayedCardFadeAfterSeconds-0.24f);

                zoomedCardView.CanvasGroup.DOFade(0, 0.24f);
            }
        }


        public void HitFromDirection(Vector2 direction, float power = 1, float duration = 0.125f)
        {
            TweenUtility.Kill(_hitTween,true);

            //Debug.Log($"hit direction = {direction} power = {power}");
            _hitTween = _object.DOPunchAnchorPos(direction * power, duration);
        }

        public void PlaySummonDialogue()
        {
            if (StateController.CurrentState == StateDefinition.GAME_CUTSCENE)
            {
                return;
            }

            if (GameServer.State.BattleEventsUpdater.IsPlaying)
            {
                return;
            }

            if (_view.cardDialogue != null)
            {
                DialogueSnippetData snippet = null;
                var snippets = State.characterData.dialogueSnippets.FindAll(d => d.Option == "Minion/Played");

                if (snippets.Count > 0)
                {
                    snippet = snippets[UnityEngine.Random.Range(0, snippets.Count)];
                }

                _view.cardDialogue.SetDialogue(snippet);
            }
        }

        public void PlayAbilityDialogue()
        {
            if (StateController.CurrentState == StateDefinition.GAME_CUTSCENE)
            {
                return;
            }

            if (GameServer.State.BattleEventsUpdater.IsPlaying)
            {
                return;
            }

            if (_view.cardDialogue != null)
            {
                DialogueSnippetData snippet = null;
                var snippets = State.characterData.dialogueSnippets.FindAll(d => d.Option == "Minion/Ability Activated");

                if (snippets.Count > 0)
                {
                    snippet = snippets[UnityEngine.Random.Range(0, snippets.Count)];
                }

                _view.cardDialogue.SetDialogue(snippet);
            }
        }

        private void Update()
        {
            if (_moveTarget && _rectTransform &&
                (_gameState == GameStateType.None ||
                 _gameState == GameStateType.EndDrag ||
                 _gameState == GameStateType.Summoning ||
                 _gameState == GameStateType.WaitForSelection))
            {
                _rectTransform.position = Vector2.MoveTowards(_rectTransform.position,
                    _moveTarget.position,_moveSpeed * Time.deltaTime);

                //if (_playSfx)
                //{
                //    Debug.Log("PLAY FLY SFX");
                //    AudioController.PlaySound("battle_card_fly", "BARDSFX");
                //    _playSfx = false;
                //}

                if (_gameState == GameStateType.EndDrag)
                {
                    if (Vector2.Distance(_rectTransform.position, _moveTarget.position) <= 0.1f)
                    {
                        _gameState = GameStateType.None;
                    }
                }
            }

            // needed this to make sure cards didn't warp to the hand because Rob added a tween effect to it when hovered.
            if (CheckForDrawIntoHand)
            {
                if (Vector2.Distance(Rect.anchoredPosition, Vector2.zero) <= 1)
                {
                    m_checkForDrawIntoHand = false;
                    _view.SetInteractable(true);
                    _view.GetCardComponent<HoverCard>().UsesHoverPunchEffect = true;
                    AudioController.PlaySound("battle_card_hand", "BARDSFX", true, gameObject);
                }
            }
        }

        private void ButtonClicked()
        {
            OnLeftClicked?.Invoke(this);
        }

        public void SetGameState(GameStateType gameState)
        {
            _gameState = gameState;
        }

        public enum CardDragError
        {
            None=-1,
            Generic=0,
            Not_Your_Turn=1,
            Not_Enough_Mana=2,
            Spell_Abilities_Not_Setup=3,
            No_Spell_Targets=4,
            Game_Is_Finished=5,
            Not_In_Hand_Zone=6,
            You_Dont_Own_This_Card=7,
            Already_A_Trap_With_Name_On_Board=8
        }

        public bool CanBePlayed(out CardDragError errorCode, out string errorDisplay)
        {
            errorDisplay = string.Empty;

            if (Current && Current != this)
            {
                errorCode = CardDragError.Generic;
                errorDisplay = "Already a current card.";

                return false;
            }

            if (!GameServer.AbleToPlayCards())
            {
                errorCode = CardDragError.Generic;
                errorDisplay = "I can't do that right now.";
                return false;
            }

            if (View.viewType != CardView.CardViewType.STANDARD)
            {
                errorCode = CardDragError.Generic;
                errorDisplay = "This card cannot be dragged.";
                return false;
            }
            if (State.Team == Team.Opponent)
            {
                errorCode = CardDragError.You_Dont_Own_This_Card;
                errorDisplay = "This card is your opponent's card.";
                return false;
            }

            if (_gameState != GameStateType.None)
            {
                errorCode = CardDragError.Generic;
                errorDisplay = "Could not find a game state on this card.";
                return false;
            }

            if (!GameServer.State.IsClientsTurn())
            {
                errorCode = CardDragError.Not_Your_Turn;
                return false;
            }

            if (GameServer.IsGamePendingFinished())
            {
                errorCode = CardDragError.Game_Is_Finished;
                errorDisplay = "The game is finished! Wait until next round.";
                return false;
            }

            if (State.location != CardLocation.Hand)
            {
                errorCode = CardDragError.Not_In_Hand_Zone;
                errorDisplay = $"This card cannot be played, its not in your hand! ({State.location}).";
                return false;
            }

            if (!ServerAPI.GameLogic.Data.GameState.CanPayCostOfCard(State))
            {
                errorCode = CardDragError.Not_Enough_Mana;
                return false;
            }

            if (State.data.type== CardType.Spell)
            {
                if (State.GetAbilities().Count <= 0)
                {
                    errorCode = CardDragError.Spell_Abilities_Not_Setup;
                    return false;
                }

                if (State.GetAbilities()[0].targetConditions != FilterCondition.NONE && State.GetAbilities()[0].targetConditions.HasFlag( FilterCondition.PLAYER_SELECTED ))
                {
                    if (global::GameLogic.GetTargets(State, State.GetAbilities()[0],null,null, false).Count <= 0)
                    {
                        errorCode = CardDragError.No_Spell_Targets;
                        return false;
                    }
                }
            }

            if (State.data.type == CardType.Trick)
            {
                var occupiedTiles = GameServer.State.Board.GetOccupiedTiles(State.Team);

                for (var i = 0; i < occupiedTiles.Count; i++)
                {
                    var tile = occupiedTiles[i];

                    if (tile.Card.data.id == State.data.id)
                    {
                        errorCode = CardDragError.Already_A_Trap_With_Name_On_Board;
                        errorDisplay = $"You can't set the same Trap.";
                        return false;
                    }
                }
            }

            errorCode = CardDragError.None;
            return true;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            var playable = CanBePlayed(out CardDragError errorCode, out var errorDisplay);

            if (!playable)
            {
                View.cardObject.DOKill();
                View.cardObject.DOPunchAnchorPos(Vector2.up * 24, 0.3f, 4, 0.5f);
                AudioController.PlaySound("battle_battle_card_illegal", "BARDSFX", true, gameObject);
            }

            var hero = GameServer.State.GetCard(GameServer.GetLocalPlayerState().heroUid);

            if (errorCode == CardDragError.Generic)
            {
                var heroView = HeroView.GetHeroView(Team.Client);
                var snippet = hero.characterData.GetRandomDialogueSnippet("Battle Error/Card Not Draggable/Generic");
                Debug.LogError($"Snippet?? {snippet==null }");
                if (snippet!=null)
                {
                    heroView.PlayDialogue(snippet.Dialogue);
                    if (!string.IsNullOrEmpty(snippet.SoundFx)) AudioController.PlaySound(snippet.SoundFx);
                }
                else
                {
                    heroView.PlayDialogue(errorDisplay);
                }
                return;
            }

            if (errorCode == CardDragError.You_Dont_Own_This_Card)
            {
                var heroView = HeroView.GetHeroView(Team.Client);
                var snippet = hero.characterData.GetRandomDialogueSnippet("Battle Error/Card Not Draggable/Opponent's Card");
                if (snippet!=null)
                {
                    heroView.PlayDialogue(snippet.Dialogue);
                    if (!string.IsNullOrEmpty(snippet.SoundFx)) AudioController.PlaySound(snippet.SoundFx);
                }
                else
                {
                    heroView.PlayDialogue("This isn't my card!");
                }

                return;
            }

            if (errorCode == CardDragError.Not_Your_Turn)
            {
                var heroView = HeroView.GetHeroView(Team.Client);
                var snippet = hero.characterData.GetRandomDialogueSnippet("Battle Error/Card Not Draggable/Not Your Turn");
                if (snippet!=null)
                {
                    heroView.PlayDialogue(snippet.Dialogue);
                    if (!string.IsNullOrEmpty(snippet.SoundFx)) AudioController.PlaySound(snippet.SoundFx);
                }
                else
                {
                    heroView.PlayDialogue("I can't do that, it's not my turn!");
                }

                return;
            }

            if (errorCode == CardDragError.Not_Enough_Mana)
            {
                var heroView = HeroView.GetHeroView(Team.Client);
                var snippet = hero.characterData.GetRandomDialogueSnippet("Battle Error/Card Not Draggable/Not Enough Mana");
                if (snippet!=null)
                {
                    heroView.PlayDialogue(snippet.Dialogue);
                    if (!string.IsNullOrEmpty(snippet.SoundFx)) AudioController.PlaySound(snippet.SoundFx);//MasterAudio.PlaySound(snippet.SoundFx);
                }
                else
                {
                    heroView.PlayDialogue("I don't have enough mana!" );
                }

                return;
            }

            if (errorCode == CardDragError.Spell_Abilities_Not_Setup)
            {
                var heroView = HeroView.GetHeroView(Team.Client);
                var snippet = hero.characterData.GetRandomDialogueSnippet("Battle Error/Card Not Draggable/Spell Abilities Not Configured");
                if (snippet!=null)
                {
                    heroView.PlayDialogue(snippet.Dialogue);
                    if (!string.IsNullOrEmpty(snippet.SoundFx)) AudioController.PlaySound(snippet.SoundFx);//MasterAudio.PlaySound(snippet.SoundFx);
                }
                else
                {
                    heroView.PlayDialogue("This spell has not been configured!" );
                }

                return;
            }

            if (errorCode == CardDragError.No_Spell_Targets)
            {
                var heroView = HeroView.GetHeroView(Team.Client);
                var snippet = hero.characterData.GetRandomDialogueSnippet("Battle Error/Card Not Draggable/No Valid Spell Targets");
                if (snippet!=null)
                {
                    heroView.PlayDialogue(snippet.Dialogue);
                    if (!string.IsNullOrEmpty(snippet.SoundFx)) AudioController.PlaySound(snippet.SoundFx); //MasterAudio.PlaySound(snippet.SoundFx);
                }
                else
                {
                    heroView.PlayDialogue("I don't see any valid targets!" );
                }

                return;
            }

            if (errorCode == CardDragError.Game_Is_Finished)
            {
                var heroView = HeroView.GetHeroView(Team.Client);
                var snippet = hero.characterData.GetRandomDialogueSnippet("Battle Error/Card Not Draggable/Game Is Finished");
                if (snippet!=null)
                {
                    heroView.PlayDialogue(snippet.Dialogue);
                    if (!string.IsNullOrEmpty(snippet.SoundFx)) AudioController.PlaySound(snippet.SoundFx); //MasterAudio.PlaySound(snippet.SoundFx);
                }
                else
                {
                    heroView.PlayDialogue("Let's get 'em next game!" );
                }

                return;
            }

            if (errorCode == CardDragError.Not_In_Hand_Zone)
            {
                var heroView = HeroView.GetHeroView(Team.Client);
                var snippet = hero.characterData.GetRandomDialogueSnippet("Battle Error/Card Not Draggable/Unknown Error");
                if (snippet!=null)
                {
                    heroView.PlayDialogue(snippet.Dialogue);
                    if (!string.IsNullOrEmpty(snippet.SoundFx)) AudioController.PlaySound(snippet.SoundFx); //MasterAudio.PlaySound(snippet.SoundFx);
                }
                else
                {
                    heroView.PlayDialogue(errorDisplay);
                }
                return;
            }

            if (errorCode == CardDragError.Already_A_Trap_With_Name_On_Board)
            {
                var heroView = HeroView.GetHeroView(Team.Client);
                var snippet = hero.characterData.GetRandomDialogueSnippet("Battle Error/Card Not Draggable/Trap Already Set");
                if (snippet!=null)
                {
                    heroView.PlayDialogue(snippet.Dialogue);
                    if (!string.IsNullOrEmpty(snippet.SoundFx)) AudioController.PlaySound(snippet.SoundFx); //MasterAudio.PlaySound(snippet.SoundFx);
                }
                else
                {
                    heroView.PlayDialogue(errorDisplay);
                }
                return;
            }

            if (!playable)
            {
                Debug.LogError($"Not playable? {errorCode}");
                return;
            }

            if (State.data.type == CardType.Spell)
            {
                if (State.data.usesTargetingEffect || State.HasTargetCondition(FilterCondition.PLAYER_SELECTED))
                {
                    var cardViewCanvas = _view.cardObject.GetComponent<CanvasGroup>();
                    DOTween.Kill(cardViewCanvas);
                    cardViewCanvas.DOFade(0, 0.12f);

                    // var spellTargetEffect = _view.GetVfx<SpellTargetComponent, SpellTargetEffect>();
                    // spellTargetEffect.ShowEffect(true);
                    var heroBattleView = GameplayScreen.Instance.heroSideBarContainer.GetHeroView(State.Team);
                    Timing.RunCoroutine(heroBattleView.ShowSpellTargetingEffect(), CommandResolver.ResolvingTag);
                }

                if (State.data.usesPreViz)
                {
                    GameVfxProvider.ShowVfxForPreViz(State,null);
                }

                AudioController.PlaySound("battle_card_cast_spell", "BARDSFX", true, gameObject);
            }
            else
            {
                AudioController.PlaySound("battle_card_cast_minion", "BARDSFX", true, gameObject);
            }

            Current = this;

            TweenUtility.Kill(_shadowAnimation, false);

            _gameState = GameStateType.Drag;
            _view.GetCardComponent<DragRotator>().enabled = true;
            _view.GetCardComponent<DragRotator>().followMouse = true;
            _view.GetCardComponent<DragRotator>().SetOffsetEuler(-transform.parent.eulerAngles);
            _canvasGroup.blocksRaycasts = false;

            _previousSortOrder = GetSortOrder();
            _shadowAnimation = _view.frontShadowRect.DOAnchorPos(new Vector2(ShadowOffsetMax, -ShadowOffsetMax), 0.12f);

            GameplayScreen.Instance.ShowTargets(State);

            OnDragStarted?.Invoke(this);
        }

        // private TargetingEffect m_spellTargetingEffect;
        // private IEnumerator<float> CreateSpellTargetEffect()
        // {
        //     var heroBattleView = GameplayScreen.Instance.heroSideBarContainer.GetHeroView(State.Team);
        //
        //     if (m_spellTargetingEffect == null)
        //     {
        //         var getTargetingEffect = GameVfxProvider.GetWorldVfx("TargetingEffect");
        //         yield return Timing.WaitUntilDone(getTargetingEffect.AsCoroutine());
        //         m_spellTargetingEffect = (TargetingEffect)getTargetingEffect.Result;
        //     }
        //
        //     m_spellTargetingEffect.Show(heroBattleView.boardTarget.RectTransform(), false);
        // }

        public void OnDrag(PointerEventData eventData)
        {
            if (Current == this)
            {
                SetSortingOrder( DragSortingOrder);
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (View.viewType != CardView.CardViewType.STANDARD)
            {
                return;
            }

            if (GameState == GameStateType.WaitForSelection)
            {
                _view.GetCardComponent<DragRotator>().followMouse = false;
                Last = Current;
                Current = null;
                return;
            }

            if (GameplayScreen.Instance.ScreenState == GameplayScreen.ScreenStateType.HandlingSelection)
            {
                return;
            }

            OnCanceledUsageDrag();
        }

        public void OnCanceledUsageDrag()
        {
            TweenUtility.Kill(_shadowAnimation, false);

            Last = Current;
            Current = null;
            _view.GetCardComponent<DragRotator>().followMouse = false;
            _view.GetCardComponent<DragRotator>().SetOffsetEuler(Vector3.zero);
            _canvasGroup.blocksRaycasts = true;

            GameVfxProvider.HideAllPreVizEffects();

            _shadowAnimation = _view.frontShadowRect.DOAnchorPos(
                new Vector2(ShadowOffsetMin, -ShadowOffsetMin),
                0.12f);

            if (_gameState == GameStateType.Drag)
            {
                _gameState = GameStateType.None;

                if (State.data.type == CardType.Spell)
                {
                    var cardViewCanvas = _view.cardObject.GetComponent<CanvasGroup>();
                    DOTween.Kill(cardViewCanvas);
                    cardViewCanvas.DOFade(1, 0.12f);
                    // var spellTargetEffect = _view.GetVfx<SpellTargetComponent, SpellTargetEffect>();
                    // spellTargetEffect.ShowEffect(false);

                    var heroBattleView = GameplayScreen.Instance.heroSideBarContainer.GetHeroView(State.Team);
                    heroBattleView.HideSpellTargetingEffect();
                }
            }

            GameplayScreen.Instance.HideTargets();

            OnDragEnded?.Invoke(this);
        }

        // public void OnPointerEnter(PointerEventData eventData)
        // {
        //     //CursorController.SetCursorState(CursorController.CursorState.Magnify);
        // }
        //
        // public void OnPointerExit(PointerEventData eventData)
        // {
        //     //CursorController.SetCursorState(CursorController.CursorState.Idle);
        // }

        public void OnPointerClick(PointerEventData eventData)
        {
            GameLogic.EventSystem.Events.Publish(this, EventType.OnCardClicked, new OnCardClickedEventArgs
            {
                cardUid = _uid,
                button = eventData.button
            });

            switch (eventData.button)
            {
                case PointerEventData.InputButton.Left:
                    OnLeftClicked?.Invoke(this);
                    break;
                case PointerEventData.InputButton.Right:
                    OnRightClicked?.Invoke(this);
                    break;
            }
        }

        public bool Interactable => false;
        public bool Grabbable => CanBePlayed(out var errorCode, out var errorDisplay);
        public bool InfoOnly => !CanBePlayed(out var errorCode, out var errorDisplay);
        public bool Loading => false;
    }
}
