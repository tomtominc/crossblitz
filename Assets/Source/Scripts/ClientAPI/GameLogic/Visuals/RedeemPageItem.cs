using System;
using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.Shop;
using CrossBlitz.UI.Widgets;
using CrossBlitz.Utils;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class RedeemPageItem : PageItem
    {
        public CanvasGroup canvas;
        public SpriteAnimation itemFrame;
        // public GameObject itemCostContainer;
        // public TextMeshProUGUI itemCost;
        // public GameObject itemAmountContainer;
        // public TextMeshProUGUI itemAmount;

        public ToggleButton itemToggle;
        public GameObject cardPrefab;
        public RectTransform cardContainer;
        public FlashWhiteEffect outlineFlash;

        [BoxGroup("Spawn Animation")] public float itemSpawnDelay = 0.06f;
        [BoxGroup("Spawn Animation")] public Vector3 startingScale = new Vector3(1.5f, 1.5f, 1.5f);
        [BoxGroup("Spawn Animation")] public float startingHeight = 20;
        [BoxGroup("Spawn Animation")] public float scaleToDuration = 0.4f;
        [BoxGroup("Spawn Animation")] public Ease scaleEase=Ease.Linear;
        [BoxGroup("Spawn Animation")] public Ease moveToEase=Ease.Linear;
        [BoxGroup("Rotation")] public Vector3 rotationAxis=new Vector3(0,0,1);
        [BoxGroup("Rotation")] public float rotationPower= 20;
        [BoxGroup("Rotation")] public float rotationDuration=0.24f;
        [BoxGroup("Rotation")] public int rotationVibration=20;
        [BoxGroup("Rotation")] public Ease rotationEase=Ease.Linear;

        [BoxGroup("Color Overlay")] public Color color;
        [BoxGroup("Color Overlay")] public BlendMode blend;

        private CardView m_card;
        private RedeemPageItemData m_itemData;
        public event Action<bool, RedeemPageItem> OnClicked;

        public RedeemPageItemData Data => m_itemData;

        private void Start()
        {
            itemToggle.OnValueChanged += Clicked;
        }

        private void OnClickCard(ClickableCard cardView)
        {
            itemToggle.Toggle.isOn = !itemToggle.Toggle.isOn;
        }

        private void Clicked(bool isOn, string content)
        {
            if (isOn)
            {
                itemFrame.RectTransform().DOKill();
                itemFrame.RectTransform().DOPunchRotation(rotationAxis * ((Random.value > 0.5f ? -1 : 1) * rotationPower), rotationDuration, rotationVibration).SetEase(rotationEase);
                itemFrame.RectTransform().DOPunchScale(Vector3.one * 0.16f, 0.24f);

                //m_card.shadow.OffsetDistance = 4;
                m_card.RectTransform().DOKill();
                m_card.RectTransform().DOAnchorPos(Vector2.zero, 0.3f).SetEase(Ease.OutBack);
                m_card.TintThenUntintOverTime(color, blend, 0.06f, 0.06f, 0.06f);

                outlineFlash.Flash();
                outlineFlash.RectTransform().DOKill();
                outlineFlash.RectTransform().localScale = new Vector3(0.8f, 0.8f, 1);
                outlineFlash.RectTransform().DOScale(1, 0.4f).SetEase(Ease.OutElastic);
            }
            else
            {
                //m_card.shadow.OffsetDistance = 0;
                m_card.RectTransform().DOKill();
                m_card.RectTransform().DOAnchorPos(new Vector2(-2, -2), 0.3f).SetEase(Ease.OutBounce);
            }

            OnClicked?.Invoke(isOn, this);
        }

        public override void SetAsEmpty(int itemIndex)
        {
            canvas.alpha = 0;
            itemFrame.Play("sold-out");
            //itemAmountContainer.SetActive(false);
            itemToggle.Toggle.isOn = false;
            itemToggle.Toggle.interactable = false;
            //itemCostContainer.SetActive(false);

            if (m_card)
            {
                m_card.SetActive(false);
                m_card.RectTransform().DOKill();
            }

            var seq = DOTween.Sequence();

            itemFrame.RectTransform().DOKill();
            itemFrame.RectTransform().localScale = startingScale;
            itemFrame.RectTransform().anchoredPosition = new Vector2(0,startingHeight);


            seq.AppendInterval(itemSpawnDelay * itemIndex);
            seq.AppendCallback(() => canvas.alpha = 1);
            seq.Append(itemFrame.RectTransform().DOScale(1, scaleToDuration).SetEase(scaleEase));
            seq.Join(itemFrame.RectTransform().DOAnchorPos(Vector2.zero, scaleToDuration).SetEase(moveToEase));
            seq.Append(itemFrame.RectTransform().DOPunchRotation(rotationAxis * ((Random.value > 0.5f ? -1 : 1) * rotationPower), rotationDuration, rotationVibration).SetEase(rotationEase));
        }

        public override void SetData(IPageItemData data, int itemIndex)
        {
            m_itemData = (RedeemPageItemData) data;

            canvas.alpha = 0;

            var cardData = Db.CardDatabase.GetCard(m_itemData.ItemId);

            if (m_card == null)
            {
                m_card = CardFactory.Create(new CreateCardFactorySettings
                {
                    Layout = cardContainer,
                    CardData = cardData,
                    AdditionalComponents = new List<Type> {typeof(ClickableCard)},
                    CardPrefab = cardPrefab,
                    RemoveCanvas = true,
                    StartsDisabled = true,
                    CardObjectOffsetPosition = Vector2.zero
                }, null);

                var hoverCard = m_card.GetCardComponent<ClickableCard>();
                hoverCard.OnRightClick += OnClickCard;
                hoverCard.OnLeftClick += OnClickCard;
            }
            else
            {
                m_card.SetCardData(cardData);
            }

            var seq = DOTween.Sequence();

            itemFrame.RectTransform().DOKill();
            itemFrame.RectTransform().localScale = startingScale;
            itemFrame.RectTransform().anchoredPosition = new Vector2(0,startingHeight);
            m_card.RectTransform().DOKill();

            seq.AppendInterval(itemSpawnDelay * itemIndex);
            seq.AppendCallback(() => canvas.alpha = 1);
            seq.Append(itemFrame.RectTransform().DOScale(1, scaleToDuration).SetEase(scaleEase));
            seq.Join(itemFrame.RectTransform().DOAnchorPos(Vector2.zero, scaleToDuration).SetEase(moveToEase));
            seq.AppendCallback(() => m_card.TintThenUntintOverTime(color, blend, 0.06f, 0.06f, 0.06f));
            seq.Append(itemFrame.RectTransform().DOPunchRotation(rotationAxis * ((Random.value > 0.5f ? -1 : 1) * rotationPower), rotationDuration, rotationVibration).SetEase(rotationEase));
            seq.Join(m_card.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, 0.3f).SetEase(Ease.OutBack));


            itemToggle.Toggle.isOn = false;

            m_card.SetActive(true);
            //m_card.interactiveShadow.enabled = false;
            //m_card.shadow.OffsetDistance = 0;
            m_card.RectTransform().anchoredPosition = new Vector2(-2, -2);

            itemFrame.Play("active");
            itemToggle.Toggle.interactable = true;

            //itemCostContainer.SetActive(true);
            //var itemData = Db.ItemDatabase.GetItem(_itemData.ItemId);
            // var price = itemData.GetCurrencyPrice(Currency.DAWN_DOLLARS);
            // var funds = App.Inventory.GetCurrencyAmount(Currency.DAWN_DOLLARS);
            // var prefixColor = price > funds ? $"<color={ColorPalette.CrossBlitz.FactionColor.WAR}>" : string.Empty;
            // itemCost.text = $"{prefixColor}{price:N0}";
            // itemAmountContainer.SetActive(false);
        }
    }

    [System.Serializable]
    public class RedeemPageItemData : IPageItemData
    {
        public string ItemId;
    }
}