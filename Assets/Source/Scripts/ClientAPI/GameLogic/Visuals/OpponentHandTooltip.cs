using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.Databases;
using CrossBlitz.InputAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.Utils;
using DG.Tweening;
using GameDataEditor;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class OpponentHandTooltip : MonoBehaviour, IPointerClickHandler
    {
        public CanvasGroup canvas;
        public RectTransform layout;
        public OpponentHandCardViewPageItem opponentHandCardPageItemPrefab;
        public RectTransform knownCardsTooltip;
        public RectTransform knownCardsLayout;
        public ControlItem controlItem;

        public OpponentKnownCardPageItem knownCardItemPagePrefab;
        public PageBehaviour OpponentKnownCardPages;

        private List<OpponentKnownCardPageData> m_knownCardItems;
        private bool m_updateInput;

        private void Start()
        {
            canvas.interactable = false;
            canvas.blocksRaycasts = false;
            canvas.alpha = 0;
        }

        private void Update()
        {
            if (m_updateInput && Controls.GetAction(Controls.Actions.TurnPageRightAlt))
            {
                OnPointerClick(null);
            }
        }

        public void Open()
        {
            if (!GameServer.IsRunning())
            {
                return;
            }

            layout.DestroyChildren();

            var opponentPlayer = GameServer.GetPlayerState(Team.Opponent);
            var cardBack = Db.CardBackDatabase.GetCardBackData(opponentPlayer.cardBackUid);

            for (var i = 0; i < opponentPlayer.hand.Count; i++)
            {
                var opponentPlayerPageItem = Instantiate(opponentHandCardPageItemPrefab, layout, false);
                opponentPlayerPageItem.SetCard(opponentPlayer.hand[i], cardBack.portraitUrl);

                opponentPlayerPageItem.GetComponent<CanvasGroup>().alpha = 0;
                opponentPlayerPageItem.GetComponent<CanvasGroup>().DOFade(1, 0.12f).SetDelay((i+1) * 0.06f);
            }

            var cardValues = new List<CardValue>();

            for (var i = 0; i < opponentPlayer.deck.Count; i++)
            {
                var cardState = GameServer.State.GetCard(opponentPlayer.deck[i]);
                var id = cardState.data.id;
                var cardValue = cardValues.Find(c => c.id == cardState.data.id);

                if (cardValue != null)
                {
                    cardValue.count++;
                }
                else
                {
                    cardValue = new CardValue
                    {
                        id = id, count = 1
                    };
                    cardValues.Add(cardValue);
                }
            }

            var cardvaluesCount = cardValues.Count;

            knownCardsTooltip.SetActive(cardvaluesCount > 0);

            //if (cardValues.Count > m_knownCardItems.Count)
            //{
            //    for (var i = m_knownCardItems.Count - 1; i > cardValues.Count - 1; i--)
            //    {
            //        Destroy(m_knownCardItems[i].gameObject);
            //    }
            //}

            m_knownCardItems = new List<OpponentKnownCardPageData>();

            for (var i = 0; i < cardvaluesCount; i++)
            {
                var knownCardItem = new OpponentKnownCardPageData { cardValueId = cardValues[i].id, cardCount = cardValues[i].count };
                m_knownCardItems.Add(knownCardItem);
            }

            OpponentKnownCardPages.InitializePages(knownCardItemPagePrefab, Mathf.Min(cardvaluesCount, 9), m_knownCardItems.ConvertAll(i => (IPageItemData)i));

            //@Rob animate in
            canvas.DOFade(1, 0.24f);

            m_updateInput = true;

            if (OpponentKnownCardPages.GetMaxNumberOfPages() > 1)
            {
                controlItem.SetActive(true);
                controlItem.SetAction(Controls.Actions.TurnPageRightAlt);
            }
            else
            {
                controlItem.SetActive(false);
            }
        }

        public void Close()
        {
            //@Rob animate out
            canvas.interactable = false;
            canvas.blocksRaycasts = false;
            canvas.DOFade(0, 0.24f);
            m_updateInput = false;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (OpponentKnownCardPages.GetMaxNumberOfPages() <= 1) return;

            if (OpponentKnownCardPages.CurrentPage + 1 < OpponentKnownCardPages.GetMaxNumberOfPages())
            {
                OpponentKnownCardPages.OnNextButton();
            }
            else
            {
                OpponentKnownCardPages.OnPreviousButton();
            }
        }
    }
}