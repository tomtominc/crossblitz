using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class MatchTurnBanner : MonoBehaviour
    {
        public CanvasGroup canvas;
        public SpriteAnimation banner;
        public SpriteAnimation overlay;

        private void Start()
        {
            canvas.alpha = 0;
            gameObject.SetActive(false);
        }

        public IEnumerator<float> OpenAndCloseTurnBanner(Team team)
        {
            gameObject.SetActive(true);

            if (team == Team.Client)
            {
                banner.Play("player");
                overlay.Play("player");
                AudioController.PlaySound("battle_turn_player", "BARDSFX");
            }
            else
            {
                banner.Play("opponent");
                overlay.Play("opponent");
                AudioController.PlaySound("battle_turn_enemy", "BARDSFX");
            }

            canvas.DOFade(1, 0.24f);

            yield return Timing.WaitForSeconds(1);

            canvas.DOFade(0, 0.24f);

            yield return Timing.WaitForSeconds(0.24f);

            gameObject.SetActive(false);
        }
    }
}