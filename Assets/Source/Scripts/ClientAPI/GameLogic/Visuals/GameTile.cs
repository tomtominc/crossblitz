using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Fables.Data;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using DG.Tweening;
using LeTai.TrueShadow;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class GameTile : MonoBehaviour
    {
        //public SpriteAnimation tileAnimator;
        public SpriteAnimation liftedTileAnimator;
        public SpriteAnimation outlineTile;
        public RectTransform cardContainer;
        public RectTransform effectsContainer;
        public CanvasGroup shineOverlay;
        public ParticleSystem shineParticles;
        public CanvasGroup swordGlow;
        public CanvasGroup staffGlow;
        public CanvasGroup bowGlow;

        private int _index;
        private Team _team;
        private Vector2Int _position;
        private RectTransform _animateRect;
        private RectTransform _rect;
        private RectTransform _shadow;
        private GameCardView _card;
        private List<GameCardView> _cardHistory;
        private BoardTarget _boardTarget;
        private bool _updatePlaceShine;
        private bool _updateWeaponGlow;
        private CanvasGroup _currentWeaponGlow;
        private TrueShadow m_shadow;

        private bool m_moveWithWaves;
        private float m_waveOffset;
        private Vector2 m_originalPosition;
        private float m_wavesMoveSpeed;

        public GameCardView Card => _card;
        public List<GameCardView> CardHistory => _cardHistory;
        public TileState State => GameServer.State.Board.GetTile(Position, Team);
        public int Index => _index;
        public Team Team => _team;
        public Vector2Int Position => _position;

        private void Awake()
        {
            _animateRect = this.RectTransform();
            _rect = transform.GetChild(0) as RectTransform;
            _boardTarget = GetComponent<BoardTarget>();
            _shadow = _boardTarget.shadow;
            _cardHistory = new List<GameCardView>();
            _boardTarget.OnShowTarget += OnBoardTargetShow;
            _boardTarget.OnHideTarget += OnBoardTargetHide;
            _boardTarget.OnCardEnterActiveBoardTarget += OnCardEnteredActiveBoardTarget;
            _boardTarget.OnCardExitActiveBoardTarget += OnCardExitActiveBoardTarget;
            m_shadow = liftedTileAnimator.GetComponent<TrueShadow>();
        }

        private void OnDestroy()
        {
            _boardTarget.OnShowTarget -= OnBoardTargetShow;
            _boardTarget.OnHideTarget -= OnBoardTargetHide;
            _boardTarget.OnCardEnterActiveBoardTarget -= OnCardEnteredActiveBoardTarget;
            _boardTarget.OnCardExitActiveBoardTarget -= OnCardExitActiveBoardTarget;
        }

        public void OnAllowedToMove(SceneEnvironment environment,  FableRoomData.RoomWeather weather)
        {
            m_originalPosition = _animateRect.anchoredPosition;
            m_moveWithWaves = environment == SceneEnvironment.Ocean;
            m_waveOffset = 0.24f * Position.y + Random.Range(0, 0.16f);
            m_wavesMoveSpeed = weather == FableRoomData.RoomWeather.RainHeavy ? 4 : 2;

            if (environment == SceneEnvironment.Ocean)
            {
                m_shadow.Color = "#4A5EA3".ToColor();
                m_shadow.BlendMode = BlendMode.Addictive;
                m_shadow.OffsetDistance = 2;
                m_shadow.OffsetAngle = 90;
            }
        }

        public bool HasAnotherCardObject(GameObject card)
        {
            if (cardContainer.childCount == 0) return false;
            if (cardContainer.childCount == 1) return cardContainer.GetChild(0) != card.transform;
            return true;
        }

        public void SetCard(GameCardView card, float moveToDuration = 0)
        {
            if (_card != null)
            {
                _cardHistory.RemoveAll(c => c == null);
                _cardHistory.Add(_card);
            }

            _card = card;

            if (_card != null)
            {
                if (moveToDuration > 0)
                {
                    _card.RectTransform().SetParent(cardContainer.RectTransform(), true);
                    _card.RectTransform().DOAnchorPos(Vector2.zero, moveToDuration).SetEase(Ease.InOutBack);
                }
                else if (moveToDuration == 0)
                {
                    _card.RectTransform().SetParent(cardContainer.RectTransform(), false);
                    _card.RectTransform().anchoredPosition = Vector2.zero;
                }
            }
        }

        public void DoPunchHit()
        {
            var rotate = Random.Range(25, 45);
            var duration = Random.Range(0.24f, 0.48f);

            _shadow.SetActive(true);

            _rect.DOBlendableRotateBy(new Vector3(0, -rotate, 0), duration);
            _rect.DOBlendableRotateBy(new Vector3(0, rotate * 2, 0), duration).SetDelay(duration/2f);
            _rect.DOBlendableRotateBy(new Vector3(0, -rotate, 0), duration).SetDelay(duration).SetEase(Ease.OutBounce);
            _rect.DOPunchScale(new Vector3(0.08f, 0.08f), duration, 3, 0.1f);
            _rect.DOPunchAnchorPos(new Vector2(0, 2), duration, 3, 0.1f).OnUpdate(() =>
            {
                if (_rect != null && _shadow != null)
                {
                    _shadow.anchoredPosition = new Vector2(_rect.anchoredPosition.y * 2, -_rect.anchoredPosition.y * 2);
                }

                if (_shadow != null)
                {
                    _shadow.SetActive(false);
                }
            });
        }

        public void DestroyAllEffects()
        {
            effectsContainer.DestroyChildren();
        }

        public bool HasAnyEffects()
        {
            return effectsContainer.childCount > 0;
        }

        private void OnBoardTargetShow(BoardTarget boardTarget)
        {
            //liftedTileAnimator.SetActive(true);

            if (GameCardView.Current && swordGlow && bowGlow && staffGlow)
            {
                if (GameCardView.Current.State.data.type == CardType.Minion)
                {
                    _currentWeaponGlow = null;

                    switch (GameCardView.Current.State.data.attackType)
                    {
                        case AttackType.Melee:
                            if (_index < 4) _currentWeaponGlow = swordGlow;
                            break;
                        case AttackType.Arcane:
                            _currentWeaponGlow = staffGlow;
                            break;
                        case AttackType.Ranged:
                            if(_index >= 4) _currentWeaponGlow = bowGlow;
                            break;
                    }

                    if (_currentWeaponGlow)
                    {
                        _currentWeaponGlow.SetActive(true);
                        _updateWeaponGlow = true;
                    }
                }
            }
        }

        private void OnBoardTargetHide(BoardTarget boardTarget)
        {
            //liftedTileAnimator.SetActive(false);
            _updateWeaponGlow = false;
            _currentWeaponGlow = null;

            swordGlow.SetActive(false);
            staffGlow.SetActive(false);
            bowGlow.SetActive(false);
        }

        private void OnCardEnteredActiveBoardTarget(BoardTarget boardTarget)
        {
            if (boardTarget == _boardTarget &&
                boardTarget.showMode == BoardTarget.ShowMode.Drop)
            {
                _updatePlaceShine = true;
            }
        }

        private void OnCardExitActiveBoardTarget(BoardTarget boardTarget)
        {
            _updatePlaceShine = false;
            shineOverlay.alpha = 0;

           // if (shineParticles.isPlaying)
            //{
                shineParticles.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            //}
        }

        private void Update()
        {
            if (_updatePlaceShine && shineOverlay && shineParticles)
            {
                shineOverlay.alpha = Mathf.PingPong(Time.time * 2f, 1f);

                if (!shineParticles.IsActive())
                {
                    shineParticles.SetActive(true);
                }

                if (!shineParticles.isPlaying)
                {
                    shineParticles.Play(true);
                    _updatePlaceShine = false;
                }
            }

            if (_updateWeaponGlow && _currentWeaponGlow)
            {
                // -10 so we can offset the glow with the shine overlay!
                _currentWeaponGlow.alpha = 0.25f  + Mathf.PingPong((Time.time-10) * 4f, 1f);
            }

            if (m_moveWithWaves)
            {
                var moveY = Mathf.Sin((Time.time + m_waveOffset) * m_wavesMoveSpeed);
                _animateRect.anchoredPosition = m_originalPosition + new Vector2(0, moveY);
            }
        }

        public void SetTeam(Team team, int index)
        {
            _index = index;
            _team = team;
            _position = new Vector2Int(_index%4, _index/4);

            if (team == Team.Opponent)
            {
                _position.y += 2;
            }

            var tileColor = _team == Team.Client ? "blue" : "red";
            var tileType = _index >= 4 ? "rm" : "mm";
            string tileEnvironment;

            switch (GameServer.Mode.SceneEnvironment)
            {
                case SceneEnvironment.PurpleWood:
                case SceneEnvironment.BrownWood:
                case SceneEnvironment.CaveWood:
                case SceneEnvironment.Stone:
                case SceneEnvironment.TownStone:
                case SceneEnvironment.MineStone:
                case SceneEnvironment.RedCarpet:
                case SceneEnvironment.PurpleCarpet:
                case SceneEnvironment.PrisonStone:
                case SceneEnvironment.PierStone:
                case SceneEnvironment.CasinoFloor:
                case SceneEnvironment.ManorFloor:
                    tileEnvironment = "wood";
                    break;
                case SceneEnvironment.Grass:
                case SceneEnvironment.StarscapeGrass:
                    tileEnvironment = "grass";
                    break;
                case SceneEnvironment.Sand: 
                    tileEnvironment = "sand";
                    break;
                case SceneEnvironment.Ocean: 
                    tileEnvironment = "ocean";
                    break;
                default: tileEnvironment = "wood";
                    break;
            }

            //tileAnimator.Play($"{tileColor}-{tileType}-{tileEnvironment}");
            liftedTileAnimator.Play($"{tileColor}-{tileType}-{tileEnvironment}");
            outlineTile.Play($"{tileColor}-{tileType}-{tileEnvironment}");

            //liftedTileAnimator.SetActive(false);
            outlineTile.SetActive(false);
        }

        //#if UNITY_EDITOR
        // private void OnGUI()
        // {
        //     if (Camera.main == null) return;
        //
        //     var point = Camera.main.WorldToScreenPoint(transform.position);
        //     Rect rect = new Rect(point.x, Screen.height - point.y, 100, 50);
        //     GUI.Label(rect, $"[{liftedTileAnimator.CurrentAnimationName}] ({State.Position.x},{State.Position.y})");
        //
        // }
        //#endif
    }
}
