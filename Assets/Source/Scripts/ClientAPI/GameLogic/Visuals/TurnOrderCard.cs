using System;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Data;
using CrossBlitz.Databases;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.UI.Widgets;
using DG.Tweening;
using GameDataEditor;
using LeTai.TrueShadow;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class TurnOrderCard : MonoBehaviour
    {
        public PlayerBattleState.TurnOrderCardType cardType;
        public SpriteAnimation cardAnimator;
        public TrueShadow shadow;
        public SpriteOutline outline;
        public CanvasGroup disabledOverlay;
        public ToggleButton toggle;
        public CanvasGroup canvas;
        public SpriteAnimation cardBack;

        private bool m_init;
        private bool m_animateRoulette;
        private const float RouletteDelay=0.08f;
        private float m_rouletteTimer;
        private Vector2 m_originalPosition;

        public Action<TurnOrderCard, bool> OnToggled;

        private void Start()
        {
            m_init = true;
            if (toggle) toggle.OnValueChanged += Toggled;
            cardAnimator.Play(cardType.ToString().ToLower());
            m_originalPosition = this.RectTransform().anchoredPosition;

            if (cardBack)
            {
                cardBack.Play( Db.CardBackDatabase.GetCardBackData( GameServer.GetPlayerState(Team.Opponent).cardBackUid ).portraitUrl);
            }
        }

        public void StartRoulette( PlayerBattleState.TurnOrderCardType targetType )
        {
            cardType = targetType;
            m_animateRoulette = true;
            m_rouletteTimer = RouletteDelay;
        }

        public void StopRoulette()
        {
            m_animateRoulette = false;
            cardAnimator.Play(cardType.ToString().ToLower());
        }

        private void Update()
        {
            if (m_animateRoulette)
            {
                m_rouletteTimer += Time.deltaTime;

                if (m_rouletteTimer >= RouletteDelay)
                {
                    m_rouletteTimer = 0;

                    if (cardAnimator.CurrentAnimationName == "rock")
                    {
                        cardAnimator.Play(UnityEngine.Random.value > 0.5f ? "paper" : "scissors");
                    }
                    else if (cardAnimator.CurrentAnimationName == "paper")
                    {
                        cardAnimator.Play(UnityEngine.Random.value > 0.5f ? "rock" : "scissors");
                    }
                    else if (cardAnimator.CurrentAnimationName == "scissors")
                    {
                        cardAnimator.Play(UnityEngine.Random.value > 0.5f ? "paper" : "rock");
                    }
                }
            }
        }

        private void Toggled(bool isOn, string content)
        {
            OnToggled?.Invoke(this,isOn);
        }

        public void FadeIn()
        {
            canvas.DOFade(1, 0.24f);
        }

        public void FadeOut()
        {
            canvas.DOFade(0, 0.24f);
        }

        public void Flip()
        {
            cardAnimator.SetActive(false);

            this.RectTransform().DOPunchAnchorPos(Vector2.down * 4f, 0.2f);

            cardBack.RectTransform().DOScaleX(0, 0.12f).SetEase(Ease.InBack)
                .OnComplete(() =>
                {
                    cardAnimator.SetActive(true);
                    cardAnimator.Play(cardType.ToString().ToLower());
                    cardAnimator.RectTransform().localScale = new Vector3(0, 1, 1);
                    cardAnimator.RectTransform().DOScaleX(1, 0.12f).SetEase(Ease.OutBack);
                });
        }

        public void SetAsNeutral(bool animate=true)
        {
            if (m_init && animate)
            {
                this.RectTransform().DOKill();
                this.RectTransform().DOAnchorPosY(m_originalPosition.y, 0.24f).SetEase(Ease.Linear);
            }

            shadow.OffsetDistance = 8;
            outline.SetActive(false);
            disabledOverlay.SetActive(false);
        }

        public void SetAsSelected()
        {
            if (m_init)
            {
                this.RectTransform().DOKill();
                this.RectTransform().DOPunchScale(Vector3.one * .4f, 0.24f);
                this.RectTransform().DOAnchorPosY(m_originalPosition.y, 0.24f).SetEase(Ease.OutBack);
            }

            shadow.OffsetDistance = 8;
            outline.SetActive(true);
            disabledOverlay.SetActive(false);
        }

        public void SetAsDisabled()
        {
            if (m_init)
            {
                this.RectTransform().DOKill();
                this.RectTransform().DOAnchorPosY(m_originalPosition.y - 6, 0.48f).SetEase(Ease.OutBounce);
            }

            shadow.OffsetDistance = 2;
            outline.SetActive(false);
            disabledOverlay.SetActive(true);
        }

    }
}