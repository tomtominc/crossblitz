using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Cursors;
using CrossBlitz.ServerAPI.GameLogic;
using DG.Tweening;
using TakoBoyStudios.Events;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class HandView : MonoBehaviour
    {
        private const float SideViewWidthStartingWidth = 60;
        private const float SideViewWidthAdditionForEachCard = 20;
        private const float SideViewMaxCardAngle = 10;
        private const float SideViewPositionOffsetX = 220f;
        private const float SideViewPositionOffsetY = -120f;

        private const float FrontViewPositionX = 0;
        private const float FrontViewPositionY = -160;
        private const float FrontViewWidthStartingWidth = 140;


        public enum ViewType
        {
            Side,
            Front,
        }

        public CardsPile hand;

        private Vector2 _screenSize;
        private ViewType _currentViewType;
        private bool _checkForDragCardAndSwapViewTypes;

        [HideInInspector] public List<GameCardView> cards;

        private void Start()
        {
            _currentViewType = ViewType.Front;
            _screenSize = new Vector2(this.RectTransform().rect.width, this.RectTransform().rect.height);

            hand.width = SideViewWidthStartingWidth;
            hand.maxCardAngle = SideViewMaxCardAngle;
            //hand.moveDuration = 500f;

            var additionalWidth = SideViewWidthAdditionForEachCard * 4;
            hand.RectTransform().anchoredPosition = new Vector2(SideViewPositionOffsetX + (additionalWidth/2f), SideViewPositionOffsetY);

            CrossBlitzInputModule.Instance.OnClickedNothing += OnClickedNothing;
            Events.Subscribe(EventType.OnCardClicked, OnCardClicked);

        }

        private void OnDestroy()
        {
            CrossBlitzInputModule.Instance.OnClickedNothing -= OnClickedNothing;
            Events.Unsubscribe(EventType.OnCardClicked, OnCardClicked);
        }

        public void UpdateCardsFromState()
        {
            cards.ForEach(x=>x.UpdateViewFromState());
        }

        public void TrackCard(GameCardView card)
        {
            if (cards.Exists(c => c.State.uid == card.State.uid))
            {
                Debug.LogError($"Card with same UID being added!! {card.State.uid}");
            }

            if (!cards.Contains(card))
            {
                cards.Add(card);
            }

            hand.Add(card.gameObject);
            ReformatHandCards(false, false);
            UpdateCardsFromState();

            card.SetCardMoveTarget(null);

            card.OnDragStarted += OnCardBeginDrag;
            card.OnDragEnded += OnCardEndDrag;

            Events.Publish(this, EventType.OnCardTrackedAndAddedToHand,
                new OnCardTrackedAndAddedToHandEventArgs { cardUid = card.State.uid, team = Team.Client });
        }

        public void RemoveCard(GameCardView card, Transform nextParent)
        {
            if (card == null) return;

            if (cards.Contains(card))
            {
                cards.Remove(card);
            }

            hand.Remove(card.gameObject, nextParent);
            ReformatHandCards(false, true);

            card.OnDragStarted -= OnCardBeginDrag;
            card.OnDragEnded -= OnCardEndDrag;
        }

        private void ReformatHandCards(bool isSwappingCurrentView, bool reformatCardsAndSnapMovement)
        {
            SetSortingOrders();

            switch (_currentViewType)
            {
                case ViewType.Side:
                {
                    var additionalWidth = SizeHand(ViewType.Side,0);
                    var position = new Vector2(SideViewPositionOffsetX + (additionalWidth / 2f), SideViewPositionOffsetY);

                    hand.RectTransform().DOKill();
                    hand.RectTransform().DOAnchorPos(position, 0.1f);
                    break;
                }
                case ViewType.Front:
                {
                    if (hand.Cards.Count <= 3)
                    {
                        SizeHand(ViewType.Side, 10);
                    }
                    else
                    {
                        SizeHand(ViewType.Front, 0);
                    }

                    hand.RectTransform().DOKill();
                    hand.RectTransform().DOAnchorPos(new Vector2(FrontViewPositionX, FrontViewPositionY), 0.1f);
                    break;
                }
            }


            if (reformatCardsAndSnapMovement)
            {
                hand.ReformatCards();
            }
        }

        private float SizeHand(ViewType viewType, float expandWidth)
        {
            var additionalWidth = 0f;

            switch (viewType)
            {
                case ViewType.Side:
                {
                    additionalWidth = SideViewWidthAdditionForEachCard * hand.Cards.Count;

                    hand.width = SideViewWidthStartingWidth + additionalWidth + expandWidth;
                    hand.height = hand.Cards.Count;
                    break;
                }
                case ViewType.Front:
                {
                    additionalWidth = SideViewWidthAdditionForEachCard * hand.Cards.Count;

                    hand.width = FrontViewWidthStartingWidth + additionalWidth + expandWidth;
                    hand.height = hand.Cards.Count;
                    break;
                }
            }

            return additionalWidth;
        }

        private void OnCardClicked(IMessage message)
        {
            if (message.Sender is GameCardView cardView && message.Data is OnCardClickedEventArgs args)
            {
                if (_currentViewType == ViewType.Side && cardView.View.viewType == CardView.CardViewType.STANDARD)
                {
                    var card = GameServer.State.GetCard(args.cardUid);

                    if (card.location == CardLocation.Hand)
                    {
                        MoveToFront();
                    }
                }
            }
        }

        private void OnClickedNothing()
        {
            if (StateController.CurrentState == StateDefinition.GAME_MULLIGAN ||
                StateController.CurrentState == StateDefinition.GAME_RO_SHAM_BO ||
                StateController.CurrentState == StateDefinition.GAME_START_GAME)
            {
                return;
            }

            if (_currentViewType == ViewType.Front)
            {
                MoveToSide();
            }
        }

        private void OnCardBeginDrag(GameCardView card)
        {
            if (!cards.Contains(card)) return;

            _checkForDragCardAndSwapViewTypes = _currentViewType == ViewType.Front;
        }

        private void OnCardEndDrag(GameCardView card)
        {
            if (!cards.Contains(card)) return;

            _checkForDragCardAndSwapViewTypes = false;
            ReformatHandCards(false, true);
        }

        private void Update()
        {
            if (_checkForDragCardAndSwapViewTypes && GameCardView.Current)
            {
                var screenMultiplier = CrossBlitzInputModule.Instance.ScreenMultiplier;
                var mousePosition = CrossBlitzInputModule.Instance.GetMousePosition(true);
                var xBound1 = Screen.width - (131 * screenMultiplier.x); // 131 is the width of the left panel
                var yBound1 = Screen.height / 2f;
                var deadZone = 4f * screenMultiplier.x;

                var xBound2 = 131 * screenMultiplier.x;// 131 is the width of the left panel
                var yBound2 = 19 * screenMultiplier.y;

                if (_currentViewType == ViewType.Side)
                {
                    // I'm canceling my action! Not targets live in this area
                    if (mousePosition.x > xBound1 + deadZone && mousePosition.y < yBound1 - deadZone)
                    {
                        MoveToFront();
                    }
                    // canceled on the very bottom of the screen -- this is also valid
                    else if (mousePosition.y < yBound2 - deadZone)
                    {
                        MoveToFront();
                    }
                }
                else if (_currentViewType == ViewType.Front)
                {
                    if (mousePosition.y < yBound2 + deadZone) return;
                    if (mousePosition.x > xBound1 - deadZone && mousePosition.y < yBound1 + deadZone) return;
                    // I'm inside target zones - we need to remove the cards from the view so we can see!
                    MoveToSide();
                }
            }
        }

        private void SetSortingOrders()
        {
            for (var i = 0; i < cards.Count; i++)
            {
                var card = cards[i];
                card.SetSortingOrder( GameCardView.HandSortingOrder + i );
            }
        }

        public void MoveToSide()
        {
            _currentViewType = ViewType.Side;
            ReformatHandCards(true,true);
            AudioController.PlaySound("battle_card_handmove_side", "BARDSFX", true, gameObject);
        }

        public void MoveToFront()
        {
            _currentViewType = ViewType.Front;
            ReformatHandCards(true,true);
            AudioController.PlaySound("battle_card_handmove_bottom", "BARDSFX", true, gameObject);
        }

        public void Setup()
        {

        }
    }
}
