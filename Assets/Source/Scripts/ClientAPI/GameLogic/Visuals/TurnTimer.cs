using System;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ServerAPI.GameLogic;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class TurnTimer : MonoBehaviour
    {
        public TextMeshProUGUI turnCount;
        public SpriteAnimation hourglass;

        public void Start()
        {
            hourglass.Play("turn-idle");
            Events.Subscribe(EventType.OnTurnStart, OnTurnStarted);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnTurnStart, OnTurnStarted);
        }

        // private void Update()
        // {
        //     if (GameServer.IsRunning())
        //     {
        //         turnCount.text = GameServer.State.GetTurnCount(Team.Client).ToString();
        //     }
        // }

        private void OnTurnStarted(IMessage message)
        {
            if (!GameServer.IsRunning()) return;

            if (message.Data is OnTurnStartEventArgs args)
            {
                if (args.isClient)
                {
                    hourglass.Play("flip-turn", () =>
                    {
                        turnCount.text = GameServer.State.GetTurnCount(Team.Client).ToString();
                        hourglass.Play("turn-idle");
                    });
                }
            }
        }
    }
}