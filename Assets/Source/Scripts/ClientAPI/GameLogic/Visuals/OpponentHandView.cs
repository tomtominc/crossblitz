using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.ServerAPI.GameLogic;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class OpponentHandView : MonoBehaviour
    {
        public RectTransform container;
        public RectTransform showPosition;
        public RectTransform hidePosition;
        public OpponentHandCard cardPrefab;
        public CardsPile cardsPile;
        public List<OpponentHandCard> opponentHandCards;
        private List<GameObject> cardsToRemove;

        private bool m_isTransitioning;
        private bool m_isShowing;
        public bool IsShowing => m_isShowing;
        public bool IsTransitioning => m_isTransitioning;

        private bool m_intialized;

        private void Awake()
        {
            m_intialized = false;
            container.anchoredPosition = hidePosition.anchoredPosition;
            cardsToRemove = new List<GameObject>();
            opponentHandCards = new List<OpponentHandCard>();
        }

        public void TrackCard(GameCardView card)
        {
            //Debug.Log($"Tracking card: {card.State.characterData.name}");
            var trackedCard = CreateCard(card.State.uid, GameplayScreen.Instance.opponentDeck.GetTopCard().transform);
            trackedCard.transform.position = card.transform.position;
            cardsPile.Add(trackedCard.gameObject);
            Destroy(card.gameObject);

            if (!m_intialized)
            {
                RefreshCards();
            }
        }

        public OpponentHandCard GetCard(string uid)
        {
            return opponentHandCards.Find(c => c.CardUid == uid);
        }

        public void RemoveCard(string uid, Transform nextParent=null, float destroyAfterTime=0)
        {
            var handCard = GetCard(uid);
            cardsPile.Remove(handCard.gameObject,nextParent,destroyAfterTime);
        }

        public IEnumerator<float> DiscardCard(string uid)
        {
            var handCard = GetCard(uid);

            if (handCard)
            {
                yield return Timing.WaitUntilDone( handCard.Discard() );
            }
        }

        private void OnDiscardedHandCard(OpponentHandCard card)
        {
            RemoveCard(card.CardUid);
        }

        [Button]
        public void Show()
        {
            if (Vector2.Distance(container.anchoredPosition, hidePosition.anchoredPosition) <= 10)
            {
                // drawn cards are already in the hand when this is called?
                RefreshCards();
            }

            if (m_isShowing)
            {
                return;
            }

            container.DOKill(true);
            m_isTransitioning = true;
            container.DOAnchorPos(showPosition.anchoredPosition, 0.4f)
                .SetEase(Ease.OutBack)
                .OnComplete(() =>
                {
                    m_isTransitioning = false;
                });

            m_isShowing = true;
        }

        [Button]
        public void Hide()
        {
            if (!m_isShowing)
            {
                return;
            }

            container.DOKill(true);
            m_isTransitioning = true;
            container.DOAnchorPos(hidePosition.anchoredPosition, 0.4f)
                .SetEase(Ease.InBack)
                .OnComplete(() =>
                {
                    m_isTransitioning = false;
                });

            m_isShowing = false;
        }

        private void Update()
        {
            var visualCount = cardsPile.Cards.Count;
            cardsPile.width = Mathf.Clamp(visualCount * 40, 40, 180);
        }

        private void RefreshCards()
        {
            m_intialized = true;

            var opponent = GameServer.GetPlayerState(Team.Opponent);

            for (var i = 0; i < opponent.hand.Count; i++)
            {
                var handView = GetCard(opponent.hand[i]);

                if (handView == null)
                {
                    var card = GameServer.State.GetCard(opponent.hand[i]);
                    Debug.Log($"Creating card: {card.characterData.name}");
                    handView = CreateCard(opponent.hand[i], this.RectTransform());
                    cardsPile.Add(handView.gameObject, i, false);
                }
            }

            //
            // for (var i = 0; i < cardsToRemove.Count; i++)
            // {
            //     var removal = cardsPile.Remove(cardsToRemove[i]);
            //     Destroy(removal);
            // }
            //
            // var visualCount = cardsPile.Cards.Count;
            // var dataCount = GameServer.GetPlayerState(Team.Opponent).GetHandCount();
            // var difference = Mathf.Abs(dataCount - visualCount);
            //
            //
            //
            // //cardsPile.width = Mathf.Clamp(visualCount * 40, 40, 180);
            //
            // if (difference > 0 && visualCount > dataCount)
            // {
            //     // lose cards
            //     for (var i = visualCount - 1; i > dataCount-1; i--)
            //     {
            //         cardsPile.Remove(cardsPile.Cards[i]);
            //     }
            // }
            // else
            // {
            //     // gain cards
            //     for (var i = 0; i < difference; i++)
            //     {
            //         var card = CreateCard();
            //         cardsPile.Add(card, false);
            //     }
            // }
        }

        private OpponentHandCard CreateCard(string cardUid, Transform parent)
        {
            var card = Instantiate(cardPrefab, parent, false);
            card.SetCard(cardUid, Db.CardBackDatabase.GetCardBackData(   GameServer.GetPlayerState(Team.Opponent).cardBackUid ) .portraitUrl);
            card.OnDiscarded += OnDiscardedHandCard;
            opponentHandCards.Add(card);
            return card;
        }
    }
}