using System.Collections.Generic;
using System.Threading.Tasks;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Data;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.ServerAPI.Models;
using CrossBlitz.Transition;
using Source.Scripts.AssetManagement;
using UnityAsync;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.ClientAPI.GameLogic.Visuals
{
    public class GameBoard : MonoBehaviour, IAssetLoader
    {
        public const int Columns = 4;
        public const int Rows = 4;

        public RectTransform playerBattlefield;
        public RectTransform opponentBattlefield;
        public GridLayoutGroup playerGrid;
        public GridLayoutGroup opponentGrid;
        public GameTile gameTilePrefab;
        public BoardTarget boardSpellTarget;

        private float m_timeBeforeMovement = -1;

        public string AssetName => name;
        private bool _isBusy = true;
        public bool IsBusy => _isBusy;

        [HideInInspector] public List<GameTile> tiles;

        private void Start()
        {
            //TransitionController.AssetsToUnload.Add(this);
        }

        public void SetupBattlefield()
        {
            // switch (GameServer.Mode.SceneEnvironment)
            // {
            //     case SceneEnvironment.Wood: boardDivider.Play("wood");
            //         break;
            //     case SceneEnvironment.Grass: boardDivider.Play("grass");
            //         break;
            //     case SceneEnvironment.Sand: boardDivider.Play("sand");
            //         break;
            // }

            SetupBattlefieldTiles();
        }

        public GameTile GetTile(Vector2Int position)
        {
            return tiles.Find(tile => tile.Position == position);
        }

        public GameTile GetTile(int index)
        {
            return tiles[index];
        }

        public GameTile GetTile(GameCardState card)
        {
            return tiles.Find(tile => tile.State.Occupied && tile.State.CardId == card.uid);
        }

        private void SetupBattlefieldTiles()
        {
            _isBusy = true;

            tiles = new List<GameTile>();

            for (var i = 0; i < 8; i++)
            {
                var battlefieldTileScript = Instantiate(gameTilePrefab, playerBattlefield, false);
                battlefieldTileScript.SetTeam(Team.Client, i);
                tiles.Add(battlefieldTileScript);

                var tileCanvas = battlefieldTileScript.GetComponent<Canvas>();
                tileCanvas.sortingOrder = 0;
            }

            for (var i = 0; i < 8; i++)
            {
                var battlefieldTileScript = Instantiate(gameTilePrefab, opponentBattlefield, false);
                battlefieldTileScript.SetTeam(Team.Opponent, i);
                tiles.Add(battlefieldTileScript);

                var tileCanvas = battlefieldTileScript.GetComponent<Canvas>();
                tileCanvas.sortingOrder = 0;
            }

            m_timeBeforeMovement = 1;

            _isBusy = false;
        }

        public void ShowSpellTarget()
        {
            boardSpellTarget.Show(BoardTarget.ShowMode.Drop);
        }

        public void HideSpellTarget()
        {
            boardSpellTarget.Hide();
        }

        public void Unload()
        {
        }

        private void Update()
        {
            if (m_timeBeforeMovement > 0)
            {
                m_timeBeforeMovement -= Time.deltaTime;

                if (m_timeBeforeMovement <= 0)
                {
                    var battle = Db.BattleDatabase.GetBattleData(GameServer.Mode.BattleUid);

                    playerGrid.enabled = false;
                    opponentGrid.enabled = false;

                    for (var i = 0; i < tiles.Count; i++)
                    {
                        tiles[i].OnAllowedToMove(GameServer.Mode.SceneEnvironment, battle?.Weather ?? FableRoomData.RoomWeather.Sunny);
                    }
                }
            }
        }

        /// <summary>
        /// Quickly tries to get the board card without waiting. Usually we wait for the board card in case its still being summoned, however,
        /// in special cases we may not care if its null.
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static GameCardView GetBoardCardNotImportant(string uid)
        {
            return GameplayScreen.Instance.GetBoardCard(uid, out var errorCode, out var errorLocation);
        }

        public static async Task<GetBoardCardResult> GetBoardCard(string uid, int maxTimeOutFrames = -1)
        {
            var errorCode = ClientErrorCode.None;
            var errorLocation = CardLocation.Null;
            var stackTrace = System.Environment.StackTrace;
            var timeOutFrames = maxTimeOutFrames > 0 ? maxTimeOutFrames : ClientRequest.MaxRequestLoops;

            GameCardView boardCard =null;

            var awaitTimeOut = 0;
            while (boardCard == null && errorCode == ClientErrorCode.None)
            {
                boardCard = GameplayScreen.Instance.GetBoardCard(uid, out errorCode, out errorLocation);
                awaitTimeOut++;

                if (awaitTimeOut >= timeOutFrames)
                {
                    //Debug.LogError($"Timed out trying to get board card {uid} {errorLocation}");
                    errorCode = ClientErrorCode.TimedOut;
                    break;
                }

                await Await.NextUpdate();
            }

            if (boardCard == null || errorCode != ClientErrorCode.None)
            {
                var worldPosition = Vector3.zero;
                var tile = GameServer.State.Board.GetTileWithCardOrHistoryOfCard(uid);

                if (tile != null)
                {
                    worldPosition = tile.GetView().transform.position;
                }

                return new GetBoardCardResult { worldPositionIfNotFound = worldPosition, boardCardUid = uid, error = errorCode, errorLocation = errorLocation };
            }

            return new GetBoardCardResult {boardCard = boardCard};
        }
    }
}
