using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Data;
using CrossBlitz.Databases;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using CrossBlitz.ViewAPI;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class TurnOrderOverlay : MonoBehaviour
    {
        [BoxGroup("Animation")] public CanvasGroup canvas;
        [BoxGroup("Animation")] public CanvasGroup background;
        [BoxGroup("Animation")] public RectTransform headerBanner;
        [BoxGroup("Animation")] public CanvasGroup headerGlow;
        [BoxGroup("Animation")] public GameObject dustParticles;
        [BoxGroup("Animation")] public CanvasGroup backgroundBanner;
        [BoxGroup("Animation")] public CanvasGroup backgroundBannerFight;
        [BoxGroup("Animation")] public CanvasGroup backgroundBannerChangerOverlay;

        [BoxGroup("Selection Phase")] public ButtonContainer confirmButton;
        [BoxGroup("Selection Phase")] public CanvasGroup confirmButtonContainer;
        [BoxGroup("Selection Phase")] public TextMeshProUGUI instructions;
        [BoxGroup("Selection Phase")] public List<TurnOrderCard> turnOrderCards;

        [BoxGroup("Fight Phase")] public GameObject fightLayout;
        [BoxGroup("Fight Phase")] public SpriteAnimation fightArrow;
        [BoxGroup("Fight Phase")] public SpriteAnimation fightText;
        [BoxGroup("Fight Phase")] public List<TurnOrderCard> fightCards;

        private TurnOrderCard m_currentCard;
        private bool m_isFinished;

        public bool Finished => m_isFinished;

        public void Start()
        {
            m_isFinished = false;

            canvas.alpha = 1;
            confirmButton.Button.onClick.AddListener(OnConfirm);
            confirmButton.SetInteractable(false);

            for (var i = 0; i < turnOrderCards.Count; i++)
            {
                turnOrderCards[i].SetAsNeutral();
                turnOrderCards[i].OnToggled += OnTurnOrderCardToggled;
            }

            for (var i = 0; i < fightCards.Count; i++)
            {
                fightCards[i].SetActive(false);
            }

            fightArrow.SetActive(false);
            fightText.SetActive(false);

            background.alpha = 0;
            background.interactable = true;
            background.blocksRaycasts = true;

            backgroundBanner.SetActive(true);
            backgroundBanner.alpha = 0;

            backgroundBannerFight.SetActive(false);
            backgroundBannerChangerOverlay.alpha = 0;

            headerBanner.localScale = new Vector3(1, 0, 1);

            headerGlow.SetActive(false);
            dustParticles.SetActive(false);

            instructions.text = "SELECT A CARD TO PLAY";

            Timing.RunCoroutine(AnimateIn().CancelWith(gameObject));
        }

        private IEnumerator<float> AnimateIn()
        {
            
            background.DOFade(1f, 0.25f);
            backgroundBanner.DOFade(1, 0.25f);
            headerBanner.DOScaleY(1, 0.5f).SetEase(Ease.OutElastic);
            AudioController.PlaySound("battle_intro_topbanner_appear", "BARDSFX");
            yield return Timing. WaitForSeconds(0.25f);

            
            headerGlow.SetActive(true);
            headerGlow.DOFade(0, 0.25f);
            AudioController.PlaySound("battle_intro_topbanner_flash", "BARDSFX");

            dustParticles.SetActive(true);

            for (var i = 0; i < turnOrderCards.Count; i++)
            {
                turnOrderCards[i].FadeIn();
            }
        }

        private IEnumerator<float> MoveToFightPhase()
        {
            fightCards[0].cardType = GameServer.ClientPlayerState.turnOrderSelection;

            m_currentCard.SetAsNeutral(false);
            m_currentCard.transform.DOMove(fightCards[0].transform.position, 0.5f)
                .SetEase(Ease.OutBack);

            backgroundBannerChangerOverlay.DOFade(1, 0.24f).OnComplete(() =>
            {
                instructions.text = "WAITING FOR OPPONENT";
                backgroundBannerFight.SetActive(true);
                backgroundBanner.alpha = 0;
                fightLayout.SetActive(true);
                backgroundBannerChangerOverlay.DOFade(0, 0.24f);
            });

            yield return Timing.WaitForSeconds(.5f);

            m_currentCard.SetActive(false);
            fightCards[0].SetActive(true);
            fightCards[0].SetAsNeutral();
            fightCards[0].canvas.alpha = 1;

            yield return Timing.WaitForSeconds(0.25f);

            if (GameServer.State.ConnectionMode == ConnectionMode.LOCAL)
            {
                var battle = Db.BattleDatabase.GetBattleData(GameServer.Mode.BattleUid);

                if (battle?.CustomBattleOptions != null && battle.CustomBattleOptions.Enabled)
                {
                    if (battle.CustomBattleOptions.ForceOpponentStartsFirst)
                    {
                        GameServer.OpponentPlayerState.turnOrderSelection =
                            GameServer.ClientPlayerState.turnOrderSelection switch
                            {
                                PlayerBattleState.TurnOrderCardType.Paper => PlayerBattleState.TurnOrderCardType.Scissors,
                                PlayerBattleState.TurnOrderCardType.Rock => PlayerBattleState.TurnOrderCardType.Paper,
                                _ => PlayerBattleState.TurnOrderCardType.Rock
                            };
                    }
                    else if (battle.CustomBattleOptions.ForcePlayerStartsFirst)
                    {
                        GameServer.OpponentPlayerState.turnOrderSelection =
                            GameServer.ClientPlayerState.turnOrderSelection switch
                            {
                                PlayerBattleState.TurnOrderCardType.Paper => PlayerBattleState.TurnOrderCardType.Rock,
                                PlayerBattleState.TurnOrderCardType.Rock => PlayerBattleState.TurnOrderCardType.Scissors,
                                _ => PlayerBattleState.TurnOrderCardType.Paper
                            };
                    }
                    else
                    {
                        GameServer.OpponentPlayerState.turnOrderSelection = (PlayerBattleState.TurnOrderCardType) Random.Range(0, 3);
                    }
                }
                else
                {
                    GameServer.OpponentPlayerState.turnOrderSelection = (PlayerBattleState.TurnOrderCardType) Random.Range(0, 3);
                }
            }

            while (GameServer.OpponentPlayerState.turnOrderSelection == PlayerBattleState.TurnOrderCardType.None)
                yield return Timing.WaitForOneFrame;

            //GameServer.OpponentPlayerState.turnOrderSelection = GameServer.ClientPlayerState.turnOrderSelection;

            instructions.text = "SHOWDOWN";
            fightCards[1].cardType = GameServer.OpponentPlayerState.turnOrderSelection;
            fightCards[1].SetActive(true);
            fightCards[1].canvas.alpha = 0;

            yield return Timing.WaitForOneFrame;

            fightCards[1].cardBack.SetActive(true);
            fightCards[1].cardAnimator.SetActive(false);

            for (var i = 0; i < turnOrderCards.Count; i++)
            {
                turnOrderCards[i].SetActive(false);
            }

            var originalPos = fightCards[1].RectTransform().anchoredPosition;
            fightCards[1].FadeIn();
            fightCards[1].RectTransform().anchoredPosition = new Vector2(originalPos.x + 300, originalPos.y);
            fightCards[1].RectTransform().DOAnchorPosX(originalPos.x, 0.4f).SetEase(Ease.OutBack);
            AudioController.PlaySound("battle_rockpaper_cardfly", "BARDSFX", true, fightCards[1].gameObject);

            yield return Timing.WaitForSeconds(0.5f);

            fightCards[1].Flip();
            AudioController.PlaySound("battle_rockpaper_cardflip", "BARDSFX", true, fightCards[1].gameObject);

            yield return Timing.WaitForSeconds(0.5f);

            fightArrow.SetActive(true);
            fightArrow.GetComponent<CanvasGroup>().alpha = 0;
            fightArrow.GetComponent<CanvasGroup>().DOFade(1, 0.1f);
            fightArrow.RectTransform().DOPunchAnchorPos(Vector2.up * 12, 0.24f);

            fightText.SetActive(true);
            fightText.GetComponent<CanvasGroup>().alpha = 0;
            fightText.GetComponent<CanvasGroup>().DOFade(1, 0.1f);
            fightText.RectTransform().DOPunchAnchorPos(Vector2.up *4, 0.24f);

            if (GameServer.OpponentPlayerState.turnOrderSelection == GameServer.ClientPlayerState.turnOrderSelection)
            {
                fightArrow.Play("draw");
                fightText.Play("draw");
                AudioController.PlaySound("battle_rockpaper_tie", "BARDSFX");

                var randomPlayerSelection = GameServer.ClientPlayerState.turnOrderSelection;
                var randomOpponentSelection=GameServer.OpponentPlayerState.turnOrderSelection;

                while (randomPlayerSelection == randomOpponentSelection)
                {
                    randomPlayerSelection = (PlayerBattleState.TurnOrderCardType) Random.Range(0, 3);
                    randomOpponentSelection = (PlayerBattleState.TurnOrderCardType) Random.Range(0, 3);
                }

                GameServer.ClientPlayerState.turnOrderSelection = randomPlayerSelection;
                GameServer.OpponentPlayerState.turnOrderSelection = randomOpponentSelection;

                fightCards[0].RectTransform().DOPunchAnchorPos(Vector2.left * 4, 0.4f);
                fightCards[1].RectTransform().DOPunchAnchorPos(Vector2.right * 4, 0.4f);

                fightCards[0].RectTransform().DOPunchRotation(Vector3.forward * 4, 0.4f);
                fightCards[1].RectTransform().DOPunchRotation(Vector3.back * 4, 0.4f);

                yield return Timing.WaitForSeconds(1f);
                AudioController.PlaySound("battle_rockpaper_draw_shuffle", "BARDSFX");

                fightCards[0].StartRoulette(randomPlayerSelection);
                fightCards[1].StartRoulette(randomOpponentSelection);

                yield return Timing.WaitForSeconds(1.3f);

                fightCards[0].StopRoulette();
                fightCards[1].StopRoulette();
            }

            if (GameServer.IsClientGoingFirst())
            {
                fightArrow.Play("you-go-first");
                fightText.Play("you-go-first");
                AudioController.PlaySound("battle_rockpaper_win", "BARDSFX");

                fightCards[0].SetAsSelected();
                fightCards[1].SetAsDisabled();

                GameServer.State.SetCurrentPlayer(GameServer.ClientPlayerState.Uid);

                // GameServer.OpponentPlayerState
                //     .CreateCardAtLocation(
                //         Db.CardDatabase.GetCard(GameServer.OpponentPlayerState.turnOrderSelection.ToString()),
                //         CardLocation.Deck, atIndex:3);
            }
            else
            {
                fightArrow.Play("opponent-goes-first");
                fightText.Play("opponent-goes-first");
                AudioController.PlaySound("battle_rockpaper_lose", "BARDSFX");

                fightCards[0].SetAsDisabled();
                fightCards[1].SetAsSelected();

                GameServer.State.SetCurrentPlayer(GameServer.OpponentPlayerState.Uid);

                // GameServer.ClientPlayerState
                //     .CreateCardAtLocation(
                //         Db.CardDatabase.GetCard(GameServer.ClientPlayerState.turnOrderSelection.ToString()),
                //         CardLocation.Deck, atIndex:3);
            }

            yield return Timing.WaitForSeconds(1.5f);

            canvas.DOFade(0, 0.24f);

            yield return Timing.WaitForSeconds(0.24f);

            gameObject.SetActive(false);

            m_isFinished = true;
        }

        private void OnTurnOrderCardToggled(TurnOrderCard card, bool isOn)
        {
            m_currentCard = null;

            for (var i = 0; i < turnOrderCards.Count; i++)
            {
                if (turnOrderCards[i].toggle.Toggle.isOn)
                {
                    turnOrderCards[i].SetAsSelected();
                    m_currentCard = turnOrderCards[i];
                }
                else
                {
                    turnOrderCards[i].SetAsDisabled();
                }
            }

            if (m_currentCard == null)
            {
                for (var i = 0; i < turnOrderCards.Count; i++)
                {
                    turnOrderCards[i].SetAsNeutral();
                }
            }

            confirmButton.SetInteractable( m_currentCard != null );
        }

        private void OnConfirm()
        {
            if (m_currentCard == null)
            {
                return;
            }


            AudioController.PlaySound("ui_click", "BARDSFX");

            confirmButton.SetInteractable(false);
            confirmButtonContainer.interactable = false;
            confirmButtonContainer.blocksRaycasts = false;

            for (var i = 0; i < turnOrderCards.Count; i++)
            {
                turnOrderCards[i].OnToggled -= OnTurnOrderCardToggled;

                if (m_currentCard != turnOrderCards[i])
                {
                    turnOrderCards[i].FadeOut();
                }
            }

            confirmButtonContainer.DOFade(0, 0.24f);

            GameServer.ClientPlayerState.turnOrderSelection = m_currentCard.cardType;

            Timing.RunCoroutine(MoveToFightPhase().CancelWith(gameObject));
        }
    }
}