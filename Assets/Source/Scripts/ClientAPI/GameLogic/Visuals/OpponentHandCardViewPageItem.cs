using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ServerAPI.GameLogic;
using TMPro;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class OpponentHandCardViewPageItem : MonoBehaviour
    {
        public SpriteAnimation cardBack;
        public TextMeshProUGUI turnObtained;
        public GameObject cardViewPrefab;

        private CardView m_cardView;

        public void SetCard(string uid, string cardBackPortrait)
        {
            var card = GameServer.State.GetCard(uid);
            turnObtained.text = card.turnAddedToHand.ToString();
            cardBack.Play(cardBackPortrait);

            if (m_cardView == null)
            {
                m_cardView = CardFactory.Create(new CreateCardFactorySettings
                {
                    CardState = card,
                    CardPrefab = cardViewPrefab,
                    Layout = this.RectTransform(),
                    AdditionalComponents = new List<Type> { typeof(GameCardView) },
                    RemoveCanvas = true,
                    RemoveShadow = true
                }, null);
            }
        }
    }
}