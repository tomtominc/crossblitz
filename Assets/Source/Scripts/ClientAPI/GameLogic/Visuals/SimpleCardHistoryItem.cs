using System;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using DG.Tweening;
using QFSW.QC.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class SimpleCardHistoryItem : MonoBehaviour
    {
        public CanvasGroup canvasGroup;
        public RectTransform containerRect;
        public TextMeshProUGUI titleText;
        public Image containerImage;
        public TextMeshProUGUI historyLabel;

        private void AnimateIn()
        {
            // canvasGroup.alpha = 0;
            // containerRect.anchoredPosition = new Vector2(20, 0);
            //
            // canvasGroup.DOFade(1, 0.12f);
            // containerRect.DOAnchorPos(Vector2.zero, 0.24f).SetEase(Ease.OutBack);
        }

        public void SetTitle(string title)
        {
            containerImage.enabled = false;
            titleText.SetActive(true);
            titleText.text = title;
            historyLabel.SetActive(false);
        }

        public void SetModifierHistory(CardHistory history)
        {
            AnimateIn();

            historyLabel.text = string.Empty;
            historyLabel.text += history.Applied ? "[APPLIED]\n" : "[PENDING]\n";

            var appliedBy = GameServer.State.GetCard(history.AppliedBy);
            var characterName = appliedBy?.characterData?.name;

            if (history.PowerModifier > 0 && history.HealthModifier > 0)
            {
                historyLabel.text += $"+{history.PowerModifier}/+{history.HealthModifier} from {characterName}.";
            }
            else if (history.PowerModifier < 0 && history.HealthModifier < 0)
            {
                historyLabel.text += $"{history.PowerModifier}/{history.HealthModifier} from {characterName}.";
            }
            else if (history.PowerModifier > 0)
            {
                historyLabel.text += $"+{history.PowerModifier} power from {characterName}.";
            }
            else if (history.PowerModifier < 0)
            {
                historyLabel.text += $"{history.PowerModifier} power from {characterName}.";
            }
            else if (history.HealthModifier > 0)
            {
                historyLabel.text += $"+{history.HealthModifier} health from {characterName}.";
            }
            else if (history.HealthModifier < 0)
            {
                historyLabel.text += $"{history.HealthModifier} health from {characterName}.";
            }
            else if (history.CostModifier > 0)
            {
                historyLabel.text += $"+{history.CostModifier} cost from {characterName}.";
            }
            else if (history.CostModifier < 0)
            {
                historyLabel.text += $"-{history.CostModifier} cost from {characterName}.";
            }
            else if (history.DamageAmount > 0)
            {
                historyLabel.text += $"Damaged by {characterName} for {history.DamageAmount} damage.";
            }
            else if (history.HealAmount > 0)
            {
                historyLabel.text += $"Healed by {characterName} for {history.HealAmount}.";
            }
            else if (history.ArmorModifier > 0)
            {
                historyLabel.text += $"Gained +{history.ArmorModifier} armor from {characterName}.";
            }
            else if (history.ArmorRemoval > 0)
            {
                historyLabel.text += $"Lost {history.ArmorRemoval} from {characterName}.";
            }
        }

        public void SetActionHistory(Command command)
        {
            AnimateIn();
            historyLabel.text = command.GetLabelDisplay();
        }

        private void OnDestroy()
        {
            containerRect.DOKill();
            canvasGroup.DOKill();
        }
    }
}