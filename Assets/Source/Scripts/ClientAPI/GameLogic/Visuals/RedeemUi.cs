using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.Dialogue;
using CrossBlitz.InputAPI;
using CrossBlitz.Shop;
using CrossBlitz.Utils;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class RedeemUi : MonoBehaviour
    {
        [BoxGroup("Shop Pages")] public PageBehaviour shopPage;
        [BoxGroup("Shop Pages")] public RedeemPageItem shopPageItem;
        [BoxGroup("Shop Pages")] public ToggleGroup toggleGroup;

        [BoxGroup("Purchase Window")] public Button buyButton;

        [BoxGroup("Shopkeeper Custom Dialogue")] public DialogueBox shopKeeperDialogue;
        [BoxGroup("Shopkeeper Custom Dialogue")] public RectTransform newDialogueSpeechBubble;
        [BoxGroup("Shopkeeper Custom Dialogue")] public Button shopKeeperPlayDialogueButton;

        [BoxGroup("Misc")] public CanvasGroup canvasGroup;
        [BoxGroup("Misc")] public Button hideButton;
        [BoxGroup("Misc")] public CanvasGroup hideButtonCanvas;

        [BoxGroup("Misc")] public Button showButton;
        [BoxGroup("Misc")] public CanvasGroup showButtonCanvas;

        [BoxGroup("Animation")] public RectTransform shopContainer;
        [BoxGroup("Animation")] public CanvasGroup background;
        [BoxGroup("Animation/Timings")] public float delayBeforeOpen=0.5f;
        [BoxGroup("Animation/Timings")] public float durationMultiplier = 1;
        [BoxGroup("Animation/Properties")] public Vector3 headerPunchRotationAxis = new Vector3(0,0,1);

        private RedeemPageItem m_current;
        private Vector2 shopContainerOriginalPos;

        private bool _init;
        public event Action<string> OnCardRedeemed;

        private void Init()
        {
            if (_init) return;

            _init = true;


            shopContainerOriginalPos = shopContainer.anchoredPosition;

            buyButton.onClick.RemoveAllListeners();
            buyButton.onClick.AddListener(OnBuyItem);
            buyButton.interactable = false;

            shopKeeperPlayDialogueButton.onClick.RemoveAllListeners();
            shopKeeperPlayDialogueButton.onClick.AddListener(OnShopKeeperShowCustomDialogue);

            hideButton.onClick.RemoveAllListeners();
            hideButton.onClick.AddListener(OnHideButton);

            showButton.onClick.RemoveAllListeners();
            showButton.onClick.AddListener(OnShowButton);

            shopPage.OnPageChanged -= OnPageChanged;
            shopPage.OnPageChanged += OnPageChanged;
        }

        private void OnShopKeeperShowCustomDialogue()
        {

        }

        public IEnumerator<float> Open(RedeemCommand command)
        {
            Init();

            // setup animation
            background.alpha = 0;

            shopContainer.anchoredPosition = new Vector2(shopContainerOriginalPos.x, -400);
            shopKeeperDialogue.SetActive(false);

            hideButton.SetActive(true);
            hideButtonCanvas.alpha = 0;
            hideButton.interactable = false;

            showButton.SetActive(false);
            showButtonCanvas.alpha = 1;
            showButton.interactable = false;

            gameObject.SetActive(true);
            RefreshStore(command,true);

            // ------ animation stuff! ------

            yield return Timing.WaitForSeconds(delayBeforeOpen);

            background.DOFade(1, 0.3f * durationMultiplier);
            shopContainer.DOAnchorPos(shopContainerOriginalPos, 0.6f* durationMultiplier).SetEase(Ease.OutBack);
            hideButtonCanvas.DOFade(1, 0.3f * durationMultiplier);

            AudioController.PlaySound("ui_window_open", "BARDSFX");

            yield return Timing.WaitForSeconds(0.4f* durationMultiplier);

            shopKeeperDialogue.SetActive(true);
            shopKeeperDialogue.InitDialogueBox(null);

            hideButton.interactable = true;

            Timing.RunCoroutine( shopKeeperDialogue.ShowText(new DialogueInfo
            {
                SkipPressedActions = true,
                Text = "Select a card to add to your hand.",
                SkipAnimateIn = false
            }));
        }

        public IEnumerator<float> Close()
        {
            yield return Timing.WaitUntilDone(shopKeeperDialogue.AnimateOut());
            yield return Timing.WaitForSeconds(0.2f* durationMultiplier);

            shopContainer.DOAnchorPosY(-400f, 0.6f* durationMultiplier).SetEase(Ease.InBack);

            AudioController.PlaySound("ui_window_close", "BARDSFX");

            background.DOFade(0, 0.3f * durationMultiplier);
            hideButtonCanvas.DOFade(0, 0.3f * durationMultiplier);

            yield return Timing.WaitForSeconds(0.6f * durationMultiplier);

            gameObject.SetActive(false);
        }

        private void RefreshStore(RedeemCommand command, bool resetItems=false)
        {
            var shownItems = new List<RedeemPageItemData>();

            for (var i = 0; i < command.Cards.Count; i++)
            {
                var item = new RedeemPageItemData
                {
                    ItemId = command.Cards[i]
                };

                //var card = Db.CardDatabase.GetCard(item.ItemId);
                shownItems.Add(item);
            }

            shopPage.InitializePages(shopPageItem, 3, shownItems.Cast<IPageItemData>().ToList());

            for (var i = 0; i < shopPage.Items.Count; i++)
            {
                var item = (RedeemPageItem) shopPage.Items[i];
                item.itemToggle.Toggle.group = toggleGroup;
                item.OnClicked += OnShopItemSelected;
            }
        }

        private void OnShopItemSelected(bool isOn, RedeemPageItem itemClicked)
        {
            m_current = null;

            for (var i = 0; i < shopPage.Items.Count; i++)
            {
                var item = (RedeemPageItem)shopPage.Items[i];

                if (item.itemToggle.Toggle.isOn)
                {
                    m_current = item;
                }
            }

            if (m_current)
            {
                buyButton.interactable = true;
            }
            else
            {
                buyButton.interactable = false;
            }
        }

        private void OnBuyItem()
        {
            if (m_current != null)
            {
                AudioController.PlaySound("ui_click", "BARDSFX");
                OnCardRedeemed?.Invoke( m_current.Data.ItemId );
                hideButton.interactable = false;
                showButton.interactable = false;
                //toggleButton.onClick.RemoveAllListeners();
            }
        }

        private void OnHideButton()
        {
            hideButton.SetActive(false);
            hideButton.interactable = false;
            showButton.SetActive(true);
            showButton.interactable = false;

            canvasGroup.interactable = false;
            Timing.RunCoroutine(FadeMenu(0));

        }

        private void OnShowButton()
        {
            hideButton.SetActive(true);
            hideButton.interactable = false;
            showButton.SetActive(false);
            showButton.interactable = false;

            Timing.RunCoroutine(FadeMenu(1));
        }

        private IEnumerator<float> FadeMenu(int alpha)
        {
            yield return Timing.WaitUntilDone(canvasGroup.DOFade(alpha, 0.1f).WaitForCompletion(true));

            if (alpha == 1)
            {
                canvasGroup.interactable = true;
                hideButton.interactable = true;
            }
            else
            {
                showButton.interactable = true;
            }
        }


        private void OnPageChanged(int page)
        {
        }
    }
}