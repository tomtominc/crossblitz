using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.Utils;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class RelicSlot : MonoBehaviour, ICursorSelectable
    {
        public SpriteAnimation slot;
        public SpriteAnimation relicIcon;
        public RectTransform zoomedCardParent;
        public SpriteAnimation relicTriggerEffect;

        private string m_relicId;
        private string m_relicGameUid;
        private HoverCard m_hoverCard;

        public string RelicUid => m_relicGameUid;

        public void SetRelic(string relicId, string relicGameUid)
        {
            m_relicId = relicId;
            m_relicGameUid = relicGameUid;
            m_hoverCard = gameObject.AddComponent<HoverCard>();
            m_hoverCard.Initialize(Db.CardDatabase.GetCard(m_relicId), zoomedCardParent, 0);
            zoomedCardParent.SetActive(true);

            UpdateRelicUsage();
        }

        public void SetAsEmpty()
        {
            if (slot.IsActive())
            {
                slot.Play("empty");
            }

            relicIcon.SetActive(false);
            zoomedCardParent.SetActive(false);
        }

        // public void SetAsLocked()
        // {
        //     slot.Play("locked");
        //     relicIcon.SetActive(false);
        //     zoomedCardParent.SetActive(false);
        // }

        public void Trigger()
        {
            relicTriggerEffect.SetActive(true);
            relicTriggerEffect.Play("trigger", ()=> relicTriggerEffect.SetActive(false));
            AudioController.PlaySound("battle_relic_activate", "BARDSFX", true, gameObject);
        }

        public void UpdateRelicUsage()
        {
            if (string.IsNullOrEmpty(m_relicId))
            {
                SetAsEmpty();

                return;
            }

            if (slot.IsActive())
            {
                slot.Play("equipped");
            }

            relicIcon.SetActive(true);
            zoomedCardParent.SetActive(true);

            var cardData = Db.CardDatabase.GetCard(m_relicId);

            if (GameServer.IsRunning() && !string.IsNullOrEmpty(m_relicGameUid))
            {
                var gameCardData = GameServer.State.GetCard(m_relicGameUid);

                if (gameCardData.location == CardLocation.Graveyard)
                {
                    relicIcon.Play($"{cardData.portraitUrl}-disabled");
                }
                else if (!relicIcon.Play(cardData.portraitUrl))
                {
                    relicIcon.Play("redwing-flag-disabled");
                }
            }
            else if (!relicIcon.Play(cardData.portraitUrl))
            {
                relicIcon.Play("redwing-flag-disabled");
            }
        }

        public bool Interactable => false;
        public bool Grabbable => false;
        public bool InfoOnly => true;
        public bool Loading => false;
    }
}