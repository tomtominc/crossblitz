using CrossBlitz.Card;
using TMPro;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class OpponentKnownCardItem : MonoBehaviour
    {
        public TextMeshProUGUI cardName;
        public TextMeshProUGUI cardCount;
        public SpriteAnimation factionBackground;
        public SpriteAnimation typeIcon;

        public void SetCard(CardData data, int count)
        {
            cardName.text = data.name.ToUpper();
            cardCount.text = $"X{count}";
            factionBackground.Play(data.faction.ToString().ToLower());
            typeIcon.Play(data.type.ToString().ToLower());
        }
    }
}