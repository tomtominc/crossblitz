using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class SimpleCardHistoryView : MonoBehaviour
    {
        public SimpleCardHistoryItem historyPrefab;
        public RectTransform modifiersLayout;
        public Button closeButton;
        private GameCardState m_cardState;

        private void Start()
        {
            closeButton.onClick.AddListener(Close);
        }

        public void Open(GameCardState cardState)
        {
            if (!App.Settings.GetShowDebugHistories()) return;

            m_cardState = cardState;
            UpdateCardHistory();
            gameObject.SetActive(true);
        }

        public void Open()
        {
            if (!App.Settings.GetShowDebugHistories()) return;

            m_cardState = null;
            UpdateHistory();
            gameObject.SetActive(true);
        }

        public void Close()
        {
            gameObject.SetActive(false);
        }

        public void UpdateCardHistory()
        {
            modifiersLayout.DestroyChildren();

            var modifierTitle = Instantiate(historyPrefab, modifiersLayout, false);
            modifierTitle.SetTitle("MODIFIERS");

            for (var i = 0; i < m_cardState.pendingHistory.Count; i++)
            {
                var history = Instantiate(historyPrefab, modifiersLayout, false);
                history.SetModifierHistory(m_cardState.pendingHistory[i]);
            }

            var actionsTitle = Instantiate(historyPrefab, modifiersLayout, false);
            actionsTitle.SetTitle("ACTION HISTORY");

            var historyOfCard = GameServer.State.GetCardHistories(m_cardState.uid);

            for (var i = 0; i < historyOfCard.Count; i++)
            {
                var history = Instantiate(historyPrefab, modifiersLayout, false);
                history.SetActionHistory(historyOfCard[i]);
            }
        }

        private void UpdateHistory()
        {
            modifiersLayout.DestroyChildren();

            var modifierTitle = Instantiate(historyPrefab, modifiersLayout, false);
            modifierTitle.SetTitle("HISTORY ALL TIME");

            for (var i = 0; i < GameServer.State.History.Count; i++)
            {
                var history = Instantiate(historyPrefab, modifiersLayout, false);
                history.SetActionHistory(GameServer.State.History[i]);
            }
        }
    }
}