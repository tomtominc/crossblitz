using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asyncoroutine;
using CrossBlitz.AssetManagement;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.GameVfx;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using DG.Tweening;
using MEC;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class MulliganView : MonoBehaviour
    {
        public CanvasGroup background;
        public RectTransform headerBanner;
        public CanvasGroup headerGlow;
        public GameObject dustParticles;
        public CanvasGroup backgroundBanner;
        public RectTransform layout;
        public RectTransform cardHolder;
        public Button readyButton;
        public CanvasGroup readyButtonCanvas;
        public TextMeshProUGUI instructionsLabel;

        private bool _isReady;
        private List<CardView> _mulliganCards;

        private void Awake()
        {
            headerBanner.localScale = new Vector3(1, 0, 1);
            headerGlow.alpha = 1;
            backgroundBanner.alpha = 0;
            background.alpha = 0;
            readyButtonCanvas.alpha = 0;
        }

        private void Start()
        {
            Events.Subscribe(EventType.OnContinueFromMatchFinished, OnContinueFromMatchFinished);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnContinueFromMatchFinished, OnContinueFromMatchFinished);
        }

        private void OnContinueFromMatchFinished(IMessage message)
        {
            gameObject.SetActive(false);
        }

        private async Task LoadAllHandCards()
        {
            var zoomedCardPrefab = await AddressableReferenceLoader.GetAsset("Card_Zoomed", true);
            var settings = new CreateCardFactorySettings
            {
                CardPrefab = zoomedCardPrefab,
                Layout = cardHolder,
                AdditionalComponents = new List<Type>{ typeof (GameCardView), typeof(MulliganCrossComponent), typeof(CardTransformComponent) }
            };

            for (var i = 0; i < GameServer.ClientPlayerState.hand.Count; i++)
            {
                var tempLayoutItem = new GameObject($"temp{i}", typeof(RectTransform));
                tempLayoutItem.RectTransform().SetParent(layout, false);

                var cardState = GameServer.State.GetCard(GameServer.ClientPlayerState.hand[i]);
                settings.CardState = cardState;
                var cardView = CardFactory.Create(settings, null);
                cardView.SetInteractable(false);
                cardView.cardOutline.RemoveOutline();

                cardView.RectTransform().anchoredPosition = new Vector2(
                    (cardHolder.rect.width/2) + (cardView.RectTransform().sizeDelta.x/2),
                    -(cardHolder.rect.height/2) - (cardView.RectTransform().sizeDelta.y/2));

                await Task.Delay(200);

                cardView.SetActive(true);
                cardView.GetCardComponent<GameCardView>().SetCardMoveTarget(tempLayoutItem.RectTransform(), 32);

                var randomPitch = UnityEngine.Random.Range(0.9f, 1.1f);
                //AudioController.PlaySound("battle_card_fly", "BARDSFX");//.setPitch(randomPitch);
            }
        }

        private async Task<List<GameCardView>> CreateStandardCards()
        {
            var reference = await AddressableReferenceLoader.GetAsset("Card_Standard", true);

            var cards = new List<GameCardView>();
            var settings = new CreateCardFactorySettings
            {
                CardPrefab = reference,
                Layout = GameplayScreen.Instance.handView.RectTransform(),
                AdditionalComponents = CardView.GetRequiredComponentsForStandardHandCard(),
            };

            for (var i = 0; i < cardHolder.childCount; i++)
            {
                var zoomedCardView = cardHolder.GetChild(i).GetComponent<GameCardView>();

                settings.CardState = zoomedCardView.State;
                settings.SortingOrder = GameCardView.HandSortingOrder + i;

                var standardCardView = CardFactory.Create(settings, null);
                var gameCardView = standardCardView.GetCardComponent<GameCardView>();

                standardCardView.SetInteractable(false);
                standardCardView.SetActive(false);

                cards.Add(gameCardView);

                // await Task.Delay(120);
                //
                // zoomedCardView.SetCardMoveTarget(GameplayScreen.Instance.handView.layout.GetChild(i).RectTransform());
                //
                // GameplayScreen.Instance.handView.TrackCard(gameCardView, i);
            }

            return cards;
        }

        public IEnumerator<float> AnimateMulligan()
        {
            _isReady = false;
            _mulliganCards = new List<CardView>();

            readyButton.onClick.AddListener(ReadyButtonPressed);
            readyButtonCanvas.alpha = 0;
            readyButtonCanvas.interactable = false;
            readyButtonCanvas.blocksRaycasts = true;

            background.interactable = true;
            background.blocksRaycasts = true;
            background.DOFade(1f, 0.25f);

            layout.SetLeft(0f);
            layout.SetRight(0f);

            instructionsLabel.text =
                LocalizationController.Localize("MulliganInstructionsSelectCard", "SELECT CARDS TO REPLACE");

            backgroundBanner.alpha = 0;
            backgroundBanner.DOFade(1, 0.25f);
            headerBanner.localScale = new Vector3(1, 0, 1);
            headerBanner.DOScaleY(1, 0.5f).SetEase(Ease.OutElastic);
            AudioController.PlaySound("battle_intro_topbanner_appear", "BARDSFX");

            yield return Timing. WaitForSeconds(0.25f);
            headerGlow.DOFade(0, 0.25f);
            dustParticles.SetActive(true);

            // animate the cards coming up
            yield return Timing.WaitUntilDone(LoadAllHandCards().AsCoroutine());

            // have the cards become selectable
            for (var i = 0; i < cardHolder.childCount; i++)
            {
                var cardObject = cardHolder.GetChild(i);
                var cardView = cardObject.GetComponent<GameCardView>();
                cardView.SetupForMulligan();
                cardView.OnLeftClicked += SelectedForMulligan;
            }

            readyButtonCanvas.DOFade(1,0.25f);
            readyButtonCanvas.interactable = true;

            // wait until the player selects they're ready
            while (!_isReady) yield return Timing.WaitForOneFrame;

            for (var i = 0; i < cardHolder.childCount; i++)
            {
               var cardView = cardHolder.GetChild(i).GetComponent<CardView>();
               cardView.cardOutline.RemoveOutline();
               cardView.SetInteractable(false);
            }

            // replace the cards you mulligan & keep a reference to the new ones
            for (var i = 0; i < _mulliganCards.Count; i++)
            {
                var cardUid = _mulliganCards[i].GetCardComponent<GameCardView>().State.uid;
                var replacement = GameServer.ClientPlayerState.Mulligan(cardUid);

                Debug.Log($"Replaced {cardUid} w/ {replacement}");
            }

            var battle = Db.BattleDatabase.GetBattleData(GameServer.Mode.BattleUid);

            if (battle.CustomBattleOptions != null &&
                battle.CustomBattleOptions.Enabled &&
                battle.CustomBattleOptions.PlayerDeckOverride != null &&
                battle.CustomBattleOptions.PlayerDeckOverride.Enabled &&
                battle.CustomBattleOptions.PlayerDeckOverride.DrawInOrder)
            {
                // do not shuffle!
            }
            else
            {
                GameServer.ClientPlayerState.ShuffleDeck();
            }

            // animate the cards mulligan back to your deck.
            for (var i = 0; i < _mulliganCards.Count; i++)
            {
                _mulliganCards[i].GetCardComponent<GameCardView>().SetCardMoveTarget(null);
                _mulliganCards[i].RectTransform().DOAnchorPos(new Vector2(
                    (cardHolder.rect.width / 2) + (_mulliganCards[i].RectTransform().sizeDelta.x / 2),
                    -(cardHolder.rect.height / 2) - (_mulliganCards[i].RectTransform().sizeDelta.y / 2)),
                    0.5f);

                yield return Timing. WaitForSeconds(0.25f);
            }

            yield return Timing. WaitForSeconds(0.25f);

            // animate the new cards to your hand
            for (var i = 0; i < _mulliganCards.Count; i++)
            {
                _mulliganCards[i].SetInteractable(false);

                var index = _mulliganCards[i].RectTransform().GetSiblingIndex();
                var cardUid =  GameServer.ClientPlayerState.GetHandCard(index);
                var cardState = GameServer.State.GetCard(cardUid);

                _mulliganCards[i].GetCardComponent<GameCardView>().SetCardData(cardState);

                yield return Timing. WaitForSeconds(0.2f);
                while (_mulliganCards[i].loadingData) yield return Timing.WaitForOneFrame;

                var mulliganEffect = _mulliganCards[i].GetVfx<MulliganCrossComponent, MulliganCrossEffect>();
                mulliganEffect.Show(false);

                _mulliganCards[i].GetCardComponent<GameCardView>().SetCardMoveTarget(layout.GetChild(index).RectTransform(), 48);
            }

            yield return Timing. WaitForSeconds(0.5f);

            while (!GameServer.OpponentPlayerState.finishedMulligan ||
                   !GameServer.OpponentPlayerState.finishedDrawingInitialCards)
            {
                Debug.LogError($"Finished Mulligan? {GameServer.OpponentPlayerState.finishedMulligan} Finish Draw? {GameServer.OpponentPlayerState.finishedDrawingInitialCards}");
                instructionsLabel.text =
                    LocalizationController.Localize("MulliganInstructionsWaitingForOpponent", "WAITING FOR OPPONENT...");

                yield return Timing.WaitForOneFrame;
            }

            headerBanner.DOScaleY(0, 0.25f).SetEase(Ease.OutBack);
            headerGlow.DOFade(1, 0.125f);
            backgroundBanner.DOFade(0, 0.125f);
            background.DOFade(0, 0.125f);
            readyButtonCanvas.DOFade(0, 0.125f);

            yield return Timing. WaitForSeconds(0.2f);

            GameplayScreen.Instance.SetupHandView();

            var zoomCardTransformEffects = new List<CardTransformEffect>();

            var createCardsTask = CreateStandardCards();
            yield return Timing.WaitUntilDone(createCardsTask.AsCoroutine());

            for (var i = 0; i < cardHolder.childCount; i++)
            {
                var cardObject = cardHolder.GetChild(i);
                var cardView = cardObject.GetComponent<CardView>();
                if (cardView == null) continue;

                var transformEffect = cardView.GetVfx<CardTransformComponent, CardTransformEffect>();
                Timing.RunCoroutine(transformEffect.TransformFromZoomedCard(createCardsTask.Result[i]));

                zoomCardTransformEffects.Add(transformEffect);

                yield return Timing. WaitForSeconds(0.12f);
            }

            while (zoomCardTransformEffects.Exists(t => !t.IsFinishedTransforming))
            {
                //Debug.LogWarning("Waiting to finish");
                yield return Timing.WaitForOneFrame;
            }


            for (var i = 0; i < createCardsTask.Result.Count; i++)
            {
                yield return Timing.WaitForSeconds(0.08f);
                GameplayScreen.Instance.handView.TrackCard(createCardsTask.Result[i]);
            }

            for (var i = cardHolder.childCount-1; i > -1; i--)
            {
                Destroy(cardHolder.GetChild(i).gameObject);
            }

            for (var i = 0; i < GameplayScreen.Instance.handView.cards.Count; i++)
            {
                var cardView = GameplayScreen.Instance.handView.cards[i];
                cardView.View.SetInteractable(true);
            }

            gameObject.SetActive(false);
        }

        private void SelectedForMulligan(GameCardView gameCardView)
        {
            if (_mulliganCards.Contains(gameCardView.View))
            {
                var mulliganEffect = gameCardView.View.GetVfx<MulliganCrossComponent, MulliganCrossEffect>();
                mulliganEffect.Show(false);
                gameCardView.View.cardOutline.ShowOutline();
                _mulliganCards.Remove(gameCardView.View);
                AudioController.PlaySound("battle_intro_hand_uncross", "BARDSFX", true, gameCardView.gameObject); ;
            }
            else
            {
                var mulliganEffect = gameCardView.View.GetVfx<MulliganCrossComponent, MulliganCrossEffect>();
                mulliganEffect.Show(true);
                gameCardView.View.cardOutline.RemoveOutline();
                _mulliganCards.Add(gameCardView.View);
                AudioController.PlaySound("battle_intro_hand_crossout", "BARDSFX", true, gameCardView.gameObject);
            }
        }

        private void ReadyButtonPressed()
        {
            readyButton.onClick.RemoveListener(ReadyButtonPressed);
            readyButtonCanvas.interactable = false;
            readyButtonCanvas.DOFade(0, 0.25f);
            AudioController.PlaySound("ui_click", "BARDSFX");

            Server.FinishedMulligan();

            GameplayScreen.Instance.handView.MoveToFront();

            _isReady = true;
        }
    }
}
