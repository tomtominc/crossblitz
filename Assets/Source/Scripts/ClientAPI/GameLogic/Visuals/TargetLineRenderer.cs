using System.Collections.Generic;
using Shapes;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class TargetLineRenderer : ImmediateModeShapeDrawer
    {
        public int pixelSize=4;
        public int bezierDensity = 100;
        public float maxRadius = 0.5f;
        public float shadowDistance = 0.016f;
        public float shadingDistance = 0.03f;

        private Color m_shineColor;
        private Color m_shadingColor;
        private Color m_innerColor;
        private Color m_outerColor;
        private float m_radius;

        private void Start()
        {
            m_shineColor = "#fafbef".ToColor();
            m_innerColor = "#e7edb5".ToColor();
            m_outerColor = "#684054".ToColor();
            m_shadingColor = "#cdb684".ToColor();
        }

        public override void DrawShapes( Camera cam )
        {
            using( Draw.Command( cam ) )
            {
                var pixelMultiplier = Screen.width / 360f;
                // set up static parameters. these are used for all following Draw.Line calls
                Draw.LineGeometry = LineGeometry.Flat2D;
                Draw.ThicknessSpace = ThicknessSpace.Pixels;
                Draw.Thickness = pixelSize * pixelMultiplier;
                Draw.LineEndCaps = LineEndCap.None;
                Draw.Matrix = transform.localToWorldMatrix;

                var mousePositionWorldSpace = cam.ScreenToWorldPoint(Input.mousePosition);
                var point = new[] {Vector3.zero, Vector3.zero, mousePositionWorldSpace};
                var perp = point[0].x > point[2].x ?
                    Vector2.Perpendicular(point[0] - point[2]) :
                    Vector2.Perpendicular(point[2] - point[0]);
                var shadowOffset = -perp.normalized * shadowDistance;
                var shadingOffset = -perp.normalized * shadingDistance;
                var diff = Mathf.Abs(point[0].x - point[2].x);

                m_radius = Mathf.Clamp( maxRadius * (diff / 5f), 0, maxRadius);
                point[1] = (point[0] + (point[2] - point[0]) / 2) + (Vector3)perp * m_radius;

                var shinePolyPath = new PolylinePath();
                var innerPolyPath = new PolylinePath();
                var outerPolyPath = new PolylinePath();
                var shadingPolyPath = new PolylinePath();

                if (bezierDensity <= 0) bezierDensity = 10;

                for (var i = 0; i < bezierDensity; i++)
                {
                    var time = i / (float)bezierDensity;
                    var m1 = Vector2.Lerp(point[0], point[1], time);
                    var m2 = Vector2.Lerp(point[1], point[2], time);
                    var lerp = Vector2.Lerp(m1, m2, time);

                    innerPolyPath.AddPoint(lerp);
                    outerPolyPath.AddPoint(lerp + shadowOffset);
                    shadingPolyPath.AddPoint(lerp + shadingOffset);
                }

                Draw.Polyline(outerPolyPath, (pixelSize + 2) * pixelMultiplier, m_outerColor);
                Draw.Polyline(innerPolyPath, pixelSize * pixelMultiplier, m_innerColor );
                Draw.Polyline(shadingPolyPath, (pixelSize-2) * pixelMultiplier, m_shadingColor);
                //Draw.Polyline(innerPolyPath, pixelMultiplier, m_shineColor);

            }

        }
    }
}