using Coffee.UIExtensions;
using UnityEngine;

namespace CrossBlitz.ClientAPI.GameLogic.Visuals
{
    public class CardOutline : MonoBehaviour
    {
        public UIShadow outline;

        public void RemoveOutline()
        {
            gameObject.SetActive(false);
        }

        public void ShowOutline()
        {
            gameObject.SetActive(true);
        }

        public void SetOutline(Color color, float size)
        {
            if (outline)
            {
                outline.effectColor = color;
                outline.effectDistance = new Vector2(size,-size);
            }

            gameObject.SetActive(true);
        }
    }
}
