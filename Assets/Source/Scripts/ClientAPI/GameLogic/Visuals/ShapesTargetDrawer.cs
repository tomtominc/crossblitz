using System;
using Shapes;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class ShapesTargetDrawer : MonoBehaviour
    {
        // this camera component should be disabled,
        // to prevent it from automatically rendering every frame
        //public Camera rtCamera;
        public Camera worldCamera;
        public int pixelSize=4;
        public int bezierDensity = 100;
        public float maxRadius = 0.5f;
        public float shadowDistance = 0.016f;
        public float shadingDistance = 0.03f;

        private Color m_shineColor;
        private Color m_shadingColor;
        private Color m_innerColor;
        private Color m_outerColor;
        private float m_radius;
        private RenderTexture rt;

        void Start()
        {
            m_shineColor = "#fafbef".ToColor();
            m_innerColor = "#e7edb5".ToColor();
            m_outerColor = "#684054".ToColor();
            m_shadingColor = "#cdb684".ToColor();

            // Create an RT
            rt = new RenderTexture(640, 360, 0, RenderTextureFormat.ARGB32);

            RenderTexture.active = rt;
            GL.PushMatrix();
            GL.LoadProjectionMatrix( Matrix4x4.TRS( Vector3.zero, Quaternion.identity, Vector3.one ) );
           // Draw.Disc( Vector2.zero, 0.1f, Color.red );


            //rtCamera.targetTexture = rt;
            //rt.DiscardContents( true, true ); // clear before drawing, in case you have old data in the RT
           // using( Draw.Command( rtCamera ) ) { // set up a Shapes draw command in the camera
               var pixelMultiplier = Screen.width / 360f;
                // set up static parameters. these are used for all following Draw.Line calls
                Draw.LineGeometry = LineGeometry.Flat2D;
                Draw.ThicknessSpace = ThicknessSpace.Pixels;
                Draw.Thickness = 10;// pixelSize * pixelMultiplier;
                Draw.LineEndCaps = LineEndCap.None;
                Draw.Matrix = transform.localToWorldMatrix;

                var mousePositionWorldSpace = new Vector3(20, 20, 0);//worldCamera.ScreenToWorldPoint(Input.mousePosition);
                var point = new[] {Vector3.zero, Vector3.zero, mousePositionWorldSpace};
                var perp = point[0].x > point[2].x ?
                    Vector2.Perpendicular(point[0] - point[2]) :
                    Vector2.Perpendicular(point[2] - point[0]);
                var shadowOffset = -perp.normalized * shadowDistance;
                var shadingOffset = -perp.normalized * shadingDistance;
                var diff = Mathf.Abs(point[0].x - point[2].x);

                m_radius = Mathf.Clamp( maxRadius * (diff / 5f), 0, maxRadius);
                point[1] = (point[0] + (point[2] - point[0]) / 2) + (Vector3)perp * m_radius;

                var shinePolyPath = new PolylinePath();
                var innerPolyPath = new PolylinePath();
                var outerPolyPath = new PolylinePath();
                var shadingPolyPath = new PolylinePath();

                if (bezierDensity <= 0) bezierDensity = 10;

                for (var i = 0; i < bezierDensity; i++)
                {
                    var time = i / (float)bezierDensity;
                    var m1 = Vector2.Lerp(point[0], point[1], time);
                    var m2 = Vector2.Lerp(point[1], point[2], time);
                    var lerp = Vector2.Lerp(m1, m2, time);

                    innerPolyPath.AddPoint(lerp);
                    outerPolyPath.AddPoint(lerp + shadowOffset);
                    shadingPolyPath.AddPoint(lerp + shadingOffset);
                }

                Draw.Polyline(outerPolyPath, (pixelSize + 2) * pixelMultiplier, m_outerColor);
                Draw.Polyline(innerPolyPath, pixelSize * pixelMultiplier, m_innerColor );
                Draw.Polyline(shadingPolyPath, (pixelSize-2) * pixelMultiplier, m_shadingColor);
           // }

           // rtCamera.Render(); // Render the camera immediately// make sure the camera is configured to target the RT
           GL.PopMatrix();
        }

        private void Update()
        {
            // rt.DiscardContents( true, true ); // clear before drawing, in case you have old data in the RT
            // using( Draw.Command( rtCamera ) ) { // set up a Shapes draw command in the camera
            //    var pixelMultiplier = Screen.width / 360f;
            //     // set up static parameters. these are used for all following Draw.Line calls
            //     Draw.LineGeometry = LineGeometry.Flat2D;
            //     Draw.ThicknessSpace = ThicknessSpace.Pixels;
            //     Draw.Thickness = pixelSize * pixelMultiplier;
            //     Draw.LineEndCaps = LineEndCap.None;
            //     Draw.Matrix = transform.localToWorldMatrix;
            //
            //     var mousePositionWorldSpace = worldCamera.ScreenToWorldPoint(Input.mousePosition);
            //     var point = new[] {Vector3.zero, Vector3.zero, mousePositionWorldSpace};
            //     var perp = point[0].x > point[2].x ?
            //         Vector2.Perpendicular(point[0] - point[2]) :
            //         Vector2.Perpendicular(point[2] - point[0]);
            //     var shadowOffset = -perp.normalized * shadowDistance;
            //     var shadingOffset = -perp.normalized * shadingDistance;
            //     var diff = Mathf.Abs(point[0].x - point[2].x);
            //
            //     m_radius = Mathf.Clamp( maxRadius * (diff / 5f), 0, maxRadius);
            //     point[1] = (point[0] + (point[2] - point[0]) / 2) + (Vector3)perp * m_radius;
            //
            //     var shinePolyPath = new PolylinePath();
            //     var innerPolyPath = new PolylinePath();
            //     var outerPolyPath = new PolylinePath();
            //     var shadingPolyPath = new PolylinePath();
            //
            //     if (bezierDensity <= 0) bezierDensity = 10;
            //
            //     for (var i = 0; i < bezierDensity; i++)
            //     {
            //         var time = i / (float)bezierDensity;
            //         var m1 = Vector2.Lerp(point[0], point[1], time);
            //         var m2 = Vector2.Lerp(point[1], point[2], time);
            //         var lerp = Vector2.Lerp(m1, m2, time);
            //
            //         innerPolyPath.AddPoint(lerp);
            //         outerPolyPath.AddPoint(lerp + shadowOffset);
            //         shadingPolyPath.AddPoint(lerp + shadingOffset);
            //     }
            //
            //     Draw.Polyline(outerPolyPath, (pixelSize + 2) * pixelMultiplier, m_outerColor);
            //     Draw.Polyline(innerPolyPath, pixelSize * pixelMultiplier, m_innerColor );
            //     Draw.Polyline(shadingPolyPath, (pixelSize-2) * pixelMultiplier, m_shadingColor);
            // }
            //
            // rtCamera.Render(); // Render the camera immediately
        }

        private void OnDestroy()
        {
            DestroyImmediate(rt);
        }
    }
}