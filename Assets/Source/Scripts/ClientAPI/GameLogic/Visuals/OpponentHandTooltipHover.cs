using CrossBlitz.Utils;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class OpponentHandTooltipHover : MonoBehaviour,
        IPointerEnterHandler, IPointerExitHandler, ICursorSelectable
    {
        public OpponentHandTooltip tooltip;

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (GameplayScreen.Instance != null &&
                GameplayScreen.Instance.ScreenState == GameplayScreen.ScreenStateType.NormalUpdate)
            {
                tooltip.Open();
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            tooltip.Close();
        }

        // changes the cursor to hand click
        public bool Interactable => false;
        // changes the cursor to grab
        public bool Grabbable => false;
        // changes the cursor to magnifying glass
        public bool InfoOnly => true;
        // changes the cursor to loading
        public bool Loading => false;
    }
}