using System.Collections.Generic;
using Source.Scripts.Card;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class GameEffectsManager : MonoBehaviour
    {
        public enum VFX
        {
            UseCard,
            SpawnMinion,
            HeroDamaged,
            HeroHpDamaged,
        }

        public static GameEffectsManager Instance;

        public RectTransform effectsParent;
        public List<CardEffect> effectsPrefabs;
        public Dictionary<VFX,Stack< CardEffect >> pool;

        private List<CardEffect> _used;

        private void Awake()
        {
            Instance = this;

            pool = new Dictionary<VFX, Stack<CardEffect>>();

            for (var i = 0; i < effectsPrefabs.Count; i++)
            {
                var effect = Instantiate(effectsPrefabs[i], effectsParent, false);

                if (!pool.ContainsKey(effect.type))
                {
                    pool.Add(effect.type, new Stack<CardEffect>());
                }

                pool[effect.type].Push(effect);
                effect.SetActive(false);
            }
        }

        public CardEffect GetEffect(VFX type)
        {
            CardEffect effect;

            if (pool.ContainsKey(type) && pool[type].Count > 0)
            {
                effect = pool[type].Pop();
            }
            else if (pool.ContainsKey(type))
            {
                effect = Instantiate(effectsPrefabs.Find(x=>x.type == type),
                    effectsParent, false);

                pool[type].Push(effect);
            }
            else
            {
                effect = Instantiate(effectsPrefabs.Find(x=>x.type == type),
                    effectsParent, false);

                pool.Add(effect.type, new Stack<CardEffect>());
                pool[type].Push(effect);
            }

            effect.SetActive(true);

            return effect;
        }

        public void RemoveEffect(CardEffect effect)
        {
            if (!pool.ContainsKey(effect.type))
            {
                pool.Add(effect.type, new Stack<CardEffect>());
            }

            pool[effect.type].Push(effect);
            effect.SetActive(false);
            effect.transform.SetParent(effectsParent, false);
        }
    }
}
