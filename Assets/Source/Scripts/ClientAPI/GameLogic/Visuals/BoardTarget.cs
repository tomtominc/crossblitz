using System;
using System.Collections.Generic;
using Asyncoroutine;
using BlendModes;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.GameVfx;
using CrossBlitz.Hero;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using DG.Tweening;
using MEC;
using Source.Scripts.Card;
using UnityEngine;
using UnityEngine.EventSystems;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class BoardTarget : MonoBehaviour,
        IDropHandler,
        IPointerClickHandler,
        ICardComponent,
        IPointerEnterHandler,
        IPointerExitHandler
    {
        public enum ShowMode
        {
            None,
            Drop,
            Select
        }

        public Canvas canvas;
        public RectTransform targetRect;
        public CardOutline outline;
        public RectTransform shadow;

        private Tweener _showAnimation;
        private Tweener _shadowAnimation;
        private RectTransform _rect;
        private int _previousSortOrder;
        private GameTile _gameTile;
        private CardView _cardView;
        private HeroViewBattle _heroView;
        private float _shadowNormalDistance;
        private float _shadowShowDistance;

        public ShowMode _showMode;

        public event Action<BoardTarget> OnShowTarget;
        public event Action<BoardTarget> OnHideTarget;
        public event Action<BoardTarget> OnSelectedTarget;
        public event Action<BoardTarget> OnCardEnterActiveBoardTarget;
        public event Action<BoardTarget> OnCardExitActiveBoardTarget;
        public GameTile GameTile => _gameTile;
        public CardView CardView => _cardView;
        public HeroViewBattle HeroView => _heroView;
        public ShowMode showMode => _showMode;

        private bool m_updateTargetGlow;

        private int m_glowIndex;
        private float m_glowDelay;
        private BlendMode m_glowBlend = BlendMode.Overlay;
        private const int GlowAlpha = 160;

        private static readonly List<Color32> m_targetGlows = new List<Color32>
        {
            new Color32(243,199,37, GlowAlpha),
            new Color32(243,199,37, GlowAlpha),
            new Color32(243,199,37, GlowAlpha),
            new Color32(243,199,37, GlowAlpha),

            new Color32(240,147,40, GlowAlpha),
            new Color32(239,114,43, GlowAlpha),
            new Color32(237,79,45, GlowAlpha),

            new Color32(235,59,47, GlowAlpha),
            new Color32(235,59,47, GlowAlpha),
            new Color32(235,59,47, GlowAlpha),
            new Color32(235,59,47, GlowAlpha),

            new Color32(237,79,45, GlowAlpha),
            new Color32(239,114,43, GlowAlpha),
            new Color32(240,147,40, GlowAlpha),
        };

        public Vector2Int Position
        {
            get
            {
                if (GameTile != null) return GameTile.Position;
                if (CardView != null) return CardView.GetCardComponent<GameCardView>().TileState.Position;
                if (HeroView != null) return new Vector2Int(-1,-1);

                Debug.LogError("Trying to get a position but GameTile and GameCardView are null!");
                return new Vector2Int(-1,-1);
            }
        }

        public void Initialize(CardView card)
        {
            _cardView = card;
            _shadowNormalDistance = 2;
            _shadowShowDistance = 4;

            canvas = _cardView.Canvas;
            targetRect = _cardView.Rect;
            outline = _cardView.cardOutline;
            shadow = _cardView.frontShadowRect;

            _rect = targetRect;
            shadow.anchoredPosition = new Vector2(_shadowNormalDistance, -_shadowNormalDistance);
        }

        public void Initialize(HeroViewBattle heroView)
        {
            _heroView = heroView;
        }

        public void OnAfterCreation()
        {

        }

        private void Awake()
        {
            _rect = targetRect;
            _gameTile = GetComponent<GameTile>();

            if (_gameTile != null)
            {
                _shadowNormalDistance = 2;
                _shadowShowDistance = 8;
                if (shadow) shadow.anchoredPosition = new Vector2(_shadowNormalDistance, -_shadowNormalDistance);
            }
        }

        private void Update()
        {
            // if (Input.GetKeyDown(KeyCode.Alpha1))
            // {
            //     m_glowBlend = BlendMode.Multiply;
            // }
            // if (Input.GetKeyDown(KeyCode.Alpha2))
            // {
            //     m_glowBlend = BlendMode.Overlay;
            // }
            // if (Input.GetKeyDown(KeyCode.Alpha3))
            // {
            //     m_glowBlend = BlendMode.Screen;
            // }
            // if (Input.GetKeyDown(KeyCode.Alpha4))
            // {
            //     m_glowBlend = BlendMode.VividLight;
            // }
            // if (Input.GetKeyDown(KeyCode.Alpha5))
            // {
            //     m_glowBlend = BlendMode.SoftLight;
            // }

            // if (m_updateTargetGlow && _cardView)
            // {
            //     m_glowDelay -= Time.deltaTime;
            //
            //     if (m_glowDelay <= 0)
            //     {
            //         _cardView.Tint(m_targetGlows[m_glowIndex], m_glowBlend);
            //
            //         m_glowIndex++;
            //         m_glowDelay = 0.06f;
            //
            //         if (m_glowIndex >= m_targetGlows.Count)
            //         {
            //             m_glowIndex = 0;
            //         }
            //     }
            // }
        }

        public void OnDrop(PointerEventData eventData)
        {
            // we aren't allowing any drops on this object because the show mode is none or the show mode is select which uses the pointer down method.
            if (_showMode == ShowMode.None || _showMode == ShowMode.Select)
            {
                return;
            }

            // We need to check multiple things about this event data to make sure we are actually dropping a card.
            if (!eventData.dragging || eventData.pointerDrag == null )
            {
                Debug.Log($"Something went wrong: {eventData.dragging}, {eventData.pointerDrag}, {eventData.selectedObject}");
                return;
            }

            if (eventData.pointerDrag == null)
            {
                return;
            }

            // we grab the card so we can use it in the drop event.
            var card = eventData.pointerDrag.GetComponent<GameCardView>();

            // check if our card is null
            if (card == null)
            {
                Debug.LogError("Card is null!");
                return;
            }

            OnCardExitActiveBoardTarget?.Invoke(this);
            Events.Publish(this, EventType.OnCardExitActiveBoardTarget, new OnCardExitActiveBoardTargetEventArgs { cardUid = card.State.uid });

            // The drop method needs to stop and wait in certain cases so we use a coroutine.
            Timing.RunCoroutine(OnDrop(card));
        }

        private IEnumerator<float> OnDrop(GameCardView card)
        {
            switch (card.State.data.type)
            {
                case CardType.Trick:
                case CardType.Minion:
                {
                    GameplayScreen.Instance.HideTargets();

                    var shouldSelectTarget = card.State.ShouldSelectTarget(_gameTile!=null?_gameTile.State:null);
                    var summonAnimation = Timing.RunCoroutine(card.AnimateSummon(_gameTile, SummonCommand.Method.PlayedFromHand, shouldSelectTarget? GameplayScreen.BoardTargetSortOrder+1 : -1 ));
                    var selection = Vector2Int.zero;
                    var selectedUid = string.Empty;

                    if (shouldSelectTarget)
                    {
                        card.SetGameState(GameCardView.GameStateType.WaitForSelection);
                        GameplayScreen.Instance.SetScreenState(GameplayScreen.ScreenStateType.HandlingSelection);

                        var getTargetingEffect = GameVfxProvider.GetWorldVfx("TargetingEffect");
                        yield return Timing.WaitUntilDone(getTargetingEffect.AsCoroutine());
                        var targetingEffect = (TargetingEffect)getTargetingEffect.Result;
                        targetingEffect.Show( _gameTile.cardContainer, true, Vector3.zero );

                        while (summonAnimation.IsRunning)
                        {
                            yield return Timing.WaitForOneFrame;
                        }

                        var getBoardCardTask = GameBoard.GetBoardCard(card.State.uid);
                        yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                        var getBoardCardResult = getBoardCardTask.Result;
                        var boardCard = getBoardCardResult.boardCard;

                        // var getTargetEffect = boardCard.GetVfx<TargetingEffectComponent>();
                        // yield return Timing.WaitUntilDone( getTargetEffect.AsCoroutine() );
                        //
                        // var targetEffect = (TargetingEffect)getTargetEffect.Result.Effect;
                        // targetEffect.Show();

                        // Wait for the player to select a target
                        yield return Timing.WaitUntilDone(GameplayScreen.Instance.HandleSelectingTarget(card.State, _gameTile!=null?_gameTile.State:null, targetingEffect));

                        targetingEffect.Hide();
                        //targetEffect.Hide();

                        if (!GameplayScreen.Instance.SelectedBoardTarget)
                        {
                            card.transform.position = boardCard.transform.position;
                            card.View.SetAlpha(0);
                            card.View.cardObject.SetActive(true);
                            card.transform.localScale = Vector3.one;
                            card.View.RemoveTint();

                            yield return Timing.WaitUntilDone(boardCard.TransformToStandardCardViewFromBoardCard(card.View));

                            card.SetCardMoveTarget(null, -1, false);
                            card.OnCanceledUsageDrag();

                            // destroy the board card that was summoned.
                            Destroy(boardCard.gameObject);

                            yield break;
                        }

                        var selectedBoardTarget = GameplayScreen.Instance.SelectedBoardTarget;
                        selection = selectedBoardTarget.Position;

                        if (selection.x < 0 && selection.y < 0)
                        {
                            selectedUid = selectedBoardTarget.HeroView.State.uid;
                        }

                        GameplayScreen.Instance.SetBoardTarget(null);
                    }

                    var command = new SummonCommand
                    {
                        PlayerUid = GameServer.ClientPlayerState.Uid,
                        SummonMethod = SummonCommand.Method.PlayedFromHand,
                        Position = _gameTile.Position,
                        SourceUid = card.State.uid,
                        Selection = selection,
                        SelectedHeroUid = selectedUid
                    };

                    global::GameLogic.StartCommand(command);
                    Events.Publish(this, EventType.OnCardPlayed, new OnCardPlayedEventArgs{ isClientCard = card.State.Team == Team.Client, cardUid = card.State.uid });

                    //global::GameLogic.ResolveTriggeredCards(card.State, null, TriggerType.PLAYED_CARD);
                    GameplayScreen.Instance.handView.RemoveCard(card,_gameTile.cardContainer.transform);

                    // when we selected a target we didn't update the state properly, so lets do that now.
                    if (!shouldSelectTarget)
                    {
                        yield break;
                    }

                    {
                        var getBoardCardTask = GameBoard.GetBoardCard(card.State.uid);
                        yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                        var getBoardCardResult = getBoardCardTask.Result;

                        if (getBoardCardResult.error != ClientErrorCode.None)
                        {
                            Server.HandleClientError(getBoardCardResult, true);
                        }

                        if (getBoardCardResult.boardCard)
                        {
                            getBoardCardResult.boardCard.OnSummonedOnBoard( SummonCommand.Method.PlayedFromHand );
                            getBoardCardResult.boardCard.UpdateViewFromState();
                        }
                    }
                    yield break;
                }
                case CardType.Spell:
                {
                    if (card.State.GetAbilities().Count <= 0)
                    {
                        GameplayScreen.Instance.HideTargets();
                        Debug.LogError($"Could not play spell {card.State.data.name}, because it has no abilities.");
                        yield break;
                    }

                    var command = new PlayCommand
                    {
                        PlayerUid = GameServer.ClientPlayerState.Uid,
                        SourceUid = card.State.uid
                    };

                    var shouldSelectTarget = card.State.ShouldSelectTarget(null);

                    // we are playing this card on top of a card right now! So this card becomes the target of this spell
                    if (shouldSelectTarget)
                    {
                        if (_cardView != null)
                        {
                            var gameCardView = _cardView.GetCardComponent<GameCardView>();

                            if (gameCardView  == null) yield break;

                            command.Selection = gameCardView.State;
                        }
                        else if (_heroView != null)
                        {
                            command.Selection = _heroView.State;
                        }

                        if (command.Selection == null)
                        {
                            yield break;
                        }
                    }

                    global::GameLogic.StartCommand(command);

                    Events.Publish(this, EventType.OnCardPlayed, new OnCardPlayedEventArgs{ isClientCard = card.State.Team == Team.Client,cardUid = card.State.uid });

                    //global::GameLogic.ResolveTriggeredCards(card.State, null, TriggerType.PLAYED_CARD);

                    GameplayScreen.Instance.handView.RemoveCard(card,transform);

                    Timing.RunCoroutine(card.AnimateUseSpell());

                    yield break;
                }
            }
        }

        public void Show(ShowMode sm)
        {
            _showMode = sm;

            TweenUtility.Kill(_showAnimation, false);
            TweenUtility.Kill(_shadowAnimation, false);

            _previousSortOrder = canvas.sortingOrder;
            canvas.sortingOrder = GameplayScreen.BoardTargetSortOrder;

            if (outline)
            {
                outline.SetActive(true);
            }
            if (shadow)
            {
                shadow.SetActive(true);
            }

            _showAnimation = _rect.DOAnchorPosY(2, 0.12f);

            if (shadow)
            {
                _shadowAnimation = shadow.DOAnchorPos(new Vector2(_shadowShowDistance, -_shadowShowDistance), 0.12f);
            }

            OnShowTarget?.Invoke(this);
        }

        public void Hide()
        {
            _showMode = ShowMode.None;

            TweenUtility.Kill(_showAnimation, false);
            TweenUtility.Kill(_shadowAnimation, false);

            _rect.localScale = new Vector3(1, 1, 1);

            canvas.sortingOrder = _previousSortOrder;

            if (outline)
            {
                outline.SetActive(false);
            }

            if (shadow)
            {
                shadow.SetActive(false);
            }

            if (_cardView)
            {
                m_updateTargetGlow = false;
                _cardView.RemoveTint();
            }

            _showAnimation = _rect.DOAnchorPosY(0, 0.24f).SetEase(Ease.OutBounce);

            if (shadow)
            {
                _shadowAnimation =
                    shadow.DOAnchorPos(new Vector2(_shadowNormalDistance, -_shadowNormalDistance), 0.12f);
            }

            OnHideTarget?.Invoke(this);
        }

        public void AnimateClickAndHide()
        {
            _showMode = ShowMode.None;

            TweenUtility.Kill(_showAnimation, false);
            TweenUtility.Kill(_shadowAnimation, false);

            canvas.sortingOrder = _previousSortOrder;

            // _showAnimation = _rect.DOPunchScale(Vector3.one * 0.24f, 0.24f)
            //     .OnComplete(() =>
            //     {
            //         if (outline) outline.SetActive(false);
            //         _showAnimation = _rect.DOAnchorPosY(0, 0.24f).SetEase(Ease.OutBounce);
            //         if (shadow) _shadowAnimation = shadow.DOAnchorPos(new Vector2(_shadowNormalDistance, -_shadowNormalDistance), 0.12f);
            //
            //     });

            if (outline) outline.SetActive(false);
            _showAnimation = _rect.DOAnchorPosY(0, 0.24f).SetEase(Ease.OutBounce);
            if (shadow) _shadowAnimation = shadow.DOAnchorPos(new Vector2(_shadowNormalDistance, -_shadowNormalDistance), 0.12f);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (_heroView != null && _heroView.State != null)
            {
                GameplayScreen.Instance.simpleCardHistory.Open(_heroView.State);
            }

            // we aren't allowing any drops on this object because the show mode is none or the show mode is select which uses the pointer down method.
            if (_showMode == ShowMode.None || _showMode == ShowMode.Drop) return;

            // we need to make sure we're clicking with our left mouse
            if (eventData.button == PointerEventData.InputButton.Middle ||
                eventData.button == PointerEventData.InputButton.Right) return;

            // we just send an event to whoever is listening that we were clicked.
            OnSelectedTarget?.Invoke(this);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (_showMode == ShowMode.None)
            {
                return;
            }

            if (outline)
            {
                outline.SetOutline(Color.white, 2);
            }

            if (_cardView)
            {
                m_updateTargetGlow = true;
            }

            if (eventData.pointerDrag == null)
            {
                return;
            }

            var card = eventData.pointerDrag.GetComponent<GameCardView>();

            if (card == null)
            {
                return;
            }

            OnCardEnterActiveBoardTarget?.Invoke(this);

            Events.Publish(this, EventType.OnCardEnteredActiveBoardTarget, new OnCardEnteredActiveBoardTargetEventArgs { cardUid = card.State.uid });
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (_showMode == ShowMode.None)
            {
                return;
            }

            if (outline)
            {
                outline.SetOutline(Color.white, 1);
            }

            if (_cardView)
            {
                m_updateTargetGlow = false;
                _cardView.RemoveTint();
            }

            OnCardExitActiveBoardTarget?.Invoke(this);

            if (eventData.pointerDrag == null)
            {
                return;
            }

            var card = eventData.pointerDrag.GetComponent<GameCardView>();

            if (card == null) return;

            Events.Publish(this, EventType.OnCardExitActiveBoardTarget, new OnCardExitActiveBoardTargetEventArgs { cardUid = card.State.uid });
        }
    }
}
