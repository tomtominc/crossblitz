using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Utils;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class OpponentKnownCardPageItem : PageItem
    {

        public Vector2 animateFromOffset = new Vector2(20, 0);
        public CanvasGroup canvasGroup;
        public OpponentKnownCardItem opponentKnownCardItem;

        public override void SetAsEmpty(int itemIndex)
        {
            canvasGroup.alpha = 0;
        }

        public override void SetData(IPageItemData data, int itemIndex)
        {
            canvasGroup.DOKill();
            opponentKnownCardItem.RectTransform().DOKill();

            canvasGroup.alpha = 0;

            if (data is OpponentKnownCardPageData ingData)
            {
                var card = Db.CardDatabase.GetCard(ingData.cardValueId);
                opponentKnownCardItem.SetCard(card, ingData.cardCount);
            }

            opponentKnownCardItem.RectTransform().anchoredPosition = animateFromOffset;
            canvasGroup.DOFade(1, 0.12f).SetDelay(itemIndex * 0.06f);
            opponentKnownCardItem.RectTransform().DOAnchorPos(Vector2.zero, 0.24f).SetEase(Ease.OutBack).SetDelay(itemIndex * 0.06f);
        }
    }

    public class OpponentKnownCardPageData : IPageItemData
    {
        public string cardValueId;
        public int cardCount;
    }
}