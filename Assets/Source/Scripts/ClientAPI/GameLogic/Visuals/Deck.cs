using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Data;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.ServerAPI.GameLogic;
using DG.Tweening;
using GameDataEditor;
using Sirenix.OdinInspector;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class Deck : MonoBehaviour
    {
        [BoxGroup("Deck")] public Team team;
        [BoxGroup("Deck")] public RectTransform topCardContainer;
        [BoxGroup("Deck")] public SpriteAnimation card;
        [BoxGroup("Fatigue")] public RectTransform fatigueContainer;
        [BoxGroup("Fatigue")] public TextMeshProUGUI fatigueCounter;
        [BoxGroup("Fatigue")] public SpriteAnimation fatigueAnimator;


        private const float toolTipHeight = 84;
        private const float toolTipFatigueHeight = 107;
        [BoxGroup("Tooltip")] public RectTransform tooltipContainer;
        [BoxGroup("Tooltip")] public TextMeshProUGUI handTooltipLabel;
        [BoxGroup("Tooltip")] public GameObject fullHandLabel;
        [BoxGroup("Tooltip")] public TextMeshProUGUI deckTooltipLabel;
        [BoxGroup("Tooltip")] public TextMeshProUGUI exhaustTooltipLabel;
        [BoxGroup("Tooltip")] public GameObject exhaustItem;

        private string m_cardBackPortrait;

        public void Start()
        {
            MoveOutOfScreen(0f);
            RefreshDeckList();
        }

        public GameObject GetTopCard()
        {
            // if (cardsPile.Cards == null || cardsPile.Cards.Count <= 0) return gameObject;
            // return cardsPile.Cards[cardsPile.Cards.Count - 1];
            return topCardContainer.gameObject;
        }

        public void MoveOutOfScreen(float duration)
        {
            // var newPos = offScreenTarget.anchoredPosition;
            //
            // if (duration <= 0)
            // {
            //     this.RectTransform().anchoredPosition = newPos;
            // }
            // else
            // {
            //     this.RectTransform().DOKill();
            //     this.RectTransform().DOAnchorPos(newPos, duration).SetEase(Ease.InBack);
            // }
        }

        public void MoveOnScreen(float duration)
        {
            // var newPos = onScreenTarget.anchoredPosition;
            //
            // if (duration <= 0)
            // {
            //     this.RectTransform().anchoredPosition = newPos;
            // }
            // else
            // {
            //     this.RectTransform().DOKill();
            //     this.RectTransform().DOAnchorPos(newPos, duration).SetEase(Ease.InBack);
            // }
        }

        public void PunchShake()
        {
            this.RectTransform().DOPunchAnchorPos(Vector2.down * 10f, 0.4f);
        }

        private void Update()
        {
            if (Time.frameCount % 60 == 0)
            {
                RefreshDeckList();
            }
        }

        public void RefreshDeckList()
        {
            var playerState = GameServer.GetPlayerState(team);

            RefreshTooltip(playerState);

            if (playerState.GetDeckCount() <= 0)
            {
                card.SetActive(false);
                fatigueContainer.SetActive(true);

                if (fatigueAnimator.CurrentAnimationName != "idle")
                {
                    fatigueAnimator.Play("idle");
                }

                fatigueCounter.text = $"{playerState.fatigue}";
            }
            else
            {
                if (string.IsNullOrEmpty(m_cardBackPortrait))
                {
                    var cardBack = Db.CardBackDatabase.GetCardBackData(playerState.cardBackUid);
                    m_cardBackPortrait = cardBack.portraitUrl;
                }

                if (card.CurrentAnimationName != m_cardBackPortrait)
                {
                    card.Play(m_cardBackPortrait);
                }

                fatigueContainer.SetActive(false);
            }

            // if (cardsPile.Cards.Count < playerState.GetDeckCount())
            // {
            //     var diff = playerState.GetDeckCount()-cardsPile.Cards.Count;
            //
            //     for (var i = 0; i < diff; i++)
            //     {
            //         var card = Instantiate(cardPrefab, cardsPile.transform, false);
            //         var cardBack = new GDECardBackData(playerState.cardBack);
            //         card.Play(cardBack.PortraitUrl);
            //         cardsPile.Add(card.gameObject, false);
            //     }
            // }
            // else if (cardsPile.Cards.Count > playerState.GetDeckCount())
            // {
            //     var diff = cardsPile.Cards.Count - playerState.GetDeckCount();
            //
            //     for (var i = 0; i < diff; i++)
            //     {
            //         var card = cardsPile.RemoveAt(cardsPile.Cards.Count-1);
            //         if (card) Destroy(card);
            //     }
            // }
        }

        private void RefreshTooltip(PlayerBattleState playerState)
        {
            handTooltipLabel.text = $"<font=\"HopeGold-Outlined-Brown\"><color=#ffffff>{playerState.GetHandCount()}</color></font> <voffset=2><space=1>CARDS IN HAND.";
            fullHandLabel.SetActive(playerState.GetHandCount() >= 10);

            var deckCount = playerState.GetDeckCount();
            if (deckCount <= 0) // fatigue
            {
                tooltipContainer.sizeDelta = new Vector2(168, toolTipFatigueHeight);
                deckTooltipLabel.text =
                    $"<color=#f1504f><font=\"HopeGold-Outlined-Brown\">{deckCount}</font> <voffset=2><space=1>CARDS IN DECK.";
                exhaustItem.SetActive(true);
                exhaustTooltipLabel.text =$"<font=\"HopeGold-Outlined-Brown\"><color=#f1504f>{playerState.fatigue}</color></font> <voffset=2><space=1>damage next draw.</font> <color=#E0443E>";
            }
            else
            {
                tooltipContainer.sizeDelta = new Vector2(168, toolTipHeight);
                deckTooltipLabel.text = $"<font=\"HopeGold-Outlined-Brown\"><color=#ffffff>{deckCount}</color></font> <voffset=2><space=1>CARDS IN DECK.";
                exhaustItem.SetActive(false);
            }
        }
    }
}