using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asyncoroutine;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class OpponentHandCard : MonoBehaviour
    {
        public CanvasGroup canvasGroup;
        public SpriteAnimation backAnimator;
        public RectTransform cardParent;
        public SpriteAnimation discardAnimator;

        public string cardUid;
        public string CardUid => cardUid;

        private GameCardView cardView;
        public GameCardView Card => cardView;

        public event Action<OpponentHandCard> OnDiscarded;

        public void SetCard(string uid, string backAnimation)
        {
            cardUid = uid;
            backAnimator.Play(backAnimation);
            discardAnimator.SetActive(false);
            canvasGroup.alpha = 1;
        }

        public IEnumerator<float> Discard(bool disableDiscardEffect = false, bool handleOnDiscardCallback = false)
        {
            if (!GameplayScreen.Instance.opponentHandView.IsShowing)
            {
                GameplayScreen.Instance.opponentHandView.Show();
            }

            var card = GameServer.State.GetCard(cardUid);

            if (cardView == null)
            {
                var cardViewTask = GetCard(card);
                yield return Timing.WaitUntilDone(cardViewTask.AsCoroutine());
                cardView = cardViewTask.Result;
            }

            while (GameplayScreen.Instance.opponentHandView.IsTransitioning) yield return Timing.WaitForOneFrame;

            yield return Timing.WaitUntilDone(backAnimator.RectTransform().DOAnchorPosY(-110, 0.24f).SetEase(Ease.OutBack).WaitForCompletion(true));

            cardView.Rect.anchoredPosition = new Vector2(cardView.Rect.anchoredPosition.x, -110);

            DiscardEffectComponent discardEffect = null;

            if (!disableDiscardEffect)
            {
                var discardEffectTask = cardView.GetVfx<DiscardEffectComponent>();
                yield return Timing.WaitUntilDone(discardEffectTask.AsCoroutine());
                discardEffect = discardEffectTask.Result;
            }

            // I need to "flip" the card over, revealing it to the opponent, then do the effect.
            // The card should be shown in the preview view, with correct values!

            yield return Timing.WaitUntilDone(Flip(false) );

            if (discardEffect)
            {
                yield return Timing.WaitUntilDone(((DiscardEffect)discardEffect.Effect).Play(null, null));
            }

            if (card.location == CardLocation.Hand)
            {
                yield return Timing.WaitUntilDone( Flip(true) );

                cardView.Rect.anchoredPosition = new Vector2(cardView.Rect.anchoredPosition.x, 0);

                yield return Timing.WaitUntilDone(backAnimator.RectTransform().DOAnchorPosY(0, 0.24f)
                    .SetEase(Ease.InBack)
                    .WaitForCompletion(true));
            }
            else if (!handleOnDiscardCallback)
            {
                OnDiscard();
            }
        }

        public IEnumerator<float> ModifyHealthAndPower(int modifyPowerAmount, int modifyHealthAmount, bool isHealing = false)
        {
            if (cardView == null)
            {
                yield break;
            }

            cardView.UpdateViewFromState(true);

            var getModifyEffectTask = cardView.GetVfx<ModifyHealthAndPowerEffectComponent>();
            yield return Timing.WaitUntilDone(getModifyEffectTask.AsCoroutine());
            var modifyEffect = getModifyEffectTask.Result;
            yield return Timing.WaitUntilDone(((ModifyHealthAndPowerEffect)modifyEffect.Effect).PlayModifyAnimation(modifyPowerAmount, modifyHealthAmount, isHealing));
        }

        public async Task<GameCardView> GetCard(GameCardState cardState)
        {
            if (cardView != null)
            {
                return cardView;
            }

            var standardPrefab = await AddressableReferenceLoader.GetAsset("Card_Standard", true);

            // Get the card this has or spawn one if it doesn't have it.
            // Opponent cards don't spawn cards by default (only the card back) you only need one for certain abilities or effects.
            // Usually this happens with Flux or Focus cards.

            var standardCard = CardFactory.Create(new CreateCardFactorySettings
            {
                CardPrefab = standardPrefab,
                CardState = cardState,
                Layout = this.RectTransform(),
                StartsDisabled = true,
                AdditionalComponents = CardView.GetRequiredComponentsForStandardHandCard(),
            }, null);

            var gameCard = standardCard.GetCardComponent<GameCardView>();

            if (gameCard.State.HasTrigger(TriggerType.FOCUS))
            {
                await gameCard.GetVfx<FocusEffectComponent>();
            }

            return gameCard;
        }

        public IEnumerator<float> PlayFocusEffect(bool isComplete)
        {
            if (!GameplayScreen.Instance.opponentHandView.IsShowing)
            {
                GameplayScreen.Instance.opponentHandView.Show();
            }

            if (cardView == null)
            {
                var cardViewTask = GetCard(GameServer.State.GetCard(cardUid));
                yield return Timing.WaitUntilDone(cardViewTask.AsCoroutine());
                cardView = cardViewTask.Result;
            }

            while (GameplayScreen.Instance.opponentHandView.IsTransitioning) yield return Timing.WaitForOneFrame;

            yield return Timing.WaitUntilDone(backAnimator.RectTransform().DOAnchorPosY(-110, 0.24f)
                .SetEase(Ease.OutBack)
                .WaitForCompletion(true));

            cardView.Rect.anchoredPosition = new Vector2(cardView.Rect.anchoredPosition.x, -110);

            var focusEffectTask =cardView.GetVfx<FocusEffectComponent>();
            yield return Timing.WaitUntilDone(focusEffectTask.AsCoroutine());
            var focusEffect = focusEffectTask.Result;

            // I need to "flip" the card over, revealing it to the opponent, then do the effect.
            // The card should be shown in the preview view, with correct values!

            yield return Timing.WaitUntilDone(Flip(false) );

            if (isComplete)
            {
                yield return Timing.WaitUntilDone( ((FocusEffect)focusEffect.Effect).CompleteEffect() );
            }
            else
            {
                yield return Timing.WaitUntilDone( ((FocusEffect)focusEffect.Effect).SingleEffect() );
            }

            yield return Timing.WaitUntilDone( Flip(true) );

            cardView.Rect.anchoredPosition = new Vector2(cardView.Rect.anchoredPosition.x, 0);

            yield return Timing.WaitUntilDone(backAnimator.RectTransform().DOAnchorPosY(0, 0.24f)
                .SetEase(Ease.InBack)
                .WaitForCompletion(true));


        }

        private IEnumerator<float> Flip(bool toBack)
        {
            var rectFlip = toBack ? cardView.Rect : backAnimator.RectTransform();
            var rectNew = toBack ? backAnimator.RectTransform() : cardView.Rect;

            rectFlip.localScale = Vector3.one;

            yield return Timing.WaitUntilDone(rectFlip.DOScaleX(0, 0.24f).SetEase(Ease.InBack, 3f).WaitForCompletion(true));

            rectFlip.SetActive(false);
            rectNew.SetActive(true);
            rectNew.localScale = new Vector3(0, 1, 1);

            yield return Timing.WaitUntilDone(rectNew.DOScaleX(1, 0.24f).SetEase(Ease.OutBack, 3f).WaitForCompletion(true));
        }

        public void OnDiscard()
        {
            OnDiscarded?.Invoke(this);
        }
    }
}