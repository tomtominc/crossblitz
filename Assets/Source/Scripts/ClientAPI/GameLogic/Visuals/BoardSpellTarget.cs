using System;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Visuals
{

    // the big ass board target used for dropping spells -
    // uses the board target like everything else, this is just the visual
    public class BoardSpellTarget : MonoBehaviour
    {
        public CanvasGroup canvas;
        public ParticleSystem shineParticles;
        public ParticleSystem activeOverlay;
        public CanvasGroup cardEnterShine;


        private RectTransform _rect;
        private BoardTarget _boardTarget;
        private float _enterShineFadeSpeed = 10;
        private bool _isActive;
        private bool _cardIsHovering;

        private void Start()
        {
            _boardTarget = GetComponent<BoardTarget>();
            _boardTarget.OnHideTarget += OnHide;
            _boardTarget.OnShowTarget += OnShow;
            _boardTarget.OnCardEnterActiveBoardTarget += OnCardEnter;
            _boardTarget.OnCardExitActiveBoardTarget += OnCardExit;

            shineParticles.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            activeOverlay.Stop(true, ParticleSystemStopBehavior.StopEmitting);

            canvas.alpha = 0;
            canvas.interactable = false;
            canvas.blocksRaycasts = false;
        }

        private void OnDestroy()
        {
            _boardTarget.OnHideTarget -= OnHide;
            _boardTarget.OnShowTarget -= OnShow;
            _boardTarget.OnCardEnterActiveBoardTarget -= OnCardEnter;
            _boardTarget.OnCardExitActiveBoardTarget -= OnCardExit;
        }

        private void Update()
        {
            if (!_cardIsHovering && cardEnterShine.alpha > 0)
            {
                cardEnterShine.alpha -= _enterShineFadeSpeed * Time.deltaTime;
            }

            if (!_isActive && canvas.alpha > 0)
            {
                canvas.alpha -= _enterShineFadeSpeed * Time.deltaTime;
            }
        }

        private void OnShow(BoardTarget bt)
        {
            _isActive = true;

            canvas.alpha = 1;
            canvas.interactable = true;
            canvas.blocksRaycasts = true;

            shineParticles.Play(true);
        }

        private void OnHide(BoardTarget bt)
        {
            _isActive = false;

            canvas.interactable = false;
            canvas.blocksRaycasts = false;

            shineParticles.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            activeOverlay.Stop(true, ParticleSystemStopBehavior.StopEmitting);
        }

        private void OnCardEnter(BoardTarget bt)
        {
            _cardIsHovering = true;

            cardEnterShine.alpha = 1;
            activeOverlay.Play(true);
        }

        private void OnCardExit(BoardTarget bt)
        {
            _cardIsHovering = false;
            activeOverlay.Stop(true, ParticleSystemStopBehavior.StopEmitting);
        }
    }
}