using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.Databases;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.ServerAPI.GameLogic.Intelligence;
using CrossBlitz.ViewAPI.Popups;
using GameDataEditor;
using Source.Scripts.Server.GameLogic.Data;
using UnityEngine;
using Debug = UnityEngine.Debug;

public static class GameLogic
{
    private static CommandQueue _queue;

    public static void StartCommand(Command command, bool resolveVisually = true)
    {
        _queue = new CommandQueue();

        if (command.Type == CommandType.PLAY || (command.Type == CommandType.SUMMON && ((SummonCommand)command).SummonMethod == SummonCommand.Method.PlayedFromHand))
        {
            ResolveTriggeredCards(GameServer.State.GetCard(command.SourceUid), null, TriggerType.BEFORE_PLAYED_CARD, new ResolveTriggeredCardsProperties { showLog = !AI.Thinking });
        }

        var triggerCommandsCount = CommandsInQueue();

        ExecuteCommand(command);

        if (triggerCommandsCount > 0)
        {
            for (var i = 0; i < triggerCommandsCount; i++)
            {
                 _queue.commands.Add(_queue.commands[i]);
            }

            for (var i = 0; i < triggerCommandsCount; i++)
            {
                _queue.commands.RemoveAt(0);
            }
        }

        if (command.Type == CommandType.PLAY || (command.Type == CommandType.SUMMON && ((SummonCommand)command).SummonMethod == SummonCommand.Method.PlayedFromHand))
        {
            ResolveTriggeredCards(GameServer.State.GetCard(command.SourceUid), null, TriggerType.PLAYED_CARD, new ResolveTriggeredCardsProperties { showLog = !AI.Thinking });
        }

        if (resolveVisually)
        {
            Server.Instance.RemoteCallToExecuteGameLogic(_queue);
            GameplayScreen.Instance.UpdateBlitzButtonForNoActionsLeft();
        }
    }

    public static void StartCommands(List<Command> commands, bool resolveVisually = true)
    {
        for (var i = 0; i < commands.Count; i++)
        {
            StartCommand(commands[i], resolveVisually);
        }
    }

    /// <summary>
    /// When 'Using a Card' it moves through the logic of how the card will be played out changing
    /// the board state.
    /// This function assumes that you've already handled any error checking. or conditional abilities and only passed in whats going to happen
    /// e.g. summoning a minion on the same space as another is not allowed, checking for mana costs, making sure things are not null etc.
    /// </summary>
    /// <param name="command"></param>
    /// <<param name="queue"></param>
    public static void ExecuteCommand(Command command)
    {
        if (command == null || command.Type == CommandType.NULL)
        {
            return;
        }

        _queue.commands.Add(command);

        // we add the history of the command
        GameServer.State.AddHistory(command);

        command.OnPrepareCommand();

#if GAME_DIAGNOSTICS_ENABLED
        var stopWatch = new System.Diagnostics.Stopwatch();
        stopWatch.Start();
#endif

        command.OnExecuteCommand();

#if GAME_DIAGNOSTICS_ENABLED
        stopWatch.Stop();
        Debug.Log($"Executing (AI Thinking? {AI.Thinking}): {command.Type} takes {stopWatch.Elapsed.Milliseconds}");
#endif
    }

    public static void ExecuteCommand(List<Command> commands)
    {
        for (var i = 0; i < commands.Count; i++)
        {
            ExecuteCommand(commands[i]);
        }
    }

    public static void ExecuteCommand(int index, Command command)
    {
        if (command == null || command.Type == CommandType.NULL)
        {
            // Debug.LogError("Command was set but is null!");
            return;
        }

        _queue.commands.Insert(index,command);

        // we add the history of the command
        GameServer.State.AddHistory(command);

        //command.OnPrepareCommand();
        // var stopWatch = new System.Diagnostics.Stopwatch();
        // stopWatch.Start();
        command.OnExecuteCommand();
        // stopWatch.Stop();
        // Debug.Log($"Executing (AI Thinking? {AI.Thinking}): {command.Type} takes {stopWatch.Elapsed.Milliseconds}");
    }

    public static void ExecutePendingCommands(List<PendingCommand> pendingCommands)
    {
        for (var i = 0; i < pendingCommands.Count; i++)
        {
            var card = GameServer.State.GetCard(pendingCommands[i].cardUid);
            var ability = card.GetAbilities()[pendingCommands[i].abilityIndex];
            var command = pendingCommands[i].parentCommand;
            var previousCommand = pendingCommands[i].previousCommand;

            //Debug.LogError($"Previous Command = {previousCommand.GetLabelDisplay()}");

            var commands = CreateCommandFromAbility(card, ability, command, previousCommand);
            ExecuteCommand(commands);
        }
    }

    public static void MoveCommand(Command command, int index)
    {
        if (index >= 0 && index < _queue.commands.Count)
        {
            var indexOf = _queue.commands.IndexOf(command);

            if (indexOf != index)
            {
                var temp = _queue.commands[index];
                _queue.commands[index] = command;
                _queue.commands[indexOf] = temp;
            }
        }
    }

    public static void MoveCommandToLast(Command command)
    {
        _queue.commands.Remove(command);
        _queue.commands.Add(command);

        // var index = _queue.commands.IndexOf(command);
        // if (index >= 0 && index < _queue.commands.Count)
        // {
        //     var temp =_queue.commands[index];
        //     _queue.commands[index] = _queue.commands[_queue.commands.Count - 1];
        //     _queue.commands[_queue.commands.Count - 1] = temp;
        // }
    }

    // public static void ReorderAllDestroyCommandsToLast()
    // {
    //     var commandsToMove = new List<Command>();
    //
    //     for (var i = 0; i < _queue.commands.Count; i++)
    //     {
    //         if (_queue.commands[i].Type == CommandType.DESTROY)
    //         {
    //             commandsToMove.Add(_queue.commands[i]);
    //         }
    //     }
    //
    //     for (var i = 0; i < commandsToMove.Count; i++)
    //     {
    //         MoveCommandToLast(commandsToMove[i]);
    //     }
    // }

    public static int CommandsInQueue()
    {
        if (_queue?.commands == null) return 0;
        return _queue.commands.Count;
    }

    public static int IndexOfCommand(Command command)
    {
        return _queue.commands.IndexOf(command);
    }

    public static List<Command> GetCommandsAtIndexAndBeyond(int index)
    {
        var commands = new List<Command>();

        if (index >= _queue.commands.Count || index < 0)
        {
            return commands;
        }

        for (var i = index; i < _queue.commands.Count; i++)
        {
            commands.Add(_queue.commands[i]);
        }

        return commands;
    }

    public static Command GetCommand(int index)
    {
        return _queue.commands[index];
    }

    public static void PlayPower(PlayCommand command, out bool countered)
    {
        // other power only things do here (like remove power from player)
        ExecuteCommand(new HeroPowerCinematicCommand
        {
            PlayerUid = command.PlayerUid,
            SourceUid = command.SourceUid
        });
        PlaySpell(command, out countered);
    }

    public static void PlayRelic(PlayCommand command, out bool countered)
    {
        // other relic only things (not sure what..)
        PlaySpell(command, out countered);
    }

    // public static void PlayTrick(PlayCommand command)
    // {
    //     // when a trick is played it just gets stored within the board state for further execution.
    //     GameServer.State.SetTrap(command);
    // }

    public static void PlaySpell(PlayCommand command, out bool countered)
    {
        countered = false;

        GameServer.State.PlaySpell(command);
        var playedSpell = GameServer.State.GetCard(command.SourceUid);

        // check for counters!
        if (!AI.Thinking)
        {
            for (var i = 0; i < GameServer.State.Cards.Count; i++)
            {
                var card = GameServer.State.Cards[i];

                if (card.location == CardLocation.Board &&
                    card.owner != playedSpell.owner &&
                    card.characterData.type == CardType.Trick &&
                    card.HasAbility(AbilityKeyword.COUNTER))
                {
                    var revealCommand = new RevealTrapCommand();
                    revealCommand.PlayerUid = card.owner;
                    revealCommand.SourceUid = card.uid;
                    ExecuteCommand(revealCommand);
                    countered = true;
                }
            }

            if (countered) return;
        }

        var abilities = playedSpell.GetAbilities();

        if (abilities.Count <= 0 || abilities.All( a => a.triggerLocation == CardLocation.Hand))
        {
            Debug.LogError("This spell doesn't have abilities, should never get this far in the logic!!");
            return;
        }

        if (playedSpell.GetAbilities().Count > 0)
        {
            var playerSelectedTarget = GetPlayerSelectedTarget(playedSpell, playedSpell.GetAbilities()[0], command, true);

            if (playedSpell.ConditionalAbilitiesHaveBeenMet(string.IsNullOrEmpty(playerSelectedTarget)
                    ? playedSpell.uid
                    : playerSelectedTarget))
            {
                abilities = playedSpell.SwapToConditionalAbilities();
            }
        }

        Command previousCommand = null;
        var holdOtherCommands = false;

        for (var i = 0; i < abilities.Count; i++)
        {
            if (holdOtherCommands && previousCommand != null)
            {
                previousCommand.AddHeldCommands(new PendingCommand
                {
                    cardUid = playedSpell.uid,
                    abilityIndex = i,
                    parentCommand = command,
                    previousCommand = previousCommand
                });

                continue;
            }

            if (abilities[i].triggerType == TriggerType.PLUNDER)
            {
                continue;
            }

            var commands = CreateCommandFromAbility(playedSpell, abilities[i], command, previousCommand);
            ExecuteCommand(commands);

            if (commands.Count > 0)
            {
                previousCommand = commands[commands.Count - 1];
            }

            if (abilities[i].keyword == AbilityKeyword.REDEEM && abilities.Count - 1 > i)
            {
                holdOtherCommands = true;
            }
        }

        ResolveTriggeredCards(playedSpell, null, TriggerType.PLAYED_SPELL);

        if (!AI.Thinking)
        {
            var commandsFromFocus = FocusCards(command.PlayerUid, playedSpell.uid,command,previousCommand);
            ExecuteCommand(commandsFromFocus);
        }
    }

    public static List<Command> FocusCards(string playerUid, string sourceUid, Command command, Command previousCommand,  int times = 1)
    {
        var playerThatPlayedSpell = GameServer.GetPlayerState(playerUid);
        var commandsFromFocus = new List<Command>();

        for (var i = 0; i < playerThatPlayedSpell.hand.Count; i++)
        {
            var cardInHand = GameServer.State.GetCard(playerThatPlayedSpell.hand[i]);

            if (cardInHand != null && cardInHand.uid != sourceUid && cardInHand.HasTrigger(TriggerType.FOCUS))
            {
                var focusAbility = cardInHand.GetAbility(TriggerType.FOCUS);
                var abilityState = cardInHand.GetAbilityState(focusAbility);

                if (focusAbility == null ||
                    abilityState == null ||
                    abilityState.focusValue >= focusAbility.focusCount ||
                    abilityState.exhausted)
                {
                    continue;
                }

                var focusCommand = new FocusCommand
                {
                    PlayerUid = playerThatPlayedSpell.Uid,
                    SourceUid = cardInHand.uid,
                };

                commandsFromFocus.Add(focusCommand);

                abilityState.focusValue += times;
                abilityState.focusValue = Mathf.Clamp(abilityState.focusValue, 0, focusAbility.focusCount);

                focusCommand.isComplete = abilityState.focusValue >= focusAbility.focusCount;

                if (focusCommand.isComplete)
                {
                    var commands = CreateCommandFromAbility(cardInHand, focusAbility, command, previousCommand);
                    commandsFromFocus.AddRange(commands);
                }
            }
        }

        return commandsFromFocus;
    }

    public static void PlayHero(PlayCommand command)
    {
        Debug.LogWarning("Hero cards can't be used currently");
    }

    public struct ResolveTriggeredCardsProperties
    {
        public bool showLog;
        public bool verbose;
        public bool allowFromTheGraveyard;
    }

    public static void ResolveTriggeredCards(GameCardState source, GameCardState target, TriggerType trigger,ResolveTriggeredCardsProperties triggeredCardProperties=default)
    {
        var triggeredCards = new List<GameCardState>();

        if (triggeredCardProperties.showLog)
        {
            Debug.LogWarning($"[{trigger}] Source={source?.characterData?.name} Target={target?.GetDebugName()}  Target Location: {target?.location}");
        }

        for (var i = 0; i < GameServer.State.Cards.Count; i++)
        {
            var card = GameServer.State.Cards[i];

            if (!triggeredCardProperties.allowFromTheGraveyard || AI.Thinking)
            {
                if (GameCardState.IsNullOrDead(card))
                {
                    continue;
                }

                if (card.location != CardLocation.Board && card.location != CardLocation.Hand)
                {
                    continue;
                }
            }

            if (card.IsHero)
            {
                continue;
            }

            if (AI.Thinking && card.data.aiProperties.strategy.disableTriggerDuringAiThink)
            {
                continue;
            }

            if (card.HasStatusEffect(StatusEffect.Frozen))
            {
                continue;
            }

            if (!card.GetAbilities().Exists(a => a.triggerType.HasFlag(trigger)))
            {
                if(triggeredCardProperties.verbose)
                {
                    Debug.LogWarning($"[{trigger}] {card.data.name} did not have this trigger as a trigger type.");
                }

                continue;
            }

            if(triggeredCardProperties.verbose)
            {
                Debug.LogWarning($"[{trigger}] Adding {card.data.name} for triggered.");
            }

            triggeredCards.Add(card);
        }

        triggeredCards = triggeredCards.OrderByDescending(x => x.Team == GameServer.State.CurrentTeamTurn()).ToList();

        for (var i = 0; i < triggeredCards.Count; i++)
        {
            var triggeredCard = triggeredCards[i];
            var triggeredAbilities = triggeredCard.GetAbilities().FindAll(x => x.triggerType == trigger);

            for (var j = 0; j < triggeredAbilities.Count; j++)
            {
                var triggeredAbility = triggeredAbilities[j];

                if (triggeredCardProperties.showLog)
                {
                    Debug.LogWarning($"[{trigger}] {triggeredCard.GetDebugName()}");
                }

                if (source != null && source.uid == triggeredCard.uid &&
                    !triggeredAbility.triggerConditions.HasFlag(FilterCondition.SELF))
                {
                    Debug.LogWarning(
                        $"[{trigger}] Removed because this card is not the triggered card. Source Card={source.data.name}, Triggered Card={triggeredCard.data.name}\n{source.uid}=={triggeredCard.uid} && ({triggeredAbility.triggerConditions})");
                    continue; // remove self from being triggered if the trigger condition doesn't equal SELF
                }

                Command previousCommand = null;

                if (triggeredAbility != null && triggeredAbility.triggerLocation == triggeredCard.location)
                {
                    var isTargetOfAbility = target != null && target.uid == triggeredCard.uid;

                    Debug.LogWarning(
                        $"[{trigger}] Trying to trigger card {triggeredCard?.characterData.name} : Target = {target?.characterData.name}, Is Target? {isTargetOfAbility}");

                    if (CanBeTriggered(source, target, triggeredCard, triggeredAbility, isTargetOfAbility))
                    {
                        Debug.LogWarning($"[{trigger}] [{triggeredAbility.keyword}] Executing command! {triggeredCard.data.name} : Target = {target?.characterData.name}");

                        var commands = CreateCommandFromAbility(triggeredCard, triggeredAbility, null, null, 0, source?.uid, target?.uid);

                        ExecuteCommand(commands);

                        if (triggeredAbility.triggerConditions.HasFlag(FilterCondition.DISABLE_LINKED))
                        {
                            continue;
                        }

                        if (commands.Count > 0)
                        {
                            previousCommand = commands[commands.Count - 1];
                        }

                        if (triggeredAbility.hasConditionalAbility)
                        {
                            var passesAllConditions =
                                PassesAllConditions(triggeredCard.owner, triggeredAbility, target);
                            if (passesAllConditions)
                            {
                                var conditionalAbilities = triggeredCard.data.GetConditionalAbilities();

                                for (var k = 0; k < conditionalAbilities.Count; k++)
                                {
                                    ExecuteCommand(CreateCommandFromAbility(triggeredCard,
                                        conditionalAbilities[k], null, previousCommand, 0, source.uid,target.uid));
                                }
                            }
                        }

                        var linkedAbilities = triggeredCard.GetAbilities().FindAll(x => x.triggerType == TriggerType.LINKED_ABILITY);
                        var linkedCommands = new List<Command>();

                        for (var k = 0; k < linkedAbilities.Count; k++)
                        {
                            try
                            {
                                if (linkedAbilities[k].abilityCondition.condition == Condition.PREVIOUS_COMMAND_TARGETS &&
                                    linkedAbilities[k].abilityCondition.usePreviousLinkedCommandNotParent &&
                                    linkedCommands != null && linkedCommands.Count > 0)
                                {
                                    previousCommand = linkedCommands[0];
                                }

                                linkedCommands = CreateCommandFromAbility(triggeredCard, linkedAbilities[k], null, previousCommand, 0, source?.uid, target?.uid);

                                ExecuteCommand(linkedCommands);

                            }
                            catch (Exception e)
                            {
                                Debug.LogError($"Something went wrong executing this. Card = {triggeredCard.characterData.name} Linked Ability [{k}]\n{e}");
                            }
                        }
                    }
                }
            }
        }
    }

    public static bool PassesAllConditions(string abilityOwner, Ability ability, GameCardState targetOfAbility)
    {
        if (ability.conditions == null || ability.conditions.Count <= 0) return false;

        var passesAllConditions = true;

        for (var i = 0; i < ability.conditions.Count; i++)
        {
            switch (ability.conditions[i].condition)
            {
                case Condition.CLASS_ON_FIELD:
                    string playerToCheck = abilityOwner;

                    if (ability.damageAmountCondition.filters.HasFlag(FilterCondition.PLAYER) &&
                        ability.damageAmountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                    {
                        playerToCheck = string.Empty;
                    }
                    else if (ability.damageAmountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                    {
                        var player = GameServer.GetPlayerState(abilityOwner);
                        playerToCheck = GameServer.GetPlayerState(player.GetTeam() == Team.Client ? Team.Opponent : Team.Client).Uid;
                    }
                    else if (!ability.damageAmountCondition.filters.HasFlag(FilterCondition.PLAYER))
                    {
                        playerToCheck = string.Empty;
                    }

                    passesAllConditions = passesAllConditions && GameServer.State.ClassExistsOnBoard(ability.conditions[i].@class, playerToCheck);
                    break;
                case Condition.DAMAGED:
                    passesAllConditions = passesAllConditions && targetOfAbility.IsDamaged();
                    break;
                case Condition.TARGET_FROZEN:
                    passesAllConditions =
                        passesAllConditions && targetOfAbility.statusEffect.HasFlag(StatusEffect.Frozen);
                    break;
                case Condition.FROZEN_THIS_GAME:
                    passesAllConditions =
                        passesAllConditions && GameServer.State.gameStats.TotalMinionsFrozen >=
                        ability.triggerValues.maxCount;
                    break;
            }
        }

        return passesAllConditions;
    }

    /// <summary>
    /// Checks to see if another ability is able to be triggered, the trigger type has already been set off and we're just now checking for other conditions.
    /// For example: if the trigger type was Damage and something is damaged then we run this. it will check other conditions like owner etc.
    /// </summary>
    /// <param name="sourceOfTheAbility">The source of the initial ability</param>
    /// <param name="targetOfTheSourceAbility">The target of that ability, this could be null if the ability did not have a target.</param>
    /// <param name="cardWantingToTriggerTheAbility">The card that is trying to trigger, it's trigger type is set off but we need to check other conditions.</param>
    /// <param name="ability">The ability that wants to trigger.</param>
    /// <param name="isTargetOfAbility">true if the targetOfTheSourceAbility == cardWantingToTriggerTheAbility, otherwise, false</param>
    /// <param name="forceCheckSourceCard">This happens if we need to check both the source card AND the target card! This allows us to loop through the function again in these cases.</param>
    /// <returns></returns>
    public static bool CanBeTriggered(GameCardState sourceOfTheAbility, GameCardState targetOfTheSourceAbility,
        GameCardState cardWantingToTriggerTheAbility, Ability ability, bool isTargetOfAbility,
        bool forceCheckSourceCard = false)
    {
        if (ability.triggerConditions == FilterCondition.NONE)
            return true;

        GameCardState card = sourceOfTheAbility;

        if (ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.TARGET) && !forceCheckSourceCard)
        {
            card = targetOfTheSourceAbility;
        }
        else if (ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.SOURCE) || forceCheckSourceCard)
        {
            card = sourceOfTheAbility;
        }
        else if (ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.TRIGGERED) )
        {
            card = cardWantingToTriggerTheAbility;
        }

        var log =
            $"[{ability.triggerEvaluateCondition}] Evaluating: {card?.data?.name}\n" +
            $"Target Of Source Ability = {targetOfTheSourceAbility?.data?.name}\n" +
            $"Source Of Ability = {sourceOfTheAbility?.data?.name}\n" +
            $"Card Wanted to Trigger Ability = {cardWantingToTriggerTheAbility?.data?.name}";

        var showLogs = !AI.Thinking;

        if (card == null)
        {
            if (showLogs) Debug.Log(
                $"Failed {ability.keyword} because the card was null. TriggerEvaluationCondition={ability.triggerEvaluateCondition}\n{log}");
            return false;
        }

        if (ability.triggerType == TriggerType.DEALT_DAMAGE_THROUGH_ATTACKING && targetOfTheSourceAbility?.uid != cardWantingToTriggerTheAbility?.uid)
        {
            if (showLogs) Debug.Log(
                $"failed check on Dealt Damage through attacking because the attacker was not the target source!\n{log}");
            return false;
        }

        if (ability.triggerType.HasFlag(TriggerType.CARD_CHANGED_LOCATIONS) && sourceOfTheAbility != null)
        {
            if (ability.triggerValues.fromLocation == CardLocation.Null &&
                ability.triggerValues.toLocation == CardLocation.Null)
            {
                if (showLogs) Debug.LogError(
                    $"Filter values not setup for CARD_CHANGED_LOCATIONS ability type. ({cardWantingToTriggerTheAbility?.characterData.name})");
                return false;
            }

            if (ability.triggerConditions.HasFlag(FilterCondition.PLAYER) &&
                card.Team != cardWantingToTriggerTheAbility?.Team)
            {
                if (showLogs) Debug.LogWarning(
                    $"[CARD_CHANGED_LOCATIONS] Not a Player card Team = {sourceOfTheAbility.Team} = {sourceOfTheAbility.characterData.name}");
                return false;
            }

            if (ability.triggerConditions.HasFlag(FilterCondition.OPPONENT) &&
                card.Team == cardWantingToTriggerTheAbility?.Team)
            {
                if (showLogs) Debug.LogWarning(
                    $"[CARD_CHANGED_LOCATIONS] Not an Opponent card Team = {sourceOfTheAbility.Team} = {sourceOfTheAbility.characterData.name}");
                return false;
            }

            if (ability.triggerValues.fromLocation != CardLocation.Null &&
                ability.triggerValues.fromLocation != sourceOfTheAbility.lastLocation)
            {
                if (showLogs) Debug.LogWarning(
                    $"[CARD_CHANGED_LOCATIONS] Last Location = {sourceOfTheAbility.lastLocation} From Location = {ability.triggerValues.fromLocation}");
                return false;
            }

            if (ability.triggerValues.toLocation != CardLocation.Null &&
                ability.triggerValues.toLocation != sourceOfTheAbility.location)
            {
                if (showLogs) Debug.LogWarning(
                    $"[CARD_CHANGED_LOCATIONS] Location = {sourceOfTheAbility.location} To Location = {ability.triggerValues.toLocation}");
                return false;
            }

            if (!string.IsNullOrEmpty(ability.triggerValues.specificTargetCard) &&
                sourceOfTheAbility.characterData.id != ability.triggerValues.specificTargetCard)
            {
                if (showLogs) Debug.LogWarning(
                    $"[CARD_CHANGED_LOCATIONS] Card = {sourceOfTheAbility.characterData.id} Target Card = {ability.triggerValues.specificTargetCard}");
                return false;
            }

            return true;
        }

        if (ability.triggerConditions.HasFlag(FilterCondition.EXCLUDE_SELF) && isTargetOfAbility)
        {
            if (!forceCheckSourceCard && ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.TARGET) &&
                ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.SOURCE))
            {
                return CanBeTriggered(sourceOfTheAbility, targetOfTheSourceAbility, cardWantingToTriggerTheAbility,
                    ability, isTargetOfAbility, true);
            }

            if (showLogs)  Debug.Log($"failed check on EXCLUDE_SELF\n{log}");
            return false;
        }

        if (ability.triggerConditions.HasFlag(FilterCondition.SELF) && !isTargetOfAbility)
        {
            if (!forceCheckSourceCard && ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.TARGET) &&
                ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.SOURCE))
            {
                return CanBeTriggered(sourceOfTheAbility, targetOfTheSourceAbility, cardWantingToTriggerTheAbility,
                    ability, isTargetOfAbility, true);
            }

            if (showLogs) Debug.Log($"failed check on SELF\n{log}");
            return false;
        }

        if (ability.triggerConditions.HasFlag(FilterCondition.SAME_TEAM) &&
            cardWantingToTriggerTheAbility?.Team != card.Team)
        {
            if (!forceCheckSourceCard && ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.TARGET) &&
                ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.SOURCE))
            {
                return CanBeTriggered(sourceOfTheAbility, targetOfTheSourceAbility, cardWantingToTriggerTheAbility,
                    ability, isTargetOfAbility, true);
            }

            if (showLogs) Debug.Log($"failed check on SAME_TEAM\n{log}");
            return false;
        }

        if (ability.triggerConditions.HasFlag(FilterCondition.PLAYER) &&
            card.Team != cardWantingToTriggerTheAbility?.Team)
        {
            if (!forceCheckSourceCard && ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.TARGET) &&
                ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.SOURCE))
            {
                return CanBeTriggered(sourceOfTheAbility, targetOfTheSourceAbility, cardWantingToTriggerTheAbility,
                    ability, isTargetOfAbility, true);
            }

            if (showLogs) Debug.Log($"failed check on player\n{log}");
            return false;
        }

        if (ability.triggerConditions.HasFlag(FilterCondition.OPPONENT) &&
            card.Team == cardWantingToTriggerTheAbility?.Team)
        {
            if (!forceCheckSourceCard && ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.TARGET) &&
                ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.SOURCE))
            {
                return CanBeTriggered(sourceOfTheAbility, targetOfTheSourceAbility, cardWantingToTriggerTheAbility,
                    ability, isTargetOfAbility, true);
            }

            if (showLogs) Debug.Log($"failed check on opponent\n{log}");
            return false;
        }

        if (ability.triggerConditions.HasFlag(FilterCondition.HERO) && !card.IsHero)
        {
            if (!forceCheckSourceCard && ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.TARGET) &&
                ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.SOURCE))
            {
                return CanBeTriggered(sourceOfTheAbility, targetOfTheSourceAbility, cardWantingToTriggerTheAbility,
                    ability, isTargetOfAbility, true);
            }

            if (showLogs)  Debug.Log($"failed check on hero\n{log}");
            return false;
        }

        if (ability.triggerConditions.HasFlag(FilterCondition.MINION) &&
            (card.IsHero || card.characterData.type != CardType.Minion))
        {
            if (!forceCheckSourceCard && ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.TARGET) &&
                ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.SOURCE))
            {
                return CanBeTriggered(sourceOfTheAbility, targetOfTheSourceAbility, cardWantingToTriggerTheAbility,
                    ability, isTargetOfAbility, true);
            }

            if (showLogs)  Debug.Log($"failed check on minion\n{log}");
            return false;
        }

        if (ability.triggerConditions.HasFlag(FilterCondition.SPELL) &&
            (card.IsHero || card.data.type != CardType.Spell))
        {
            if (!forceCheckSourceCard && ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.TARGET) &&
                ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.SOURCE))
            {
                return CanBeTriggered(sourceOfTheAbility, targetOfTheSourceAbility, cardWantingToTriggerTheAbility,
                    ability, isTargetOfAbility, true);
            }

            if (showLogs) Debug.Log($"failed check on spell\n{log}");
            return false;
        }

        if (ability.triggerConditions.HasFlag(FilterCondition.TRICK) &&
            (card.IsHero || card.data.type != CardType.Trick))
        {
            if (!forceCheckSourceCard && ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.TARGET) &&
                ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.SOURCE))
            {
                return CanBeTriggered(sourceOfTheAbility, targetOfTheSourceAbility, cardWantingToTriggerTheAbility,
                    ability, isTargetOfAbility, true);
            }

            if (showLogs) Debug.Log($"failed check on trick\n{log}");
            return false;
        }

        if (ability.triggerConditions.HasFlag(FilterCondition.BOARD) && card.location != CardLocation.Board)
        {
            if (!forceCheckSourceCard && ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.TARGET) &&
                ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.SOURCE))
            {
                return CanBeTriggered(sourceOfTheAbility, targetOfTheSourceAbility, cardWantingToTriggerTheAbility,
                    ability, isTargetOfAbility, true);
            }

            if (showLogs)  Debug.Log($"failed check on board\n{log}");
            return false;
        }

        if (ability.triggerConditions.HasFlag(FilterCondition.DECK) && card.location != CardLocation.Deck)
        {
            if (!forceCheckSourceCard && ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.TARGET) &&
                ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.SOURCE))
            {
                return CanBeTriggered(sourceOfTheAbility, targetOfTheSourceAbility, cardWantingToTriggerTheAbility,
                    ability, isTargetOfAbility, true);
            }

            if (showLogs)  Debug.Log($"failed check on deck\n{log}");
            return false;
        }

        if (ability.triggerConditions.HasFlag(FilterCondition.HAND) && card.location != CardLocation.Hand)
        {
            if (!forceCheckSourceCard && ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.TARGET) &&
                ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.SOURCE))
            {
                return CanBeTriggered(sourceOfTheAbility, targetOfTheSourceAbility, cardWantingToTriggerTheAbility,
                    ability, isTargetOfAbility, true);
            }

            if (showLogs) Debug.Log($"failed check on hand\n{log}");
            return false;
        }

        if (ability.triggerConditions.HasFlag(FilterCondition.IS_DAMAGED) &&
            card.GetHealth() >= card.GetDatabaseMaxHealth())
        {
            if (!forceCheckSourceCard && ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.TARGET) &&
                ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.SOURCE))
            {
                return CanBeTriggered(sourceOfTheAbility, targetOfTheSourceAbility, cardWantingToTriggerTheAbility,
                    ability, isTargetOfAbility, true);
            }

            if (showLogs) Debug.Log($"failed check on is damaged\n{log}");
            return false;
        }

        if (ability.triggerConditions.HasFlag(FilterCondition.PLAYED_CARD_THIS_TURN) &&
            GameServer.GetPlayerState(card.Team).cardsPlayedThisTurn <= 0)
        {
            if (!forceCheckSourceCard && ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.TARGET) &&
                ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.SOURCE))
            {
                return CanBeTriggered(sourceOfTheAbility, targetOfTheSourceAbility, cardWantingToTriggerTheAbility,
                    ability, isTargetOfAbility, true);
            }

            if (showLogs) Debug.Log($"failed check on has played a card this turn!\n{log}");
            return false;
        }

        if (ability.triggerConditions.HasFlag(FilterCondition.ADVANCED_FILTERS))
        {
            if (ability.triggerValues.turnRestricted && (ability.triggerValues.turnItNeedsToBe == Team.Client ? GameServer.State.CurrentTeamTurn() != cardWantingToTriggerTheAbility?.Team : GameServer.State.CurrentTeamTurn() == cardWantingToTriggerTheAbility?.Team))
            {
                if (showLogs)  Debug.Log($"failed check on TURN RESTRICTED!\n{log}");
                return false;
            }

            if (ability.triggerValues.@class != Class.All && ability.triggerValues.@class != Class.None && ability.triggerValues.@class != card.data.@class)
            {
                if (!forceCheckSourceCard &&
                    ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.TARGET) &&
                    ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.SOURCE))
                {
                    return CanBeTriggered(sourceOfTheAbility, targetOfTheSourceAbility, cardWantingToTriggerTheAbility,
                        ability, isTargetOfAbility, true);
                }

                if (showLogs)  Debug.Log($"failed check on CLASS!\n{log}");
                return false;
            }

            if (!string.IsNullOrEmpty(ability.triggerValues.specificTargetCard))
            {
                if (card.data.id != ability.triggerValues.specificTargetCard)
                {
                    if (!forceCheckSourceCard && ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.TARGET) && ability.triggerEvaluateCondition.HasFlag(TriggerEvaluateCondition.SOURCE))
                    {
                        return CanBeTriggered(sourceOfTheAbility, targetOfTheSourceAbility, cardWantingToTriggerTheAbility, ability, isTargetOfAbility, true);
                    }

                    if (showLogs)  Debug.Log($"failed check on SPECIFIC CARD!\n{log}");
                    return false;
                }
            }

            if (ability.triggerValues.checkAbilityConditionToTrigger)
            {
                if (ability.abilityCondition.condition == Condition.TRAPS_SET)
                {
                    List<TileState> occupiedTiles;

                    if (ability.abilityCondition.filters.HasFlag(FilterCondition.PLAYER) && ability.abilityCondition.filters.HasFlag(FilterCondition.OPPONENT))
                    {
                        occupiedTiles = GameServer.State.Board.GetOccupiedTiles();
                    }
                    else  if (ability.abilityCondition.filters.HasFlag(FilterCondition.PLAYER))
                    {
                        occupiedTiles = GameServer.State.Board.GetOccupiedTiles( card.Team );
                    }
                    else if ( ability.abilityCondition.filters.HasFlag(FilterCondition.OPPONENT))
                    {
                        occupiedTiles = GameServer.State.Board.GetOccupiedTiles( card.Team == Team.Client ? Team.Opponent : Team.Client);
                    }
                    else
                    {
                        occupiedTiles = GameServer.State.Board.GetOccupiedTiles();
                    }

                    var passed = false;
                    var numTraps = 0;

                    for (var i = 0; i < occupiedTiles.Count; i++)
                    {
                        var trap = GameServer.State.GetCard(occupiedTiles[i].CardId);
                        if (trap.characterData.type == CardType.Trick)
                        {
                            numTraps++;

                            if (numTraps >= ability.abilityCondition.conditionCount)
                            {
                                passed = true;
                                break;
                            }

                        }
                    }

                    if (!passed)
                    {
                        if (showLogs)  Debug.Log($"failed check on checkAbilityConditionToTrigger : TRAPS_SET!\n{log}");
                        return false;
                    }
                }
            }

            if (ability.triggerValues.stat != FilterConditionValues.Stat.None && ability.triggerValues.statFormula != FilterConditionValues.StatFormula.None)
            {
                var score = 0;

                switch (ability.triggerValues.stat)
                {
                    case FilterConditionValues.Stat.Health:
                        score = card.GetPendingHealth();
                        if (card.HasAbility(AbilityKeyword.TOUGH))
                        {
                            score++;
                        }
                        break;
                    case FilterConditionValues.Stat.Power:
                        score = card.GetPendingPower();
                        break;
                    case FilterConditionValues.Stat.ManaCost:
                        score = card.GetCost();
                        break;
                    case FilterConditionValues.Stat.Armor:
                        score = card.GetArmor();
                        break;
                }

                switch (ability.triggerValues.statFormula)
                {
                    case FilterConditionValues.StatFormula.LessThanOrEqualTo:
                        return score <= ability.triggerValues.statScore;
                    case FilterConditionValues.StatFormula.LessThan:
                        return score < ability.triggerValues.statScore;
                    case FilterConditionValues.StatFormula.GreaterThanOrEqualTo:
                        return score >= ability.triggerValues.statScore;
                    case FilterConditionValues.StatFormula.GreaterThan:
                        return score > ability.triggerValues.statScore;
                    case FilterConditionValues.StatFormula.Equal:
                        return score == ability.triggerValues.statScore;
                }
            }

            if (ability.triggerValues.keyword != AbilityKeyword.NONE)
            {
                return card.HasAbility(ability.triggerValues.keyword);
            }

            if (ability.triggerValues.trigger != TriggerType.NONE)
            {
                return card.HasTrigger(ability.triggerValues.trigger);
            }
        }

        return true;
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="card"></param>
    /// <param name="ability"></param>
    /// <param name="parentCommand"></param>
    /// <param name="previousCommand"></param>
    /// <param name="delay"></param>
    /// <param name="sourceCardFromTriggerUid">The source parameter when this is called from ResolveTriggeredCards</param>
    /// <param name="targetCardFromTriggerUid">The target parameter when this is called from ResolveTriggeredCards</param>
    /// <param name="overrideAbilityIndex">Override the ability index, used if you pass a custom ability in the ability parameter (used in summon for relentless).</param>
    /// <returns></returns>
    public static List<Command> CreateCommandFromAbility(GameCardState card, Ability ability, Command parentCommand, Command previousCommand, float delay = 0, string sourceCardFromTriggerUid = default, string targetCardFromTriggerUid = default, int overrideAbilityIndex = -1)
    {
        var commands = new List<Command>();

        if (card.IsAbilityExhausted(ability))
        {
            return commands;
        }

        if (ability.exhaust)
        {
            card.ExhaustAbility(ability);
        }

        // causes throttling with AI
        //Debug.Log(card.data.name.ToUpper() + ": " + ability.keyword.ToString().ToUpper() +" COMMAND");

        switch (ability.keyword)
        {
            case AbilityKeyword.HEAL:
            {
                var healCommand = new HealCommand();
                healCommand.PlayerUid = card.owner;
                healCommand.SourceUid = card.uid;
                healCommand.Amount = HealCommand.GetHealingAmount(card.uid, ability, parentCommand, previousCommand, card.Team);
                healCommand.TargetUids = GetTargets(card, ability, parentCommand, previousCommand, true,sourceCardFromTriggerUid: sourceCardFromTriggerUid, targetCardFromTriggerUid: targetCardFromTriggerUid);
                healCommand.Delay = delay;
                commands.Add(healCommand.TargetUids.Count <= 0 ? Command.Empty() : healCommand);
                break;
            }
            case AbilityKeyword.SILENCE:
            {
                var silenceCommand = new SilenceCommand();
                silenceCommand.PlayerUid = card.owner;
                silenceCommand.SourceUid = card.uid;
                silenceCommand.AbilityIndex = ability.usesCustomEffect ? card.GetAbilityIndex(ability) : -1;
                silenceCommand.TargetUids = GetTargets(card, ability, parentCommand, previousCommand, true,sourceCardFromTriggerUid: sourceCardFromTriggerUid, targetCardFromTriggerUid: targetCardFromTriggerUid);
                silenceCommand.Delay = delay;
                commands.Add(silenceCommand.TargetUids.Count <= 0 ? Command.Empty() : silenceCommand);
                break;
            }
            case AbilityKeyword.DAMAGE:
            {
                if (GameServer.State.AuraUpdater.AuraExists(AuraData.AuraEffect.Damage_Spells_Heal_Instead))
                {
                    var healCommand = new HealCommand();
                    healCommand.PlayerUid = card.owner;
                    healCommand.SourceUid = card.uid;
                    healCommand.TargetUids = GetTargets(card, ability, parentCommand, previousCommand, true,sourceCardFromTriggerUid: sourceCardFromTriggerUid, targetCardFromTriggerUid: targetCardFromTriggerUid);
                    healCommand.Amount = DamageCommand.GetDamageAmount(healCommand.PlayerUid,healCommand.SourceUid,healCommand.TargetUids, ability);
                    healCommand.Delay = delay;
                    commands.Add(healCommand.TargetUids.Count <= 0 ? Command.Empty() : healCommand);
                }
                else
                {
                    var damageCommand = new DamageCommand();
                    damageCommand.PlayerUid = card.owner;
                    damageCommand.SourceUid = card.uid;
                    damageCommand.RandomlySplit = ability.damageRandomlySplit;
                    damageCommand.TargetUids = GetTargets(card, ability, parentCommand, previousCommand, true,sourceCardFromTriggerUid: sourceCardFromTriggerUid, targetCardFromTriggerUid: targetCardFromTriggerUid);
                    damageCommand.Repeats = ability.repeatDamageBasedOnConditions;
                    damageCommand.RepeatDamageCount = damageCommand.Repeats ? damageCommand.GetRepeatCount(ability) : 0;
                    damageCommand.AbilityIndex = ability.usesCustomEffect ? card.GetAbilityIndex(ability) : -1;
                    damageCommand.Amount = DamageCommand.GetDamageAmount(damageCommand.PlayerUid,damageCommand.SourceUid,damageCommand.TargetUids, ability);
                    damageCommand.Delay = delay;
                    commands.Add(damageCommand.TargetUids.Count <= 0 ? Command.Empty() : damageCommand);
                }

                break;
            }
            case AbilityKeyword.MOVE_MINION_TO_ZONE:
            {
                var moveMinionToZoneCommand = new MoveMinionToZone();
                moveMinionToZoneCommand.PlayerUid = card.owner;
                moveMinionToZoneCommand.SourceUid = card.uid;
                moveMinionToZoneCommand.TargetUids = GetTargets(card, ability, parentCommand, previousCommand, true,sourceCardFromTriggerUid: sourceCardFromTriggerUid, targetCardFromTriggerUid: targetCardFromTriggerUid);
                moveMinionToZoneCommand.AbilityIndex = card.GetAbilityIndex(ability);
                moveMinionToZoneCommand.TeamLocation = ability.ownerOfZone.HasFlag(FilterCondition.OPPONENT)
                    ?
                    card.Team == Team.Client ? Team.Opponent : Team.Client
                    : card.Team == Team.Client
                        ? Team.Client
                        : Team.Opponent;
                moveMinionToZoneCommand.NewLocation = ability.zoneToMoveTo;
                commands.Add(moveMinionToZoneCommand);

                break;
            }
            case AbilityKeyword.DESTROY:
            {
                var destroyCommand = new DestroyCommand();
                destroyCommand.PlayerUid = card.owner;
                destroyCommand.SourceUid = card.uid;
                destroyCommand.AbilityIndex = ability.usesCustomEffect ? card.GetAbilityIndex(ability) : -1;
                destroyCommand.TargetUids = GetTargets(card, ability, parentCommand, previousCommand, true,sourceCardFromTriggerUid: sourceCardFromTriggerUid, targetCardFromTriggerUid: targetCardFromTriggerUid);
                destroyCommand.Delay = delay;
                commands.Add(destroyCommand.TargetUids.Count <= 0 ? Command.Empty() : destroyCommand);
                break;
            }
            case AbilityKeyword.MODIFY_HEALTH:
            {
                var modifyHealthCommand = new ModifyHealthCommand();
                modifyHealthCommand.PlayerUid = card.owner;
                modifyHealthCommand.SourceUid = card.uid;
                modifyHealthCommand.TargetUids = GetTargets(card, ability, parentCommand, previousCommand, true,sourceCardFromTriggerUid: sourceCardFromTriggerUid, targetCardFromTriggerUid: targetCardFromTriggerUid);
                modifyHealthCommand.Amount = ModifyHealthCommand.GetHealthModifier(modifyHealthCommand.SourceUid, ability, previousCommand, modifyHealthCommand.GetTeam());
                modifyHealthCommand.Set = ability.setInsteadOfModify;
                modifyHealthCommand.Delay = delay;
                commands.Add(modifyHealthCommand.TargetUids.Count <= 0 ? Command.Empty() : modifyHealthCommand);
                break;
            }
            case AbilityKeyword.MODIFY_POWER:
            {
                var modifyPowerCommand = new ModifyPowerCommand();
                modifyPowerCommand.PlayerUid = card.owner;
                modifyPowerCommand.SourceUid = card.uid;
                modifyPowerCommand.TargetUids = GetTargets(card, ability, parentCommand, previousCommand, true,sourceCardFromTriggerUid: sourceCardFromTriggerUid, targetCardFromTriggerUid: targetCardFromTriggerUid);
                modifyPowerCommand.Amount = ModifyPowerCommand.GetPowerModifier(modifyPowerCommand.SourceUid, ability, previousCommand, modifyPowerCommand.GetTeam());
                modifyPowerCommand.Set = ability.setInsteadOfModify;
                modifyPowerCommand.AbilityIndex = ability.usesCustomEffect ? card.GetAbilityIndex(ability) : -1;
                modifyPowerCommand.Delay = delay;
                commands.Add(modifyPowerCommand.TargetUids.Count <= 0 ? Command.Empty() : modifyPowerCommand);
                break;
            }
            case AbilityKeyword.MODIFY_HEALTH_AND_POWER:
            {
                var modifyHealthAndPowerCommand = new ModifyHealthAndPowerCommand();
                modifyHealthAndPowerCommand.PlayerUid = card.owner;
                modifyHealthAndPowerCommand.SourceUid = card.uid;
                modifyHealthAndPowerCommand.AbilityIndex = ability.usesCustomEffect ? card.GetAbilityIndex(ability) : -1;
                modifyHealthAndPowerCommand.TargetUids = GetTargets(card, ability, parentCommand, previousCommand, true,sourceCardFromTriggerUid: sourceCardFromTriggerUid, targetCardFromTriggerUid: targetCardFromTriggerUid);
                modifyHealthAndPowerCommand.Power = ModifyPowerCommand.GetPowerModifier(modifyHealthAndPowerCommand.SourceUid, ability, previousCommand, modifyHealthAndPowerCommand.GetTeam());
                modifyHealthAndPowerCommand.Health = ModifyHealthCommand.GetHealthModifier(modifyHealthAndPowerCommand.SourceUid, ability, previousCommand, modifyHealthAndPowerCommand.GetTeam());
                modifyHealthAndPowerCommand.Set = ability.setInsteadOfModify;
                modifyHealthAndPowerCommand.Delay = delay;
                commands.Add(modifyHealthAndPowerCommand.TargetUids.Count <= 0 ? Command.Empty() : modifyHealthAndPowerCommand);
                break;
            }
            case AbilityKeyword.GIVE_ABILITY:
            {
                var giveAbilityCommand = new GiveAbilityCommand();
                giveAbilityCommand.PlayerUid = card.owner;
                giveAbilityCommand.SourceUid = card.uid;
                giveAbilityCommand.AbilityIndex = ability.usesCustomEffect ? card.GetAbilityIndex(ability) : -1;
                giveAbilityCommand.TargetUids = GetTargets(card, ability, parentCommand, previousCommand, true,sourceCardFromTriggerUid: sourceCardFromTriggerUid, targetCardFromTriggerUid: targetCardFromTriggerUid);

                if (ability.usesRandomGivenAbility)
                {
                    var cardPool = Db.CardDatabase.GetValidGameCards();
                    var cards = CardFilter.GetCardsWithFilter(cardPool,ability.cardFilter);

                    if (cards.Count <= 0)
                    {
                        break;
                    }

                    var random = new System.Random();
                    var randomCard = cards[random.Next(0, cards.Count)];

                    if (ability.checkTriggerInsteadOfKeyword)
                    {
                        giveAbilityCommand.AbilitiesToGive = randomCard.GetAbilities()
                            .FindAll(a =>
                                a.triggerType == ability.cardFilter.trigger || (ability.getLinkedAbilities &&
                                                                            a.triggerType ==
                                                                            TriggerType.LINKED_ABILITY));
                    }
                    else
                    {
                        giveAbilityCommand.AbilitiesToGive = randomCard.GetAbilities()
                            .FindAll(a =>
                                a.keyword == ability.cardFilter.keyword || (ability.getLinkedAbilities &&
                                                                            a.triggerType ==
                                                                            TriggerType.LINKED_ABILITY));
                    }

                    giveAbilityCommand.givenFromCardId = randomCard.id;
                }
                else
                {
                    giveAbilityCommand.AbilitiesToGive = new List<Ability>(card.data.GetGivenAbilities());
                }

                giveAbilityCommand.replaceAbility = ability.replaceAbility;
                giveAbilityCommand.replaceAbilityIndex = ability.replacementIndex;
                giveAbilityCommand.Delay = delay;
                commands.Add(giveAbilityCommand.TargetUids.Count <= 0 ? Command.Empty() : giveAbilityCommand);
                break;
            }
            case AbilityKeyword.SUMMON:
            {
                var summonCommand = new SummonCommand();
                summonCommand.PlayerUid = card.owner;
                summonCommand.SourceUid = card.uid;
                summonCommand.AbilityIndex = card.GetAbilityIndex(ability);
                summonCommand.TargetUids = SummonCommand.GetMinionsToSummon(card, ability, parentCommand,previousCommand, out var copyFromTargetsWillAlwaysBeNullHere, out var summonMethod);
                summonCommand.SummonMethod = summonMethod;

                if ((card.data.type == CardType.Relic || card.data.type == CardType.Elder_Relic) && !string.IsNullOrEmpty(sourceCardFromTriggerUid))
                {
                    var sourceCard = GameServer.State.GetCard(sourceCardFromTriggerUid);
                    summonCommand.Positions = SummonCommand.GetSummonPositionFromAbility(sourceCard, ability, summonCommand, targetCardFromTriggerUid);
                }
                else
                {
                    summonCommand.Positions = SummonCommand.GetSummonPositionFromAbility(card, ability, summonCommand, targetCardFromTriggerUid);
                }

                if (summonCommand.TargetUids.Count > summonCommand.Positions.Count)
                {
                    for (var i = summonCommand.TargetUids.Count - 1; i > summonCommand.Positions.Count - 1; i--)
                    {
                        GameServer.State.ReleaseCard(summonCommand.TargetUids[i]);
                        summonCommand.TargetUids.RemoveAt(i);
                    }
                }

                summonCommand.Delay = delay;
                commands.Add(summonCommand.TargetUids.Count > 0 && summonCommand.Positions.Count > 0 ? summonCommand : Command.Empty());
                break;
            }
            case AbilityKeyword.SUMMON_COPIES:
            {
                var summonCopiesCommand = new SummonCommand();
                summonCopiesCommand.PlayerUid = card.owner;
                summonCopiesCommand.SourceUid = card.uid;
                summonCopiesCommand.AbilityIndex = card.GetAbilityIndex(ability);
                summonCopiesCommand.SummonMethod = SummonCommand.Method.PlayedWithAbility;
                summonCopiesCommand.TargetUids = SummonCommand.GetMinionsToSummon(card, ability, parentCommand,previousCommand, out var copyFromTargets, out var summonMethod);
                summonCopiesCommand.SummonMethod = summonMethod;
                summonCopiesCommand.CopyFromTargets = new List<string>(copyFromTargets);
                summonCopiesCommand.Positions = SummonCommand.GetSummonPositionFromAbility(card, ability, summonCopiesCommand, targetCardFromTriggerUid, summonCopiesCommand.TargetUids.Count);
                summonCopiesCommand.Delay = delay;

                if (summonCopiesCommand.TargetUids.Count > summonCopiesCommand.Positions.Count)
                {
                    for (var i = summonCopiesCommand.TargetUids.Count - 1; i > summonCopiesCommand.Positions.Count - 1; i--)
                    {
                        GameServer.State.ReleaseCard(summonCopiesCommand.TargetUids[i]);
                        summonCopiesCommand.TargetUids.RemoveAt(i);
                    }
                }

                commands.Add(summonCopiesCommand.TargetUids.Count > 0 && summonCopiesCommand.Positions.Count > 0 ? summonCopiesCommand : Command.Empty());
                break;
            }
            case AbilityKeyword.OVERGROW:
            {
                var summonCopiesCommand = new SummonCommand();
                summonCopiesCommand.PlayerUid = card.owner;
                summonCopiesCommand.SourceUid = card.uid;
                summonCopiesCommand.AbilityIndex = card.GetAbilityIndex(ability);
                summonCopiesCommand.TargetUids = SummonCommand.GetMinionsToSummon(card, ability, parentCommand, previousCommand,out var copyFromTargets, out var summonMethod);
                summonCopiesCommand.SummonMethod = summonMethod;
                summonCopiesCommand.CopyFromTargets = new List<string>(copyFromTargets);
                summonCopiesCommand.Positions = SummonCommand.GetSummonPositionFromAbility(card, ability, summonCopiesCommand, targetCardFromTriggerUid, summonCopiesCommand.TargetUids.Count);
                summonCopiesCommand.SummonMethod = SummonCommand.Method.PlayedWithAbility;
                summonCopiesCommand.Delay = delay;

                if (summonCopiesCommand.TargetUids.Count > summonCopiesCommand.Positions.Count)
                {
                    for (var i = summonCopiesCommand.TargetUids.Count - 1; i > summonCopiesCommand.Positions.Count - 1; i--)
                    {
                        GameServer.State.ReleaseCard(summonCopiesCommand.TargetUids[i]);
                        summonCopiesCommand.TargetUids.RemoveAt(i);
                    }
                }

                commands.Add(summonCopiesCommand.TargetUids.Count > 0 && summonCopiesCommand.Positions.Count > 0 ? summonCopiesCommand : Command.Empty());
                break;
            }
            case AbilityKeyword.REFILL_MANA:
            {
                var refillMana = new RefillManaCommand();
                refillMana.PlayerUid = card.owner;
                refillMana.SourceUid = card.uid;
                commands.Add(refillMana);
                break;
            }
            case AbilityKeyword.GAIN_ARMOR:
            {
                var gainArmorCommand = new GainArmorCommand();
                gainArmorCommand.PlayerUid = card.owner;
                gainArmorCommand.SourceUid = card.uid;
                gainArmorCommand.ArmorGain = gainArmorCommand.GetArmorGain(ability,parentCommand, previousCommand, sourceCardFromTriggerUid, targetCardFromTriggerUid);
                gainArmorCommand.TargetUids = GetTargets(card, ability, parentCommand, previousCommand, true,sourceCardFromTriggerUid: sourceCardFromTriggerUid, targetCardFromTriggerUid: targetCardFromTriggerUid);
                gainArmorCommand.Delay = delay;

                if (gainArmorCommand.ArmorGain > 0)
                {
                    commands.Add(gainArmorCommand.TargetUids.Count <= 0 ? Command.Empty() : gainArmorCommand);
                }

                break;
            }
            case AbilityKeyword.FREEZE:
            {
                var freezeCommand = new FreezeCommand();
                freezeCommand.PlayerUid = card.owner;
                freezeCommand.SourceUid = card.uid;
                freezeCommand.TargetUids = GetTargets(card, ability, parentCommand, previousCommand, true,sourceCardFromTriggerUid: sourceCardFromTriggerUid, targetCardFromTriggerUid: targetCardFromTriggerUid);
                freezeCommand.Delay = delay;
                commands.Add(freezeCommand.TargetUids.Count <= 0 ? Command.Empty() : freezeCommand);
                break;
            }
            case AbilityKeyword.BOOST_SPELL_DAMAGE:
            {
                var boostSpellDamageCommand = new BoostSpellDamageCommand();
                boostSpellDamageCommand.PlayerUid = card.owner;
                boostSpellDamageCommand.SourceUid = card.uid;
                boostSpellDamageCommand.boostAmount = 1;
                boostSpellDamageCommand.TargetUids = GetTargets(card, ability, parentCommand, previousCommand, true,sourceCardFromTriggerUid: sourceCardFromTriggerUid, targetCardFromTriggerUid: targetCardFromTriggerUid);
                boostSpellDamageCommand.Delay = delay;
                commands.Add(boostSpellDamageCommand);
                break;
            }
            case AbilityKeyword.GET_CARD_FROM_DECK:
            {
                var getCardFromDeckCommand = new GetCardFromDeckCommand();
                getCardFromDeckCommand.PlayerUid = card.owner;
                getCardFromDeckCommand.SourceUid = card.uid;
                getCardFromDeckCommand.TargetUids =
                    getCardFromDeckCommand.GetCardsFromDeckWithFilter(ability.cardFilter, ability.drawCount);
                getCardFromDeckCommand.Delay = delay;
                commands.Add(getCardFromDeckCommand.TargetUids.Count <= 0 ? Command.Empty() : getCardFromDeckCommand);
                break;
            }
            case AbilityKeyword.ADD_CARD_TO_DECK:
            {
                var addCardToDeckCommand = new AddCardToZoneCommand();
                addCardToDeckCommand.PlayerUid = card.owner;
                addCardToDeckCommand.SourceUid = card.uid;
                addCardToDeckCommand.AbilityIndex = card.GetAbilityIndex(ability);
                addCardToDeckCommand.locationToAddTo = CardLocation.Deck;
                var faction = GameServer.GetPlayerState(addCardToDeckCommand.PlayerUid).faction;
                addCardToDeckCommand.TargetUids = addCardToDeckCommand.CreateCardsFromFilter(ability.cardFilter, faction, ability.drawCount, ability, parentCommand,previousCommand,sourceCardFromTriggerUid,targetCardFromTriggerUid);
                addCardToDeckCommand.Delay = delay;
                //addCardToDeckCommand.MoveToCommandIndex = CommandsInQueue();
                commands.Add(addCardToDeckCommand);
                break;
            }
            case AbilityKeyword.ADD_CARD_TO_HAND:
            {
                var addCardToHandCommand = new AddCardToZoneCommand();
                addCardToHandCommand.PlayerUid = card.owner;
                addCardToHandCommand.SourceUid = card.uid;
                addCardToHandCommand.locationToAddTo = CardLocation.Hand;
                var faction = GameServer.GetPlayerState(addCardToHandCommand.PlayerUid).faction;
                addCardToHandCommand.TargetUids = addCardToHandCommand.CreateCardsFromFilter(ability.cardFilter,faction, ability.drawCount, ability, parentCommand,previousCommand,sourceCardFromTriggerUid,targetCardFromTriggerUid);
                addCardToHandCommand.Delay = delay;
                //addCardToHandCommand.MoveToCommandIndex = CommandsInQueue();
                commands.Add(addCardToHandCommand);
                break;
            }
            case AbilityKeyword.ATTACK_IMMEDIATELY_WHEN_SUMMONED:
            {
                var attackCommand = new AttackCommand
                {
                    PlayerUid = card.owner,
                    Team = card.Team,
                    SourceUid = card.uid,
                    TargetUids = GetTargets(card, ability, parentCommand, previousCommand, true,sourceCardFromTriggerUid: sourceCardFromTriggerUid, targetCardFromTriggerUid: targetCardFromTriggerUid),
                    WaitForColumnExplosionBeforeFinishing = false
                };
                attackCommand.Delay = delay;
                commands.Add(attackCommand);
                break;
            }
            case AbilityKeyword.FORCE_TARGETS_TO_ATTACK:
            {
                var targets = GetTargets(card, ability, parentCommand, previousCommand, true,sourceCardFromTriggerUid: sourceCardFromTriggerUid, targetCardFromTriggerUid: targetCardFromTriggerUid);

                for (var i = 0; i < targets.Count; i++)
                {
                    var tile = GameServer.State.Board.GetTile(targets[i]);
                    var source = GameServer.State.GetCard(targets[i]);
                    var attack = AttackCommand.GetStandardAttackCommandForCard(card.owner, source, tile.LocalPosition);
                    commands.Add(attack);
                }

                break;
            }
            case AbilityKeyword.DISCARD:
            {
                var removeCardsCommand = new DiscardCardCommand();
                removeCardsCommand.PlayerUid = card.owner;
                removeCardsCommand.SourceUid = card.uid;
                removeCardsCommand.TargetUids = removeCardsCommand.GetCardsToRemove(ability, parentCommand, previousCommand);
                removeCardsCommand.Delay = delay;
                commands.Add(removeCardsCommand);
                break;
            }
            case AbilityKeyword.MODIFY_MANA_CRYSTALS:
            {
                var modifyManaCommand = new ModifyManaCommand();
                modifyManaCommand.PlayerUid = card.owner;
                modifyManaCommand.SourceUid = card.uid;
                modifyManaCommand.ModifyOnlyMaximumMana = ability.modifyMaximumManaOnly;
                modifyManaCommand.Amount = ability.manaModifier;
                modifyManaCommand.Delay = delay;
                modifyManaCommand.AbilityIndex = ability.usesCustomEffect ? card.GetAbilityIndex(ability) : -1;
                commands.Add(modifyManaCommand);
                break;
            }
            case AbilityKeyword.TAUNT:
            {
                if (parentCommand is SummonCommand sc)
                {
                    var selectedTile = GameServer.State.Board.GetTile(sc.Selection);

                    if (selectedTile == null || !selectedTile.Occupied)
                    {
                        break;
                    }

                    var tauntCommand = new TauntCommand();
                    tauntCommand.PlayerUid = card.owner;
                    tauntCommand.SourceUid = card.uid;
                    tauntCommand.TargetUids = new List<string> { selectedTile.CardId };
                    tauntCommand.Delay = delay;
                    commands.Add(tauntCommand);
                }

                break;
            }
            case AbilityKeyword.REDEEM:
            {
                var redeemCommand = new RedeemCommand();
                redeemCommand.PlayerUid = card.owner;
                redeemCommand.SourceUid = card.uid;
                redeemCommand.Cards = redeemCommand.GetRedeemCards(card, ability.cardFilter);
                redeemCommand.Delay = delay;
                if (redeemCommand.Cards.Count > 0) commands.Add(redeemCommand);
                break;
            }
            case AbilityKeyword.DRAW:
            {
                var drawCommand = new DrawCardCommand();
                drawCommand.PlayerUid = drawCommand.GetDrawingPlayer(card, ability);
                drawCommand.SourceUid = card.uid;
                drawCommand.Mulligan = false;
                drawCommand.CardCountBeforeDraw = GameServer.GetPlayerState(drawCommand.PlayerUid).hand.Count;
                drawCommand.DrawCount = drawCommand.GetDrawAmount(ability);
                drawCommand.Delay = delay;
                commands.Add(drawCommand);
                break;
            }
            case AbilityKeyword.TRANSFORM_CARDS:
            {
                var transformCardsCommand = new TransformCardsCommand();
                transformCardsCommand.PlayerUid = card.owner;
                transformCardsCommand.SourceUid = card.uid;
                transformCardsCommand.AbilityIndex =  card.GetAbilityIndex(ability);
                transformCardsCommand.TargetUids = GetTargets(card, ability, parentCommand, previousCommand, true,sourceCardFromTriggerUid: sourceCardFromTriggerUid, targetCardFromTriggerUid: targetCardFromTriggerUid);
                transformCardsCommand.Delay = delay;
                transformCardsCommand.TransformCardData = ability.transformCardData;
                commands.Add(transformCardsCommand);
                break;
            }
            case AbilityKeyword.TRIGGER_ABILITY:
            {
                var triggerAbility = new TriggerAbilityCommand();
                triggerAbility.PlayerUid = card.owner;
                triggerAbility.SourceUid = card.uid;
                triggerAbility.TargetUids = GetTargets(card, ability, parentCommand, previousCommand, true,
                    sourceCardFromTriggerUid: sourceCardFromTriggerUid,
                    targetCardFromTriggerUid: targetCardFromTriggerUid);
                triggerAbility.Trigger = ability.triggeredAbility;
                triggerAbility.TriggerTimes =Mathf.Clamp(ability.triggerTimes, 1, 99);
                triggerAbility.AbilityIndex = card.GetAbilityIndex(ability);
                triggerAbility.Delay = delay;
                commands.Add(triggerAbility);

                break;
            }
            case AbilityKeyword.RELEASE_BRASS:
            {
                var releaseBrassCommand = new ReleaseBrassCommand();
                releaseBrassCommand.PlayerUid = card.owner;
                releaseBrassCommand.SourceUid = card.uid;
                releaseBrassCommand.Delay = delay;
                commands.Add(releaseBrassCommand);
                break;
            }
            case AbilityKeyword.MODIFY_COST:
            {
                var modifyCostCommand = new ModifyCostCommand();
                modifyCostCommand.PlayerUid = card.owner;
                modifyCostCommand.SourceUid = card.uid;
                modifyCostCommand.TargetUids = GetTargets(card, ability, parentCommand, previousCommand, true,sourceCardFromTriggerUid: sourceCardFromTriggerUid, targetCardFromTriggerUid: targetCardFromTriggerUid);
                modifyCostCommand.ModifyAmount = ModifyCostCommand.GetManaModifier(card.owner, ability, card.Team);
                commands.Add(modifyCostCommand);
                break;
            }
            case AbilityKeyword.GAIN_CONTROL:
            {
                var gainControlCommand = new GainControlCommand();
                gainControlCommand.PlayerUid = card.owner;
                gainControlCommand.SourceUid = card.uid;
                gainControlCommand.AbilityIndex = card.GetAbilityIndex(ability);
                gainControlCommand.TargetUids = GetTargets(card, ability, parentCommand, previousCommand, true,sourceCardFromTriggerUid: sourceCardFromTriggerUid, targetCardFromTriggerUid: targetCardFromTriggerUid);
                commands.Add(gainControlCommand);
                break;
            }
            case AbilityKeyword.BLITZ_BURST_INSTANT_FILL:
            {
                var blitzBurstInstantFill = new BlitzBurstInstantFillCommand();
                blitzBurstInstantFill.PlayerUid = card.owner;
                blitzBurstInstantFill.SourceUid = card.uid;
                blitzBurstInstantFill.TargetUids = new List<string>
                {
                    ability.targetConditions.HasFlag(FilterCondition.PLAYER)
                        ? GameServer.ClientPlayerState.Uid
                        : GameServer.OpponentPlayerState.Uid
                };
                blitzBurstInstantFill.Delay = delay;
                commands.Add(blitzBurstInstantFill);
                break;
            }
            case AbilityKeyword.AURA:
            {
                var auraCommand = new AuraCommand();
                auraCommand.PlayerUid = card.owner;
                auraCommand.SourceUid = card.uid;
                auraCommand.abilityIndex = card.GetAbilities().IndexOf(ability);
                auraCommand.TargetUids = GetTargets(card, ability, parentCommand, previousCommand, true,sourceCardFromTriggerUid: sourceCardFromTriggerUid, targetCardFromTriggerUid: targetCardFromTriggerUid);
                commands.Add( auraCommand );
                break;
            }
            case AbilityKeyword.FOCUS:
            {
                commands.AddRange( FocusCards(card.owner, card.uid,parentCommand, previousCommand, ability.focusCount) );
                break;
            }
            case AbilityKeyword.CAST_CARD:
            {
                var repeatCount = 1;
                var rand = new System.Random();

                if (ability.repeats)
                {
                    repeatCount = rand.Next(ability.repeatMin, ability.repeatMax + 1);
                }

                for (var j = 0; j < repeatCount; j++)
                {
                    var castCommand = new CastCardCommand();
                    castCommand.PlayerUid = card.owner;
                    castCommand.SourceUid = card.uid;
                    castCommand.TargetUids = GetTargets(card, ability, parentCommand, previousCommand, true,sourceCardFromTriggerUid: sourceCardFromTriggerUid, targetCardFromTriggerUid: targetCardFromTriggerUid);
                    castCommand.AbilityIndex = card.GetAbilityIndex(ability);
                    var cardIds = CardFilter.GetCardsWithFilter(Db.CardDatabase.Cards, ability.cardFilter);
                    castCommand.cardId = cardIds[rand.Next(0, cardIds.Count)].id;
                    commands.Add(castCommand);
                }

                break;
            }
            case AbilityKeyword.TRIGGER_ALL_DEATHRATTLES:
            {
                var triggerDeathrattles = new TriggerAllDeathrattlesCommand();
                triggerDeathrattles.PlayerUid = card.owner;
                triggerDeathrattles.SourceUid = card.uid;
                triggerDeathrattles.TargetUids = TriggerAllDeathrattlesCommand.GetTargets();
                commands.Add(triggerDeathrattles);
                break;
            }
            default:
                Debug.LogWarning($"Could not create Command {ability.keyword}, it has not been setup yet.");
                break;
        }

        // only reveal for the intial ability, some traps have multiple abilities.
        if (card.characterData.type == CardType.Trick && card.GetAbilityIndex(ability) == 0)
        {
            var revealCommand = new RevealTrapCommand();
            revealCommand.PlayerUid = card.owner;
            revealCommand.SourceUid = card.uid;
            commands.Push(revealCommand);
        }

        for (var i = 0; i < commands.Count; i++)
        {
            commands[i].Delay = delay;
        }

        card.UseAbility(ability);

        return commands;
    }

    private static void ThrowFatalError(string error)
    {
        PopupController.OnPopupChoice += b => { Server.Terminate(); };
        PopupController.Open(new PopupInfo
        {
            title = "FATAL ERROR",
            body = error,
            confirm = "OKAY"
        });
    }

    public static string GetPlayerSelectedTarget(GameCardState card, Ability ability, Command parentCommand,
        bool showDebugLog)
    {
        if (parentCommand != null && (ability.targetConditions.HasFlag(FilterCondition.PLAYER_SELECTED) ||
                                      ability.triggerConditions.HasFlag(FilterCondition.PLAYER_SELECTED)))
        {
            switch (parentCommand)
            {
                case SummonCommand summon:
                {
                    if (!string.IsNullOrEmpty(summon.SelectedHeroUid))
                    {
                        return summon.SelectedHeroUid;
                    }

                    if (summon.Selection.x < 0 || summon.Selection.y < 0)
                    {
                        //Debug.LogError($"No selection: {summon.Selection}");
                        return string.Empty;
                    }

                    var tileState = GameServer.State.Board.GetTile(summon.Selection);

                    if (tileState == null)
                    {
                        //if (showDebugLog) Debug.LogError($"There is no tile at {summon.Selection}");
                        return string.Empty;
                    }

                    var cardState = tileState.Card;
                    if (cardState == null)
                    {
                        //if (showDebugLog) Debug.LogError($"There is no card at {summon.Selection}");
                        return string.Empty;
                    }

                    if (parentCommand.IsClientCommand())
                    {
                        if (cardState.GetPendingHealth() <= 0)
                        {
                            //if (showDebugLog) Debug.Log("The target doesn't have any health left so we ignore it.");
                            return string.Empty;
                        }
                    }

                    return cardState.uid;
                }
                case PlayCommand playCommand:
                {
                    if (playCommand.Selection == null)
                    {
                        if (showDebugLog)
                        {
                            Debug.LogError($"There is no card at {playCommand.Selection}");
                        }

                        return string.Empty;
                    }

                    var cardState = playCommand.Selection;

                    if (parentCommand.IsClientCommand())
                    {
                        if (cardState.GetPendingHealth() <= 0)
                        {
                            if (showDebugLog) Debug.Log("The target doesn't have any health left so we ignore it.");
                            return string.Empty;
                        }
                    }

                    return cardState.uid;
                }
                default:
                    // overriding the conditions to add a player select and max target count of 1 so it keeps the same "valid" targets but does it randomly instead.
                    var overrideTargetConditions = new TargetConditions
                    {
                        triggerCondition =  FilterCondition.NONE,
                        targetCondition = ability.targetConditions & ~FilterCondition.PLAYER_SELECTED,
                        targetConditionValues = ability.filterValues
                    };

                    Debug.Log($"Target Filters = {overrideTargetConditions.targetCondition}");

                    var targets = GetTargets(card, ability, parentCommand, null, true, null, overrideTargetConditions);

                    if (targets.Count > 0)
                    {
                        return targets[new System.Random().Next(0, targets.Count)];
                    }

                    break;
            }

            // add more commands that might use this system.
        }

        return string.Empty;
    }

    /// <summary>
    /// Get targets for the ability, this gets targets based on certain parameters like the player has selected something.
    /// Use GetSelectTargets to get valid targets for selection
    /// </summary>
    /// <param name="card"></param>
    /// <param name="ability"></param>
    /// <param name="parentCommand"></param>
    /// <param name="previousCommand">The command that came right before this.</param>
    /// <param name="showDebugLog"></param>
    /// <param name="summoningOn">Only used for Taunt, I need to know what column the card is going to be summoned to</param>
    /// <param name="overrideTargetConditions">Used for AI, overrides the targeting to get something more specific.</param>
    /// <param name="sourceCardFromTriggerUid">The source parameter when this is called from ResolveTriggeredCards</param>
    /// <param name="targetCardFromTriggerUid">The target parameter when this is called from ResolveTriggeredCards</param>
    /// <returns></returns>
    public static List<string> GetTargets(GameCardState card, Ability ability, Command parentCommand,
        Command previousCommand, bool showDebugLog, TileState summoningOn = null,
        TargetConditions overrideTargetConditions = null, string sourceCardFromTriggerUid = default, string targetCardFromTriggerUid = default)
    {
        var targets = new List<string>();
        var random = new System.Random(System.Guid.NewGuid().GetHashCode());
        //showDebugLog = !AI.Thinking && showDebugLog;

        if (overrideTargetConditions != null)
        {
            ability = GameUtilities.Copy(ability);
            ability.triggerConditions = overrideTargetConditions.triggerCondition;
            ability.targetConditions = overrideTargetConditions.targetCondition;
            ability.filterValues = overrideTargetConditions.targetConditionValues;
        }

        if (ability.keyword == AbilityKeyword.TAUNT)
        {
            if (card != null && summoningOn != null &&
                !GameServer.State.Board.IsFullColumn(summoningOn.Position.x))
            {
                for (var i = 0; i < GameServer.State.Cards.Count; i++)
                {
                    var cardTarget = GameServer.State.Cards[i];

                    if (cardTarget == null)
                    {
                        continue;
                    }

                    if (cardTarget.data == null)
                    {
                        continue;
                    }

                    if (cardTarget.location != CardLocation.Board)
                    {
                        continue;
                    }

                    var invertedPosition = GameServer.State.Board.InvertPosition(summoningOn.Position);

                    if (cardTarget.position.x == invertedPosition.x)
                    {
                        //Debug.LogError($"{cardTarget.data.name} failed at position: {cardTarget.position} == {invertedPosition}");
                        continue;
                    }

                    if (cardTarget.Team == card.Team)
                    {
                        //Debug.LogError($"{cardTarget.data.name} failed at team: {cardTarget.Team}");
                        continue;
                    }

                    targets.Add(cardTarget.uid);
                }
            }

            return targets;
        }

        if (ability.targetConditions == FilterCondition.NONE)
        {
            return targets;
        }

        if (ability.targetConditions.HasFlag(FilterCondition.SOURCE_CARD_FROM_RESOLVE_TRIGGERED_CARDS))
        {
            if (!string.IsNullOrEmpty(sourceCardFromTriggerUid))
            {
                targets.Add(sourceCardFromTriggerUid);
            }
            else
            {
                Debug.LogError("Source card is null?");
            }

            return targets;
        }

        if (ability.targetConditions.HasFlag(FilterCondition.TARGET_CARD_FROM_RESOLVE_TRIGGERED_CARDS))
        {
            if (!string.IsNullOrEmpty(targetCardFromTriggerUid))
            {
                targets.Add(targetCardFromTriggerUid);
            }
            else
            {
                Debug.LogError("Target card is null?");
            }

            return targets;
        }

        if (ability.targetConditions.HasFlag(FilterCondition.ADVANCED_FILTERS) && ability.filterValues.lastRedeemedCard)
        {
            var lastRedeemCommand = GameServer.State.GetLastHistoryOf(CommandType.REDEEM);

            if (lastRedeemCommand != null && lastRedeemCommand is RedeemCommand redeemCommand)
            {
                targets.Add(redeemCommand.RedeemCardUid);
            }
            else
            {
                Debug.LogError($"No Redeem command!!!");
            }

            return targets;
        }

        if (ability.targetConditions == FilterCondition.SELF)
        {
            if (card?.data != null && card.data.type == CardType.Minion && card.GetPendingHealth() <= 0)
            {
                if (showDebugLog) Debug.Log("The target doesn't have any health left so we ignore it.");
                return targets;
            }

            if(card != null)
            {
                targets.Add(card.uid);
            }

            return targets;
        }

        if (showDebugLog)
        {
            if (parentCommand != null && previousCommand != null)
            {
                Debug.Log(
                    $"[{ability.keyword}] Parent Command [{parentCommand.Type}] Previous Command [{previousCommand.Type}] Target Conditions: {ability.targetConditions}");
            }
            else if (previousCommand != null)
            {
                Debug.Log(
                    $"[{ability.keyword}] Previous Command [{previousCommand.Type}] Target Conditions: {ability.targetConditions}");
            }
        }

        if (previousCommand?.TargetUids != null && ability.targetConditions.HasFlag(FilterCondition.PREVIOUS_COMMAND_TARGETS))
        {
            if (previousCommand.TargetUids.Count > 0)
            {
                targets = new List<string>(previousCommand.TargetUids);
            }

            if (showDebugLog)
            {
                Debug.Log($"[{ability.keyword}] Previous Command [{previousCommand.Type}]: Targets = {previousCommand.TargetUids.Count}");

                for (var i = 0; i < targets.Count; i++)
                {
                    var target = GameServer.State.GetCard(targets[i]);
                    var owner = target.owner == GameServer.ClientPlayerState.Uid ? "Client" : "Opponent";
                    Debug.Log($"[{ability.keyword}] target[{i}] = {target.characterData.name} {target.GetHealth()} ({target.location}) (OWNER = {owner})");
                }
            }

            return targets;
        }

        if (ability.targetConditions.HasFlag(FilterCondition.ADVANCED_FILTERS) && !string.IsNullOrEmpty(ability.filterValues.specificTargetCard))
        {
            var specificCards = GameServer.State.GetCards(c =>
                c.characterData.id == ability.filterValues.specificTargetCard);

            if (specificCards != null && specificCards.Count > 0)
            {
                if (ability.filterValues.allowMultipleSpecificCards)
                {
                    for (var i = 0; i < specificCards.Count; i++)
                    {
                        if (ability.filterValues.maxCount > 0 && targets.Count >= ability.filterValues.maxCount)
                        {
                            break;
                        }

                        if (!ability.targetConditions.HasFlag(FilterCondition.PLAYER) && specificCards[i].owner == card.owner) continue;
                        if (!ability.targetConditions.HasFlag(FilterCondition.OPPONENT) && specificCards[i].owner != card.owner) continue;

                        if (ability.targetConditions.HasFlag(FilterCondition.DECK) && specificCards[i].location == CardLocation.Deck)
                        {
                            targets.Add(specificCards[i].uid);
                        }
                        else if (specificCards[i].location == CardLocation.Board)
                        {
                            targets.Add(specificCards[i].uid);
                        }
                    }
                }
                else
                {
                    targets.Add(specificCards[new System.Random().Next(0, specificCards.Count)].uid);
                }
            }

            return targets;
        }

        if (ability.targetConditions.HasFlag(FilterCondition.ADJACENT_SPACES))
        {
            var tileRight = GameServer.State.Board.GetTile(card.position + Vector2Int.right);
            var tileLeft = GameServer.State.Board.GetTile(card.position + Vector2Int.left);

            if (tileRight != null && tileRight.Occupied) targets.Add(tileRight.Card.uid);
            if (tileLeft != null && tileLeft.Occupied) targets.Add(tileLeft.Card.uid);

            return targets;
        }

        // if (parentCommand != null && (ability.targetConditions.HasFlag(FilterCondition.PLAYER_SELECTED_MULTI) ||
        //                               ability.triggerConditions.HasFlag(FilterCondition.PLAYER_SELECTED_MULTI)))
        // {
        //     var playerSelectedTargets = GetPlayerSelectedTargets(card, ability, parentCommand, showDebugLog);
        //
        //     targets.AddRange(playerSelectedTargets);
        //
        //     return targets;
        // }

        if (parentCommand != null && (ability.targetConditions.HasFlag(FilterCondition.PLAYER_SELECTED) || ability.triggerConditions.HasFlag(FilterCondition.PLAYER_SELECTED)))
        {
            var playerSelectedTarget = GetPlayerSelectedTarget(card, ability, parentCommand, showDebugLog);

            if (!string.IsNullOrEmpty(playerSelectedTarget))
            {
                targets.Add(playerSelectedTarget);

                if (ability.targetConditions.HasFlag(FilterCondition.ROW))
                {
                    var targetCard = GameServer.State.GetCard(playerSelectedTarget);
                    var occupiedTilesInRow = GameServer.State.Board.GetOccupiedTilesInSameRow(targetCard.position.y);

                    for (var i = 0; i < occupiedTilesInRow.Count; i++)
                    {
                        var tile = occupiedTilesInRow[i];

                        if (!targets.Contains(tile.CardId) && tile.CardId != playerSelectedTarget)
                        {
                            targets.Add(tile.CardId);
                        }
                    }
                }

                if (ability.targetConditions.HasFlag(FilterCondition.COLUMN))
                {
                    var targetCard = GameServer.State.GetCard(playerSelectedTarget);

                    List<TileState> occupiedTilesInColumn;

                    if (ability.targetConditions.HasFlag(FilterCondition.PLAYER))
                    {
                        occupiedTilesInColumn = GameServer.State.Board.GetOccupiedTilesInColumn(Team.Client, targetCard.position.x);
                    }
                    else if (ability.targetConditions.HasFlag(FilterCondition.OPPONENT))
                    {
                        occupiedTilesInColumn = GameServer.State.Board.GetOccupiedTilesInColumn(Team.Opponent, targetCard.position.x);
                    }
                    else
                    {
                        occupiedTilesInColumn = GameServer.State.Board.GetOccupiedTilesInColumn(targetCard.position.x);
                    }

                    for (var i = 0; i < occupiedTilesInColumn.Count; i++)
                    {
                        var tile = occupiedTilesInColumn[i];

                        if (!targets.Contains(tile.CardId) && tile.CardId != playerSelectedTarget)
                        {
                            targets.Add(tile.CardId);
                        }
                    }
                }

                if (ability.targetConditions.HasFlag(FilterCondition.DIAGONAL_SPACES))
                {
                    var targetCard = GameServer.State.GetCard(playerSelectedTarget);
                    var occupiedTilesInDiagonalSpaces = GameServer.State.Board.GetOccupiedTilesInDiagonalSpaces(targetCard.position);

                    for (var i = 0; i < occupiedTilesInDiagonalSpaces.Count; i++)
                    {
                        var tile = occupiedTilesInDiagonalSpaces[i];

                        if (!targets.Contains(tile.CardId) && tile.CardId != playerSelectedTarget)
                        {
                            targets.Add(tile.CardId);
                        }
                    }
                }
            }

            return targets;
        }

        var filteredTargets = FilterLocation(card, ability, new List<GameCardState>(GameServer.State.Cards));
        if (filteredTargets.Count <= 0)
        {
            if (showDebugLog) Debug.Log($"{ability.keyword}: Failed filter LOCATION");
            return targets;
        }

        filteredTargets = FilterType(card, ability, filteredTargets);
        if (filteredTargets.Count <= 0)
        {
            if (showDebugLog) Debug.Log($"{ability.keyword}: Failed filter TYPE. Targets: {targets.Count}");
            return targets;
        }

        filteredTargets = FilterOwner(card, ability, filteredTargets);
        if (filteredTargets.Count <= 0)
        {
            if (showDebugLog) Debug.Log($"{ability.keyword}: Failed filter OWNER");
            return targets;
        }

        filteredTargets = FilterSpecific(card, ability, filteredTargets);
        if (filteredTargets.Count <= 0)
        {
            if (showDebugLog) Debug.Log($"{ability.keyword}: Failed filter SPECIFIC");
            return targets;
        }

        filteredTargets = FilterAttackType(card, ability, filteredTargets);
        if (filteredTargets.Count <= 0)
        {
            if (showDebugLog) Debug.Log($"{ability.keyword}: Failed filter ATTACK TYPE");
            return targets;
        }

        filteredTargets = FilterMisc(card, ability, filteredTargets);
        if (filteredTargets.Count <= 0)
        {
            if (showDebugLog) Debug.Log($"{ability.keyword}: Failed filter MISC");
            return targets;
        }

        filteredTargets = FilterConditionalValues(card, ability, filteredTargets);

        if (filteredTargets.Count <= 0)
        {
            if (showDebugLog) Debug.Log($"{ability.keyword}: Failed filter CONDITIONAL");
            return targets;
        }

        filteredTargets.RemoveAll(target =>
        {
            if (target.location == CardLocation.Graveyard)
            {
                return false;
            }

            if (target.data == null)
            {
                return false;
            }

            return target.data.type == CardType.Minion && target.GetPendingHealth() <= 0;
        });

        if (filteredTargets.Count <= 0)
        {
            if (showDebugLog) Debug.Log($"{ability.keyword}: Failed filter REMOVING ALL DEAD CARDS.");
            return targets;
        }

        targets.AddRange(filteredTargets.Select(t => t.uid));
        targets = targets.Distinct().ToList();

        if (ability.targetConditions.HasFlag(FilterCondition.MAX_TARGET_COUNT) && targets.Count > ability.filterValues.maxCount)
        {
            var removeCount = targets.Count - ability.filterValues.maxCount;

            if (showDebugLog)
            {
                Debug.Log($"{targets.Count} - {ability.filterValues.maxCount} = (Removals) {removeCount}");
            }

            //var random = new System.Random(System.Guid.NewGuid().GetHashCode());

            for (var i = 0; i < removeCount; i++)
            {
                targets.RemoveAt(random.Next(0, targets.Count));
            }
        }
        else if (ability.targetConditions.HasFlag(FilterCondition.ADVANCED_FILTERS) &&
                 ability.filterValues.maxCountBasedOnPreviousCommandTargetCount)
        {
            var removeCount = targets.Count - previousCommand.TargetUids.Count;

            if (showDebugLog)
            {
                Debug.Log($"{targets.Count} - {previousCommand.TargetUids.Count} = (Removals) {removeCount}");
            }

            for (var i = 0; i < removeCount; i++)
            {
                targets.RemoveAt(new System.Random().Next(0, targets.Count));
            }
        }
        else if (ability.targetConditions.HasFlag(FilterCondition.ALL_BUT_ONE) && targets.Count > 2)
        {
            var target = targets[new System.Random().Next(0, targets.Count)];
            targets.Remove(target);
        }

        if (targets.Count <= 0)
        {
            if (showDebugLog) Debug.Log($"[{ability.keyword}] Failed filter MAX_TARGET_COUNT or ALL_BUT_ONE.");
            return targets;
        }

        if (showDebugLog)
        {
            Debug.Log($"[{ability.keyword}] {card.data?.name} target count = {targets.Count}");
        }

        for (var i = 0; i < targets.Count; i++)
        {
            var target = GameServer.State.GetCard(targets[i]);

            if (showDebugLog)
            {
                var owner = target.owner == GameServer.ClientPlayerState.Uid ? "Client" : "Opponent";
                Debug.Log($"[{ability.keyword}] target[{i}] = {target.characterData.name} ({target.location}) (OWNER = {owner})");
            }
        }

        return targets;
    }

    private static List<GameCardState> FilterLocation(GameCardState card, Ability ability, List<GameCardState> filtered)
    {
        var locationFilter = new List<GameCardState>();


        if (ability.targetConditions.HasFlag(FilterCondition.BOARD))
        {
            locationFilter.AddRange(filtered.FindAll(x => x != null && x.location == CardLocation.Board && x.GetPendingHealth() > 0 && !x.isPendingDeath));
        }

        if (ability.targetConditions.HasFlag(FilterCondition.NOT_PENDING_DEAD))
        {
            locationFilter.RemoveAll(x => x == null || x.isPendingDeath);
        }

        if (ability.targetConditions.HasFlag(FilterCondition.HAND))
        {
            var handCards = filtered.FindAll(x => x != null && x.location == CardLocation.Hand);
            locationFilter.AddRange(handCards);
        }

        if (ability.targetConditions.HasFlag(FilterCondition.DECK))
        {
            locationFilter.AddRange(filtered.FindAll(x => x != null && x.location == CardLocation.Deck));
        }

        if (ability.targetConditions.HasFlag(FilterCondition.GRAVEYARD))
        {
            locationFilter.AddRange(filtered.FindAll(x => x != null && x.location == CardLocation.Graveyard));
        }

        if (ability.targetConditions.HasFlag(FilterCondition.COLUMN) && card.characterData.type == CardType.Minion)
        {
            locationFilter.RemoveAll(x => BoardState.BoardPositionToSimpleMap[x.position].x != BoardState.BoardPositionToSimpleMap[card.position].x);
        }

        if (ability.targetConditions.HasFlag(FilterCondition.ROW) && card.characterData.type == CardType.Minion)
        {
            locationFilter.RemoveAll(x => BoardState.BoardPositionToSimpleMap[x.position].y != BoardState.BoardPositionToSimpleMap[card.position].y);
        }

        if (!ability.targetConditions.HasFlag(FilterCondition.BOARD) &&
            !ability.targetConditions.HasFlag(FilterCondition.HAND) &&
            !ability.targetConditions.HasFlag(FilterCondition.DECK) &&
            !ability.targetConditions.HasFlag(FilterCondition.GRAVEYARD))
        {
            locationFilter.AddRange(filtered);
        }

        return locationFilter;
    }

    private static List<GameCardState> FilterType(GameCardState card, Ability ability, List<GameCardState> filtered)
    {
        var filter = new List<GameCardState>();

        if (ability.targetConditions.HasFlag(FilterCondition.MINION))
        {
            filter.AddRange(filtered.FindAll(x => x != null && !x.IsHero && x.data.type == CardType.Minion));
        }

        if (ability.targetConditions.HasFlag(FilterCondition.HERO))
        {
            filter.AddRange(filtered.FindAll(x => x != null && x.IsHero));
        }

        if (ability.targetConditions.HasFlag(FilterCondition.SPELL))
        {
            filter.AddRange(filtered.FindAll(x => x != null && !x.IsHero && x.data?.type == CardType.Spell));
        }

        if (ability.targetConditions.HasFlag(FilterCondition.TRICK))
        {
            filter.AddRange(filtered.FindAll(x => x != null && !x.IsHero && x.data?.type == CardType.Trick));
        }

        return filter;
    }

    private static List<GameCardState> FilterOwner(GameCardState card, Ability ability, List<GameCardState> filtered)
    {
        var locationFilter = new List<GameCardState>();

        if (ability.targetConditions.HasFlag(FilterCondition.SAME_TEAM))
        {
            locationFilter.AddRange(filtered.FindAll(x => x != null && x.Team == card.Team));
        }

        if (ability.targetConditions.HasFlag(FilterCondition.PLAYER))
        {
            locationFilter.AddRange(filtered.FindAll(x => x != null && x.owner == card.owner));
        }

        if (ability.targetConditions.HasFlag(FilterCondition.OPPONENT))
        {
            locationFilter.AddRange(filtered.FindAll(x => x != null && x.owner != card.owner));
        }

        if (!ability.targetConditions.HasFlag(FilterCondition.PLAYER) &&
            !ability.targetConditions.HasFlag(FilterCondition.OPPONENT) &&
            !ability.targetConditions.HasFlag(FilterCondition.SAME_TEAM))
        {
            locationFilter.AddRange(filtered);
        }

        return locationFilter;
    }

    private static List<GameCardState> FilterSpecific(GameCardState card, Ability ability, List<GameCardState> filtered)
    {
        var specificFilter = new List<GameCardState>();

        if (ability.targetConditions.HasFlag(FilterCondition.ALL))
        {
            specificFilter.AddRange(filtered);
        }

        if (ability.targetConditions.HasFlag(FilterCondition.EXCLUDE_SELF))
        {
            filtered.RemoveAll(x => x != null && x.uid == card.uid);
        }

        if (ability.targetConditions.HasFlag(FilterCondition.SELF))
        {
            specificFilter.Add(filtered.Find(x => x != null && x.uid == card.uid));
        }

        if (ability.targetConditions.HasFlag(FilterCondition.LAST))
        {
            specificFilter.AddRange(
                GameServer.State.GetLastSummonMinions().Select(uid => GameServer.State.GetCard(uid)));
        }

        if (ability.targetConditions.HasFlag(FilterCondition.PLAYER_SELECTED))
        {
            var selection = GameServer.State.GetCard(PlayerSelections.CharacterSelectionUid);
            if (selection != null) specificFilter.Add(selection);
        }

        if (!ability.targetConditions.HasFlag(FilterCondition.ALL) &&
            !ability.targetConditions.HasFlag(FilterCondition.SELF) &&
            !ability.targetConditions.HasFlag(FilterCondition.LAST))
        {
            specificFilter.AddRange(filtered);
        }

        return specificFilter;
    }

    private static List<GameCardState> FilterAttackType(GameCardState card, Ability ability, List<GameCardState> filtered)
    {
        //var locationFilter = new List<GameCardState>();

        // if (ability.targetConditions.HasFlag(FilterCondition.IS_MELEE))
        // {
        //     locationFilter.AddRange(filtered.FindAll(x => x != null && x.data.attackType == AttackType.Melee));
        // }

        // if (ability.targetConditions.HasFlag(FilterCondition.IS_RANGED))
        // {
        //     locationFilter.AddRange(filtered.FindAll(x => x != null && x.data.attackType == AttackType.Ranged));
        // }

        // if (ability.targetConditions.HasFlag(FilterCondition.IS_ARCANE))
        // {
        //     locationFilter.AddRange(filtered.FindAll(x => x != null && x.data.attackType == AttackType.Arcane));
        // }
        //
        // if (/*!ability.targetConditions.HasFlag(FilterCondition.IS_MELEE) &&*/
        //     /*!ability.targetConditions.HasFlag(FilterCondition.IS_RANGED) &&*/
        //     !ability.targetConditions.HasFlag(FilterCondition.IS_ARCANE))
        // {
        //     locationFilter.AddRange(filtered);
        // }

        return filtered;
    }

    private static List<GameCardState> FilterMisc(GameCardState card, Ability ability, List<GameCardState> filtered)
    {
        var locationFilter = new List<GameCardState>();

        if (ability.targetConditions.HasFlag(FilterCondition.IS_DAMAGED))
        {
            locationFilter.AddRange(filtered.FindAll(x =>
                x != null && x.GetPendingHealth() < x.GetDatabaseMaxHealth()));
        }

        if (!ability.targetConditions.HasFlag(FilterCondition.IS_DAMAGED))
        {
            locationFilter.AddRange(filtered);
        }

        return locationFilter;
    }

    private static List<GameCardState> FilterConditionalValues(GameCardState card, Ability ability, List<GameCardState> filtered)
    {
        var locationFilter = new List<GameCardState>();

        if (ability.targetConditions.HasFlag(FilterCondition.ADVANCED_FILTERS))
        {
            var filteredAny = false;

            if (ability.filterValues.faction != Faction.All && ability.filterValues.faction != Faction.None)
            {
                locationFilter.AddRange(filtered.FindAll(x =>
                    x != null && x.data != null && x.data.faction == ability.filterValues.faction));
                filteredAny = true;
            }

            if (ability.filterValues.@class != Class.All && ability.filterValues.@class != Class.None)
            {
                locationFilter.AddRange(filtered.FindAll(x =>
                    x != null && x.data != null && x.data.@class == ability.filterValues.@class));
                filteredAny = true;
            }

            if (ability.filterValues.statusEffect > 0)
            {
                if (ability.filterValues.doesNotHaveStatusEffect)
                {
                    locationFilter.AddRange(filtered.FindAll(x =>
                        x != null && !x.HasStatusEffect(ability.filterValues.statusEffect)));
                }
                else
                {
                    locationFilter.AddRange(filtered.FindAll(x =>
                        x != null && x.HasStatusEffect(ability.filterValues.statusEffect)));
                }

                filteredAny = true;
            }

            if (ability.filterValues.useExactTilePositions)
            {
                locationFilter.AddRange(filtered.FindAll( x =>
                    x.location == CardLocation.Board && ability.filterValues.tilePositions.Contains(x.position)));
                filteredAny = true;

            }

            if (ability.filterValues.notFromSameFaction)
            {
                locationFilter.AddRange( filtered.FindAll( x => x.characterData.faction != Faction.Neutral &&
                                                                x.characterData.faction != GameServer.State.GetHero(x.Team).characterData.faction));
                filteredAny = true;
            }

            if (ability.filterValues.stat != FilterConditionValues.Stat.None && ability.filterValues.statFormula != FilterConditionValues.StatFormula.None)
            {
                locationFilter.AddRange(filtered.FindAll(c =>
                {
                    var score = 0;

                    switch (ability.filterValues.stat)
                    {
                        case FilterConditionValues.Stat.Health:
                            score = c.GetPendingHealth();
                            if (c.HasAbility(AbilityKeyword.TOUGH))
                            {
                                score++;
                            }
                            break;
                        case FilterConditionValues.Stat.Power:
                            score = c.GetPendingPower();
                            break;
                        case FilterConditionValues.Stat.ManaCost:
                            score = c.GetCost();
                            break;
                    }

                    switch (ability.filterValues.statFormula)
                    {
                        case FilterConditionValues.StatFormula.LessThanOrEqualTo:
                            return score <= ability.filterValues.statScore;
                        case FilterConditionValues.StatFormula.LessThan:
                            return score < ability.filterValues.statScore;
                        case FilterConditionValues.StatFormula.GreaterThanOrEqualTo:
                            return score >= ability.filterValues.statScore;
                        case FilterConditionValues.StatFormula.GreaterThan:
                            return score > ability.filterValues.statScore;
                        case FilterConditionValues.StatFormula.Equal:
                            return score == ability.filterValues.statScore;
                    }

                    return false;
                }));

                filteredAny = true;
            }

            if (ability.filterValues.keyword != AbilityKeyword.NONE)
            {
                if (ability.filterValues.doesNotHaveKeyword)
                {
                    locationFilter.AddRange(filtered.FindAll(c => !c.HasAbility(ability.filterValues.keyword)));
                }
                else
                {
                    locationFilter.AddRange(filtered.FindAll(c => c.HasAbility(ability.filterValues.keyword)));
                }

                filteredAny = true;
            }

            if (ability.filterValues.trigger != TriggerType.NONE)
            {
                if (ability.filterValues.doesNotHaveTrigger)
                {
                    locationFilter.AddRange(filtered.FindAll(c => !c.HasTrigger(ability.filterValues.trigger)));
                }
                else
                {
                    locationFilter.AddRange(filtered.FindAll(c => c.HasTrigger(ability.filterValues.trigger)));
                }

                filteredAny = true;
            }

            if (!filteredAny)
            {
                locationFilter.AddRange(filtered);
            }
        }

        if (!ability.targetConditions.HasFlag(FilterCondition.ADVANCED_FILTERS))
        {
            locationFilter.AddRange(filtered);
        }

        return locationFilter;
    }
}