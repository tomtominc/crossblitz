using System.Collections.Generic;
using System.Linq;
using Asyncoroutine;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    public class GiveAbilityCommand : Command
    {
        // should only be used on SELF
        public bool replaceAbility;
        public int replaceAbilityIndex;
        public string givenFromCardId; // used if the ability was "from" another card, this is only possible if the ability has "usesRandomGivenAbility"

        public List<Ability> AbilitiesToGive;

        public GiveAbilityCommand()
        {
            Type = CommandType.GIVE_ABILITY;
        }

        public override bool IsEquivalent(Command command)
        {
            if (command is GiveAbilityCommand cmd)
            {
                if (cmd.AbilitiesToGive.Count != AbilitiesToGive.Count) return false;

                for (var i = 0; i < cmd.AbilitiesToGive.Count; i++)
                {
                    if (cmd.AbilitiesToGive[i].keyword != AbilitiesToGive[i].keyword) return false;
                }

                var cmdSource = GameServer.MainState.GetCard(cmd.SourceUid);
                var source = GameServer.MainState.GetCard(SourceUid);

                return cmdSource.characterData.id == source.characterData.id;
            }

            return false;
        }

        public override string GetLabelDisplay()
        {
            var source = GameServer.State.GetCard(SourceUid);
            return base.GetLabelDisplay() + $"{source.characterData.name} gave {GetTargetsAsString(this)} these abilities: {string.Join(",", AbilitiesToGive.Select(ability => ability.keyword))}.";
        }

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            return AbilitiesToGive.Count;
        }

        public override void OnPrepareCommand()
        {

        }

        public override void OnExecuteCommand()
        {
            if (AI.Thinking)
            {
                // this execute is too slow and the ai doesnt calculate based on abilities usually
                return;
            }

            //Debug.LogError($"Thinking? {AI.Thinking}");

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var target = GameServer.State.GetCard(TargetUids[i]);
                var abilities = target.GetAbilities();

                if (AbilitiesToGive.Exists(a => a.keyword == AbilityKeyword.THORNS) &&
                    abilities.Exists( a => a.keyword == AbilityKeyword.THORNS))
                {
                    target.thornsIncrease += AbilitiesToGive.Find(a => a.keyword == AbilityKeyword.THORNS).damageValue;
                }
                else if (replaceAbility && AbilitiesToGive.Count > 0)
                {
                    target.ReplaceAbility(replaceAbilityIndex, AbilitiesToGive[replaceAbilityIndex]);
                }
                else
                {
                    target.AddAbilities(AbilitiesToGive, Guid);
                }

                // if (AbilitiesToGive.Exists(a => a.keyword == AbilityKeyword.BIDE))
                // {
                //     target.turnSummonedOnBoard = GameServer.State.GetTurnCount(target.Team);
                // }
            }
        }

        // ReSharper disable Unity.PerformanceAnalysis
        public override IEnumerator<float> Resolve()
        {
            var shouldWait = false;

            if (!string.IsNullOrEmpty(givenFromCardId))
            {
                GameplayScreen.Instance.opponentPlayedCardPreview.SpawnCardPreview(givenFromCardId);
            }

            var ability = GetAbility();

            if (ability != null && ability.usesCustomEffect && ability.usesBoardEffect)
            {
                yield return Timing.WaitUntilDone(PlayCustomOrGenericEffectOnBoardCard(ability, this, string.Empty));
            }
            else
            {
                for (var i = 0; i < TargetUids.Count; i++)
                {
                    var targetCard = GameServer.State.GetCard(TargetUids[i]);
                    var boardCard = GameplayScreen.Instance.GetBoardCard(targetCard.uid, out _, out _);

                    if (boardCard == null && targetCard.location == CardLocation.Board)
                    {
                        var getBoardCardTask = GameBoard.GetBoardCard(TargetUids[i]);
                        yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                        var getBoardCardResult = getBoardCardTask.Result;

                        if (getBoardCardResult.error != ClientErrorCode.None)
                        {
                            Server.HandleClientError(getBoardCardResult, false);
                        }

                        boardCard = getBoardCardResult.boardCard;
                    }

                    if (boardCard == null)
                    {
                        continue;
                    }

                    shouldWait = true;
                    boardCard.UpdateViewFromState();

                    if (ability != null && ability.usesCustomEffect)
                    {
                        yield return Timing.WaitUntilDone(
                            PlayCustomOrGenericEffectOnBoardCard(ability, this, boardCard.State.uid, boardCard));
                    }

#pragma warning disable CS4014
                    boardCard.ShowAbilityEffect();
#pragma warning restore CS4014
                }

                if (shouldWait) yield return Timing.WaitForSeconds(1);
            }
        }
    }
}
