using System.Collections.Generic;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;

namespace CrossBlitz.ClientAPI.Commands
{
    public class HeroPowerCinematicCommand : Command
    {
        public HeroPowerCinematicCommand() => Type = CommandType.HERO_POWER_CINEMATIC;

        public override string GetLabelDisplay()
        {
            return base.GetLabelDisplay() + $"{GetPlayerName()} used their Blitz Burst!";
        }

        public override IEnumerator<float> Resolve()
        {
            if (IsClientCommand())
            {
                yield return Timing.WaitUntilDone(
                    GameplayScreen.Instance.heroPowerCinematic
                        .PlayCinematic(GameServer.ClientPlayerState.heroUid, GameServer.State.GetCard(SourceUid)));
            }
            else
            {
                yield return Timing.WaitUntilDone(
                    GameplayScreen.Instance.opponentHeroPowerCinematic
                        .PlayCinematic(GameServer.OpponentPlayerState.heroUid, GameServer.State.GetCard(SourceUid)));
            }
        }
    }
}