using System;
using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI.GameLogic;
using GameDataEditor;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    public class ReleaseBrassCommand : Command
    {
        private string BrassUid;

        public ReleaseBrassCommand() => Type = CommandType.RELEASE_BRASS;

        public override void OnExecuteCommand()
        {
            base.OnExecuteCommand();

            BrassUid = string.Empty;

            var minionsOnField = GameServer.State.Board.GetOccupiedTiles(GetTeam());

            for (var i = 0; i < minionsOnField.Count; i++)
            {
                var minion = GameServer.State.GetCard(minionsOnField[i].CardId);

                if (minion.characterData?.id == GDEItemKeys.Card_AdmiralBrass)
                {
                    minion.ChangeOwners(Team.Client);
                    BrassUid = minion.uid;
                    Debug.LogError("Got card state!");
                    break;
                }
            }

            if (!string.IsNullOrEmpty(BrassUid))
            {
                global::GameLogic.ExecuteCommand(new MoveMinionToZone
                {
                    PlayerUid = GameServer.ClientPlayerState.Uid,
                    TeamLocation = Team.Client,
                    NewLocation = CardLocation.Hand,
                    SourceUid = BrassUid,
                    TargetUids = new List<string> { BrassUid }
                });
            }
        }

        public override IEnumerator<float> Resolve()
        {
            yield break;
            // if (string.IsNullOrEmpty(BrassUid))
            // {
            //     yield break;
            // }
            //
            // var cardTask = GameBoard.GetBoardCard(BrassUid);
            // yield return Timing.WaitUntilDone(cardTask.AsCoroutine());
            // var card = cardTask.Result.boardCard;
            //
            // if (card == null)
            // {
            //     yield break;
            // }
            //
            // var reference = AddressableReferenceLoader.GetAsset("Card_Standard", true);
            // yield return Timing.WaitUntilDone(reference.AsCoroutine());
            //
            // var settings = new CreateCardFactorySettings
            // {
            //     CardPrefab = reference.Result,
            //     Layout = GameplayScreen.Instance.handView.RectTransform(),
            //     AdditionalComponents = CardView.GetRequiredComponentsForStandardHandCard(),
            //     StartsDisabled = true,
            //     CardState = card.State,
            //     SortingOrder = GameCardView.HandSortingOrder + 20
            // };
            //
            // var standardCardView = CardFactory.Create(settings, null);
            // standardCardView.transform.position = card.transform.position;
            // standardCardView.SetAlpha(0);
            // standardCardView.SetActive(true);
            //
            // yield return Timing.WaitUntilDone(card.TransformToStandardCardViewFromBoardCard(standardCardView));
            //
            // UnityEngine.Object.Destroy(card.gameObject);
            // GameplayScreen.Instance.handView.TrackCard(standardCardView.GetCardComponent<GameCardView>());
            // yield return Timing.WaitForSeconds(0.5f);
            // standardCardView.SetInteractable(true);
        }
    }
}