using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ServerAPI.GameLogic;

namespace CrossBlitz.ClientAPI.Commands
{
    public class AuraCommand : Command
    {
        public int abilityIndex;
        public AuraCommand() => Type = CommandType.AURA;

        public override string GetLabelDisplay()
        {
            var sourceCard = GameServer.State.GetCard(SourceUid);
            var ability = sourceCard.GetAbilities()[abilityIndex];
            return $"{base.GetLabelDisplay()}{sourceCard.characterData.name} added aura, {ability.auraData.auraEffect}.";
        }

        public override void OnExecuteCommand()
        {
            var source = GameServer.State.GetCard(SourceUid);
            GameServer.State.AuraUpdater.AddAura(source, new List<Ability> { source.GetAbilities()[abilityIndex] }, source.data.GetGivenAbilities() );
        }
    }
}