using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using CrossBlitz.ServerAPI;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    public class AnimateEffectCommand : Command
    {
        public string effectType;
        public float timeBetweenEffects;
        public float waitBeforeSpawningEffect;
        public float waitAfterEffectFinishes;

        public AnimateEffectCommand() => Type = CommandType.ANIMATE_EFFECT;

        public override void OnExecuteCommand()
        {
        }

        public override IEnumerator<float> Resolve()
        {
            for (var i = 0; i < TargetUids.Count; i++)
            {
                var targetOfEffect = TargetUids[i];
                var getBoardCardTask = GameBoard.GetBoardCard(targetOfEffect);
                yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                var getBoardCardResult = getBoardCardTask.Result;

                if (getBoardCardResult.error != ClientErrorCode.None)
                {
                    Server.HandleClientError(getBoardCardResult, false);
                }

                var targetBoardCard = getBoardCardResult.boardCard;

                if (targetBoardCard)
                {
                    var vfxType = System.Type.GetType(effectType);
                    var vfxComponent = (GameVfxComponent)targetBoardCard.GetComponent(vfxType);

                    if (vfxComponent == null || vfxComponent.Effect == null)
                    {
                        vfxComponent = targetBoardCard.View.AddCardComponent<GameVfxComponent>(vfxType);
                        vfxComponent.Initialize(targetBoardCard.View);
                        vfxComponent.OnAfterCreation();
                    }

                    while (vfxComponent.Effect == null)
                    {
                        yield return Timing.WaitForOneFrame;
                    }

                    if (vfxComponent != null)
                    {
                        yield return Timing.WaitUntilDone(vfxComponent.Effect.Play(this, new VfxParameters()));
                        yield break;
                    }
                }
            }

            yield return Timing.WaitForSeconds(0.1f);
        }
    }
}