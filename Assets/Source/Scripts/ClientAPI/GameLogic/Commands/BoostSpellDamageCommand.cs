using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ServerAPI.GameLogic;

namespace CrossBlitz.ClientAPI.Commands
{
    public class BoostSpellDamageCommand : Command
    {
        public int boostAmount;

        public BoostSpellDamageCommand() => Type = CommandType.BOOST_SPELL_DAMAGE;

        public override void OnExecuteCommand()
        {
            for (var i = 0; i < TargetUids.Count; i++)
            {
                var target = GameServer.State.GetCard(TargetUids[i]);
                var targetAbilities = target.GetAbilities();

                for (var j = 0; j < targetAbilities.Count; j++)
                {
                    var ability = targetAbilities[j];

                    if (ability.keyword == AbilityKeyword.AURA && ability.auraData.auraEffect == AuraData.AuraEffect.SpellDamage)
                    {
                        var abilityState = target.GetAbilityState(ability);
                        if ( abilityState != null ) abilityState.spellDamageIncrease += boostAmount;
                    }
                }
            }
        }

        public override IEnumerator<float> Resolve()
        {
            //todo: need some sort of burst for the cards that are in hand and on the field.
            yield break;
        }
    }
}