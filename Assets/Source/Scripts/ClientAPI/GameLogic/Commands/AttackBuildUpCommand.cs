using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    public class AttackBuildUpCommand : Command
    {
        public AttackBuildUpCommand()
        {
            Type = CommandType.ATTACK_BUILD_UP;
        }

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            return 0;
        }

        public override string GetLabelDisplay()
        {
            return string.Empty;
        }

        public override void OnExecuteCommand()
        {
            
        }


        public override void OnPrepareCommand()
        {
            
        }

        public override void Pause()
        {
            
        }

        public override void Resume()
        {
            
        }

        public override void AddHeldCommands(PendingCommand pendingCommand)
        {
            
        }

        public override IEnumerator<float> Resolve()
        {
            var getBoardCardTask = GameBoard.GetBoardCard(SourceUid);
            yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
            var getBoardCardResult = getBoardCardTask.Result;

            if (getBoardCardResult.error != ClientErrorCode.None)
            {
                Server.HandleClientError(getBoardCardResult, true);
            }

            if (!getBoardCardResult.boardCard)
            {
                yield break;
            }

            var sourceBoardCard = getBoardCardResult.boardCard;
            var attackFx = (CustomAttackEffect)sourceBoardCard.View.GetVfx<AttackEffectComponent, AttackEffect>();

            if (sourceBoardCard.State.data.hasCustomAttackProperties)
            {
                attackFx = (CustomAttackEffect)((CustomAttackComponent)sourceBoardCard.GetComponent(
                    System.Type.GetType(sourceBoardCard.State.data.customAttackEffect))).Effect;
            }
            
            var attackDirection = GetTeam() == Team.Client ? new Vector2(1, 1) : new Vector2(1, -1);
            yield return Timing.WaitUntilDone(attackFx.AttackBuildUp(attackDirection));

            yield return Timing.WaitForOneFrame;
        }
    }
}