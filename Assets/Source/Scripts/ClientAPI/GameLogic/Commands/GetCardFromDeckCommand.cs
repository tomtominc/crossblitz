using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Data;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.GameVfx;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ClientAPI.Commands
{
    public class GetCardFromDeckCommand : Command
    {
        public GetCardFromDeckCommand() => Type = CommandType.GET_CARD_FROM_DECK;

        public override string GetLabelDisplay()
        {
            return $"Player {PlayerUid} got a card from their deck!";
        }

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            return TargetUids.Count;
        }

        public List<string> GetCardsFromDeckWithFilter(CardFilter filter, int count)
        {
            var playerState = IsClientCommand() ? GameServer.ClientPlayerState : GameServer.OpponentPlayerState;
            var choices = new List<string>();
            var random = new System.Random();

            for (var i = 0; i < playerState.deck.Count; i++)
            {
                var card = GameServer.State.GetCard(playerState.deck[i]);

                if (filter.fromClass != Class.None && filter.fromClass != Class.All)
                {
                    if (card.data.@class == filter.fromClass)
                    {
                        choices.Add(playerState.deck[i]);
                    }
                }

                if (filter.fromType != CardType.None)
                {
                    if (card.data.type == filter.fromType)
                    {
                        choices.Add(playerState.deck[i]);
                    }
                }
            }

            choices.Shuffle(random);

            var cards = new List<string>();
            var addingChoices = Mathf.Min(choices.Count, count);

            for (var i = 0; i < addingChoices; i++)
            {
                cards.Add(choices[i]);
            }

            return cards;
        }

        public override void OnExecuteCommand()
        {
            for (var i = 0; i < TargetUids.Count; i++)
            {
                var playerState = IsClientCommand() ? GameServer.ClientPlayerState : GameServer.OpponentPlayerState;

                if (playerState.GetHandCount() >= PlayerBattleState.MaxHandSize)
                {
                    var overdrawnCard = TargetUids[i];

                    global::GameLogic.ExecuteCommand( new OverdrawCommand
                    {
                        SourceUid = SourceUid,
                        PlayerUid = PlayerUid,
                        TargetUids = new List<string> { overdrawnCard  }
                    });

                    continue;
                }

                playerState.AddCardToHand(TargetUids[i], true);

                global::GameLogic.ResolveTriggeredCards(null, null, TriggerType.DRAW_CARD);
            }
        }

        public override IEnumerator<float> Resolve()
        {
            if (!string.IsNullOrEmpty(SourceUid))
            {
                var sourceCard = GameServer.State.GetCard(SourceUid);
                Ability getCardFromDeckAbility = null;

                if (sourceCard.GetAbilities().Count > 0)
                {
                    getCardFromDeckAbility = sourceCard.GetAbilities()
                        .Find(ability => ability.keyword == AbilityKeyword.GET_CARD_FROM_DECK);
                }

                if (getCardFromDeckAbility != null)
                {
                    yield return Timing.WaitUntilDone(PlayCustomOrGenericEffectOnBoardCard(getCardFromDeckAbility, this, SourceUid));
                }
            }

            for (var i = 0; i < TargetUids.Count; i++)
            {
                yield return Timing.WaitUntilDone(DrawCardCommand.DrawCard(TargetUids[i], GetTeam(),false, i==TargetUids.Count-1));
                Events.Publish(this, EventType.OnCardDrawn, new OnCardDrawnEventArgs { team = GetTeam(), drawCount = 1, mulligan = false });
            }
        }
    }
}