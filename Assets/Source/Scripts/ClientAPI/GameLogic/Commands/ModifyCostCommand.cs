using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    public class ModifyCostCommand : Command
    {
        public int ModifyAmount;
        public List<CardHistory> PendingManaModifierHistories;

        public ModifyCostCommand() => Type = CommandType.MODIFY_COST;

        public override string GetLabelDisplay()
        {
            var source = GameServer.State.GetCard(SourceUid);
            return base.GetLabelDisplay() + $"{source.characterData.name} modified {GetTargetsAsString(this)} mana cost by {ModifyAmount}.";
        }

        public static int GetManaModifier(string sourceUid, Ability ability, Team team)
        {
            var mana = 0;

            // ONLY ADD CONDITIONS ARE ARE USED FOR MODIFY STATS! Condition Enum is used for other things too - e.g. conditional abilities.
            // Admiral Brass = Class On Field
            // Merow = Highest Attack Minion
            // Abraxas = Discarded this game.
            switch (ability.abilityCondition.condition)
            {
                case Condition.CLASS_ON_FIELD:
                    List<TileState> occupiedTiles;

                    if (ability.armorCondition.filters.HasFlag(FilterCondition.PLAYER) &&
                        ability.armorCondition.filters.HasFlag(FilterCondition.OPPONENT))
                    {
                        occupiedTiles = GameServer.State.Board.GetOccupiedTiles();
                    }
                    else if (ability.armorCondition.filters.HasFlag(FilterCondition.PLAYER))
                    {
                        occupiedTiles = GameServer.State.Board.GetOccupiedTiles( team );
                    }
                    else
                    {
                        occupiedTiles = GameServer.State.Board.GetOccupiedTiles( team  == Team.Client ? Team.Opponent : Team.Client );
                    }

                    for (var i = 0; i < occupiedTiles.Count; i++)
                    {
                        if (occupiedTiles[i].Card.data.@class == ability.abilityCondition.@class && occupiedTiles[i].Card.uid != sourceUid)
                        {
                            mana += ability.manaModifier;
                        }
                    }
                    break;
                case Condition.DISCARDED_THIS_GAME:
                {
                    var player = GameServer.GetPlayerState(team);
                    mana = player.discardCount * ability.manaModifier;
                    break;
                }
                default:
                    mana = ability.manaModifier;
                    break;
            }

            return mana;
        }

        public override void OnExecuteCommand()
        {
            PendingManaModifierHistories=new List<CardHistory>();

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var targetCard = GameServer.State.GetCard(TargetUids[i]);
                PendingManaModifierHistories.Add(targetCard.ModifyCost(SourceUid, ModifyAmount));
            }
        }

        public override IEnumerator<float> Resolve()
        {
            for (var i = 0; i < PendingManaModifierHistories.Count; i++)
            {
                var targetCard = GameServer.State.GetCard(PendingManaModifierHistories[i].CardUid);

                if (targetCard != null)
                {
                    targetCard.ApplyHistory(PendingManaModifierHistories[i].Uid);
                    Debug.Log($"Mana Modified: {targetCard?.GetDebugName()} {targetCard.GetCost()}");

                    if (targetCard.Team == Team.Client && targetCard.location == CardLocation.Hand)
                    {
                        var handCard = GameplayScreen.Instance.GetHandCard(targetCard.uid);

                        if (handCard)
                        {
                            handCard.UpdateViewFromState();
                            Debug.Log($"Update View Mana Modified: {targetCard?.GetDebugName()} {targetCard.GetCost()}");
                        }
                    }
                }
            }

            yield break;
        }
    }
}