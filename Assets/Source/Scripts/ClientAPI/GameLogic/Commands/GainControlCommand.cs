using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    public class GainControlCommand : Command
    {
        public GainControlCommand() => Type = CommandType.GAIN_CONTROL;

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            return 10;
        }

        public override string GetLabelDisplay()
        {
            return base.GetLabelDisplay() + $"{GetTargetsAsString(this)} were taken.";
        }

        public override void OnExecuteCommand()
        {
            var source = GameServer.State.GetCard(SourceUid);
            var gainControlAbilities = source.GetAbilities( AbilityKeyword.GAIN_CONTROL);

            if (gainControlAbilities.Count <= 0)
            {
                TargetUids.Clear();
                return;
            }

            var unoccupiedTiles = GameServer.State.Board.GetUnOccupiedTiles(GetTeam());

            if (unoccupiedTiles.Count <= 0)
            {
                TargetUids.Clear();
                return;
            }

            var gainControlAbility = GameUtilities.Copy( gainControlAbilities[0] );

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var target = GameServer.State.GetCard(TargetUids[i]);

                if (target == null)
                {
                    continue;
                }

                var controllingTeam = target.Team == Team.Client ? Team.Opponent : Team.Client;

                gainControlAbility.summonMinionData = new SummonMinionData();
                gainControlAbility.summonMinionData.count = 1;
                gainControlAbility.summonMinionData.cardId = target.characterData?.id;
                gainControlAbility.summonMinionData.placement = SummonPlacement.OPPONENT | SummonPlacement.EXACT_POSITION;
                gainControlAbility.summonMinionData.tilePosition = GameServer.State.Board.OppositeSide(target.position);

                //Debug.LogError($"Placement: {gainControlAbility.summonMinionData.placement} (Controlled By? {target.Team} Will go to {(target.Team == Team.Opponent ? Team.Client : Team.Opponent).ToString()}) Curr Pos: {target.position} Invert: {gainControlAbility.summonMinionData.tilePosition }");

                var positions= SummonCommand.GetSummonPositionFromAbility(target, gainControlAbility, null, null, 1);

                if (positions.Count > 0)
                {
                    target.ChangeOwners( controllingTeam );

                    var targetTile = GameServer.State.Board.GetTile(target.position);
                    var moveTile = GameServer.State.Board.GetTile(positions[0]);

                    GameServer.State.Board.SetTile(targetTile.Position, string.Empty );
                    GameServer.State.Board.SetTile(moveTile.Position, target.uid );
                }
            }
        }

        public override IEnumerator<float> Resolve()
        {
            for (var i = 0; i < TargetUids.Count; i++)
            {
                var target = GameServer.State.GetCard(TargetUids[i]);
                var tile = GameServer.State.Board.GetTile(target.uid);
                var getBoardCardTask = GameBoard.GetBoardCard(target.uid);
                yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                var getBoardCardResult = getBoardCardTask.Result;

                if (getBoardCardResult.error != ClientErrorCode.None)
                {
                    Server.HandleClientError(getBoardCardResult, true);
                }

                if (getBoardCardResult.boardCard == null)
                {
                    yield break;
                }

                var boardCard = getBoardCardResult.boardCard;
                // boardCard.View.TintOverTime("#a2162b".ToColor(), BlendModes.BlendMode.VividLight, 0.24f);

                var ability = GetAbility();

                if (ability != null && ability.usesCustomEffect)
                {
                    yield return Timing.WaitUntilDone(PlayCustomOrGenericEffectOnBoardCard(ability, this, boardCard.State.uid, boardCard));
                }

                // shaking!
                for (var j = 0; j < 6; j++)
                {
                    boardCard.ObjectCanvasGroup.RectTransform().anchoredPosition =
                        new Vector2(3 * (j % 2 == 0 ? 1 : -1), 0);
                    yield return Timing.WaitForSeconds(0.04f);
                }
                
                boardCard.ObjectCanvasGroup.RectTransform().anchoredPosition = Vector2.zero;

                yield return Timing.WaitForSeconds(0.24f);

                AudioController.PlaySound("battle_card_draw", "BARDSFX", true, boardCard.gameObject);

                var tileView = GameplayScreen.Instance.gameBoard.GetTile(tile.Position);
                tileView.SetCard(boardCard, 0.6f);
                yield return Timing.WaitForSeconds(0.4f);

                AudioController.PlaySound("battle_card_flip", "BARDSFX", true, boardCard.gameObject);
                yield return Timing.WaitForSeconds(0.6f);

                boardCard.UpdateViewFromState();

                yield return Timing.WaitUntilDone(boardCard.ShowAbilityEffect().AsCoroutine());
            }
        }
    }
}