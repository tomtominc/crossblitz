using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.GameVfx;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    public class TransformCardsCommand : Command
    {
        public TransformCardData TransformCardData;
        public List<string> TransformPendingHistories;

        public TransformCardsCommand() => Type = CommandType.TRANSFORM_CARDS;

        public override string GetLabelDisplay()
        {
            var log = string.Empty;

            if (TransformPendingHistories != null)
            {
                for (var i = 0; i < TransformPendingHistories.Count; i++)
                {
                    var target = GameServer.State.GetCard(TargetUids[i]);
                    var history = target.GetHistory(TransformPendingHistories[i]);
                    var transformedInto = Db.CardDatabase.GetCard(history.TransformedInto);

                    log += $"{target.characterData.name} Transformed into {transformedInto.name}\n";
                }
            }

            return base.GetLabelDisplay() + log;
        }

        public override IEnumerator<float> Resolve()
        {
            if (TransformPendingHistories == null || TargetUids == null ||
                TransformPendingHistories.Count != TargetUids.Count)
            {
                yield break;
            }

            var ability = GetAbility();

            if (ability != null && ability.usesCustomEffect && ability.usesBoardEffect)
            {
                yield return Timing.WaitUntilDone(PlayCustomOrGenericEffectOnBoardCard(ability, this, string.Empty));
                if (ability.exitResolveAfterCustomEffect) yield break;
            }

            yield return Timing.WaitUntilDone(PlayEffect(this, "TransformPoofEffect"));
        }


        public override void OnExecuteCommand()
        {
            if (string.IsNullOrEmpty(SourceUid))
            {
                return;
            }

            if (TargetUids == null || TargetUids.Count <= 0)
            {
                return;
            }

            TransformPendingHistories = new List<string>();

            var rand = new System.Random(System.Guid.NewGuid().GetHashCode());
            var ability = GetAbility();

            if (ability != null && ability.repeatDamageBasedOnConditions)
            {
                var splitTimes =
                    GameVfxUtilities.GetCountWithConditions(ability, ability.repeatCountCondition, GetTeam(), 0);

                if (splitTimes <= 0) return;

                var newTargets = new List<string>();

                for (var i = 0; i < splitTimes; i++)
                {
                    newTargets.Add(TargetUids[rand.Next(0, TargetUids.Count)]);
                }

                TargetUids = newTargets;
            }

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var target = GameServer.State.GetCard(TargetUids[i]);

                global::GameLogic.ResolveTriggeredCards(GameServer.State.GetCard(SourceUid), target,
                    TriggerType.MINION_TRANSFORMED);

                CardData cardData = null;

                switch (TransformCardData.transformType)
                {
                    case TransformCardData.TransformType.SpecificCard:
                    {
                        cardData = Db.CardDatabase.GetCard(TransformCardData.cardId);
                        break;
                    }
                    case TransformCardData.TransformType.ByRarity:
                    {
                        var evolveTargets = Db.CardDatabase.GetCards(card =>
                        {
                            if (card.type != CardType.Minion || card.set == CardSet.All || card.set == CardSet.Token ||
                                card.set == CardSet.Deprecated)
                            {
                                return false;
                            }

                            if (card.attackType == AttackType.Melee &&
                                (target.position.y == 1 || target.position.y == 3))
                            {
                                return false;
                            }

                            if (card.attackType == AttackType.Ranged &&
                                (target.position.y == 0 || target.position.y == 2))
                            {
                                return false;
                            }

                            return TransformCardData.rarity == Rarity.All || card.rarity == TransformCardData.rarity;
                        });

                        cardData = evolveTargets[rand.Next(0, evolveTargets.Count)];

                        break;
                    }
                    case TransformCardData.TransformType.Evolve:
                    {
                        var evolveCostTarget = target.data.cost + TransformCardData.evolveCostChange;

                        if (target.Team != GetTeam())
                        {
                            evolveCostTarget = target.data.cost - TransformCardData.evolveCostChange;
                        }

                        evolveCostTarget = Mathf.Clamp(evolveCostTarget, 1, 11);

                        var evolveTargets = Db.CardDatabase.GetCards(card =>
                        {
                            if (card.type != CardType.Minion || card.set == CardSet.All || card.set == CardSet.Token ||
                                card.set == CardSet.Deprecated)
                            {
                                return false;
                            }

                            // evolving over 10 can roll anything above 10 cost (11+)
                            if (evolveCostTarget > 10 && card.cost <= 10)
                            {
                                return false;
                            }

                            // anything at 10 or less has to equal the cost
                            if (evolveCostTarget <= 10 && card.cost != evolveCostTarget)
                            {
                                return false;
                            }

                            if (card.attackType == AttackType.Melee &&
                                (target.position.y == 1 || target.position.y == 3))
                            {
                                return false;
                            }

                            if (card.attackType == AttackType.Ranged &&
                                (target.position.y == 0 || target.position.y == 2))
                            {
                                return false;
                            }

                            return true;
                        });

                        cardData = evolveTargets[rand.Next(0, evolveTargets.Count)];
                        break;
                    }
                    case TransformCardData.TransformType.CostBased:
                    {
                        var evolveTargets = Db.CardDatabase.GetCards(card =>
                        {
                            if (card.type != CardType.Minion || card.set == CardSet.All ||
                                card.set == CardSet.Token || card.set == CardSet.Deprecated ||
                                TransformCardData.costFormula == FilterConditionValues.StatFormula.None)
                            {
                                return false;
                            }

                            if (card.attackType == AttackType.Melee &&
                                (target.position.y == 1 || target.position.y == 3))
                            {
                                return false;
                            }

                            if (card.attackType == AttackType.Ranged &&
                                (target.position.y == 0 || target.position.y == 2))
                            {
                                return false;
                            }

                            var passes = false;

                            switch (TransformCardData.costFormula)
                            {
                                case FilterConditionValues.StatFormula.Equal:
                                    passes = card.cost == TransformCardData.cost;
                                    break;
                                case FilterConditionValues.StatFormula.GreaterThan:
                                    passes = card.cost > TransformCardData.cost;
                                    break;
                                case FilterConditionValues.StatFormula.LessThan:
                                    passes = card.cost < TransformCardData.cost;
                                    break;
                                case FilterConditionValues.StatFormula.GreaterThanOrEqualTo:
                                    passes = card.cost >= TransformCardData.cost;
                                    break;
                                case FilterConditionValues.StatFormula.LessThanOrEqualTo:
                                    passes = card.cost <= TransformCardData.cost;
                                    break;
                            }

                            return passes;
                        });

                        cardData = evolveTargets[rand.Next(0, evolveTargets.Count)];

                        break;
                    }
                    case TransformCardData.TransformType.LastSummonedMinion:
                    {
                        var lastSummoned = GameServer.State.GetLastHistoryOf(CommandType.SUMMON);

                        // if (lastSummoned != null)
                        // {
                        //     Debug.Log(GameServer.State.GetCard(lastSummoned.SourceUid).characterData.name);
                        // }

                        if (lastSummoned != null && lastSummoned.TargetUids != null &&
                            lastSummoned.TargetUids.Count > 0)
                        {
                            var lastSummonedCardState = GameServer.State.GetCard(lastSummoned.TargetUids[0]);

                            if (lastSummonedCardState != null)
                            {
                                cardData = Db.CardDatabase.GetCard(lastSummonedCardState.characterData.id);
                            }
                        }

                        if (cardData == null && lastSummoned != null && !string.IsNullOrEmpty(lastSummoned.SourceUid))
                        {
                            var lastSummonedCardState = GameServer.State.GetCard(lastSummoned.SourceUid);

                            if (lastSummonedCardState != null)
                            {
                                cardData = Db.CardDatabase.GetCard(lastSummonedCardState.characterData.id);
                            }
                        }

                        break;
                    }
                }

                if (cardData == null)
                {
                    Debug.LogError($"Could not find case for transform type {TransformCardData.transformType}");
                    continue;
                }

                TransformPendingHistories.Add(target.TransformCard(cardData.id, TransformCardData.keepStatsAndUpgradesOfPreviousMinion));
            }
        }

    }
}