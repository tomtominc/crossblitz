using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ClientAPI.Commands
{
    public class FreezeCommand : Command
    {
        public FreezeCommand()
        {
            Type = CommandType.FREEZE;
        }

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            var score = 0;

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var target = GameServer.State.GetCard(TargetUids[i]);

                if (target.Team == Team.Client)
                {
                    score+=2;
                }
                else
                {
                    score-=2;
                }
            }

            return score;
        }

        public override string GetLabelDisplay()
        {
            return base.GetLabelDisplay() + $"{GetTargetsAsString(this)} has been unfrozen";
        }

        public override void OnExecuteCommand()
        {
            var source = GameServer.State.GetCard(SourceUid);

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var target = GameServer.State.GetCard(TargetUids[i]);

                if (target.IsHero) continue;

                target.AddStatusEffect(StatusEffect.Frozen);
                var turnCount = GameServer.State.GetTurnCount(target.Team);
                var currentPlayer = GameServer.GetPlayerState(GameServer.State.CurrentPlayer);
                target.turnFrozen = currentPlayer.GetTeam() != target.Team ? turnCount + 1 : turnCount;
                GameServer.State.FreezeUpdater.AddTarget(TargetUids[i]);

                // putting this here instead of in the AddStatusEffect method because its expensive
                global::GameLogic.ResolveTriggeredCards(source, target, TriggerType.CHARACTER_STATUS_EFFECT_CHANGED);
            }
        }

        public override IEnumerator<float> Resolve()
        {
            var source = GameServer.State.GetCard(SourceUid);

            // OLD SYSTEM
            if (source.data != null && source.data.usesCustomEffect)
            {
                GameCardView parentView = null;

                if (source.characterData.type == CardType.Minion && source.location == CardLocation.Board)
                {
                    var getBoardCardTask = GameBoard.GetBoardCard(SourceUid);
                    yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                    var getBoardCardResult = getBoardCardTask.Result;

                    if (getBoardCardResult.error != ClientErrorCode.None)
                    {
                        Server.HandleClientError(getBoardCardResult, false);
                    }

                    if (getBoardCardResult.boardCard)
                    {
                        parentView = getBoardCardResult.boardCard;
                    }
                }

                var freezeLoadTask = GameVfxProvider.GetVfxForSpell(source, parentView);
                yield return Timing.WaitUntilDone(freezeLoadTask.AsCoroutine());
                var freezeEffect = freezeLoadTask.Result;
                yield return Timing.WaitUntilDone(freezeEffect.Play(this, new VfxParameters()));
            }

            // NEW SYSTEM
            var ability = GetAbility();
            if (ability != null && ability.usesCustomEffect)
            {
                yield return Timing.WaitUntilDone(PlayCustomOrGenericEffectOnBoardCard(ability, this, string.Empty));
            }

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var target = GameServer.State.GetCard(TargetUids[i]);

                if (target.IsHero) continue;

                var getBoardCardTask = GameBoard.GetBoardCard(target.uid);
                yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                var getBoardCardResult = getBoardCardTask.Result;

                var getFreezeEffectTask = getBoardCardResult.boardCard.GetVfx<FreezeEffectComponent>();
                yield return Timing.WaitUntilDone(getFreezeEffectTask.AsCoroutine());

                if (getFreezeEffectTask.Result != null)
                {
                    var freezeEffect = (FreezeEffect)getFreezeEffectTask.Result.Effect;
                    Timing.RunCoroutine(freezeEffect.Freeze());

                    Events.Publish( this, EventType.OnMinionFrozen, new OnMinionFrozenEventArgs { cardUid = TargetUids[i] });
                }
            }

            yield return Timing.WaitForSeconds(0.16f);
        }
    }
}