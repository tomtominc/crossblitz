using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using MEC;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ClientAPI.Commands
{
    [System.Serializable]
    public class EndTurnCommand : Command
    {
        public Team Team;

        public EndTurnCommand()
        {
            Type = CommandType.END_TURN;
        }

        public override string GetLabelDisplay()
        {
            return base.GetLabelDisplay() + $"{GetPlayerName()} ended their turn.";
        }

        public override void OnPrepareCommand()
        {

        }

        public override void OnExecuteCommand()
        {
            var player = GameServer.GetPlayerState(Team);

            var playerHero = GameServer.State.GetCard(player.heroUid);
            global::GameLogic.ResolveTriggeredCards(playerHero, null, TriggerType.TURN_END, new global::GameLogic.ResolveTriggeredCardsProperties { showLog = true });

            for (var col = 0; col < GameBoard.Columns; col++)
            {
                for (var row = 0; row < GameBoard.Rows - 2; row++)
                {
                    var position = new Vector2Int(col, row);
                    var tile = GameServer.State.Board.GetTile(position, Team);

                    if (tile.Occupied && tile.Card.AbleToAttack(position.y))
                    {
                        var source = tile.Card;
                        var attackCount = source.GetAttackCount();

                        for (var i = 0; i < attackCount; i++)
                        {
                            var attack = AttackCommand.GetStandardAttackCommandForCard(PlayerUid, source, position);

                            if (source.HasAbility(AbilityKeyword.SWIFT_STRIKE))
                            {
                                attack.ableToRetaliate = false;
                            }
                            else
                            {
                                attack.ableToRetaliate = true;
                            }

                            // needs to execute the attack for things like dual strike.
                            global::GameLogic.ExecuteCommand(attack);
                        }
                    }
                }
            }

            // reorder all commands so death is last.
            // global::GameLogic.ReorderAllDestroyCommandsToLast();

            // if (!GameServer.IsGameFinished())
            // {
            //     GameServer.State.ChangeTurns();
            // }
        }

        public override IEnumerator<float> Resolve()
        {
            Timing.KillCoroutines("AI");

            yield return Timing.WaitForSeconds(0.25f);

            while (AI.Thinking /*|| Server.CommandResolver.Executing*/)
            {
                yield return Timing.WaitForOneFrame;
            }

            Events.Publish(this, EventType.OnTurnEnd, new OnTurnEndEventArgs() { isClient = IsClientCommand(), totalTurns = GameServer.State.GetTurnCount(IsClientCommand() ? Team.Client : Team.Opponent) });
            GameServer.ChangeGameState(StateDefinition.GAME_COMBAT_PHASE);

            if (GameplayScreen.Instance.IsShowingTargets())
            {
                GameplayScreen.Instance.HideTargets();
            }

            GameplayScreen.Instance.opponentHandView.Hide();
        }
    }
}