using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;

namespace CrossBlitz.ClientAPI.Commands
{
    public class RevealTrapCommand: Command
    {
        public RevealTrapCommand() => Type = CommandType.REVEAL_TRAP;

        public override void OnExecuteCommand()
        {
            var trapCard = GameServer.State.GetCard(SourceUid);

            global::GameLogic.ResolveTriggeredCards(trapCard,null, TriggerType.TRAP_REVEALED );

            if (trapCard != null)
            {
                GameServer.State.Board.RemoveCardFromTile(trapCard.uid, CardLocation.Graveyard);
            }
        }

        public override IEnumerator<float> Resolve()
        {
            var trapCard = GameplayScreen.Instance.GetBoardCard(SourceUid, out _, out _);

            if (trapCard != null)
            {
                var trapEffect = trapCard.View.GetVfx<TrapCardEffectComponents, TrapCardEffect>();
                if (trapEffect)
                {
                    trapEffect.Initialize(trapCard.View);
                    var trapRoutine = Timing.RunCoroutine(trapEffect.Reveal());
                    if (trapRoutine.IsRunning) yield return Timing.WaitForOneFrame;
                    Events.Publish(this,EventType.OnTrapTriggered, new OnTrapTriggeredEventArgs { sourceUid =  SourceUid });
                }
            }

            yield return Timing.WaitForSeconds(2);

            var card = GameServer.State.GetCard(SourceUid);

            var destroyCommand = new DestroyCommand();
            destroyCommand.PlayerUid = card.owner;
            destroyCommand.SourceUid = card.uid;
            destroyCommand.TargetUids = new List<string> { SourceUid };

            global::GameLogic.StartCommand(destroyCommand);
        }
    }
}