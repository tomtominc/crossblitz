using CrossBlitz.ServerAPI.GameLogic;

namespace CrossBlitz.ClientAPI.Commands
{
    public class FinishedMulliganCommand : Command
    {
        public FinishedMulliganCommand()
        {
            Type = CommandType.FINISHED_MULLIGAN;
        }

        public override string GetLabelDisplay()
        {
            return base.GetLabelDisplay() + $"{GetPlayerName()} finished mulligan.";
        }

        public override void OnPrepareCommand()
        {

        }

        public override void OnExecuteCommand()
        {
            if (IsClientCommand())
            {
                GameServer.ClientPlayerState.finishedMulligan = true;
            }
            else
            {
                GameServer.OpponentPlayerState.finishedMulligan = true;
            }
        }
    }
}
