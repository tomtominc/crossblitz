using System.Collections.Generic;
using MEC;

namespace CrossBlitz.ClientAPI.Commands
{
    [System.Serializable]
    public class WaitCommand : Command
    {
        public float waitTime;
        public WaitCommand() => Type = CommandType.WAIT;

        public override IEnumerator<float> Resolve()
        {
            yield return Timing.WaitForSeconds(waitTime);
        }
    }
}