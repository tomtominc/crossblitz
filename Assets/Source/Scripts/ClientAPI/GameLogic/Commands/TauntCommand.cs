using System.Collections.Generic;
using Asyncoroutine;
using BlendModes;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    public class TauntCommand : Command
    {
        public TauntCommand() => Type = CommandType.TAUNT;

        public override string GetLabelDisplay()
        {
            return base.GetLabelDisplay();
        }

        public override void OnExecuteCommand()
        {
            if (TargetUids.Count <= 0) return;

            var source = GameServer.State.GetCard(SourceUid);
            var targetTile = GameServer.State.Board.GetTile(TargetUids[0]);
            var targetCard = targetTile.Card;

            if (targetCard == null) return;

            var invertedPosition = GameServer.State.Board.InvertPosition(source.position);
            var moveTilePosition = new Vector2Int(invertedPosition.x, targetTile.Position.y);
            var moveTile = GameServer.State.Board.GetTile(moveTilePosition);

            if (moveTile.Occupied)
            {
                if (moveTilePosition.y == 0) moveTilePosition.y = 1;
                else if (moveTilePosition.y == 1) moveTilePosition.y = 0;
                else if (moveTilePosition.y == 2) moveTilePosition.y = 3;
                else if (moveTilePosition.y == 3) moveTilePosition.y = 2;

                moveTile = GameServer.State.Board.GetTile(moveTilePosition);
            }

            if (moveTile.Occupied)
            {
                return;
            }

            GameServer.State.Board.SetTile( targetTile.Position, string.Empty );
            GameServer.State.Board.SetTile( moveTile.Position, targetCard.uid );
        }

        public override IEnumerator<float> Resolve()
        {
            if (TargetUids.Count <= 0)
            {
                Debug.LogError("No targets!");
                yield break;
            }

            var target = GameServer.State.GetCard(TargetUids[0]);
            var tile = GameServer.State.Board.GetTile(target.uid);
            var getBoardCardTask = GameBoard.GetBoardCard(target.uid);
            yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
            var getBoardCardResult = getBoardCardTask.Result;

            if (getBoardCardResult.error != ClientErrorCode.None)
            {
                Server.HandleClientError(getBoardCardResult, true);
            }

            if (getBoardCardResult.boardCard == null)
            {
                yield break;
            }

            var boardCard = getBoardCardResult.boardCard;
            boardCard.View.TintOverTime("#a2162b".ToColor(), BlendMode.VividLight, 0.24f);

            for (var i = 0; i < 6; i++)
            {
                boardCard.ObjectCanvasGroup.RectTransform().anchoredPosition =
                    new Vector2(3 * (i % 2 == 0 ? 1 : -1), 0);
                yield return Timing.WaitForSeconds(0.04f);
            }

            boardCard.ObjectCanvasGroup.RectTransform().anchoredPosition = Vector2.zero;

            yield return Timing.WaitForSeconds(0.24f);

            var tileView = GameplayScreen.Instance.gameBoard.GetTile(tile.Position);
            tileView.SetCard(boardCard, 0.6f);
            yield return Timing.WaitForSeconds(1);
            boardCard.UpdateViewFromState();
            boardCard.View.RemoveTintOverTime(0.24f);
        }
    }
}