using System.Collections.Generic;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;

namespace CrossBlitz.ClientAPI.Commands
{
    public class BlitzBurstInstantFillCommand : Command
    {
        public BlitzBurstInstantFillCommand() => Type = CommandType.BLITZ_BURST_INSTANT_FILL;

        public override void OnExecuteCommand()
        {
            base.OnExecuteCommand();

            if (TargetUids == null || TargetUids.Count <= 0)
            {
                TargetUids = new List<string> { PlayerUid };
            }

            Events.Publish(this, EventType.OnBlitzBurstInstantFill, new OnBlitzBurstInstantFillArgs
            {
                playerUid = TargetUids[0]
            });
        }
    }
}