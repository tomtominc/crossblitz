using CrossBlitz.Card;
using CrossBlitz.ServerAPI.GameLogic;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    public class RefillManaCommand : Command
    {
        public RefillManaCommand() => Type = CommandType.REFILL_MANA;

        public override void OnExecuteCommand()
        {
            if (IsClientCommand())
            {
                GameServer.SetMana(Team.Client, GameServer.ClientPlayerState.totalMana, true);
            }
            else
            {
                GameServer.SetMana(Team.Opponent, GameServer.OpponentPlayerState.totalMana, true);
            }
        }
    }
}