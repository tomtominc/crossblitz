using System.Collections.Generic;
using System.Linq;
using Asyncoroutine;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.ServerAPI.GameLogic.Intelligence;
using MEC;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ClientAPI.Commands
{
    public class HealCommand :Command
    {
        public int Amount;
        public List<CardHistory> PendingHealHistories;

        public HealCommand()
        {
            Type = CommandType.HEAL;
        }

        public override string GetLabelDisplay()
        {
            var source = GameServer.State.GetCard(SourceUid);
            return base.GetLabelDisplay() + $"{source.characterData.name} healed {GetTargetsAsString(this)} by {Amount}.";
        }

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            if (TargetUids == null || TargetUids.Count <= 0) return -10;
            if (PendingHealHistories == null || PendingHealHistories.Count <= 0) return -10;

            var score = 0;

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var target = GameServer.State.GetCard(TargetUids[i]);
                var healthWithHeal = target.GetPendingHealth();
                var healthWithoutHeal = target.GetPendingHealth(PendingHealHistories.Select(p => p.Uid).ToList());
                score += healthWithHeal - healthWithoutHeal;
            }

            return score - Amount;
        }

        public override void OnPrepareCommand()
        {

        }

        public static int GetHealingAmount(string sourceUid, Ability ability, Command parentCommand, Command previousCommand, Team team)
        {
            var healAmount = 0;

            // ONLY ADD CONDITIONS ARE ARE USED FOR MODIFY STATS! Condition Enum is used for other things too - e.g. conditional abilities.
            // Admiral Brass = Class On Field
            // Merow = Highest Attack Minion
            // Abraxas = Discarded this game.
            switch (ability.abilityCondition.condition)
            {
                case Condition.CLASS_ON_FIELD:
                    List<TileState> occupiedTiles;

                    if (ability.armorCondition.filters.HasFlag(FilterCondition.PLAYER) &&
                        ability.armorCondition.filters.HasFlag(FilterCondition.OPPONENT))
                    {
                        occupiedTiles = GameServer.State.Board.GetOccupiedTiles();
                    }
                    else if (ability.armorCondition.filters.HasFlag(FilterCondition.PLAYER))
                    {
                        occupiedTiles = GameServer.State.Board.GetOccupiedTiles( team );
                    }
                    else
                    {
                        occupiedTiles = GameServer.State.Board.GetOccupiedTiles( team  == Team.Client ? Team.Opponent : Team.Client );
                    }

                    for (var i = 0; i < occupiedTiles.Count; i++)
                    {
                        if (occupiedTiles[i].Card.data.@class == ability.abilityCondition.@class && occupiedTiles[i].Card.uid != sourceUid)
                        {
                            healAmount += ability.healAmount;
                        }
                    }
                    break;
                case Condition.DISCARDED_THIS_GAME:
                {
                    var player = GameServer.GetPlayerState(team);
                    healAmount = player.discardCount * ability.healAmount;
                    break;
                }
                case Condition.TARGETS_HEALTH:
                {
                    var card = GameServer.State.GetCard(sourceUid);
                    var targetConditions = new TargetConditions
                    {
                        targetCondition = ability.abilityCondition.filters,
                        targetConditionValues = ability.abilityCondition.filterValues
                    };

                    var targets = global::GameLogic.GetTargets(card, ability, parentCommand, previousCommand, true, null, targetConditions);

                    for (var i = 0; i < targets.Count; i++)
                    {
                        var target = GameServer.State.GetCard(targets[i]);
                        healAmount = target.GetPendingHealth() * ability.healAmount;
                    }
                    break;
                }
                case Condition.TARGETS_POWER:
                {
                    var card = GameServer.State.GetCard(sourceUid);
                    var targetConditions = new TargetConditions
                    {
                        targetCondition = ability.abilityCondition.filters,
                        targetConditionValues = ability.abilityCondition.filterValues
                    };

                    var targets = global::GameLogic.GetTargets(card, ability, parentCommand, previousCommand, true, null, targetConditions);

                    for (var i = 0; i < targets.Count; i++)
                    {
                        var target = GameServer.State.GetCard(targets[i]);
                        healAmount = target.GetPendingPower() * ability.healAmount;
                    }
                    break;
                }
                default:
                    healAmount = ability.healAmount;
                    break;
            }

            return healAmount;
        }

        public override void OnExecuteCommand()
        {
            if (TargetUids == null || TargetUids.Count <= 0)
            {
                return;
            }

            PendingHealHistories = new List<CardHistory>();

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var target = GameServer.State.GetCard(TargetUids[i]);
                PendingHealHistories.Add(target.Heal(SourceUid, Amount));
            }
        }

        public override IEnumerator<float> Resolve()
        {
            for (var i = 0; i < PendingHealHistories.Count; i++)
            {
                var target = GameServer.State.GetCard(PendingHealHistories[i].CardUid);

                if (target.IsHero)
                {
                    var heroView = GameplayScreen.Instance.heroSideBarContainer.GetHeroView(target.Team);
                    target.ApplyHistory(PendingHealHistories[i].Uid);
                    heroView.Heal( PendingHealHistories[i] );
                }
                else
                {
                    var cardState = GameServer.State.GetCard(target.uid);

                    if (cardState == null)
                    {
                        Debug.LogError($"Card state isn null!! {PendingHealHistories[i].CardUid}");
                        continue;
                    }

                    cardState.ApplyHistory(PendingHealHistories[i].Uid);

                    var getBoardCardTask = GameBoard.GetBoardCard(cardState.uid);
                    yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                    var getBoardCardResult = getBoardCardTask.Result;

                    if (getBoardCardResult.error != ClientErrorCode.None)
                    {
                        Server.HandleClientError(getBoardCardResult, true);
                    }

                    if (getBoardCardResult.boardCard)
                    {
                        getBoardCardResult.boardCard.State.ApplyHistory(PendingHealHistories[i].Uid);
                        var getModifyEffectTask =
                            getBoardCardResult.boardCard.GetVfx<ModifyHealthAndPowerEffectComponent>();
                        yield return Timing.WaitUntilDone(getModifyEffectTask.AsCoroutine());
                        var getModifyEffectResult = getModifyEffectTask.Result;
                        Timing.RunCoroutine(((ModifyHealthAndPowerEffect) getModifyEffectResult.Effect)
                            .PlayModifyAnimation(0, Amount));
                    }
                }

                Events.Publish(this, EventType.OnCharacterHealed, new OnCharacterHealedEventArgs
                {
                    sourceUid = SourceUid,
                    healedId = target.uid,
                    amount = Amount
                });
            }

            yield return Timing.WaitForSeconds(1);
        }
    }
}
