using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using TMPro;

namespace CrossBlitz.ClientAPI.Commands
{
    public class FocusCommand : Command
    {
        //public int focusIncrement;
        public bool isComplete;

        public FocusCommand() => Type = CommandType.FOCUS;

        public override void OnExecuteCommand()
        {
        }

        public override IEnumerator<float> Resolve()
        {
            if (GetTeam() == Team.Client)
            {
                var sourceCard = GameplayScreen.Instance.GetHandCard(SourceUid);
                var getFocusEffectTask = sourceCard.GetVfx<FocusEffectComponent>();
                yield return Timing.WaitUntilDone(getFocusEffectTask.AsCoroutine());
                var focusEffect = getFocusEffectTask.Result;

                if (isComplete)
                {
                    yield return Timing.WaitUntilDone( ((FocusEffect)focusEffect.Effect).CompleteEffect() );
                }
                else
                {
                    yield return Timing.WaitUntilDone( ((FocusEffect)focusEffect.Effect).SingleEffect() );
                }
            }
            else
            {

                var sourceCard = GameplayScreen.Instance.opponentHandView.GetCard(SourceUid);

                if (sourceCard != null)
                {
                    yield return Timing.WaitUntilDone( sourceCard.PlayFocusEffect(isComplete) );
                }
            }
        }
    }
}