using System.Collections.Generic;
using System.Security.Cryptography;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using MEC;
using TakoBoyStudios.Events;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ClientAPI.Commands
{
    public class TurnStartCommand : Command
    {
        public Team Team;

        public TurnStartCommand()
        {
            Type = CommandType.TURN_START;
        }

        public override string GetLabelDisplay()
        {
            return base.GetLabelDisplay() + $"Turn started for {GetPlayerName()}";
        }

        public override void OnPrepareCommand()
        {
            if (!IsClientCommand())
            {
                GameplayScreen.Instance.opponentHandView.Show();
            }
        }

        public override void OnExecuteCommand()
        {
            if (GameServer.Mode.ConnectionMode == ConnectionMode.ONLINE && !IsClientCommand())
            {
                Server.LogWarning("Trying to start an opponents turn in online. This is not allowed");
                return;
            }

            for (var i = 0; i < GameServer.State.Cards.Count; i++)
            {
                if (GameServer.State.Cards[i] != null)
                {
                    GameServer.State.Cards[i].ClearUsedAbilities();
                }
            }

            var occupiedTiles = GameServer.State.Board.GetOccupiedTiles(Team);
            var destroyFleetingMinionsCommand = new DestroyCommand
                { PlayerUid = PlayerUid, TargetUids = new List<string>() };

            for (var i = 0; i < occupiedTiles.Count; i++)
            {
                var card = occupiedTiles[i].Card;

                if (card.HasAbility(AbilityKeyword.FLEETING))
                {
                    destroyFleetingMinionsCommand.TargetUids.Add(card.uid);
                }
            }

            if (destroyFleetingMinionsCommand.TargetUids.Count > 0)
            {
                global::GameLogic.ExecuteCommand(destroyFleetingMinionsCommand);
            }

            var drawModifier = 0;
            GameServer.State.IncreaseTurnCounter(Team);

            var turnCount = GameServer.State.GetTurnCount(Team);
            var opponentTurnCount = GameServer.State.GetTurnCount(Team == Team.Client ? Team.Opponent : Team.Client);

            // both players use this method, its their first turn.
            if ( turnCount <= 1 && turnCount >= opponentTurnCount )
            {
                var player = GameServer.GetPlayerState(Team);

                for (var i = 0; i < player.relics.Count; i++)
                {
                    var relic = GameServer.State.GetCard(player.relics[i]);
                    if (relic.HasAbility(AbilityKeyword.MODIFY_STARTING_MANA) && relic.location == CardLocation.Board)
                    {
                        relic.Use();
                        var ability = relic.GetAbilities().Find(a => a.keyword == AbilityKeyword.MODIFY_STARTING_MANA);
                        player.totalMana += ability.manaModifier;
                    }

                    if (relic.HasAbility(AbilityKeyword.MODIFY_STARTING_DRAW) && relic.location == CardLocation.Board)
                    {
                        relic.Use();
                        var ability = relic.GetAbilities().Find(a => a.keyword == AbilityKeyword.MODIFY_STARTING_DRAW);
                        drawModifier += ability.drawCount;
                    }

                    if (relic.HasAbility(AbilityKeyword.MODIFY_STARTING_HEALTH) && relic.location == CardLocation.Board)
                    {
                        relic.Use();
                        var ability = relic.GetAbilities().Find(a => a.keyword == AbilityKeyword.MODIFY_STARTING_HEALTH);
                        //var hero = GameServer.State.GetCard(player.heroUid);
                        //hero.ModifyHealth(relic.uid, ability.healthModifier);
                        global::GameLogic.ExecuteCommand(new ModifyHealthCommand()
                        {
                            SourceUid = relic.uid,
                            Amount = ability.healthModifier,
                            TargetUids = new List<string> { player.heroUid }
                        });
                    }
                }
            }

            if (!IsClientCommand())
            {
                GameServer.OpponentPlayerState.totalMana = Mathf.Clamp(GameServer.OpponentPlayerState.totalMana + 1, 0, 10);
                GameServer.SetMana(Team.Opponent, GameServer.OpponentPlayerState.totalMana, false, true);
            }
            else
            {
                GameServer.ClientPlayerState.totalMana = Mathf.Clamp(GameServer.ClientPlayerState.totalMana + 1, 0, 10);
                if (App.Settings.GetUnlimitedMana()) GameServer.SetMana(Team.Client, 10, false, true);
                else GameServer.SetMana(Team.Client, GameServer.ClientPlayerState.totalMana, false, true);
            }

            var playerState = GameServer.GetPlayerState(Team);
            var heroState = GameServer.State.GetCard(playerState.heroUid);

            //var currentCommands = global::GameLogic.CommandsInQueue();

            global::GameLogic.ResolveTriggeredCards(heroState, null, TriggerType.TURN_START, new global::GameLogic.ResolveTriggeredCardsProperties{ showLog = true });

            //var afterTriggeredTurnStart = global::GameLogic.CommandsInQueue();

            // if (afterTriggeredTurnStart > currentCommands)
            // {
            //     var waitCommand = new WaitCommand();
            //     waitCommand.waitTime = 1;
            //     global::GameLogic.ExecuteCommand(waitCommand);
            // }

            var drawCardCommand = new DrawCardCommand
            {
                PlayerUid = PlayerUid,
                DrawCount =  GameServer.State.AuraUpdater.GetDrawCount( PlayerUid ) + drawModifier,
                CardCountBeforeDraw = GameServer.GetPlayerState(Team).GetHandCount()
            };

            global::GameLogic.ExecuteCommand(drawCardCommand);

            // first players turn, only used once on the first turn of the game.
            if ( turnCount == 1 && turnCount > opponentTurnCount )
            {
                // var opponentPlayer = GameServer.GetPlayerState(Team == Team.Client ? Team.Opponent : Team.Client);
                // var roshamboCardData = Db.CardDatabase.GetCard(opponentPlayer.turnOrderSelection.ToString());
                // var cardFilter = new CardFilter { cardId = roshamboCardData.id };
                //
                // var addCardToHandCommand = new AddCardToZoneCommand();
                // addCardToHandCommand.PlayerUid = opponentPlayer.Uid;
                // addCardToHandCommand.SourceUid = opponentPlayer.heroUid;
                // addCardToHandCommand.locationToAddTo = CardLocation.Hand;
                // addCardToHandCommand.TargetUids = addCardToHandCommand.CreateCardsFromFilter(cardFilter, opponentPlayer.faction, 1, null,null, null, string.Empty, string.Empty);
                // global::GameLogic.ExecuteCommand(addCardToHandCommand);
            }
        }

        public override IEnumerator<float> Resolve()
        {
            Events.Publish(this, EventType.OnTurnStart, new OnTurnStartEventArgs() { isClient = IsClientCommand(), totalTurns = GameServer.State.GetTurnCount(IsClientCommand() ? Team.Client : Team.Opponent ) });

            yield return Timing.WaitForSeconds(0.24f);

            Server.CommandResolver.CleanUpDestroyedMinions();

            yield return Timing.WaitForSeconds(0.24f);

            if (IsClientCommand())
            {
                yield return Timing. WaitForSeconds(0.25f);
            }
            else
            {
                GameServer.ChangeGameState(StateDefinition.GAME_OPPONENT_UPDATE_TURN);
            }
        }
    }
}
