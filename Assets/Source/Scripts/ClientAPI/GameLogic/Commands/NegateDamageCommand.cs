using CrossBlitz.ServerAPI.GameLogic;

namespace CrossBlitz.ClientAPI.Commands
{
    public class NegateDamageCommand : Command
    {
        public NegateDamageCommand() => Type = CommandType.NEGATE_DAMAGE;

        public override string GetLabelDisplay()
        {
            return base.GetLabelDisplay() + $"{GetTargetsAsString(this)} negated damage.";
        }

        public override void OnExecuteCommand()
        {
            for (var i = 0; i < TargetUids.Count; i++)
            {
                var card = GameServer.State.GetCard(TargetUids[i]);
                var damageCommand = (DamageCommand)GameServer.State.GetLastHistoryOf(CommandType.DAMAGE);

                for (var j = 0; j < damageCommand.PendingDamageHistories.Count; j++)
                {
                    damageCommand.PendingDamageHistories[j].DamageAmount = 0;
                    damageCommand.PendingDamageHistories[j].ArmorRemoval = 0;
                    damageCommand.PendingDamageHistories[j].ArmorBreak = false;
                    damageCommand.PendingDamageHistories[j].Guarded = false;

                    var history = card.GetHistory(damageCommand.PendingDamageHistories[j].Uid);

                    history.DamageAmount = 0;
                    history.ArmorRemoval = 0;
                    history.ArmorBreak = false;
                    history.Guarded = false;
                }

                card.RefreshStats();
            }
        }
    }
}