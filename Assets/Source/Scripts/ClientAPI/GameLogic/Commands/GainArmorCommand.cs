using System.Collections.Generic;
using System.Linq;
using Asyncoroutine;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using MEC;
using UnityEngine;
using EventType = UnityEngine.EventType;

namespace CrossBlitz.ClientAPI.Commands
{
    public class GainArmorCommand : Command
    {
        public int ArmorGain;
        public List<CardHistory> PendingGainArmorHistories;

        public GainArmorCommand()
        {
            Type = CommandType.GAIN_ARMOR;
        }

        public override string GetLabelDisplay()
        {
            var source = GameServer.State.GetCard(SourceUid);
            return base.GetLabelDisplay() + $"{source.characterData.name} gave {GetTargetsAsString(this)} +{ArmorGain} Armor.";
        }

        public int GetArmorGain(Ability ability, Command parentCommand, Command previousCommand, string sourceCardFromTriggerUid, string targetCardFromTriggerUid)
        {
            //Debug.LogError($"{ability.armorGainBasedOnConditions}; {ability.armorCondition.condition}");
            if (ability.armorGainBasedOnConditions)
            {
                var numTargets = 0;

                switch (ability.armorCondition.condition)
                {
                    case Condition.FACTION_ON_FIELD:
                    {
                        List<TileState> occupiedTiles;

                        if ( ability.damageAmountCondition.filters.HasFlag(FilterCondition.PLAYER) &&
                             ability.damageAmountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles();
                        }
                        else if (ability.damageAmountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles( GetTeam() == Team.Client ? Team.Opponent : Team.Client );
                        }
                        else
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles( GetTeam() );
                        }

                        for (var i = 0; i < occupiedTiles.Count; i++)
                        {
                            if (occupiedTiles[i].Card.data.faction == ability.armorCondition.faction &&
                                occupiedTiles[i].Card.uid != SourceUid)
                            {
                                numTargets++;
                            }
                        }
                        break;
                    }
                    case Condition.COST_OF_LAST_SPELL_USED:
                    {
                        var lastPlayCommand = GameServer.State.GetLastHistoryOf(CommandType.PLAY);
                        if (lastPlayCommand.Type == CommandType.PLAY)
                        {
                            var playedCard = GameServer.State.GetCard(lastPlayCommand.SourceUid);
                            if (playedCard != null)
                            {
                                numTargets = playedCard.GetCost();
                            }
                            else
                            {
                                Debug.LogError("Card is null???");
                            }
                        }
                        else
                        {
                            Debug.Log($"Could not find play command! {lastPlayCommand.Type}");

                            for (var i = 0; i < GameServer.State.History.Count; i++)
                            {
                                Debug.Log($"History: {GameServer.State.History[i].Type}");
                            }
                        }
                        break;
                    }
                    case Condition.CLASS_ON_FIELD:
                    {
                        List<TileState> occupiedTiles;

                        if (ability.damageAmountCondition.filters.HasFlag(FilterCondition.PLAYER) &&
                            ability.damageAmountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles();
                        }
                        else if (ability.damageAmountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles(GetTeam() == Team.Client ? Team.Opponent : Team.Client);
                        }
                        else
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles(GetTeam());
                        }

                        for (var i = 0; i < occupiedTiles.Count; i++)
                        {
                            if (occupiedTiles[i].Card.data.@class == ability.armorCondition.@class &&
                                occupiedTiles[i].Card.uid != SourceUid)
                            {
                                numTargets++;
                            }
                        }

                        break;
                    }
                    case Condition.DAMAGED:
                    case Condition.DAMAGED_MINIONS:
                    {
                        List<TileState> occupiedTiles;

                        if (ability.armorCondition.filters.HasFlag(FilterCondition.PLAYER) &&
                            ability.armorCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles();
                        }
                        else if (ability.armorCondition.filters.HasFlag(FilterCondition.PLAYER))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles( GetTeam() );
                        }
                        else
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles( GetTeam()  == Team.Client ? Team.Opponent : Team.Client );
                        }

                        for (var i = 0; i < occupiedTiles.Count; i++)
                        {
                            if (occupiedTiles[i].Card.GetPendingHealth() < occupiedTiles[i].Card.GetMaxHealthWithBuffs())
                            {
                                numTargets++;
                            }
                        }

                        break;
                    }
                    case Condition.CARDS_PLAYED_THIS_TURN:
                    {
                        numTargets = GameServer.GetPlayerState(PlayerUid).cardsPlayedThisTurn;
                        break;
                    }
                    case Condition.NUMBER_OF_SPECIFIC_CARDS_IN_DECK:
                    {
                        if (ability.armorCondition.filters.HasFlag(FilterCondition.PLAYER))
                        {
                            numTargets = GameServer.GetPlayerState(PlayerUid)
                                .GetNumberOfCardsWithIdInDeck(ability.armorCondition.cardId);
                        }
                        else if (ability.armorCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            numTargets = GameServer.GetPlayerState(GetTeam() == Team.Client ? Team.Opponent : Team.Client)
                                .GetNumberOfCardsWithIdInDeck(ability.armorCondition.cardId);
                        }
                        break;
                    }
                    case Condition.SOURCE_CARD_FROM_RESOLVE_TRIGGER:
                    {
                        if (!string.IsNullOrEmpty(sourceCardFromTriggerUid))
                        {
                            numTargets = ability.armorModifier > 0 ? ability.armorModifier : 1;

                            var sourceCard = GameServer.State.GetCard(sourceCardFromTriggerUid);

                            switch (ability.armorCondition.stat)
                            {
                                case FilterConditionValues.Stat.Power:
                                    numTargets *= sourceCard.GetPower();
                                    break;
                                case FilterConditionValues.Stat.ManaCost:
                                    numTargets *= sourceCard.GetCost();
                                    break;
                                case FilterConditionValues.Stat.Health:
                                    numTargets *= sourceCard.GetHealth();
                                    break;
                            }
                        }
                        break;
                    }
                    case Condition.TARGET_CARD_FROM_RESOLVE_TRIGGER:
                    {
                        if (!string.IsNullOrEmpty(targetCardFromTriggerUid))
                        {
                            numTargets = ability.armorModifier > 0 ? ability.armorModifier : 1;
                            var targetCard = GameServer.State.GetCard(targetCardFromTriggerUid);

                            switch (ability.armorCondition.stat)
                            {
                                case FilterConditionValues.Stat.Power:
                                    numTargets *= targetCard.GetPower();
                                    break;
                                case FilterConditionValues.Stat.ManaCost:
                                    numTargets *= targetCard.GetCost();
                                    break;
                                case FilterConditionValues.Stat.Health:
                                    numTargets *= targetCard.GetHealth();
                                    break;
                            }
                        }
                        break;
                    }

                    case Condition.TARGETS_POWER:
                    {
                        if (TargetUids.Count > 0)
                        {
                            var target = GameServer.State.GetCard(TargetUids[0]);

                            if (target!=null)
                            {
                                numTargets = target.GetPendingPower();
                            }
                        }
                        break;
                    }
                    case Condition.TARGET_FROZEN:
                    {
                        numTargets = 1;

                        var occupiedTiles = GameServer.State.Board.GetOccupiedTiles();

                        for (var i = 0; i < occupiedTiles.Count; i++)
                        {
                            if (occupiedTiles[i].Card.HasStatusEffect(StatusEffect.Frozen))
                            {
                                numTargets++;
                            }
                        }
                        break;
                    }
                    default:
                    {
                        numTargets = 1;
                        break;
                    }
                }

                var armorMod = ability.armorModifier <= 0 ? 1 : ability.armorModifier;
                return numTargets * armorMod;
            }

            return ability.armorModifier;
        }

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            return ArmorGain;
        }

        public override void OnPrepareCommand()
        {

        }

        public override void OnExecuteCommand()
        {
            PendingGainArmorHistories=new List<CardHistory>();

            for (var i = TargetUids.Count-1; i > -1; i--)
            {
                var hero = GameServer.State.GetCard(TargetUids[i]);

                if (!hero.IsHero)
                {
                    TargetUids.RemoveAt(i);
                    Debug.LogError("Target Uid is not a hero, giving armor to anything but a hero is not supported!");
                    continue;
                }

                PendingGainArmorHistories.Add( hero.GrantArmor(SourceUid, ArmorGain) );
            }
        }

        public override IEnumerator<float> Resolve()
        {
            if (TargetUids.Count <= 0 || PendingGainArmorHistories.Count <= 0)
            {
                Debug.LogError("No Targets found");
                yield break;
            }

            if (TargetUids.Count != PendingGainArmorHistories.Count)
            {
                Debug.LogError($"There should be the same amount of pending histories as targets! Targets={TargetUids.Count} : Pending Histories={PendingGainArmorHistories.Count}");
                yield break;
            }

            var routines = new List<CoroutineHandle>();

            // try this first.
            for (var i = 0; i < PendingGainArmorHistories.Count; i++)
            {
                var pendingHistory = PendingGainArmorHistories[i];
                var sourceCard = GameServer.State.GetCard(pendingHistory.AppliedBy);

                if (sourceCard.characterData.type == CardType.Minion || sourceCard.characterData.type == CardType.Trick)
                {
                    // @Rob add the GetArmorEffect stuff here.
                }

                var targetHero = GameServer.State.GetCard(pendingHistory.CardUid);
                targetHero.ApplyHistory(pendingHistory.Uid);

                var heroView = GameplayScreen.Instance.heroSideBarContainer.GetHeroView(targetHero.Team);
                var totalArmor = heroView.State.GetArmor();

                routines.Add( Timing.RunCoroutine(heroView.armorView.AnimateGain(pendingHistory.ArmorModifier, totalArmor)));

                Events.Publish(this,GameLogic.EventSystem.EventType.OnGainedArmor, new OnGainedArmorEventArgs
                {
                    team = targetHero.Team,
                    armorGained = pendingHistory.ArmorModifier
                });
            }

            while (routines.Any(x => x.IsRunning))
            {
                yield return Timing.WaitForOneFrame;
            }
        }
    }
}
