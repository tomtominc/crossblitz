using System;
using System.Collections.Generic;
using System.Linq;
using Asyncoroutine;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Data;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using CrossBlitz.Hero;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using KennethDevelops.ProLibrary.Util.Serialization;
using MEC;
using Source.Scripts.Card;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    [Serializable]
    public class Command
    {
        public int Guid;
        public CommandType Type;
        public string PlayerUid;
        public string SourceUid;
        public int HandIndex = -1; // used when AI "creates" a card and adds it as a pending command
        public float Delay;
        public int AbilityIndex=-1;
        public List<string> TargetUids;
        public List<PendingCommand> HeldCommands;

        protected Command()
        {
            Guid = GUID.CreateUniqueInt();
            Type = CommandType.NULL;
        }

        public virtual void OnPrepareCommand()
        {
        }

        public virtual void OnExecuteCommand()
        {
        }

        public virtual IEnumerator<float> Resolve()
        {
            //Debug.LogError($"Command {Type} does not have a RESOLVE METHOD IMPLEMENTED!");
            yield break;
        }

        public virtual void Pause()
        {
        }

        public virtual void Resume()
        {
        }

        public virtual void AddHeldCommands(PendingCommand pendingCommand)
        {
            HeldCommands ??= new List<PendingCommand>();
            HeldCommands.Add(pendingCommand);
        }

        public virtual string GetLabelDisplay()
        {
            return $"<color=red>[{Type as object}]</color>\n";
        }

        public bool IsClientCommand()
        {
            return PlayerUid == GameServer.ClientPlayerState.Uid;
        }

        public bool IsLocalPlayerCommand()
        {
            return PlayerUid == GameServer.GetLocalPlayerState().Uid;
        }

        public bool IsAiPlayer()
        {
            return GameServer.GetPlayerState(PlayerUid).controlMode == PlayerBattleState.ControlMode.Bot;
        }

        public string GetPlayerName()
        {
            if (string.IsNullOrEmpty(PlayerUid)) return "Unknown";
            return GameServer.GetPlayerState(PlayerUid).name;
        }

        public virtual int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            return 0;
        }

        public static string GetTargetsAsString(Command command)
        {
            if (command.TargetUids == null || command.TargetUids.Count <= 0) return String.Empty;
            return string.Join(",", command.TargetUids.Select(cardUid =>
            {
                var card = GameServer.State.GetCard(cardUid);
                return card.characterData.name;
            }));
        }

        // what did you do here?
        public Team GetTeam()
        {
            return GameServer.GetPlayerState(PlayerUid).GetTeam();
            //return IsClientCommand() ? Team.Client : Team.Opponent;
        }

        public Ability GetAbility()
        {
            if (AbilityIndex < 0) return null;

            var card = GameServer.State.GetCard(SourceUid);

            if (card == null)
            {
                return null;
            }

            if (card.gameAbilities.Count <= AbilityIndex || AbilityIndex < 0)
            {
                return null;
            }

            return card.gameAbilities[AbilityIndex];
        }

        public static IEnumerator<float> PlayEffect(Command command, string effectName)
        {
            var getWorldVfxTask = GameVfxProvider.GetWorldVfx(effectName);
            yield return Timing.WaitUntilDone(getWorldVfxTask.AsCoroutine());
            yield return Timing.WaitUntilDone(getWorldVfxTask.Result.Play(command, new VfxParameters()));
        }

        public static IEnumerator<float> PlayCustomOrGenericEffectOnBoardCard(Ability abilityToTry, Command command, string targetOfEffect, GameCardView boardCard = null, HeroViewBattle heroView = null)
        {
            if (abilityToTry != null && abilityToTry.usesCustomEffect && abilityToTry.usesBoardEffect)
            {
                var getWorldVfxTask = GameVfxProvider.GetWorldVfx(abilityToTry.boardEffectPrefab);
                yield return Timing.WaitUntilDone(getWorldVfxTask.AsCoroutine());
                yield return Timing.WaitUntilDone(getWorldVfxTask.Result.Play(command, new VfxParameters()));
                yield break;
            }

            var targetCard = GameServer.State.GetCard(targetOfEffect);
            var targetBoardCard = boardCard;

            switch (targetCard.characterData.type)
            {
                case CardType.Minion:
                    if (targetBoardCard == null)
                    {
                        var getBoardCardTask = GameBoard.GetBoardCard(targetOfEffect);
                        yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                        var getBoardCardResult = getBoardCardTask.Result;

                        if (getBoardCardResult.error != ClientErrorCode.None)
                        {
                            Server.HandleClientError(getBoardCardResult, false);
                        }

                        targetBoardCard = getBoardCardResult.boardCard;
                    }

                    if (targetBoardCard)
                    {
                        if (abilityToTry != null && abilityToTry.usesCustomEffect)
                        {
                            var vfxType = System.Type.GetType(abilityToTry.customEffectComponent);
                            var vfxComponent = (GameVfxComponent)targetBoardCard.GetComponent(vfxType);

                            if (vfxComponent == null || vfxComponent.Effect == null)
                            {
                                vfxComponent = targetBoardCard.View.AddCardComponent<GameVfxComponent>(vfxType);
                                vfxComponent.Initialize(targetBoardCard.View);
                                vfxComponent.OnAfterCreation();
                            }

                            while (vfxComponent.Effect == null)
                            {
                                yield return Timing.WaitForOneFrame;
                            }

                            if (vfxComponent != null)
                            {
                                yield return Timing.WaitUntilDone(vfxComponent.Effect.Play(command, new VfxParameters()));
                                yield break;
                            }
                        }
                        else
                        {
                            Timing.RunCoroutine(targetBoardCard.AnimateEffect<GenericEffectComponent>(command));
                        }
                    }

                    break;
                case CardType.Spell:
                    break;
                case CardType.Hero:
                {
                    if (abilityToTry != null && abilityToTry.usesCustomEffect && heroView != null)
                    {
                        var vfxType = System.Type.GetType(abilityToTry.customEffectComponent);
                        var boardTarget = heroView.boardTarget;
                        var vfxComponent = (GameVfxComponent)boardTarget.gameObject.AddComponent(vfxType);
                        vfxComponent.OnAfterCreation();

                        // if (vfxComponent == null || vfxComponent.Effect == null)
                        // {
                        //     vfxComponent = (GameVfxComponent)boardTarget.gameObject.AddComponent(vfxType);
                        //     //vfxComponent.Initialize()
                        //     vfxComponent.OnAfterCreation();
                        // }

                        while (vfxComponent.Effect == null)
                        {
                            yield return Timing.WaitForOneFrame;
                        }

                        if (vfxComponent != null)
                        {
                            yield return Timing.WaitUntilDone(vfxComponent.Effect.Play(command, new VfxParameters()));
                            yield break;
                        }
                    }
                    break;
                }
            }

            yield return Timing.WaitForSeconds(0.4f);
        }

        public virtual bool IsEquivalent(Command command)
        {
            return Type == command.Type;
        }

        public static Command Empty()
        {
            return new Command();
        }
    }

    public class PendingCommand
    {
        public string cardUid;
        public int abilityIndex;
        public Command parentCommand;
        public Command previousCommand;
    }
}