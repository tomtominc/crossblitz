using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.ServerAPI.GameLogic;

namespace CrossBlitz.ClientAPI.Commands
{
    public class CastCardCommand : Command
    {
        public string cardId;
        public CastCardCommand()=> Type = CommandType.CAST_CARD;

        public override string GetLabelDisplay()
        {
            return base.GetLabelDisplay() + $"{Db.CardDatabase.GetCard(cardId).name} tried to be cast.";
        }

        public override void OnExecuteCommand()
        {
            var player = GameServer.GetPlayerState(PlayerUid);
            var oppPlayer = GameServer.GetPlayerState(player.GetTeam() == Team.Client ? Team.Opponent : Team.Client);
            var cardData = Db.CardDatabase.GetCard(cardId);
            var card = player.CreateCardAtLocation(cardData.id, CardLocation.Board);
            var selection = string.Empty;
            var selectionAbility = card.GetAbilities().Find(ability => ability.targetConditions.HasFlag(FilterCondition.PLAYER_SELECTED));
            var sourceAbility = GameServer.State.GetCard(SourceUid).GetAbilities()[AbilityIndex];

            if (sourceAbility.targetConditions.HasFlag(FilterCondition.TARGET_CARD_FROM_RESOLVE_TRIGGERED_CARDS) ||
                sourceAbility.targetConditions.HasFlag(FilterCondition.SOURCE_CARD_FROM_RESOLVE_TRIGGERED_CARDS))
            {
                selection = TargetUids.Count > 0 ? TargetUids[0] : string.Empty;
            }

            if (string.IsNullOrEmpty(selection) && selectionAbility != null)
            {
                var selectionOptions = new List<string>();

                if (selectionAbility.targetConditions.HasFlag(FilterCondition.PLAYER))
                {
                    if (selectionAbility.targetConditions.HasFlag(FilterCondition.HERO))
                    {
                        selectionOptions.Add(player.heroUid);
                    }

                    if (selectionAbility.targetConditions.HasFlag(FilterCondition.MINION))
                    {
                        var minions = GameServer.State.Board.GetOccupiedTiles();

                        for (var i = 0; i < minions.Count; i++)
                        {
                            if (minions[i].Card.owner == player.Uid)
                            {
                                selectionOptions.Add(minions[i].Card.uid);
                            }
                        }
                    }
                }

                if (selectionAbility.targetConditions.HasFlag(FilterCondition.OPPONENT))
                {
                    if (selectionAbility.targetConditions.HasFlag(FilterCondition.HERO))
                    {
                        selectionOptions.Add(oppPlayer.heroUid);
                    }

                    if (selectionAbility.targetConditions.HasFlag(FilterCondition.MINION))
                    {
                        var minions = GameServer.State.Board.GetOccupiedTiles();

                        for (var i = 0; i < minions.Count; i++)
                        {
                            if (minions[i].Card.owner == oppPlayer.Uid)
                            {
                                selectionOptions.Add(minions[i].Card.uid);
                            }
                        }
                    }
                }

                if(selectionOptions.Count > 0)
                {
                    var rand = new System.Random();
                    selection = selectionOptions[rand.Next(0, selectionOptions.Count)];
                }
            }

            var command = new PlayCommand
            {
                PlayerUid = PlayerUid,
                SourceUid = card.uid,
                Selection = !string.IsNullOrEmpty(selection) ? GameServer.State.GetCard(selection) : null,
                OnlyShowPreview = true,
                AutoCast = true,
            };

            global::GameLogic.ExecuteCommand(command);
        }
    }
}
