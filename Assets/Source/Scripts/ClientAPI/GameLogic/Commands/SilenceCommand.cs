using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;

namespace CrossBlitz.ClientAPI.Commands
{
    public class SilenceCommand : Command
    {
        public SilenceCommand()
        {
            Type = CommandType.SILENCE;
        }

        public override void OnExecuteCommand()
        {
            for (var i = 0; i < TargetUids.Count; i++)
            {
                var target = GameServer.State.GetCard( TargetUids[i] );
                target.Silence();
            }
        }

        public override IEnumerator<float> Resolve()
        {
            var shouldWait = false;

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var targetCard = GameServer.State.GetCard(TargetUids[i]);
                var boardCard = GameplayScreen.Instance.GetBoardCard(targetCard.uid, out _, out _);

                if (boardCard == null && targetCard.location == CardLocation.Board)
                {
                    var getBoardCardTask = GameBoard.GetBoardCard(TargetUids[i]);
                    yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                    var getBoardCardResult = getBoardCardTask.Result;

                    if (getBoardCardResult.error != ClientErrorCode.None)
                    {
                        Server.HandleClientError(getBoardCardResult, false);
                    }

                    boardCard = getBoardCardResult.boardCard;
                }

                if (boardCard == null) continue;

                shouldWait = true;
                boardCard.UpdateViewFromState();
                boardCard.Events.Silence();

                if (AbilityIndex > -1)
                {
                    var sourceCard = GameServer.State.GetCard(SourceUid);
                    yield return Timing.WaitUntilDone(PlayCustomOrGenericEffectOnBoardCard(sourceCard.GetAbilities()[AbilityIndex], this, boardCard.State.uid, boardCard));
                }
            }

            if (shouldWait) yield return Timing.WaitForSeconds(0.4f);
        }
    }
}
