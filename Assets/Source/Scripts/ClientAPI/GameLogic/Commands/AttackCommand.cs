using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    [System.Serializable]
    public class AttackCommand : Command
    {
        public Team Team;
        public bool WaitForColumnExplosionBeforeFinishing;
        public List<CardHistory> PendingDamageHistories;
        public List<CardHistory> PendingRetaliateHistories;
        public bool alreadyBuiltUpAttack;
        public bool ableToRetaliate;

        public AttackCommand()
        {
            Type = CommandType.ATTACK;
            WaitForColumnExplosionBeforeFinishing = true;
        }

        /// <summary>
        /// Gets an attack command for standard attacks, will attack the minion in front.
        /// </summary>
        /// <returns></returns>
        public static AttackCommand GetStandardAttackCommandForCard(string playerUid, GameCardState source, Vector2Int localPosition)
        {
            AttackCommand attackCommand = null;
            var targetTeam = source.Team == Team.Client ? Team.Opponent : Team.Client;

            TileState targetTile;
            GameCardState target = null;

            var invertedPosition = GameServer.State.Board.InvertPosition(localPosition);

            if (GameServer.State.Board.IsOccupied(new Vector2Int(invertedPosition.x, 0), targetTeam))
            {
                targetTile = GameServer.State.Board.GetTile(new Vector2Int(invertedPosition.x, 0), targetTeam);
                target = GameServer.State.GetCard(targetTile.CardId);

                if (source.HasAbility(AbilityKeyword.FLYING) && !target.HasAbility(AbilityKeyword.FLYING))
                {
                    target = null;
                }

                if (target != null && target.data.type == CardType.Trick)
                {
                    target = null;
                }
            }

            if (target == null && GameServer.State.Board.IsOccupied(new Vector2Int(invertedPosition.x, 1), targetTeam))
            {
                targetTile = GameServer.State.Board.GetTile(new Vector2Int(invertedPosition.x, 1), targetTeam);
                target = GameServer.State.GetCard(targetTile.CardId);

                if (source.HasAbility(AbilityKeyword.FLYING) && !target.HasAbility(AbilityKeyword.FLYING))
                {
                    target = null;
                }

                if (target != null && target.data.type == CardType.Trick)
                {
                    target = null;
                }
            }

            if (target == null || string.IsNullOrEmpty(target.uid) || target.data == null)
            {
                var targetHero = targetTeam == Team.Client ? GameServer.ClientPlayerState.heroUid : GameServer.OpponentPlayerState.heroUid;
                target = GameServer.State.GetCard(targetHero);
            }

            if (target != null)
            {
                attackCommand = new AttackCommand
                {
                    PlayerUid = playerUid,
                    Team = source.Team,
                    SourceUid = source.uid,
                    TargetUids = new List<string> { target.uid }
                };
            }

            return attackCommand;
        }

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            var score = 0;

            if (PendingDamageHistories == null) return score;

            for (var i = 0; i < PendingDamageHistories.Count; i++)
            {
                var pendingScore = 0;
                var target = GameServer.State.GetCard(PendingDamageHistories[i].CardUid);

                if (target == null) continue;

                if (target.IsHero)
                {
                    if (target.GetPendingHealth() <= 0)
                    {
                        pendingScore += PendingDamageHistories[i].DamageAmount * 1000;
                    }
                    else
                    {
                        pendingScore += PendingDamageHistories[i].DamageAmount * 2;
                    }
                }
                else
                {
                    if (target.GetPendingHealth() <= 0)
                    {
                        pendingScore += PendingDamageHistories[i].DamageAmount * 2;
                    }
                    else
                    {
                        pendingScore += PendingDamageHistories[i].DamageAmount;
                    }
                }

                if (target.Team == Team.Opponent)
                {
                    pendingScore *= -1;
                }

                score += pendingScore;
            }

            return score;
        }

        public override string GetLabelDisplay()
        {
            var card = GameServer.State.GetCard(SourceUid);
            var power = card.GetPendingPower() as object;
            return base.GetLabelDisplay() + $"{card.data.name} dealt {power} to {GetTargetsAsString(this)}.";
        }

        public override void OnPrepareCommand()
        {
        }

        public override void OnExecuteCommand()
        {
            var sourceCard = GameServer.State.GetCard(SourceUid);

            if (GameCardState.IsNullOrDead(sourceCard) || sourceCard.location != CardLocation.Board)
            {
                return;
            }

            var damageCommand = new DamageCommand
            {
                PlayerUid = PlayerUid,
                SourceUid = SourceUid,
                Amount = sourceCard.GetPendingPower(),
                TargetUids = TargetUids,
                VisualResolvedInPreviousCommand = true
            };

            var commandsBeforeTrigger = global::GameLogic.CommandsInQueue();

            // triggers for cards that were attacked
            for (var i = 0; i < damageCommand.TargetUids.Count; i++)
            {
                global::GameLogic.ResolveTriggeredCards(sourceCard,
                    GameServer.State.GetCard(damageCommand.TargetUids[i]), TriggerType.ATTACKED);
            }

            var indexOfAttackCommand = global::GameLogic.IndexOfCommand(this);
            // move this command to be resolved "last" after commands that were put on the stack by triggering the attack.
            global::GameLogic.MoveCommandToLast(this);

            // if we triggered cards while testing the "Attack" Trigger, we replace the attack command with an attack build up to get some feedback.
            if (commandsBeforeTrigger < global::GameLogic.CommandsInQueue())
            {
                var attackBuildUp = new AttackBuildUpCommand
                {
                    PlayerUid = PlayerUid,
                    SourceUid = SourceUid,
                };

                global::GameLogic.ExecuteCommand(indexOfAttackCommand, attackBuildUp);

                alreadyBuiltUpAttack = true;
            }

            if (sourceCard.GetPendingHealth() <= 0)
            {
                return;
            }

            // triggers when the source card attacks SOMETHING (BOOM BIRD)
            for (var i = 0; i < damageCommand.TargetUids.Count; i++)
            {
                global::GameLogic.ResolveTriggeredCards(GameServer.State.GetCard(damageCommand.TargetUids[i]),
                    sourceCard, TriggerType.DEALT_DAMAGE_THROUGH_ATTACKING);
            }

            global::GameLogic.ExecuteCommand(damageCommand);

            PendingDamageHistories = new List<CardHistory>(damageCommand.PendingDamageHistories);

            if (TargetUids?.Count > 0 && ableToRetaliate)
            {
                var targetCard = GameServer.State.GetCard(TargetUids[0]);

                if (targetCard.GetPendingPower() > 0 && !targetCard.HasStatusEffect(StatusEffect.Frozen))
                {
                    var retaliateCommand = new DamageCommand
                    {
                        PlayerUid = targetCard.owner,
                        SourceUid = targetCard.uid,
                        TargetUids = new List<string> { SourceUid },
                        Amount = targetCard.GetPendingPower(),
                        retaliateCommand = true,
                        VisualResolvedInPreviousCommand = true
                    };

                    global::GameLogic.ExecuteCommand(retaliateCommand);

                    PendingRetaliateHistories = new List<CardHistory>();
                    PendingRetaliateHistories.AddRange( retaliateCommand.PendingDamageHistories );
                }
            }
        }

        public override IEnumerator<float> Resolve()
        {
            var getBoardCardTask = GameBoard.GetBoardCard(SourceUid);
            yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
            var getBoardCardResult = getBoardCardTask.Result;

            if (getBoardCardResult.error != ClientErrorCode.None)
            {
                Server.HandleClientError(getBoardCardResult, true);
            }

            if (!getBoardCardResult.boardCard)
            {
                yield break;
            }

            var sourceBoardCard = getBoardCardResult.boardCard;
            var attackFx = (CustomAttackEffect)sourceBoardCard.View.GetVfx<AttackEffectComponent, AttackEffect>();

            if (sourceBoardCard.State.data.hasCustomAttackProperties)
            {
                attackFx = (CustomAttackEffect)((CustomAttackComponent)sourceBoardCard.GetComponent(System.Type.GetType(sourceBoardCard.State.data.customAttackEffect))).Effect;
            }

            if (PendingDamageHistories == null)
            {
                yield break;
            }

            for (var i = 0; i < PendingDamageHistories.Count; i++)
            {
                var target = GameServer.State.GetCard(PendingDamageHistories[i].CardUid);

                if (target == null)
                {
                    continue;
                }

                // it's a hero!
                if (target.IsHero)
                {
                    if (PendingDamageHistories[i].ArmorRemoval > 0 && !PendingDamageHistories[i].ArmorBreak)
                    {
                        target.ApplyHistory(PendingDamageHistories[i].Uid);
                        continue;
                    }

                    var heroView = Team == Team.Client
                        ? GameplayScreen.Instance.heroSideBarContainer.opponentHeroView
                        : GameplayScreen.Instance.heroSideBarContainer.playerHeroView;

                    var retaliateHistory = PendingRetaliateHistories != null && PendingRetaliateHistories.Count > i ? PendingRetaliateHistories[i] : null;
                    // we need to animate the hero taking damage
                    yield return Timing.WaitUntilDone(attackFx.AttackHero(heroView, this, PendingDamageHistories[i], retaliateHistory));
                }
                else
                {
                    var getBoardCardTargetTask = GameBoard.GetBoardCard(target.uid);
                    yield return Timing.WaitUntilDone(getBoardCardTargetTask.AsCoroutine());
                    var getBoardCardTargetResult = getBoardCardTargetTask.Result;

                    if (getBoardCardTargetResult.error != ClientErrorCode.None)
                    {
                        Server.HandleClientError(getBoardCardTargetResult, true);
                    }

                    if (getBoardCardTargetResult.boardCard)
                    {
                        var retaliateHistory = PendingRetaliateHistories != null && PendingRetaliateHistories.Count > i ? PendingRetaliateHistories[i] : null;
                        yield return Timing.WaitUntilDone(attackFx.AttackMinion(getBoardCardTargetResult.boardCard, this, PendingDamageHistories[i],retaliateHistory));
                    }
                }
            }

            yield return Timing.WaitForOneFrame;
        }
    }
}