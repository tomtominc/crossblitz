using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ServerAPI;
using Newtonsoft.Json;

namespace CrossBlitz.ClientAPI.Commands
{
    public class CommandQueue
    {
        public List<Command> commands;

        public CommandQueue()
        {
            commands = new List<Command>();
        }

        public Queue<Command> ToQueue()
        {
            return new Queue<Command>(commands);
        }

        public void Squash()
        {
            var squashedList = new List<Command>();

            for (var i = 0; i < commands.Count; i++)
            {
                var command = commands[i];

            }
        }

        public void ReorderAndSquashVisuals()
        {
            SquashSummons();
        }

        private void SquashSummons()
        {
            SummonCommand masterSummon = null;
            var removeSummons = new List<SummonCommand>();

            for (var i = 0; i < commands.Count; i++)
            {
                if (commands[i] is SummonCommand summon)
                {
                    if (!summon.CanBeSquashed()) continue;

                    if (masterSummon == null)
                    {
                        masterSummon = summon;
                    }
                    else
                    {
                        masterSummon.Append(summon);
                        removeSummons.Add(summon);
                    }
                }
            }

            for (var i = 0; i < removeSummons.Count; i++)
            {
                commands.Remove(removeSummons[i]);
            }
        }

        /// <summary>
        /// Converts the game state into a json object for the server.
        /// </summary>
        /// <returns></returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Server.JsonSettings);
        }

        public static CommandQueue FromJson(string json)
        {
            return JsonConvert.DeserializeObject<CommandQueue>(json, Server.JsonSettings);
        }
    }
}
