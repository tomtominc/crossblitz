using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    public class TriggerAbilityCommand : Command
    {
        public TriggerType Trigger;
        public int TriggerTimes;

        public TriggerAbilityCommand() => Type = CommandType.TRIGGER_ABILITY;

        public override string GetLabelDisplay()
        {
            var label = base.GetLabelDisplay() ;

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var card = GameServer.State.GetCard(TargetUids[i]);
                label += $"[{Trigger}] {card?.characterData?.name}";
            }

            return label;
        }

        public override void OnExecuteCommand()
        {
            var source = GameServer.State.GetCard(SourceUid);

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var target = GameServer.State.GetCard(TargetUids[i]);
                List<Ability> abilities;

                if (Trigger == TriggerType.PLAYED_CARD)
                {
                    abilities = target.GetAbilities();
                }
                else
                {
                    abilities = target.GetAbilities().FindAll(a => a.triggerType == Trigger);
                }

                var triggerTimes = 1;

                if (Trigger == TriggerType.BATTLECRY)
                {
                    triggerTimes = GameServer.State.AuraUpdater.GetBattlecryCount(target.owner);
                }
                else if (Trigger == TriggerType.DEATHRATTLE)
                {
                    triggerTimes = GameServer.State.AuraUpdater.GetDeathrattleCount(target.owner);
                }

                triggerTimes *= TriggerTimes;

                for (var j = 0; j < triggerTimes; j++)
                {
                    for (var k = 0; k < abilities.Count; k++)
                    {
                        var battlecryCommands = global::GameLogic.CreateCommandFromAbility(target, abilities[k], this,
                            this, source.characterData.type == CardType.Minion ? 0.4f : 0);
                        global::GameLogic.ExecuteCommand(battlecryCommands);
                        Command previousCommand = null;

                        if (battlecryCommands.Count > 0)
                        {
                            previousCommand = battlecryCommands[0];
                        }

                        var linkedAbilities = target.GetAbilities().FindAll(ability => ability.triggerType == TriggerType.LINKED_ABILITY);
                        var linkedCommands = new List<Command>();

                        for (var l = 0; l < linkedAbilities.Count; l++)
                        {
                            if (linkedAbilities[k].abilityCondition.condition == Condition.PREVIOUS_COMMAND_TARGETS &&
                                linkedAbilities[k].abilityCondition.usePreviousLinkedCommandNotParent &&
                                linkedCommands.Count > 0)
                            {
                                previousCommand = linkedCommands[0];
                            }

                            linkedCommands = global::GameLogic.CreateCommandFromAbility(target, linkedAbilities[l], this, previousCommand);

                            global::GameLogic.ExecuteCommand(linkedCommands);
                        }
                    }
                }
            }
        }

        public override IEnumerator<float> Resolve()
        {
            var source = GameServer.State.GetCard(SourceUid);
            CardView sourceCard = null;

            if (source.characterData.type == CardType.Minion)
            {
                yield return Timing.WaitForSeconds(0.4f);
            }

            if (source.characterData.type == CardType.Minion && source.location == CardLocation.Board)
            {
                var boardCard = GameBoard.GetBoardCardNotImportant(SourceUid);

                if (boardCard != null)
                {
                    sourceCard = boardCard.View;
                    //sourceCard.IsAnimating(true);
                    var ability = GetAbility();

                    if (ability != null && ability.usesCustomEffect)
                    {
                        yield return Timing.WaitUntilDone(
                            PlayCustomOrGenericEffectOnBoardCard(ability, this, boardCard.State.uid, boardCard));

                    }
                    else if (boardCard)
                    {
                        Timing.RunCoroutine(boardCard.AnimateEffect<GenericEffectComponent>(this));
                    }
                }
            }

            for (var i = 0; i < TargetUids.Count; i++)
            {
                if (Trigger == TriggerType.DEATHRATTLE)
                {
                    var target = GameBoard.GetBoardCardNotImportant(TargetUids[i]);
                    if (target) Timing.RunCoroutine(target.AnimateEffect<DeathrattleEffectComponent>(this));
                }
            }

            yield return Timing.WaitForSeconds(0.24f);

            if (sourceCard)
            {
                //sourceCard.IsAnimating(false);
            }
        }
    }
}