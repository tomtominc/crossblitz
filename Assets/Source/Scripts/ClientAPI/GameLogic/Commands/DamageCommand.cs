using System;
using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.Hero;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.ServerAPI.Models;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    public class DamageCommand : Command
    {
        public bool UseDifferentAmounts;
        public int Amount;
        public List<int> Amounts;
        public bool RandomlySplit;
        public bool Repeats;
        public int RepeatDamageCount;
        public bool VisualResolvedInPreviousCommand;
        public List<CardHistory> PendingDamageHistories;
        public bool retaliateCommand = false;

        public DamageCommand()
        {
            Type = CommandType.DAMAGE;
        }

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            var score = 0;

            if (VisualResolvedInPreviousCommand) // already counted it towards another command
            {
                return score;
            }

            for (var i = 0; i < PendingDamageHistories.Count; i++)
            {
                var pendingScore = 0;
                var target = GameServer.State.GetCard(PendingDamageHistories[i].CardUid);

                if(target == null) continue;

                if (target.IsHero)
                {
                    if (target.GetPendingHealth() <= 0)
                    {
                        pendingScore += PendingDamageHistories[i].DamageAmount * 1000;
                    }
                    else
                    {
                        pendingScore += PendingDamageHistories[i].DamageAmount * 2;
                    }
                }
                else
                {
                    if (target.GetPendingHealth() <= 0)
                    {
                        pendingScore += PendingDamageHistories[i].DamageAmount * 2;
                    }
                    else
                    {
                        pendingScore += PendingDamageHistories[i].DamageAmount;
                    }
                }

                if (target.Team == Team.Opponent)
                {
                    pendingScore *= -1;
                }

                score += pendingScore;
            }

            return score;
        }

        public override string GetLabelDisplay()
        {
            var baseDisplay = base.GetLabelDisplay();
            var returnString = string.Empty;
            var source = GameServer.State.GetCard( SourceUid );

            if (PendingDamageHistories != null )
            {
                for (var i = 0; i < PendingDamageHistories.Count; i++)
                {
                    var card = GameServer.State.GetCard(PendingDamageHistories[i].CardUid);

                    if (card != null && source != null)
                    {
                        returnString += baseDisplay +
                                        $"{source.characterData.name} damaged {card.characterData.name} for {PendingDamageHistories[i].DamageAmount}.\n";
                    }
                }
            }
            else if (TargetUids != null)
            {
                for (var i = 0; i < TargetUids.Count; i++)
                {
                    var card = GameServer.State.GetCard(TargetUids[i]);

                    if (card != null && source != null)
                    {
                        var amount = UseDifferentAmounts ? Amounts[i] : Amount;
                        returnString += baseDisplay +
                                        $"{source.characterData.name} damaged {card.characterData.name} for {amount}.\n";
                    }
                }
            }

            return returnString;
        }

        public override void OnPrepareCommand()
        {
        }

        public static int GetDamageAmount(string playerUid, string sourceUid, List<string> targets, Ability ability )
        {
            var damageAmount = 0;
            var sourceCard = GameServer.State.GetCard(sourceUid);
            var player = GameServer.GetPlayerState(playerUid);

            if (ability.damageBasedOnConditions)
            {
                switch (ability.damageAmountCondition.condition)
                {
                    case Condition.FACTION_ON_FIELD:
                    {
                        List<TileState> occupiedTiles;

                        if ( ability.damageAmountCondition.filters.HasFlag(FilterCondition.PLAYER) &&
                             ability.damageAmountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles();
                        }
                        else if (ability.damageAmountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles( player.GetTeam() == Team.Client ? Team.Opponent : Team.Client );
                        }
                        else
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles( player.GetTeam() );
                        }

                        for (var i = 0; i < occupiedTiles.Count; i++)
                        {
                            if (occupiedTiles[i].Card.data.faction == ability.damageAmountCondition.faction && occupiedTiles[i].Card.uid != sourceUid)
                            {
                                damageAmount++;
                            }
                        }
                        break;
                    }
                    case Condition.CLASS_ON_FIELD:
                    {
                        List<TileState> occupiedTiles;

                        if ( ability.damageAmountCondition.filters.HasFlag(FilterCondition.PLAYER) &&
                             ability.damageAmountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles();
                        }
                        else if (ability.damageAmountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles( player.GetTeam() == Team.Client ? Team.Opponent : Team.Client );
                        }
                        else
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles( player.GetTeam() );
                        }

                        for (var i = 0; i < occupiedTiles.Count; i++)
                        {
                            if (occupiedTiles[i].Card.data.@class == ability.damageAmountCondition.@class &&
                                occupiedTiles[i].Card.uid != sourceUid)
                            {
                                damageAmount++;
                            }
                        }

                        break;
                    }
                    case Condition.THIS_MINIONS_POWER:
                    {
                        damageAmount = sourceCard.GetPendingPower();
                        break;
                    }
                    case Condition.DAMAGED:
                    {
                        List<TileState> occupiedTiles;

                        if (ability.damageAmountCondition.filters.HasFlag(FilterCondition.PLAYER) &&
                            ability.damageAmountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles();
                        }
                        else if (ability.damageAmountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles(player.GetTeam() == Team.Client ? Team.Opponent : Team.Client);
                        }
                        else
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles(player.GetTeam());
                        }

                        for (var i = 0; i < occupiedTiles.Count; i++)
                        {
                            if (occupiedTiles[i].Card.GetPendingHealth() < occupiedTiles[i].Card.GetMaxHealthWithBuffs())
                            {
                                damageAmount++;
                            }
                        }

                        break;
                    }
                    case Condition.DAMAGED_MINIONS:
                    {
                        List<TileState> occupiedTiles;

                        if (ability.damageAmountCondition.filters.HasFlag(FilterCondition.PLAYER) &&
                            ability.damageAmountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles();
                        }
                        else if (ability.damageAmountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles(player.GetTeam() == Team.Client ? Team.Opponent : Team.Client);
                        }
                        else
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles(player.GetTeam());
                        }

                        for (var i = 0; i < occupiedTiles.Count; i++)
                        {
                            if (occupiedTiles[i].Card.GetPendingHealth() < occupiedTiles[i].Card.GetMaxHealthWithBuffs())
                            {
                                damageAmount++;
                            }
                        }

                        break;
                    }
                    case Condition.TRAPS_SET:
                    {
                        List<TileState> occupiedTiles;

                        if (ability.damageAmountCondition.filters.HasFlag(FilterCondition.PLAYER))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles( player.GetTeam() );
                        }
                        else
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles();
                        }

                        var traps = 0;

                        for (var i = 0; i < occupiedTiles.Count; i++)
                        {
                            var card = GameServer.State.GetCard(occupiedTiles[i].CardId);
                            if(card.characterData.type == CardType.Trick) traps++;
                        }

                        damageAmount = ability.damageValue * traps;
                        break;
                    }
                    case Condition.CARDS_PLAYED_THIS_TURN:
                    {
                        damageAmount = GameServer.GetPlayerState(player.Uid).cardsPlayedThisTurn;
                        break;
                    }
                    case Condition.NUMBER_OF_SPECIFIC_CARDS_IN_DECK:
                    {
                        if (ability.damageAmountCondition.filters.HasFlag(FilterCondition.PLAYER))
                        {
                            damageAmount = GameServer.GetPlayerState(player.Uid)
                                .GetNumberOfCardsWithIdInDeck(ability.damageAmountCondition.cardId);
                        }
                        else if (ability.damageAmountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            damageAmount = GameServer.GetPlayerState(player.GetTeam() == Team.Client ? Team.Opponent : Team.Client)
                                .GetNumberOfCardsWithIdInDeck(ability.damageAmountCondition.cardId);
                        }
                        break;
                    }
                    case Condition.NUMBER_OF_SPECIFIC_CARDS_ON_FIELD:
                    {
                        var cardsOnField = 0;
                        List<TileState> occupiedTiles;

                        if (ability.damageAmountCondition.filters.HasFlag(FilterCondition.PLAYER) &&
                            ability.damageAmountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles();
                        }
                        else if (ability.damageAmountCondition.filters.HasFlag(FilterCondition.PLAYER))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles(player.GetTeam());
                        }
                        else
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles(player.GetTeam() == Team.Client ? Team.Opponent : Team.Client);
                        }

                        for (var i = 0; i < occupiedTiles.Count; i++)
                        {
                            var characterId = occupiedTiles[i].Card.characterData.id;

                            if (ability.damageAmountCondition.cardId == "BandWorshiper" &&
                                occupiedTiles[i].Card.characterData.id == "DevilcoreGroupie")
                            {
                                characterId = "BandWorshiper";
                            }

                            if (characterId == ability.damageAmountCondition.cardId)
                            {
                                cardsOnField++;
                            }
                        }

                        damageAmount = ability.damageValue * cardsOnField;
                        break;
                    }
                    case Condition.TARGETS_POWER:
                    {
                        if (targets.Count > 0)
                        {
                            var target = GameServer.State.GetCard(targets[0]);

                            if (target!=null)
                            {
                                damageAmount = target.GetPendingPower();
                            }
                        }
                        break;
                    }
                    case Condition.COST_OF_LAST_DRAWN_CARD:
                    {
                        var lastPlayCommand = GameServer.State.GetLastHistoryOf(CommandType.DRAW_CARD);
                        if (lastPlayCommand.Type == CommandType.DRAW_CARD)
                        {
                            if (lastPlayCommand.TargetUids.Count > 0)
                            {
                                var playedCard = GameServer.State.GetCard(lastPlayCommand.TargetUids[lastPlayCommand.TargetUids.Count-1]);

                                if (playedCard != null)
                                {
                                    damageAmount = playedCard.GetCost();
                                }
                                else
                                {
                                    Debug.LogError("Card is null???");
                                }
                            }
                            else
                            {
                                Debug.LogError("Did not draw any cards from draw command?");
                            }

                        }
                        else
                        {
                            Debug.LogError($"Could not find play command! {lastPlayCommand.Type}");

                            for (var i = 0; i < GameServer.State.History.Count; i++)
                            {
                                Debug.LogError($"History: {GameServer.State.History[i].Type}");
                            }
                        }
                        break;
                    }
                    default:
                    {
                        damageAmount = ability.damageValue;
                        break;
                    }
                }
            }
            else if (ability.keyword == AbilityKeyword.THORNS)
            {
                damageAmount = ability.damageValue + sourceCard.thornsIncrease;
            }
            else
            {
                damageAmount = ability.damageValue;
            }

            var spellDamage = 0;

            if (ability.damageSchool != Class.All)
            {
                spellDamage = GameServer.State.AuraUpdater.GetSpellDamageForSchool(player.Uid, ability.damageSchool);
            }

            var additionalDamage = GameServer.State.AuraUpdater.GetAdditionalDamage(sourceUid);

            return damageAmount + spellDamage + sourceCard.modifierIncrease + additionalDamage;
        }

        public int GetRepeatCount(Ability ability)
        {
            var repeatCount = 1;

            if (ability.repeatDamageBasedOnConditions)
            {
                switch (ability.repeatCountCondition.condition)
                {
                    case Condition.CLASS_ON_FIELD:
                    {
                        List<TileState> occupiedTiles;

                        if (ability.damageAmountCondition.filters.HasFlag(FilterCondition.PLAYER) &&
                            ability.damageAmountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles();
                        }
                        else if (ability.damageAmountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles(GetTeam() == Team.Client ? Team.Opponent : Team.Client);
                        }
                        else
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles(GetTeam());
                        }

                        for (var i = 0; i < occupiedTiles.Count; i++)
                        {
                            if (occupiedTiles[i].Card.data.@class == ability.repeatCountCondition.@class)
                            {
                                repeatCount++;
                            }
                        }

                        break;
                    }
                    case Condition.NUMBER_OF_SPECIFIC_CARDS_IN_DECK:
                    {
                        if (ability.repeatCountCondition.filters.HasFlag(FilterCondition.PLAYER))
                        {
                            repeatCount = GameServer.GetPlayerState(GetTeam())
                                .GetNumberOfCardsWithIdInDeck(ability.repeatCountCondition.cardId);
                        }
                        else if (ability.repeatCountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            repeatCount = GameServer.GetPlayerState(GetTeam() == Team.Client ? Team.Opponent : Team.Client)
                                .GetNumberOfCardsWithIdInDeck(ability.repeatCountCondition.cardId);
                        }
                        break;
                    }
                    case Condition.NUMBER_OF_SPECIFIC_CARDS_ON_FIELD:
                    {
                        var cardsOnField = 0;
                        List<TileState> occupiedTiles;

                        if (ability.repeatCountCondition.filters.HasFlag(FilterCondition.PLAYER) &&
                            ability.repeatCountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles();
                        }
                        else if (ability.repeatCountCondition.filters.HasFlag(FilterCondition.PLAYER))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles(GetTeam());
                        }
                        else
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles(GetTeam() == Team.Client ? Team.Opponent : Team.Client);
                        }

                        for (var i = 0; i < occupiedTiles.Count; i++)
                        {
                            var characterId = occupiedTiles[i].Card.characterData.id;

                            if (ability.repeatCountCondition.cardId == "BandWorshiper" &&
                                occupiedTiles[i].Card.characterData.id == "DevilcoreGroupie")
                            {
                                characterId = "BandWorshiper";
                            }

                            if (characterId == ability.repeatCountCondition.cardId)
                            {
                                cardsOnField++;
                            }
                        }

                        repeatCount = cardsOnField + 1;
                        break;
                    }
                    case Condition.PREVIOUS_COMMAND_TARGETS:
                    {
                        var lastDestroyCommand = GameServer.State.GetHistories(CommandType.DESTROY);

                        for (var i = 0; i < lastDestroyCommand.Count; i++)
                        {
                            if (lastDestroyCommand[i].SourceUid == SourceUid)
                            {
                                repeatCount = lastDestroyCommand[i].TargetUids.Count + 1;
                                break;
                            }
                        }

                        break;
                    }
                }
            }

            return repeatCount;
        }

        public override void OnExecuteCommand()
        {
            PendingDamageHistories=new List<CardHistory>();

            if (RandomlySplit)
            {
                var random = new System.Random();
                var source = GameServer.State.GetCard(SourceUid);

                UseDifferentAmounts = true;

                var currentAmount = Amount;

                while (currentAmount > 0)
                {
                    GameCardState target=null;

                    while (target == null || target.GetPendingHealth() <= 0)
                    {
                        if (TargetUids.Count <= 0)
                        {
                            break;
                        }

                        var randomTarget = TargetUids[random.Next(0, TargetUids.Count)];
                        target = GameServer.State.GetCard(randomTarget);

                        if (target == null || target.GetPendingHealth() <= 0)
                        {
                            TargetUids.Remove(randomTarget);
                            target = null;
                        }
                    }

                    if (target == null)
                    {
                        break;
                    }

                    DamageCard(target, 1, source);
                    currentAmount--;
                }

                TargetUids.Clear();

                for (var i = 0; i < PendingDamageHistories.Count; i++)
                {
                    TargetUids.Add(PendingDamageHistories[i].CardUid);
                }
            }
            else if (Repeats)
            {
                var random = new System.Random();
                var source = GameServer.State.GetCard(SourceUid);

                for (var i = 0; i < RepeatDamageCount; i++)
                {
                    GameCardState target = null;

                    while (target == null || target.GetPendingHealth() <= 0)
                    {
                        if (TargetUids.Count <= 0)
                        {
                            break;
                        }

                        var randomTarget = TargetUids[random.Next(0, TargetUids.Count)];
                        target = GameServer.State.GetCard(randomTarget);

                        if (target == null || target.GetPendingHealth() <= 0)
                        {
                            TargetUids.Remove(randomTarget);
                            target = null;
                        }
                    }

                    if (target == null)
                    {
                        Debug.LogWarning($"card is null in Damage Command!");
                        continue;
                    }

                    DamageCard(target, Amount, source);
                }

                TargetUids.Clear();

                for (var i = 0; i < PendingDamageHistories.Count; i++)
                {
                    TargetUids.Add(PendingDamageHistories[i].CardUid);
                }
            }
            else
            {
                var source = GameServer.State.GetCard(SourceUid);

                for (var i = 0; i < TargetUids.Count; i++)
                {
                    var card = GameServer.State.GetCard(TargetUids[i]);

                    if (card == null)
                    {
                        Debug.LogWarning($"card is null in Damage Command!");
                        continue;
                    }

                    var amount = Amount;

                    if (UseDifferentAmounts)
                    {
                        amount = Amounts[i];
                    }

                    DamageCard(card, amount, source);
                }

                TargetUids.Clear();

                for (var i = 0; i < PendingDamageHistories.Count; i++)
                {
                    TargetUids.Add(PendingDamageHistories[i].CardUid);
                }
            }
        }

        private void DamageCard(GameCardState target, int amount, GameCardState source)
        {
            var healthCurr = target.GetPendingHealth();
            var damageHistories = target.DealDamage(SourceUid, amount);
            var healthPending = target.GetPendingHealth();
            var healthDelta = healthCurr - healthPending;

            if (damageHistories.Count <= 0)
            {
                return;
            }

            PendingDamageHistories.AddRange(damageHistories);

            if (healthDelta > 0)
            {
                global::GameLogic.ResolveTriggeredCards(source, target, TriggerType.DAMAGED);

                // check if healthPending is still correct.
                healthPending = target.GetPendingHealth();
            }

            if (healthCurr > 0 && healthPending <= 0 && !target.IsHero)
            {
                DestroyCommand.DestroyCard(target, source, this, this);
            }

            if (source != null && source.data != null && source.HasAbility(AbilityKeyword.LIFESTEAL))
            {
                var healCommand = new HealCommand
                {
                    PlayerUid = PlayerUid,
                    SourceUid = SourceUid,
                    TargetUids = new List<string> { GameServer.GetPlayerState(PlayerUid).heroUid },
                    Amount = amount
                };

                global::GameLogic.ExecuteCommand(healCommand);
            }
        }

        public void ApplyDamageHistories()
        {
            for (var i = 0; i < PendingDamageHistories.Count; i++)
            {
                var damageHistory = PendingDamageHistories[i];
                var damagedCardState = GameServer.State.GetCard(damageHistory.CardUid);
                damagedCardState?.ApplyHistory(damageHistory.Uid);

                if (damagedCardState == null)
                {
                    Debug.LogError($"Could not find {damageHistory.CardUid} in visible cards?");
                }
            }
        }

        public override IEnumerator<float> Resolve()
        {
            var sourceCard = GameServer.State.GetCard(SourceUid);

            if (sourceCard == null)
            {
                yield break;
            }

            if (VisualResolvedInPreviousCommand)
            {
                //Debug.LogError($"Visual is resolved from other command! {sourceCard.data.name} deathrattle? {sourceCard.HasTrigger(TriggerType.DEATHRATTLE)}");

                for (var i = 0; i < PendingDamageHistories.Count; i++)
                {
                    var targetCard = GameServer.State.GetCard(PendingDamageHistories[i].CardUid);

                    if (targetCard.HasTrigger(TriggerType.DEATHRATTLE) && targetCard.IsDead())
                    {
                        yield return Timing.WaitForSeconds(2);
                        break;
                    }
                }

                yield break;
            }

            Ability damageAbility = GetAbility();

            // gets the ability if it was returned null
            if (damageAbility == null && sourceCard.GetAbilities().Count > 0)
            {
                damageAbility = sourceCard.GetAbilities().Find(ability => ability.keyword == AbilityKeyword.DAMAGE || ability.keyword == AbilityKeyword.DESTROY);
            }

            // ========================================================================================================
            // SINGLE EFFECTS THAT ARE APPLIED TO THE BOARD CARD THAT IS CALLING THE EFFECT
            // if the card has a "custom" effect the effect will be spawned on the minion and that effect must handle
            // every target within the effect.
            // You can turn this off by ticking "spawnEffectOnEachTarget" in the ability settings.
            // This will skip this part and spawn the "custom effect" on top of every "target"
            // ========================================================================================================

            if (damageAbility != null && damageAbility.usesCustomEffect && damageAbility.usesBoardEffect)
            {
                yield return Timing.WaitUntilDone(PlayCustomOrGenericEffectOnBoardCard(damageAbility, this, SourceUid, null));
                yield break;
            }

            switch (sourceCard.characterData.type)
            {
                case CardType.Minion when sourceCard.location == CardLocation.Board:
                {
                    var getBoardCardTask = GameBoard.GetBoardCard(SourceUid);
                    yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                    var getBoardCardResult = getBoardCardTask.Result;

                    if (getBoardCardResult.error != ClientErrorCode.None)
                    {
                        Server.HandleClientError(getBoardCardResult, false);
                    }

                    if (getBoardCardResult.boardCard)
                    {
                        var boardCard = getBoardCardResult.boardCard;

                        if (damageAbility != null && damageAbility.usesCustomEffect && !damageAbility.spawnEffectOnEachTarget)
                        {
                            var vfxComponent =
                                (GameVfxComponent)getBoardCardResult.boardCard.GetComponent(
                                    System.Type.GetType(damageAbility.customEffectComponent));

                            if (vfxComponent != null)
                            {
                                var vfxParams = new VfxParameters();
                                vfxParams.OnApply += ApplyDamageHistories;
                                // the custom effect needs to handle ALL applies
                                yield return Timing.WaitUntilDone(vfxComponent.Effect.Play(this, vfxParams));
                                yield break;
                            }
                        }
                        else
                        {
                            Timing.RunCoroutine(getBoardCardResult.boardCard.AnimateEffect<GenericEffectComponent>(this));
                        }
                    }
                    break;
                }
            }

            yield return Timing.WaitForSeconds(0.4f);

            if (damageAbility == null || !damageAbility.applyDamageInEffect)
            {
                ApplyDamageHistories();
            }

            // ========================================================================================================
            // EFFECTS THAT ARE SPAWNED ON EACH TARGET GO HERE.
            // If there's a custom effect, the custom effect will try to be applied to every hero and minion
            // it also calls the apply history and "hits" them generically after the effect.
            // you can turn off hitting generically by ticking "Apply Damage In Effect"
            // The effect will have to handle Applying History and Doing the generic damage (or some other damage animation)
            // ========================================================================================================

            switch (sourceCard.characterData.type)
            {
                case CardType.Minion:
                {
                    var damagedBoardCards = new List<GameCardView>();
                    var damagedHeroes = new List<HeroViewBattle>();
                    var damagedHeroesUids = new List<string>();
                    var damagedHeroesDamageUid = new List<string>();

                    for (var i = 0; i < PendingDamageHistories.Count; i++)
                    {
                        var damageHistory = PendingDamageHistories[i];
                        var card = GameServer.State.GetCard(damageHistory.CardUid);
                        if (card.IsHero)
                        {
                            var heroView = GameplayScreen.Instance.heroSideBarContainer.GetHeroView(card.Team);
                            damagedHeroes.Add(heroView);
                            damagedHeroesUids.Add(card.uid);
                            damagedHeroesDamageUid.Add(damageHistory.Uid);
                        }
                        else
                        {
                            var getBoardCardTask = GameBoard.GetBoardCard(damageHistory.CardUid);
                            yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                            var getBoardCardResult = getBoardCardTask.Result;

                            if (getBoardCardResult.error != ClientErrorCode.None)
                            {
                                Server.HandleClientError(getBoardCardResult, true);
                            }

                            if (getBoardCardResult.boardCard)
                            {
                                damagedBoardCards.Add(getBoardCardResult.boardCard);
                            }
                        }
                    }

                    var ability = GetAbility();

                    if (ability != null && ability.usesCustomEffect)
                    {
                        for (var i = 0; i < damagedBoardCards.Count; i++)
                        {
                            var damagedBoardCard = damagedBoardCards[i];
                            // var damageFx = damagedBoardCard.View.GetVfx<DamageEffectComponent, DamageEffect>();
                            // var amount = PendingDamageHistories[i].DamageAmount;

                            if (i == damagedBoardCards.Count - 1)
                            {
                                yield return Timing.WaitUntilDone(PlayCustomOrGenericEffectOnBoardCard(ability, this, damagedBoardCard.State.uid, damagedBoardCard));
                            }
                            else
                            {
                                Timing.RunCoroutine(PlayCustomOrGenericEffectOnBoardCard(ability, this, damagedBoardCard.State.uid, damagedBoardCard));
                            }
                        }

                        for (var i = 0; i < damagedHeroes.Count; i++)
                        {
                            if (i == damagedBoardCards.Count - 1)
                            {
                                yield return Timing.WaitUntilDone(PlayCustomOrGenericEffectOnBoardCard(ability, this, damagedHeroesUids[i], null, damagedHeroes[i]));
                            }
                            else
                            {
                                Timing.RunCoroutine(PlayCustomOrGenericEffectOnBoardCard(ability, this, damagedHeroesUids[i], null, damagedHeroes[i]));
                            }
                        }
                    }

                    for (var i = 0; i < damagedBoardCards.Count; i++)
                    {
                        var damagedBoardCard = damagedBoardCards[i];
                        var damageFx = damagedBoardCard.View.GetVfx<DamageEffectComponent, DamageEffect>();

                        var amount = PendingDamageHistories[i].DamageAmount;

                        if (i == damagedBoardCards.Count - 1 || damagedBoardCard.State.HasTrigger(TriggerType.DEATHRATTLE))
                        {
                            if (retaliateCommand)
                            {
                                var attackType = sourceCard.data.attackType;
                                switch (attackType)
                                {
                                    case AttackType.Melee:
                                        AudioController.PlaySound("battle_impact_melee", "BARDSFX", true, damagedBoardCard.gameObject).setPitch(0.75f);
                                        break;
                                    case AttackType.Ranged:
                                        AudioController.PlaySound("battle_impact_ranged", "BARDSFX", true, damagedBoardCard.gameObject).setPitch(0.75f);
                                            break;
                                    case AttackType.Arcane:
                                        AudioController.PlaySound("battle_impact_arcane", "BARDSFX", true, damagedBoardCard.gameObject).setPitch(0.75f);
                                            break;
                                }
                            }
                            yield return Timing.WaitUntilDone(damageFx.GenericDamageAnimation(amount));
                        }
                        else
                        {
                            Timing.RunCoroutine(damageFx.GenericDamageAnimation(amount));
                        }
                    }

                    for (var i = 0; i < damagedHeroes.Count; i++)
                    {
                        var heroTarget = damagedHeroes[i];
                        heroTarget.State.ApplyHistory(damagedHeroesDamageUid[i]);
                        yield return Timing.WaitUntilDone(GameplayScreen.Instance.heroSideBarContainer.HitPortraitNonAttack(heroTarget.State.Team, this, damagedHeroesDamageUid[i]));
                    }

                    break;
                }
                default:
                {
                    if (PendingDamageHistories != null && PendingDamageHistories.Count > 0)
                    {
                        var damagedBoardCards = new List<GameCardView>();
                        var damagedBoardCardsPendingHistories = new List<CardHistory>();
                        var damagedHeroes = new List<HeroViewBattle>();
                        var damagedHeroesUids = new List<string>();
                        var damagedHeroesDamageUid = new List<string>();

                        for (var i = 0; i < PendingDamageHistories.Count; i++)
                        {
                            var damageHistory = PendingDamageHistories[i];
                            var card = GameServer.State.GetCard(damageHistory.CardUid);

                            if (card.IsHero)
                            {
                                var heroView = GameplayScreen.Instance.heroSideBarContainer.GetHeroView(card.Team);
                                damagedHeroes.Add(heroView);
                                damagedHeroesUids.Add(card.uid);
                                damagedHeroesDamageUid.Add(damageHistory.Uid);
                            }
                        }

                        for (var i = 0; i < PendingDamageHistories.Count; i++)
                        {
                            var damageHistory = PendingDamageHistories[i];
                            var targetCard = GameServer.State.GetCard(damageHistory.CardUid);

                            if (!targetCard.IsHero)
                            {
                                var getBoardCardTask = GameBoard.GetBoardCard(damageHistory.CardUid);
                                yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                                var getBoardCardResult = getBoardCardTask.Result;

                                if (getBoardCardResult.error != ClientErrorCode.None)
                                {
                                    Server.HandleClientError(getBoardCardResult, true);
                                    continue;
                                }

                                damagedBoardCards.Add(getBoardCardResult.boardCard);
                                damagedBoardCardsPendingHistories.Add(damageHistory);
                            }
                        }

                        // custom effect should handle all the damage effects / custom effects on each card (using TargetUids)
                        if (sourceCard.data != null && sourceCard.data.usesCustomEffect /*&& damagedBoardCards.Count > 0*/)
                        {
                            switch (sourceCard.data.parentBehaviour)
                            {
                                // Effect that covers the whole screen
                                case EffectParentBehaviour.GameBoard:
                                {
                                    var loadTask = GameVfxProvider.GetVfxForSpell(sourceCard, null);
                                    yield return Timing.WaitUntilDone(loadTask.AsCoroutine());
                                    loadTask.Result.Initialize(card: null);
                                    yield return Timing.WaitUntilDone(loadTask.Result.Play(this, new VfxParameters()));
                                    break;
                                }

                                // Effect that touches each card at the same time
                                default:
                                {
                                    for (var i = 0; i < damagedBoardCards.Count; i++)
                                    {
                                        var loadTask = GameVfxProvider.GetVfxForSpell(sourceCard, damagedBoardCards[i]);
                                        yield return Timing.WaitUntilDone(loadTask.AsCoroutine());
                                        loadTask.Result.Initialize(damagedBoardCards[i].GetComponent<CardView>());
                                        var vfxParams = new VfxParameters { targetUid = damagedBoardCards[i].State.uid, historyUid = damagedBoardCardsPendingHistories[i].Uid };
                                        if (i == damagedBoardCards.Count - 1)
                                        {
                                            yield return Timing.WaitUntilDone(loadTask.Result.Play(this, vfxParams));
                                        }
                                        else
                                        {
                                            Timing.RunCoroutine(loadTask.Result.Play(this, vfxParams));
                                        }
                                    }

                                    for (var i = 0; i < damagedHeroes.Count; i++)
                                    {
                                        var loadTask = GameVfxProvider.GetVfxForSpell(sourceCard, damagedHeroes[i].boardTarget.RectTransform(), false);
                                        yield return Timing.WaitUntilDone(loadTask.AsCoroutine());
                                        loadTask.Result.Initialize(damagedHeroes[i].boardTarget.RectTransform());

                                        var vfxParams = new VfxParameters { targetUid = damagedHeroesUids[i], historyUid = damagedHeroesDamageUid[i] };

                                        if (i == damagedHeroes.Count - 1)
                                        {
                                            yield return Timing.WaitUntilDone(loadTask.Result.Play(this, vfxParams));
                                        }
                                        else
                                        {
                                            Timing.RunCoroutine(loadTask.Result.Play(this, vfxParams));
                                        }
                                    }

                                    if (damageAbility == null || !damageAbility.applyDamageInEffect)
                                    {
                                        for (var i = 0; i < damagedBoardCards.Count; i++)
                                        {
                                            var damagedBoardCard = damagedBoardCards[i];
                                            var damageFx = damagedBoardCard.View
                                                .GetVfx<DamageEffectComponent, DamageEffect>();
                                            var amount = damagedBoardCardsPendingHistories[i].DamageAmount;

                                            if (i == damagedBoardCards.Count - 1 ||
                                                damagedBoardCard.State.HasTrigger(TriggerType.DEATHRATTLE))
                                            {
                                                yield return Timing.WaitUntilDone(
                                                    damageFx.GenericDamageAnimation(amount));
                                            }
                                            else
                                            {
                                                Timing.RunCoroutine(damageFx.GenericDamageAnimation(amount));
                                            }
                                        }
                                    }

                                    break;
                                }
                            }
                        }
                        else if (damagedBoardCards.Count > 0 || damagedHeroes.Count > 0) // just do a generic effect on all the cards if nothing custom was created
                        {
                            for (var i = 0; i < damagedBoardCards.Count; i++)
                            {
                                var damagedBoardCard = damagedBoardCards[i];
                                var damageFx = damagedBoardCard.View.GetVfx<DamageEffectComponent, DamageEffect>();

                                if (Repeats)
                                {
                                    Timing.RunCoroutine(
                                        damageFx.GenericDamageAnimation(PendingDamageHistories[i].DamageAmount));
                                    yield return Timing.WaitForSeconds(0.4f);
                                }
                                else
                                {
                                    Timing.RunCoroutine(
                                        damageFx.GenericDamageAnimation(PendingDamageHistories[i].DamageAmount));
                                }
                            }

                            if (damagedBoardCards.Exists(c => c.State.GetPendingHealth() <= 0 && c.State.HasTrigger(TriggerType.DEATHRATTLE)))
                            {
                                yield return Timing.WaitForSeconds(3);
                            }

                            for (var i = 0; i < damagedHeroes.Count; i++)
                            {
                                var heroTarget = damagedHeroes[i];
                                heroTarget.State.ApplyHistory(damagedHeroesDamageUid[i]);
                                if (Repeats)
                                {
                                    Timing.RunCoroutine(GameplayScreen.Instance.heroSideBarContainer.HitPortraitNonAttack(heroTarget.State.Team, this, damagedHeroesDamageUid[i]));
                                    yield return Timing.WaitForSeconds(0.4f);
                                }
                                else
                                {
                                    Timing.RunCoroutine(GameplayScreen.Instance.heroSideBarContainer.HitPortraitNonAttack(heroTarget.State.Team,this, damagedHeroesDamageUid[i]));
                                }

                            }
                        }
                        else
                        {
                            Server.HandleClientError(new ClientResult {error = ClientErrorCode.BoardCardNotFound}, false);
                        }
                    }
                    else
                    {
                        Debug.LogError("There were no targets in this Damage command!");
                    }
                    break;
                }
            }
        }
    }
}
