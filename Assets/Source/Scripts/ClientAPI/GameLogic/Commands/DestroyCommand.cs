using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using MEC;
using UnityEngine;
using UnityEngine.XR;

namespace CrossBlitz.ClientAPI.Commands
{
    public class DestroyCommand : Command
    {
        public DestroyCommand() => Type = CommandType.DESTROY;

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            return 10;
        }

        public override string GetLabelDisplay()
        {
            return base.GetLabelDisplay() + $"{GetTargetsAsString(this)}";
        }

        public override void OnExecuteCommand()
        {
            for (var i = 0; i < TargetUids.Count; i++)
            {
                var card = GameServer.State.GetCard(TargetUids[i]);

                if (card == null)
                {
                    Debug.LogError($"card is null in Destroy Command!");
                    continue;
                }

                if (card.location == CardLocation.Deck)
                {
                    // handles removing the card from the players deck!
                    card.SetCardLocation(CardLocation.Graveyard);
                }
                else
                {
                    DestroyCard(card, null, this, this);
                }
            }
        }

        public static void DestroyCard(GameCardState card, GameCardState cardThatDestroyedThisCard, Command parentCommand, Command previousCommand)
        {
            card.SetAsPendingDead(true, cardThatDestroyedThisCard != null ? cardThatDestroyedThisCard.uid : string.Empty);

            var deathrattles = card.GetAbilities().FindAll(ability => ability.triggerType == TriggerType.DEATHRATTLE);

            if (deathrattles.Count > 0)
            {
                var deathrattleCounter = GameServer.State.AuraUpdater.GetDeathrattleCount(card.owner);

                for (var j = 0; j < deathrattleCounter; j++)
                {
                    for (var k = 0; k < deathrattles.Count; k++)
                    {
                        var deathrattleCommands = global::GameLogic.CreateCommandFromAbility(card, deathrattles[k], parentCommand, previousCommand);
                        global::GameLogic.ExecuteCommand(deathrattleCommands);

                        Command deathrattle = null;

                        if (deathrattleCommands.Count > 0)
                        {
                            deathrattle = deathrattleCommands[0];
                        }

                        var linkedAbilities = card.GetAbilities().FindAll(ability => ability.triggerType == TriggerType.LINKED_ABILITY);

                        for (var l = 0; l < linkedAbilities.Count; l++)
                        {
                            global::GameLogic.ExecuteCommand(global::GameLogic.CreateCommandFromAbility(card, linkedAbilities[l], parentCommand, deathrattle));
                        }
                    }
                }
            }

            if (cardThatDestroyedThisCard != null && cardThatDestroyedThisCard.uid != card.uid)
            {
                var plunders = cardThatDestroyedThisCard.GetAbilities().FindAll(ability => ability.triggerType == TriggerType.PLUNDER);
                if (plunders.Count > 0)
                {
                    var plunderCounter = GameServer.State.AuraUpdater.GetPlunderCount(parentCommand.PlayerUid);

                    for (var i = 0; i < plunderCounter; i++)
                    {
                        for (var j = 0; j < plunders.Count; j++)
                        {
                            var plunderCommands = global::GameLogic.CreateCommandFromAbility(cardThatDestroyedThisCard, plunders[j], parentCommand, previousCommand);
                            global::GameLogic.ExecuteCommand(plunderCommands);

                            Command plunder = null;

                            if (plunderCommands.Count > 0)
                            {
                                plunder = plunderCommands[0];
                            }

                            var linkedAbilities = cardThatDestroyedThisCard.GetAbilities().FindAll(ability => ability.triggerType == TriggerType.LINKED_ABILITY);

                            for (var k = 0; k < linkedAbilities.Count; k++)
                            {
                                global::GameLogic.ExecuteCommand(global::GameLogic.CreateCommandFromAbility(cardThatDestroyedThisCard, linkedAbilities[k], parentCommand, plunder));
                            }
                        }
                    }
                }
            }

            card.SetAsPendingDead(false);
            global::GameLogic.ResolveTriggeredCards(GameServer.State.GetCard(parentCommand.SourceUid), card, TriggerType.MINION_DESTROYED, new global::GameLogic.ResolveTriggeredCardsProperties { showLog = true });

            if (card.location == CardLocation.Board)
            {
                GameServer.State.Board.DestroyCardFromTile(card.uid);
            }
            else
            {
                card.Destroy();
            }

            if (card.HasAbility(AbilityKeyword.RELENTLESS))
            {
                // dumb little trip, code might look weird.
                // - The Relentless Keyword: Resummon this minion the first time it dies.
                // - Check if the card has the relentless keyword first.
                // - Check if the card has used it already (it's exhausted).
                // - if it's exhausted, unexhaust it and kill this minion like normal.
                // - if it's not exhausted, still kill this minion off but summon a copy of it in the same place and exhaust the keyword.
                // - why?
                // - I want both minions to keep their keywords unexhausted in the graveyard, in case there are resummon effects.

                var relentlessAbility = card.GetAbility(AbilityKeyword.RELENTLESS);
                var relentlessAbilityState = card.GetAbilityState(relentlessAbility);
                var relentlessAbilityIndex = card.GetAbilityIndex(relentlessAbility);


                if (relentlessAbilityState.exhausted)
                {
                    relentlessAbilityState.exhausted = false;
                }
                else
                {
                    var summonAbility = new Ability();
                    summonAbility.keyword = AbilityKeyword.SUMMON;
                    summonAbility.summonMinionData = new SummonMinionData
                    {
                        count = 1,
                        placement = SummonPlacement.EXACT_POSITION,
                        tilePosition = card.lastPosition,
                        summonFrom = SummonMinionData.SummonFrom.SpecificCard,
                        cardId = card.data.id,
                        summonVisualEffect = SummonMinionData.SummonVisualEffect.Blood,
                        relentlessSummon = true,
                    };

                    var abilities = global::GameLogic.CreateCommandFromAbility(card, summonAbility, null, null, overrideAbilityIndex: relentlessAbilityIndex);
                    global::GameLogic.ExecuteCommand(abilities);
                }
            }

        }

        public override IEnumerator<float> Resolve()
        {
            var destroyCards = new List<GameCardView>();
            var ability = GetAbility();

            if (ability != null && ability.usesCustomEffect && ability.usesBoardEffect)
            {
                yield return Timing.WaitUntilDone(PlayCustomOrGenericEffectOnBoardCard(ability, this, string.Empty));
                yield break;
            }

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var targetCard = GameServer.State.GetCard(TargetUids[i]);

                if (targetCard == null)
                {
                    continue;
                }

                if (targetCard.lastLocation == CardLocation.Board && (targetCard.characterData.type == CardType.Minion || targetCard.characterData.type == CardType.Trick))
                {
                    var getBoardCardTask = GameBoard.GetBoardCard(TargetUids[i]);

                    yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());

                    var getBoardCardResult = getBoardCardTask.Result;

                    if (getBoardCardResult.error != ClientErrorCode.None)
                    {
                        Server.HandleClientError(getBoardCardResult, true);
                    }

                    if (getBoardCardResult.boardCard)
                    {
                        destroyCards.Add(getBoardCardResult.boardCard);
                    }
                }
                else if (targetCard.lastLocation == CardLocation.Deck)
                {
                    yield return Timing.WaitUntilDone(DrawCardCommand.DrawCard(TargetUids[i], targetCard.Team, true,
                        i >= TargetUids.Count - 1));
                }
            }

            // destroyed cards are only added if it was on board.
            for (var i = 0; i < destroyCards.Count; i++)
            {
                if (ability != null && ability.usesCustomEffect)
                {
                    yield return Timing.WaitUntilDone(PlayCustomOrGenericEffectOnBoardCard(ability, this, destroyCards[i].State.uid, destroyCards[i]));
                }

                yield return Timing.WaitUntilDone(DeathEffect.EvaluateDeath( destroyCards[i].State, destroyCards[i].View));
            }
        }
    }
}