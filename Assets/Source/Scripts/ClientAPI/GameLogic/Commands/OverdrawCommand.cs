using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;

namespace CrossBlitz.ClientAPI.Commands
{
    [System.Serializable]
    public class OverdrawCommand : Command
    {
        public OverdrawCommand()
        {
            Type = CommandType.OVERDRAW;
        }

        public override string GetLabelDisplay()
        {
            return base.GetLabelDisplay() + $"{PlayerUid} overdrew! Destroyed card {TargetUids[0]}";
        }

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            return IsClientCommand() ? TargetUids.Count : -TargetUids.Count;
        }

        public override void OnExecuteCommand()
        {
            if (TargetUids.Count <= 0) return;

            var card = GameServer.State.GetCard(TargetUids[0]);
            card.SetCardLocation(CardLocation.Graveyard);
        }

        public override IEnumerator<float> Resolve()
        {
            yield return Timing.WaitUntilDone(DrawCardCommand.DrawCard(TargetUids[0],GetTeam(), true, true));
        }
    }
}