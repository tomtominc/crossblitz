using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.Databases;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    public class TriggerAllDeathrattlesCommand : Command
    {
        public TriggerAllDeathrattlesCommand() => Type = CommandType.TRIGGER_ALL_DEATHRATTLES;

        public static List<string> GetTargets()
        {
            var targets = new List<string>();
            var minionsInGraveyard = GameServer.State.GetCardsInLocation(CardLocation.Graveyard);

            for (var i = 0; i < minionsInGraveyard.Count; i++)
            {
                if (minionsInGraveyard[i].characterData.type == CardType.Minion)
                {
                    var rawMinion = Db.CardDatabase.GetCard(minionsInGraveyard[i].characterData.id);
                    var abilities = rawMinion.GetAbilities();

                    if (abilities.Exists(a => a.triggerType == TriggerType.DEATHRATTLE))
                    {
                        targets.Add(minionsInGraveyard[i].uid);
                    }

                    var deathrattles = GameUtilities.Copy(abilities);
                }
            }

            return targets;
        }

        public override void OnExecuteCommand()
        {
            var source = GameServer.State.GetCard(SourceUid);

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var card = GameServer.State.GetCard(TargetUids[i]);
                var rawMinion = Db.CardDatabase.GetCard(card.characterData.id);
                var abilities = rawMinion.GetAbilities();
                var deathrattles = abilities.FindAll(ability => ability.triggerType == TriggerType.DEATHRATTLE);

                if (deathrattles.Count > 0)
                {
                    var deathrattleCounter = GameServer.State.AuraUpdater.GetDeathrattleCount(PlayerUid);
                    var displayEffect = typeof(DeathrattleEffectComponent).AssemblyQualifiedName;

                    for (var j = 0; j < deathrattleCounter; j++)
                    {
                        for (var k = 0; k < deathrattles.Count; k++)
                        {
                            var animateEffectCommand = new AnimateEffectCommand
                            {
                                PlayerUid = PlayerUid,
                                SourceUid = SourceUid,
                                TargetUids = new List<string> { SourceUid },
                                effectType = displayEffect
                            };

                            global::GameLogic.ExecuteCommand(animateEffectCommand);

                            var deathrattleCommands = global::GameLogic.CreateCommandFromAbility(source, deathrattles[k], this, this);
                            global::GameLogic.ExecuteCommand(deathrattleCommands);

                            Command deathrattle = null;

                            if (deathrattleCommands.Count > 0)
                            {
                                deathrattle = deathrattleCommands[0];
                            }

                            var linkedAbilities = abilities.FindAll(ability => ability.triggerType == TriggerType.LINKED_ABILITY);

                            for (var l = 0; l < linkedAbilities.Count; l++)
                            {
                                global::GameLogic.ExecuteCommand(global::GameLogic.CreateCommandFromAbility(source, linkedAbilities[l], this, deathrattle));
                            }
                        }
                    }
                }
            }
        }
        public override IEnumerator<float> Resolve()
        {
            yield break;
        }


    }
}