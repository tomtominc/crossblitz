using System.Collections.Generic;
using System.Threading.Tasks;
using Asyncoroutine;
using BlendModes;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Databases;
using CrossBlitz.GameVfx;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.ServerAPI.GameLogic.Intelligence;
using DG.Tweening;
using KennethDevelops.ProLibrary.Extensions;
using MEC;
using Mono.CSharp;
using Source.Scripts.Server.GameLogic.Data;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace CrossBlitz.ClientAPI.Commands
{
    public class SummonCommand : Command
    {
        public enum Method
        {
            PlayedFromHand,
            PlayedFromDeck,
            PlayedWithAbility,
            PlayedWithAbilityFromHand,
            PlayedWithAbilityFromDeck
        }

        public Method SummonMethod;
        public Vector2Int Position = new Vector2Int(-1,-1);
        public Vector2Int Selection = new Vector2Int(-1,-1);
        public string SelectedHeroUid;

        public bool SummonAllAtOnce;
        public List<Vector2Int> Positions;
        public List<string> CopyFromTargets;

        public SummonCommand()
        {
            Type = CommandType.SUMMON;
        }

        /// <summary>
        /// Adds a new summon command to this command so summons go off at the same time (looks better)
        /// </summary>
        /// <param name="summon">The summon command to append</param>
        public void Append(SummonCommand summon)
        {
            TargetUids.AddRange(summon.TargetUids);
            Positions.AddRange(summon.Positions);
        }

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            var source = GameServer.State.GetCard(SourceUid);
            if (source == null) return 0;
            return source.GetPendingPower() + source.GetPendingHealth() + source.data.aiProperties.strategy.priority;
        }

        /// <summary>
        /// Checks to see if this summon command can be squashed into others
        /// </summary>
        /// <returns></returns>
        public bool CanBeSquashed()
        {
            var ability = GetAbility();

            if (ability != null && ability.summonMinionData.disableSquash)
            {
                return false;
            }

            var sourceCard = GameServer.State.GetCard(SourceUid);

            return SummonMethod == Method.PlayedWithAbility && TargetUids.Count > 0 && Positions.Count > 0 && (sourceCard==null ||
                !sourceCard.data.usesCustomEffect && sourceCard.GetAbilities().Exists(a => a.keyword == AbilityKeyword.SUMMON && !a.usesCustomEffect));
        }

        public override string GetLabelDisplay()
        {
            var label = $"Summoned ({SummonMethod}) ";
            var sourceCard = GameServer.State.GetCard(SourceUid);

            if (TargetUids == null || TargetUids.Count == 0)
            {
                return base.GetLabelDisplay() + label + $"{sourceCard.characterData?.name} at position {Position}.";
            }

            if (Positions == null || Positions.Count == 0)
            {
                var minion = GameServer.State.GetCard( TargetUids[0] );
                return base.GetLabelDisplay() + label + $"{minion.characterData?.name} ({minion.Team}) at position {Position}.";
            }

            for (var i = 0; i < TargetUids.Count; i++)
            {
                if (Positions.Count <= i) break;

                var minion = GameServer.State.GetCard( TargetUids[i] );
                label += $"{minion.characterData?.name} ({minion.Team}) at position {Positions[i]}" + (i == TargetUids?.Count - 1 ? "." : ", ");
            }

            return base.GetLabelDisplay() + label;
        }

        //======================================================================================================
        // GET TARGETS CODE
        //======================================================================================================

        public static List<string> GetMinionsToSummon(GameCardState gameCard, Ability ability, Command parentCommand, Command previousCommand, out List<string> copyFromTargets, out SummonCommand.Method summonMethod)
        {
            summonMethod = Method.PlayedWithAbility;

            var cards = new List<string>();
            var summonProperties = ability.summonMinionData;

            copyFromTargets=new List<string>();

            switch (ability.keyword)
            {
                case AbilityKeyword.SUMMON:
                {
                    if (ability.summonMinionData.disableIfMinionExists)
                    {
                        var occupiedTiles = GameServer.State.Board.GetOccupiedTiles(gameCard.Team);

                        for (var i = 0; i < occupiedTiles.Count; i++)
                        {
                            if (occupiedTiles[i].Card.data.id == ability.summonMinionData.cardId)
                            {
                                return cards;
                            }
                        }
                    }

                    if (ability.targetConditions.HasFlag(FilterCondition.SELF))
                    {
                        cards.Add(gameCard.uid);
                    }
                    else if (ability.targetConditions.HasFlag(FilterCondition.HAND) || ability.targetConditions.HasFlag(FilterCondition.DECK))
                    {
                        summonMethod = Method.PlayedWithAbilityFromHand;
                        var targets = global::GameLogic.GetTargets(gameCard, ability, parentCommand,null, true);
                        if (targets.Count > 0)
                        {
                            cards.AddRange(targets);
                        }
                    }
                    else
                    {
                        if (summonProperties.summonFrom == SummonMinionData.SummonFrom.Filter)
                        {
                            var playerForFaction = GameServer.GetPlayerState(gameCard.owner);

                            List<CardData> cardsFromFilter;

                            switch (summonProperties.potentialCardTargets)
                            {
                                case SummonMinionData.PotentialCardTargets.Card_Database_No_Faction_Restrictions:
                                    cardsFromFilter = CardFilter.GetCardsWithFilter(Db.CardDatabase.Cards, summonProperties.cardFilter);
                                    break;
                                case SummonMinionData.PotentialCardTargets.Card_Database_Faction_Restricted:
                                    cardsFromFilter = CardFilter.GetCardsWithFilter(Db.CardDatabase.GetCardsByFaction(playerForFaction.faction), summonProperties.cardFilter);
                                    break;
                                case SummonMinionData.PotentialCardTargets.GameCards:
                                    cardsFromFilter = CardFilter.GetCardsWithFilter(summonProperties.cardFilter);
                                    break;
                                default:
                                    cardsFromFilter = CardFilter.GetCardsWithFilter(Db.CardDatabase.Cards, summonProperties.cardFilter);
                                    break;
                            }

                            var rand = new System.Random();

                            if (cardsFromFilter.Count > 0)
                            {
                                for (var i = 0; i < summonProperties.count; i++)
                                {
                                    var player = summonProperties.placement.HasFlag(SummonPlacement.PLAYER) ? GameServer.ClientPlayerState : GameServer.OpponentPlayerState;
                                    var randomIndex = rand.Next(0, cardsFromFilter.Count);
                                    var card = player.CreateCardAtLocation(cardsFromFilter[randomIndex].id, CardLocation.Board);
                                    cards.Add(card.uid);
                                }
                            }
                        }
                        else if (summonProperties.summonFrom == SummonMinionData.SummonFrom.Discarded_From_Last_Ability)
                        {
                            var player = GameServer.GetPlayerState(gameCard.owner);
                            var lastDiscard = GameServer.State.GetHistories(CommandType.DISCARD_CARD);

                            for (var i = 0; i < lastDiscard.Count; i++)
                            {
                                if (lastDiscard[i].SourceUid == gameCard.characterData.id)
                                {
                                    for (var j = 0; j < lastDiscard[i].TargetUids.Count; j++)
                                    {
                                        var discardedCard = GameServer.State.GetCard(lastDiscard[i].TargetUids[j]);
                                        if (discardedCard != null && discardedCard.characterData.type == CardType.Minion)
                                        {
                                            var copy = player.CreateCardAtLocation(discardedCard.data.id,
                                                CardLocation.Board);
                                            cards.Add(copy.uid);
                                        }
                                    }
                                }
                            }
                        }
                        else if (summonProperties.summonFrom == SummonMinionData.SummonFrom.SpecificCard)
                        {
                            for (var i = 0; i < summonProperties.count; i++)
                            {
                                var player = summonProperties.placement.HasFlag(SummonPlacement.PLAYER) ? GameServer.ClientPlayerState : GameServer.OpponentPlayerState;
                                var card = player.CreateCardAtLocation(summonProperties.cardId, CardLocation.Board);
                                cards.Add(card.uid);
                            }
                        }
                    }
                    break;
                }
                case AbilityKeyword.SUMMON_COPIES:
                {
                    var targets = global::GameLogic.GetTargets(gameCard, ability, parentCommand, previousCommand,true);

                    for (var i = 0; i < targets.Count; i++)
                    {
                        var summonMinionData = GameServer.State.GetCard(targets[i]);

                        // add the copy from, so we can animate this minion on board.
                        copyFromTargets.Add(targets[i]);

                        for (var j = 0; j < summonProperties.count; j++)
                        {
                            var player = summonProperties.placement.HasFlag(SummonPlacement.PLAYER) ? GameServer.ClientPlayerState : GameServer.OpponentPlayerState;
                            var copy = player.CreateCardFromCopy(summonMinionData, CardLocation.Board);
                            cards.Add(copy.uid);
                        }
                    }

                    break;
                }
                case AbilityKeyword.OVERGROW:
                {
                    if (!AI.Thinking)
                    {
                        var targets = new List<string> { gameCard.uid };

                        for (var i = 0; i < targets.Count; i++)
                        {
                            var player = summonProperties.placement.HasFlag(SummonPlacement.PLAYER) ? GameServer.ClientPlayerState : GameServer.OpponentPlayerState;
                            var copy = player.CreateCardFromCopy(GameServer.State.GetCard(targets[i]), CardLocation.Board);
                            copyFromTargets.Add(targets[i]);
                            cards.Add(copy.uid);
                        }
                    }

                    break;
                }
            }

            return cards;
        }

        public static List<Vector2Int> GetSummonPositionFromAbility(GameCardState card, Ability ability, SummonCommand command, string targetCardFromTriggerUid, int fixedCount = 0)
        {
            var summonProperties = ability.summonMinionData;
            var placement = summonProperties.placement;
            var positionsToGet = fixedCount > 0 ? fixedCount : (summonProperties.count<=0 ? 1 : summonProperties.count);

            List<TileState> possibleTiles;
            var positions = new List<Vector2Int>();

            if (ability.keyword == AbilityKeyword.OVERGROW)
            {
                if (AI.Thinking)
                {
                    positions.Clear();
                    return positions;
                }

                if (!BoardState.BoardPositionToSimpleMap.ContainsKey(card.position))
                {
                    Debug.LogError($"Card Position = {card.position} and is not present in the dictionary!");
                    positions.Clear();
                    return positions;
                }

                var pos0 = BoardState.BoardPositionToSimpleMap[card.position];
                var pos1 = pos0 + Vector2Int.right;

                if (BoardState.SimpleMapToBoardPosition.ContainsKey(pos1))
                {
                    var tile = GameServer.State.Board.GetTile(BoardState.SimpleMapToBoardPosition[pos1]);

                    if (tile != null && !tile.Occupied)
                    {
                        positions.Add(tile.Position);
                    }
                }
                else
                {
                    positions.Clear();
                }

                return positions;
            }

            if (placement.HasFlag(SummonPlacement.EXACT_POSITION))
            {
                var tile = GameServer.State.Board.GetTile(summonProperties.tilePosition);

                if (!tile.Occupied)
                {
                    positions.Add(tile.Position);
                    return positions;
                }
            }

            if (placement.HasFlag(SummonPlacement.FRONT))
            {
                if (positionsToGet <= 1 && card.characterData.type == CardType.Minion)
                {
                    var position = card.position + Vector2Int.down;
                    var tile = GameServer.State.Board.GetTile(position);

                    if (tile != null &&!tile.Occupied)
                    {
                        positions.Add(position);
                        return positions;
                    }
                }

                if (placement.HasFlag(SummonPlacement.STRICT))
                {
                    return positions;
                }

                var isPlayer = card.owner == GameServer.ClientPlayerState.Uid;
                var rowTiles = GameServer.State.Board.GetUnOccupiedTilesInRow(isPlayer ? 0 : 2);
                rowTiles = rowTiles.Randomize();

                for (var i = 0; i < positionsToGet; i++)
                {
                    if (i >= rowTiles.Count) break;
                    positions.Add(rowTiles[i].Position);
                }

                return positions;
            }

            if (placement.HasFlag(SummonPlacement.BACK))
            {
                if (positionsToGet <= 1 && card.characterData.type == CardType.Minion)
                {
                    var position = card.position + Vector2Int.up;
                    var tile = GameServer.State.Board.GetTile(position);

                    if (tile != null && !tile.Occupied)
                    {
                        positions.Add(position);
                        return positions;
                    }
                }

                if (placement.HasFlag(SummonPlacement.STRICT))
                {
                    return positions;
                }

                var isPlayer = card.owner == GameServer.ClientPlayerState.Uid;
                var rowTiles = GameServer.State.Board.GetUnOccupiedTilesInRow(isPlayer ? 1 : 3);
                rowTiles = rowTiles.Randomize();

                for (var i = 0; i < positionsToGet; i++)
                {
                    if (i >= rowTiles.Count)
                    {
                        break;
                    }

                    positions.Add(rowTiles[i].Position);
                }

                return positions;
            }

            if (placement.HasFlag(SummonPlacement.ROW))
            {
                var rowTiles = GameServer.State.Board.GetUnOccupiedTilesInRow(card.position.y);

                for (var i = 0; i < rowTiles.Count; i++)
                {
                    if (i >= rowTiles.Count) break;
                    positions.Add(rowTiles[i].Position);
                }

                return positions;
            }

            // in place only works for 1 count because its overriding the minion already there!
            if (placement.HasFlag(SummonPlacement.IN_PLACE) && summonProperties.count == 1)
            {
                TileState tile = null;

                if (card.data.type != CardType.Minion)
                {
                    if (!string.IsNullOrEmpty(targetCardFromTriggerUid))
                    {
                        var targetCard = GameServer.State.GetCard(targetCardFromTriggerUid);

                        if (targetCard != null)
                        {
                            tile = GameServer.State.Board.GetTile(targetCard.position);
                        }
                    }

                    if (tile == null)
                    {
                        return positions;
                    }
                }
                else
                {
                    tile = GameServer.State.Board.GetTile(card.position);
                }

                if (tile.Occupied && GameCardState.IsPendingNullOrDead( tile.Card ))
                {
                    positions.Add(tile.Position);
                    return positions;
                }

                if (!tile.Occupied)
                {
                    positions.Add(tile.Position);
                    return positions;
                }

                Debug.LogError($"Tile is occupied by card: {tile.Card.characterData.name} Location = {tile.Card.location} Health = {tile.Card.GetPendingHealth()}");

                    // the tile is occupied, we cant summon this "in place" lets look for another spot and change the placement.
                placement = SummonPlacement.PLAYER | SummonPlacement.LEFT_SIDE | SummonPlacement.RIGHT_SIDE;
            }

            if (placement.HasFlag(SummonPlacement.FRONT))
            {
                var positionInFront = new Vector2Int(card.position.x, card.position.y);

                if (card.Team == Team.Client)
                {
                    positionInFront.y -= 1;

                    if (positionInFront.y < 0) // failed
                    {
                        return positions;
                    }
                }
                else if (card.Team == Team.Opponent)
                {
                    positionInFront.y -= 1;

                    if (positionInFront.y < 2) // failed
                    {
                        return positions;
                    }
                }

                var tile = GameServer.State.Board.GetTile(positionInFront);

                if (tile.Occupied)
                {
                    // could not get a valid position, FRONT summoning always fails and does not try and get a new placement.
                    return positions;
                }

                positions.Add(positionInFront);

                return positions;
            }

            if (placement.HasFlag(SummonPlacement.BACK))
            {
                var positionInBack = new Vector2Int(card.position.x, card.position.y);

                Debug.Log($"Position: {positionInBack} Team: {card.Team}");

                if (card.Team == Team.Client)
                {
                    positionInBack.y += 1;

                    if (positionInBack.y > 1) // failed
                    {
                        return positions;
                    }
                }
                else if (card.Team == Team.Opponent)
                {
                    positionInBack.y += 1;

                    if (positionInBack.y > 3) // failed
                    {
                        return positions;
                    }

                    Debug.Log($"GOT POSITION! {positionInBack}");
                }

                var tile = GameServer.State.Board.GetTile(positionInBack);

                if (tile.Occupied)
                {
                    // could not get a valid position, BACK summoning always fails and does not try and get a new placement.
                    return positions;
                }

                positions.Add(positionInBack);

                return positions;
            }

            if (placement.HasFlag(SummonPlacement.RIGHT_SIDE) && placement.HasFlag(SummonPlacement.LEFT_SIDE))
            {
                if (card.position.x < 3 && positions.Count < positionsToGet)
                {
                    var positionRight = new Vector2Int(card.position.x + 1, card.position.y);
                    var tile = GameServer.State.Board.GetTile(positionRight);

                    if (!tile.Occupied)
                    {
                        positions.Add(positionRight);
                    }
                }

                if (card.position.x > 0 && positions.Count < positionsToGet)
                {
                    var positionLeft = new Vector2Int(card.position.x - 1, card.position.y);
                    var tile = GameServer.State.Board.GetTile(positionLeft);

                    if (!tile.Occupied)
                    {
                        positions.Add(positionLeft);
                    }
                }

                // we're trying to summon 1 minion, this is probably a deathrattle that is triggering without
                // destroying the minion first, so we need to try and find another placement for it.
                if (positionsToGet == 1 && positions.Count <= 0)
                {
                    placement = SummonPlacement.PLAYER;
                }
                else
                {
                    return positions;
                }
            }

            if (placement.HasFlag(SummonPlacement.RIGHT_SIDE))
            {
                if (card.position.x < 3)
                {
                    var positionRight = new Vector2Int(card.position.x + 1, card.position.y);
                    positions.Add(positionRight);
                }

                return positions;
            }

            if (placement.HasFlag(SummonPlacement.LEFT_SIDE))
            {
                if (card.position.x > 0)
                {
                    var positionLeft = new Vector2Int(card.position.x - 1, card.position.y);
                    positions.Add(positionLeft);
                }

                return positions;
            }

            if (placement.HasFlag(SummonPlacement.PLAYER))
            {
                possibleTiles = GameServer.State.Board.GetUnOccupiedTiles(card.Team);
            }
            else if (placement.HasFlag(SummonPlacement.OPPONENT))
            {
                var unoccupiedTeam = card.Team == Team.Opponent ? Team.Client : Team.Opponent;
                possibleTiles = GameServer.State.Board.GetUnOccupiedTiles(unoccupiedTeam);
            }
            else
            {
                possibleTiles = GameServer.State.Board.GetUnOccupiedTiles();
            }

            var possibleBoardSpaces = possibleTiles.ConvertAll(x => x.Position);

            if (possibleBoardSpaces.Count == 0)
            {
                return possibleBoardSpaces;
            }

            List<CardData> summonedMinions = null;
            //CardData summonedMinion = null;

            if (!string.IsNullOrEmpty(ability.summonMinionData.cardId))
            {
                summonedMinions = new List<CardData>();

                for (var i = 0; i < positionsToGet; i++)
                {
                    summonedMinions.Add(Db.CardDatabase.GetCard(ability.summonMinionData.cardId));
                }
            }
            else if (command != null && command.TargetUids != null && command.TargetUids.Count > 0)
            {
                summonedMinions = new List<CardData>();

                for (var i = 0; i < command.TargetUids.Count; i++)
                {
                    var target = GameServer.State.GetCard(command.TargetUids[i]);
                    if (target == null ) continue;
                    summonedMinions.Add( Db.CardDatabase.GetCard(target.data.id));
                }
            }

            if (summonedMinions != null && summonedMinions.Count > 0)
            {
                var meleePositions =new List<Vector2Int>(possibleBoardSpaces.
                    FindAll(p0 =>
                    {
                        var sp = BoardState.BoardPositionToSimpleMap[p0];
                        return sp.y == 2 || sp.y == 1;
                    }));

                var rangedPositions = new List<Vector2Int>(possibleBoardSpaces.
                    FindAll(p0 =>
                    {
                        var sp = BoardState.BoardPositionToSimpleMap[p0];
                        return sp.y == 3 || sp.y == 0;
                    }));

                var arcanePositions = new List<Vector2Int>(possibleBoardSpaces);
                var rand = new System.Random();

                for (var i = 0; i < summonedMinions.Count; i++)
                {
                    var summonedMinion = summonedMinions[i];
                    var fromList = arcanePositions;

                    switch (summonedMinion.attackType)
                    {
                        case AttackType.Melee: if (meleePositions.Count > 0)
                            {
                                fromList = meleePositions;
                            }
                            break;
                        case AttackType.Ranged: if (rangedPositions.Count > 0)
                            {
                                fromList = rangedPositions;
                            }
                            break;
                    }

                    var position = fromList[rand.Next(0, fromList.Count)];

                    arcanePositions.Remove(position);
                    rangedPositions.Remove(position);
                    meleePositions.Remove(position);

                    positions.Add(position);

                    if (arcanePositions.Count <= 0) break;
                }

                return positions;
            }


            // if we have enough positions or we have no more possible spaces to give, we return
            if (positions.Count >= positionsToGet || possibleBoardSpaces.Count <= 0)
            {
                return positions;
            }

            // setup a new "random" variable to use for board spaces
            var random = new System.Random();

            // loop again to get random positions from the board spaces list.
            for (var i = 0; i < positionsToGet; i++)
            {
                var randomPosition = possibleBoardSpaces[random.Next(0, possibleBoardSpaces.Count)];
                possibleBoardSpaces.Remove(randomPosition);
                positions.Add(randomPosition);

                // check to make sure we have spaces left or we have enough positions
                if (possibleBoardSpaces.Count <= 0 || positions.Count >= positionsToGet)
                {
                    return positions;
                }
            }
            // we didn't get enough positions, we just return this list
            return positions;
        }

        public override void OnPrepareCommand()
        {
        }

        public override void OnExecuteCommand()
        {
            var successful = GameServer.State.SummonMinion(this, out var summonedMinions);

            if (!successful)
            {
                Debug.LogError("Summon was not successful!");
                return;
            }

            var sourceCard = GameServer.State.GetCard(SourceUid);
            var sourceAbility = GetAbility();

            for (var i = 0; i < summonedMinions.Count; i++)
            {
                var card = GameServer.State.GetCard(summonedMinions[i]);

                if (sourceAbility != null && sourceAbility.summonMinionData != null)
                {
                    if (sourceAbility.summonMinionData.grantAbilities)
                    {
                        card.AddAbilities(sourceCard.data.GetGivenAbilities(), Guid);
                    }

                    if (sourceAbility.keyword == AbilityKeyword.RELENTLESS)
                    {
                        var relentlessAbility = card.GetAbility(AbilityKeyword.RELENTLESS);
                        card.ExhaustAbility(relentlessAbility);
                    }
                }

                if (card.data.type == CardType.Minion)
                {
                    global::GameLogic.ResolveTriggeredCards(card, card, TriggerType.MINION_SUMMONED,
                        new global::GameLogic.ResolveTriggeredCardsProperties { showLog = false });

                    if (SummonMethod == Method.PlayedFromHand)
                    {
                        global::GameLogic.ResolveTriggeredCards(card, card, TriggerType.MINION_PLAYED,
                            new global::GameLogic.ResolveTriggeredCardsProperties { showLog = false });
                    }
                }
                else if (card.data.type== CardType.Trick)
                {
                    global::GameLogic.ResolveTriggeredCards(card, card, TriggerType.TRAP_SET,
                        new global::GameLogic.ResolveTriggeredCardsProperties { showLog = false });
                }
            }

            if (SummonMethod == Method.PlayedFromHand)
            {
                for (var i = 0; i < summonedMinions.Count; i++)
                {
                    var card = GameServer.State.GetCard( summonedMinions[i] );

                    if (card.GetAbilities().Count > 0)
                    {
                        //var playerSelectedTarget = global::GameLogic.GetPlayerSelectedTarget(card, card.GetAbilities()[0], command, true);

                        if (card.ConditionalAbilitiesHaveBeenMet(card.uid))
                        {
                            card.SwapToConditionalAbilities();
                            Debug.Log("Swapped to conditionals!");
                        }
                    }

                    var battlecries = card.GetAbilities().FindAll(ability => ability.triggerType == TriggerType.BATTLECRY);

                    if (battlecries.Count > 0)
                    {
                        var battlecryCounter = GameServer.State.AuraUpdater.GetBattlecryCount(PlayerUid);

                        for (var j = 0; j < battlecryCounter; j++)
                        {
                            for (var k = 0; k < battlecries.Count; k++)
                            {
                                if (battlecries[k].triggerConditions.HasFlag(FilterCondition.ADVANCED_FILTERS) &&
                                    !global::GameLogic.CanBeTriggered(card,card,card,battlecries[k], true))
                                {
                                    continue;
                                }

                                var battlecryCommands = global::GameLogic.CreateCommandFromAbility(card, battlecries[k], this, this);
                                global::GameLogic.ExecuteCommand(battlecryCommands);
                                Command previousCommand = null;

                                if (battlecryCommands.Count > 0)
                                {
                                    previousCommand = battlecryCommands[0];
                                }

                                var linkedAbilities = card.GetAbilities().FindAll(ability => ability.triggerType == TriggerType.LINKED_ABILITY);
                                var linkedCommands = new List<Command>();

                                for (var l = 0; l < linkedAbilities.Count; l++)
                                {
                                    if (linkedAbilities[k].abilityCondition.condition == Condition.PREVIOUS_COMMAND_TARGETS &&
                                        linkedAbilities[k].abilityCondition.usePreviousLinkedCommandNotParent &&
                                        linkedCommands.Count > 0)
                                    {
                                        previousCommand = linkedCommands[0];
                                    }

                                    linkedCommands = global::GameLogic.CreateCommandFromAbility(card, linkedAbilities[l], this, previousCommand);

                                    global::GameLogic.ExecuteCommand(linkedCommands);
                                }
                            }
                        }
                    }

                    if (!AI.Thinking)
                    {
                        var overgrowth = card.GetAbilities()
                            .Find(ability => ability.keyword == AbilityKeyword.OVERGROW);

                        if (overgrowth != null)
                        {
                            global::GameLogic.ExecuteCommand(
                                global::GameLogic.CreateCommandFromAbility(card, overgrowth, this, this));
                        }
                    }

                    // var taunt = card.GetAbilities().Find(ability => ability.keyword == AbilityKeyword.TAUNT);
                    //
                    // if (taunt != null)
                    // {
                    //     global::GameLogic.ExecuteCommand(
                    //         global::GameLogic.CreateCommandFromAbility(card, taunt, this, this));
                    // }
                }
            }
        }

        private IEnumerator<float> SummonOpponentCardFromHand(string cardUid, Vector2Int position)
        {
            var handCardObject = GameplayScreen.Instance.opponentHandView.GetCard(cardUid);
            var tileObject = GameplayScreen.Instance.gameBoard.GetTile(position);
            var cardState = GameServer.State.GetCard(cardUid);

            if (tileObject.State.Occupied && tileObject.State.CardId != cardState.uid)
            {
                // cancel this action!!
                var occupyingCard = tileObject.State.Card;
                Debug.LogError($"Canceling summon of {cardState.data.name} because another card is there! {occupyingCard.data.name} {tileObject.State.Position}");
                GameServer.State.Board.RemoveCardFromTile(cardState.uid, CardLocation.Hand);
                AI.FailedSummonAttempt = true;
                yield break;
            }

            GameplayScreen.Instance.opponentHandView.Show();

            yield return Timing.WaitForSeconds(0.24f);

            Events.Publish(null, EventType.OnCardPlayed, new OnCardPlayedEventArgs { isClientCard = false, cardUid = cardState.uid });

            if (handCardObject == null)
            {
                yield return Timing.WaitUntilDone(AI.SummonBoardCard(tileObject, GameServer.State.GetCard(cardUid), SummonMethod));
            }
            else
            {
                // loading standard card and setting it to its back side
                var reference = AddressableReferenceLoader.GetAsset("Card_Standard", true);

                yield return Timing.WaitUntilDone(reference.AsCoroutine());

                var settings = new CreateCardFactorySettings
                {
                    CardPrefab = reference.Result,
                    Layout = GameplayScreen.Instance.handView.RectTransform(),
                    AdditionalComponents = CardView.GetRequiredComponentsForStandardHandCard(),
                    StartsDisabled = true,
                    SortingLayer = "Overlay"
                };

                settings.CardState = cardState;
                settings.SortingOrder = GameCardView.HandSortingOrder + 20;

                var standardCardView = CardFactory.Create(settings, null);
                var gameCardView = standardCardView.GetCardComponent<GameCardView>();

                standardCardView.SetInteractable(false);
                standardCardView.cardObject.SetActive(false);
                standardCardView.cardBackObject.SetActive(true);

                var dragRotator = standardCardView.GetCardComponent<DragRotator>();
                dragRotator.enabled = false;

                standardCardView.transform.SetParent(handCardObject.transform.parent, false);
                standardCardView.transform.localEulerAngles = Vector3.zero;
                standardCardView.RectTransform().anchoredPosition = Vector3.zero;
                standardCardView.SetActive(true);
                // end loading card

                handCardObject.SetActive(false);

                standardCardView.transform.DOMove(tileObject.transform.position, 0.4f)
                    .SetEase(Ease.OutBack);

                yield return Timing.WaitForSeconds(0.2f);

                GameplayScreen.Instance.opponentHandView.RemoveCard(cardUid, null, 3);

                yield return Timing.WaitForSeconds(0.2f);

                if (cardState.characterData.type != CardType.Trick)
                {
                    yield return Timing.WaitUntilDone(gameCardView.Flip());
                }
                else
                {
                    gameCardView.View.cardBackObject.SetActive(false);
                }

                yield return Timing.WaitUntilDone(gameCardView.AnimateSummon(tileObject, SummonCommand.Method.PlayedFromHand));
            }

            GameplayScreen.Instance.opponentHandView.Hide();
        }

        public override IEnumerator<float> Resolve()
        {
            switch (SummonMethod)
            {
                case Method.PlayedWithAbilityFromHand when !IsClientCommand():
                {
                    for (var i = 0; i < TargetUids.Count; i++)
                    {
                        yield return Timing.WaitUntilDone(SummonOpponentCardFromHand(TargetUids[i], Positions[i]));
                    }

                    break;
                }
                case Method.PlayedFromHand when !IsClientCommand():
                {
                    yield return Timing.WaitUntilDone(SummonOpponentCardFromHand(SourceUid, Position));
                    break;
                }
                // This card was summoned through another means.
                case Method.PlayedFromHand:
                {
                    var gameCard = GameplayScreen.Instance.GetHandCard(SourceUid);

                    if (gameCard == null)
                    {
                        yield break;
                    }

                    // Summoning is handled instantly to avoid delays with this system.
                    // All we do is wait for card to be set inactive so we can destroy it.
                    while (gameCard.GameState != GameCardView.GameStateType.None) yield return Timing.WaitForOneFrame;

                    // destroy the card after summoning it to avoid memory leak
                    Object.Destroy(gameCard.gameObject);
                    break;
                }
                case Method.PlayedWithAbilityFromHand:
                {
                    for (var i = 0; i < TargetUids.Count; i++)
                    {
                        var minion = TargetUids[i];

                        var card = GameplayScreen.Instance.GetHandCard(minion);
                        var tile = GameplayScreen.Instance.gameBoard.GetTile(card.State);

                        Debug.Log($"Tile = {tile.Position} Minion = {card.State.characterData.name}");

                        GameplayScreen.Instance.handView.RemoveCard(card,tile.cardContainer);
                        card.transform.DORotate(Vector3.zero, 0.1f);

                        yield return Timing.WaitUntilDone( card.transform.DOMove(tile.cardContainer.position, 0.6f)
                            .SetEase(Ease.OutBack).WaitForCompletion(true));

                        yield return Timing.WaitForSeconds(0.14f);

                        yield return Timing.WaitUntilDone(card.AnimateSummon(tile, Method.PlayedFromHand,-1 ));

                        var getBoardCardTask = GameBoard.GetBoardCard(card.State.uid);
                        yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                        var getBoardCardResult = getBoardCardTask.Result;

                        if (getBoardCardResult.error != ClientErrorCode.None)
                        {
                            Server.HandleClientError(getBoardCardResult, true);
                        }

                        if (getBoardCardResult.boardCard)
                        {
                            getBoardCardResult.boardCard.UpdateViewFromState();
                        }
                    }
                    break;
                }
                case Method.PlayedWithAbility:
                {
                    GameCardView boardCard = null;
                    var summons = new List<GameCardView>();

                    if (string.IsNullOrEmpty(SourceUid))
                    {
                        yield return Timing.WaitUntilDone(SummonBoardCards(this, null, summons).AsCoroutine());
                    }
                    else
                    {
                        var sourceCard = GameServer.State.GetCard(SourceUid);

                        Ability summonAbility;

                        if (AbilityIndex > -1)
                        {
                            summonAbility = sourceCard.GetAbilities()[AbilityIndex];
                        }
                        else
                        {
                            summonAbility = sourceCard.GetAbilities().Find(ability =>
                                ability.keyword == AbilityKeyword.SUMMON ||
                                ability.keyword == AbilityKeyword.SUMMON_COPIES ||
                                ability.keyword == AbilityKeyword.OVERGROW);
                        }

                        if (summonAbility != null && summonAbility.summonMinionData != null && summonAbility.summonMinionData.placement != SummonPlacement.IN_PLACE)
                        {
                            if (sourceCard.characterData.type == CardType.Minion && sourceCard.location == CardLocation.Board)
                            {
                                boardCard = GameBoard.GetBoardCardNotImportant(SourceUid);

                                if (boardCard)
                                {
                                    Timing.RunCoroutine(boardCard.AnimateEffect<GenericEffectComponent>(this));
                                }
                            }
                        }

                        if (summonAbility != null && summonAbility.usesCustomEffect && summonAbility.usesBoardEffect)
                        {
                            yield return Timing.WaitUntilDone(PlayCustomOrGenericEffectOnBoardCard(summonAbility, this, string.Empty));
                            yield return Timing.WaitUntilDone(SummonBoardCards(this, summonAbility, summons).AsCoroutine());
                        }
                        else if (summonAbility != null && (summonAbility.usesCustomEffect || sourceCard.data.usesCustomEffect))
                        {
                            if (boardCard != null) // THIS IS A MINION!
                            {
                                var vfxComponent = (GameVfxComponent) boardCard.GetComponent(System.Type.GetType(summonAbility.customEffectComponent));

                                if (vfxComponent != null)
                                {
                                    var vfxParams = new VfxParameters();
                                    //CUSTOM EFFECT NEEDS TO ADD SUMMONS TO THIS LIST!!
                                    vfxParams.summons = new List<GameCardView>();
                                    // the custom effect needs to handle ALL applies
                                    yield return Timing.WaitUntilDone(vfxComponent.Effect.Play(this, vfxParams));
                                    // add the summons to the summons list
                                    summons = new List<GameCardView>(vfxParams.summons);
                                }
                            }
                            else // THIS IS A SPELL!!
                            {
                                var vfxParams = new VfxParameters();
                                vfxParams.summons = new List<GameCardView>();
                                var spellTask = GameVfxProvider.GetVfxForSpell(sourceCard, null);
                                Debug.LogWarning("Spell has custom effect!!!");
                                yield return Timing.WaitUntilDone(spellTask.AsCoroutine());
                                yield return Timing.WaitUntilDone(spellTask.Result.Play(this, vfxParams));
                                // add the summons to the summons list
                                summons = new List<GameCardView>(vfxParams.summons);
                            }
                        }
                        else // summon generically !
                        {
                            // summon all board cards async
                            yield return Timing.WaitUntilDone(SummonBoardCards(this, summonAbility, summons).AsCoroutine());
                        }

                        if (summons.Count > 0)
                        {
                            var lastSummon = summons[summons.Count - 1];
                            while (lastSummon.GameState != GameCardView.GameStateType.None)
                                yield return Timing.WaitForOneFrame;
                        }
                    }
                    break;
                }
                case Method.PlayedFromDeck when !IsClientCommand():
                {
                    Debug.LogWarning("Summoning from Opponent deck is not supported yet.");
                    break;
                }
                case Method.PlayedFromDeck:
                {
                    Debug.LogWarning("Summoning from deck is not supported yet.");
                    break;
                }
            }
        }

        public static async Task SummonBoardCards(SummonCommand command, Ability summonAbility, List<GameCardView> summons)
        {
            var summonVisualEffect = SummonMinionData.SummonVisualEffect.FromAbility;

            if (summonAbility?.summonMinionData != null)
            {
                summonVisualEffect = summonAbility.summonMinionData.summonVisualEffect;

                switch (summonAbility.keyword)
                {
                    case AbilityKeyword.SUMMON_COPIES:
                        for (var i = 0; i < command.CopyFromTargets.Count; i++)
                        {
                            var getBoardCardResult = await GameBoard.GetBoardCard(command.CopyFromTargets[i]);

                            if (getBoardCardResult.error != ClientErrorCode.None)
                            {
                                Server.HandleClientError(getBoardCardResult, false);
                            }

                            if (!getBoardCardResult.boardCard) continue;

                            var color = "#a2162b".ToColor();
                            getBoardCardResult.boardCard.View.TintThenUntintOverTime(color, BlendMode.VividLight, 0.25f, 0.5f, 0.25f);
                        }
                        break;
                }
            }

            var boardCardPrefab = await AddressableReferenceLoader.GetAsset("Card_Board", true);

            var settings = new CreateCardFactorySettings
            {
                CardPrefab = boardCardPrefab,
                StartsDisabled = true
            };

            for (var i = 0; i < command.Positions.Count; i++)
            {
                var position = command.Positions[i];
                var cardState = GameServer.State.GetCard( command.TargetUids[i] );
                var tile = GameplayScreen.Instance.GetBoardTile(position);

                settings.CardState = cardState;
                settings.Layout = tile.RectTransform();
                settings.AdditionalComponents = GameVfxComponent.GetRequiredVfxComponents(cardState);

                switch (summonVisualEffect)
                {
                    case SummonMinionData.SummonVisualEffect.Blood:
                        settings.AdditionalComponents.Add(typeof(BloodSummonEffectComponent));
                        break;
                    case SummonMinionData.SummonVisualEffect.Soul:
                        settings.AdditionalComponents.Add(typeof(SoulSummonEffectComponent));
                        break;
                    case SummonMinionData.SummonVisualEffect.Overgrow:
                        settings.AdditionalComponents.Add(typeof(OvergrowSummonEffectComponent));
                        break;
                    case SummonMinionData.SummonVisualEffect.BrigadeTrap:
                        settings.AdditionalComponents.Add(typeof(BrigadeTrapEffectComponent));
                        break;
                    case SummonMinionData.SummonVisualEffect.Slime:
                        settings.AdditionalComponents.Add(typeof(GreenSummonEffectComponent));
                        break;
                }

                if (command.SummonAllAtOnce)
                {
                    SummonBoardCard(command, summons, settings, tile, cardState, summonVisualEffect, position);
                }
                else
                {
                    await SummonBoardCard(command, summons, settings, tile, cardState, summonVisualEffect, position);
                }
            }
        }

        private static async Task SummonBoardCard(SummonCommand command, List<GameCardView> summons, CreateCardFactorySettings settings,
            GameTile tile, GameCardState cardState, SummonMinionData.SummonVisualEffect summonVisualEffect, Vector2Int position)
        {
            var boardCard = CardFactory.Create(settings, null);
            var boardCardGameView = boardCard.GetCardComponent<GameCardView>();

            while (!boardCard.AllVfxComponentsLoaded()) await new WaitForEndOfFrame();

            boardCard.GetVfx<AttackEffectComponent, AttackEffect>().SetupDirection(boardCardGameView.State.Team);
            boardCard.SetActive(true);
            Timing.RunCoroutine(
                boardCardGameView.AnimateSummonOnTile(tile, cardState, summonVisualEffect, command.SummonMethod));
            GameplayScreen.Instance.SetBoardTile(boardCardGameView, position);
            summons.Add(boardCardGameView);
        }
    }
}