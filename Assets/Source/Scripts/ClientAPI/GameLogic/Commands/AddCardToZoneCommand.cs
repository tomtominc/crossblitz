using System;
using System.Collections.Generic;
using Asyncoroutine;
using BlendModes;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Data;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Databases;
using CrossBlitz.GameVfx;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using DG.Tweening;
using MEC;
using UnityEngine;
using Class = CrossBlitz.Card.Class;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;
using Object = UnityEngine.Object;

namespace CrossBlitz.ClientAPI.Commands
{
    public class AddCardToZoneCommand : Command
    {
        public CardLocation locationToAddTo;
        public int MoveToCommandIndex = -1;

        public AddCardToZoneCommand() => Type = CommandType.ADD_CARD_TO_ZONE;

        public override string GetLabelDisplay()
        {
            var history = base.GetLabelDisplay();

            history += "Added ";

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var card = GameServer.State.GetCard(TargetUids[i]);

                if (card != null && card.characterData != null)
                {
                    history += $"{card.characterData.name}, ";
                }
            }

            history += $"to {locationToAddTo}.";

            return history;
        }

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            var score = 0;

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var targetCard = GameServer.State.GetCard(TargetUids[i]);

                switch (targetCard.location)
                {
                    case CardLocation.Board:
                    case CardLocation.Deck:
                    case CardLocation.Hand:
                        score++;
                        break;
                    case CardLocation.Graveyard:
                        score--;
                        break;
                    case CardLocation.Null:
                        break;
                }
            }

            return score;
        }

        public List<string> CreateCardsFromFilter(CardFilter filter, Faction faction, int count, Ability ability, Command parentCommand, Command previousCommand, string sourceCardFromTriggerUid,string targetCardFromTriggerUid)
        {
            // can't create 0 cards, this was just not set properly!
            if (count == 0)
            {
                count = 1;
            }

            if (filter.drawCountBasedOnConditions)
            {
                var useAbilityConditionInstead = false;

                switch (filter.drawCountCondition)
                {
                    case Condition.PREVIOUS_COMMAND_TARGETS:
                        var cardsInGraveyard = GameServer.State.GetCardsInLocation(CardLocation.Graveyard);
                        var cardsDamagedBySource =
                            cardsInGraveyard.FindAll(c => c.pendingHistory.Exists(h => h.AppliedBy == SourceUid));
                        count = cardsDamagedBySource.Count;
                        break;
                    case Condition.SPELL_DAMAGE:
                        count += GameServer.State.AuraUpdater.GetSpellDamageForSchool(PlayerUid, filter.spellSchool);
                        break;
                    default:
                        useAbilityConditionInstead = true;
                        count = 0;
                        break;
                }

                if (useAbilityConditionInstead && ability != null)
                {
                    switch (ability.abilityCondition.condition)
                    {
                        case Condition.NUMBER_OF_SPECIFIC_CARDS_IN_DECK:
                            if (ability.abilityCondition.filters.HasFlag(FilterCondition.PLAYER))
                            {
                                count = GameServer.GetPlayerState(PlayerUid)
                                    .GetNumberOfCardsWithIdInDeck(ability.abilityCondition.cardId);
                            }
                            else if (ability.abilityCondition.filters.HasFlag(FilterCondition.OPPONENT))
                            {
                                count = GameServer.GetPlayerState(GetTeam() == Team.Client ? Team.Opponent : Team.Client)
                                    .GetNumberOfCardsWithIdInDeck(ability.abilityCondition.cardId);
                            }
                            break;
                    }
                }

                if (count <= 0)
                {
                    return new List<string>();
                }
            }

            var selected = new List<CardData>();
            var selectedGameCards = new List<GameCardState>();
            var selectedUids = new List<string>();
            GameCardState playerSelectedCard = null;
            var random = new System.Random();
            var source = GameServer.State.GetCard(SourceUid);

            if (filter.copyFromLocation.HasFlag(FilterCondition.SELF))
            {
                selectedGameCards.Add( GameServer.State.GetCard(SourceUid) );
            }

            // if its a player selected card, that means there's only 1 and we're trying to add copies to hand or deck.
            if (filter.copyFromLocation.HasFlag(FilterCondition.PLAYER_SELECTED))
            {
                var playerSelectedTarget =
                    global::GameLogic.GetPlayerSelectedTarget(source, ability, parentCommand, true);

                if (!string.IsNullOrEmpty(playerSelectedTarget))
                {
                    playerSelectedCard = GameServer.State.GetCard(playerSelectedTarget);
                }
                else
                {
                    //playerSelectedCard = GameServer.State.GetRandomCard(CardLocation.Board);
                    Debug.LogError("Could not find a selection -- should get random card!");
                }
            }

            for (var i = 0; i < count; i++)
            {
                if (filter.copyFromLocation.HasFlag(FilterCondition.SELF))
                {
                    break;
                }

                var cards = new List<CardData>();
                var gameCards = new List<GameCardState>();

                if (filter.grantACopyFromLocation)
                {
                    PlayerBattleState player = null;

                    if (filter.copyFromLocation.HasFlag(FilterCondition.PLAYER_SELECTED))
                    {
                        if (playerSelectedCard != null)
                        {
                            var copyOfCard = GameCardState.AsCopy(playerSelectedCard);
                            selectedGameCards.Add(copyOfCard);
                        }
                        else
                        {
                            var copyOfCard = GameCardState.AsCopy(source);
                            selectedGameCards.Add(copyOfCard);
                        }

                        //Debug.Log($"Current Game Card Total: {selectedGameCards.Count}");
                        continue;
                    }

                    if (filter.copyFromLocation.HasFlag(FilterCondition.LAST))
                    {
                        if (filter.copyFromLocation.HasFlag(FilterCondition.GRAVEYARD))
                        {
                            var lastDeath = GameServer.State.GetCardsInLocation(CardLocation.Graveyard);

                            if (lastDeath.Count > 0)
                            {
                                var copyOfCard = GameCardState.AsCopy(lastDeath[lastDeath.Count-1]);
                                selectedGameCards.Add(copyOfCard);
                            }
                        }
                        else
                        {
                            var lastMinionsSummoned = GameServer.State.GetLastSummonMinions();

                            for (var j = 0; j < lastMinionsSummoned.Count; j++)
                            {
                                var minion = GameServer.State.GetCard(lastMinionsSummoned[j]);

                                if (filter.copyFromLocation.HasFlag(FilterCondition.PLAYER) &&
                                    minion.Team == source.Team)
                                {
                                    var copyOfCard = GameCardState.AsCopy(minion);
                                    selectedGameCards.Add(copyOfCard);
                                }

                                if (filter.copyFromLocation.HasFlag(FilterCondition.OPPONENT) &&
                                    minion.Team != source.Team)
                                {
                                    var copyOfCard = GameCardState.AsCopy(minion);
                                    selectedGameCards.Add(copyOfCard);
                                }
                            }
                        }

                        continue;
                    }

                    if (filter.copyFromLocation.HasFlag(FilterCondition.PLAYER))
                    {
                        player = GameServer.GetPlayerState(PlayerUid);
                    }
                    else if (filter.copyFromLocation.HasFlag(FilterCondition.OPPONENT))
                    {
                        player = GameServer.GetPlayerState(PlayerUid == GameServer.ClientPlayerState.Uid
                            ? GameServer.OpponentPlayerState.Uid
                            : GameServer.ClientPlayerState.Uid);
                    }
                    else
                    {
                        Debug.LogError($"copyFromLocation: {filter.copyFromLocation} does not contain a player type!");
                        continue;
                    }

                    if (filter.copyFromLocation.HasFlag(FilterCondition.DECK))
                    {
                        if (player.deck.Count <= 0)
                        {
                            continue;
                        }

                        var deck = new List<string>(player.deck);
                        var sourceCard = GameServer.State.GetCard(SourceUid);

                        deck.RemoveAll(c =>
                        {
                            var card = GameServer.State.GetCard(c);
                            return card.IsHero || card.data.type == CardType.Relic || card.data.type == CardType.Power || sourceCard.characterData.name == card.characterData.name;
                        });

                        if (deck.Count > 0)
                        {
                            var randIndex = random.Next(0, deck.Count - 1);
                            var randCard = GameServer.State.GetCard(deck[randIndex]);
                            var copyOfCard = GameCardState.AsCopy(randCard);
                            gameCards.Add(copyOfCard);
                        }
                    }
                    else if (filter.copyFromLocation.HasFlag(FilterCondition.HAND))
                    {
                        if (player.hand.Count <= 0)
                        {
                            continue;
                        }

                        var hand = new List<string>(player.hand);
                        var sourceCard = GameServer.State.GetCard(SourceUid);

                        hand.RemoveAll(c =>
                        {
                            var card = GameServer.State.GetCard(c);
                            return card.IsHero || card.data.type == CardType.Relic || card.data.type == CardType.Power || sourceCard.characterData.name == card.characterData.name;
                        });

                        if (hand.Count > 0)
                        {
                            var randIndex = random.Next(0, hand.Count - 1);
                            var randCard = GameServer.State.GetCard(hand[randIndex]);
                            var copyOfCard = GameCardState.AsCopy(randCard);
                            gameCards.Add(copyOfCard);
                        }
                    }
                    else if (filter.copyFromLocation.HasFlag(FilterCondition.GRAVEYARD))
                    {
                        var cardsInGraveyard = GameServer.State.GetCardsInLocation(CardLocation.Graveyard);

                        if (filter.copyFromLocation.HasFlag(FilterCondition.PLAYER) &&
                            filter.copyFromLocation.HasFlag(FilterCondition.OPPONENT))
                        {
                            // do no filtering, its coming from both locations.
                        }
                        else if (filter.copyFromLocation.HasFlag(FilterCondition.PLAYER))
                        {
                            cardsInGraveyard.RemoveAll(gc => gc.owner != player.Uid);
                        }
                        else if (filter.copyFromLocation.HasFlag(FilterCondition.OPPONENT))
                        {
                            cardsInGraveyard.RemoveAll(gc => gc.owner == player.Uid);
                        }

                        // remove relics and heroes
                        cardsInGraveyard.RemoveAll(card => card.IsHero || card.data.type == CardType.Relic || card.data.type == CardType.Power);

                        if (cardsInGraveyard.Count > 0)
                        {
                            var randIndex = random.Next(0, cardsInGraveyard.Count - 1);
                            var copyOfCard = GameCardState.AsCopy(cardsInGraveyard[randIndex], true);
                            gameCards.Add(copyOfCard);
                        }
                    }
                    else
                    {
                        Debug.LogError(
                            $"copyFromLocation: {filter.copyFromLocation} does not contain a valid location!");
                        continue;
                    }

                    if (filter.fromClass != Class.None && filter.fromClass != Class.All)
                    {
                        gameCards.RemoveAll(gc => gc.data.@class != filter.fromClass);
                    }

                    if (filter.fromType != CardType.None)
                    {
                        gameCards.RemoveAll(gc => gc.data.type != filter.fromType);
                    }
                }
                else if (filter.randomCardIds != null && filter.randomCardIds.Count > 0)
                {
                    if (filter.addAllRandomCards)
                    {
                        for (var j = 0; j < filter.randomCardIds.Count; j++)
                        {
                            cards.Add(Db.CardDatabase.GetCard(filter.randomCardIds[j]));
                        }
                    }
                    else
                    {
                        cards.Add(Db.CardDatabase.GetCard(filter.randomCardIds[new System.Random().Next(filter.randomCardIds.Count)]));
                    }
                }
                else if (filter.lastRedeemedCard)
                {
                    cards = CardFilter.GetCardsWithFilter(filter);
                }
                else if (!string.IsNullOrEmpty(filter.cardId))
                {
                    cards.Add(Db.CardDatabase.GetCard(filter.cardId));
                }
                else if (filter.copiesFromAbilityTargets)
                {
                    var targets = global::GameLogic.GetTargets(source, ability, parentCommand, previousCommand, false,
                        sourceCardFromTriggerUid:sourceCardFromTriggerUid, targetCardFromTriggerUid:targetCardFromTriggerUid);

                    for (var j = 0; j < targets.Count; j++)
                    {
                        var gameCardState = GameServer.State.GetCard(targets[i]);
                        gameCards.Add(gameCardState);
                    }
                }
                else
                {
                    var randomCards = new List<CardData>(Db.CardDatabase.Cards);
                    randomCards.RemoveAll(x =>
                        x.type == CardType.Relic || x.type == CardType.Power || x.type == CardType.Elder_Relic ||
                        x.type == CardType.Hero || x.type == CardType.Character || x.type == CardType.None ||
                        x.type == CardType.Resource || x.set == CardSet.All || x.set == CardSet.Deprecated || x.set == CardSet.Token);

                    if (filter.fromClass != Class.None)
                    {
                        var filteredFaction = filter.limitToSameFaction ? faction : Faction.All;
                        randomCards=Db.CardDatabase.GetCardsByClass(randomCards,filter.fromClass, filteredFaction);
                    }

                    if (filter.fromType != CardType.None)
                    {
                        var filteredFaction = filter.limitToSameFaction ? faction : Faction.All;
                        randomCards=Db.CardDatabase.GetCardsByType(randomCards,filter.fromType, filteredFaction);
                    }

                    if (filter.withStat != FilterConditionValues.Stat.None)
                    {
                        var filteredFaction = filter.limitToSameFaction ? faction : Faction.All;
                        randomCards=Db.CardDatabase.GetCardsByStatFormula(randomCards,filter.withStat,filter.formula, filter.statAmount, filteredFaction);
                    }

                    if (filter.fromAnotherFaction)
                    {
                        randomCards.RemoveAll(x => x.faction == faction);
                    }

                    cards = new List<CardData>(randomCards);
                }

                if (cards.Count > 0)
                {
                    selected.Add(cards[random.Next(0, cards.Count)]);
                }

                if (gameCards.Count > 0)
                {
                    var randomCard = gameCards[random.Next(0, gameCards.Count)];
                    selectedGameCards.Add(randomCard);
                }
            }

            var playerUid = PlayerUid;

            if (filter.grantedToOpponent)
            {
                if (playerUid == GameServer.ClientPlayerState.Uid)
                {
                    playerUid = GameServer.OpponentPlayerState.Uid;
                }
                else
                {
                    playerUid = GameServer.ClientPlayerState.Uid;
                }
            }

            for (var i = 0; i < selected.Count; i++)
            {
                var player = GameServer.GetPlayerState(playerUid);
                var location = locationToAddTo;

                if (location == CardLocation.Hand && player.GetHandCount() >= PlayerBattleState.MaxHandSize)
                {
                    location = CardLocation.Graveyard;
                }

                var card = player.CreateCardAtLocation(selected[i].id, location, shuffle: true, startLocation: CardLocation.Null);

                if (filter.overrideCopiedCardStats)
                {
                    card.OverrideStats(filter.costOverride, filter.powerOverride, filter.healthOverride);
                }

                selectedUids.Add(card.uid);
            }

            for (var i = 0; i < selectedGameCards.Count; i++)
            {
                var player = GameServer.GetPlayerState(playerUid);
                var location = locationToAddTo;

                if (location == CardLocation.Hand && player.GetHandCount() >= PlayerBattleState.MaxHandSize)
                {
                    location = CardLocation.Graveyard;
                }

                var card = GameServer.State.GetCard(selectedGameCards[i].uid);
                card.owner = playerUid;
                card.SetCardLocation(location, !AI.Thinking);

                switch (location)
                {
                    case CardLocation.Deck:
                        player.AddCardToDeck(card.uid, true);
                        break;
                    case CardLocation.Hand:
                        player.AddCardToHand(card.uid, false);
                        break;
                }

                if (filter.overrideCopiedCardStats)
                {
                    card.OverrideStats(filter.costOverride, filter.powerOverride, filter.healthOverride);
                }

                selectedUids.Add(card.uid);
            }

            return selectedUids;
        }

        public override void OnExecuteCommand()
        {
            // no execution since the cards were created when creating this command
            //global::GameLogic.MoveCommand(this, MoveToCommandIndex);
        }

        public override IEnumerator<float> Resolve()
        {
            GameCardView boardCard = null;
            var sourceCard = GameServer.State.GetCard(SourceUid);
            var ability = GetAbility();

            if (ability != null && ability.usesCustomEffect && ability.usesBoardEffect)
            {
                yield return Timing.WaitUntilDone(PlayCustomOrGenericEffectOnBoardCard(ability, this, string.Empty));
                yield break;
            }

            if (sourceCard.characterData.type == CardType.Minion && sourceCard.location == CardLocation.Board)
            {
                boardCard = GameBoard.GetBoardCardNotImportant(SourceUid);

                if (boardCard != null)
                {
                    GameplayScreen.Instance.ShakeScreen();
                    Timing.RunCoroutine(boardCard.AnimateEffect<BattlecryEffectComponent>(this));
                    yield return Timing.WaitForSeconds(0.24f);
                }
            }

            var routines = new List<CoroutineHandle>();

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var cardUid = TargetUids[i];
                routines.Add(Timing.RunCoroutine(AnimateToZone(cardUid, boardCard)));
                yield return Timing.WaitForSeconds(2);
            }

            while (routines.Exists(r => r.IsRunning))
            {
                yield return Timing.WaitForOneFrame;
            }
        }

        private IEnumerator<float> AnimateToZone(string cardUid, GameCardView boardCard)
        {
            if (string.IsNullOrEmpty(cardUid))
            {
                yield break;
            }

            var cardState = GameServer.State.GetCard(cardUid);

            if (cardState == null)
            {
                yield break;
            }

            var location = cardState.location;
            var reference = AddressableReferenceLoader.GetAsset("Card_Zoomed", true);

            yield return Timing.WaitUntilDone(reference.AsCoroutine());

            if (location == CardLocation.Graveyard)
            {
                yield return Timing.WaitUntilDone(DrawCardCommand.Overdraw(reference.Result, cardUid, null));
                yield break; // yield break;
            }

            var settings = new CreateCardFactorySettings
            {
                CardPrefab = reference.Result,
                Layout = GameplayScreen.Instance.gameBoard.RectTransform(),
                AdditionalComponents = new List<Type>
                    { typeof(GameCardView), typeof(CardTransformComponent), typeof(CardScrappedComponent) },
                StartsDisabled = true
            };

            settings.CardState = cardState;
            settings.SortingOrder = GameCardView.HandSortingOrder + 20;

            var zoomedCardView = CardFactory.Create(settings, null);

            zoomedCardView.SetInteractable(false);

            var screenSize = new Vector2(GameplayScreen.Instance.RectTransform().rect.width,
                GameplayScreen.Instance.RectTransform().rect.height);

            zoomedCardView.SetActive(true);
            zoomedCardView.Rect.localScale = Vector3.zero;
            zoomedCardView.Tint(Color.white, BlendMode.Screen);

            if (boardCard)
            {
                zoomedCardView.Rect.position = boardCard.transform.position;
            }
            else
            {
                zoomedCardView.Rect.anchoredPosition = Vector2.zero;
            }

            zoomedCardView.Rect.DOAnchorPos(Vector2.zero, 0.5f).SetEase(Ease.OutBack);
            zoomedCardView.Rect.DOScale(Vector3.one, 0.4f).SetEase(Ease.OutBack);

            yield return Timing.WaitForSeconds(0.24f);

            zoomedCardView.RemoveTintOverTime(0.24f);

            yield return Timing.WaitForSeconds(1.24f);

            var transformEffect = zoomedCardView.GetVfx<CardTransformComponent, CardTransformEffect>();

            reference = AddressableReferenceLoader.GetAsset("Card_Standard", true);

            yield return Timing.WaitUntilDone(reference.AsCoroutine());

            settings = new CreateCardFactorySettings
            {
                CardPrefab = reference.Result,
                Layout = GameplayScreen.Instance.handView.RectTransform(),
                AdditionalComponents = CardView.GetRequiredComponentsForStandardHandCard(),
                StartsDisabled = true
            };

            settings.CardState = cardState;
            settings.SortingOrder = GameCardView.HandSortingOrder + 20;

            var standardCardView = CardFactory.Create(settings, null);
            var gameCardView = standardCardView.GetCardComponent<GameCardView>();


            yield return Timing.WaitUntilDone(transformEffect.TransformFromZoomedCard(gameCardView));

            switch (location)
            {
                case CardLocation.Hand:
                    if (cardState.Team == Team.Client)
                    {
                        GameplayScreen.Instance.handView.TrackCard(gameCardView);
                        yield return Timing.WaitForSeconds(0.5f);
                        standardCardView.SetInteractable(true);
                    }
                    else
                    {
                        GameplayScreen.Instance.opponentHandView.TrackCard(gameCardView);
                        yield return Timing.WaitForSeconds(0.5f);
                    }

                    break;
                case CardLocation.Deck:
                    yield return Timing.WaitUntilDone(AnimateStandardCardToDeck(gameCardView, GetTeam()));
                    break;
                default:
                    Debug.LogError($"Can't add to {location}!!! This is not allowed!!");
                    break;
            }

            yield return Timing.WaitForSeconds(0.5f);

            Object.Destroy(zoomedCardView.gameObject);
        }

        public static IEnumerator<float> AnimateStandardCardToDeck(GameCardView gameCardView, Team addedBy)
        {
            var deck = GameplayScreen.Instance.GetDeck(gameCardView.State.Team);
            deck.MoveOnScreen(0.24f);
            yield return Timing.WaitForSeconds(0.24f);
            Timing.WaitUntilDone(gameCardView.Flip(true));
            var tc = deck.GetTopCard();
            gameCardView.RectTransform().SetParent(tc.transform, true);
            //gameCardView.View.RemoveCanvas();
            gameCardView.View.GetCardComponent<DragRotator>().Reset();
            gameCardView.View.GetCardComponent<DragRotator>().enabled = false;

            var speed = 4f;
            var point = new List<Vector2> { gameCardView.AnchoredPosition, Vector2.zero, Vector2.zero };
            point[1] = (point[0] + (point[2] - point[0]) / 2) + Vector2.up * 320f;
            var time = 0f;
            var lastPosition = gameCardView.RectTransform().anchoredPosition;

            while (time < 1f)
            {
                var m1 = Vector2.Lerp(point[0], point[1], time);
                var m2 = Vector2.Lerp(point[1], point[2], time);
                gameCardView.RectTransform().anchoredPosition = Vector2.Lerp(m1, m2, time);

                var diff = (lastPosition - gameCardView.RectTransform().anchoredPosition).normalized;
                var angle = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;

                gameCardView.RectTransform().eulerAngles = new Vector3(0, 0, angle > 0 ? angle + 90 : 0);

                time += Time.deltaTime * speed;
                lastPosition = gameCardView.RectTransform().anchoredPosition;

                yield return Timing.WaitForOneFrame;
            }

            gameCardView.View.RectTransform().anchoredPosition = Vector2.zero;
            gameCardView.View.RectTransform().eulerAngles = new Vector3(0, 0, 180);

            GameplayScreen.Instance.ShakeScreen();
            deck.PunchShake();

            yield return Timing.WaitForOneFrame;

            Events.Publish(null, EventType.OnCardAddedToDeck, new OnCardAddedToDeckEventArgs
            {
                teamOfDeck = gameCardView.State.Team,
                addedByTeam = addedBy,
                cardUid = gameCardView.State.uid
            });

            Object.Destroy(gameCardView.View.gameObject, 0.1f);

            yield return Timing.WaitForSeconds(0.5f);
            deck.MoveOutOfScreen(0.5f);
            deck.RefreshDeckList();
        }
    }
}