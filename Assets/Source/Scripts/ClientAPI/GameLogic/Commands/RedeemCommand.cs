using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    public class RedeemCommand : Command
    {
        public string RedeemedCard;
        public string RedeemCardUid;
        public List<string> Cards;

        public RedeemCommand() => Type = CommandType.REDEEM;

        public override string GetLabelDisplay()
        {
            return base.GetLabelDisplay() + "Redeeming Cards.";
        }

        public override void OnPrepareCommand()
        {
        }

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            var player = GameServer.GetPlayerState(PlayerUid);
            if (player.GetHandCount() >= 7) return -1;

            return 2;
        }

        public List<string> GetRedeemCards(GameCardState redeemCard, CardFilter filter)
        {
            var cards = new List<string>();

            if (IsAiPlayer() &&
                redeemCard.data.aiProperties.cardPool.Count > 0) // commands don't enter here if they aren't bots
            {
                cards = new List<string>(redeemCard.data.aiProperties.cardPool);
                return cards;
            }

            var player = GameServer.GetPlayerState(PlayerUid);
            var opponent = GameServer.GetPlayerState(player.GetTeam() == Team.Client ? Team.Opponent : Team.Client);
            var random = new System.Random(System.Guid.NewGuid().GetHashCode());

            if (filter.grantACopyFromLocation)
            {
                if (filter.copyFromLocation.HasFlag(FilterCondition.PLAYER))
                {
                    player = GameServer.GetPlayerState(PlayerUid);
                }
                else if (filter.copyFromLocation.HasFlag(FilterCondition.OPPONENT))
                {
                    player = GameServer.GetPlayerState(PlayerUid == GameServer.ClientPlayerState.Uid
                        ? GameServer.OpponentPlayerState.Uid
                        : GameServer.ClientPlayerState.Uid);
                }
                else
                {
                    Debug.LogError($"copyFromLocation: {filter.copyFromLocation} does not contain a player type!");
                    return cards;
                }

                if (filter.copyFromLocation.HasFlag(FilterCondition.DECK))
                {
                    if (player.deck.Count <= 0)
                    {
                        return cards;
                    }

                    var deck = new List<string>(player.deck);

                    for (var i = deck.Count-1; i > -1; i--)
                    {
                        var card = GameServer.State.GetCard(deck[i]);

                        if (card.IsHero || card.characterData.type == CardType.Relic ||
                            card.characterData.type == CardType.Elder_Relic || card.characterData.type == CardType.Power)
                        {
                            deck.RemoveAt(i);
                            continue;
                        }

                        if (card.characterData.type == CardType.Spell && card.data.GetAbilities().Count <= 0)
                        {
                            deck.RemoveAt(i);
                            continue;
                        }

                        if (filter.fromType != CardType.None && card.characterData.type != filter.fromType)
                        {
                            deck.RemoveAt(i);
                            continue;
                        }
                    }

                    if (deck.Count > 0)
                    {
                        deck.Shuffle(random);

                        for (var i = 0; i < Mathf.Clamp(3, 0, deck.Count); i++)
                        {
                            var randCard = GameServer.State.GetCard(deck[i]);
                            cards.Add(randCard.data.id);
                        }
                    }
                }
                else if (filter.copyFromLocation.HasFlag(FilterCondition.HAND))
                {
                    if (player.hand.Count <= 0)
                    {
                        return cards;
                    }

                    var hand = new List<string>(player.hand);

                    hand.RemoveAll(c =>
                    {
                        var card = GameServer.State.GetCard(c);
                        return card.IsHero || card.data.type == CardType.Relic || card.data.type == CardType.Power;
                    });

                    if (hand.Count > 0)
                    {
                        hand.Shuffle(random);

                        for (var i = 0; i < Mathf.Clamp(3, 0, hand.Count); i++)
                        {
                            var randCard = GameServer.State.GetCard(hand[i]);
                            cards.Add(randCard.data.id);
                        }
                    }
                }
                else if (filter.copyFromLocation.HasFlag(FilterCondition.GRAVEYARD))
                {
                    var cardsInGraveyard = GameServer.State.GetCardsInLocation(CardLocation.Graveyard);

                    if (filter.copyFromLocation.HasFlag(FilterCondition.PLAYER) && filter.copyFromLocation.HasFlag(FilterCondition.OPPONENT))
                    {
                        // do no filtering, its coming from both locations.
                    }
                    else if (filter.copyFromLocation.HasFlag(FilterCondition.PLAYER))
                    {
                        cardsInGraveyard.RemoveAll(gc => gc.owner != player.Uid);
                    }
                    else if (filter.copyFromLocation.HasFlag(FilterCondition.OPPONENT))
                    {
                        cardsInGraveyard.RemoveAll(gc => gc.owner == player.Uid);
                    }

                    // remove relics and heroes
                    cardsInGraveyard.RemoveAll(card => card.IsHero || card.data.type == CardType.Relic || card.data.type == CardType.Power);

                    if (cardsInGraveyard.Count > 0)
                    {
                        cardsInGraveyard.Shuffle(random);

                        for (var i = 0; i < Mathf.Clamp(3, 0, cardsInGraveyard.Count); i++)
                        {
                            var randCard = GameServer.State.GetCard(cardsInGraveyard[i].uid);
                            cards.Add(randCard.data.id);
                        }
                    }
                }
                else
                {
                    Debug.LogError($"copyFromLocation: {filter.copyFromLocation} does not contain a valid location!");
                }

                return cards;
            }

            var redeemOptions = Db.CardDatabase.GetRedeemCards(filter, player.faction, opponent.faction);

            if (redeemOptions.Count <= 0)
            {
                return cards;
            }

            if (filter.fromAnotherFaction)
            {
                while (cards.Count < 3)
                {
                    var card = redeemOptions[random.Next(0, redeemOptions.Count)];
                    cards.Add(card.id);
                    redeemOptions.RemoveAll(c => c.faction == card.faction);

                    if (redeemOptions.Count <= 0) break;
                }

                return cards;
            }

            redeemOptions.Shuffle(random);

            for (var i = 0; i < Mathf.Min(3, redeemOptions.Count); i++)
            {
                cards.Add(redeemOptions[i].id);
            }

            return cards;
        }

        public override void OnExecuteCommand()
        {
        }

        private void OnCardRedeemed(string card)
        {
            if (Cards.Contains(card))
            {
                RedeemedCard = card;
            }
        }

        public override IEnumerator<float> Resolve()
        {
            if (IsLocalPlayerCommand())
            {
                GameplayScreen.Instance.redeemUi.OnCardRedeemed -= OnCardRedeemed;
                GameplayScreen.Instance.redeemUi.OnCardRedeemed += OnCardRedeemed;

                Timing.RunCoroutine(GameplayScreen.Instance.redeemUi.Open(this));

                while (string.IsNullOrEmpty(RedeemedCard))
                {
                    yield return Timing.WaitForOneFrame;
                }

                yield return Timing.WaitUntilDone(GameplayScreen.Instance.redeemUi.Close());
            }
            else if (Cards.Count <= 0)
            {
                yield break;
            }
            else if (string.IsNullOrEmpty(RedeemedCard))
            {
                RedeemedCard = AI.RedeemCard(Cards);
            }

            var addCardToZone = new AddCardToZoneCommand
            {
                PlayerUid = PlayerUid,
                SourceUid = SourceUid,
                locationToAddTo = CardLocation.Hand
            };

            addCardToZone.TargetUids =
                addCardToZone.CreateCardsFromFilter(new CardFilter { cardId = RedeemedCard }, Faction.None, 1, null,null, null, string.Empty, string.Empty);

            global::GameLogic.StartCommand(addCardToZone);

            if (addCardToZone.TargetUids.Count > 0)
            {
                RedeemCardUid = addCardToZone.TargetUids[0];
            }

            if (HeldCommands != null && HeldCommands.Count > 0)
            {
                // set the previous command to this command.
                for (var i = 0; i < HeldCommands.Count; i++)
                {
                    HeldCommands[i].previousCommand = this;
                }

                global::GameLogic.ExecutePendingCommands(HeldCommands);
            }

            global::GameLogic.ResolveTriggeredCards(GameServer.State.GetCard(SourceUid),
                GameServer.State.GetCard(RedeemCardUid), TriggerType.REDEEMED_CARD,
                new global::GameLogic.ResolveTriggeredCardsProperties { showLog = !AI.Thinking });
        }
    }
}