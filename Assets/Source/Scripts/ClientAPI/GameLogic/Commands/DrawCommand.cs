using System;
using System.Collections.Generic;
using Asyncoroutine;
using BlendModes;
using CrossBlitz.AssetManagement;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Data;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using DG.Tweening;
using MEC;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;
using Object = UnityEngine.Object;

namespace CrossBlitz.ClientAPI.Commands
{
    [System.Serializable]
    public class DrawCardCommand : Command
    {
        public int CardCountBeforeDraw;
        public int DrawCount;
        public bool Mulligan;

        public DrawCardCommand()
        {
            Type = CommandType.DRAW_CARD;
        }

        public override string GetLabelDisplay()
        {
            return base.GetLabelDisplay() + $"{GetPlayerName()} drew {DrawCount} card(s). Is Mulligan? {Mulligan}";
        }

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            if (DrawCount <= 0) return -1;

            var player = GameServer.GetPlayerState(PlayerUid);
            if (player.GetHandCount() + DrawCount >= PlayerBattleState.MaxHandSize) return -1;

            if (player.GetHandCount() <= 3) return DrawCount;

            return 1;
        }

        public string GetDrawingPlayer(GameCardState card, Ability ability)
        {
            if (ability.targetConditions.HasFlag(FilterCondition.OPPONENT))
            {
                return card.Team == Team.Client ? GameServer.OpponentPlayerState.Uid : GameServer.ClientPlayerState.Uid;
            }

            return card.Team == Team.Client ? GameServer.ClientPlayerState.Uid : GameServer.OpponentPlayerState.Uid;
        }

        public int GetDrawAmount(Ability ability)
        {
            var drawCount = ability.drawCount;

            switch (ability.drawCondition.condition)
            {
                case Condition.DAMAGED_MINIONS:
                {
                    List<TileState> occupiedTiles;

                    if (ability.damageAmountCondition.filters.HasFlag(FilterCondition.PLAYER) &&
                        ability.damageAmountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                    {
                        occupiedTiles = GameServer.State.Board.GetOccupiedTiles();
                    }
                    else if (ability.damageAmountCondition.filters.HasFlag(FilterCondition.OPPONENT))
                    {
                        occupiedTiles = GameServer.State.Board.GetOccupiedTiles(GetTeam() == Team.Client ? Team.Opponent : Team.Client);
                    }
                    else
                    {
                        occupiedTiles = GameServer.State.Board.GetOccupiedTiles(GetTeam());
                    }

                    for (var i = 0; i < occupiedTiles.Count; i++)
                    {
                        if (occupiedTiles[i].Card.owner == PlayerUid && occupiedTiles[i].Card.IsDamaged())
                        {
                            drawCount++;
                        }
                    }
                    break;
                }
            }

            // if we just fill the hand, do that instead.
            if (ability.fillHand)
            {
                var player = GameServer.GetPlayerState(PlayerUid);
                drawCount = PlayerBattleState.MaxHandSize - player.hand.Count;
            }

            return drawCount;
        }

        public override void OnPrepareCommand()
        {
        }

        public override void OnExecuteCommand()
        {
            TargetUids = new List<string>();

            // draw and keep track of how many cards are in my hand
            for (var i = 0; i < DrawCount; i++)
            {
                var playerState = IsClientCommand() ? GameServer.ClientPlayerState : GameServer.OpponentPlayerState;

                if (playerState.GetDeckCount() <= 0)
                {
                    //DrawCount--;

                    global::GameLogic.ExecuteCommand(new DrawFatigueCommand
                    {
                        SourceUid = GameServer.GetPlayerState(PlayerUid).heroUid,
                        PlayerUid = PlayerUid,
                        TargetUids = new List<string> { GameServer.GetPlayerState(PlayerUid).heroUid }
                    });
                    continue;
                }

                if (playerState.GetHandCount() >= PlayerBattleState.MaxHandSize)
                {
                    //DrawCount--;

                    var overdrawnCard = playerState.Overdraw();

                    global::GameLogic.ExecuteCommand(new OverdrawCommand
                    {
                        SourceUid = SourceUid,
                        PlayerUid = PlayerUid,
                        TargetUids = new List<string> {overdrawnCard}
                    });

                    continue;
                }

                while (playerState.IsCastWhenDrawn())
                {
                    var drawCard = playerState.deck.Peek();
                    var drawCardState = GameServer.State.GetCard(drawCard);

                    if (drawCardState.HasTrigger(TriggerType.CAST_WHEN_DRAWN))
                    {
                        var castWhenDrawn = new CastWhenDrawnCommand
                        {
                            PlayerUid = PlayerUid,
                            SourceUid = drawCardState.uid
                        };

                        global::GameLogic.ExecuteCommand(castWhenDrawn);
                    }
                }

                var drawnCard = playerState.Draw();

                // var card = GameServer.State.GetCard(drawnCard);

                // if (!AI.Thinking)
                // {
                //     if (card != null)
                //     {
                //         Debug.Log($"Adding {card.characterData.name} ({drawnCard}) to targets list.");
                //     }
                // }

                TargetUids.Add(drawnCard);

                global::GameLogic.MoveCommandToLast( this );
                global::GameLogic.ResolveTriggeredCards(null, null, TriggerType.DRAW_CARD);
            }

            if (IsClientCommand())
            {
                GameServer.ClientPlayerState.finishedDrawingInitialCards = true;
            }
            else
            {
                GameServer.OpponentPlayerState.finishedDrawingInitialCards = true;
            }


        }

        public override IEnumerator<float> Resolve()
        {
            if (IsClientCommand() && Mulligan)
            {
                while (!GameServer.OpponentPlayerState.finishedDrawingInitialCards) yield return Timing.WaitForOneFrame;
                yield return Timing.WaitUntilDone(GameplayScreen.Instance.mulliganView.AnimateMulligan());
            }
            else if (!Mulligan)
            {
                // for (var i = 0; i < TargetUids.Count; i++)
                // {
                //     yield return Timing.WaitUntilDone(DrawCard(TargetUids[i], GetTeam(), finish: i >= CardCountBeforeDraw + DrawCount - 1));
                //     Events.Publish(this, EventType.OnCardDrawn, new OnCardDrawnEventArgs { team = GetTeam(), drawCount = 1, mulligan = false });
                // }

                var hand = IsClientCommand() ? GameServer.ClientPlayerState.hand : GameServer.OpponentPlayerState.hand;

                for (var i = CardCountBeforeDraw; i < CardCountBeforeDraw + DrawCount; i++)
                {
                    if (hand.Count > i)
                    {
                        yield return Timing.WaitUntilDone(DrawCard(hand[i], GetTeam(), finish: i >= CardCountBeforeDraw + DrawCount - 1));
                        Events.Publish(this, EventType.OnCardDrawn, new OnCardDrawnEventArgs { team = GetTeam(), drawCount = 1, mulligan = false });
                    }
                }
            }
        }

        public static IEnumerator<float> DrawCard(string cardUid, Team team, bool overdraw = false, bool finish = false, bool castWhenDrawn = false)
        {
            var deck = GameplayScreen.Instance.GetDeck(team);
            var reference = AddressableReferenceLoader.GetAsset("Card_Standard", true);

            yield return Timing.WaitUntilDone(reference.AsCoroutine());

            var settings = new CreateCardFactorySettings
            {
                CardPrefab = reference.Result,
                Layout = GameplayScreen.Instance.handView.RectTransform(),
                AdditionalComponents = CardView.GetRequiredComponentsForStandardHandCard(),
                StartsDisabled = true
            };

            var cardState = GameServer.State.GetCard(cardUid);

            settings.CardState = cardState;
            settings.SortingOrder = GameCardView.HandSortingOrder + 20;

            var standardCardView = CardFactory.Create(settings, null);
            var gameCardView = standardCardView.GetCardComponent<GameCardView>();

            standardCardView.SetInteractable(false);
            standardCardView.cardObject.SetActive(false);
            standardCardView.cardBackObject.SetActive(true);

            var dragRotator = standardCardView.GetCardComponent<DragRotator>();
            dragRotator.enabled = false;

            var tc = deck.GetTopCard();

            standardCardView.transform.SetParent(tc.transform, false);
            standardCardView.transform.localEulerAngles = Vector3.zero;
            standardCardView.RectTransform().anchoredPosition = Vector3.zero;
            standardCardView.SetActive(true);
            standardCardView.SetGameState(cardState);

            deck.MoveOnScreen(0.4f);
            deck.RefreshDeckList();

            yield return Timing.WaitForSeconds(.5f);

            reference = AddressableReferenceLoader.GetAsset("Card_Zoomed", true);

            yield return Timing.WaitUntilDone(reference.AsCoroutine());

            standardCardView.transform.SetParent(deck.transform.parent.parent, true);
            standardCardView.transform.DORotate(Vector3.zero, 0.24f);
            standardCardView.RectTransform().DOAnchorPos(Vector3.zero, .5f).SetEase(Ease.OutBack);

            AudioController.PlaySound("battle_card_draw", "BARDSFX", true, standardCardView.gameObject);

            // if (team == Team.Opponent )
            // {
            //     GameplayScreen.Instance.opponentHandView.Show();
            // }

            yield return Timing.WaitForSeconds(.5f);

            if (overdraw || castWhenDrawn || team == Team.Client)
            {
                if (team == Team.Opponent)
                {
                    gameCardView.Rect.DOAnchorPosY(gameCardView.Rect.anchoredPosition.y + 50, 0.24f);
                }

                yield return Timing.WaitUntilDone(gameCardView.Flip());
                gameCardView.UpdateViewFromState();

                if (finish && team == Team.Opponent)
                {
                    yield return Timing.WaitForSeconds(.24f);
                }
            }

            if (overdraw)
            {
                yield return Timing.WaitUntilDone(Overdraw(reference.Result, cardUid, standardCardView));
            }
            else if (castWhenDrawn)
            {
                yield return Timing.WaitUntilDone(CastWhenDrawn(reference.Result, cardUid, standardCardView));
            }
            else if (team == Team.Client)
            {
                GameplayScreen.Instance.handView.TrackCard(gameCardView);

                yield return Timing.WaitForOneFrame;

                standardCardView.SetInteractable(false);
                standardCardView.GetCardComponent<HoverCard>().UsesHoverPunchEffect = false;
                standardCardView.GetCardComponent<GameCardView>().CheckForDrawIntoHand = true;
            }
            else
            {
                GameplayScreen.Instance.opponentHandView.TrackCard(gameCardView);

                if (finish)
                {
                    yield return Timing.WaitForSeconds(0.24f);

                    if (GameServer.State.CurrentPlayer != GameServer.OpponentPlayerState.Uid)
                    {
                        GameplayScreen.Instance.opponentHandView.Hide();
                    }
                }
            }

            if (finish)
            {
                deck.MoveOutOfScreen(0.4f);
            }
        }

        public static IEnumerator<float> Overdraw(GameObject cardPrefab, string cardUid, CardView standardCardView)
        {
            var settings = new CreateCardFactorySettings
            {
                CardPrefab = cardPrefab,
                Layout = GameplayScreen.Instance.handView.RectTransform(),
                AdditionalComponents = new List<Type>
                    {typeof(GameCardView), typeof(CardTransformComponent), typeof(CardScrappedComponent)}
            };

            var cardState = GameServer.State.GetCard(cardUid);

            settings.CardState = cardState;
            settings.SortingOrder = GameCardView.HandSortingOrder + 20;

            var zoomedCardView = CardFactory.Create(settings, null);
            zoomedCardView.SetInteractable(false);
            zoomedCardView.RectTransform().anchoredPosition = new Vector2(0, 0);
            zoomedCardView.GetCardComponent<GameCardView>().displayRealGameStatsOnZoomedView = true;
            zoomedCardView.GetCardComponent<GameCardView>().UpdateViewFromState(true);

            if (standardCardView)
            {
                zoomedCardView.transform.localScale = new Vector3(88f / 168f, 100f / 202f, 1);
                zoomedCardView.transform.DOScale(Vector3.one, 0.24f).SetEase(Ease.InOutBack);
                zoomedCardView.CanvasGroup.alpha = 0;
                zoomedCardView.CanvasGroup.DOFade(1, 0.24f);

                standardCardView.transform.DOScale(new Vector3(168f / 88f, 202f / 100f, 1), 0.24f)
                    .SetEase(Ease.InOutBack);
                standardCardView.CanvasGroup.DOFade(0, 0.24f);

                yield return Timing.WaitForSeconds(0.25f);

                standardCardView.SetActive(false);

                var dragRotator = standardCardView.GetCardComponent<DragRotator>();
                dragRotator.enabled = true;

                standardCardView.CanvasGroup.alpha = 1;
                standardCardView.frontOverlayCanvas.alpha = 0;
                standardCardView.frontOverlayCanvas.SetActive(false);

                if (standardCardView.attackTypeIconOverlayCanvas)
                {
                    standardCardView.attackTypeIconOverlayCanvas.alpha = 0;
                    standardCardView.attackTypeIconOverlayCanvas.SetActive(false);
                }

                yield return Timing.WaitForSeconds(.8f);
            }

            var scrapEffect = zoomedCardView.GetVfx<CardScrappedComponent, CardScrappedEffect>();

            yield return Timing.WaitUntilDone(scrapEffect.Play(null, null));

            Object.Destroy(zoomedCardView.gameObject);
        }

        public static IEnumerator<float> CastWhenDrawn(GameObject cardPrefab, string cardUid, CardView standardCardView)
        {
            var settings = new CreateCardFactorySettings
            {
                CardPrefab = cardPrefab,
                Layout = GameplayScreen.Instance.handView.RectTransform(),
                AdditionalComponents = new List<Type>
                    {typeof(GameCardView), typeof(CardTransformComponent), typeof(CardScrappedComponent)}
            };

            var cardState = GameServer.State.GetCard(cardUid);

            settings.CardState = cardState;
            settings.SortingOrder = GameCardView.HandSortingOrder + 20;

            var zoomedCardView = CardFactory.Create(settings, null);
            zoomedCardView.SetInteractable(false);
            zoomedCardView.RectTransform().anchoredPosition = new Vector2(0, 0);
            zoomedCardView.GetCardComponent<GameCardView>().displayRealGameStatsOnZoomedView = true;
            zoomedCardView.GetCardComponent<GameCardView>().UpdateViewFromState(true);

            if (standardCardView)
            {
                zoomedCardView.transform.localScale = new Vector3(88f / 168f, 100f / 202f, 1);
                zoomedCardView.transform.DOScale(Vector3.one, 0.24f).SetEase(Ease.InOutBack);
                zoomedCardView.CanvasGroup.alpha = 0;
                zoomedCardView.CanvasGroup.DOFade(1, 0.24f);

                standardCardView.transform.DOScale(new Vector3(168f / 88f, 202f / 100f, 1), 0.24f)
                    .SetEase(Ease.InOutBack);
                standardCardView.CanvasGroup.DOFade(0, 0.24f);

                yield return Timing.WaitForSeconds(0.25f);

                standardCardView.SetActive(false);

                var dragRotator = standardCardView.GetCardComponent<DragRotator>();
                dragRotator.enabled = true;

                standardCardView.CanvasGroup.alpha = 1;
                standardCardView.frontOverlayCanvas.alpha = 0;
                standardCardView.frontOverlayCanvas.SetActive(false);

                if (standardCardView.attackTypeIconOverlayCanvas)
                {
                    standardCardView.attackTypeIconOverlayCanvas.alpha = 0;
                    standardCardView.attackTypeIconOverlayCanvas.SetActive(false);
                }

                yield return Timing.WaitForSeconds(.8f);
            }

            zoomedCardView.TintOverTime(Color.white, BlendMode.Overlay, 0.4f);

            yield return Timing.WaitForSeconds(0.4f);

            zoomedCardView.Fade(0,0.24f, true);

            yield return Timing.WaitForSeconds(0.24f);

            Object.Destroy(zoomedCardView.gameObject);
        }
    }
}