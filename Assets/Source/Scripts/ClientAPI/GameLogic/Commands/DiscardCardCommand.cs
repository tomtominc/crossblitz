using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ClientAPI.Commands
{
    public class DiscardCardCommand : Command
    {
        public DiscardCardCommand() => Type = CommandType.DISCARD_CARD;

        public override string GetLabelDisplay()
        {
            var label = base.GetLabelDisplay();

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var card = GameServer.State.GetCard(TargetUids[i]);
                label += card.characterData.name + "\n";
            }

            return label;
        }

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            return IsClientCommand() ? TargetUids.Count : -TargetUids.Count;
        }


        public List<string> GetCardsToRemove(Ability ability, Command parentCommand, Command previousCommand)
        {
            var targets = new List<string>();

            if (ability.targetConditions == FilterCondition.NONE)
            {
                var player = GameServer.GetPlayerState(PlayerUid);
                var count = ability.removeAllCards ?
                    player.hand.Count : ability.amountOfCardsToRemove;
                var handShuffled = new List<string>(player.hand);

                if (!AI.Thinking)
                {
                    for (var i = handShuffled.Count - 1; i > -1; i--)
                    {
                        var card = GameServer.State.GetCard(handShuffled[i]);

                        if (card != null && card.HasAbility(AbilityKeyword.DISCARD_PROOF))
                        {
                            handShuffled.RemoveAt(i);
                        }
                    }
                }

                var random = new System.Random();
                handShuffled.Shuffle(random);

                for (var i = 0; i < count; i++)
                {
                    if (i >= handShuffled.Count) break;

                    targets.Add(handShuffled[i]);
                }
            }
            else
            {
                targets = global::GameLogic.GetTargets(GameServer.State.GetCard(SourceUid), ability, parentCommand, previousCommand, true);
            }

            return targets;
        }

        public override void OnExecuteCommand()
        {
            for (var i = 0; i < TargetUids.Count; i++)
            {
                var card = GameServer.State.GetCard(TargetUids[i]);

                if (card == null) continue;

                if (!card.HasAbility(AbilityKeyword.FLUX))
                {
                    var fromIndex = card.Discard();
                }

                // this is for Cursed Tome because the "exhaust" bool doesn't work for it, for some reason.
                if (card.HasAbility(AbilityKeyword.DISCARD))
                {
                    var discard = card.GetAbility(AbilityKeyword.DISCARD);

                    if (discard != null && discard.targetConditions.HasFlag(FilterCondition.SELF))
                    {
                        card.ExhaustAbility(discard);
                    }
                }

                if (!AI.Thinking)
                {
                    global::GameLogic.ResolveTriggeredCards(GameServer.State.GetCard(SourceUid), card, TriggerType.DISCARD, new global::GameLogic.ResolveTriggeredCardsProperties { allowFromTheGraveyard = true, showLog = false});

                    // if (card.HasAbility(AbilityKeyword.FLUX))
                    // {
                    //     card.Flux(fromIndex);
                    // }
                }
            }

            if (!AI.Thinking)
            {
                Events.Publish(this, EventType.OnCardDiscarded,
                    new OnCardDiscardedEventArgs { team = GetTeam(), discardCount = TargetUids.Count, discardedCards = TargetUids });
            }
        }

        public override IEnumerator<float> Resolve()
        {
            var player = GameServer.GetPlayerState(PlayerUid);

            if (player.Uid == GameServer.OpponentPlayerState.Uid)
            {
                GameplayScreen.Instance.opponentHandView.Show();

                yield return Timing.WaitForSeconds(0.4f);

                var coroutines = new List<CoroutineHandle>();

                for (var i = 0; i < TargetUids.Count; i++)
                {
                    coroutines.Add(Timing.RunCoroutine( GameplayScreen.Instance.opponentHandView.DiscardCard(TargetUids[i])));
                }

                while (coroutines.Exists(c => c.IsRunning)) yield return Timing.WaitForOneFrame;

                yield return Timing.WaitForSeconds(0.4f);

                GameplayScreen.Instance.opponentHandView.Hide();

            }
            else
            {
                for (var i = 0; i < TargetUids.Count; i++)
                {
                    var handCard = GameplayScreen.Instance.GetHandCard(TargetUids[i]);

                    if (handCard == null)
                    {
                        var c = GameServer.State.GetCard(TargetUids[i]);
                        Debug.LogError($"{c.characterData.name} doesn't exist in the hand? {c.location}");
                        continue;
                    }

                    handCard.UpdateViewFromState();

                    var getDiscardEffectTask = handCard.GetVfx<DiscardEffectComponent>();
                    yield return Timing.WaitUntilDone(getDiscardEffectTask.AsCoroutine());
                    var discardEffect = getDiscardEffectTask.Result;

                    if (discardEffect && discardEffect.Effect != null)
                    {
                        Timing.RunCoroutine(discardEffect.Effect.Play(this, null));
                    }
                    else
                    {
                        Timing.RunCoroutine(handCard.AnimateDestroyInHand());
                    }
                }

                yield return Timing.WaitForSeconds(1);
            }


        }
    }
}