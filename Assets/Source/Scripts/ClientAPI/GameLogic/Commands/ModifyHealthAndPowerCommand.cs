using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    public class ModifyHealthAndPowerCommand : Command
    {
        public int Power;
        public int Health;
        public bool Set;
        public List<CardHistory> PendingPowerHistories;

        public ModifyHealthAndPowerCommand()
        {
            Type = CommandType.MODIFY_POWER;
        }

        public override bool IsEquivalent(Command command)
        {
            if (command is ModifyHealthAndPowerCommand cmd)
            {
                return cmd.Power == Power && cmd.Health == Health;
            }

            return false;
        }

        public override string GetLabelDisplay()
        {
            var source = GameServer.State.GetCard(SourceUid);
            return base.GetLabelDisplay() + $"{source.characterData.name} modified {GetTargetsAsString(this)} health by {Health} and power by {Power}.";
        }

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            var score = 0;

            for (var i = 0; i < PendingPowerHistories.Count; i++)
            {
                var pendingScore = 0;
                var target = GameServer.State.GetCard(PendingPowerHistories[i].CardUid);

                if(target == null) continue;

                pendingScore += Power + Health;

                if (target.Team == Team.Client)
                {
                    pendingScore *= -1;
                }

                score += pendingScore;
            }

            return score;
        }

        public override void OnPrepareCommand()
        {
        }

        public override void OnExecuteCommand()
        {
            PendingPowerHistories=new List<CardHistory>();

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var card = GameServer.State.GetCard( TargetUids[i] );

                if (Set)
                {
                    var diffPow = Power - card.GetPendingPower();
                    var diffHealth = Health - card.GetPendingHealth();
                    PendingPowerHistories.Add(card.ModifyHealthAndPower(SourceUid, diffHealth, diffPow));
                }
                else
                {
                    PendingPowerHistories.Add(card.ModifyHealthAndPower(SourceUid, Health, Power));
                }
            }
        }

        public override IEnumerator<float> Resolve()
        {
            var sourceCard = GameServer.State.GetCard(SourceUid);

            switch (sourceCard.data.parentBehaviour)
            {
                // Effect that covers the whole screen
                case EffectParentBehaviour.GameBoard:
                    var loadTask = GameVfxProvider.GetVfxForSpell(sourceCard, null);
                    yield return Timing.WaitUntilDone(loadTask.AsCoroutine());
                    loadTask.Result.Initialize(card: null);
                    yield return Timing.WaitUntilDone(loadTask.Result.Play(this, new VfxParameters()));
                    break;


                // Effect that touches each card individually
                default:
                    for (var i = 0; i < PendingPowerHistories.Count; i++)
                    {
                        var card = GameServer.State.GetCard(PendingPowerHistories[i].CardUid);

                        //Debug.Log($"[Modify Health And Power] location = {card.location}");

                        switch (card.location)
                        {
                            case CardLocation.Hand:
                                if (card.Team == Team.Client)
                                {
                                    card.ApplyHistory(PendingPowerHistories[i].Uid);

                                    var timeout = 1f;
                                    GameCardView handCard = null;

                                    while (handCard == null)
                                    {
                                        handCard = GameplayScreen.Instance.GetHandCard(card.uid);
                                        yield return Timing.WaitForOneFrame;
                                        timeout -= Time.deltaTime;

                                        if (timeout <= 0)
                                        {
                                            Debug.LogError($"Timed out trying to get hand card {card.characterData.name}.");
                                            break;
                                        }
                                    }

                                    if (handCard)
                                    {
                                        Debug.Log($"[Modify Health And Power] Updating hand card to reflect updated stats.");
                                        handCard.UpdateViewFromState();
                                    }
                                }
                                else
                                {
                                    card.ApplyHistory(PendingPowerHistories[i].Uid);
                                    var oppHandCard = GameplayScreen.Instance.opponentHandView.GetCard(card.uid);

                                    if (oppHandCard != null)
                                    {
                                        yield return Timing.WaitUntilDone(oppHandCard.ModifyHealthAndPower(Power, Health, false));
                                    }
                                }
                                break;

                            case CardLocation.Board:

                                var getBoardCardTask = GameBoard.GetBoardCard(PendingPowerHistories[i].CardUid);
                                yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                                var getBoardCardResult = getBoardCardTask.Result;

                                if (getBoardCardResult.error != ClientErrorCode.None)
                                {
                                    Server.HandleClientError(getBoardCardResult, true);
                                }

                                if (getBoardCardResult.boardCard)
                                {
                                    if (AbilityIndex > -1)
                                    {
                                        //var sourceCard = GameServer.State.GetCard(SourceUid);
                                        yield return Timing.WaitUntilDone(PlayCustomOrGenericEffectOnBoardCard(sourceCard.GetAbilities()[AbilityIndex], this, getBoardCardResult.boardCard.State.uid, getBoardCardResult.boardCard));
                                    }

                                    getBoardCardResult.boardCard.State.ApplyHistory(PendingPowerHistories[i].Uid);
                                    var getModifyEffectTask = getBoardCardResult.boardCard.GetVfx<ModifyHealthAndPowerEffectComponent>();
                                    yield return Timing.WaitUntilDone(getModifyEffectTask.AsCoroutine());
                                    var getModifyEffectResult = getModifyEffectTask.Result;
                                    //todo figure out if modifier is from own ability
                                    //Timing.RunCoroutine( getBoardCardResult.boardCard.AnimateEffect<GenericEffectComponent>(null));
                                    yield return Timing.WaitUntilDone(((ModifyHealthAndPowerEffect)getModifyEffectResult.Effect).PlayModifyAnimation(Power, Health));
                                    getBoardCardResult.boardCard.UpdateViewFromState();
                                }

                                break;
                        }

                    }
                    break;
            }
        }
    }
}