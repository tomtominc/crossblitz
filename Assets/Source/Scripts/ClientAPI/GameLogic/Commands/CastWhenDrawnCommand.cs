using System.Collections.Generic;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    public class CastWhenDrawnCommand : Command
    {
        public CastWhenDrawnCommand() => Type = CommandType.CAST_WHEN_DRAWN;

        public override void OnExecuteCommand()
        {
            base.OnExecuteCommand();

            var player = GameServer.GetPlayerState(PlayerUid);
            player.CastWhenDrawn();

            var playCommand = new PlayCommand
            {
                PlayerUid = PlayerUid,
                SourceUid = SourceUid
            };

            global::GameLogic.ExecuteCommand(playCommand);
        }

        public override IEnumerator<float> Resolve()
        {
            yield return Timing.WaitUntilDone(DrawCardCommand.DrawCard(SourceUid,GetTeam(), false, true, true));
        }
    }
}