using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    public class ModifyHealthCommand : Command
    {
        public int Amount;
        public bool Set;
        public List<CardHistory> PendingHealthHistories;

        public ModifyHealthCommand()
        {
            Type = CommandType.MODIFY_HEALTH;
        }

        public override bool IsEquivalent(Command command)
        {
            if (command is ModifyHealthCommand cmd)
            {
                return cmd.Amount == Amount;
            }

            return false;
        }

        public override string GetLabelDisplay()
        {
            var source = GameServer.State.GetCard(SourceUid);
            return base.GetLabelDisplay() + $"{source.characterData.name} modified {GetTargetsAsString(this)} health by {Amount}.";
        }

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            var score = 0;

            for (var i = 0; i < PendingHealthHistories.Count; i++)
            {
                var pendingScore = 0;
                var target = GameServer.State.GetCard(PendingHealthHistories[i].CardUid);

                if(target == null) continue;

                pendingScore += Amount;

                if (target.Team == Team.Client)
                {
                    pendingScore *= -1;
                }

                score += pendingScore;
            }

            return score;
        }

        public static int GetHealthModifier(string sourceUid, Ability ability,Command previousCommand, Team team)
        {
            var health = 0;

            // ONLY ADD CONDITIONS ARE ARE USED FOR MODIFY STATS! Condition Enum is used for other things too - e.g. conditional abilities.
            // Admiral Brass = Class On Field
            // Merow = Highest Attack Minion
            // Abraxas = Discarded this game.
            switch (ability.abilityCondition.condition)
            {
                case Condition.CLASS_ON_FIELD:
                    List<TileState> occupiedTiles;

                    if (ability.armorCondition.filters.HasFlag(FilterCondition.PLAYER) &&
                        ability.armorCondition.filters.HasFlag(FilterCondition.OPPONENT))
                    {
                        occupiedTiles = GameServer.State.Board.GetOccupiedTiles();
                    }
                    else if (ability.armorCondition.filters.HasFlag(FilterCondition.PLAYER))
                    {
                        occupiedTiles = GameServer.State.Board.GetOccupiedTiles( team );
                    }
                    else
                    {
                        occupiedTiles = GameServer.State.Board.GetOccupiedTiles( team  == Team.Client ? Team.Opponent : Team.Client );
                    }

                    for (var i = 0; i < occupiedTiles.Count; i++)
                    {
                        if (occupiedTiles[i].Card.data.@class == ability.abilityCondition.@class && occupiedTiles[i].Card.uid != sourceUid)
                        {
                            health += ability.healAmount;
                        }
                    }
                    break;
                case Condition.PREVIOUS_COMMAND_TARGETS:
                {
                    for (var i = 0; i < previousCommand.TargetUids.Count; i++)
                    {
                        var card = GameServer.State.GetCard(  previousCommand.TargetUids[i] );
                        if (card != null) health += card.GetHealth();
                    }
                    break;
                }
                case Condition.NUMBER_OF_CARDS_PLAYED_FROM_DIFFERENT_FACTIONS:
                {
                    var playCommands = GameServer.State.GetHistories(CommandType.PLAY);

                    for (var i = 0; i < playCommands.Count; i++)
                    {
                        var command = playCommands[i];
                        var source = GameServer.State.GetCard(command.SourceUid);
                        var player = GameServer.GetPlayerState(command.PlayerUid);


                        if (ability.abilityCondition.filters.HasFlag(FilterCondition.PLAYER) && source.owner != command.PlayerUid)
                        {
                            continue;
                        }

                        if (ability.abilityCondition.filters.HasFlag(FilterCondition.OPPONENT) && source.owner == command.PlayerUid)
                        {
                            continue;
                        }

                        if (source.characterData != null && source.characterData.faction != Faction.Neutral && source.characterData.faction != player.faction)
                        {
                            health++;
                        }
                    }

                    var minionSummons = GameServer.State.GetHistories(CommandType.SUMMON);

                    for (var i = 0; i < minionSummons.Count; i++)
                    {
                        var command = (SummonCommand)minionSummons[i];

                        if (command.SummonMethod != SummonCommand.Method.PlayedFromHand) continue;

                        var source = GameServer.State.GetCard(command.SourceUid);
                        var player = GameServer.GetPlayerState(command.PlayerUid);

                        if (ability.abilityCondition.filters.HasFlag(FilterCondition.PLAYER) && source.owner != command.PlayerUid)
                        {
                            continue;
                        }

                        if (ability.abilityCondition.filters.HasFlag(FilterCondition.OPPONENT) && source.owner == command.PlayerUid)
                        {
                            continue;
                        }

                        if (source.characterData.faction != Faction.Neutral && source.characterData.faction != player.faction)
                        {
                            health++;
                        }

                    }
                    break;
                }
                case Condition.HIGHEST_ATTACK_MINION:
                    var boardMinions = GameServer.State.GetCardsInLocation(CardLocation.Board);
                    GameCardState highestAttackMinion = null;
                    for (var i = 0; i < boardMinions.Count; i++)
                    {
                        if (highestAttackMinion == null ||
                            boardMinions[i].GetPendingPower() > highestAttackMinion.GetPendingPower() ||
                            (boardMinions[i].GetPendingPower() == highestAttackMinion.GetPendingPower() &&
                             boardMinions[i].GetPendingHealth() > highestAttackMinion.GetPendingHealth()))
                        {
                            highestAttackMinion = boardMinions[i];
                        }
                    }

                    if (highestAttackMinion != null)
                    {
                        health = highestAttackMinion.GetPendingHealth();
                    }
                    break;
                case Condition.DISCARDED_THIS_GAME:
                {
                    var player = GameServer.GetPlayerState(team);
                    health = player.discardCount * ability.healthModifier;
                    break;
                }
                case Condition.COST_OF_LAST_COMMAND_TARGET:
                {
                    if (previousCommand != null && previousCommand.TargetUids.Count > 0)
                    {
                        var target = GameServer.State.GetCard( previousCommand.TargetUids[0] );
                        health = target.GetCost() * ability.healthModifier;
                    }
                    break;
                }
                case Condition.NUMBER_OF_SPECIFIC_CARDS_IN_DECK:
                {
                    var player = GameServer.GetPlayerState(team);

                    if (ability.abilityCondition.filters.HasFlag(FilterCondition.OPPONENT))
                    {
                        player = GameServer.GetPlayerState(team == Team.Client ? Team.Opponent : Team.Client);
                    }

                    health = ability.healthModifier * player.GetNumberOfCardsWithIdInDeck(ability.abilityCondition.cardId);
                    break;
                }
                case Condition.COST_OF_LAST_SPELL_USED:
                {
                    var lastPlayCommand = GameServer.State.GetLastHistoryOf(CommandType.PLAY);
                    if (lastPlayCommand.Type == CommandType.PLAY)
                    {
                        var playedCard = GameServer.State.GetCard(lastPlayCommand.SourceUid);
                        if (playedCard != null)
                        {
                            health = playedCard.GetCost();
                        }
                        else
                        {
                            Debug.LogError("Card is null???");
                        }
                    }
                    else
                    {
                        Debug.Log($"Could not find play command! {lastPlayCommand.Type}");

                        for (var i = 0; i < GameServer.State.History.Count; i++)
                        {
                            Debug.Log($"History: {GameServer.State.History[i].Type}");
                        }
                    }
                    break;
                }
                case Condition.CARDS_PLAYED_THIS_TURN:
                {
                    var player = GameServer.GetPlayerState(team);
                    health = ability.healthModifier * player.cardsPlayedThisTurn;
                    break;
                }
                default:
                    health = ability.healthModifier;
                    break;
            }

            return health;
        }

        public override void OnPrepareCommand()
        {

        }

        public override void OnExecuteCommand()
        {
            PendingHealthHistories=new List<CardHistory>();

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var card = GameServer.State.GetCard( TargetUids[i] );

                if (Set)
                {
                    var diff = Amount - card.GetPendingHealth();
                    PendingHealthHistories.Add(card.ModifyHealth(SourceUid, diff));
                }
                else
                {
                    PendingHealthHistories.Add(card.ModifyHealth(SourceUid, Amount));
                }
            }
        }

        public override IEnumerator<float> Resolve()
        {
            for (var i = 0; i < PendingHealthHistories.Count; i++)
            {
                var cardState = GameServer.State.GetCard(PendingHealthHistories[i].CardUid);

                if (cardState == null)
                {
                    Debug.LogError($"Card state isn null!! {PendingHealthHistories[i].CardUid}");
                    continue;
                }

                cardState.ApplyHistory(PendingHealthHistories[i].Uid);

                if (cardState.IsHero)
                {
                    // nothing?
                    var heroPanel = GameplayScreen.Instance.heroSideBarContainer.GetHeroView(cardState.Team);
                    if (heroPanel)
                    {
                        heroPanel.UpdateViewFromState(true);
                    }
                }
                else
                {
                    var getBoardCardTask = GameBoard.GetBoardCard(cardState.uid);
                    yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                    var getBoardCardResult = getBoardCardTask.Result;

                    if (getBoardCardResult.error != ClientErrorCode.None)
                    {
                        Server.HandleClientError(getBoardCardResult, true);
                    }

                    if (getBoardCardResult.boardCard)
                    {
                        getBoardCardResult.boardCard.State.ApplyHistory(PendingHealthHistories[i].Uid);
                        var getModifyEffectTask = getBoardCardResult.boardCard.GetVfx<ModifyHealthAndPowerEffectComponent>();
                        yield return Timing.WaitUntilDone(getModifyEffectTask.AsCoroutine());
                        var getModifyEffectResult = getModifyEffectTask.Result;
                        Timing.RunCoroutine(((ModifyHealthAndPowerEffect)getModifyEffectResult.Effect).PlayModifyAnimation(0,Amount));
                    }
                }

            }

            yield return Timing.WaitForSeconds(1);
        }
    }
}
