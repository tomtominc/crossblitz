using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    public class PlayCommand : Command
    {
        public bool OnlyShowPreview;
        public GameCardState Selection;
        public bool AutoCast;
        public bool Countered;

        public PlayCommand()
        {
            Type = CommandType.PLAY;
        }

        public override string GetLabelDisplay()
        {
            var source = GameServer.State.GetCard(SourceUid);
            return base.GetLabelDisplay() + $"{GetPlayerName()} played {source?.characterData.name}.";
        }

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            var source = GameServer.State.GetCard(SourceUid);
            if (source == null) return 0;
            return source.data.aiProperties.strategy.priority;
        }

        public override void OnPrepareCommand()
        {
        }

        public override void OnExecuteCommand()
        {
            var source = GameServer.State.GetCard(SourceUid);

            switch (source.characterData.type)
            {
                case CardType.Power:
                {
                    global::GameLogic.PlayPower(this, out  _);
                    Countered = false;
                    break;
                }
                case CardType.Relic:
                {
                    global::GameLogic.PlayRelic(this, out _);
                    Countered = false;
                    break;
                }
                case CardType.Spell:
                {
                    global::GameLogic.PlaySpell(this, out var countered);
                    Countered = countered;
                    break;
                }
                case CardType.Hero:
                {
                    global::GameLogic.PlayHero(this);
                    break;
                }
                default:
                    Debug.LogWarningFormat($"Don't use the play command for {source.characterData.type}");
                    break;
            }
        }

        public override IEnumerator<float> Resolve()
        {
            var sourceCard = GameServer.State.GetCard(SourceUid);

            if (OnlyShowPreview || Countered)
            {
                if (sourceCard.characterData.type == CardType.Spell)
                {
                    GameplayScreen.Instance.opponentPlayedCardPreview.disabled = false;
                    GameplayScreen.Instance.opponentPlayedCardPreview.SpawnCardPreview(sourceCard);
                }

                if (!Countered) yield return Timing.WaitForSeconds(2);
                yield break;
            }

            // resolves for AI only
            if (IsClientCommand()) yield break;
            if (sourceCard.characterData.type == CardType.Power) yield break;
            if (sourceCard.HasTrigger(TriggerType.CAST_WHEN_DRAWN)) yield break;

            var handCardObject = GameplayScreen.Instance.opponentHandView.GetCard( SourceUid );

            CardView standardCardView=null;
            GameCardView gameCardView=null;

            if (handCardObject)
            {
                var reference = AddressableReferenceLoader.GetAsset("Card_Standard", true);

                yield return Timing.WaitUntilDone(reference.AsCoroutine());

                var settings = new CreateCardFactorySettings
                {
                    CardPrefab = reference.Result,
                    Layout = GameplayScreen.Instance.handView.RectTransform(),
                    AdditionalComponents = CardView.GetRequiredComponentsForStandardHandCard(),
                    StartsDisabled = true,
                    SortingLayer = "Overlay"
                };

                var cardState = GameServer.State.GetCard(SourceUid);

                settings.CardState = cardState;
                settings.SortingOrder = GameCardView.HandSortingOrder + 20;

                standardCardView = CardFactory.Create(settings, null);
                gameCardView = standardCardView.GetCardComponent<GameCardView>();

                standardCardView.SetInteractable(false);
                standardCardView.cardObject.SetActive(false);
                standardCardView.cardBackObject.SetActive(true);

                var dragRotator = standardCardView.GetCardComponent<DragRotator>();
                dragRotator.enabled = false;

                standardCardView.transform.SetParent(handCardObject.transform.parent, false);
                standardCardView.transform.localEulerAngles = Vector3.zero;
                standardCardView.RectTransform().anchoredPosition = Vector3.zero;
                standardCardView.SetActive(true);
                // end loading card

                handCardObject.SetActive(false);

                GameplayScreen.Instance.opponentHandView.RemoveCard(SourceUid,null, 3);
            }

            if (sourceCard.HasTargetCondition(FilterCondition.PLAYER_SELECTED) && Selection != null)
            {
                if (standardCardView != null)
                {
                    var cardViewCanvas = standardCardView.cardObject.GetComponent<CanvasGroup>();
                    DOTween.Kill(cardViewCanvas);
                    cardViewCanvas.DOFade(0, 0.12f);

                    standardCardView.cardBackObject.GetComponent<CanvasGroup>().DOFade(0, .5f);

                    // var heroBattleView = GameplayScreen.Instance.heroSideBarContainer.GetHeroView(sourceCard.Team);
                    // Timing.RunCoroutine(heroBattleView.ShowSpellTargetingEffect());

                    var spellTargetEffect = standardCardView.GetVfx<SpellTargetComponent, SpellTargetEffect>();
                    spellTargetEffect.ShowEffect(true);

                    if (gameCardView)
                    {
                        gameCardView.RectTransform().DOLocalRotate(Vector3.zero, 0.24f);
                    }
                }

                Timing.RunCoroutine(GameplayScreen.Instance.HandleSelectingTarget(sourceCard, null, null, false));

                var target = Selection;

                yield return Timing.WaitForOneFrame;

                GameplayScreen.Instance.opponentPlayedCardPreview.disabled = false;
                GameplayScreen.Instance.opponentPlayedCardPreview.SpawnCardPreview(sourceCard);

                if (target.IsHero)
                {
                    var heroBoardTarget = target.Team == Team.Client
                        ? GameplayScreen.Instance.heroSideBarContainer.playerHeroView.boardTarget
                        : GameplayScreen.Instance.heroSideBarContainer.opponentHeroView.boardTarget;
                    // we need to animate the hero taking damage


                    if (standardCardView)
                    {
                        standardCardView.transform.DOMove(heroBoardTarget.transform.position, 0.4f);
                    }

                    yield return Timing.WaitForSeconds(0.8f);

                    GameplayScreen.Instance.OnSelectedBoardTarget(heroBoardTarget);
                    GameplayScreen.Instance.SetBoardTarget(null);
                }
                else
                {
                    var getBoardCardTargetTask = GameBoard.GetBoardCard(target.uid);
                    yield return Timing.WaitUntilDone(getBoardCardTargetTask.AsCoroutine());
                    var getBoardCardTargetResult = getBoardCardTargetTask.Result;

                    if (getBoardCardTargetResult.error != ClientErrorCode.None)
                    {
                        Server.HandleClientError(getBoardCardTargetResult, true);
                    }

                    if (getBoardCardTargetResult.boardCard)
                    {
                        if (standardCardView)
                        {
                            standardCardView.transform.DOMove(getBoardCardTargetResult.boardCard.transform.position, 0.4f);
                        }
                    }

                    yield return Timing.WaitForSeconds(0.4f);

                    GameplayScreen.Instance.HideTargets();
                }

                if (standardCardView != null)
                {
                    var spellTargetEffect = standardCardView.GetVfx<SpellTargetComponent, SpellTargetEffect>();
                    spellTargetEffect.canvasGroup.DOFade(0, 0.24f);
                }
            }
            else if (gameCardView!=null)
            {
                var transformed = false;
                var flipped = false;

                GameplayScreen.Instance.opponentPlayedCardPreview.disabled = true;
                GameplayScreen.Instance.opponentPlayedCardPreview.SpawnCardPreview(sourceCard);
                GameplayScreen.Instance.opponentPlayedCardPreview.SetCanvasAlpha(1);
                GameplayScreen.Instance.opponentPlayedCardPreview.SetCardPreviewAlpha(0);

                handCardObject.SetActive(false);
                gameCardView.View.GetCardComponent<DragRotator>().Reset();
                gameCardView.View.GetCardComponent<DragRotator>().enabled = false;
                gameCardView.RectTransform().SetParent(GameplayScreen.Instance.opponentPlayedCardPreview.cardPoint, true);
                gameCardView.RectTransform().DOLocalRotate(Vector3.zero, 0.24f);

                var duration = .8f;
                var angle = 120f;
                var point = new List<Vector2> {gameCardView.RectTransform().anchoredPosition, Vector2.zero, Vector2.zero };
                point[1] = (point[0] + (point[2] - point[0]) / 2) + (Vector2.down * angle);
                var time = 0f;

                yield return Timing.WaitUntilDone(DOTween.To(() => time, t => time = t, 1, duration).
                    OnUpdate(() =>
                    {
                        var m1 = Vector2.LerpUnclamped(point[0], point[1], time);
                        var m2 = Vector2.LerpUnclamped(point[1], point[2], time);
                        gameCardView.RectTransform().anchoredPosition = Vector2.Lerp(m1, m2, time);

                        //time += Time.deltaTime * speed;

                        if (!flipped && time >= 0.1f)
                        {
                            flipped = true;
                            Timing.RunCoroutine(gameCardView.Flip());
                        }

                        if (!transformed && time >= 0.5f)
                        {
                            transformed = true;
                            Timing.RunCoroutine(gameCardView.TransformFromStandardToZoomedCardView());
                        }
                    }).
                    SetEase(Ease.InCirc).WaitForCompletion(true));

                GameplayScreen.Instance.opponentPlayedCardPreview.ActivateFade();
            }
        }
    }
}