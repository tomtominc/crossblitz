using CrossBlitz.ServerAPI.GameLogic;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    public class DrawFatigueCommand : Command
    {
        public DrawFatigueCommand() => Type = CommandType.FATIGUE;

        public override string GetLabelDisplay()
        {
            return base.GetLabelDisplay() + $"{GetPlayerName()} drew into fatigue! Ouch.";
        }

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            return 0; // handled in the damage command
        }

        public override void OnExecuteCommand()
        {
            //Debug.LogError($"DRAW FATIGUE!!!!!!!!! {PlayerUid}, {SourceUid}, TARGETS: {TargetUids.Count} AMOUNT{GameServer.ClientPlayerState.fatigue }");
            global::GameLogic.ExecuteCommand(
                new DamageCommand
                {
                    PlayerUid = PlayerUid,
                    SourceUid = SourceUid,
                    TargetUids = TargetUids,
                    Amount = GameServer.GetPlayerState(PlayerUid).fatigue
                });

            GameServer.GetPlayerState(PlayerUid).fatigue++;
        }
    }
}