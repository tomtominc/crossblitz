using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.GameVfx;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using UnityEngine;
using Asyncoroutine;

namespace CrossBlitz.ClientAPI.Commands
{
    public class ModifyManaCommand : Command
    {
        public bool ModifyOnlyMaximumMana;
        public int Amount;
        public ModifyManaCommand() => Type = CommandType.MODIFY_MANA;

        public override string GetLabelDisplay()
        {
            return base.GetLabelDisplay();
        }

        public override int GetAiScore(AI.GetAiScoreParameters aiScoreParameters)
        {
            return Amount;
        }

        public override void OnExecuteCommand()
        {
            //var sourceCard = GameServer.State.GetCard(SourceUid);

            // Update Mana values if there is no effect associated with it, otherwise, update mana values within effect
            // if (sourceCard.data == null || !sourceCard.data.usesCustomEffect)
            // {
            //     UpdateManaValues();
            // }

            UpdateManaValues();
        }

        public override IEnumerator<float> Resolve()
        {
            var sourceCard = GameServer.State.GetCard(SourceUid);

            // Effect that covers the whole screen
            if (sourceCard.data != null && sourceCard.data.usesCustomEffect)
            {
                if (sourceCard == null)
                {
                    yield break;
                }

                var loadTask = GameVfxProvider.GetVfxForSpell(sourceCard, null);
                yield return Timing.WaitUntilDone(loadTask.AsCoroutine());
                loadTask.Result.Initialize(card: null);
                yield return Timing.WaitUntilDone(loadTask.Result.Play(this, new VfxParameters()));
            }
        }

        public void UpdateManaValues(bool isGold = false)
        {
            if (!AI.Thinking)
            {
                var player = GameServer.GetPlayerState(PlayerUid);

                if (ModifyOnlyMaximumMana)
                {
                    player.totalMana = Mathf.Clamp(player.totalMana + Amount, 0, 10);
                }
                else
                {
                    player.currentMana = Mathf.Clamp(player.currentMana + Amount, 0, 10);
                }

                GameServer.SetMana(player.GetTeam(), player.currentMana, true, false);
            }
        }
    }
}