using System.Collections;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using GameDataEditor;
using Sirenix.OdinInspector;

namespace CrossBlitz.ClientAPI.GameLogic.Data
{
    [System.Serializable]
    public class CustomGameProperties
    {
        public bool useCustomProperties;
        [ValueDropdown("GetAllCards")]
        public List<string> forcedStartingCards;
        [ValueDropdown("GetAllCards")]
        public string forcedBlitzBurst;

        #if UNITY_EDITOR
        private static IEnumerable GetAllCards()
        {
            if (Ability.cards != null) return Ability.cards;

            Ability.cards = new ValueDropdownList<string>();

            for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
            {
                var cardData = Db.CardDatabase.Cards[i];
                Ability.cards.Add($"{cardData.faction}/{cardData.name}", cardData.id);
            }


            return Ability.cards;
        }
        #endif
    }

    [System.Serializable]
    public class CustomGameOptions
    {
        public bool showAllSeenCutscenes;
        public bool turnShip;
        // public bool skipCutscenes;
        // public bool skipBattles;
    }
}