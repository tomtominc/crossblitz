using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Hero;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.Utils;
using UnityEngine;
using UnityEngine.Serialization;

namespace CrossBlitz.ClientAPI.Data
{

    /// <summary>
    /// The internal game state never leaves this client it's safe to store all sorts of info for the player that
    /// wouldn't normally be secure on the server. This keeps track of your deck/hand but never shares it with
    /// your opponent.
    /// </summary>
    [System.Serializable]
    public class PlayerBattleState : UniqueItem
    {
        public enum ControlMode
        {
            Player,
            Bot,
        }

        public enum TurnOrderCardType
        {
            Rock,
            Paper,
            Scissors,
            None,
        }

        public enum HealthState
        {
            /// <summary>
            /// Health is above 75%
            /// </summary>
            Healthy,
            /// <summary>
            /// Health is at or below 75%
            /// </summary>
            Good,
            /// <summary>
            /// Health is at or below 50%
            /// </summary>
            Stable,
            /// <summary>
            /// Health is at or below 25%
            /// </summary>
            Danger
        }

        public const int MaxHandSize = 10;

        public TurnOrderCardType turnOrderSelection;
        /// <summary>
        /// How is this player controlled?
        /// </summary>
        public ControlMode controlMode;
        /// <summary>
        /// the name of this player -- hero name in fables or username in pvp
        /// </summary>
        public string name;
        /// <summary>
        /// what faction does this hero belong to?
        /// </summary>
        public Faction faction;
        /// <summary>
        /// The hero card state
        /// </summary>
        public string heroUid;
        /// <summary>
        /// current blitz burst
        /// </summary>
        public string blitzBurst;
        /// <summary>
        /// Relics equipped
        /// </summary>
        public List<string> relics;
        /// <summary>
        /// The card back of the deck
        /// </summary>
        [FormerlySerializedAs("cardBack")] public string cardBackUid;
        /// <summary>
        /// The game states deck
        /// </summary>
        public List<string> deck;
        /// <summary>
        /// The players hand
        /// </summary>
        public List<string> hand;
        /// <summary>
        /// Current mana the player has.
        /// </summary>
        public int currentMana;
        /// <summary>
        /// The total amount of mana the player has
        /// </summary>
        public int totalMana;
        /// <summary>
        /// True after the player has finished drawing mulligan cards
        /// </summary>
        public bool finishedDrawingInitialCards;
        /// <summary>
        /// True after the player has finished mulligans
        /// </summary>
        public bool finishedMulligan;
        /// <summary>
        /// The current amount of "fatigue" the player has
        /// </summary>
        public int fatigue = 1;
        /// <summary>
        /// How many cards were played this turn, resets on turn start
        /// </summary>
        public int cardsPlayedThisTurn;
        /// <summary>
        /// How many spells were cast this turn, resets on turn start
        /// </summary>
        public int spellsCastThisTurn;
        /// <summary>
        /// How many cards this player has discarded this game.
        /// </summary>
        public int discardCount;

        public int GetDeckCount()
        {
            return deck.Count;
        }

        public int GetHandCount()
        {
            return hand.Count;
        }

        public HealthState GetHealthState()
        {
            var hero = GameServer.State.GetHero(GetTeam());

            if (hero != null)
            {
                if (hero.GetHealth() > hero.GetMaxHealthWithBuffs() * .75f)
                {
                    return HealthState.Healthy;
                }

                if (hero.GetHealth() > hero.GetMaxHealthWithBuffs() * .5f)
                {
                    return HealthState.Good;
                }

                if (hero.GetHealth() > hero.GetMaxHealthWithBuffs() * .25f)
                {
                    return HealthState.Stable;
                }
            }

            return HealthState.Danger;
        }

        public int GetBoardScore()
        {
            var boardState = GameServer.State.Board.GetOccupiedTiles(GetTeam());
            var score = 0;

            for (var i = 0; i < boardState.Count; i++)
            {
                score += boardState[i].Card.GetPower() + boardState[i].Card.GetHealth();
            }

            return score;
        }

        public int GetNumberOfCardsWithIdInDeck(string cardId)
        {
            var count = 0;

            for (var i = 0; i < deck.Count; i++)
            {
                var card = GameServer.State.GetCard(deck[i]);
                var characterId = card.characterData.id;

                if (cardId == "BandWorshiper" && characterId == "DevilcoreGroupie")
                {
                    characterId = "BandWorshiper";
                }

                if (cardId == "Bomb" && characterId == "UpgradedBomb")
                {
                    characterId = "Bomb";
                }

                if (characterId == cardId)
                {
                    count++;
                }
            }

            return count;
        }

        /// <summary>
        /// Draws a new card from the deck and removes that card from the deck.
        /// </summary>
        /// <returns>uid of the drawn card</returns>
        public string Draw()
        {
            var uid = deck.Pop();

            AddHandCard(uid);

            return uid;
        }

        public string Overdraw()
        {
            return deck.Pop();
        }

        public bool IsCastWhenDrawn()
        {
            if (deck.Count <= 0) return false;
            var drawCard = GameServer.State.GetCard(deck.Peek());
            if (drawCard == null) return false;
            return drawCard.HasTrigger(TriggerType.CAST_WHEN_DRAWN);
        }

        public void CastWhenDrawn()
        {
            deck.Pop();
        }

        public Team GetTeam()
        {
            return Uid == GameServer.ClientPlayerState.Uid ? Team.Client : Team.Opponent;
        }

        /// <summary>
        /// Removes a card from your hand and adds the new one from the top of the deck, this will not shuffle,
        /// you'll need to shuffle the cards at the end of the mulligan.
        /// </summary>
        /// <param name="replace">The card to replace</param>
        /// <returns>returns the new uid</returns>
        public string Mulligan(string replace)
        {
            var index = hand.IndexOf(replace);
            var newCard = deck.Pop();

            AddHandCard(newCard, index);
            AddBackToDeck(replace);

            return newCard;
        }

        public void AddBackToDeck(string uid)
        {
            if (!hand.Contains(uid) && deck.Contains(uid))
            {
                Debug.LogWarning($"Can't add {uid} back to deck.");
                return;
            }

            RemoveHandCard(uid, CardLocation.Deck);

            if (!deck.Contains(uid))
            {
                deck.Add(uid);
            }
        }

        public string GetHandCard(int index)
        {
            return hand[index];
        }

        public int GetHandCardIndex(string uid)
        {
            return hand.IndexOf(uid);
        }

        public void AddCardToHand(string uid, bool fromDeck)
        {
            AddHandCard(uid);

            if (fromDeck)
            {
                deck.Remove(uid);
            }
        }

        public int DiscardHandCard(string uid)
        {
            discardCount++;
            return RemoveHandCard(uid,CardLocation.Graveyard);
        }

        public bool CardIsInHand(string uid)
        {
            return hand.Contains(uid);
        }

        public int RemoveHandCard(string uid, CardLocation toLocation)
        {
            int index;

            if (hand.Contains(uid))
            {
                index = hand.IndexOf(uid);
                hand.Remove(uid);
            }
            else
            {
                Debug.LogError($"Can't remove: {uid}");
                return -1;
            }

            if (toLocation != CardLocation.Null)
            {
                var gameCardState = GameServer.State.GetCard(uid);
                gameCardState.SetCardLocation(toLocation);
            }

            return index;
        }

        public void AddHandCard(string uid, int index = -1, bool onTop = false)
        {
            if (hand.Contains(uid))
            {
                Debug.LogError($"You are trying to add a card to your hand that already exists!! {uid}");
                return;
            }

            if (onTop)
            {
                hand.Push(uid);
            }
            else if (index < 0)
            {
                hand.Add(uid);
            }
            else
            {
                hand.Insert(index, uid);
            }

            var gameCardState = GameServer.State.GetCard(uid);
            gameCardState.SetCardLocation(CardLocation.Hand);
        }

        public void AddCardToDeck(string uid, bool shuffle)
        {
            if (!deck.Contains(uid))
            {
                deck.Add(uid);

                if (shuffle)
                {
                    ShuffleDeck();
                }
            }
        }

        public void RemoveCardFromDeck(string uid)
        {
            if (deck.Contains(uid))
            {
                deck.Remove(uid);
            }
        }

        public List<CardValue> GetDeckCardsById()
        {
            var cards = new List<CardValue>();

            for (var i = 0; i < deck.Count; i++)
            {
                var card = GameServer.State.GetCard(deck[i]);
                if (card != null && card.location == CardLocation.Deck)
                {
                    if (cards.Exists(c => c.id == card.data.id))
                    {
                        cards.Find(c => c.id == card.data.id).count++;
                    }
                    else
                    {
                        cards.Add(new CardValue
                        {
                            id = card.data.id,
                            count = 1
                        });
                    }
                }
            }

            return cards;
        }

        public void ShuffleDeck()
        {
            deck.Shuffle();
        }

        public GameCardState CreateCardFromCopy(GameCardState cardState, CardLocation location)
        {
            GameCardState copy;

            if (cardState.location == CardLocation.Graveyard)
            {
                copy = CreateCardAtLocation(cardState.characterId, location);
                copy.owner = Uid;
                copy.SetCardLocation(location);
            }
            else
            {
                copy = GameCardState.AsCopy(cardState);
                copy.owner = Uid;
                copy.SetCardLocation(location);
            }

            return copy;
        }

        public bool HasActionsLeft()
        {
            var manaLeft = currentMana;

            for (var i = 0; i < hand.Count; i++)
            {
                var card = GameServer.State.GetCard(hand[i]);
                if (card.GetCost() <= manaLeft)
                {
                    return true;
                }
            }

            if (!string.IsNullOrEmpty(blitzBurst))
            {
                var blitz = GameServer.State.GetCard(blitzBurst);

                if (blitz != null && blitz.limitBreakValue >= blitz.data.limitBreak.value)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Creates a card at a card location (Deck, Board, Hand, Graveyard).
        /// Adds the card to your deck list or hand list if the location matches.
        /// </summary>
        /// <param name="characterId">The card data of the card to create. Use Db.CardDatabase to get card data.</param>
        /// <param name="location">The location to create it in data, still needs to be spawned visually after.</param>
        /// <param name="onTop">Added to the top of your hand or deck (only used for adding to the top of the deck)</param>
        /// <param name="shuffle">If the location is deck and this is true, the deck will be shuffled.</param>
        /// <param name="atIndex">At index, inserts the card at the specified index</param>
        /// <param name="startLocation">If the start location is anything other than "deck" it will force the call to resolving card changed locations</param>
        /// <returns>The card that was created, already adds the card to hand or deck list when created via this method.</returns>
        public GameCardState CreateCardAtLocation(string characterId, CardLocation location, bool onTop=false, bool shuffle=false, int atIndex =-1, CardLocation startLocation = CardLocation.Deck)
        {
            var card = new GameCardState
            {
                owner = Uid,
                characterId = characterId,
            };


            card.Init();
            card.location = startLocation;
            card.lastLocation = startLocation;
            card.SetCardLocation(location);

            switch (location)
            {
                case CardLocation.Deck:
                {
                    if (onTop) deck.Push(card.uid);
                    else if (atIndex >= 0 && atIndex < deck.Count) deck.Insert(atIndex, card.uid);
                    else deck.Add(card.uid);

                    if (shuffle)
                    {
                        ShuffleDeck();
                    }
                    break;
                }
                case CardLocation.Hand:
                {
                    if (onTop) AddHandCard(card.uid, onTop: true);
                    else if (atIndex >= 0 && atIndex < hand.Count) AddHandCard(card.uid, atIndex);
                    else AddHandCard(card.uid);
                    break;
                }
            }

            return card;
        }

        /// <summary>
        /// Generates the deck that is used by the player, this extracts runes, creates the hero and creates the deck.
        /// Does not add any cards to hand since you draw them off the top for the mulligan.
        /// </summary>
        /// <param name="deckData">The deck data to create the game deck with.</param>
        /// <param name="shuffle">Should the deck be shuffled after creation?</param>
        /// <param name="excludedCards">Should the deck be shuffled after creation?</param>
        /// <returns></returns>
        public void CreateGameDeck(DeckData deckData, bool shuffle = true, List<string> excludedCards = null)
        {
            deck = new List<string>();
            hand = new List<string>();
            relics = new List<string>();

            var heroData = deckData.GetHeroData(false);
            var battleData = Db.BattleDatabase.GetBattleData(GameServer.Mode.BattleUid);

            var hero = new GameCardState
            {
                owner = Uid,
                heroData = heroData ?? HeroData.ConvertFromCardData(Db.CardDatabase.GetCard(deckData.hero), battleData.Health),
            };

            hero.Init();
            hero.SetCardLocation(CardLocation.Board);

            faction = hero.characterData.faction == Faction.Neutral ? battleData.Faction : hero.characterData.faction;
            heroUid = hero.uid;
            cardBackUid = deckData.cardBackUid;

            for (var i = 0; i < deckData.cards.Count; i++)
            {
                for (var j = 0; j < deckData.cards[i].count; j++)
                {
                    if (excludedCards != null && excludedCards.Contains(deckData.cards[i].id))
                    {
                        continue;
                    }

                    var cardData = Db.CardDatabase.GetCard(deckData.cards[i].id);

                    if (cardData == null)
                    {
                        Debug.LogError($"Card {deckData.cards[i].id} is null!");
                        continue;
                    }

                    if (cardData.type == CardType.Elder_Relic || cardData.type == CardType.Relic)
                    {
                        var card = CreateCardAtLocation(cardData.id, CardLocation.Board);

                        if (cardData.type == CardType.Elder_Relic)
                        {
                            relics.Push(card.uid);
                        }
                        else
                        {
                            relics.Add(card.uid);
                        }
                    }
                    else if (cardData.type == CardType.Power)
                    {
                        var card = CreateCardAtLocation(cardData.id, CardLocation.Board);
                        blitzBurst = card.uid;
                    }
                    else
                    {
                        CreateCardAtLocation(cardData.id, CardLocation.Deck);
                    }
                }
            }

            if (shuffle)
            {
                ShuffleDeck();
            }
        }
    }
}
