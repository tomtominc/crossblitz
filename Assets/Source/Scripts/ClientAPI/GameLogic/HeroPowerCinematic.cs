using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asyncoroutine;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Databases;
using CrossBlitz.Hero;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using DG.Tweening;
using MEC;
using TakoBoyStudios.Events;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ClientAPI
{
    public class HeroPowerCinematic : MonoBehaviour
    {
        public bool testOnly;
        public CanvasGroup canvas;
        public Image backgroundImage;
        public CanvasGroup background;
        public SpriteAnimation bigX;
        public SpriteAnimation heroContainer;
        public SpriteAnimation hero;
        public SpriteAnimation heroEyeFlash;
        public SpriteAnimation containerOutline;
        public SpriteAnimation containerInnerShadow;
        public SpriteAnimation exclamation;
        public RectTransform cardContainer;

        private CardView _loadedCard;

        private void Start()
        {
            if (!testOnly)
            {
                Events.Subscribe(EventType.OnContinueFromMatchFinished, OnContinueFromMatchFinished);
            }
        }

        private void OnDestroy()
        {
            if (!testOnly)
            {
                Events.Unsubscribe(EventType.OnContinueFromMatchFinished, OnContinueFromMatchFinished);
            }
        }

        private void OnContinueFromMatchFinished(IMessage message)
        {
            gameObject.SetActive(false);
        }

        public void SetHero(CharacterData characterData, bool preview)
        {
            var heroFaction = characterData.faction.ToString().ToLower();

            if (preview)
            {
                canvas.alpha = 1;
                background.alpha = 0;
                bigX.SetActive(false);

                hero.SetActive(true);
                heroContainer.SetActive(true);
                exclamation.SetActive(true);
                heroEyeFlash.SetActive(characterData.heroPowerCinematicShowEyeFlash);
                containerOutline.SetActive(true);
                containerInnerShadow.SetActive(true);

                heroContainer.Play("idle");
                exclamation.Play($"{heroFaction}-idle");
                if (characterData.heroPowerCinematicShowEyeFlash) heroEyeFlash.Play("idle");
                containerOutline.Play("idle");
                containerInnerShadow.Play("idle");

                hero.RectTransform().anchoredPosition = characterData.heroPowerCinematicOffset;
                heroEyeFlash.RectTransform().anchoredPosition = characterData.heroPowerCinematicEyeOffset;
            }

            if (string.IsNullOrEmpty(characterData.heroPowerCinematicEmotion))
            {
                var anim = $"{characterData.portraitUrl}-idle";

                if (!hero.Play(anim))
                {
                    //Debug.LogError($"Could not play blitz burst cinematic for {anim}");
                }
            }
            else
            {
                var anim = $"{characterData.portraitUrl}-{characterData.heroPowerCinematicEmotion}";

                if (!hero.Play(anim))
                {
                    //Debug.LogError($"Could not play blitz burst cinematic for {anim}");
                }
            }
        }

        public IEnumerator<float> PlayCinematic(string heroUid, GameCardState cardState)
        {
            canvas.alpha = 0;
            gameObject.SetActive(true);

            // setup
            bigX.SetActive(false);
            heroContainer.SetActive(false);
            exclamation.SetActive(false);
            heroEyeFlash.SetActive(false);
            containerInnerShadow.SetActive(false);

            string heroFaction;
            CharacterData characterData;

            if (!testOnly)
            {
                var heroData = GameServer.State.GetCard(heroUid);
                characterData = Db.CharacterDatabase.GetCharacter(heroData.characterData.id);
                heroFaction = characterData.faction.ToString().ToLower();
            }
            else
            {
                characterData = Db.CharacterDatabase.GetCharacter(heroUid);
                heroFaction = characterData.faction.ToString().ToLower();
            }

            if (cardState != null)
            {
                yield return Timing.WaitUntilDone(LoadZoomedCardView(cardState).AsCoroutine());
            }

            canvas.alpha = 1;
            DOTween.Kill(background);
            background.DOFade(1, 0.24f);
            containerOutline.Play("spawn", ()=> containerOutline.Play("idle"));

            yield return Timing.WaitForSeconds(0.03f);

            containerInnerShadow.SetActive(true);
            containerInnerShadow.Play("spawn", () => containerInnerShadow.Play("idle"));

            var heroRect = hero.RectTransform();
            var heroSize = heroRect.sizeDelta;
            var targetPosition = characterData.heroPowerCinematicOffset;

            if (string.IsNullOrEmpty(characterData.heroPowerCinematicEmotion))
            {
                var anim = $"{characterData.portraitUrl}-idle";

                if (!hero.Play(anim))
                {
                    Debug.LogError($"Could not play blitz burst cinematic for {anim}");
                }
            }
            else
            {
                var anim = $"{characterData.portraitUrl}-{characterData.heroPowerCinematicEmotion}";

                if (!hero.Play(anim))
                {
                    Debug.LogError($"Could not play blitz burst cinematic for {anim}");
                }
            }

            DOTween.Kill(heroRect);
            heroRect.anchoredPosition = new Vector2(-heroSize.x*2, targetPosition.y);

            heroContainer.SetActive(true);
            heroContainer.Play("spawn");

            while (!heroContainer.IsDone) yield return Timing.WaitForOneFrame;

            heroContainer.Play("idle");

            bigX.SetActive(true);
            bigX.Play("spawn");

            if (_loadedCard)
            {
                _loadedCard.SetActive(true);
                _loadedCard.cardObjectCanvas.alpha = 0;
                _loadedCard.cardObjectCanvas.DOFade(1, 0.24f);
                _loadedCard.RectTransform().anchoredPosition = new Vector2(0, -40f);
                _loadedCard.RectTransform().DOAnchorPos(Vector2.zero, 0.16f);
            }


            heroRect.DOAnchorPos(targetPosition, 0.16f);

            yield return Timing.WaitForSeconds(0.16f);

            heroRect.DOAnchorPosX(targetPosition.x + 64f, 4f);

            if (_loadedCard)
            {
                _loadedCard.RectTransform().DOAnchorPos(new Vector2(0, 16), 2f);
            }

            yield return Timing.WaitForSeconds(0.24f);

            if (characterData.heroPowerCinematicShowEyeFlash)
            {
                heroEyeFlash.RectTransform().anchoredPosition = characterData.heroPowerCinematicEyeOffset;
                heroEyeFlash.SetActive(true);
                heroEyeFlash.Play("flash");
            }

            exclamation.SetActive(true);
            exclamation.Play($"{heroFaction}-spawn", () => exclamation.Play($"{heroFaction}-idle"));

            if (characterData.heroPowerCinematicShowEyeFlash)
            {
                while (!heroEyeFlash.IsDone) yield return Timing.WaitForOneFrame;
                heroEyeFlash.SetActive(false);
            }
            else
            {
                // wait for how much eye flash would have taken
                yield return Timing.WaitForSeconds(0.4f);
            }

            yield return Timing.WaitForSeconds(1f);

            canvas.DOFade(0, 0.24f).OnComplete(() =>
            {
                if (_loadedCard) Destroy(_loadedCard.gameObject);
                if (!testOnly) gameObject.SetActive(false);

                DOTween.Kill(heroRect);
                hero.RectTransform().anchoredPosition = targetPosition;
            });

            yield return Timing.WaitForSeconds(0.24f);
        }

        private async Task LoadZoomedCardView(GameCardState gameCardState)
        {
            var zoomedCardPrefab = await AddressableReferenceLoader.GetAsset("Card_Zoomed", true);
            var settings = new CreateCardFactorySettings
            {
                CardPrefab = zoomedCardPrefab,
                CardState = gameCardState,
                Layout = cardContainer,
                AdditionalComponents = new List<Type>{ typeof (GameCardView) }
            };

            _loadedCard = CardFactory.Create(settings, null);
            _loadedCard.SetInteractable(false);
            _loadedCard.SetSortingLayer("Particles");
            _loadedCard.SetSortOrder(1000);
            _loadedCard.cardOutline.RemoveOutline();
            _loadedCard.RectTransform().anchoredPosition = Vector2.zero;
            _loadedCard.cardObjectCanvas.alpha = 0;

            while (_loadedCard.isDirty || _loadedCard.loadingData) await Task.Delay(60);
        }
    }
}