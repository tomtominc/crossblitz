using UnityEngine;

namespace CrossBlitz.ClientAPI
{
    public interface IBoardTarget
    {
        GameObject Self { get; }
    }
}
