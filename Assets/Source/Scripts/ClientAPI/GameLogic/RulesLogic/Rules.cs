using System;

[System.Serializable]
public class Rules : IDisposable
{
    public virtual int StartingDrawCount(int playerIndex)
    {
        return playerIndex > 0 ? 4 : 3;
    }

    public virtual int GetDrawAmountPerTurn()
    {
        return 1;
    }

    public void Dispose()
    {

    }
}
