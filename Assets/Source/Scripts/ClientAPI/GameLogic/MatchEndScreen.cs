using System.Collections.Generic;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Fables.Battle;
using CrossBlitz.GameVfx;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.Transition;
using DG.Tweening;
using MEC;
using TakoBoyStudios.Events;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ClientAPI
{
    public class MatchEndScreen : MonoBehaviour
    {
        public CanvasGroup canvas;
        public SpriteAnimation banner;
        public SpriteAnimation continueBanner;
        public CanvasGroup overlayTile;
        public Button continueButton;
        public GoopScreenEffect goopScreen;

        private void Start()
        {
            banner.SetActive(false);
            overlayTile.SetActive(false);
            continueButton.SetActive(false);
            continueBanner.SetActive(false);

            continueButton.interactable = false;
            continueButton.onClick.AddListener(OnContinue);

            Events.Subscribe(EventType.OnMatchWon, OnMatchWon);
            Events.Subscribe(EventType.OnMatchLost, OnMatchLost);
            Events.Subscribe(EventType.OnMatchDraw, OnMatchDraw);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnMatchWon, OnMatchWon);
            Events.Unsubscribe(EventType.OnMatchLost, OnMatchLost);
            Events.Unsubscribe(EventType.OnMatchDraw, OnMatchDraw);
        }

        private IEnumerator<float> AnimateIn(string bannerAnimation)
        {

            var matchResult = App.BattleResults.GetMatchResult();

            yield return Timing.WaitForSeconds(0.5f);

            AudioController.StopSong();

            goopScreen.StartSplash();

            yield return Timing.WaitForSeconds(.8f);

            overlayTile.SetActive(true);
            overlayTile.DOFade(1, 0.6f);

            continueButton.interactable = false;
            continueButton.SetActive(true);

            yield return Timing.WaitUntilDone(goopScreen.FadeInGoop());

            AudioController.StopSong();

            var jingle = string.Empty;

            switch (matchResult)
            {
                case BattleResults.Result.Won:
                    jingle = "jingle_victory";
                    break;
                case BattleResults.Result.Lost:
                    jingle = "jingle-defeat";
                    break;
                case BattleResults.Result.Draw:
                    jingle = "jingle-draw";
                    break;
            }

            AudioController.PlaySound(jingle, "MUSIC/JINGLES");

            banner.SetActive(true);
            banner.Play(bannerAnimation);

            if (matchResult == BattleResults.Result.Won)
            {
                AudioController.PlaySound("battle_end_win_1", "BARDSFX");
                yield return Timing.WaitForSeconds(.5f);
                AudioController.PlaySound("battle_end_win_2", "BARDSFX");
                yield return Timing.WaitForSeconds(.5f);
                AudioController.PlaySound("battle_end_win_3", "BARDSFX");
                yield return Timing.WaitForSeconds(.15f);
                AudioController.PlaySound("battle_end_win_4", "BARDSFX");
            }

            if (matchResult == BattleResults.Result.Lost)
            {
                AudioController.PlaySound("battle_end_banner_lose_1", "BARDSFX");
                yield return Timing.WaitForSeconds(.5f);
                AudioController.PlaySound("battle_end_banner_lose_2", "BARDSFX");
                yield return Timing.WaitForSeconds(.5f);
                AudioController.PlaySound("battle_end_banner_lose_3", "BARDSFX");
                yield return Timing.WaitForSeconds(.18f);
                AudioController.PlaySound("battle_end_banner_lose_4", "BARDSFX");
            }

            if (matchResult == BattleResults.Result.Draw)
            {
                AudioController.PlaySound("battle_end_banner_lose_1", "BARDSFX");
                yield return Timing.WaitForSeconds(.5f);
                AudioController.PlaySound("battle_end_banner_lose_2", "BARDSFX");
                yield return Timing.WaitForSeconds(.5f);
                AudioController.PlaySound("battle_end_banner_lose_3", "BARDSFX");
                yield return Timing.WaitForSeconds(.25f);
                AudioController.PlaySound("battle_end_banner_lose_4", "BARDSFX");
            }


            while (!banner.IsDone) yield return Timing.WaitForOneFrame;

            continueBanner.SetActive(true);
            continueBanner.Play("spawn");

            while (!continueBanner.IsDone) yield return Timing.WaitForOneFrame;

            continueButton.interactable = true;
        }

        private IEnumerator<float> AnimateOut()
        {
            yield return Timing.WaitUntilDone(canvas.DOFade(0, 0.2f).WaitForCompletion(true));
            UnloadGameScene();
        }

        private async void UnloadGameScene()
        {
            await SceneController.Instance.UnloadScene("Game");
            GameServer.Dispose();
        }

        private void OnMatchWon(IMessage message)
        {
            Timing.RunCoroutine(AnimateIn("victory"));
        }

        private void OnMatchLost(IMessage message)
        {
            Timing.RunCoroutine(AnimateIn("defeat"));
        }

        private void OnMatchDraw(IMessage message)
        {
            Timing.RunCoroutine(AnimateIn("draw"));
        }

        private void OnContinue()
        {
            if (!continueButton.interactable) return;

            continueButton.interactable = false;

            Timing.RunCoroutine(FadeOut());

            //GoToBattleResults();
        }



        private IEnumerator<float> FadeOut()
        {
           // yield return Timing.WaitUntilDone(TransitionController.TransitionIn(TransitionController.TransitionType.Hex));
            yield return Timing.WaitUntilDone(TransitionController.TransitionIn(TransitionController.TransitionType.Fade));
            yield return Timing.WaitForSeconds(1f);

            GoToBattleResults();
        }

        private async void GoToBattleResults()
        {
            Events.Publish(this, EventType.OnContinueFromMatchFinished, null);

            await SceneController.Instance.LoadScene(new SceneLoadParams { SceneToLoad = "MatchFinished" });

            if (App.BattleResults.GetMatchResult() == BattleResults.Result.Won) AudioController.PlaySong("battle-results");
            else AudioController.PlaySong("battle-results-defeat");

            Timing.RunCoroutine(AnimateOut());
        }
    }
}