using System;
using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Databases;
using CrossBlitz.Developer;
using CrossBlitz.Fables.Cutscene;
using CrossBlitz.Fables.Data;
using CrossBlitz.GameVfx;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.Settings;
using CrossBlitz.Transition;
using CrossBlitz.ViewAPI;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using Source.Scripts.AssetManagement;
using Source.Scripts.Hero;
using TakoBoyStudios.Events;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;
using Random = UnityEngine.Random;

namespace CrossBlitz.ClientAPI
{
    public class GameplayScreen : View, IAssetLoader
    {
        public enum ScreenStateType
        {
            NormalUpdate,
            DraggingCard,
            HandlingSelection,
        }

        public static GameplayScreen Instance;

        public const int ScreenSortOrder = -10;
        public const int TargetShadowSortOrder = 100;
        public const int BoardTargetSortOrder = TargetShadowSortOrder + 1;

        public HeroPowerCinematic heroPowerCinematic;
        public HeroPowerCinematic opponentHeroPowerCinematic;
        public OpponentPlayedCardPreview opponentPlayedCardPreview;
        public Visuals.Deck playerDeck;
        public Visuals.Deck opponentDeck;
        public RedeemUi redeemUi;
        public RectTransform shakeContainer;
        public GameBoard gameBoard;
        public Button historyButton;
        public SimpleCardHistoryView simpleCardHistory;
        public HeroSideBarContainer heroSideBarContainer;
        public MulliganView mulliganView;
        public MatchTurnBanner matchTurnBanner;
        public HandView handView;
        public OpponentHandView opponentHandView;
        public CanvasGroup boardTargetShadow;
        public BlitzButton blitzButton;
        public TurnOrderOverlay turnOrderOverlay;
        public Button settingsButton;
        public MatchDialogueStage dialogueStage;
        public List<GameObject> environmentBackgrounds;
        public List<GameObject> weatherEffects;

        public string AssetName => name;
        private bool _isBusy = true;
        public bool IsBusy => _isBusy;

        private ScreenStateType _screenState;
        public ScreenStateType ScreenState => _screenState;
        private Tweener _boardTargetShadowAnimation;
        private List<BoardTarget> _showingTargets;
        public List<BoardTarget> ShowingTargets => _showingTargets;

        private Vector2 originPosition;
        private float shake_duration;
        private float shake_time;

        private float shake_magnitude;
        private float shake_remain;

        private void Awake()
        {
            Instance = this;
        }

        public override void GatherAssetLoaders()
        {
            // TransitionController.AssetsToWaitFor.Add(gameBoard);
            // TransitionController.AssetsToWaitFor.Add(heroSideBarContainer);
        }

        private void Start()
        {
            if (settingsButton) settingsButton.onClick.AddListener(OnSettingsButton);
            if (historyButton) historyButton.onClick.AddListener(OnHistoryButton);

            Events.Subscribe(EventType.OnMatchWon,OnMatchWon);
            Events.Subscribe(EventType.OnMatchLost, OnMatchLost);
            Events.Subscribe(EventType.OnMatchDraw, OnMatchDraw);
            Events.Subscribe(EventType.OnContinueFromMatchFinished, OnContinueFromMatchFinished);
            Events.Subscribe(EventType.OnModifiedMana, OnModifyMana);
            Events.Subscribe(EventType.OnMinionDestroyed, OnMinionDestroyed);

            if (!string.IsNullOrEmpty(GameServer.Mode.BattleUid))
            {
                var battle = Db.BattleDatabase.GetBattleData(GameServer.Mode.BattleUid);
                if (battle != null && battle.Weather != FableRoomData.RoomWeather.Sunny)
                {
                    weatherEffects[(int) battle.Weather - 1].SetActive(true);
                }
            }
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnMatchWon,OnMatchWon);
            Events.Unsubscribe(EventType.OnMatchLost, OnMatchLost);
            Events.Unsubscribe(EventType.OnMatchDraw, OnMatchDraw);
            Events.Unsubscribe(EventType.OnContinueFromMatchFinished, OnContinueFromMatchFinished);
            Events.Unsubscribe(EventType.OnModifiedMana, OnModifyMana);
            Events.Unsubscribe(EventType.OnMinionDestroyed, OnMinionDestroyed);
        }

        private void OnContinueFromMatchFinished(IMessage message)
        {
            gameObject.SetActive(false);
        }

        private void OnMatchWon(IMessage message)
        {
            heroSideBarContainer.opponentHeroView.PlayDeath(1);
        }

        private void OnMatchLost(IMessage message)
        {
            heroSideBarContainer.playerHeroView.PlayDeath(0);
        }

        private void OnMatchDraw(IMessage message)
        {
            heroSideBarContainer.playerHeroView.PlayDeath(0);
            heroSideBarContainer.opponentHeroView.PlayDeath(2);
        }

        private void OnHistoryButton()
        {
            simpleCardHistory.Open();
        }

        public override void Open()
        {
            _showingTargets = new List<BoardTarget>();
            SetScreenState(ScreenStateType.NormalUpdate);

            heroSideBarContainer.LoadForBattle();
            gameBoard.SetupBattlefield();

            blitzButton.SetInteractable(false);
            blitzButton.OnPressed += EndTurnPressed;

            boardTargetShadow.alpha = 0;
            boardTargetShadow.SetActive(false);
            boardTargetShadow.GetComponent<Canvas>().sortingOrder = TargetShadowSortOrder;

            if ((int) GameServer.Mode.SceneEnvironment >= environmentBackgrounds.Count)
            {
                for (var i = 0; i < environmentBackgrounds.Count; i++)
                {
                    environmentBackgrounds[i].SetActive(i == 0);
                }
            }
            else
            {
                for (var i = 0; i < environmentBackgrounds.Count; i++)
                {
                    environmentBackgrounds[i].SetActive(i == (int) GameServer.Mode.SceneEnvironment);
                }
            }

            LoadPrefabs();
        }

        private void EndTurnPressed()
        {
            if (!GameServer.State.IsClientsTurn())
            {
                Debug.Log("State is not clients turn?");
                return;
            }

            if (StateController.CurrentState != StateDefinition.GAME_PLAYER_UPDATE_TURN)
            {
                Debug.Log("we are not in the turn update");
                return;
            }

            if (ScreenState != ScreenStateType.NormalUpdate)
            {
                return;
            }

            var endTurnCommand = new EndTurnCommand
            {
                PlayerUid = GameServer.ClientPlayerState.Uid,
                Team = Team.Client,
            };

            global::GameLogic.StartCommand(endTurnCommand);

            Refresh();
            blitzButton.FlipToCombatPhase(Team.Client);
        }

        public void PlayerStartedTurn()
        {
            blitzButton.FlipToPlayerTurn();
            handView.UpdateCardsFromState();
        }

        public void OpponentTurnStarted()
        {
            blitzButton.FlipToOpponentTurn();
            handView.UpdateCardsFromState();
        }

        public void OnGameStateChanged()
        {
            handView.UpdateCardsFromState();
        }

        public void UpdateBlitzButtonForNoActionsLeft()
        {
            blitzButton.ClientHasNoActionsLeft(GameServer.State.IsClientsTurn() && !GameServer.ClientPlayerState.HasActionsLeft());
        }

        private async void LoadPrefabs()
        {
        }

        private void Update()
        {
            if (shake_remain > 0)
            {
                shakeContainer.anchoredPosition = new Vector2(
                    originPosition.x + Random.Range(-shake_remain, shake_remain),
                    originPosition.y + Random.Range(-shake_remain, shake_remain));

                shake_time = shake_time + Time.deltaTime;
                shake_remain = Mathf.Lerp(shake_remain, 0, shake_time / shake_duration);

                if (shake_remain <= float.Epsilon)
                {
                    shake_remain = 0;
                    shakeContainer.anchoredPosition = originPosition;
                }
            }

            historyButton.interactable = App.Settings.GetShowDebugHistories() && !simpleCardHistory.IsActive();

            //#if UNITY_EDITOR
            if (Application.platform == RuntimePlatform.WindowsEditor ||
                Application.platform == RuntimePlatform.WindowsPlayer)
            {
                if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.W))
                {
                    TerminalCommands.FinishMatch("win");
                }
                if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.L))
                {
                    TerminalCommands.FinishMatch("lose");
                }
                if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.D))
                {
                    TerminalCommands.FinishMatch("draw");
                }
                if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.T))
                {
                    GameServer.Dispose();
                    TerminalCommands.OpenTools();
                }
                if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.R))
                {
                    Refresh();
                }
            }
            else
            {
                if (Input.GetKey(KeyCode.LeftCommand) && Input.GetKeyDown(KeyCode.W))
                {
                    TerminalCommands.FinishMatch("win");
                }
                if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.L))
                {
                    TerminalCommands.FinishMatch("lose");
                }
                if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.D))
                {
                    TerminalCommands.FinishMatch("draw");
                }
                if (Input.GetKey(KeyCode.LeftCommand) && Input.GetKeyDown(KeyCode.T))
                {
                    GameServer.Dispose();
                    TerminalCommands.OpenTools();
                }
                if (Input.GetKey(KeyCode.LeftCommand) && Input.GetKeyDown(KeyCode.R))
                {
                    Refresh();
                }
            }
            //#endif
        }

        public void Refresh()
        {
            for (var i = 0; i < handView.cards.Count; i++)
            {
                handView.cards[i].State.RefreshStats();
                handView.cards[i].UpdateViewFromState();
            }

            for (var i = 0; i < gameBoard.tiles.Count; i++)
            {
               if (gameBoard.tiles[i].Card)
               {
                   gameBoard.tiles[i].Card.State.RefreshStats();
                   gameBoard.tiles[i].Card.UpdateViewFromState();
               }
            }

            heroSideBarContainer.playerHeroView.UpdateViewFromState(true);
            heroSideBarContainer.opponentHeroView.UpdateViewFromState(true);
        }

        public void ShakeScreen(int magnitude=4, float duration=2, bool chromaticAberration=false)
        {
            shake_duration = duration;
            shake_magnitude = magnitude;
            shake_remain = magnitude;
            shake_time = 0;

            if (chromaticAberration)
            {
                FilterController.HitWithChromaticAberration();
            }
        }

        public void ShowTargets(List<BoardTarget> boardTargets, float alpha = 0.5f)
        {
            TweenUtility.Kill(_boardTargetShadowAnimation, false);

            boardTargetShadow.SetActive(true);
            _boardTargetShadowAnimation = boardTargetShadow.DOFade(alpha, 0.24f);

            for (var i = 0; i < boardTargets.Count; i++)
            {
                boardTargets[i].Show(BoardTarget.ShowMode.Select);
                _showingTargets.Add(boardTargets[i]);
            }
        }

        public async void ShowTargets(GameCardState card)
        {
            TweenUtility.Kill(_boardTargetShadowAnimation, false);

            SetScreenState(ScreenStateType.DraggingCard);

            boardTargetShadow.SetActive(true);
            _boardTargetShadowAnimation = boardTargetShadow.DOFade(1f, 0.24f);

            if (card.data.type == CardType.Minion || card.data.type == CardType.Trick)
            {
                // we need to figure out all sorts of shit here
                var playerTiles = gameBoard.tiles.FindAll(x => x.Team == Team.Client);

                for (var i = 0; i < playerTiles.Count; i++)
                {
                    var tile = playerTiles[i];

                    if (string.IsNullOrEmpty(tile.State.CardId))
                    {
                        var target = playerTiles[i].GetComponent<BoardTarget>();

                        target.Show(BoardTarget.ShowMode.Drop);
                        _showingTargets.Add(target);
                    }
                }
            }
            else if (card.data.type == CardType.Spell)
            {
                if (card.data.usesTargetingEffect)
                {
                    var targets = global::GameLogic.GetTargets(card, card.GetAbilities()[0], null,null, false);

                    if (targets.Count > 0)
                    {
                        for (var i = 0; i < targets.Count; i++)
                        {
                            BoardTarget target;

                            var cardTarget = GameServer.State.GetCard(targets[i]);

                            if (cardTarget.characterData.type == CardType.Hero)
                            {
                                target = cardTarget.Team == Team.Client
                                    ? heroSideBarContainer.opponentHeroView.boardTarget
                                    : heroSideBarContainer.playerHeroView.boardTarget;
                            }
                            else
                            {
                                var getBoardCardResult = await GameBoard.GetBoardCard(targets[i]);

                                if (getBoardCardResult.boardCard == null ||
                                    getBoardCardResult.error != ClientErrorCode.None)
                                {
                                    Server.HandleClientError(getBoardCardResult, false);
                                    continue;
                                }

                                target = getBoardCardResult.boardCard.GetComponent<BoardTarget>();

                                if (target == null)
                                {
                                    Debug.LogError($"Something went wrong, the target is null Card uid = ({targets[i]})");
                                    continue;
                                }
                            }


                            target.Show(BoardTarget.ShowMode.Drop);
                            _showingTargets.Add(target);
                        }
                    }
                    else
                    {
                        gameBoard.ShowSpellTarget();
                    }
                }
                else
                {
                    gameBoard.ShowSpellTarget();
                }
            }
        }

        public void HideTargets()
        {
            SetScreenState(ScreenStateType.NormalUpdate);

            TweenUtility.Kill(_boardTargetShadowAnimation, false);

            _boardTargetShadowAnimation = boardTargetShadow.DOFade(0f, 0.5f)
                .OnComplete(() => boardTargetShadow.SetActive(false));

            for (var i = 0; i < _showingTargets.Count; i++)
            {
                _showingTargets[i].Hide();
            }

            _showingTargets.Clear();
            gameBoard.HideSpellTarget();
        }

        public bool IsShowingTargets()
        {
            return _showingTargets.Count > 0;
        }

        /// <summary>
        /// The selected board target is used with the HandleSelectingTarget method which
        /// waits until the player selects something.
        /// </summary>
        private BoardTarget m_selectedBoardTarget;

        /// <summary>
        /// The property to use in other classes - selectedBoardTarget cannot be set outside this class
        /// </summary>
        public BoardTarget SelectedBoardTarget => m_selectedBoardTarget;

        public event Action<ScreenStateType> OnScreenStateChanged;
        public void SetScreenState(ScreenStateType screenState)
        {
            _screenState = screenState;
            OnScreenStateChanged?.Invoke(_screenState);
        }

        private void OnCancelSelection()
        {
            m_selectionHasBeenCanceled = true;
        }
        private bool m_selectionHasBeenCanceled;

        /// <summary>
        /// Handles allowing a visual selection of targets. This is only used for cards that are CAST before
        /// selection. The only use case for this as of now is for battlecry minions. Because you summon the minion
        /// on a slot before you select its target.
        /// NOTE: When passing in the game card view you should check if this card has abilities BEFORE invoking this
        /// method.
        /// </summary>
        /// <param name="card">The game card to handle the selection for, should have an ability!</param>
        /// <param name="summoningOn">Used when checking for taunt targets</param>
        /// <param name="handlePlayerInput">Used when checking for taunt targets</param>
        /// <param name="targetingEffect">The targeting effect on the card, this allows us to see if the action has been canceled</param>
        /// <returns></returns>
        public IEnumerator<float> HandleSelectingTarget(GameCardState card, TileState summoningOn, TargetingEffect targetingEffect, bool handlePlayerInput = true)
        {
            m_selectedBoardTarget = null;
            m_selectionHasBeenCanceled = false;

            if (targetingEffect)
            {
                targetingEffect.OnCancel += OnCancelSelection;
            }

            var targets = card.GetSelectTargets(summoningOn);

            if (targets == null)
            {
                targets = new List<string>();
            }

            // lets hide our existing targets - we don't need them anymore.
            HideTargets();

            SetScreenState(ScreenStateType.HandlingSelection);

            // lets darken the screen again, we kill the animation that is fading the screen
            TweenUtility.Kill(_boardTargetShadowAnimation, false);

            boardTargetShadow.SetActive(true);
            _boardTargetShadowAnimation = boardTargetShadow.DOFade(1f, 0.24f);

            if (targets.Count > 0)
            {
                for (var i = 0; i < targets.Count; i++)
                {
                    BoardTarget target;
                    var targetCard = GameServer.State.GetCard(targets[i]);

                    if (targetCard.characterData.type == CardType.Hero)
                    {
                        switch (targetCard.Team)
                        {
                            case Team.Client:
                                target = heroSideBarContainer.playerHeroView.boardTarget;
                                break;
                            case Team.Opponent:
                                target = heroSideBarContainer.opponentHeroView.boardTarget;
                                break;
                            default: continue;
                        }

                        if (handlePlayerInput)
                        {
                            target.OnSelectedTarget += OnSelectedBoardTarget;
                        }
                        target.Show(BoardTarget.ShowMode.Select);
                        _showingTargets.Add(target);
                        continue;
                    }

                    var getBoardCardTask = GameBoard.GetBoardCard(targets[i]);
                    yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                    var getBoardCardResult = getBoardCardTask.Result;

                    if (getBoardCardResult.boardCard == null || getBoardCardResult.error != ClientErrorCode.None)
                    {
                        Server.HandleClientError(getBoardCardResult, false);
                        continue;
                    }

                    target = getBoardCardResult.boardCard.GetComponent<BoardTarget>();

                    if (target == null)
                    {
                        Debug.Log($"Something went wrong, the target is null {targets[i]}");
                        continue;
                    }

                    if (handlePlayerInput)
                    {
                        target.OnSelectedTarget += OnSelectedBoardTarget;
                    }

                    target.Show(BoardTarget.ShowMode.Select);
                    _showingTargets.Add(target);
                }
            }
            else
            {
                Debug.Log($"{card.data.name} can't target anything!");
            }

            while (!m_selectionHasBeenCanceled && m_selectedBoardTarget == null)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (targetingEffect)
            {
                targetingEffect.OnCancel -= OnCancelSelection;
            }
        }

        public void OnSelectedBoardTarget(BoardTarget boardTarget)
        {
            m_selectedBoardTarget = boardTarget;

            if (m_selectedBoardTarget == null)
            {
                Debug.LogError("We selected a board target but it's null?");
                return;
            }

            // we need to remove the event data that we added to the targets.
            for (var i = 0; i < _showingTargets.Count; i++)
            {
                _showingTargets[i].OnSelectedTarget -= OnSelectedBoardTarget;
            }

            // we remove the selected board target so it doesn't animate and hide, we're going to handle this separately so
            // we can animate a selected effect.
            _showingTargets.Remove(m_selectedBoardTarget);

            // lets hide the targets
            HideTargets();

            // animate the select board target so the player has some feedback from their click.
            m_selectedBoardTarget.AnimateClickAndHide();
        }

        public void SetBoardTarget(BoardTarget boardTarget)
        {
            m_selectedBoardTarget = boardTarget;
        }

        public IEnumerator<float> OpenTurnStartBanner()
        {
            yield return Timing.WaitUntilDone(
                matchTurnBanner.OpenAndCloseTurnBanner(
                    GameServer.GetPlayerState(GameServer.State.CurrentPlayer).GetTeam()));
            yield return Timing.WaitForSeconds(0.25f);
        }

        public IEnumerator<float> CloseTurnStartBanner()
        {
            yield return Timing.WaitForSeconds(0.25f);
        }

        private void OnModifyMana(IMessage message)
        {
            for (var i = 0; i < GetHandCards().Count; i++)
            {
                handView.cards[i].UpdateViewFromState();
            }
        }

        private void OnMinionDestroyed(IMessage message)
        {
            for (var i = 0; i < GetHandCards().Count; i++)
            {
                handView.cards[i].UpdateViewFromState();
            }
        }

        public GameCardView GetHandCard(string uid)
        {
            GameCardView gameCard = null;

            for (var i = 0; i < handView.cards.Count; i++)
            {
                var card = handView.cards[i];
                if (card.State.uid != uid) continue;
                gameCard = card;
                break;
            }

            return gameCard;
        }

        public List<GameCardView> GetHandCards()
        {
            return handView.cards;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="errorCode">
        /// 0: nothings wrong, returned card.
        /// 1: Waiting until the card is available we don't see it yet.
        /// 2: The card is not valid anymore, it's probably in the graveyard.
        /// 3: Unknown error!</param>
        /// <param name="showLog"></param>
        /// <returns></returns>
        public GameCardView GetBoardCard(string uid, out ClientErrorCode errorCode, out CardLocation errorLocation)
        {
            errorCode = ClientErrorCode.None;
            errorLocation = CardLocation.Null;

            var tile = gameBoard.tiles.Find(x => x != null &&
                                                 x.Card != null &&
                                                 x.Card.State != null &&
                                                 x.Card.State.uid == uid);



            if (tile)
            {
                return tile.Card;
            }

            tile = gameBoard.tiles.Find(x => x != null &&
                                             x.CardHistory != null &&
                                             x.CardHistory.Exists( c => c!=null && c.State != null && c.State.uid == uid));

            if (tile)
            {
                //Debug.LogError($"Had to get Last Card for {uid}");
                return tile.CardHistory.Find( c=> c!=null && c.State!=null && c.State.uid == uid);
            }

            var cardState = GameServer.State.GetCard(uid);

            if (cardState != null)
            {
                // we're in the graveyard this is not a valid request (but happens sometimes which is okay!)
                if (cardState.location == CardLocation.Graveyard)
                {
                    errorCode = ClientErrorCode.BoardCardInGraveyard;
                    errorLocation = CardLocation.Graveyard;
                }
                else if (cardState.location != CardLocation.Board)
                {
                    errorCode = ClientErrorCode.BoardCardNotFound;
                    errorLocation = cardState.location;
                }
            }
            else
            {
                errorCode = ClientErrorCode.BoardCardNotFound;
                Debug.LogError($"Card {uid} is not available.");
            }

            return null;
        }

        public List<GameCardView> GetBoardCards()
        {
            return gameBoard.tiles.FindAll(tile => tile != null &&
                                                   tile.Card != null &&
                                                   tile.Card.State != null)
                .ConvertAll(tile => tile.Card);
        }

        public GameTile GetBoardTile(Team team, Vector2Int position)
        {
            if (team == Team.Opponent)
            {
                position.y += 2;
            }

            return GetBoardTile(position);
        }

        public GameTile GetBoardTile(Vector2Int position)
        {
            return gameBoard.tiles.Find(x => x.Position == position);
        }

        public GameTile GetBoardTile(string uid)
        {
            return gameBoard.tiles.Find(x => x.Card != null && x.Card.State != null && x.Card.State.uid == uid);
        }

        public void SetBoardTile(GameCardView card, Team team, Vector2Int position)
        {
            var tile = GetBoardTile(team, position);

            if (tile != null)
            {
                tile.SetCard(card);
            }
            else
            {
                Debug.LogError($"cant find tile at {position} for team {team}");
            }
        }

        public void SetBoardTile(GameCardView card, Vector2Int position)
        {
            var tile = GetBoardTile(position);

            if (tile != null)
            {
                tile.SetCard(card);
            }
        }

        public void RemoveMinionFromBoard(string uid, CardLocation toLocation)
        {
            GameServer.State.Board.RemoveCardFromTile(uid, toLocation);
        }

        public void SetupHandView()
        {
            handView.Setup();
        }

        public Visuals.Deck GetDeck(Team team)
        {
            if (team == GameServer.LocalPlayerTeam)
            {
                return playerDeck;
            }

            if (team == GameServer.GetOpposingTeam())
            {
                return opponentDeck;
            }

            return playerDeck;
        }

        private async void OnSettingsButton()
        {
            if (SceneManager.GetSceneByName("OptionsWindow").isLoaded) return;

            var sceneModel = await SceneController.Instance.LoadScene(new SceneLoadParams
            {
                SceneToLoad = "OptionsWindow"
            });

            var settingsWindow = sceneModel.GetView<SettingsWindow>("SettingsWindow");
            settingsWindow.Initialize(SettingsContext.Game);
            settingsWindow.OnClose += OnSettingsClosed;
        }

        private void OnSettingsClosed()
        {

        }

        // private AddCardToDeckEffect m_localPlayerAddCardEffect;
        // private AddCardToDeckEffect m_remotePlayerAddCardEffect;
        //
        // public IEnumerator<float> AddCardToDeck(string cardUid)
        // {
        //     var targetCard = GameServer.State.GetCard(cardUid);
        //     var moveToLocalDeck = targetCard.Team == GameServer.LocalPlayerTeam;
        //
        //     if (moveToLocalDeck && m_localPlayerAddCardEffect == null || !moveToLocalDeck && m_remotePlayerAddCardEffect == null)
        //     {
        //         var getWorldVfxTask = GameVfxProvider.GetWorldVfx("AddCardToDeckEffect");
        //         yield return Timing.WaitUntilDone(getWorldVfxTask.AsCoroutine());
        //
        //         if (moveToLocalDeck)
        //         {
        //             m_localPlayerAddCardEffect = (AddCardToDeckEffect) getWorldVfxTask.Result;
        //         }
        //         else
        //         {
        //             m_remotePlayerAddCardEffect = (AddCardToDeckEffect) getWorldVfxTask.Result;
        //         }
        //     }
        //
        //     var addCardEffect = moveToLocalDeck ? m_localPlayerAddCardEffect : m_remotePlayerAddCardEffect;
        //     addCardEffect.QueueCard(cardUid);
        // }

        public void Unload()
        {
        }

        [BoxGroup("Debug")]
        public bool disableDebug = true;
        [BoxGroup("Debug")]
        public bool showDebug = true;
        [BoxGroup("Debug")]
        public Rect scrollRectPosition = new Rect(10, 300, 400, 100);
        private Vector2 scrollPosition = Vector2.zero;
        private const float ButtonSize = 32;

        private void OnGUI()
        {
            // GUI.color = Color.black;
            // GUI.Label(new Rect(0,0, 400, 200), ScreenState.ToString());
            // GUI.Label(new Rect(0,16, 400, 200), "AI.Thinking: " + AI.Thinking);
            // GUI.Label(new Rect(0,32, 400, 200), "State " + StateController.CurrentState);

            if (disableDebug)
            {
                return;
            }

            if (showDebug)
            {

                GUI.Box(scrollRectPosition, string.Empty);
                GUI.Box(scrollRectPosition, string.Empty);
                GUI.Box(scrollRectPosition, string.Empty);

                const float textSize = 12;
                const float lineHeight = 64;
                var scrollHeight = lineHeight * GameServer.State.HistoryDisplays.Count;

                scrollPosition = GUI.BeginScrollView(scrollRectPosition, scrollPosition,
                    new Rect(0, 0, scrollRectPosition.width, scrollHeight));
                {
                    for (var i = 0; i < GameServer.State.HistoryDisplays.Count; i++)
                    {
                        var history = GameServer.State.HistoryDisplays[i];
                        var historyRect = new Rect(0, textSize * i, scrollRectPosition.width, lineHeight);
                        GUI.Label(historyRect, history);
                    }
                }
                GUI.EndScrollView(true);

                if (GUI.Button(new Rect(Screen.width - ButtonSize, Screen.height - ButtonSize, ButtonSize, ButtonSize),
                    "x"))
                {
                    showDebug = false;
                }
            }
            else
            {

                if (GUI.Button(new Rect(Screen.width - ButtonSize, Screen.height - ButtonSize, ButtonSize, ButtonSize),
                    "^"))
                {
                    showDebug = true;
                }
            }
        }
    }
}