﻿using System;
using CrossBlitz.ClientAPI.GameLogic.Data;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using GameDataEditor;
using Source.Scripts.Server.GameLogic.Data;
using UnityEngine;

namespace CrossBlitz.ClientAPI
{
    // public static class Client
    // {
    //     public static PlayerBattleState ClientState;
    //     public static PlayerBattleState OpponentState;
    //
    //
    //     /// <summary>
    //     /// This will be filled in with a card that the player chooses
    //     /// when selecting targets for a spell or anything else.
    //     /// </summary>
    //     public static string TargetCardId;
    //
    //     public static void OnCreatedGame()
    //     {
    //         CreateClientPlayerState(0);
    //         GameServer.SetHero(ServerPlayerInfoIndex, ClientState.hero);
    //     }
    //
    //     /// <summary>
    //     /// Generates the interal info only shown to this client
    //     /// Pass in the index of which player you are, if you're generating the game
    //     /// this will be 0, if you receive a game this will be 1
    //     /// </summary>
    //     /// <param name="index"></param>
    //     private static void CreateClientPlayerState(int index)
    //     {
    //         ServerPlayerInfoIndex = index;
    //         ClientState = new PlayerBattleState();
    //
    //         var currentDeck = App.PlayerDecks.GetCurrentDeck();
    //
    //         if (currentDeck == null)
    //         {
    //             Debug.LogError("The current Deck is null!");
    //         }
    //
    //         ClientState.GenerateCards(App.AccountInfo.PlayerGuid, App.PlayerDecks.GetCurrentDeck());
    //     }
    //
    //     public static void CreateLocalAiPlayer(string id)
    //     {
    //         OpponentState = new PlayerBattleState();
    //
    //         var deck = App.PlayerDecks.GetDeckWithHero(GDEItemKeys.Hero_Quill);
    //
    //         OpponentState.GenerateCards(id, deck);
    //
    //         GameServer.SetHero(1, OpponentState.hero);
    //     }
    //
    //     /// <summary>
    //     /// We call this after we join our opponents room
    //     /// This is only called if we did not create the game.
    //     /// </summary>
    //     /// <param name="state"></param>
    //     public static void OnJoinedMatch(GameState state)
    //     {
    //         CreateClientPlayerState(1);
    //         GameServer.SetHero(ServerPlayerInfoIndex, ClientState.hero);
    //     }
    //
    //     /// <summary>
    //     /// This is called when the opponent joins our room and sets up his/her data
    //     /// We need to put all of their cards in the lookup table like they did for us
    //     /// </summary>
    //     public static void OnOpponentJoined()
    //     {
    //
    //     }
    // }
}
