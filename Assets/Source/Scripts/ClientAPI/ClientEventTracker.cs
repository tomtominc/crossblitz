using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Hero;
using CrossBlitz.PlayFab.Authentication;
using TakoBoyStudios.Events;

namespace CrossBlitz.ClientAPI
{
    public class ClientEventTracker
    {
        public void StartListening()
        {
            Events.Subscribe(EventType.OnHeroChanged, OnHeroChanged);
        }

        public void StopListening()
        {
            Events.Unsubscribe(EventType.OnHeroChanged, OnHeroChanged);
        }

        private void OnHeroChanged(IMessage message)
        {
            if (message.Data is HeroData heroData)
            {
                App.HeroData.SetCurrentHero(heroData.id);
            }
        }
    }
}