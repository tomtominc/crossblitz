using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.GameLogic.States;
using CrossBlitz.ClientAPI.States;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Data;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.Utils;
using TakoBoyStudios.Events;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ClientAPI
{
    public class BattleController : MonoBehaviour
    {
        private void Awake()
        {
            Events.Subscribe(EventType.StartBattle, StartBattle);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.StartBattle, StartBattle);
        }

        private void StartBattle(IMessage message)
        {
            if (message.Data is StartBattleEventArgs args)
            {
                StateController.ChangeState(StateDefinition.GAME_MATCHMAKING);
                var matchmaking = StateController.GetState<GameStateMatchmaking>(StateDefinition.GAME_MATCHMAKING);
                matchmaking.StartGame(args.gameMode);
            }
        }

        public static void OnBattleSkipped(BattleData battle, Faction playedAs, FableChapterData chapter, int tileUid, bool firstTime)
        {
            var dawnDollars = 0;
            var manaShards = battle.GetManaShardsFromWin(firstTime);
            var exp = Xp.GetXpGainedFromEnemy(battle.BattleType, battle.Level, firstTime, chapter.ChapterNumber);
            var cards = battle.GetCardRewardsForWin(playedAs,chapter.Uid, firstTime);
            var ingredients = battle.GetIngredientsFromWin(playedAs, firstTime);
            var clientTileData = App.FableData.GetTile(tileUid);

            clientTileData.BattleData.FixDependencies(battle);
            clientTileData.BattleData.SetCompleteStatus(true);

            for (var i = 0; i < battle.Accolades.Count; i++)
            {
                var accolade = battle.Accolades[i];

                if (!clientTileData.BattleData.Accolades[i].IsComplete)
                {
                    dawnDollars += accolade.DawnDollarReward;
                    clientTileData.BattleData.Accolades[i].IsComplete = true;
                }
            }

            if (!App.Settings.GetNoBattleRewards())
            {
                //App.Inventory.AddCurrency(Currency.MANA_SHARDS, manaShards);
                //App.FableData.SetDawnDollarAmount(App.FableData.GetDawnDollarAmount() + dawnDollars);

                var currentBook = App.FableData.GetCurrentBook();

                if (currentBook != null)
                {
                    currentBook.AddDawnDollars(dawnDollars);
                }

                if (exp > 0)
                {
                    var hero = App.HeroData.GetHero(Db.HeroDatabase.GetHeroByFaction(playedAs).id);

                    if (hero != null)
                    {
                        App.HeroData.IncreaseXp(hero.id, exp);

                        hero = App.HeroData.GetHero(hero.id);

                        var levelsGained = Xp.GetGainedLevelsFromXp(hero.level, hero.xp, out var xpLeft);

                        App.HeroData.SetXp(hero.id, xpLeft);

                        HeroDatabase.ModifyHeroLevel(hero.id, hero.level + levelsGained);
                    }
                }

                var bookData = App.FableData.GetCurrentBook();

                if (bookData != null)
                {
                    for (var i = 0; i < cards.Count; i++)
                    {
                        bookData.AddRecipe(cards[i]);
                    }
                }

                if (!App.Settings.GetDisplayRewardsWhenSkippingBattles())
                {
                    App.Inventory.GrantItems(ingredients);
                    App.Inventory.AddCurrency(Currency.MANA_SHARDS, manaShards);
                }
                else
                {
                    //ClientInventory.GrantAndDisplayItems(cards);
                    ClientInventory.GrantAndDisplayItems(ingredients, new List<ItemValue> { new ItemValue
                {
                    ItemUid = Currency.MANA_SHARDS,
                    Count = manaShards
                }});
                }
            }
        }
    }
}