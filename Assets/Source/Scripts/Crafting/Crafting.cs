using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.PlayFab.Authentication;
using GameDataEditor;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz
{
    [Serializable]
    public class IngredientData
    {
        public int Amount;
        public string Ingredient;
    }

    public static class Crafting
    {
        public static bool Initialized;
        public const int ManaShardsPerChest = 5;
        public const float ScrapManaShardsExchange = 0.5f;
        public const float ScrapIngredientExchange = 0.5f;
        public static Dictionary<Faction, List<GDEItemData>> Ingredients;

        public static void Init()
        {
            Ingredients = new Dictionary<Faction, List<GDEItemData>>();

            var gdeItems = GDEDataManager.GetAllItems<GDEItemData>();

            for (var i = 0; i < gdeItems.Count; i++)
            {
                var gdeItem = gdeItems[i];
                if (gdeItem.ItemType != "Ingredient") continue;
                var faction = GameUtilities.ParseEnum(gdeItem.Faction, Faction.All);
                if (Ingredients.ContainsKey(faction))
                {
                    Ingredients[faction].Add(gdeItem);
                }
                else
                {
                    Ingredients.Add( faction, new List<GDEItemData>{gdeItem});
                }
            }

            Initialized = true;
        }

        public static int GetLevelRewardIngredientAmountForIngredient(string ingredientId)
        {
            var ingredient = new GDEItemData(ingredientId);

            switch (ingredient.Rarity)
            {
                case "Common": return 4;
                case "Rare": return 2;
                case "Mythic": return 2;
                case "Legendary": return 1;
            }

            return 0;
        }

        public static Faction GetItemFaction(string itemId)
        {
            if (Ingredients ==null)
            {
                Init();
            }

            var faction = Faction.Neutral;

            foreach (var itemFactionList in Ingredients)
            {
                for (var i = 0; i < itemFactionList.Value.Count; i++)
                {
                    var item = itemFactionList.Value[i];
                    if (item.Key == itemId)
                    {
                        faction = itemFactionList.Key;
                        return faction;
                    }
                }
            }

            return faction;
        }

        public static int GetCraftingCost(Rarity rarity)
        {
            switch (rarity)
            {
                case Rarity.Common:
                    return 1;
                case Rarity.Rare:
                    return 2;
                case Rarity.Mythic:
                    return 4;
                case Rarity.Legendary:
                    return 8;
            }

            return 0;
        }

        public static int GetIngredientAmountForAChest(string ingId)
        {
            return 4;
        }

        public static bool IsCraftable(CardData card)
        {
            // if (!card.craftingData.craftable)
            // {
            //     return false;
            // }
            //
            // if (card.locations.Exists(l =>
            //         l.location == CardLocationData.Location.Shop ||
            //         l.location == CardLocationData.Location.Cutscene ||
            //         l.location == CardLocationData.Location.Chest ||
            //         l.location == CardLocationData.Location.LevelReward))
            // {
            //     return false;
            // }

            return true;
        }

        public static bool CanCraftCard(CardData card, out string errorMessage)
        {
            var canCraft = true;
            errorMessage = string.Empty;

            if (!IsCraftable(card))
            {
                errorMessage = "This card is uncraftable.";
                return false;
            }

            var cardAmount = App.Inventory.GetItemAmount(card.id);

            switch (card.rarity)
            {
                case Rarity.Common:
                    if (cardAmount >= DeckData.MaxCopiesOfCommonCards)
                    {
                        canCraft = false;
                        errorMessage = "You can't meld anymore of that card.";
                    }
                    break;
                case Rarity.Rare:
                    if (cardAmount >= DeckData.MaxCopiesOfRareCards)
                    {
                        canCraft = false;
                        errorMessage = "You can't meld anymore of that card.";
                    }
                    break;
                case Rarity.Mythic:
                    if (cardAmount >= DeckData.MaxCopiesOfMysticCards)
                    {
                        canCraft = false;
                        errorMessage = "You can't meld anymore of that card.";
                    }
                    break;
                case Rarity.Legendary:
                    if (cardAmount >= DeckData.MaxCopiesOfLegendaryCards)
                    {
                        canCraft = false;
                        errorMessage = "You can't meld anymore of that card.";
                    }
                    break;
            }

            if (!canCraft)
            {
                return false;
            }

            for (var i = 0; i < card.craftingData.ingredients.Count; i++)
            {
                var itemAmount = App.Inventory.GetItemAmount(card.craftingData.ingredients[i].ItemUid);

                if (card.craftingData.ingredients[i].Count > itemAmount)
                {
                    canCraft = false;
                    errorMessage = "You need more resources.";
                    break;
                }
            }

            if (App.Inventory.GetCurrencyAmount(Currency.MANA_SHARDS) < card.craftingData.cost)
            {
                canCraft = false;
                errorMessage = "You need more resources.";
            }

            return canCraft;
        }

        public static bool CanCraftCardCustomInventory(CardData card, Dictionary<string,ItemValue> inventory, out Dictionary<string,ItemValue> itemsUsed)
        {
            var canCraft = true;

            itemsUsed = new Dictionary<string, ItemValue>();

            if (!card.craftingData.craftable)
            {
                return false;
            }

            // if (card.locations.Exists(l =>
            //         l.location == CardLocationData.Location.Shop ||
            //         l.location == CardLocationData.Location.Cutscene ||
            //         l.location == CardLocationData.Location.Chest ||
            //         l.location == CardLocationData.Location.LevelReward))
            // {
            //     return false;
            // }

            for (var i = 0; i < card.craftingData.ingredients.Count; i++)
            {
                var item = card.craftingData.ingredients[i];

                if (!inventory.ContainsKey(item.ItemUid))
                {
                    continue;
                }

                var itemAmount = inventory[item.ItemUid].Count;

                if (item.Count > itemAmount)
                {
                    canCraft = false;
                    break;
                }

                if (itemsUsed.ContainsKey(item.ItemUid))
                {
                    itemsUsed[item.ItemUid].Count += item.Count;
                }
                else
                {
                    itemsUsed.Add(item.ItemUid, new ItemValue { ItemUid = item.ItemUid, Count = item.Count });
                }
            }

            if (inventory[Currency.MANA_SHARDS].Count < card.craftingData.cost)
            {
                canCraft = false;
            }
            else
            {
                itemsUsed.Add(Currency.MANA_SHARDS, new ItemValue { ItemUid = Currency.MANA_SHARDS, Count = card.craftingData.cost });
            }

            return canCraft;
        }

        public static bool CanScrapCard(CardData card)
        {
            return HasExtraForScrapping(card);
        }

        public static bool HasExtraForScrapping(CardData card)
        {
            var maxCount = 0;
            switch (card.rarity)
            {
                case Rarity.Common: maxCount = DeckData.MaxCopiesOfCommonCards;
                    break;
                case Rarity.Rare: maxCount = DeckData.MaxCopiesOfRareCards;
                    break;
                case Rarity.Mythic: maxCount = DeckData.MaxCopiesOfMysticCards;
                    break;
                case Rarity.Legendary: maxCount = DeckData.MaxCopiesOfLegendaryCards;
                    break;
            }
            return App.Inventory.GetItemAmount(card.id) > maxCount;
        }

        public static List<CardData> GetAllExtraScrapCards()
        {
            var results = new List<CardData>();

            for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
            {
                var card = Db.CardDatabase.Cards[i];
                if (card.set == CardSet.Token || card.set == CardSet.Deprecated) continue;

                if (HasExtraForScrapping(card))
                {
                    results.Add(card);
                }
            }

            return results;
        }

        public static List<string> GetIngredientNamesByFaction(Faction faction)
        {
            if (!Initialized)
            {
                Init();
            }

            return Ingredients[faction].Select(x => x.Key).ToList();
        }

        public static List<GDEItemData> GetIngredientsByFaction(Faction faction)
        {
            if (!Initialized)
            {
                Init();
            }

            return Ingredients[faction].Select(x => x).ToList();
        }

        public static string GetIngredient(string rarity, Faction faction)
        {
            if (!Initialized)
            {
                Init();
            }

            var ingredient = Ingredients[faction].Find(x => x.Rarity == rarity);
            return ingredient?.Key;
        }

        public static List<ItemValue> GetIngredientsForCard(CardData card)
        {
            if (!Initialized)
            {
                Init();
            }

            var rarity = card.rarity;
            var faction = card.faction2 == Faction.All || card.faction2 == Faction.None ? card.faction : card.faction2;

            if (card.faction == Faction.All || card.faction == Faction.None)
            {
                return new List<ItemValue>();
            }

            if (card.type == CardType.Elder_Relic || card.type == CardType.Relic)
            {
                return new List<ItemValue>();
            }

            const int Multiplier = 1;

            if (faction != Faction.Neutral)
            {
                switch (rarity)
                {
                    case Rarity.Common:
                        return new List<ItemValue>
                        {
                            new ItemValue
                            {
                                Count = 2 * Multiplier,
                                ItemUid = Ingredients?[faction][0].Key
                            },
                            new ItemValue
                            {
                                Count = 2 * Multiplier,
                                ItemUid = Ingredients?[Faction.Neutral][0].Key
                            },
                        };
                    case Rarity.Rare:
                        return new List<ItemValue>
                        {
                            new ItemValue
                            {
                                Count = 4 * Multiplier,
                                ItemUid = Ingredients?[faction][0].Key
                            },
                            new ItemValue
                            {
                                Count = 2 * Multiplier,
                                ItemUid = Ingredients?[Faction.Neutral][0].Key
                            },
                            new ItemValue
                            {
                                Count = 2 * Multiplier,
                                ItemUid = Ingredients?[Faction.Neutral][3].Key
                            }
                        };
                    case Rarity.Mythic:
                        return new List<ItemValue>
                        {
                            new ItemValue
                            {
                                Count = 4 * Multiplier,
                                ItemUid = Ingredients?[faction][1].Key
                            },
                            new ItemValue
                            {
                                Count = 4 * Multiplier,
                                ItemUid = Ingredients?[faction][2].Key
                            },
                            new ItemValue
                            {
                                Count = 2 * Multiplier,
                                ItemUid = Ingredients?[Faction.Neutral][3].Key
                            }
                        };
                    case Rarity.Legendary:
                        return new List<ItemValue>
                        {
                            new ItemValue
                            {
                                Count = 8 * Multiplier,
                                ItemUid = Ingredients?[faction][1].Key
                            },
                            new ItemValue
                            {
                                Count = 8 * Multiplier,
                                ItemUid = Ingredients?[faction][2].Key
                            },
                            new ItemValue
                            {
                                Count = 8 * Multiplier,
                                ItemUid = Ingredients?[Faction.Neutral][3].Key
                            }
                        };
                }
            }

            switch (rarity)
            {
                case Rarity.Common:
                        return new List<ItemValue>
                        {
                            new ItemValue
                            {
                                Count = 2 * Multiplier,
                                ItemUid = Ingredients?[Faction.Neutral][0].Key
                            },
                            new ItemValue
                            {
                                Count = 2 * Multiplier,
                                ItemUid = Ingredients?[Faction.Neutral][3].Key
                            },
                        };
                    case Rarity.Rare:
                        return new List<ItemValue>
                        {
                            new ItemValue
                            {
                                Count = 2 * Multiplier,
                                ItemUid = Ingredients?[Faction.Neutral][0].Key
                            },
                            new ItemValue
                            {
                                Count = 4 * Multiplier,
                                ItemUid = Ingredients?[Faction.Neutral][3].Key
                            }
                        };
                    case Rarity.Mythic:
                        return new List<ItemValue>
                        {
                            new ItemValue
                            {
                                Count = 4 * Multiplier,
                                ItemUid = Ingredients?[Faction.Neutral][0].Key
                            },
                            new ItemValue
                            {
                                Count = 4 * Multiplier,
                                ItemUid = Ingredients?[Faction.Neutral][3].Key
                            }
                        };
                    case Rarity.Legendary:
                        return new List<ItemValue>
                        {
                            new ItemValue
                            {
                                Count = 8 * Multiplier,
                                ItemUid = Ingredients?[Faction.Neutral][0].Key
                            },
                            new ItemValue
                            {
                                Count = 8 * Multiplier,
                                ItemUid = Ingredients?[Faction.Neutral][3].Key
                            }
                        };
            }

            return null;
        }

    }
}