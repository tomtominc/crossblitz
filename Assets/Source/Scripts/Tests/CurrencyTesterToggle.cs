using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Tests
{
    public class CurrencyTesterToggle : MonoBehaviour
    {
        public Toggle toggle;
        public TextMeshProUGUI label;
    }
}