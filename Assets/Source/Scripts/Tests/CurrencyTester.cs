using System;
using System.Collections.Generic;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.PlayFab.Authentication;
using PlayFab;
using TakoBoyStudios.Events;
using TMPro;
using UnityAsync;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Tests
{
    public class CurrencyTester : MonoBehaviour
    {
        public TextMeshProUGUI networkStatusLabel;
        public TextMeshProUGUI currencyTotals;
        public TMP_InputField amountInput;
        public Button addButton;
        public Button subtractButton;
        public RectTransform togglesLayout;
        public CurrencyTesterToggle togglePrefab;

        private List<CurrencyTesterToggle> _toggles;

        private async void Start()
        {
            InternetReachabilityVerifier.Instance.statusChangedDelegate += OnNetworkStatusChanged;

            networkStatusLabel.text = $"Network Status: {InternetReachabilityVerifier.Instance.status}";
            addButton.SetActive(false);
            subtractButton.SetActive(false);

            while (!PlayFabClientAPI.IsClientLoggedIn()) await Await.NextUpdate();

            addButton.SetActive(true);
            subtractButton.SetActive(true);

            Events.Subscribe(EventType.OnModifiedCurrency, OnModifyCurrency);

            OnModifyCurrency(null);

            _toggles = new List<CurrencyTesterToggle>();

            foreach (var currency in App.Inventory.GetCurrency())
            {
                var toggle = Instantiate(togglePrefab, togglesLayout, false);
                toggle.label.text = currency.Key;
                toggle.toggle.isOn = false;
                _toggles.Add(toggle);
            }

            addButton.onClick.AddListener(OnAddButton);
            subtractButton.onClick.AddListener(OnSubtractButton);

        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnModifiedCurrency, OnModifyCurrency);
        }

        private void OnAddButton()
        {
            if (!int.TryParse(amountInput.text, out var amount)) return;

            for (var i = 0; i < _toggles.Count; i++)
            {
                if (_toggles[i].toggle.isOn)
                {
                    App.Inventory.AddCurrency(_toggles[i].label.text, amount);
                }
            }
        }

        private void OnSubtractButton()
        {
            if (!int.TryParse(amountInput.text, out var amount)) return;

            for (var i = 0; i < _toggles.Count; i++)
            {
                if (_toggles[i].toggle.isOn)
                {
                    App.Inventory.SubtractCurrency(_toggles[i].label.text, amount);
                }
            }
        }

        private void OnModifyCurrency(IMessage message)
        {
            currencyTotals.text = string.Empty;

            foreach (var currency in App.Inventory.GetCurrency())
            {
                currencyTotals.text += $"{currency.Key} Total: {currency.Value}\n";
            }
        }

        private void OnNetworkStatusChanged(InternetReachabilityVerifier.Status status)
        {
            networkStatusLabel.text = $"Network Status: {status}";
        }
    }
}