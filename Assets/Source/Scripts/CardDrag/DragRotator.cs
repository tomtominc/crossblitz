﻿using System.Collections;
using System.Collections.Generic;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Card;
using Source.Scripts.Card;
using UnityEngine;

public class DragRotator : MonoBehaviour, ICardComponent
{
    private const float EPSILON = 0.0001f;
    private const float SMOOTH_DAMP_SEC_FUDGE = 0.1f;
    private DragRotatorInfo m_info;
    private float m_pitchDeg;
    private float m_rollDeg;
    private float m_pitchVel;
    private float m_rollVel;
    private Vector3 m_prevPos;
    private Vector3 m_originalAngles;

    public bool followMouse;
    public bool rotationEnabled =true;
    private Camera cam;
    private Vector3 offsetEuler;

    public void SetCamera(Camera c)
    {
        cam = c;
    }

    public void Initialize(CardView card)
    {
        Reset();
        m_prevPos = transform.position;
        m_originalAngles = transform.localRotation.eulerAngles;
        CreateDragRotatorSettings();
    }

    public void OnAfterCreation()
    {

    }

    private void CreateDragRotatorSettings()
    {
        m_info = new DragRotatorInfo();
        m_info.m_PitchInfo = new DragRotatorAxisInfo
            {m_ForceMultiplier = 4, m_MinDegrees = 20, m_MaxDegrees = 40, m_RestSeconds = 2};
        m_info.m_RollInfo = new DragRotatorAxisInfo
            {m_ForceMultiplier = 4, m_MinDegrees = 20, m_MaxDegrees = 40, m_RestSeconds = 2};
    }

    public void SetOffsetEuler(Vector3 o)
    {
        offsetEuler = o;
    }

    private void Update()
    {
        if (m_info ==null)
        {
            CreateDragRotatorSettings();
        }

        if (followMouse)
        {
            if (cam == null)
            {
                cam =
                    SceneModel.GetCurrentSceneModel() != null &&
                    SceneModel.GetCurrentSceneModel().sceneCamera != null
                        ? SceneModel.GetCurrentSceneModel().sceneCamera
                        : Camera.main;
            }

            if (cam != null)
            {
                if (cam.orthographic)
                {
                    this.transform.position =
                        (Vector2) cam.ScreenToWorldPoint(Input.mousePosition);
                }
                else
                {
                    transform.position = GetWorldPositionOnPlane(cam, Input.mousePosition, 0);
                }
            }
        }

        Vector3 position = this.transform.position;
        Vector3 direction = position - this.m_prevPos; //this.m_prevPos - position;

        if (rotationEnabled)
        {
            m_pitchDeg -= direction.y * m_info.m_PitchInfo.m_ForceMultiplier;
            m_rollDeg += direction.x * m_info.m_RollInfo.m_ForceMultiplier;

            m_pitchDeg = Mathf.SmoothDamp(m_pitchDeg, 0.0f, ref m_pitchVel, m_info.m_PitchInfo.m_RestSeconds * 0.1f);
            m_rollDeg = Mathf.SmoothDamp(m_rollDeg, 0.0f, ref m_rollVel, m_info.m_RollInfo.m_RestSeconds * 0.1f);
            transform.localRotation = Quaternion.Euler(m_originalAngles.x + m_pitchDeg, m_originalAngles.y + m_rollDeg,
                m_originalAngles.z);
            transform.localEulerAngles += offsetEuler;
        }

        m_prevPos = position;
    }

    public Vector3 GetWorldPosition()
    {
        var mousePos = Input.mousePosition;
        mousePos.z = cam.nearClipPlane;
        return  cam.ScreenToWorldPoint(mousePos);
    }

    public static Vector3 GetWorldPositionOnPlane(Camera cam, Vector3 screenPosition, float z)
    {
        Ray ray = cam.ScreenPointToRay(screenPosition);
        Plane xy = new Plane(Vector3.forward, new Vector3(0, 0, z));
        xy.Raycast(ray, out var distance);
        return ray.GetPoint(distance);
    }

    public DragRotatorInfo GetInfo()
    {
        return m_info;
    }

    public void SetInfo(DragRotatorInfo info)
    {
        m_info = info;
    }

    public void Reset()
    {
        m_prevPos = transform.position;
        transform.localRotation = Quaternion.Euler(this.m_originalAngles);
        m_rollDeg = 0.0f;
        m_rollVel = 0.0f;
        m_pitchDeg = 0.0f;
        m_pitchVel = 0.0f;
    }

    public void ResetAndTeleportToMouse()
    {
        if (cam == null)
        {
            cam =
                SceneModel.GetCurrentSceneModel() != null &&
                SceneModel.GetCurrentSceneModel().sceneCamera != null
                    ? SceneModel.GetCurrentSceneModel().sceneCamera
                    : Camera.main;

            if (cam == null) return;
        }

        transform.position = (Vector2) cam.ScreenToWorldPoint(Input.mousePosition);
        Reset();
    }
}