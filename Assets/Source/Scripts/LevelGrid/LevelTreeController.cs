using System;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Databases;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CrossBlitz.LevelGrid
{
    public static class LevelTreeController
    {
        public static event Action<LevelGridMenu> OnOpened;
        public static event Action OnClosed;
        public static async void Open()
        {
            await SceneController.Instance.LoadScene(new SceneLoadParams
            {
                SceneToLoad = "LevelGrid",
                LoadSceneMode = LoadSceneMode.Additive
            });

            var sceneModel = SceneController.GetScene("LevelGrid");
            var levelGridMenu = sceneModel.GetView<LevelGridMenu>("LevelGridMenu");
            levelGridMenu.Open();


            OnOpened?.Invoke(levelGridMenu);
        }

        public static void ResetAllLevelPoints()
        {

        }

        public static async void Open(string heroId)
        {
            if (!Db.HeroDatabase.IsHero(heroId))
            {
                Open();
                return;
            }

            await SceneController.Instance.LoadScene(new SceneLoadParams
            {
                SceneToLoad = "LevelGrid",
                LoadSceneMode = LoadSceneMode.Additive
            });

            var sceneModel = SceneController.GetScene("LevelGrid");
            var levelGridMenu = sceneModel.GetView<LevelGridMenu>("LevelGridMenu");
            levelGridMenu.Open(heroId);

            OnOpened?.Invoke(levelGridMenu);
        }

        public static async void Close()
        {
            OnClosed?.Invoke();
            await SceneController.Instance.UnloadScene("LevelGrid");
        }
    }
}