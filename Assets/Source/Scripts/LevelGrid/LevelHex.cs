using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Hero;
using CrossBlitz.PlayFab.Data;
using MEC;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using UnityEngine.EventSystems;
using DG.Tweening;
using CrossBlitz.Utils;

namespace CrossBlitz.LevelGrid
{
    public class LevelHex : MonoBehaviour,
        IPointerEnterHandler,
        IPointerExitHandler,
        ICursorSelectable
    {
        public enum State
        {
            None,
            Locked,
            LevelUp,
            Collected
        }

        public State m_currentState;
        public SpriteAnimation background;
        public SpriteAnimation outline;
        public SpriteAnimation path;
        public SpriteAnimation pendingRewardIcon;
        public SpriteAnimation hoverOutline;
        public SpriteAnimation readyOverlay;

        public RectTransform hoverCardParent;

        [BoxGroup("Ready To Level Up")] public GameObject readyToLevelUpParent;
        [BoxGroup("Ready To Level Up")] public SpriteAnimation transitionToReadyToLevelUpEffect;
        [BoxGroup("Ready To Level Up")] public SpriteAnimation readyToLevelUpSparkles;

        [BoxGroup("Complete Hex")] public GameObject completeHexParent;
        [BoxGroup("Complete Hex")] public SpriteAnimation transitionToCompleteBurstEffect;
        [BoxGroup("Complete Hex")] public SpriteAnimation transitionToCompleteShineEffect;
        [BoxGroup("Complete Hex")] public CanvasGroup transitionToCompleteShineEffectCanvas;
        [BoxGroup("Complete Hex")] public SpriteAnimation transitionToCompleteSparkleEffect;
        [BoxGroup("Complete Hex")] public SpriteAnimation prizeIcon;

        [BoxGroup("Interactions")] public Button levelUpButton;

        [NonSerialized] public string m_heroId;
        [NonSerialized] public int m_rewardUid;
        [NonSerialized] public string m_faction;

        public event Action<LevelHex> OnLevelUp;
        public event Action<LevelHex> OnHoverEnter;
        public event Action<LevelHex> OnHoverExit;

        private float timer;
        private int m_frameSkipsUntilSpawnHoverCard;
        private HoverCard m_hoverCard;
        private CanvasGroup readyOverlayCanvas;


        public bool Interactable => levelUpButton.interactable;
        public bool Grabbable => false;
        public bool InfoOnly => !levelUpButton.interactable;
        public bool Loading => false;

        private void Start()
        {
            levelUpButton.onClick.AddListener(LevelUp);
            timer = Random.Range(1f, 20f);
            m_frameSkipsUntilSpawnHoverCard = 5;
        }

        private void LevelUp()
        {
            OnLevelUp?.Invoke(this);
        }

        private void Update()
        {
            // Sparkle complete nodes at random
            if(m_currentState == State.Collected)
            {
                timer -= Time.deltaTime;

                if( timer <= 0)
                {
                    timer = Random.Range(1f, 20f);
                    transitionToCompleteSparkleEffect.SetActive(true);
                    transitionToCompleteSparkleEffect.Play("level-up-end-sparkles", () =>
                    {
                        transitionToCompleteSparkleEffect.SetActive(false);
                    });
                }
            }

            // Overlay the color over the ready to level up node
            if (m_currentState == State.LevelUp)
            {
                readyOverlayCanvas.alpha = outline.CurrentFrame switch
                {
                    0 => 0f,
                    1 => .07f,
                    2 => .13f,
                    3 => .25f,
                    4 => .30f,
                    5 => .25f,
                    6 => .13f,
                    7 => .07f,
                    _ => 0f,
                };
            }
        }

        private string GetPendingPrizeIcon(LevelReward.EType rewardType)
        {
            switch (rewardType)
            {
                case LevelReward.EType.Card: return "card";
                case LevelReward.EType.HeathUp: return "HP";
                case LevelReward.EType.RelicSlot: return "relic-slot";
            }

            return string.Empty;
        }

        public void SetHexState(string heroId, LevelReward reward, State state, Faction faction)
        {
            m_heroId = heroId;
            m_rewardUid = reward.uid;
            m_currentState = state;
            m_faction = faction.ToString().ToLower();

            var cardData = CardData.FromLevelReward(reward);
            m_hoverCard = gameObject.AddComponent<HoverCard>();
            m_hoverCard.Initialize(cardData, hoverCardParent, 0);
            m_hoverCard.UsesHoverPunchEffect = false;
            m_hoverCard.UsesScalingPunchEffect = true;
            m_hoverCard.ForceToolTipsOpposite = true;

            readyOverlayCanvas = readyOverlay.GetComponent<CanvasGroup>();

            switch (m_currentState)
            {
                case State.Locked:
                    levelUpButton.interactable = false;
                    background.SetActive(true);
                    background.Play($"{m_faction}-inactive");
                    pendingRewardIcon.SetActive(true);
                    pendingRewardIcon.Play($"{GetPendingPrizeIcon(reward.type)}-{m_faction}");
                    outline.SetActive(false);
                    readyOverlay.SetActive(false);
                    readyToLevelUpParent.SetActive(false);
                    completeHexParent.SetActive(false);
                    path.Play($"vertical-0-{m_faction}");
                    break;
                case State.LevelUp:
                    levelUpButton.interactable = true;
                    background.SetActive(true);
                    background.Play($"{m_faction}-inactive");
                    pendingRewardIcon.SetActive(true);
                    pendingRewardIcon.Play($"{GetPendingPrizeIcon(reward.type)}-{m_faction}");
                    outline.SetActive(false);
                    readyOverlay.SetActive(false);
                    readyToLevelUpParent.SetActive(true);
                    completeHexParent.SetActive(false);
                    path.Play($"vertical-0-{m_faction}");
                    Timing.RunCoroutine(TransitionToNextLevelUp().CancelWith(gameObject));
                    break;
                case State.Collected:
                    levelUpButton.interactable = false;
                    background.SetActive(true);
                    background.Play($"{m_faction}-active");
                    pendingRewardIcon.SetActive(false);
                    outline.SetActive(true);
                    outline.Play(m_faction);
                    readyOverlay.SetActive(true);
                    readyOverlay.Play(m_faction);
                    readyToLevelUpParent.SetActive(true);
                    completeHexParent.SetActive(true);
                    path.Play($"vertical-2-{m_faction}");
                    Timing.RunCoroutine(TransitionToComplete(reward,faction).CancelWith(gameObject));
                    break;
            }
        }

        private IEnumerator<float> TransitionToNextLevelUp()
        {
            transitionToReadyToLevelUpEffect.SetActive(true);
            transitionToReadyToLevelUpEffect.Play("transition");

            while (transitionToReadyToLevelUpEffect.CurrentFrame < 4) yield return Timing.WaitForOneFrame;

            outline.SetActive(true);
            outline.Play(m_faction);

            readyOverlay.SetActive(true);
            readyOverlay.Play(m_faction);

            while (!transitionToReadyToLevelUpEffect.IsDone) yield return Timing.WaitForOneFrame;

            transitionToReadyToLevelUpEffect.SetActive(false);
            readyToLevelUpSparkles.SetActive(true);
            readyToLevelUpSparkles.Play("level-up-end-sparkles");
        }

        private IEnumerator<float> TransitionToComplete(LevelReward reward,Faction faction)
        {
            path.Play($"vertical-2-{m_faction}");

            transitionToCompleteBurstEffect.SetActive(true);
            transitionToCompleteBurstEffect.Play("level-up-burst");

            while (transitionToCompleteBurstEffect.CurrentFrame < 10)
            {
                yield return Timing.WaitForOneFrame;
            }

            background.Play($"{m_faction}-active");

            outline.SetActive(false);
            readyOverlay.SetActive(false);
            readyToLevelUpParent.SetActive(false);
            prizeIcon.SetActive(true);

            var prizeIconAnimation = string.Empty;

            switch (reward.type)
            {
                case LevelReward.EType.HeathUp:
                    prizeIconAnimation = "HP";
                    break;
                case LevelReward.EType.IngredientPouch:
                    prizeIconAnimation = "ingredients";
                    break;
                case LevelReward.EType.ManaShards:
                    prizeIconAnimation = "mana-shards";
                    break;
                case LevelReward.EType.RelicSlot:
                    prizeIconAnimation = "relic-slot";
                    break;
                default:
                    var card = Db.CardDatabase.GetCard(reward.cardId);
                    if (card != null)
                    {
                        switch (card.type)
                        {
                            case CardType.Minion:
                                prizeIconAnimation = "minion-card";
                                break;
                            case CardType.Spell:
                                prizeIconAnimation = "spell-card";
                                break;
                            case CardType.Trick:
                                prizeIconAnimation = "trick-card";
                                break;
                            case CardType.Relic:
                                prizeIconAnimation = "relic-card";
                                break;
                            case CardType.Power:
                                prizeIconAnimation = "blitz-burst";
                                break;
                        }
                    }
                    break;
            }

            prizeIcon.Play(prizeIconAnimation);

            for (var i = 0; i < 10; i++)
            {
                switch (i)
                {
                    case 1:
                        prizeIcon.RectTransform().anchoredPosition = new Vector2(0, 2-0.5f);
                        transitionToCompleteShineEffect.SetActive(true);
                        transitionToCompleteShineEffect.Play("level-up-shine", ShineEffectTransparency);
                        break;
                    case 2: prizeIcon.RectTransform().anchoredPosition = new Vector2(0, 3-0.5f);
                        break;
                    case 3: prizeIcon.RectTransform().anchoredPosition = new Vector2(0, 4-0.5f);
                        break;
                    case 4: prizeIcon.RectTransform().anchoredPosition = new Vector2(0, 3-0.5f);
                        break;
                    case 5: prizeIcon.RectTransform().anchoredPosition = new Vector2(0, 2-0.5f);
                        break;
                    case 6: prizeIcon.RectTransform().anchoredPosition = new Vector2(0, -1-0.5f);
                        break;
                    case 7: prizeIcon.RectTransform().anchoredPosition = new Vector2(0, 1-0.5f);
                        break;
                    case 8: prizeIcon.RectTransform().anchoredPosition = new Vector2(0, -1-0.5f);
                        break;
                    case 9: prizeIcon.RectTransform().anchoredPosition = new Vector2(0, -0.5f);
                        break;
                }

                yield return Timing.WaitForSeconds(0.04f);
            }

            transitionToCompleteBurstEffect.SetActive(false);
            transitionToCompleteSparkleEffect.SetActive(true);
            transitionToCompleteSparkleEffect.Play("level-up-end-sparkles", () =>
            {
                transitionToCompleteSparkleEffect.SetActive(false);
            });
        }

        private void ShineEffectTransparency(int frame)
        {
            switch (frame)
            {
                case 0: transitionToCompleteShineEffectCanvas.alpha = .4f;
                    break;
                case 1: transitionToCompleteShineEffectCanvas.alpha = .4f;
                    break;
                case 2: transitionToCompleteShineEffectCanvas.alpha = .8f;
                    break;
                case 3: transitionToCompleteShineEffectCanvas.alpha = 1f;
                    break;
                case 4: transitionToCompleteShineEffectCanvas.alpha = .8f;
                    break;
                case 5: transitionToCompleteShineEffectCanvas.alpha = .8f;
                    break;
                case 6: transitionToCompleteShineEffectCanvas.alpha = .4f;
                    break;
                case 8:
                    transitionToCompleteShineEffectCanvas.SetActive(false);
                    break;
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            hoverOutline.SetActive(true);
            hoverOutline.Play("idle");


            OnHoverEnter?.Invoke(this);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            hoverOutline.SetActive(false);
            OnHoverExit?.Invoke(this);
        }


    }
}