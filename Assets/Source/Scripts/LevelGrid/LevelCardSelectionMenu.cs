using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asyncoroutine;
using BlendModes;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.Databases;
using DG.Tweening;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.LevelGrid
{
    public class LevelCardSelectionMenu : MonoBehaviour
    {
        public CanvasGroup mainCanvas;
        public CanvasGroup background;
        public RectTransform headerBanner;
        public CanvasGroup headerBannerCanvas;
        public SpriteAnimation headerBannerAnimator;
        public CanvasGroup headerGlow;
        public GameObject dustParticles;
        public CanvasGroup backgroundBanner;
        public RectTransform layout;
        public CanvasGroup layoutCanvas;
        public RectTransform chooseButtonContainer;
        public CanvasGroup chooseButtonCanvas;
        public Button chooseButton;
        public Button closeButton;

        public SpriteAnimation levelIcon;
        public TextMeshProUGUI levelLabel;

        private bool m_updateButton;
        private List<CardView> m_cards;
        private CardView m_currentCard;
        private int m_currentLevel;
        public event Action<CardData> OnCardChosen;
        public event Action<CardData> OnClosed;

        private void Start()
        {
            closeButton.onClick.AddListener(OnCloseButton);
            chooseButton.onClick.AddListener(OnChoose);
        }

        public void Open( string heroId, List<LevelReward> prizes, int level)
        {
            mainCanvas.alpha = 1;
            m_updateButton = true;
            m_currentCard = null;
            m_currentLevel = level;

            var dbHeroData = Db.HeroDatabase.GetHero(heroId);

            levelIcon.Play(dbHeroData.faction.ToString().ToLower());
            levelLabel.text = (level).ToString();

            layout.DestroyChildren();

            var layoutGroup = layout.GetComponent<HorizontalLayoutGroup>();
            layoutGroup.enabled = true;

            chooseButtonContainer.anchoredPosition=Vector2.zero;

            m_cards = new List<CardView>();
            //headerBanner.localScale = new Vector3(1, 0, 1);
            headerGlow.alpha = 1;
            backgroundBanner.alpha = 0;
            background.alpha = 0;
            headerBannerCanvas.alpha = 0;
            chooseButtonCanvas.alpha = 0;

            Timing.RunCoroutine(AnimateOpen(heroId, prizes));
        }

        private IEnumerator<float> AnimateOpen( string heroId, List<LevelReward> prizes )
        {
            background.interactable = true;
            background.blocksRaycasts = true;
            background.alpha = 0;
            background.DOFade(1f, 0.25f);

            backgroundBanner.alpha = 0;
            backgroundBanner.DOFade(1, 0.25f);

            chooseButtonCanvas.alpha = 0;
            chooseButtonCanvas.DOFade(1, 0.25f);

            headerBannerCanvas.alpha = 0;
            headerBannerCanvas.DOFade(1, 0.25f);
            //headerBanner.localScale = new Vector3(1, 0, 1);
            //headerBanner.DOScaleY(1, 0.75f).SetEase(Ease.OutElastic);

            var dbHeroData = Db.HeroDatabase.GetHero(heroId);
            headerBannerAnimator.Play(dbHeroData.faction.ToString().ToLower());

            yield return Timing. WaitForSeconds(0.25f);

            headerGlow.DOFade(0, 0.25f);
            dustParticles.SetActive(true);

            layout.RectTransform().anchoredPosition = new Vector2(0f, -60f);
            layoutCanvas.alpha = 0;
            yield return Timing.WaitUntilDone(LoadAllHandCards(heroId,prizes).AsCoroutine());
        }

        private async Task LoadAllHandCards(string heroId, List<LevelReward> levelCards)
        {
            for (var i = m_cards.Count-1; i > -1; i--)
            {
                Destroy(m_cards[i].gameObject);
            }

            m_cards.Clear();

            var zoomedCardPrefab = await AddressableReferenceLoader.GetAsset("Card_Zoomed", true);
            var settings = new CreateCardFactorySettings
            {
                CardPrefab = zoomedCardPrefab,
                Layout = layout,
                AdditionalComponents = new List<Type>{ typeof(ClickableCard), typeof(CardTransformComponent) }
            };

            var dbHeroData = Db.HeroDatabase.GetHero(heroId);

            for (var i = 0; i < levelCards.Count; i++)
            {
                var cardData = new CardData();

                switch (levelCards[i].type)
                {
                    case LevelReward.EType.Card:
                        cardData = Db.CardDatabase.GetCard(levelCards[i].cardId);
                        cardData.levelRewardUid = levelCards[i].uid;
                        break;
                    case LevelReward.EType.HeathUp:
                        cardData.type = CardType.Resource;
                        cardData.levelRewardUid = levelCards[i].uid;
                        cardData.name = "HP UP";
                        cardData.resourceType = "hp-up";
                        cardData.portraitUrl = "hp-up";
                        cardData.resourceAmount = levelCards[i].healthPoints;
                        cardData.description = $"INCREASES {dbHeroData.name.ToUpper()}'S <color=#e56569>MAXIMUM HEALTH.</color>";
                        break;
                    case LevelReward.EType.IngredientPouch:
                        var ingredientName = levelCards[i].ingredient;
                        cardData = CardData.FromItem(Db.ItemDatabase.GetItem(ingredientName), Crafting.GetLevelRewardIngredientAmountForIngredient(ingredientName));
                        cardData.levelRewardUid = levelCards[i].uid;
                        break;
                    case LevelReward.EType.ManaShards:
                        cardData.type = CardType.Resource;
                        cardData.levelRewardUid = levelCards[i].uid;
                        cardData.resourceType = "mana-shard";
                        cardData.portraitUrl = "mana-shard";
                        cardData.name = "MANA SHARDS";
                        cardData.resourceAmount = levelCards[i].manaShards;
                        cardData.cost = levelCards[i].uid;
                        cardData.description = "A SPECIAL FORM OF CURRENCY USED FOR <color=#c24ca4>MANA MELDING.</color>";
                        break;
                }

                settings.CardData = cardData;

                var cardView = CardFactory.Create(settings, null);
                cardView.SetInteractable(true);
                cardView.cardOutline.RemoveOutline();
                cardView.GetCardComponent<ClickableCard>().OnLeftClick += OnLeftClickCard;
                cardView.SetActive(true);
                cardView.SetOutlineAlpha(0);
                cardView.Tint(new Color(0f,0f,0f,.2f), BlendMode.Darken);

                m_cards.Add(cardView);
            }

            layoutCanvas.DOFade(1, 0.24f);
            layout.RectTransform().DOAnchorPosY(0, 0.24f).SetEase(Ease.OutBack);
        }

        private void Update()
        {
            if (m_updateButton) chooseButton.interactable = m_currentCard != null;
        }

        private void OnLeftClickCard(ClickableCard card)
        {
            if (!m_updateButton) return;

            for (var i = 0; i < m_cards.Count; i++)
            {
                m_cards[i].SetOutlineAlpha(0);
                m_cards[i].Tint(new Color(0f,0f,0f,.2f), BlendMode.Darken);
            }

            var cardView = card.GetComponent<CardView>();
            m_currentCard = cardView;
            cardView.SetOutlineAlpha(1);
            cardView.RemoveTint();
            cardView.transform.DOPunchScale(Vector3.one * -0.2f, 0.2f);
        }

        private void OnChoose()
        {
            if (m_currentCard && m_updateButton)
            {
                m_currentCard.SetOutlineAlpha(0);
                m_currentCard.RemoveTint();

                OnCardChosen?.Invoke(m_currentCard.data);
                Timing.RunCoroutine( Close(true) );
            }
        }

        private void OnCloseButton()
        {
            Timing.RunCoroutine( Close(false) );
        }

        private IEnumerator<float> Close(bool didChooseCard)
        {
            m_updateButton = false;
            chooseButton.interactable = false;

            if (didChooseCard)
            {
                var layoutGroup = layout.GetComponent<HorizontalLayoutGroup>();
                layoutGroup.enabled = false;

                var targetPos = m_cards[1].Rect.anchoredPosition;
                m_currentCard.Rect.DOAnchorPos(targetPos, 0.4f).SetEase(Ease.OutBack);

                for (var i = 0; i < m_cards.Count; i++)
                {
                    if (m_cards[i] != m_currentCard)
                    {
                        m_cards[i].Fade(0, 0.1f);
                    }
                }

                chooseButtonContainer.DOAnchorPosY(-chooseButtonContainer.sizeDelta.y, 0.24f).SetEase(Ease.InBack);
                yield return Timing.WaitForSeconds(.5f);
            }

            mainCanvas.DOFade(0, 0.24f);
            if( m_currentCard != null)
            {
                m_currentCard.Fade(0, 0.24f);
            }

            yield return Timing.WaitForSeconds(0.24f);

            gameObject.SetActive(false);

            if (didChooseCard)
            {
                OnClosed?.Invoke(m_currentCard.data);
            }
        }

    }
}