using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.PlayFab.Authentication;
using TMPro;
using UnityEngine;

namespace CrossBlitz.LevelGrid
{
    public class LevelTreeColumn : MonoBehaviour
    {
        public TextMeshProUGUI archetypeName;
        public SpriteAnimation archetypeIcon;
        public SpriteAnimation columnAnimator;
        public SpriteAnimation columnBarAnimator;
        public Transform nodeParent;
        public LevelHex levelNodePrefab;

        private string m_heroId;
        private int m_treeIndex;
        private List<LevelHex> levelNodes;

        public event Action<LevelHex> OnLevelUp;

        public void Build ( string heroId, int treeIndex )
        {
            m_heroId = heroId;
            m_treeIndex = treeIndex;

            var hero = Db.HeroDatabase.GetHero(m_heroId);
            var savedHero = App.HeroData.GetHero(m_heroId);
            var tree = hero.levelTrees[m_treeIndex];
            var nextUnlock = savedHero.levelPoints > 0;
            var collectedNodes = 0;

            levelNodes ??= new List<LevelHex>();

            for (var i = 0; i < tree.levels.Count; i++)
            {
                var reward = tree.levels[i];
                var savedReward = savedHero.GetLevelReward(reward.uid);

                LevelHex node;

                if (levelNodes.Count > i)
                {
                    node = levelNodes[i];
                }
                else
                {
                    node = Instantiate(levelNodePrefab, nodeParent, false);
                    node.OnLevelUp += OnNodeCollected;

                    levelNodes.Add(node);
                }

                LevelHex.State state;

                if (savedReward.Collected)
                {
                    state = LevelHex.State.Collected;
                    collectedNodes++;
                }
                else if (nextUnlock)
                {
                    nextUnlock = false;
                    state = LevelHex.State.LevelUp;
                }
                else
                {
                    state = LevelHex.State.Locked;
                }

                if (node.m_currentState != state)
                {
                    node.SetHexState(hero.id, tree.levels[i], state, hero.faction);
                }
            }

            var factionLower = hero.faction.ToString().ToLower();
            var deckRecipe = Db.DeckRecipeDatabase.GetRecipe(tree.archetype);

            archetypeName.text = Db.DeckRecipeDatabase.GetArchetypeName(tree.archetype);
            archetypeIcon.Play(deckRecipe.archetype);
            columnAnimator.Play(factionLower + "-inactive");
            columnBarAnimator.Play(factionLower + "-active");


            switch (collectedNodes)
            {
                case 0:
                    columnBarAnimator.image.fillAmount = 0;
                    break;
                case 1:
                    columnBarAnimator.image.fillAmount = 0.1f;
                    break;
                case 2:
                    columnBarAnimator.image.fillAmount = 0.3f;
                    break;
                case 3:
                    columnBarAnimator.image.fillAmount = 0.55f;
                    break;
                case 4:
                    columnBarAnimator.image.fillAmount = 0.8f;
                    break;
                case 5:
                    columnBarAnimator.image.fillAmount = 1f;
                    break;
            }
        }

        private void OnNodeCollected(LevelHex levelHex)
        {
            OnRewardObtained(levelHex.m_heroId, levelHex.m_rewardUid);
            OnLevelUp?.Invoke(levelHex);

            // var hero = Db.HeroDatabase.GetHero(m_heroId);
            // var savedHero = App.HeroData.GetHero(m_heroId);
            // var reward = hero.GetLevelReward(levelHex.m_rewardUid);
            //
            // levelHex.SetHexState( hero.id, reward, LevelHex.State.Collected, hero.faction );
            //
            // if (savedHero.levelPoints > 0)
            // {
            //     var indexOfNode = levelNodes.IndexOf(levelHex);
            //     var nextReward = levelNodes[indexOfNode + 1];
            // }
        }

        private void OnRewardObtained(string heroId, int levelRewardUid )
        {
            if (string.IsNullOrEmpty(heroId))
            {
                return;
            }

            var dbHeroData = Db.HeroDatabase.GetHero(heroId);
            var levelReward = dbHeroData.GetLevelReward(levelRewardUid);

            App.HeroData.OnHeroLevelRewardClaimed(heroId,levelRewardUid);

            switch (levelReward.type)
            {
                case LevelReward.EType.Card:
                {
                    var itemGrant = new ItemValue
                    {
                        ItemUid = levelReward.cardId,
                        Count = levelReward.GetCardAmount()
                    };
                    App.Inventory.GrantItems(new List<ItemValue> {itemGrant});
                    break;
                }
                case LevelReward.EType.HeathUp:
                {
                    App.HeroData.IncreaseHeroHealth(heroId, levelReward.healthPoints);
                    break;
                }
                case LevelReward.EType.IngredientPouch:
                {
                    var itemGrant = new ItemValue
                    {
                        ItemUid = levelReward.ingredient,
                        Count = Crafting.GetLevelRewardIngredientAmountForIngredient(levelReward.ingredient)
                    };
                    App.Inventory.GrantItems(new List<ItemValue> {itemGrant});
                    //App.HandleItemGrant(result, null,PurchaseErrorCode.NONE, false);
                    break;
                }
                case LevelReward.EType.ManaShards:
                {
                    App.Inventory.AddCurrency(Currency.MANA_SHARDS, levelReward.manaShards);
                    break;
                }
            }
        }
    }
}