using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Cutscene;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.MainMenu;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ViewAPI;
using GameDataEditor;
using MEC;
using Sirenix.OdinInspector;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;
using Random = UnityEngine.Random;
using CrossBlitz.Audio;
using CrossBlitz.ViewAPI.Popups;

namespace CrossBlitz.LevelGrid
{
    public class LevelGridMenu : View
    {
        //public LevelCardSelectionMenu levelCardSelectionMenu;

        [BoxGroup("Animate In")] public CanvasGroup canvasGroup;
        [BoxGroup("Animate In")] public CanvasGroup heroContainerCanvasGroup;

        [BoxGroup("Header")] public SpriteAnimation headerBanner;
        [BoxGroup("Header")] public RectTransform headerBannerTransform;
        [BoxGroup("Hero Properties")] public RectTransform levelContainer;
        [BoxGroup("Hero Properties")] public SpriteAnimation levelIcon;
        [BoxGroup("Hero Properties")] public TextMeshProUGUI levelText;
        [BoxGroup("Hero Properties")] public SpriteAnimation levelPointsContainer;
        [BoxGroup("Hero Properties")] public SpriteAnimation heroPropertiesContainer;
        [BoxGroup("Hero Properties")] public List<TextMeshProUGUI> levelPointFonts;
        [BoxGroup("Hero Properties")] public SpriteAnimation heroBackDrop;
        [BoxGroup("Hero Properties")] public RectTransform hpIcon;
        [BoxGroup("Hero Properties")] public TextMeshProUGUI hpLabel;
        [BoxGroup("Hero Properties")] public HeroCutscenePortraitAnimator heroCutscenePortraitAnimator;

        [BoxGroup("Grid Properties")] public SpriteAnimation gridOutlineA;
        [BoxGroup("Grid Properties")] public CanvasGroup gridOutlineACanvas;
        [BoxGroup("Grid Properties")] public SpriteAnimation gridOutlineB;
        [BoxGroup("Grid Properties")] public CanvasGroup gridOutlineBCanvas;
        [BoxGroup("Grid Properties")] public SpriteAnimation backgroundTextureA;
        [BoxGroup("Grid Properties")] public CanvasGroup backgroundTextureACanvas;
        [BoxGroup("Grid Properties")] public SpriteAnimation backgroundTextureB;
        [BoxGroup("Grid Properties")] public CanvasGroup backgroundTextureBCanvas;
        [BoxGroup("Grid Properties")] public RectTransform levelTreeColumnsParent;
        [BoxGroup("Grid Properties")] public LevelTreeColumn levelTreeColumnPrefab;

        [BoxGroup("Animation")] public RectTransform heroShakeContainer;
        [BoxGroup("Animation")] public Image heroShakeContainerBG;
        [BoxGroup("Animation")] public RectTransform heroProperties;
        [BoxGroup("Animation")] public RectTransform levelPoints;

        [BoxGroup("Misc")] public Button closeButton;
        [BoxGroup("Misc")] public Button respecButton;

        [BoxGroup("Grid Properties")] public CanvasGroup AllGrids;
        [BoxGroup("Grid Properties")] public RectTransform AllGridsTransform;
        [BoxGroup("Grid Properties")] public HexInfoWindow hexTooltip;

        private string m_currentHeroId;

        private float headerBannerX;
        private float headerBannerY;
        private float heroShakeContainerX;
        private float heroShakeContainerY;
        private float heroPropertiesX;
        private float heroPropertiesY;
        private float levelPointsX;
        private float levelPointsY;

        private List<LevelTreeColumn> m_levelTreeColumns;
        //private float allGridsX;
        //private float allGridsY;

        private HeroMainMenuSlot prevHeroSlot;

        private readonly Color[] colorBGs = {
            new Color32(255, 189, 195, 255),
            new Color32(213, 151, 255, 255),
            new Color32(251, 255, 189, 255),
            new Color32(158, 199, 255, 255),
            new Color32(184, 255, 174, 255)
       };

        private float xOffset;

        private static List<string> m_heroes = new List<string>
        {
            GDEItemKeys.Hero_Redcroft,
            GDEItemKeys.Hero_Violet,
            GDEItemKeys.Hero_Quill,
            GDEItemKeys.Hero_Seto,
            GDEItemKeys.Hero_Mereena
        };

        private void Start()
        {
            Events.Subscribe(EventType.OnHeroLevelChanged, OnHeroLevelChanged);
            Events.Subscribe(EventType.OnHeroesReset, OnHeroesReset);

            // levelCardSelectionMenu.OnCardChosen += OnCardChosen;
            // levelCardSelectionMenu.OnClosed += OnLevelCardSelectionMenuClosed;
            // levelCardSelectionMenu.SetActive(false);

            closeButton.onClick.AddListener(OnCloseButton);
            respecButton.onClick.AddListener(OnRespecButton);

            headerBannerX = headerBannerTransform.anchoredPosition.x;
            headerBannerY = headerBannerTransform.anchoredPosition.y;
            heroShakeContainerX = heroShakeContainer.anchoredPosition.x;
            heroShakeContainerY = heroShakeContainer.anchoredPosition.y;
            heroPropertiesX = heroProperties.anchoredPosition.x;
            heroPropertiesY = heroProperties.anchoredPosition.y;
            levelPointsX = levelPoints.anchoredPosition.x;
            levelPointsY = levelPoints.anchoredPosition.y;
            //allGridsX = AllGridsTransform.anchoredPosition.x;
            //allGridsY = AllGridsTransform.anchoredPosition.y;

            backgroundTextureBCanvas.alpha = 0;
            gridOutlineBCanvas.alpha = 0;


            xOffset = 225f;

            canvasGroup.alpha = 0;
            heroContainerCanvasGroup.alpha = 0;

            canvasGroup.DOFade(1, 0.2f);
            heroContainerCanvasGroup.DOFade(1, 0.4f);

            OpenGrid();
        }

        public override void Open()
        {
            Open(App.HeroData.GetCurrentHero());
        }

        public void Open(string heroId)
        {
            m_currentHeroId = heroId;
            m_levelTreeColumns = new List<LevelTreeColumn>();
            levelTreeColumnsParent.DestroyChildren();

            var dbHeroData = Db.HeroDatabase.GetHero(heroId);
            var factionToLower = dbHeroData.faction.ToString().ToLower();
            backgroundTextureA.Play(factionToLower);
            backgroundTextureB.Play(factionToLower);
            gridOutlineA.Play(factionToLower);
            gridOutlineB.Play(factionToLower);

            SetupForHero(heroId);
        }

        public void OpenGrid()
        {
            headerBannerTransform.RectTransform().anchoredPosition = new Vector2(headerBannerX, headerBannerY + (xOffset / 3));
            heroShakeContainer.RectTransform().anchoredPosition = new Vector2(heroShakeContainerX - xOffset, heroShakeContainerY);
            heroProperties.RectTransform().anchoredPosition = new Vector2(heroPropertiesX - xOffset, heroPropertiesY);
            levelPoints.RectTransform().anchoredPosition = new Vector2(levelPointsX - xOffset, levelPointsY);

            AllGrids.alpha = 0;

            Timing.RunCoroutine(AnimateOpen());
        }


        private IEnumerator<float> AnimateOpen()
        {
            AllGrids.DOFade(1, 0.15f);
            headerBannerTransform.RectTransform().DOAnchorPosY(headerBannerY, 0.24f).SetEase(Ease.OutBack);
            yield return Timing.WaitForSeconds(0.05f);

            heroShakeContainer.RectTransform().DOAnchorPosX(heroShakeContainerX, 0.24f).SetEase(Ease.InOutQuint);
            yield return Timing.WaitForSeconds(0.05f);

            heroProperties.RectTransform().DOAnchorPosX(heroPropertiesX, 0.24f).SetEase(Ease.OutBack);
            yield return Timing.WaitForSeconds(0.05f);

            levelPoints.RectTransform().DOAnchorPosX(levelPointsX, 0.24f).SetEase(Ease.OutBack);
            yield return Timing.WaitForSeconds(0.05f);
        }

        private IEnumerator<float> AnimateClose()
        {
            AllGrids.DOFade(0, 0.15f);
            headerBannerTransform.RectTransform().DOAnchorPosY(headerBannerY + (xOffset/3), 0.24f).SetEase(Ease.OutBack);
            yield return Timing.WaitForSeconds(0.05f);

            heroShakeContainer.RectTransform().DOAnchorPosX(heroShakeContainerX - xOffset, 0.24f).SetEase(Ease.InOutQuint);
            yield return Timing.WaitForSeconds(0.05f);

            heroProperties.RectTransform().DOAnchorPosX(heroPropertiesX - xOffset, 0.24f).SetEase(Ease.OutBack);
            yield return Timing.WaitForSeconds(0.05f);

            levelPoints.RectTransform().DOAnchorPosX(levelPointsX - xOffset, 0.24f).SetEase(Ease.OutBack);
            yield return Timing.WaitForSeconds(0.05f);

        }

        private IEnumerator<float> AnimateTransition(string heroID)
        {
            AudioController.PlaySound("UI-Battle_RevealCard");
            Timing.RunCoroutine(AnimateClose());
            Timing.RunCoroutine(FadeBackgroundB(heroID));
            yield return Timing.WaitForSeconds(0.4f);
            Timing.RunCoroutine(AnimateOpen());
            Timing.RunCoroutine(FadeBackgroundA(heroID));
        }

        private IEnumerator<float> FadeBackgroundB(string heroID)
        {
            var dbHeroData = Db.HeroDatabase.GetHero(heroID);
            var factionToLower = dbHeroData.faction.ToString().ToLower();

            backgroundTextureB.Play(factionToLower);
            backgroundTextureBCanvas.DOFade(1, 0.5f);
            gridOutlineB.Play(factionToLower);
            gridOutlineBCanvas.DOFade(1, 0.5f);
            //AllGrids.DOFade(0, 0.25f);
            yield return Timing.WaitForSeconds(0.5f);
        }

        private IEnumerator<float> FadeBackgroundA(string heroID)
        {
            var dbHeroData = Db.HeroDatabase.GetHero(heroID);
            var factionToLower = dbHeroData.faction.ToString().ToLower();

            backgroundTextureA.Play(factionToLower);
            backgroundTextureBCanvas.DOFade(0, 0.1f);
            gridOutlineA.Play(factionToLower);
            gridOutlineBCanvas.DOFade(0, 0.1f);
            //AllGrids.DOFade(1, 0.25f);
            yield return Timing.WaitForSeconds(0.1f);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnHeroLevelChanged, OnHeroLevelChanged);
            Events.Unsubscribe(EventType.OnHeroesReset, OnHeroesReset);
        }

        private void OnHeroesReset(IMessage message)
        {
            Open(m_currentHeroId);
        }

        public void UpdateHeroLevel(int customLevel =-1 )
        {
            if (!App.Initialized)
            {
                App.Init();
            }

            var heroData = App.HeroData.GetHero(m_currentHeroId);
            var level = customLevel > 0 ? customLevel : heroData.level;

            levelIcon.Play(heroData.GetData().faction.ToString().ToLower());
            levelText.text = level.ToString();

            //var offset = levelText.text.Count(c => c.Equals('1'));

            if (level < 10)
            {
                levelContainer.anchoredPosition = new Vector2(4 -41f, levelContainer.anchoredPosition.y);
            }
            else if (level < 100)
            {
                levelContainer.anchoredPosition = new Vector2(-4 -41f,levelContainer.anchoredPosition.y);
            }
            else
            {
                levelContainer.anchoredPosition=new Vector2(-12 -41f,levelContainer.anchoredPosition.y);
            }
        }

        private void OnRewardObtained(LevelHex levelHex)
        {
            var dbHeroData = Db.HeroDatabase.GetHero(levelHex.m_heroId);
            var levelReward = dbHeroData.GetLevelReward(levelHex.m_rewardUid);

            SetupForHero(m_currentHeroId);

            DialogueSnippetData snippet = null;

            switch (levelReward.type)
            {
                case LevelReward.EType.Card:
                {
                    var snippets = dbHeroData.dialogueSnippets.FindAll(d => d.Option == "Level Grid/Card");

                    if (snippets.Count > 0)
                    {
                        snippet = snippets[Random.Range(0, snippets.Count)];
                    }
                    break;
                }
                case LevelReward.EType.HeathUp:
                {
                    var snippets = dbHeroData.dialogueSnippets.FindAll(d => d.Option == "Level Grid/Hp Up");

                    if (snippets.Count > 0)
                    {
                        snippet = snippets[Random.Range(0, snippets.Count)];
                    }
                    break;
                }
                case LevelReward.EType.IngredientPouch:
                {
                    var snippets = dbHeroData.dialogueSnippets.FindAll(d => d.Option == "Level Grid/Ingredient");

                    if (snippets.Count > 0)
                    {
                        snippet = snippets[Random.Range(0, snippets.Count)];
                    }
                    break;
                }
                case LevelReward.EType.ManaShards:
                {
                    var snippets = dbHeroData.dialogueSnippets.FindAll(d => d.Option == "Level Grid/Mana Shards");

                    if (snippets.Count > 0)
                    {
                        snippet = snippets[Random.Range(0, snippets.Count)];
                    }
                    break;
                }
            }

            Timing.RunCoroutine(heroCutscenePortraitAnimator.AnimateLevelUp(snippet));
        }

        private void OnHeroLevelChanged(IMessage message)
        {
            if (message.Data is OnHeroLevelChangedEventArgs args)
            {
                Open(args.heroId);
            }
        }

        private void SetupForHero(string heroId)
        {
            var hero = Db.HeroDatabase.GetHero(m_currentHeroId);

            for (var i = 0; i < hero.levelTrees.Count; i++)
            {
                LevelTreeColumn levelTreeColumn;

                if (m_levelTreeColumns.Count > i)
                {
                    levelTreeColumn = m_levelTreeColumns[i];
                }
                else
                {
                    levelTreeColumn = Instantiate(levelTreeColumnPrefab, levelTreeColumnsParent, false);
                    levelTreeColumn.OnLevelUp += OnRewardObtained;
                    m_levelTreeColumns.Add(levelTreeColumn);
                }

                levelTreeColumn.Build( heroId, i );
            }

            RefreshHeroData();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                App.HeroData.ResetLevelPoints(m_currentHeroId);
                SetupForHero(m_currentHeroId);
            }
        }

        private void RefreshHeroData()
        {
            var heroData = App.HeroData.GetHero(m_currentHeroId);
            var dbHeroData = Db.HeroDatabase.GetHero(m_currentHeroId);

            var factionToLower = dbHeroData.faction.ToString().ToLower();

            headerBanner.Play(factionToLower);
            levelPointsContainer.Play(factionToLower);
            hpLabel.text = heroData.health.ToString();
            heroBackDrop.Play(m_currentHeroId.ToLower());
            heroPropertiesContainer.Play(factionToLower);
            heroCutscenePortraitAnimator.SetHeroId(m_currentHeroId);

            var gridIndex = 0;

            if (m_currentHeroId == GDEItemKeys.Hero_Redcroft)
            {
                gridIndex = 0;
            }
            else if (m_currentHeroId == GDEItemKeys.Hero_Violet)
            {
                gridIndex = 1;
            }
            else if (m_currentHeroId == GDEItemKeys.Hero_Quill)
            {
                gridIndex = 2;
            }
            else if (m_currentHeroId == GDEItemKeys.Hero_Seto)
            {
                gridIndex = 3;
            }
            else if (m_currentHeroId == GDEItemKeys.Hero_Mereena)
            {
                gridIndex = 4;
            }

            for (var i = 0; i < levelPointFonts.Count; i++)
            {
                levelPointFonts[i].SetActive(i == gridIndex);
                if (i == gridIndex) levelPointFonts[i].text = heroData.levelPoints.ToString();
            }

            heroShakeContainerBG.color = colorBGs[gridIndex];

            UpdateHeroLevel();
        }

        private void OnCloseButton()
        {
            Timing.RunCoroutine(AnimateClose());
            Timing.RunCoroutine(AnimateClosed().CancelWith(gameObject));
        }

        private void OnRespecButton()
        {
            PopupController.Open(new PopupInfo
            {
                style = PopupWindowStyle.Generic,
                title = "REALLOCATE LEVEL POINTS",
                body = "All unlocked level nodes will be <#D8694F>deactivated,</color> and all <#839735>Level Points (LvP)</color> will be <#D8694F>refunded.</color>\n\nAny cards acquired from the <#839735>Level Grid</color> will be <#D8694F>removed</color> from your decks.\n\nAre you sure you'd like to <#D8694F>reallocate Level Points?</color>",
                confirm = "REALLOCATE",
                deny = "CANCEL",
            });

            PopupController.OnPopupChoice += OnRespecActionResponse;
        }

        private void OnRespecActionResponse(bool respec)
        {
            PopupController.OnPopupChoice -= OnRespecActionResponse;

            if (respec)
            {
                App.HeroData.ResetLevelPoints(m_currentHeroId);
                SetupForHero(m_currentHeroId);
            }
        }

        private IEnumerator<float> AnimateClosed()
        {
            AudioController.SetMusicParameter("map_cutscene", 0);

            gridOutlineBCanvas.DOFade(0, 0.24f);
            gridOutlineACanvas.DOFade(0, 0.24f);
            yield return Timing.WaitForSeconds(0.2f);

            canvasGroup.DOFade(0, 0.24f);
            heroContainerCanvasGroup.DOFade(0, 0.24f);
            yield return Timing.WaitForSeconds(0.24f);

            LevelTreeController.Close();
        }
    }
}