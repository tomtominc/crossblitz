using System.Collections.Generic;
using System.IO;
using FMOD;
using FMOD.Studio;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace CrossBlitz.Audio
{
    public static class AudioController
    {
        private static EventInstance m_musicLoop;
        private static Bus m_masterBus;
        private static Bus m_musicBus;
        private static Bus m_soundBus;
        private static Bus m_ambienceBus;
        private static Dictionary<string, EventInstance> m_audioInstances;
        private static Dictionary<string, List<string>> m_audioNamesByCategory;
        private static Dictionary<string, int> m_audioPositions;

        public static void Init()
        {
            m_masterBus = FMODUnity.RuntimeManager.GetBus("bus:/MASTER");
            m_musicBus = FMODUnity.RuntimeManager.GetBus("bus:/MASTER/MUSIC");
            m_soundBus = FMODUnity.RuntimeManager.GetBus("bus:/MASTER/SFX");
            m_ambienceBus = FMODUnity.RuntimeManager.GetBus("bus:/MASTER/AMBIENCE");
            m_audioInstances = new Dictionary<string, EventInstance>();
            m_audioNamesByCategory = new Dictionary<string, List<string>>();
            m_audioPositions = new Dictionary<string, int>();

            FMODUnity.RuntimeManager.StudioSystem.getBankList(out FMOD.Studio.Bank[] loadedBanks);

            foreach(Bank bank in loadedBanks)
            {
                bank.getEventList(out EventDescription[] eventDescriptions);

                foreach(EventDescription eventDesc in eventDescriptions)
                {
                    eventDesc.getPath(out string eventPath);

                    var audioName = Path.GetFileNameWithoutExtension(eventPath);
                    var category = eventPath.Replace("event:/", string.Empty).Replace(audioName, string.Empty);
                    category = category.Remove(category.Length - 1, 1);

                    if (m_audioNamesByCategory.ContainsKey(category))
                    {
                        m_audioNamesByCategory[category].Add(audioName);
                    }
                    else
                    {
                        m_audioNamesByCategory.Add(category, new List<string> { audioName });
                    }

                    //Debug.Log($"Category={category} File={audioName}");
                }
            }

            foreach (var audioCategory in m_audioNamesByCategory)
            {
                audioCategory.Value.Sort();
            }
        }

        public static Dictionary<string, List<string>> GetAllAudioByCategory()
        {
            return m_audioNamesByCategory;
        }


        public static EventInstance SetSoundInstance(string sound, string category = "SFX", GameObject attachTo = null)
        {
            var instance = FMODUnity.RuntimeManager.CreateInstance($"event:/{category}/{sound}");

            if (attachTo != null)
            {
                instance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(attachTo));
            }

            return instance;
        }

        public static EventInstance PlaySound(string sound, string category = "SFX", bool oneShot = true, GameObject attachTo=null)
        {
            var instance = FMODUnity.RuntimeManager.CreateInstance($"event:/{category}/{sound}");

            if (attachTo != null)
            {
                instance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(attachTo));
            }

            instance.start();

            if (oneShot)
            {
                instance.release();
            }
            else
            {
                instance.release();

                if (m_audioInstances.ContainsKey(sound))
                {
                    m_audioInstances[sound].release();
                    m_audioInstances.Remove(sound);
                }

                m_audioInstances.Add(sound, instance);
            }

            return instance;
        }

        public static void StopSound(string sound)
        {
            if (string.IsNullOrEmpty(sound))
            {
                return;
            }

            if (m_audioInstances.ContainsKey(sound))
            {
                m_audioInstances[sound].stop(STOP_MODE.ALLOWFADEOUT);
                m_audioInstances[sound].release();
                m_audioInstances.Remove(sound);
            }
        }

        public static void SetSoundParameter(string sound, string key, int value)
        {
            if (string.IsNullOrEmpty(sound))
            {
                return;
            }

            if (m_audioInstances.ContainsKey(sound))
            {
                m_audioInstances[sound].setParameterByName(key, value);
            }
        }

        public static void SetMusicParameter(string key, int value)
        {
            if (m_musicLoop.isValid())
            {
                m_musicLoop.setParameterByName(key, value);
            }
        }

        public static EventInstance PlaySong(string song, bool loop = true, bool startFromLastPos = false)
        {
            if (!IsPlaying(song))
            {
                StopSong();
                m_musicLoop = FMODUnity.RuntimeManager.CreateInstance($"event:/MUSIC/{song}");

                if (startFromLastPos) SetLastPosition(song);

                m_musicLoop.start();

                if (!loop)
                {
                    m_musicLoop.release();
                }
            }

            return m_musicLoop;
        }

        public static void StopSong()
        {
            if (m_musicLoop.isValid())
            {
                // Save the Songs Position
                m_musicLoop.getDescription(out EventDescription description);
                description.getPath(out var path);

                if (!string.IsNullOrEmpty(path))
                {
                    var songSplit = path.Split('/');

                    if (songSplit.Length > 2)
                    {
                        var song = songSplit[2];
                        SaveLastPosition(song);
                    }
                }

                m_musicLoop.stop(STOP_MODE.ALLOWFADEOUT);
                m_musicLoop.release();
            }
        }

        public static void PauseMapSong(int pause)
        {
            if (m_musicLoop.isValid())
            {
                m_musicLoop.setParameterByName("map_pause", pause);
            }
        }

        public static void SaveLastPosition(string song)
        {
            m_musicLoop.getTimelinePosition(out var pos);

            if (m_audioPositions.ContainsKey(song))
            {
                m_audioPositions[song] = pos;
            }
            else
            {
                m_audioPositions.Add(song, pos);
            }
        }

        public static void SetLastPosition(string song)
        {
            if (m_audioPositions.ContainsKey(song))
            {
                var pos = m_audioPositions[song];
                m_musicLoop.setTimelinePosition(pos);
            }
            else
            {
                m_audioPositions.Add(song, 0);
            }
        }

        public static EventInstance GetCurrentSong()
        {
            return m_musicLoop;
        }

        public static bool IsPlaying(string songName)
        {
            if (!m_musicLoop.isValid())
            {
                return false;
            }

            if (m_musicLoop.getDescription(out var e) == RESULT.OK && e.getPath(out var path) == RESULT.OK)
            {
                return path.Contains(songName);
            }

            return false;
        }

        public static bool IsSoundPlaying(string soundName)
        {
            if (m_audioInstances.ContainsKey(soundName))
            {
                if (!m_audioInstances[soundName].isValid())
                {
                    m_audioInstances.Remove(soundName);
                    return false;
                }
                return true;
            }
            return false;
        }

        //public static bool IsSoundPlaying(string soundName)
        //{
        //    if (m_audioInstances.ContainsKey(soundName))
        //    {
        //        if (!m_audioInstances[soundName].isValid())
        //        {
        //            m_audioInstances.Remove(soundName);
        //            Debug.Log("NOT VALID SFX");
        //            return false;
        //        }

        //        if (m_audioInstances[soundName].getDescription(out var e) == RESULT.OK && e.getPath(out var path) == RESULT.OK)
        //        {
        //            if (!path.Contains(soundName))
        //            {
        //                m_audioInstances[soundName].release();
        //                m_audioInstances.Remove(soundName);
        //                Debug.Log("REMOVE AUDIO INSTANCE");
        //                return false;
        //            }
        //        }

        //        return true;
        //    }

        //    return false;
        //}

        public static void SetMasterVolume(float volumePercent)
        {
            // var busVolume = 90 * volumePercent - 80;
            // var volume = Mathf.Pow(10f, busVolume / 20f);
            Debug.Log("Master Audio Set: " + volumePercent);
            m_masterBus.setVolume(volumePercent);
        }

        public static void SetMusicVolume(float volumePercent)
        {
            // var busVolume = 90 * volumePercent - 80;
            // var volume = Mathf.Pow(10f, busVolume / 20f);
            Debug.Log("Music Audio Set: " + volumePercent);
            m_musicBus.setVolume(volumePercent);

            //MasterAudio.OnlyPlaylistController.PlaylistVolume = volume;
        }

        public static void SetSoundVolume(float volumePercent)
        {
            // var busVolume = 90 * volumePercent - 80;
            // var volume = Mathf.Pow(10f, busVolume / 20f);
            Debug.Log("SoundFX Audio Set: " + volumePercent);
            m_soundBus.setVolume(volumePercent);
        }

        public static void SetAmbienceVolume(float volumePercent)
        {
            // var busVolume = 90 * volumePercent - 80;
            // var volume = Mathf.Pow(10f, busVolume / 20f);
            Debug.Log("Ambience Audio Set: " + volumePercent);
            m_ambienceBus.setVolume(volumePercent);
        }

        public static void FadePlaylistVolume(float volume, float time)
        {
            //MasterAudio.FadePlaylistToVolume(volume, time);
        }
        // private static void OnSongEnded(string songName)
        // {
        //     if (!m_loopNextPlaylist)
        //     {
        //         MasterAudio.StopPlaylist();
        //     }
        // }
    }
}