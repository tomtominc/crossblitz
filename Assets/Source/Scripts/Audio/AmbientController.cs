using UnityEngine;

namespace CrossBlitz.Audio
{
    public class AmbientController
    {
        private static string m_currentAmbientName;

        public static void PlayAmbient(string ambientName)
        {
            if (m_currentAmbientName == ambientName)
            {
                return;
            }

            Stop();

            m_currentAmbientName = ambientName;
            AudioController.PlaySound(m_currentAmbientName, "AMBIENCE", false);
        }

        public static void Stop()
        {
            Debug.Log($"Trying to stop sound: {m_currentAmbientName}");
            AudioController.StopSound(m_currentAmbientName);
        }
    }
}