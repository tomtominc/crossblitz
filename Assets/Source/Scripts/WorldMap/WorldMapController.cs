using System;
using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Cameras;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Data;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.Plugins.TakoBoyStudios.Utilities;
using CrossBlitz.WorldMap.UI;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using SWS;
using UnityEngine;

namespace CrossBlitz.WorldMap
{
    public class WorldMapController : MonoBehaviour
    {
        public static WorldMapController Instance;

        [BoxGroup("Components")]
        public WorldMapUI ui;
        [BoxGroup("Components")]
        public WorldMapPointer pointer;
        [BoxGroup("Components")]
        public WorldMapCheckpoint checkpoint;
        [BoxGroup("Components")]
        public Camera worldMapCamera;
        [BoxGroup("Components")]
        public WorldMapCamera dragCamera;

        [BoxGroup("Prefabs")]
        public WorldMapNode nodePrefab;

        [BoxGroup("Containers")]
        public Transform pathNodeContainer;

        [BoxGroup("Paths")]
        public List<BezierPathManager> storyPaths;

        private FableChapterData _storyData;

        public event Action OnFinishedAnimatingPath;
        public event Action OnFinishedMovingPointerToDestination;

        private void Awake()
        {
            Instance = this;
        }

        public void SetUpWorldMap(FableChapterData chapterData)
        {
            _storyData = chapterData;
            //_clientChapterData = clientChapterData;
            Timing.RunCoroutine(AnimateStoryIn().CancelWith(gameObject));
        }

        private IEnumerator<float> AnimateStoryIn()
        {
            dragCamera.enabled = false;

            var illustration = Db.CutsceneDatabase.GetCutscene(_storyData.IntroIllustrationUid);
            if (illustration == null)
            {
                ui.prologueBG.alpha = 0;
            }


            Debug.Log($"{_storyData.ChapterHero}-{_storyData.BookNumber}-{_storyData.ChapterNumber}");
            //todo: we need to setup the PREVIOUS paths to be already in before animating the new path
            var path = storyPaths.Find(p => p.name == $"{_storyData.ChapterHero}-{_storyData.BookNumber}-{_storyData.ChapterNumber}");

            if (path == null || path.waypoints.Length <= 1)
            {
                yield return Timing.WaitUntilDone(ui.AnimateIn(_storyData));

                OnFinishedAnimatingPath?.Invoke();
                yield return Timing.WaitForSeconds(0.24f);
                OnFinishedMovingPointerToDestination?.Invoke();
                yield break;
            }

            var start = path.waypoints[0].position;
            var end = path.waypoints[path.waypoints.Length - 1].position;
            var midpoint =(start + end) / 2 + _cameraOffset;

            pointer.SetActive(false);
            pointer.SetStoryCharacter(_storyData.ChapterHero.ToLower());
            pointer.transform.position = start + pointer.nodeOffset;

            worldMapCamera.transform.position = new Vector3(midpoint.x, midpoint.y, -100);
            worldMapCamera.orthographicSize = 5.625f * 1.5f;
            yield return Timing.WaitForSeconds(0.5f);

            //yield return Timing.WaitUntilDone(ui.AnimateIn(_storyData));
            //yield return Timing.WaitForSeconds(1f);

            // Load the intro illustration scene
            if (illustration != null)
            {
                ui.prologueBG.alpha = 1;
                ui.PlayIllustration(illustration);
                while (!ui.cutsceneInputMode.CutsceneInput.CutsceneFinished)
                {
                    yield return Timing.WaitForOneFrame;
                }

                ui.cutsceneInputMode.EndMode();

                yield return Timing.WaitUntilDone(ui.prologueBG.DOFade(0, 2f).WaitForCompletion(true));
            }

            // Drop the pointer
            pointer.SetActive(true);
            AudioController.PlaySound("SpecialTile_Fall", "SFX", true);
            yield return Timing.WaitForSeconds(0.25f);

            // Zoom in with camera
            yield return Timing.WaitUntilDone(worldMapCamera.DOOrthoSize(5.625f, 2f).SetEase(Ease.InOutSine).WaitForCompletion(true));
            
            // Spawn the map poins
            var lastPoint = new Vector3(100,100);
            var points = new List<Vector3>(path.pathPoints);
            var pointSFXPitch = .8f;
            points.Insert(0, start);

            for (var i = 0; i < points.Count; i++)
            {

                var point = points[i];
                if (Vector2.Distance(point, lastPoint) < 0.6f) continue;
                var node = Instantiate(nodePrefab, pathNodeContainer);
                node.transform.position = point;
                lastPoint = point;
                AudioController.PlaySound("battle_card_draw", "BARDSFX", true).setPitch(pointSFXPitch);
                pointSFXPitch += 0.1f;

                if (i == points.Count - 2) yield return Timing.WaitForSeconds(.45f);
                else yield return Timing.WaitForSeconds(0.16f);

            }

            checkpoint.transform.position = end;
            checkpoint.SetActive(true);
            checkpoint.transform.DOPunchScale(Vector2.up * 0.25f, 0.25f);
            AudioController.PlaySound("battle_card_flip", "BARDSFX", true).setPitch(pointSFXPitch);


            yield return Timing.WaitForSeconds(0.5f);

            yield return Timing.WaitUntilDone(ui.AnimateIn(_storyData));

            OnFinishedAnimatingPath?.Invoke();

            yield return Timing.WaitForSeconds(0.24f);

            _pointerIsMoving = true;
            _cameraCanMove = false;
            _pointerMoveDirection = (int)Mathf.Sign(points[points.Count-1].x - points[0].x);
            _cameraMoveToPosition = points[points.Count - 1];

            yield return Timing.WaitUntilDone(pointer.MoveAlongPath(points).CancelWith(gameObject));

            _pointerIsMoving = false;

            OnFinishedMovingPointerToDestination?.Invoke();
        }

        // is the pointer moving left or right?
        private int _pointerMoveDirection;
        // is the pointer moving?
        private bool _pointerIsMoving;
        // where is our destination?
        private Vector3 _cameraMoveToPosition;
        private bool _cameraCanMove;
        private Vector3 _cameraOffset = new Vector3(-1.25f, 1.25f);

        private void Update()
        {
            if (_pointerIsMoving)
            {
                if (_pointerMoveDirection > 0)
                {
                    if (pointer.transform.position.x > worldMapCamera.transform.position.x)
                    {
                        _cameraCanMove = true;
                        _pointerIsMoving = false;
                    }
                }
                else
                {
                    if (pointer.transform.position.x < worldMapCamera.transform.position.x)
                    {
                        _cameraCanMove = true;
                        _pointerIsMoving = false;
                    }
                }
            }

            if (_cameraCanMove)
            {
                _cameraCanMove = false;

                var newPosition = new Vector3(
                    _cameraMoveToPosition.x + _cameraOffset.x,
                    _cameraMoveToPosition.y + _cameraOffset.y,
                    worldMapCamera.transform.position.z);

                worldMapCamera.transform.DOMove(newPosition, pointer.moveSpeed * 0.8f)
                    .SetEase(Ease.OutQuad).SetSpeedBased(true);

                //worldMapCamera.transform.position = Vector3.MoveTowards(worldMapCamera.transform.position,newPosition, pointer.moveSpeed * 0.8f * Time.deltaTime );
            }
        }
    }
}