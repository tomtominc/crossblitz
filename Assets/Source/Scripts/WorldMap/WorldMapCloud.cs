using UnityEngine;

namespace CrossBlitz.WorldMap
{
    public class WorldMapCloud : MonoBehaviour
    {
        private const float SIZE = 2;
        public SpriteAnimation cloudAnimator;
        public SpriteAnimation cloudShadowAnimator;

        private float _horizontalBound;
        private float _verticalBound;

        private Transform _cacheTransform;

        private void Start()
        {
            var cloudName = $"cloud-{Random.Range(1, 4)}";

            _cacheTransform = transform;

            cloudAnimator.Play(cloudName);
            cloudShadowAnimator.Play(cloudName);

            cloudAnimator.renderer.sortingOrder = -((int)_cacheTransform.position.y * 10) + 1000;
            cloudShadowAnimator.renderer.sortingOrder = 2;
        }

        public void SetBounds(float horizontalBound, float verticalBound)
        {
            _horizontalBound = horizontalBound;
            _verticalBound = verticalBound;
        }

        public void CheckBounds()
        {
            if (_cacheTransform == null) return;

            if (_cacheTransform.position.x >= _horizontalBound + SIZE)
            {
                _cacheTransform.position = new Vector2(-_horizontalBound-SIZE, _cacheTransform.position.y);
            }
        }
    }
}