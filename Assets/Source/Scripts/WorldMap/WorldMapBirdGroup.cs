using System.Collections.Generic;
using Steer2D;
using UnityEngine;

namespace CrossBlitz.WorldMap
{
    public class WorldMapBirdGroup : MonoBehaviour
    {
        public List<Seek> birds;
        private Vector2 _destination;

        public void SetupBirdTarget(Vector2 target)
        {
            _destination = target;
            for (var i = 0; i < birds.Count; i++)
            {
                birds[i].TargetPoint = _destination;
            }
        }

        private void Update()
        {
            if (Vector2.Distance(birds[0].transform.position, _destination) <= 3)
            {
                Destroy(gameObject);
            }
        }
    }
}