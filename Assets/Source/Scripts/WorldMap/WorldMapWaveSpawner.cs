using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CrossBlitz.WorldMap
{
    public class WorldMapWaveSpawner : MonoBehaviour
    {
        public float delayBetweenSpawns = 0.04f;

        private float _timer;
        private PolygonCollider2D _spawnBounds;
        private List<WorldMapWave> _waves;

        private void Start()
        {
            _timer = delayBetweenSpawns;
            _spawnBounds = GetComponent<PolygonCollider2D>();
            _waves = new List<WorldMapWave>();
        }
        private void Update()
        {
            _timer += Time.deltaTime;

            if (_timer > delayBetweenSpawns)
            {
                // var wave = _wavePool.AcquireObject<WorldMapWave>(Vector2.zero, Quaternion.identity);
                //
                // wave.transform.position = PointInArea(wave);
                //
                // _waves.Add(wave);
                // wave.OnFinished += OnWaveFinished;
                //
                // _timer = 0;
            }
        }

        private void OnWaveFinished(WorldMapWave wave)
        {
            wave.OnFinished -= OnWaveFinished;
            _waves.Remove(wave);
        }

        private Vector2 PointInArea(WorldMapWave other)
        {
            var bounds = _spawnBounds.bounds;
            var center = bounds.center;
            var point = Vector2.zero;

            var attempt = 0;

            do {
                point.x = Random.Range(center.x - bounds.extents.x, center.x + bounds.extents.x);
                point.y = Random.Range(center.y - bounds.extents.y, center.y + bounds.extents.y);
                other.transform.position = point;
                attempt++;
            } while (!_spawnBounds.OverlapPoint(point) && _waves.Any( wave => wave.IsTouching(other)) && attempt <= 1000);

            if (attempt >= 1000)
            {
                Debug.LogError("Too many attempts!!!");
            }

            return point;
        }
    }
}