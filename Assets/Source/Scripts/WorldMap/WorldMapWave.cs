using System;
using KennethDevelops.ProLibrary.DataStructures.Pool;
using KennethDevelops.ProLibrary.Managers;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.WorldMap
{
    public class WorldMapWave : MonoBehaviour
    {
        private const float MoveSpeed = 0.08f;
        private const float FadeSpeed = 1;

        private BoxCollider2D _collider;
        private SpriteRenderer _renderer;
        private SpriteAnimation _animator;
        private float _alpha;
        private float _pingTime;
        private float _randomStartDelay;
        private bool _playing;
        private float _timer;
        private Vector2 _originalPosition;

        public event Action<WorldMapWave> OnFinished;

        public void Start()
        {
            if (_renderer == null)
                _renderer = GetComponent<SpriteRenderer>();

            if (_animator == null)
                _animator = GetComponent<SpriteAnimation>();

            if (_collider == null)
                _collider = GetComponent<BoxCollider2D>();

            _originalPosition = transform.position;
            ResetWave();
        }

        private void ResetWave()
        {
            _alpha = 0;
            _pingTime = 0;
            _renderer.color = new Color(1,1,1,_alpha);
            _animator.Play($"wave-{Random.Range(1, 4)}");
            _randomStartDelay = Random.Range(0.04f, 4f);
            _playing = false;
            _timer = 0;

            transform.position = _originalPosition;
        }

        private void Update()
        {
            if (!_playing)
            {
                _timer += Time.deltaTime;
                _playing = _timer >= _randomStartDelay;
            }
            else
            {
                transform.position += Vector3.right * (MoveSpeed * Time.deltaTime);
                _pingTime += Time.deltaTime * FadeSpeed;
                _alpha = Mathf.PingPong(_pingTime, 2);

                if (_pingTime >= 4)
                {
                    _playing = false;
                    ResetWave();
                    return;
                }

                _renderer.color = new Color(1, 1, 1, _alpha);
            }
        }

        // public void OnDispose()
        // {
        //     OnFinished?.Invoke(this);
        // }

        // public bool OverlapsPoint(Vector2 point)
        // {
        //     return _collider.OverlapPoint(point);
        // }

        public bool IsTouching(WorldMapWave other)
        {
            return _collider.IsTouching(other._collider);
        }
    }
}