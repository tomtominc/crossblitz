using System.Collections.Generic;
using CrossBlitz.Fables.Data;
using CrossBlitz.Story.Data;
using DG.Tweening;
using Febucci.UI;
using MEC;
using UnityEngine;

namespace CrossBlitz.WorldMap
{
    public class WorldMapStoryTitleBanner : MonoBehaviour
    {
        public CanvasGroup backgroundCanvas;

        public RectTransform headerBanner;
        public CanvasGroup headerGlow;
        public GameObject dustParticles;
        //public Canvas titleCanvas;

        public TextAnimatorPlayer titleLabel;

        public IEnumerator<float> AnimateIn(FableChapterData storyData)
        {
            backgroundCanvas.alpha = 0;
            headerBanner.localScale = new Vector2(1, 0);
            //titleCanvas.overrideSorting = false;
            headerGlow.alpha = 1;

            headerBanner.SetActive(true);
            backgroundCanvas.SetActive(true);

            yield return Timing.WaitForOneFrame;

            titleLabel.ShowText(string.Empty);

            backgroundCanvas.DOFade(1, 0.25f);

            headerBanner.DOScaleY(1, 0.5f)
                .SetEase(Ease.OutElastic, 2);

            yield return Timing. WaitForSeconds(0.25f);
            headerGlow.DOFade(0, 0.25f);
            dustParticles.SetActive(true);

            yield return Timing. WaitForSeconds(0.25f);

            //titleCanvas.overrideSorting = true;
            //titleCanvas.sortingOrder = 7100;
            titleLabel.ShowText(storyData.ChapterName);
        }
    }
}