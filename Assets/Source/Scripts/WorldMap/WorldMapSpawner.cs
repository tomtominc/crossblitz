using System.Collections.Generic;
using KennethDevelops.ProLibrary.Managers;
using UnityEngine;

namespace CrossBlitz.WorldMap
{
    public class WorldMapSpawner : MonoBehaviour
    {
        public Transform cloudContainer;
        public WorldMapCloud cloudPrefab;

        private float _horizontalBound = 25f;
        private float _verticalBound = 14.25f;
        private List<WorldMapCloud> _clouds;

        private void Start()
        {
            _clouds=new List<WorldMapCloud>();

            const float boundX = 25;
            var columns = 6;
            var rows = 4;

            var horzSpacing = (_horizontalBound * 2f + 8) / columns;
            var vertSpacing = _verticalBound * 1.2f / rows;

            for (var col = 1; col <= columns; col++)
            {
                for (var row = 1; row <= rows+1; row++)
                {
                    if (Random.value > 0.8f) continue; // skip 20% of all spawns

                    var posX = -_horizontalBound + (horzSpacing * col) + Random.Range(-2, 3);
                    var posY = _verticalBound - (vertSpacing * row) + Random.Range(-2, 3);

                    var position = new Vector2(posX, posY);
                    var cloud = Instantiate(cloudPrefab, cloudContainer, false);
                    cloud.transform.position = position;
                    cloud.SetBounds(boundX, _verticalBound);
                    _clouds.Add(cloud);
                }
            }
        }

        private void Update()
        {
            for (var i = 0; i < _clouds.Count; i++)
            {
                _clouds[i].CheckBounds();
            }
        }
    }
}