using UnityEngine;

namespace CrossBlitz.WorldMap
{
    public class WorldMapBird : MonoBehaviour
    {
        private SpriteAnimation _animator;
        private Vector2 _lastPosition;
        private Transform _cachedTransform;

        public void Start()
        {
            _animator = GetComponent<SpriteAnimation>();
            _animator.Play("fly", Random.Range(0, 28));
            _animator.renderer.sortingOrder = 10;
            _cachedTransform = transform;
        }

        public void Update()
        {
            var currentPosition = _cachedTransform.position;
            _cachedTransform.localScale = new Vector3(_lastPosition.x > currentPosition.x ? 1 : -1,1,1);
            _lastPosition = currentPosition;
        }
    }
}