using UnityEngine;

namespace CrossBlitz.WorldMap
{
    public class WorldMapBirdIdler : MonoBehaviour
    {
        private SpriteAnimation _animator;

        private void Start()
        {
            _animator = GetComponent<SpriteAnimation>();
            _animator.Play("idle", Random.Range(0, 45));
        }
    }
}