using System.Collections.Generic;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.WorldMap.UI
{
    public class WorldMapPointer : MonoBehaviour
    {
        public float moveSpeed = 0.75f;
        public SpriteAnimation characterIcon;
        public ShakeContainer worldShaker;
        public Vector3 nodeOffset = new Vector3(0, -0.45f);

        public void SetStoryCharacter(string character)
        {
            characterIcon.Play(character);
        }

        public void ShakeCamera()
        {
            worldShaker.Shake(0.3f, 1f);
        }

        public IEnumerator<float> MoveAlongPath(List<Vector3> waypoints)
        {
            for (var i = 0; i < waypoints.Count; i++)
            {
                var waypoint = waypoints[i] + nodeOffset;

                while (Vector3.Distance(transform.position, waypoint) > 0.01f)
                {
                    transform.position = Vector3.MoveTowards(transform.position,
                        waypoint, moveSpeed * Time.deltaTime);

                    yield return Timing.WaitForOneFrame;
                }

                transform.position = waypoint;
            }
        }
    }
}