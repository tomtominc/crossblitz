using System;
using System.Collections.Generic;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Fables.Cutscene;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.Fables.Data;
using CrossBlitz.InputAPI;
using DG.Tweening;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.WorldMap.UI
{
    public class WorldMapUI : MonoBehaviour
    {
        public SceneLoadInputMode cutsceneInputMode;
        public WorldMapSidePanel sidePanel;
        public WorldMapStoryTitleBanner banner;
        public TextMeshProUGUI chapterTitle;
        public SpriteAnimation chapterBanner;
        public CanvasGroup prologueBG;
        public Button skipButton;

        private FableChapterData _storyData;
        private bool _endIntro;
        public event Action OnFinishedCutscene;

        public void Start()
        {
            sidePanel.SetActive(false);
            banner.SetActive(false);
            skipButton.SetActive(true);
            prologueBG.SetActive(true);

            skipButton.onClick.AddListener(EndIntro);

            _endIntro = false;
        }

        public IEnumerator<float> AnimateIn(FableChapterData storyData)
        {
            _storyData = storyData;
            chapterTitle.text = storyData.ChapterName;
            chapterBanner.Play(storyData.ChapterFaction.ToString().ToLower());

            sidePanel.SetActive(true);
            yield return Timing.WaitUntilDone(sidePanel.AnimateIn(_storyData));

            //banner.SetActive(true);
            //yield return Timing.WaitUntilDone(banner.AnimateIn(_storyData));
            //yield return Timing.WaitForSeconds(0.25f);

            Timing.RunCoroutine(sidePanel.SwitchToObjectiveView());

            yield return Timing.WaitForSeconds(0.5f);
        }

        public void PlayIllustration(CutsceneData illustrationData)
        {
            //sidePanel.heroPortrait.image.DOFade(0, 0.125f);
            Timing.RunCoroutine(LoadIllustration(illustrationData));
        }

        public void PlayCutscene(CutsceneData cutsceneData)
        {
            //sidePanel.heroPortrait.image.DOFade(0, 0.125f);
            Timing.RunCoroutine(LoadCutscene(cutsceneData));
        }

        private IEnumerator<float> LoadIllustration(CutsceneData illustrationData)
        {
            cutsceneInputMode.StartMode();
            while (cutsceneInputMode.CutsceneInput == null)
            {
                Debug.LogWarning("Cutscene Input is null!!");
                yield return Timing.WaitForOneFrame;
            }

            cutsceneInputMode.CutsceneInput.StartEvent(illustrationData, new EventOptions { dimBackground = false, usesSkipButton = true, usesDialogueBox = false, centerIllustration = true });
            cutsceneInputMode.CutsceneInput.OnFinishedCutscene += FinishedIllustration;
        }

        private IEnumerator<float> LoadCutscene(CutsceneData cutsceneData)
        {
            cutsceneInputMode.StartMode();
            while (cutsceneInputMode.CutsceneInput == null)
            {
                Debug.LogWarning("Cutscene Input is null!!");
                yield return Timing.WaitForOneFrame;
            }

            cutsceneInputMode.CutsceneInput.StartEvent(cutsceneData, new EventOptions { dimBackground = false, usesSkipButton = true, usesDialogueBox = true});
            cutsceneInputMode.CutsceneInput.OnFinishedCutscene += FinishedCutscene;
        }



        private void FinishedIllustration()
        {
            var skipIntro = false;
            if (cutsceneInputMode.CutsceneInput != null)
            {
                skipIntro = cutsceneInputMode.CutsceneInput.GetSkipped();
                cutsceneInputMode.CutsceneInput.OnFinishedCutscene -= FinishedIllustration;
                cutsceneInputMode.CutsceneInput.EndMode();
            }

            if (skipIntro) EndIntro();
        }

        private void FinishedCutscene()
        {
            var skipIntro = false;

            if (cutsceneInputMode.CutsceneInput != null)
            {
                skipIntro = cutsceneInputMode.CutsceneInput.GetSkipped();
                cutsceneInputMode.CutsceneInput.OnFinishedCutscene -= FinishedCutscene;
                cutsceneInputMode.CutsceneInput.EndMode();
            }

            if (skipIntro)
            {
                EndIntro();
            }
            else
            {
                // Zoom in with camera
                Timing.RunCoroutine(MapZoomInEnd());
            }

        }

        private IEnumerator<float> MapZoomInEnd()
        {
            WorldMapController.Instance.worldMapCamera.DOOrthoSize(10f, 2f).SetEase(Ease.InOutSine);
            yield return Timing.WaitForSeconds(1.5f);

            //sidePanel.heroPortrait.image.DOFade(1, 0.25f);
            EndIntro();
        }

        private void EndIntro()
        {
            if (!_endIntro)
            {
                _endIntro = true;
                skipButton.SetActive(false);
                OnFinishedCutscene?.Invoke();
            }
        }
    }
}