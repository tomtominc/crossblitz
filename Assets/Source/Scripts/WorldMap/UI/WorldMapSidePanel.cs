using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Databases;
using CrossBlitz.FableCharacterSelect;
using CrossBlitz.Fables.Data;
using CrossBlitz.Hero;
using CrossBlitz.MainMenu;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace CrossBlitz.WorldMap.UI
{
    public class WorldMapSidePanel : MonoBehaviour
    {
        [BoxGroup("Background Elements")]public HeroPanelBackground factionSetter;

        [BoxGroup("Top Book Info")]public HeroMainMenuSlot bookHeroSlot;
        [BoxGroup("Top Book Info")]public SpriteAnimation bookNumberRomanNumeral;

        [BoxGroup("Objective")]public GameObject objectiveContainer;
        [BoxGroup("Objective")]public CanvasGroup objectiveTitle;
        [BoxGroup("Objective")]public TextMeshProUGUI objective;

        private RectTransform _cachedRectTransform;

        public RectTransform RectTransform
        {
            get
            {
                if (!_cachedRectTransform) _cachedRectTransform = this.RectTransform();
                return _cachedRectTransform;
            }
        }

        private void Start()
        {
            objectiveContainer.SetActive(false);
        }

        public IEnumerator<float> AnimateIn(FableChapterData storyData)
        {
            var playerChapterData = App.FableData.GetChapter(storyData.Uid);
            playerChapterData.Init(storyData);

            var factionName = storyData.ChapterFaction.ToString().ToLower();
            var chapter = Db.FablesDatabase.GetChapter(playerChapterData.ChapterUid);
            var quest = Db.FableQuestDatabase.GetQuest(chapter.MainQuest);

            factionSetter.SetFaction(factionName);
            bookHeroSlot.SetHeroId(storyData.ChapterHero);
            bookNumberRomanNumeral.Play($"{factionName}-{storyData.ChapterNumber}");
            objective.text = quest.QuestDescription.ToUpper();

            yield return Timing.WaitForOneFrame;

            RectTransform.anchoredPosition = new Vector2(-RectTransform.sizeDelta.x, 0);
            AudioController.PlaySound("Battle_GenericAttack-Charge");
            yield return Timing.WaitUntilDone( RectTransform.DOAnchorPosX(-16f, 0.5f)
                .SetEase(Ease.OutBack)
                .WaitForCompletion(true));
        }

        public IEnumerator<float> SwitchToHeroView()
        {
            objectiveTitle.DOFade(0, 0.12f);
            objective.DOFade(0, 0.12f);

            yield return Timing.WaitForSeconds(0.12f);
        }

        public IEnumerator<float> SwitchToObjectiveView()
        {
            objectiveTitle.alpha = 0;
            objective.alpha = 0;
            objectiveContainer.SetActive(true);

            yield return Timing.WaitForSeconds(0.08f);

            objectiveTitle.RectTransform().anchoredPosition = new Vector2(-15f, 0f);
            objectiveTitle.DOFade(1, 0.12f);
            objectiveTitle.RectTransform().DOAnchorPosX(0f, 0.12f).SetEase(Ease.OutBack);

            yield return Timing.WaitForSeconds(0.08f);

            objective.RectTransform().anchoredPosition = new Vector2(-15f, -10f);
            objective.alpha = 1;
            objective.RectTransform().DOAnchorPosX(0, 0.12f).SetEase(Ease.OutBack);

            yield return Timing.WaitForSeconds(0.24f);
        }
    }
}