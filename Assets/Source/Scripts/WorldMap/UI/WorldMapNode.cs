using UnityEngine;

namespace CrossBlitz.WorldMap.UI
{
    public class WorldMapNode : MonoBehaviour
    {
        public SpriteAnimation animator;
        public GameObject shadow;

        private void Start()
        {
            shadow.SetActive(false);
            animator.Play("spawn", OnSpawning);
        }

        private void OnSpawning(int frame)
        {
            if (frame == 3) shadow.SetActive(true);
        }
    }
}