using Steer2D;
using UnityEngine;

namespace CrossBlitz.WorldMap
{
    public class WorldMapMover : SteeringBehaviour
    {
        // private float _minSpeed = 0.08f;
        // private float _maxSpeed = 0.16f;
        private float _moveSpeed;

        private void Awake()
        {
            _moveSpeed = 0.08f; //Random.Range(_minSpeed, _maxSpeed);
        }

        public override Vector2 GetVelocity()
        {
            return new Vector2(_moveSpeed, 0);
        }
    }
}
