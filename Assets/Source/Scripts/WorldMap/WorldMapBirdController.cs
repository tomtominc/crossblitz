using System.Collections.Generic;
using UnityEngine;

namespace CrossBlitz.WorldMap
{
    public class WorldMapBirdController : MonoBehaviour
    {
        public float spawnDelay = 10;
        public WorldMapBirdGroup birdGroupPrefab;
        public List<Transform> spawnPoints;

        private float _spawnTimer;
        private Transform _lastSpawnPoint;

        private void Start()
        {
            _spawnTimer = spawnDelay;
            _lastSpawnPoint = null;
        }

        private void Update()
        {
            _spawnTimer += Time.deltaTime;
            if (_spawnTimer >= spawnDelay)
            {
                var randomSpawnPoint = _lastSpawnPoint;

                while (randomSpawnPoint == _lastSpawnPoint)
                {
                    randomSpawnPoint = spawnPoints[Random.Range(0, spawnPoints.Count)];
                }

                var birdGroup = Instantiate(birdGroupPrefab, transform);
                var spawnPosition = randomSpawnPoint.position;
                birdGroup.transform.position = spawnPosition;
                birdGroup.SetupBirdTarget(new Vector2(-spawnPosition.x, -spawnPosition.y));

                _lastSpawnPoint = randomSpawnPoint;

                _spawnTimer = 0;
            }
        }
    }
}