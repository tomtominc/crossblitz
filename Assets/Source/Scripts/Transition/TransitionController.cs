
using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Audio;
using DG.Tweening;
using MEC;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Transition
{
    public class TransitionController : MonoBehaviour
    {
        private const float TransitionTime = 1;
        private const float BlackFadeTime = 0.25f;
        private static TransitionController _instance;

        public bool printWaitingBundles;
        //public Camera transitionCamera;
        public Canvas canvas;
        public CanvasGroup blackOverlay;
        public GameObject loadingScreen;
        public RectTransform hexTransitionContainer;
        public HexTransitionObject blackHexTile;
        public CanvasGroup flash;
        public Image flashImage;
        public BlendModeEffect flashBlendMode;
        public Animator blackXTransition;
        public Animator blackXTransitionOut;
        public SpriteAnimation hourglass;
        public SpriteAnimation saveIcon;
        //public List<Transitioner> transitions;

        // public static List<IAssetLoader> AssetsToUnload;
        // public static List<IAssetLoader> AssetsToWaitFor;

        private static bool m_canTransition=true;


        public void Awake()
        {
            _instance = this;
            _instance.blackOverlay.SetActive(true);
            _instance.blackOverlay.alpha = 1;
            //SetupHexTiles();
            // AssetsToUnload = new List<IAssetLoader>();
            // AssetsToWaitFor = new List<IAssetLoader>();
        }

        public static void FlashScreen()
        {
            FlashScreen(Color.white, BlendMode.Screen);
        }

        public static void FlashScreen(Color color, BlendMode blendMode)
        {
            DOTween.Kill(_instance.flash);

            _instance.flash.SetActive(true);
            _instance.flash.alpha = 1;
            _instance.flashBlendMode.BlendMode = blendMode;
            _instance.flashImage.color = color;
            _instance.flash.DOFade(0, 0.1f). OnComplete(() =>
            {
                _instance.flash.SetActive(false);
            });
        }

        #if UNITY_EDITOR

        // [Button("Check Loaders")]
        // public void CheckLoaders()
        // {
        //     var assessment = string.Empty;
        //
        //     // if (AssetsToUnload != null)
        //     // {
        //     //     assessment += "===== ASSETS TO UNLOAD =====\n";
        //     //     for (int i = 0; i < AssetsToUnload.Count; i++)
        //     //     {
        //     //         assessment += $"> {AssetsToUnload[i]}\n";
        //     //     }
        //     // }
        //     //
        //     // if (AssetsToWaitFor != null)
        //     // {
        //     //     assessment += "===== ASSETS TO WAIT FOR =====\n";
        //     //     for (int i = 0; i < AssetsToWaitFor.Count; i++)
        //     //     {
        //     //         assessment += $"> {AssetsToWaitFor[i]}\n";
        //     //     }
        //     // }
        //     //
        //     // Debug.Log(assessment);
        // }

        #endif

        // [Button]
        // public void PlayTransition(string transitionName)
        // {
        //     Timing.RunCoroutine(PlayTransitionWithName(transitionName));
        // }

        // private IEnumerator<float> PlayTransitionWithName(string transitionName)
        // {
        //     var transition = Instantiate(transitions.Find(x => x.name == transitionName), transform);
        //     transition._transitionCamera = transitionCamera;
        //     transition.TransitionOutWithoutChangingScene();
        //     while(!transition.CanTransition()) yield return Timing.WaitForOneFrame;
        //     transition.TransitionInWithoutChangingScene();
        // }

        public enum TransitionType
        {
            Fade,
            Cross,
            Hex
        }

        private static void SpawnHourGlass()
        {
            _instance.hourglass.SetActive(true);
            _instance.hourglass.GetComponent<CanvasGroup>().alpha = 0;
            _instance.hourglass.GetComponent<CanvasGroup>().DOFade(1, 0.12f);
            _instance.hourglass.Play("idle");
        }

        private static void SpawnSaveIcon()
        {
            _instance.saveIcon.SetActive(true);
            _instance.saveIcon.GetComponent<CanvasGroup>().alpha = 0;
            _instance.saveIcon.GetComponent<CanvasGroup>().DOFade(1, 0.12f);
            _instance.saveIcon.Play("idle");
        }

        public static IEnumerator<float> TransitionIn(TransitionType transitionType)
        {
            if (!m_canTransition)
            {
                yield break;
            }

            m_canTransition = false;

            switch (transitionType)
            {
                case TransitionType.Fade:
                    _instance.blackOverlay.SetActive(true);
                    _instance.blackOverlay.alpha = 0;
                    _instance.blackOverlay.DOFade(1, 0.2f);
                    yield return Timing.WaitForSeconds(0.1f);
                    SpawnHourGlass();
                    yield return Timing.WaitForSeconds(0.1f);
                    break;
                case TransitionType.Cross:
                    _instance.blackXTransition.SetActive(true);
                    _instance.blackXTransition.Play("TransitionIn", -1);
                    AudioController.PlaySound("battle_intro_cross", "BARDSFX");
                    yield return Timing.WaitForSeconds(1.133f);
                    SpawnHourGlass();
                    break;
                case TransitionType.Hex:
                    Debug.Log("HEX TRANSITION IN");
                    var hexTimer = 0f;
                    var foundAll = false;
                    var foundCount = 0;
                    var childCount = _instance.hexTransitionContainer.transform.childCount;
                    var uniqueHex = new List<int>();

                    while (!foundAll)
                    {
                        hexTimer += Time.deltaTime;

                        var randomVal = Random.Range(0, childCount);
                        var hex = _instance.hexTransitionContainer.transform.GetChild(randomVal);
                        if (!uniqueHex.Contains(randomVal) && !hex.IsActive())
                        {
                            uniqueHex.Add(randomVal);
                            hex.SetActive(true);
                            foundCount++;

                            if (foundCount == childCount) foundAll = true;
                            yield return Timing.WaitForSeconds(0.0025f);
                            //yield return Timing.WaitForOneFrame;
                        }

                        if (foundCount == childCount)
                        {
                            var hexAnimator = hex.GetComponent<SpriteAnimation>();
                            yield return Timing.WaitUntilTrue(() => hexAnimator.IsDone);
                        }

                        if(hexTimer >= 3f)
                        {
                            for(int x = 0; x < childCount; x++)
                            {
                                hex = _instance.hexTransitionContainer.transform.GetChild(x);
                                hex.SetActive(true);
                            }
                            foundAll = true;
                            hexTimer = 0;
                            Debug.LogError("FORCE QUIT HEX TRANSITION");
                        }
                    }

                    SpawnHourGlass();
                    break;
            }

            // for (var i = 0; i < AssetsToUnload.Count; i++)
            // {
            //     if (AssetsToUnload[i] != null)
            //     {
            //         AssetsToUnload[i].Unload();
            //     }
            // }
            //
            // AssetsToUnload.Clear();
        }

        public static IEnumerator<float> TransitionOut(TransitionType transitionType)
        {
            switch (transitionType)
            {
                case TransitionType.Fade:
                    _instance.blackOverlay.SetActive(true);
                    _instance.blackOverlay.DOFade(0, 0.2f);
                    yield return Timing.WaitForSeconds(0.2f);
                    _instance.hourglass.GetComponent<CanvasGroup>().DOFade(0, 0.24f);
                    yield return Timing.WaitForSeconds(0.24f);
                    _instance.hourglass.SetActive(false);
                    break;

                case TransitionType.Cross:
                    _instance.blackOverlay.SetActive(false);
                    _instance.hourglass.GetComponent<CanvasGroup>().DOFade(0, 0.24f);
                    yield return Timing.WaitForSeconds(0.12f);
                    _instance.blackXTransition.SetActive(false);
                    _instance.blackXTransitionOut.SetActive(true);
                    _instance.blackXTransitionOut.Play("TransitionIn", -1);
                    AudioController.PlaySound("battle_intro_cross", "BARDSFX");
                    yield return Timing.WaitForSeconds(1.2f);
                    _instance.blackXTransitionOut.SetActive(false);
                    _instance.hourglass.SetActive(false);
                    break;

                case TransitionType.Hex:
                    Debug.Log("HEX TRANSITION OUT");
                    var hexTimer = 0f;
                    var foundAll = false;
                    var foundCount = 0;
                    var childCount = _instance.hexTransitionContainer.transform.childCount;
                    var uniqueHex = new List<int>();
                    var finalHex = new GameObject();

                    while (!foundAll)
                    {
                        hexTimer += Time.deltaTime;

                        var randomVal = Random.Range(0, childCount);
                        var hex = _instance.hexTransitionContainer.transform.GetChild(randomVal);

                        if (!uniqueHex.Contains(randomVal) && hex.IsActive())
                        {
                            uniqueHex.Add(randomVal);
                            hex.GetComponent<SpriteAnimation>().Play("transition-out", () => hex.SetActive(false));
                            foundCount++;
                            if (foundCount == childCount) foundAll = true;
                            yield return Timing.WaitForSeconds(0.0025f);
                        }

                        if(foundCount == childCount) yield return Timing.WaitUntilTrue(() => !hex.IsActive());

                        if (hexTimer >= 3f)
                        {
                            for (int x = 0; x < childCount; x++)
                            {
                                hex = _instance.hexTransitionContainer.transform.GetChild(x);
                                hex.SetActive(false);
                            }
                            foundAll = true;
                            hexTimer = 0;
                            Debug.LogError("FORCE QUIT HEX TRANSITION");
                        }
                    }

                    _instance.hourglass.SetActive(false);
                    break;
            }

            m_canTransition = true;
        }

        public void SetupHexTiles()
        {
            var height = 30;
            var width = 96;
            var halfWidth = width/2;

            var xx = 0;
            var yy = 0;

            for (int i = 0; i < 26; i++)
            {
                xx = width * i;
                yy = 0;

                while (xx >= 0)
                {
                    if (xx <= 672 && yy >= -360)
                    {
                        var hex_tile = Instantiate(blackHexTile, hexTransitionContainer);
                        hex_tile.RectTransform().anchoredPosition = new Vector2(xx, yy);
                    }
                    xx -= halfWidth;
                    yy -= height;
                }
            }
        }
    }
}
