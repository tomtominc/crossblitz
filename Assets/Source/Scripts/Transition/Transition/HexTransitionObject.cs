using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TakoBoyStudios.Events;
using Events = CrossBlitz.ClientAPI.GameLogic.EventSystem.Events;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;
using UnityEngine.UI;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.Fables;
using CrossBlitz.Card;

namespace CrossBlitz
{
    public class HexTransitionObject : MonoBehaviour
    {
        public Image hexImage;
        public int index;

        public void SetUpHex(Faction faction)
        {
            switch (faction)
            {
                case Faction.War:
                    if(index == 0) hexImage.color = "#B75252".ToColor();
                    else hexImage.color = "#CCA185".ToColor(); 
                    break;
                case Faction.Fortune:
                    if (index == 0) hexImage.color = "#D78A1D".ToColor();
                    else hexImage.color = "#EAB162".ToColor();
                    break;
                case Faction.Balance:
                    if (index == 0) hexImage.color = "#638AB6".ToColor();
                    else hexImage.color = "#B5CC93".ToColor();
                    break;
                case Faction.Chaos:
                    if (index == 0) hexImage.color = "#784B9B".ToColor();
                    else hexImage.color = "#CC85AB".ToColor();
                    break;
                default:
                    if (index == 0) hexImage.color = "#E4DCD3".ToColor();
                    else hexImage.color = "#C9B9A6".ToColor();
                    break;
            }
        }
    }
}
