using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TakoBoyStudios.Events;
using Events = CrossBlitz.ClientAPI.GameLogic.EventSystem.Events;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz
{
    public class SaveIconController : MonoBehaviour
    {
        public SpriteAnimation saveIcon;
        private CanvasGroup m_saveIconCanvas;
        private float m_fadeDelay;
        private float m_fadeSpeed;

        void Start()
        {
            Events.Subscribe(EventType.OnSpawnSaveIcon, OnSpawnSaveIcon);
            m_saveIconCanvas = saveIcon.GetComponent<CanvasGroup>();
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnSpawnSaveIcon, OnSpawnSaveIcon);
        }

        private void Update()
        {
            if (m_saveIconCanvas.alpha > 0)
            {
                m_fadeDelay -= Time.deltaTime;

                if (m_fadeDelay <= 0)
                {
                    m_saveIconCanvas.alpha -= m_fadeSpeed * Time.deltaTime;
                }
            }
        }

        private void OnSpawnSaveIcon(IMessage message)
        {
            saveIcon.SetActive(true);
            m_saveIconCanvas.alpha = 1;
            m_fadeDelay = 2f;
            m_fadeSpeed = 1f / 0.12f;

            if (!saveIcon.playing)
            {
                saveIcon.Play("idle");
            }
        }
    }
}
