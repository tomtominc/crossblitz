using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossBlitz
{
    public class TransitionHex : MonoBehaviour
    {
        SpriteAnimation hexTile;

        // Start is called before the first frame update
        void Awake()
        {
            hexTile = GetComponent<SpriteAnimation>();
        }

        public void Play(bool transitionIn)
        {
            if (transitionIn)
            {
                hexTile.Play("transition-in");
            }
            else
            {
                hexTile.Play("transition-out",() => Destroy(gameObject));
            }
        }
    }
}
