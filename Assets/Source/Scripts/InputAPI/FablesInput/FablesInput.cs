﻿using System;
using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Fables;
using CrossBlitz.Fables.Cutscene;
using CrossBlitz.Fables.DeckViewInputApi;
using CrossBlitz.Fables.EventDialogueAPI;
using CrossBlitz.Fables.Grid;
using CrossBlitz.Fables.MapDialogueAPI;
using CrossBlitz.Shop;
using Sirenix.OdinInspector;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.InputAPI
{
    public class FablesInput : InputProcessor
    {
        public enum Mode
        {
            None,
            Explore,
            Dialogue,
            MapDialogue,
            Editor,
            Cutscene,
            DeckView,
            ManaMeld,
            CardShop,
            RelicShop
        }

        public static FablesInput Instance;

        public Mode currentMode;

        [BoxGroup("Inputs")] public ExploreMenu exploreInput;
        [BoxGroup("Inputs")] public SceneLoadInputMode eventSceneInput;
        [BoxGroup("Inputs")] public SceneLoadInputMode cutsceneInput;
        [BoxGroup("Inputs")] public MapDialogue mapDialogueInput;
        [BoxGroup("Inputs")] public DeckViewInput deckViewInput;
        [BoxGroup("Inputs")] public ManaMeldInput manaMeldInput;
        [BoxGroup("Inputs")] public HexGridEditor editorInput;
        [BoxGroup("Inputs")] public ShopUi shopInput;

        private Dictionary<Mode, InputMode> _inputModes;

        public event Action<Mode> OnChangedMode;

        private bool on = true;

        private void Awake()
        {
            if (Instance!=null && Instance != this)
            {
                Destroy(Instance.gameObject);
            }

            Instance = this;

            _inputModes = new Dictionary<Mode, InputMode>
            {
                {Mode.Explore, exploreInput.InitMode(this)},
                {Mode.Dialogue, eventSceneInput.InitMode(this)},
                {Mode.MapDialogue, mapDialogueInput.InitMode(this)},
                {Mode.Editor,editorInput.InitMode(this)},
                {Mode.Cutscene, cutsceneInput.InitMode(this)},
                {Mode.DeckView, deckViewInput.InitMode(this)},
                {Mode.ManaMeld, manaMeldInput.InitMode(this)},
                {Mode.CardShop, shopInput.InitMode(this)},
                {Mode.RelicShop, shopInput},
            };
        }

        public override async void GoToMode(object mode)
        {
            if (_inputModes.ContainsKey(currentMode))
            {
                _inputModes[currentMode].EndMode();
            }

            var lastMode = currentMode;
            currentMode = (Mode) mode;
            OnChangedMode?.Invoke(currentMode);

            Events.Publish(this, EventType.OnFableInputStateChanged, new OnFableInputStateChangedEventArgs
            {
                inputMode = currentMode,
                lastMode = lastMode
            });

            if (_inputModes.ContainsKey(currentMode))
            {
                _inputModes[currentMode].StartMode();
            }
        }

        private void Update()
        {
            if (_inputModes.ContainsKey(currentMode))
            {
                _inputModes[currentMode].UpdateMode();
            }
        }
    }
}
