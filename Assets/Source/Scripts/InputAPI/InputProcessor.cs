﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InputProcessor : MonoBehaviour
{
    public virtual bool IsLeftMouseDown()
    {
        return Input.GetMouseButtonDown(0);
    }

    public virtual bool IsRightMouseDown()
    {
        return Input.GetMouseButtonDown(1);
    }

    public virtual Vector3 MousePosition()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    public virtual bool IsAcceptButtonPressed()
    {
        return Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space);
    }

    public virtual bool IsCancelButtonPressed()
    {
        return Input.GetKeyDown(KeyCode.Escape);
    }

    public virtual float GetHorizontialAxis()
    {
        return Input.GetAxis("Horizontal");
    }

    public virtual float GetVerticalAxis()
    {
        return Input.GetAxis("Vertical");
    }

    public virtual float GetHorizontialAxisRaw()
    {
        return Input.GetAxisRaw("Horizontal");
    }

    public virtual float GetVerticalAxisRaw()
    {
        return Input.GetAxisRaw("Vertical");
    }

    public virtual bool GetRightButtonDown()
    {
        return Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow);
    }

    public virtual bool GetLeftButtonDown()
    {
        return Input.GetKeyDown(KeyCode.A)|| Input.GetKeyDown(KeyCode.LeftArrow);
    }

    public virtual bool GetUpButtonDown()
    {
        return Input.GetKeyDown(KeyCode.W)|| Input.GetKeyDown(KeyCode.UpArrow);
    }

    public virtual bool GetDownButtonDown()
    {
        return Input.GetKeyDown(KeyCode.S)|| Input.GetKeyDown(KeyCode.DownArrow);
    }


    public virtual bool GetRightButton()
    {
        return Input.GetKey(KeyCode.D)|| Input.GetKey(KeyCode.RightArrow);
    }

    public virtual bool GetLeftButton()
    {
        return Input.GetKey(KeyCode.A)|| Input.GetKey(KeyCode.LeftArrow);
    }

    public virtual bool GetUpButton()
    {
        return Input.GetKey(KeyCode.W)|| Input.GetKey(KeyCode.UpArrow);
    }

    public virtual bool GetDownButton()
    {
        return Input.GetKey(KeyCode.S)|| Input.GetKey(KeyCode.DownArrow);
    }

    public virtual bool GetKeyDown(KeyCode key)
    {
        return Input.GetKeyDown(key);
    }

    public abstract void GoToMode(object mode);
}
