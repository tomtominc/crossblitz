using CrossBlitz.AddressablesAPI;
using CrossBlitz.Fables;
using CrossBlitz.Fables.Cutscene;
using CrossBlitz.Fables.EventDialogueAPI;
using CrossBlitz.Fables.Grid;
using CrossBlitz.PlayFab.Authentication;
using UnityEngine;

namespace CrossBlitz.InputAPI
{
    public class SceneLoadInputMode : InputMode
    {
        public enum LoadMode
        {
            None,
            Cutscene,
            EventScene
        }

        public LoadMode loadMode;

        public EventDialogueScreen _eventSceneInput;
        public CutsceneDialogueScreen _cutsceneInput;

        public EventDialogueScreen EventSceneInput => _eventSceneInput;
        public CutsceneDialogueScreen CutsceneInput => _cutsceneInput;

        public override void StartMode()
        {
            if (FableController.PlayerIsInAFable() && FablesMapController.Instance)
            {
                App.FableData.OnEnteredCutscene();
            }

            switch (loadMode)
            {
                case LoadMode.Cutscene:
                    LoadCutscene();
                    break;
                case LoadMode.EventScene:
                    LoadEventScene();
                    break;
            }
        }

        private async void LoadEventScene()
        {
            SceneModel scene;

            if (!SceneController.IsLoaded("Cutscene"))
            {
                scene = await SceneController.Instance.LoadScene(new SceneLoadParams {SceneToLoad = "Cutscene"});
            }
            else
            {
                scene = SceneController.GetScene("Cutscene");
            }
            scene.sceneCamera.depth = 20;
            _eventSceneInput = scene.GetSceneData<EventDialogueScreen>();
            _eventSceneInput.InitMode(InputProcessor);
            _eventSceneInput.StartMode();
        }

        private async void LoadCutscene()
        {
            SceneModel scene;

            if (!SceneController.IsLoaded("Cutscene"))
            {
                scene = await SceneController.Instance.LoadScene(new SceneLoadParams {SceneToLoad = "Cutscene"});
            }
            else
            {
                scene = SceneController.GetScene("Cutscene");
            }

            scene.sceneCamera.depth = 20;
            _cutsceneInput = scene.GetSceneData<CutsceneDialogueScreen>();
            _cutsceneInput.InitMode(InputProcessor);
            _cutsceneInput.StartMode();
        }

        public override void UpdateMode()
        {
            if (EventSceneInput) EventSceneInput.UpdateMode();
            if (CutsceneInput) CutsceneInput.UpdateMode();
        }

        public override async void EndMode()
        {
            if (FableController.PlayerIsInAFable() && FablesMapController.Instance)
            {
                App.FableData.OnExitCutscene();
            }

            if (SceneController.IsLoaded("Cutscene"))
            {
                await SceneController.Instance.UnloadScene("Cutscene");
            }

            if (EventSceneInput) EventSceneInput.EndMode();
            if (CutsceneInput) CutsceneInput.EndMode();
        }
    }
}