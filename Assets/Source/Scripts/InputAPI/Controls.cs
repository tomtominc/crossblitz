using System;
using System.Collections.Generic;
using UnityEngine;

namespace CrossBlitz.InputAPI
{
    public class Controls : MonoBehaviour
    {
        public enum Actions
        {
            None,
            AddCard,
            RemoveCard,
            Mark,
            Swap,
            TurnPageRight,
            TurnPageLeft,
            TurnPageRightAlt,
            TurnPageLeftAlt,
        }

        private static readonly Dictionary<Actions, KeyCode> PlayerActions = new Dictionary<Actions, KeyCode>
        {
            { Actions.AddCard, KeyCode.Mouse0 },
            { Actions.RemoveCard, KeyCode.Mouse0 },
            { Actions.Mark, KeyCode.M },
            { Actions.Swap, KeyCode.Mouse1 },
            { Actions.TurnPageRight, KeyCode.D },
            { Actions.TurnPageLeft, KeyCode.A },
            { Actions.TurnPageRightAlt, KeyCode.Mouse0 },
        };

        private static bool LeftClick;
        private static bool RightClick;

        public static KeyCode GetKey(Actions action)
        {
            return PlayerActions[action];
        }

        public static bool GetAction(Actions action)
        {
            // not assigned
            if (!PlayerActions.ContainsKey(action))
            {
                return false;
            }

            var keycode = PlayerActions[action];

            // special conditions for mouse input
            if (keycode == KeyCode.Mouse0)
            {
                return Input.GetMouseButtonDown(0);
            }

            if (keycode == KeyCode.Mouse1)
            {
                return Input.GetMouseButtonDown(1);
            }

            return Input.GetKeyDown(keycode);
        }

        public static void OnRightClick()
        {
            RightClick = true;
        }

        public static void OnLeftClick()
        {
            LeftClick = true;
        }

        public void LateUpdate()
        {
            LeftClick = false;
            RightClick = false;
        }
    }
}