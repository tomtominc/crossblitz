﻿using UnityEngine;

namespace CrossBlitz.InputAPI
{
    public abstract class InputMode : MonoBehaviour
    {
        private InputProcessor _inputProcessor;
        public InputProcessor InputProcessor => _inputProcessor;
        public virtual InputMode InitMode(InputProcessor inputProcessor)
        {
            _inputProcessor = inputProcessor;
            return this;
        }

        public abstract void StartMode();

        public abstract void UpdateMode();

        public abstract void EndMode();
    }
}
