using UnityEngine.AddressableAssets;

namespace CrossBlitz.AddressablesAPI
{
    [System.Serializable]
    public class AssetReferenceObject
    {
        public string name;
        public AssetReference reference;
    }
}