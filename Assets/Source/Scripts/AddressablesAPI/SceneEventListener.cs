using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Developer.D90;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Grid;
using CrossBlitz.InputAPI;
using CrossBlitz.Transition;
using CrossBlitz.ViewAPI.Popups;
using MEC;
using TakoBoyStudios.Events;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.AddressablesAPI
{
    public class SceneEventListener : MonoBehaviour
    {
        private void Awake()
        {
            Events.Subscribe(EventType.OpenNodeMetadataEditor, OnOpenNodeMetadataEditor);
            Events.Subscribe(EventType.OpenRoomEditor, OnOpenRoomEditor);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OpenNodeMetadataEditor, OnOpenNodeMetadataEditor);
            Events.Unsubscribe(EventType.OpenRoomEditor, OnOpenRoomEditor);
        }

        private async void OnOpenNodeMetadataEditor(IMessage message)
        {
            if (message.Data is OpenNodeMetadataEditorEventArgs args)
            {
                if (SceneController.IsLoaded("Tools"))
                {
                    return;
                }

                await SceneController.Instance.UnloadAllScenes();

                var toolsSceneLoad = new SceneLoadParams
                {
                    SceneToLoad = "Tools"
                };

                await SceneController.Instance.LoadScene(toolsSceneLoad);

                while (D90Tools.Instance == null) await UnityAsync.Await.NextUpdate();

                if (args.tile != null)
                {
                    Events.Publish(this, EventType.OpenTileInfo, new OpenTileEditorEventArgs
                    {
                        tileUid = args.tile.uid
                    });
                }
            }
        }

        private void OnOpenRoomEditor(IMessage message)
        {
            if (message.Data is OpenRoomEditorEventArgs args)
            {
                Timing.RunCoroutine(OpenRoomEditor(args));
            }
        }

        private IEnumerator<float> OpenRoomEditor(OpenRoomEditorEventArgs args)
        {
            if (SceneController.IsLoaded("FablesMap"))
            {
                PopupController.Open(new PopupInfo
                {
                    style = PopupWindowStyle.Dialogue,
                    body = "There is already a map open, please exit that map before opening a new one."
                });

                yield break;
            }

            yield return Timing.WaitUntilDone(TransitionController.TransitionIn(TransitionController.TransitionType.Fade));

            SceneController.Instance.UnloadAllScenes(new List<string> { "Main", "FablesMap" });

            var chapterData = Db.FablesDatabase.GetChapterByRoomUid( args.roomUid );
            var roomIndex = Db.MapDatabase.GetRoomIndex(args.roomUid);

            if (chapterData == null)
            {
                chapterData = FableChapterData.Fake(Db.MapDatabase.GetMapByRoomUid(args.roomUid).Uid);
            }

            var toolsSceneLoad = new SceneLoadParams
            {
                SceneToLoad = "FablesMap"
            };

            var toolsLoadTask =SceneController.Instance.LoadScene(toolsSceneLoad);
            yield return Timing.WaitUntilDone(toolsLoadTask.AsCoroutine());
            var toolsSceneModel = toolsLoadTask.Result;
            var fablesMapConfig = toolsSceneModel.GetComponent<FablesMapController>();
            fablesMapConfig.InitializeFable(FablesInput.Mode.Editor, chapterData, roomIndex);

            Timing.RunCoroutine(TransitionController.TransitionOut(TransitionController.TransitionType.Fade));
        }
    }
}