using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityAsync;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace CrossBlitz.AddressablesAPI
{
    public class SceneController : MonoBehaviour
    {
        private static SceneController _instance;

        public static SceneController Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<SceneController>();

                    if (_instance == null)
                    {
                        var go = new GameObject("SceneController", typeof(SceneController));
                        _instance = go.GetComponent<SceneController>();
                    }

                    _instance.Init();
                }

                return _instance;
            }
        }

        public static event Action<SceneModel> OnSceneLoaded;
        public static event Action<SceneModel> OnSceneUnloaded;

        public static Dictionary<string, AsyncOperationHandle<SceneInstance>> sceneLoadHandles;
       // public static int sceneCounter;
        //public static Queue<SceneModel> sceneQueue;

        private void Init()
        {
          //  sceneCounter = 0;
            Application.backgroundLoadingPriority = ThreadPriority.Low;

            DontDestroyOnLoad(gameObject);
            //sceneQueue = new Queue<SceneModel>();
            sceneLoadHandles = new Dictionary<string, AsyncOperationHandle<SceneInstance>>();
        }

        public static SceneModel GetScene(string sceneName)
        {
            return SceneModel.GetScene(sceneName + "SceneModel");
        }

        public static bool IsLoaded(string sceneName)
        {
            return sceneLoadHandles.ContainsKey(sceneName);
        }

        public async Task<SceneModel> LoadScene(SceneLoadParams loadParams)
        {
            var asyncOperationHandle = Addressables.LoadSceneAsync(loadParams.SceneToLoad, LoadSceneMode.Additive);

            if(sceneLoadHandles.ContainsKey(loadParams.SceneToLoad))
            {
                sceneLoadHandles.Remove(loadParams.SceneToLoad);
            }

            sceneLoadHandles.Add(loadParams.SceneToLoad, asyncOperationHandle);

            await sceneLoadHandles[loadParams.SceneToLoad].Task;

            if (sceneLoadHandles[loadParams.SceneToLoad].Status != AsyncOperationStatus.Succeeded)
            {
                Debug.LogError($"Unable to load scene {loadParams.SceneToLoad}");
                return null;
            }

            var scene = sceneLoadHandles[loadParams.SceneToLoad].Result.Scene;

            if (!SceneManager.SetActiveScene(scene))
            {
                Debug.LogError($"{loadParams.SceneToLoad} could not be set as the active scene.");
            }

            // wait 1 frame so we can setup the SceneConfigurationManager
            await Await.NextUpdate();

            // check to make sure there's a SceneConfigurationManager inside this scene.
            var sceneModel = SceneModel.GetOrCreateSceneModel($"{loadParams.SceneToLoad}SceneModel");

            // add the model to the scene queue
            //sceneQueue.Enqueue(sceneModel);
         //   sceneCounter++;

           // if (sceneCounter <= 0)
         //   {
           //     sceneCounter = 1;
          //  }

            if (sceneModel.sceneCamera)
            {
                // the scene camera's depth should be in front of other scenes.
                sceneModel.sceneCamera.depth = SceneManager.sceneCount;
            }

            if (sceneModel.sceneCameras != null)
            {
                // move through all the cameras and make their depth higher dependant on where it is in the list.
                for (var i = 0; i < sceneModel.sceneCameras.Length; i++)
                {
                    sceneModel.sceneCameras[i].depth = SceneManager.sceneCount + i;
                }
            }

            // initializing the configuration, this should be awaited since it may take a while.
            await sceneModel.InitializeWithConfiguration(scene,loadParams.Params);

            // call on scene loaded (only after init)
            OnSceneLoaded?.Invoke(sceneModel);

            return sceneModel;
        }

        public async Task UnloadScene(string sceneName)
        {
            if (!IsLoaded(sceneName))
            {
                for (var i = 0; i < SceneManager.sceneCount; i++)
                {
                    var scene = SceneManager.GetSceneAt(i);
                    if (scene.isLoaded && scene.name == sceneName)
                    {
                        await SceneManager.UnloadSceneAsync(scene);
                      //  sceneCounter--;
                    }
                }
                return;
            }

            var sceneModel = SceneModel.GetOrCreateSceneModel($"{sceneName}SceneModel");
            sceneModel.OnFocused(false);

            OnSceneUnloaded?.Invoke(sceneModel);

            var sceneHandle = sceneLoadHandles[sceneName];
            sceneLoadHandles.Remove(sceneName);
            await Addressables.UnloadSceneAsync(sceneHandle).Task;
           // sceneCounter--;
        }

        public async Task UnloadAllScenes(List<string> exceptions=null)
        {
            var sceneList = sceneLoadHandles.Keys.ToList();

            for (var i = sceneList.Count - 1; i > -1; i--)
            {
                if (exceptions !=null && exceptions.Contains(sceneList[i])) continue;

                await UnloadScene(sceneList[i]);
            }
        }
    }
}