using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine.SceneManagement;

namespace CrossBlitz.AddressablesAPI
{
    public interface ISceneConfiguration
    {
        Task OnCreated(SceneModel sceneModel);
        void OnFocused(bool hasFocus);
        void OnSceneLoaded(SceneModel model);
        void OnSceneUnLoaded(SceneModel model);
    }
}