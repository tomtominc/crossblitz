using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace CrossBlitz.AddressablesAPI
{
    public class SceneLoadParams
    {
        public string SceneToLoad;
        public LoadSceneMode LoadSceneMode;
        public Dictionary<string, object> Params;
    }
}