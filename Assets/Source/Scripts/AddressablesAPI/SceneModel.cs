using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CrossBlitz.Cameras;
using CrossBlitz.ClientAPI;
using CrossBlitz.Cursors;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ViewAPI;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;

namespace CrossBlitz.AddressablesAPI
{
    public class SceneModel : SerializedMonoBehaviour
    {
        [OnValueChanged("OnInitSelfValueChanged")]
        public bool initializeSelf;
        [OnValueChanged("OnNeedsToBeLoggedInChanged")]
        public bool needsToBeLoggedIn;
        [OnValueChanged("OnNeedsToBeConnectedToInternetChanged")]
        public bool needsToBeConnectedToInternet;
        [OnValueChanged("OnUsesDatabaseChanged")]
        public bool usesDatabase;
        public Camera sceneCamera;
        public Camera[] sceneCameras;
        public List<ISceneConfiguration> configs;
        public List<View> viewsInScene;
        [NonSerialized]
        public Dictionary<string, View> views;

        private Scene _scene;
        public Scene Scene => _scene;

        private Dictionary<string,object> _creationParams;

        private InternetVerifier _internetVerifier;
        private AuthVerifier _authVerifier;
        private DatabaseVerifier _databaseVerifier;

        protected virtual void Awake()
        {
            SceneManagers ??= new Dictionary<string, SceneModel>();

            if (!SceneManagers.ContainsKey(name))
            {
                SceneManagers.Add(name, this);
            }

            _internetVerifier = GetComponent<InternetVerifier>();
            _authVerifier = GetComponent<AuthVerifier>();
            _databaseVerifier = GetComponent<DatabaseVerifier>();

            InitializeViews();
            SetupCameras();

            //CrossBlitzInputModule.Instance.SetRenderCamera(sceneCamera);
        }

        private void Start()
        {
            if (initializeSelf)
            {
                LoadSelf();
            }
        }

        private async void LoadSelf()
        {
            await InitializeWithConfiguration(SceneManager.GetActiveScene(), null);
        }

        private void SetupCameras()
        {
            if (sceneCamera)
            {
                var camVar = sceneCamera.GetComponent<CameraVariant>();

                if (camVar == null)
                {
                    sceneCamera.gameObject.AddComponent<CameraVariant>();
                }
            }

            if (sceneCameras != null && sceneCameras.Length > 0)
            {
                for (var i = 0; i < sceneCameras.Length; i++)
                {
                    var camVar = sceneCameras[i].GetComponent<CameraVariant>();

                    if (camVar == null)
                    {
                        sceneCameras[i].gameObject.AddComponent<CameraVariant>();
                    }
                }
            }
        }

        public virtual T GetView<T>(string viewName) where T : View
        {
            if (!views.ContainsKey(viewName))
            {
                var tryFind = FindObjectOfType<GameplayScreen>(true);

                if (tryFind != null)
                {
                    views.Add(viewName, tryFind);
                }
                else
                {
                    Debug.LogError($"Could not find View {viewName} in scene: {_scene.name}.");
                    return null;
                }
            }

            return views[viewName] as T;
        }

        public virtual T GetSceneParameter<T>(string key)
        {
            if (_creationParams == null || _creationParams.ContainsKey(key)) return default;
            return (T)_creationParams[key];
        }

        public virtual T GetSceneData<T>() where T : ISceneConfiguration
        {
            return (T)configs.Find(x => x != null && x is T);
        }

        protected virtual void OnEnable()
        {
            SceneController.OnSceneLoaded += OnSceneLoaded;
            SceneController.OnSceneUnloaded += OnSceneUnLoaded;
        }

        protected virtual void OnDisable()
        {
            SceneController.OnSceneLoaded -= OnSceneLoaded;
            SceneController.OnSceneUnloaded -= OnSceneUnLoaded;

            if (SceneManagers.ContainsKey(name))
            {
                SceneManagers.Remove(name);
            }
        }

        public Camera GetCamera()
        {
            if (sceneCamera != null) return sceneCamera;
            if (sceneCameras != null && sceneCameras.Length > 0) return sceneCameras[0];
            return null;
        }

        public virtual async Task InitializeWithConfiguration(Scene scene, Dictionary<string,object> creationParams)
        {
            _scene = scene;
            _creationParams = creationParams;

            if (needsToBeConnectedToInternet)
            {
                await _internetVerifier.WaitUntilOnline();
            }

            // if (needsToBeLoggedIn)
            // {
            //     await _authVerifier.WaitUntilLoggedIn();
            // }

            if (usesDatabase)
            {
                await _databaseVerifier.WaitUntilDatabaseIsInitialized();
            }

            if (configs != null)
            {
                for (var i = 0; i < configs.Count; i++)
                {
                    var config = configs[i];
                    if (config == null) continue;

                    await  config.OnCreated(this);
                }
            }
        }

        public virtual void OnFocused(bool hasFocus)
        {
            if (configs == null) return;

            for (var i = 0; i < configs.Count; i++)
            {
                var config = configs[i];
                if (config == null) continue;
                config.OnFocused(hasFocus);
            }
        }

        protected virtual void OnSceneLoaded(SceneModel model)
        {
            if (configs == null) return;

            for (var i = 0; i < configs.Count; i++)
            {
                var config = configs[i];
                if (config == null) continue;
                config.OnSceneLoaded(model);
            }
        }

        protected virtual void OnSceneUnLoaded(SceneModel model)
        {
            if (configs == null) return;

            for (var i = 0; i < configs.Count; i++)
            {
                var config = configs[i];
                if (config == null) continue;
                config.OnSceneUnLoaded(model);
            }
        }

        private void InitializeViews()
        {
            views = new Dictionary<string, View>();

            for (var i = 0; i < viewsInScene?.Count; i++)
            {
                var view = viewsInScene[i];

                if (view == null) continue;

                if (!views.ContainsKey(view.name))
                {
                    views.Add(view.name, view);
                }
            }
        }

        #region Static Implementations

        public static Dictionary<string, SceneModel> SceneManagers;

        public static SceneModel GetScene(string sceneName)
        {
            if (SceneManagers.ContainsKey(sceneName)) return SceneManagers[sceneName];
            return null;
        }

        public static SceneModel GetOrCreateSceneModel(string configurationName)
        {
            if (!SceneManagers.ContainsKey(configurationName))
            {
                var configObject = new GameObject(configurationName, typeof(SceneModel));
                var sceneManager = configObject.GetComponent<SceneModel>();
                SceneManagers ??= new Dictionary<string, SceneModel>();

                if (!SceneManagers.ContainsKey(configurationName))
                {
                    SceneManagers.Add(configurationName, sceneManager);
                }
            }

            return SceneManagers[configurationName];
        }

        public static SceneModel GetCurrentSceneModel()
        {
            var currentScene = SceneManager.GetActiveScene();
            return GetOrCreateSceneModel(currentScene.name + "SceneModel");
        }

        #endregion

        #region Editor Only Implmentation

        [Button("Setup Scene Model")]
        public void AddAllSceneConfigs()
        {
            configs ??= new List<ISceneConfiguration>();

            var sceneConfigs = FindObjectsOfType<GameObject>();
            for (var i = 0; i < sceneConfigs.Length; i++)
            {
                var sceneConfig = sceneConfigs[i].GetComponent<ISceneConfiguration>();
                if (sceneConfig != null && !configs.Contains(sceneConfig))
                {
                    configs.Add(sceneConfig);
                }
            }

            viewsInScene = new List<View>();
            var foundViews = FindObjectsOfType<View>();
            for (var i = 0; i < foundViews.Length; i++)
            {
                viewsInScene.Add(foundViews[i]);
            }
        }


        private void OnNeedsToBeLoggedInChanged(bool value)
        {
            if (value)
            {
                var simpleLogin = GetComponent<AuthVerifier>();

                if (simpleLogin == null)
                {
                    gameObject.AddComponent<AuthVerifier>();
                }
            }
            else
            {
                var simpleLogin = GetComponent<AuthVerifier>();

                if (simpleLogin)
                {
                    DestroyImmediate(GetComponent<AuthVerifier>());
                }
            }
        }

        private void OnNeedsToBeConnectedToInternetChanged(bool value)
        {
            if (value)
            {
                var internetVerifier = GetComponent<InternetVerifier>();

                if (internetVerifier == null)
                {
                    gameObject.AddComponent<InternetVerifier>();
                }
            }
            else
            {
                var internetVerifier = GetComponent<InternetVerifier>();

                if (internetVerifier)
                {
                    DestroyImmediate(GetComponent<InternetVerifier>());
                }
            }
        }

        private void OnUsesDatabaseChanged(bool value)
        {
            if (value)
            {
                var internetVerifier = GetComponent<DatabaseVerifier>();

                if (internetVerifier == null)
                {
                    gameObject.AddComponent<DatabaseVerifier>();
                }
            }
            else
            {
                var internetVerifier = GetComponent<DatabaseVerifier>();

                if (internetVerifier)
                {
                    DestroyImmediate(GetComponent<DatabaseVerifier>());
                }
            }
        }

        private void OnInitSelfValueChanged(bool value)
        {
            if (value)
            {
                var authController = GetComponent<AuthenticationController>();

                if (authController == null)
                {
                    gameObject.AddComponent<AuthenticationController>();
                }
            }
            else
            {
                var authController = GetComponent<AuthenticationController>();

                if (authController)
                {
                    DestroyImmediate(GetComponent<AuthenticationController>());
                }
            }
        }

        #endregion
    }
}