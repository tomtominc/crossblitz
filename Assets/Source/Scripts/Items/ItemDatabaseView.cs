using System;
using System.Collections.Generic;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Hero;
using CrossBlitz.Items.Data;
using CrossBlitz.Shop.Data;
using CrossBlitz.UI.Widgets;
using PlayFab.ClientModels;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

namespace CrossBlitz.Items
{
    public class ItemDatabaseView : MonoBehaviour
    {
        [BoxGroup("General Item Properties")]
        public GameObject ingredientAnimator;
        [BoxGroup("General Item Properties")]
        public GameObject chestAnimator;
        [BoxGroup("General Item Properties")]
        public Image cardImage;
        [BoxGroup("General Item Properties")]
        public TextMeshProUGUI itemNameLabel;
        [BoxGroup("General Item Properties")]
        public TextMeshProUGUI descriptionLabel;

        [BoxGroup("Craft Ingredients List")]
        public GameObject ingredientsList;
        [BoxGroup("Craft Ingredients List")]
        public RectTransform ingredientsListLayout;

        [BoxGroup("Obtained From List")]
        public RectTransform obtainedFromList;

        [BoxGroup("Bundle Contents List")]
        public GameObject bundlesList;
        [BoxGroup("Bundle Contents List")]
        public RectTransform bundlesListLayout;

        [BoxGroup("Item Type")]
        public TextMeshProUGUI itemTypeLabel;

        [BoxGroup("Cost Container")]
        public RectTransform pricesLayout;

        [BoxGroup("General Prefabs")]
        public LayoutItem iconLayoutPrefab;

        [BoxGroup("Action Buttons")]
        public Button addButton;

        private string _playFabId;
        private ItemData _item;
        private string _storeLink;
        public event Action<ShopCategory, string> OnAddToStore;
        private void Start()
        {
            addButton.onClick.AddListener(AddButton);
        }

        public void SetupAddButton(string storeLink)
        {
            _storeLink = storeLink;
            addButton.SetActive(!string.IsNullOrEmpty(_storeLink));

            if (!string.IsNullOrEmpty(_storeLink))
            {
                SetupObtainedFrom(_playFabId, true);
            }
        }

        private void AddButton()
        {
            OnAddToStore?.Invoke( ShopsDatabase.GetShopCategoryFromItemType[_item.ItemClass], _item.ItemId);
            addButton.SetActive(false);
        }

        public async void SetItem(string storeToLink, string playFabId) //todo: add container link, quest link, other links!
        {
            _playFabId = playFabId;
            _storeLink = storeToLink;
            addButton.SetActive(!string.IsNullOrEmpty(_storeLink));
            _item = Db.ItemDatabase.GetItem(_playFabId);

            if (_item == null)
            {
                Debug.Log($"Item with Id {_playFabId} is null?");
                Destroy(gameObject);
                return;
            }

            itemNameLabel.text = _item.DisplayName;
            descriptionLabel.text = _item.Description;
            itemTypeLabel.text = _item.ItemClass;

            SetupCurrency(_item);
            SetupObtainedFrom(_playFabId);

            switch (_item.ItemClass)
            {
                case "Ingredient":
                {
                    cardImage.SetActive(false);
                    ingredientAnimator.SetActive(false);
                    bundlesList.SetActive(false);
                    chestAnimator.SetActive(false);
                    break;
                }
                case "Card":
                {
                    ingredientsList.SetActive(true);
                    bundlesList.SetActive(false);
                    cardImage.SetActive(true);
                    ingredientAnimator.SetActive(false);
                    chestAnimator.SetActive(false);

                    var cardData = Db.CardDatabase.GetCard(_playFabId);

                    SetupIngredients(cardData);

                    var atlas = await AddressableReferenceLoader.Load<SpriteAtlas>("cards-atlas");
                    cardImage.sprite  = atlas.GetSprite(cardData.portraitUrl);

                    break;
                }
                case "Chest":
                {
                    ingredientsList.SetActive(false);
                    bundlesList.SetActive(true);

                    cardImage.SetActive(false);
                    ingredientAnimator.SetActive(false);
                    chestAnimator.SetActive(true);

                    var bundledItems = new Dictionary<string, int>();

                    if (_item.Bundle != null && _item.Bundle.BundledItems != null)
                    {
                        for (var i = 0; i < _item.Bundle.BundledItems.Count; i++)
                        {
                            if (bundledItems.ContainsKey(_item.Bundle.BundledItems[i]))
                            {
                                bundledItems[_item.Bundle.BundledItems[i]]++;
                            }
                            else
                            {
                                bundledItems.Add(_item.Bundle.BundledItems[i], i);
                            }
                        }

                        foreach (var bundle in bundledItems)
                        {
                            var bundleItem = Instantiate(iconLayoutPrefab, bundlesListLayout, false);
                            bundleItem.SetLayoutItem(new LayoutItemData
                            {
                                title = $"x{bundle.Value} {bundle.Key}",
                                sideIconAnimation = "item"
                            });
                        }
                    }

                    if (_item.Bundle != null && _item.Bundle.BundledResultTables != null)
                    {
                        var bundledDropTable = new Dictionary<string, int>();

                        for (var i = 0; i < _item.Bundle.BundledResultTables.Count; i++)
                        {
                            if (bundledDropTable.ContainsKey(_item.Bundle.BundledResultTables[i]))
                            {
                                bundledDropTable[_item.Bundle.BundledResultTables[i]]++;
                            }
                            else
                            {
                                bundledDropTable.Add(_item.Bundle.BundledResultTables[i], i);
                            }
                        }

                        foreach (var dropTable in bundledDropTable)
                        {
                            var bundleItem = Instantiate(iconLayoutPrefab, bundlesListLayout, false);
                            bundleItem.SetLayoutItem(new LayoutItemData
                            {
                                title = $"x{dropTable.Value} {dropTable.Key}",
                                sideIconAnimation = "random"
                            });
                        }
                    }

                    break;
                }
            }
        }

        private void SetupIngredients(CardData cardData)
        {
            if (!cardData.craftingData.craftable)
            {
                var ingredientItem = Instantiate(iconLayoutPrefab, ingredientsListLayout, false);
                ingredientItem.SetLayoutItem(new LayoutItemData
                {
                    title = "Not Craftable",
                    sideIconAnimation = "x-none"
                });
            }
            else
            {
                for (var i = 0; i < cardData.craftingData.ingredients.Count; i++)
                {
                    // var item = Db.ItemDatabase.GetItem(cardData.craftingData.ingredients[i].Ingredient);
                    // if (item !=null)
                    // {
                    //     var ingredientItem = Instantiate(iconLayoutPrefab, ingredientsListLayout, false);
                    //     ingredientItem.SetLayoutItem(new LayoutItemData
                    //     {
                    //         title = item.DisplayName,
                    //         sideIconAnimation = item.ItemImageUrl
                    //     });
                    // }
                }
            }
        }

        private void SetupCurrency(ItemData item)
        {
            if (item.VirtualCurrencyPrices == null) return;

            foreach (var currencyPair in item.VirtualCurrencyPrices)
            {
                var readableCurrencyName = Currency.GetReadableName[currencyPair.CurrencyCode];
                var iconName = Currency.GetIconName[currencyPair.CurrencyCode];
                var currencyItem = Instantiate(iconLayoutPrefab, pricesLayout, false);
                currencyItem.SetLayoutItem(new LayoutItemData
                {
                    title = $"x{currencyPair.Price} {readableCurrencyName}",
                    sideIconAnimation = iconName
                });
            }
        }

        private void SetupObtainedFrom(string playFabId, bool configureAddButtonOnly = false)
        {
            // foreach (var shop in Db.ShopsDatabase.Shops)
            // {
            //     foreach (var shopCategoryPair in shop.items)
            //     {
            //         var category = shopCategoryPair;
            //
            //         if (!category.enabled) continue;
            //
            //         if (category.items.Exists(itemId => itemId == playFabId))
            //         {
            //             if (!configureAddButtonOnly)
            //             {
            //                 var shopItem = Instantiate(iconLayoutPrefab, obtainedFromList, false);
            //                 shopItem.SetLayoutItem(new LayoutItemData
            //                 {
            //                     title = shop.displayName,
            //                     sideIconAnimation = "shop"
            //                 });
            //             }
            //
            //             if (_storeLink == shop.Uid)
            //             {
            //                 addButton.SetActive(false);
            //             }
            //         }
            //     }
            // }

            //todo: add "tile chests" as a way to obtain
            //todo: add "event presents" as a way to obtain
            //todo: add "quests" as a way to obtain
        }
    }
}