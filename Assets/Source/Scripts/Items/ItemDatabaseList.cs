using System;
using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.Developer.Tools;
using CrossBlitz.Shop.Data;
using UnityEngine;

namespace CrossBlitz.Items
{
    public class ItemDatabaseList : DatabaseEditorView
    {
        public RectTransform layout;
        public ItemDatabaseView itemViewPrefab;

        private Dictionary<string, ItemDatabaseView> _itemViews;

        public event Action<ShopCategory, string> OnItemLinked;

        public override void Open()
        {
            _itemViews ??= new Dictionary<string, ItemDatabaseView>();

            foreach (var itemPair in Db.ItemDatabase.Items)
            {
                if (_itemViews.ContainsKey(itemPair.ItemId))
                {
                    _itemViews[itemPair.ItemId].SetupAddButton(_options.itemContainerLink);
                }
                else
                {
                    var itemView = Instantiate(itemViewPrefab, layout, false);
                    itemView.SetItem(_options.itemContainerLink, itemPair.ItemId);
                    _itemViews.Add(itemPair.ItemId, itemView);

                    if (!string.IsNullOrEmpty(_options.itemContainerLink))
                    {
                        itemView.OnAddToStore += AddedToStore;
                    }
                }
            }
        }

        private void AddedToStore(ShopCategory category, string playFabId)
        {
            OnItemLinked?.Invoke(category, playFabId);
        }
    }
}