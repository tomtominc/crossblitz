using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.PlayFab.Data;
using Selectionator;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.Items.Data
{
    public static class ItemClassType
    {
        public const string Card = "Card";
        public const string Ingredient = "Ingredient";
        public const string CardBack = "CardBack";
        public const string HpUp = "HpUp";
        public const string Recipe = "Recipe";
        public const string ManaShard = "ManaShard";
    }

    /// <summary>
    /// A unique instance of an item in a user's inventory. Note, to retrieve additional information for an item such as Tags,
    /// Description that are the same across all instances of the item, a call to GetCatalogItems is required. The ItemID of can
    /// be matched to a catalog entry, which contains the additional information. Also note that Custom Data is only set when
    /// the User's specific instance has updated the CustomData via a call to UpdateUserInventoryItemCustomData. Other fields
    /// such as UnitPrice and UnitCurrency are only set when the item was granted via a purchase.
    /// </summary>
    [Serializable]
    public class ItemDataInstance
    {
        /// <summary>
        /// Unique identifier for the inventory item, as defined in the catalog.
        /// </summary>
        public string ItemId;
        /// <summary>
        /// Unique item identifier for this specific instance of the item.
        /// </summary>
        public string ItemInstanceId;
        /// <summary>
        /// Class name for the inventory item, as defined in the catalog.
        /// </summary>
        public string ItemClass;
        /// <summary>
        /// Was the item purchased?
        /// </summary>
        public bool Purchased;
        /// <summary>
        /// Currency type for the cost of the catalog item. Not available when granting items.
        /// </summary>
        public string UnitCurrency;
        /// <summary>
        /// Cost of the catalog item in the given currency. Not available when granting items.
        /// </summary>
        public int UnitPrice;
        /// <summary>
        /// Game specific comment associated with this instance when it was added to the user inventory.
        /// </summary>
        public string Annotation;
        /// <summary>
        /// Timestamp for when this instance was purchased.
        /// </summary>
        public DateTime PurchaseDate;
        /// <summary>
        /// Total number of remaining uses, if this is a consumable item.
        /// </summary>
        public int RemainingUses;
        /// <summary>
        /// Array of unique items that were awarded when this catalog item was purchased.
        /// </summary>
        public List<string> BundleContents;
        /// <summary>
        /// Unique identifier for the parent inventory item, as defined in the catalog, for object which were added from a bundle or
        /// container.
        /// </summary>
        public string BundleParent;
        /// <summary>
        /// Catalog version for the inventory item, when this instance was created.
        /// </summary>
        public string CatalogVersion;
        /// <summary>
        /// A set of custom key-value pairs on the instance of the inventory item, which is not to be confused with the catalog
        /// item's custom data.
        /// </summary>
        public Dictionary<string,string> CustomData;
        /// <summary>
        /// CatalogItem.DisplayName at the time this item was purchased.
        /// </summary>
        public string DisplayName;
        /// <summary>
        /// Timestamp for when this instance will expire.
        /// </summary>
        public DateTime Expiration;
        /// <summary>
        /// The number of uses that were added or removed to this item in this call.
        /// </summary>
        public int UsesIncrementedBy;
        /// <summary>
        /// The receipt for this item, merges will use the last one.
        /// </summary>
        public string Receipt;
        /// <summary>
        /// Is this item new? Items that are granted and the player does not have one will get this tag
        /// The items is new tag will need to be removed manually
        /// </summary>
        public bool IsNew=true;

        public static ItemDataInstance Convert(ItemData item, ItemAcquisitionData acquisition)
        {
            var instance = new ItemDataInstance();
            instance.ItemId = item.ItemId;
            instance.ItemClass = item.ItemClass;
            instance.ItemInstanceId = GUID.CreateUnique();
            instance.PurchaseDate = DateTime.UtcNow;
            instance.RemainingUses = 1;
            instance.CatalogVersion = item.CatalogVersion;
            instance.CustomData = new Dictionary<string, string>();
            instance.DisplayName = item.DisplayName;
            instance.UnitCurrency = acquisition.PurchasedCurrency;
            instance.UnitPrice = acquisition.PurchasedAmount;
            instance.Purchased = acquisition.Purchased;
            instance.Annotation = acquisition.Annotation.ToString();
            instance.Receipt = acquisition.Receipt;
            return instance;
        }

        public void Merge(ItemDataInstance instance)
        {
            PurchaseDate = DateTime.UtcNow;
            RemainingUses += instance.RemainingUses;
            Receipt = instance.Receipt;
        }
    }
    [Serializable]
    public class ItemData
    {
        /// <summary>
        /// unique identifier for this item
        /// </summary>
        [FoldoutGroup("Properties")][Tooltip("Text name for the item, to show in-game.")]
        public string ItemId;
        /// <summary>
        /// class to which the item belongs
        /// </summary>
        [FoldoutGroup("Properties")][Tooltip("Text name for the item, to show in-game.")]
        public string ItemClass;
        /// <summary>
        /// URL to the item image. For Facebook purchase to display the image on the item purchase page, this must be set to an HTTP
        /// URL.
        /// </summary>
        [FoldoutGroup("Properties")][Tooltip("URL to the item image.")]
        public string ItemImageUrl;
        /// <summary>
        /// list of item tags
        /// </summary>
        [FoldoutGroup("Properties")][Tooltip("List of item tags. Used for various things in game.")]
        public List<string> Tags;
        /// <summary>
        /// text name for the item, to show in-game
        /// </summary>
        [FoldoutGroup("Basic Information")][Tooltip("Text name for the item, to show in-game.")]
        public string DisplayName;
        /// <summary>
        /// text description of item, to show in-game
        /// </summary>
        [FoldoutGroup("Basic Information")][Tooltip("Text description of item, to show in-game.")]
        public string Description;
        /// <summary>
        /// catalog version for this item
        /// </summary>
        [FoldoutGroup("Basic Information")][Tooltip("Catalog version for this item.")]
        public string CatalogVersion;
        /// <summary>
        /// defines the bundle properties for the item - bundles are items which contain other items, including random drop tables
        /// and virtual currencies
        /// </summary>
        [FoldoutGroup("Bundle Information")][Tooltip("Defines the bundle properties for the item - bundles are items which contain other items, including random drop tables and virtual currencies")]
        public ItemBundleInfo Bundle;
        /// <summary>
        /// defines the consumable properties (number of uses, timeout) for the item
        /// </summary>
        [FoldoutGroup("Consumable Information")][Tooltip("Defines the consumable properties (number of uses, timeout) for the item.")]
        public ItemConsumableInfo Consumable;
        /// <summary>
        /// defines the container properties for the item - what items it contains, including random drop tables and virtual
        /// currencies, and what item (if any) is required to open it via the UnlockContainerItem API
        /// </summary>
        [FoldoutGroup("Container Information")][Tooltip("Defines the container properties for the item - what items it contains, including random drop tables and virtual currencies, and what item (if any) is required to open it via the UnlockContainerItem API.")]
        public ItemContainerInfo Container;
        /// <summary>
        /// game specific custom data
        /// </summary>
        [FoldoutGroup("Advanced Information")][Tooltip("Game specific custom data in JSON format.")]
        public string CustomData;
        /// <summary>
        /// if true, then an item instance of this type can be used to grant a character to a user.
        /// </summary>
        [FoldoutGroup("Advanced Information")][Tooltip("If true, then an item instance of this type can be used to grant a character to a user.")]
        public bool CanBecomeCharacter;
        /// <summary>
        /// BETA: If true, then only a fixed number can ever be granted.
        /// </summary>
        [FoldoutGroup("Limited Edition")][Tooltip("BETA: If true, then only a fixed number can ever be granted.")]
        public bool IsLimitedEdition;
        /// <summary>
        /// If the item has IsLImitedEdition set to true, and this is the first time this ItemId has been defined as a limited
        /// edition item, this value determines the total number of instances to allocate for the title. Once this limit has been
        /// reached, no more instances of this ItemId can be created, and attempts to purchase or grant it will return a Result of
        /// false for that ItemId. If the item has already been defined to have a limited edition count, or if this value is less
        /// than zero, it will be ignored.
        /// </summary>
        [FoldoutGroup("Limited Edition")][ShowIf("IsLimitedEdition")][Tooltip("If the item has IsLImitedEdition set to true, and this is the first time this ItemId has been defined as a limited edition item, this value determines the total number of instances to allocate for the title. Once this limit has been reached, no more instances of this ItemId can be created, and attempts to purchase or grant it will return a Result of false for that ItemId. If the item has already been defined to have a limited edition count, or if this value is less than zero, it will be ignored.")]
        public int InitialLimitedEditionCount;
        /// <summary>
        /// if true, then only one item instance of this type will exist and its remaininguses will be incremented instead.
        /// RemainingUses will cap out at Int32.Max (2,147,483,647). All subsequent increases will be discarded
        /// </summary>
        [FoldoutGroup("Options")][Tooltip("if true, then only one item instance of this type will exist and its remaininguses will be incremented instead. RemainingUses will cap out at Int32.Max (2,147,483,647). All subsequent increases will be discarded.")]
        public bool IsStackable;
        /// <summary>
        /// if true, then an item instance of this type can be traded between players using the trading APIs
        /// </summary>
        [FoldoutGroup("Options")][Tooltip("if true, then an item instance of this type can be traded between players using the trading APIs")]
        public bool IsTradable;
        /// <summary>
        /// price of this item in virtual currencies and "RM" (the base Real Money purchase price, in USD pennies)
        /// </summary>
        [FoldoutGroup("Currencies")][Tooltip("Price of this item in virtual currencies and \"RM\" (the base Real Money purchase price, in USD pennies).")]
        public List<ItemCurrencyPrice> VirtualCurrencyPrices;
        /// <summary>
        /// override prices for this item for specific currencies
        /// </summary>
        [FoldoutGroup("Currencies")][Tooltip("Override prices for this item for specific currencies.")]
        public List<ItemCurrencyPrice> RealCurrencyPrices;

        public bool IsBundle()
        {
            // we don't use bundles in this game.
            return false;

            if (Bundle != null)
            {
                if (Bundle.BundledItems.Count > 0) return true;
                if (Bundle.BundledResultTables.Count > 0) return true;
                if (Bundle.BundledVirtualCurrencies.Count > 0) return true;
            }

            return false;
        }

        public bool IsContainer()
        {
            // we don't use containers in this game.
            return false;

            if (Container != null)
            {
                if (Container.ItemContents != null && Container.ItemContents.Count > 0) return true;
                if (Container.ResultTableContents != null && Container.ResultTableContents.Count > 0) return true;
                if (Container.VirtualCurrencyContents != null && Container.VirtualCurrencyContents.Count > 0) return true;
            }

            return false;
        }

        public AddItemResult OpenBundle(ItemAcquisitionData acquisition)
        {
            var result = new AddItemResult();
            result.AddedCurrencies = new Dictionary<string, int>();
            result.AddedItems = new List<ItemDataInstance>();

            if (IsBundle())
            {
                for (var i = 0; i < Bundle.BundledItems.Count; i++)
                {
                    result.AddedItems.Add(Db.ItemDatabase.CreateItemInstance(Bundle.BundledItems[i], acquisition));
                }

                for (var i = 0; i < Bundle.BundledResultTables.Count; i++)
                {
                    result.AddedItems.Add(Db.ItemDatabase.EvaluateDropTable(Bundle.BundledResultTables[i], acquisition));
                }

                for (var i = 0; i < Bundle.BundledVirtualCurrencies.Count; i++)
                {
                    result.AddedCurrencies.Add(Bundle.BundledVirtualCurrencies[i].CurrencyCode, Bundle.BundledVirtualCurrencies[i].Price);
                }
            }

            return result;
        }

        public int GetCurrencyPrice(string currencyCode)
        {
            if (ItemClass == "Card")
            {
                var card = Db.CardDatabase.GetCard(ItemId);

                if (card.type == CardType.Relic)
                {
                    return 1000;
                }

                if (card.type == CardType.Elder_Relic)
                {
                    return 3000;
                }

                switch (card.rarity)
                {
                    case Rarity.Common:
                        if (currencyCode == Currency.DAWN_DOLLARS) return 100;
                        break;
                    case Rarity.Rare:
                        if (currencyCode == Currency.DAWN_DOLLARS) return 200;
                        break;
                    case Rarity.Mythic:
                        if (currencyCode == Currency.DAWN_DOLLARS) return 500;
                        break;
                    case Rarity.Legendary:
                        if (currencyCode == Currency.DAWN_DOLLARS) return 1000;
                        break;
                }
            }

            var currency = VirtualCurrencyPrices.Find(c => c.CurrencyCode == currencyCode);
            if (currency == null) return -1;
            return currency.Price;
        }


// #if UNITY_EDITOR && ENABLE_PLAYFABADMIN_API
//         public static ItemData Convert(CatalogItem catalogItem)
//         {
//             var item = new ItemData
//             {
//                 Bundle = new ItemBundleInfo(),
//                 CanBecomeCharacter = catalogItem.CanBecomeCharacter,
//                 CatalogVersion = catalogItem.CatalogVersion,
//                 Consumable = new ItemConsumableInfo(),
//                 CustomData = catalogItem.CustomData,
//                 Description = catalogItem.Description,
//                 DisplayName = catalogItem.DisplayName,
//                 InitialLimitedEditionCount = catalogItem.InitialLimitedEditionCount,
//                 IsLimitedEdition = catalogItem.IsLimitedEdition,
//                 IsStackable = catalogItem.IsStackable,
//                 IsTradable = catalogItem.IsTradable,
//                 ItemClass = catalogItem.ItemClass,
//                 ItemId = catalogItem.ItemId,
//                 ItemImageUrl = catalogItem.ItemImageUrl,
//                 RealCurrencyPrices = ItemCurrencyPrice.Convert(catalogItem.RealCurrencyPrices),
//                 Tags = new List<string>(),
//                 VirtualCurrencyPrices = ItemCurrencyPrice.Convert(catalogItem.VirtualCurrencyPrices)
//             };
//
//             if (catalogItem.Bundle != null)
//             {
//                 item.Bundle.BundledItems = new List<string>();
//
//                 if (catalogItem.Bundle.BundledItems != null)
//                     item.Bundle.BundledItems = new List<string>(catalogItem.Bundle.BundledItems);
//
//                 item.Bundle.BundledResultTables = new List<string>();
//
//                 if (catalogItem.Bundle.BundledResultTables != null)
//                     item.Bundle.BundledResultTables = new List<string>(catalogItem.Bundle.BundledResultTables);
//
//                 item.Bundle.BundledVirtualCurrencies = ItemCurrencyPrice.Convert(catalogItem.Bundle.BundledVirtualCurrencies);
//             }
//
//             if (catalogItem.Consumable != null)
//             {
//                 item.Consumable.UsageCount = catalogItem.Consumable.UsageCount != null ? (int) catalogItem.Consumable.UsageCount : 0;
//                 item.Consumable.UsagePeriod = catalogItem.Consumable.UsagePeriod != null ? (int) catalogItem.Consumable.UsagePeriod : 0;
//                 item.Consumable.UsagePeriodGroup = catalogItem.Consumable.UsagePeriodGroup;
//             }
//
//             if (catalogItem.Container != null)
//             {
//                 item.Container = new ItemContainerInfo();
//                 item.Container.KeyItemId = catalogItem.Container.KeyItemId;
//
//                 if (catalogItem.Container.ItemContents != null)
//                     item.Container.ItemContents = new List<string>(catalogItem.Container.ItemContents);
//                 if (catalogItem.Container.ResultTableContents != null)
//                     item.Container.ResultTableContents = new List<string>(catalogItem.Container.ResultTableContents);
//                 if (catalogItem.Container.VirtualCurrencyContents != null)
//                     item.Container.VirtualCurrencyContents = ItemCurrencyPrice.Convert(catalogItem.Container.VirtualCurrencyContents);
//             }
//
//             if (catalogItem.Tags != null)
//             {
//                 item.Tags = new List<string>(catalogItem.Tags);
//             }
//
//             return item;
//         }
// #endif
    }

    [Serializable]
    public class ItemBundleInfo
    {
        /// <summary>
        /// unique ItemId values for all items which will be added to the player inventory when the bundle is added
        /// </summary>
        public List<string> BundledItems;
        /// <summary>
        /// unique TableId values for all RandomResultTable objects which are part of the bundle (random tables will be resolved and
        /// add the relevant items to the player inventory when the bundle is added)
        /// </summary>
        public List<string> BundledResultTables;
        /// <summary>
        /// virtual currency types and balances which will be added to the player inventory when the bundle is added
        /// </summary>
        public List<ItemCurrencyPrice> BundledVirtualCurrencies;
    }
    [Serializable]
    public class ItemConsumableInfo
    {
        /// <summary>
        /// number of times this object can be used, after which it will be removed from the player inventory
        /// </summary>
        public int UsageCount;
        /// <summary>
        /// duration in seconds for how long the item will remain in the player inventory - once elapsed, the item will be removed
        /// (recommended minimum value is 5 seconds, as lower values can cause the item to expire before operations depending on
        /// this item's details have completed)
        /// </summary>
        public int UsagePeriod;
        /// <summary>
        /// all inventory item instances in the player inventory sharing a non-null UsagePeriodGroup have their UsagePeriod values
        /// added together, and share the result - when that period has elapsed, all the items in the group will be removed
        /// </summary>
        public string UsagePeriodGroup;
    }
    [Serializable]
    public class ItemContainerInfo
    {
        /// <summary>
        /// unique ItemId values for all items which will be added to the player inventory, once the container has been unlocked
        /// </summary>
        public List<string> ItemContents;
        /// <summary>
        /// ItemId for the catalog item used to unlock the container, if any (if not specified, a call to UnlockContainerItem will
        /// open the container, adding the contents to the player inventory and currency balances)
        /// </summary>
        public string KeyItemId;
        /// <summary>
        /// unique TableId values for all RandomResultTable objects which are part of the container (once unlocked, random tables
        /// will be resolved and add the relevant items to the player inventory)
        /// </summary>
        public List<string> ResultTableContents;
        /// <summary>
        /// virtual currency types and balances which will be added to the player inventory when the container is unlocked
        /// </summary>
        public List<ItemCurrencyPrice> VirtualCurrencyContents;
    }
    [Serializable]
    public class ItemCurrencyPrice
    {
        public string CurrencyCode;
        public int Price;

        public static List<ItemCurrencyPrice> Convert(Dictionary<string, uint> currencies)
        {
            var itemCurrencies = new List<ItemCurrencyPrice>();

            if (currencies != null)
            {
                foreach (var currency in currencies)
                {
                    itemCurrencies.Add(new ItemCurrencyPrice
                    {
                        CurrencyCode = currency.Key,
                        Price = (int) currency.Value
                    });
                }
            }

            return itemCurrencies;
        }
    }

    [Serializable]
    public class DropTable
    {
        /// <summary>
        /// Unique name for this drop table
        /// </summary>
        public string TableId;
        /// <summary>
        /// Catalog version this table is associated with
        /// </summary>
        public string CatalogVersion;
        /// <summary>
        /// Child nodes that indicate what kind of drop table item this actually is.
        /// </summary>
        public List<DropTableNode> Nodes;


        // #if UNITY_EDITOR && ENABLE_PLAYFABADMIN_API
        // public static DropTable Convert(RandomResultTableListing table)
        // {
        //     var dropTable = new DropTable();
        //     dropTable.CatalogVersion = table.CatalogVersion;
        //     dropTable.TableId = table.TableId;
        //     dropTable.Nodes = new List<DropTableNode>();
        //     if (table.Nodes != null)
        //     {
        //         for (var i = 0; i < table.Nodes.Count; i++)
        //         {
        //             dropTable.Nodes.Add( new DropTableNode
        //             {
        //                 ResultItem = table.Nodes[i].ResultItem,
        //                 DropTableNodeType = (DropTableNodeType)(int)table.Nodes[i].ResultItemType,
        //                 Weight = table.Nodes[i].Weight
        //             });
        //         }
        //     }
        //
        //     return dropTable;
        // }
        // #endif
    }

    [Serializable]
    public class DropTableNode : ISelectable
    {
        /// <summary>
        /// Either an ItemId, or the TableId of another random result table
        /// </summary>
        public string ResultItem;
        /// <summary>
        /// Whether this entry in the table is an item or a link to another table
        /// </summary>
        public DropTableNodeType DropTableNodeType;
        /// <summary>
        /// How likely this is to be rolled - larger numbers add more weight
        /// </summary>
        public int Weight;

        public float SelectionWeight
        {
            get => Weight;
            set => Weight = (int)value;
        }
    }

    public enum DropTableNodeType
    {
        ItemId,
        TableId
    }

    [Serializable]
    public class ItemAcquisitionData
    {
        public enum AnnotationReason
        {
            NOT_VALID,
            PLAYER_SELECTED,
            ACQUIRED_AT_START,
            PURCHASED,
            GRANTED,
        }

        public bool Purchased;
        public string PurchasedCurrency;
        public int PurchasedAmount;
        public AnnotationReason Annotation;
        public string Receipt;

        public static string GetReceipt()
        {
            return Guid.NewGuid().ToString();
        }

        public bool IsValidItem()
        {
            return Guid.TryParse(Receipt, out var guid);
        }
    }

    public enum PurchaseErrorCode
    {
        NONE,
        RECEIPT_NOT_VALID,
        NOT_ENOUGH_CURRENCY,
        CANNOT_PURCHASE_USING_CURRENCY_TYPE
    }
}