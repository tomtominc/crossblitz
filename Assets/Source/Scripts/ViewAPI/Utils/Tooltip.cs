﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace CrossBlitz.ViewAPI.Utils
{
    public class Tooltip : View
    {
        public RectTransform container;
        public RectTransform arrow;
        public TextMeshProUGUI infoLabel;

        public void SetDetails(TooltipDetails details)
        {
            Vector2 size = details.content.rect.size;
            Vector2 position = details.content.GetPosition(CoordinateSystem.AsChildOfCanvas, true);

            container.sizeDelta = new Vector2(details.width, 80);
            infoLabel.text = details.info;
            infoLabel.ForceMeshUpdate();

            Canvas.ForceUpdateCanvases();

            //set the position to be equal to the content.
            container.SetPosition(position, CoordinateSystem.AsChildOfCanvas, true);

            // move the container up or down
            float offsetY = (size.y / 2) + (container.sizeDelta.y / 2) + (arrow.sizeDelta.y / 2);
            container.anchoredPosition = container.anchoredPosition + (Vector2.up * (details.direction == DisplayDirection.ABOVE ? offsetY : -offsetY));

            // set up arrow
            arrow.anchoredPosition = container.anchoredPosition + new Vector2(0f, ((container.sizeDelta.y / 2) + (arrow.sizeDelta.y / 2) - 4) * (details.direction == DisplayDirection.ABOVE ? -1 : 1));
            arrow.localScale = new Vector3(1, details.direction == DisplayDirection.ABOVE ? 1 : -1, 1);

        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                ViewController.Close(name);
            }
        }
    }

    public enum DisplayDirection
    {
        ABOVE,
        BELOW
    }

    public class TooltipDetails
    {
        public string info;
        public DisplayDirection direction;
        public RectTransform content;
        public float width;

        public TooltipDetails()
        {
            width = (Screen.width / 2f) - 20f;
        }
    }
}