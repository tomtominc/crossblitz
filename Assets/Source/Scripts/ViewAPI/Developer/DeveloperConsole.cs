﻿using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI;
using PlayFab;
using TMPro;

namespace CrossBlitz.ViewAPI.Developer
{
    public class DeveloperConsole : View
    {
        public UnityEngine.UI.Button onlineMatchButton;
        public UnityEngine.UI.Button aiMatchButton;

        public UnityEngine.UI.Button logoutButton;

        public TextMeshProUGUI consoleLog;

        public bool CreateOnlineMatch { get; set; }
        public bool CreateLocalMatch { get; set; }

        public override void Open()
        {
            SetupLog();

            logoutButton.onClick.AddListener(OnLogout);
            onlineMatchButton.onClick.AddListener(StartOnlineMatch);
        }

        private void StartOnlineMatch()
        {
            CreateOnlineMatch = true;
        }

        private void StartLocalMatch()
        {
            CreateLocalMatch = true;
        }

        private void OnLogout()
        {
            //Server.Logout();
        }

        private void SetupLog()
        {
            string log = string.Empty;

            log += string.Format("Title ID: <color=#d33b2f>{0}</color>\n", PlayFabSettings.TitleId);
            log += string.Format("Player ID: <color=#d33b2f>{0}</color>\n", App.AccountInfo.GetPlayerGuid());
            log += string.Format("Username: <color=#d33b2f>{0}</color>\n", App.AccountInfo.GetUsername());
            log += string.Format("Created Date: <color=#d33b2f>{0}</color>\n", App.AccountInfo.GetCreated().ToLocalTime());
            log += string.Format("Time Played: <color=#d33b2f>{0}</color>\n", App.AccountInfo.GetTimePlayedTotal());
            consoleLog.text = log;
        }
    }
}
