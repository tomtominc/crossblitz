﻿using System;
using System.Collections;
using System.Collections.Generic;
using Coffee.UIExtensions;
using DG.Tweening;
using MEC;
using TMPro;
using UnityEngine;

namespace CrossBlitz.ViewAPI.Developer
{
    public class ScreenNavigationPopup : View
    {
        public UIEffectCapturedImage captureBackground;
        public CanvasGroup backgroundCanvas;
        public CanvasGroup darkFadeCanvas;
        public RectTransform popupContent;

        public UnityEngine.UI.Button backButton;

        public RectTransform buttonLayout;
        public ButtonEvents screenNavigationButtonPrefab;
        public List<string> screens;

        public event Action OnStartClose;

        private Canvas _previous;
        private Tweener _scaleAnim;
        private Tweener _fadeAnim;
        private bool _closeByFade;
        private bool _openingNewScreen;

        public override void Open()
        {
            backButton.onClick.AddListener(BackButtonPressed);
            popupContent.localScale = Vector3.zero;
            backgroundCanvas.alpha = 0;
            captureBackground.Capture();

            for (int i = 0; i < screens.Count; i++)
            {
                ButtonEvents nav = Instantiate(screenNavigationButtonPrefab, buttonLayout, false);
                nav.name = screens[i];
                nav.GetComponentInChildren<TextMeshProUGUI>().text = screens[i];
                nav.OnClick += NavigateToScreen;
            }
        }

        private void NavigateToScreen(string screenName)
        {
            _closeByFade = true;

            ViewController.Close(_previous.name);
            ViewController.Close(name);
            darkFadeCanvas.blocksRaycasts = true;

            OpenNextScreen(screenName);
        }

        private async void OpenNextScreen(string screenName)
        {
            _openingNewScreen = true;
            await ViewController.OpenAsync<View>(screenName);
            _openingNewScreen = false;

        }

        private void BackButtonPressed()
        {
            _closeByFade = false;

            backButton.onClick.RemoveListener(BackButtonPressed);
            ViewController.Close(name);
        }

        public void SetPreviousCanvas(Canvas prev)
        {
            _previous = prev;

            _scaleAnim = popupContent.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutElastic);
            _fadeAnim = backgroundCanvas.DOFade(1, 0.5f);

            _fadeAnim.OnComplete(() => _previous.enabled = false);
        }

        public override IEnumerator<float> Close()
        {
            if (!_closeByFade)
            {
                OnStartClose?.Invoke();
            }

            _previous.enabled = true;

            TweenUtility.Kill(_scaleAnim, true);
            TweenUtility.Kill(_fadeAnim, true);


            if (_closeByFade)
            {
                yield return Timing.WaitUntilDone( darkFadeCanvas.DOFade(1, 1.5f).WaitForCompletion(true));
            }
            else
            {
                popupContent.DOScale(Vector3.zero, 0.25f).SetEase(Ease.InBack);
                yield return Timing.WaitUntilDone(  backgroundCanvas.DOFade(0, 0.25f).WaitForCompletion(true) );
            }

            backgroundCanvas.alpha = 0;
            captureBackground.Release();
            popupContent.SetActive(false);

            if (_closeByFade)
            {
                while (_openingNewScreen)
                {
                    yield return Timing.WaitForOneFrame;
                }

                yield return Timing.WaitUntilDone( darkFadeCanvas.DOFade(0, 1.5f).WaitForCompletion(true));
            }

            yield return Timing.WaitForOneFrame;

            Destroy();
        }
    }
}
