using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.ViewAPI
{
    public class MultiButtonContainer : MonoBehaviour
    {
        public List<ButtonContainer> buttons;
        private ButtonContainer _currentButton;

        public ButtonContainer CurrentButton => _currentButton;
        public event Action OnClick;

        private void Start()
        {
            for (var i = 0; i < buttons.Count; i++)
            {
                buttons[i].Button.onClick.AddListener(OnButtonClicked);
                buttons[i].SetActive(false);
            }

            SetActiveButton(0);
        }
        public void SetActiveButton(int index)
        {
            if (index < buttons.Count)
            {
                if (_currentButton == buttons[index]) return;
                if (_currentButton != null) _currentButton.SetActive(false);

                _currentButton = buttons[index];
                _currentButton.SetActive(true);
            }
        }

        private void OnButtonClicked()
        {
            OnClick?.Invoke();
        }

        [Button]
        public void AddChildButtonsToButtonList()
        {
            if (buttons == null) buttons = new List<ButtonContainer>();

            buttons.Clear();

            for (var i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i).GetComponent<ButtonContainer>();
                if (child != null)
                {
                    buttons.Add(child);
                }
            }
        }
    }
}