﻿
using BlendModes;
using CrossBlitz.Audio;
using CrossBlitz.Utils;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace CrossBlitz.ViewAPI
{
    [RequireComponent(typeof(UnityEngine.UI.Button))]
    [RequireComponent(typeof(CanvasGroup))]
    public class ButtonContainer : MonoBehaviour,
        IPointerEnterHandler,
        IPointerExitHandler,
        IPointerClickHandler,
        IPointerDownHandler,
        IPointerUpHandler,
        ICursorSelectable
    {
        public bool usesTextTexture;

        [ShowIf("usesTextTexture")] public Sprite normalTextTexture;
        [ShowIf("usesTextTexture")] public Sprite pressedTextTexture;

        public bool noTransform;

        public bool flashes;
        public bool glows;
        [ShowIf("flashes")] [ShowIf("glows")] public CanvasGroup flashCanvas;
        [ShowIf("flashes")] public int flashCount = 16;

        public bool fadeWhenNotClickable;
        public bool overlayWhenNotClickable;
        [ShowIf("overlayWhenNotClickable")] public CanvasGroup disabledOverlay;

        public bool notifies;
        [ShowIf("notifies")] public CanvasGroup notificationBadge;

        public string hoverSfx;
        public string clickSfx="UI-SingleClick";

        private Button _button;
        private RectTransform _overlay;
        private CanvasGroup _overlayCanvasGroup;
        private Image _textImage;
        private CanvasGroup _canvas;
        private bool glowIn;
        private bool glowOut;
        public Button Button
        {
            get
            {
                if (_button == null) _button = GetComponent<Button>();
                return _button;
            }
        }

        public Image TextImage => _textImage;


        public void OnEnable()
        {
            _button = GetComponent<UnityEngine.UI.Button>();
            _overlay = transform.Find("Overlay") as RectTransform;
            _overlayCanvasGroup = _overlay.GetComponent<CanvasGroup>();
            _canvas = GetComponent<CanvasGroup>();

            _overlay.anchoredPosition = Vector2.zero;
            _overlay.sizeDelta = this.RectTransform().sizeDelta;

            if (usesTextTexture)
            {
                _textImage = transform.Find("Label").GetComponent<Image>();
            }

            if (glows) glowIn = true;
        }

        public void SetInteractable(bool interactable)
        {
            if (_button == null || _canvas == null)
            {
                _button = GetComponent<UnityEngine.UI.Button>();
                _canvas = GetComponent<CanvasGroup>();
                if (_button == null || _canvas == null) return;
            }

            _button.interactable = interactable;
            _canvas.interactable = interactable;
            _canvas.blocksRaycasts = interactable;

            if (fadeWhenNotClickable)
            {
                _canvas.alpha = interactable ? 1 : 0.5f;
            }

            if (overlayWhenNotClickable)
            {
                disabledOverlay.SetActive(!interactable);
            }
        }

        public void SetAlpha(float alpha)
        {
            if (_button == null || _canvas == null)
            {
                _button = GetComponent<UnityEngine.UI.Button>();
                _canvas = GetComponent<CanvasGroup>();
                if (_button == null || _canvas == null) return;
            }

            _canvas.alpha = alpha;
        }

        public void FadeOut()
        {
            if (_button == null || _canvas == null)
            {
                _button = GetComponent<UnityEngine.UI.Button>();
                _canvas = GetComponent<CanvasGroup>();
                if (_button == null || _canvas == null) return;
            }

            _canvas.DOFade(0, 0.24f);
        }

        public void DisableAndFlash()
        {
            SetInteractable(false);

            Flash();
        }

        private void Flash()
        {
            if (flashes && flashCanvas)
            {
                flashCanvas.DOKill();
                flashCanvas.alpha = 0;
                flashCanvas.SetActive(true);
                flashCanvas.DOFade(1, 0.16f)
                    .SetEase(Ease.Flash)
                    .SetLoops(flashCount, LoopType.Yoyo)
                    .OnComplete(() =>
                    {
                        if (this)
                        {
                            flashCanvas.alpha = 0;
                            flashCanvas.SetActive(false);
                        }
                    });
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!string.IsNullOrEmpty(hoverSfx))
            {
                AudioController.PlaySound(hoverSfx);
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (!_button.interactable) return;

            if (!noTransform)
            {
                // Old Button Code
                _button.RectTransform().localScale = new Vector3(1.1f, 1.25f, 1);
                _button.RectTransform().DOScale(
                        new Vector3(1f, 1f, 1f), 0.25f)
                    .SetEase(Ease.OutBack);

                // New Robert Button Code
                // _button.RectTransform().DOScale (new Vector3(1f, 1f, 1f),0f);
                // _button.RectTransform().DOPunchScale(
                //         new Vector3(.25f, -.25f, .25f), 0.20f, 10)
                //     .SetEase(Ease.OutBack);
            }

            if (_overlay)
            {
                _overlayCanvasGroup.alpha = 1;
                _overlayCanvasGroup.DOFade(0f, 0.25f)
                    .SetDelay(0.25f);
            }

            if (flashes)
            {
                Flash();
            }

            if (!string.IsNullOrEmpty(clickSfx))
            {
                AudioController.PlaySound(clickSfx);
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
        }

        public void OnPointerUp(PointerEventData eventData)
        {
        }

        public void Notify(bool notify)
        {
            if (notificationBadge)
            {
                notificationBadge.SetActive(notify);

                if (notify)
                {
                    var anim = notificationBadge.GetComponent<SpriteAnimation>();
                    if (anim != null)
                    {
                        anim.Play("idle");
                    }
                }
            }
            else
            {
                Debug.LogError("Trying to notify but no notification badge has been setup.");
            }
        }


#if UNITY_EDITOR
        [Button("Configure Button")]
        public void CreateButton()
        {
            Image image = GetComponentInChildren<Image>();

            if (image == null)
            {
                Debug.LogError("Please add an image of a button as a child to configure!");
                return;
            }

            this.RectTransform().sizeDelta = image.rectTransform.sizeDelta;

            Transform overlaytrans = transform.Find("Overlay");

            if (!overlaytrans)
            {
                GameObject overlay = new GameObject("Overlay", typeof(Image), typeof(CanvasGroup));
                overlay.transform.SetParent(transform, false);

                Image overlayImage = overlay.GetComponent<Image>();
                overlayImage.sprite = image.sprite;
                overlayImage.raycastTarget = false;
                overlayImage.maskable = false;

                var rect = overlayImage.rectTransform;
                var imgRect = image.rectTransform;
                rect.sizeDelta = imgRect.sizeDelta;
                rect.anchoredPosition = imgRect.anchoredPosition;

                CanvasGroup canvasGroup = overlay.GetComponent<CanvasGroup>();
                canvasGroup.alpha = 0;
                canvasGroup.blocksRaycasts = false;
                canvasGroup.interactable = false;
            }

            if (flashes || glows)
            {
                GameObject fc = new GameObject("FlashCanvas", typeof(Image), typeof(CanvasGroup),
                    typeof(BlendModeEffect));
                fc.transform.SetParent(transform, false);

                Image overlayImage = fc.GetComponent<Image>();
                overlayImage.sprite = image.sprite;
                overlayImage.raycastTarget = false;
                overlayImage.maskable = false;

                var rect = overlayImage.rectTransform;
                var imgRect = image.rectTransform;
                rect.sizeDelta = imgRect.sizeDelta;
                rect.anchoredPosition = imgRect.anchoredPosition;

                CanvasGroup canvasGroup = fc.GetComponent<CanvasGroup>();
                canvasGroup.alpha = 0;
                canvasGroup.blocksRaycasts = false;
                canvasGroup.interactable = false;

                BlendModeEffect blendMode = fc.GetComponent<BlendModeEffect>();
                blendMode.BlendMode = BlendMode.Screen;

                flashCanvas = canvasGroup;
            }

            Button button = GetComponent<Button>();

            button.targetGraphic = image;
            button.transition = Selectable.Transition.SpriteSwap;

            SpriteState state = button.spriteState;
            state.highlightedSprite = image.sprite;
            state.disabledSprite = image.sprite;
            state.selectedSprite = image.sprite;
            string assetPath = UnityEditor.AssetDatabase.GetAssetPath(image.sprite);
            assetPath = assetPath.Remove(assetPath.IndexOf('@'), assetPath.Length - assetPath.IndexOf('@'));
            state.pressedSprite = UnityEditor.AssetDatabase.LoadAssetAtPath<Sprite>($"{assetPath}@pressed.png");
            button.spriteState = state;

            if (usesTextTexture)
            {
                _textImage = transform.Find("Label").GetComponent<Image>();

                if (!_textImage)
                {
                    Debug.Log("No Text Image under this button called 'Label'");
                    return;
                }

                _textImage.sprite = normalTextTexture;
                var textImagePath = UnityEditor.AssetDatabase.GetAssetPath(_textImage.sprite);
                pressedTextTexture =
                    UnityEditor.AssetDatabase.LoadAssetAtPath<Sprite>(assetPath.Replace("_", "-pressed_"));

                _textImage.SetNativeSize();

                var textButtonUpdater = gameObject.AddComponent<TextButtonUpdater>();
                textButtonUpdater.buttonContainer = this;
            }
        }

#endif
        public bool Interactable => _button.interactable;
        public bool Grabbable => false;
        public bool InfoOnly => false;
        public bool Loading => false;
    }
}