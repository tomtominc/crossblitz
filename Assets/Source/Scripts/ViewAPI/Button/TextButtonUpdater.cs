using UnityEngine;

namespace CrossBlitz.ViewAPI
{
    public class TextButtonUpdater : MonoBehaviour
    {
        public ButtonContainer buttonContainer;

        private void Update()
        {
            if (buttonContainer.Button.image.sprite == buttonContainer.Button.spriteState.selectedSprite)
            {
                if (buttonContainer.TextImage.sprite != buttonContainer.normalTextTexture)
                {
                    buttonContainer.TextImage.sprite = buttonContainer.normalTextTexture;
                }
            }
            else if (buttonContainer.Button.image.sprite == buttonContainer.Button.spriteState.pressedSprite)
            {
                if (buttonContainer.TextImage.sprite != buttonContainer.pressedTextTexture)
                {
                    buttonContainer.TextImage.sprite = buttonContainer.pressedTextTexture;
                }
            }
        }
    }
}