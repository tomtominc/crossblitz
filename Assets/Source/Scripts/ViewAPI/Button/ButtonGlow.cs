﻿using DG.Tweening;
using UnityEngine;


namespace CrossBlitz.ViewAPI
{

    [RequireComponent(typeof(CanvasGroup))]

    public class ButtonGlow : MonoBehaviour

    {

        public CanvasGroup flashCanvas;
        private bool glowIn = true;
        private bool glowOut = false;


        private void Update()
        {
            Glow();
        }


        private void Glow()
        {
            if (glowIn)
            {
                glowIn = false;

                flashCanvas.alpha = 0;
                flashCanvas.SetActive(true);
                flashCanvas.DOFade(1, 1f)
                    .SetEase(Ease.Flash)
                    .OnComplete(() =>
                    {
                        if (this)
                        {
                            glowOut = true;
                            glowIn = false;
                        }
                    });
            }

            if (glowOut)
            {
                glowOut = false;

                flashCanvas.alpha = 1;
                flashCanvas.SetActive(true);
                flashCanvas.DOFade(0, 1f)
                    .SetEase(Ease.Flash)
                    .OnComplete(() =>
                    {
                        if (this)
                        {
                            glowOut = false;
                            glowIn = true;
                        }
                    });
            }
        }
    }
}


