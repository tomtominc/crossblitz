﻿using System.Collections;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ViewAPI.Popups;
using UnityEngine;

namespace CrossBlitz.ViewAPI.Accounts
{
    public class AccountScreen : View
    {
        public AccountLoginDisplay loginDisplay;
        public AccountCreateDisplay createDisplay;
        public AccountForgotPasswordDisplay forgotPasswordDisplay;

        public override void Open()
        {
            SetLoginAccountDisplayActive();
            BlockUI(false);
        }

        public void LoginSuccessful()
        {
            if (loginDisplay.IsActive())
            {
                loginDisplay.HandleLoginSuccessful();
                App.SetCredentials(loginDisplay.email.field.text, loginDisplay.password.field.text);
            }

            if (createDisplay.IsActive())
            {
                createDisplay.HandleLoginSuccessful();
                App.SetCredentials(createDisplay.email.field.text, createDisplay.password.field.text);
            }

            ViewController.Close("AccountScreen");
        }

        public void LoginFailed(string details, FieldErrorType fieldError)
        {
            if (fieldError == FieldErrorType.All)
            {
                PopupController.Open(new PopupInfo
                {
                    title = "ERROR LOGGING IN!",
                    body = details,
                    confirm = "CONFIRM"
                });

                return;
            }

            if (loginDisplay.IsActive())
            {
                loginDisplay.HandleLoginError(details, fieldError);
            }

            if (createDisplay.IsActive())
            {
                createDisplay.HandleLoginError(details, fieldError);
            }
        }

        public void AccountRecoveryEmailSent()
        {
            //display success popup

            forgotPasswordDisplay.HandleSuccess();
        }

        public void AccountRecoveryEmailFailed(string details, FieldErrorType fieldError)
        {
            if (fieldError == FieldErrorType.All)
            {
                //display error popup
                return;
            }

            forgotPasswordDisplay.HandleFailed(details, fieldError);
        }

        public void OpenPrivacyPolicy()
        {
            Application.OpenURL("https://www.rivalsofaether.com/privacy-policy/");
        }

        public void SetCreateAccountDisplayActive()
        {
            BlockUI(true);
            StartCoroutine(SetScreenActive(createDisplay));
        }

        public void SetForgotPasswordDisplayActive()
        {
            BlockUI(true);
            StartCoroutine(SetScreenActive(forgotPasswordDisplay));
        }

        public void SetLoginAccountDisplayActive()
        {
            BlockUI(true);
            StartCoroutine(SetScreenActive(loginDisplay));
        }

        private IEnumerator SetScreenActive(AccountDisplay display)
        {
            yield return new WaitForSeconds(0.25f);

            if (loginDisplay.IsActive())
            {
                loginDisplay.SetActive(false);
            }

            if (forgotPasswordDisplay.IsActive())
            {
                forgotPasswordDisplay.SetActive(false);
            }

            if (createDisplay.IsActive())
            {
                createDisplay.SetActive(false);
            }


            BlockUI(false);

            display.SetActive(true);
            display.Open(this);

        }

        public void BlockUI(bool block)
        {
            LoadingPromptController.BlockUI(block);
        }

        public void CloseAccountScreen()
        {
            ViewController.Close("AccountScreen");
        }
    }
}
