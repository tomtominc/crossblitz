﻿using TMPro;
using UnityEngine;

namespace CrossBlitz.ViewAPI.Accounts
{
    public class AccountInputField : MonoBehaviour
    {
        public TMP_InputField field;
        public TextMeshProUGUI errorText;

        public RectTransform okImage;
        public RectTransform errorImage;

        public void Start()
        {
            ResetField();
        }

        public void ResetField()
        {
            okImage.SetActive(false);
            errorImage.SetActive(false);
            errorText.SetActive(false);
        }

        public void FieldIsOkay()
        {
            ResetField();

            if (okImage)
            {
                okImage.SetActive(true);
            }
        }

        public void SetErrorMessage(string errorMessage)
        {

            ResetField();

            if (errorImage)
            {
                errorImage.SetActive(true);
            }

            if (errorText)
            {
                errorText.SetActive(true);
                errorText.text = errorMessage;
            }
        }

    }
}
