﻿using CrossBlitz.ViewAPI.Utils;

namespace CrossBlitz.ViewAPI.Accounts
{
    public class AccountCreateDisplay : AccountDisplay
    {
        public AccountInputField email;
        public AccountInputField username;
        public AccountInputField password;
        public AccountInputField passwordConfirm;

        public UnityEngine.UI.Button createButton;
        public UnityEngine.UI.Button goToLoginButton;
        public UnityEngine.UI.Button privacyPolicyButton;

        public UnityEngine.UI.Button usernameInfoButton;
        public UnityEngine.UI.Button passwordInfoButton;

        public override void Open(AccountScreen _accountScreen)
        {
            base.Open(_accountScreen);

            createButton.onClick.AddListener(OnCreateButtonPressed);
            goToLoginButton.onClick.AddListener(accountScreen.SetLoginAccountDisplayActive);
            privacyPolicyButton.onClick.AddListener(accountScreen.OpenPrivacyPolicy);

            usernameInfoButton.onClick.AddListener(OnUsernameInfoButton);
            passwordInfoButton.onClick.AddListener(OnPasswordInfoButton);
        }

        private void OnDisable()
        {
            if (accountScreen == null)
                return;

            createButton.onClick.RemoveListener(OnCreateButtonPressed);
            goToLoginButton.onClick.RemoveListener(accountScreen.SetLoginAccountDisplayActive);
            privacyPolicyButton.onClick.RemoveListener(accountScreen.OpenPrivacyPolicy);

            usernameInfoButton.onClick.RemoveListener(OnUsernameInfoButton);
            passwordInfoButton.onClick.RemoveListener(OnPasswordInfoButton);
        }

        private async void OnUsernameInfoButton()
        {
            Tooltip tooltip = await ViewController.OpenAsync<Tooltip>("Tooltip");

            TooltipDetails details = new TooltipDetails
            {
                info = LocalizationController.Localize(LocalizationKeys.USERNAME_TOOLTIP,
                    "Username must be between <color=#d33b2f>3-15 characters.</color>\nNo special characters allowed, only [A-Z] and [0-9]."),
                direction = DisplayDirection.ABOVE,
                content = username.RectTransform(),
            };

            tooltip.SetDetails(details);
        }

        private async void OnPasswordInfoButton()
        {
            Tooltip tooltip = await ViewController.OpenAsync<Tooltip>("Tooltip");

            TooltipDetails details = new TooltipDetails
            {
                info = LocalizationController.Localize(LocalizationKeys.PASSWORD_TOOLTIP,
                    "Password must be between <color=#d33b2f>6-100 characters.</color>"),
                direction = DisplayDirection.ABOVE,
                content = password.RectTransform(),
            };

            tooltip.SetDetails(details);
        }

        private void Update()
        {
            if (!string.IsNullOrEmpty(password.field.text) && !string.IsNullOrEmpty(passwordConfirm.field.text))
            {
                if (!password.field.text.Equals(passwordConfirm.field.text))
                {
                    passwordConfirm.SetErrorMessage(LocalizationController.Localize(LocalizationKeys.PASSWORDS_DONT_MATCH_ERROR, "Does not match."));
                }
                else
                {
                    passwordConfirm.FieldIsOkay();
                }
            }
        }

        public void HandleLoginSuccessful()
        {
            username.FieldIsOkay();
            email.FieldIsOkay();
            password.FieldIsOkay();
            passwordConfirm.FieldIsOkay();
        }

        public void HandleLoginError(string errorMessage, FieldErrorType fieldError)
        {
            if (fieldError == FieldErrorType.Username)
            {
                username.SetErrorMessage(errorMessage);
            }
            else
            {
                username.ResetField();
            }

            if (fieldError == FieldErrorType.Email)
            {
                email.SetErrorMessage(errorMessage);
            }
            else
            {
                email.ResetField();
            }

            if (fieldError == FieldErrorType.Password)
            {
                password.SetErrorMessage(errorMessage);
            }
            else
            {
                password.ResetField();
            }
        }

        private void OnCreateButtonPressed()
        {
            PF_Authentication.RegisterNewPlayFabAccount(username.field.text, password.field.text, passwordConfirm.field.text, email.field.text);
        }
    }
}
