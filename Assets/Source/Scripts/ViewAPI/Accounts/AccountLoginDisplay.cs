﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.ViewAPI.Accounts
{
    public class AccountDisplay : MonoBehaviour
    {
        protected AccountScreen accountScreen;

        public virtual void Open(AccountScreen _accountScreen)
        {
            accountScreen = _accountScreen;

        }
    }

    public class AccountLoginDisplay : AccountDisplay
    {


        public AccountInputField email;
        public AccountInputField password;

        public UnityEngine.UI.Button loginButton;
        public UnityEngine.UI.Button createAccountButton;
        public UnityEngine.UI.Button privacyPolicyButton;
        public UnityEngine.UI.Button forgotPasswordButton;
        public UnityEngine.UI.Button closeButton;

        private Tweener forgotPasswordButtonMove;

        public override void Open(AccountScreen _accountScreen)
        {
            base.Open(_accountScreen);

            loginButton.onClick.AddListener(OnLoginButtonPressed);
            createAccountButton.onClick.AddListener(accountScreen.SetCreateAccountDisplayActive);
            forgotPasswordButton.onClick.AddListener(accountScreen.SetForgotPasswordDisplayActive);
            privacyPolicyButton.onClick.AddListener(accountScreen.OpenPrivacyPolicy);
            closeButton.onClick.AddListener(accountScreen.CloseAccountScreen);
        }

        private void OnDisable()
        {
            if (accountScreen == null)
                return;

            loginButton.onClick.RemoveListener(OnLoginButtonPressed);
            createAccountButton.onClick.RemoveListener(accountScreen.SetCreateAccountDisplayActive);
            forgotPasswordButton.onClick.RemoveListener(accountScreen.SetForgotPasswordDisplayActive);
            privacyPolicyButton.onClick.RemoveListener(accountScreen.OpenPrivacyPolicy);
        }

        public void HandleLoginSuccessful()
        {
            email.FieldIsOkay();
            password.FieldIsOkay();

            TweenUtility.Kill(forgotPasswordButtonMove, true);
            forgotPasswordButtonMove = forgotPasswordButton.RectTransform().DOAnchorPosY(13, 0.25f);
        }

        public void HandleLoginError(string errorMessage, FieldErrorType fieldError)
        {
            if (fieldError == FieldErrorType.Email || fieldError == FieldErrorType.Username)
            {
                email.SetErrorMessage(errorMessage);
            }
            else
            {
                email.ResetField();
            }

            if (fieldError == FieldErrorType.Password)
            {
                password.SetErrorMessage(errorMessage);

                TweenUtility.Kill(forgotPasswordButtonMove, true);
                forgotPasswordButtonMove = forgotPasswordButton.RectTransform().DOAnchorPosY(5, 0.25f);
            }
            else
            {
                password.ResetField();

                TweenUtility.Kill(forgotPasswordButtonMove, true);
                forgotPasswordButtonMove = forgotPasswordButton.RectTransform().DOAnchorPosY(13, 0.25f);
            }
        }

        private void OnLoginButtonPressed()
        {
            PF_Authentication.LoginWithEmail(email.field.text, password.field.text);
        }
    }
}