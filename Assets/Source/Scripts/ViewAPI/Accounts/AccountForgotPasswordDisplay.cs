﻿namespace CrossBlitz.ViewAPI.Accounts
{
    public class AccountForgotPasswordDisplay : AccountDisplay
    {
        public AccountInputField email;

        public UnityEngine.UI.Button goToLoginButton;
        public UnityEngine.UI.Button privacyPolicyButton;
        public UnityEngine.UI.Button sendEmailButton;

        public override void Open(AccountScreen _accountScreen)
        {
            base.Open(_accountScreen);

            goToLoginButton.onClick.AddListener(accountScreen.SetLoginAccountDisplayActive);
            privacyPolicyButton.onClick.AddListener(accountScreen.OpenPrivacyPolicy);
            sendEmailButton.onClick.AddListener(SendEmailButtonPressed);
        }

        private void SendEmailButtonPressed()
        {
            PF_Authentication.SendAccountRecoveryEmail(email.field.text);
        }

        public void HandleSuccess()
        {
            accountScreen.SetLoginAccountDisplayActive();
        }

        public void HandleFailed(string details, FieldErrorType fieldError)
        {
            if (fieldError == FieldErrorType.Email)
            {
                email.SetErrorMessage(details);
            }
        }
    }
}
