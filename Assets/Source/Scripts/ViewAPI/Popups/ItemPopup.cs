using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.Databases;
using CrossBlitz.Deck;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.Items.Data;
using CrossBlitz.PlayFab.Authentication;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using CrossBlitz.Audio;

namespace CrossBlitz.ViewAPI.Popups
{
    public class ItemPopup : PopupWindow
    {

        [BoxGroup("Item Popup")] public RectTransform itemLayout;
        [BoxGroup("Item Popup")] public CanvasGroup itemCanvas;
        [BoxGroup("Item Popup")] public RectTransform headerLayout;
        [BoxGroup("Item Popup")] public CanvasGroup headerCanvas;
        [BoxGroup("Item Popup")] public RectTransform bodyLayout;
        [BoxGroup("Item Popup")] public CanvasGroup bodyCanvas;
        [BoxGroup("Item Popup")] public GameObject standardCardViewPrefab;
        [BoxGroup("Item Popup")] public CanvasGroup shadowCanvas;
        [BoxGroup("Item Popup/Deck Recipe")] public DeckView deckViewPrefab;
        [BoxGroup("Item Popup/Deck Recipe")] public RectTransform recipeInfoButtonContainer;
        [BoxGroup("Item Popup/Deck Recipe")] public Button recipeInfoButton;
        [BoxGroup("Chest")] public SpriteAnimation chestAnimation;

        private DeckRecipeData m_recipeData;
        private string m_chestType;
        private Tweener shadowOpenAnimation;

        protected override void Start()
        {
            base.Start();
            recipeInfoButton.onClick.AddListener(OnRecipeButton);
        }

        public override void Open(PopupInfo info)
        {
            base.Open(info);

            shadowCanvas.alpha = 0f;
            shadowCanvas.DOFade(0.5f, 0.25f);
            TweenUtility.Kill(shadowOpenAnimation, true);

            shadowCanvas.GetComponent<RectTransform>().localScale = Vector3.zero;
            shadowOpenAnimation = shadowCanvas.GetComponent<RectTransform>().DOScale(Vector3.one, 0.75f)
                .SetEase(Ease.OutElastic);

            itemLayout.DestroyChildren();

            confirmButton.interactable = false;
            confirmButton.GetComponent<CanvasGroup>().alpha = 0;

            recipeInfoButton.interactable = false;
            recipeInfoButton.GetComponent<CanvasGroup>().alpha = 0;

            headerCanvas.alpha = 0;
            bodyCanvas.alpha = 0;
            itemCanvas.alpha = 0;

            if (info.currencyAcquired != null)
            {
                for (var i = 0; i < info.currencyAcquired.Count; i++)
                {
                    var cardData =
                        CardData.FromCurrency(info.currencyAcquired[i].ItemUid, info.currencyAcquired[i].Count);
                    var card = CardFactory.Create(new CreateCardFactorySettings
                    {
                        CardData = cardData,
                        Layout = itemLayout,
                        CardPrefab = standardCardViewPrefab,
                        RemoveCanvas = true,
                        StartsDisabled = false,
                        RemoveShadow = true,
                        AdditionalComponents = new List<Type> { typeof(HoverCard) }
                    }, null);

                    card.SetActive(true);
                    recipeInfoButtonContainer.SetActive(false);
                    m_chestType = "standard";
                }
            }

            for (var i = 0; i < info.itemsAcquired.Count; i++)
            {
                var item = info.itemsAcquired[i];

                if (item.ItemClass == ItemClassType.Recipe)
                {
                    m_recipeData = Db.DeckRecipeDatabase.GetRecipe(item.ItemId);
                    var deckView = Instantiate(deckViewPrefab, itemLayout, false);
                    deckView.SetDeck(m_recipeData.deckData);
                    deckView.SetActive(true);
                    m_chestType = "recipe";
                    recipeInfoButtonContainer.SetActive(true);
                }
                else
                {
                    var cardData = CardData.FromItem(item);
                    var card = CardFactory.Create(new CreateCardFactorySettings
                    {
                        CardData = cardData,
                        Layout = itemLayout,
                        CardPrefab = standardCardViewPrefab,
                        RemoveCanvas = true,
                        StartsDisabled = false,
                        RemoveShadow = true,
                        AdditionalComponents = new List<Type> {typeof(HoverCard), typeof(CardObtainedEffectComponent)}
                    }, null);

                    Timing.RunCoroutine(SpawnCard(info, cardData, card, item));

                    if (cardData != null)
                    {
                        if (cardData.type == CardType.Resource) m_chestType = "ingredients";
                        else if (cardData.type == CardType.Relic || cardData.type == CardType.Elder_Relic) m_chestType = "relic";
                        else m_chestType = "standard";
                    }

                    recipeInfoButtonContainer.SetActive(false);
                }
            }

            Timing.RunCoroutine(SpawnChest(m_chestType, chestAnimation, itemLayout, itemCanvas, headerCanvas, bodyCanvas, confirmButton, recipeInfoButton));

        }

        private static IEnumerator<float> SpawnCard(PopupInfo info, CardData cardData, CardView card, ItemDataInstance addedItem)
        {
            var cardObtainedEffectComponent = card.GetCardComponent<CardObtainedEffectComponent>();
            while (cardObtainedEffectComponent.Effect == null) yield return Timing.WaitForOneFrame;
            var cardObtainedEffect = card.GetVfx<CardObtainedEffectComponent, CardObtainedEffect>();

            if (cardData != null && cardData.type != CardType.Resource)
            {
                var cardItemCount = App.Inventory.GetItemAmount(cardData.id);
                cardObtainedEffect.SetNewlyObtained( cardItemCount <= addedItem.RemainingUses );
                cardObtainedEffect.SetParticleSize( 32f * (Screen.width / 640f));
            }
            else
            {
                cardObtainedEffect.SetNewlyObtained( false );
            }

            card.SetActive(true);

            yield return Timing.WaitForSeconds(.2f);
        }

        private static IEnumerator<float> SpawnChest(string chestType, SpriteAnimation chestAnimation, RectTransform itemLayout, CanvasGroup itemCanvas, CanvasGroup headerCanvas, CanvasGroup bodyCanvas, Button confirmButton, Button recipeInfoButton)
        {
            yield return Timing.WaitForSeconds(0.4f);

            chestAnimation.GetComponent<CanvasGroup>().alpha = 1;
            chestAnimation.SetActive(true);
            chestAnimation.Play(chestType,()=> chestAnimation.SetActive(false));


            yield return Timing.WaitForSeconds(0.25f);
            AudioController.PlaySound("SpecialTile_Fall");

            yield return Timing.WaitForSeconds(1.1f);
            AudioController.PlaySound("TerrainTile-Poof");
            yield return Timing.WaitUntilTrue(() => chestAnimation.IsDone);

            chestAnimation.GetComponent<CanvasGroup>().alpha = 0;

            yield return Timing.WaitForSeconds(0.1f);

            itemLayout.anchoredPosition = new Vector2(0, 20f);
            itemLayout.DOAnchorPosY(-20f, .4f).SetEase(Ease.OutBounce);
            itemCanvas.DOFade(1f, 0.1f);

            yield return Timing.WaitForSeconds(0.1f);

            headerCanvas.DOFade(1f, 0.2f);
            bodyCanvas.DOFade(1f, 0.2f);
            confirmButton.GetComponent<CanvasGroup>().DOFade(1f, 0.2f);
            recipeInfoButton.GetComponent<CanvasGroup>().DOFade(1f, 0.2f);
            AudioController.PlaySound("BattleStart-ShowCard");

            yield return Timing.WaitForSeconds(0.4f);

            confirmButton.interactable = true;
            recipeInfoButton.interactable = true;
        }

        private async void OnRecipeButton()
        {
            //gameObject.SetActive(false);
            recipeInfoButton.interactable = false;
            confirmButton.interactable = false;
            var sceneModel = await SceneController.Instance.LoadScene(new SceneLoadParams
            {
                SceneToLoad = "RecipeViewer",
                LoadSceneMode = LoadSceneMode.Additive
            });

            var recipeViewer = sceneModel.GetSceneData<RecipeViewerManager>();
            recipeViewer.OnClosed += OnRecipeViewerClosed;
            recipeViewer.Open( m_recipeData );



            Timing.RunCoroutine(FadeOutActive());

        }
        private IEnumerator<float> FadeOutActive()
        {
            yield return Timing.WaitForSeconds(0.4f);
            shadowCanvas.DOFade(0f, 0.2f);
            itemCanvas.DOFade(0f, 0.2f);
            headerCanvas.DOFade(0f, 0.2f);
            bodyCanvas.DOFade(0f, 0.2f);
            confirmButton.GetComponent<CanvasGroup>().DOFade(0f, 0.2f);
            recipeInfoButton.GetComponent<CanvasGroup>().DOFade(0f, 0.2f);

            yield return Timing.WaitForSeconds(0.2f);

            gameObject.SetActive(false);
        }

        private void OnRecipeViewerClosed()
        {
            gameObject.SetActive(true);
            recipeInfoButton.interactable = true;
            confirmButton.interactable = true;

            shadowCanvas.DOFade(0.5f, 0.2f);
            itemCanvas.DOFade(1f, 0.2f);
            headerCanvas.DOFade(1f, 0.2f);
            bodyCanvas.DOFade(1f, 0.2f);
            confirmButton.GetComponent<CanvasGroup>().DOFade(1f, 0.2f);
            recipeInfoButton.GetComponent<CanvasGroup>().DOFade(1f, 0.2f);
        }

        public override void Close()
        {
            base.Close();
            shadowCanvas.DOFade(0f, 0.25f);


            TweenUtility.Kill(shadowOpenAnimation, true);
            shadowOpenAnimation = shadowCanvas.GetComponent<RectTransform>().DOScale(Vector3.zero, 0.25f)
                .SetEase(Ease.InBack).OnComplete(() =>
                {
                    shadowCanvas.GetComponent<RectTransform>().localScale = Vector3.one;
                });
        }
    }
}