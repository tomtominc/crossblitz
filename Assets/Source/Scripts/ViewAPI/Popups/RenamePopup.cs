using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.Databases;
using CrossBlitz.Deck;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.Items.Data;
using CrossBlitz.PlayFab.Authentication;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using CrossBlitz.Audio;
using TMPro;

namespace CrossBlitz.ViewAPI.Popups
{
    public class RenamePopup : PopupWindow
    {
        [BoxGroup("Old Name")] public TextMeshProUGUI oldNameLabel;
        [BoxGroup("Old Name")] public TextMeshProUGUI oldNameLabelShadow;
        [BoxGroup("Name Entry")] public TMP_InputField inputLabel;

        private DeckData _deckData;

        protected override void Start()
        {
            base.Start();
        }

        public override void Open(PopupInfo info)
        {
            base.Open(info);

            _deckData = info.deckData;
            oldNameLabel.text = _deckData.name;
            oldNameLabelShadow.text = _deckData.name;

            confirmButton.interactable = false;
        }

        public void Update()
        {
            if(inputLabel.text.Length > 0 && inputLabel.text.Length <= 20)
            {
                confirmButton.interactable = true;
            }
            else
            {
                confirmButton.interactable = false;
            }
        }

        protected override void OnConfirmButton()
        {
            base.OnConfirmButton();
            _deckData.name = inputLabel.text;
        }
    }
}