﻿using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.ViewAPI.Popups
{
    public enum PopupWindowStyle
    {
        Generic,
        Dialogue,
        ItemGranted,
        Rename,
    }

    public class PopupWindow : MonoBehaviour
    {
        [BoxGroup("Info")]
        public PopupWindowStyle style;
        [BoxGroup("Info"), HideIf("style", Value = PopupWindowStyle.Dialogue)]
        public TextMeshProUGUI title;
        [BoxGroup("Info"), HideIf("style", Value = PopupWindowStyle.Dialogue)]
        public TextMeshProUGUI body;

        [BoxGroup("Misc"), HideIf("style", Value = PopupWindowStyle.Dialogue)]
        public CanvasGroup background;
        [BoxGroup("Misc")]
        public RectTransform content;
        [BoxGroup("Misc")]
        public CanvasGroup popupCanvas;

        [BoxGroup("Buttons"), HideIf("style", Value = PopupWindowStyle.Dialogue)]
        public RectTransform confirmButtonContainer;
        [BoxGroup("Buttons"), HideIf("style", Value = PopupWindowStyle.Dialogue)]
        public RectTransform cancelButtonContainer;
        [BoxGroup("Buttons"), HideIf("style", Value = PopupWindowStyle.Dialogue)]
        public TextMeshProUGUI confirmButtonLabel;
        [BoxGroup("Buttons"), HideIf("style", Value = PopupWindowStyle.Dialogue)]
        public TextMeshProUGUI cancelButtonLabel;
        [BoxGroup("Buttons"), HideIf("style", Value = PopupWindowStyle.Dialogue)]
        public Button confirmButton;
        [BoxGroup("Buttons"), HideIf("style", Value = PopupWindowStyle.Dialogue)]
        public Button cancelButton;

        public event Action OnConfirm = delegate { };
        public event Action OnCancel = delegate { };

        private Tweener openAnimation;
        

        protected virtual void Start()
        {
            confirmButton.onClick.AddListener(OnConfirmButton);
            if (cancelButton) cancelButton.onClick.AddListener(OnDenyButton);
        }

        protected virtual void OnConfirmButton()
        {
            OnConfirm();
        }

        protected virtual void OnDenyButton()
        {
            OnCancel();
        }

        public virtual void Open(PopupInfo info)
        {
            this.SetActive(true);

            background.blocksRaycasts = true;
            background.interactable = true;

            DOTween.Kill(background);
            background.DOFade(1f, 0.25f);

            popupCanvas.alpha = 0f;
            popupCanvas.DOFade(1f, 0.25f);

            title.text = info.title;
            body.text = info.body;

            if (confirmButtonLabel) confirmButtonLabel.text = info.confirm;
            if (cancelButtonLabel) cancelButtonLabel.text = info.deny;
            if (cancelButtonContainer) cancelButtonContainer.SetActive(!string.IsNullOrEmpty(info.deny));

            TweenUtility.Kill(openAnimation, true);

            content.localScale = Vector3.zero;
            openAnimation = content.DOScale(Vector3.one, 0.75f)
                .SetEase(Ease.OutElastic);
        }

        public virtual void Close()
        {
            if (background != null)
            {
                background.blocksRaycasts = false;
                background.interactable = false;

                DOTween.Kill(background);
                background.DOFade(0f, 0.25f);
            }

            popupCanvas.DOFade(0f, 0.25f);

            TweenUtility.Kill(openAnimation, true);
            openAnimation = content.DOScale(Vector3.zero, 0.25f)
                .SetEase(Ease.InBack).OnComplete(() =>
                {
                    gameObject.SetActive(false);
                    content.localScale = Vector3.one;
                });
        }
    }
}
