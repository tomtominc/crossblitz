﻿using System.Collections.Generic;
using CrossBlitz.Dialogue;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.ViewAPI.Popups
{
    public class PopupDialogueBox : DialogueBox
    {

        [BoxGroup("Popup Dialogue Box Properties")] public Canvas dialogueContainerCanvas;
        [BoxGroup("Popup Dialogue Box Properties")] public RectTransform portraitContainer;
        [BoxGroup("Popup Dialogue Box Properties/Animation")] public Vector2 speechBoxSmallSize = new Vector2(84f, 72f);
        [BoxGroup("Popup Dialogue Box Properties/Animation")] public Vector2 speechBoxRegularSize = new Vector2(284f, 72f);

        private float _originalPortraitY;
        private float _originalContainerPositionY;
        private float _originalEmoteWindowPositionY;
        private RectTransform _speechBox;
        private float _portraitDeltaTimer;
        private float _dialogueDeltaTimer;

        public override void InitDialogueBox(object customInfo)
        {
            _originalPortraitY = portraitContainer.anchoredPosition.y;
            _speechBox = box;
            box = boxCanvas.RectTransform();
            _originalContainerPositionY = box.anchoredPosition.y;
            base.InitDialogueBox(customInfo);
        }

        protected override void SetupStartingPosition()
        {

        }

        public override IEnumerator<float> AnimateIn(DialogueInfo dialogueInfo)
        {
            boxCanvas.DOFade(1, 0.25f);
            portraitCanvas.DOFade(1, 0.25f);
            _speechBox.sizeDelta = speechBoxSmallSize;
            portraitContainer.DOAnchorPosY(_originalPortraitY + 4f, 0.25f).SetEase(Ease.OutBack);
            if (dialogueContainerCanvas) dialogueContainerCanvas.sortingOrder = 6100;
            box.anchoredPosition=new Vector2(box.anchoredPosition.x, _originalContainerPositionY + 4);
            yield return Timing.WaitUntilDone( _speechBox.DOSizeDelta(speechBoxRegularSize, 0.25f)
                .SetEase(Ease.OutBack).WaitForCompletion(true));
        }

        public override IEnumerator<float> AnimateOut()
        {
            portraitContainer.DOAnchorPosY(_originalPortraitY, 0.25f)
                .SetEase(Ease.InBack);
            portraitCanvas.DOFade(0, 0.25f);
            boxCanvas.DOFade(0, 0.25f);
            if (dialogueContainerCanvas)  dialogueContainerCanvas.sortingOrder = 6001;
            yield return Timing.WaitUntilDone( _speechBox.DOSizeDelta(speechBoxSmallSize, 0.25f)
                .SetEase(Ease.InBack).WaitForCompletion(true));
        }

        public override IEnumerator<float> ShowText(DialogueInfo dialogueBaseInfo)
        {
            dialogue.ShowText(string.Empty);

            var dialogueInfo =  dialogueBaseInfo;

            GetTypewriterBleepSoundPitch(dialogueInfo);

            if (!dialogueBaseInfo.SkipAnimateIn)
            {
                isReady = false;
                yield return Timing.WaitUntilDone(AnimateIn(dialogueInfo));
            }

            isReady = true;
            DialogueShowed = false;
            NextButtonPressed = dialogueInfo.SkipPressedActions;

            if (boxArrow) boxArrow.SetActive(false);

            if (portrait != null && portrait.CurrentAnimationName != dialogueInfo.PortraitAnimation)
            {
                portrait.Play( dialogueInfo.PortraitAnimation );
            }

            dialogue.ShowText(dialogueInfo.Text);

            while (!DialogueShowed)
            {
                yield return Timing.WaitForOneFrame;
            }

            DialogueShowed = false;
            NextButtonPressed = dialogueInfo.SkipPressedActions;

            if (!dialogueInfo.SkipPressedActions)
            {
                if (boxArrow) boxArrow.SetActive(true);
            }

            while (!NextButtonPressed) yield return Timing.WaitForOneFrame;

            NextButtonPressed = false;
        }
    }
}
