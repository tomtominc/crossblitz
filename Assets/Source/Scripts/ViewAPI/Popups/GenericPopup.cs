﻿using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.ViewAPI.Popups
{
    public class GenericPopup : MonoBehaviour
    {
        public TextMeshProUGUI title;
        public TextMeshProUGUI body;
        public TextMeshProUGUI confirmLabel;
        public TextMeshProUGUI denyLabel;
        public Button confirmButton;
        public Button denyButton;

        public event Action ConfirmPressed = delegate { };
        public event Action DenyPressed = delegate { };

        private Tweener openAnimation;

        private void Start()
        {
            confirmButton.onClick.AddListener(OnConfirmButton);
            denyButton.onClick.AddListener(OnDenyButton);
        }

        private void OnConfirmButton()
        {
            ConfirmPressed();
        }

        private void OnDenyButton()
        {
            DenyPressed();
        }

        public void Open(PopupInfo info)
        {
            title.text = info.title;
            body.text = info.body;
            confirmLabel.text = info.confirm;
            denyLabel.text = info.deny;
            denyButton.SetActive(!string.IsNullOrEmpty(info.deny));

            TweenUtility.Kill(openAnimation, true);

            transform.localScale = Vector3.zero;
            openAnimation = transform.DOScale(Vector3.one, 0.5f)
                .SetEase(Ease.OutElastic);
        }

        public void Close()
        {
            TweenUtility.Kill(openAnimation, true);
            openAnimation = transform.DOScale(Vector3.zero, 0.25f)
                .SetEase(Ease.InBack).OnComplete(() =>
                {
                    gameObject.SetActive(false);
                    transform.localScale = Vector3.one;
                });
        }
    }
}
