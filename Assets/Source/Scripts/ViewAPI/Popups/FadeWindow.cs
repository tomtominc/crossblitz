using UnityEngine;

namespace CrossBlitz.ViewAPI.Popups
{
    public class FadeWindow : MonoBehaviour
    {
        public CanvasGroup canvas;
        public float fadeSpeed = 5;

        private bool _show;

        public virtual void Show()
        {
            _show = true;
        }

        private void Update()
        {
            if (_show)
            {
                canvas.alpha = Mathf.MoveTowards(canvas.alpha, 1, Time.deltaTime*fadeSpeed);
            }
            else
            {
                canvas.alpha = Mathf.MoveTowards(canvas.alpha, 0, Time.deltaTime*fadeSpeed);
            }
        }

        public virtual void Hide()
        {
            _show = false;
        }
    }
}