﻿using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Items.Data;
using DG.Tweening;
using UnityEngine;

namespace CrossBlitz.ViewAPI.Popups
{
    public class PopupController : MonoBehaviour
    {
        private static PopupController _instance;

        public List<PopupWindow> popups;

        private Tweener backgroundFade;
        private Tweener popupOpen;
        private static PopupWindow currentPopup;

        public static event Action<bool> OnPopupChoice = delegate { };

        private Queue<PopupInfo> _popupQueue;
        public static bool IsOpen => currentPopup != null || (_instance._popupQueue != null && _instance._popupQueue.Count > 0);

        private void Awake()
        {
            _instance = this;

            for (var i = 0; i < popups.Count; i++)
            {
                popups[i].OnConfirm += OnPopupConfirm;
                popups[i].OnCancel += OnPopupDeny;
            }
        }

        private void Update()
        {

            if (Input.GetKeyDown(KeyCode.Escape))
            {
               // if(currentPopup != null) OnPopupDeny();
            }

            if (_popupQueue == null) return;
            if (_popupQueue.Count <= 0) return;
            if (currentPopup != null) return;

            var info = _popupQueue.Dequeue();
            OpenPopup(info);
        }

        private void OpenPopup(PopupInfo info)
        {
            currentPopup = _instance.popups.Find(p => p.style == info.style);

            if (currentPopup == null)
            {
                Debug.LogError($"No popup with style {info.style} as been setup.");
                return;
            }

            currentPopup.Open(info);
        }

        public static void Open(PopupInfo info)
        {
            if (_instance._popupQueue == null)
            {
                _instance._popupQueue = new Queue<PopupInfo>();
            }

            _instance._popupQueue.Enqueue(info);
        }

        public static void OpenMany(List<PopupInfo> info)
        {
            if (_instance._popupQueue == null)
            {
                _instance._popupQueue = new Queue<PopupInfo>();
            }

            for (var i = 0; i < info.Count; i++)
            {
                _instance._popupQueue.Enqueue(info[i]);
            }
        }

        private static void OnPopupConfirm()
        {
            if (currentPopup == null)
            {
                Debug.LogError("There is no popup to close!!");
                return;
            }

            if (_instance._popupQueue.Count > 0)
            {
                var nextInfo = _instance._popupQueue.Peek();

                if (nextInfo.style == currentPopup.style) // this makes sure it doesn't close if we're using the same popup
                {
                    currentPopup = null;
                    OnPopupChoice?.Invoke(true);
                    return;
                }
            }

            currentPopup.Close();
            currentPopup = null;

            OnPopupChoice?.Invoke(true);
        }

        private static void OnPopupDeny()
        {
            if (currentPopup == null)
            {
                Debug.LogError("There is no popup to close!!");
                return;
            }

            if (_instance._popupQueue.Count > 0)
            {
                var nextInfo = _instance._popupQueue.Peek();

                if (nextInfo.style == currentPopup.style) // this makes sure it doesn't close if we're using the same popup
                {
                    currentPopup = null;
                    OnPopupChoice?.Invoke(false);
                    return;
                }
            }

            currentPopup.Close();
            currentPopup = null;

            OnPopupChoice?.Invoke(false);
        }

        public static PopupInfo GetPopupInfoForItems(List<ItemDataInstance> items, List<ItemValue> currencies)
        {
            var title = string.Empty;
            var body = string.Empty;

            var item = items[0];

            switch (item.ItemClass)
            {
                case ItemClassType.Card:
                    title = items.Count > 1 ? "Cards Acquired!" : "Card Acquired!";
                    body = items.Count > 1
                        ? "These cards can now be added to <color=#cdb684>decks of their faction!</color>"
                        : "This card can now be added to a <color=#cdb684>deck of its faction!</color>";
                    break;
                case ItemClassType.Ingredient:
                    title = items.Count > 1 ? "Ingredients Acquired!" : "Ingredient Acquired!";
                    body =
                        "Ingredients can be used to <color=#b455a0>meld</color> new cards!";
                    break;
                case ItemClassType.CardBack:
                    title = items.Count > 1 ? "Card Back Acquired!" : "Card Backs Acquired!";
                    body = "Equip different card backs per deck in the <color=#cdb684>Cards Menu.</color>";
                    break;
                case ItemClassType.Recipe:
                    title = "Deck Recipe Acquired!";
                    body = "Select the recipe when creating a new deck in the <color=#cdb684>Cards Menu.</color>";
                    break;
            }

            return new PopupInfo
            {
                style = PopupWindowStyle.ItemGranted,
                title = title,
                body = body,
                confirm = "Confirm",
                itemsAcquired = items,
                currencyAcquired = currencies
            };
        }
    }

    public struct PopupInfo
    {
        public PopupWindowStyle style;
        public string title;
        public string body;
        public string confirm;
        public string deny;
        public List<ItemDataInstance> itemsAcquired;
        public List<ItemValue> currencyAcquired;
        public DeckData deckData;

        public static PopupInfo Error(string errorMessage)
        {
            return new PopupInfo {title = "Error", body = errorMessage, confirm = "OKAY"};
        }
    }
}