﻿using System.Collections.Generic;
using CrossBlitz.Dialogue;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.ViewAPI.Popups
{
    public class PopupDialogueWindow : PopupWindow
    {
        [BoxGroup("Dialogue")]
        public PopupDialogueBox dialogueBox;

        private bool _init;

        protected override void Start()
        {
            
        }

        public override void Open(PopupInfo info)
        {
            Debug.Log("OPENING ANIMATION OEPN DIALOGUE WINDOW");
            if (this.IsActive()) return;

            if (!_init)
            {
                _init = true;
                dialogueBox.InitDialogueBox(null);
            }

            this.SetActive(true);

            var dialogueInfo = new DialogueInfo
            {
                SkipPressedActions = true,
                Text = info.body
            };

            Timing.RunCoroutine(OpenAndCloseDialogue(dialogueInfo));
        }

        private IEnumerator<float> OpenAndCloseDialogue(DialogueInfo dialogueInfo)
        {
            yield return Timing.WaitUntilDone(dialogueBox.ShowText(dialogueInfo));
            yield return Timing.WaitForSeconds(2);
            yield return Timing.WaitUntilDone(dialogueBox.AnimateOut());
            OnConfirmButton();
            //Close();
            //currentPopup = null;
            //this.SetActive(false);

        }

    }
}
