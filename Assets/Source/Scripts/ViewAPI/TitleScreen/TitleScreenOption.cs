using System;
using CrossBlitz.Audio;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace CrossBlitz.ViewAPI.TitleScreen
{
    public class TitleScreenOption : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public CanvasGroup canvasGroup;
        public RectTransform container;
        public Image background;
        public Button button;
        public TextMeshProUGUI optionLabel;
        public string hoverSound = "UI-SingleClick";

        private string m_option;
        private Vector2 m_originalSize;
        private bool m_hoverEnabled;
        private TitleScreenOptionSelector m_selector;

        private static Color highlightColor = new Color32(231, 237, 181, 255);
        private static Color defaultColor = new Color32(255, 255, 255, 255);

        public string Option => m_option;
        public event Action<TitleScreenOption> OnClick;

        private void Start()
        {
            button.onClick.AddListener(Click);
        }

        public void SetOption(string optionName, TitleScreenOptionSelector selector)
        {
            m_option = optionName;
            m_hoverEnabled = false;
            optionLabel.text = m_option;
            m_selector = selector;
            m_originalSize = container.sizeDelta;
            background.color = new Color(0, 0, 0, 0.3f);
        }

        public void AnimateIn(float delay)
        {
            canvasGroup.DOKill();
            container.DOKill();

            canvasGroup.alpha = 0;
            container.anchoredPosition = new Vector2(20, 0);

            canvasGroup.DOFade(1, 0.12f).SetDelay(delay);
            container.DOAnchorPos(Vector2.zero, 0.24f).SetEase(Ease.OutBack).SetDelay(delay)
                .OnComplete(()=> m_hoverEnabled=true);
        }

        public void AnimateSelect()
        {
            container.DOKill();
            container.sizeDelta = m_originalSize;
            container.DOSizeDelta(new Vector2(m_originalSize.x - 14f,  m_originalSize.y), 0.12f).SetEase(Ease.Linear)
                .OnComplete(() =>
                {
                    container.DOSizeDelta(m_originalSize, 0.24f)
                        .SetEase(Ease.OutBack);
                });

            optionLabel.RectTransform().DOKill();
            optionLabel.RectTransform().anchoredPosition = new Vector2(0, -1);
            optionLabel.RectTransform().DOPunchAnchorPos(Vector2.down * 4, 0.2f);

            background.color = new Color(0, 0, 0, 0.6f);
        }

        private void Click()
        {
            OnClick?.Invoke(this);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!m_hoverEnabled) return;

            AudioController.PlaySound(hoverSound);
            m_selector.SelectOption(this);
            AnimateSelect();
            optionLabel.color = highlightColor;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (!m_hoverEnabled) return;
            m_selector.Deselect();
            container.DOKill();
            container.sizeDelta = m_originalSize;
            background.color = new Color(0, 0, 0, 0.3f);
            optionLabel.color = defaultColor;
        }
    }
}