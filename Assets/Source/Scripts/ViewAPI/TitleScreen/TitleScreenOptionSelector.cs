using DG.Tweening;
using UnityEngine;

namespace CrossBlitz.ViewAPI.TitleScreen
{
    public class TitleScreenOptionSelector : MonoBehaviour
    {
        public CanvasGroup canvasGroup;
        private RectTransform m_rect;
        private bool m_animate;
        private int m_animateDirection;
        private Vector2 m_originalSize;

        private void Start()
        {
            m_rect = this.RectTransform();
            m_originalSize = m_rect.sizeDelta;
        }

        public void SelectOption(TitleScreenOption option)
        {
            canvasGroup.alpha = 1;
            m_animate = false;
            m_rect.SetParent(option.transform, false);
            m_rect.anchoredPosition = Vector2.zero;

            m_rect.DOKill();
            m_rect.sizeDelta = m_originalSize;
            m_rect.DOSizeDelta(new Vector2(m_originalSize.x - 14f,  m_originalSize.y), 0.12f).SetEase(Ease.Linear)
                .OnComplete(() =>
                {
                    m_rect.DOSizeDelta(m_originalSize, 0.24f)
                        .SetEase(Ease.OutBack).OnComplete(() =>
                        {
                            m_animateDirection = 1;
                            m_animate = true;
                        });
                });
        }

        public void Deselect()
        {
            canvasGroup.alpha = 0;
        }

        private void Update()
        {
            if (m_animate)
            {
                var target = m_animateDirection > 0 ? m_originalSize + new Vector2(8, 0) : m_originalSize;
                m_rect.sizeDelta = Vector2.MoveTowards(m_rect.sizeDelta,target, 20 * Time.deltaTime);

                if (Vector2.Distance(target, m_rect.sizeDelta) <= 0.01f)
                {
                    m_rect.sizeDelta = target;
                    m_animateDirection *= -1;
                }
            }
        }
    }
}