﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Audio;
using CrossBlitz.Settings;
using CrossBlitz.Transition;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace CrossBlitz.ViewAPI.TitleScreen
{
    public class TitleScreen : View
    {
        public string buttonClickSfx = "UI-Selection";
        public SpriteAnimation logo;

        public RectTransform optionsLayout;
        public TitleScreenOption optionPrefab;
        public TitleScreenOptionSelector selector;

        public GameObject dayBackground;
        public GameObject nightBackground;

        public Button twitterButton;
        public Button discordButton;
        public TextMeshProUGUI version;

        private bool m_disableScreen;
        private bool m_skipWait;

        public event Action OnContinueButton;
        public event Action OnDeveloperToolsButton;

        private void Awake()
        {
            if (DateTime.Now.Hour >= 18 || DateTime.Now.Hour < 6)
            {
                nightBackground.SetActive(true);
                dayBackground.SetActive(false);
            }
            else
            {
                nightBackground.SetActive(false);
                dayBackground.SetActive(true);
            }

            version.text = $"V. {Application.version}";
            m_skipWait = true;
        }

        private void Start()
        {
            Open();

            twitterButton.onClick.AddListener(OnTwitterButton);
            discordButton.onClick.AddListener(OnDiscordButton);
            selector.Deselect();
        }

        public override void Open()
        {
            Timing.RunCoroutine( AnimateIn().CancelWith(gameObject) );
        }

        public IEnumerator<float> AnimateIn()
        {
           Timing.RunCoroutine( TransitionController.TransitionOut(TransitionController.TransitionType.Fade) );

           logo.SetActive(true);

            if (!logo.Play("animate"))
            {
                Debug.LogError("Could not animate logo????");
            }

            AudioController.PlaySound("CB-Logo - Whole Sequence_NoCharacterWhooshes","SFX",false);

            while (logo.CurrentFrame < 40 && !m_skipWait)
            {
                yield return Timing.WaitForOneFrame;
            }

            var option = Instantiate(optionPrefab, optionsLayout, false);
            option.SetOption("START", selector);
            option.AnimateIn(0.06f);
            option.OnClick += OnOptionClicked;

            option = Instantiate(optionPrefab, optionsLayout, false);
            option.SetOption("SETTINGS", selector);
            option.AnimateIn(0.12f);
            option.OnClick += OnOptionClicked;

            option = Instantiate(optionPrefab, optionsLayout, false);
            option.SetOption("DEVELOPER TOOLS", selector);
            option.AnimateIn(0.18f);
            option.OnClick += OnOptionClicked;

            option = Instantiate(optionPrefab, optionsLayout, false);
            option.SetOption("QUIT GAME", selector);
            option.AnimateIn(0.24f);
            option.OnClick += OnOptionClicked;

            while (!logo.IsDone && !m_skipWait) yield return Timing.WaitForOneFrame;

            AudioController.PlaySong("titlescreen");

            if (DateTime.Now.Hour >= 18 || DateTime.Now.Hour < 6)
            {
                AmbientController.PlayAmbient("AMB-night-ambience-1");
            }
            else
            {
                AmbientController.PlayAmbient("AMB-daytime-ambience-1");
            }
        }

        private async void OnOptionClicked(TitleScreenOption option)
        {
            if (m_disableScreen) return;

            AudioController.StopSound("CB-Logo - Whole Sequence_NoCharacterWhooshes");

            if (option.Option == "START")
            {
                AudioController.StopSong();
                AmbientController.Stop();
                AudioController.PlaySound("UI-ColiseumStart-NoFX");
                m_disableScreen = true;
                OnContinueButton?.Invoke();
            }
            else if (option.Option == "DEVELOPER TOOLS")
            {
                AudioController.StopSong();
                AmbientController.Stop();
                AudioController.PlaySound("UI-ColiseumStart-NoFX");
                m_disableScreen = true;
                OnDeveloperToolsButton?.Invoke();
            }
            else if (option.Option == "SETTINGS")
            {
                if (SceneManager.GetSceneByName("OptionsWindow").isLoaded) return;

                AudioController.PlaySound(buttonClickSfx);

                await Task.Delay(100);

                var sceneModel = await SceneController.Instance.LoadScene(new SceneLoadParams
                {
                    SceneToLoad = "OptionsWindow"
                });

                var settingsWindow = sceneModel.GetView<SettingsWindow>("SettingsWindow");
                settingsWindow.Initialize( SettingsContext.TitleScreen );
            }
            else if (option.Option == "QUIT GAME")
            {
                AudioController.PlaySound(buttonClickSfx);
                m_disableScreen = true;
                SettingsManager.QuitGame();
            }
        }

        private async void OnTwitterButton()
        {
            if (m_disableScreen) return;
            AudioController.PlaySound(buttonClickSfx);
            Application.OpenURL("https://www.twitter.com/takoboystudios");
        }

        private async void OnDiscordButton()
        {
            if (m_disableScreen) return;
            AudioController.PlaySound(buttonClickSfx);
            Application.OpenURL("https://www.discord.gg/takoboystudios");
        }
    }
}
