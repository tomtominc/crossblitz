using System;
using System.Collections.Generic;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Databases;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.Hero;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.RewardScreenAPI;
using CrossBlitz.ServerAPI;
using CrossBlitz.Transition;
using CrossBlitz.ViewAPI.Developer;
using CrossBlitz.ViewAPI.Popups;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace CrossBlitz.ViewAPI.Lobby
{
    public class LobbyScreen : View
    {
        public CanvasGroup canvasGroup;
        public Button settingsButton;
        public DeckToggleFilter deckScrollView;
        public HeroView heroView;
        public TextMeshProUGUI userNameLabel;
        public Button playButton;
        public Button deckEditorButton;

        private ScreenNavigationPopup _navPopup;
        private DeckData _currentDeck;

        public event Action OnPlayGame;

        public override void GatherAssetLoaders()
        {
            //TransitionController.AssetsToWaitFor.Add(heroView);
        }

        public override void Open()
        {
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;

            settingsButton.onClick.AddListener(SettingsButtonPressed);
            playButton.onClick.AddListener(PlayButtonPressed);
            deckEditorButton.onClick.AddListener(DeckEditorButtonPressed);
            userNameLabel.text = App.AccountInfo.GetUsername();

            var currentDeck = App.PlayerDecks.GetDeckBasedOnGameState();

            deckScrollView.OnDeckToggle += DeckSelected;
            // deckScrollView.Init(new DeckToggleFilter.InitParams
            // {
            //     ForcedHero = currentDeck.hero,
            //     ForcedFaction = currentDeck.faction,
            //     Decks = new List<DeckData>(App.PlayerDecks.Decks)
            // });

            DeckSelected(true, currentDeck.ToJson(false));
        }

        private void DeckSelected(bool selected, string content)
        {
            if (!selected) return;
            if (string.IsNullOrEmpty(content)) return;

            var deckData = DataModel.FromJson<DeckData>(content,false);

            if (deckData == null)
            {
                Debug.LogError("Deck Data is null!");
                return;
            }

            OnDeckSelected(deckData);
        }

        private void OnDeckSelected(DeckData deckData)
        {
            _currentDeck = deckData;

            var hero = Db.HeroDatabase.GetHero(_currentDeck.hero);

            if (hero == null)
            {
                Debug.LogError($"The Hero for this deck is null? How is this possible! {_currentDeck}: {_currentDeck.hero}");
                return;
            }

            heroView.SetHero(hero, App.HeroData.GetHero(hero.id));
        }

        private void SettingsButtonPressed()
        {
            OpenScreenNavigationPopup();
        }

        private async void OpenScreenNavigationPopup()
        {
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;

            _navPopup = await ViewController.OpenAsync<ScreenNavigationPopup>("ScreenNavigationPopup");
            _navPopup.OnStartClose += OverlayScreenClosed;
            _navPopup.SetPreviousCanvas(Canvas);
        }

        private void OverlayScreenClosed()
        {
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;
        }

        private void PlayButtonPressed()
        {
            if (_currentDeck == null)
            {
                PopupController.Open(PopupInfo.Error("The current Deck is null; Please select a deck before going into the game."));
                return;
            }

            if (_currentDeck.GetCardCount() < DeckData.MaxCardsPerDeck)
            {
                PopupController.Open(PopupInfo.Error($"Deck {_currentDeck.name}, has fewer than {DeckData.MaxCardsPerDeck} cards. Select a deck with more cards."));
                return;
            }

            Debug.Log($"Current Hero? {_currentDeck.hero}");

            if (!_currentDeck.GetHeroSaveData().unlocked)
            {
                PopupController.Open(PopupInfo.Error($"The deck has a hero that you don't currently own. Go to PlayFab and add the hero to your inventory to use it."));
                return;
            }

            //todo: equip deck here? probably wont use this class
            //App.PlayerDecks.CurrentDeckId = _currentDeck.uid;

            OnPlayGame?.Invoke();
        }

        private async void DeckEditorButtonPressed()
        {
            var collectionSceneLoadParams = new SceneLoadParams
            {
                SceneToLoad = "Collection",
                LoadSceneMode = LoadSceneMode.Additive
            };
            var titleScene = await SceneController.Instance.LoadScene(collectionSceneLoadParams);

            var deckEditMenu = titleScene.GetView<DeckEditMenu>("DeckEditMenu");
            deckEditMenu.Open();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                OpenRewardScreen();
            }
        }

        private async void OpenRewardScreen()
        {
            await ViewController.OpenAsync<RewardScreen>("RewardScreen", worldCamera: SceneModel.GetOrCreateSceneModel("GameLobbySceneModel").sceneCamera);
        }
    }
}
