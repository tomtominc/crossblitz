﻿using System;
using System.Collections;
using System.Collections.Generic;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ViewAPI
{
    public class View : MonoBehaviour
    {
        public const float PlayerSidePanelWidth = 130;

        protected Canvas _canvas;
        protected SceneModel model;

        [HideInInspector]
        public AssetReference AssetReference;

        public event Action<AssetReference, View> OnClose;

        public Canvas Canvas
        {
            get
            {
                if (_canvas == null)
                    _canvas = GetComponent<Canvas>();

                return _canvas;
            }
        }

        public virtual void Initialize(SceneModel sceneModel)
        {
            model = sceneModel;
            LocalizationController.Localize(transform);
            GatherAssetLoaders();
        }

        private void Start()
        {
            Events.Publish(this, EventType.OnMenuStatusChanged, new OnMenuStatusChangedEventArgs
            {
                Status = OnMenuStatusChangedEventArgs.MenuStatus.Opened,
                MenuName = name
            } );
        }

        public virtual void GatherAssetLoaders()
        {

        }

        public virtual void Open()
        {

        }

        private void OnDestroy()
        {
            Events.Publish(this, EventType.OnMenuStatusChanged, new OnMenuStatusChangedEventArgs
            {
                Status = OnMenuStatusChangedEventArgs.MenuStatus.Closed,
                MenuName = name
            } );
        }

        public void Destroy()
        {
            OnClose?.Invoke(AssetReference, this);
            Destroy(gameObject);
        }

        public virtual IEnumerator<float> Close()
        {
            Destroy();
            yield break;
        }

        [Button("Apply Default Material")]
        private void ApplyDefaultMaterialToAllImages(Material material)
        {
            var img = GetComponentsInChildren<Image>(true);
            for (var i = 0; i < img.Length; i++)
            {
                img[i].material = material;
            }
        }
    }
}
