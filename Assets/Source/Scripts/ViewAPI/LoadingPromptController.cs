﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace CrossBlitz.ViewAPI
{
    public class LoadingPromptController : MonoBehaviour
    {
        public static LoadingPromptController Instance;
        public static List<PlayFabAPIMethods> waiting;
        public CanvasGroup genericLoadingPrompt;

        private Tweener _fadeTween;

        private void Awake()
        {
            Instance = this;
        }

        public static void BlockUI(bool block)
        {
            if (Instance == null) return;

            Instance.genericLoadingPrompt.SetActive(block);
            Instance.genericLoadingPrompt.alpha = 0;
            Instance.genericLoadingPrompt.blocksRaycasts = block;
        }

        public static void RequestLoadingPrompt(PlayFabAPIMethods method = PlayFabAPIMethods.Generic)
        {
            if (Instance == null) return;

            TweenUtility.Kill(Instance._fadeTween, false);
            Instance.genericLoadingPrompt.SetActive(true);
            Instance.genericLoadingPrompt.alpha = 0;
            Instance.genericLoadingPrompt.blocksRaycasts = true;
            Instance._fadeTween = Instance.genericLoadingPrompt
                .DOFade(1, 0.5f);

        }

        public static void RemoveLoadingPrompt(PlayFabAPIMethods method = PlayFabAPIMethods.Generic)
        {
            if (Instance == null) return;

            TweenUtility.Kill(Instance._fadeTween, false);
            Instance.genericLoadingPrompt.blocksRaycasts = false;
            Instance._fadeTween = Instance.genericLoadingPrompt
                .DOFade(0, 0.5f)
                .OnComplete(() =>
                {
                    Instance.genericLoadingPrompt.SetActive(false);
                });
        }
    }
}
