using CrossBlitz.ClientAPI.Data;
using CrossBlitz.ClientAPI.GameLogic.Data;
using CrossBlitz.ServerAPI.GameLogic;
using TMPro;
using UnityEngine;

namespace CrossBlitz.ViewAPI.Vs
{
    public class VsHeroView : MonoBehaviour
    {
        public SpriteAnimation heroBigAnimator;
        public SpriteAnimation heroBigAnimatorShadow;
        public TextMeshProUGUI username;
        public TextMeshProUGUI prestigeAmount;
        public TextMeshProUGUI heroName;

        public void SetData(PlayerBattleState playerInfo)
        {
            username.text = playerInfo.name;
            prestigeAmount.text = "1,000";

            var hero = GameServer.State.GetCard(playerInfo.heroUid);
            var heroAnimation = hero.characterData.name.ToLower();

            heroBigAnimator.Play(heroAnimation);
            heroBigAnimatorShadow.Play(heroAnimation);

            heroBigAnimator.image.SetNativeSize();
            heroBigAnimatorShadow.image.SetNativeSize();

            heroName.text = hero.characterData.name;
        }
    }
}