using System.Collections.Generic;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Hero;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.Transition;
using DG.Tweening;
using MEC;
using Source.Scripts.AssetManagement;
using TakoBoyStudios.Events;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ViewAPI.Vs
{
    public class VsScreen : View, IAssetLoader
    {
        private const float DelayBeforeClosing = 2;

        public GameplayScreen gameplayScreen;
        public CanvasGroup canvasGroup;
        public RectTransform playerHeroPortraitContainer;
        public RectTransform opponentHeroPortraitContainer;
        public VsHeroView clientHeroView;
        public VsHeroView opponentHeroView;
        public string AssetName => name;
        private bool _isBusy = true;
        public bool IsBusy => _isBusy;

        private HeroViewBattle _playerHeroView;
        private HeroViewBattle _opponentHeroView;

        public override void GatherAssetLoaders()
        {
            //TransitionController.AssetsToWaitFor.Add(this);
        }

        public override void Open()
        {
            gameObject.SetActive(true);
            Timing.RunCoroutine(WaitForGameToLoad());
        }

        private void Start()
        {
            Events.Subscribe(EventType.OnContinueFromMatchFinished, OnContinueFromMatchFinished);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnContinueFromMatchFinished, OnContinueFromMatchFinished);
        }

        private void OnContinueFromMatchFinished(IMessage message)
        {
            gameObject.SetActive(false);
        }

        private IEnumerator<float> WaitForGameToLoad()
        {
            _isBusy = true;

            while (gameplayScreen.heroSideBarContainer.IsBusy) yield return Timing.WaitForOneFrame;

            _playerHeroView = gameplayScreen.
                heroSideBarContainer.playerHeroView;

            _opponentHeroView = gameplayScreen.
                heroSideBarContainer.opponentHeroView;

            _playerHeroView.portraitCanvas.sortingOrder = 300;
            _opponentHeroView.portraitCanvas.sortingOrder = 300;

            while (_playerHeroView.IsBusy || _opponentHeroView.IsBusy) yield return Timing.WaitForOneFrame;

            _playerHeroView.heroPortraitContainer
                .SetParent(playerHeroPortraitContainer, true);

            _opponentHeroView.heroPortraitContainer
                .SetParent(opponentHeroPortraitContainer, true);

            _playerHeroView.heroPortraitContainer
                .anchoredPosition3D = new Vector3(0,0, -5);
            _opponentHeroView.heroPortraitContainer
                .anchoredPosition3D = new Vector3(0,0, -5);

            clientHeroView.SetData(GameServer.ClientPlayerState);
            opponentHeroView.SetData(GameServer.OpponentPlayerState);

            // because it rotates when it moves we want to give it some time to stabilize
            yield return Timing. WaitForSeconds(0.5f);

            _isBusy = false;
        }

        public IEnumerator<float> PlayOpeningAnimation()
        {
            //todo: slide stuff in do cool things

            yield return Timing. WaitForSeconds(DelayBeforeClosing);

            _playerHeroView.heroPortraitContainer
                .SetParent(_playerHeroView.portraitContainerParent.RectTransform(), true);

            _playerHeroView.heroPortraitContainer.SetAsFirstSibling();

            _opponentHeroView.heroPortraitContainer
                .SetParent(_opponentHeroView.portraitContainerParent.RectTransform(), true);

            _opponentHeroView.heroPortraitContainer.SetAsFirstSibling();

            canvasGroup.DOFade(0, 1f);

            yield return Timing. WaitForSeconds(0.25f);

            _playerHeroView.heroPortraitContainer
                .DOAnchorPos3D(new Vector3(0, 2, -5), 1);

            _opponentHeroView.heroPortraitContainer
                .DOAnchorPos3D(new Vector3(0, 2, -5), 1);

            yield return Timing. WaitForSeconds(1.25f);

            _playerHeroView.SetToNormalSort();
            _opponentHeroView.SetToNormalSort();

            _playerHeroView.heroPortraitContainer
                .DOAnchorPos3D(new Vector3(0, 0, 0), 0.16f)
                .SetEase(Ease.OutBack);

            _opponentHeroView.heroPortraitContainer
                .DOAnchorPos3D(new Vector3(0, 0, 0), 0.16f)
                .SetEase(Ease.OutBack);

            yield return Timing. WaitForSeconds(0.16f);

        }

        public void Unload()
        {

        }
    }
}
