﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using CrossBlitz.AddressablesAPI;
using MEC;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace CrossBlitz.ViewAPI
{
    public class ViewController : MonoBehaviour
    {
        public static ViewController Instance;
        public static Dictionary<string, View> OpenViews;
        public static Dictionary<string, AssetReference> ViewReferences;
        public static List<string> PendingOpen;

        public List<AssetReferenceObject> viewReferences;

        public static void Initialize()
        {
            Instance = FindObjectOfType<ViewController>();
            OpenViews = new Dictionary<string, View>();
            PendingOpen = new List<string>();

            FillReferences();
        }

        private static void FillReferences()
        {
            ViewReferences=new Dictionary<string, AssetReference>();

            for (var i = 0; i < Instance.viewReferences.Count; i++)
            {
                ViewReferences.Add(Instance.viewReferences[i].name, Instance.viewReferences[i].reference);
            }
        }

        public static bool IsOpen(string key)
        {
            return OpenViews.ContainsKey(key);
        }

        public static async Task<T> OpenAsync<T>(
            string key,
            Transform parent = default,
            Camera worldCamera = default,
            bool openLoadingModal = true) where T : View
        {
            if (PendingOpen.Contains(key))
            {
                return null;
            }

            if (OpenViews.ContainsKey(key))
            {
                return OpenViews[key] as T;
            }

            if (openLoadingModal)
            {
                LoadingPromptController.RequestLoadingPrompt();
            }

            PendingOpen.Add(key);
            var asset = await ViewReferences[key].InstantiateAsync(parent).Task;
            if (asset == null) Debug.LogErrorFormat("Failed to load View {0}", key);
            var view = asset.GetComponent<T>();
            view.GetComponent<Canvas>().worldCamera = !worldCamera ? Camera.main : worldCamera;
            //view.Initialize();
            view.OnClose += OnViewClosed;
            view.AssetReference = ViewReferences[key];
            view.Open();
            view.name = key;

            OpenViews.Add(key, view);
            PendingOpen.Remove(key);

            if (openLoadingModal)
            {
                LoadingPromptController.RemoveLoadingPrompt();
            }

            return view;
        }

        public static void Close(View view)
        {
            Timing.RunCoroutine(view.Close());
        }

        public static void Close(string key)
        {
            if (OpenViews.ContainsKey(key))
            {
                Timing.RunCoroutine(CloseAsync(key));
            }
            else
            {
                Debug.LogWarning($"{key} is not in the Open Views list and you are trying to close it.");
            }
        }

        private static IEnumerator<float> CloseAsync(string key)
        {
            yield return Timing.WaitUntilDone(OpenViews[key].Close());
            OpenViews.Remove(key);
        }

        private static void OnViewClosed(AssetReference assetReference, View obj)
        {
            Addressables.ReleaseInstance(obj.gameObject);
            if (assetReference.Asset !=null)
                assetReference.ReleaseAsset();
        }

        public static void CloseAll()
        {
            foreach (var viewPair in OpenViews)
            {
                viewPair.Value.Destroy();
            }

            OpenViews = new Dictionary<string, View>();
        }
    }
}
