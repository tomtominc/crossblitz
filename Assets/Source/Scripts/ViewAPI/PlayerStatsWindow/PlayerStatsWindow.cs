﻿using CrossBlitz.PlayFab.Authentication;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.ViewAPI.PlayerStatsWindow
{
    public class PlayerStatsWindow : MonoBehaviour
    {
        public TextMeshProUGUI userLevel;
        public TextMeshProUGUI experienceValue;
        public Slider experienceSlider;
        public TextMeshProUGUI prestigeValue;
        public Slider prestigeSlider;
        public TextMeshProUGUI rankLevel;

        public void Start()
        {
            if (App.PlayerData != null)
            {
                var pvpRank = App.PlayerData.GetPvpRank();

                userLevel.text = pvpRank.ToString();
                rankLevel.text = $"RANK.<color=#e7edb5>{pvpRank}";

                experienceValue.text = $"{App.Inventory.GetCurrencyAmount(Currency.EXPERIENCE)}/100";
                experienceSlider.minValue = 0;
                experienceSlider.maxValue = 100;
                experienceSlider.value = App.Inventory.GetCurrencyAmount(Currency.EXPERIENCE);

                prestigeValue.text = $"{App.Inventory.GetCurrencyAmount(Currency.PRESTIGE)}/100";
                prestigeSlider.minValue = 0;
                prestigeSlider.maxValue = 100;
                prestigeSlider.value = App.Inventory.GetCurrencyAmount(Currency.PRESTIGE);
            }
        }
    }
}
