using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asyncoroutine;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.GameVfx;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.Universal;
using DG.Tweening;
using GameDataEditor;
using MEC;
using Sirenix.OdinInspector;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;
using Random = System.Random;

namespace CrossBlitz.CardCollection
{
    public class MeldOverlay : MonoBehaviour
    {
        private const int CardSortingOrder = 8000;

        public CanvasGroup canvasGroup;

        public RectTransform cardLayout;
        public CanvasGroup cardCanvas;
        public Button closeButton;
        public Button closeButton2;
        public Button meldButton;
        public Button scrapButton;

        public GameObject cardCountView;
        public TextMeshProUGUI cardCountDisplay;

        public CurrencyDisplay playerCurrency;
        public CurrencyDisplay meldAndScrapCurrencyPrefab;

        [BoxGroup("Mawlder")] public TextMeshProUGUI dialogue;

        [BoxGroup("Error Message")] public GameObject errorContainer;
        [BoxGroup("Error Message")] public CanvasGroup errorIconCanvas;
        [BoxGroup("Error Message")] public RectTransform errorIcon;
        [BoxGroup("Error Message")] public CanvasGroup errorTextCanvas;
        [BoxGroup("Error Message")] public TextMeshProUGUI errorText;

        [BoxGroup("Ingredient Displays")] public IngredientDisplayLayout playerIngredientList;
        [BoxGroup("Ingredient Displays")] public IngredientDisplayLayout meldList;
        [BoxGroup("Ingredient Displays")] public IngredientDisplayLayout scrapList;

        [BoxGroup("Animation")] public RectTransform playerIngredientListRect;
        [BoxGroup("Animation")] public CanvasGroup playerIngredientListCanvas;
        [BoxGroup("Animation")] public RectTransform meldControlsRect;
        [BoxGroup("Animation")] public CanvasGroup meldControlsCanvas;
        [BoxGroup("Animation")] public RectTransform mawlerRect;
        [BoxGroup("Animation")] public CanvasGroup mawlerCanvas;


        [BoxGroup("Cinematic")] public MeldCinematic cinematic;

        private CardView _card;
        private bool _init;

        private CurrencyDisplay _meldCurrency;
        private CurrencyDisplay _scrapCurrency;

        private const float MeldButtonDelayTime = 0.5f;
        private bool _delayMeldButtonInput;
        private float _delayMeldTimer;
        private bool _canEscape;

        private void Awake()
        {
            closeButton.onClick.AddListener(OnCloseButton);
            closeButton2.onClick.AddListener(OnCloseButton);
            meldButton.onClick.AddListener(OnMeldButton);
            scrapButton.onClick.AddListener(OnScrapButton);

            meldButton.interactable = false;
            scrapButton.interactable = false;
            closeButton.interactable = false;

            _canEscape = false;

            if (!_init)
            {
                canvasGroup.alpha = 0;
                this.SetActive(false);
            }
        }

        private void Start()
        {
            Events.Subscribe(EventType.OnModifiedCurrency, OnModifyCurrency);
            Events.Subscribe(EventType.OnModifiedItemUses, OnModifyItemUses);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnModifiedCurrency, OnModifyCurrency);
            Events.Unsubscribe(EventType.OnModifiedItemUses, OnModifyItemUses);
        }

        private void Update()
        {
            if (_delayMeldButtonInput)
            {
                _delayMeldTimer -= Time.deltaTime;

                if (_delayMeldTimer <= 0)
                {
                    _delayMeldTimer = 0;
                    _delayMeldButtonInput = false;

                    meldButton.interactable = Crafting.CanCraftCard(_card.data, out _);
                    scrapButton.interactable = Crafting.CanScrapCard(_card.data);
                }
            }

            if (errorText && errorText.textInfo !=null&&errorText.textInfo.lineCount > 1)
            {
                errorIcon.anchoredPosition = new Vector2(0,6);
            }
            else
            {
                errorIcon.anchoredPosition = new Vector2(0,0);
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (_canEscape && !cinematic.IsActive())
                {
                    OnCloseButton();
                }
            }
        }

        private void OnModifyCurrency(IMessage message)
        {
            meldButton.interactable = Crafting.CanCraftCard(_card.data, out var e);
        }

        private void OnModifyItemUses(IMessage message)
        {
            meldButton.interactable = Crafting.CanCraftCard(_card.data, out var e);
        }

        private async Task<bool> CreateCardIfNull(CardData data)
        {
            if (_card == null)
            {
                var obj = await AddressableReferenceLoader.GetAsset("Card_Zoomed");

                var settings = new CreateCardFactorySettings
                {
                    CardPrefab = obj,
                    CardData = data,
                    Layout = cardLayout,
                    SortingOrder = CardSortingOrder,
                    SortingLayer = "Collection",
                    AdditionalComponents = new List<Type> {typeof(CardScrappedComponent)}
                };
                _card = CardFactory.Create(settings, null);
                _card.RectTransform().SetAsFirstSibling();
            }

            _card.SetCardData(data);

            return true;
        }

        public async void Open( HoverCard hoverCard )
        {
            _init = true;

            //Events.Publish(this, EventType.OnMenuOpened, new OnMenuOpenedEventArgs{ MenuName = "ManaMeld"} );

            var createdCard = await CreateCardIfNull(hoverCard.Data);

            if (createdCard) await UnityAsync.Await.NextUpdate();

            this.SetActive(true);

            // _originalParent = _currentCard.ZoomedCard.transform.parent;
            // _currentCard.KillMoveTween();
            // _currentCard.ZoomedCard.transform.SetParent(cardLayout, true);
            // _currentCard.ZoomedCard.RectTransform().anchoredPosition3D = new Vector3(
            //     _currentCard.ZoomedCard.RectTransform().anchoredPosition3D.x,
            //     _currentCard.ZoomedCard.RectTransform().anchoredPosition3D.y, 0);
            // _currentCard.ZoomedCard.RectTransform().DOAnchorPos3D(Vector3.zero, 0.1f);


            canvasGroup.DOFade(1, 0.25f).OnComplete(() =>
            {
                meldButton.interactable = Crafting.CanCraftCard(_card.data, out var e);
                scrapButton.interactable = Crafting.CanScrapCard(_card.data);
                closeButton.interactable = true;
            });

            var amount = App.Inventory.GetItemAmount(_card.data.id);

            if (amount <= 1)
            {
                cardCountView.SetActive(false);
            }
            else
            {
                cardCountView.SetActive(true);
                cardCountDisplay.text = $"<color=#b29678>x</color>{amount}<color=#b29678>/{DeckData.GetMaxCopies(_card.data.rarity)}</color>";
            }

            var costToCraft = _card.data.craftingData.cost;

            playerCurrency.Blink(true);

            meldList.Clear();
            scrapList.Clear();
            playerIngredientList.Clear();

            _meldCurrency = Instantiate(meldAndScrapCurrencyPrefab, meldList.layout, false);
            _meldCurrency.labelFormatter.prefix = "-";
            _meldCurrency.SetCurrencyCode(Currency.MANA_SHARDS, true, costToCraft);
            _meldCurrency.GetComponent<CanvasGroup>().alpha = 0;

            _scrapCurrency = Instantiate(meldAndScrapCurrencyPrefab, scrapList.layout, false);
            _scrapCurrency.labelFormatter.prefix = "+";
            _scrapCurrency.GetComponent<CurrencyMeldDisplay>().insufficientFundsDisplay = false;
            _scrapCurrency.SetCurrencyCode(Currency.MANA_SHARDS, true, (int)(costToCraft * Crafting.ScrapManaShardsExchange));
            _scrapCurrency.GetComponent<CanvasGroup>().alpha = 0;

            // using the gde data instead of the database because its ordered properly
            var allIngredients =
                GDEDataManager.GetAllItems<GDEItemData>().FindAll(item => item.ItemType == "Ingredient");// Db.ItemDatabase.GetItemsOfType("Ingredient");
            var allIngredientsById = allIngredients.Select(i => i.Key).ToList();
            playerIngredientList.DisplayIngredients(allIngredientsById);

            meldList.DisplayIngredients(_card.data.craftingData.ingredients);

            for (var i = 0; i < meldList.ingredientDisplays.Count; i++)
            {
                meldList.ingredientDisplays[i].GetComponent<CanvasGroup>().alpha = 0;
                meldList.ingredientDisplays[i].labelFormatter.prefix = "-";
                meldList.ingredientDisplays[i].GetComponent<NumberLabelFormatter>().RefreshLabel();
            }

            scrapList.DisplayIngredients(_card.data.craftingData.ingredients);

            for (var i = 0; i < scrapList.ingredientDisplays.Count; i++)
            {
                scrapList.ingredientDisplays[i].GetComponent<CanvasGroup>().alpha = 0;
                scrapList.ingredientDisplays[i].GetComponent<IngredientMeldDisplay>().insufficientFundsDisplay = false;
                scrapList.ingredientDisplays[i].labelFormatter.prefix = "+";
                scrapList.ingredientDisplays[i].GetComponent<NumberLabelFormatter>().RefreshLabel();
            }

            for (var i = 0; i < playerIngredientList.ingredientDisplays.Count; i++)
            {
                if (_card.data.craftingData.ingredients.Exists(item => item.ItemUid == playerIngredientList.ingredientDisplays[i].itemName))
                {
                    playerIngredientList.ingredientDisplays[i].Blink(true);
                }
            }

            errorContainer.SetActive(false);

            Timing.RunCoroutine(AnimateIn());
        }

        private IEnumerator<float> AnimateIn()
        {
            const float timingScale = 1;
            playerIngredientListRect.anchoredPosition = new Vector2(-200, -12);
            playerIngredientListCanvas.alpha = 0;

            meldControlsRect.anchoredPosition = new Vector2(-39, 32);
            //meldControlsRect.localScale = new Vector3(1, 0.8f, 1);
            meldControlsCanvas.alpha = 0;

            mawlerRect.anchoredPosition = new Vector2(300, 80f);
            mawlerCanvas.alpha = 0;

            cardCanvas.alpha = 0;

            yield return Timing.WaitForSeconds(0.08f * timingScale);

            mawlerRect.DOAnchorPosX(206f, 0.5f*timingScale).SetEase(Ease.OutBack);
            mawlerCanvas.DOFade(1, 0.25f*timingScale);

            yield return Timing.WaitForSeconds(0.25f* timingScale);

            meldControlsRect.DOAnchorPosY(16, 0.16f* timingScale).SetEase(Ease.OutBack);
            //meldControlsRect.DOScaleY(1.1f, 0.16f * timingScale);
            meldControlsCanvas.DOFade(1, 0.16f);

            if (!Crafting.CanCraftCard(_card.data, out var errorMessage))
            {
                errorContainer.SetActive(true);
                errorText.text = errorMessage;
                //todo: animation for the error text?
            }

            dialogue.text = "Need some cards hammered up?";

            yield return Timing.WaitForSeconds(0.16f * timingScale);

            //meldControlsRect.DOPunchAnchorPos(Vector2.up * 4f, 0.09f * timingScale);
            //meldControlsRect.DOPunchScale(new Vector2(1,-1)*0.32f, 0.16f);


            cardLayout.DOPunchPosition(Vector2.down * 4f, 0.3f).SetEase(Ease.OutBack);
            cardCanvas.DOFade(1, 0.24f * timingScale);

            yield return Timing.WaitForSeconds(0.16f * timingScale);

            playerIngredientListRect.DOAnchorPosX(-25, 0.25f * timingScale).SetEase(Ease.OutBack);
            playerIngredientListCanvas.DOFade(1, 0.16f * timingScale);

            yield return Timing.WaitForSeconds(0.16f * timingScale);

            playerIngredientListRect.DOPunchAnchorPos(Vector2.left * 4f, 0.4f * timingScale);

            _canEscape = true;

            var fadeSpeed = 0.24f;
            _meldCurrency.GetComponent<CanvasGroup>().DOFade(1, fadeSpeed);
            _scrapCurrency.GetComponent<CanvasGroup>().DOFade(1, fadeSpeed);

            for (var i = 0; i < meldList.ingredientDisplays.Count; i++)
            {
                meldList.ingredientDisplays[i].GetComponent<CanvasGroup>().DOFade(1, fadeSpeed).SetDelay((fadeSpeed/2) * (i+1));
            }

            for (var i = 0; i < scrapList.ingredientDisplays.Count; i++)
            {
                scrapList.ingredientDisplays[i].GetComponent<CanvasGroup>().DOFade(1, fadeSpeed).SetDelay((fadeSpeed/2) * (i+1));
            }


            // mawlerRect.DOScale(new Vector3(-1.06f, .97f), 0.08f*timingScale);
            // yield return Timing.WaitForSeconds(0.08f * timingScale);
            // mawlerRect.DOScale(new Vector3(-.97f, 1.06f), 0.04f*timingScale);
            // yield return Timing.WaitForSeconds(0.04f * timingScale);
            // mawlerRect.DOScale(new Vector3(-1f, 1f), 0.04f*timingScale);
        }

        private IEnumerator<float> AnimateOut()
        {
            yield break;
        }

        private bool m_isClosing;

        public void Close()
        {
            if (m_isClosing) return;

            m_isClosing = true;

            canvasGroup.DOFade(0, 0.25f)
                .OnComplete(() =>
                {
                    m_isClosing = false;
                    this.SetActive(false);
                });
        }

        public void OnCloseButton()
        {
            meldButton.interactable = false;
            scrapButton.interactable = false;
            closeButton.interactable = false;
            _canEscape = false;

            Close();
        }

        private void OnMeldButton()
        {
            if (_card == null) return;
            if (!Crafting.CanCraftCard(_card.data, out var e)) return;

            meldButton.interactable = false;
            scrapButton.interactable = false;

            _delayMeldTimer = MeldButtonDelayTime;
            _delayMeldButtonInput = true;

            Events.Publish(this, EventType.ModifyCurrency, new ModifyCurrencyEventArgs
            {
                CurrencyCode = Currency.MANA_SHARDS,
                Amount = -_card.data.craftingData.cost
            });

            for (var i = 0; i < _card.data.craftingData.ingredients.Count; i++)
            {
                Events.Publish(this, EventType.ModifyItemUses, new ModifyItemUsagesEventArgs
                {
                    ItemName = _card.data.craftingData.ingredients[i].ItemUid,
                    UseAmount = -_card.data.craftingData.ingredients[i].Count,
                });
            }

            Timing.RunCoroutine( cinematic.PlayCinematic(_card.data) );


            Events.Publish(this, EventType.ModifyItemUses, new ModifyItemUsagesEventArgs
            {
                ItemName = _card.data.id,
                UseAmount = 1
            });
        }

        private void OnScrapButton()
        {
            if (_card == null) return;
            if (!Crafting.CanScrapCard(_card.data)) return;

            meldButton.interactable = false;
            scrapButton.interactable = false;

            _delayMeldTimer = MeldButtonDelayTime;
            _delayMeldButtonInput = true;

            Events.Publish(this, EventType.ModifyCurrency, new ModifyCurrencyEventArgs
            {
                CurrencyCode = Currency.MANA_SHARDS,
                Amount = (int)(_card.data.craftingData.cost * Crafting.ScrapManaShardsExchange)
            });

            // for (var i = 0; i < _card.data.craftingData.scraps.Count; i++)
            // {
            //     Events.Publish(this, EventType.ModifyItemUses, new ModifyItemUsagesEventArgs
            //     {
            //         ItemName = _card.data.craftingData.scraps[i].Ingredient,
            //         UseAmount = _card.data.craftingData.scraps[i].Amount,
            //     });
            // }

            var items = _card.data.craftingData.scraps.ConvertAll(s => s.ItemUid);
            var amounts = _card.data.craftingData.scraps.ConvertAll(s => s.Count);

            items.Add(_card.data.id);
            amounts.Add(-1);

            Events.Publish(this, EventType.ModifyItemsUses, new ModifyItemsUsagesEventArgs
            {
                ItemNames = items,
                UseAmounts = amounts,
            });

            Timing.RunCoroutine(ScrapCard());
        }

        private IEnumerator<float> ScrapCard()
        {
            var scrapCard = _card;
            scrapCard.SetSortOrder(CardSortingOrder+1);

            _card = null;

            var task = CreateCardIfNull(scrapCard.data);
            yield return Timing.WaitUntilDone( task.AsCoroutine() );

            yield return Timing.WaitUntilDone(
                scrapCard.GetVfx<CardScrappedComponent, CardScrappedEffect>().Play(null, null));

            Destroy(scrapCard.gameObject);
        }
    }
}