using CrossBlitz.Hero;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.UI.Widgets;
using DG.Tweening;
using UnityEngine;

namespace CrossBlitz.DeckEditAPI
{
    public class HeroToggleButton : ToggleButton
    {
        public RectTransform heroContainer;
        public RectTransform shadow;
        public HeroView heroView;
        public RectTransform lockedOverlay;
        public CanvasGroup hitEffect;

        private bool _isOn;

        private void Start()
        {
            if (hitEffect)
            {
                hitEffect.alpha = 0;
            }
        }

        protected override void ToggleValueChanged(bool isOn)
        {
            base.ToggleValueChanged(isOn);

            DOTween.Kill(heroContainer,true);
            DOTween.Kill(shadow,true);
            DOTween.Kill(hitEffect, true);

            if (isOn && !_isOn)
            {
                heroContainer.DOPunchScale(new Vector3(0.125f,-0.125f, 0), 0.5f);
                heroContainer.DOAnchorPosY(3, 0.25f).SetEase(Ease.OutBack);
                shadow.DOPunchScale(new Vector3(0.125f,-0.125f, 0), 0.5f);
                shadow.DOAnchorPos(new Vector2(3, -4), 0.25f).SetEase(Ease.OutBack);

                if (hitEffect)
                {
                    hitEffect.alpha = 1;
                    hitEffect.DOFade(0, 0.25f);
                }
            }
            else if (isOn && _isOn)
            {
                heroContainer.DOPunchAnchorPos(Vector2.down, 0.5f);
                shadow.DOPunchAnchorPos(Vector2.up, 0.5f);
            }
            else
            {
                heroContainer.DOAnchorPosY(0, 0.4f).SetEase(Ease.OutBounce);
                shadow.DOAnchorPos(new Vector2(0, -2), 0.4f).SetEase(Ease.OutBounce);
            }

            _isOn = isOn;
        }

        public virtual void SetHero(HeroData heroData)
        {
            content = heroData.id;
            heroView.SetHero(heroData, App.HeroData.GetHero(heroData.id));
            lockedOverlay.SetActive(!App.Inventory.OwnsItem(heroData.id));
        }

        private void OnDestroy()
        {
            DOTween.Kill(heroContainer);
            DOTween.Kill(shadow);
            DOTween.Kill(hitEffect);
        }
    }
}