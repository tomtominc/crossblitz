using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.Transition;
using CrossBlitz.Universal;
using CrossBlitz.Utils;
using CrossBlitz.ViewAPI;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.CardCollection
{
    public class MassScrapPopup : MonoBehaviour
    {
        public MassScrapSidePanel massScrapSidePanel;
        public CanvasGroup background;
        public CanvasGroup container;
        public RectTransform containerRect;
        public List<NumberLabelFormatter> cardCounts;
        public List<NumberLabelFormatter> manaShardAmounts;
        public PageBehaviour ingredientsPageBehaviour;
        public IngredientDisplayPageItem ingredientPageItemPrefab;
        public Button confirmButton;
        public Button cancelButton;
        public CanvasGroup confirmButtonCanvas;
        public CanvasGroup cancelButtonCanvas;

        [BoxGroup("Mass Scrap")] public RectTransform itemParticleTargetMin;
        [BoxGroup("Mass Scrap")] public RectTransform itemParticleTargetMax;
        [BoxGroup("Mass Scrap")] public GameObject rainbowEffectObject;

        private int m_totalAcquiredMana;
        private List<CardData> m_scrapCards;
        private List<IngredientInventoryPageData> m_ingredientsAcquired;

        public event Action<int, List<CardData>, List<IngredientInventoryPageData>> OnScrapConfirmed;

        private void Start()
        {
            confirmButton.onClick.AddListener(OnConfirm);
            cancelButton.onClick.AddListener(OnCancel);
        }

        public void Open()
        {
            gameObject.SetActive(true);

            background.alpha = 0;
            container.alpha = 0;
            containerRect.localScale = Vector3.zero;

            rainbowEffectObject.SetActive(false);

            confirmButton.RectTransform().anchoredPosition = new Vector2(-78f, -106f);
            cancelButton.RectTransform().anchoredPosition = new Vector2(78f, -106f);

            confirmButtonCanvas.alpha = 1;
            cancelButtonCanvas.alpha = 1;

            confirmButton.GetComponent<ButtonContainer>().SetInteractable(true);
            cancelButton.GetComponent<ButtonContainer>().SetInteractable(true);

            background.DOFade(1, 0.25f);
            container.DOFade(1, 0.25f);
            containerRect.DOScale(1, 0.25f).SetEase(Ease.OutBack);

            m_scrapCards = Crafting.GetAllExtraScrapCards();

            var commons = 0;
            var rares = 0;
            var mystics = 0;
            var legendaries= 0;

            for (var i = 0; i < m_scrapCards.Count; i++)
            {
                switch (m_scrapCards[i].rarity)
                {
                    case Rarity.Common:
                        commons += App.Inventory.GetItemAmount(m_scrapCards[i].id) - DeckData.MaxCopiesOfCommonCards;
                        break;
                    case Rarity.Rare:
                        rares += App.Inventory.GetItemAmount(m_scrapCards[i].id) - DeckData.MaxCopiesOfRareCards;
                        break;
                    case Rarity.Mythic:
                        mystics += App.Inventory.GetItemAmount(m_scrapCards[i].id) - DeckData.MaxCopiesOfMysticCards;
                        break;
                    case Rarity.Legendary:
                        legendaries += App.Inventory.GetItemAmount(m_scrapCards[i].id) - DeckData.MaxCopiesOfLegendaryCards;
                        break;
                }
            }

            cardCounts[0].UpdateLabel(commons);
            cardCounts[1].UpdateLabel(rares);
            cardCounts[2].UpdateLabel(mystics);
            cardCounts[3].UpdateLabel(legendaries);

            var commonManaExchange =
                (int) (commons * Crafting.GetCraftingCost(Rarity.Common) * Crafting.ScrapManaShardsExchange);
            var rareManaExchange =
                (int) (rares * Crafting.GetCraftingCost(Rarity.Rare) * Crafting.ScrapManaShardsExchange);
            var mysticManaExchange =
                (int) (mystics * Crafting.GetCraftingCost(Rarity.Mythic) * Crafting.ScrapManaShardsExchange);
            var legendaryManaExchange =
                (int) (legendaries * Crafting.GetCraftingCost(Rarity.Legendary) * Crafting.ScrapManaShardsExchange);
            m_totalAcquiredMana = commonManaExchange + rareManaExchange + mysticManaExchange + legendaryManaExchange;

            manaShardAmounts[0].UpdateLabel(commonManaExchange);
            manaShardAmounts[1].UpdateLabel(rareManaExchange);
            manaShardAmounts[2].UpdateLabel(mysticManaExchange);
            manaShardAmounts[3].UpdateLabel(legendaryManaExchange);
            manaShardAmounts[4].UpdateLabel(m_totalAcquiredMana);

            m_ingredientsAcquired = new List<IngredientInventoryPageData>();

            for (var i = 0; i < m_scrapCards.Count; i++)
            {
                var scrapCount = 0;

                switch (m_scrapCards[i].rarity)
                {
                    case Rarity.Common:
                        scrapCount = App.Inventory.GetItemAmount(m_scrapCards[i].id) - DeckData.MaxCopiesOfCommonCards;
                        break;
                    case Rarity.Rare:
                        scrapCount = App.Inventory.GetItemAmount(m_scrapCards[i].id) - DeckData.MaxCopiesOfRareCards;
                        break;
                    case Rarity.Mythic:
                        scrapCount = App.Inventory.GetItemAmount(m_scrapCards[i].id) - DeckData.MaxCopiesOfMysticCards;
                        break;
                    case Rarity.Legendary:
                        scrapCount = App.Inventory.GetItemAmount(m_scrapCards[i].id) - DeckData.MaxCopiesOfLegendaryCards;
                        break;
                }
                for (var j = 0; j < scrapCount; j++)
                {
                    for (var k = 0; k < m_scrapCards[i].craftingData.scraps.Count; k++)
                    {
                        var ingredient = m_scrapCards[i].craftingData.scraps[k];
                        var itemData = m_ingredientsAcquired.Find(r => r.ingredientId == ingredient.ItemUid);

                        if (itemData != null)
                        {
                            itemData.customAmount += ingredient.Count;
                        }
                        else
                        {
                            itemData = new IngredientInventoryPageData();
                            itemData.hasCustomAmount = true;
                            itemData.customAmount = ingredient.Count;
                            itemData.ingredientId = ingredient.ItemUid;
                            m_ingredientsAcquired.Add(itemData);
                        }
                    }
                }
            }

            m_ingredientsAcquired = m_ingredientsAcquired.OrderBy(i =>
                Crafting.GetItemFaction(i.ingredientId)).ToList();

            ingredientsPageBehaviour.InitializePages(ingredientPageItemPrefab,
                5, m_ingredientsAcquired.ConvertAll( i => (IPageItemData)i));
        }

        public void Close()
        {
            background.DOFade(0, 0.25f);
            container.DOFade(0, 0.25f);
            containerRect.DOScale(0, 0.25f).SetEase(Ease.InBack).OnComplete(() =>
            {
                gameObject.SetActive(false);
            });
        }

        private void OnConfirm()
        {
            rainbowEffectObject.SetActive(true);
            OnScrapConfirmed?.Invoke(m_totalAcquiredMana, m_scrapCards, m_ingredientsAcquired);

            Timing.RunCoroutine(AnimateClose());

            Events.Publish(this, EventType.ModifyCurrency, new ModifyCurrencyEventArgs
            {
                CurrencyCode = Currency.MANA_SHARDS,
                Amount = m_totalAcquiredMana
            });

            var items = m_ingredientsAcquired.ConvertAll(i => i.ingredientId);
            var amounts = m_ingredientsAcquired.ConvertAll(i => i.customAmount);

            items.AddRange( m_scrapCards.ConvertAll( c => c.id ));
            amounts.AddRange( m_scrapCards.ConvertAll(c =>
            {
                var scrapCount = 0;

                switch (c.rarity)
                {
                    case Rarity.Common:
                        scrapCount = App.Inventory.GetItemAmount(c.id) - DeckData.MaxCopiesOfCommonCards;
                        break;
                    case Rarity.Rare:
                        scrapCount = App.Inventory.GetItemAmount(c.id) - DeckData.MaxCopiesOfRareCards;
                        break;
                    case Rarity.Mythic:
                        scrapCount = App.Inventory.GetItemAmount(c.id) - DeckData.MaxCopiesOfMysticCards;
                        break;
                    case Rarity.Legendary:
                        scrapCount = App.Inventory.GetItemAmount(c.id) - DeckData.MaxCopiesOfLegendaryCards;
                        break;
                }

                return -scrapCount;
            }));

            Events.Publish(this, EventType.ModifyItemsUses, new ModifyItemsUsagesEventArgs
            {
                ItemNames = items,
                UseAmounts = amounts,
            });
        }

        private IEnumerator<float> AnimateClose()
        {
            cancelButtonCanvas.DOFade(0, 0.12f);
            confirmButton.RectTransform().DOAnchorPosX(0, 0.24f).SetEase(Ease.OutBack);

            cancelButton.GetComponent<ButtonContainer>().SetInteractable(false);
            confirmButton.GetComponent<ButtonContainer>().DisableAndFlash();

            //TransitionController.FlashScreen();
            yield return Timing.WaitForSeconds(0.12f);
            //TransitionController.FlashScreen();

            //yield return Timing.WaitForSeconds( massScrapSidePanel.TotalMassScrapTime + 1f);

            Close();
        }

        private IEnumerator<float> AnimateCloseFromCancel()
        {
            cancelButton.interactable = false;
            confirmButton.interactable = false;

            confirmButtonCanvas.DOFade(0, 0.12f);
            cancelButton.RectTransform().DOAnchorPosX(0, 0.24f).SetEase(Ease.OutBack);

            confirmButton.GetComponent<ButtonContainer>().SetInteractable(false);
            cancelButton.GetComponent<ButtonContainer>().DisableAndFlash();

            yield return Timing.WaitForSeconds(0.4f);

            Close();
        }

        private void OnCancel()
        {
            Close();
            //Timing.RunCoroutine(AnimateCloseFromCancel());
        }
    }
}