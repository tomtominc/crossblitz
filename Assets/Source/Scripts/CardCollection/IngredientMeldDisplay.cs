using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.Universal;
using GameDataEditor;
using TakoBoyStudios.Events;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.CardCollection
{
    [RequireComponent(typeof(IngredientDisplay))]
    public class IngredientMeldDisplay : MonoBehaviour
    {
        public bool insufficientFundsDisplay;
        public SpriteAnimation factionCorner;
        public Image container;
        public IngredientDisplay display;
        public GameObject notEnoughResourcesIcon;

        public void Start()
        {
            Events.Subscribe(EventType.OnModifiedItemUses, OnModifyItemUses);
            UpdateView();
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnModifiedItemUses, OnModifyItemUses);
        }

        private void UpdateView()
        {
            var item = Db.ItemDatabase.GetItem(display.itemName);
            var inventoryItem = App.Inventory.GetItem(display.itemName);
            var gdeItem = new GDEItemData(item.ItemId);

            factionCorner.Play(gdeItem.Faction.ToLower());

            if (inventoryItem != null && inventoryItem.RemainingUses >= display.CustomNumber || !insufficientFundsDisplay)
            {
                display.itemIcon.Play(item.ItemImageUrl);
                display.itemNameLabel.color = "#916154".ToColor();
                display.labelFormatter.label.color = "#916154".ToColor();
                container.color = "#bfa68f".ToColor();
                notEnoughResourcesIcon.SetActive(false);
            }
            else
            {
                display.itemIcon.Play($"{item.ItemImageUrl}-grey");
                display.itemNameLabel.color = "#bfa68f".ToColor();
                display.labelFormatter.label.color = "#bfa68f".ToColor();
                container.color = "#bfa68f".ToColor();
                notEnoughResourcesIcon.SetActive(true);
            }
        }

        private void OnModifyItemUses(IMessage message)
        {
            if (message.Data is OnModifiedItemUsesEventArgs args)
            {
                if (args.ItemNames.Contains( display.itemName) )
                {
                    UpdateView();
                }
            }
        }
    }
}