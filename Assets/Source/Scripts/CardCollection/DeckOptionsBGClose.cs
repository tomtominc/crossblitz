using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CrossBlitz
{
    public class DeckOptionsBGClose : MonoBehaviour, IPointerClickHandler
    {
        public DeckOptions deckOptions;

        public void OnPointerClick(PointerEventData eventData)
        {
            switch (eventData.button)
            {
                case PointerEventData.InputButton.Right:
                    deckOptions.OnCloseButtonPressed();
                    break;
            }
        }
    }
}
