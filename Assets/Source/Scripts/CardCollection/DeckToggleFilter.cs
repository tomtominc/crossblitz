using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.UI.Widgets.Dropdown;
using Sirenix.OdinInspector;
using Source.Scripts.AssetManagement;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.DeckEditAPI
{

    public class DeckToggleFilter : MonoBehaviour
    {
        public class InitParams
        {
            public string ForcedHero;
            public Faction ForcedFaction;
            public List<DeckRecipeData> Decks;

        }
        public RectTransform contentLayout;
        public RectTransform customDeckLayout;
        public DeckToggleButton deckToggleButtonPrefab;

        [BoxGroup("Faction Filter")]
        public bool filterableByFaction;
        [ShowIf("filterableByFaction")]
        [BoxGroup("Faction Filter")]
        public FactionToggleFilter factionFilter;

        [ShowIf("filterableByFaction")]
        [BoxGroup("Faction Filter")]
        public DropdownBox factionFilterDropdown;

        [BoxGroup("Page Properties")]
        public bool usePageProperties;
        [ShowIf("usePageProperties")]
        [BoxGroup("Page Properties")]
        public int decksPerPage = -1;
        [ShowIf("usePageProperties")]
        [BoxGroup("Page Properties")]
        public Button previousArrow;
        [ShowIf("usePageProperties")]
        [BoxGroup("Page Properties")]
        public Button nextArrow;
        [BoxGroup("Page Number")]
        public TextMeshProUGUI pageNumber;

        private List<DeckToggleButton> _deckToggleButtons;
        private ToggleGroup _toggleGroup;
        private List<DeckData> _decks;
        private List<DeckData> _currentDecks;
        private InitParams _initParams;

        // pages
        private int _currentPage;
        private int _maxPages;

        public event Action<bool, string> OnDeckToggle;

        public void Init(InitParams initParams)
        {
            if (customDeckLayout)
            {
                customDeckLayout.DestroyChildren();
            }

            if (contentLayout)
            {
                contentLayout.DestroyChildren();
            }

            _toggleGroup = GetComponent<ToggleGroup>();
            _deckToggleButtons = new List<DeckToggleButton>();
            _initParams = initParams;

            if (previousArrow)
            {
                previousArrow.onClick.RemoveAllListeners();
                previousArrow.onClick.AddListener(OnPreviousButton);
            }

            if (nextArrow)
            {
                nextArrow.onClick.RemoveAllListeners();
                nextArrow.onClick.AddListener(OnNextButton);
            }

            if (usePageProperties)
            {
                _currentPage = 0;
                SetupPages(_decks);
                ShowCurrentPage();
            }
            else
            {
                ShowDecks(_decks);
            }


            if (filterableByFaction && factionFilter)
            {
                factionFilter.OnFactionToggle -= OnFactionsToggled;
                factionFilter.OnFactionToggle += OnFactionsToggled;
            }

            if (filterableByFaction && factionFilterDropdown)
            {
                factionFilterDropdown.ItemSelected -= OnFactionsToggledByDropdown;
                factionFilterDropdown.ItemSelected += OnFactionsToggledByDropdown;
            }
        }

        public void OnDisable()
        {
            if (factionFilter)
            {
                factionFilter.OnFactionToggle -= OnFactionsToggled;
            }

            if (factionFilterDropdown)
            {
                factionFilterDropdown.ItemSelected -= OnFactionsToggledByDropdown;
            }

            if (previousArrow)
            {
                previousArrow.onClick.RemoveListener(OnPreviousButton);
            }

            if (nextArrow)
            {
                nextArrow.onClick.RemoveListener(OnNextButton);
            }

            if (customDeckLayout)
            {
                customDeckLayout.DestroyChildren();
            }

            if (contentLayout)
            {
                contentLayout.DestroyChildren();
            }

            Debug.Log("DeckToggleFilter (OnDisable)");
        }

        private void DeckToggled(bool isOn, string content)
        {
            OnDeckToggle?.Invoke(isOn, content);
        }

        private void OnFactionsToggled(bool isOn, string content)
        {
            if (isOn)
            {
                var faction = GameUtilities.ParseEnum(content, Faction.All);

                if (usePageProperties)
                {
                    if (faction == Faction.All)
                    {
                        SetupPages(_decks);
                    }
                    else
                    {
                        SetupPages(_decks.FindAll(hero => hero.faction == faction));
                    }

                    ShowCurrentPage();
                }
                else
                {
                    if (faction == Faction.All)
                    {
                        ShowDecks(_decks);
                    }
                    else
                    {
                        ShowDecks(_decks.FindAll(hero => hero.faction == faction));
                    }
                }
            }
        }

        private void OnFactionsToggledByDropdown(DropdownItemData choice)
        {
            OnFactionsToggled(true, choice.value);
        }

        private void SetupPages(List<DeckData> decks)
        {
            _currentDecks = decks;
            _maxPages = _currentDecks.Count / decksPerPage + (_currentDecks.Count % decksPerPage == 0 ? 0 : 1);
            _currentPage = Mathf.Clamp(_currentPage, 0, _maxPages - 1);
        }

        private void ShowCurrentPage()
        {
            UpdateArrowButtons();

            if (_currentDecks.Count <= 0)
            {
                ShowEmpty();
            }
            else
            {
                var startingIndex = _currentPage * decksPerPage;
                var endingIndex = startingIndex + decksPerPage;
                var showingHeroes = new List<DeckData>();

                for (var i = startingIndex; i < endingIndex; i++)
                {
                    if (i >= _currentDecks.Count) break;

                    showingHeroes.Add(_currentDecks[i]);
                }

                ShowDecks(showingHeroes);
            }
        }

        private void UpdateArrowButtons()
        {
            previousArrow.SetActive(_currentPage > 0);
            nextArrow.SetActive(_currentPage < _maxPages - 1);

            if (pageNumber) pageNumber.text = $"{_currentPage+1}/{_maxPages}";
        }

        private void OnPreviousButton()
        {
            _currentPage--;
            ShowCurrentPage();
        }

        private void OnNextButton()
        {
            _currentPage++;
            ShowCurrentPage();
        }

        private void ShowEmpty()
        {
            _deckToggleButtons.ForEach( deck => deck.SetActive(false));
        }

        private void ShowDecks(List<DeckData> decks)
        {
            if (customDeckLayout && customDeckLayout.childCount <= 0)
            {
                var custom = Instantiate(deckToggleButtonPrefab, customDeckLayout, false);
                custom.SetDeckAsCustom(new DeckData
                {
                    name = "NEW CUSTOM DECK",
                    faction = _initParams.ForcedFaction,
                    hero = _initParams.ForcedHero,
                    info = "Start with a blank deck and build it the way you want."
                });

                custom.RectTransform().anchoredPosition  = Vector2.zero;
                custom.OnValueChanged += DeckToggled;
                custom.Toggle.group = _toggleGroup;
                custom.Toggle.isOn = true;
            }
            if (decks.Count <= 0)
            {
                Debug.LogError("No heroes passing through here!!");
                return;
            }

            var showCount = !usePageProperties ? decks.Count : decksPerPage <= 0 ? decks.Count : decksPerPage;
            showCount = Mathf.Clamp(showCount, 1, decks.Count);

            for (var i = 0; i < _deckToggleButtons.Count; i++)
            {
                _deckToggleButtons[i].Toggle.isOn = false;
                _deckToggleButtons[i].SetActive(false);
            }

            // This is to make sure that in the next page there's no current hero
            OnDeckToggle?.Invoke(false, string.Empty);

            for (var i = 0; i < showCount; i++)
            {
                if (_deckToggleButtons.Count <= i)
                {
                    var factionToggleButton = Instantiate(deckToggleButtonPrefab, contentLayout, false);
                    factionToggleButton.OnValueChanged += DeckToggled;
                    factionToggleButton.Toggle.group = _toggleGroup;
                    _deckToggleButtons.Add(factionToggleButton);
                }

                _deckToggleButtons[i].SetDeck(decks[i]);
                _deckToggleButtons[i].Toggle.isOn = false;
                _deckToggleButtons[i].SetActive(true);
            }
        }
    }
}