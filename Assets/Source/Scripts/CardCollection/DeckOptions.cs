using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Deck;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.UI;
using CrossBlitz.ViewAPI;
using CrossBlitz.ViewAPI.Popups;
using MEC;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz
{
    public class DeckOptions : MonoBehaviour
    {
        [BoxGroup("Labels")] public TextMeshProUGUI deckTitleLabel;
        [BoxGroup("Labels")] public TextMeshProUGUI optionDescriptionLabel;
        [BoxGroup("Labels")] public List<TextMeshProUGUI> cardDataLabels;
        [BoxGroup("Labels")] public List<TextMeshProUGUI> manaSpreadLabels;

        [BoxGroup("Sprite")] public SpriteAnimation deckArchtype;

        [BoxGroup("Sliders")] public List<Slider> cardDataSliders;
        [BoxGroup("Sliders")] public List<Slider> manaSpreadSliders;

        [BoxGroup("Buttons")] public ButtonContainer renameButton;
        [BoxGroup("Buttons")] public ButtonContainer cloneButton;
        [BoxGroup("Buttons")] public ButtonContainer shareButton;
        [BoxGroup("Buttons")] public ButtonContainer deleteButton;
        [BoxGroup("Buttons")] public ButtonContainer closeButton;

        [BoxGroup("Background")] public RectTransform selectBG;
        [BoxGroup("Background")] public RectTransform bottomBG;

        [BoxGroup("Arrow")] public RectTransform arrow;

        [BoxGroup("DeckList")] public RectTransform deckListYOffset;


        private List<CardValue> _cardsInDeck;
        private List<int> numOfCardTypes;
        private List<int> numOfManaCards;

        private DeckData _deckData;
        private DeckViewItem _deckViewItem;
        private bool _init = true;
        private bool _updatePage = false;
        private bool _animateBars = false;

        private float _ratio = 0;
        private float _ratio_speed = .01f;
        private float _ratio_accel = .05f;

        public void Init()
        {
            renameButton.Button.onClick.AddListener(OnRenameButtonPressed);
            cloneButton.Button.onClick.AddListener(OnCloneButtonPressed);
            shareButton.Button.onClick.AddListener(OnShareButtonPressed);
            deleteButton.Button.onClick.AddListener(OnDeleteButtonPressed);
            closeButton.Button.onClick.AddListener(OnCloseButtonPressed);

            // Initialize the Lists
            numOfCardTypes = new List<int>();
            for (int x = 0; x < 6; x++)
            {
                numOfCardTypes.Add(0);
            }

            numOfManaCards = new List<int>();
            for (int x = 0; x < 8; x++)
            {
                numOfManaCards.Add(0);
            }

            _init = false;
        }

        public void Update()
        {
            if (_updatePage) 
            {
                deckTitleLabel.text = _deckData.name;
                _deckViewItem.deckNameLabel.text = _deckData.name;
                App.PlayerDecks.SetIsDirty(true);
                App.SaveAll();
                //Debug.LogError("RenameDeck()");
                _updatePage = false;
            }

            if (_animateBars)
            {
                if (_ratio < 1)
                {
                    _ratio += _ratio_speed;
                    _ratio_speed += (_ratio_accel * Time.deltaTime);
                }
                else
                {
                    _animateBars = false;
                    _ratio = 1;
                }

                //Setup the Card Data
                var numberOfMinionCards = (numOfCardTypes[0] + numOfCardTypes[1] + numOfCardTypes[2]);
                cardDataLabels[0].text = Mathf.Floor(numberOfMinionCards * _ratio).ToString();
                cardDataLabels[1].text = Mathf.Floor(numOfCardTypes[3] * _ratio).ToString();
                cardDataLabels[2].text = Mathf.Floor(numOfCardTypes[4] * _ratio).ToString();
                cardDataLabels[3].text = Mathf.Floor(numOfCardTypes[5] * _ratio).ToString();

                // Set the Melee Minion Bar
                var barXOffsetMelee = 0;
                var barWidthMelee = 0f;
                if(numOfCardTypes[0] > 0 && _deckData.GetCardCount() > 0)
                {
                    var barValueMelee = (float)numOfCardTypes[0] / (float)_deckData.GetCardCount();
                    barWidthMelee = barValueMelee * 130 * _ratio;
                    cardDataSliders[0].RectTransform().SetWidth(barWidthMelee);
                    cardDataSliders[0].SetActive(true); barXOffsetMelee = 2;
                }
                else
                {
                    cardDataSliders[0].SetActive(false);
                }


                // Set the Ranged Minion Bar
                var barPosRanged = barWidthMelee;
                cardDataSliders[1].RectTransform().anchoredPosition = new Vector2(cardDataSliders[0].RectTransform().anchoredPosition.x + barPosRanged + barXOffsetMelee, cardDataSliders[1].RectTransform().anchoredPosition.y);
                var barXOffsetRanged = 0;
                var barWidthRanged = 0f;
                if (numOfCardTypes[1] > 0 && _deckData.GetCardCount() > 0)
                {
                    var barValueRanged = (float)numOfCardTypes[1] / (float)_deckData.GetCardCount();
                    barWidthRanged = barValueRanged * 130 * _ratio;
                    cardDataSliders[1].RectTransform().SetWidth(barWidthRanged);
                    cardDataSliders[1].SetActive(true); 
                    barXOffsetRanged = 2;
                }
                else
                {
                    cardDataSliders[1].SetActive(false);
                }

                // Set the Arcane Minion Bar
                var barPosArcane = barPosRanged + barWidthRanged;
                cardDataSliders[2].RectTransform().anchoredPosition = new Vector2(cardDataSliders[0].RectTransform().anchoredPosition.x + barPosArcane + barXOffsetMelee + barXOffsetRanged, cardDataSliders[2].RectTransform().anchoredPosition.y);
                var barWidthArcane = 0f;
                if (numOfCardTypes[2] > 0 && _deckData.GetCardCount() > 0)
                {
                    var barValueArcane = (float)numOfCardTypes[2] / (float)_deckData.GetCardCount();
                    barWidthArcane = barValueArcane * 130 * _ratio;
                    cardDataSliders[2].RectTransform().SetWidth(barWidthArcane);
                    cardDataSliders[2].SetActive(true);
                }
                else
                {
                    cardDataSliders[2].SetActive(false);
                }
                
                // Set the Rest of the Card Bars
                for (var i = 3; i < cardDataSliders.Count; i++)
                {
                    if(numOfCardTypes[i] > 0 && _deckData.GetCardCount() > 0)
                    {
                        var barValue = (float)numOfCardTypes[i] / (float)_deckData.GetCardCount();
                        cardDataSliders[i].SetValueWithoutNotify(barValue * _ratio);
                        cardDataSliders[i].fillRect.SetActive(true);
                    }
                    else
                    {
                        cardDataSliders[i].fillRect.SetActive(false);
                    }
                }

                //Setup the Mana Spread Data
                for (var i = 0; i < manaSpreadLabels.Count; i++)
                {
                    if (numOfManaCards[i] > 0 && _deckData.GetCardCount() > 0)
                    {
                        var barValue = (float)numOfManaCards[i] / (float)_deckData.GetCardCount();
                        manaSpreadSliders[i].SetValueWithoutNotify(barValue * _ratio);
                        manaSpreadSliders[i].fillRect.SetActive(true);
                    }
                    else
                    {
                        manaSpreadSliders[i].fillRect.SetActive(false);
                    }
                    manaSpreadLabels[i].text = Mathf.Floor(numOfManaCards[i] * _ratio).ToString();
                }
            }
        }
        public void Setup(DeckData deck, DeckViewItem deckViewItem)
        {

            if (_init) Init();

            // Reset the Lists
            for (int x = 0; x < numOfCardTypes.Count; x++)
            {
                numOfCardTypes[x] = 0;
            }
            for (int x = 0; x < cardDataLabels.Count; x++)
            {
                cardDataLabels[x].text = "0";
            }
            for (int x = 0; x < cardDataSliders.Count; x++)
            {
                if( x <3 ) cardDataSliders[x].SetActive(false);
                else cardDataSliders[x].fillRect.SetActive(false);
            }
            
            for (int x = 0; x < numOfManaCards.Count; x++)
            {
                numOfManaCards[x] = 0;
            }
            for (int x = 0; x < manaSpreadLabels.Count; x++)
            {
                manaSpreadLabels[x].text = "0";
            }
            for (int x = 0; x < manaSpreadSliders.Count; x++)
            {
                manaSpreadSliders[x].fillRect.SetActive(false);
            }

            deckArchtype.SetActive(false);


            // Reset the Minion Bar Widths
            for (var i = 0; i < 3; i++)
            {
                cardDataSliders[i].RectTransform().anchoredPosition = new Vector2(0f, 0f);
            }

            // Set the current Deck Data
            _deckData = deck;
            _deckViewItem = deckViewItem;

            // Setup the Name of the Deck
            deckTitleLabel.text = _deckData.name;

            var relicCount = deck.GetRelicCount();

            if(deck.cards.Count > 0 && deck.cards.Count != relicCount)
            { 
                // Setup the Deck Archtype
                var archetype = Db.DeckRecipeDatabase.GetClosestArchetype(_deckData);
                if(archetype != null)
                {
                    deckArchtype.Play(archetype.archetype);
                    deckArchtype.SetActive(true);
                }

                //Setup the Cards Data
                foreach (var card in _deckData.cards)
                {

                    var cardData = Db.CardDatabase.GetCard(card.id);
                    var cost = cardData.GetCost();
                    var cardType = cardData.type;
                    var attackType = cardData.attackType;
                    var numOfCard = _deckData.GetAmountOfSingleCard(card.id);

                    // Skip Relic Cards
                    if (cardData.type == CardType.Relic || cardData.type == CardType.Elder_Relic) continue;

                    // Count the Card Types
                    switch (cardType)
                    {
                        case CardType.Minion:
                            if (attackType == AttackType.Melee)
                            {
                                numOfCardTypes[0] += numOfCard;
                            }
                            else if (attackType == AttackType.Ranged)
                            {
                                numOfCardTypes[1] += numOfCard;
                            }
                            else if (attackType == AttackType.Arcane)
                            {
                                numOfCardTypes[2] += numOfCard;
                            }
                            break;
                        case CardType.Spell:
                            numOfCardTypes[3] += numOfCard;
                            break;
                        case CardType.Trick:
                            numOfCardTypes[4] += numOfCard;
                            break;
                        case CardType.Power:
                            numOfCardTypes[5] += numOfCard;
                            break;
                    }

                    // Count the Mana Costs
                    if (cardData.type != CardType.Power)
                    {
                        if (cost >= 7) cost = 7;
                        if (cost - 1 >= 0 && cost - 1 < (numOfManaCards.Count))
                        {
                            numOfManaCards[cost - 1] += numOfCard;
                        }
                        else
                        {
                            Debug.LogError(cardData.name + " cost: " + cost);
                        }
                    }
                }

                ////Setup the Card Data
                //var numberOfMinionCards = (numOfCardTypes[0] + numOfCardTypes[1] + numOfCardTypes[2]);
                //cardDataLabels[0].text = numberOfMinionCards.ToString();
                //cardDataLabels[1].text = numOfCardTypes[3].ToString();
                //cardDataLabels[2].text = numOfCardTypes[4].ToString();
                //cardDataLabels[3].text = numOfCardTypes[5].ToString();

                //// Set the Melee Minion Bar
                //var barXOffset = 0;
                //var barValue = (float)numOfCardTypes[0] / (float)_deckData.GetCardCount();
                //var barWidth = barValue * 130;
                //cardDataSliders[0].RectTransform().SetWidth(barWidth);
                //if (barValue <= 0) cardDataSliders[0].SetActive(false);
                //else { cardDataSliders[0].SetActive(true); barXOffset += 6; }

                //// Set the Ranged Minion Bar
                //var barPos = barWidth;
                //cardDataSliders[1].RectTransform().anchoredPosition = new Vector2(cardDataSliders[1].RectTransform().anchoredPosition.x + barWidth + barXOffset, cardDataSliders[1].RectTransform().anchoredPosition.y);
                //barValue = (float)numOfCardTypes[1] / (float)_deckData.GetCardCount();
                //barWidth = barValue * 130;
                //cardDataSliders[1].RectTransform().SetWidth(barWidth);
                //if (barValue <= 0) cardDataSliders[1].SetActive(false);
                //else {cardDataSliders[1].SetActive(true); barXOffset += 6; }

                //// Set the Arcane Minion Bar
                //barPos += barWidth;
                //cardDataSliders[2].RectTransform().anchoredPosition = new Vector2(cardDataSliders[2].RectTransform().anchoredPosition.x + barPos + barXOffset, cardDataSliders[2].RectTransform().anchoredPosition.y);
                //barValue = (float)numOfCardTypes[2] / (float)_deckData.GetCardCount();
                //barWidth = barValue * 130;
                //cardDataSliders[2].RectTransform().SetWidth(barWidth);
                //if (barValue <= 0) cardDataSliders[2].SetActive(false);
                //else cardDataSliders[2].SetActive(true);

                //for (var i = 3; i < cardDataSliders.Count; i++)
                //{
                //    barValue = (float)numOfCardTypes[i] / (float)_deckData.GetCardCount();
                //    cardDataSliders[i].SetValueWithoutNotify(barValue);

                //    if (barValue <= 0) cardDataSliders[i].fillRect.SetActive(false);
                //    else cardDataSliders[i].fillRect.SetActive(true);
                //}

                ////Setup the Mana Spread Data
                //for (var i = 0; i < manaSpreadLabels.Count; i++)
                //{
                //    manaSpreadLabels[i].text = numOfManaCards[i].ToString();

                //    barValue = (float)numOfManaCards[i] / (float)_deckData.GetCardCount();
                //    manaSpreadSliders[i].SetValueWithoutNotify(barValue);

                //    if (barValue <= 0) manaSpreadSliders[i].fillRect.SetActive(false);
                //    else manaSpreadSliders[i].fillRect.SetActive(true);
                //}
            }

            // Setup the Background and Arrow
            var yOffsetSelectBox = -7f;
            var yBottom = 159f;
            var yOffsetDeckScroll = 0f;
            var yOffsetArrow = 0f;

            // If in deck edit menu
            if (!_deckViewItem.isHeader)
            {
                yOffsetSelectBox = 34;
                yBottom = 165;
                yOffsetDeckScroll = deckListYOffset.localPosition.y;
            }
            else
            {
                yOffsetArrow = 34;
            }

            selectBG.anchoredPosition = new Vector2(selectBG.anchoredPosition.x, deckViewItem.RectTransform().anchoredPosition.y- yOffsetSelectBox + yOffsetDeckScroll);
            bottomBG.SetBottom(yBottom);
            arrow.anchoredPosition = new Vector2(arrow.anchoredPosition.x, deckViewItem.RectTransform().anchoredPosition.y + yOffsetDeckScroll + yOffsetArrow);
            _ratio = 0;
            _ratio_speed = .01f;
            _animateBars = true;
        }

        private void OnRenameButtonPressed()
        {
            PopupController.Open(new PopupInfo
            {
                style = PopupWindowStyle.Rename,
                title = "RENAME DECK",
                body = "CURRENT DECK NAME",
                confirm = "RENAME",
                deny = "CANCEL",
                deckData = _deckData
            });

            PopupController.OnPopupChoice += OnRenameActionResponse;
        }

        private void OnRenameActionResponse(bool rename)
        {
            PopupController.OnPopupChoice -= OnRenameActionResponse;

            if (rename) _updatePage = true;
        }

        public void OnCloneButtonPressed()
        {
            PopupController.Open(new PopupInfo
            {
                style = PopupWindowStyle.Generic,
                title = "CLONE DECK",
                body = "CREATE A CLONE OF THE SELECTED DECK?",
                confirm = "CLONE",
                deny = "CANCEL",
            });

            PopupController.OnPopupChoice += OnCloneActionResponse;
        }

        private void OnCloneActionResponse(bool clone)
        {
            PopupController.OnPopupChoice -= OnCloneActionResponse;

            if (clone) DeckEditMenu.Instance.deckCollection.cardList.CloneDeck(_deckData);

        }

        public void OnShareButtonPressed()
        {
            PopupController.Open(new PopupInfo
            {
                style = PopupWindowStyle.Generic,
                title = "SHARE DECK",
                body = "The selected deck's <#ED5E5E>Recipe Code</color> has been <#ED5E5E>copied</color> to your device's clipboard.\n\nWhen creating a new deck, <#ED5E5E>paste</color> the deck's <#ED5E5E>Recipe Code</color> in the <#ED5E5E>'IMPORT DECK'</color> menu to <#ED5E5E>share with your friends!</color>",
                confirm = "OKAY!",
            });

            PopupController.OnPopupChoice += OnShareActionResponse;
            
        }

        private void OnShareActionResponse(bool SHARE)
        {
            PopupController.OnPopupChoice -= OnShareActionResponse;
            GUIUtility.systemCopyBuffer = _deckData.ToRecipe();
        }

        public void OnDeleteButtonPressed()
        {
            PopupController.Open(new PopupInfo
            {
                style = PopupWindowStyle.Generic,
                title = "DELETE DECK",
                body = "DELETE THE SELECTED DECK?",
                confirm = "DELETE",
                deny = "CANCEL",
            });

            PopupController.OnPopupChoice += OnDeleteActionResponse;
        }

        private void OnDeleteActionResponse(bool delete)
        {
            PopupController.OnPopupChoice -= OnDeleteActionResponse;

            if (delete) 
            {
                DeckEditMenu.Instance.deckCollection.cardList.OnForceQuitDeckEditAndDelete(_deckData);
                gameObject.SetActive(false);
            }
        }

        public void OnCloseButtonPressed()
        {
            //_deckViewItem.iconAnimation.Play("magnifying-lens");
           gameObject.SetActive(false);
        }
    }
}
