using System;
using CrossBlitz.UI.Widgets;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz
{
    public class CardSearchFilter : MonoBehaviour
    {

        [BoxGroup("Setup")] public ToggleButton searchToggleButton;
        [BoxGroup("Setup")] public Modal dropdownModal;
        [BoxGroup("Setup")] public bool searchActive;

        [BoxGroup("Setup")] public Sprite searchSpriteNormal;
        [BoxGroup("Setup")] public Sprite searchSpritePressed;
        [BoxGroup("Setup")] public Sprite clearSpriteNormal;
        [BoxGroup("Setup")] public Sprite clearSpritePressed;

        [BoxGroup("Search Dropdown")] public GameObject searchContainer;
        [BoxGroup("Search Dropdown")] public TextMeshProUGUI searchLabel;
        [BoxGroup("Search Dropdown")] public TMP_InputField inputLabel;
        [BoxGroup("Search Dropdown")] public Button searchButton;
        [BoxGroup("Search Dropdown")] public Button clearButton;

        private bool _searchToggleIsEnabled;

        public event Action<string> OnSearchChanged;

        void Start()
        {
            inputLabel.SetTextWithoutNotify(string.Empty);
            
            searchActive = false;

            searchButton.onClick.AddListener(OnSearchButton);
            clearButton.onClick.AddListener(OnClearButton);
        }

        private void OnEnable()
        {
            if (!dropdownModal)
            {
                dropdownModal = FindObjectOfType<Modal>();
            }
            
            if (dropdownModal)
            {
                dropdownModal.Disabled -= OnModalDisabled;
                dropdownModal.Disabled += OnModalDisabled;
            }

            if (searchToggleButton)
            {
                searchToggleButton.OnValueChanged -= OnSearchToggleButton;
                searchToggleButton.OnValueChanged += OnSearchToggleButton;
            }  
        }

        private void OnDisable()
        {
            if (!dropdownModal)
            {
                dropdownModal = FindObjectOfType<Modal>();
            }
            
            if (dropdownModal)
            {
                dropdownModal.Disabled -= OnModalDisabled;
            }

            if (searchToggleButton)
            {
                searchToggleButton.OnValueChanged -= OnSearchToggleButton;
            }  
        }

        private void OnSearchToggleButton(bool isOn, string content)
        {
            _searchToggleIsEnabled = !isOn;
            OnDropDownButton();
        }

        private void OnDropDownButton()
        {

            if (!_searchToggleIsEnabled)
            {
                _searchToggleIsEnabled = true;
                searchContainer.SetActive(true);

                if (dropdownModal)
                {
                    dropdownModal.ParentObjectToModal(searchContainer.RectTransform());
                }

                //OnClickedDropdown?.Invoke(this);
            }
            else
            {
                _searchToggleIsEnabled = false;

                if (dropdownModal)
                {
                    searchContainer.RectTransform().SetParent(transform);
                }

                searchContainer.SetActive(false);

                if (searchToggleButton)
                {
                    searchToggleButton.Toggle.SetIsOnWithoutNotify(false);
                }
            }
        }

        private void OnModalDisabled()
        {
            if (_searchToggleIsEnabled)
            {
                OnDropDownButton();
            }
        }

        private void OnSearchButton()
        {
            searchActive = true;
            searchToggleButton.activeGroup.GetComponent<Image>().sprite = searchSpritePressed;
            searchToggleButton.inactiveGroup.GetComponent<Image>().sprite = searchSpriteNormal;
            
            OnSearchChanged?.Invoke(searchLabel.text);
        }

        private void OnClearButton()
        {
            inputLabel.SetTextWithoutNotify(string.Empty);

            searchActive = false;
            searchToggleButton.activeGroup.GetComponent<Image>().sprite = clearSpritePressed;
            searchToggleButton.inactiveGroup.GetComponent<Image>().sprite = clearSpriteNormal;
            
            OnSearchChanged?.Invoke(string.Empty);
        }
    }
}
