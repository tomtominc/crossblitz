using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CrossBlitz.AssetManagement;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.CardCollection;
using CrossBlitz.CardCollection.CardBacks;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Deck;
using CrossBlitz.Fables;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.Items.Data;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.UI.Widgets.Dropdown;
using DG.Tweening;
using GameDataEditor;
using MEC;
using Sirenix.OdinInspector;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.DeckEditAPI
{
    public class DeckCollectionMenu : MonoBehaviour
    {
        private int CardsPerPage = 8;

        [BoxGroup("Filters")] public DropdownBox factionFilter;
        [BoxGroup("Filters")] public DropdownBox classFilter;
        [BoxGroup("Filters")] public DropdownBox rarityFilter;
        [BoxGroup("Filters")] public DropdownBox typeFilter;
        [BoxGroup("Filters")] public DropdownBox keywordFilter;
        [BoxGroup("Filters")] public DropdownBox cardSetFilter;
        [BoxGroup("Filters")] public CardSearchFilter cardSearchFilter;
        [BoxGroup("Filters")] public DropdownBox relicFilter;
        [BoxGroup("Filters")] public DropdownBox cardBacksFilter;

        [BoxGroup("Camera")] public Camera camera3d;

        [BoxGroup("Card Collection")] public SpriteAnimation backgroundContainerAnimator;
        [BoxGroup("Card Collection")] public RectTransform menu;
        [BoxGroup("Card Collection")] public RectTransform draggableCardModal;
        [BoxGroup("Card Collection")] public GameObject cardCollection;
        [BoxGroup("Card Collection")] public RectTransform cardCollectionLayout;
        [BoxGroup("Card Collection")] public TextMeshProUGUI pageCount;
        [BoxGroup("Card Collection")] public Button nextButtonCardCollection;
        [BoxGroup("Card Collection")] public Button previousButtonCardCollection;

        [BoxGroup("Deck Editor")] public DecksListMenu deckList;
        [BoxGroup("Deck Editor")] public DeckCardList cardList;

        [BoxGroup("Card Backs Menu")] public CardBackMenu cardBackMenu;

        [BoxGroup("Universal")] public GameObject noCardsFoundLabel;
        [BoxGroup("Universal")] public SpriteAnimation noCardsFoundAnimation;

        private DeckEditSettings _currentSettings;
        private List<CardData> _cards;
        private Dictionary<Faction, List<CardData>> _cardsByFaction;
        private List<CardView> _cardViews;
        private int _pageIndex;
        private GameObject _cardPrefab;
        private int _factionIndex;
        private DragCardDeckEditor _draggableCard;
        private bool _skipCardDeckList;
        [HideInInspector] public bool onMeldCard = false;
        private FilterCardSettings savedCardFilterSettings = new FilterCardSettings();
        private FilterCardSettings savedRelicFilterSettings = new FilterCardSettings();
        [HideInInspector] public FilterCardSettings savedCardBackFilterSettings = new FilterCardSettings();
        private bool _transferSettings;
        private List<Type> _additionalComponents;
        private bool _spawnRightDir = true;


        public GameObject CardPrefab => _cardPrefab;
        public DragCardDeckEditor DraggableCard => _draggableCard;

        private static readonly List<Faction> FactionsByOrder = new List<Faction>
        {
            Faction.War,
            Faction.Chaos,
            Faction.Balance,
            Faction.Fortune,
            Faction.Nature,
            Faction.Neutral
        };

        private List<Faction> _factionsAvailable;
        private Faction CurrentFaction
        {
            get
            {
                if (_factionIndex >= _factionsAvailable.Count)
                    _factionIndex = _factionsAvailable.Count - 1;
                else if (_factionIndex < 0)
                    _factionIndex = 0;

                return _factionsAvailable[_factionIndex];
            }
        }

        private Faction PreviousFaction
        {
            get
            {
                var prev = _factionIndex - 1;

                if (prev >= _factionsAvailable.Count)
                    prev = _factionsAvailable.Count - 1;
                else if (prev < 0)
                    prev = 0;

                return _factionsAvailable[prev];
            }
        }

        private Faction NextFaction
        {
            get
            {
                var next = _factionIndex + 1;

                if (next >= _factionsAvailable.Count)
                    next = _factionsAvailable.Count - 1;
                else if (next < 0)
                    next = 0;

                return _factionsAvailable[next];
            }
        }

        private void Start()
        {
            Events.Subscribe(EventType.OnModifiedItemUses, OnModifiedItemUses);
            if (!_skipCardDeckList) cardList.OnDeckChanged += OnDeckChanged;
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnModifiedItemUses, OnModifiedItemUses);
            if(!_skipCardDeckList) cardList.OnDeckChanged -= OnDeckChanged;
        }

        private void OnDeckChanged()
        {
            for (var i = 0; i < _cardViews.Count; i++)
            {
                if (_cardViews[i].IsActive())
                {
                    var collectionComponent = _cardViews[i].GetCardComponent<CardCollectionComponentsLoader>();

                    if (collectionComponent != null)
                    {
                        collectionComponent.cardCollectionComponent.Refresh(DeckEditMenu.Instance.Settings, _cardViews[i].data );
                    }
                }
            }
        }

        private void OnModifiedItemUses(IMessage message)
        {
            if (message.Data is OnModifiedItemUsesEventArgs args)
            {
                for (var i = 0; i < App.PlayerDecks.GetDecks().Count; i++)
                {
                    App.PlayerDecks.GetDecks()[i].IsValid();
                }

                var anyCards = args.ItemNames.Exists( i => Db.ItemDatabase.GetItem(i)?.ItemClass == ItemClassType.Card);

                if (anyCards)
                {
                    ConfigureScreen();
                    RefreshFactionFilter();
                    FilterCards(_currentSettings.FilterCardSettings);
                }
            }
        }

        public async Task Init()
        {
            _cardPrefab = await AddressableReferenceLoader.GetAsset("Card_Standard", true);
            var cardDeckViewPrefab = await AddressableReferenceLoader.GetAsset("Card_DeckView", true);

            nextButtonCardCollection.onClick.AddListener(OnNextButton);
            previousButtonCardCollection.onClick.AddListener(OnPreviousButton);

            nextButtonCardCollection.interactable = false;
            previousButtonCardCollection.interactable = false;

            _transferSettings = false;
            savedCardFilterSettings.SetAsDefault();
            savedRelicFilterSettings.SetAsDefault();
            savedCardBackFilterSettings.SetAsDefault();

            factionFilter.ItemSelected += OnFactionFilter;
            classFilter.ItemSelected += OnClassFilter;
            //rarityFilter.OnRarityToggle += OnRarityToggle;
            rarityFilter.ItemSelected += OnRarityFilter;
            typeFilter.ItemSelected += OnTypeFilter;
            keywordFilter.ItemSelected += OnKeywordFilter;
            cardSetFilter.ItemSelected += OnSetFilter;
            if (cardSearchFilter) cardSearchFilter.OnSearchChanged += OnSearchChanged;

            relicFilter.ItemSelected += OnRelicFilter;
            cardBacksFilter.ItemSelected += OnCardBackFilter;

            _skipCardDeckList = DeckEditMenu.Instance == null;

            if (!_skipCardDeckList)
            {
                deckList.OnDeckSelected += OnDeckSelected;
                cardList.OnFinishedEditingDeck += OnFinishedDeckEdit;
            }

            var settings = new CreateCardFactorySettings
            {
                CardPrefab = _cardPrefab,
                CardData = new CardData { id = GDEItemKeys.Card_HeavySwordHero },
                Layout = draggableCardModal,
                SortingOrder = 800,
                SortingLayer = "Collection",
                CardObjectOffsetPosition = Vector2.zero,
                StartsDisabled = true,
                AdditionalComponents = new List<Type>{typeof(DragRotator), typeof(DragCardDeckEditor) }
            };

            var card = CardFactory.Create(settings, null);
            card.RectTransform().localScale = Vector3.one;
            _draggableCard = card.GetComponent<DragCardDeckEditor>();
            _draggableCard.SetDeckViewItemPrefab(cardDeckViewPrefab);
            var dragRotator = card.GetComponent<DragRotator>();
            dragRotator.SetCamera(camera3d);

            if (!_skipCardDeckList)
            {
                cardList.Initialize(cardDeckViewPrefab, _draggableCard);
                deckList.Init();
            }
        }

        public void Open(DeckEditSettings settings)
        {
            if (!_skipCardDeckList)
            {
                switch (settings.MenuMode)
                {
                    case CollectionMenuMode.Collection:
                        backgroundContainerAnimator.Play("standard");
                        break;
                    case CollectionMenuMode.Relics:
                        backgroundContainerAnimator.Play("relics");
                        break;
                    case CollectionMenuMode.CardBacks:
                        backgroundContainerAnimator.Play("card-backs");
                        break;
                    case CollectionMenuMode.ManaMeld:
                        backgroundContainerAnimator.Play("mana-meld");
                        break;
                }
            }

            if(settings.MenuMode == CollectionMenuMode.ManaMeld)
            {
                CardsPerPage = 4;
                cardCollectionLayout.GetComponent<GridLayoutGroup>().constraintCount = 2;
                cardCollectionLayout.GetComponent<GridLayoutGroup>().cellSize = new Vector2(100, 100);
                cardCollectionLayout.GetComponent<GridLayoutGroup>().spacing = new Vector2(100, 12);
            }

            _currentSettings = settings;
            _transferSettings = true;

            _factionIndex = 0;
            _pageIndex = 0;

            cardCollectionLayout.DestroyChildren();
            _cardViews = new List<CardView>();

            for (var i = 0; i < App.PlayerDecks.GetDecks().Count; i++)
            {
                App.PlayerDecks.GetDecks()[i].IsValid();
            }

            ConfigureScreen();
            RefreshFactionFilter();
            FilterCards(_currentSettings.FilterCardSettings);

        }

        private void ConfigureScreen()
        {
            gameObject.SetActive(true);

            switch (_currentSettings.OpenMode)
            {
                case DeckOpenMode.Collection when _currentSettings.MenuMode == CollectionMenuMode.ManaMeld:
                case DeckOpenMode.DeckEditor when _currentSettings.MenuMode == CollectionMenuMode.ManaMeld:
                {
                    deckList.SetActive(false);
                    cardList.SetActive(false);
                    RefreshFactionFilter();
                    break;
                }
                case DeckOpenMode.Collection:
                {
                    deckList.SetActive(true);
                    cardList.SetActive(false);
                    deckList.Open(_currentSettings);
                    break;
                }
                case DeckOpenMode.DeckEditor:
                {
                    deckList.SetActive(false);
                    cardList.SetActive(true);
                    RefreshFactionFilter();

                    if (_currentSettings.DeckSettings?.deckData != null && _currentSettings.DeckSettings.deckData.IsValid())
                    {
                        cardList.OpenWithDeck(_currentSettings.DeckSettings.deckData,
                            _currentSettings.MenuMode != CollectionMenuMode.Collection ? 0 : 2,
                            _currentSettings.MenuMode == CollectionMenuMode.CardBacks);
                    }
                    else
                    {
                        Debug.LogError($"THIS IS NOT A VALID DECK!!! {_currentSettings.DeckSettings?.deckData?.name}");
                    }
                    break;
                }
            }
        }

        private void RefreshFactionFilter()
        {
            if (_currentSettings.MenuMode == CollectionMenuMode.CardBacks)
            {
                return;
            }

            switch (_currentSettings.OpenMode)
            {
                case DeckOpenMode.Collection:
                {
                    factionFilter.itemsToLeaveOut = new List<string>();

                    if (_currentSettings.FilterCardSettings.forcedFaction != Faction.All)
                    {
                        var factions = new List<Faction>(FactionsByOrder);

                        for (var i = factions.Count - 1; i > -1; i--)
                        {
                            if (factions[i] != Faction.All &&
                                factions[i] != Faction.Neutral &&
                                factions[i] != _currentSettings.FilterCardSettings.forcedFaction)
                            {
                                factionFilter.itemsToLeaveOut.Add(factions[i].ToString());
                            }
                        }
                    }

                    break;
                }
                case DeckOpenMode.DeckEditor:
                {
                    factionFilter.itemsToLeaveOut = new List<string>();

                    var factions = new List<Faction>(FactionsByOrder);

                    for (var i = factions.Count - 1; i > -1; i--)
                    {
                        if (factions[i] != Faction.All && factions[i] != Faction.Neutral &&
                            factions[i] != _currentSettings.DeckSettings.deckData.faction)
                        {
                            factionFilter.itemsToLeaveOut.Add(factions[i].ToString());
                        }
                    }

                    break;
                }
            }

            //factionFilter.RefreshDropBox();

        }

        private void OnDeckSelected(DeckViewItem deck)
        {
            _currentSettings.OpenMode = DeckOpenMode.DeckEditor;
            _currentSettings.DeckSettings = new DeckSettings { deckData = deck.MDeckData };
            DeckEditMenu.Instance.ChangeOpenSettings(_currentSettings);
        }

        private void OnFinishedDeckEdit()
        {
            _currentSettings.OpenMode = DeckOpenMode.Collection;
            _currentSettings.DeckSettings =new DeckSettings();
            DeckEditMenu.Instance.ChangeOpenSettings(_currentSettings);
        }

        public void OnForceQuitDeckEditAndDelete(DeckData deckData)
        {

            switch (_currentSettings.OpenMode)
            {
                case DeckOpenMode.Collection: // do nothing for now.
                    break;
                case DeckOpenMode.DeckEditor:
                    _currentSettings.OpenMode = DeckOpenMode.Collection;
                    _currentSettings.DeckSettings = new DeckSettings();
                    DeckEditMenu.Instance.ChangeOpenSettings(_currentSettings);

                    break;
            }

            //App.PlayerDecks.DeleteDeck(deckData.uid);
        }

        private void OnFactionFilter(DropdownItemData itemData)
        {
            AudioController.PlaySound("UI-StartingHand");
            _currentSettings.FilterCardSettings.faction = GameUtilities.ParseEnum(itemData.value, Faction.All);
            _currentSettings.FilterCardSettings.TransferSettings(savedCardFilterSettings);
            _transferSettings = false;
            FilterCards(_currentSettings.FilterCardSettings);
        }

        private void OnClassFilter(DropdownItemData itemData)
        {
            AudioController.PlaySound("UI-StartingHand");
            _currentSettings.FilterCardSettings.@class = GameUtilities.ParseEnum(itemData.value, Class.All);
            _currentSettings.FilterCardSettings.TransferSettings(savedCardFilterSettings);
            _transferSettings = false;
            FilterCards(_currentSettings.FilterCardSettings);
        }

        private void OnRarityFilter(DropdownItemData itemData)
        {
            AudioController.PlaySound("UI-StartingHand");
            _currentSettings.FilterCardSettings.rarity = GameUtilities.ParseEnum(itemData.value, Rarity.All);
            _currentSettings.FilterCardSettings.TransferSettings(savedCardFilterSettings);
            _transferSettings = false;
            FilterCards(_currentSettings.FilterCardSettings);
        }

        private void OnSetFilter(DropdownItemData itemData)
        {
            AudioController.PlaySound("UI-StartingHand");
            _currentSettings.FilterCardSettings.cardSet = GameUtilities.ParseEnum(itemData.value, CardSet.All);

            Debug.Log($"Card set? {_currentSettings.FilterCardSettings.cardSet}");

            _currentSettings.FilterCardSettings.TransferSettings(savedCardFilterSettings);

            Debug.Log($"Card set? {_currentSettings.FilterCardSettings.cardSet} {savedCardFilterSettings.cardSet}");
            _transferSettings = false;
            FilterCards(_currentSettings.FilterCardSettings);
        }

        public void OnSearchChanged(string search)
        {
            _currentSettings.FilterCardSettings.searchFilter = search;
            _currentSettings.FilterCardSettings.TransferSettings(savedCardFilterSettings);
            _transferSettings = false;
            FilterCards(_currentSettings.FilterCardSettings);
        }

        private void OnRarityToggle(bool isOn, string rarity)
        {
            AudioController.PlaySound("UI-StartingHand");

            if (isOn)
            {
                _currentSettings.FilterCardSettings.rarity = GameUtilities.ParseEnum(rarity, Rarity.All);
            }
            else
            {
                _currentSettings.FilterCardSettings.rarity = Rarity.All;
            }

            _currentSettings.FilterCardSettings.TransferSettings(savedCardFilterSettings);
            _transferSettings = false;
            FilterCards(_currentSettings.FilterCardSettings);

        }

        private void OnTypeFilter(DropdownItemData itemData)
        {
            AudioController.PlaySound("UI-StartingHand");

            _currentSettings.FilterCardSettings.typeFilter = GameUtilities.ParseEnum(itemData.value, TypeFilter.All);
            _currentSettings.FilterCardSettings.TransferSettings(savedCardFilterSettings);
            _transferSettings = false;
            FilterCards(_currentSettings.FilterCardSettings);
        }

        private void OnKeywordFilter(DropdownItemData itemData)
        {
            AudioController.PlaySound("UI-StartingHand");

            _currentSettings.FilterCardSettings.keywordFilter = GameUtilities.ParseEnum(itemData.value, KeywordFilter.All);
            _currentSettings.FilterCardSettings.TransferSettings(savedCardFilterSettings);
            _transferSettings = false;
            FilterCards(_currentSettings.FilterCardSettings);
        }

        private void OnRelicFilter(DropdownItemData itemData)
        {
            AudioController.PlaySound("UI-StartingHand");

            _currentSettings.FilterCardSettings.relicFilter = GameUtilities.ParseEnum(itemData.value, RelicFilter.All);
            _currentSettings.FilterCardSettings.TransferSettings(savedRelicFilterSettings);
            _transferSettings = false;
            FilterCards(_currentSettings.FilterCardSettings);
        }

        private void OnCardBackFilter(DropdownItemData itemData)
        {
            AudioController.PlaySound("UI-StartingHand");

            _currentSettings.FilterCardSettings.cardBackFilter = GameUtilities.ParseEnum(itemData.value, CardBackFilter.All);
            _currentSettings.FilterCardSettings.TransferSettings(savedCardBackFilterSettings);
            _transferSettings = false;
            cardBackMenu.transferSettings = false;
            cardBackMenu.OnRefreshMenu();
        }

        private void FilterCards(FilterCardSettings filter)
        {
            var cards = new List<CardData> ( Db.CardDatabase.Cards );

            switch (_currentSettings.MenuMode)
            {
                case CollectionMenuMode.ManaMeld:
                    if (FableController.PlayerIsInAFable())
                    {
                        //var fable = FableController.GetCurrentChapterId();
                        var book = App.FableData.GetCurrentBook();

                        cards.RemoveAll(card => !book.Recipes.Contains(card.id));

                    }
                    else
                    {
                        cards.RemoveAll(card => card.set == CardSet.Token || card.set == CardSet.Deprecated || !Crafting.IsCraftable(card) || card.type == CardType.Relic || card.type == CardType.Elder_Relic);
                    }
                    break;
                case CollectionMenuMode.Relics:

                    if (_transferSettings)
                    {
                        filter.SetAsDefault();
                        savedRelicFilterSettings.TransferSettings(filter);
                    }

                    cards.RemoveAll(card => card.set == CardSet.Token || card.set == CardSet.Deprecated || card.type != CardType.Relic && card.type != CardType.Elder_Relic);
                    if (filter.relicFilter == RelicFilter.ElderRelics) cards.RemoveAll(card => card.type == CardType.Relic);
                    if (filter.relicFilter == RelicFilter.Relics) cards.RemoveAll(card => card.type == CardType.Elder_Relic);

                    break;
                default:

                    if (_transferSettings)
                    {
                        filter.SetAsDefault();
                        savedCardFilterSettings.TransferSettings(filter);
                    }
                    cards.RemoveAll(card => card.set == CardSet.Token || card.set == CardSet.Deprecated || card.type == CardType.Relic || card.type == CardType.Elder_Relic);

                    break;
            }

            if (_currentSettings.MenuMode == CollectionMenuMode.ManaMeld)
            {
                if (filter.isCraftable)
                {
                    cards.RemoveAll(card => !Crafting.CanCraftCard(card, out _));
                }

                if (filter.onlyNotOwned)
                {
                    cards.RemoveAll(card => App.Inventory.OwnsCard(card.id));
                }
            }
            else if (filter.ownedCardsOnly)
            {
                cards.RemoveAll(card => !App.Inventory.OwnsCard(card.id));
            }

            if (_currentSettings.OpenMode == DeckOpenMode.DeckEditor)
            {
                _factionsAvailable = new List<Faction>(filter.faction == Faction.All ?
                    new List<Faction>{_currentSettings.DeckSettings.deckData.faction, Faction.Neutral} : new List<Faction>{filter.faction});
            }
            else if (_currentSettings.OpenMode == DeckOpenMode.Collection)
            {
                if (filter.faction == Faction.All && filter.forcedFaction == Faction.All)
                {
                    _factionsAvailable = new List<Faction>(FactionsByOrder);
                }
                else if (filter.faction == Faction.All && filter.forcedFaction != Faction.All)
                {
                    _factionsAvailable = new List<Faction> {filter.forcedFaction, Faction.Neutral};
                }
                else
                {
                    _factionsAvailable = new List<Faction> {filter.faction};
                }
            }

            _cardsByFaction = new Dictionary<Faction, List<CardData>>();

            if (_currentSettings.MenuMode == CollectionMenuMode.Relics)
            {
                for (var i = 0; i < _factionsAvailable.Count; i++)
                {
                    _cardsByFaction.Add(_factionsAvailable[i], cards.FindAll(card => card.faction == _factionsAvailable[i] || card.faction2 == _factionsAvailable[i])
                        .OrderBy(card => card.type == CardType.Relic)
                        .ThenBy(card => card.type == CardType.Elder_Relic)
                        .ToList());
                }
            }
            else if (_factionsAvailable.Count > 0)
            {
                if (_currentSettings.OpenMode == DeckOpenMode.DeckEditor || FableController.PlayerIsInAFable() ||
                    filter.faction != Faction.All || filter.forcedFaction != Faction.All)
                {
                    _cardsByFaction.Add(_factionsAvailable[0], cards
                        .FindAll(card =>
                            _factionsAvailable.Contains(card.faction) || _factionsAvailable.Contains(card.faction2))
                        .OrderBy(card => card.type != CardType.Power)
                        .ThenBy(card => card.cost)
                        .ThenBy(card => FactionsByOrder.IndexOf(card.faction))
                        .ToList());
                }
                else
                {
                    for (var i = 0; i < _factionsAvailable.Count; i++)
                    {
                        _cardsByFaction.Add(_factionsAvailable[i], cards
                            .FindAll(card =>
                                card.faction == _factionsAvailable[i] || card.faction2 == _factionsAvailable[i])
                            .OrderBy(card => card.type != CardType.Power)
                            .ThenBy(card => card.cost)
                            .ThenBy(card => FactionsByOrder.IndexOf(card.faction))
                            .ToList());
                    }
                }
            }

            if (_cardsByFaction.Count == 0)
            {
                Debug.LogError("No Cards found.");
                return;
            }

            // for (var i = 0; i < _factionsAvailable.Count; i++)
            // {
            //     if (_currentSettings.MenuMode == CollectionMenuMode.Relics)
            //     {
            //         _cardsByFaction.Add(_factionsAvailable[i], cards.FindAll(card => card.faction == _factionsAvailable[i] || card.faction2 == _factionsAvailable[i])
            //             .OrderBy(card => card.type != CardType.Power)
            //             .ThenBy(card => card.type == CardType.Relic)
            //             .ThenBy(card => card.type == CardType.Elder_Relic)
            //             .ThenBy(card => card.cost)
            //             .ThenBy(card => card.name)
            //             .ToList());
            //     }
            //     else if (!FableController.PlayerIsInAFable())
            //     {
            //         _cardsByFaction.Add(_factionsAvailable[i], cards.FindAll(card => card.faction == _factionsAvailable[i] || card.faction2 == _factionsAvailable[i])
            //             .OrderBy(card => card.type != CardType.Power)
            //             .ThenBy(card => card.cost)
            //             .ThenBy(card => card.name)
            //             .ToList());
            //     }
            // }

            // if (/*FableController.PlayerIsInAFable() &&*/ _currentSettings.MenuMode != CollectionMenuMode.Relics)
            // {
            //     _cardsByFaction.Add(_factionsAvailable[0], cards
            //         .FindAll(card => _factionsAvailable.Contains(card.faction) || _factionsAvailable.Contains(card.faction2))
            //         .OrderBy(card => card.type != CardType.Power)
            //         //.ThenBy(card => FactionsByOrder.IndexOf(card.faction))
            //         .ThenBy(card => card.cost)
            //         .ThenBy(card => card.name)
            //         .ToList());
            // }

            _factionIndex = filter.faction == Faction.All ? _factionIndex : 0;

            if (filter.cardSet != CardSet.All && filter.cardSet != CardSet.Deprecated)
            {
                foreach (var cL in _cardsByFaction)
                {
                    cL.Value.RemoveAll(card => card.set != filter.cardSet);
                }
            }

            if (filter.@class != Class.All)
            {
                foreach (var cL in _cardsByFaction)
                {
                    cL.Value.RemoveAll(card => card.@class != filter.@class);
                }
            }

            if (filter.rarity != Rarity.All)
            {
                foreach (var cL in _cardsByFaction)
                {
                    cL.Value.RemoveAll(card => card.rarity != filter.rarity);
                }
            }

            if (filter.typeFilter != TypeFilter.All)
            {
                foreach (var cL in _cardsByFaction)
                {
                    cL.Value.RemoveAll(card =>
                    {
                        if (filter.typeFilter == TypeFilter.Minion)
                            return card.type != CardType.Minion;
                        if (filter.typeFilter == TypeFilter.Melee)
                            return card.type != CardType.Minion || card.attackType != AttackType.Melee;
                        if (filter.typeFilter == TypeFilter.Ranged)
                            return card.type != CardType.Minion || card.attackType != AttackType.Ranged;
                        if (filter.typeFilter == TypeFilter.Arcane)
                            return card.type != CardType.Minion || card.attackType != AttackType.Arcane;
                        if (filter.typeFilter == TypeFilter.Spell)
                            return card.type != CardType.Spell;
                        if (filter.typeFilter == TypeFilter.Trick)
                            return card.type != CardType.Trick;
                        if (filter.typeFilter == TypeFilter.Power)
                            return card.type != CardType.Power;

                        return false;
                    });
                }
            }

            if (filter.keywordFilter != KeywordFilter.All)
            {
                foreach (var cL in _cardsByFaction)
                {
                    cL.Value.RemoveAll(card =>
                    {
                        var abilities = card.GetAbilities();

                        switch (filter.keywordFilter)
                        {
                            case KeywordFilter.All: break;
                            case KeywordFilter.None:
                                return abilities == null || abilities.Count == 0;
                            // case KeywordFilter.Ally: break;
                            // case KeywordFilter.Enemy: break;
                            case KeywordFilter.Battlecry:
                                return abilities == null || abilities.Count <= 0 || abilities.Find(a => a.triggerType == TriggerType.BATTLECRY) == null;
                            case KeywordFilter.Damaged:
                                return abilities == null || abilities.Count <= 0 || abilities.Find(a => a.triggerType == TriggerType.DAMAGED) == null;
                            case KeywordFilter.Deathrattle:
                                return abilities == null || abilities.Count <= 0 || abilities.Find(a => a.triggerType == TriggerType.DEATHRATTLE) == null;
                            case KeywordFilter.Turn_End:
                                return abilities == null || abilities.Count <= 0 || abilities.Find(a => a.triggerType == TriggerType.TURN_END) == null;
                            case KeywordFilter.Turn_Start:
                                return abilities == null || abilities.Count <= 0 || abilities.Find(a => a.triggerType == TriggerType.TURN_START) == null;
                            case KeywordFilter.Barrier:
                                return abilities == null || abilities.Count <= 0 || abilities.Find(a => a.keyword == AbilityKeyword.BARRIER) == null;
                            case KeywordFilter.Dual_Strike:
                                return abilities == null || abilities.Count <= 0 || abilities.Find(a => a.keyword == AbilityKeyword.DUAL_STRIKE) == null;
                            case KeywordFilter.Pierce:
                                return abilities == null || abilities.Count <= 0 || abilities.Find(a => a.keyword == AbilityKeyword.PIERCE) == null;
                            case KeywordFilter.Silence:
                                return abilities == null || abilities.Count <= 0 || abilities.Find(a => a.keyword == AbilityKeyword.SILENCE) == null;
                            case KeywordFilter.Tough:
                                return abilities == null || abilities.Count <= 0 || abilities.Find(a => a.keyword == AbilityKeyword.TOUGH) == null;
                            case KeywordFilter.Flying:
                                return abilities == null || abilities.Count <= 0 || abilities.Find(a => a.keyword == AbilityKeyword.FLYING) == null;
                            case KeywordFilter.Redeem:
                                return abilities == null || abilities.Count <= 0 || abilities.Find(a => a.keyword == AbilityKeyword.REDEEM) == null;
                            case KeywordFilter.Lifesteal:
                                return abilities == null || abilities.Count <= 0 || abilities.Find(a => a.keyword == AbilityKeyword.LIFESTEAL) == null;
                            case KeywordFilter.Freeze:
                                return abilities == null || abilities.Count <= 0 || abilities.Find(a => a.keyword == AbilityKeyword.FREEZE) == null;
                            case KeywordFilter.Thorns:
                                return abilities == null || abilities.Count <= 0 || abilities.Find(a => a.keyword == AbilityKeyword.THORNS) == null;
                            case KeywordFilter.Plunder:
                                return abilities == null || abilities.Count <= 0 || abilities.Find(a => a.triggerType == TriggerType.PLUNDER) == null;
                            case KeywordFilter.Overgrow:
                                return abilities == null || abilities.Count <= 0 || abilities.Find(a => a.keyword == AbilityKeyword.OVERGROW) == null;
                            case KeywordFilter.Focus:
                                return abilities == null || abilities.Count <= 0 || abilities.Find(a => a.keyword == AbilityKeyword.FOCUS) == null;
                            case KeywordFilter.Flux:
                                return abilities == null || abilities.Count <= 0 || abilities.Find(a => a.keyword == AbilityKeyword.FLUX) == null;
                            case KeywordFilter.Rush:
                                return abilities == null || abilities.Count <= 0 || abilities.Find(a => a.keyword == AbilityKeyword.RUSH) == null;
                            default:
                                return false;
                        }

                        return false;
                    });
                }
            }

            if (!string.IsNullOrEmpty(filter.searchFilter))
            {
                var powerAmount = -1;
                var healthAmount = -1;
                var costAmount = -1;

                if (filter.searchFilter.Contains("power:"))
                {
                    var numText = Regex.Replace(filter.searchFilter, "[^0-9]", "");
                    if (int.TryParse(numText, out var amount))
                    {
                        powerAmount = amount;
                        Debug.Log($"power amount ={powerAmount}");
                    }
                }

                if (filter.searchFilter.Contains("health:"))
                {
                    var numText = Regex.Replace(filter.searchFilter, "[^0-9]", "");
                    if (int.TryParse(numText, out var amount))
                    {
                        healthAmount = amount;
                    }
                }

                if (filter.searchFilter.Contains("cost:"))
                {
                    var numText = Regex.Replace(filter.searchFilter, "[^0-9]", "");
                    if (int.TryParse(numText, out var amount))
                    {
                        costAmount = amount;
                    }
                }

                var searchFilter = new string(filter.searchFilter.Where(Char.IsLetter).ToArray());

                foreach (var cL in _cardsByFaction)
                {
                    cL.Value.RemoveAll(card =>
                    {
                        if (powerAmount >= 0)
                        {
                            return card.power != powerAmount;
                        }

                        if (healthAmount >= 0)
                        {
                            return card.health != healthAmount;
                        }

                        if (costAmount >= 0)
                        {
                            return card.cost != costAmount;
                        }

                        var cardName = card.name.ToLower();
                        var description = card.description.ToLower();
                        var @class = card.@class.ToString().ToLower();
                        var set = card.set.ToString().ToLower();
                        var rarity = card.rarity.ToString().ToLower();
                        var type = card.type.ToString().ToLower();

                        //Debug.Log($"{@class} == {searchFilter} contains? {@class.Contains(searchFilter)}");

                        return !cardName.Contains(searchFilter) && !description.Contains(searchFilter) &&
                               !@class.Contains(searchFilter) && !set.Contains(searchFilter) &&
                               !rarity.Contains(searchFilter) && !type.Contains(searchFilter);
                    });
                }
            }

            noCardsFoundLabel.SetActive(false);
            // we need to make sure there's cards in the faction we're in or we need to move to another faction.
            _cards = _cardsByFaction[CurrentFaction];
            // there's no cards in this faction for the parameters indicated
            var tempFaction = 0;

            while (_cards.Count <= 0)
            {
                if (tempFaction >= _factionsAvailable.Count)
                {
                    // we couldn't find any cards so leave the list
                    noCardsFoundLabel.SetActive(true);
                    noCardsFoundAnimation.Play("cards");
                    if(_currentSettings.MenuMode == CollectionMenuMode.Collection || _currentSettings.MenuMode == CollectionMenuMode.ManaMeld) noCardsFoundAnimation.Play("cards");
                    else noCardsFoundAnimation.Play("relics");

                    _cardViews.ForEach(card => card.SetActive(false));
                    nextButtonCardCollection.SetActive(false);
                    previousButtonCardCollection.SetActive(false);
                    pageCount.text = $"1/1";
                    return;
                }

                if (_cardsByFaction.ContainsKey(_factionsAvailable[tempFaction]))
                {
                    _cards = _cardsByFaction[_factionsAvailable[tempFaction]];
                }

                if (_cards.Count > 0)
                {
                    _factionIndex = tempFaction;
                }

                tempFaction++;
            }

            _pageIndex = _pageIndex >= GetPageCount() ? GetPageCount() - 1 : _pageIndex;

            RefreshArrowButtonsCardCollection();

            _needsRefreshing = true;
        }

        private bool _needsRefreshing;
        private CoroutineHandle _refreshCoroutine;

        private void Update()
        {
            if (_currentSettings.MenuMode == CollectionMenuMode.CardBacks) return;

            if (_needsRefreshing && !Timing.IsRunning(_refreshCoroutine))
            {
                _needsRefreshing = false;
                _refreshCoroutine=Timing.RunCoroutine(RefreshPage());
            }
        }

        private IEnumerator<float> RefreshPage()
        {

            nextButtonCardCollection.interactable = _currentSettings.MenuMode != CollectionMenuMode.ManaMeld;
            previousButtonCardCollection.interactable = _currentSettings.MenuMode != CollectionMenuMode.ManaMeld;

            while (Db.IsBusy)
            {
                yield return Timing.WaitForOneFrame;
            }

            while (_cardPrefab == null)
            {
                yield return Timing.WaitForOneFrame;
            }

            var startingIndex = _pageIndex * CardsPerPage;
            var index = 0;
            var spawnedCards = new List<CardView>();

            for (var i = startingIndex; i < startingIndex + CardsPerPage; i++)
            {
                if (i >= _cards.Count)
                {
                    if (index < _cardViews.Count)
                    {
                        _cardViews[index].SetActive(false);
                    }
                    index++;
                    continue;
                }

                if (index >= _cardViews.Count)
                {

                    if (_currentSettings.MenuMode == CollectionMenuMode.ManaMeld)
                    {
                        _additionalComponents = new List<Type> { typeof(ClickableCard), typeof(DragCard), typeof(HoverCard), typeof(ManaMeldRecipeComponent)};
                    }
                    else
                    {
                        _additionalComponents = new List<Type> { typeof(ClickableCard), typeof(DragCard), typeof(HoverCard), typeof(CardCollectionComponentsLoader), typeof(CardObtainedEffectComponent)};
                    }


                    var settings = new CreateCardFactorySettings
                    {
                        CardPrefab = _cardPrefab,
                        CardData = _cards[i],
                        Layout = cardCollectionLayout,
                        SortingOrder = 400,
                        SortingLayer = "Collection",
                        CardObjectOffsetPosition = Vector2.zero,
                        StartsDisabled = true,
                        AdditionalComponents = _additionalComponents
                    };

                    var card = CardFactory.Create(settings, _cardViews);
                    card.GetCardComponent<DragCard>().SetDragCardDeckEditor(_draggableCard);
                    card.GetCardComponent<DragCard>().SetCardData(_cards[i]);

                    if (_currentSettings.MenuMode != CollectionMenuMode.ManaMeld)
                    {
                        var collectionComponentLoader = card.GetCardComponent<CardCollectionComponentsLoader>();

                        while (collectionComponentLoader.cardCollectionComponent == null)
                        {
                            yield return Timing.WaitForOneFrame;
                        }

                        collectionComponentLoader.cardCollectionComponent.Refresh(_currentSettings, _cards[i]);

                        var cardObtainedEffectComponent = card.GetCardComponent<CardObtainedEffectComponent>();

                        while (cardObtainedEffectComponent.Effect == null)
                        {
                            yield return Timing.WaitForOneFrame;
                        }

                        // var cardObtainedEffect = card.GetVfx<CardObtainedEffectComponent, CardObtainedEffect>();
                        // cardObtainedEffect.SetNewlyObtained(false);
                    }
                    else
                    {

                        var manaMeldRecipe = card.GetCardComponent<ManaMeldRecipeComponent>();
                        while (manaMeldRecipe.manaMeldCollectionRecipe == null)
                        {
                            yield return Timing.WaitForOneFrame;
                        }
                        manaMeldRecipe.manaMeldCollectionRecipe.Refresh(_currentSettings, _cards[i]);

                    }

                    spawnedCards.Add(card);
                }
                else
                {


                    _cardViews[index].SetActive(false);
                    _cardViews[index].GetCardComponent<DragCard>().SetCardData(_cards[i]);

                    if (_currentSettings.MenuMode != CollectionMenuMode.ManaMeld)
                    {
                        var collectionComponentLoader = _cardViews[index].GetCardComponent<CardCollectionComponentsLoader>();
                        var cardObtainedEffect = _cardViews[index].GetVfx<CardObtainedEffectComponent, CardObtainedEffect>();
                        cardObtainedEffect.SetNewlyObtained(false);

                        while (collectionComponentLoader.cardCollectionComponent == null)
                        {
                            yield return Timing.WaitForOneFrame;
                        }

                        collectionComponentLoader.cardCollectionComponent.Refresh(_currentSettings, _cards[i]);
                    }
                    else
                    {

                        var manaMeldRecipe = _cardViews[index].GetCardComponent<ManaMeldRecipeComponent>();
                        while (manaMeldRecipe.manaMeldCollectionRecipe == null)
                        {
                            yield return Timing.WaitForOneFrame;
                        }
                        manaMeldRecipe.manaMeldCollectionRecipe.Refresh(_currentSettings, _cards[i]);
                    }

                    spawnedCards.Add(_cardViews[index]);
                }

                index++;
            }

            if (App.Settings.GetDeckEditorCardAnimationsEnabled())
            {
                for (var i = 0; i < spawnedCards.Count; i++)
                {
                    spawnedCards[i].CanvasGroup.alpha = 0;
                    spawnedCards[i].SetActive(true);
                    spawnedCards[i].GetCardComponent<HoverCard>().IsHoverable = false;
                }
            }

            for (var i = 0; i < spawnedCards.Count; i++)
            {

                if (App.Settings.GetDeckEditorCardAnimationsEnabled())
                {
                    Timing.RunCoroutine(SpawnCardIn(spawnedCards[i]));
                    if(_currentSettings.MenuMode != CollectionMenuMode.ManaMeld) yield return Timing.WaitForSeconds(0.02f);
                    //else yield return Timing.WaitForSeconds(0.05f);
                }

                spawnedCards[i].SetActive(true);

                if (_currentSettings.MenuMode != CollectionMenuMode.ManaMeld)
                {
                    spawnedCards[i].CanvasGroup.alpha = 1;
                    spawnedCards[i].GetCardComponent<HoverCard>().IsHoverable = true;

                    var itemInstance = App.Inventory.GetItem(spawnedCards[i].data.id);
                    var cardObtainedEffect = spawnedCards[i].GetVfx<CardObtainedEffectComponent, CardObtainedEffect>();
                    cardObtainedEffect.SetNewlyObtained(itemInstance?.IsNew ?? false);
                    cardObtainedEffect.SetParticleSize(1);
                }
                else
                {
                    var previewCard = ManaMeldEditMenu.Instance.massScrapSidePanel.previewCard;
                    if (previewCard != null && previewCard.data.name == spawnedCards[i].data.name)
                    {
                        ManaMeldEditMenu.Instance.UpdatePreview(spawnedCards[i]);
                    }
                }
            }

            onMeldCard = false;
            nextButtonCardCollection.interactable = true;
            previousButtonCardCollection.interactable = true;
        }

        private IEnumerator<float> SpawnCardIn(CardView cardView)
        {
            if (_currentSettings.MenuMode != CollectionMenuMode.ManaMeld)
            {
                var cardRect = cardView.cardObject;
                cardRect.DOKill();

                cardRect.localEulerAngles = Vector3.zero;
                cardRect.localScale = new Vector3(1.2f, .8f, 1f);
                cardRect.anchoredPosition = new Vector2(0, -24f);
                cardView.RemoveTint();

                const float timing = 0.24f;

                cardView.SetSortOrder(500);
                cardView.CanvasGroup.DOKill(true);
                cardView.CanvasGroup.alpha = 0f;
                cardView.CanvasGroup.DOFade(1, timing);
                cardRect.DOScale(1, timing).SetEase(Ease.OutBack);
                cardRect.DOAnchorPosY(0, timing).SetEase(Ease.OutBack).SetDelay(0.06f);

                AudioController.PlaySound("UI-Text_Next");

                yield return Timing.WaitForSeconds(timing);

                cardView.SetSortOrder(400);
                //cardView.TintThenUntintOverTime(Color.white, BlendMode.Overlay, 0.06f, 0.06f, 0.06f);
                // cardRect.DOPunchRotation(Vector3.up * ((UnityEngine.Random.value > 0.5f ? -1 : 1) * 60f), 0.4f)
                //     .SetEase(Ease.Linear);
                //cardRect.DOPunchPosition(Vector2.up * 4f, 0.3f).SetEase(Ease.OutBack);
            }
            else
            {
                //if (!onMeldCard)
               // {
                    var spawnDir = 35f;
                    if(_spawnRightDir) spawnDir = 35f;
                    else spawnDir = -35f;

                    var cardRect = cardView.cardObject;
                    cardRect.DOKill();
                    cardRect.anchoredPosition = new Vector2(spawnDir, 0);
                    cardView.RemoveTint();

                    const float timing = 0.25f;

                    cardView.SetSortOrder(500);
                    cardView.CanvasGroup.DOKill(true);
                    cardView.CanvasGroup.alpha = 0f;
                    //cardView.CanvasGroup.alpha = 1f;
                    cardView.CanvasGroup.DOFade(1, timing/2f);

                    cardRect.DOAnchorPosX(0, timing).SetEase(Ease.OutBack);

                    AudioController.PlaySound("UI-Text_Next");

                    yield return Timing.WaitForSeconds(timing);

                    cardView.SetSortOrder(400);
                    //cardView.TintThenUntintOverTime(Color.white, BlendMode.Overlay, 0.06f, 0.06f, 0.06f);
                    // cardRect.DOPunchRotation(Vector3.up * ((UnityEngine.Random.value > 0.5f ? -1 : 1) * 60f), 0.4f)
                    //     .SetEase(Ease.Linear);
                    //cardRect.DOPunchPosition(Vector2.up * 4f, 0.3f).SetEase(Ease.OutBack);
              //  }
            }
        }

        public IEnumerator<float> SpawnCardsOut()
        {

            if (_currentSettings.MenuMode == CollectionMenuMode.ManaMeld)
            {
                const float timing = 0.25f;

                for (var i = 0; i < _cardViews.Count; i++)
                {
                    var spawnDir = 35f;
                    if (_spawnRightDir) spawnDir = -35f;
                    else spawnDir = 35f;

                    var cardRect = _cardViews[i].cardObject;
                    var cardView = _cardViews[i];

                    cardRect.DOKill();
                    //cardRect.anchoredPosition = new Vector2(spawnDir, 0);
                    cardView.RemoveTint();

                    cardView.SetSortOrder(500);
                    cardView.CanvasGroup.DOKill(true);
                    cardView.CanvasGroup.DOFade(0, timing/2f).SetDelay(timing/2f);

                    cardRect.DOAnchorPosX(spawnDir, timing).SetEase(Ease.InBack);

                    AudioController.PlaySound("UI-Text_Next");

                    cardView.SetSortOrder(400);
                }
                yield return Timing.WaitForSeconds(timing);
            }

            if (!onMeldCard)
            {
                if (_spawnRightDir) _pageIndex++;
                else _pageIndex--;

                RefreshCardsList();
                RefreshArrowButtonsCardCollection();
                _needsRefreshing = true;
            }
        }

        private void OnNextButton()
        {
            _spawnRightDir = true;
            nextButtonCardCollection.interactable = _currentSettings.MenuMode != CollectionMenuMode.ManaMeld;
            previousButtonCardCollection.interactable = _currentSettings.MenuMode != CollectionMenuMode.ManaMeld;
            Timing.RunCoroutine(SpawnCardsOut());

            //_pageIndex++;
            //RefreshCardsList();
            //RefreshArrowButtonsCardCollection();
            //_needsRefreshing = true;
        }

        private void OnPreviousButton()
        {
            _spawnRightDir = false;
            nextButtonCardCollection.interactable = _currentSettings.MenuMode != CollectionMenuMode.ManaMeld;
            previousButtonCardCollection.interactable = _currentSettings.MenuMode != CollectionMenuMode.ManaMeld;
            Timing.RunCoroutine(SpawnCardsOut());

            //_pageIndex--;
            //RefreshCardsList();
            //RefreshArrowButtonsCardCollection();
            //_needsRefreshing = true;
        }

        private void RefreshCardsList()
        {
            // means we are out of pages in this faction and we need to go up 1 faction
            // the second check should always be true otherwise we are on an invalid page
            if (_pageIndex > (GetPageCount()-1) && _factionIndex < _factionsAvailable.Count-1)
            {
                var tempFaction = _factionIndex + 1;

                while (_cardsByFaction[_factionsAvailable[tempFaction]].Count <= 0)
                {
                    tempFaction++;
                }

                _factionIndex = tempFaction;
                _pageIndex = 0;
                _cards = _cardsByFaction[CurrentFaction];
            }

            if (_pageIndex < 0 && _factionIndex > 0)
            {
                var tempFaction = _factionIndex - 1;

                while (_cardsByFaction[_factionsAvailable[tempFaction]].Count <= 0)
                {
                    tempFaction--;
                }

                _factionIndex = tempFaction;
                _cards = _cardsByFaction[CurrentFaction];
                _pageIndex = GetPageCount()-1;
            }
        }

        public void OnMeldCard()
        {
            onMeldCard = true;
            Timing.RunCoroutine(SpawnCardsOut());
        }

        private void RefreshArrowButtonsCardCollection()
        {
            if (_currentSettings.MenuMode == CollectionMenuMode.CardBacks)
            {
                pageCount.SetActive(false);
                nextButtonCardCollection.SetActive(false);
                previousButtonCardCollection.SetActive(false);
                return;
            }

            nextButtonCardCollection.SetActive((_pageIndex+1) * CardsPerPage < _cards.Count || (_factionIndex < _factionsAvailable.Count-1 && NextPageHasCards()));

            // 1. Page Index is greater than zero; meaning we can move pages within our current faction
            // 2. Faction index is greater than zero; so even if we're on the last page for the current faction we can still move to the previous faction

            previousButtonCardCollection.SetActive(_pageIndex > 0 || (_factionIndex > 0 && PreviousPageHasCards()));

            RefreshPageNumberUI();
        }

        private void RefreshPageNumberUI()
        {
            var pages= 0;
            var currentPage = 0;
            var i = 0;

            foreach (var cardsList in _cardsByFaction)
            {
                var count = GetPageCount(cardsList.Value.Count);

                if (_factionIndex > i)
                {
                    currentPage += count;
                }
                else if (_factionIndex == i)
                {
                    currentPage += _pageIndex;
                }

                pages += count;
                i++;
            }

            pageCount.SetActive(true);
            pageCount.text = $"{currentPage+1}/{pages}";
        }

        private int GetPageCount()
        {
            return _cards.Count / CardsPerPage + (_cards.Count % CardsPerPage > 0 ? 1 : 0);
        }

        private int GetPageCount(int count)
        {
            return count / CardsPerPage + (count % CardsPerPage > 0 ? 1 : 0);
        }

        private bool NextPageHasCards()
        {
            for (var i = _factionIndex+1; i < _cardsByFaction.Count; i++)
            {
                var hasCards = _cardsByFaction[_factionsAvailable[i]].Count > 0;
                if (hasCards) return true;
            }

            return false;
        }
        private bool PreviousPageHasCards()
        {
            for (var i = _factionIndex-1; i >-1; i--)
            {
                var hasCards = _cardsByFaction[_factionsAvailable[i]].Count > 0;
                if (hasCards) return true;
            }

            return false;
        }

        public void Close(DeckEditSettings settings)
        {
            if (settings.MenuMode != CollectionMenuMode.CardBacks)
            {
                deckList.SetActive(false);
                cardList.SetActive(false);
            }

            this.SetActive(false);
        }

        public void OnBackButton()
        {
            switch (_currentSettings.OpenMode)
            {
                case DeckOpenMode.Collection: // do nothing for now.
                    break;
                case DeckOpenMode.DeckEditor:
                    cardList.OnFinishButton();
                    break;
            }
        }
    }
}