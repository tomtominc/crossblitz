using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.Items.Data;
using CrossBlitz.PlayFab.Authentication;
using BlendModes;
using CrossBlitz.UI.Widgets;
using CrossBlitz.Universal;
using CrossBlitz.Utils;
using DG.Tweening;
using GameDataEditor;
using MEC;
using Sirenix.OdinInspector;
using TakoBoyStudios.Events;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;
using Random = System.Random;
using CrossBlitz.Hero;
using CrossBlitz.Fables;

namespace CrossBlitz.CardCollection
{
    public class MassScrapSidePanel : MonoBehaviour
    {
        public CanvasGroup inventoryMenu;

        [BoxGroup("Filters")] public ToggleButton meldableToggle;
        [BoxGroup("Filters")] public ToggleButton notOwnedToggle;

        [BoxGroup("Inventory")] public GameObject inventoryObject;
        [BoxGroup("Inventory")] public RectTransform ingredientsHeader;
        [BoxGroup("Inventory")] public IngredientDisplayPageItem ingredientPageItemPrefab;
        [BoxGroup("Inventory")] public PageBehaviour ingredientsPages;

        [BoxGroup("Preview")] public GameObject previewObject;
        [BoxGroup("Preview")] public RectTransform previewHeader;
        [BoxGroup("Preview")] public CanvasGroup previewCanvas;
        [BoxGroup("Preview")] public RectTransform cardLayout;
        [BoxGroup("Preview")] public RectTransform closePreviewButton;

        [BoxGroup("Mana Meld")] public Button manaMeldButton;
        [BoxGroup("Mana Meld")] public MeldCinematic cinematic;

        private const int CardSortingOrder = 8000;
        public CardView previewCard;
        private CardView _previousCard;
        private bool _previewHeaderInit = false;
        private float m_totalMassScrapTime;
        private Color _tintColor;
        private BlendMode _blendMode;
        private Vector3 _origPosPreview;
        

        private void Start()
        {
            Events.Subscribe(EventType.OnModifiedCurrency, OnModifyCurrency);
            Events.Subscribe(EventType.OnModifiedItemUses, OnModifyItemUses);

            meldableToggle.OnValueChanged += OnMeldableToggled;
            notOwnedToggle.OnValueChanged += OnNotOwnedToggled;

            manaMeldButton.onClick.AddListener(OnManaMeldButton);

            //_tintColor = new Color(0.5019608f, 0.254902f, 0.5843138f,.80f);
            _tintColor = new Color(1f, 1f, 1f, .80f);
            //ColorUtility.TryParseHtmlString("#FFB2E4", out _tintColor);
            _blendMode = new BlendMode();
            _blendMode = BlendMode.Overlay;
            _previousCard = null;
            previewCanvas.alpha = 0;
            cardLayout.anchoredPosition = new Vector2(cardLayout.sizeDelta.x, cardLayout.anchoredPosition.y);
            closePreviewButton.anchoredPosition = new Vector2(closePreviewButton.sizeDelta.x, closePreviewButton.anchoredPosition.y);
            //cardLayout.DOLocalMoveX(_origPosPreview.x + 50f, 0.05f);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnModifiedCurrency, OnModifyCurrency);
            Events.Unsubscribe(EventType.OnModifiedItemUses, OnModifyItemUses);
        }

        private void OnMeldableToggled(bool isOn, string value)
        {
            var manaMeldEditMenu = ManaMeldEditMenu.Instance;

            if (manaMeldEditMenu == null)
            {
                DeckEditMenu.Instance.Settings.FilterCardSettings.isCraftable = isOn;
                DeckEditMenu.Instance.ChangeOpenSettings(DeckEditMenu.Instance.Settings);
            }
            else
            {
                ManaMeldEditMenu.Instance.Settings.FilterCardSettings.isCraftable = isOn;
                ManaMeldEditMenu.Instance.ChangeOpenSettings(ManaMeldEditMenu.Instance.Settings);
            }

        }

        private void OnNotOwnedToggled(bool isOn, string value)
        {
            var manaMeldEditMenu = ManaMeldEditMenu.Instance;
            if (manaMeldEditMenu == null)
            {
                DeckEditMenu.Instance.Settings.FilterCardSettings.onlyNotOwned = isOn;
                DeckEditMenu.Instance.ChangeOpenSettings(DeckEditMenu.Instance.Settings);
            }
            else
            {
                ManaMeldEditMenu.Instance.Settings.FilterCardSettings.onlyNotOwned = isOn;
                ManaMeldEditMenu.Instance.ChangeOpenSettings(ManaMeldEditMenu.Instance.Settings);
            }
        }

        public void OpenIngredients()
        {
            gameObject.SetActive(true);

            var ingredients = new List<IngredientInventoryPageData>();

            //public enum Faction
            //{
            //    All,
            //    Neutral,
            //    War,
            //    Nature,
            //    Fortune,
            //    Balance,
            //    Chaos,
            //    None,
            //}

            var currentFaction = Faction.War;
            if (FableController.PlayerIsInAFable())
            {
                currentFaction = HeroData.GetFactionForHero(FableController.GetCurrentHeroName());
            }
            
            var validIngredients = Crafting.GetIngredientsByFaction(currentFaction);
            var neutralIng = Crafting.GetIngredientsByFaction(Faction.Neutral);
            validIngredients.AddRange(new List<GDEItemData> { neutralIng[0], neutralIng[3] });

            // Add mana Shard
            var manaShard = new IngredientInventoryPageData {isManaShard = true};
            ingredients.Add(manaShard);

            // Add rest of ingredients
            for (var i = 0; i < validIngredients.Count; i++)
            {
                var ingredientData = validIngredients[i];

                if (ingredientData.ItemType == ItemClassType.Ingredient)
                {
                    var ingredient = new IngredientInventoryPageData {ingredientId = ingredientData.Key};
                    ingredients.Add(ingredient);
                }
            }

            ingredientsPages.InitializePages(ingredientPageItemPrefab, 6, ingredients.ConvertAll( i => (IPageItemData)i));
        }

        private async Task<bool> CreateCardIfNull(CardData data)
        {
            if (previewCard == null)
            {
                var obj = await AddressableReferenceLoader.GetAsset("Card_Zoomed");

                var settings = new CreateCardFactorySettings
                {
                    CardPrefab = obj,
                    CardData = data,
                    Layout = cardLayout,
                    SortingOrder = CardSortingOrder,
                    SortingLayer = "Collection",
                    AdditionalComponents = new List<Type> { typeof(CardScrappedComponent), typeof(HoverCard) }
                };
                previewCard = CardFactory.Create(settings, null);
                previewCard.RectTransform().SetAsFirstSibling();
            }

            previewCard.SetCardData(data);

            return true;
        }

        public async void UpdatePreview(CardData cardData)
        {
            var createdCard = await CreateCardIfNull(cardData);
            if (createdCard) await UnityAsync.Await.NextUpdate();

            // Header Update Effects
            if (!_previewHeaderInit)
            {
                previewObject.SetActive(true);

                // Turn off ingredients
                for (var i = 0; i < ingredientsPages.pageLayout.childCount; i++)
                {
                    var ingredientDisplayPageItem = ingredientsPages.pageLayout.GetChild(i).GetComponent<IngredientDisplayPageItem>();
                    ingredientDisplayPageItem.HideData(i);
                }

                ingredientsHeader.DOScaleY(0.0f, 0.10f);
                previewHeader.localScale.Set(1f, 0f, 1f);
                //previewHeader.SetActive(true);
                previewHeader.DOScaleY(1.0f, 0.10f).SetDelay(0.10f);
                _previewHeaderInit = true;

                cardLayout.DOAnchorPosX(0, 0.5f).SetEase(Ease.InOutQuint);
                closePreviewButton.DOAnchorPosX(0, 0.5f).SetEase(Ease.InOutQuint);
                previewCanvas.DOFade(1, 0.25f).OnComplete(() =>
                {
                    inventoryObject.SetActive(false);
                });

                previewCard.GetComponent<CanvasGroup>().blocksRaycasts = false;

                ManaMeldEditMenu.Instance.closePreviewButton.interactable = true;

                
            }
            else
            {
                cardLayout.DOPunchPosition(Vector3.up * 5f, 0.25f).SetEase(Ease.OutBack);
                previewCard.TintThenUntintOverTime(_tintColor, _blendMode, 0.06f, 0.06f, 0.06f);
            }
            
            manaMeldButton.interactable = Crafting.CanCraftCard(cardData, out var e);


            _previousCard = previewCard;

            // _originalParent = _currentCard.ZoomedCard.transform.parent;
            // _currentCard.KillMoveTween();
            // _currentCard.ZoomedCard.transform.SetParent(cardLayout, true);
            // _currentCard.ZoomedCard.RectTransform().anchoredPosition3D = new Vector3(
            //     _currentCard.ZoomedCard.RectTransform().anchoredPosition3D.x,
            //     _currentCard.ZoomedCard.RectTransform().anchoredPosition3D.y, 0);
            // _currentCard.ZoomedCard.RectTransform().DOAnchorPos3D(Vector3.zero, 0.1f);

            //canvasGroup.DOFade(1, 0.25f);

            //Timing.RunCoroutine(AnimateIn());
        }

        public void ResetPreview()
        {

            inventoryObject.SetActive(true);

            // Turn off mana meld button
            manaMeldButton.interactable = false;

            // Turn off ingredients
            for (var i = 0; i < ingredientsPages.pageLayout.childCount; i++)
            {
                var ingredientDisplayPageItem = ingredientsPages.pageLayout.GetChild(i).GetComponent<IngredientDisplayPageItem>();
                ingredientDisplayPageItem.ShowData(i);
            }
            

            // Switch Header
            previewHeader.DOScaleY(0.0f, 0.10f);
            ingredientsHeader.localScale.Set(1f, 0f, 1f);
           // ingredientsHeader.SetActive(true);
            ingredientsHeader.DOScaleY(1.0f, 0.10f).SetDelay(0.10f);
            _previewHeaderInit = false;

            // Card Transition
            // cardLayout.DOLocalMoveX(_origPosPreview.x + 50f, 0.25f);//.SetEase(Ease.InBack);
            cardLayout.DOAnchorPosX(cardLayout.sizeDelta.x + 20, 0.5f).SetEase(Ease.InOutQuint);
            closePreviewButton.DOAnchorPosX(closePreviewButton.sizeDelta.x, 0.5f).SetEase(Ease.InOutQuint);
            previewCanvas.DOFade(0, 0.25f).OnComplete(() =>
            {
                previewObject.SetActive(false);
            });
        }


        //private IEnumerator<float> AnimateOut()
        //{
        //    yield return Timing.WaitUntilDone(
        //        panel.DOAnchorPosX(panel.sizeDelta.x + 20, 0.5f)
        //            .SetEase(Ease.InOutQuint)
        //        .WaitForCompletion(true));

        //    panel.SetActive(false);
        //}

        //private IEnumerator<float> AnimateIn()
        //{
        //    panel.SetActive(true);

        //    yield return Timing.WaitUntilDone(
        //        panel.DOAnchorPosX(0, 0.5f)
        //            .SetEase(Ease.InOutQuint)
        //            .WaitForCompletion(true));
        //}

        public void OnManaMeldButton()
        {
            // MELD THE CARD
            if (previewCard == null) return;
            if (!Crafting.CanCraftCard(previewCard.data, out var e)) return;

            manaMeldButton.interactable = false;
            previewCard.keywordDescriptionPopup = null;

            Timing.RunCoroutine(ManaMeldAnimate());
        }

        public IEnumerator<float> ManaMeldAnimate()
        {
            ManaMeldEditMenu.Instance.deckCollection.OnMeldCard();
            ManaMeldEditMenu.Instance._canEscape = false;
            yield return Timing.WaitForSeconds(0.25f);

            // Play the Cinematic
            Timing.RunCoroutine(cinematic.PlayCinematic(previewCard.data));
            yield return Timing.WaitForSeconds(0.25f);

            Events.Publish(this, EventType.ModifyCurrency, new ModifyCurrencyEventArgs
            {
                CurrencyCode = Currency.MANA_SHARDS,
                Amount = -previewCard.data.craftingData.cost
            });

            for (var i = 0; i < previewCard.data.craftingData.ingredients.Count; i++)
            {
                Events.Publish(this, EventType.ModifyItemUses, new ModifyItemUsagesEventArgs
                {
                    ItemName = previewCard.data.craftingData.ingredients[i].ItemUid,
                    UseAmount = -previewCard.data.craftingData.ingredients[i].Count,
                });
            }

            Events.Publish(this, EventType.ModifyItemUses, new ModifyItemUsagesEventArgs
            {
                ItemName = previewCard.data.id,
                UseAmount = 1
            });
        }

        private void OnModifyCurrency(IMessage message)
        {
            manaMeldButton.interactable = Crafting.CanCraftCard(previewCard.data, out var e);
        }

        private void OnModifyItemUses(IMessage message)
        {
            manaMeldButton.interactable = Crafting.CanCraftCard(previewCard.data, out var e);
        }

        public void Close()
        {
            gameObject.SetActive(false);
        }
    }
}