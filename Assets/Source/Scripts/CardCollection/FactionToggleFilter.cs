using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.DeckEditAPI
{
    public class FactionToggleFilter : MonoBehaviour
    {
        public bool includeNeutral;
        public RectTransform contentLayout;
        public FactionToggleButton factionToggleButtonPrefab;

        private List<FactionToggleButton> _factionToggles;
        public event Action<bool, string> OnFactionToggle;
        private ToggleGroup _toggleGroup;

        private void Awake()
        {
            _toggleGroup = GetComponent<ToggleGroup>();
            _factionToggles=new List<FactionToggleButton>();
            var factions = Enum.GetNames(typeof(Faction));

            for (var i = 0; i < factions.Length; i++)
            {
                if (!factions[i].Equals("Neutral") || includeNeutral)
                {
                    var factionToggleButton = Instantiate(factionToggleButtonPrefab, contentLayout, false);
                    factionToggleButton.SetContent(factions[i]);
                    factionToggleButton.OnValueChanged += FactionToggled;
                    factionToggleButton.Toggle.group = _toggleGroup;
                    _factionToggles.Add(factionToggleButton);
                }
            }
        }

        private void FactionToggled(bool isOn, string content)
        {
            OnFactionToggle?.Invoke(isOn,content);
        }
    }
}