using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.CardCollection
{
    public class NewDeckButton : MonoBehaviour
    {
        public Button button;
        public CanvasGroup glow;
        public RectTransform rightArrow;
        public RectTransform leftArrow;

        private int m_side = 1;
        private float m_waitDelay=0.4f;
        private float m_timer;
        private PropertyInfo m_selectableStateInfo = null;

        private void Start()
        {
            glow.alpha = 0;
            m_selectableStateInfo = typeof(Selectable).GetProperty("currentSelectionState", BindingFlags.NonPublic | BindingFlags.Instance);
        }

        private void Update()
        {
            m_timer += Time.deltaTime;

            if (m_timer >= m_waitDelay)
            {
                m_timer = 0;
                leftArrow.anchoredPosition = new Vector2(-35.5f - (m_side == 1 ? 2 : 0), 2);
                rightArrow.anchoredPosition = new Vector2(36.5f + (m_side == 1 ? 2 : 0), 2);
                m_side *= -1;
            }

            glow.alpha = Mathf.PingPong(Time.time, 1);

            int selectableState = (int)m_selectableStateInfo.GetValue(button);
            switch (selectableState)
            {
                // case 0:
                //     //Normal Selection State
                //     break;
                // case 1:
                //     //Highlighted Selection State
                //     break;
                case 2:
                    leftArrow.SetActive(false);
                    rightArrow.SetActive(false);
                    glow.SetActive(false);
                    break;
                default:
                    leftArrow.SetActive(true);
                    rightArrow.SetActive(true);
                    glow.SetActive(true);
                    break;
                // case 3:
                //     //Selected Selection State
                //     break;
                // case 4:
                //     //Disabled Selection State
                //     break;
            }
        }
    }
}