using CrossBlitz.Databases;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CrossBlitz.CardCollection.CardBacks
{
    public class CardBackView : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler
    {
        public SpriteAnimation cardBack;
        public SpriteOutline outline;
        private string m_portraitUrl;
        public string PortraitUrl => m_portraitUrl;

        public void SetCardBack(string portraitUrl)
        {
            m_portraitUrl = portraitUrl;
            cardBack.Play(m_portraitUrl);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            outline.Regenerate();
            outline.Show();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            outline.Hide();
        }
    }
}