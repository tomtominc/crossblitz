using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Databases;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using UnityEngine;

namespace CrossBlitz.CardCollection.CardBacks
{
    public class PlayerCardBacks : ClientData
    {
        public List<ClientCardBackData> CardBacks;

        public override void Init()
        {
            CardBacks = App.Load("PlayerCardBacks.CardBackData", new List<ClientCardBackData>());

            for (var i = 0; i < Db.CardBackDatabase.cardBacks.Count; i++)
            {
                var gdeCardBack = Db.CardBackDatabase.cardBacks[i];
                var cardBack = GetCardBack(gdeCardBack.Uid);

                if (cardBack == null)
                {
                    cardBack = new ClientCardBackData();
                    cardBack.CardBackUid = gdeCardBack.Uid;
                    cardBack.Unlocked = false;
                    CardBacks.Add(cardBack);
                    dirty = true;
                }

                if (gdeCardBack.portraitUrl == "default")
                {
                    cardBack.Unlocked = true;
                    dirty = true;
                }
            }

            for (var i = CardBacks.Count-1; i > -1; i--)
            {
                var cardBack = CardBacks[i];

                var gdeCardBack = Db.CardBackDatabase.GetCardBackDataCanBeNull(cardBack.CardBackUid);

                if (gdeCardBack == null)
                {
                    CardBacks.RemoveAt(i);
                    dirty = true;
                }
            }

            if (Db.CardDatabase != null )
            {
                CardBacks = CardBacks.OrderBy(
                        cb => Db.CardBackDatabase.GetCardBackData(cb.CardBackUid)?.portraitUrl == "default" ? 0 : 1)
                    .ToList();
            }
        }

        public List<ClientCardBackData> GetCardBacks()
        {
            return CardBacks;
        }

        public void SetCardBacks(List<ClientCardBackData> cardBacks)
        {
            CardBacks = cardBacks;
            dirty = true;
        }

        public override void ResetData()
        {
            base.ResetData();
            CardBacks = new List<ClientCardBackData>();

            dirty = true;
        }

        public override void Save()
        {
            App.Save("PlayerCardBacks.CardBacks", CardBacks);
        }

        public ClientCardBackData GetCardBack(string key)
        {
            return CardBacks.Find(c => c.CardBackUid == key);
        }
    }

    [Serializable]
    public class ClientCardBackData
    {
        public string CardBackUid;
        public bool Unlocked;
    }
}