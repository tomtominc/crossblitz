using System;
using CrossBlitz.Databases;
using CrossBlitz.Quests;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.Utils;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace CrossBlitz.CardCollection.CardBacks
{
    public class CardBackPageItem : PageItem, IPointerEnterHandler, IPointerClickHandler, IPointerExitHandler
    {
        public CanvasGroup canvasGroup;
        public RectTransform rect;
        public RectTransform cardBackSlot;
        public CardBackView cardBackPrefab;
        public GameObject emptySlot;
        public GameObject equippedOverlay;
        public GameObject equippedOutline;
        public GameObject lockedOverlay;
        public AchievementContainerView achievementView;
        private CardBackView m_cardBack;
        private CardBackPageItemData m_data;

        public CardBackPageItemData Data => m_data;
        public event Action<CardBackPageItem> OnCardBackClicked;

        private Tweener m_punchTween;

        private void Start()
        {
            achievementView.SetActive(false);
            achievementView.canvas.alpha = 0;
            equippedOutline.SetActive(false);
        }

        public override void SetAsEmpty(int itemIndex)
        {
            m_data = null;
            emptySlot.SetActive(true);
            rect.SetActive(false);
        }

        public override void SetData(IPageItemData data, int itemIndex)
        {
            emptySlot.SetActive(false);
            rect.SetActive(true);

            if (m_cardBack == null)
            {
                m_cardBack = Instantiate(cardBackPrefab, cardBackSlot, false);
            }

            equippedOverlay.SetActive(false);
            lockedOverlay.SetActive(false);
            achievementView.SetActive(false);

            if (data is CardBackPageItemData cardBackData)
            {
                m_data = cardBackData;

                if (m_data.Equipped)
                {
                    equippedOverlay.SetActive(true);
                }
                else if (m_data.Locked)
                {
                    lockedOverlay.SetActive(true);
                }

                var cb = Db.CardBackDatabase.GetCardBackData(m_data.CardBackId);
                m_cardBack.SetCardBack(cb.portraitUrl);

                achievementView.SetActive(false);
                achievementView.canvas.alpha = 0;
                achievementView.SetAchievementId(m_data.AchievementId);

                if (m_data.AnimationOff)
                {
                    canvasGroup.alpha = 1;
                    rect.anchoredPosition = Vector2.zero;
                }
                else
                {
                    canvasGroup.alpha = 0;
                    rect.anchoredPosition = new Vector2(0, -8f);
                    rect.DOAnchorPosY(6, 0.24f).SetEase(Ease.OutBack).SetDelay(itemIndex*0.02f);
                    canvasGroup.DOFade(1, 0.12f).SetDelay(itemIndex*0.02f);
                }
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (m_data == null) return;

            var cardBack = Db.CardBackDatabase.GetCardBackData(m_data.CardBackId);

            if (!string.IsNullOrEmpty(cardBack.achievementId))
            {
                achievementView.SetActive(true);
                achievementView.RectTransform().DOKill();
                achievementView.canvas.DOKill();

                achievementView.RectTransform().anchoredPosition = new Vector2(132-100, -1);
                achievementView.RectTransform().DOAnchorPosX(132, 0.24f).SetEase(Ease.OutBack).SetDelay(0.24f);
                achievementView.RectTransform().localScale = Vector3.one;
                achievementView.RectTransform().DOPunchScale(Vector3.right * 0.16f, 0.12f).SetDelay(0.24f);
                achievementView.canvas.DOFade(1, 0.24f).SetDelay(0.24f);
            }

            if (m_data.Equipped)
            {
                equippedOutline.SetActive(true);
            }

            if (m_punchTween == null || !m_punchTween.IsActive())
            {
                this.RectTransform().DOKill(true);
                m_punchTween = this.RectTransform().DOPunchAnchorPos(Vector2.down * 5, 0.2f);
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (m_data == null) return;

            OnCardBackClicked?.Invoke(this);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (m_data == null) return;

            achievementView.RectTransform().DOKill();
            achievementView.canvas.DOKill();
            achievementView.SetActive(false);
            achievementView.canvas.alpha = 0;
            equippedOutline.SetActive(false);
        }
    }

    [Serializable]
    public class CardBackPageItemData : IPageItemData
    {
        public string CardBackId;
        public bool Equipped;
        public bool Locked;
        public bool AnimationOff;
        public string AchievementId;
    }
}