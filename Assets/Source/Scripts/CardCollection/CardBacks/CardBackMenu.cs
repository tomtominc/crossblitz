using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.Utils;
using TMPro;
using UnityEngine;

namespace CrossBlitz.CardCollection.CardBacks
{
    public class CardBackMenu : MonoBehaviour
    {
        public CardBackSidePanel sidePanel;
        public CardBackPageItem cardBackPageItemPrefab;
        public TextMeshProUGUI collectionCount;
        public PageBehaviour pages;

        private int m_currentPage;
        private DeckEditSettings m_settings;
        public FilterCardSettings savedCardBackFilterSettings;
        public bool transferSettings;
        private string m_currentCardBack;
        private int m_unlockedCount;

        private void Start()
        {
            sidePanel.OnEquipCardBack += OnEquipCardBack;
            pages.OnPageChanged += OnPageChanged;
        }

        private void OnDestroy()
        {
            sidePanel.OnEquipCardBack -= OnEquipCardBack;
            pages.OnPageChanged -= OnPageChanged;
        }

        public void Open(DeckEditSettings settings)
        {
            this.SetActive(true);

            m_settings = settings;
            RefreshMenu(m_currentPage,true);
        }

        public void Close()
        {
            sidePanel.Close();
            this.SetActive(false);
        }

        public void RefreshMenu(int page, bool animate)
        {
            var cardBacks = App.CardBacks.GetCardBacks();
            var cardBackData = new List<CardBackPageItemData>();
            m_unlockedCount = 0;

            for (var i = 0; i < cardBacks.Count; i++)
            {
                var clientCardBackData = cardBacks[i];
                var cardBack = Db.CardBackDatabase.GetCardBackData(clientCardBackData.CardBackUid);
                var pageData = new CardBackPageItemData();

                if (m_settings.OpenMode == DeckOpenMode.DeckEditor)
                {
                    var deck = App.PlayerDecks.GetDeck(m_settings.DeckSettings.deckData.uid);

                    if (deck.cardBackUid == cardBack.portraitUrl)
                    {
                        pageData.Equipped = true;
                    }
                }

                pageData.CardBackId = clientCardBackData.CardBackUid;
                pageData.Locked = !clientCardBackData.Unlocked;
                pageData.AnimationOff = !animate;

                if (!pageData.Locked) m_unlockedCount += 1;

                cardBackData.Add(pageData);
            }

            collectionCount.text = m_unlockedCount + "/" + cardBacks.Count;

            if (m_settings.OpenMode == DeckOpenMode.DeckEditor)
            {
                sidePanel.SetCardBackForDeck(m_settings.DeckSettings.deckData.uid);
            }

            // Filter Cards
            if (transferSettings)
            {
                m_settings.FilterCardSettings.SetAsDefault();
                savedCardBackFilterSettings.TransferSettings(m_settings.FilterCardSettings);
            }
            if (m_settings.FilterCardSettings.cardBackFilter == Card.CardBackFilter.Unlocked) cardBackData.RemoveAll(card => card.Locked == true);
            if (m_settings.FilterCardSettings.cardBackFilter == Card.CardBackFilter.Locked) cardBackData.RemoveAll(card => card.Locked == false);

            pages.InitializePages(cardBackPageItemPrefab, 8, cardBackData.ConvertAll(c => (IPageItemData) c), page);

            for (var i = 0; i < pages.Items.Count; i++)
            {
                if (pages.Items[i] is CardBackPageItem item)
                {
                    item.OnCardBackClicked += OnCardBackClicked;
                }
            }
        }

        public void OnRefreshMenu()
        {
            RefreshMenu(m_currentPage, true);
        }

        private void OnPageChanged(int page)
        {
            m_currentPage = page;
        }

        private void OnCardBackClicked(CardBackPageItem item)
        {
            if (m_settings.OpenMode == DeckOpenMode.DeckEditor)
            {
                var cardBack = App.CardBacks.GetCardBack(item.Data.CardBackId);

                if (cardBack.Unlocked)
                {
                    var deck = App.PlayerDecks.GetDeck(m_settings.DeckSettings.deckData.uid);
                    sidePanel.HasPendingCardBackChange(deck.cardBackUid != item.Data.CardBackId);
                    m_currentCardBack = item.Data.CardBackId;
                }
                else
                {
                    sidePanel.HasPendingCardBackChange(false);
                    m_currentCardBack = string.Empty;
                }
            }
        }

        private void OnEquipCardBack()
        {
            if (m_settings.OpenMode == DeckOpenMode.DeckEditor)
            {
                if (string.IsNullOrEmpty(m_currentCardBack)) return;

                var deck = App.PlayerDecks.GetDeck(m_settings.DeckSettings.deckData.uid);
                deck.cardBackUid = m_currentCardBack;
                sidePanel.HasPendingCardBackChange(false);
                m_currentCardBack = string.Empty;
                RefreshMenu(m_currentPage, false);
            }
        }
    }
}