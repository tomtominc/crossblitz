using System;
using CrossBlitz.Databases;
using CrossBlitz.PlayFab.Authentication;
using GameDataEditor;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.CardCollection.CardBacks
{
    public class CardBackSidePanel : MonoBehaviour
    {
        public TextMeshProUGUI cardBackName;
        public RectTransform cardBackSlot;
        public CardBackView cardBackViewPrefab;
        public Button equipButton;
        public GameObject sidePanel;

        private string m_deckId;
        public string DeckId => m_deckId;

        private CardBackView m_cardBackView;

        public event Action OnEquipCardBack;
        private void Start()
        {
            equipButton.interactable = false;
            equipButton.onClick.AddListener(EquipCardBack);
        }

        public void SetCardBackForDeck(string deckId)
        {
            sidePanel.SetActive(true);

            m_deckId = deckId;
            var deck = App.PlayerDecks.GetDeck(m_deckId);

            if (deck == null) return;

            var cardBack = Db.CardBackDatabase.GetCardBackByPortraitUrl(deck.cardBackUid);

            if (m_cardBackView == null)
            {
                m_cardBackView = Instantiate(cardBackViewPrefab, cardBackSlot, false);
            }

            m_cardBackView.SetCardBack(deck.cardBackUid);
            cardBackName.text = cardBack.displayName;
        }

        public void Close()
        {
            sidePanel.SetActive(false);
        }

        public void HasPendingCardBackChange(bool value)
        {
            equipButton.interactable = value;
        }

        private void EquipCardBack()
        {
            OnEquipCardBack?.Invoke();
        }
    }
}