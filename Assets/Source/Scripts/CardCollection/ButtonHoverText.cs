using CrossBlitz.ViewAPI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CrossBlitz
{
    public class ButtonHoverText : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public string buttonDescription;
        public TextMeshProUGUI descriptionContainerLabel;

        public void  OnPointerEnter(PointerEventData eventData)
        {
            descriptionContainerLabel.text = buttonDescription;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            descriptionContainerLabel.text = "";
        }
    }
}
