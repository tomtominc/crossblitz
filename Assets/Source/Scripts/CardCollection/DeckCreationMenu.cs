using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asyncoroutine;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.Hero;
using CrossBlitz.MainMenu;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.Utils;
using CrossBlitz.ViewAPI;
using DG.Tweening;
using GameDataEditor;
using MEC;
using Sirenix.OdinInspector;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.CardCollection
{
    public class DeckCreationMenu : MonoBehaviour
    {
        public TextMeshProUGUI titleLabel;
        public ButtonContainer backButton;
        public SpriteAnimation backgroundContainerAnimator;

        public RectTransform CreationHeaderPanel;
        public RectTransform CreationDeckFooterPanel;
        public CanvasGroup CreationGuidePanel;

        [BoxGroup("Hero Select")] public HeroToggleFilter heroFilter;
        [BoxGroup("Hero Select")] public HeroInfoWindow heroInfoMenu;
        [BoxGroup("Hero Select")] public GameObject selectFactionList;
        [BoxGroup("Hero Select")] public GameObject selectDisabledOverlay;
        [BoxGroup("Hero Select")] public ButtonContainer selectHeroButton;

        [BoxGroup("Deck Select")] public PageBehaviour deckPages;
        [BoxGroup("Deck Select")] public DeckSelectPageItem deckItemPrefab;
        [BoxGroup("Deck Select")] public ToggleGroup decksToggleGroup;
        [BoxGroup("Deck Select")] public RectTransform customDeckContainer;
        [BoxGroup("Deck Select")] public GameObject deckRecipeRules;
        [BoxGroup("Deck Select")] public Button deckRecipeInfoButton;
        [BoxGroup("Deck Select")] public DeckRecipeInfoMenu deckInfoMenu;
        [BoxGroup("Deck Select")] public GameObject selectDeckInfoOverlay;
        [BoxGroup("Deck Select")] public GameObject selectDeckDisabledOverlay;
        [BoxGroup("Deck Select")] public ButtonContainer selectDeckButton;
        [BoxGroup("Deck Select")] public DeckCardList deckCardList;
        [BoxGroup("Deck Select")] public DecksListMenu deckList;
        [BoxGroup("Deck Select")] public DeckCollectionMenu deckCollectionMenu;
        [BoxGroup("Deck Select")] public GameObject pageButtons;

        private HeroData m_hero;
        private DeckData m_deck;
        private DeckSelectPageItem m_currentlySelectedDeck;
        private DeckSelectPageItem m_customDeckToggle;

        private void Start()
        {
            Events.Subscribe(EventType.OnHeroChanged, OnHeroToggled);
        }

        public async Task Init()
        {
            selectHeroButton.SetInteractable(false);
            selectHeroButton.Button.onClick.RemoveAllListeners();
            selectHeroButton.Button.onClick.AddListener(OnSelectHero);

            deckRecipeInfoButton.onClick.RemoveAllListeners();
            deckRecipeInfoButton.onClick.AddListener(OpenDeckRecipeInfoPanel);

            //deckFilter.OnDeckToggle -= OnDeckToggled;
            //deckFilter.OnDeckToggle += OnDeckToggled;

            deckPages.OnPageChanged -= OnDecksPageChanged;
            deckPages.OnPageChanged += OnDecksPageChanged;
            deckCardList.OnDeckSelected -= OnDeckSelected;
            deckCardList.OnDeckSelected += OnDeckSelected;

            while (Db.IsBusy) await new WaitForEndOfFrame();
        }



        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnHeroChanged, OnHeroToggled);
        }

        public void Open(DeckEditSettings settings)
        {
            switch (settings.OpenMode)
            {
                case DeckOpenMode.DeckCreator_SelectHero:
                {
                    OpenSelectHero();
                    break;
                }
                // We can open the deck creator to selecting a deck
                // but we need to know which hero we are selecting,
                // if no hero has been set in settings then we go to selecting a hero
                case DeckOpenMode.DeckCreator_SelectDeck:
                {
                    if (m_hero == null)
                    {
                        if (string.IsNullOrEmpty(settings.CurrentHero))
                        {
                            OpenSelectHero();
                            break;
                        }

                        m_hero = Db.HeroDatabase.GetHero(settings.CurrentHero);

                        if (m_hero == null)
                        {
                            OpenSelectHero();
                            break;
                        }
                    }

                    OpenSelectDeck();
                    break;
                }
            }
        }

        private void OpenSelectHero()
        {
            customDeckContainer.DestroyChildren();

            titleLabel.text = LocalizationController.Localize("SELECT_A_HERO", "SELECT A HERO");
            backgroundContainerAnimator.Play("new-deck");
            heroInfoMenu.SetActive(true);
            selectFactionList.SetActive(true);
            heroFilter.SetActive(true);
            heroFilter.Init();

            deckInfoMenu.SetActive(false);
            deckInfoMenu.EraseDeckData();
            //deckFilter.SetActive(false);
            deckPages.SetActive(false);
            deckCardList.SetActive(false);
            pageButtons.SetActive(false);
            deckRecipeRules.SetActive(false);

            SetHeroData(Db.HeroDatabase.GetHero(GDEItemKeys.Hero_Redcroft));
        }

        private void OpenSelectDeck()
        {
            titleLabel.text = LocalizationController.Localize("SELECT_A_DECK", "SELECT A DECK RECIPE");
            backgroundContainerAnimator.Play("new-deck");
            heroInfoMenu.SetActive(false);
            heroInfoMenu.EraseHeroData();
            selectFactionList.SetActive(false);
            heroFilter.SetActive(false);

            deckInfoMenu.SetActive(true);
            deckInfoMenu.EraseDeckData();
            deckCardList.SetActive(true);
            deckRecipeRules.SetActive(true);
            deckCardList.ClearDeck("SELECT A DECK", m_hero.faction, 1);

            decksToggleGroup.allowSwitchOff = true;
            deckPages.SetActive(true);

            var deckData = new List<DeckSelectPageItemData>();
            var recipes = Db.DeckRecipeDatabase.GetCollectableDecksByFaction(m_hero.faction);

            for (var i = 0; i < recipes.Count; i++)
            {
                var data = new DeckSelectPageItemData();
                var item = App.Inventory.GetItem(recipes[i].Uid);

                data.recipeUid = recipes[i].Uid;
                data.isLocked = item == null;
                data.isNew = item?.IsNew ?? false;
                data.deckData = recipes[i].deckData;

                deckData.Add(data);
            }

            OnDecksPageChanged(0);

            deckPages.InitializePages(deckItemPrefab, 4, deckData.ConvertAll( c => (IPageItemData)c));

            for (var i = 0; i < deckPages.Items.Count; i++)
            {
                if (deckPages.Items[i] is DeckSelectPageItem deckItem)
                {
                    deckItem.OnClicked += OnDeckToggled;
                }
            }

            customDeckContainer.DestroyChildren();

            var customRecipe = Db.DeckRecipeDatabase.GetRecipeByName($"Custom {m_hero.faction} Deck");
            m_customDeckToggle = Instantiate(deckItemPrefab, customDeckContainer, false);

            m_customDeckToggle.SetAsCustom( new DeckSelectPageItemData
            {
                recipeUid = customRecipe.Uid,
                deckData = customRecipe.deckData
            });

            m_customDeckToggle.OnClicked += OnDeckToggled;

            pageButtons.SetActive(true);
        }

        private void OnHeroToggled(IMessage message)
        {
            if (message.Data is HeroData heroData)
            {
                SetHeroData(heroData);
            }
        }

        private void SetHeroData(HeroData heroData)
        {
            m_hero = heroData;
            selectHeroButton.SetInteractable(App.Inventory.OwnsItem(m_hero.id));
            heroInfoMenu.SetHeroData(m_hero);
        }

        private void OnDeckToggled(bool isOn, DeckSelectPageItem item)
        {
            if (item == null || item.DeckPageData == null || item.DeckPageData.deckData == null)
            {
                return;
            }

            if (isOn &&  m_currentlySelectedDeck != item)
            {
                if (m_currentlySelectedDeck != null)
                {
                    m_currentlySelectedDeck.DeckView.SetIsSelected(false);
                }

                m_currentlySelectedDeck = item;
            }

            var deckData = GameUtilities.Copy(item.DeckPageData.deckData);

            if (isOn)
            {
                if (m_deck !=null && deckData.uid==m_deck.uid)
                {
                    return;
                }

                var recipe = Db.DeckRecipeDatabase.GetRecipe(item.DeckPageData.recipeUid);

                m_deck = deckData;
                selectDeckButton.SetInteractable(true);
                deckInfoMenu.SetDeckData(recipe);
                selectDeckDisabledOverlay.SetActive(false);
                selectDeckInfoOverlay.SetActive(false);
                deckCardList.OpenWithDeck(m_deck, 1, false, true);
            }
            else
            {
                m_deck = null;
                selectDeckButton.SetInteractable(false);
                deckInfoMenu.EraseDeckData();
                selectDeckDisabledOverlay.SetActive(true);
                selectDeckInfoOverlay.SetActive(true);
                deckCardList.ClearDeck("SELECT A DECK", m_hero.faction, 1);
            }
        }

        private void OnDecksPageChanged(int page)
        {
            m_deck = null;
            m_currentlySelectedDeck = null;

            if (m_customDeckToggle)
            {
                m_customDeckToggle.DeckView.SetIsSelected(false);
            }

            selectDeckButton.SetInteractable(false);
            deckInfoMenu.EraseDeckData();
            selectDeckDisabledOverlay.SetActive(true);
            selectDeckInfoOverlay.SetActive(true);
            deckCardList.ClearDeck("SELECT A DECK", m_hero.faction, 1);
        }

        private void OnSelectHero()
        {
            if (m_hero == null)
            {
                Debug.LogError("Current hero is null!");
                return;
            }

            selectHeroButton.DisableAndFlash();

            Timing.RunCoroutine(AnimateCreateADeckToSelectDeck());

        }

        private void OnDeckSelected(DeckData deckData)
        {
            deckData = GameUtilities.Copy(deckData);
            deckData.GenerateNewId();
            App.PlayerDecks.GetDecks().Add(deckData);
            App.PlayerDecks.Save();
            deckList.RefreshDeckList();
            deckCardList.FromDeckView = deckList.GetDeckView(deckData.uid);
            DeckEditMenu.Instance.Settings.OpenMode = DeckOpenMode.DeckEditor;
            var filterSettings = new FilterCardSettings
            {
                faction = Faction.All, ownedCardsOnly = true, @class = Class.All, rarity = Rarity.All
            };
            DeckEditMenu.Instance.Settings.FilterCardSettings = filterSettings;
            var deckSettings = new DeckSettings
            {
                deckData = deckData
            };
            DeckEditMenu.Instance.Settings.DeckSettings = deckSettings;
            DeckEditMenu.Instance.ChangeOpenSettings(DeckEditMenu.Instance.Settings);
            DeckEditMenu.Instance.SetMostRecentlyEditedDeck(deckData.uid);
        }

        public void Close(DeckEditSettings settings)
        {
            selectFactionList.SetActive(false);
            this.SetActive(false);
        }

        public void OnBackButton()
        {
            // select a hero no longer really exists in the current game. might come back later though.
            selectDeckInfoOverlay.SetActive(false);
            Timing.RunCoroutine(AnimateCreateADeckToDeckList());

            //Timing.RunCoroutine(CanEscape());

            // switch (DeckEditMenu.Instance.Settings.OpenMode)
            // {
            //     case DeckOpenMode.DeckCreator_SelectHero:
            //     {
            //         Timing.RunCoroutine(AnimateCreateADeckToDeckList());
            //         break;
            //     }
            //     case DeckOpenMode.DeckCreator_SelectDeck:
            //     {
            //         Timing.RunCoroutine(AnimateSelectDeckToCreateADeck());
            //         break;
            //     }
            // }
        }

        private IEnumerator<float> AnimateCreateADeckToDeckList()
        {
            deckList.SetActive(true);

            CreationHeaderPanel.RectTransform().DOScaleY(0.0f, 0.10f);
            CreationDeckFooterPanel.RectTransform().DOScaleY(0.0f, 0.10f);

            yield return Timing.WaitUntilDone( CreationGuidePanel.DOFade(0, 0.10f)
                .WaitForCompletion(true));

            deckList.canvasGroup.DOFade(0, 0f);
            deckList.canvasGroup.DOFade(1, 0.10f);

            deckList.HeaderPanel.RectTransform().localScale = new Vector3(1, 0, 1);
            deckList.HeaderPanel.RectTransform().DOScaleY(1f, 0.20f);

            deckList.deckFooterPanel.localScale = new Vector3(1, 0, 1);
            deckList.deckFooterPanel.DOScaleY(1f, 0.20f);

            DeckEditMenu.Instance.Settings.OpenMode = DeckOpenMode.Collection;
            DeckEditMenu.Instance.ChangeOpenSettings(DeckEditMenu.Instance.Settings);

            CreationHeaderPanel.RectTransform().DOScaleY(1.0f, 0.0f);
            CreationDeckFooterPanel.RectTransform().DOScaleY(1.0f, 0.0f);
            CreationGuidePanel.DOFade(1, 0.0f);
        }


        private IEnumerator<float> AnimateCreateADeckToSelectDeck()
        {
            CreationHeaderPanel.RectTransform().DOScaleY(0.0f, 0.10f);
            CreationDeckFooterPanel.RectTransform().DOScaleY(0.0f, 0.10f);

            yield return Timing.WaitUntilDone( CreationGuidePanel.DOFade(0, 0.10f)
                .WaitForCompletion(true));

            deckCardList.deckViewHeader.RectTransform().localScale = new Vector3(1, 0, 1);
            deckCardList.deckViewHeader.RectTransform().DOScaleY(1f, 0.20f);

            deckCardList.deckFooterPanel.localScale = new Vector3(1, 0, 1);
            deckCardList.deckFooterPanel.DOScaleY(1f, 0.20f);

            DeckEditMenu.Instance.Settings.OpenMode = DeckOpenMode.DeckCreator_SelectDeck;
            DeckEditMenu.Instance.ChangeOpenSettings(DeckEditMenu.Instance.Settings);
        }

        private IEnumerator<float> AnimateSelectDeckToCreateADeck()
        {
            deckCardList.deckViewHeader.RectTransform().DOScaleY(0f, 0.10f);
            deckCardList.deckFooterPanel.DOScaleY(0f, 0.10f);

            yield return Timing.WaitUntilDone(CreationGuidePanel.DOFade(1, 0.10f)
             .WaitForCompletion(true));

            CreationHeaderPanel.RectTransform().DOScaleY(0.0f, 0.0f);
            CreationHeaderPanel.RectTransform().DOScaleY(1f, 0.10f);

            CreationDeckFooterPanel.RectTransform().DOScaleY(0.0f, 0f);
            CreationDeckFooterPanel.RectTransform().DOScaleY(1, 0.10f);

            DeckEditMenu.Instance.Settings.OpenMode = DeckOpenMode.DeckCreator_SelectHero;
            DeckEditMenu.Instance.ChangeOpenSettings(DeckEditMenu.Instance.Settings);
        }

        private void OpenDeckRecipeInfoPanel()
        {

        }
    }
}