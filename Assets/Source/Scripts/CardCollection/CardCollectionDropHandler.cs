using CrossBlitz.Card;
using CrossBlitz.CardCollection;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CrossBlitz.DeckEditAPI
{
    /// <summary>
    /// This component handles dropping a card from the deck to the collection and adds it back to the collection.
    /// </summary>
    public class CardCollectionDropHandler : MonoBehaviour,
        IDropHandler,
        IPointerEnterHandler,
        IPointerExitHandler
    {
        public DeckCardList list;
        public CanvasGroup canvasGroup;
        public void Update()
        {
            if (DragCardDeckEditor.Current != null)
            {
                canvasGroup.interactable = true;
                canvasGroup.blocksRaycasts = true;
            }
            else
            {
                canvasGroup.interactable = false;
                canvasGroup.blocksRaycasts = false;
            }
        }

        public void OnDrop(PointerEventData eventData)
        {
            if (DragCardDeckEditor.Current == null ||
                !DragCardDeckEditor.Current.IsInDeck) return;

            // todo: remove this to the deck - in data!
            list.RemoveCard(DragCardDeckEditor.Current.CardData, true);
            DragCardDeckEditor.Current.OnEndDrag(null);
            DragCardDeckEditor.Current = null;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {

        }

        public void OnPointerExit(PointerEventData eventData)
        {

        }
    }
}