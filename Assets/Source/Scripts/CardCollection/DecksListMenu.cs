using System;
using System.Collections.Generic;
using System.Collections;
using CrossBlitz.Card;
using CrossBlitz.CardCollection;
using CrossBlitz.Deck;
using CrossBlitz.Hero;
using CrossBlitz.PlayFab.Authentication;
using DG.Tweening;
using Ionic.Zlib;
using KennethDevelops.ProLibrary.Util.Serialization;
using MEC;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.DeckEditAPI
{
    public class DecksListMenu : MonoBehaviour
    {
        public RectTransform content;
        public CanvasGroup canvasGroup;
        public DeckCardList cardList;
        public GameObject finishButtonOverlay;
        public Button newDeckButton;
        public DeckViewItem deckViewItemPrefab;
        public RectTransform HeaderPanel;
        public RectTransform deckFooterPanel;
        public TextMeshProUGUI deckCountLabel;

        [BoxGroup("Deck Edit")] public Animator deckEditFlipAnimator;
        [BoxGroup("Deck Edit")] public Button deckEditButton;
        [BoxGroup("Deck Edit")] public Button deckEquipButton;

        //public ToggleButton deckEditToggle;
        private List<DeckViewItem> _deckViews;
        private bool _hasDeckSelected;
        public DeckViewItem _currentDeck;
        private DeckEditSettings _settings;

        public event Action<DeckViewItem> OnDeckSelected;

        public void Init()
        {
            canvasGroup.alpha = 1;
            finishButtonOverlay.SetActive(false);
            newDeckButton.onClick.AddListener(OnNewDeckButton);
            deckEditButton.onClick.AddListener(OnDeckEdit);
            deckEquipButton.onClick.AddListener(OnEquipDeck);
        }

        private void Update()
        {
            deckEquipButton.interactable = _currentDeck != null &&
                                           _currentDeck.MDeckData.uid != App.PlayerDecks.GetDeckBasedOnGameState().uid;
        }

        public async void Open(DeckEditSettings settings)
        {
            _settings = settings;

            newDeckButton.interactable = true;
            finishButtonOverlay.SetActive(false);
            RefreshDeckList();

            _hasDeckSelected = false;
        }

        public async void RefreshDeckList()
        {
            var decks = App.PlayerDecks.GetDecks();

            if (decks.Count > 0)
            {
                if (_deckViews == null)
                {
                    _deckViews = new List<DeckViewItem>();
                }

                deckCountLabel.text = $"{decks.Count}/{DeckData.MaxDecks}";
                newDeckButton.SetActive(decks.Count < DeckData.MaxDecks);

                for (var i = 0; i < decks.Count; i++)
                {
                    if (_settings.FilterCardSettings.faction != Faction.All &&
                        _settings.FilterCardSettings.faction != Faction.None)
                    {
                        if (_settings.FilterCardSettings.faction != decks[i].faction)
                        {
                            continue;
                        }
                    }

                    DeckViewItem deckView;

                    if (i >= _deckViews.Count)
                    {
                        deckView = Instantiate(deckViewItemPrefab, content, false);
                        deckView.transform.SetSiblingIndex(content.childCount - 2);
                        deckView.OnDeckSelected += DeckSelected;
                        _deckViews.Add(deckView);
                    }

                    deckView = _deckViews[i];
                    deckView.SetDeck(decks[i]);
                }
            }
        }

        public void RebuildDeckList()
        {
            for (var i = _deckViews.Count - 1; i > -1; i--)
            {
                Destroy(_deckViews[i].gameObject);
            }

            _deckViews = null;
        }

        public DeckViewItem GetDeckView(string uid)
        {
            return _deckViews.Find(deck => deck.MDeckData.uid == uid);
        }

        private void OnNewDeckButton()
        {
            if (App.PlayerDecks.GetDecks().Count >= DeckData.MaxDecks)
            {
                return;
            }

            Timing.RunCoroutine(AnimateDeckListToCreateADeck());

            //newDeckButton.interactable = false;
            //DeckEditMenu.Instance.Settings.OpenMode = DeckOpenMode.DeckCreator_SelectHero;
            //DeckEditMenu.Instance.ChangeOpenSettings(DeckEditMenu.Instance.Settings);
        }

        private void DeckSelected(DeckViewItem deckView)
        {
            if (_currentDeck)
            {
                _currentDeck.OnDeckUnselectedForEdit();
            }

            _currentDeck = deckView;

            if (_currentDeck)
            {
                if (!_hasDeckSelected)
                {
                    _hasDeckSelected = true;
                    deckEditFlipAnimator.Play($"Toggle-Change-{_hasDeckSelected}", -1, 0);
                }

                _currentDeck.OnDeckSelectedForEdit();
            }
        }

        private void OnEquipDeck()
        {
            if (_currentDeck == null) return;

            App.PlayerDecks.EquipDeck(App.PlayerDecks.GetContextBasedOnGameState(), _currentDeck.MDeckData.uid);
            RefreshDeckList();
        }

        private void OnDeckEdit()
        {
            if (!_currentDeck)
            {
                Debug.LogError("No deck selected!");
                return;
            }

            DeckEditMenu.Instance.SetMostRecentlyEditedDeck(_currentDeck.MDeckData.uid);

            Timing.RunCoroutine(AnimateDeckViewItemToHeader(_currentDeck));
        }

        private IEnumerator<float> AnimateDeckListToCreateADeck()
        {
            newDeckButton.interactable = false;
            content.RectTransform().DOKill(true);
            canvasGroup.DOFade(0, 0.10f);
            HeaderPanel.DOScaleY(0.0f, 0.10f);
            deckFooterPanel.DOScaleY(0.0f, 0.10f);
            DeckEditMenu.Instance.backButton.SetActive(true);
            DeckEditMenu.Instance.closeButton.SetActive(false);
            yield return Timing.WaitForSeconds(0.10f);

            cardList.deckViewHeader.RectTransform().DOScaleY(0f, 0.0f);
            cardList.deckViewHeader.RectTransform().DOScaleY(1f, 0.10f);

            cardList.deckFooterPanel.RectTransform().DOScaleY(0f, 0.0f);
            cardList.deckFooterPanel.RectTransform().DOScaleY(1f, 0.10f);

            if (DeckEditMenu.Instance.Settings.FilterCardSettings.forcedFaction == Faction.All)
            {
                DeckEditMenu.Instance.Settings.OpenMode = DeckOpenMode.DeckCreator_SelectHero;
                DeckEditMenu.Instance.ChangeOpenSettings(DeckEditMenu.Instance.Settings);
            }
            else
            {
                DeckEditMenu.Instance.Settings.OpenMode = DeckOpenMode.DeckCreator_SelectDeck;
                DeckEditMenu.Instance.Settings.CurrentHero =
                    HeroData.GetHeroFromFaction(DeckEditMenu.Instance.Settings.FilterCardSettings.forcedFaction);
                DeckEditMenu.Instance.ChangeOpenSettings(DeckEditMenu.Instance.Settings);
            }

            DeckEditMenu.Instance.deckCreation.CreationGuidePanel.DOFade(0, 0f);
            DeckEditMenu.Instance.deckCreation.CreationGuidePanel.DOFade(1, 0.10f);

            DeckEditMenu.Instance.deckCreation.CreationHeaderPanel.RectTransform().DOScaleY(0f, 0f);
            DeckEditMenu.Instance.deckCreation.CreationHeaderPanel.RectTransform().DOScaleY(1.0f, 0.10f);

            DeckEditMenu.Instance.deckCreation.CreationDeckFooterPanel.RectTransform().DOScaleY(0f, 0f);
            DeckEditMenu.Instance.deckCreation.CreationDeckFooterPanel.RectTransform().DOScaleY(1.0f, 0.10f);
        }

        private IEnumerator<float> AnimateDeckViewItemToHeader(DeckViewItem deckView)
        {

            content.RectTransform().DOKill(true);
            deckView.RectTransform().DOKill(true);
            deckView.PunchSelect();

            canvasGroup.DOFade(0, 0.10f);

            HeaderPanel.DOScaleY(0.0f, 0.10f);
            deckFooterPanel.DOScaleY(0.0f, 0.10f);
            yield return Timing.WaitForSeconds(0.10f);

            cardList.FromDeckView = deckView;

            cardList.SetActive(true);

            cardList.deckViewHeader.RectTransform().DOScaleY(0f, 0.0f);
            cardList.deckViewHeader.RectTransform().DOScaleY(1f, 0.10f);

            cardList.deckFooterPanel.RectTransform().DOScaleY(0f, 0.0f);
            cardList.deckFooterPanel.RectTransform().DOScaleY(1f, 0.10f);
            // cardList.deckViewHeader.PunchSelect();
            OnDeckSelected?.Invoke(deckView);
        }
    }
}