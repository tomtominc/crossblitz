using System.Collections.Generic;
using UnityEngine;

namespace CrossBlitz.CardCollection
{
    public class PowerContainer : MonoBehaviour
    {
        public List<RectTransform> powerSlots;

        public void DestroyPowerCards()
        {
            for (var i = 0; i < powerSlots.Count; i++)
            {
                if (powerSlots[i].childCount > 1)
                {
                    Destroy(powerSlots[i].GetChild(1).gameObject);
                }
            }
        }
    }
}