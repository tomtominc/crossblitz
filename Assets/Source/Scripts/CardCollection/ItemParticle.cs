using System.Collections.Generic;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.CardCollection
{
    public class ItemParticle : MonoBehaviour
    {
        public RectTransform rectTransform;
        public CanvasGroup canvas;
        public SpriteAnimation animator;
        public GameObject hitEffect;
        public GameObject sparkleEffect;

        public float initialMoveDuration = 0.12f;
        public float initialMoveAmount = 4;
        public float waitUntilMoveToEnd = 0.12f;
        public float moveToEndDuration = 0.24f;

        public void StartMove(string item, Vector3 startWorldSpace, Vector3 endWorldSpace, Vector2 initialDirection)
        {
            animator.Play(item);
            hitEffect.SetActive(false);
            sparkleEffect.SetActive(true);
            canvas.alpha = 0;
            rectTransform.position = startWorldSpace;

            Timing.RunCoroutine( AnimateToTarget(endWorldSpace, initialDirection).CancelWith(gameObject) );
        }

        private IEnumerator<float> AnimateToTarget(Vector3 endWorldSpace, Vector2 initialDirection)
        {
            canvas.DOFade(1,initialMoveDuration * 0.1f);
            rectTransform.DOAnchorPos(rectTransform.anchoredPosition + (initialDirection * initialMoveAmount), initialMoveDuration)
                .SetEase(Ease.OutExpo);

            animator.RectTransform().DOScaleY(1.2f, initialMoveDuration / 2f).SetEase(Ease.OutBack);
            animator.RectTransform().DOScaleY(1, initialMoveDuration / 2f ).SetDelay(initialMoveDuration/2f).SetEase(Ease.InBack);

            yield return Timing.WaitForSeconds(waitUntilMoveToEnd + moveToEndDuration);

            canvas.DOFade(0, moveToEndDuration);

            yield return Timing.WaitForSeconds(moveToEndDuration);

            if (gameObject) Destroy(gameObject);
        }
    }
}