using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.Universal;
using TakoBoyStudios.Events;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.CardCollection
{
    [RequireComponent(typeof(CurrencyDisplay))]
    public class CurrencyMeldDisplay : MonoBehaviour
    {
        public bool insufficientFundsDisplay;
        public Image container;
        public CurrencyDisplay display;
        public GameObject notEnoughResourcesIcon;

        public void Start()
        {
            Events.Subscribe(ClientAPI.GameLogic.EventSystem.EventType.OnModifiedCurrency, OnModifyCurrency);

            UpdateView();
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(ClientAPI.GameLogic.EventSystem.EventType.OnModifiedCurrency, OnModifyCurrency);
        }

        private void UpdateView()
        {
            var currencyAmount = App.Inventory.GetCurrencyAmount(display.currencyCode);

            if (currencyAmount >= display.CustomNumber || !insufficientFundsDisplay)
            {
                display.currencyIcon.Play(Currency.GetIconName[display.currencyCode]);
                display.nameLabel.color = "#c050a3".ToColor();
                container.color = "#c050a3".ToColor();
                display.labelFormatter.label.color = "#c050a3".ToColor();
                notEnoughResourcesIcon.SetActive(false);
            }
            else
            {
                display.currencyIcon.Play($"{Currency.GetIconName[display.currencyCode]}-grey");
                display.nameLabel.color = "#bfa68f".ToColor();
                container.color = "#bfa68f".ToColor();
                display.labelFormatter.label.color = "#bfa68f".ToColor();
                notEnoughResourcesIcon.SetActive(true);
            }
        }

        private void OnModifyCurrency(IMessage message)
        {
            if (message.Data is OnModifiedCurrencyEventArgs args)
            {
                if (args.CurrencyCode == display.currencyCode) UpdateView();
            }
        }
    }
}