using System.Collections.Generic;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;
using CrossBlitz.ViewAPI;
using CrossBlitz.Dialogue;
using UnityEngine.UI;
using CrossBlitz.ViewAPI.Popups;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.Audio;

namespace CrossBlitz.CardCollection
{
    public class MeldCinematic : MonoBehaviour
    {
        public ShakeContainer shake;
        public ParticleSystem smashParticles;
        public CanvasGroup background;
        public CanvasGroup scrollingBG;
        public SpriteAnimation mawler;
        public CanvasGroup fadeScreen;
        public List<ParticleSystem> cardParticles;

        [BoxGroup("Mawlder Big Container")]
        public GameObject mawlerBigContainer;
        [BoxGroup("Mawlder Big Container")]
        public SpriteAnimation exclamationPoint;
        [BoxGroup("Mawlder Big Container")]
        public SpriteAnimation mawlerBig;
        [BoxGroup("Mawlder Big Container")]
        public SpriteAnimation mawlerEyeGlow;
        [BoxGroup("Mawlder Big Container")]
        public RectTransform mawlerContainer;

        [BoxGroup("Melded Card")]
        public RectTransform parentRect;
        [BoxGroup("Melded Card")]
        public SpriteAnimation meldAnimator;

        [BoxGroup("Mawler Custom Dialogue")] public DialogueBox mawlerDialogue;
        [BoxGroup("Mawler Custom Dialogue")] public RectTransform newDialogueSpeechBubble;
        [BoxGroup("Mawler Custom Dialogue")] public Button mawlerPlayDialogueButton;
        [BoxGroup("Mawler Custom Dialogue")] [TextArea] public List<string> enterDialogue;

        [BoxGroup("Card Particles")] public GameObject particlesOverlayObject;
        [BoxGroup("Card Particles")] public CanvasGroup particlesHitOverlay;
        [BoxGroup("Card Particles")] public Image particlesImage;


        private CardView _zoomedCardPrefab;
        private CardView _meldedCard;
        private CardData _currentCard;

        public ButtonContainer skipButton;
        public CanvasGroup skipButtonCanvas;
        public ButtonContainer continueButton;
        public CanvasGroup continueButtonCanvas;

        private bool skipScene = false;

        private void OnEnable()
        {
            background.alpha = 0;
            scrollingBG.alpha = 0;
            mawler.SetActive(false);
            mawlerBigContainer.SetActive(false);
            parentRect.SetActive(false);
            meldAnimator.SetActive(false);
            mawlerDialogue.SetActive(false);
            skipButton.Button.interactable = false;
            skipButtonCanvas.alpha = 0;
            continueButton.Button.interactable = true;
            continueButtonCanvas.alpha = 0;
            skipScene = false;
        }
        private void Start()
        {
            this.SetActive(true);
            skipButton.Button.onClick.AddListener(OnSkipClicked);
            continueButton.Button.onClick.AddListener(OnContinueClicked);
        }

        private void OnSkipClicked()
        {
            skipScene = true;
            Timing.RunCoroutine(FinishMeldCinematic());
        }
        private void OnContinueClicked()
        {
            skipScene = true;
            Timing.RunCoroutine(FinishMeldResults());
        }


        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (skipButton.isActiveAndEnabled && !skipScene)
                {
                    OnSkipClicked();
                    return;
                }
            }

            if (mawler.CurrentAnimationName == "idle")
            {
                cardParticles.ForEach(x =>
                {
                    if (!x.isPlaying)
                    {
                        x.Play(true);
                    }
                });
            }
            else
            {
                cardParticles.ForEach(x =>
                {
                    if (x.isPlaying)
                    {
                        x.Stop(true, ParticleSystemStopBehavior.StopEmitting);
                    }
                });
            }
        }

        private async void LoadCard()
        {
            if (_zoomedCardPrefab == null)
            {
                var obj = await AddressableReferenceLoader.GetAsset("Card_Zoomed");
                _zoomedCardPrefab = obj.GetComponent<CardView>();

                var settings = new CreateCardFactorySettings
                {
                    CardPrefab = _zoomedCardPrefab.gameObject,
                    Layout = parentRect,
                    CardData = _currentCard,
                    SortingLayer = "Collection",
                    SortingOrder = 11000
                };

                _meldedCard = CardFactory.Create(settings, null);
                _meldedCard.SetInteractable(false);
                _meldedCard.cardOutline.RemoveOutline();
                _meldedCard.RectTransform().anchoredPosition3D = Vector3.zero;
                _meldedCard.SetActive(false);
            }

            _zoomedCardPrefab.keywordDescriptionPopup = null;
            _meldedCard.keywordDescriptionPopup = null;

            _zoomedCardPrefab.SetCardData(_currentCard);
            _meldedCard.SetCardData(_currentCard);
        }

        public IEnumerator<float> PlayCinematic(CardData card)
        {
            _currentCard = card;
            LoadCard();

            background.alpha = 0;
            fadeScreen.alpha = 0;
            mawler.SetActive(false);
            exclamationPoint.GetComponent<CanvasGroup>().alpha = 1;
            exclamationPoint.SetActive(false);
            mawlerEyeGlow.SetActive(false);
            _meldedCard.SetActive(false);
            mawlerBig.Play("open-eyes");
            mawlerBig.Stop();
            this.SetActive(true);
            AudioController.PlaySound("manameld_fire", "BARDSFX");

            skipButtonCanvas.DOFade(1, 0.25f);

            yield return Timing.WaitUntilDone(background.DOFade(1, 0.25f).WaitForCompletion(true));

            skipButton.Button.interactable = true;

            mawler.SetActive(true);
            mawler.Play("spawn");

            while (mawler.IsDone == false) yield return Timing.WaitForOneFrame;

            mawler.Play("idle");

            yield return Timing.WaitForSeconds(0.5f);

            const int hammerTimes = 3;

            for (var i = 0; i < hammerTimes; i++)
            {
                mawler.Play("hammer", OnSmashFrameUpdate);

                while (mawler.IsDone == false)
                {
                    yield return Timing.WaitForOneFrame;
                }

                mawler.Play("idle");
                yield return Timing.WaitForSeconds(0.05f);
            }

            yield return Timing.WaitForSeconds(0.5f);

           // mawlerBigContainer.SetActive(true);
            //fadeScreen.DOFade(1, 0.24f);
            mawlerContainer.anchoredPosition = new Vector2(mawlerContainer.anchoredPosition.x, 12);
           // yield return Timing.WaitUntilDone(mawlerContainer.DOSizeDelta(new Vector2(mawlerContainer.sizeDelta.x, 100), .25f)
           //     .SetEase(Ease.OutBack).WaitForCompletion(true));
          //  mawlerBig.Play("open-eyes",OnOpenEyes);

           // while (mawlerBig.IsDone == false) yield return Timing.WaitForOneFrame;

           // yield return Timing.WaitForSeconds(1f);

           // exclamationPoint.GetComponent<CanvasGroup>().DOFade(0, 0.12f);

          //  yield return Timing.WaitUntilDone(mawlerContainer.DOSizeDelta(new Vector2(mawlerContainer.sizeDelta.x, 0), .25f)
   // .SetEase(Ease.InBack).WaitForCompletion(true));


            if (!skipScene)
            {
                skipScene = true;
                Timing.RunCoroutine(FinishMeldCinematic());
            }

        }

        private void OnOpenEyes(int frames)
        {
            if (frames == 7)
            {
                mawlerEyeGlow.SetActive(true);
                mawlerEyeGlow.Play("mawlder", () =>
                {
                    mawlerEyeGlow.SetActive(false);
                });
                exclamationPoint.SetActive(true);
                exclamationPoint.Play("mawlder");
            }
        }

        private void OnSmashFrameUpdate(int frame)
        {
            if (frame == 3)
            {
                HammerSmash();
            }
        }

        private void HammerSmash()
        {
            
            Debug.Log("Smash?");
            AudioController.PlaySound("manameld_hammer", "BARDSFX");
            shake.transform.DOPunchPosition(Vector2.down * 2f, 0.2f).SetEase(Ease.OutBack);
            //shake.Shake(10,0.24f);
           // smashParticles.Play();
        }

        public IEnumerator<float> FinishMeldCinematic()
        {
            Debug.Log("FINISH MELD CINEMATIC");
            skipButton.Button.interactable = false;
            skipButtonCanvas.DOFade(0, 0.25f);

            scrollingBG.DOFade(1, 0.25f);

            yield return Timing.WaitForSeconds(0.5f);

            AudioController.PlaySound("manameld_card_spawn", "BARDSFX");

            var hero = Db.CharacterDatabase.GetCharacter("Mawlder");
            DialogueSnippetData snippet = null;

            var snippets = hero.dialogueSnippets.FindAll(d => d.Option == "Shop/Purchase");
            if (snippets.Count > 0)
            {
                snippet = snippets[UnityEngine.Random.Range(0, snippets.Count)];
            }

            if (snippet != null)
            {
                if (!string.IsNullOrEmpty(snippet.SoundFx))
                {
                    AudioController.PlaySound(snippet.SoundFx);
                }

                mawlerDialogue.SetActive(true);
                mawlerDialogue.InitDialogueBox(null);

                Timing.RunCoroutine(mawlerDialogue.ShowText(new DialogueInfo
                {
                    SkipPressedActions = true,
                    Text = snippet.Dialogue,
                    SkipAnimateIn = false
                }));
            }

            //yield return Timing.WaitForSeconds(1f);

            parentRect.SetActive(true);
            meldAnimator.SetActive(true);
            if(_currentCard.type.ToString() == "Power") meldAnimator.Play("trick");
            else meldAnimator.Play(_currentCard.type.ToString().ToLower());
            while (meldAnimator.CurrentFrame < 7) yield return Timing.WaitForOneFrame;
            _meldedCard.SetActive(true);
            _meldedCard.frontOverlayCanvas.alpha = 1;
            _meldedCard.frontOverlay.image.color = "8a488e".ToColor();
            _meldedCard.frontOutline.DOFade(0, 0.12f);

            particlesImage.sprite = _meldedCard.frontOverlay.image.sprite;
            particlesImage.SetNativeSize();

            shake.Shake(10, 0.24f);

            particlesHitOverlay.SetActive(true);
            particlesOverlayObject.SetActive(true);
            particlesHitOverlay.alpha = 0.5f;

            yield return Timing.WaitForSeconds(0.5f);

            continueButton.Button.interactable = true;
            continueButtonCanvas.DOFade(1, 0.25f);

            // Timing.RunCoroutine(FinishMeldResults());
        }

        public IEnumerator<float> FinishMeldResults()
        {
            particlesHitOverlay.DOFade(0, 0.25f);
            scrollingBG.DOFade(0, 0.25f);
            yield return Timing.WaitUntilDone(background.DOFade(0, 0.25f).WaitForCompletion(true));

            ManaMeldEditMenu.Instance._canEscape = true;
            particlesHitOverlay.SetActive(false);
            this.SetActive(false);
        }


    }
}