
using System;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using DG.Tweening;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.CardCollection
{
    public class CardTalesContainer : MonoBehaviour
    {
        public CanvasGroup fadedBackground;
        public RectTransform container;
        public TextMeshProUGUI description;

        private void Start()
        {
            Events.Subscribe(EventType.OnCardHoveredOver, OnCardHovered);
            Events.Subscribe(EventType.OnMenuStatusChanged, OnMenuOpened);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnCardHoveredOver, OnCardHovered);
            Events.Unsubscribe(EventType.OnMenuStatusChanged, OnMenuOpened);
        }

        //private void Update()
        //{
        //    var percentageIn = Math.Abs((container.anchoredPosition.y+64) / 64f);
        //    fadedBackground.alpha = percentageIn;
        //}

        private void Open()
        {
            //DOTween.Kill(container);

            //if (container.anchoredPosition.y > -64f)
            //{
            //    container.DOAnchorPosY(0, 0.24f)
            //        .SetEase(Ease.OutBack);
            //}
            //else
            //{

            //    container.DOAnchorPosY(0, 0.24f)
            //        .SetEase(Ease.OutBack).SetDelay(1f);
            //}

        }

        private void Close()
        {
            DOTween.Kill(container);

            container.DOAnchorPosY(-64f, 0.24f);
        }

        private void OnCardHovered(IMessage message)
        {
            if (message.Data == null)
            {
                Close();
            }
            else if (message.Data is CardData data)
            {
                description.text = data.description;
                Open();
            }
        }

        private void OnMenuOpened(IMessage message)
        {
            // if (message.Data is OnMenuStatusChangedEventArgs args)
            // {
            //     switch (args.MenuName)
            //     {
            //         case "ManaMeld": Close();
            //             break;
            //     }
            // }
        }
    }
}