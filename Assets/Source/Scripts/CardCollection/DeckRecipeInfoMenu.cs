using CrossBlitz.AddressablesAPI;
using CrossBlitz.Databases;
using CrossBlitz.DeckEditAPI;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace CrossBlitz.CardCollection
{
    public class DeckRecipeInfoMenu : MonoBehaviour
    {
        public TextMeshProUGUI title;
        public TextMeshProUGUI info;
        public SpriteAnimation heroEmblem;
        public SpriteAnimation factionWatermark;
        public Button deckRecipeOpenButton;

        private DeckRecipeData m_recipe;

        private void Start()
        {
            deckRecipeOpenButton.onClick.AddListener(OpenRecipe);
        }

        public void SetDeckData(DeckRecipeData recipe)
        {
            m_recipe = recipe;
            title.text = m_recipe.deckData.name;
            info.text = $"<color=#916154>{m_recipe.overview}</color>";

            var faction = m_recipe.deckData.faction.ToString().ToLower();
            factionWatermark.Play(faction);
            var hero = m_recipe.deckData.hero.ToLower();
            heroEmblem.Play(hero);

            deckRecipeOpenButton.SetActive(m_recipe.deckData.cards.Count > 0);
        }

        public void EraseDeckData()
        {
            m_recipe = null;
            title.text = string.Empty;
            info.text = "Select a recipe above, or select\n<color=#916154>custom deck</color> to begin.";
            deckRecipeOpenButton.SetActive(false);
        }

        private async void OpenRecipe()
        {
            if (m_recipe == null) return;

            var sceneModel = await SceneController.Instance.LoadScene(new SceneLoadParams
            {
                SceneToLoad = "RecipeViewer",
                LoadSceneMode = LoadSceneMode.Additive
            });

            var recipeViewer = sceneModel.GetSceneData<RecipeViewerManager>();
            recipeViewer.OnClosed += OnRecipeViewerClosed;
            recipeViewer.Open( m_recipe );
        }

        private void OnRecipeViewerClosed()
        {

        }
    }
}