using System;
using CrossBlitz.Deck;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.UI.Widgets;
using CrossBlitz.Utils;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CrossBlitz.CardCollection
{
    public class DeckSelectPageItem : PageItem, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
    {
        public CanvasGroup canvas;
        public DeckView deckViewPrefab;
        public RectTransform deckAnimator;

        private DeckView m_deckView;
        public DeckView DeckView => m_deckView;
        private DeckSelectPageItemData m_deckPageData;
        public DeckSelectPageItemData DeckPageData => m_deckPageData;
        public event Action<bool, DeckSelectPageItem> OnClicked;

        private void OnDestroy()
        {
            deckAnimator.DOKill();
        }

        public override void SetAsEmpty(int itemIndex)
        {
            deckAnimator.DOKill();
            deckAnimator.anchoredPosition = new Vector2(0, 0);
            deckAnimator.SetActive(false);
        }

        public override void SetData(IPageItemData data, int itemIndex)
        {
            canvas.DOKill();
            canvas.alpha = 0;
            m_deckPageData = (DeckSelectPageItemData) data;

            if (m_deckView == null)
            {
                m_deckView = Instantiate(deckViewPrefab, deckAnimator, false);
                m_deckView.RectTransform().anchoredPosition = Vector2.zero;
            }

            m_deckView.SetDeck( m_deckPageData.deckData );
            m_deckView.SetIsNew( m_deckPageData.isNew );
            m_deckView.SetIsLocked( m_deckPageData.isLocked );
            m_deckView.SetIsSelected(false);

            var seq = DOTween.Sequence();

            deckAnimator.SetActive(true);
            deckAnimator.RectTransform().DOKill();
            deckAnimator.anchoredPosition = new Vector2(0, 16);

            seq.AppendInterval(0.1f * itemIndex);
            seq.AppendCallback(() => canvas.DOFade(1,0.1f));
            seq.Join(deckAnimator.RectTransform().DOAnchorPos(Vector2.zero, 0.3f).SetEase(Ease.OutBounce));
        }

        public void SetAsCustom(DeckSelectPageItemData itemData)
        {
            m_deckPageData = itemData;

            if (m_deckView == null)
            {
                m_deckView = Instantiate(deckViewPrefab, deckAnimator, false);
                m_deckView.RectTransform().anchoredPosition = Vector2.zero;
            }

            m_deckView.SetDeckAsCustom(itemData.deckData);
            m_deckView.SetIsNew( false );
            m_deckView.SetIsLocked( false );
            m_deckView.SetIsSelected(false);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (m_deckView == null)
            {
                return;
            }

            m_deckView.SetIsOutlined(true);

            if (m_deckPageData != null && m_deckPageData.isNew && !string.IsNullOrEmpty(m_deckPageData.recipeUid))
            {
                m_deckView.SetIsNew(false);

                var item = App.Inventory.GetItem(m_deckPageData.recipeUid);

                if (item != null)
                {
                    item.IsNew = false;
                    App.Inventory.SetIsDirty(true);
                }
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (m_deckView == null) return;
            m_deckView.SetIsOutlined(false);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (m_deckView == null)
            {
                return;
            }

            if (m_deckPageData !=null && m_deckPageData.isLocked)
            {
                return;
            }

            var isSelected = m_deckView.ToggleIsSelected();

            if (isSelected)
            {
                m_deckView.Punch();
            }

            OnClicked?.Invoke(isSelected,this);
        }
    }

    public class DeckSelectPageItemData : IPageItemData
    {
        public string recipeUid;
        public bool isNew;
        public bool isLocked;
        public bool isCustom;
        public DeckData deckData;
    }
}