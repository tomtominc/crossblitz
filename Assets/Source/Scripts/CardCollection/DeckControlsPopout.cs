using System;
using CrossBlitz.Deck;
using CrossBlitz.PlayFab.Authentication;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace CrossBlitz.DeckEditAPI
{
    public class DeckControlsPopout : MonoBehaviour
    {
        public RectTransform buttonContainer;
        public Button renameDeckButton;
        public Button deleteDeckButton;
        public Button closeButton;
        public Button closeButton2;
        public TMP_InputField deckNameChangeInput;

        private DeckViewItem _currentDeckView;

        public event Action<DeckViewItem> OnDeckDeleted;
        public event Action OnClosed;

        public void Init()
        {
            renameDeckButton.onClick.AddListener(OnRenameDeck);
            deleteDeckButton.onClick.AddListener(OnDeleteDeck);
            closeButton.onClick.AddListener(OnClose);
            closeButton2.onClick.AddListener(OnClose);
            deckNameChangeInput.onEndEdit.AddListener(OnEndEdit);
            //deckNameChangeInput.onSubmit.AddListener(OnSubmit);
        }

        public void Open(DeckViewItem deckView)
        {
            _currentDeckView = deckView;

            this.SetActive(true);
            buttonContainer.DOPunchAnchorPos(Vector2.down * 4f, 0.25f);

            deckNameChangeInput.text = string.Empty;
            deckNameChangeInput.SetActive(false);
            ((TextMeshProUGUI) deckNameChangeInput.placeholder).color = deckView.deckNameLabel.color;
            ((TextMeshProUGUI) deckNameChangeInput.placeholder).text = deckView.MDeckData.name;
        }

        private void OnRenameDeck()
        {
            deckNameChangeInput.SetActive(true);
            EventSystem.current.SetSelectedGameObject(deckNameChangeInput.gameObject);
        }

        private void OnDeleteDeck()
        {
            OnDeckDeleted?.Invoke(_currentDeckView);
            OnClose();
        }

        private void OnClose()
        {
            OnClosed?.Invoke();
            Close();
        }

        private void Close()
        {
            this.SetActive(false);
        }

        private void OnEndEdit(string edit)
        {
            deckNameChangeInput.SetActive(false);

            var deck = App.PlayerDecks.GetDeck(_currentDeckView.MDeckData.uid);

            if (deck != null)
            {
                deck.name = edit;
                _currentDeckView.SetDeck(deck);
            }
        }
    }
}