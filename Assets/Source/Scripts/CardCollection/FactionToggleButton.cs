using CrossBlitz.Card;
using CrossBlitz.UI.Widgets;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace CrossBlitz.DeckEditAPI
{
    public class FactionToggleButton : ToggleButton
    {
        public RectTransform animationRect;
        public SpriteAnimation factionSlot;
        public TextMeshProUGUI factionName;
        public Material allMaterial;
        public Material factionDefaultMaterial;
        public Color allColor;
        public Color factionDefaultColor;
        private Faction _faction;

        protected override void ToggleValueChanged(bool isOn)
        {
            base.ToggleValueChanged(isOn);

            if (isOn)
            {
                DOTween.Kill(animationRect,true);

                animationRect.localScale = new Vector3(1.1f, 1.25f, 1);
                animationRect.DOScale(
                        new Vector3(1f, 1f, 1f), 0.25f)
                    .SetEase(Ease.OutBack);
            }
        }

        public override void SetContent(string c)
        {
            base.SetContent(c);

            _faction = GameUtilities.ParseEnum(content, Faction.All);

            factionName.fontSharedMaterial = _faction == Faction.All ?
                allMaterial : factionDefaultMaterial;

            factionName.color = _faction == Faction.All ?
                allColor : factionDefaultColor;

            factionName.text = content.ToUpper();
            factionSlot.Play(content.ToLower());
        }
    }
}