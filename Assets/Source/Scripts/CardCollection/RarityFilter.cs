using System;
using System.Collections.Generic;
using CrossBlitz.UI.Widgets;
using UnityEngine;

namespace CrossBlitz.DeckEditAPI
{
    public class RarityFilter : MonoBehaviour
    {
        public List<ToggleButton> rarityToggles;
        public event Action<bool, string> OnRarityToggle;

        private void Awake()
        {
            for (var i = 0; i < rarityToggles.Count; i++)
            {
                rarityToggles[i].OnValueChanged += RarityToggled;
            }
        }

        private void RarityToggled(bool isOn, string rarity)
        {
            OnRarityToggle?.Invoke(isOn, rarity);
        }
    }
}