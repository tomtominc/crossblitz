using System;
using System.Collections.Generic;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Audio;
using CrossBlitz.CardCollection;
using CrossBlitz.CardCollection.CardBacks;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.UI.Widgets;
using CrossBlitz.UI.Widgets.Dropdown;
using CrossBlitz.ViewAPI;
using CrossBlitz.ViewAPI.Popups;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.DeckEditAPI
{

    public class DeckEditMenu : View
    {
        public static DeckEditMenu Instance;

        [BoxGroup("Menu")] public CanvasGroup globalFadeGroup;
        [BoxGroup("Menu")] public CanvasGroup disableCanvas;
        [BoxGroup("Menu")] public CanvasGroup backgroundFader;
        [BoxGroup("Menu")] public Button backButton;
        [BoxGroup("Menu")] public Button closeButton;

        [BoxGroup("Menu/Sub Menu")] public DeckCollectionMenu deckCollection;
        [BoxGroup("Menu/Sub Menu")] public DeckCreationMenu deckCreation;
        [BoxGroup("Menu/Sub Menu")] public CardBackMenu cardBackMenu;
        [BoxGroup("Menu/Sub Menu")] public DeckOptions deckOptions;

        [BoxGroup("Filters")] public GameObject cardFilterContainer;
        [BoxGroup("Filters")] public GameObject tabContainer;
        [BoxGroup("Filters")] public ToggleButton cardsTabButton;
        [BoxGroup("Filters")] public ToggleButton relicsTabButton;
        [BoxGroup("Filters")] public ToggleButton cardBacksTabButton;
        [BoxGroup("Filters")] public GameObject cardsTabNotification;
        [BoxGroup("Filters")] public GameObject relicsTabNotification;
        [BoxGroup("Filters")] public GameObject cardBacksTabNotification;


        [BoxGroup("Animation")] public RectTransform menuHeaderRect;
        [BoxGroup("Animation")] public SpriteAnimation menuHeader;
        [BoxGroup("Sfx")] public string revealHeader = "UI-Battle_RevealCard";
        [BoxGroup("Sfx")] public string removeHeader = "UI-ButtonFlip";

        private DeckEditSettings _settings;
        public DeckEditSettings Settings => _settings;

        public event Action OnCollectionClosed;

        public String lastEditedDeckID;
        private String equippedDeckID;
        private bool _canEscape;

        private void Awake()
        {
            globalFadeGroup.alpha = 0;
            _canEscape = false;

            AudioController.SetMusicParameter("map_cutscene", 1);

            cardsTabButton.OnValueChanged += OnTabMenuChanged;
            relicsTabButton.OnValueChanged += OnTabMenuChanged;
            cardBacksTabButton.OnValueChanged += OnTabMenuChanged;
        }

        public override void Initialize(SceneModel sceneModel)
        {
            base.Initialize(sceneModel);

            Instance = this;
            backButton.onClick.AddListener(OnBackButton);

            cardsTabButton.Toggle.isOn = true;
            cardsTabButton.Toggle.interactable = false;

            relicsTabButton.Toggle.isOn = false;
            relicsTabButton.Toggle.interactable = true;

            cardBacksTabButton.Toggle.isOn = false;
            cardBacksTabButton.Toggle.interactable = true;
        }

        public async void OpenCollectionManagerWithSettings(DeckEditSettings _deckEditSettings)
        {
            Instance = this;
            backButton.onClick.AddListener(OnBackButton);
            closeButton.onClick.AddListener(OnCloseButton);

            backButton.RectTransform().anchoredPosition = new Vector2(backButton.RectTransform().anchoredPosition.x, 32);
            closeButton.RectTransform().anchoredPosition = new Vector2(closeButton.RectTransform().anchoredPosition.x, 32);

            Timing.RunCoroutine(AnimateNewHeaderIn("cards"));

            InitializeNewCardsNotifications();

            backgroundFader.alpha = 0;
            backgroundFader.DOFade(1, 0.4f);

            await deckCollection.Init();
            await deckCreation.Init();

            await System.Threading.Tasks.Task.Delay(240);

            AudioController.PlaySound("Hero_Appear-Whoosh");

            globalFadeGroup.transform.DOPunchScale(Vector3.one * 0.25f, 0.4f);
            globalFadeGroup.DOFade(1, 0.3f);

            backButton.GetComponent<CanvasGroup>().alpha = 1;
            backButton.RectTransform().DOAnchorPosY(-1, 0.24f).SetEase(Ease.OutBack);
            closeButton.GetComponent<CanvasGroup>().alpha = 1;
            closeButton.RectTransform().DOAnchorPosY(-1, 0.24f).SetEase(Ease.OutBack);


            if (_deckEditSettings != null)
            {
                Open(_deckEditSettings);
            }

            Timing.RunCoroutine(CanEscape());
        }

        // this is where all the inits go, only happens once while this is open
        public override async void Open()
        {
            Timing.RunCoroutine(AnimateNewHeaderIn("cards"));

            backgroundFader.alpha = 0;
            backgroundFader.DOFade(1, 0.4f);

            await deckCollection.Init();
            await deckCreation.Init();

            var deckEditSettings = model.GetSceneParameter<DeckEditSettings>("DeckEditSettings");

            if (deckEditSettings != null)
            {
                Open(deckEditSettings);
            }
        }

        protected virtual void Open(DeckEditSettings settings)
        {
            _settings = settings;

            switch (_settings.OpenMode)
            {
                case DeckOpenMode.Collection when _settings.MenuMode == CollectionMenuMode.CardBacks:
                case DeckOpenMode.DeckEditor when _settings.MenuMode == CollectionMenuMode.CardBacks:
                {
                    deckCollection.factionFilter.SetActive(false);
                    deckCollection.classFilter.SetActive(false);
                    deckCollection.keywordFilter.SetActive(false);
                    deckCollection.typeFilter.SetActive(false);
                    deckCollection.rarityFilter.SetActive(false);
                    deckCollection.relicFilter.SetActive(false);
                    deckCollection.cardSearchFilter.SetActive(false);
                    deckCollection.cardSetFilter.SetActive(false);
                    deckCollection.cardBacksFilter.SetActive(true);
                    tabContainer.SetActive(true);
                    OpenCardBacks(_settings);
                    break;
                }
                case DeckOpenMode.Collection when _settings.MenuMode == CollectionMenuMode.Relics:
                case DeckOpenMode.DeckEditor when _settings.MenuMode == CollectionMenuMode.Relics:
                {
                    deckCollection.factionFilter.SetActive(false);
                    deckCollection.classFilter.SetActive(false);
                    deckCollection.keywordFilter.SetActive(false);
                    deckCollection.typeFilter.SetActive(false);
                    deckCollection.rarityFilter.SetActive(false);
                    deckCollection.cardSetFilter.SetActive(false);
                    deckCollection.cardSearchFilter.SetActive(false);
                    deckCollection.relicFilter.SetActive(true);
                    deckCollection.cardBacksFilter.SetActive(false);
                    tabContainer.SetActive(true);
                    OpenCollection(_settings);
                    break;
                }
                case DeckOpenMode.Collection:
                case DeckOpenMode.DeckEditor:
                {
                    deckCollection.factionFilter.SetActive(true);
                    deckCollection.classFilter.SetActive(true);
                    deckCollection.keywordFilter.SetActive(true);
                    deckCollection.typeFilter.SetActive(true);
                    deckCollection.rarityFilter.SetActive(true);
                    deckCollection.cardSetFilter.SetActive(true);
                    deckCollection.cardSearchFilter.SetActive(true);
                    deckCollection.relicFilter.SetActive(false);
                    deckCollection.cardBacksFilter.SetActive(false);
                    tabContainer.SetActive(true);
                    OpenCollection(_settings);
                    break;
                }
                case DeckOpenMode.DeckCreator_SelectHero:
                case DeckOpenMode.DeckCreator_SelectDeck:
                {
                    deckCollection.factionFilter.SetActive(false);
                    deckCollection.classFilter.SetActive(false);
                    deckCollection.keywordFilter.SetActive(false);
                    deckCollection.typeFilter.SetActive(false);
                    deckCollection.rarityFilter.SetActive(false);
                    deckCollection.cardSetFilter.SetActive(false);
                    deckCollection.cardSearchFilter.SetActive(false);
                    deckCollection.relicFilter.SetActive(false);
                    deckCollection.cardBacksFilter.SetActive(false);
                    tabContainer.SetActive(false);
                    OpenDeckCreator(_settings);
                    break;
                }
            }

            Events.Publish(this, EventType.OnOpenCollectionMenu, new OnOpenCollectionMenuEventArgs { deckEditSettings = Settings } );
        }

        public void ChangeOpenSettings(DeckEditSettings settings)
        {
            Open(settings);
        }

        private void OpenCardBacks(DeckEditSettings settings)
        {
            deckCreation.Close(settings);
            deckCollection.Open(settings);
            cardBackMenu.transferSettings = true;
            cardBackMenu.savedCardBackFilterSettings = deckCollection.savedCardBackFilterSettings;
            cardBackMenu.Open(settings);
            backButton.SetActive(false);
            closeButton.SetActive(true);
        }

        private void OpenRelics(DeckEditSettings settings)
        {
            cardBackMenu.Close();
            deckCreation.Close(settings);
            deckCollection.SetActive(true);
            deckCollection.Open(settings);
            backButton.SetActive(false);
            closeButton.SetActive(true);
        }

        private void OpenCollection(DeckEditSettings settings)
        {
            cardBackMenu.Close();
            deckCreation.Close(settings);
            deckCollection.SetActive(true);
            deckCollection.Open(settings);
            backButton.SetActive(false);
            closeButton.SetActive(true);
        }

        private void OpenDeckCreator(DeckEditSettings settings)
        {
            cardBackMenu.Close();
            deckCollection.Close(settings);
            deckCreation.SetActive(true);
            deckCreation.Open(settings);
            backButton.SetActive(true);
            closeButton.SetActive(false);
        }

        public void SetMostRecentlyEditedDeck(String uid)
        {
            lastEditedDeckID = uid;
        }

        public string GetMostRecentlyEditedDeck()
        {
            return lastEditedDeckID;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (!PopupController.IsOpen && _canEscape)
                {
                    _canEscape = false;
                    OnBackButton();
                    return;
                }
            }

            //if (Input.GetKeyDown(KeyCode.Escape) && !disableCanvas.interactable)
            //{
            //    OnBackButton();
            //}

            if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.LeftCommand))  && Input.GetKeyDown(KeyCode.C) &&
                _settings.DeckSettings != null && _settings.DeckSettings.deckData != null)
            {
                GUIUtility.systemCopyBuffer = _settings.DeckSettings.deckData.ToRecipe();
                Debug.Log("Deck successfully copied.");
            }

            if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.LeftCommand))  && Input.GetKeyDown(KeyCode.N) &&
                _settings.DeckSettings != null && _settings.DeckSettings.deckData != null)
            {
                if (!string.IsNullOrEmpty(GUIUtility.systemCopyBuffer))
                {
                    var currentDeck = App.PlayerDecks.GetDeck(_settings.DeckSettings.deckData.uid);
                    currentDeck.name = GUIUtility.systemCopyBuffer;
                    _settings.DeckSettings.deckData = currentDeck;
                    OpenCollection(_settings);
                }
            }

            if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.LeftCommand)) && Input.GetKeyDown(KeyCode.V) &&
                _settings.DeckSettings != null && _settings.DeckSettings.deckData != null)
            {
                var currentDeck = App.PlayerDecks.GetDeck(_settings.DeckSettings.deckData.uid);

                if (currentDeck != null)
                {
                    var deckData = DeckData.FromRecipe(GUIUtility.systemCopyBuffer, currentDeck.uid);

                    if (deckData != null)
                    {
                        var indexOfDeck = App.PlayerDecks.GetDecks().IndexOf(currentDeck);
                        App.PlayerDecks.GetDecks()[indexOfDeck] = deckData;
                        _settings.DeckSettings.deckData = currentDeck;
                        OpenCollection(_settings);
                        //Debug.Log("Deck successfully pasted.");
                    }
                    else
                    {
                        Debug.LogError("deck data is null");
                    }
                }
                else
                {
                    Debug.LogError("Current Deck is null.");
                }
            }
        }

        private async void CloseCollection()
        {
            AudioController.PlaySound("Hero_Appear-Whoosh");
            AudioController.SetMusicParameter("map_cutscene", 0);

            Timing.RunCoroutine(AnimateHeaderOut());

            DisableScreen(true);

            backButton.RectTransform().DOAnchorPosY(32, 0.24f).SetEase(Ease.InBack);
            closeButton.RectTransform().DOAnchorPosY(32, 0.24f).SetEase(Ease.InBack);

            globalFadeGroup.transform.DOScale(Vector3.zero, 0.3f).SetEase(Ease.InBack);
            globalFadeGroup.DOFade(0, 0.24f);
            backgroundFader.DOFade(0, 0.24f);
            backgroundFader.DOFade(0, 0.4f);

            await System.Threading.Tasks.Task.Delay(300);

            App.SaveAll();
            //Debug.LogError("CloseCollection()");

            await System.Threading.Tasks.Task.Delay(300);

            await SceneController.Instance.UnloadScene("Collection");
        }

        public void DisableScreen(bool shouldDisable)
        {
            disableCanvas.SetActive(shouldDisable);
            disableCanvas.interactable = shouldDisable;
            disableCanvas.blocksRaycasts = shouldDisable;
        }

        private IEnumerator<float> CanEscape()
        {
            yield return Timing.WaitForSeconds(0.5f);
            _canEscape = true;
        }

        private IEnumerator<float> AnimateNewHeaderIn(string newMenu)
        {
            menuHeaderRect.DOKill(true);

            AudioController.PlaySound(revealHeader);

            menuHeader.Play(newMenu);
            menuHeaderRect.localEulerAngles = new Vector3(-90, 0, 0);

            menuHeaderRect.DOLocalRotate(Vector3.zero, 1.7f).SetEase(Ease.OutElastic);
            menuHeaderRect.DOAnchorPosY(0, 0.24f).SetEase(Ease.OutQuart);
            yield break;
        }

        private IEnumerator<float> AnimateHeaderOut()
        {
            menuHeaderRect.DOKill(true);
            AudioController.PlaySound(removeHeader);
            menuHeaderRect.DOAnchorPosY(36f, 0.24f).SetEase(Ease.OutBack);
            yield return Timing.WaitForSeconds(0.24f);
        }

        private void OnMenuChanged(DropdownItemData data)
        {
            _settings.MenuMode = GameUtilities.ParseEnum(data.value, CollectionMenuMode.Collection);

            Debug.Log(data.value);
            switch (_settings.MenuMode)
            {
                case CollectionMenuMode.Collection:
                    _settings.FilterCardSettings.ownedCardsOnly = true;
                    break;
                case CollectionMenuMode.CardBacks:
                    _settings.FilterCardSettings.ownedCardsOnly = true;
                    break;
                case CollectionMenuMode.Relics:
                    _settings.FilterCardSettings.ownedCardsOnly = true;
                    break;
            }

            ChangeOpenSettings(_settings);
        }

        private void OnTabMenuChanged(bool isOn, string content)
        {
            _settings.MenuMode = GameUtilities.ParseEnum(content, CollectionMenuMode.Collection);

            var notificationVector = new Vector2(0, 5f);

            switch (_settings.MenuMode)
            {
                case CollectionMenuMode.Collection:

                    _settings.FilterCardSettings.ownedCardsOnly = true;

                    cardsTabButton.Toggle.SetIsOnWithoutNotify(true);
                    cardsTabButton.Toggle.interactable = false;
                    cardsTabNotification.RectTransform().anchoredPosition += notificationVector;

                    if (!relicsTabButton.Toggle.interactable)
                    {
                        relicsTabButton.Toggle.SetIsOnWithoutNotify(false);
                        relicsTabButton.Toggle.interactable = true;
                        relicsTabNotification.RectTransform().anchoredPosition -= notificationVector;
                    }

                    if (!cardBacksTabButton.Toggle.interactable)
                    {
                        cardBacksTabButton.Toggle.SetIsOnWithoutNotify(false);
                        cardBacksTabButton.Toggle.interactable = true;
                        cardBacksTabNotification.RectTransform().anchoredPosition -= notificationVector;
                    }

                    break;

                case CollectionMenuMode.Relics:

                    _settings.FilterCardSettings.ownedCardsOnly = true;

                    if (!cardsTabButton.Toggle.interactable)
                    {
                        cardsTabButton.Toggle.SetIsOnWithoutNotify(false);
                        cardsTabButton.Toggle.interactable = true;
                        cardsTabNotification.RectTransform().anchoredPosition -= notificationVector;
                    }

                    relicsTabButton.Toggle.SetIsOnWithoutNotify(true);
                    relicsTabButton.Toggle.interactable = false;
                    relicsTabNotification.RectTransform().anchoredPosition += notificationVector;

                    if (!cardBacksTabButton.Toggle.interactable)
                    {
                        cardBacksTabButton.Toggle.SetIsOnWithoutNotify(false);
                        cardBacksTabButton.Toggle.interactable = true;
                        cardBacksTabNotification.RectTransform().anchoredPosition -= notificationVector;
                    }

                    break;

                case CollectionMenuMode.CardBacks:

                    _settings.FilterCardSettings.ownedCardsOnly = true;

                    if (!cardsTabButton.Toggle.interactable)
                    {
                        cardsTabButton.Toggle.SetIsOnWithoutNotify(false);
                        cardsTabButton.Toggle.interactable = true;
                        cardsTabNotification.RectTransform().anchoredPosition -= notificationVector;
                    }

                    if (!relicsTabButton.Toggle.interactable)
                    {
                        relicsTabButton.Toggle.SetIsOnWithoutNotify(false);
                        relicsTabButton.Toggle.interactable = true;
                        relicsTabNotification.RectTransform().anchoredPosition -= notificationVector;
                    }

                    cardBacksTabButton.Toggle.SetIsOnWithoutNotify(true);
                    cardBacksTabButton.Toggle.interactable = false;
                    cardBacksTabNotification.RectTransform().anchoredPosition += notificationVector;

                    break;
            }

            RefreshNewCardsNotifications();

            ChangeOpenSettings(_settings);

        }

        private void InitializeNewCardsNotifications()
        {
            cardsTabNotification.SetActive(App.Inventory.HasAnyNewCardsExclusve());
            relicsTabNotification.SetActive(App.Inventory.HasAnyNewRelics());
            cardBacksTabNotification.SetActive(App.Inventory.HasAnyNewCardBacks());
        }

        private void RefreshNewCardsNotifications()
        {
            if (cardsTabNotification.activeSelf)
            {
                cardsTabNotification.SetActive(App.Inventory.HasAnyNewCardsExclusve());
            }
            if (relicsTabNotification.activeSelf)
            {
                relicsTabNotification.SetActive(App.Inventory.HasAnyNewRelics());
            }
            if (cardBacksTabNotification.activeSelf)
            {
                cardBacksTabNotification.SetActive(App.Inventory.HasAnyNewCardBacks());
            }
        }

        private void OnCloseButton()
        {
            equippedDeckID = App.PlayerDecks.GetDeckBasedOnGameState().uid;

            //Debug.Log(App.PlayerDecks);
            //Debug.Log(lastEditedDeckID);

            if ((equippedDeckID != lastEditedDeckID) &&  (App.PlayerDecks != null && lastEditedDeckID != "" ))
            {
                OnCloseButtonEquipDeck();
            }
            else
            {
                CloseCollection();
                OnCollectionClosed?.Invoke();
            }

        }

        private void OnCloseButtonEquipDeck()
        {
            PopupController.Open(new PopupInfo
            {
                style = PopupWindowStyle.Generic,
                title = "UPDATED DECK ISN'T EQUIPPED",
                body =
                "The deck you've most recently edited isn't <color=#F35C3B>equipped.</color> Would you like to equip it before exiting?",
                confirm = "EQUIP!",
                deny = "NO!"
            });

            PopupController.OnPopupChoice += OnEquipActionResponse;
        }

        private void OnEquipActionResponse(bool equip)
        {
            PopupController.OnPopupChoice -= OnEquipActionResponse;

            if (equip)
            {
                App.PlayerDecks.EquipDeck(App.PlayerDecks.GetContextBasedOnGameState(), lastEditedDeckID);
            }

            CloseCollection();
            OnCollectionClosed?.Invoke();
            //Timing.RunCoroutine(AnimateCloseCollection().CancelWith(gameObject));
        }

        private IEnumerator<float> AnimateCloseCollection()
        {
           // CloseCollection();
            OnCollectionClosed?.Invoke();
            yield return Timing.WaitForSeconds(.01f);
        }

        private void OnBackButton()
        {
            Debug.Log("ONBACK BUTTON");
            switch (_settings.OpenMode)
            {
                // case DeckOpenMode.Collection:
                // {
                //     OnCollectionClosed?.Invoke();
                //     CloseCollection();
                //     break;
                // }
                // case DeckOpenMode.DeckEditor:
                // {
                //     deckCollection.OnBackButton();
                //     break;
                // }
                case DeckOpenMode.DeckCreator_SelectHero:
                case DeckOpenMode.DeckCreator_SelectDeck:
                {
                    deckCreation.OnBackButton();
                    _canEscape = true;
                    break;
                }
                default:
                    OnCloseButton();
                    break;
            }
        }
    }
}