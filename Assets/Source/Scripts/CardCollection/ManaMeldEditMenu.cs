using System;
using System.Collections.Generic;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.CardCollection;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.InputAPI;
using CrossBlitz.UI.Widgets.Dropdown;
using CrossBlitz.ViewAPI;
using CrossBlitz.ViewAPI.Popups;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace CrossBlitz.DeckEditAPI
{

    public class ManaMeldEditMenu : View
    {
        public static ManaMeldEditMenu Instance;

        [BoxGroup("Menu")] public CanvasGroup globalFadeGroup;
        [BoxGroup("Menu")] public CanvasGroup disableCanvas;
        [BoxGroup("Menu")] public CanvasGroup backgroundFader;
        [BoxGroup("Menu")] public Button closeButton;
        [BoxGroup("Menu")] public Button closePreviewButton;

        [BoxGroup("Menu/Sub Menu")] public DeckCollectionMenu deckCollection;
        [BoxGroup("Menu/Sub Menu")] public MeldOverlay meldOverlay;
        [BoxGroup("Menu/Sub Menu")] public MassScrapSidePanel massScrapSidePanel;
        [BoxGroup("Menu/Sub Menu")] public CardView previouslySelectedCard;

        [BoxGroup("Filters")] public GameObject cardFilterContainer;

        [BoxGroup("Mawlder")] public SpriteAnimation mawlder;
        [BoxGroup("Mawlder")] public TextMeshProUGUI mawlderText;

        [BoxGroup("Animation")] public RectTransform menuHeaderRect;
        [BoxGroup("Animation")] public SpriteAnimation menuHeader;
        [BoxGroup("Sfx")] public string revealHeader = "UI-Battle_RevealCard";
        [BoxGroup("Sfx")] public string removeHeader = "UI-ButtonFlip";


        private DeckEditSettings _settings;
        private float m_nextTextDelay;
        private int m_currentTextIndex;
        private List<DialogueSnippetData> snippets;
        [HideInInspector] public bool _canEscape;
        public DeckEditSettings Settings => _settings;

        public event Action OnCollectionClosed;

        private void Awake()
        {
            globalFadeGroup.alpha = 0;
            _canEscape = false;

            var characterData = Db.CharacterDatabase.GetCharacter("Mawlder");

            snippets = characterData != null ?
                characterData.dialogueSnippets.FindAll(d => d.Option == "Shop/Enter") :
                new List<DialogueSnippetData>();

            mawlder.Play("melding-idle");
        }

        private void Update()
        {
            if (snippets.Count > 0)
            {
                m_nextTextDelay -= Time.deltaTime;

                if (m_nextTextDelay <= 0)
                {
                    m_nextTextDelay = 12;

                    mawlderText.text = snippets[m_currentTextIndex].Dialogue;

                    var previousSnippet = m_currentTextIndex;

                    while (previousSnippet == m_currentTextIndex)
                    {
                        m_currentTextIndex = Random.Range(1, snippets.Count);
                    }
                }
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (!PopupController.IsOpen && _canEscape && !meldOverlay.IsActive())
                {
                    _canEscape = false;
                    OnCloseButton();
                    return;
                }
            }
        }

        public override void Initialize(SceneModel sceneModel)
        {
            base.Initialize(sceneModel);

            Instance = this;
        }

        public async void OpenCollectionManagerWithSettings(DeckEditSettings _deckEditSettings)
        {
            Instance = this;
            closeButton.onClick.AddListener(OnCloseButton);
            closePreviewButton.onClick.AddListener(OnClosePreviewButton);

            closeButton.RectTransform().DOAnchorPosX(60f, 0.4f).SetEase(Ease.OutBack);

            Timing.RunCoroutine(AnimateNewHeaderIn("cards"));

            backgroundFader.alpha = 0;
            backgroundFader.DOFade(1, 0.4f);
            AudioController.PlaySong("mawlders-shop");

            await deckCollection.Init();

            await System.Threading.Tasks.Task.Delay(240);

            //MasterAudio.PlaySound("Hero_Appear-Whoosh");
            AudioController.PlaySound("Hero_Appear-Whoosh");

            globalFadeGroup.transform.DOPunchScale(Vector3.one * 0.25f, 0.4f);
            globalFadeGroup.DOFade(1, 0.3f);

            if (_deckEditSettings != null)
            {
                Open(_deckEditSettings);
            }

            Timing.RunCoroutine(CanEscape());
        }

        // this is where all the inits go, only happens once while this is open
        public override async void Open()
        {
            Timing.RunCoroutine(AnimateNewHeaderIn("cards"));

            backgroundFader.alpha = 0;
            backgroundFader.DOFade(1, 0.4f);

            closePreviewButton.interactable = false;

            await deckCollection.Init();

            var deckEditSettings = model.GetSceneParameter<DeckEditSettings>("DeckEditSettings");

            if (deckEditSettings != null)
            {
                Open(deckEditSettings);
            }

        }

        public void UpdatePreview(CardView cardView)
        {
            var recipeCard = cardView.GetComponent<ManaMeldRecipeComponent>();

            if (massScrapSidePanel.previewCard == null)
            {
                // Update the Preview Card
                massScrapSidePanel.cardLayout.DestroyChildren();
                massScrapSidePanel.UpdatePreview(cardView.data);

                // Turn on the selected card outline
                recipeCard.manaMeldCollectionRecipe.recipeSelectedContainer.SetActive(true);
                recipeCard.RectTransform().DOKill();
                recipeCard.RectTransform().DOAnchorPosY(4f + recipeCard.RectTransform().anchoredPosition.y, 0.25f).SetEase(Ease.OutBack);

                previouslySelectedCard = cardView;
            }
            else
            {
                if (massScrapSidePanel.previewCard.data.name == cardView.data.name)
                {
                    recipeCard.manaMeldCollectionRecipe.recipeSelectedContainer.SetActive(!recipeCard.manaMeldCollectionRecipe.recipeSelectedContainer.activeSelf);

                    if (!recipeCard.manaMeldCollectionRecipe.recipeSelectedContainer.activeSelf)
                    {
                        massScrapSidePanel.previewCard = null;
                        massScrapSidePanel.ResetPreview();

                        // Turn off the selected card outline
                        recipeCard.RectTransform().DOKill();
                        recipeCard.RectTransform().DOAnchorPosY(recipeCard.RectTransform().anchoredPosition.y-4f, 0.25f).SetEase(Ease.OutBack);

                        previouslySelectedCard = null;

                    }
                    else
                    {
                        massScrapSidePanel.UpdatePreview(cardView.data);

                        // Turn on the selected card outline
                        if (deckCollection.onMeldCard == false)
                        {
                            recipeCard.RectTransform().DOKill();
                            recipeCard.RectTransform().DOAnchorPosY(4f + recipeCard.RectTransform().anchoredPosition.y, 0.25f).SetEase(Ease.OutBack);
                        }
                        else
                        {
                            //cardView.TintThenUntintOverTime(Color.magenta, BlendMode.Overlay, 0.06f, 0.06f, 0.06f);
                            recipeCard.RectTransform().DOKill();
                            recipeCard.RectTransform().DOAnchorPosY(recipeCard.RectTransform().anchoredPosition.y, 0.25f).SetEase(Ease.OutBack);
                        }
                        previouslySelectedCard = cardView;
                    }
                }
                else
                {
                    // Turn off the previous selected card outline
                    var previewCardRecipe = previouslySelectedCard.GetComponent<ManaMeldRecipeComponent>();
                    previewCardRecipe.manaMeldCollectionRecipe.recipeSelectedContainer.SetActive(false);
                    previewCardRecipe.RectTransform().DOKill();
                    previewCardRecipe.RectTransform().DOAnchorPosY(previewCardRecipe.RectTransform().anchoredPosition.y - 4f, 0.25f).SetEase(Ease.OutBack);

                    // Turn on the selected card outline
                    recipeCard.manaMeldCollectionRecipe.recipeSelectedContainer.SetActive(true);
                    recipeCard.RectTransform().DOKill();
                    recipeCard.RectTransform().DOAnchorPosY(4f + recipeCard.RectTransform().anchoredPosition.y, 0.25f).SetEase(Ease.OutBack);

                    massScrapSidePanel.UpdatePreview(cardView.data);

                    previouslySelectedCard = cardView;
                }
            }



        }

        protected virtual void Open(DeckEditSettings settings)
        {
            _settings = settings;

            cardFilterContainer.SetActive(true);
            OpenManaMeld(_settings);
        }

        public void ChangeOpenSettings(DeckEditSettings settings)
        {
            Open(settings);
        }


        private void OpenManaMeld(DeckEditSettings settings)
        {
            deckCollection.SetActive(true);
            deckCollection.Open(settings);
            massScrapSidePanel.OpenIngredients();
        }

        private void OnClosePreviewButton()
        {
            closePreviewButton.interactable = false;

            massScrapSidePanel.previewCard = null;
            massScrapSidePanel.ResetPreview();

            // Turn off the selected card outline
            var recipeCard = previouslySelectedCard.GetComponent<ManaMeldRecipeComponent>();
            recipeCard.RectTransform().DOKill();
            if (recipeCard.manaMeldCollectionRecipe.recipeSelectedContainer.activeSelf)
            {
                recipeCard.RectTransform().DOAnchorPosY(recipeCard.RectTransform().anchoredPosition.y - 4f, 0.25f).SetEase(Ease.OutBack);
            }
            recipeCard.manaMeldCollectionRecipe.recipeSelectedContainer.SetActive(false);


            previouslySelectedCard = null;
        }

        private async void CloseCollection()
        {
           // Debug.Log("CLOSING COLLECTION A");

            if(FablesInput.Instance != null) FablesInput.Instance.deckViewInput.RefreshNewCardsLabel();

           // MasterAudio.PlaySound("Hero_Appear-Whoosh");
            AudioController.PlaySound("Hero_Appear-Whoosh");

            Timing.RunCoroutine(AnimateHeaderOut());

            DisableScreen(true);

            closeButton.RectTransform().DOAnchorPosX(-60f, 0.4f).SetEase(Ease.InBack);

            globalFadeGroup.transform.DOScale(Vector3.zero, 0.3f).SetEase(Ease.InBack);
            globalFadeGroup.DOFade(0, 0.24f);
            backgroundFader.DOFade(0, 0.24f);
            backgroundFader.DOFade(0, 0.4f);

            await System.Threading.Tasks.Task.Delay(400);
            await SceneController.Instance.UnloadScene("Collection");
        }

        public void DisableScreen(bool shouldDisable)
        {
            disableCanvas.SetActive(shouldDisable);
            disableCanvas.interactable = shouldDisable;
            disableCanvas.blocksRaycasts = shouldDisable;
        }

        private IEnumerator<float> CanEscape()
        {
            yield return Timing.WaitForSeconds(0.5f);
            _canEscape = true;
        }

        private IEnumerator<float> AnimateNewHeaderIn(string newMenu)
        {
            menuHeaderRect.DOKill(true);

           // MasterAudio.PlaySound(revealHeader);
            AudioController.PlaySound(revealHeader);

            //menuHeader.Play(newMenu);
            menuHeaderRect.localEulerAngles = new Vector3(-90, 0, 0);

            menuHeaderRect.DOLocalRotate(Vector3.zero, 1.7f).SetEase(Ease.OutElastic);
            menuHeaderRect.DOAnchorPosY(0, 0.24f).SetEase(Ease.OutQuart);
            yield break;
        }

        private IEnumerator<float> AnimateHeaderOut()
        {
            menuHeaderRect.DOKill(true);
           // MasterAudio.PlaySound(removeHeader);
            AudioController.PlaySound(removeHeader);
            menuHeaderRect.DOAnchorPosY(36f, 0.24f).SetEase(Ease.OutBack);
            yield return Timing.WaitForSeconds(0.24f);
        }

        // THIS IS A STARTING SETTING THAT IS CHANGED WHEN CYCLING BETWEEN MODES
        private void OnMenuChanged(DropdownItemData data)
        {
            _settings.MenuMode = GameUtilities.ParseEnum(data.value, CollectionMenuMode.Collection);

            switch (_settings.MenuMode)
            {
                case CollectionMenuMode.Collection:
                    _settings.FilterCardSettings.ownedCardsOnly = true;
                    break;
                case CollectionMenuMode.CardBacks:
                    _settings.FilterCardSettings.ownedCardsOnly = true;
                    break;
                case CollectionMenuMode.ManaMeld:
                    _settings.FilterCardSettings.ownedCardsOnly = false;
                    break;
            }

            ChangeOpenSettings(_settings);
        }


        private async void OnCloseButton()
        {
            CloseCollection();
            OnCollectionClosed?.Invoke();
        }

    }
}