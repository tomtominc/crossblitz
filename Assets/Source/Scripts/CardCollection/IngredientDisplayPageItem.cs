using System.Collections.Generic;
using CrossBlitz.Universal;
using CrossBlitz.Utils;
using DG.Tweening;
using UnityEngine;

namespace CrossBlitz.CardCollection
{
    public class IngredientDisplayPageItem : PageItem
    {
        public Vector2 animateFromOffset = new Vector2(20, 0);
        public CanvasGroup canvasGroup;
        public IngredientDisplay ingredientDisplay;

        public override void SetAsEmpty(int itemIndex)
        {
            canvasGroup.alpha = 0;
        }

        public override void SetData(IPageItemData data, int itemIndex)
        {
            canvasGroup.DOKill();
            ingredientDisplay.RectTransform().DOKill();

            canvasGroup.alpha = 0;

            if (data is IngredientInventoryPageData ingData)
            {
                ingredientDisplay.SetItemId(ingData.ingredientId, ingData.hasCustomAmount, ingData.customAmount, ingData.isManaShard);
            }

            ingredientDisplay.RectTransform().anchoredPosition = animateFromOffset;
            canvasGroup.DOFade(1, 0.12f).SetDelay(itemIndex * 0.06f);
            ingredientDisplay.RectTransform().DOAnchorPos(Vector2.zero, 0.24f).SetEase(Ease.OutBack).SetDelay(itemIndex * 0.06f);
        }

        public void HideData (int itemIndex)
        {
            canvasGroup.DOKill();
            ingredientDisplay.RectTransform().DOKill();
            //ingredientDisplay.RectTransform().anchoredPosition = animateFromOffset;
            canvasGroup.DOFade(0, 0.12f).SetDelay(itemIndex * 0.06f);
            ingredientDisplay.RectTransform().DOAnchorPos(animateFromOffset, 0.24f).SetEase(Ease.OutBack).SetDelay(itemIndex * 0.06f);
        }

        public void ShowData(int itemIndex)
        {
            canvasGroup.DOKill();
            ingredientDisplay.RectTransform().DOKill();
           // ingredientDisplay.RectTransform().anchoredPosition = animateFromOffset;
            canvasGroup.DOFade(1, 0.12f).SetDelay(itemIndex * 0.06f);
            ingredientDisplay.RectTransform().DOAnchorPos(Vector2.zero, 0.24f).SetEase(Ease.OutBack).SetDelay(itemIndex * 0.06f);
        }

    }

    public class IngredientInventoryPageData : IPageItemData
    {
        public string ingredientId;

        public bool hasCustomAmount;
        public int customAmount;
        public bool isManaShard;
    }
}