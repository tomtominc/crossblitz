using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Hero;
using CrossBlitz.PlayFab.Authentication;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.DeckEditAPI
{
    [RequireComponent(typeof(ToggleGroup))]
    public class HeroToggleFilter : MonoBehaviour
    {
        public bool displayInReverse;
        public RectTransform contentLayout;
        public HeroToggleButton heroToggleButtonPrefab;

        [BoxGroup("Faction Filter")] public bool filterableByFaction;
        [ShowIf("filterableByFaction")][BoxGroup("Faction Filter")] public FactionToggleFilter factionFilter;

        [BoxGroup("Page Properties")] public bool usePageProperties;
        [ShowIf("usePageProperties")][BoxGroup("Page Properties")] public int heroesPerPage = -1;
        [ShowIf("usePageProperties")][BoxGroup("Page Properties")] public Button previousArrow;
        [ShowIf("usePageProperties")][BoxGroup("Page Properties")] public Button nextArrow;

        private List<HeroToggleButton> _heroToggleButtons;
        private ToggleGroup _toggleGroup;
        private List<HeroData> _heroes;
        private List<HeroData> _currentHeroes;

        // pages
        private int _currentPage;
        private int _maxPages;

        public List<HeroToggleButton> HeroToggles => _heroToggleButtons;

        //public event Action<bool, string> OnHeroToggle;

        public void Init()
        {
            contentLayout.DestroyChildren();

            _toggleGroup = GetComponent<ToggleGroup>();
            _heroToggleButtons=new List<HeroToggleButton>();
            _heroes = new List<HeroData>( Db.HeroDatabase.Heroes );

            if (previousArrow)
            {
                previousArrow.onClick.RemoveListener(OnPreviousButton);
                previousArrow.onClick.AddListener(OnPreviousButton);
            }

            if (nextArrow)
            {
                nextArrow.onClick.RemoveListener(OnNextButton);
                nextArrow.onClick.AddListener(OnNextButton);
            }

            if (usePageProperties)
            {
                _currentPage = 0;
                SetupPages(_heroes);
                ShowCurrentPage();
            }
            else
            {
                ShowHeroes(_heroes);
            }


            if (filterableByFaction && factionFilter)
            {
                factionFilter.OnFactionToggle -= OnFactionsToggled;
                factionFilter.OnFactionToggle += OnFactionsToggled;
            }
        }

        public void OnDisable()
        {
            if (factionFilter)
            {
                factionFilter.OnFactionToggle -= OnFactionsToggled;
            }

            if (previousArrow)
            {
                previousArrow.onClick.RemoveListener(OnPreviousButton);
            }

            if (nextArrow)
            {
                nextArrow.onClick.RemoveListener(OnNextButton);
            }

            Debug.Log("DeckToggleFilter (OnDisable)");
        }

        private void HeroToggled(bool isOn, string content)
        {
            if (isOn) AudioController.PlaySound("TMP-UI-ClickGrab");//MasterAudio.PlaySound("UI-Selection");

            Events.Publish(this, EventType.OnHeroChanged, Db.HeroDatabase.GetHero(content));
        }

        public void UpdateTogglesToBeAccurate(string content)
        {
            var heroToggle = _heroToggleButtons.Find(hero => hero.heroView.HeroData.id == content);

            if (heroToggle != null && heroToggle.Toggle.isOn == false)
            {
                heroToggle.Toggle.SetIsOnWithoutNotify(true);
            }
        }

        private void OnFactionsToggled(bool isOn, string content)
        {
            if (isOn)
            {
                var faction = GameUtilities.ParseEnum(content, Faction.All);

                if (usePageProperties)
                {
                    if (faction == Faction.All)
                    {
                        SetupPages(_heroes);
                    }
                    else
                    {
                        SetupPages(_heroes.FindAll(hero => hero.faction == faction));
                    }

                    ShowCurrentPage();
                }
                else
                {
                    if (faction == Faction.All)
                    {
                        ShowHeroes(_heroes);
                    }
                    else
                    {
                        ShowHeroes(_heroes.FindAll(hero => hero.faction == faction));
                    }
                }
            }
        }

        private void SetupPages(List<HeroData> heroes)
        {
            _currentHeroes = heroes;
            _maxPages = _currentHeroes.Count / heroesPerPage + ( _currentHeroes.Count % heroesPerPage == 0 ? 0 : 1);
            _currentPage = Mathf.Clamp(_currentPage, 0, _maxPages-1);
        }

        private void ShowCurrentPage()
        {
            UpdateArrowButtons();

            var startingIndex = _currentPage * heroesPerPage;
            var endingIndex = startingIndex + heroesPerPage;
            var showingHeroes = new List<HeroData>();

            for (var i = startingIndex; i < endingIndex; i++)
            {
                if (i >= _currentHeroes.Count) break;

                showingHeroes.Add(_currentHeroes[i]);
            }

            ShowHeroes(showingHeroes);
        }

        private void UpdateArrowButtons()
        {
            previousArrow.SetActive(_currentPage > 0);
            nextArrow.SetActive(_currentPage < _maxPages-1);
        }

        private void OnPreviousButton()
        {
            _currentPage--;
            ShowCurrentPage();
        }

        private void OnNextButton()
        {
            _currentPage++;
            ShowCurrentPage();
        }

        private void ShowHeroes( List<HeroData> heroes)
        {
            if (heroes.Count <= 0)
            {
                Debug.LogError("No heroes passing through here!!");
                return;
            }

            var showCount = !usePageProperties ? heroes.Count : heroesPerPage <= 0 ? heroes.Count : heroesPerPage;
            showCount = Mathf.Clamp(showCount, 1, heroes.Count);

            for (var i = 0; i < _heroToggleButtons.Count; i++)
            {
                _heroToggleButtons[i].Toggle.isOn = false;
                _heroToggleButtons[i].SetActive(false);
            }

            HeroToggled(true, App.HeroData.GetCurrentHero() );

            // This is to make sure that in the next page there's no current hero
            //OnHeroToggle?.Invoke(false, GDEItemKeys.Hero_Redcroft);

            if (displayInReverse)
            {
                heroes.Reverse();
            }

            for (var i = 0; i < showCount; i++)
            {
                if (_heroToggleButtons.Count <= i)
                {
                    var factionToggleButton = Instantiate(heroToggleButtonPrefab, contentLayout, false);
                    factionToggleButton.OnValueChanged += HeroToggled;
                    factionToggleButton.Toggle.group = _toggleGroup;
                    _heroToggleButtons.Add(factionToggleButton);
                }

                _heroToggleButtons[i].SetHero(heroes[i]);
                _heroToggleButtons[i].Toggle.isOn = false;
                _heroToggleButtons[i].SetActive(true);
            }

            if (displayInReverse)
            {
                _heroToggleButtons[_heroToggleButtons.Count - 1].Toggle.isOn = true;
            }
        }
    }
}