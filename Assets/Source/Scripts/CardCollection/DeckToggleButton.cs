using System;
using CrossBlitz.Deck;
using CrossBlitz.UI.Widgets;
using DG.Tweening;
using Newtonsoft.Json;
using UnityEngine;

namespace CrossBlitz.DeckEditAPI
{
    public class DeckToggleButton : ToggleButton
    {
        public RectTransform container;
        public DeckView deckView;
        public CanvasGroup hitEffect;

        private bool _isOn;
        private void Start()
        {
            if (hitEffect)
            {
                hitEffect.alpha = 0;
            }
        }

        protected override void ToggleValueChanged(bool isOn)
        {
            base.ToggleValueChanged(isOn);

            DOTween.Kill(container,true);
            DOTween.Kill(hitEffect, true);

            if (isOn && !_isOn)
            {
                container.DOPunchScale(new Vector3(0.125f,-0.125f, 0), 0.5f);
                container.DOAnchorPosY(3, 0.25f)
                    .SetEase(Ease.OutBack);

                if (hitEffect)
                {
                    hitEffect.alpha = 1;
                    hitEffect.DOFade(0, 0.25f);
                }
            }
            else if (isOn && _isOn)
            {
                container.DOPunchAnchorPos(Vector2.down, 0.5f);
            }
            else
            {
                container.DOAnchorPosY(0, 0.4f)
                    .SetEase(Ease.OutBounce);
            }

            _isOn = isOn;
        }

        public virtual void SetDeck(DeckData deckData)
        {
            content = deckData.ToJson(true);
            deckView.SetDeck(deckData);
        }

        public virtual void SetDeckAsCustom(DeckData deckData)
        {
            content = deckData.ToJson(true);
            deckView.SetDeckAsCustom(deckData);
        }

        public virtual void SetDeckAsWild(DeckData deckData)
        {
            content = deckData.ToJson(true);
            deckView.SetDeckAsWild(deckData);
        }

        private void OnDestroy()
        {
            DOTween.Kill(container);
            DOTween.Kill(hitEffect);
        }
    }
}