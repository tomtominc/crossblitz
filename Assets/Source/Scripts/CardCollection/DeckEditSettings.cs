using CrossBlitz.Card;
using CrossBlitz.PlayFab.Data;

namespace CrossBlitz.DeckEditAPI
{
    public enum DeckOpenMode
    {
        Collection,
        DeckCreator_SelectHero,
        DeckCreator_SelectDeck,
        DeckEditor,
    }
    public enum DeckEditMode
    {
        Edit,
    }

    public enum CollectionMenuMode
    {
        Collection,
        ManaMeld,
        CardBacks,
        Relics
    }

    [System.Serializable]
    public class DeckEditSettings
    {
        public DeckEditMode Mode;
        public CollectionMenuMode MenuMode;
        public DeckOpenMode OpenMode;
        public DeckEquipContext EquipContext;
        public FilterCardSettings FilterCardSettings;
        public DeckSettings DeckSettings;

        // This is used for select deck open mode.
        public string CurrentHero;
    }

    [System.Serializable]
    public class FilterCardSettings
    {
        public bool ownedCardsOnly;
        public Faction forcedFaction;
        public Faction faction;
        public Class @class;
        public Rarity rarity;
        public TypeFilter typeFilter;
        public KeywordFilter keywordFilter;
        public RelicFilter relicFilter;
        public CardBackFilter cardBackFilter;
        public CardSet cardSet;
        public string searchFilter;

        // mana meld only
        public bool isCraftable;
        public bool onlyNotOwned;

        public static FilterCardSettings Default()
        {
            return new FilterCardSettings
            {
                forcedFaction = Faction.All,
                faction = Faction.All,
                @class = Class.All,
                rarity = Rarity.All,
                typeFilter = TypeFilter.All,
                keywordFilter = KeywordFilter.All,
                relicFilter = RelicFilter.All,
                cardBackFilter = CardBackFilter.All,
                cardSet = CardSet.All,
                searchFilter = string.Empty
            };
        }

        public bool IsDefault()
        {
            return faction == Faction.All &&
                   @class == Class.All &&
                   rarity == Rarity.All &&
                   typeFilter == TypeFilter.All &&
                   keywordFilter == KeywordFilter.All &&
                   relicFilter == RelicFilter.All &&
                   cardBackFilter == CardBackFilter.All &&
                   cardSet == CardSet.All && 
                   searchFilter == string.Empty;
        }

        public void SetAsDefault()
        {
            faction = Faction.All;
            @class = Class.All;
            rarity = Rarity.All;
            typeFilter = TypeFilter.All;
            keywordFilter = KeywordFilter.All;
            relicFilter = RelicFilter.All;
            cardBackFilter = CardBackFilter.All;
            cardSet = CardSet.All;
            searchFilter = string.Empty;
        }

        public void TransferSettings(FilterCardSettings copy)
        {
            copy.faction = faction;
            copy.@class = @class;
            copy.rarity = rarity;
            copy.typeFilter = typeFilter;
            copy.keywordFilter = keywordFilter;
            copy.relicFilter = relicFilter;
            copy.cardBackFilter = cardBackFilter;
            copy.cardSet = cardSet;
            copy.searchFilter = copy.searchFilter;
        }
    }

    [System.Serializable]
    public class DeckSettings
    {
        public DeckData deckData;
    }

    [System.Serializable]
    public class DeckViewerSettings
    {
        public bool IsShownOnLoad;
    }
}