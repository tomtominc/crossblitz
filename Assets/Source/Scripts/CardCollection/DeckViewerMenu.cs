using CrossBlitz.AssetManagement;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI.GameLogic;
using DG.Tweening;
using UnityEngine;

namespace CrossBlitz.CardCollection
{
    public class DeckViewerMenu : MonoBehaviour
    {
        public enum ViewType
        {
            SidePanel
        }

        /// <summary>
        /// The main canvas for this menu
        /// </summary>
        public Canvas canvas;
        /// <summary>
        /// Use this enum to change the view type for other behaviours
        /// </summary>
        public ViewType viewType;
        /// <summary>
        /// The card list, this is also used in the deck collection editor
        /// </summary>
        public DeckCardList cardList;
        /// <summary>
        /// The container used for opening and closing the deck viewer
        /// </summary>
        public RectTransform container;
        /// <summary>
        /// The x offset the container will be at when opened.
        /// </summary>
        public float openXOffset=130;

        public async void Init()
        {
            switch (viewType)
            {
                case ViewType.SidePanel: InitSidePanel();
                    break;
            }

            var deckViewPrefab = await AddressableReferenceLoader.GetAsset("Card_DeckView", true);
            cardList.Initialize(deckViewPrefab,null);
        }

        private void InitSidePanel()
        {
            canvas.enabled = false;
            container.anchoredPosition= new Vector2(-container.sizeDelta.x, container.anchoredPosition.y);
        }

        public void Open()
        {
            switch (viewType)
            {
                case ViewType.SidePanel: OpenSidePanel();
                    break;
            }
        }

        private void OpenSidePanel()
        {
            container.DOKill();

            canvas.enabled = true;
            container.DOAnchorPosX(openXOffset, 0.8f)
                .SetEase(Ease.InOutQuint);

            AudioController.PlaySound("Battle_GenericAttack-Charge");

            if (GameServer.IsRunning())
            {
                var player = GameServer.GetPlayerState(Team.Client);
                var deck = GameUtilities.Copy( App.PlayerDecks.GetDeckBasedOnGameState() );
                var gameDeck = player.GetDeckCardsById();

                for (var i = 0; i < deck.cards.Count; i++)
                {
                    var cardValue = gameDeck.Find(c => c.id == deck.cards[i].id);

                    if (cardValue != null)
                    {
                        deck.cards[i].count = cardValue.count;
                    }
                    else
                    {
                        deck.cards[i].count = 0;
                    }
                }

                for (var i = 0; i < gameDeck.Count; i++)
                {
                    var cardValue = deck.cards.Find(c => c.id == gameDeck[i].id);

                    if (cardValue == null)
                    {
                        deck.cards.Add(new CardValue
                        {
                            id = gameDeck[i].id,
                            count = gameDeck[i].count
                        });
                    }
                }

                cardList.OpenWithDeck(deck, 0);
            }
            else
            {
                cardList.OpenWithDeck(App.PlayerDecks.GetDeckBasedOnGameState(), 0);
            }

        }

        public void Close()
        {
            switch (viewType)
            {
                case ViewType.SidePanel: CloseSidePanel();
                    break;
            }
        }

        private void CloseSidePanel()
        {
            AudioController.PlaySound("Battle_SlashingAttack01");
            container.DOKill();
            container.DOAnchorPosX(-container.sizeDelta.x, 0.4f)
                .SetEase(Ease.InOutQuint).OnComplete(() =>
                {
                    canvas.enabled = false;
                });
        }
    }
}