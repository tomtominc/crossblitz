using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BlendModes;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Deck;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.PlayFab.Authentication;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace CrossBlitz.CardCollection
{
    public class DeckCardList : MonoBehaviour,
        IDropHandler,
        IPointerEnterHandler,
        IPointerExitHandler
    {
        public static bool IsPointerOverCardList;

        public bool isViewOnly;

        public bool forceHoverCardToolTipsRight;
        public bool forceHoverCardToolTipsLeft;

        [HideIf("isViewOnly")]
        public DecksListMenu deckList;
        public RectTransform layout;
        public CanvasGroup cardLayoutCanvas;
        public ScrollRect scrollRect;
        public DeckViewItem deckViewHeader;
        public ShakeContainer menuShakeContainer;
        public TextMeshProUGUI cardsCount;
        public TextMeshProUGUI cardsCountLabel;
        public GameObject deckFullOverlay;
        public GameObject headerShadow;
        public Button createDeckButton;
        public Button swapDeckButton;
        public Button finishButton;
        public GameObject finishedDisabledOverlay;
        public GameObject CardsBG;
        public GameObject RelicsBG;
        public GameObject DummyRelicPrefab;
        public GameObject MissingCardsObject;

        [HideIf("isViewOnly")]
        public DeckControlsPopout deckControlsPopout;
        public RectTransform deckFooterPanel;

        private GameObject _cardItemPrefab;
        private List<CardDeckViewItem> _cardViews;
        private DragCardDeckEditor _draggableCard;
        private DeckData m_currentDeckData;
        private bool m_relicsFilter;
        private bool m_hasDummyRelic = false;
        private GameObject m_currentDummyRelic = null;
        private int m_cardViewIndex;

        private Color color;
        private BlendMode blend;

        private Coroutine _openWithDeckRoutine;
        public DeckViewItem FromDeckView { get; set; }
        public event Action OnFinishedEditingDeck;
        public event Action<DeckData> OnDeckSelected;
        public event Action OnDeckChanged;

        /// <summary>
        /// Initializes the card list. Do this when you start a scene if the card list is used here so it can load its data
        /// before its needed. To actually display cards in the card list use "OpenWithDeck"
        /// </summary>
        /// <param name="cardItemPrefab">The prefab to use when adding them to your deck..</param>
        /// <param name="draggableCard">Draggable card is the card single card that is always dragged around inside the deck editor. When initializing this for only viewing you can pass null for this.</param>
        public void Initialize(GameObject cardItemPrefab, DragCardDeckEditor draggableCard)
        {
            _cardItemPrefab = cardItemPrefab;
            _cardViews=new List<CardDeckViewItem>();
            _draggableCard = draggableCard;
            if(_draggableCard != null) _draggableCard.SetDeckCardList(this);

            color = new Color(255f/255f, 148f/255f, 251f/255f, 150f/255f);
            blend = new BlendMode();
            blend = BlendMode.Overlay;

            headerShadow.SetActive(false);

            m_hasDummyRelic = false;

            if (finishButton) finishButton.onClick.AddListener(OnFinishButton);
            if (swapDeckButton) swapDeckButton.onClick.AddListener(OnFinishButton);
            if (createDeckButton) createDeckButton.onClick.AddListener(OnFinishButton);

            if (finishedDisabledOverlay)
            {
                finishedDisabledOverlay.SetActive(false);
            }

            deckViewHeader.OnDeckSelected += OnDeckHeaderPressed;

            if (deckControlsPopout)
            {
                deckControlsPopout.OnDeckDeleted += OnDeckDeleted;
                deckControlsPopout.OnClosed += OnDeckControlsClosed;
                deckControlsPopout.Init();
            }
        }

        public void ClearDeck(string header, Faction faction, int showButton)
        {
            m_currentDeckData = null;
            deckViewHeader.Clear(header, faction);
            layout.DestroyChildren();
            FormatCardCount();
            ShowButton(showButton);
            EnableButton(false);
        }

        public void OpenWithDeck(DeckData deckData, int showButton, bool onlyShowDeckHeader = false, bool recipeDeck = false)
        {
            deckViewHeader.canvasGroup.alpha = 1;
            m_currentDeckData = deckData;
            ShowButton(showButton);

            if (_openWithDeckRoutine != null)
            {
                StopCoroutine(_openWithDeckRoutine);
            }

            if (onlyShowDeckHeader)
            {
                EnableButton(false);
                deckViewHeader.SetDeck(m_currentDeckData);
            }
            else
            {
                EnableButton(true);

                if(recipeDeck) _openWithDeckRoutine = StartCoroutine(OpenWithDeckRecipeRoutine(deckData));
                else _openWithDeckRoutine = StartCoroutine(OpenWithDeckRoutine(deckData));
            }
        }

        private void ShowButton(int index)
        {
            if (finishButton) finishButton.SetActive(false);
            if (createDeckButton) createDeckButton.SetActive(false);
            if (swapDeckButton) swapDeckButton.SetActive(false);

            if (index == 0 && finishButton) finishButton.SetActive(true);
            if (index == 1 && createDeckButton) createDeckButton.SetActive(true);
            if (index == 2 && swapDeckButton) swapDeckButton.SetActive(true);
        }

        private void EnableButton(bool e)
        {
            if (finishButton) finishButton.interactable = e;
            if (createDeckButton) createDeckButton.interactable = e;
            if (swapDeckButton) swapDeckButton.interactable = e;
        }

        private IEnumerator OpenWithDeckRoutine(DeckData deckData)
        {
            layout.DestroyChildren();

            m_currentDeckData = deckData;
            DeckChanged();
            headerShadow.SetActive(true);
            deckViewHeader.SetDeck(m_currentDeckData);
            cardLayoutCanvas.alpha = 0;
            deckViewHeader.canvasGroup.alpha = 0;
            cardLayoutCanvas.DOFade(1, 0.20f);
            deckViewHeader.canvasGroup.DOFade(1, 0.20f);
            m_hasDummyRelic = false;

            if (DeckEditMenu.Instance != null)
            {
                if (DeckEditMenu.Instance.Settings.MenuMode == CollectionMenuMode.Relics)
                {
                    m_relicsFilter = true;
                    scrollRect.vertical = false;
                    RelicsBG.SetActive(true);
                    CardsBG.SetActive(false);
                }
                else
                {
                    m_relicsFilter = false;
                    scrollRect.vertical = true;
                    RelicsBG.SetActive(false);
                    CardsBG.SetActive(true);
                }
            }
            else
            {
                m_relicsFilter = false;
            }

            _cardViews =new List<CardDeckViewItem>();

            var cardList = new List<CardData>();

            for (var i = 0; i < m_currentDeckData.cards.Count; i++)
            {
                if (m_currentDeckData.cards[i].count == 0)
                {

                    var cardData = Db.CardDatabase.GetCard(m_currentDeckData.cards[i].id);

                    if (cardData == null) continue;

                    if (m_relicsFilter)
                    {
                        if (cardData.type == CardType.Relic || cardData.type == CardType.Elder_Relic) cardList.Add(cardData);
                    }
                    else
                    {
                        if (cardData.type != CardType.Relic && cardData.type != CardType.Elder_Relic) cardList.Add(cardData);
                    }
                    continue;
                }

                for (var j = 0; j < m_currentDeckData.cards[i].count; j++)
                {
                    var cardData = Db.CardDatabase.GetCard(m_currentDeckData.cards[i].id);

                    if (cardData == null) continue;

                    if (m_relicsFilter)
                    {
                        if (cardData.type == CardType.Relic || cardData.type == CardType.Elder_Relic) cardList.Add(cardData);
                    }
                    else
                    {
                        if (cardData.type != CardType.Relic && cardData.type != CardType.Elder_Relic) cardList.Add(cardData);
                    }
                }
            }

            SortCards();

            CardData last = null;

            for (var i = 0; i < cardList.Count; i++)
            {
                if (cardList[i] == null)
                {
                    yield break;
                }

                if (last == null || last.id != cardList[i].id)
                {
                    AddCard(cardList[i], true,true,false);
                }
                else
                {
                    AddCard(cardList[i], false, false, false);
                }

                last = cardList[i];
            }

            // Add Dummy Relic
            var currentElderRelic = m_currentDeckData.GetElderRelic();

            if(last != null)
            {
                if ((last.type == CardType.Elder_Relic || last.type == CardType.Relic))
                {
                    m_currentDummyRelic = Instantiate(DummyRelicPrefab, layout, false);
                    m_currentDummyRelic.transform.SetAsFirstSibling();

                    if (currentElderRelic == null)
                    {
                        if (m_currentDummyRelic == null) m_currentDummyRelic = Instantiate(DummyRelicPrefab, layout, false);

                        m_currentDummyRelic.SetActive(true);
                        m_currentDummyRelic.transform.SetAsFirstSibling();
                        m_hasDummyRelic = true;
                    }
                    else
                    {
                        m_currentDummyRelic.SetActive(false);
                        m_currentDummyRelic.transform.SetAsFirstSibling();
                        m_hasDummyRelic = false;
                    }
                }
            }
            Debug.Log(m_hasDummyRelic);
        }

        private IEnumerator OpenWithDeckRecipeRoutine(DeckData deckData)
        {
            layout.DestroyChildren();

            m_currentDeckData = deckData;
            DeckChanged();
            headerShadow.SetActive(true);
            deckViewHeader.SetDeck(m_currentDeckData);
            cardLayoutCanvas.alpha = 0;
            deckViewHeader.canvasGroup.alpha = 0;
            cardLayoutCanvas.DOFade(1, 0.20f);
            deckViewHeader.canvasGroup.DOFade(1, 0.20f);
            m_hasDummyRelic = false;

            if (DeckEditMenu.Instance != null)
            {
                m_relicsFilter = false;
                scrollRect.vertical = true;
                RelicsBG.SetActive(false);
                CardsBG.SetActive(true);
             }

            _cardViews = new List<CardDeckViewItem>();
            m_cardViewIndex = -1;

            // Get Owned Cards
            var ownedCardList = new List<CardData>();
            var missingCardList = new List<CardData>();
            for (var i = 0; i < m_currentDeckData.cards.Count; i++)
            {
                var cardData = Db.CardDatabase.GetCard(m_currentDeckData.cards[i].id);

                if (cardData == null) continue;

                var cardOwnedAmount = App.Inventory.GetItemAmount(cardData.id);
                var cardNeededAmount = m_currentDeckData.cards[i].count;
                var cardMissingAmount = 0;

                if (cardOwnedAmount >= cardNeededAmount) cardOwnedAmount = cardNeededAmount;
                else cardMissingAmount = cardNeededAmount - cardOwnedAmount;

                for (var j = 0; j < cardOwnedAmount; j++)
                {
                    ownedCardList.Add(cardData);
                }

                for (var y = 0; y < cardMissingAmount; y++)
                {
                    missingCardList.Add(cardData);
                }
            }

            // cardList = cardList
            //     .OrderBy(card => card.type != CardType.Power)
            //     .ThenBy(card => card.cost)
            //     .ThenBy(card => card.name)
            //     .ToList();

            SortCards();

            for (var c = 0; c < ownedCardList.Count; c++)
            {
                if (ownedCardList[c] == null)
                {
                    yield break;
                }
                AddCard(ownedCardList[c], false, false, false, true, false);
            }

            if(missingCardList.Count >0)
            {
                Instantiate(MissingCardsObject, layout, false);

                for (var d = 0; d < missingCardList.Count; d++)
                {
                    if (missingCardList[d] == null)
                    {
                        yield break;
                    }
                    AddCard(missingCardList[d], false, false, false, true, true);
                }
            }
        }

        private void AddCard(CardData cardData, bool sortCards = true, bool shakeScreen = true, bool spawnEffect = true, bool deckRecipe = false, bool missingCards = false)
        {
            var duplicateCard = _cardViews.Find(card => card.CardData.id == cardData.id);
            var cardValue = m_currentDeckData.GetCardValue(cardData.id);


            //If creating a deck recipe
            var cardOwnedAmount = 0;
            var cardNeededAmount = 0;
            var cardMissingAmount = 0;
            var deckRecipieCount = 0;

            var trackDeckIndex = true;

            if (deckRecipe)
            {
                cardOwnedAmount = App.Inventory.GetItemAmount(cardData.id);
                cardNeededAmount = m_currentDeckData.cards.Find(card => card.id == cardData.id).count;

                if (cardOwnedAmount >= cardNeededAmount) cardOwnedAmount = cardNeededAmount;
                else cardMissingAmount = cardNeededAmount - cardOwnedAmount;

                deckRecipieCount = missingCards ? cardMissingAmount : cardOwnedAmount;

                if (missingCards)
                {
                    trackDeckIndex = false;

                    // Get all instances of the card data in the list
                    var allDuplicates = _cardViews.FindAll(card => card.CardData.id == cardData.id);

                    if (!allDuplicates.IsNullOrEmpty())
                    {
                        var firstDuplicate = allDuplicates[0];
                        var firstDuplicateIndex = _cardViews.IndexOf(firstDuplicate);

                        if (allDuplicates.Count == 1 && firstDuplicateIndex <= m_cardViewIndex)
                        {
                            duplicateCard = null;
                        }
                        else if (allDuplicates.Count == 2)
                        {
                            duplicateCard = allDuplicates[1];
                        }
                    }
                    else
                    {
                        duplicateCard = null;
                    }
                }
            }

            if (duplicateCard != null)
            {
                duplicateCard.AnimateDoubleCard(cardValue, spawnEffect, deckRecipieCount);
            }
            else
            {
                var cardDeckViewObject = Instantiate(_cardItemPrefab, layout, false);

                if (deckRecipe && missingCards)
                {
                    var locationCard = cardDeckViewObject.AddComponent<CardLocationComponent>();
                    locationCard.Initialize(cardData, cardDeckViewObject.transform);
                }
                else
                {
                    var hoverCard = cardDeckViewObject.AddComponent<HoverCard>();
                    hoverCard.Initialize(cardData, cardDeckViewObject.transform, 60);
                    hoverCard.ForceToolTipsLeft = forceHoverCardToolTipsLeft;
                    hoverCard.ForceToolTipsRight = forceHoverCardToolTipsRight;
                }

                var dragCard = cardDeckViewObject.AddComponent<DragCard>();
                dragCard.SetIsInDeck(true);
                dragCard.SetDragCardDeckEditor(_draggableCard);

                dragCard.SetCardData(cardData);
                dragCard.SetScrollRect(scrollRect);

                var cardItem = cardDeckViewObject.GetComponent<CardDeckViewItem>();
                cardItem.SetDeckCardList(this);
                cardItem.Load(cardValue, true, deckRecipieCount, missingCards);

                _cardViews.Add(cardItem);

                if(trackDeckIndex) m_cardViewIndex += 1;

                if (m_hasDummyRelic) m_currentDummyRelic.transform.SetAsFirstSibling();

                if (sortCards)
                {
                    SortCards();
                }

                cardItem.SetActive(true);

                if (spawnEffect)
                {
                    cardItem.AnimatePlop();
                }

                if (shakeScreen && menuShakeContainer)
                {
                   // menuShakeContainer.Shake(5, 0.25f);
                }
            }
        }

        private void SortCards()
        {
            _cardViews = _cardViews.
                OrderBy(card => card.CardData.type != CardType.Power)
                .ThenBy(card => card.CardData.type != CardType.Elder_Relic)
                .ThenBy(card => card.CardData.cost)
                .ToList();

            for (var i = 0; i < _cardViews.Count; i++)
            {
                _cardViews[i].transform.SetSiblingIndex(i);
            }
        }

        public void RemoveCard(CardData cardData, bool insertDummyRelic = false)
        {
            if (cardData == null || m_currentDeckData == null) return;

            m_currentDeckData.RemoveCard(cardData.id);

            var cardView = _cardViews.Find(card => card.CardData.id == cardData.id);
            var cardValue = m_currentDeckData.GetCardValue(cardData.id);

            // if this card is not null & the count is greater than 0, it still exists in the deck
            if (cardValue != null && cardValue.count > 0)
            {
                cardView.AnimateDoubleCard(cardValue);
            }
            else
            {

                // Add Dummy Relic?
                var currentElderRelic = m_currentDeckData.GetElderRelic();
                if (m_relicsFilter && insertDummyRelic && currentElderRelic == null && !m_hasDummyRelic)
                {
                    if(m_currentDummyRelic == null) m_currentDummyRelic = Instantiate(DummyRelicPrefab, layout, false);
                    m_currentDummyRelic.SetActive(true);
                    m_currentDummyRelic.transform.SetAsFirstSibling();
                    m_hasDummyRelic = true;

                    // no animation when removing a elder relic
                    _cardViews.Remove(cardView);
                    Destroy(cardView.gameObject);

                }
                else
                {
                    cardView.AnimatePlopDie();
                    _cardViews.Remove(cardView);
                    cardView.AnimateRemove();
                }
            }

            layout.RectTransform().DOPunchAnchorPos(Vector2.left * 5, 0.15f, 0, .25f);



            App.PlayerDecks.SetIsDirty(true);
            DeckChanged();


        }

        public void OnDrop(PointerEventData eventData)
        {
            if (DragCardDeckEditor.Current == null || DragCardDeckEditor.Current.IsInDeck) return;

            var cardData = Db.CardDatabase.GetCard(DragCardDeckEditor.Current.CardData.id);

            if(cardData.type != CardType.Relic && cardData.type != CardType.Elder_Relic)
            {
                if (m_currentDeckData.GetCardCount(false) >= DeckData.MaxCardsPerDeck)
                {
                    //PopupController.Open(new PopupInfo
                    //{
                    //    style = PopupWindowStyle.Dialogue,
                    //    body = "I have too many cards in my deck. I need to <color=#F35C3B>remove a card</color> before adding more."
                    //});

                    return;
                }
            }
            else if(cardData.type == CardType.Relic)
            {
                if (m_currentDeckData.GetCardCount(false,true) >= 3)
                {
                    //PopupController.Open(new PopupInfo
                    //{
                    //    style = PopupWindowStyle.Dialogue,
                    //    body = "I have too many relics in my deck. I need to <color=#F35C3B>remove a Relic</color> before adding more."
                    //});
                    return;
                }
            }


            //var cardData = Db.CardDatabase.GetCard(DragCardDeckEditor.Current.CardData.id);

            if (cardData == null) return;

            var cardCount = m_currentDeckData.GetAmountOfSingleCard(cardData.id);

            switch (cardData.rarity)
            {
                case Rarity.Common:
                    if (cardCount >= DeckData.MaxCopiesOfCommonCards)
                    {
                        ErrorTooManyOfDuplicateCard();
                        return;
                    }
                    break;
                case Rarity.Rare:
                    if (cardCount >= DeckData.MaxCopiesOfRareCards)
                    {
                        ErrorTooManyOfDuplicateCard();
                        return;
                    }
                    break;
                case Rarity.Mythic:
                    if (cardCount >= DeckData.MaxCopiesOfMysticCards)
                    {
                        ErrorTooManyOfDuplicateCard();
                        return;
                    }
                    break;
                case Rarity.Legendary:
                    if (cardCount >= DeckData.MaxCopiesOfLegendaryCards)
                    {
                        ErrorTooManyOfDuplicateCard();
                        return;
                    }
                    break;
            }

            var currentElderRelic = m_currentDeckData.GetElderRelic();

            // Add Dummy Relic
            if (m_relicsFilter &&  currentElderRelic == null && !m_hasDummyRelic)
            {
                if (m_currentDummyRelic == null) m_currentDummyRelic = Instantiate(DummyRelicPrefab, layout, false);
                m_currentDummyRelic.SetActive(true);
                m_currentDummyRelic.transform.SetAsFirstSibling();
                m_hasDummyRelic = true;
            }

            if (cardData.type == CardType.Elder_Relic)
            {
                if (currentElderRelic != null)
                {
                    m_currentDeckData.RemoveCard(currentElderRelic.id);
                    DeckChanged();
                    RemoveCard(currentElderRelic);
                }

                if (m_currentDummyRelic.activeInHierarchy)
                {
                    m_currentDummyRelic.SetActive(false);
                    m_hasDummyRelic = false;
                }
            }

            m_currentDeckData.AddCard(cardData.id);

            App.PlayerDecks.SetIsDirty(true);
            //Debug.Log("DeckChanged()");

            DeckChanged();
            AudioController.PlaySound("UI-DeckBuilding_PlaceCard-NoFX");
            AddCard(cardData);


            if(m_hasDummyRelic) m_currentDummyRelic.transform.SetAsFirstSibling();

            layout.RectTransform().DOKill();
            layout.RectTransform().DOPunchAnchorPos(Vector2.right * 5, 0.15f, 0, .25f);
        }

        public void AddToDeckCardList(CardView card)
        {
            var cardData = card.data;

            if (cardData.type != CardType.Relic && cardData.type != CardType.Elder_Relic)
            {
                if (m_currentDeckData.GetCardCount(false) >= DeckData.MaxCardsPerDeck)
                {
                    //PopupController.Open(new PopupInfo
                    //{
                    //    style = PopupWindowStyle.Dialogue,
                    //    body = "I have too many cards in my deck. I need to <color=#F35C3B>remove a card</color> before adding more."
                    //});

                    return;
                }
            }
            else if (cardData.type == CardType.Relic)
            {
                if (m_currentDeckData.GetCardCount(false, true) >= 3)
                {
                    //PopupController.Open(new PopupInfo
                    //{
                    //    style = PopupWindowStyle.Dialogue,
                    //    body = "I have too many relics in my deck. I need to <color=#F35C3B>remove a Relic</color> before adding more."
                    //});
                    return;
                }
            }


            //var cardData = Db.CardDatabase.GetCard(DragCardDeckEditor.Current.CardData.id);

            if (cardData == null) return;

            var cardCount = m_currentDeckData.GetAmountOfSingleCard(cardData.id);

            switch (cardData.rarity)
            {
                case Rarity.Common:
                    if (cardCount >= DeckData.MaxCopiesOfCommonCards)
                    {
                        ErrorTooManyOfDuplicateCard();
                        return;
                    }
                    break;
                case Rarity.Rare:
                    if (cardCount >= DeckData.MaxCopiesOfRareCards)
                    {
                        ErrorTooManyOfDuplicateCard();
                        return;
                    }
                    break;
                case Rarity.Mythic:
                    if (cardCount >= DeckData.MaxCopiesOfMysticCards)
                    {
                        ErrorTooManyOfDuplicateCard();
                        return;
                    }
                    break;
                case Rarity.Legendary:
                    if (cardCount >= DeckData.MaxCopiesOfLegendaryCards)
                    {
                        ErrorTooManyOfDuplicateCard();
                        return;
                    }
                    break;
            }


            var currentElderRelic = m_currentDeckData.GetElderRelic();

            // Add Dummy Relic
            if (m_relicsFilter && currentElderRelic == null && !m_hasDummyRelic)
            {
                if(m_currentDummyRelic == null) m_currentDummyRelic = Instantiate(DummyRelicPrefab, layout, false);

                m_currentDummyRelic.SetActive(true);
                m_currentDummyRelic.transform.SetAsFirstSibling();
                m_hasDummyRelic = true;
            }

            if (cardData.type == CardType.Elder_Relic)
            {
                if (currentElderRelic != null)
                {
                    m_currentDeckData.RemoveCard(currentElderRelic.id);
                    DeckChanged();
                    RemoveCard(currentElderRelic);
                }

                if (m_currentDummyRelic.activeInHierarchy)
                {
                    m_currentDummyRelic.SetActive(false);
                    m_hasDummyRelic = false;
                }
            }

            App.PlayerDecks.SetIsDirty(true);
            //Debug.Log("DeckChanged()");

            m_currentDeckData.AddCard(cardData.id);
            DeckChanged();
            AudioController.PlaySound("UI-DeckBuilding_PlaceCard-NoFX");
            AddCard(cardData);

            if (m_hasDummyRelic) m_currentDummyRelic.transform.SetAsFirstSibling();

            card.RectTransform().DOPunchScale(Vector3.one * 0.1f, 0.2f);
            card.TintThenUntintOverTime(color, blend, 0.1f, 0f, 0.1f);
            card.cardGlowEffect.SpawnActionDrips();

            layout.RectTransform().DOKill();
            layout.RectTransform().DOPunchAnchorPos(Vector2.right * 5, 0.15f, 0, .25f);
        }

        private void ErrorTooManyOfDuplicateCard()
        {

        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            IsPointerOverCardList = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            IsPointerOverCardList = false;
        }

        private void FormatCardCount()
        {
            if (DeckEditMenu.Instance != null && DeckEditMenu.Instance.Settings.MenuMode == CollectionMenuMode.Relics)
            {

                deckFullOverlay.SetActive(false);
                var count = m_currentDeckData?.GetCardCount(false, false, false, true);
                cardsCount.text = $"{count}/{4}";
               // cardsCount.color = new Color(255, 255, 255, 1f);
                cardsCountLabel.text = "RELICS";

            }
            else
            {
                var count = m_currentDeckData?.GetCardCount();

                if (deckFullOverlay)
                {
                    deckFullOverlay.SetActive(count >= DeckData.MaxCardsPerDeck);
                }

                if (cardsCount)
                {
                    cardsCount.text = $"{count}/{DeckData.MaxCardsPerDeck}";
                }
                if (cardsCountLabel)
                {
                    cardsCountLabel.text = "CARDS";
                }

               // cardsCount.color = new Color(205,182,132,255);
            }
        }

        private void DeckChanged()
        {
            FormatCardCount();
            OnDeckChanged?.Invoke();
        }

        public void OnFinishButton()
        {
            layout.RectTransform().DOKill(true);

            switch (DeckEditMenu.Instance.Settings.OpenMode)
            {
                case DeckOpenMode.DeckCreator_SelectDeck:
                    if (m_currentDeckData == null) break;
                    EnableButton(false);
                    OnDeckSelected?.Invoke(m_currentDeckData);
                    break;
                case DeckOpenMode.DeckEditor:
                    EnableButton(false);
                    Timing.RunCoroutine(AnimateToCardCollection());
                    break;
                default:
                    Debug.LogError($"This button should not be visible in Open Mode: {DeckEditMenu.Instance.Settings.OpenMode}");
                    break;
            }

        }

        public void OnForceQuitDeckEditAndDelete(DeckData deckData)
        {
            layout.RectTransform().DOKill(true);

            App.PlayerDecks.DeleteDeck(deckData.uid);

            switch (DeckEditMenu.Instance.Settings.OpenMode)
            {
                case DeckOpenMode.DeckEditor:
                    DeckEditMenu.Instance.lastEditedDeckID = null;
                    deckList.RebuildDeckList();
                    FromDeckView = null;
                    OnFinishButton();
                    break;

                case DeckOpenMode.Collection:
                    deckList.RebuildDeckList();
                    deckList.RefreshDeckList();
                    App.PlayerDecks.SetIsDirty(true);
                    App.SaveAll();
                    //Debug.LogError("OnForceQuitDeckEditAndDelete()");
                    break;
            }
        }

        public void CloneDeck(DeckData deckData)
        {
            layout.RectTransform().DOKill(true);

            App.PlayerDecks.AddDeck(deckData.name + " *", deckData.hero, deckData.cards, null, PlayFab.Data.DeckEquipContext.Pvp, false);

            switch (DeckEditMenu.Instance.Settings.OpenMode)
            {
                case DeckOpenMode.DeckEditor:

                    break;

                case DeckOpenMode.Collection:
                    deckList.RebuildDeckList();
                    deckList.RefreshDeckList();
                    break;
            }
        }

        private IEnumerator<float> AnimateToCardCollection()
        {
            deckViewHeader.canvasGroup.DOFade(0, 0.10f);
            deckList.SetActive(true);

            m_currentDeckData = null;

            var op = deckViewHeader.transform.position;

            if (FromDeckView != null)
            {
                FromDeckView.canvasGroup.alpha = 0;
            }

            deckViewHeader.RectTransform().DOScaleY(0.0f, 0.10f);
            deckFooterPanel.DOScaleY(0.0f, 0.10f);


            yield return Timing.WaitUntilDone( cardLayoutCanvas.DOFade(0, 0.10f)
                .WaitForCompletion(true));

            deckList.canvasGroup.DOFade(1, 0.10f);

            if (FromDeckView != null)
            {
                FromDeckView.canvasGroup.alpha = 1;
            }
            else
            {
                deckViewHeader.canvasGroup.DOFade(0, 0.10f);
            }

            this.SetActive(false);
            layout.DestroyChildren();

            cardLayoutCanvas.alpha = 1;

            deckViewHeader.transform.position = op;

            deckList.HeaderPanel.RectTransform().DOScaleY(0f, 0.0f);
            deckList.HeaderPanel.RectTransform().DOScaleY(1f, 0.20f);

            deckList.deckFooterPanel.DOScaleY(0f, 0.0f);
            deckList.deckFooterPanel.DOScaleY(1f, 0.20f);
            _cardViews =new List<CardDeckViewItem>();


            App.SaveAll();
            //Debug.LogError("AnimateToCardCollection()");

            OnFinishedEditingDeck?.Invoke();
        }

        private void OnDeckHeaderPressed(DeckViewItem deckView)
        {
            if (DeckEditMenu.Instance.Settings.OpenMode == DeckOpenMode.DeckEditor)
            {
                deckControlsPopout.Open(deckView);
            }
        }

        private void OnDeckDeleted(DeckViewItem deckView)
        {
            if (deckView == null || deckView.MDeckData == null) return;

            App.PlayerDecks.DeleteDeck(deckView.MDeckData.uid);

            deckList.RebuildDeckList();
            FromDeckView = null;
            OnFinishButton();
        }

        private void OnDeckControlsClosed()
        {
            // do nothing?
        }
    }
}