using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossBlitz
{
    public class FMODMasterBus : MonoBehaviour
    {

        FMOD.Studio.EventInstance instance;
        FMOD.Studio.Bus bus;

        [SerializeField]
        [Range(-80f, 10f)]
        private float busVolume;
        private float volume;

        void Start()
        {
            instance.start();
            bus = FMODUnity.RuntimeManager.GetBus("bus:/MASTER");
        }

        void Update()
        {
            volume = Mathf.Pow(10.0f, busVolume / 20f);
            bus.setVolume(volume);
        }
    }
}