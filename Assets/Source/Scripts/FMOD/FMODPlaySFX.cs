using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossBlitz
{
    public class FMODPlaySFX : MonoBehaviour
    {
        private FMOD.Studio.EventInstance instance;

        void Start()
        {
            //In this example we're using the Battle_ArrowAttack01 SFX, all SFX will have the same filepath of "event:/SFX/"
            instance = FMODUnity.RuntimeManager.CreateInstance("event:/SFX/Battle_ArrowAttack01");
            // If the SFX is 3D use the line below, if not skip it
            //instance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
            instance.start();
            //release clears it out of memory, good practice to always do
            instance.release();
        }

        void Update()
        {
            //If the SFX is looping use this and remove the instance.release line from Start
            //if ()
            //{
            //    instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            //    instance.release();
            //}

            // We can also change parameters in Update with an "if" statement, in this example the parameter is called "buildup" with binary values 0 and 1
            //if ()
            //{
            //instance.setParameterByName("buildup", 1);
            //}
        }
    }
}
