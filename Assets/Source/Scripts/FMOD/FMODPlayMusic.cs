using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossBlitz
{
    public class FMODPlayMusic : MonoBehaviour
    {
        private FMOD.Studio.EventInstance instance;

        void Start()
        {
            //In this example we're using the battle-boss song, all music will have the same filepath of "event:/MUSIC/"
            instance = FMODUnity.RuntimeManager.CreateInstance("event:/MUSIC/battle-boss");
            instance.start();
        }

        //In this example using left control triggers the sound to stop looping and end the sound
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                //release clears it out of memory, good practice to always do
                instance.release();
            }

            // We can also change parameters in Update with an "if" statement, in this example the parameter is called "mus_intense" with binary values 0 and 1
            //if ()
            //{
            //instance.setParameterByName("mus_instense", 1);
            //}
        }
    }
}
