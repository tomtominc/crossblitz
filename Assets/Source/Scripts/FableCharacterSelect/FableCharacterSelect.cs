using CrossBlitz.AddressablesAPI;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Hero;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.UI.Widgets;
using DG.Tweening;
using MEC;
using System.Collections.Generic;
using TakoBoyStudios.Events;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.FableCharacterSelect
{
    public class FableCharacterSelect : MonoBehaviour
    {
        public SpriteAnimation background;
        public SpriteAnimation portrait;

        public ToggleButton selectButton;
        public GameObject lockedOverlay;
        public GameObject disabledOverlay;
        public GameObject activeFrame;

        public FableCharacterSelectionPanel characterSelectPanel;

        private string _id;
        private bool _canInteract;

        private void Start()
        {
            SceneController.OnSceneUnloaded += OnSceneUnloaded;
            Events.Subscribe(EventType.OnHeroChanged, OnHeroChanged);
            selectButton.OnValueChanged += ValueChanged;
            _canInteract = true;
        }

        private void Update()
        {
            if (characterSelectPanel.canSwitch && _canInteract)
            {
                selectButton.Toggle.interactable = true;
            }
            else
            {
                selectButton.Toggle.interactable = false;
            }
        }

        private void OnSceneUnloaded(SceneModel model)
        {
            selectButton.OnValueChanged -= ValueChanged;
            SceneController.OnSceneUnloaded -= OnSceneUnloaded;
            Events.Unsubscribe(EventType.OnHeroChanged, OnHeroChanged);
        }

        public void SetCharacter(string heroId)
        {
            _id = heroId;
            var hero = App.HeroData.GetHero(_id);

            if (hero != null)
            {
                lockedOverlay.SetActive(!hero.unlocked);
                disabledOverlay.SetActive(!hero.unlocked);
                selectButton.content = _id;
                background.Play(hero.GetData().faction.ToString().ToLower());
                portrait.Play(hero.id.ToLower() + "-idle", Random.Range(0, 11));
            }
        }

        public void SetParent(FableCharacterSelectionPanel parent)
        {
            characterSelectPanel = parent;
        }

        private void ValueChanged(bool isOn, string content)
        {
           // var hero = App.HeroData.GetHero(_id);

            if (App.Inventory.OwnsItem(_id))
            {
                if (isOn)
                {
                    activeFrame.SetActive(true);

                    if (characterSelectPanel.skipSFX) characterSelectPanel.skipSFX = false;
                    else AudioController.PlaySound("TMP-UI-ClickGrab");

                    var heroData = Db.HeroDatabase.GetHero(_id);
                    Events.Publish(this, EventType.OnHeroChanged, heroData);

                    _canInteract = false;

                    GetComponent<RectTransform>().DOPunchAnchorPos(Vector2.down * 3f, 0.2f);
                }
                else
                {
                    _canInteract = true;
                    activeFrame.SetActive(false);
                }
            }
            else
            {
                if (isOn)
                {
                    AudioController.PlaySound("TMP-UI-SelectLockedItem");
                    Timing.RunCoroutine(PlayContainerShake());
                }
                selectButton.Toggle.isOn = false;
                _canInteract = true;
            }
        }

        private void OnHeroChanged(IMessage message)
        {
            var heroData = (HeroData)message.Data;

            if (heroData != null)
            {
                var isOn = heroData.id == _id;

                if (selectButton.Toggle.isOn != isOn)
                {
                    selectButton.Toggle.isOn = isOn;
                }
            }
        }

        private IEnumerator<float> PlayContainerShake()
        {
            var rect = GetComponent<RectTransform>();
            var origAnchorPos = rect.anchoredPosition;
            yield return Timing.WaitUntilDone(rect.DOShakeAnchorPos(0.15f, 3f, 40, 90, false, true).WaitForCompletion(true));
            yield return Timing.WaitUntilDone(rect.DOAnchorPos(origAnchorPos, 0.1f).WaitForCompletion(true));
        }

    }
}