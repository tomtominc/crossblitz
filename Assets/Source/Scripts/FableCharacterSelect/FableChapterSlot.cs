using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using UnityEngine;

namespace CrossBlitz.FableCharacterSelect
{
    public class FableChapterSlot : MonoBehaviour
    {
        public enum State
        {
            None,
            Completed,
            Current,
        }

        public Animator _unityAnim;
        public SpriteAnimation _animator;
        public State state;

        private string _chapterHero;
        private string _bookNumber;
        private int _chapterNumber;
        //private ClientChapterData _chapterData;

        //public ClientChapterData ChapterData => _chapterData;

        public void SetChapter(string chapterHero, int bookNumber, int chapterNumber)
        {
            // _chapterHero = chapterHero;
            // _bookNumber = bookNumber;
            // _chapterNumber = chapterNumber;
            // _animator.Play($"{_chapterNumber+1}");
            // _unityAnim.Play("None", -1);
            //
            // var book = App.FableData.GetChapter(_chapterHero, _bookNumber, _chapterNumber);
            //
            // if (book.Chapters == null || book.Chapters.Count <= _chapterNumber)
            // {
            //     //Debug.LogWarning($"No chapter data in book {_chapterHero}-{_bookNumber}-{_chapterNumber} only {book.Chapters?.Count}");
            //     return;
            // }

            //_chapterData = book.Chapters[_chapterNumber];

        }

        public void ResetChapterSlot()
        {
            _animator.Play($"{_chapterNumber+1}");
            _unityAnim.Play("None", -1);
        }

        public void AnimateCurrentValue()
        {
            // if (_chapterData == null)
            // {
            //     state = State.None;
            //     return;
            // }
            //
            // if (_chapterData.IsComplete)
            // {
            //     state = State.Completed;
            //     _unityAnim.Play("Completed", -1);
            // }
            // // else if (_chapterData.IsCurrent)
            // // {
            // //     state = State.Current;
            // //     _unityAnim.Play("Current", -1);
            // // }
            // else
            // {
            //     state = State.None;
            //     _unityAnim.Play("None", -1);
            // }
        }
    }
}