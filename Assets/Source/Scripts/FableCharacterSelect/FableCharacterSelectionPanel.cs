using CrossBlitz.Databases;
using UnityEngine;

namespace CrossBlitz.FableCharacterSelect
{
    public class FableCharacterSelectionPanel : MonoBehaviour
    {
        public RectTransform characterSelectLayout;
        public FableCharacterSelect characterSelectPrefab;
        public bool canSwitch;
        public bool skipSFX;

        private void Start()
        {
            var heroes = Db.HeroDatabase.Heroes;

            for (var i = 0; i < heroes.Count; i++)
            {
                var characterSelect = Instantiate(characterSelectPrefab, characterSelectLayout);
                characterSelect.SetCharacter(heroes[i].id);
                characterSelect.SetParent(this);
            }

            canSwitch = true;
            skipSFX = true;
        }
    }
}