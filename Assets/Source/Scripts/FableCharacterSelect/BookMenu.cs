using System;
using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Hero;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ViewAPI;
using CrossBlitz.ViewAPI.Popups;
using DG.Tweening;
using MEC;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.FableCharacterSelect
{
    public class BookMenu : MonoBehaviour
    {
        public CanvasGroup canvas;
        public CanvasGroup outlineCanvas;
        public RectTransform bookObjectLayout;
        public FableBookObject bookObjectPrefab;
        public RectTransform bookDescriptionContainer;
        public RectTransform bookTitleContainer;
        public SpriteAnimation bookDescriptionFactionIcon;
        public TextMeshProUGUI bookDescription;
        public TextMeshProUGUI bookTitle;
        public ToggleGroup toggleGroup;
        public ButtonContainer backButton;
        public TextMeshProUGUI accoladesLabel;

        private bool _menuIsLoaded;
        private List<FableBookObject> _books;
        private void Start()
        {
            canvas.interactable = false;
            outlineCanvas.SetActive(false);
            outlineCanvas.alpha = 0;

            backButton.SetInteractable(false);
            backButton.SetAlpha(0);
            backButton.Button.onClick.AddListener(OnBackButton);

            _books = new List<FableBookObject>();

            for (var i = 0; i < 3; i++)
            {
                var bookObject = Instantiate(bookObjectPrefab, bookObjectLayout);

                if (i == 0)
                {
                    bookObject.animator.Play($"redcroft-{i+1}");
                }
                else
                {
                    bookObject.animator.Play($"redcroft-{i+1}-locked");
                }

                bookObject.animator.SetActive(false);
                bookObject.toggle.Toggle.group = toggleGroup;

                _books.Add(bookObject);
            }

            Events.Subscribe(EventType.OnBookChanged, OnBookChanged);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnBookChanged, OnBookChanged);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (backButton.IsActive() && backButton.Interactable)
                {
                    if (!PopupController.IsOpen) OnBackButton();
                    return;
                }
            }
        }

        private void OnBackButton()
        {

            backButton.SetInteractable(false);
            backButton.SetAlpha(0);
            AudioController.PlaySound("UI-BattlePhaseShift");
            //AudioController.PlaySound("TMP-UI-SelectLockedItem");
            Events.Publish(this,EventType.OnBookChanged, new OnBookChangedEventArgs());
        }

        private IEnumerator<float> BookChanged(string bookHero, int chapterNumber)
        {
            // todo: remove this once I have the book data stuff!

            // if (bookNumber == "1")
            // {
            //     bookTitle.text = "A PIRATE'S PLOY";
            //     bookDescription.text =
            //         "Wrongly accused and imprisoned for raiding a supply ship, <color=#c64a4e>Redcroft</color> and his crew strike a deal to win back their freedom.";
            // }
            // else
            // {
            //     bookTitle.text = "???";
            //     bookDescription.text = "This book is locked and will be released in the future.";
            // }

            // todo: fix this so its setup with data!!

            var chapter = Db.FablesDatabase.GetChapterByChapterId($"{bookHero}-1-{chapterNumber}");

            if (chapter != null)
            {
                bookTitle.text = chapter.ChapterName.ToUpper();
                bookDescription.text = chapter.ChapterDescription;

                var accoladeChapterData = App.FableData.GetChapter(chapter.Uid);
                accoladeChapterData.GetAccoladeCounts(out var completeCount, out var maxCount);

                if(maxCount == 0)
                {
                    accoladesLabel.text = "??<color=#768a51>  / ??</color>";
                }
                else if (maxCount == completeCount)
                {
                    accoladesLabel.text = "<color=#768a51>" + completeCount + "  / " + maxCount + "</color>";
                }
                else
                {
                    accoladesLabel.text = completeCount + "<color=#768a51>  / " + maxCount + "</color>";
                }
            }
            else
            {
                bookTitle.text = "???";
                bookDescription.text = "This book is locked and will be released in the future.";
                accoladesLabel.text = "??<color=#768a51>/ ??</color>";
            }

            var faction = HeroData.GetFactionForHero(bookHero);
            bookDescriptionFactionIcon.Play(faction.ToString().ToLower());

            if (!_menuIsLoaded)
            {
                bookTitleContainer.SetActive(false);

                bookDescriptionContainer.SetActive(true);
                bookDescriptionContainer.anchoredPosition = new Vector2(0, -124f);
                bookDescriptionContainer.DOAnchorPosX(-65f, 0.24f)
                    .SetEase(Ease.OutBack);

                outlineCanvas.SetActive(true);

                var startPos = outlineCanvas.RectTransform().anchoredPosition;
                outlineCanvas.RectTransform().anchoredPosition = new Vector2(startPos.x, startPos.y - 32);

                outlineCanvas.alpha = 0;
                outlineCanvas.DOFade(1, 0.24f);

                yield return Timing.WaitUntilDone(outlineCanvas.RectTransform()
                    .DOAnchorPos(startPos, 0.24f).SetEase(Ease.OutBack)
                    .WaitForCompletion(true));

                var startTitlePos = new Vector2(0, 42);
                bookTitleContainer.SetActive(true);
                bookTitleContainer.anchoredPosition = Vector2.zero;
                bookTitleContainer.DOAnchorPos(startTitlePos, 0.12f);

                for (var i = 0; i < _books.Count; i++)
                {
                    _books[i].SetBook(bookHero, i+1);
                    _books[i].animator.SetActive(true);
                    Timing.RunCoroutine(_books[i].Drop());
                    yield return Timing.WaitForSeconds(0.18f);
                }
            }

            if (backButton)
            {
                backButton.SetInteractable(true);
                backButton.SetAlpha(1);
            }

            _menuIsLoaded = true;
            canvas.interactable = _menuIsLoaded;
            canvas.blocksRaycasts = _menuIsLoaded;

            _books[chapterNumber-1].Select();
        }

        private IEnumerator<float> AnimateOut()
        {
            _menuIsLoaded = false;
            AudioController.PlaySound("TMP-UI-SelectLockedItem");
            canvas.interactable = _menuIsLoaded;
            canvas.blocksRaycasts = _menuIsLoaded;

            for (var i = 0; i < _books.Count; i++)
            {
                _books[i].animator.SetActive(false);
            }

            outlineCanvas.DOFade(0, 0.24f);
            bookDescriptionContainer.SetActive(false);

            yield break;
        }

        private void OnBookChanged(IMessage message)
        {
            if (message.Data is OnBookChangedEventArgs args)
            {
                if (string.IsNullOrEmpty(args.bookHero))
                {
                    Timing.RunCoroutine(AnimateOut());
                }
                else
                {
                    Timing.RunCoroutine(BookChanged(args.bookHero, args.chapterNumber));
                }
            }
        }
    }
}