using System.Collections.Generic;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Hero;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ViewAPI;
using DG.Tweening;
using MEC;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.FableCharacterSelect
{
    public class FableCharacterSelectHeroView : MonoBehaviour
    {
        public SpriteAnimation heroPortrait;
        public SpriteAnimation factionBanner;
        public SpriteAnimation factionIcon;
        public RectTransform heroDescriptionContainer;
        public TextMeshProUGUI heroDescription;
        public RectTransform heroSelectionContainer;
        public CanvasGroup heroDisplayMenuCanvas;
        public CanvasGroup heroFrontDisplayCanvas;
        public ButtonContainer nextButton;
        public ButtonContainer backButton;
        public List<GameObject> heroBackgrounds;

        private HeroData _currentHero;
        private GameObject _currentBackground;

        private void Start()
        {
            nextButton.Button.onClick.AddListener(OnNextButtonPressed);
            nextButton.SetInteractable(true);
            nextButton.SetAlpha(1);

            backButton.Button.onClick.AddListener(OnBackButtonPressed);
            backButton.SetInteractable(true);
            backButton.SetAlpha(1);

            Events.Subscribe(EventType.OnHeroChanged, OnHeroChanged);
            Events.Subscribe(EventType.OnBookChanged, OnBookChanged);
        }

        private void OnDisable()
        {
            Events.Unsubscribe(EventType.OnHeroChanged, OnHeroChanged);
            Events.Unsubscribe(EventType.OnBookChanged, OnBookChanged);
        }

        private IEnumerator<float> AnimateIn()
        {
            heroDescriptionContainer.SetActive(true);
            heroDescriptionContainer.anchoredPosition = new Vector2(-65f, -124);
            heroDescriptionContainer.DOAnchorPosX(0, 0.24f).SetEase(Ease.OutBack);

            backButton.SetInteractable(true);
            nextButton.SetInteractable(true);
            nextButton.RectTransform().DOAnchorPosY(-158, 0.24f)
                .SetEase(Ease.InBack);

            heroSelectionContainer.SetActive(true);
            heroDisplayMenuCanvas.SetActive(true);
            heroFrontDisplayCanvas.SetActive(true);

            heroSelectionContainer.DOAnchorPosY(-26f, 0.24f)
                .SetEase(Ease.OutBack);

            heroDisplayMenuCanvas.DOFade(1, 0.24f);
            yield return Timing.WaitUntilDone( heroFrontDisplayCanvas.DOFade(1, 0.24f)
                .WaitForCompletion(true));
        }

        private IEnumerator<float> AnimateOut()
        {
            heroDescriptionContainer.SetActive(false);

            nextButton.SetInteractable(false);
            nextButton.RectTransform().DOAnchorPosY(-300, 0.24f)
                .SetDelay(0.12f)
                .SetEase(Ease.InBack);

            heroSelectionContainer.DOAnchorPosY(100, 0.24f)
                .SetEase(Ease.InBack);

            heroDisplayMenuCanvas.DOFade(0, 0.24f);

            yield return Timing.WaitUntilDone( heroFrontDisplayCanvas.DOFade(0, 0.24f)
                .WaitForCompletion(true));

            heroSelectionContainer.SetActive(false);
            heroDisplayMenuCanvas.SetActive(false);
            heroFrontDisplayCanvas.SetActive(false);
        }

        private void OnHeroChanged(IMessage message)
        {
            Debug.LogError(gameObject);

            var tempCurrent = message.Data as HeroData;

            if (tempCurrent == null) return;

            if (_currentHero != null && _currentHero.id == tempCurrent.id) return;

            _currentHero = tempCurrent;

            heroPortrait.Play(_currentHero.id.ToLower());
            factionBanner.Play(_currentHero.faction.ToString().ToLower());
            factionIcon.Play(_currentHero.faction.ToString().ToLower());
            heroDescription.text = _currentHero.description;

            if (_currentBackground != null) _currentBackground.SetActive(false);

            var factionIndex = (int)_currentHero.faction - 2;

            for (var i = 0; i < heroBackgrounds.Count; i++)
            {
                Debug.Log(heroBackgrounds[i].name);
            }

            if (heroBackgrounds.Count > factionIndex)
            {
                _currentBackground = heroBackgrounds[factionIndex];
                _currentBackground.SetActive(true);
            }
        }

        private void OnBookChanged(IMessage message)
        {
            if (message.Data is OnBookChangedEventArgs args)
            {
                if (string.IsNullOrEmpty(args.bookHero) || string.IsNullOrEmpty(args.bookHero))
                {
                    Timing.RunCoroutine(AnimateIn());
                }
                else
                {
                    Timing.RunCoroutine(AnimateOut());
                }
            }
        }

        private void OnNextButtonPressed()
        {
            backButton.SetInteractable(false);

            Events.Publish(this, EventType.OnBookChanged, new OnBookChangedEventArgs
            {
                bookHero = App.HeroData.GetCurrentHero(),
                chapterNumber = 1
            });
        }

        private void OnBackButtonPressed()
        {
            backButton.SetInteractable(false);

            Events.Publish(this, EventType.ChangeState, new StateChangeEventArgs
            {
                state = StateDefinition.MAIN_MENU
            });
        }
    }
}