
using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.UI.Widgets;
using DG.Tweening;
using LeTai.TrueShadow;
using MEC;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.FableCharacterSelect
{
    public class FableBookObject : MonoBehaviour
    {
        public SpriteAnimation animator;
        public SpriteAnimation outlineAnimator;
        public RectTransform bookObject;
        public TrueShadow shadow;
        public ToggleButton toggle;

        private const float MaxDistance = 250;
        private const float MaxAlpha = 153f / 255f;

        private string m_bookHero;
        private int m_chapterNumber;

        private void Start()
        {
            toggle.OnValueChanged += OnToggled;
        }

        private void OnDestroy()
        {
            toggle.OnValueChanged -= OnToggled;
        }

        public void SetBook(string bookHero, int chapterNumber)
        {
            m_bookHero = bookHero;
            m_chapterNumber = chapterNumber;

            var heroData = Db.HeroDatabase.GetHero(m_bookHero);
            var chapter = Db.FablesDatabase.GetChapterByChapterId($"{m_bookHero}-1-{m_chapterNumber}");
            var chapterData = App.FableData.GetChapter(bookHero,1, chapterNumber);

            var append = string.Empty;

            if (heroData == null || chapter == null || chapterData == null || !chapterData.IsUnlocked())
            {
                append = "-locked";
            }

            animator.Play($"{m_bookHero.ToLower()}-{m_chapterNumber}{append}");
        }

        private void OnToggled(bool isOn, string content)
        {
            if (!isOn)
            {
                Deselect();
            }
            else
            {
                AudioController.PlaySound("UI-Selection");
                //MasterAudio.PlaySound("UI-Selection");

                Events.Publish(this,EventType.OnBookChanged, new OnBookChangedEventArgs
                {
                    bookHero = m_bookHero,
                    chapterNumber = m_chapterNumber
                });
            }
        }

        public IEnumerator<float> Drop()
        {
            toggle.Toggle.SetIsOnWithoutNotify(false);
            bookObject.anchoredPosition = new Vector3(0, MaxDistance);

            bookObject.DOAnchorPosY(0, 0.5f).SetEase(Ease.OutBounce);

            yield return Timing.WaitForSeconds(0.025f);
            AudioController.PlaySound("Hero_Appear-Whoosh").setPitch(0.5f);
        }

        public void Select()
        {
            if (!toggle.Toggle.isOn)
            {
                toggle.Toggle.SetIsOnWithoutNotify(true);
            }

            DOTween.Kill(bookObject);

            bookObject.DOAnchorPosY(16f, 0.24f).SetEase(Ease.OutBack);

            if (animator.CurrentAnimationName.Contains("locked"))
            {
                outlineAnimator.Play("locked");
            }
            else
            {
                outlineAnimator.Play("unlocked");
            }
        }

        private void Deselect()
        {
            DOTween.Kill(bookObject);

            bookObject.DOAnchorPosY(0f, 0.5f)
                .SetEase(Ease.OutBounce);
        }

        private void Update()
        {
            var offsetY = bookObject.anchoredPosition.y;
            var offset = offsetY + 4;

            if (Mathf.Abs(shadow.OffsetDistance - offset) > float.Epsilon)
            {
                shadow.OffsetDistance = offset;
            }

            var alpha = MaxAlpha * (MaxDistance -offsetY ) / MaxDistance;

            if (Mathf.Abs(shadow.Color.a - alpha) > float.Epsilon)
            {
                shadow.Color = new Color(shadow.Color.r, shadow.Color.g, shadow.Color.b, alpha);
            }
        }
    }
}