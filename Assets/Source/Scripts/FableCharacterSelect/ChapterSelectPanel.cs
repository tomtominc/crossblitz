using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables;
using CrossBlitz.Hero;
using CrossBlitz.MainMenu;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.UI.Widgets;
using CrossBlitz.ViewAPI.Popups;
using DG.Tweening;
using MEC;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.FableCharacterSelect
{
    public class ChapterSelectPanel : MonoBehaviour
    {
        public const int MaxChapters = 3;

        public RectTransform panel;
        public HeroPanelBackground background;
        public TextMeshProUGUI objectiveLabel;
        public HeroMainMenuSlot bookHeroSlot;
        public SpriteAnimation bookNumberRomanNumeral;
       // public ToggleButton viewTreasuresToggleButton; // not used yet! Add a toggle button to the gameobject in scene
        //public Image treasureCompleteBar;
        //public TextMeshProUGUI treasureCompletePercentage;
        public ToggleButton normalModeToggle;
        public ToggleButton hardModeToggle;
        public Button playButton;
        public Button resumeButton;
        public Button restartButton;

        private string m_bookHeroId;
        private int m_chapterNumber;

        // private List<FableChapterSlot> _slots;

        public void Awake()
        {
            // _slots = new List<FableChapterSlot>(MaxChapters);

            // for (var i = 0; i < MaxChapters; i++)
            // {
            //     var chapterSlot = Instantiate(chapterSlotPrefab, chapterSlotLayout);
            //     chapterSlot._animator.Play($"{i+1}");
            //     _slots.Add(chapterSlot);
            // }

            playButton.onClick.AddListener(OnPlayButton);
            resumeButton.onClick.AddListener(OnPlayButton);
            restartButton.onClick.AddListener(OnRestartButton);

            panel.SetActive(false);
            panel.anchoredPosition = new Vector2(panel.sizeDelta.x +20, panel.anchoredPosition.y);

            Events.Subscribe(EventType.OnBookChanged, OnBookChanged);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnBookChanged, OnBookChanged);
        }

        private void OnBookChanged(IMessage message)
        {
            if (message.Data is OnBookChangedEventArgs args)
            {
               Timing.RunCoroutine( BookChanged(args.bookHero, args.chapterNumber) );
            }
        }

        private IEnumerator<float> BookChanged(string bookHero, int chapterNumber)
        {
            m_bookHeroId = bookHero;
            m_chapterNumber = chapterNumber;

            if (string.IsNullOrEmpty(bookHero))
            {
                yield return Timing.WaitUntilDone(AnimateOut());
                yield break;
            }

            var heroData = Db.HeroDatabase.GetHero(m_bookHeroId);
            var chapter = App.FableData.GetChapter(m_bookHeroId, 1, m_chapterNumber);
            var chapterData = Db.FablesDatabase.GetChapterByChapterId($"{m_bookHeroId}-1-{m_chapterNumber}");

            if (chapter == null || !chapter.IsUnlocked())
            {
                playButton.SetActive(true);
                resumeButton.SetActive(false);
                restartButton.SetActive(false);
                playButton.interactable = false;
                objectiveLabel.text = "???";
            }
            else if (!chapter.isChapterReset())
            {
                playButton.SetActive(false);
                resumeButton.SetActive(true);
                restartButton.SetActive(true);
                playButton.interactable = true;
            }
            else
            {
                playButton.SetActive(true);
                resumeButton.SetActive(false);
                restartButton.SetActive(false);
                playButton.interactable = true;
            }

            if (chapterData != null)
            {
                var mainQuest = Db.FableQuestDatabase.GetQuest(chapterData.MainQuest);

                if (mainQuest != null && !string.IsNullOrEmpty(mainQuest.QuestDescription))
                {
                    objectiveLabel.text = mainQuest.QuestDescription.ToUpper();
                }
                else
                {
                    objectiveLabel.text = "???";
                }
            }

            background.SetFaction(HeroData.GetFactionForHero(bookHero).ToString().ToLower());

            bookHeroSlot.SetHeroId(bookHero);

            if (!bookNumberRomanNumeral.Play($"{heroData.faction.ToString().ToLower()}-{m_chapterNumber}"))
            {
                Debug.LogError($"Could not play {heroData.faction.ToString().ToLower()}-{m_chapterNumber}");
            }

           // treasureCompleteBar.fillAmount = 0;
           // treasureCompletePercentage.text = "0%";

            if (!panel.IsActive())
            {
                yield return Timing.WaitUntilDone(AnimateIn());
            }

            // Timing.RunCoroutine(AnimateSlotValues());
        }

        private IEnumerator<float> AnimateOut()
        {
            yield return Timing.WaitUntilDone(
                panel.DOAnchorPosX(panel.sizeDelta.x+20, 0.5f)
                    .SetEase(Ease.InOutQuint)
                .WaitForCompletion(true));

            panel.SetActive(false);
        }

        private IEnumerator<float> AnimateIn()
        {
            panel.SetActive(true);

            yield return Timing.WaitUntilDone(
                panel.DOAnchorPosX(0, 0.5f)
                    .SetEase(Ease.InOutQuint)
                    .WaitForCompletion(true));
        }

        // private IEnumerator<float> AnimateSlotValues()
        // {
        //     for (var i = 0; i < _slots.Count; i++)
        //     {
        //         _slots[i].AnimateCurrentValue();
        //         yield return Timing.WaitForSeconds(0.16f);
        //     }
        // }

        private void OnPlayButton()
        {
            AmbientController.Stop();
            AudioController.PlaySound("UI-ColiseumStart-NoFX");
            AudioController.StopSong();
            FableController.SetChapter(m_bookHeroId, 1, m_chapterNumber);
            App.BattleResults.ResetData();
            Events.Publish(this, EventType.OnPlayBook,null);
        }

        private void OnRestartButton()
        {
            PopupController.Open(new PopupInfo
            {
                style = PopupWindowStyle.Generic,
                title = "RESTART CHAPTER",
                body =
                "Would you like to <color=#F35C3B>restart</color> this chapter?\n\n<color=#F35C3B>Story progress will be reset</color> but all Hero levels, Cards Collected, Accolades, Deck Recipes, and Treasures already acquired will not be effected.\n\nYou can replay the story and make new decisions, leading to <color=#F35C3B>new rewards!</color>",
                confirm = "OKAY!",
                deny = "NO!"
            });

            PopupController.OnPopupChoice += OnRestartChoiceSelected;
        }


        private void OnRestartChoiceSelected(bool restart)
        {
            PopupController.OnPopupChoice -= OnRestartChoiceSelected;

            if (restart)
            {
                var chapter = App.FableData.GetChapter(m_bookHeroId, 1, m_chapterNumber);
                if (chapter != null)
                {
                    if (!App.FableData.ResetChapter(chapter.ChapterUid))
                    {
                        Debug.LogError($"Could not save chapter {chapter.ChapterUid}.");
                    }
                    else
                    {
                        Debug.Log("Chapter reset successful.");
                        playButton.SetActive(true);
                        resumeButton.SetActive(false);
                        restartButton.SetActive(false);

                        App.SaveAll();
                    }
                }
                else
                {
                    Debug.Log("Chapter Already Reset!");
                }
            }
        }
    }
}