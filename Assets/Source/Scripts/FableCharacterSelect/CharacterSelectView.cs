using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Hero;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ViewAPI;
using DG.Tweening;
using MEC;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.FableCharacterSelect
{
    public class CharacterSelectView : MonoBehaviour
    {
        public SpriteAnimation heroPortrait;
        public SpriteAnimation factionBanner;
        public SpriteAnimation factionIcon;
        public RectTransform heroDescriptionContainer;
        public TextMeshProUGUI heroDescription;
        public RectTransform heroSelectionContainer;
        public CanvasGroup heroDisplayMenuCanvas;
        public CanvasGroup heroFrontDisplayCanvas;
        public ButtonContainer nextButton;
        public ButtonContainer backButton;
        public List<GameObject> heroBackgrounds;
        public RectTransform heroBackgroundFrame;
        public CanvasGroup heroBackgroundShadow;
        public FableCharacterSelectionPanel characterSelectPanel;

        private HeroData _currentHero;
        private GameObject _currentBackground;
        private bool m_onBooksPage;
        private bool _canEscape;
        private bool _waitOnLoad;
        private float _OnLoad;

        private Vector2 heroPortraitOrigPos;
        private Vector2 heroBannerOrigPos;

        private void Start()
        {
            nextButton.Button.onClick.AddListener(OnNextButtonPressed);
            nextButton.SetInteractable(true);
            nextButton.SetAlpha(1);

            backButton.Button.onClick.AddListener(OnBackButtonPressed);
            backButton.SetInteractable(true);
            backButton.SetAlpha(1);

            Events.Subscribe(EventType.OnHeroChanged, OnHeroChanged);
            Events.Subscribe(EventType.OnBookChanged, OnBookChanged);

            AudioController.PlaySong("character-select");

            heroPortraitOrigPos = heroPortrait.RectTransform().anchoredPosition;
            heroBannerOrigPos = factionBanner.RectTransform().anchoredPosition;

            _canEscape = true;
            _waitOnLoad = true;
            _OnLoad = .25f;

        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (backButton.Interactable && _canEscape)
                {
                    OnBackButtonPressed();
                }
            }
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnHeroChanged, OnHeroChanged);
            Events.Unsubscribe(EventType.OnBookChanged, OnBookChanged);
        }

        private IEnumerator<float> AnimateIn()
        {
            m_onBooksPage = false;
            //MasterAudio.PlaySound("Hero_Appear-Whoosh");
            AudioController.PlaySound("Hero_Appear-Whoosh");
            // MasterAudio.ChangePlaylistByName("character-select");

            heroDescriptionContainer.SetActive(true);
            heroDescriptionContainer.anchoredPosition = new Vector2(-65f, -124);
            heroDescriptionContainer.DOAnchorPosX(0, 0.24f).SetEase(Ease.OutBack);

            backButton.SetInteractable(true);


            heroSelectionContainer.SetActive(true);
            heroDisplayMenuCanvas.SetActive(true);
            heroFrontDisplayCanvas.SetActive(true);

            heroSelectionContainer.DOAnchorPosY(-26f, 0.24f)
                .SetEase(Ease.OutBack);

            heroDisplayMenuCanvas.DOFade(1, 0.24f);
            yield return Timing.WaitUntilDone( heroFrontDisplayCanvas.DOFade(1, 0.4f)
                .WaitForCompletion(true));


            yield return Timing.WaitUntilDone(nextButton.RectTransform().DOAnchorPosY(-158, .5f)
                .SetEase(Ease.InOutQuint).WaitForCompletion(true));

            nextButton.SetInteractable(true);
        }

        private IEnumerator<float> AnimateOut()
        {
            nextButton.SetInteractable(false);
            nextButton.RectTransform().DOAnchorPosY(-200, 0.25f)
            .SetDelay(0.12f)
            .SetEase(Ease.InOutQuint);

            m_onBooksPage = true;
            //MasterAudio.PlaySound("Hero_Appear-Whoosh");
            AudioController.PlaySound("Hero_Appear-Whoosh");
            //MasterAudio.ChangePlaylistByName("select-fable");

            heroDescriptionContainer.SetActive(false);



            heroSelectionContainer.DOAnchorPosY(100, 0.24f)
                .SetEase(Ease.InBack);

            heroDisplayMenuCanvas.DOFade(0, 0.24f);

            yield return Timing.WaitUntilDone( heroFrontDisplayCanvas.DOFade(0, 0.4f)
                .WaitForCompletion(true));

            heroSelectionContainer.SetActive(false);
            heroDisplayMenuCanvas.SetActive(false);
            heroFrontDisplayCanvas.SetActive(false);

        }

        private void OnHeroChanged(IMessage message)
        {
            var tempCurrent = message.Data as HeroData;

            if (tempCurrent == null) return;

            if (_currentHero != null && _currentHero.id == tempCurrent.id)
            {
                return;
            }

            if (characterSelectPanel.canSwitch)
            {
                characterSelectPanel.canSwitch = false;
                nextButton.SetInteractable(false);
            }
            else return;

            _currentHero = tempCurrent;

            Timing.RunCoroutine(AnimateBackground().CancelWith(gameObject));

            _canEscape = true;
        }

        public IEnumerator<float> AnimateBackground()
        {
            yield return Timing.WaitForOneFrame;

            // Animate Out
            heroBackgroundFrame.DOSizeDelta(new Vector2(heroBackgroundFrame.sizeDelta.x, 26f), 0.3f * _OnLoad).SetEase(Ease.InBack);
            if (!_waitOnLoad) AudioController.PlaySound("ui_window_open", "BARDSFX");

            heroPortrait.GetComponent<CanvasGroup>().DOFade(0f, 0.15f * _OnLoad);
            factionBanner.GetComponent<CanvasGroup>().DOFade(0f, 0.15f * _OnLoad);
            factionIcon.GetComponent<CanvasGroup>().DOFade(0f, 0.15f * _OnLoad);
            heroDescription.GetComponent<CanvasGroup>().DOFade(0f, 0.15f * _OnLoad);

            heroPortrait.GetComponent<RectTransform>().DOAnchorPosY(heroPortraitOrigPos.y-50f, 0.3f * _OnLoad).SetEase(Ease.InBack);
            factionBanner.GetComponent<RectTransform>().DOAnchorPosY(heroBannerOrigPos.y+50f, 0.3f * _OnLoad).SetEase(Ease.InBack);
            //heroDescriptionContainer.DOPunchAnchorPos(Vector2.right * 5f, 0.4f * _OnLoad);
            // factionIcon.GetComponent<RectTransform>().DOPunchAnchorPos(Vector2.down * 10f, 1f);

            yield return Timing.WaitForSeconds(0.3f * _OnLoad);

            // Change Background
            if (_currentBackground != null) _currentBackground.SetActive(false);

            var factionIndex = (int)_currentHero.faction - 2;

            if (heroBackgrounds.Count > factionIndex)
            {
                _currentBackground = heroBackgrounds[factionIndex];
                _currentBackground.SetActive(true);
            }

            // Change Hero Information
            heroDescription.text = _currentHero.description;
            heroPortrait.Play(_currentHero.id.ToLower());
            factionBanner.Play(_currentHero.faction.ToString().ToLower());
            factionIcon.Play(_currentHero.faction.ToString().ToLower());

            while (_waitOnLoad)
            {
                yield return Timing.WaitForSeconds(.7f);
                _waitOnLoad = false;
                _OnLoad = 1f;
            }

            // Animate In
            heroBackgroundFrame.DOSizeDelta(new Vector2(heroBackgroundFrame.sizeDelta.x, 130f), 0.3f).SetEase(Ease.OutBack);
            //AudioController.PlaySound("ui_window_close", "BARDSFX");
            yield return Timing.WaitForSeconds(0.15f);

            characterSelectPanel.canSwitch = true;
            nextButton.SetInteractable(true);

            heroPortrait.GetComponent<CanvasGroup>().DOFade(1f, 0.15f);
            factionBanner.GetComponent<CanvasGroup>().DOFade(1f, 0.15f);
            factionIcon.GetComponent<CanvasGroup>().DOFade(1f, 0.15f);
            heroDescription.GetComponent<CanvasGroup>().DOFade(1f, 0.15f);

            heroPortrait.GetComponent<RectTransform>().DOAnchorPosY(heroPortraitOrigPos.y, 0.3f).SetEase(Ease.OutBack);
            factionBanner.GetComponent<RectTransform>().DOAnchorPosY(heroBannerOrigPos.y, 0.3f).SetEase(Ease.OutBack);
            //factionIcon.GetComponent<RectTransform>().DOPunchAnchorPos(Vector2.up * 10f, 1f);
            yield return Timing.WaitForSeconds(0.2f);
        }

        private void OnBookChanged(IMessage message)
        {
            if (message.Data is OnBookChangedEventArgs args)
            {
                if (string.IsNullOrEmpty(args.bookHero) || string.IsNullOrEmpty(args.bookHero))
                {
                    Timing.RunCoroutine(AnimateIn());
                }
                else if (!m_onBooksPage)
                {
                    Timing.RunCoroutine(AnimateOut());
                }
            }
        }

        private void OnNextButtonPressed()
        {
            backButton.SetInteractable(false);

            Events.Publish(this, EventType.OnBookChanged, new OnBookChangedEventArgs
            {
                bookHero = App.HeroData.GetCurrentHero(),
                chapterNumber = 1
            });

            AudioController.PlaySound("UI-StartingHand");
            AudioController.PlaySound("UI-BattlePhaseShift");
            //MasterAudio.PlaySound("UI-StartingHand");
        }

        private void OnBackButtonPressed()
        {
            AudioController.PlaySound("TMP-UI-SelectLockedItem");
            backButton.SetInteractable(false);
            _canEscape = false;
            Events.Publish(this, EventType.ChangeState, new StateChangeEventArgs
            {
                state = StateDefinition.MAIN_MENU
            });
        }
    }
}