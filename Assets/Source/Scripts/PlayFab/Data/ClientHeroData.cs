using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Hero;
using CrossBlitz.PlayFab.Authentication;
using GameDataEditor;
using UnityEngine;

namespace CrossBlitz.PlayFab.Data
{
    [System.Serializable]
    public class ClientHeroData : ClientData
    {
        public string CurrentHero;
        public List<SavedHeroData> Heroes;

        public override void Init()
        {
            CurrentHero = App.Load("ClientHeroData.CurrentHero", GDEItemKeys.Hero_Redcroft);
            Heroes = App.Load("ClientHeroData.Heroes", new List<SavedHeroData>());
        }

        public string GetCurrentHero()
        {
            return CurrentHero;
        }
        public void SetCurrentHero(string currentHero)
        {
            if(CurrentHero != currentHero)
            {
                CurrentHero = currentHero;
                dirty = true;
            }
        }

        public List<SavedHeroData> GetHeroes()
        {
            return Heroes;
        }
        public void SetHeroes(List<SavedHeroData> heroes)
        {
            Heroes = heroes;
            dirty = true;
        }

        public override void ResetData()
        {
            CurrentHero = GDEItemKeys.Hero_Redcroft;
            Heroes = new List<SavedHeroData>();

            dirty = true;
        }

        public override void Save()
        {
            App.Save("ClientHeroData.CurrentHero", CurrentHero);
            App.Save("ClientHeroData.Heroes", Heroes);
        }

        public SavedHeroData GetHero(string id)
        {
            Heroes ??= new List<SavedHeroData>();

            var heroData = Heroes.Find(hero => hero.id == id);

            if (heroData == null)
            {
                heroData = new SavedHeroData {id = id, health = 20, level = 1};
                heroData.FixDependencies();
                Heroes.Add(heroData);

                dirty = true;
            }

            return heroData;
        }

        // public bool IsRelicSlotUnlocked(string heroId, int relicIndex)
        // {
        //     if (heroId != "Redcroft" || heroId != "Violet" || heroId != "Seto" || heroId != "Quill" || heroId != "Mereena")
        //     {
        //         return true;
        //     }
        //
        //     var heroData = GetHero(heroId);
        //
        //     if (heroData.relicSlotLockStates == null || heroData.relicSlotLockStates.Count < 4)
        //     {
        //         heroData.relicSlotLockStates = new List<bool> { true, false, false, false };
        //         dirty = true;
        //     }
        //
        //     if (relicIndex >= heroData.relicSlotLockStates.Count)
        //     {
        //         Debug.LogError($"Relic index is out of bounds ({relicIndex}).");
        //         return false;
        //     }
        //
        //     return heroData.relicSlotLockStates[relicIndex];
        // }
        //
        // public void UnlockNextRelicSlot(string heroId)
        // {
        //     var heroData = GetHero(heroId);
        //
        //     if (heroData.relicSlotLockStates == null || heroData.relicSlotLockStates.Count < 4)
        //     {
        //         heroData.relicSlotLockStates = new List<bool> { true, false, false, false };
        //         dirty = true;
        //     }
        //
        //     for (var i = 0; i < heroData.relicSlotLockStates.Count; i++)
        //     {
        //         if (heroData.relicSlotLockStates[i] == false)
        //         {
        //             heroData.relicSlotLockStates[i] = true;
        //             dirty = true;
        //             break;
        //         }
        //     }
        // }
        //
        // public void ResetRelicSlots(string heroId)
        // {
        //     var heroData = GetHero(heroId);
        //     heroData.relicSlotLockStates = new List<bool> { true, false, false, false };
        //     dirty = true;
        // }

        public void SetXp(string heroId, int xp)
        {
            var heroData = GetHero(heroId);
            heroData.xp = xp;

            dirty = true;
        }

        public void IncreaseXp(string heroId, int xp)
        {
            var heroData = GetHero(heroId);
            heroData.xp += xp;

            dirty = true;
        }

        public void ChangeHeroLevel(string heroId, int level)
        {
            var heroData = GetHero(heroId);

            if (heroData.level != level)
            {
                var levelDifference = level - heroData.level;
                heroData.levelPoints += levelDifference;
                heroData.levelPoints = (int) Mathf.Clamp(heroData.levelPoints, 0, Mathf.Infinity);
                heroData.level = level;
            }

            dirty = true;
        }

        public void IncreaseHeroHealth(string heroId, int increaseAmount)
        {
            var heroData = GetHero(heroId);

            if (Mathf.Abs(increaseAmount) > 0)
            {
                heroData.health += increaseAmount;
                Events.Publish(this, ClientAPI.GameLogic.EventSystem.EventType.OnHeroHealthChanged, new OnHeroHealthChangedEventArgs {heroId = heroId, healthChange = increaseAmount, totalHealth = heroData.health});
            }

            dirty = true;
        }

        public void OnHeroLevelRewardClaimed(string heroId, int rewardUid)
        {
            var hero = GetHero(heroId);
            hero.levelPoints--;

            var clientReward = hero.GetLevelReward(rewardUid);
            clientReward.Collected = true;

            dirty = true;
        }

        public void ResetLevelPoints(string heroId)
        {
            var hero = GetHero(heroId);
            var heroData = hero.GetData();

            hero.levelPoints = hero.level - 1;
            hero.health = 20;

            for (var i = 0; i < heroData.levelTrees.Count; i++)
            {
                for (var j = 0; j < heroData.levelTrees[i].levels.Count; j++)
                {
                    var reward = heroData.levelTrees[i].levels[j];

                    if (reward.type == LevelReward.EType.Card)
                    {
                        var card = Db.CardDatabase.GetCard(reward.cardId);
                        var cardItem = App.Inventory.GetItem(card.id);
                        if (cardItem != null) cardItem.RemainingUses = 0;
                    }
                }
            }

            for (var i = 0; i < hero.levelGridData.Count; i++)
            {
                hero.levelGridData[i].Collected = false;
                hero.levelGridData[i].RewardUid = 0;
            }

            // ResetRelicSlots(heroId);

            dirty = true;

            App.ShowSaveIcon();
            Save();
        }

        public void ResetHeroes()
        {
            for (var i = 0; i < Heroes.Count; i++)
            {
                Heroes[i].Reset();
            }

            dirty = true;

            Events.Publish(this, ClientAPI.GameLogic.EventSystem.EventType.OnHeroesReset, null);
        }
    }

    [System.Serializable]
    public class SavedHeroData
    {
        public string id;
        public int health;
        public int level;
        public int xp;
        public bool unlocked;
        public int levelPoints;
        public List<ClientLevelHexData> levelGridData;

        public ClientLevelHexData GetLevelReward(int uid)
        {
            var reward = levelGridData.Find(r => r.RewardUid == uid);

            if (reward == null)
            {
                reward = new ClientLevelHexData { Collected = false, RewardUid = uid };
                levelGridData.Add(reward);
            }

            return reward;
        }

        public bool FixDependencies()
        {
            var isDirty = false;

            if (levelGridData == null)
            {
                levelGridData = new List<ClientLevelHexData>();

                for (var i = 0; i < 30; i++)
                {
                    levelGridData.Add(new ClientLevelHexData());
                }

                isDirty = true;
            }

            return isDirty;
        }

        public void Reset()
        {
            health = 20;
            level = 1;
            xp = 0;
            levelPoints = 0;

            for (var i = 0; i < levelGridData.Count; i++)
            {
                levelGridData[i].Collected = false;
                levelGridData[i].RewardUid = 0;
            }
        }

        public HeroData GetData()
        {
            if (!Db.Initialized)
            {
                Db.Initialize();
            }

            return Db.HeroDatabase.GetHero(id);
        }
    }

    [System.Serializable]
    public class ClientLevelHexData
    {
        public bool Collected;
        public int RewardUid;
    }
}