using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Databases;
using CrossBlitz.Fables;
using CrossBlitz.Fables.Grid;
using CrossBlitz.Hero;
using CrossBlitz.PlayFab.Authentication;
using GameDataEditor;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.PlayFab.Data
{
    public enum DeckEquipContext
    {
        Pvp,

        Redcroft_Fable,
        Violet_Fable,
        Seto_Fable,
        Quill_Fable,
        Mereena_Fable,
    }

    /// <summary>
    /// This will be jsonified and stored on the server
    /// This will hold a list of decks created by the player
    /// </summary>
    [System.Serializable]
    public class PlayerDecks : ClientData
    {
        /// <summary>
        /// The number of decks that the player can have. I do this so we don't go over our set limit inside PlayFab.
        /// </summary>
        public const int DeckLimit = 20;

        /// <summary>
        /// The currently selected deck for pvp
        /// </summary>
        public string PvpEquippedDeck;

        /// <summary>
        /// All the decks the user has created (we should limit this so it doesn't go over our byte limit)
        /// </summary>
        public List<DeckData> Decks;

        public override void Init()
        {
            base.Init();

            Decks = App.Load("PlayerDecks.Decks", new List<DeckData>());
            PvpEquippedDeck = App.Load("PlayerDecks.PvpEquippedDeck", Decks.Count > 0 ? Decks[0].uid : string.Empty);
        }

        public List<DeckData> GetDecks()
        {
            return Decks;
        }

        public void SetDecks(List<DeckData> decks)
        {
            Decks = decks;
            dirty = true;
        }

        public string GetPvpEquippedDeck()
        {
            return PvpEquippedDeck;
        }

        public void SetPvpEquippedDeck(string pvpEquippedDeck)
        {
            PvpEquippedDeck = pvpEquippedDeck;
            dirty = true;
        }

        public override void ResetData()
        {
            base.ResetData();

            Decks = new List<DeckData>();
            PvpEquippedDeck = string.Empty;
            dirty = true;
        }

        public override void Save()
        {
            App.Save("PlayerDecks.PvpEquippedDeck", PvpEquippedDeck);
            App.Save("PlayerDecks.Decks", Decks);
        }

        public void ValidateDecks()
        {
            if (Decks == null || Decks.Count <= 0)
            {
                //todo: do deck init
                Debug.LogError("Decks are null or there is no count");
                return;
            }

            var decksWithSameId = Decks.GroupBy(deck => deck.uid).Where(deckGroup => deckGroup.Count() > 1);

            foreach (var deckGroup in decksWithSameId)
            {
                foreach (var deck in deckGroup)
                {
                    deck.GenerateNewId();
                }
            }

            GetDeck(DeckEquipContext.Pvp);
        }

        public void AddDeck(DeckRecipeData recipe, DeckEquipContext equipContext = DeckEquipContext.Pvp,
            bool equip = true)
        {
            AddDeck(recipe.deckData.name, recipe.deckData.hero, recipe.deckData.cards, recipe.deckData.info,
                equipContext, equip);
            dirty = true;
            //Debug.Log("AddDeck()");
        }

        public void AddDeck(string deckName, string hero, List<CardValue> cards, string info = default,
            DeckEquipContext equipContext = DeckEquipContext.Pvp, bool equip = true)
        {
            var deck = new DeckData
            {
                uid = GUID.CreateUnique(),
                name = deckName,
                hero = hero,
                info = info,
                faction = HeroData.GetFactionForHero(hero)
            };

            for (var i = 0; i < cards.Count; i++)
            {
                for (var j = 0; j < cards[i].count; j++)
                {
                    deck.AddCard(cards[i].id);
                    dirty = true;
                }
            }

            Decks.Add(deck);

            if (equip)
            {
                EquipDeck(equipContext, deck.uid);
            }

            dirty = true;

            //Debug.Log("AddDeck()");
        }

        public void EquipDeck(DeckEquipContext equipContext, string deckId)
        {
            switch (equipContext)
            {
                case DeckEquipContext.Pvp:
                    if (PvpEquippedDeck != deckId)
                    {
                        PvpEquippedDeck = deckId;
                        dirty = true;
                    }

                    break;
                case DeckEquipContext.Redcroft_Fable:
                    var rb1Book = App.FableData.GetBook(GDEItemKeys.Hero_Redcroft, "1");
                    if (rb1Book != null && rb1Book.EquippedDeck != deckId)
                    {
                        rb1Book.SetEquippedDeck(deckId);
                        dirty = true;
                    }

                    break;
                case DeckEquipContext.Violet_Fable:
                    var vb1Book = App.FableData.GetBook(GDEItemKeys.Hero_Violet, "1");
                    if (vb1Book != null && vb1Book.EquippedDeck != deckId)
                    {
                        vb1Book.SetEquippedDeck(deckId);
                        dirty = true;
                    }

                    break;
                case DeckEquipContext.Seto_Fable:
                    var sb1Book = App.FableData.GetBook(GDEItemKeys.Hero_Seto, "1");
                    if (sb1Book != null && sb1Book.EquippedDeck != deckId)
                    {
                        sb1Book.SetEquippedDeck(deckId);
                        dirty = true;
                    }

                    break;
                case DeckEquipContext.Quill_Fable:
                    var qb1Book = App.FableData.GetBook(GDEItemKeys.Hero_Quill, "1");
                    if (qb1Book != null && qb1Book.EquippedDeck != deckId)
                    {
                        qb1Book.SetEquippedDeck(deckId);
                        dirty = true;
                    }

                    break;
                case DeckEquipContext.Mereena_Fable:
                    var mb1Book = App.FableData.GetBook(GDEItemKeys.Hero_Mereena, "1");
                    if (mb1Book != null && mb1Book.EquippedDeck != deckId)
                    {
                        mb1Book.SetEquippedDeck(deckId);
                        dirty = true;
                    }

                    break;
            }

            // Debug.Log("EquipDeck()");
        }

        public void EquipPvpDeckBasedOnGameState()
        {
            EquipDeck(DeckEquipContext.Pvp, GetDeckBasedOnGameState().uid);
        }

        public DeckEquipContext GetContextBasedOnGameState()
        {
            //Debug.Log(StateController.CurrentState);

            if (FableController.PlayerIsInAFable())
            {
                switch (FableController.GetCurrentHeroName())
                {
                    case "Redcroft":
                        return DeckEquipContext.Redcroft_Fable;
                    case "Violet":
                        return DeckEquipContext.Violet_Fable;
                    case "Seto":
                        return DeckEquipContext.Seto_Fable;
                    case "Quill":
                        return DeckEquipContext.Quill_Fable;
                    case "Mereena":
                        return DeckEquipContext.Mereena_Fable;
                }
            }

            // switch (StateController.CurrentState)
            // {
            //     case StateDefinition.FABLE_ENTER when FablesMapController.Instance != null:
            //     case StateDefinition.FABLES_ROOM when FablesMapController.Instance != null:
            //         switch (FablesMapController.Instance.ChapterData.ChapterHero)
            //         {
            //             case "Redcroft":
            //                 return DeckEquipContext.Redcroft_Fable;
            //             case "Violet":
            //                 return DeckEquipContext.Violet_Fable;
            //             case "Seto":
            //                 return DeckEquipContext.Seto_Fable;
            //             case "Quill":
            //                 return DeckEquipContext.Quill_Fable;
            //             case "Mereena":
            //                 return DeckEquipContext.Mereena_Fable;
            //         }
            //         break;
            // }

            return DeckEquipContext.Pvp;
        }

        public DeckData GetDeckBasedOnGameState()
        {
            var equipContext = GetContextBasedOnGameState();
            //Debug.Log($"Getting deck in {equipContext}.");
            return GetDeck(equipContext);
        }

        public DeckData GetDeck(DeckEquipContext context)
        {
            switch (context)
            {
                case DeckEquipContext.Pvp:
                {
                    var deck = GetDeck(PvpEquippedDeck);
                    if (deck != null || Decks.Count <= 0) return deck;
                    PvpEquippedDeck = Decks[0].uid;
                    EquipDeck(context, PvpEquippedDeck);
                    deck = GetDeck(PvpEquippedDeck);
                    return deck;
                }
                case DeckEquipContext.Redcroft_Fable:
                {
                    var book = App.FableData.GetBook(GDEItemKeys.Hero_Redcroft, "1");
                    var deck = GetDeck(book.EquippedDeck);
                    if (deck == null && Decks.Exists(d => d.hero == GDEItemKeys.Hero_Redcroft))
                    {
                        book.SetEquippedDeck( Decks.Find(d => d.hero == GDEItemKeys.Hero_Redcroft).uid );
                        EquipDeck(context, book.EquippedDeck);
                        deck = GetDeck(book.EquippedDeck);
                    }

                    return deck;
                }
                case DeckEquipContext.Violet_Fable:
                {
                    var book = App.FableData.GetBook(GDEItemKeys.Hero_Violet, "1");
                    var deck = GetDeck(book.EquippedDeck);
                    if ((deck == null || deck.hero != GDEItemKeys.Hero_Violet) &&
                        Decks.Exists(d => d.hero == GDEItemKeys.Hero_Violet))
                    {
                        book.SetEquippedDeck(Decks.Find(d => d.hero == GDEItemKeys.Hero_Violet).uid);
                        EquipDeck(context, book.EquippedDeck);
                        deck = GetDeck(book.EquippedDeck);
                    }

                    return deck;
                }
                case DeckEquipContext.Seto_Fable:
                {
                    var book = App.FableData.GetBook(GDEItemKeys.Hero_Seto, "1");
                    var deck = GetDeck(book.EquippedDeck);
                    if (deck == null && Decks.Exists(d => d.hero == GDEItemKeys.Hero_Seto))
                    {
                        book.SetEquippedDeck(Decks.Find(d => d.hero == GDEItemKeys.Hero_Seto).uid);
                        EquipDeck(context, book.EquippedDeck);
                        deck = GetDeck(book.EquippedDeck);
                    }

                    return deck;
                }
                case DeckEquipContext.Quill_Fable:
                {
                    var book = App.FableData.GetBook(GDEItemKeys.Hero_Quill, "1");
                    var deck = GetDeck(book.EquippedDeck);
                    if (deck == null && Decks.Exists(d => d.hero == GDEItemKeys.Hero_Quill))
                    {
                        book.SetEquippedDeck(Decks.Find(d => d.hero == GDEItemKeys.Hero_Quill).uid);
                        EquipDeck(context, book.EquippedDeck);
                        deck = GetDeck(book.EquippedDeck);
                    }

                    return deck;
                }
                case DeckEquipContext.Mereena_Fable:
                {
                    var book = App.FableData.GetBook(GDEItemKeys.Hero_Mereena, "1");
                    var deck = GetDeck(book.EquippedDeck);
                    if (deck == null && Decks.Exists(d => d.hero == GDEItemKeys.Hero_Mereena))
                    {
                        book.SetEquippedDeck(Decks.Find(d => d.hero == GDEItemKeys.Hero_Mereena).uid);
                        EquipDeck(context, book.EquippedDeck);
                        deck = GetDeck(book.EquippedDeck);
                    }

                    return deck;
                }
            }

            return null;
        }

        public DeckData GetDeck(string uid)
        {
            if (Decks.Count <= 0)
            {
                Debug.LogError("Decks should be setup on first login, this is a problem!");
                return null;
            }

            return Decks.Find(deck => deck.uid == uid);
        }

        public DeckData GetDeckWithHero(string heroId)
        {
            if (Decks.Count <= 0)
            {
                Debug.LogError("Decks should be setup on first login, this is a problem!");
                return null;
            }

            var curr = Decks.Find(deck => deck.hero == heroId);

            if (curr == null)
            {
                curr = Decks[0];
            }

            return curr;
        }

        public DeckData GetRandomDeck()
        {
            return Decks[Random.Range(0, Decks.Count)];
        }

        public bool DeleteDeck(string uid)
        {
            //Debug.Log("DeleteDeck()");
            var removals = Decks.RemoveAll(deck => deck.uid == uid);
            dirty = true;
            return removals > 0;
        }
    }
}