using CrossBlitz.Audio;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.Settings;
using UnityEngine;

namespace CrossBlitz.PlayFab.Data
{
    public enum CrtFilterOption
    {
        Off,
        ScanLines,
        Arcade
    }

    public class ClientSettings : ClientData
    {
        public float MasterVolume;
        public float MusicVolume;
        public float SfxVolume;
        public float AmbienceVolume;
        public int ResolutionScale;
        public FullScreenMode ScreenMode;
        public CrtFilterOption FilterOption;
        public int FilterIntensity;
        public bool SkipCutscenes;
        public bool SkipBattles;
        public bool DeckEditorCardAnimationsEnabled;
        public bool NewCardNotificationsEnabled;
        public bool ShowDebugHistories;
        public bool DisplayRewardsWhenSkippingBattles;
        public int FablesSpeedUpCharacter;
        public int ZoomedCardPopupDelay;
        public int ScreenShake;
        public int ScreenFlash;
        public bool NoBattleRewards;
        public bool UnlockAllNodes;
        public bool UnlimitedPlayerHP;
        public bool UnlimitedCardHP;
        public bool UnlimitedMana;
        public bool UnlockAllCards;
        public int DamageDisplaySpeed;
        public int CardDisplaySpeed;

        public override void Init()
        {
            MasterVolume = App.Load("PlayerData.MasterVolume", 0.5f);
            MusicVolume = App.Load("PlayerData.MusicVolume", 0.5f);
            SfxVolume = App.Load("PlayerData.SfxVolume", 0.5f);
            ResolutionScale = App.Load("PlayerData.ResolutionScale",3);
            ScreenMode = App.Load("PlayerData.ScreenMode", FullScreenMode.Windowed);
            FilterOption = App.Load("PlayerData.FilterOption", CrtFilterOption.Off);
            FilterIntensity = App.Load("PlayerData.FilterIntensity", 2);
            SkipCutscenes = App.Load("PlayerData.SkipCutscenes", false);
            SkipBattles = App.Load("PlayerData.SkipBattles", false);
            DeckEditorCardAnimationsEnabled = App.Load("PlayerData.DeckEditorCardAnimationsEnabled", true);
            ShowDebugHistories = App.Load("PlayerData.ShowDebugHistories", false);
            DisplayRewardsWhenSkippingBattles = App.Load("PlayerData.DisplayRewardsWhenSkippingBattles", false);
            FablesSpeedUpCharacter = App.Load("PlayerData.FablesSpeedUpCharacter", 1);
            ZoomedCardPopupDelay = App.Load("PlayerData.ZoomedCardPopupDelay", 1);
            ScreenShake = App.Load("PlayerData.ScreenShake", 2);
            ScreenFlash = App.Load("PlayerData.ScreenFlash", 1);
            NoBattleRewards = App.Load("PlayerData.NoBattleRewards", false);
            UnlockAllNodes = App.Load("PlayerData.UnlockAllNodes", false);
            UnlimitedPlayerHP = App.Load("PlayerData.UnlimitedPlayerHP", false);
            UnlimitedCardHP = App.Load("PlayerData.UnlimitedCardHP", false);
            UnlimitedMana = App.Load("PlayerData.UnlimitedMana", false);
            UnlockAllCards = App.Load("PlayerData.UnlockAllCards", false);
            NewCardNotificationsEnabled = App.Load("PlayerData.NewCardNotificationsEnabled", true);
            AmbienceVolume = App.Load("PlayerData.AmbienceVolume", 0.5f);
            DamageDisplaySpeed = App.Load("PlayerData.DamageDisplaySpeed", 3);
            CardDisplaySpeed = App.Load("PlayerData.CardDisplaySpeed", 2);


            AudioController.SetMasterVolume(MasterVolume);
            AudioController.SetMusicVolume(MusicVolume);
            AudioController.SetSoundVolume(SfxVolume);
            AudioController.SetAmbienceVolume(AmbienceVolume);

            switch (FilterOption)
            {
                case CrtFilterOption.Off:
                    FilterController.Instance.scanlines.enabled = false;
                    FilterController.Instance.arcade.enabled = false;
                    break;
                case CrtFilterOption.ScanLines:
                    FilterController.Instance.scanlines.enabled = true;
                    FilterController.Instance.arcade.enabled = false;
                    break;
                case CrtFilterOption.Arcade:
                    FilterController.Instance.scanlines.enabled = false;
                    FilterController.Instance.arcade.enabled = true;
                    break;
            }

            FilterController.Instance.scanlines.Fade = (FilterIntensity+1) *.2f;
            FilterController.Instance.arcade.Fade = (FilterIntensity+1) * .2f;
        }

        public float GetMasterVolume()
        {
            return MasterVolume;
        }
        public void SetMasterVolume(float masterVolume)
        {
            if (MasterVolume != masterVolume)
            {
                MasterVolume = masterVolume;
                dirty = true;
            }
        }

        public float GetMusicVolume()
        {
            return MusicVolume;
        }
        public void SetMusicVolume(float musicVolume)
        {
            if (MusicVolume != musicVolume)
            {
                MusicVolume = musicVolume;
                dirty = true;
            }
        }

        public float GetSfxVolume()
        {
            return SfxVolume;
        }
        public void SetSfxVolume(float sfxVolume)
        {
            if (SfxVolume != sfxVolume)
            {
                SfxVolume = sfxVolume;
                dirty = true;
            }
        }

        public int GetResolutionScale()
        {
            return ResolutionScale;
        }
        public void SetResolutionScale(int resolutionScale)
        {
            if (ResolutionScale != resolutionScale)
            {
                ResolutionScale = resolutionScale;
                dirty = true;
            }
        }

        public FullScreenMode GetScreenMode()
        {
            return ScreenMode;
        }
        public void SetScreenMode(FullScreenMode mode)
        {
            if (ScreenMode != mode)
            {
                ScreenMode = mode;
                dirty = true;
            }
        }

        public CrtFilterOption GetFilterOption()
        {
            return FilterOption;
        }
        public void SetFilterOption(CrtFilterOption filter)
        {
            if (FilterOption != filter)
            {
                FilterOption = filter;
                dirty = true;
            }
        }

        public int GetFilterIntensity()
        {
            return FilterIntensity;
        }
        public void SetFilterIntensity(int filterIntensity)
        {
            if (FilterIntensity != filterIntensity)
            {
                FilterIntensity = filterIntensity;
                dirty = true;
            }
        }

        public bool GetSkipCutscenes()
        {
            return SkipCutscenes;
        }
        public void SetSkipCutscenes(bool skipCutscenes)
        {
            if (SkipCutscenes != skipCutscenes)
            {
                SkipCutscenes = skipCutscenes;
                dirty = true;
            }
        }

        public bool GetSkipBattles()
        {
            return SkipBattles;
        }
        public void SetSkipBattles(bool skipBattles)
        {
            if (SkipBattles != skipBattles)
            {
                SkipBattles = skipBattles;
                dirty = true;
            }
        }

        public bool GetDeckEditorCardAnimationsEnabled()
        {
            return DeckEditorCardAnimationsEnabled;
        }
        public void SetDeckEditorCardAnimationsEnabled(bool deckEditorCardAnimationsEnabled)
        {
            if (DeckEditorCardAnimationsEnabled != deckEditorCardAnimationsEnabled)
            {
                DeckEditorCardAnimationsEnabled = deckEditorCardAnimationsEnabled;
                dirty = true;
            }
        }

        public bool GetShowDebugHistories()
        {
            return ShowDebugHistories;
        }
        public void SetShowDebugHistories(bool showDebugHistories)
        {
            if (ShowDebugHistories != showDebugHistories)
            {
                ShowDebugHistories = showDebugHistories;
                dirty = true;
            }
        }

        public bool GetDisplayRewardsWhenSkippingBattles()
        {
            return DisplayRewardsWhenSkippingBattles;
        }
        public void SetDisplayRewardsWhenSkippingBattles(bool displayRewardsWhenSkippingBattles)
        {
            if (DisplayRewardsWhenSkippingBattles != displayRewardsWhenSkippingBattles)
            {
                DisplayRewardsWhenSkippingBattles = displayRewardsWhenSkippingBattles;
                dirty = true;
            }
        }

        public int GetFablesSpeedUpCharacter()
        {
            return FablesSpeedUpCharacter;
        }
        public void SetFablesSpeedUpCharacter(int fablesSpeedUpCharacter)
        {
            if (FablesSpeedUpCharacter != fablesSpeedUpCharacter)
            {
                FablesSpeedUpCharacter = fablesSpeedUpCharacter;
                dirty = true;
            }
        }

        public int GetZoomedCardPopupDelay()
        {
            return ZoomedCardPopupDelay;
        }
        public void SetZoomedCardPopupDelay(int zoomedCardPopupDelay)
        {
            if (ZoomedCardPopupDelay != zoomedCardPopupDelay)
            {
                ZoomedCardPopupDelay = zoomedCardPopupDelay;
                dirty = true;
            }
        }

        public int GetScreenShake()
        {
            return ScreenShake;
        }
        public void SetScreenShake(int screenShake)
        {
            if (ScreenShake != screenShake)
            {
                ScreenShake = screenShake;
                dirty = true;
            }
        }

        public int GetScreenFlash()
        {
            return ScreenFlash;
        }
        public void SetScreenFlash(int screenFlash)
        {
            if (ScreenFlash != screenFlash)
            {
                ScreenFlash = screenFlash;
                dirty = true;
            }
        }

        public bool GetNoBattleRewards()
        {
            return NoBattleRewards;
        }
        public void SetNoBattleRewards(bool noBattleRewards)
        {
            if (NoBattleRewards != noBattleRewards)
            {
                NoBattleRewards = noBattleRewards;
                dirty = true;
            }
        }

        public bool GetUnlockAllNodes()
        {
            return UnlockAllNodes;
        }
        public void SetUnlockAllNodes(bool unlockAllNodes)
        {
            if (UnlockAllNodes != unlockAllNodes)
            {
                UnlockAllNodes = unlockAllNodes;
                dirty = true;
            }
        }

        public bool GetUnlimitedPlayerHP()
        {
            return UnlimitedPlayerHP;
        }
        public void SetUnlimitedPlayerHP(bool unlimitedPlayerHP)
        {
            if (UnlimitedPlayerHP != unlimitedPlayerHP)
            {
                UnlimitedPlayerHP = unlimitedPlayerHP;
                dirty = true;
            }
        }

        public bool GetUnlimitedCardHP()
        {
            return UnlimitedCardHP;
        }
        public void SetUnlimitedCardHP(bool unlimitedCardHP)
        {
            if (UnlimitedCardHP != unlimitedCardHP)
            {
                UnlimitedCardHP = unlimitedCardHP;
                dirty = true;
            }
        }

        public bool GetUnlimitedMana()
        {
            return UnlimitedMana;
        }
        public void SetUnlimitedMana(bool unlimitedMana)
        {
            if (UnlimitedMana != unlimitedMana)
            {
                UnlimitedMana = unlimitedMana;
                dirty = true;
            }
        }

        public bool GetUnlockAllCards()
        {
            return UnlockAllCards;
        }
        public void SetUnlockAllCards(bool unlockAllCards)
        {
            if (UnlockAllCards != unlockAllCards)
            {
                UnlockAllCards = unlockAllCards;
                dirty = true;
            }
        }

        public bool GetNewCardNotificationsEnabled()
        {
            return NewCardNotificationsEnabled;
        }
        public void SetNewCardNotificationsEnabled(bool newCardNotificationsEnabled)
        {
            if (NewCardNotificationsEnabled != newCardNotificationsEnabled)
            {
                NewCardNotificationsEnabled = newCardNotificationsEnabled;
                dirty = true;
            }
        }

        public float GetAmbienceVolume()
        {
            return AmbienceVolume;
        }
        public void SetAmbienceVolume(float ambienceVolume)
        {
            if (AmbienceVolume != ambienceVolume)
            {
                AmbienceVolume = ambienceVolume;
                dirty = true;
            }
        }

        public int GetDamageDisplaySpeed()
        {
            return DamageDisplaySpeed;
        }
        public void SetDamageDisplaySpeed(int damageDisplaySpeed)
        {
            if (DamageDisplaySpeed != damageDisplaySpeed)
            {
                DamageDisplaySpeed = damageDisplaySpeed;
                dirty = true;
            }
        }

        public int GetCardDisplaySpeed()
        {
            return CardDisplaySpeed;
        }
        public void SetCardDisplaySpeed(int cardDisplaySpeed)
        {
            if (CardDisplaySpeed != cardDisplaySpeed)
            {
                CardDisplaySpeed = cardDisplaySpeed;
                dirty = true;
            }
        }

        public override void ResetData()
        {
            ResolutionScale = 3;
            ScreenMode = FullScreenMode.Windowed;
            FilterOption = CrtFilterOption.Off;
            FilterIntensity = 3;
            SkipCutscenes = false;
            SkipBattles = false;
            DeckEditorCardAnimationsEnabled = true;
            ShowDebugHistories = false;
            DisplayRewardsWhenSkippingBattles = false;
            FablesSpeedUpCharacter = 1;
            ZoomedCardPopupDelay = 1;
            ScreenShake = 2;
            ScreenFlash = 1;
            NoBattleRewards = false;
            UnlockAllNodes = false;
            UnlimitedPlayerHP = false;
            UnlimitedCardHP = false;
            UnlimitedMana = false;
            UnlockAllCards = false;
            NewCardNotificationsEnabled = true;
            DamageDisplaySpeed = 3;
            CardDisplaySpeed = 2;

            AudioController.SetMasterVolume(MasterVolume);
            AudioController.SetMusicVolume(MusicVolume);
            AudioController.SetSoundVolume(SfxVolume);
            AudioController.SetAmbienceVolume(AmbienceVolume);

            dirty = true;
        }

        public override void Save()
        {
            App.Save("PlayerData.MasterVolume", MasterVolume);
            App.Save("PlayerData.MusicVolume", MusicVolume);
            App.Save("PlayerData.SfxVolume", SfxVolume);
            App.Save("PlayerData.ResolutionScale", ResolutionScale);
            App.Save("PlayerData.ScreenMode", ScreenMode);
            App.Save("PlayerData.FilterOption", FilterOption);
            App.Save("PlayerData.FilterIntensity", FilterIntensity);
            App.Save("PlayerData.SkipCutscenes", SkipCutscenes);
            App.Save("PlayerData.SkipBattles", SkipBattles);
            App.Save("PlayerData.DeckEditorCardAnimationsEnabled", DeckEditorCardAnimationsEnabled);
            App.Save("PlayerData.ShowDebugHistories", ShowDebugHistories);
            App.Save("PlayerData.DisplayRewardsWhenSkippingBattles", DisplayRewardsWhenSkippingBattles);
            App.Save("PlayerData.FablesSpeedUpCharacter", FablesSpeedUpCharacter);
            App.Save("PlayerData.ZoomedCardPopupDelay", ZoomedCardPopupDelay);
            App.Save("PlayerData.ScreenShake", ScreenShake);
            App.Save("PlayerData.ScreenFlash", ScreenFlash);
            App.Save("PlayerData.NoBattleRewards", NoBattleRewards);
            App.Save("PlayerData.UnlockAllNodes", UnlockAllNodes);
            App.Save("PlayerData.UnlimitedPlayerHP", UnlimitedPlayerHP);
            App.Save("PlayerData.UnlimitedCardHP", UnlimitedCardHP);
            App.Save("PlayerData.UnlimitedMana", UnlimitedMana);
            App.Save("PlayerData.UnlockAllCards", UnlockAllCards);
            App.Save("PlayerData.NewCardNotificationsEnabled", NewCardNotificationsEnabled);
            App.Save("PlayerData.AmbienceVolume", AmbienceVolume);
            App.Save("PlayerData.DamageDisplaySpeed", DamageDisplaySpeed);
            App.Save("PlayerData.CardDisplaySpeed", CardDisplaySpeed);
        }
    }
}