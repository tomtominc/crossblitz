﻿using CrossBlitz.PlayFab.Authentication;

namespace CrossBlitz.PlayFab.Data
{
    [System.Serializable]
    public class PlayerData : ClientData
    {
        public bool IsNewUser;
        public int PvpRank;

        public override void Init()
        {
            base.Init();

            IsNewUser = App.Load("PlayerData.IsNewUser", true);
            PvpRank = App.Load("PlayerData.PvpRank", 0);
        }

        public bool GetIsNewUser()
        {
            return IsNewUser;
        }

        public void SetIsNewUser(bool isNewUser)
        {
            if(IsNewUser != isNewUser)
            {
                IsNewUser = isNewUser;
                dirty = true;
            }
        }

        public int GetPvpRank()
        {
            return PvpRank;
        }

        public void SetPvpRank(int pvpRank)
        {
            if (PvpRank != pvpRank)
            {
                PvpRank = pvpRank;
                dirty = true;
            }
        }

        public override void ResetData()
        {
            IsNewUser = true;
            PvpRank = 0;

            dirty = true;
        }

        public override void Save()
        {
            App.Save("PlayerData.IsNewUser", IsNewUser);
            App.Save("PlayerData.PvpRank", PvpRank);
        }
    }
}
