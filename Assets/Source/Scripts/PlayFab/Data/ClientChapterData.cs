using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Grid;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI;
using UnityEngine;
using UnityEngine.Rendering;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.PlayFab.Data
{
    [System.Serializable]
    // All save data for the chapter goes here - we'll want to save hex tile states and map states etc
    // there will be a lot of data stored here!
    public class ClientChapterData
    {
        public bool Initialized;
        public bool Unlocked;
        public string ChapterUid;
        public string ChapterId;
        public int ChapterNumber;

        public string CurrentMapUid;
        public string CurrentRoomUid;
        public int CurrentRoomIndex;

        public int TileOccupiedByPlayerUid;

        public bool IsComplete;
        public bool HasSeenIntro;

        public List<string> SeenCutscenes;
        public List<ClientFableQuestData> QuestData;
        public Dictionary<string, ClientRoomData> Rooms;

        private bool _dirty;

        public bool IsDirty()
        {
            return _dirty;
        }

        public void SetIsDirty(bool isDirty)
        {
            _dirty = isDirty;
        }

        public void Init(string chapterUid)
        {
            var chapterData = Db.FablesDatabase.GetChapter(chapterUid);
            var map = Db.MapDatabase.GetMap(chapterData.mapId);

            Initialized = true;
            ChapterUid = chapterUid;
            ChapterId = chapterData.ChapterId;
            ChapterNumber = chapterData.ChapterNumber;
            IsComplete = false;
            HasSeenIntro = false;
            Rooms = new Dictionary<string, ClientRoomData>();
            QuestData = new List<ClientFableQuestData>();
            SeenCutscenes = new List<string>();

            if (map == null)
            {
                return;
            }

            InitRooms(map);
        }


        public void InitRooms(FableMapData map)
        {
            //var extraMaps = new List<FableMapData>();
            var addedMaps = new List<FableMapData>();
            var maps = new Queue<FableMapData>();
            maps.Enqueue(map);

            while (maps.Count > 0)
            {
                var nextMap = maps.Dequeue();
                addedMaps.Add(nextMap);

                for (var j = 0; j < nextMap.Rooms.Count; j++)
                {
                    GetRoomData(nextMap.Rooms[j].Uid);

                    for (var k = 0; k < nextMap.Rooms[j].Tiles.Count; k++)
                    {
                        SetTile(nextMap.Rooms[j].Uid, nextMap.Rooms[j].Tiles[k]);

                        var tile = nextMap.Rooms[j].Tiles[k];

                        if (tile.entranceTileUid != 0)
                        {
                            var mapData = Db.MapDatabase.GetMapByTileUid(tile.entranceTileUid);

                            //Debug.Log($"Tile: {tile.displayName} Tile Entrance:{ Db.MapDatabase.GetTile( tile.entranceTileUid ).displayName } Has Map: {mapData.Uid}");
                            if (!addedMaps.Contains(mapData))
                            {
                                maps.Enqueue(mapData);
                            }
                        }
                    }
                }
            }
        }

        public void Init(FableChapterData chapterData)
        {
            if (QuestData == null || QuestData.Count <= 0 || !QuestData.Exists(q => q.QuestUid == chapterData.MainQuest))
            {
                GetFableQuest(chapterData.MainQuest);
            }
            QuestData = QuestData.OrderBy(q => q.QuestUid == chapterData.MainQuest ? 0 : 1).ToList();
        }

        public void FixDependencies()
        {
            if (string.IsNullOrEmpty(ChapterUid))
            {
                Debug.LogError("Chapter has not been setup!");
                return;
            }

            var chapterData = Db.FablesDatabase.GetChapter(ChapterUid);

            if (string.IsNullOrEmpty(chapterData.mapId))
            {
                Debug.LogWarning($"No map has been set for {chapterData.ChapterName}.");
                return;
            }

            var map = Db.MapDatabase.GetMap(chapterData.mapId);

            if (map == null)
            {
                Debug.LogError($"No map at {chapterData.ChapterName}. Map ID = {chapterData.mapId}");
                return;
            }

            if (Rooms == null || Rooms.Count <= 0)
            {
                ChapterId = chapterData.ChapterId;
                ChapterNumber = chapterData.ChapterNumber;
                IsComplete = false;
                HasSeenIntro = false;
                Rooms = new Dictionary<string, ClientRoomData>();
                QuestData = new List<ClientFableQuestData>();
                SeenCutscenes = new List<string>();
            }

            InitRooms(map);

            if (ChapterNumber == 1)
            {
                SetUnlocked(true);
            }

            // todo: The removals dont work because this is only 1 map and the room has several
            //
            // var roomRemovals = new List<string>();
            // var tileRemovals = new Dictionary<string, int>();
            //
            // foreach (var roomValue in Rooms)
            // {
            //     var roomData = map.GetRoom(roomValue.Key);
            //
            //     if (roomData == null)
            //     {
            //         roomRemovals.Add(roomValue.Key);
            //         continue;
            //     }
            //
            //     foreach (var tilePair in roomValue.Value.TileData)
            //     {
            //         var tile = roomData.GetTile(tilePair.Key);
            //
            //         if (tile == null)
            //         {
            //             tileRemovals.Add(roomValue.Key, tilePair.Key);
            //         }
            //     }
            // }
            //
            // for (var i = 0; i < roomRemovals.Count; i++)
            // {
            //     Rooms.Remove(roomRemovals[i]);
            // }
            //
            // foreach (var tileRemoval in tileRemovals)
            // {
            //     Rooms[tileRemoval.Key].TileData.Remove(tileRemoval.Value);
            // }
        }

        public void Reset()
        {
            CurrentMapUid = string.Empty;
            CurrentRoomUid = string.Empty;
            CurrentRoomIndex = 0;
            TileOccupiedByPlayerUid = 0;
            IsComplete = false;
            HasSeenIntro = false;
            SeenCutscenes = new List<string>();

            if (Rooms == null)
            {
                Rooms = new SerializedDictionary<string, ClientRoomData>();
            }

            foreach (var room in Rooms)
            {
                room.Value.Reset();

            }

            if (QuestData == null)
            {
                QuestData = new List<ClientFableQuestData>();
            }

            for (var i = 0; i < QuestData.Count; i++)
            {
                QuestData[i].Reset();
            }

            SetIsDirty(true);
        }

        public bool isChapterReset()
        {
            return !HasSeenIntro;
        }

        public bool IsUnlocked()
        {
            return Unlocked;
        }

        public void SetUnlocked(bool unlocked)
        {
            if (Unlocked != unlocked)
            {
                Unlocked = unlocked;
                _dirty = true;
            }
        }

        public void SetChapterUid(string chapterUid)
        {
            if (ChapterUid != chapterUid)
            {
                ChapterUid = chapterUid;
                SetIsDirty(true);
            }
        }

        public void SetHasSeenIntro(bool hasSeenIntro)
        {
            if (HasSeenIntro != hasSeenIntro)
            {
                HasSeenIntro = hasSeenIntro;
                SetIsDirty(true);
            }
        }

        public void SetCurrentRoomAndSave(string mapUid, string roomId, int roomIndex)
        {
            CurrentMapUid = mapUid;
            CurrentRoomUid = roomId;
            CurrentRoomIndex = roomIndex;

            SetIsDirty(true);
        }

        public void SetCurrentMap(string mapUid)
        {
            if (CurrentMapUid != mapUid)
            {
                CurrentMapUid = mapUid;
                SetIsDirty(true);
            }
        }

        private string GetQuestObjective(int index)
        {
            var objective = string.Empty;
            var questData = QuestData[index];
            var quest = Db.FableQuestDatabase.GetQuest(questData.QuestUid);

            if (quest != null && quest.QuestObjectives.Count > questData.CurrentStep)
            {
                objective = quest.QuestObjectives[questData.CurrentStep];
            }

            return objective;
        }

        public string GetMainQuestObjective()
        {
            return GetQuestObjective(0);
        }

        public int GetMainQuestStep()
        {
            if (QuestData !=null && QuestData.Count > 0) return QuestData[0].CurrentStep;
            return 0;
        }

        public bool HasSideQuest()
        {
            return QuestData != null && QuestData.Count > 1 && QuestData[1].IsActive;
        }

        public string GetSideQuestObjective()
        {
            var hasSideQuest = HasSideQuest();

            if (hasSideQuest == false)
            {
                return string.Empty;
            }

            return GetQuestObjective(1);
        }

        public void SetPlayerTile(int playerTileUid)
        {
            TileOccupiedByPlayerUid = playerTileUid;
        }

        public ClientTileData GetTile(int tileUid)
        {
            foreach (var room in Rooms)
            {
                if (room.Value.TileData.ContainsKey(tileUid)) return room.Value.TileData[tileUid];
            }

            return null;
        }

        public void MarkAllPendingTilesAsComplete(string roomId)
        {
            var room = GetRoomData(roomId);
            if (room != null)
            {
                foreach (var tile in room.TileData)
                {
                    if (tile.Value.PendingComplete)
                    {
                        SetTileAsComplete(tile.Key, true);
                    }
                }
            }
        }

        public void SetTileAsPendingComplete(int tileUid)
        {
            var tile = GetTile(tileUid);
            tile.PendingComplete = true;
        }

        public void SetTileAsComplete(int tileUid, bool markCutsceneSeen)
        {
            var tile = GetTile(tileUid);

            tile.Complete = true;
            tile.PendingComplete = false;

            var tileData = Db.MapDatabase.GetTile(tileUid);

            if (!string.IsNullOrEmpty(tileData.introCutsceneUid) && markCutsceneSeen)
            {
                MarkCutsceneAsSeen(tileData.introCutsceneUid);
            }

            if (!string.IsNullOrEmpty(tileData.outroCutsceneUid) && markCutsceneSeen)
            {
                MarkCutsceneAsSeen(tileData.outroCutsceneUid);
            }
        }

        public void MarkCutsceneAsSeen(string cutsceneUid)
        {
            if (SeenCutscenes == null)
            {
                SeenCutscenes = new List<string>();
                SetIsDirty(true);
            }

            if (!SeenCutscenes.Contains(cutsceneUid))
            {
                SeenCutscenes.Add(cutsceneUid);
                SetIsDirty(true);
            }
        }

        /// <summary>
        /// Get accolade counts.
        /// Usage = GetAccoladeCounts(out var completeCount, out var maxCount);
        /// </summary>
        /// <param name="completeCount"></param>
        /// <param name="maxCount"></param>
        public void GetAccoladeCounts(out int completeCount, out int maxCount)
        {
            completeCount = 0;
            maxCount = 0;

            foreach (var roomPair in Rooms)
            {
                var room = roomPair.Value;

                foreach (var tilePair in room.TileData)
                {
                    var tile = tilePair.Value;

                    if (tile.BattleData != null && tile.BattleData.Accolades != null)
                    {
                        for (var i = 0; i < tile.BattleData.Accolades.Count; i++)
                        {
                            maxCount++;

                            if (tile.BattleData.Accolades[i].IsComplete)
                            {
                                completeCount++;
                            }
                        }
                    }
                }
            }
        }

        public string GetChapterPlaytime()
        {
            var chapterData = Db.FablesDatabase.GetChapter(ChapterUid);
            var chapterPlayTime = App.FableData.GetChapterPlayTime(chapterData.ChapterHero, chapterData.BookNumber, chapterData.ChapterNumber);
            return new TimeSpan(0, 0, 0, (int)chapterPlayTime.TotalChapterPlayTime).ToString("g");
        }

        public bool HasSeenCutscene(string cutsceneUid)
        {
#if UNITY_EDITOR
            if (Server.Instance.customGameOptions.showAllSeenCutscenes)
            {
                Debug.Log("Showing all seen cutscenes!");
                return false;
            }
#endif

            if (SeenCutscenes == null)
            {
                SeenCutscenes = new List<string>();
            }

            return SeenCutscenes.Contains(cutsceneUid);
        }

        public ClientTileData SetTile(string roomUid, FableTileData tileData)
        {
            var room = GetRoomData(roomUid);
            return room.GetTile(tileData);
        }

        public ClientRoomData GetRoomData(string roomUid)
        {
            if (Rooms == null)
            {
                Rooms = new SerializedDictionary<string, ClientRoomData>();
            }

            if (string.IsNullOrEmpty(roomUid))
            {
                return null;
            }

            if (!Rooms.ContainsKey(roomUid))
            {
                Rooms.Add(roomUid, new ClientRoomData
                {
                    RoomUid = roomUid,
                    Explored = false,
                    TileData = new Dictionary<int, ClientTileData>()
                });

                SetIsDirty(true);
            }

            return Rooms[roomUid];
        }

        public bool FinishedAllAccoladesInRoom(string roomUid, bool printDebug=false)
        {
            var room = GetRoomData(roomUid);

            foreach (var tile in room.TileData)
            {
                if (tile.Value.BattleData?.Accolades != null)
                {
                    var battle = Db.BattleDatabase.GetBattleData(tile.Value.BattleData.BattleUid);
                    tile.Value.BattleData.FixDependencies(battle);

                    for (var j = 0; j < tile.Value.BattleData.Accolades.Count; j++)
                    {
                        var accolade = tile.Value.BattleData.Accolades[j];

                        if (!string.IsNullOrEmpty(accolade.AccoladeUid) && !accolade.IsComplete)
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        public bool CanShowQuestNode(string questUid, int step)
        {
            var quest = GetFableQuest(questUid);

            if (App.Settings.GetUnlockAllNodes())
            {
                return true;
            }

            return quest.CurrentStep >= step;
        }

        public void ResolveQuestIfApplicable(CutsceneData cutsceneData)
        {
            if (!string.IsNullOrEmpty(cutsceneData.QuestUid))
            {
                if (cutsceneData.IncrementQuestStepInstead)
                {
                    IncrementQuestStep(cutsceneData.QuestUid, cutsceneData.QuestStepChange);
                }
                else
                {
                    SetQuestStep(cutsceneData.QuestUid, cutsceneData.QuestStepChange);
                }

                SetIsDirty(true);
            }
        }

        public void SetQuestStep(string questUid, int step)
        {
            var quest = GetFableQuest(questUid);
            quest.CurrentStep = step > quest.CurrentStep ? step : quest.CurrentStep;
            var questData = Db.FableQuestDatabase.GetQuest(quest.QuestUid);
            quest.IsActive = questData.QuestSteps > quest.CurrentStep;
            Debug.Log($"Setting Quest {questData.Name} ({quest.IsActive}) ({quest.CurrentStep} / {questData.QuestSteps})");
            Events.Publish(this, EventType.OnPlayerQuestsChanged, new OnPlayerQuestsChangedEventArgs
            {
                readOnlyChapterData = this
            });

            SetIsDirty(true);
        }

        public void IncrementQuestStep(string questUid, int incrementBy)
        {
            var quest = GetFableQuest(questUid);
            quest.CurrentStep+=incrementBy;
            var questData = Db.FableQuestDatabase.GetQuest(quest.QuestUid);
            quest.IsActive = questData.QuestSteps > quest.CurrentStep;
            Events.Publish(this, EventType.OnPlayerQuestsChanged, new OnPlayerQuestsChangedEventArgs
            {
                readOnlyChapterData = this
            });

            SetIsDirty(true);
        }

        public bool SetQuestNodeAsRevealed(int tileUid)
        {
            var tile = GetTile(tileUid);

            if (!tile.QuestNodeRevealed)
            {
                SetIsDirty(true);
                tile.QuestNodeRevealed = true;
                return true;
            }

            return false;
        }

        public ClientFableQuestData GetFableQuest(string questUid)
        {
            if (QuestData == null)
            {
                QuestData = new List<ClientFableQuestData>();
                SetIsDirty(true);
            }

            if (QuestData.Count > 2)
            {
                for (var i = QuestData.Count - 1; i > 1; i--)
                {
                    QuestData.RemoveAt(i);
                    SetIsDirty(true);
                }
            }

            var quest = QuestData.Find(q => q.QuestUid == questUid);

            if (quest == null)
            {
                var questData = Db.FableQuestDatabase.GetQuest(questUid);

                quest = new ClientFableQuestData
                {
                    QuestUid = questUid,
                    CurrentStep = 0
                };

                if (questData.IsSideQuest)
                {
                    if (QuestData.Count > 1)
                    {
                        QuestData[1] = quest;
                        SetIsDirty(true);
                    }
                    else if (QuestData.Count > 0)
                    {
                        QuestData.Add(quest);
                        SetIsDirty(true);
                    }
                    else // needs to add 2 because the Side-Quest needs to be in the [1] spot.
                    {
                        QuestData.Add( new ClientFableQuestData());
                        QuestData.Add(quest);
                        SetIsDirty(true);
                    }
                }
                else if (QuestData.Count > 0)
                {
                    QuestData[0] = quest;
                    SetIsDirty(true);
                }
                else
                {
                    QuestData.Add(quest);
                    SetIsDirty(true);
                }
            }

            return quest;
        }
    }

    [System.Serializable]
    public class ClientFableQuestData
    {
        public string QuestUid;
        public int CurrentStep;
        public bool IsActive;
        public bool HasBeenCompleted; // true throughout run resets

        public void Reset()
        {
            CurrentStep = 0;
            IsActive = false;
        }
    }

    [System.Serializable]
    public class ClientRoomData
    {
        public string RoomUid;
        public bool Explored;
        public Dictionary<int, ClientTileData> TileData;

        public void Reset()
        {
            Explored = false;

            foreach (var tile in TileData)
            {
                var tileData = Db.MapDatabase.GetTile(tile.Value.TileUid);
                if (tileData.NodeType != NodeType.Chest) tile.Value.Reset();
            }
        }

        public ClientTileData GetTile(FableTileData tileData, bool addNewData = false)
        {
            if (TileData == null)
            {
                TileData = new Dictionary<int, ClientTileData>();
            }

            if (!TileData.ContainsKey(tileData.uid))
            {
                var newTile = new ClientTileData();
                newTile.Init(tileData);
                TileData.Add(newTile.TileUid, newTile);
            }

            var tile = TileData[tileData.uid];

            if (addNewData)
            {
                var battleData = Db.BattleDatabase.GetBattleData(tileData.battleUid);

                if (battleData != null)
                {
                    for (var i = 0; i < battleData.Accolades.Count; i++)
                    {
                        var accolade = battleData.Accolades[i];

                        if (accolade == null) continue;

                        var clientAccoladeData = tile.BattleData.GetAccolade(accolade.Uid);

                        if (tile.BattleData == null) continue;

                        if (clientAccoladeData == null)
                        {
                            tile.BattleData.Accolades.Add(new ClientAccoladeData {AccoladeUid = accolade.Uid});
                        }
                    }
                }
            }

            return tile;
        }
    }

    [System.Serializable]
    public class ClientTileData
    {
        public int TileUid;
        public bool PendingComplete;
        public bool Complete;
        public bool QuestNodeRevealed;
        public ClientBattleData BattleData;

        public void Reset()
        {
            PendingComplete = false;
            Complete = false;
            QuestNodeRevealed = false;

            if (BattleData != null)
            {
                BattleData.Reset();
            }
        }

        public void Init(FableTileData tile)
        {
            TileUid = tile.uid;
            // Revealed = !tile.hiddenAtStart;
            Complete = false;
            PendingComplete = false;

            var battleData = Db.BattleDatabase.GetBattleData(tile.battleUid);

            if (battleData != null)
            {
                BattleData = new ClientBattleData
                {
                    BattleUid = battleData.Uid,
                    IsComplete = false,
                    Accolades = new List<ClientAccoladeData>()
                };

                BattleData.FixDependencies(battleData);
            }
            else if (BattleData == null)
            {
                BattleData = new ClientBattleData();
            }
        }
    }

    [System.Serializable]
    public class ClientBattleData
    {
        public string BattleUid;
        public bool IsComplete;
        public List<ClientAccoladeData> Accolades;

        public ClientAccoladeData GetAccolade(string uid)
        {
            Accolades ??= new List<ClientAccoladeData>();

            return string.IsNullOrEmpty(uid) ? null : Accolades?.Find(a => a.AccoladeUid == uid);
        }

        public void SetCompleteStatus(bool complete)
        {
            IsComplete = complete;
        }

        public void Reset()
        {
            SetCompleteStatus(false);

            if (Accolades != null)
            {
                for (var i = 0; i < Accolades.Count; i++)
                {
                    Accolades[i].Reset();
                }
            }
        }

        public void FixDependencies(BattleData battleData)
        {
            if (battleData?.Accolades == null || battleData.Accolades.Count <= 0)
            {
                return;
            }

            Accolades ??= new List<ClientAccoladeData>();

            var removals = Accolades.RemoveAll(a => string.IsNullOrEmpty(a.AccoladeUid));

            for (var i = 0; i < battleData.Accolades.Count; i++)
            {
                if (string.IsNullOrEmpty(battleData.Accolades[i].Uid))
                {
                    battleData.Accolades[i].UpdateIdentifier();
                }

                if (i >= Accolades.Count)
                {
                    Accolades.Add(new ClientAccoladeData
                    {
                        AccoladeUid = battleData.Accolades[i].Uid,
                        IsComplete = false,
                        IsPending = false
                    });
                }

                Accolades[i].AccoladeUid = battleData.Accolades[i].Uid;
            }

            if (Accolades.Count < battleData.Accolades.Count)
            {
                // App.FableData.Save();
            }
        }
    }

    [System.Serializable]
    public class ClientAccoladeData
    {
        public string AccoladeUid;
        public bool IsComplete;
        public bool IsPending;

        public void Reset()
        {
            // doesn't need to be touched
        }
    }
}