using System.Collections.Generic;
using CrossBlitz.PlayFab.Authentication;

namespace CrossBlitz.PlayFab.Data
{
    public class ClientTutorialData : ClientData
    {
        public List<string> SeenTutorials;

        public override void Init()
        {
            base.Init();

            SeenTutorials = App.Load("ClientTutorialData.SeenTutorials", new List<string>());
        }

        public void OnTutorialShown( string tutorial )
        {
            if (!SeenTutorials.Contains(tutorial))
            {
                SeenTutorials.Add(tutorial);
                dirty = true;
            }
        }

        public bool HasSeenTutorial(string tutorial)
        {
            return SeenTutorials.Contains(tutorial);
        }

        public void ClearSeenTutorials()
        {
            SeenTutorials = new List<string>();
            dirty = true;
        }

        public override void Save()
        {
            App.Save("ClientTutorialData.SeenTutorials", SeenTutorials);
        }
    }
}