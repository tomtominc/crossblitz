using System.Collections.Generic;
using CrossBlitz.ClientAPI;
using CrossBlitz.Databases;
using CrossBlitz.Fables;
using CrossBlitz.PlayFab.Authentication;
using Newtonsoft.Json;
using UnityEngine;

namespace CrossBlitz.PlayFab.Data
{
    [System.Serializable]
    public class ChapterPlayTime
    {
        public float TotalChapterPlayTime;
        public float TotalTimeWatchingCutscenes;
        public float TotalTimeInBattles;
        public float TotalTimeInShops;
        public float TotalTimeMelding;
        public float TotalTimeDeckEditing;
    }

    public class ClientFableData : ClientData
    {
        private string m_currentBookUid;
        private string m_currentChapterUid;

        [JsonIgnore]
        public ClientBookData CurrentBookData
        {
            get
            {
                if (!string.IsNullOrEmpty(m_currentBookUid) && Books.ContainsKey(m_currentBookUid))
                {
                    return Books[m_currentBookUid];
                }

                return null;
            }
        }

        [JsonIgnore]
        public ClientChapterData CurrentChapterData
        {
            get
            {
                if (!string.IsNullOrEmpty(m_currentChapterUid) && ChapterUidToId.ContainsKey(m_currentChapterUid) && Chapters.ContainsKey( ChapterUidToId[ m_currentChapterUid ]))
                {
                    return Chapters[ChapterUidToId[ m_currentChapterUid ]];
                }

                return null;
            }
        }

        /// <summary>
        /// The "book" holds various data that is persistent across chapters like the store and how many dawn dollars the player has.
        /// </summary>
        public Dictionary<string, ClientBookData> Books;

        /// <summary>
        /// Chapters carry all the room data and
        /// </summary>
        public Dictionary<string, ClientChapterData> Chapters;

        /// <summary>
        /// Chapters carry all the room data and
        /// </summary>
        public Dictionary<string, string> ChapterUidToId;

        public Dictionary<string, ChapterPlayTime> ChapterTotalPlayTimes;

        public override bool IsDirty()
        {
            var isDirty = false;
            if(CurrentChapterData != null)
            {
                if (CurrentChapterData.IsDirty()) isDirty = true;
            }
            if (CurrentBookData != null)
            {
                if (CurrentBookData.IsDirty()) isDirty = true;
            }
            if (base.IsDirty())
            {
                isDirty = true;
            }

            return isDirty;
        }
        public override void SetIsDirty(bool isDirty)
        {
            dirty = isDirty;

            if (CurrentChapterData != null)
            {
                CurrentChapterData.SetIsDirty(isDirty);
            }

            if (CurrentBookData != null)
            {
                CurrentBookData.SetIsDirty(isDirty);
            }
        }

        public override void Init()
        {
            base.Init();

            ChapterTotalPlayTimes = App.Load("ClientFableData.ChapterTotalPlayTimes", new Dictionary<string, ChapterPlayTime>());

            ChapterUidToId = new Dictionary<string, string>();
            Chapters = new Dictionary<string, ClientChapterData>();
            Books = new Dictionary<string, ClientBookData>();

            foreach (var fableBook in Db.FablesDatabase.Books)
            {
                var book = GetBook(fableBook.BookId); // get also creates
                book.FixDependencies();

                foreach (var chapter in fableBook.Chapters)
                {
                    var chap = GetChapter(chapter.Uid); // get also creates
                    chap.FixDependencies();
                }
            }

            foreach (var chapter in Chapters)
            {
                if (!ChapterUidToId.ContainsKey(chapter.Value.ChapterId))
                {
                    ChapterUidToId.Add(chapter.Value.ChapterId, chapter.Value.ChapterUid);
                }
            }
        }
        public void OnOpenFable()
        {
            Timers.Start($"{FableController.CurrentBookName}-{FableController.CurrentBookNumber}-{FableController.CurrentChapterNumber}" );

            m_currentBookUid = $"{FableController.CurrentBookName}-{FableController.CurrentBookNumber}";
            m_currentChapterUid = FableController.GetCurrentChapterId();
        }

        public void OnClosedFable()
        {
            StopAllTimers();

            m_currentBookUid = string.Empty;
            m_currentChapterUid = string.Empty;

        }

        public void OnEnteredShop()
        {
            Timers.Start($"{FableController.CurrentBookName}-" +
                         $"{FableController.CurrentBookNumber}-" +
                         $"{FableController.CurrentChapterNumber}-Shops" );
        }

        public void OnExitShop()
        {
            Timers.Stop($"{FableController.CurrentBookName}-" +
                        $"{FableController.CurrentBookNumber}-" +
                        $"{FableController.CurrentChapterNumber}-Shops");
        }

        public void OnEnteredCutscene()
        {
            Timers.Start($"{FableController.CurrentBookName}-" +
                         $"{FableController.CurrentBookNumber}-" +
                         $"{FableController.CurrentChapterNumber}-Cutscenes" );
        }

        public void OnExitCutscene()
        {
            Timers.Stop($"{FableController.CurrentBookName}-" +
                        $"{FableController.CurrentBookNumber}-" +
                        $"{FableController.CurrentChapterNumber}-Cutscenes");
        }

        public void OnEnterBattle()
        {
            Timers.Start($"{FableController.CurrentBookName}-" +
                         $"{FableController.CurrentBookNumber}-" +
                         $"{FableController.CurrentChapterNumber}-Battles" );
        }

        public void OnExitBattle()
        {
            Timers.Stop($"{FableController.CurrentBookName}-" +
                        $"{FableController.CurrentBookNumber}-" +
                        $"{FableController.CurrentChapterNumber}-Battles");
        }

        public void OnEnterMeld()
        {
            Timers.Start($"{FableController.CurrentBookName}-" +
                         $"{FableController.CurrentBookNumber}-" +
                         $"{FableController.CurrentChapterNumber}-Meld" );
        }

        public void OnExitMeld()
        {
            Timers.Stop($"{FableController.CurrentBookName}-" +
                         $"{FableController.CurrentBookNumber}-" +
                         $"{FableController.CurrentChapterNumber}-Meld" );
        }

        public void OnEnterDeckEdit()
        {
            Timers.Start($"{FableController.CurrentBookName}-" +
                         $"{FableController.CurrentBookNumber}-" +
                         $"{FableController.CurrentChapterNumber}-DeckEdit" );
        }

        public void OnExitDeckEdit()
        {
            Timers.Stop($"{FableController.CurrentBookName}-" +
                         $"{FableController.CurrentBookNumber}-" +
                         $"{FableController.CurrentChapterNumber}-DeckEdit" );
        }

        public void StopAllTimers()
        {
            foreach (var fableBook in Db.FablesDatabase.Books)
            {
                var book = GetBook(fableBook.BookId);

                foreach (var chapter in fableBook.Chapters)
                {
                    Timers.Stop($"{book.BookHero}-{book.BookNumber}-{chapter.ChapterNumber}");
                    Timers.Stop($"{book.BookHero}-{book.BookNumber}-{chapter.ChapterNumber}-Cutscenes");
                    Timers.Stop($"{book.BookHero}-{book.BookNumber}-{chapter.ChapterNumber}-Battles");
                    Timers.Stop($"{book.BookHero}-{book.BookNumber}-{chapter.ChapterNumber}-Shops");
                    Timers.Stop($"{book.BookHero}-{book.BookNumber}-{chapter.ChapterNumber}-Meld");
                    Timers.Stop($"{book.BookHero}-{book.BookNumber}-{chapter.ChapterNumber}-DeckEdit");
                }
            }
        }

        public override void ResetData()
        {
            Books = new Dictionary<string, ClientBookData>();
            Chapters = new Dictionary<string, ClientChapterData>();
            ChapterUidToId = new Dictionary<string, string>();
            ChapterTotalPlayTimes = new Dictionary<string, ChapterPlayTime>();

            StopAllTimers();

            SetIsDirty(true);
        }

        public override void Save()
        {
            if (FableController.PlayerIsInAFable() && CurrentBookData != null && CurrentChapterData != null)
            {
                SetBook(CurrentBookData);
                SetChapter(CurrentChapterData);
            }

            foreach (var fableBook in Db.FablesDatabase.Books)
            {
                var book = GetBook(fableBook.BookId);

                foreach (var chapter in fableBook.Chapters)
                {
                    var chapterPlayTime = GetChapterPlayTime(book.BookHero, book.BookNumber, chapter.ChapterNumber);
                    chapterPlayTime.TotalChapterPlayTime += Timers.GetWatch($"{book.BookHero}-{book.BookNumber}-{chapter.ChapterNumber}").Time;
                    chapterPlayTime.TotalTimeWatchingCutscenes += Timers.GetWatch($"{book.BookHero}-{book.BookNumber}-{chapter.ChapterNumber}-Cutscenes").Time;
                    chapterPlayTime.TotalTimeInBattles += Timers.GetWatch($"{book.BookHero}-{book.BookNumber}-{chapter.ChapterNumber}-Battles").Time;
                    chapterPlayTime.TotalTimeInShops += Timers.GetWatch($"{book.BookHero}-{book.BookNumber}-{chapter.ChapterNumber}-Shops").Time;
                    chapterPlayTime.TotalTimeMelding += Timers.GetWatch($"{book.BookHero}-{book.BookNumber}-{chapter.ChapterNumber}-Meld").Time;
                    chapterPlayTime.TotalTimeDeckEditing += Timers.GetWatch($"{book.BookHero}-{book.BookNumber}-{chapter.ChapterNumber}-DeckEdit").Time;

                    Timers.Restart($"{book.BookHero}-{book.BookNumber}-{chapter.ChapterNumber}");
                    Timers.Restart($"{book.BookHero}-{book.BookNumber}-{chapter.ChapterNumber}-Cutscenes");
                    Timers.Restart($"{book.BookHero}-{book.BookNumber}-{chapter.ChapterNumber}-Battles");
                    Timers.Restart($"{book.BookHero}-{book.BookNumber}-{chapter.ChapterNumber}-Shops");
                    Timers.Restart($"{book.BookHero}-{book.BookNumber}-{chapter.ChapterNumber}-Meld");
                    Timers.Restart($"{book.BookHero}-{book.BookNumber}-{chapter.ChapterNumber}-DeckEdit");
                }
            }

            App.Save("ClientFableData.ChapterTotalPlayTimes", ChapterTotalPlayTimes);

            if (CurrentBookData != null && CurrentBookData.IsDirty())
            {
                App.Save($"ClientFableData.Books.{CurrentBookData.BookId}", CurrentBookData);
            }

            if (CurrentChapterData != null && CurrentChapterData.IsDirty())
            {
                App.Save($"ClientFableData.Chapters.{CurrentChapterData.ChapterUid}", CurrentChapterData);
            }
        }

        public ChapterPlayTime GetChapterPlayTime(string bookHero, string bookNumber, int chapterNumber)
        {
            var id = $"{bookHero}-{bookNumber}-{chapterNumber}";

            if (!ChapterTotalPlayTimes.ContainsKey(id))
            {
                ChapterTotalPlayTimes.Add(id, new ChapterPlayTime());
            }

            return ChapterTotalPlayTimes[id];
        }

        private void SetBook(ClientBookData bookData)
        {
            Books[bookData.BookId] = bookData;
        }

        private void SetChapter(ClientChapterData chapterData)
        {
            Chapters[chapterData.ChapterUid] = chapterData;
        }

        public int GetDawnDollarAmount()
        {
            var book = GetCurrentBook();

            return book?.DawnDollars ?? 0;
        }

        public void SetDawnDollarAmount(int amount)
        {
            var bookData = GetCurrentBook();

            if (bookData != null)
            {
                bookData.SetDawnDollars( (int) Mathf.Clamp(amount, 0, Mathf.Infinity) );
            }

            SetIsDirty(true);
        }

        public ClientBookData GetBook(string chapterHero, string bookNumber)
        {
            var bookId = $"{chapterHero}-{bookNumber}";
            return GetBook(bookId);
        }

        public ClientBookData GetBook(string bookId)
        {
            if (!Books.ContainsKey(bookId))
            {
                var book = App.Load($"ClientFableData.Books.{bookId}", new ClientBookData());

                if (!book.Initialized)
                {
                    book.Initialize(bookId);
                }

                Books.Add(bookId, book);
                SetIsDirty(true);
            }

            return Books[bookId];
        }

        public ClientChapterData GetChapter(string uid)
        {
            if (!Chapters.ContainsKey(uid))
            {
                var chapter = App.Load($"ClientFableData.Chapters.{uid}", new ClientChapterData());

                if (!chapter.Initialized)
                {
                    chapter.Init(uid);
                }

                Chapters.Add(uid, chapter);
                ChapterUidToId.Add(chapter.ChapterId, uid);
                SetIsDirty(true);
            }

            return Chapters[uid];
        }

        public ClientChapterData GetChapter(string bookHero, int bookNumber, int chapterNumber)
        {
            var chapterId = $"{bookHero}-{bookNumber}-{chapterNumber}";

            if (ChapterUidToId.ContainsKey(chapterId))
            {
                return GetChapter(ChapterUidToId[chapterId]);
            }

            Debug.LogError($"FATAL! NO CHAPTER WITH ID {chapterId}");
            return null;
        }

        public bool ResetChapter(string uid)
        {
            var chapter = GetChapter(uid);

            if (chapter != null)
            {
                chapter.Reset();
                return true;
            }

            return false;
        }

        public ClientChapterData GetCurrentChapter()
        {
            return CurrentChapterData;
        }

        public ClientBookData GetCurrentBook()
        {
            return CurrentBookData;
        }

        public ClientTileData GetTile(int tileUid)
        {
            foreach (var chapter in Chapters)
            {
                var tile = chapter.Value.GetTile(tileUid);

                if (tile != null)
                {
                    return tile;
                }
            }

            Debug.LogError($"This tile id {tileUid} was not found.");

            return null;
        }
    }
}