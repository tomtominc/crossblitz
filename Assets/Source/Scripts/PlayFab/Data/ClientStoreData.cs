using System;
using System.Collections.Generic;
using CrossBlitz.Utils;
using UnityEditor;
using UnityEngine;

namespace CrossBlitz.PlayFab.Data
{
    [Serializable]
    public class ClientStoreData
    {
        public void Init()
        {
            Items = new List<ClientStoreItemData>();
        }

        /// <summary>
        /// All the items in the shop, this is per book
        /// </summary>
        public List<ClientStoreItemData> Items;

        private bool m_dirty;

        public bool IsDirty()
        {
            return m_dirty;
        }

        public void SetIsDirty(bool isDirty)
        {
            m_dirty = isDirty;
        }

        public ClientStoreItemData GetItem(string itemId)
        {
            Items ??= new List<ClientStoreItemData>();

            var item = Items.Find(item => item.ItemId == itemId);

            if (item == null)
            {
                item = new ClientStoreItemData { ItemId = itemId, isNew = true };
                Items.Add(item);
            }
            else
            {
                Items.Find(item => item.ItemId == itemId).isNew = false;
            }

            return item;
        }
        public bool HasItem(string itemId)
        {
            return GetItem(itemId) != null;
        }

        public void AddItem(string itemId)
        {
            if (!HasItem(itemId))
            {
                Items.Add(new ClientStoreItemData { ItemId = itemId });
                SetIsDirty(true);
            }
        }

        public void RemoveItem(string itemId)
        {
            if (HasItem(itemId))
            {
                Items.RemoveAll(item => item.ItemId == itemId);
                SetIsDirty(true);
            }
        }

        public void PurchasedItem(string itemId)
        {
            SetIsDirty(true);
        }
    }

    [Serializable]
    public class ClientStoreItemData : IPageItemData
    {
        /// <summary>
        /// The item id for this item, you can get the item by using Db.ShopDatabase.GetItem(ItemId);
        /// </summary>
        public string ItemId;
        public bool isNew;
    }
}