namespace CrossBlitz.PlayFab.Data
{
    public class ClientData
    {
        protected bool dirty;

        public virtual void Init()
        {

        }

        public virtual void Save()
        {

        }

        public virtual void OnApplicationClose()
        {

        }

        public virtual void OnUpdate()
        {

        }

        public virtual void ResetData()
        {

        }

        public virtual bool IsDirty()
        {
            return dirty;
        }

        public virtual void SetIsDirty(bool isDirty)
        {
            dirty = isDirty;
        }

        protected bool IsOnline()
        {
            if (InternetReachabilityVerifier.Instance == null) return false;
            return InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified;
        }

        protected virtual void OnNetworkStatusChanged(InternetReachabilityVerifier.Status status)
        {

        }
    }
}