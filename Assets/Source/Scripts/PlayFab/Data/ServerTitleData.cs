using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;

namespace CrossBlitz.PlayFab.Data
{
    public abstract class ServerTitleData
    {
        protected long LastUpdated;

        public void Save()
        {
            LastUpdated = DateTime.UtcNow.Ticks;

            var saveData = GetSaveData();

            foreach (var saveReq in saveData)
            {
                PlayerPrefs.SetString(saveReq.Key, saveReq.Value);
            }

            var req = new UpdateUserDataRequest
            {
                Permission = UserDataPermission.Public,
                Data = GetSaveData()
            };

            PlayFabClientAPI.UpdateUserData(req, OnSaveSuccessful, OnSaveFailed);
        }

        protected virtual void OnSaveSuccessful(UpdateUserDataResult result)
        {
            var req = ((UpdateUserDataRequest) result.Request).Data;

            foreach (var saveReq in req)
            {
                Debug.Log($"Save Successful: {saveReq.Key}");
            }
        }

        protected virtual void OnSaveFailed(PlayFabError error)
        {
            var saveData = GetSaveData();

            foreach (var saveReq in saveData)
            {
                Debug.LogError($"Save Failed: {saveReq.Key}");
            }
        }

        public abstract string GetKey();
        protected abstract Dictionary<string, string> GetSaveData();
        protected abstract ServerTitleData Default<T>() where T : ServerTitleData;

        public static T Load<T>(string dataKey, UserDataRecord dataRecord) where T : ServerTitleData, new()
        {
            var dataValue = PlayerPrefs.GetString(dataKey, string.Empty);

            if (string.IsNullOrEmpty(dataValue) && dataRecord == null)
            {
                return (T) new T().Default<T>();
            }

            if (string.IsNullOrEmpty(dataValue))
            {
                dataValue = dataRecord.Value;
            }

            var data = JsonConvert.DeserializeObject<T>(dataValue);
            var serverData = JsonConvert.DeserializeObject<T>(dataRecord.Value);

            if (serverData.LastUpdated > data.LastUpdated)
            {
                data = serverData;
            }

            return data;
        }

        protected string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}