﻿using System.Collections.Generic;
using UnityEngine;

namespace CrossBlitz.PlayFab.Data
{
    public static class CrossBlitzTitleData
    {
        private const string ConfigurationKey = "configuration";

        public static TitleDataConfiguration config;

        public static void SetTitleData(Dictionary<string, string> titleData)
        {
            if (!titleData.ContainsKey(ConfigurationKey)) return;
            var configurationJson = titleData[ConfigurationKey].UnZip();
            config = JsonUtility.FromJson<TitleDataConfiguration>(configurationJson);
        }
    }

    [System.Serializable]
    public class TitleDataConfiguration
    {
        public List<string> assetBundles;
    }
}