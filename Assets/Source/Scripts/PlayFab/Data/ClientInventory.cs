using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Hero;
using CrossBlitz.Items.Data;
using CrossBlitz.PlayFab.Authentication;
using QFSW.QC;
using Sirenix.Utilities;
using TakoBoyStudios.Events;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.PlayFab.Data
{
    public class ClientInventory : ClientData
    {
        public Dictionary<string, int> Currency;
        public Dictionary<string, ItemDataInstance> Items;

        public override void Init()
        {
            base.Init();

            Currency = App.Load("ClientInventory.Currency", new Dictionary<string, int>());
            Items = App.Load("ClientInventory.Items", new Dictionary<string, ItemDataInstance>());

            Events.Subscribe(EventType.ModifyItemUses, OnModifyItemUses);
            Events.Subscribe(EventType.ModifyItemsUses, OnModifyItemsUses);
            Events.Subscribe(EventType.ModifyCurrency, OnModifyCurrency);
        }

        public Dictionary<string, int> GetCurrency()
        {
            return Currency;
        }
        public void SetCurrency(Dictionary<string, int> currency)
        {
            Currency = currency;
            dirty = true;
        }

        public Dictionary<string, ItemDataInstance> GetItems()
        {
            return Items;
        }
        public void SetItems(Dictionary<string, ItemDataInstance> items)
        {
            Items = items;
            dirty = true;
        }

        public override void ResetData()
        {
            base.ResetData();

            Currency = new Dictionary<string, int>();
            Items = new Dictionary<string, ItemDataInstance>();
            dirty = true;
            Events.Unsubscribe(EventType.ModifyItemUses, OnModifyItemUses);
            Events.Unsubscribe(EventType.ModifyItemsUses, OnModifyItemsUses);
            Events.Unsubscribe(EventType.ModifyCurrency, OnModifyCurrency);
        }

        public override void Save()
        {
            App.Save("ClientInventory.Currency", Currency);
            App.Save("ClientInventory.Items", Items);
        }

        public override void OnApplicationClose()
        {
            base.OnApplicationClose();
            Events.Unsubscribe(EventType.ModifyItemUses, OnModifyItemUses);
            Events.Unsubscribe(EventType.ModifyItemsUses, OnModifyItemsUses);
            Events.Unsubscribe(EventType.ModifyCurrency, OnModifyCurrency);
        }

        [Command("modify-currency")]
        public static void ModifyCurrency(string currencyCode, int newAmount)
        {
            Events.Publish(null, EventType.ModifyCurrency, new ModifyCurrencyEventArgs
            {
                CurrencyCode = currencyCode,
                Amount = newAmount
            });
        }

        [Command("modify-item-amount")]
        public static void ModifyItemAmount(string itemId, int newAmount)
        {
            Events.Publish(null, EventType.ModifyItemUses, new ModifyItemUsagesEventArgs
            {
                ItemName = itemId,
                UseAmount = newAmount
            });
        }

        private void OnModifyItemsUses(IMessage message)
        {
            if (message.Data is ModifyItemsUsagesEventArgs args)
            {
                for (var i = 0; i < args.ItemNames.Count; i++)
                {
                    OnModifyItemUses(args.ItemNames[i], args.UseAmounts[i], false);
                }

                Events.Publish(this, EventType.OnModifiedItemUses,
                    new OnModifiedItemUsesEventArgs { ItemNames = args.ItemNames } );
            }
        }

        private void OnModifyItemUses(IMessage message)
        {
            if (message.Data is ModifyItemUsagesEventArgs args)
            {
                OnModifyItemUses(args.ItemName, args.UseAmount, true);
            }
        }

        private void OnModifyItemUses(string itemId, int useAmount, bool sendEvent)
        {
            var item = GetItem(itemId);

            if (item != null)
            {
                item.RemainingUses += useAmount;
                SetItem(item,sendEvent);
            }
            else if (useAmount > 0)
            {
                GrantItem(itemId, 1, new ItemAcquisitionData
                {
                    Receipt = ItemAcquisitionData.GetReceipt(),
                    Annotation = ItemAcquisitionData.AnnotationReason.PURCHASED,
                    Purchased = false,
                });

                useAmount--;

                if (useAmount > 0)
                {
                    OnModifyItemUses(itemId, useAmount, sendEvent);
                }
            }

            dirty = true;
        }

        private void OnModifyCurrency(IMessage message)
        {
            if (message.Data is ModifyCurrencyEventArgs args)
            {
                if (args.Amount > 0)
                {
                    AddCurrency(args.CurrencyCode, args.Amount);
                }
                else
                {
                    SubtractCurrency(args.CurrencyCode, -args.Amount);
                }
            }

            dirty = true;
        }

        public bool HasMaxedOrMoreOfCard(string cardId)
        {
            if (!Db.Initialized)
            {
                Db.Initialize();
            }

            var cardAmount = GetItemAmount(cardId);
            var card = Db.CardDatabase.GetCard(cardId);

            if (card == null) return true;

            return card.rarity switch
            {
                Rarity.Common => cardAmount >= DeckData.MaxCopiesOfCommonCards,
                Rarity.Rare => cardAmount >= DeckData.MaxCopiesOfRareCards,
                Rarity.Mythic => cardAmount >= DeckData.MaxCopiesOfMysticCards,
                Rarity.Legendary => cardAmount >= DeckData.MaxCopiesOfLegendaryCards,
                _ => true
            };
        }

        public int GetItemAmount(string itemId)
        {
            if (Items.ContainsKey(itemId))
            {
                return Items[itemId].RemainingUses;
            }

            return 0;
        }

        public ItemDataInstance GetItem(string itemId)
        {
            if (Items == null)
            {
                return null;
            }

            if (string.IsNullOrEmpty(itemId))
            {
                return null;
            }

            if (Items.ContainsKey(itemId))
            {
                return Items[itemId];
            }

            return null;
        }

        public void SetItem(ItemDataInstance instance, bool sendEvent)
        {
            // Check for card cap of 99
            if (instance.ItemClass == "Card")
            {
                if (instance.RemainingUses > 99)
                {
                    instance.RemainingUses = 99;
                }
            }

            if (Items.ContainsKey(instance.ItemId))
            {
                Items[instance.ItemId] = instance;
            }
            else
            {
                Items.Add(instance.ItemId, instance);
            }

            if (sendEvent)
            {
                Events.Publish(this, EventType.OnModifiedItemUses,
                    new OnModifiedItemUsesEventArgs
                    {
                        ItemNames = new List<string> {instance.ItemId}
                    });
            }

            dirty = true;
        }

        public bool OwnsItem(string itemId)
        {
            return GetItem(itemId) != null;
        }

        public bool OwnsCard(string cardId)
        {
            return GetItemAmount(cardId) > 0;
        }

        public bool HasAnyNewCards()
        {
            var cards = GetItemsByType(ItemClassType.Card);
            return cards.Any(c => c.IsNew );
        }

        public bool HasAnyNewCards(Faction faction)
        {
            var cards = GetItemsByType(ItemClassType.Card);
            return cards.Any(c => c.IsNew && App.Settings.GetNewCardNotificationsEnabled() && (Db.CardDatabase.GetCard(c.ItemId).faction == faction || Db.CardDatabase.GetCard(c.ItemId).faction == Faction.Neutral));
        }

        public bool HasAnyNewCardsExclusve()
        {
            if (!App.Settings.GetNewCardNotificationsEnabled()) return false;

            var cards = GetItemsByType(ItemClassType.Card);
            foreach (var card in cards)
            {
                if (card.IsNew)
                {
                    var cardView = Db.CardDatabase.GetCard(card.ItemId);
                    if (cardView.type != CardType.Relic && cardView.type != CardType.Elder_Relic)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool HasAnyNewRelics()
        {
            if (!App.Settings.GetNewCardNotificationsEnabled()) return false;

            var cards = GetItemsByType(ItemClassType.Card);
            foreach (var card in cards)
            {
                if (card.IsNew)
                {
                    var cardView = Db.CardDatabase.GetCard(card.ItemId);

                    if (cardView != null)
                    {
                        if (cardView.type == CardType.Relic || cardView.type == CardType.Elder_Relic)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public bool HasAnyNewCardBacks()
        {
            if (!App.Settings.GetNewCardNotificationsEnabled()) return false;

            var cards = GetItemsByType(ItemClassType.CardBack);
            return cards.Any(c => c.IsNew);
        }

        public List<ItemDataInstance> GetItemsByType(string itemType)
        {
            return Items.Values.ToList().FindAll(item => item.ItemClass == itemType);
        }

        public AddItemResult GrantItems(List<string> items)
        {
            return GrantItems(items.Convert(i => new ItemValue {ItemUid = (string)i, Count = 1}).ToList());
        }

        public AddItemResult GrantItems(List<ItemValue> items)
        {
            return GrantItems(items.Select(item => item.ItemUid).ToList(), items.Select(item => item.Count).ToList());
        }

        public AddItemResult GrantItems(List<string> itemIds, List<int> grantAmounts)
        {
            var fullResult = new AddItemResult
            {
                AddedItems = new List<ItemDataInstance>(),
                AddedCurrencies = new Dictionary<string, int>()
            };

            if (grantAmounts.Count < itemIds.Count)
            {
                for (var i = grantAmounts.Count; i < itemIds.Count; i++)
                {
                    grantAmounts.Add(1);
                }
            }

            for (var i = 0; i < itemIds.Count; i++)
            {
                //Debug.Log($"Granting= {itemIds[i]} x{grantAmounts[i]}");

                var result = GrantItem(itemIds[i], grantAmounts[i], new ItemAcquisitionData
                {
                    Receipt = ItemAcquisitionData.GetReceipt(),
                    Annotation = ItemAcquisitionData.AnnotationReason.GRANTED,
                });

                fullResult.AddedItems.AddRange(result.AddedItems);
            }

            var itemsCombined = new List<ItemDataInstance>();

            for (var i = 0; i < fullResult.AddedItems.Count; i++)
            {
                var addedItem = fullResult.AddedItems[i];
                var instance = itemsCombined.Find(item => item.ItemId == addedItem.ItemId);

                if (instance != null)
                {
                    instance.Merge(addedItem);
                }
                else
                {
                    itemsCombined.Add(addedItem);
                }
            }

            fullResult.AddedItems = itemsCombined;

            dirty = true;

            return fullResult;
        }

        public AddItemResult GrantItem(string itemId, int grantAmount, ItemAcquisitionData acquisition)
        {
            var result = new AddItemResult
            {
                AddedItems = new List<ItemDataInstance>(),
                AddedCurrencies = new Dictionary<string, int>()
            };

            var item = Db.ItemDatabase.GetItem(itemId);

            if (item == null)
            {
                Debug.LogError($"Could not find item with id: {itemId}");
                return result;
            }

            if (item.IsBundle())
            {
                result = item.OpenBundle(acquisition);
            }
            else
            {
                var itemInstance = Db.ItemDatabase.CreateItemInstance(item, acquisition);

                if (itemInstance != null)
                {
                    itemInstance.RemainingUses = grantAmount;
                    result.AddedItems.Add(itemInstance);
                }
            }

            for (var i = 0; i < result.AddedItems.Count; i++)
            {
                var clientInstance = GetItem(result.AddedItems[i].ItemId);

                if (clientInstance == null)
                {
                    clientInstance = result.AddedItems[i];
                }
                else
                {
                    clientInstance.Merge(result.AddedItems[i]);
                }

                SetItem(clientInstance, true);
            }

            foreach (var currency in result.AddedCurrencies)
            {
                AddCurrency(currency.Key, currency.Value);
            }

            UnlockHero(itemId);

            dirty = true;
            return result;
        }

        public AddItemResult PurchaseItem(string itemId, int grantAmount, string currencyCode,
            ItemAcquisitionData acquisition, out PurchaseErrorCode errorCode)
        {
            var result = new AddItemResult();
            var item = Db.ItemDatabase.GetItem(itemId);
            var currencyPrice = item.GetCurrencyPrice(currencyCode);

            errorCode = PurchaseErrorCode.NONE;

            if (!acquisition.IsValidItem())
            {
                errorCode = PurchaseErrorCode.RECEIPT_NOT_VALID;
                return result;
            }

            if (currencyPrice < 0)
            {
                errorCode = PurchaseErrorCode.CANNOT_PURCHASE_USING_CURRENCY_TYPE;
                return result;
            }

            var clientAmount = GetCurrencyAmount(currencyCode);

            if (clientAmount < currencyPrice)
            {
                errorCode = PurchaseErrorCode.NOT_ENOUGH_CURRENCY;
                return result;
            }

            SubtractCurrency(currencyCode, currencyPrice);
            result = GrantItem(itemId, grantAmount, acquisition);

            dirty = true;
            return result;
        }

        public int GetCurrencyAmount(string currencyCode)
        {
            if (currencyCode == "DD")
            {
                return App.FableData.GetDawnDollarAmount();
            }

            return Currency.ContainsKey(currencyCode) ? Currency[currencyCode] : 0;
        }

        public void AddCurrency(string currencyCode, int amount)
        {
            if (!Currency.ContainsKey(currencyCode))
            {
                Currency.Add(currencyCode, amount);
            }
            else
            {
                Currency[currencyCode] += amount;
            }

            dirty = true;
            Events.Publish(this, EventType.OnModifiedCurrency,
                new OnModifiedCurrencyEventArgs {CurrencyCode = currencyCode, AmountLeft = Currency[currencyCode]});
            // App.Save("Currency", Currency);
        }

        public void SubtractCurrency(string currencyCode, int amount)
        {
            if (currencyCode == "DD")
            {
                App.FableData.SetDawnDollarAmount(App.FableData.GetDawnDollarAmount() - amount);
                Events.Publish(this, EventType.OnModifiedCurrency,
                    new OnModifiedCurrencyEventArgs {CurrencyCode = currencyCode, AmountLeft = App.FableData.GetDawnDollarAmount()});
                return;
            }

            if (!Currency.ContainsKey(currencyCode))
            {
                Currency.Add(currencyCode, 0);
            }
            else
            {
                Currency[currencyCode] -= amount;
                if (Currency[currencyCode] < 0) Currency[currencyCode] = 0;
            }

            dirty = true;
            Events.Publish(this, EventType.OnModifiedCurrency,
                new OnModifiedCurrencyEventArgs {CurrencyCode = currencyCode, AmountLeft = Currency[currencyCode]});
            // App.Save("Currency", Currency);
        }

        public void UnlockHero(string heroID)
        {
            if(heroID == "Redcroft" || heroID == "Violet" || heroID == "Mereena" || heroID == "Violet" || heroID == "Quill" || heroID == "Seto")
            App.HeroData.GetHero(heroID).unlocked = true;

            dirty = true;
        }

        public static void GrantAndDisplayItems(List<string> items, bool display = true)
        {
            if (!Db.Initialized)
            {
                Db.Initialize();
            }

            if (!App.Initialized)
            {
                App.Init();
            }

            var fullResult = new AddItemResult
            {
                AddedItems = new List<ItemDataInstance>(),
                AddedCurrencies = new Dictionary<string, int>()
            };

            for (var i = 0; i < items.Count; i++)
            {
                var result = App.Inventory.GrantItem(items[i],1, new ItemAcquisitionData
                {
                    Receipt = ItemAcquisitionData.GetReceipt(),
                    Annotation = ItemAcquisitionData.AnnotationReason.GRANTED,
                });

                fullResult.AddedItems.AddRange(result.AddedItems);
            }

            var itemsCombined = new List<ItemDataInstance>();

            for (var i = 0; i < fullResult.AddedItems.Count; i++)
            {
                var addedItem = fullResult.AddedItems[i];
                var instance = itemsCombined.Find(item => item.ItemId == addedItem.ItemId);

                if (instance != null)
                {
                    instance.Merge(addedItem);
                }
                else
                {
                    itemsCombined.Add(addedItem);
                }
            }

            fullResult.AddedItems = itemsCombined;

            if (display) App.HandleItemGrant(fullResult,null, PurchaseErrorCode.NONE, false);
        }

        public static void GrantAndDisplayItems(List<ItemValue> items, List<ItemValue> currencies, bool display = true)
        {
            if (!Db.Initialized)
            {
                Db.Initialize();
            }

            if (!App.Initialized)
            {
                App.Init();
            }

            var fullResult = new AddItemResult
            {
                AddedItems = new List<ItemDataInstance>(),
                AddedCurrencies = new Dictionary<string, int>()
            };

            for (var i = 0; i < items.Count; i++)
            {
                var result = App.Inventory.GrantItem(items[i].ItemUid,items[i].Count, new ItemAcquisitionData
                {
                    Receipt = ItemAcquisitionData.GetReceipt(),
                    Annotation = ItemAcquisitionData.AnnotationReason.GRANTED,
                });

                fullResult.AddedItems.AddRange(result.AddedItems);
            }

            var itemsCombined = new List<ItemDataInstance>();

            for (var i = 0; i < fullResult.AddedItems.Count; i++)
            {
                var addedItem = fullResult.AddedItems[i];
                var instance = itemsCombined.Find(item => item.ItemId == addedItem.ItemId);

                if (instance != null)
                {
                    instance.Merge(addedItem);
                }
                else
                {
                    itemsCombined.Add(addedItem);
                }
            }

            fullResult.AddedItems = itemsCombined;

            if (currencies != null)
            {
                for (var i = 0; i < currencies.Count; i++)
                {
                    App.Inventory.AddCurrency(currencies[i].ItemUid, currencies[i].Count);
                }
            }

            if(display) App.HandleItemGrant(fullResult, currencies, PurchaseErrorCode.NONE, false);
        }
    }

    public class AddItemResult
    {
        public Dictionary<string, int> AddedCurrencies;
        public List<ItemDataInstance> AddedItems;
    }
}