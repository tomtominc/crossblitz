namespace CrossBlitz.PlayFab.Data
{
    public interface ISyncable
    {
        void OnGameWentOffline();
        void OnGameWentOnline();
    }
}