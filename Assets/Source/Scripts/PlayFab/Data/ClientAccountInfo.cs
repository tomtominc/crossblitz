using System;
using CrossBlitz.ClientAPI;
using CrossBlitz.PlayFab.Authentication;
using UnityEngine;

namespace CrossBlitz.PlayFab.Data
{
    [Serializable]
    public class ClientAccountInfo : ClientData
    {
        public string PlayerGuid;
        public string Username;
        public DateTime Created;
        public float TimePlayedTotal;
        public bool IsBanned;

        public override void Init()
        {
            base.Init();

            PlayerGuid = App.Load("ClientAccountInfo.PlayerGuid", GUID.CreateUnique());
            Username = App.Load("ClientAccountInfo.Username", string.Empty);
            Created = App.Load("ClientAccountInfo.Created", DateTime.UtcNow);
            TimePlayedTotal = App.Load("ClientAccountInfo.TimePlayedTotal", 0f);
            IsBanned = App.Load("ClientAccountInfo.IsBanned", false);

            Timers.Start("TotalPlayedTime");
        }

        public string GetPlayerGuid()
        {
            return PlayerGuid;
        }
        public void SetPlayerGuid(string playerGuid)
        {
            if (PlayerGuid != playerGuid)
            {
                PlayerGuid = playerGuid;
                dirty = true;
            }
        }

        public string GetUsername()
        {
            return Username;
        }
        public void SetUsername(string username)
        {
            if(Username != username)
            {
                Username = username;
                dirty = true;
            }
        }

        public DateTime GetCreated()
        {
            return Created;
        }
        public void SetCreated(DateTime created)
        {
            if (Created != created)
            {
                Created = created;
                dirty = true;
            }
        }

        public float GetTimePlayedTotal()
        {
            return TimePlayedTotal;
        }
        public void SetTimePlayedTotal(float timePlayedTotal)
        {
            if (TimePlayedTotal != timePlayedTotal)
            {
                TimePlayedTotal = timePlayedTotal;
                dirty = true;
            }
        }

        public bool GetIsBanned()
        {
            return IsBanned;
        }
        public void SetIsBanned(bool isBanned)
        {
            if (IsBanned != isBanned)
            {
                IsBanned = isBanned;
                dirty = true;
            }
        }

        public override void ResetData()
        {
            PlayerGuid = GUID.CreateUnique();
            Username = string.Empty;
            Created = DateTime.UtcNow;
            TimePlayedTotal = 0;
            IsBanned = false;

            dirty = true;

            Timers.Restart("TotalPlayedTime");
        }

        public override void Save()
        {
            TimePlayedTotal += Timers.GetWatch("TotalPlayedTime").Time;
            Timers.Restart("TotalPlayedTime");

            App.Save("ClientAccountInfo.PlayerGuid", PlayerGuid);
            App.Save("ClientAccountInfo.Username", Username);
            App.Save("ClientAccountInfo.Created", Created);
            App.Save("ClientAccountInfo.TimePlayedTotal", TimePlayedTotal);
            App.Save("ClientAccountInfo.IsBanned", IsBanned);
        }
    }
}