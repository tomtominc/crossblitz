using CrossBlitz.ServerAPI;
using Newtonsoft.Json;

namespace CrossBlitz.PlayFab.Data
{
    public class DataModel
    {
        public virtual string ToJson(bool shallow)
        {
            if (shallow)
            {
                return JsonConvert.SerializeObject(this);
            }

            return JsonConvert.SerializeObject(this,Server.JsonSettings);

        }

        public static T FromJson<T>(string json, bool shallow) where T : DataModel
        {
            if (shallow)
            {
                return JsonConvert.DeserializeObject<T>(json);
            }

            return JsonConvert.DeserializeObject<T>(json, Server.JsonSettings);
        }
    }
}