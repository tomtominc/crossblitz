using System.Collections.Generic;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Data;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.PlayFab.Data
{
    [System.Serializable]
    public class ClientBookData
    {
        public bool Initialized;
        public string BookId;
        public string BookHero =>  BookId.Split('-')[0];
        public string BookNumber => BookId.Split('-')[1];

        public ClientStoreData StoreData;
        public List<string> Recipes;
        public int DawnDollars;
        public string EquippedDeck;

        private bool _dirty;

        public bool IsDirty()
        {
            return _dirty || StoreData.IsDirty();
        }

        public void SetIsDirty(bool isDirty)
        {
            _dirty = isDirty;
            StoreData.SetIsDirty(isDirty);
        }

        public void Initialize(string chapterHero, int bookNumber)
        {
            Initialize($"{chapterHero}-{bookNumber}");
        }

        public void Initialize(string bookId)
        {
            Initialized = true;
            BookId = bookId;
            StoreData = new ClientStoreData();
            Recipes = new List<string>();
            StoreData.Init();
        }

        public void AddRecipe(string recipe)
        {
            if (!Recipes.Contains(recipe))
            {
                Recipes.Add(recipe);
                SetIsDirty(true);
            }
        }

        public void AddDawnDollars(int dawnDollars)
        {
            SetIsDirty(true);
            DawnDollars += dawnDollars;
            Events.Publish(this, EventType.OnModifiedCurrency, new OnModifiedCurrencyEventArgs {CurrencyCode = "DD", AmountLeft = DawnDollars});
        }

        public void SetDawnDollars(int dawnDollars)
        {
            if (DawnDollars != dawnDollars)
            {
                SetIsDirty(true);
                DawnDollars = dawnDollars;
            }
        }

        public void SetEquippedDeck(string equippedDeck)
        {
            if (EquippedDeck != equippedDeck)
            {
                EquippedDeck = equippedDeck;
                SetIsDirty(true);
            }
        }

        public void FixDependencies()
        {
            var book = Db.FablesDatabase.GetBook(BookId);

            if (book == null)
            {
                return;
            }

            // setting up the store

            if (StoreData == null)
            {
                StoreData = new ClientStoreData();
                StoreData.Init();
            }

            if (Recipes == null)
            {
                Recipes = new List<string>();
            }

            StoreData.Items.RemoveAll(i => string.IsNullOrEmpty(i.ItemId));

            SetIsDirty(true);
        }
    }
}