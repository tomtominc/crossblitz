using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Hero;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using GameDataEditor;
using UnityEngine;
using UnityEngine.Serialization;

[System.Serializable]
public class CardValue
{
    public string id;
    public int count;
}
/// <summary>
/// Holds the contents of 1 deck and all of it's properties
/// </summary>
[System.Serializable]
public class DeckData : DataModel
{
    public const int MaxCardsPerDeck = 30;
    public const int MaxDecks = 99;
    public const int MaxCopiesPerCard = 4;
    public const int MaxCopiesOfCommonCards = 8;
    public const int MaxCopiesOfRareCards = 4;
    public const int MaxCopiesOfMysticCards = 2;
    public const int MaxCopiesOfLegendaryCards = 1;
    public const int MaxCopiesOfPowerCards = 1;

    /// <summary>
    /// The unique id for this deck. Make sure to query all decks before making this.
    /// The unique id can be the same as a game state card though as it would conflict with anything.
    /// I do this so you can name your decks the same if you want
    /// </summary>
    public string uid;
    /// <summary>
    /// The name of the deck
    /// </summary>
    public string name;
    /// <summary>
    /// Optional info about the deck.
    /// </summary>
    public string info;
    /// <summary>
    /// I think it's easier to have the hero be separate because it allows me to find it more easily
    /// The hero string is formatted: "heroId,skin"[string,int]
    /// </summary>
    public string hero;
    /// <summary>
    /// This is a list of all the cards in the deck, it includes the hero, cards, runes etc.
    /// The card, runes etc are formatted: "cardId,count"[string,int]
    /// </summary>
    public List<CardValue> cards;
    /// <summary>
    /// The faction this deck belongs to.
    /// </summary>
    public Faction faction;
    /// <summary>
    /// The card back for this card - standard if not set.
    /// </summary>
    [FormerlySerializedAs("cardBack")] public string cardBackUid = GDEItemKeys.CardBack_Standard_Back_001;

    public DeckData()
    {
        uid = GUID.CreateUnique();
        name = string.Empty;
        cards = new List<CardValue>();
    }

    public static int GetMaxCopies(Rarity rarity)
    {
        switch (rarity)
        {
            case Rarity.Common: return MaxCopiesOfCommonCards;
            case Rarity.Rare: return MaxCopiesOfRareCards;
            case Rarity.Mythic: return MaxCopiesOfMysticCards;
            case Rarity.Legendary: return MaxCopiesOfLegendaryCards;
        }

        return 0;
    }

    public bool IsValid()
    {
        var isValid = true;

        cards.RemoveAll(value => string.IsNullOrEmpty(value.id) || Db.CardDatabase.GetCard(value.id) == null);

        if (!Db.Initialized)
        {
            Db.Initialize();
        }

        for (var i = 0; i < cards.Count; i++)
        {
            var card = Db.CardDatabase.GetCard(cards[i].id);

            switch (card.rarity)
            {
                case Rarity.Common:
                    cards[i].count = Mathf.Min(cards[i].count, Mathf.Min(App.Inventory.GetItemAmount(cards[i].id), MaxCopiesOfCommonCards));
                    break;
                case Rarity.Rare:
                    cards[i].count = Mathf.Min(cards[i].count, Mathf.Min(App.Inventory.GetItemAmount(cards[i].id), MaxCopiesOfRareCards));
                    break;
                case Rarity.Mythic:
                    cards[i].count = Mathf.Min(cards[i].count, Mathf.Min(App.Inventory.GetItemAmount(cards[i].id), MaxCopiesOfMysticCards));
                    break;
                case Rarity.Legendary:
                    cards[i].count = Mathf.Min(cards[i].count, Mathf.Min(App.Inventory.GetItemAmount(cards[i].id), MaxCopiesOfLegendaryCards));
                    break;
            }
        }

        if (string.IsNullOrEmpty(uid))
        {
            uid = GUID.CreateUnique();
        }

        if (faction == Faction.All || faction == Faction.None || faction == Faction.Neutral)
        {
            faction = HeroData.GetFactionForHero(hero);

            if (faction == Faction.All || faction == Faction.None || faction == Faction.Neutral)
            {
                for (var i = 0; i < cards.Count; i++)
                {
                    var card = Db.CardDatabase.GetCard(cards[i].id);

                    if (card.faction != Faction.All && card.faction != Faction.None && card.faction != Faction.Neutral)
                    {
                        faction = card.faction;
                        break;
                    }
                }
            }

            if (faction == Faction.All || faction == Faction.None || faction == Faction.Neutral)
            {
                isValid = false;
            }
        }

        if (string.IsNullOrEmpty(hero))
        {
            hero = HeroData.GetHeroFromFaction(faction);
        }

        if (string.IsNullOrEmpty(name))
        {
            name = "???";
        }

        return !string.IsNullOrEmpty(uid) &&
               !string.IsNullOrEmpty(name) &&
               !string.IsNullOrEmpty(hero) &&
               isValid;
    }

    public void GenerateNewId()
    {
        uid = GUID.CreateUnique();
    }

    public HeroData GetHeroData(bool convertFromCardData = true)
    {
        if (string.IsNullOrEmpty(hero))
        {
            hero = GDEItemKeys.Hero_Redcroft;
        }

        var heroData = Db.HeroDatabase.GetHero(hero);

        if (heroData == null && convertFromCardData)
        {
            var cardData = Db.CardDatabase.GetCard(hero);
            heroData = HeroData.ConvertFromCardData( cardData, 0 );
        }

        return heroData;
    }

    public SavedHeroData GetHeroSaveData()
    {
        if (string.IsNullOrEmpty(hero))
        {
            hero = GDEItemKeys.Hero_Redcroft;
        }

        return App.HeroData.GetHero(hero);
    }

    public List<string> GetHeroPowers()
    {
        return cards.FindAll(card => Db.CardDatabase.GetCard(card.id).type == CardType.Power).Select(card => card.id).ToList();
    }

    public CardValue GetCardValue(string id)
    {
        return cards.Find(card => card.id == id);
    }

    public void AddCard(string id)
    {
        var cardInDeck = cards.Find(card => card.id == id);

        if (cardInDeck != null)
        {
            cardInDeck.count++;
        }
        else
        {
            cards.Add( new CardValue { id = id, count = 1 });
        }
    }

    public void RemoveCard(string id)
    {
        var cardInDeck = cards.Find(card => card.id == id);

        if (cardInDeck != null)
        {
            cardInDeck.count -= 1;

            if (cardInDeck.count <= 0)
            {
                cards.Remove(cardInDeck);
            }
        }
    }

    public int GetCardAtIndexCount(int index)
    {
        if (index < 0) return 0;
        if (index >= cards.Count) return 0;

        return cards[index].count;
    }

    public string GetCardId(int index)
    {
        if (index < 0) return string.Empty;
        if (index >= cards.Count) return string.Empty;

        return cards[index].id;
    }

    public int GetCardCount(bool checkOwnership=true, bool checkRelic=false, bool checkElderRelic = false, bool checkBothRelics = false)
    {
        var count = 0;

        for (var i = 0; i < cards.Count; i++)
        {
            var id = cards[i].id;
            if (string.IsNullOrEmpty(id)) continue;
            var c = cards[i].count;
            var card = Db.CardDatabase.GetCard(cards[i].id);

            if (card == null) continue;

            if (checkOwnership)
            {
                if (card.type != CardType.Relic && card.type != CardType.Elder_Relic)
                {
                    var ownedAmount = App.Inventory.GetItemAmount(id);
                    count += Mathf.Min(ownedAmount, c);
                }
            }
            else if (checkRelic)
            {
                if(card.type == CardType.Relic) count += c;
            }
            else if (checkElderRelic)
            {
                if (card.type == CardType.Elder_Relic) count += c;
            }
            else if (checkBothRelics)
            {
                if (card.type == CardType.Elder_Relic || card.type == CardType.Relic) count += c;
            }
            else
            {
                if (card.type != CardType.Relic && card.type != CardType.Elder_Relic) count += c;
            }

        }

        return count;
    }

    public CardData GetElderRelic()
    {
        for (var i = 0; i < cards.Count; i++)
        {
            var id = cards[i].id;
            if (string.IsNullOrEmpty(id)) continue;
            var card = Db.CardDatabase.GetCard(cards[i].id);
            if (card == null) continue;
            if (card.type == CardType.Elder_Relic) return card;
        }
        return null;
    }

    public int GetRelicCount()
    {
        var relicCount = 0;
        for (var i = 0; i < cards.Count; i++)
        {
            var id = cards[i].id;
            if (string.IsNullOrEmpty(id)) continue;
            var card = Db.CardDatabase.GetCard(cards[i].id);
            if (card == null) continue;
            if (card.type == CardType.Elder_Relic || card.type == CardType.Relic) relicCount += 1;
        }

        return relicCount;
    }

    public int GetAmountOfSingleCard(string lookUpId)
    {
        var card = GetCardValue(lookUpId);

        return card?.count ?? 0;
    }

    public string ToRecipe()
    {
        var header = $"##{name}\n";
        cards.RemoveAll(c => Db.CardDatabase.GetCard(c.id) == null);
        cards = cards.OrderBy(c => Db.CardDatabase.GetCard(c.id).cost).ToList();
        var zipped = ToJson(true).Zip();
        return $"{header}{zipped}";
    }

    public static DeckData FromRecipe(string recipe, string uid = default)
    {
        if (string.IsNullOrEmpty(recipe))
        {
            return null;
        }

        try
        {
            var splitRecipe = recipe.Split('\n');

            DeckData deck = null;

            if (splitRecipe.Length > 1)
            {
                var header = splitRecipe[0];
                var zipped = splitRecipe[1];

                if (header.StartsWith("##"))
                {
                    header = header.Remove(0, 2);
                }

                deck = FromJson<DeckData>(zipped.UnZip(), true);
                deck.name = header;
            }
            else if (splitRecipe.Length == 1)
            {
                var zipped = splitRecipe[0];
                var deckJson = zipped.UnZip();

                if (deckJson.StartsWith("{"))
                {
                    deck = FromJson<DeckData>(zipped.UnZip(), true);
                }
            }

            if (deck != null)
            {
                deck.uid = string.IsNullOrEmpty(uid) ? GUID.CreateUnique() : uid;

                return deck;
            }
        }
        catch (Exception e)
        {
            // no catch
        }

        return null;
    }
}
