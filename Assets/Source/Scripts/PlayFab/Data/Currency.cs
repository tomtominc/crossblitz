﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// All currencies for the game.
/// </summary>
public static class Currency
{
    /// <summary>
    /// Keystones are used to open chests and other things
    /// </summary>
    public const string KEYSTONES = "KS";
    /// <summary>
    /// Mana shards are used to forge new cards
    /// </summary>
    public const string MANA_SHARDS = "MS";
    /// <summary>
    /// Gold is used to purchase things in the shop
    /// </summary>
    public const string GOLD = "GD";
    /// <summary>
    /// The experience is for the player leveling up
    /// </summary>
    public const string EXPERIENCE = "XP";
    /// <summary>
    /// prestige is used inside the match making it's basically experience for ranking up
    /// </summary>
    public const string PRESTIGE = "PR";
    /// <summary>
    /// chest medtals are gained by playing pvp mode, the allow you to open chests.
    /// </summary>
    public const string CHEST_MEDALS = "CM";
    /// <summary>
    /// Dawn dollars are currencies used inside the story modes.
    /// </summary>
    public const string DAWN_DOLLARS = "DD";

    public static readonly Dictionary<string, string> GetReadableName = new Dictionary<string, string>
    {
        {KEYSTONES, "Keystones"},
        {MANA_SHARDS, "Mana Shards"},
        {GOLD, "Gold"},
        {EXPERIENCE, "Xp"},
        {PRESTIGE, "Prestige"},
        {CHEST_MEDALS, "Chest Medals"}
    };

    public static readonly Dictionary<string, string> GetIconName = new Dictionary<string, string>
    {
        {KEYSTONES, "keystone"},
        {MANA_SHARDS, "mana-shard"},
        {GOLD, "gold"},
        {EXPERIENCE, "xp"},
        {PRESTIGE, "prestige"},
        {CHEST_MEDALS, "chest-medals"}
    };
}
