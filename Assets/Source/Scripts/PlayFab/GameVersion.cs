namespace CrossBlitz.PlayFab
{
    public static class GameVersion
    {
        public static string Version = "0.0.1";

        public static bool VersionIsEqualToOrLowerThan(string version1, string version2)
        {
            SplitVersion(version1, out var majorTest, out var minorTest, out var patchTest);
            SplitVersion(version2, out var major, out var minor, out var patch);

            if (majorTest > major) return false;
            if (minorTest > minor) return false;
            if (patchTest > patch) return false;

            return true;
        }

        public static void SplitVersion(string version, out int major, out int minor, out int patch)
        {
            var versionInfo = version.Split('.');

            major = int.Parse(versionInfo[0]);
            minor = int.Parse(versionInfo[1]);
            patch = int.Parse(versionInfo[2]);
        }
    }
}