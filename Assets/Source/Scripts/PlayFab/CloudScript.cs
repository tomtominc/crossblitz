﻿using System;
using System.Collections.Generic;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI.Models;
using CrossBlitz.ViewAPI;
using CrossBlitz.ViewAPI.Popups;
using Newtonsoft.Json;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using ConsumeItemRequest = CrossBlitz.ServerAPI.Models.ConsumeItemRequest;
using ConsumeItemResult = CrossBlitz.ServerAPI.Models.ConsumeItemResult;

namespace CrossBlitz.PlayFab
{
    public static class CloudScript
    {
        // =================================================================================================================
        // CONSUME ITEM REQUEST
        // =================================================================================================================
        public static event Action OnPlayerSetupSuccessful;
        public static void SetupPlayer()
        {
            var cloudScriptRequest = GetRequest("OnPlayerLoggedIn", new CloudScriptRequest());
            PlayFabClientAPI.ExecuteCloudScript(cloudScriptRequest, HandlePlayerSetup,
                HandlePlayFabError, cloudScriptRequest.FunctionName);
        }

        private static void HandlePlayerSetup(ExecuteCloudScriptResult result)
        {
            if (HandleLogs(result.Logs) || HandleError(result)) return;

            var json = result.FunctionResult.ToString();
            var settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Auto };
            var funcResult = JsonConvert.DeserializeObject<PlayerLoggedInResult>(json, settings);
            //ClientSessionData.Inventory.SetItems(funcResult.Inventory.Inventory);
            //ClientSessionData.SetUserData(funcResult.PlayerData.Data);

            OnPlayerSetupSuccessful?.Invoke();
        }

        // =================================================================================================================
        // PURCHASE ITEM REQUEST
        // =================================================================================================================
        public static event Action<PurchaseItemResult> OnPurchaseItemRequestComplete;
        public static void PurchaseItemRequest(PurchaseItemRequest request)
        {
            PlayFabClientAPI.PurchaseItem(request, HandlePurchaseItemRequest, HandlePlayFabError);
        }

        private static void HandlePurchaseItemRequest(PurchaseItemResult result)
        {
            OnPurchaseItemRequestComplete?.Invoke(result);
        }

        public static event Action<UnlockContainerResult> OnUnlockContainerRequestComplete;
        public static event Action<ExecuteCloudScriptResult> OnUnlockContainerRequestFailedInServer;
        public static event Action<PlayFabError> OnUnlockContainerRequestFailedInPlayFab;

        public static void UnlockContainerRequest(UnlockContainerRequest request)
        {
            var cloudScriptRequest = GetRequest("UnlockContainerRequest", request);
            PlayFabClientAPI.ExecuteCloudScript(cloudScriptRequest,
                HandleUnlockContainerRequest, HandleUnlockContainerRequestFailed, cloudScriptRequest.FunctionName);
        }

        private static void HandleUnlockContainerRequest(ExecuteCloudScriptResult result)
        {
            if (HandleLogs(result.Logs) || HandleError(result))
            {
                OnUnlockContainerRequestFailedInServer?.Invoke(result);
                return;
            }

            var json = result.FunctionResult.ToString();
            var settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Auto };
            var funcResult = JsonConvert.DeserializeObject<UnlockContainerResult>(json, settings);

            OnUnlockContainerRequestComplete?.Invoke(funcResult);
        }

        private static void HandleUnlockContainerRequestFailed(PlayFabError error)
        {
            //todo: actually handle this error properly
            HandlePlayFabError(error);
            OnUnlockContainerRequestFailedInPlayFab?.Invoke(error);
        }

        // =================================================================================================================
        // CONSUME ITEM REQUEST
        // =================================================================================================================
        public static event Action<ConsumeItemResult> OnConsumeItemRequestComplete;
        public static void ConsumeItemRequest(ConsumeItemRequest request)
        {
            var cloudScriptRequest = GetRequest("ConsumeItemRequest", request);

            PlayFabClientAPI.ExecuteCloudScript(cloudScriptRequest,HandleConsumeItemRequest,
                HandlePlayFabError, cloudScriptRequest.FunctionName);
        }

        private static void HandleConsumeItemRequest(ExecuteCloudScriptResult result)
        {
            if (HandleLogs(result.Logs) || HandleError(result))
            {
                return;
            }

            var json = result.FunctionResult.ToString();
            var settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Auto };
            var funcResult = JsonConvert.DeserializeObject<ConsumeItemResult>(json, settings);

            OnConsumeItemRequestComplete?.Invoke(funcResult);
        }

        #region CREATION OF CLOUD SCRIPT REQUESTS

        private static ExecuteCloudScriptRequest GetRequest(string functionName, CloudScriptRequest request)
        {
            return new ExecuteCloudScriptRequest
            {
                FunctionName = functionName,
                RevisionSelection = CloudScriptRevisionOption.Latest,
                FunctionParameter = new { Request = JsonConvert.SerializeObject(request) },
                GeneratePlayStreamEvent = true
            };
        }

        #endregion

        #region ERROR AND MESSAGE HANDLING


        private static void HandlePlayFabError(PlayFabError error)
        {
            PopupController.Open(new PopupInfo
            {
                title = error.CustomData.ToString(),
                body = error.ErrorMessage,
                confirm = "OKAY"
            });
        }

        private static void HandleServerError(ServerError error, string raw)
        {
            if (error?.apiError == null)
            {
                if (error == null ) Debug.LogError($"There was an error getting the server error. {raw}");
                else if (error.apiError == null) Debug.LogError($"There was an error getting the api error. API = {error.api}\n{raw}");
                return;
            }

            PopupController.Open(new PopupInfo
            {
                title = $"{error.apiError.error} ({error.apiError.errorCode})",
                body = error.apiError.errorMessage,
                confirm = "OKAY"
            });
        }

        private static bool HandleLogs(List<LogStatement> logs)
        {
            var hasError = false;

            if (logs != null && logs.Count > 0)
            {
                for (var i = 0; i < logs.Count; i++)
                {
                    var log = logs[i];

                    if (log.Level.Equals("Error"))
                    {
                        var rawData = log.Data.ToString();
                        hasError = true;

                        try
                        {
                            var errorResult = JsonConvert.DeserializeObject<ServerError>(rawData);
                            HandleServerError(errorResult, rawData);
                        }
                        catch
                        {
                            Debug.LogError($"Could not parse Json\n{rawData}");
                        }

                        LoadingPromptController.RemoveLoadingPrompt();
                    }
                    else
                    {
                        Debug.Log(log.Message);
                    }
                }
            }

            return hasError;
        }

        private static bool HandleError(ExecuteCloudScriptResult result)
        {
            if (result.Error != null && !string.IsNullOrEmpty(result.Error.Error))
            {
                PopupController.Open(new PopupInfo
                {
                    title = result.Error.Error,
                    body = $"{result.Error.Message}\n{result.Error.StackTrace}",
                    confirm = "Okay"
                });

                LoadingPromptController.RemoveLoadingPrompt();

                return true;
            }

            return false;
        }

        #endregion
    }
}
