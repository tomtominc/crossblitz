﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.CardCollection.CardBacks;
using CrossBlitz.ClientAPI;
using CrossBlitz.Databases;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.Fables;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Items.Data;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.Quests;
using CrossBlitz.Results;
using CrossBlitz.Settings;
using CrossBlitz.ViewAPI.Popups;
using GameDataEditor;
using MEC;
using UnityEngine;
using UnityEngine.SceneManagement;
using Events = CrossBlitz.ClientAPI.GameLogic.EventSystem.Events;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.PlayFab.Authentication
{
    public static class App
    {
        private const string CredentialsKey = "player_credentials";

        public static bool Initialized;
        public static ClientAccountInfo AccountInfo;
        public static PlayerData PlayerData;
        public static PlayerDecks PlayerDecks;
        public static ClientInventory Inventory;
        public static ClientFableData FableData;
        public static ClientSettings Settings;
        public static BattleResults BattleResults;
        public static ChapterResults ChapterResults;
        public static PlayerAchievements Achievements;
        public static PlayerCardBacks CardBacks;
        public static ClientHeroData HeroData;
        public static ClientTutorialData TutorialData;
        public static List<ClientData> SaveData;

        public static Action<bool> UpdatePlayerDataComplete = delegate { };

        private static ClientEventTracker m_eventTracker;

        public static void Awake()
        {
            Initialized = false;

            Timers.Init();
            AudioController.Init();
            FableController.Init();

            AccountInfo = null;
            PlayerData = null;
            PlayerDecks = null;
            Inventory = null;
            FableData = null;
            BattleResults = null;
            ChapterResults = null;
            Achievements = null;
            CardBacks = null;

            SaveData = new List<ClientData>();
            Settings = new ClientSettings();
            Settings.Init();
            SaveData.Add(Settings);
        }

        public static IEnumerator<float> InitWithDelays(float delay)
        {
            AccountInfo = new ClientAccountInfo();
            AccountInfo.Init();
            SaveData.Add(AccountInfo);

            yield return Timing.WaitForSeconds(delay);

            PlayerData = new PlayerData();
            PlayerData.Init();
            SaveData.Add(PlayerData);

            yield return Timing.WaitForSeconds(delay);

            HeroData = new ClientHeroData();
            HeroData.Init();
            SaveData.Add(HeroData);

            yield return Timing.WaitForSeconds(delay);

            PlayerDecks = new PlayerDecks();
            PlayerDecks.Init();
            SaveData.Add(PlayerDecks);

            yield return Timing.WaitForSeconds(delay);

            Inventory = new ClientInventory();
            Inventory.Init();
            SaveData.Add(Inventory);

            yield return Timing.WaitForSeconds(delay);

            FableData = new ClientFableData();
            FableData.Init();
            SaveData.Add(FableData);

            yield return Timing.WaitForSeconds(delay);

            TutorialData = new ClientTutorialData();
            TutorialData.Init();
            SaveData.Add(TutorialData);

            yield return Timing.WaitForSeconds(delay);

            BattleResults = new BattleResults();
            BattleResults.Init();
            SaveData.Add(BattleResults);

            yield return Timing.WaitForSeconds(delay);

            Achievements = new PlayerAchievements();
            Achievements.Init();
            SaveData.Add(Achievements);

            yield return Timing.WaitForSeconds(delay);

            CardBacks = new PlayerCardBacks();
            CardBacks.Init();
            SaveData.Add(CardBacks);

            yield return Timing.WaitForSeconds(delay);

            m_eventTracker?.StopListening();
            m_eventTracker = new ClientEventTracker();
            m_eventTracker.StartListening();

            yield return Timing.WaitForSeconds(delay);

            AddUserCharacters();
            var isNew = PlayerData.GetIsNewUser();
            if (isNew) PlayerData.SetIsNewUser(false);

            Initialized = true;
        }

        public static void Init()
        {
            AccountInfo = new ClientAccountInfo();
            AccountInfo.Init();
            SaveData.Add(AccountInfo);

            PlayerData = new PlayerData();
            PlayerData.Init();
            SaveData.Add(PlayerData);

            HeroData = new ClientHeroData();
            HeroData.Init();
            SaveData.Add(HeroData);

            PlayerDecks = new PlayerDecks();
            PlayerDecks.Init();
            SaveData.Add(PlayerDecks);

            Inventory = new ClientInventory();
            Inventory.Init();
            SaveData.Add(Inventory);

            FableData = new ClientFableData();
            FableData.Init();
            SaveData.Add(FableData);

            TutorialData = new ClientTutorialData();
            TutorialData.Init();
            SaveData.Add(TutorialData);

            BattleResults = new BattleResults();
            BattleResults.Init();
            SaveData.Add(BattleResults);

            Achievements = new PlayerAchievements();
            Achievements.Init();
            SaveData.Add(Achievements);

            CardBacks = new PlayerCardBacks();
            CardBacks.Init();
            SaveData.Add(CardBacks);

            m_eventTracker?.StopListening();
            m_eventTracker = new ClientEventTracker();
            m_eventTracker.StartListening();

            AddUserCharacters();
            var isNew = PlayerData.GetIsNewUser();
            if(isNew) PlayerData.SetIsNewUser(false);

            Initialized = true;
        }

        private static void AddUserCharacters()
        {
            AddCharacter("Redcroft", "Redcroft Starter", GDEItemKeys.Hero_Redcroft, DeckEquipContext.Redcroft_Fable);
            // AddCharacter("Violet", "Violet Starter", GDEItemKeys.Hero_Violet, DeckEquipContext.Violet_Fable);
            // AddCharacter("Quill", "Quill Starter", GDEItemKeys.Hero_Quill, DeckEquipContext.Quill_Fable);
        }

        public static void AddCharacter(string characterName, string starterDeckName, string heroKey, DeckEquipContext deckEquipContext)
        {
            var hero = HeroData.GetHero(characterName);

            if (!hero.unlocked)
            {
                hero.unlocked = true;

                var starterDeck = Db.DeckRecipeDatabase.GetRecipeByName(starterDeckName);
                var itemValues = new List<ItemValue> { new ItemValue { ItemUid = starterDeck.Uid, Count = 1 } };

                for (var i = 0; i < starterDeck.deckData.cards.Count; i++)
                {
                    var itemValue = new ItemValue
                    {
                        ItemUid = starterDeck.deckData.cards[i].id,
                        Count = starterDeck.deckData.cards[i].count
                    };

                    itemValues.Add(itemValue);
                }

                Inventory.GrantItems(itemValues);
                Inventory.GrantItem(heroKey, 1, new ItemAcquisitionData
                {
                    Annotation = ItemAcquisitionData.AnnotationReason.PLAYER_SELECTED,
                    Receipt = ItemAcquisitionData.GetReceipt(),
                    Purchased = false
                });

                PlayerDecks.AddDeck(starterDeck, deckEquipContext);

                var chapterData = FableData.GetChapter(characterName, 1, 1);
                chapterData.SetUnlocked(true);
            }
        }

        public static void ResetData()
        {
            // if (ES3.FileExists("SaveFile.es3"))
            // {
            //     ES3.DeleteFile(ES3Settings.defaultSettings);
            //     ES3.DeleteFile("SaveFile.es3", ES3Settings.defaultSettings);
            // }
            // else
            // {
            //     Debug.LogError("No Save File exists!");
            // }

            DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);

            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                dir.Delete(true);
            }

            for (var i = 0; i < SaveData?.Count; i++)
            {
                SaveData[i]?.ResetData();
            }

            Awake();
            Init();
        }

        public static void ResetGame()
        {
            Awake();
            Init();
        }

        public static void Save<T>(string key, T obj)
        {
            ES3.Save(key, obj, ES3Settings.defaultSettings);
        }

        public static T Load<T>(string key, T defaultValue)
        {
            return ES3.Load(key, defaultValue,ES3Settings.defaultSettings);
        }

        public static bool CheckForStoredCredentials()
        {
            return PlayerPrefs.HasKey(CredentialsKey);
        }

        public static StoredCredentials GetCredentials()
        {
            string json = PlayerPrefs.GetString(CredentialsKey, JsonUtility.ToJson(new StoredCredentials()));
            StoredCredentials credentials = JsonUtility.FromJson<StoredCredentials>(json);
            credentials.password = credentials.password.Decrypt();
            return credentials;
        }

        public static void SetCredentials(string email, string password)
        {
            StoredCredentials credentials = GetCredentials();
            credentials.username = email;
            credentials.password = password.Encrypt();

            PlayerPrefs.SetString(CredentialsKey, JsonUtility.ToJson(credentials));
            PlayerPrefs.Save();
        }

        public static void SaveAll()
        {
            if (SaveData == null)
            {
                return;
            }

            var saved = false;
            var stopWatch = new System.Diagnostics.Stopwatch();
            stopWatch.Start();

            for (var i = 0; i < SaveData.Count; i++)
            {
                if (SaveData[i].IsDirty())
                {
                    Debug.Log("SAVING WITH " + SaveData[i].GetType());
                    saved = true;
                    SaveData[i].Save();
                    SaveData[i].SetIsDirty(false);
                }
            }

            if(saved)
            {
                ShowSaveIcon();

                stopWatch.Stop();
                Debug.Log($"Saved! Milliseconds: {stopWatch.Elapsed.Milliseconds}");
            }
        }

        public static void ShowSaveIcon()
        {
            Events.Publish(null, EventType.OnSpawnSaveIcon, null);
        }

        public static void OnUpdate()
        {
            if (SaveData == null)
            {
                return;
            }

            Timers.Update();

            for (var i = 0; i < SaveData.Count; i++)
            {
                SaveData[i].OnUpdate();
            }

            if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.LeftCommand)) && Input.GetKeyDown(KeyCode.S) && Input.GetKey(KeyCode.LeftShift))
            {
                OpenSettings();
            }

            if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.LeftCommand)) && Input.GetKeyDown(KeyCode.C) && Input.GetKey(KeyCode.LeftShift) && (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)))
            {
                OpenCollectionMenu();
            }
        }

        private static async void OpenSettings()
        {
            if (SceneManager.GetSceneByName("OptionsWindow").isLoaded) return;

            var sceneModel = await SceneController.Instance.LoadScene(new SceneLoadParams
            {
                SceneToLoad = "OptionsWindow"
            });

            var settingsWindow = sceneModel.GetView<SettingsWindow>("SettingsWindow");
            settingsWindow.Initialize(SettingsContext.Fables);
            //settingsWindow.OnClose += OnSettingsClosed;
        }

        private static async void OpenCollectionMenu()
        {
            var sceneModel = await SceneController.Instance.LoadScene(new SceneLoadParams
            {
                SceneToLoad = "Collection"
            });

            var collectionManager = sceneModel.GetView<DeckEditMenu>("CollectionManager");

            collectionManager.OpenCollectionManagerWithSettings(new DeckEditSettings
            {
                Mode = DeckEditMode.Edit,
                OpenMode = DeckOpenMode.Collection,
                EquipContext = DeckEquipContext.Pvp,
                FilterCardSettings = new FilterCardSettings
                    { ownedCardsOnly = true, faction = Faction.All, @class = Class.All, rarity = Rarity.All, typeFilter = TypeFilter.All, relicFilter = RelicFilter.All, cardBackFilter = CardBackFilter.All },
            });

            for (var i = 0; i < sceneModel.sceneCameras.Length; i++)
            {
                sceneModel.sceneCameras[i].depth = 20;
            }
        }

        public static void OnApplicationClose()
        {
            if (SaveData == null)
            {
                return;
            }

            m_eventTracker?.StopListening();

            //SaveAll();

            Timers.StopAll(true);
        }

        public static void Logout()
        {
            //SaveAll();
        }

        public static void HandleItemGrant(AddItemResult result, List<ItemValue> currencies, PurchaseErrorCode errorCode, bool showErrorPopup = true)
        {
            switch (errorCode)
            {
                case PurchaseErrorCode.RECEIPT_NOT_VALID:
                    if (showErrorPopup)
                    {
                        PopupController.Open(new PopupInfo
                        {
                            style = PopupWindowStyle.Generic,
                            title = "RECEIPT NOT VALID",
                            body = "The receipt for your purchase was not valid.\nPlease make sure you're not cheating.",
                            confirm = "Okay"
                        });
                    }
                    break;
                case PurchaseErrorCode.NOT_ENOUGH_CURRENCY:
                    if (showErrorPopup)
                    {
                        PopupController.Open(new PopupInfo
                        {
                            style = PopupWindowStyle.Generic,
                            title = "NOT ENOUGH CURRENCY",
                            body = "You don't have enough currency to purchase this item.",
                            confirm = "Okay"
                        });
                    }
                    break;
                case PurchaseErrorCode.CANNOT_PURCHASE_USING_CURRENCY_TYPE:
                    if (showErrorPopup)
                    {
                        PopupController.Open(new PopupInfo
                        {
                            style = PopupWindowStyle.Generic,
                            title = "INVALID PURCHASE OPTIONS",
                            body = "The currency you're trying to use to purchase this item is not valid.",
                            confirm = "Okay"
                        });
                    }
                    break;
                case PurchaseErrorCode.NONE:

                    if (result?.AddedItems == null || result.AddedItems.Count <= 0)
                    {
                        if (showErrorPopup)
                        {
                            PopupController.Open(new PopupInfo
                            {
                                style = PopupWindowStyle.Generic,
                                title = "NO ITEM FOUND",
                                body = "Could not confirm which item you were granted or purchased.",
                                confirm = "Okay"
                            });
                        }

                        return;
                    }

                    var itemPopups = new List<PopupInfo>();
                    var split = result.AddedItems.GroupBy(item => item.ItemClass).ToList();
                    var itemSplitLists = new List<List<ItemDataInstance>>();

                    foreach (var itemList in split)
                    {
                        itemSplitLists.Add(itemList.ToList());
                    }

                    for (var i = 0; i < itemSplitLists.Count; i++)
                    {
                        var itemLists = itemSplitLists[i].ChunkBy(4);

                        for (var j = 0; j < itemLists.Count; j++)
                        {
                            if (itemLists[j].Count > 0)
                            {
                                // force currencies into the Ingredient popup because its only mana shards!
                                if (itemLists[j][0].ItemClass == ItemClassType.Ingredient && j == 0)
                                {
                                    itemPopups.Add(PopupController.GetPopupInfoForItems(itemLists[j], currencies));
                                }
                                else
                                {
                                    itemPopups.Add(PopupController.GetPopupInfoForItems(itemLists[j], null));
                                }
                            }
                        }
                    }

                    PopupController.OpenMany(itemPopups);
                    break;
            }
        }
    }

    [Serializable]
    public class StoredCredentials
    {
        public string username;
        public string password;

        public bool IsValid()
        {
            return !string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password);
        }
    }
}