using System.Threading.Tasks;
using Asyncoroutine;
using UnityEngine;

namespace CrossBlitz.PlayFab.Authentication
{
    public class InternetVerifier : MonoBehaviour
    {
        public static InternetReachabilityVerifier VerifierReference
        {
            get
            {
                if (InternetReachabilityVerifier.Instance == null)
                {
                    var go = new GameObject("InternetReachabilityVerifier", typeof(InternetReachabilityVerifier));
                    var verifier = go.GetComponent<InternetReachabilityVerifier>();
                    verifier.captivePortalDetectionMethod =
                        InternetReachabilityVerifier.CaptivePortalDetectionMethod.DefaultByPlatform;
                    verifier.customMethodExpectedData = "OK";
                    verifier.dontDestroyOnLoad = true;
                    return verifier;
                }

                return InternetReachabilityVerifier.Instance;
            }
        }
        public bool IsOnline()
        {
            return VerifierReference.status == InternetReachabilityVerifier.Status.NetVerified;
        }

        public async Task WaitUntilOnline()
        {
            // broken?
            //await VerifierReference.waitForNetVerifiedStatus();
        }
    }
}