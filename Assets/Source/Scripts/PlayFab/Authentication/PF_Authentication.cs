﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.ViewAPI;
using PlayFab.ClientModels;
using PlayFab;

public static class PF_Authentication
{
    // used for device ID
    public static string android_id = string.Empty; // device ID to use with PlayFab login
    public static string ios_id = string.Empty; // device ID to use with PlayFab login
    public static string custom_id = string.Empty; // custom id for other platforms

    //tracked actions
    public static bool isLoggedOut = false;
    //public static bool hasLoggedInOnce = false;

    /* Communication is diseminated across these 4 events */
    //called after a successful login
    public delegate void SuccessfulLoginHandler(string details, MessageDisplayStyle style);
    public static event SuccessfulLoginHandler OnLoginSuccess;

    //called after a login error or when logging out
    public delegate void FailedLoginHandler(string details, FieldErrorType fieldError, MessageDisplayStyle style);
    public static event FailedLoginHandler OnLoginFail;

    //called after a login error or when logging out
    public delegate void FailedAccountRecovery(string details, FieldErrorType fieldError, MessageDisplayStyle style);
    public static event FailedAccountRecovery OnAccountRecoveryFailed;

    //called after a login error or when logging out
    public delegate void SuccessfulAccountRecovery(string details, MessageDisplayStyle style);
    public static event SuccessfulAccountRecovery OnAccountRecoverySuccess;

    // regex pattern for validating email syntax
    private const string emailPattern = @"^([0-9a-zA-Z]([\+\-_\.][0-9a-zA-Z]+)*)+@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17})$";
    public static bool usedManualFacebook = false;
    private static readonly List<string> FacebookPermissionKeys = new List<string> { "public_profile", "email", "user_friends" };

    /// <summary>
    /// Informs lisenters when a successful login occurs
    /// </summary>
    /// <param name="details">Details - general string for additional details </param>
    /// <param name="style">Style - controls how the message should be handled </param>
    public static void RaiseLoginFailedEvent(string details, FieldErrorType fieldError, MessageDisplayStyle style)
    {
        if (OnLoginFail != null)
            OnLoginFail(details, fieldError, style);
    }

    #region PlayFab API calls

    /// <summary>
    /// Registers the new PlayFab account.
    /// </summary>
    public static void RegisterNewPlayFabAccount(string user, string pass1, string pass2, string email)
    {
        if (user.Length == 0 || pass1.Length == 0 || pass2.Length == 0 || email.Length == 0)
        {
            if (OnLoginFail != null)
                OnLoginFail("All fields are required.", FieldErrorType.All, MessageDisplayStyle.error);
            return;
        }

        var passwordCheck = ValidatePassword(pass1, pass2);
        var emailCheck = ValidateEmail(email);

        if (!passwordCheck)
        {
            if (OnLoginFail != null)
                OnLoginFail("Password cannot be less than 6 characters.", FieldErrorType.Password, MessageDisplayStyle.error);
            return;
        }
        else if (!emailCheck)
        {
            if (OnLoginFail != null)
                OnLoginFail("Invalid Email format.", FieldErrorType.Email, MessageDisplayStyle.error);
            return;
        }
        else if (user.Length > 15)
        {
            if (OnLoginFail != null)
                OnLoginFail("Usernames cannot exceed 15 characters.", FieldErrorType.Username, MessageDisplayStyle.error);
        }
        else if (user.Length < 3)
        {
            if (OnLoginFail != null)
                OnLoginFail("Usernames cannot be less than 3 characters.", FieldErrorType.Username, MessageDisplayStyle.error);
        }
        else
        {
            //ClientSessionData.TempEmail = email;
            //ClientSessionData.TempPassword = pass1;

            var request = new RegisterPlayFabUserRequest
            {
                TitleId = PlayFabSettings.TitleId,
                Username = user,
                Email = email,
                Password = pass1
            };

            LoadingPromptController.RequestLoadingPrompt(PlayFabAPIMethods.GenericLogin);

            PlayFabClientAPI.RegisterPlayFabUser(request, OnRegisterResult, OnLoginError);
        }
    }

    /// <summary>
    /// Login with PlayFab username.
    /// </summary>
    /// <param name="user">Username to use</param>
    /// <param name="password">Password to use</param>
    public static void LoginWithUsername(string user, string password)
    {
        if (user.Length > 0 && password.Length > 5)
        {
            var request = new LoginWithPlayFabRequest
            {
                Username = user,
                Password = password,
                TitleId = PlayFabSettings.TitleId,
                InfoRequestParameters = new GetPlayerCombinedInfoRequestParams
                {
                    GetUserData = true,
                    GetUserAccountInfo = true,
                    GetPlayerProfile = true,
                    GetTitleData = true,
                    GetPlayerStatistics = true,
                    GetUserVirtualCurrency = true,
                    GetUserInventory = true,
                }
            };

            LoadingPromptController.RequestLoadingPrompt(PlayFabAPIMethods.GenericLogin);
            PlayFabClientAPI.LoginWithPlayFab(request, OnLoginResult, OnLoginError);
        }
        else if (password.Length <= 0)
        {
            if (OnLoginFail != null)
                OnLoginFail(LocalizationController.Localize(LocalizationKeys.PASSWORD_BLANK_ERROR, "Password cannot be blank."), FieldErrorType.Password, MessageDisplayStyle.error);
        }
        else if (password.Length <= 5)
        {
            if (OnLoginFail != null)
                OnLoginFail(LocalizationController.Localize(LocalizationKeys.PASSWORD_NOT_LONG_ENOUGH_ERROR, "Password MUST be at least 6 characters long."), FieldErrorType.Password, MessageDisplayStyle.error);
        }
        else if (user.Length <= 0)
        {
            if (OnLoginFail != null)
                OnLoginFail(LocalizationController.Localize(LocalizationKeys.USERNAME_BLANK_ERROR, "Username cannot be blank."), FieldErrorType.Password, MessageDisplayStyle.error);
        }
    }

    /// <summary>
    /// Login using the email associated with a PlayFab account.
    /// </summary>
    /// <param name="email">Email.</param>
    /// <param name="password">Password.</param>
    public static void LoginWithEmail(string email, string password)
    {
        bool emailValidate = ValidateEmail(email);

        if (email.Length > 0 && password.Length > 5 && emailValidate)
        {
            //LoginMethodUsed = LoginPathways.pf_email;
            var request = new LoginWithEmailAddressRequest
            {
                Email = email,
                Password = password,
                TitleId = PlayFabSettings.TitleId,
                InfoRequestParameters = new GetPlayerCombinedInfoRequestParams
                {
                    GetUserData = true,
                    GetUserAccountInfo = true,
                    GetPlayerProfile = true,
                    GetTitleData = true,
                    GetPlayerStatistics = true,
                    GetUserVirtualCurrency = true,
                    GetUserInventory = true
                }
            };

            LoadingPromptController.RequestLoadingPrompt(PlayFabAPIMethods.GenericLogin);
            PlayFabClientAPI.LoginWithEmailAddress(request, OnLoginResult, OnLoginError);

        }
        else if (email.Length <= 0)
        {
            if (OnLoginFail != null)
                OnLoginFail(LocalizationController.Localize(LocalizationKeys.EMAIL_BLANK_ERROR, "Email cannot be blank."), FieldErrorType.Email, MessageDisplayStyle.error);
        }
        else if (password.Length <= 0)
        {
            if (OnLoginFail != null)
                OnLoginFail(LocalizationController.Localize(LocalizationKeys.PASSWORD_BLANK_ERROR, "Password cannot be blank."), FieldErrorType.Password, MessageDisplayStyle.error);
        }
        else if (password.Length <= 5)
        {
            if (OnLoginFail != null)
                OnLoginFail(LocalizationController.Localize(LocalizationKeys.PASSWORD_NOT_LONG_ENOUGH_ERROR, "Password MUST be at least 6 characters long."), FieldErrorType.Password, MessageDisplayStyle.error);
        }
        else if (!emailValidate)
        {
            Debug.Log("We failed on email validation");
            if (OnLoginFail != null)
                OnLoginFail(LocalizationController.Localize(LocalizationKeys.EMAIL_FORMAT_ERROR, "Invalid Email format."), FieldErrorType.Email, MessageDisplayStyle.error);
        }
    }

    /// <summary>
    /// Gets the device identifier and updates the static variables
    /// </summary>
    /// <returns><c>true</c>, if device identifier was obtained, <c>false</c> otherwise.</returns>
    public static bool GetDeviceId(bool silent = false) // silent suppresses the error
    {
        if (CheckForSupportedMobilePlatform())
        {
#if UNITY_ANDROID
            //http://answers.unity3d.com/questions/430630/how-can-i-get-android-id-.html
            AndroidJavaClass clsUnity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject objActivity = clsUnity.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject objResolver = objActivity.Call<AndroidJavaObject>("getContentResolver");
            AndroidJavaClass clsSecure = new AndroidJavaClass("android.provider.Settings$Secure");
            android_id = clsSecure.CallStatic<string>("getString", objResolver, "android_id");
#endif

#if UNITY_IPHONE
            ios_id = UnityEngine.iOS.Device.vendorIdentifier;
#endif
            return true;
        }
        else
        {
            custom_id = SystemInfo.deviceUniqueIdentifier;
            return false;
        }
    }

    /// <summary>
    /// Check to see if our current platform is supported (iOS & Android)
    /// </summary>
    /// <returns><c>true</c>, for supported mobile platform, <c>false</c> otherwise.</returns>
    public static bool CheckForSupportedMobilePlatform()
    {
        return Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer;
    }

    /// <summary>
    /// Logs the user out.
    /// </summary>
    public static void Logout()
    {
        android_id = string.Empty;
        ios_id = string.Empty;
        custom_id = string.Empty;
        isLoggedOut = true;

        App.Logout();
    }


    /*
 * Step 3
 * This is the final and the simplest step. We create new AuthenticationValues instance.
 * This class describes how to authenticate a players inside Photon environment.
 */
   /* private static void AuthenticateWithPhoton(GetPhotonAuthenticationTokenResult photonToken)
    {
        Debug.Log("Photon token acquired: " + photonToken.PhotonCustomAuthenticationToken + "  Authentication complete.");

        //We set AuthType to custom, meaning we bring our own, PlayFab authentication procedure.
        var customAuth = new AuthenticationValues { AuthType = CustomAuthenticationType.Custom };

        //We add "username" parameter. Do not let it confuse you: PlayFab is expecting this parameter to contain player PlayFab ID (!) and not username.
        customAuth.AddAuthParameter("username", PF_PlayerData.PlayerId);    // expected by PlayFab custom auth service

        //We add "token" parameter. PlayFab expects it to contain Photon Authentication Token issues to your during previous step.
        customAuth.AddAuthParameter("token", photonToken.PhotonCustomAuthenticationToken);

        PF_PlayerData.PhotonAuthToken = photonToken.PhotonCustomAuthenticationToken;

        //We finally tell Photon to use this authentication parameters throughout the entire application.
        PhotonNetwork.AuthValues = customAuth;

        OnLoginSuccess?.Invoke($"SUCCESS: {PF_PlayerData.SessionTicket}", MessageDisplayStyle.success);
        LoadingPromptController.RemoveLoadingPrompt(PlayFabAPIMethods.GenericLogin);
    }*/

    /// <summary>
    /// Called on a successful login attempt
    /// </summary>
    /// <param name="result">Result object returned from PlayFab server</param>
    private static void OnLoginResult(LoginResult result) //LoginResult
    {
        /* GetUserData = true,
                   GetUserAccountInfo = true,
                   GetPlayerProfile = true,
                   GetTitleData = true,
                   GetPlayerStatistics = true,
                   GetUserVirtualCurrency = true */

        //ClientSessionData.PlayerId = result.PlayFabId;
        //ClientSessionData.SessionTicket = result.SessionTicket;
        //ClientSessionData.SetAccountInfo(result.InfoResultPayload.AccountInfo);
        //ClientSessionData.SetInventoryFromLogin(result.InfoResultPayload.UserVirtualCurrency, result.InfoResultPayload.UserInventory);
        CrossBlitzTitleData.SetTitleData(result.InfoResultPayload.TitleData);

        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer || Application.isEditor)
        {
            if (usedManualFacebook)
            {
                LinkDeviceId();
                usedManualFacebook = false;
            }
        }

        //OnLoginSuccess?.Invoke($"SUCCESS: {ClientSessionData.SessionTicket}", MessageDisplayStyle.success);
        LoadingPromptController.RemoveLoadingPrompt(PlayFabAPIMethods.GenericLogin);
    }

    /*private static void RequestPhotonToken()
    {
        PlayFabClientAPI.GetPhotonAuthenticationToken(new GetPhotonAuthenticationTokenRequest()
        {
            PhotonApplicationId = PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime
        }, AuthenticateWithPhoton, OnPhotonAuthError);
    }*/

    /// <summary>
    /// Called on a successful registration result
    /// </summary>
    /// <param name="result">Result object returned from the PlayFab server</param>
    private static void OnRegisterResult(RegisterPlayFabUserResult result)
    {
        //LoginWithEmail(ClientSessionData.TempEmail, ClientSessionData.TempPassword);
    }

    /// <summary>
    /// Raises the login error event.
    /// </summary>
    /// <param name="error">Error.</param>
    private static void OnLoginError(PlayFabError error) //PlayFabError
    {
        string errorMessage;
        FieldErrorType fieldError;

        if (error.Error == PlayFabErrorCode.InvalidParams && error.ErrorDetails.ContainsKey("Password"))
        {
            errorMessage = "Invalid Password";
            fieldError = FieldErrorType.Password;
        }
        else if (error.Error == PlayFabErrorCode.InvalidParams && error.ErrorDetails.ContainsKey("Username") || (error.Error == PlayFabErrorCode.InvalidUsername))
        {
            errorMessage = string.Format("Invalid Username: {0}", error.ErrorDetails);
            fieldError = FieldErrorType.Username;
        }

        else if (error.Error == PlayFabErrorCode.AccountNotFound)
        {
            errorMessage = "This email is not associated with any account.";
            fieldError = FieldErrorType.Email;
        }

        else if (error.Error == PlayFabErrorCode.AccountBanned)
        {
            errorMessage = "The account associated with this email is been banned.";
            fieldError = FieldErrorType.Email;
        }

        else if (error.Error == PlayFabErrorCode.InvalidUsernameOrPassword)
        {
            errorMessage = "Invalid Username or Password.";
            fieldError = FieldErrorType.All;
        }

        else
        {
            errorMessage = string.Format("Error {0}: {1}", error.HttpCode, error.ErrorMessage);
            fieldError = FieldErrorType.All;
        }


        if (OnLoginFail != null)
            OnLoginFail(errorMessage, fieldError, MessageDisplayStyle.error);

        LoadingPromptController.RemoveLoadingPrompt(PlayFabAPIMethods.GenericLogin);
    }

    private static void OnPhotonAuthError(PlayFabError error)
    {
        if (OnLoginFail != null)
            OnLoginFail("Photon could not be initialized", FieldErrorType.All, MessageDisplayStyle.error);

        LoadingPromptController.RemoveLoadingPrompt(PlayFabAPIMethods.GenericLogin);
    }

    /// <summary>
    /// Calls the UpdateUserTitleDisplayName request API
    /// </summary>
    public static void UpdateDisplayName(string displayName, UnityAction<UpdateUserTitleDisplayNameResult> callback = null)
    {
        //if (displayName.Length < 3 || 20 < displayName.Length)
        //    return;

        //DialogCanvasController.RequestLoadingPrompt(PlayFabAPIMethods.UpdateDisplayName);

        //var request = new UpdateUserTitleDisplayNameRequest { DisplayName = displayName };

        //PlayFabClientAPI.UpdateUserTitleDisplayName(request, result =>
        //{
        //    PF_Bridge.RaiseCallbackSuccess(string.Empty, PlayFabAPIMethods.UpdateDisplayName, MessageDisplayStyle.none);
        //    if (callback != null)
        //        callback(result);
        //}, PF_Bridge.PlayFabErrorCallback);

    }

    /// <summary>
    /// Triggers the backend to send an account recovery email to the account provided
    /// </summary>
    /// <param name="email">Email to match</param>
    public static void SendAccountRecoveryEmail(string email)
    {
        var request = new SendAccountRecoveryEmailRequest
        {
            Email = email,
            TitleId = PlayFabSettings.TitleId
        };

        LoadingPromptController.RequestLoadingPrompt(PlayFabAPIMethods.SendAccountRecoveryEmail);

        PlayFabClientAPI.SendAccountRecoveryEmail(request, OnAccountRecoveryEmailSent, OnAccountRecoveryEmailFailed);
    }

    /// <summary>
    /// Links a mobile device to a PlayFab account via the unique device id (A device can only be linked to one account at a time)
    /// </summary>
    public static void LinkDeviceId()
    {
        if (!GetDeviceId())
            return;

        UnityAction<PlayFabError> onLinkError = error =>
        {
            PF_Bridge.RaiseCallbackError("Unable to link device: " + error.ErrorMessage, PlayFabAPIMethods.LinkDeviceID, MessageDisplayStyle.error);
        };

        if (!string.IsNullOrEmpty(android_id))
        {
            Debug.Log("Linking Android");
            var request = new LinkAndroidDeviceIDRequest { AndroidDeviceId = android_id };

            PlayFabClientAPI.LinkAndroidDeviceID(request, null, error => { onLinkError(error); });
        }
        else if (!string.IsNullOrEmpty(ios_id))
        {
            Debug.Log("Linking iOS");
            var request = new LinkIOSDeviceIDRequest { DeviceId = ios_id };

            PlayFabClientAPI.LinkIOSDeviceID(request, null, error => { onLinkError(error); });
        }
    }

    /// <summary>
    /// Unlinks a mobile device from a PlayFab account
    /// </summary>
    public static void UnlinkDeviceId()
    {
        if (!GetDeviceId())
            return;

        //if (!string.IsNullOrEmpty(android_id))
        //{
        //    Debug.Log("Unlinking Android");
        //    UnlinkAndroidDeviceIDRequest request = new UnlinkAndroidDeviceIDRequest();
        //    PlayFabClientAPI.UnlinkAndroidDeviceID(request, OnUnlinkAndroidDeviceIdSuccess, OnPlayFabCallbackError);
        //}
        //else if (!string.IsNullOrEmpty(ios_id))
        //{
        //    Debug.Log("Unlinking iOS");
        //    UnlinkIOSDeviceIDRequest request = new UnlinkIOSDeviceIDRequest();
        //    PlayFabClientAPI.UnlinkIOSDeviceID(request, OnUnlinkIosDeviceIdSuccess, OnPlayFabCallbackError);
        //}
    }
    #endregion

    #region pf_callbacks


    private static void OnAccountRecoveryEmailSent(SendAccountRecoveryEmailResult result)
    {
        OnAccountRecoverySuccess?.Invoke("Email sent", MessageDisplayStyle.success);

        LoadingPromptController.RemoveLoadingPrompt(PlayFabAPIMethods.SendAccountRecoveryEmail);
    }

    private static void OnAccountRecoveryEmailFailed(PlayFabError error)
    {
        string errorMessage;
        FieldErrorType fieldError;

        if (error.Error == PlayFabErrorCode.AccountNotFound)
        {
            errorMessage = LocalizationController.Localize(LocalizationKeys.ACCOUNT_NOT_FOUND, "The Email you entered does not have a valid account.");
            fieldError = FieldErrorType.Email;
        }
        else
        {
            errorMessage = string.Format("Error {0}: {1}", error.HttpCode, error.ErrorMessage);
            fieldError = FieldErrorType.All;
        }

        if (OnAccountRecoveryFailed != null)
            OnAccountRecoveryFailed(errorMessage, fieldError, MessageDisplayStyle.error);

        LoadingPromptController.RemoveLoadingPrompt(PlayFabAPIMethods.SendAccountRecoveryEmail);
    }

    #endregion

    #region helperfunctions
    /// <summary>
    /// Validates the email.
    /// </summary>
    /// <returns><c>true</c>, if email was validated, <c>false</c> otherwise.</returns>
    /// <param name="em">Email address</param>
    public static bool ValidateEmail(string em)
    {
        return Regex.IsMatch(em, emailPattern);
    }

    /// <summary>
    /// Validates the password.
    /// </summary>
    /// <returns><c>true</c>, if password was validated, <c>false</c> otherwise.</returns>
    /// <param name="p1">P1, text from password field one</param>
    /// <param name="p2">P2, text from password field two</param>
    public static bool ValidatePassword(string p1, string p2)
    {
        return ((p1 == p2) && p1.Length > 5);
    }

    #endregion
}

