using System.Threading.Tasks;
using CrossBlitz.Databases;
using CrossBlitz.ServerAPI;
using UnityAsync;
using UnityEngine;

namespace CrossBlitz.PlayFab.Authentication
{
    public class DatabaseVerifier : MonoBehaviour
    {
        public async Task WaitUntilDatabaseIsInitialized()
        {
            if (!Db.Initialized) Db.Initialize();
            while (Db.IsBusy) await Await.NextUpdate();
        }
    }
}