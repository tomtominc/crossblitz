using System.Threading.Tasks;
using Asyncoroutine;
using PlayFab;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.PlayFab.Authentication
{
    public class SimpleLogin : MonoBehaviour
    {
        [ReadOnly]
        public bool loggedIn;
        [ReadOnly]
        public string playFabId;

        public bool useStoredLoginCredentials=true;
        [ShowIf("@this.useStoredLoginCredentials == false")]
        public string email;
        [ShowIf("@this.useStoredLoginCredentials == false")]
        public string password;

        public async Task WaitUntilLoggedIn()
        {
            if (PlayFabClientAPI.IsClientLoggedIn()) return;

            if (useStoredLoginCredentials && App.GetCredentials().IsValid())
            {
                var credentials = App.GetCredentials();
                await AuthenticationController.AttemptLogin(credentials.username,credentials.password);
            }
            else
            {
                await AuthenticationController.AttemptLogin(email,password);
            }

            CloudScript.OnPlayerSetupSuccessful += OnPlayerSetupSuccessful;
            CloudScript.SetupPlayer();
        }

        private void OnPlayerSetupSuccessful()
        {
            CloudScript.OnPlayerSetupSuccessful -= OnPlayerSetupSuccessful;
            loggedIn = PlayFabClientAPI.IsClientLoggedIn();
            //playFabId = ClientSessionData.PlayerId;
        }
    }
}