using System.Collections.Generic;
using System.Threading.Tasks;
using Asyncoroutine;
using MEC;
using PlayFab;
using Sirenix.OdinInspector;
using UnityAsync;
using UnityEngine;

namespace CrossBlitz.PlayFab.Authentication
{
    public class AuthVerifier : MonoBehaviour
    {
        [ReadOnly]
        public bool loggedIn;
        [ReadOnly]
        public string playFabId;

        public bool useStoredLoginCredentials=true;
        [ShowIf("@this.useStoredLoginCredentials == false")]
        public string email;
        [ShowIf("@this.useStoredLoginCredentials == false")]
        public string password;

        public async Task WaitUntilLoggedIn()
        {
            if (PlayFabClientAPI.IsClientLoggedIn())
            {
                return;
            }

            AuthenticationController.OnLoggedInSuccessful += OnLoggedInSuccessful;
            AuthenticationController.OnLoginFailed += OnLoggedInFailed;

            if (useStoredLoginCredentials && App.GetCredentials().IsValid())
            {
                var credentials = App.GetCredentials();

                Timing.RunCoroutine( AuthenticationController.AttemptLogin(credentials.username,credentials.password) );
            }
            else
            {
                Timing.RunCoroutine( AuthenticationController.AttemptLogin(email,password) );
            }

            while (!loggedIn) await Await.NextUpdate();

            AuthenticationController.OnLoggedInSuccessful -= OnLoggedInSuccessful;
            AuthenticationController.OnLoginFailed -= OnLoggedInFailed;

            CloudScript.OnPlayerSetupSuccessful += OnPlayerSetupSuccessful;
            CloudScript.SetupPlayer();
        }

        private void OnPlayerSetupSuccessful()
        {
            //CloudScript.OnPlayerSetupSuccessful -= OnPlayerSetupSuccessful;
            //loggedIn = PlayFabClientAPI.IsClientLoggedIn();
            //playFabId = ClientSessionData.PlayerId;
        }

        private void OnLoggedInSuccessful(string details)
        {
            loggedIn = true;
        }

        private void OnLoggedInFailed(string details)
        {
            Debug.LogError($"Failed to login: {details}");
        }
    }
}