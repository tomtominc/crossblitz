using PlayFab.ClientModels;

public class PF_Item
{
    public PF_ItemType Type;
    public ItemInstance Instance;

    public PF_Item()
    {
        Type = PF_ItemType.NULL;
    }
}
