using CrossBlitz.AddressablesAPI;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Deck;
using CrossBlitz.Fables;
using CrossBlitz.Fables.Grid;
using CrossBlitz.InputAPI;
using CrossBlitz.LevelGrid;
using CrossBlitz.MainMenu;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.Settings;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Hero
{
    public class HeroPanel : MonoBehaviour
    {
        public HeroPanelBackground background;
        public HeroView heroView;
        public RectTransform deckViewParent;
        public DeckView deckViewPrefab;
        public HeroInfoWindow heroInfoWindow;
        public TextMeshProUGUI dawnDollarLabel;
        public Button settingsButton;

        private FablesInput.Mode m_lastMode;
        private DeckView m_deckView;

        private void Start()
        {
            settingsButton.onClick.AddListener(OnSettingsButton);

            Events.Subscribe(EventType.OnHeroChanged, OnHeroChanged);
            Events.Subscribe(EventType.OnModifiedCurrency, OnModifyCurrency);

            LevelTreeController.OnOpened += OnLevelTreeOpened;
            LevelTreeController.OnClosed += OnLevelTreeClosed;

            FablesInput.Instance.OnChangedMode += OnChangedMode;
        }

        private void OnDestroy()
        {
            LevelTreeController.OnOpened -= OnLevelTreeOpened;
            LevelTreeController.OnClosed -= OnLevelTreeClosed;
            FablesInput.Instance.OnChangedMode -= OnChangedMode;

            Events.Unsubscribe(EventType.OnHeroChanged, OnHeroChanged);
            Events.Unsubscribe(EventType.OnModifiedCurrency, OnModifyCurrency);
        }

        private async void OnSettingsButton()
        {
            if (FablesInput.Instance.currentMode == FablesInput.Mode.None)
            {
                Debug.LogError("Fables input is NONE!!!");
                return;
            }

            m_lastMode = FablesInput.Instance.currentMode;
            FablesInput.Instance.GoToMode(FablesInput.Mode.None);

            if (SceneManager.GetSceneByName("OptionsWindow").isLoaded) return;

            var sceneModel = await SceneController.Instance.LoadScene(new SceneLoadParams
            {
                SceneToLoad = "OptionsWindow"
            });

            var settingsWindow = sceneModel.GetView<SettingsWindow>("SettingsWindow");
            settingsWindow.Initialize(SettingsContext.Fables);
            settingsWindow.OnClose += OnSettingsClosed;
        }

        private void OnSettingsClosed()
        {
            FablesInput.Instance.GoToMode(m_lastMode);
        }

        private void OnModifyCurrency(IMessage message)
        {
            UpdateDawnDollarAmount();
        }

        private void OnChangedMode(FablesInput.Mode mode)
        {
            CreateAndSetDeckView();
        }

        private void CreateAndSetDeckView()
        {
            if (m_deckView == null)
            {
                m_deckView = Instantiate(deckViewPrefab, deckViewParent, false);
                m_deckView.RectTransform().anchoredPosition = Vector2.zero;
            }

            var deckData = App.PlayerDecks.GetDeckBasedOnGameState();

            if (deckData != null)
            {
                m_deckView.SetDeck(deckData);
            }
        }

        public void SetHero(HeroData hero)
        {
            if (hero == null)
            {
                Debug.LogError("No hero found!");
                return;
            }

            if (background)
            {
                background.SetFaction(hero.faction.ToString().ToLower());
            }

            if (heroView)
            {
                heroView.SetHero(hero, App.HeroData.GetHero(hero.id));
            }

            CreateAndSetDeckView();

            if (heroInfoWindow)
            {
                heroInfoWindow.SetHeroData(Db.HeroDatabase.GetHero(hero.id));
            }

            UpdateDawnDollarAmount();
        }

        private void OnLevelTreeOpened( LevelGridMenu levelGrid)
        {
            if (FablesInput.Instance.currentMode == FablesInput.Mode.None)
            {
                return;
            }

            m_lastMode = FablesInput.Instance.currentMode;
            FablesInput.Instance.GoToMode(FablesInput.Mode.None);
        }

        private void OnLevelTreeClosed()
        {
            if (heroInfoWindow)
            {
                heroInfoWindow.RefreshLevelData();
            }

            FablesInput.Instance.GoToMode(m_lastMode);
        }

        private void Update()
        {
            if (FablesInput.Instance == null)
            {
                return;
            }

            var interactable = (FablesInput.Instance.currentMode == FablesInput.Mode.Explore || FablesInput.Instance.currentMode == FablesInput.Mode.DeckView) && ExploreMenu.IsAbleToSelectTile();

            if (!interactable)
            {
                settingsButton.interactable = false;
                heroInfoWindow.levelTreeButton.interactable = false;
                GetComponent<Canvas>().sortingOrder = 0;
            }
            else
            {
                settingsButton.interactable = true;
                heroInfoWindow.levelTreeButton.interactable = true;
                GetComponent<Canvas>().sortingOrder = 200;
            }
        }

        public void UpdateDawnDollarAmount()
        {
            if (dawnDollarLabel)
            {
                dawnDollarLabel.text = $"{App.FableData.GetDawnDollarAmount():N0}";
            }
        }

        private void OnHeroChanged(IMessage message)
        {
            if (!(message.Data is HeroData heroData))
            {
                Debug.LogError("Hero wasn't changed properly!");
                return;
            }

            SetHero(heroData);
        }
    }
}