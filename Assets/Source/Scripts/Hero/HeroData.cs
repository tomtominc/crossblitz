﻿using System.Collections;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.PlayFab.Authentication;
using GameDataEditor;
using Newtonsoft.Json;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace CrossBlitz.Hero
{
    [System.Serializable]
    public class HeroData :  CharacterData
    {
        public const int MaxLevel = 30;
        [FoldoutGroup("Basic Information")]
        public string title;
        [FoldoutGroup("Level Tree Properties")]
        public LevelTree levelTree;
        public List<LevelTree> levelTrees;

        // public LevelTree archetypeA;
        // public LevelTree archetypeB;
        // public LevelTree archetypeC;
        // public LevelTree archetypeD;


        [HideInInspector] public int m_overrideHealth;

        public override int GetHealth()
        {
            if (m_overrideHealth > 0)
            {
                return m_overrideHealth;
            }

            var hero = App.HeroData.GetHero(id);

            if (hero != null)
            {
                return hero.health;
            }

            return 20;
        }

        public LevelReward GetRandomNewPrize()
        {
            var levelReward = new LevelReward
            {
                uid = GUID.CreateUniqueInt(),
                type = LevelReward.EType.Card
            };

            switch (levelReward.type)
            {
                case LevelReward.EType.Card:
                case LevelReward.EType.HeathUp:
                case LevelReward.EType.ManaShards:
                    levelReward.type = LevelReward.EType.ManaShards;
                    levelReward.manaShards = 25;
                    break;
                case LevelReward.EType.IngredientPouch:
                    var factionIngredients = Crafting.GetIngredientNamesByFaction(faction);
                    levelReward.ingredient = factionIngredients[Random.Range(0, factionIngredients.Count)];
                    break;
            }

            return levelReward;
        }

        public static LevelReward GetLevelReward(string heroId, int uid)
        {
            var hero = Db.HeroDatabase.GetHero(heroId);
            return hero.GetLevelReward(uid);
        }

        public LevelReward GetLevelReward(int uid)
        {
            for (var i = 0; i < levelTrees.Count; i++)
            {
                for (var j = 0; j < levelTrees[i].levels.Count; j++)
                {
                    var reward = levelTrees[i].levels[j];
                    if (reward.uid == uid) return reward;
                }
            }

            return null;
        }

        public void ConvertData()
        {
            var gde = new GDEHeroData(id);

            name = gde.Name;
            title = gde.Title;
            description = gde.Description;
            portraitUrl = gde.PortraitUrl;
            faction = GameUtilities.ParseEnum(gde.Faction, Faction.Neutral);

            type = CardType.Hero;
        }

        public static HeroData ConvertFromCardData(CardData cardData, int overrideHealth)
        {
            var heroData = new HeroData();
            heroData.id = cardData.id;
            heroData.name = cardData.name;
            heroData.portraitUrl = cardData.portraitUrl;
            heroData.faction = cardData.faction;
            heroData.type = CardType.Hero;
            heroData.standardOffset = cardData.boardOffset;
            //heroData.levelTree = new LevelTree { levels = new List<LevelReward>() };
            heroData.levelTrees = new List<LevelTree>();
            heroData.m_overrideHealth = overrideHealth;
            return heroData;
        }

        public static Faction GetFactionForHero(string hero)
        {
            if (hero == GDEItemKeys.Hero_Redcroft) return Faction.War;
            if (hero == GDEItemKeys.Hero_Seto) return Faction.Balance;
            if (hero == GDEItemKeys.Hero_Mereena) return Faction.Nature;
            if (hero == GDEItemKeys.Hero_Quill) return Faction.Fortune;
            if (hero == GDEItemKeys.Hero_Violet) return Faction.Chaos;

            Debug.LogError($"Could not find faction for hero {hero}!!");
            return Faction.Neutral;
        }

        public static List<CardSet> GetAvailableSetsForHero(string hero)
        {
            if (hero == GDEItemKeys.Hero_Redcroft) return new List<CardSet> { CardSet.Core, CardSet.PiratesAhoy };
            if (hero == GDEItemKeys.Hero_Seto) return new List<CardSet> { CardSet.Core, CardSet.MonksMantra };
            if (hero == GDEItemKeys.Hero_Mereena) return new List<CardSet> { CardSet.Core, CardSet.SproutwoodGuardian };
            if (hero == GDEItemKeys.Hero_Quill) return new List<CardSet> { CardSet.Core, CardSet.PerilousPilfering };
            if (hero == GDEItemKeys.Hero_Violet) return new List<CardSet> { CardSet.Core, CardSet.StarscapeTwilight };

            Debug.LogError($"Could not find faction for hero {hero}!!");
            return new List<CardSet> { CardSet.Core };
        }

        public static string GetHeroFromFaction(Faction faction)
        {
            switch (faction)
            {
                case Faction.War: return GDEItemKeys.Hero_Redcroft;
                case Faction.Balance: return GDEItemKeys.Hero_Seto;
                case Faction.Nature: return GDEItemKeys.Hero_Mereena;
                case Faction.Fortune: return GDEItemKeys.Hero_Quill;
                case Faction.Chaos: return GDEItemKeys.Hero_Violet;
            }

            return GDEItemKeys.Hero_Redcroft;
        }
    }


}