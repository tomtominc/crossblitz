using System;
using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using DG.Tweening;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace Source.Scripts.Hero
{
    public class ManaView : MonoBehaviour
    {
        private const float PipDistance = 9;
        private const float PipFirstDistance = 11;
        private const float PipGroundPos = -21;
        private const float FirstPip = -9;

        public Team team;
        public TMPSpriteFont manaLabel;
        public TextMeshProUGUI maxManaLabel;
        public List<SpriteAnimation> pips;
        public SpriteAnimation activePip;

        private float _targetMana;
        private float _currentMana;
        private bool _playSfx = false;
        private bool _goldMana = false;
        private bool _newTurn = false;
        private int _goldManaAmt = 0;
        private int _goldManaFloor = 0;
        private int _goldManaCeiling = 0;

        private void Start()
        {
            _targetMana = 0;
            _currentMana = 0;

            manaLabel.text = ((int)_currentMana).ToString();

            for (var i = 0; i < pips.Count; i++)
            {
                pips[i].SetActive(false);
            }

            activePip.RectTransform().localScale = new Vector3(-1, 1, 1);
            activePip.RectTransform().anchoredPosition = new Vector2(14f, PipGroundPos);
            activePip.Play("top");

            Events.Subscribe(EventType.OnModifiedMana, OnModifiedMana);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnModifiedMana, OnModifiedMana);
        }

        private void OnModifiedMana(IMessage message)
        {
            if (message.Data is OnModifiedManaEventArgs args)
            {
                if (args.team == team)
                {
                    _targetMana = args.currentMana;
                    _goldMana = args.goldMana;
                    _newTurn = args.newTurn;

                    if (_goldMana)
                    {
                        _goldManaAmt += (int)( _targetMana - _currentMana);
                        _goldManaFloor = (int)(_targetMana - _goldManaAmt);
                        _goldManaCeiling = (int) _targetMana;
                    }
                    else
                    {
                        if (_goldManaCeiling > _targetMana)
                        {
                            _goldManaCeiling = (int)_targetMana;
                            _goldManaAmt += (int)(_targetMana - _currentMana);
                        }

                        if (_newTurn || _goldManaFloor >= (int)_targetMana)
                        {
                            _goldManaFloor = 0;
                            _goldManaCeiling = 0;
                            _goldManaAmt = 0;
                        }
                    }

                    //Debug.Log("CURRENT MANA: " + _currentMana);
                    //Debug.Log("TARGET MANA: " + _targetMana);
                    //Debug.Log("GOLD MANA AMOUNT: " + _goldManaAmt);
                    //Debug.Log("GOLD MANA FLOOR: " + _goldManaFloor);
                    //Debug.Log("GOLD MANA CEILING: " + _goldManaCeiling);

                    maxManaLabel.text = $"/{args.totalMana}";
                    manaLabel.RectTransform().DOKill(true);
                    manaLabel.RectTransform().DOPunchAnchorPos(Vector2.up * 8, 0.25f);
                    
                    if (_targetMana > _currentMana)
                    {
                        _playSfx = true;
                        //AudioController.PlaySound("battle_turn_manafill", "BARDSFX", false, gameObject);
                    }
                }
            }
        }

        private void Update()
        {
            if (Math.Abs(_targetMana - _currentMana) > float.Epsilon)
            {
                if (!_goldMana) _currentMana = Mathf.MoveTowards(_currentMana, _targetMana, Time.deltaTime * 4f);
                else _currentMana = _targetMana;

                manaLabel.text = ((int)_currentMana).ToString();

                // if (!activePip.IsActive())
                // {
                //     activePip.SetActive(true);
                //     activePip.Play("top");
                // }

                var first2 = Mathf.Clamp(PipFirstDistance * _currentMana, 0, PipFirstDistance*2);
                var nextPips = Mathf.Clamp( PipDistance * (_currentMana-2), 0, Mathf.Infinity);
                var targetPipPosition = PipGroundPos + first2 + nextPips + 1;

                activePip.RectTransform().anchoredPosition = new Vector2(0, targetPipPosition);

                pips[0].SetActive(targetPipPosition >= FirstPip);

                if (pips[0].IsActive())
                {
                    if (_goldManaFloor == 0 && _goldManaCeiling > 0) pips[0].Play("bottom-gold");
                    else pips[0].Play("bottom");
                }


                for (var i = 1; i < pips.Count; i++)
                {
                    pips[i].SetActive( targetPipPosition >= (FirstPip + PipFirstDistance) + (PipDistance * (i-1)));

                    if(pips[i].IsActive())
                    {
                        if(i >= _goldManaFloor && i < _goldManaCeiling) pips[i].Play("mid-2-gold");
                        else pips[i].Play("mid-2");
                    }
                }

                if (pips.TrueForAll(p => p.IsActive()))
                {
                    activePip.SetActive(false);
                }
                else if (!activePip.IsActive())
                {
                    activePip.SetActive(true);
                }

                if (activePip.IsActive())
                {
                    if (_goldManaAmt > 0) activePip.Play("top-gold");
                    else activePip.Play("top");
                }
            }
            else
            {
                if (_playSfx)
                {
                    AudioController.StopSound("battle_turn_manafill");
                    _playSfx = false;
                }
            }
        }
    }
}
