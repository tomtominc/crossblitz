
using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Databases;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ViewAPI.Popups;
using UnityEngine;

namespace CrossBlitz.Hero
{
    public class HeroLimitBreakCardView : MonoBehaviour
    {
        public RectTransform cardLayout;
        public GameObject cardPrefab;
        public FadeWindow fadeWindow;

        private CardView _cardView;
        private string _cardUid;

        public void Show(string cardUid)
        {
            fadeWindow.Show();

            //if (_cardUid != cardUid)
            {
                _cardUid = cardUid;

                if (_cardView != null)
                {
                    Destroy(_cardView.gameObject);
                }

                if (GameServer.IsRunning())
                {
                    var gameState = GameServer.State.GetCard(_cardUid);

                    _cardView = CardFactory.Create(new CreateCardFactorySettings
                    {
                        CardState = gameState,
                        CardPrefab = cardPrefab,
                        Layout = cardLayout,
                        StartsDisabled = false,
                        AdditionalComponents = new List<Type> { typeof(GameCardView) }
                    }, null);

                    _cardView.RectTransform().anchoredPosition = Vector2.zero;
                    _cardView.CanvasGroup.blocksRaycasts = false;
                    _cardView.CanvasGroup.interactable = false;
                    _cardView.SetGameState( gameState );
                }
                else
                {
                    var card = Db.CardDatabase.GetCard(_cardUid);

                    _cardView = CardFactory.Create(new CreateCardFactorySettings
                    {
                        CardData = card,
                        CardPrefab = cardPrefab,
                        Layout = cardLayout,
                        StartsDisabled = false
                    }, null);

                    _cardView.RectTransform().anchoredPosition = Vector2.zero;
                    _cardView.CanvasGroup.blocksRaycasts = false;
                    _cardView.CanvasGroup.interactable = false;
                }
            }
        }

        public void Hide()
        {
            fadeWindow.Hide();
        }
    }
}