using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Databases;
using CrossBlitz.GameVfx;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using CrossBlitz.Settings;
using Source.Scripts.Hero;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using MEC;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Hero
{
    public class HeroViewBattle : HeroView
    {
        public BoardTarget boardTarget;
        public RectTransform portraitContainerParent;
        public TextMeshProUGUI cardCounter;
        public TrickCardContainer trickCardContainer;
        public BattleArmorView armorView;
        public ManaView manaView;
        public SpriteAnimation portraitGlow;
        public SpriteAnimation frame;
        public HeroLimitBreakView heroLimitBreak;
        public GameObject healParticleObject;
        public RectTransform healEffectContainer;
        public RectTransform healEffectPortraitContainer;
        public SpriteAnimation healEffectPortrait;
        public TextMeshProUGUI healEffectlabel;

        //public List<RelicSlot> relicSlots;

        private Team _team;
        private string _uid;

        public RectTransform deathExplosionParent;
        public SpriteAnimation deathExplosionPrefab;
        public bool guardAnimation;
        private bool _updateDeathExplosions;
        private float _deathExplosionTimer;
        private float _stopDeathExplosionTimer;
        private float _shakeTimer;
        private bool _updateHealParticles;
        private float _healParticleTimer;
        private bool _blitzBurstActive;
        private string[] sfxArray;

        public GameCardState State => GameServer.State.GetCard(_uid);

        private Vector2 _originalPortraitPosition;
        private Vector2 _originalHealthIconPosition;
        private Vector2 _originalHealthMaxPosition;

        protected override void Start()
        {
            base.Start();

            boardTarget.Initialize(this);

            Events.Subscribe(EventType.OnCardDrawn, OnPlayerDraw);
            Events.Subscribe(EventType.OnCardChangedLocation, OnCardChangedLocation);

            _originalPortraitPosition = portraitContainerParent.anchoredPosition;
            _originalHealthIconPosition = healthIcon.anchoredPosition;
            _originalHealthMaxPosition = healthMaxLabel.rectTransform.anchoredPosition;
            _blitzBurstActive = false;
            guardAnimation = false;

            sfxArray = new string[3];
            sfxArray[0] = "CutsceneSFX-Smack1";
            sfxArray[1] = "CutsceneSFX-Smack2";
            sfxArray[2] = "CutsceneSFX-Smack3";
        }



        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnCardDrawn, OnPlayerDraw);
            Events.Unsubscribe(EventType.OnCardChangedLocation, OnCardChangedLocation);
        }

        protected override void Update()
        {
            base.Update();

            if (_updateDeathExplosions)
            {
                _deathExplosionTimer -= Time.deltaTime;
                _stopDeathExplosionTimer += Time.deltaTime;

                if (_deathExplosionTimer <= 0)
                {
                    _deathExplosionTimer = 0.08f;
                    var deathExplosion = Instantiate(deathExplosionPrefab, deathExplosionParent);
                    var x = Random.Range(deathExplosionParent.anchoredPosition.x - deathExplosionParent.rect.width / 2,
                        deathExplosionParent.anchoredPosition.x + deathExplosionParent.rect.width / 2);
                    var y = Random.Range(deathExplosionParent.anchoredPosition.y - deathExplosionParent.rect.height / 2,
                        deathExplosionParent.anchoredPosition.y + deathExplosionParent.rect.height / 2);
                    deathExplosion.RectTransform().anchoredPosition = new Vector2(x, y);
                    deathExplosion.Play("explosion");

                    //AudioController.PlaySound(sfxArray[Random.Range(0, sfxArray.Length)]);
                }

                _shakeTimer -= Time.deltaTime;

                if (_shakeTimer <= 0)
                {
                    _shakeTimer = 0.04f;
                    portraitContainerParent.anchoredPosition = _originalPortraitPosition + new Vector2( Random.Range(-8f, 12f),Random.Range(-12f, 8f));
                }

                if(_stopDeathExplosionTimer >= 2)
                {
                    _updateDeathExplosions = false;
                }

            }

            if (_updateHealParticles)
            {
                _healParticleTimer +=  Time.deltaTime;

                if(_healParticleTimer >= 0.1f)
                {
                    var healParticle = Instantiate(healParticleObject, healEffectContainer);

                    var parentTransform = healthLabel.RectTransform();
                    var x = Random.Range(parentTransform.anchoredPosition.x - parentTransform.rect.width / 3,
                        parentTransform.anchoredPosition.x + parentTransform.rect.width / 3);
                    var y = Random.Range(parentTransform.anchoredPosition.y - parentTransform.rect.height / 2,
                    parentTransform.anchoredPosition.y + parentTransform.rect.height / 2);
                    healParticle.RectTransform().anchoredPosition = new Vector2(x, y);

                    var newAnchorPosition = new Vector2(x, y + 15f);

                    healParticle.RectTransform().DOAnchorPos(newAnchorPosition, 0.5f).SetEase(Ease.OutCubic);
                    _healParticleTimer = 0f;
                }
            }
        }

        private void OnPlayerDraw(IMessage message)
        {
            if (message.Data is OnCardDrawnEventArgs args)
            {
                if (args.team == Team.Opponent)
                {
                    UpdateOpponentHandCount();
                }
            }
        }

        private void OnCardChangedLocation(IMessage message)
        {
            if (message.Data is OnCardChangedLocationEventArgs args)
            {
                var card = GameServer.State.GetCard(args.cardUid);

                if (card.Team == Team.Opponent)
                {
                    UpdateOpponentHandCount();
                }
            }
        }

        public void Heal(CardHistory history)
        {
            // todo: cool heal effects
            Timing.RunCoroutine(HealEffect(history.HealAmount));
            UpdateViewFromState(true);
        }

        private TargetingEffect m_spellTargetingEffect;
        public IEnumerator<float> ShowSpellTargetingEffect()
        {
            if (m_spellTargetingEffect == null)
            {
                var getTargetingEffect = GameVfxProvider.GetWorldVfx("TargetingEffect");
                yield return Timing.WaitUntilDone(getTargetingEffect.AsCoroutine());
                m_spellTargetingEffect = (TargetingEffect)getTargetingEffect.Result;
            }

            m_spellTargetingEffect.Show(boardTarget.RectTransform(), false, Vector3.down);
        }

        public void HideSpellTargetingEffect()
        {
            if (m_spellTargetingEffect != null)
            {
                m_spellTargetingEffect.Hide();
                m_spellTargetingEffect = null;
            }
        }

        public IEnumerator<float> HealEffect(int healAmount)
        {
            _updateHealParticles = true;

            healEffectlabel.text = "+" + healAmount.ToString();
            Debug.Log("healAmount: " + healAmount.ToString());
            Debug.Log(healthLabel.text);

            // Burst the bottom Bit where your health is
            healthLabel.RectTransform().DOPunchAnchorPos(Vector2.up * 5f, 0.5f).SetEase(Ease.InBack);
            healthHealEffect.SetActive(true);
            healthHealEffect.Play("burst", () => healthHealEffect.SetActive(false));

            // Burst the Top bit showing the healing value
            healEffectPortraitContainer.SetActive(true);

            healEffectlabel.GetComponent<CanvasGroup>().alpha = 0;
            healEffectPortrait.SetActive(true);
            healEffectPortrait.Play("burst", () => healEffectPortrait.SetActive(false));

            // Update Portrait container positioning
            healEffectPortraitContainer.anchoredPosition = new Vector2(0, -22);
            var newAnchorPosition = new Vector2(healEffectPortraitContainer.anchoredPosition.x, healEffectPortraitContainer.anchoredPosition.y + 25f);
            healEffectPortraitContainer.DOAnchorPos(newAnchorPosition, 0.5f).SetEase(Ease.OutCubic);
            healEffectlabel.GetComponent<CanvasGroup>().DOFade(1f, 0.25f);

            // Punch and glow the portrait
            portraitContainerParent.DOPunchAnchorPos(Vector2.up * 5f, 0.5f).SetEase(Ease.InBack);
            portraitGlow.SetActive(true);
            portraitGlow.image.color = "#ffffff".ToColor();
            portraitGlow.image.DOFade(1f, 0.2f);

            yield return Timing.WaitForSeconds(0.2f);

            portraitGlow.image.DOFade(0f, 0.2f);

            yield return Timing.WaitForSeconds(0.8f);

            newAnchorPosition = new Vector2(healEffectPortraitContainer.anchoredPosition.x, healEffectPortraitContainer.anchoredPosition.y + 25f);
            healEffectPortraitContainer.DOAnchorPos(newAnchorPosition, 0.5f).SetEase(Ease.OutCubic);
            healEffectlabel.GetComponent<CanvasGroup>().DOFade(0f, 0.25f);

            //healthLabel.Label.color = "ffffff".ToColor();

            yield return Timing.WaitForSeconds(0.25f);

            healEffectPortraitContainer.SetActive(false);
            healEffectPortrait.SetActive(true);
            _updateHealParticles = false;


            //var resetPortraitGlow = !portraitGlow.IsActive();
            //if (resetPortraitGlow) portraitGlow.SetActive(true);

            //_glowAlpha = Mathf.PingPong(Time.time, 0.8f);
            //portraitGlow.color = "#ffffff".ToColor(_glowAlpha);

        }


        private void UpdateOpponentHandCount()
        {
            if (cardCounter == null) return;

            cardCounter.text = $"x{GameServer.OpponentPlayerState.hand.Count}";
        }

        public virtual async void SetHeroForBattle(GameCardState heroCard)
        {
            _uid = heroCard.uid;

            if (_team == Team.Client)
            {
                // todo: This might be a regular card in the future (like ship mini game or something!)
                SetHero(heroCard.characterData, App.HeroData.GetHero(heroCard.characterData.id));
            }
            else // fake this for now for fables!
            {
                if (GameServer.Mode.GameType == GameType.STORY && !string.IsNullOrEmpty( GameServer.Mode.BattleUid) )
                {
                    var battleData = Db.BattleDatabase.GetBattleData( GameServer.Mode.BattleUid );

                    if (battleData != null)
                    {
                        heroCard.characterData.name = battleData.Name;
                        heroCard.characterData.faction = battleData.Faction;

                        SetHero(heroCard.characterData, new SavedHeroData
                        {
                            health = battleData.Health,
                            level = battleData.Level,
                            unlocked = true
                        });
                    }
                }

                frame.Play("enemy");
            }

            while (isBusy) await UnityAsync.Await.NextUpdate();

            if (Db.HeroDatabase.IsHero(HeroData.id))
            {
                portraitGlow.Play($"{HeroData.id.ToLower()}-idle");
                portraitGlow.SetActive(false);
            }
            else
            {
                portraitGlow.image.sprite = portraitCropped.image.sprite;
                portraitGlow.image.SetNativeSize();
                portraitGlow.RectTransform().anchoredPosition = HeroData.standardOffset;
            }

            //healthMaxLabel.text = "/ " + 55//State.GetDatabaseMaxHealth().ToString();
        }

        public void DealDamageUsingPendingHealth()
        {
            // UpdateViewFromState(false, true);
            //
            // portraitCropped.Play($"{HeroData.id.ToLower()}-hurt", () =>
            // {
            //     UpdateViewFromState(true, true);
            // });

            DealDamage();
        }

        public void DealDamage()
        {
            UpdateViewFromState(false);

            portraitCropped.Play($"{HeroData.id.ToLower()}-hurt", () =>
            {
                UpdateViewFromState(true);
            });
        }

        public virtual void UpdateViewFromState(bool setIdleAnimation)
        {
            var health = State.GetHealth();
            var armor = State.GetArmor();

            if (health < 0)
            {
                health = 0;
            }

            armorView.AnimateShield(armor);
            healthLabel.text = health.ToString();
            healthMaxLabel.text = "/ " + State.GetMaxHealthWithBuffs();

            if (health > 9)
            {
                healthIcon.anchoredPosition = _originalHealthIconPosition;
                healthMaxLabel.rectTransform.anchoredPosition = _originalHealthMaxPosition;
            }
            else
            {
                healthMaxLabel.rectTransform.anchoredPosition = _originalHealthMaxPosition - new Vector2(8f, 0f);
            }

            if (_blitzBurstActive)
            {
                if (portraitCropped.CurrentAnimationName != $"{HeroData.id.ToLower()}-bb-idle")
                {
                    portraitCropped.Play($"{HeroData.id.ToLower()}-bb-idle", Random.Range(0, 5));
                }
            }
            else if (setIdleAnimation)
            {
                var lowHealthAmt = State.GetMaxHealthWithBuffs() / 3f;
                if (health <= lowHealthAmt)
                {
                    if (portraitCropped.CurrentAnimationName != $"{HeroData.id.ToLower()}-low-hp")
                    {
                        portraitCropped.Play($"{HeroData.id.ToLower()}-low-hp", Random.Range(0, 5));
                    }
                }
                else
                {
                    if (portraitCropped.CurrentAnimationName != $"{HeroData.id.ToLower()}-idle")
                    {
                        portraitCropped.Play($"{HeroData.id.ToLower()}-idle", Random.Range(0, 11));
                    }
                }
            }

        }

        public virtual void SetPlayerInfo(Team team)
        {
            _team = team;

            var player = GameServer.GetPlayerState(_team);
            var hero = GameServer.State.GetCard(player.heroUid);
            var blitzBurst = GameServer.State.GetCard(player.blitzBurst);

            SetHeroForBattle(hero);

            trickCardContainer.StartBattle(_team);
            armorView.StartBattle(_team);

            if (blitzBurst != null)
            {
                heroLimitBreak.SetCard(blitzBurst.uid);
            }

            var slot = 1;

            for (var i = 0; i < player.relics.Count; i++)
            {
                var relic = GameServer.State.GetCard(player.relics[i]);

                if (relic.data.type == CardType.Elder_Relic)
                {
                    relicSlots[0].SetRelic(relic.data.id, relic.uid);
                }
                else
                {
                    relicSlots[slot].SetRelic(relic.data.id, relic.uid);
                    slot++;
                }
            }

            for (var i = 0; i < relicSlots.Count; i++)
            {
                relicSlots[i].UpdateRelicUsage();
            }
        }

        public void PlayDeath(int num)
        {

            _updateDeathExplosions = true;

            switch (num)
            {
                case 0:
                    AudioController.PlaySound("battle_end_punchgoop_lose", "BARDSFX");
                    break;
                case 1:
                    AudioController.PlaySound("battle_end_punchgoop_win", "BARDSFX");
                    break;
                case 2:
                    break;
            }

            portraitCropped.Play($"{HeroData.id.ToLower()}-hurt");

        }

        public void SetBlitzBurstActive(bool active)
        {
            _blitzBurstActive = active;
        }
    }
}
