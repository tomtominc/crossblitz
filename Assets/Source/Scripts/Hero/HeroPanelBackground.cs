using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.Hero
{
    public class HeroPanelBackground : MonoBehaviour
    {
        [InfoBox("Sets all SpriteAnimators to the faction.ToLower() animation, this is an easy way to setup a lot of images at once.")]
        public List<SpriteAnimation> shadows;

        public virtual void SetFaction(string factionLower)
        {
            for (var i = 0; i < shadows.Count; i++)
            {
                shadows[i].Play(factionLower);
            }
        }
    }
}