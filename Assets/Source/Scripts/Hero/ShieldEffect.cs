using System.Collections.Generic;
using DG.Tweening;
using MEC;
using TMPro;
using UnityEngine;

namespace CrossBlitz.Hero
{
    public class ShieldEffect : MonoBehaviour
    {
        public SpriteAnimation animator;
        public TMPSpriteFont armorGain;

        public IEnumerator<float> Animate(int gain)
        {
            armorGain.text = gain.ToString();
            armorGain.SetActive(false);

            animator.SetActive(true);
            animator.Play("spawn");

            while (animator.CurrentFrame < 3)
            {
                yield return Timing.WaitForOneFrame;
            }

            armorGain.SetActive(true);

            yield return Timing.WaitForSeconds(0.88f);

            this.RectTransform().DOAnchorPosY(this.RectTransform().anchoredPosition.y + 32, 0.32f);
            animator.image.DOFade(0, 0.32f);

            yield return Timing.WaitForSeconds(0.32f);

            Destroy(gameObject);
        }
    }
}