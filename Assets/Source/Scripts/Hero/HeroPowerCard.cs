using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.Hero
{
    public class HeroPowerCard : MonoBehaviour
    {
        public bool isClientPowerCard;
        public SpriteAnimation animator;
        [ShowIf("isClientPowerCard")]
        public SpriteAnimation glow;

        private int _index;
        private string _guid;
        private string _cardId;

        public GameCardState GameCardState
        {
            get
            {
                if (string.IsNullOrEmpty(_guid)) return null;
                if (GameServer.State == null) return null;
                return GameServer.State.GetCard(_guid);
            }
        }

        public CardData CardData
        {
            get
            {
                if (string.IsNullOrEmpty(_cardId)) return null;
                return Db.CardDatabase.GetCard(_cardId);
            }
        }


        public void SetGuid(int index, string guid)
        {
            _index = index;
            _guid = guid;

            UpdateState();
        }

        public void SetCardId(int index, string cardId)
        {
            _index = index;
            _cardId = cardId;

            UpdateState();
        }

        public void UpdateState()
        {
            //todo: remove this when actually impolementing
            if (isClientPowerCard)
            {
                if (_index == 1)
                {
                    animator.Play($"{_index}-ready");
                    glow.SetActive(true);
                    glow.Play("idle");
                }
                else
                {
                    animator.Play($"{_index}-unavailable");
                    glow.SetActive(false);
                }
            }
            else
            {
                if (_index == 1) animator.Play($"{_index}-standard");
                else animator.Play($"{_index}-unavailable");
            }

            return;
            //-----------
            if (GameCardState != null)
            {
                if (GameCardState.powerHasBeenTaken)
                {
                    animator.Play($"{_index}-unavailable");
                    if (glow) glow.SetActive(false);
                }
                else if (isClientPowerCard &&
                         GameCardState.data.cost <= GameServer.ClientPlayerState.currentMana)
                {
                    animator.Play($"{_index}-ready");
                    if (glow)
                    {
                        glow.SetActive(true);
                        glow.Play("idle");
                    }
                }
                else
                {
                    animator.Play($"{_index}-standard");
                    if (glow) glow.SetActive(false);
                }
            }
            else
            {
                animator.Play($"{_index}-standard");
                if (glow) glow.SetActive(false);
            }
        }
    }
}