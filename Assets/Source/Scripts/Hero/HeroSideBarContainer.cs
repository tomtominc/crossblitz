using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using CrossBlitz.Hero;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.Transition;
using DG.Tweening;
using MEC;
using Source.Scripts.AssetManagement;
using Source.Scripts.Card;
using UnityEngine;

namespace Source.Scripts.Hero
{
    public class HeroSideBarContainer : MonoBehaviour, IAssetLoader
    {
        public RectTransform playerPanel;
        public RectTransform opponentPanel;

        public CanvasGroup playerOverlayHitEffect;
        public CanvasGroup opponentOverlayHitEffect;

        public RectTransform heroEffectsScreen;
        public HeroAttackedColumnEffect columnEffectPrefab;

        public RectTransform playerHeroContainer;
        public RectTransform opponentHeroContainer;

        public HeroViewBattle playerHeroView;
        public HeroViewBattle opponentHeroView;

        private List<GameObject> _assetsToUnload;
        public string AssetName => name;
        private bool _isBusy = true;
        public bool IsBusy => _isBusy;

        public void LoadForBattle()
        {
            _assetsToUnload = new List<GameObject>();

            LoadForBattleAsync();
        }

        // private void Update()
        // {
        //     // if (Input.GetKeyDown(KeyCode.A))
        //     // {
        //     //     Timing.RunCoroutine(HitPortrait(Team.Opponent, null, new AttackCommand()));
        //     // }
        // }

        public void TriggerRelicSlot(string relicUid)
        {
            for (var i = 0; i < playerHeroView.relicSlots.Count; i++)
            {
                if (!string.IsNullOrEmpty(playerHeroView.relicSlots[i].RelicUid) &&
                    playerHeroView.relicSlots[i].RelicUid == relicUid)
                {
                    playerHeroView.relicSlots[i].Trigger();
                }
            }

            for (var i = 0; i < opponentHeroView.relicSlots.Count; i++)
            {
                if (!string.IsNullOrEmpty(opponentHeroView.relicSlots[i].RelicUid) &&
                    opponentHeroView.relicSlots[i].RelicUid == relicUid)
                {
                    opponentHeroView.relicSlots[i].Trigger();
                }
            }
        }

        public HeroViewBattle GetHeroView(Team team)
        {
            if (team == GameServer.LocalPlayerTeam)
            {
                return playerHeroView;
            }

            return opponentHeroView;
        }

        public IEnumerator<float> HitPortraitNonAttack(Team team, DamageCommand damageCommand, string damageHistoryUid)
        {
            var drag = opponentHeroView.heroPortraitContainer.GetComponent<DragRotator>();
            drag.enabled = false;

            var heroPanel = team == Team.Client ? playerPanel : opponentPanel;
            var heroPortrait =team == Team.Client ?playerHeroView.heroPortraitContainer : opponentHeroView.heroPortraitContainer;
            var hpContainer = team == Team.Client ? playerHeroView.healthLabel.RectTransform() : opponentHeroView.healthLabel.RectTransform();
            var tiles = GameplayScreen.Instance.gameBoard.tiles.FindAll(x => x.Team == team);
            var overlayEffect =team == Team.Client ? playerOverlayHitEffect : opponentOverlayHitEffect;
            var heroView = team == Team.Client ? playerHeroView : opponentHeroView;
            var hitsArmorOnly = heroView.State.GetArmor() > 0;
            var heroDamagedEffect = hitsArmorOnly ? heroView.armorHitEffect : heroView.healthHitEffect;

            // if (hitEffectHandle.IsRunning)
            // {
            //     Timing.KillCoroutines(hitEffectHandle);
            // }

            if (!hitEffectHandle.IsRunning)
            {
                hitEffectHandle = Timing.RunCoroutine(DoHitEffect(heroDamagedEffect,hitsArmorOnly));
            }

            // heroDamagedEffect.SetActive(true);
            //
            // for (var i = 0; i < heroDamagedEffect.animations.Count; i++)
            // {
            //     heroDamagedEffect.animations[i].Play("burst");
            // }

            for (int i = 0; i < tiles.Count; i++)
            {
                tiles[i].DoPunchHit();
            }

            const float duration = 0.25f;

            GameplayScreen.Instance.ShakeScreen(6, 1f);

            overlayEffect.SetActive(true);
            overlayEffect.DOKill();
            overlayEffect.alpha = 0;
            overlayEffect.DOFade(1, 0.08f);
            overlayEffect.DOFade(0, 0.8f).SetDelay(0.4f);

            heroPanel.DOKill();
            heroPanel.DOPunchAnchorPos(Vector2.left * 4f, 1f, 10, 0.1f);

            heroPortrait.DOKill();
            heroPortrait.DOBlendableRotateBy(new Vector3(0, -45f, 0), duration);
            heroPortrait.DOBlendableRotateBy(new Vector3(0, 90f, 0), duration).SetDelay(duration/2f);
            heroPortrait.DOBlendableRotateBy(new Vector3(0, -45f, 0), duration*2f).SetDelay(duration).SetEase(Ease.OutBounce);
            heroPortrait.DOPunchScale(new Vector3(0.16f, 0.16f), duration, 3, 0.1f);
            heroPortrait.DOPunchAnchorPos(new Vector2(0, 6), duration, 3, 0.1f);

            // var color = new Color(1,1,1, 0.9f);
            // heroDamagedEffect.images[0].color = color;
            hpContainer.anchoredPosition = new Vector2(0, 5);

            yield return Timing.WaitForOneFrame;

            heroView.DealDamageUsingPendingHealth();

            yield return Timing.WaitForSeconds(0.04f);

            hpContainer.anchoredPosition = new Vector2(-5,-4);
            // color = hitsArmorOnly ? "8cb0b1".ToColor() :  "ad2e2e".ToColor();
            // color.a = 0.9f;
            // heroDamagedEffect.images[0].color = color;

            if (hitsArmorOnly)
            {
                heroView.armorView.armorValue.resourceName = "card-font-1";
                heroView.armorView.armorValue.ForceRefresh();
            }
            else
            {
                heroView.healthLabel.resourceName = "HP-font-damaged";
                heroView.healthLabel.ForceRefresh();
            }

            yield return Timing. WaitForSeconds(0.04f);

            if (!hitsArmorOnly)
            {
                hpContainer.anchoredPosition = new Vector2(8,-6);
            }

            // color.a = 0.75f;
            // heroDamagedEffect.images[0].color = color;
            yield return Timing. WaitForSeconds(0.04f);

            if (!hitsArmorOnly)
            {
                hpContainer.anchoredPosition = new Vector2(-2,2);
            }

            // color.a = 0.6f;
            // heroDamagedEffect.images[0].color = color;
            // heroDamagedEffect.images[0].DOFade(0, 0.4f);
            yield return Timing. WaitForSeconds(0.04f);

            if (!hitsArmorOnly)
            {
                hpContainer.anchoredPosition = new Vector2(1,-2);
            }

            if (hitsArmorOnly)
            {
                heroView.armorView.armorValue.resourceName = "player-shield-font";
                heroView.armorView.armorValue.ForceRefresh();
            }
            else
            {
                heroView.healthLabel.resourceName = "HP-font";
                heroView.healthLabel.ForceRefresh();
            }


            yield return Timing. WaitForSeconds(0.04f);

            if (!hitsArmorOnly)
            {
                hpContainer.anchoredPosition = new Vector2(0,0);
            }
        }

        private CoroutineHandle hitEffectHandle;
        private IEnumerator<float> DoHitEffect(CardEffect hitEffect, bool hitsArmorOnly)
        {
            hitEffect.SetActive(true);
            hitEffect.images[0].DOKill();

            for (var i = 0; i < hitEffect.animations.Count; i++)
            {
                hitEffect.animations[i].Play("burst");
            }

            var color = new Color(1,1,1, 0.9f);
            hitEffect.images[0].color = color;

            yield return Timing.WaitForOneFrame;
            yield return Timing.WaitForSeconds(0.04f);

            color = hitsArmorOnly ? "8cb0b1".ToColor() :  "ad2e2e".ToColor();
            color.a = 0.9f;
            hitEffect.images[0].color = color;

            yield return Timing. WaitForSeconds(0.04f);

            color.a = 0.75f;
            hitEffect.images[0].color = color;

            yield return Timing. WaitForSeconds(0.04f);

            color.a = 0.6f;
            hitEffect.images[0].color = color;
            hitEffect.images[0].DOFade(0, 0.4f);

            yield return Timing.WaitForSeconds(0.4f);

            hitEffect.SetActive(false);
        }

        public IEnumerator<float> HitPortrait(Team team, GameCardState attacker, AttackCommand command)
        {
            var drag = opponentHeroView.heroPortraitContainer.GetComponent<DragRotator>();
            drag.enabled = false;

            var heroPanel = team == Team.Client ? playerPanel : opponentPanel;
            var heroPortrait =team == Team.Client ?playerHeroView.heroPortraitContainer : opponentHeroView.heroPortraitContainer;
            var hpContainer = team == Team.Client ? playerHeroView.healthLabel.RectTransform() : opponentHeroView.healthLabel.RectTransform();
            var tiles = GameplayScreen.Instance.gameBoard.tiles.FindAll(x => x.Team == team);
            var overlayEffect =team == Team.Client ? playerOverlayHitEffect : opponentOverlayHitEffect;
            var heroView = team == Team.Client ? playerHeroView : opponentHeroView;
            var hitsArmorOnly = heroView.State.GetArmor() > 0;
            var heroDamagedEffect = hitsArmorOnly ? heroView.armorHitEffect : heroView.healthHitEffect;

            if (hitEffectHandle.IsRunning)
            {
                Timing.KillCoroutines(hitEffectHandle);
            }

            if (!hitEffectHandle.IsRunning)
            {
                hitEffectHandle = Timing.RunCoroutine(DoHitEffect(heroDamagedEffect,hitsArmorOnly).CancelWith(gameObject));
            }

            hpContainer.DOKill(true);
            // heroDamagedEffect.SetActive(true);
            //
            // for (var i = 0; i < heroDamagedEffect.animations.Count; i++)
            // {
            //     heroDamagedEffect.animations[i].Play("burst");
            // }

            for (int i = 0; i < tiles.Count; i++)
            {
                tiles[i].DoPunchHit();
            }

            HeroAttackedColumnEffect columnEffect = null;

            if (attacker != null && !attacker.IsHero)
            {
                switch (attacker.data.type)
                {
                    case CardType.Minion:

                        var getBoardCardTask = GameBoard.GetBoardCard(attacker.uid);
                        yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                        var getBoardCardResult = getBoardCardTask.Result;

                        if (getBoardCardResult.error != ClientErrorCode.None)
                        {
                            CrossBlitz.ServerAPI.Server.HandleClientError(getBoardCardResult, false);
                        }

                        if (getBoardCardResult.boardCard)
                        {
                            columnEffect = Instantiate(columnEffectPrefab, heroEffectsScreen, false);
                            columnEffect.Initialize(team, getBoardCardResult.boardCard);
                            Timing.RunCoroutine(columnEffect.Hit());
                        }
                        break;
                }
            }

            const float duration = 0.25f;

            GameplayScreen.Instance.ShakeScreen(6, 1f);

            overlayEffect.SetActive(true);
            overlayEffect.DOKill();
            overlayEffect.alpha = 0;
            overlayEffect.DOFade(1, 0.08f);
            overlayEffect.DOFade(0, 0.8f).SetDelay(0.4f);

            heroPanel.DOKill(true);
            heroPanel.DOPunchAnchorPos(Vector2.left * 4f, 1f, 10, 0.1f);

            heroPortrait.DOKill(true);
            heroPortrait.DOBlendableRotateBy(new Vector3(0, -45f, 0), duration);
            heroPortrait.DOBlendableRotateBy(new Vector3(0, 90f, 0), duration).SetDelay(duration/2f);
            heroPortrait.DOBlendableRotateBy(new Vector3(0, -45f, 0), duration*2f).SetDelay(duration).SetEase(Ease.OutBounce);
            heroPortrait.DOPunchScale(new Vector3(0.16f, 0.16f), duration, 3, 0.1f);
            heroPortrait.DOPunchAnchorPos(new Vector2(0, 6), duration, 3, 0.1f);

            // var color = new Color(1,1,1, 0.9f);
            // heroDamagedEffect.images[0].color = color;
            hpContainer.anchoredPosition = new Vector2(0, 5);

            yield return Timing.WaitForOneFrame;

            // wait until the health/armor values have been applied
            heroView.DealDamage();

            yield return Timing.WaitForSeconds(0.04f);

            hpContainer.anchoredPosition = new Vector2(-5,-4);
            // color = hitsArmorOnly ? "8cb0b1".ToColor() :  "ad2e2e".ToColor();
            // color.a = 0.9f;
            // heroDamagedEffect.images[0].color = color;

            if (hitsArmorOnly)
            {
                heroView.armorView.armorValue.resourceName = "card-font-1";
                heroView.armorView.armorValue.ForceRefresh();
            }
            else
            {
                heroView.healthLabel.resourceName = "HP-font-damaged";
                heroView.healthLabel.ForceRefresh();
            }

            yield return Timing. WaitForSeconds(0.04f);

            if (!hitsArmorOnly)
            {
                hpContainer.anchoredPosition = new Vector2(8,-6);
            }

            // color.a = 0.75f;
            // heroDamagedEffect.images[0].color = color;
            yield return Timing. WaitForSeconds(0.04f);

            if (!hitsArmorOnly)
            {
                hpContainer.anchoredPosition = new Vector2(-2,2);
            }

            // color.a = 0.6f;
            // heroDamagedEffect.images[0].color = color;
            // heroDamagedEffect.images[0].DOFade(0, 0.4f);
            yield return Timing. WaitForSeconds(0.04f);

            if (!hitsArmorOnly)
            {
                hpContainer.anchoredPosition = new Vector2(1,-2);
            }

            if (hitsArmorOnly)
            {
                heroView.armorView.armorValue.resourceName = "player-shield-font";
                heroView.armorView.armorValue.ForceRefresh();
            }
            else
            {
                heroView.healthLabel.resourceName = "HP-font";
                heroView.healthLabel.ForceRefresh();
            }


            yield return Timing. WaitForSeconds(0.04f);

            if (!hitsArmorOnly)
            {
                hpContainer.anchoredPosition = new Vector2(0,0);
            }

            if (columnEffect && command != null && command.WaitForColumnExplosionBeforeFinishing)
            {
                while (!columnEffect.IsDone) yield return Timing.WaitForOneFrame;
            }
        }

        private void LoadForBattleAsync()
        {
            _isBusy = true;

            //TransitionController.AssetsToWaitFor.Add(playerHeroView);

            playerHeroView.SetPlayerInfo(Team.Client);

            //TransitionController.AssetsToWaitFor.Add(opponentHeroView);

            opponentHeroView.SetPlayerInfo(Team.Opponent);

            _isBusy = false;
        }

        public void Unload()
        {
            for (var i = 0; i < _assetsToUnload.Count; i++)
            {
                AddressableReferenceLoader.Unload(_assetsToUnload[i]);
            }
        }
    }
}
