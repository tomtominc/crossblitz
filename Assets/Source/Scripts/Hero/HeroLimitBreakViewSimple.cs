using CrossBlitz.Card;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using System.Collections.Generic;
using TakoBoyStudios.Events;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;
using CrossBlitz.Audio;
using CrossBlitz.Utils;
using CrossBlitz.Databases;

namespace CrossBlitz.Hero
{
    public class HeroLimitBreakViewSimple : MonoBehaviour,
        IPointerEnterHandler,
        IPointerExitHandler,
        ICursorSelectable
    {
        public RectTransform zoomedCardParent;

        private string _cardUid;
        private HoverCard m_hoverCard;

        public void SetCard(string cardUid)
        {
            _cardUid = cardUid;
            m_hoverCard = gameObject.AddComponent<HoverCard>();
            m_hoverCard.Initialize(Db.CardDatabase.GetCard(_cardUid), zoomedCardParent, 0);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            //if (string.IsNullOrEmpty(_cardUid) && string.IsNullOrEmpty(_cardId)) return;

            //cardView.Show(_cardUid);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
           // cardView.Hide();
        }

        public bool Interactable => false;
        public bool Grabbable => false;
        public bool InfoOnly => true;
        public bool Loading => false;
    }
}