
using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Databases;
using CrossBlitz.Dialogue;
using CrossBlitz.Fables.Cutscene;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.Transition;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using Source.Scripts.AssetManagement;
using Source.Scripts.Card;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using UnityEngine.U2D;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;
using Random = UnityEngine.Random;

namespace CrossBlitz.Hero
{
    public class HeroView : MonoBehaviour, IAssetLoader
    {
        public const int NormalSortingView = -1;

        public Canvas portraitCanvas;
        public RectTransform heroPortraitContainer;
        public SpriteAnimation portraitCropped;
        public SpriteAnimation portraitBackground;
        public bool formatLevelLabelCentered;
        [ShowIf("formatLevelLabelCentered")]
        public RectTransform levelContainer;
        public SpriteAnimation levelIcon;
        public TextMeshProUGUI levelLabel;
        public TMPSpriteFont healthLabel;
        public RectTransform healthIcon;
        public TextMeshProUGUI healthMaxLabel;
        public SpriteAnimation healthHealEffect;

        public List<RelicSlot> relicSlots;

        public CanvasGroup dialogueContainer;
        public TextMeshProUGUI dialogue;
        public TextMeshProUGUI characterName;
        public SpriteAnimation speechEffect;

        public SpriteAnimation nameContainer;
        public TextMeshProUGUI nameLabel;
        public CardDialogue cardDialogue;

        [Header("Effects")]
        public CardEffect healthHitEffect;
        public CardEffect armorHitEffect;
        public string AssetName => name;
        public CharacterData HeroData => heroData;
        private CharacterData heroData;

        private List<string> assetsToUnload;

        private const float DialogueFadeTime = 1;

        private bool _fadeDialogue;
        private float _fadeDialogueAfterTime;
        private float _fadeTime;


        protected bool isBusy = true;
        public bool IsBusy => isBusy;

        protected virtual void Start()
        {
            Events.Subscribe(EventType.OnHeroLevelChanged, OnHeroLevelChanged);

            // if (TransitionController.AssetsToUnload != null)
            // {
            //     TransitionController.AssetsToUnload.Add(this);
            // }
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnHeroLevelChanged, OnHeroLevelChanged);
        }

        public void SetToNormalSort()
        {
            if (portraitCanvas)
            {
                portraitCanvas.sortingOrder = NormalSortingView + 1;
            }
        }

        protected virtual void Update()
        {
            if (_fadeDialogue)
            {
                _fadeDialogueAfterTime -= Time.deltaTime;

                if (_fadeDialogueAfterTime <= 0)
                {
                    _fadeTime -= Time.deltaTime;
                    dialogueContainer.alpha = _fadeTime / DialogueFadeTime;
                    _fadeDialogue = _fadeTime > 0;
                    if (!_fadeDialogue) dialogueContainer.SetActive(false);
                }
            }
        }

        public void PlayDialogue(string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                Debug.LogError("Trying to play a message on the hero but its empty!");
                return;
            }

            if (cardDialogue == null)
            {
                Debug.LogError("No cutscene character assigned to this hero view!");
                return;
            }

            if (GameServer.State.BattleEventsUpdater.IsPlaying)
            {
                Debug.LogWarning("A battle event is playing, can't play error text when this happens!");
                return;
            }

            cardDialogue.SetDialogue(new DialogueSnippetData
            {
                Dialogue = message
            });

            //cardDialogue.AnimateStartTalking( new DialogueInfo { Text = message}, false );

            // characterName.text =
            //     $"<color={ColorPalette.CrossBlitz.GetFactionColorMedium(heroData.faction)}>{heroData.name.ToUpper()}</color>";
            // dialogue.text = message;
            //
            // dialogueContainer.SetActive(true);
            // dialogueContainer.alpha = 1;
            //
            // dialogueContainer.RectTransform().DOKill();
            // dialogueContainer.RectTransform().DOPunchAnchorPos(Vector2.up * 10f, 0.24f);
            //
            // speechEffect.Play("battlecry-activate");
            //
            // _fadeDialogueAfterTime = DialogueFadeTime;
            // _fadeTime = DialogueFadeTime;
            // _fadeDialogue = true;
        }

        public virtual void SetHero(CharacterData hero,  SavedHeroData savedData)
        {
            if (assetsToUnload == null)
            {
                assetsToUnload = new List<string>();
            }

            heroData = hero;

            //var savedData = App.PlayerData.GetHero(heroData.id);

            if (levelLabel)
            {
                levelLabel.text =  savedData.level.ToString();

                if (formatLevelLabelCentered && levelContainer)
                {
                    var offset = levelLabel.text.Count(c => c.Equals('1'));

                    if (savedData.level < 10)
                    {
                        levelContainer.anchoredPosition = new Vector2(4 + offset + levelContainer.anchoredPosition.x, levelContainer.anchoredPosition.y);
                    }
                    else if (savedData.level < 100)
                    {
                        levelContainer.anchoredPosition = new Vector2(0 + offset+ levelContainer.anchoredPosition.x, levelContainer.anchoredPosition.y);
                    }
                    else
                    {
                        levelContainer.anchoredPosition = new Vector2(-3 + offset+ levelContainer.anchoredPosition.x, levelContainer.anchoredPosition.y);
                    }
                }
            }

            if (levelIcon)
            {
                levelIcon.Play(heroData.faction.ToString().ToLower());
            }

            if (healthLabel)
            {
                healthLabel.text = savedData.health.ToString();
            }

            if (healthMaxLabel)
            {
                healthMaxLabel.text = "/ " + savedData.health.ToString();
            }

            if (nameContainer)
            {
                nameContainer.Play(heroData.faction.ToString().ToLower());
            }

            if (nameLabel)
            {
                nameLabel.text = heroData.name.ToUpper();
            }

            LoadAssetsAsync();
        }

        private void OnHeroLevelChanged(IMessage message)
        {
            if (heroData == null) return;

            if (message.Data is OnHeroLevelChangedEventArgs args)
            {
                if (args.heroId != heroData.id) return;
                SetHero(heroData, App.HeroData.GetHero(heroData.id));
            }
        }

        public void UpdateRelics()
        {
            if (!GameServer.IsRunning())
            {
                String[] prevRelicsAnim = new string[4];
                bool[] prevRelicsActive = new bool[4];
                prevRelicsAnim.Initialize();
                prevRelicsActive.Initialize();

                // Reset Relic Slots
                for (var j = 0; j < relicSlots.Count; j++)
                {

                    if (relicSlots[j].relicIcon)
                    {
                        prevRelicsAnim[j] = relicSlots[j].relicIcon.image.sprite.name;
                        prevRelicsActive[j] = relicSlots[j].relicIcon.image.IsActive();
                    }

                    relicSlots[j].SetAsEmpty();
                }

                //Find relics from deck
                var deckData = App.PlayerDecks.GetDeckBasedOnGameState();
                var k = 1;
                for (var i = 0; i < deckData.cards.Count; i++)
                {
                    var card = Db.CardDatabase.GetCard(deckData.cards[i].id);

                    if (card.type == CardType.Elder_Relic)
                    {
                        relicSlots[0].SetRelic(card.id, deckData.uid);
                    }
                    else if (card.type == CardType.Relic)
                    {
                        relicSlots[k].SetRelic(card.id, deckData.uid);
                        k += 1;

                    }
                }

                for (var j = 0; j < relicSlots.Count; j++)
                {
                    if(prevRelicsAnim[j] != relicSlots[j].relicIcon.image.sprite.name || prevRelicsActive[j] != relicSlots[j].relicIcon.image.IsActive())
                    {
                        relicSlots[j].RectTransform().DOPunchScale(Vector3.one * 0.25f, 0.25f);
                    }
                }
            }
        }

        protected async void LoadAssetsAsync()
        {
            isBusy = true;

            if (portraitCropped)
            {
                if (!Db.HeroDatabase.IsHeroById(HeroData.id) && !string.IsNullOrEmpty(HeroData.portraitUrl))
                {
                    var atlas = await AddressableReferenceLoader.Load<SpriteAtlas>("cards-atlas");
                    portraitCropped.image.sprite = atlas.GetSprite(HeroData.portraitUrl);

                    portraitCropped.image.SetNativeSize();
                    portraitCropped.RectTransform().anchoredPosition = HeroData.standardOffset;
                }
                else
                {
                    portraitCropped.Play($"{HeroData.portraitUrl}-idle", Random.Range(0, 11));
                }

                portraitCropped.SetActive(portraitCropped.image.sprite != null);
            }

            if (portraitBackground)
            {
                portraitBackground.Play(HeroData.faction.ToString().ToLower());
                portraitBackground.SetActive(true);
            }

            isBusy = false;
        }

        public void Unload()
        {
        }

        public static HeroView GetHeroView(Team team)
        {
            if (GameServer.State != null)
            {
                return team == Team.Client ?
                    GameplayScreen.Instance.heroSideBarContainer.playerHeroView :
                    GameplayScreen.Instance.heroSideBarContainer.opponentHeroView;
            }

            return null;
        }
    }
}
