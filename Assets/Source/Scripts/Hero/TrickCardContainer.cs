using System.Collections.Generic;
using CrossBlitz.Card;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Source.Scripts.Hero
{
    public class TrickCardContainer : MonoBehaviour
    {
        public TextMeshProUGUI countLabel;
        public Image trickCardIcon;

        private Team _team;

        public void StartBattle(Team team)
        {
            _team = team;
            gameObject.SetActive(false);
        }
    }
}
