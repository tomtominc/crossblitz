using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using CrossBlitz.Card;
using CrossBlitz.Hero;
using DG.Tweening;
using MEC;
using TMPro;
using CrossBlitz.Audio;

namespace Source.Scripts.Hero
{
    public class BattleArmorView : MonoBehaviour
    {
        public TMPSpriteFont armorValue;
        public Image shieldIcon;
        public SpriteAnimation shieldIconAnimator;
        public CanvasGroup shieldIconCanvas;
        public CanvasGroup shieldOverlay;
        public RectTransform shieldParticleContainer;
        public DestroyAfterAnimation shieldParticle;
        public RectTransform shieldEffectContainer;
        public ShieldEffect shieldEffectPrefab;
        // public TextMeshProUGUI armorGain;

        private Team _team;
        private int _currentArmorValue;
        private int _targetArmorValue;
        private int _targetDirection;
        private bool m_animateArmor;
        private const float TickDelay = 0.08f;
        private float m_ticks;

        public void StartBattle(Team team)
        {
            _team = team;

            armorValue.resourceName = "player-shield-font";

            gameObject.SetActive(false);

            shieldOverlay.SetActive(false);
        }

        private void InstantlySetArmor()
        {
            _currentArmorValue = _targetArmorValue;
            armorValue.text = _currentArmorValue.ToString();
        }

        public IEnumerator<float> AnimateGain(int gain, int totalArmor)
        {
            if (!gameObject.activeSelf || !shieldIconCanvas.IsActive())
            {
                gameObject.SetActive(true);
                armorValue.SetActive(true);
                _currentArmorValue = 0;

                Timing.RunCoroutine( AnimateShieldSpawn() );
            }

            _targetArmorValue = totalArmor;
            _targetDirection = 1;
            m_ticks = 0;

            AudioController.PlaySound("battle_shield_grant", "BARDSFX", true, gameObject);
            Timing.RunCoroutine(PlayShieldParticles(Mathf.Clamp(gain, 1, 10)));

            var shieldEffect = Instantiate(shieldEffectPrefab, shieldEffectContainer, false);
            shieldEffect.RectTransform().anchoredPosition = Vector2.zero;

            InstantlySetArmor();

            yield return Timing.WaitUntilDone(shieldEffect.Animate(gain));
        }

        private IEnumerator<float> PlayShieldParticles(int particleCount)
        {
            var lastPosition = Vector2Int.zero;
            var halfWidth = (int) (shieldParticleContainer.sizeDelta.x * 0.5f);
            var halfHeight = (int) (shieldParticleContainer.sizeDelta.y * 0.5f);
            const int maxNumerations = 100;

            for (var i = 0; i < particleCount; i++)
            {
                var particle = Instantiate(shieldParticle, shieldParticleContainer, false);
                var position = Vector2Int.zero;
                var numerations = 0;

                while ( Mathf.Abs( position.x - lastPosition.x) < 6f || Mathf.Abs( position.y - lastPosition.y) < 6f )
                {
                    var rX = Random.Range(-halfWidth, halfWidth);
                    var rY =  Random.Range(-halfHeight, halfHeight);
                    position = new Vector2Int(rX, rY);
                    numerations++;
                    if (numerations >= maxNumerations) break;
                }

                particle.RectTransform().anchoredPosition = position;
                yield return Timing.WaitForSeconds(0.08f);
            }
        }

        public void AnimateShield(int currentArmor)
        {
            _targetArmorValue = currentArmor;
            m_ticks = 0;

            var diff = _targetArmorValue - _currentArmorValue;

            if (diff == 0)
            {
                if (_targetArmorValue <= 0 && shieldIconCanvas.IsActive())
                {
                    Timing.RunCoroutine( AnimateArmorBreak() );
                }

                return;
            }

            if (diff > 0)
            {
                Timing.RunCoroutine(AnimateGain(diff, currentArmor));
                return;
            }

            // always losing armor if you're here!

            _targetDirection = -1;

            if (_targetArmorValue <= 0 && gameObject.activeSelf)
            {
                _currentArmorValue = 0;
                _targetArmorValue = 0;

                Timing.RunCoroutine( AnimateArmorBreak() );
            }
            else if (_targetDirection < 0)
            {
                shieldIconAnimator.Play("hit");
                AudioController.PlaySound("battle_shield_damage", "BARDSFX", true, gameObject);
                armorValue.RectTransform().DOKill();
                // armorValue.RectTransform().anchoredPosition = Vector2.zero;
                armorValue.RectTransform().DOShakeAnchorPos(0.16f, new Vector2(-4,4));
            }
        }

        private void Update()
        {
            if (_targetArmorValue != _currentArmorValue)
            {
                m_ticks -= Time.deltaTime;

                if (m_ticks <= 0)
                {
                    m_ticks = TickDelay;
                    _currentArmorValue += _targetDirection;
                    armorValue.text = _currentArmorValue.ToString();
                }
            }
        }

        private IEnumerator<float> AnimateShieldSpawn()
        {
            shieldIcon.SetActive(true);
            shieldIconCanvas.alpha = 0;
            shieldOverlay.SetActive(true);
            shieldOverlay.alpha = 1;

            shieldOverlay.DOKill();
            shieldOverlay.DOFade(0, 0.25f);
            shieldIconCanvas.DOKill();
            shieldIconCanvas.DOFade(1, 0.25f);

            shieldIcon.RectTransform().DOKill();
            shieldIcon.RectTransform()
                .DOPunchAnchorPos(Vector2.up * 8, 0.25f);

            shieldOverlay.RectTransform().DOKill();

            yield return  Timing.WaitUntilDone( shieldOverlay.RectTransform()
                .DOScale(Vector3.one * 2, 0.25f)
                .WaitForCompletion(true));

            shieldOverlay.SetActive(false);
        }

        private IEnumerator<float> AnimateArmorBreak()
        {
            InstantlySetArmor();
            armorValue.SetActive(false);
            yield return Timing.WaitUntilDone( shieldIconCanvas.DOFade(0, 0.12f).WaitForCompletion(true));
            shieldIconCanvas.SetActive(false);
        }
    }
}
