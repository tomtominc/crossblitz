using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Cutscene.Data;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.Hero
{
    [System.Serializable]
    public class CharacterData
    {
        [FoldoutGroup("Basic Information", true)]
        public string id;
        [FoldoutGroup("Basic Information", true)]
        public string name;
        [FoldoutGroup("Basic Information", true)][TextArea]
        public string description;
        [FoldoutGroup("Basic Information")]
        public CardType type;

        [FoldoutGroup("Basic Information", true)]
        public string portraitUrl;
        [FoldoutGroup("Basic Information", true)]
        public Faction faction;
        [FoldoutGroup("Basic Information", true)]
        public Faction faction2;
        [FoldoutGroup("Offset Properties")]
        public Vector2Int standardOffset;
        [FoldoutGroup("Offset Properties")]
        public Vector2Int zoomedOffset;
        [FoldoutGroup("Offset Properties")]
        public Vector2Int eventDialogueOffset;
        [FoldoutGroup("Offset Properties")]
        public Vector2Int cutsceneDialogueOffset;
        [FoldoutGroup("Event Dialogue")]
        public Vector2Int eventDialogueShadowOffset;
        [FoldoutGroup("Event Dialogue")]
        public string eventDialogueShadowType;
        [ShowIf("@this.type == CardType.Power")][FoldoutGroup("Blitz Burst")]
        public bool heroPowerCinematicShowEyeFlash;
        [ShowIf("@this.type == CardType.Power")][FoldoutGroup("Blitz Burst")]
        public Vector2Int heroPowerCinematicOffset;
        [ShowIf("@this.type == CardType.Power")][FoldoutGroup("Blitz Burst")]
        public Vector2Int heroPowerCinematicEyeOffset;
        [ShowIf("@this.type == CardType.Power")][FoldoutGroup("Blitz Burst")]
        public string heroPowerCinematicEmotion;


        [FoldoutGroup("Lore Information")][TextArea]
        public string loreDescription;
        [FoldoutGroup("Lore Information")][TextArea]
        public string quote;
        [FoldoutGroup("Lore Information")][TextArea]
        public string likes;
        [FoldoutGroup("Lore Information")][TextArea]
        public string dislikes;
        [FoldoutGroup("Lore Information")]
        public int height;
        [FoldoutGroup("Lore Information")]
        public int weight;
        [FoldoutGroup("Lore Information")]
        public Race race1;
        [FoldoutGroup("Lore Information")]
        public Race race2;

        [FoldoutGroup("Dialogue Properties")]
        public bool rightFacing;
        [FoldoutGroup("Dialogue Properties")]
        public string speechBleep = "Text-LetterC";
        [FoldoutGroup("Dialogue Properties")]
        public string speechPitch = "3";
        [FoldoutGroup("Dialogue Properties")]
        public List<DialogueSnippetData> dialogueSnippets;

        public virtual int GetCost()
        {
            return 0;
        }

        public virtual int GetPower()
        {
            return 0;
        }

        public virtual int GetHealth()
        {
            return 0;
        }

        public string GetIdForSpecificCardAbility()
        {
            if (id == "DevilcoreGroupie") return "BandWorshiper";
            if (id == "UpgradedBomb") return "Bomb";
            return id;
        }

        public DialogueSnippetData GetRandomDialogueSnippet(string category)
        {
            DialogueSnippetData snippet = null;

            var snippets = dialogueSnippets.FindAll(d => d.Option == category);

            if (snippets.Count > 0)
            {
                snippet = snippets[Random.Range(0, snippets.Count)];
            }

            return snippet;
        }

        public static List<string> GetLevelGridDialogueSnippetOptions()
        {
            return new List<string>
            {
                "Level Grid/Hp Up", "Level Grid/Ingredient", "Level Grid/Card", "Level Grid/Mana Shards",
                "Level Grid/Chest"
            };
        }

        public static List<string> GetMatchResultsDialogueSnippetOptions()
        {
            return new List<string>
            {
                "Match Results/Win", "Match Results/Lose", "Match Results/Tie", "Match Results/Level Up", "Fable Results/Complete"
            };
        }

        public static List<string> GetShopDialogueSnippetOptions()
        {
            return new List<string>
            {
                "Shop/Lore", "Shop/Enter", "Shop/Purchase","Shop/Insufficent Funds", "Shop/Item Click", "Shop/Idle", "Shop/Exit", "Shop/OutOfStock"
            };
        }

        public static List<string> GetMinionDialogueSnippetOptions()
        {
            return new List<string>
            {
                "Minion/Enter Hand", "Minion/Played", "Minion/Ability Activated"
            };
        }

        public static List<string> GetHeroErrorDialogueSnippetOptions()
        {
            return new List<string>
            {
                "Battle Error/Card Not Draggable/Generic",
                "Battle Error/Card Not Draggable/Opponent's Card",
                "Battle Error/Card Not Draggable/Not Your Turn",
                "Battle Error/Card Not Draggable/Not Enough Mana",
                "Battle Error/Card Not Draggable/Abilities Haven't Been Setup",
                "Battle Error/Card Not Draggable/No Valid Spell Targets",
                "Battle Error/Card Not Draggable/Game Is Finished",
                "Battle Error/Card Not Draggable/Unknown Error",
            };
        }

        public static List<string> GetAllDialogueSnippetOptions()
        {
            var allDialogues = new List<string>();
            allDialogues.AddRange(GetMatchResultsDialogueSnippetOptions());
            allDialogues.AddRange(GetLevelGridDialogueSnippetOptions());
            allDialogues.AddRange(GetShopDialogueSnippetOptions());
            allDialogues.AddRange(GetMinionDialogueSnippetOptions());
            allDialogues.AddRange(GetHeroErrorDialogueSnippetOptions());
            return allDialogues;

        }


    }

    [System.Serializable]
    public class BlitzBurstCharacterData
    {
        public string emotion;
        public Vector2Int portraitOffset;
    }
}