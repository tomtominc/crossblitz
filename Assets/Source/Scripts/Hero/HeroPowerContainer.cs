using System.Collections.Generic;
using UnityEngine;

namespace CrossBlitz.Hero
{
    public class HeroPowerContainer : MonoBehaviour
    {
        public List<HeroPowerCard> heroPowers;

        /// <summary>
        /// Used ONLY during games, will get the exact guid for the hero powers.
        /// GameCardStates are important in case some card would modify hero powers for whatever reason.
        /// </summary>
        /// <param name="heroCardGuids"></param>
        public void SetupHeroCards(List<string> heroCardGuids)
        {
            // remove hero powers and then add them back when we know they're available
            for (var i = 0; i < heroPowers.Count; i++)
            {
                heroPowers[i].SetActive(false);
            }

            for (var i = 0; i < heroCardGuids.Count; i++)
            {
                heroPowers[i].SetActive(true);
                heroPowers[i].SetGuid(i+1, heroCardGuids[i]);
            }
        }

        public void UpdateCardStates()
        {
            for (var i = 0; i < heroPowers.Count; i++)
            {
                if (heroPowers[i].IsActive())
                {
                    heroPowers[i].UpdateState();
                }
            }
        }

        /// <summary>
        /// Used anywhere but games, this sets up the hero powers with just data.
        /// </summary>
        /// <param name="heroCardIds"></param>
        public void SetupHeroCardsWithCardIds(List<string> heroCardIds)
        {
            // remove hero powers and then add them back when we know they're available
            for (var i = 0; i < heroPowers.Count; i++)
            {
                heroPowers[i].SetActive(false);
            }

            for (var i = 0; i < heroCardIds.Count; i++)
            {
                heroPowers[i].SetActive(true);
                heroPowers[i].SetCardId(i+1,heroCardIds[i]);
            }
        }
    }
}