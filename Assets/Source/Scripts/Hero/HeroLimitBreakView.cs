using CrossBlitz.Card;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using System.Collections.Generic;
using TakoBoyStudios.Events;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;
using CrossBlitz.Audio;

namespace CrossBlitz.Hero
{
    public class HeroLimitBreakView : MonoBehaviour,
        IPointerEnterHandler,
        IPointerExitHandler,
        IPointerClickHandler,
        IPointerDownHandler,
        IPointerUpHandler
    {
        public Team team;
        public HeroLimitBreakCardView cardView;
        public SpriteAnimation bbCard;
        public Image bbBar;
        public RectTransform okayTag;
        public TextMeshProUGUI limitBreakValue;
        public RectTransform barOverlayFader;
        public SpriteAnimation overlayEffect;
        public RectTransform outlineEffect;
        public GameObject blitzBurstReady;
        public SpriteAnimation blitzBurstReadyAnim;
        public TextMeshProUGUI blitzBurstReadyLabel;
        public CanvasGroup blitzBurstReadyLabelCanvas;

        private string _cardUid;
       //private string _cardId;
        private bool _onlyForShow;
        private string _readyText;
        private bool _testBurst = true;


        public void Start()
        {
            SetAsNa();
        }

        //public void Update()
        //{
        //    if (_testBurst && Input.GetMouseButtonDown(1))
        //    {
        //        Debug.Log("CHEESE");
        //        _testBurst = false;
        //        Timing.RunCoroutine(BlitzBurstSpawn());
        //    }
        //}

        public void SetCard(string cardUid)
        {
            _cardUid = cardUid;

            SubscribeToEvents();
            UpdateLimitBreak();
        }

        public void SetCardForShow(string cardId)
        {

        }

        private void SubscribeToEvents()
        {
            var card = GameServer.State.GetCard(_cardUid);

            if (card == null) return;

            Events.Subscribe(EventType.OnBlitzBurstInstantFill, OnBlitzBurstInstantFill);

            switch (card.data.limitBreak.type)
            {
                case LimitBreakType.DRAW_CARDS:
                    Events.Subscribe(EventType.OnCardDrawn, OnCardDrawn);
                    break;
                case LimitBreakType.PLAY_CARD_BY_NAME:
                case LimitBreakType.PLAY_CARD_FROM_CLASS:
                case LimitBreakType.PLAY_CARD_FROM_ANOTHER_FACTION:
                    Events.Subscribe(EventType.OnCardPlayed, OnCardPlayed);
                    break;
                case LimitBreakType.ADD_CARD_BY_NAME_TO_OPPONENTS_DECK:
                    Events.Subscribe(EventType.OnCardAddedToDeck, OnCardAddedToDeck);
                    break;
                case LimitBreakType.MINIONS_ON_YOUR_SIDE_OF_THE_FIELD:
                case LimitBreakType.SUMMON_CLASS:
                case LimitBreakType.PLAY_MINION_WITH_TRIGGER:
                case LimitBreakType.SUMMON_CARD_BY_NAME:
                case LimitBreakType.SUMMON_ANY_MINION:
                case LimitBreakType.YOUR_MINIONS_HAVE_BEEN_DESTROYED:
                    Events.Subscribe(EventType.OnMinionSummoned, OnMinionSummoned);
                    Events.Subscribe(EventType.OnMinionDestroyed, OnMinionDestroyed);
                    break;
                case LimitBreakType.GIVE_OR_RESTORE_HEALTH:
                    Events.Subscribe(EventType.OnCharacterHealed, OnCharacterHealed);
                    Events.Subscribe(EventType.OnCharacterStatsModified, OnCharacterStatsModified);
                    break;
                case LimitBreakType.MINIONS_ARE_FROZEN:
                    Events.Subscribe(EventType.OnMinionFrozen, OnMinionFrozen);
                    break;
                case LimitBreakType.DISCARD_CARDS:
                    Events.Subscribe(EventType.OnCardDiscarded, OnCardDiscarded);
                    break;
                case LimitBreakType.GAIN_ARMOR:
                    Events.Subscribe(EventType.OnGainedArmor, OnGainedArmor);
                    break;
                case LimitBreakType.DAMAGE_WITH_SPELLS:
                case LimitBreakType.FRIENDLY_MINIONS_DAMAGED_BY_YOU:
                    Events.Subscribe(EventType.OnCharacterDamaged, OnCharacterDamaged);
                    break;
                case LimitBreakType.TRAPS_TRIGGERED_BY_PLAYER:
                    Events.Subscribe(EventType.OnTrapTriggered, OnTrapTriggered);
                    break;
                case LimitBreakType.SET_TRAPS:
                    Events.Subscribe(EventType.OnTrapSet, OnTrapSet);
                    break;
            }
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnCardDrawn, OnCardDrawn);
            Events.Unsubscribe(EventType.OnMinionSummoned, OnMinionSummoned);
            Events.Unsubscribe(EventType.OnCardPlayed, OnCardPlayed);
            Events.Unsubscribe(EventType.OnCardAddedToDeck, OnCardAddedToDeck);
            Events.Unsubscribe(EventType.OnMinionFrozen, OnMinionFrozen);
            Events.Unsubscribe(EventType.OnMinionDestroyed, OnMinionDestroyed);
            Events.Unsubscribe(EventType.OnGainedArmor, OnGainedArmor);
            Events.Unsubscribe(EventType.OnCharacterDamaged, OnCharacterDamaged);
            Events.Unsubscribe(EventType.OnCharacterHealed, OnCharacterHealed);
            Events.Unsubscribe(EventType.OnCharacterStatsModified, OnCharacterStatsModified);
            Events.Unsubscribe(EventType.OnCardDiscarded, OnCardDiscarded);
            Events.Unsubscribe(EventType.OnTrapTriggered, OnTrapTriggered);
            Events.Unsubscribe(EventType.OnTrapSet, OnTrapSet);
        }

        public void UpdateLimitBreak()
        {
            var card = GameServer.State.GetCard(_cardUid);

            if (card == null)
            {
                SetAsNa();
                return;
            }

            var value = card.limitBreakValue / (float) card.data.limitBreak.value;

            bbBar.SetActive(true);
            bbCard.SetActive(true);
            limitBreakValue.SetActive(true);

            bbBar.fillAmount = value;
            //bbBar.fillAmount = 100;
            //card.limitBreakValue = 100;

            var heroPanel = GameplayScreen.Instance.heroSideBarContainer.GetHeroView(team);

            if (value >= 1 || card.limitBreakValue >= card.data.limitBreak.value)
            {
                if(okayTag.IsActive()) Timing.RunCoroutine(BlitzBurstSpawn());

                limitBreakValue.SetActive(false);
                okayTag.SetActive(true);
                outlineEffect.SetActive(true);
                barOverlayFader.SetActive(true);
                bbCard.Play("ready");

                if (heroPanel)
                {
                    heroPanel.SetBlitzBurstActive(true);
                    heroPanel.UpdateViewFromState(false);
                }

            }
            else
            {
                limitBreakValue.SetActive(true);
                okayTag.SetActive(false);
                outlineEffect.SetActive(false);
                barOverlayFader.SetActive(false);
                limitBreakValue.text = $"{card.limitBreakValue}/{card.data.limitBreak.value}";
                bbCard.Play("unavailable");

                if (heroPanel)
                {
                    heroPanel.SetBlitzBurstActive(false);
                    heroPanel.UpdateViewFromState(true);
                }
            }
        }

        private void SetAsNa()
        {
            bbBar.fillAmount = 0;
            bbBar.SetActive(false);
            bbCard.SetActive(false);
            okayTag.SetActive(false);
            outlineEffect.SetActive(false);
            barOverlayFader.SetActive(false);
            limitBreakValue.SetActive(false);
        }

        private void OnBlitzBurstInstantFill(IMessage message)
        {
            var card = GameServer.State.GetCard(_cardUid);

            if (card == null)
            {
                SetAsNa();
                return;
            }

            if (message.Data is OnBlitzBurstInstantFillArgs args)
            {
                var fillForTeam = args.playerUid == GameServer.ClientPlayerState.Uid ? Team.Client : Team.Opponent;

                if (team == fillForTeam)
                {
                    if (card.limitBreakValue < card.data.limitBreak.value)
                    {
                        card.limitBreakValue = card.data.limitBreak.value;
                    }

                    UpdateLimitBreak();
                }
            }
        }
        private void OnCardDrawn(IMessage message)
        {
            var card = GameServer.State.GetCard(_cardUid);

            if (card == null)
            {
                SetAsNa();
                return;
            }

            if (message.Data is OnCardDrawnEventArgs args && card.data.limitBreak.type == LimitBreakType.DRAW_CARDS && card.Team == args.team && !args.mulligan)
            {
                card.limitBreakValue++;
                UpdateLimitBreak();
            }
        }

        private void OnTrapTriggered(IMessage message)
        {
            var card = GameServer.State.GetCard(_cardUid);

            if (card == null)
            {
                SetAsNa();
                return;
            }

            if (message.Data is OnTrapTriggeredEventArgs args)
            {
                var triggeredTrap = GameServer.State.GetCard(args.sourceUid);
                if (triggeredTrap == null) return;

                if (triggeredTrap.Team == card.Team)
                {
                    card.limitBreakValue++;
                    UpdateLimitBreak();
                }
            }
        }

        private void OnTrapSet(IMessage message)
        {
            var card = GameServer.State.GetCard(_cardUid);

            if (card == null)
            {
                SetAsNa();
                return;
            }

            if (message.Data is OnTrapSetEventArgs args)
            {
                var trapSet = GameServer.State.GetCard(args.sourceUid);

                if (trapSet == null)
                {
                    return;
                }

                if (trapSet.Team == card.Team)
                {
                    card.limitBreakValue++;
                    UpdateLimitBreak();
                }
            }
        }

        private void OnCharacterStatsModified(IMessage message)
        {
            var card = GameServer.State.GetCard(_cardUid);

            if (card == null)
            {
                SetAsNa();
                return;
            }

            if (message.Data is OnCharacterStatsModifiedEventArgs args && card.data.limitBreak.type == LimitBreakType.GIVE_OR_RESTORE_HEALTH)
            {
                var source = GameServer.State.GetCard(args.appliedBy);
                var target = GameServer.State.GetCard(args.characterUid);

                if(source?.Team == card.Team && target.Team == card.Team)
                {
                    card.limitBreakValue += args.healthAmount;
                    UpdateLimitBreak();
                }
            }
        }

        private void OnCharacterHealed(IMessage message)
        {
            var card = GameServer.State.GetCard(_cardUid);

            if (card == null)
            {
                SetAsNa();
                return;
            }

            if (message.Data is OnCharacterHealedEventArgs args && card.data.limitBreak.type == LimitBreakType.GIVE_OR_RESTORE_HEALTH)
            {
                var source = GameServer.State.GetCard(args.sourceUid);
                var target = GameServer.State.GetCard(args.healedId);

                if(source?.Team == card.Team && target.Team == card.Team)
                {
                    card.limitBreakValue+= args.amount;
                    UpdateLimitBreak();
                }
            }
        }

        private void OnMinionSummoned(IMessage message)
        {
            var card = GameServer.State.GetCard(_cardUid);

            if (card == null)
            {
                SetAsNa();
                return;
            }

            if (message.Data is OnMinionSummonedEventArgs args)
            {
                switch (card.data.limitBreak.type)
                {
                    case LimitBreakType.SUMMON_ANY_MINION:
                    {
                        var summonedMinion = GameServer.State.GetCard(args.cardUid);
                        if ( args.method == SummonCommand.Method.PlayedFromHand && card.Team == summonedMinion.Team)
                        {
                            card.limitBreakValue++;
                            UpdateLimitBreak();
                        }
                        break;
                    }
                    case LimitBreakType.SUMMON_CLASS:
                    {
                        var summonedMinion = GameServer.State.GetCard(args.cardUid);
                        if (args.method == SummonCommand.Method.PlayedFromHand && summonedMinion.data.@class == card.data.limitBreak.@class && card.Team == summonedMinion.Team)
                        {
                            card.limitBreakValue++;
                            UpdateLimitBreak();
                        }

                        break;
                    }
                    case LimitBreakType.SUMMON_CARD_BY_NAME:
                    {
                        var summonedMinion = GameServer.State.GetCard(args.cardUid);
                            // Debug.Log(summonedMinion.data.id);
                            // Debug.Log(card.data.limitBreak.cardId);
                        var minionId = summonedMinion.data.id;
                        if ( minionId == "DevilcoreGroupie" && card.data.limitBreak.cardId == "BandWorshiper")
                        {
                            minionId = "BandWorshiper";
                        }
                        if (args.method == SummonCommand.Method.PlayedFromHand && minionId == card.data.limitBreak.cardId && card.Team == summonedMinion.Team)
                        {
                            card.limitBreakValue++;
                            UpdateLimitBreak();
                        }
                        break;
                    }
                    case LimitBreakType.PLAY_MINION_WITH_TRIGGER:
                    {
                        var summonedMinion = GameServer.State.GetCard(args.cardUid);
                        if (args.method == SummonCommand.Method.PlayedFromHand && summonedMinion.HasTrigger( card.data.limitBreak.triggerType ) && card.Team == summonedMinion.Team)
                        {
                            card.limitBreakValue++;
                            UpdateLimitBreak();
                        }
                        break;
                    }
                    case LimitBreakType.MINIONS_ON_YOUR_SIDE_OF_THE_FIELD:
                    {
                        card.limitBreakValue = 0;
                        var occupiedTiles = GameServer.State.Board.GetOccupiedTiles(card.Team);
                        for (var i = 0; i < occupiedTiles.Count; i++)
                        {
                            var tile = occupiedTiles[i];
                            if (tile.Occupied)
                            {
                                if (card.data.limitBreak.cardIds.Contains(tile.Card.data.id))
                                {
                                    card.limitBreakValue++;
                                }
                            }
                        }
                        UpdateLimitBreak();
                        break;
                    }
                }
            }
        }

        private void OnMinionDestroyed(IMessage message)
        {
            var card = GameServer.State.GetCard(_cardUid);

            if (card == null)
            {
                SetAsNa();
                return;
            }

            if (message.Data is OnMinionDestroyedEventArgs args)
            {
                switch (card.data.limitBreak.type)
                {
                    case LimitBreakType.MINIONS_ON_YOUR_SIDE_OF_THE_FIELD:
                    {
                        card.limitBreakValue = 0;
                        var occupiedTiles = GameServer.State.Board.GetOccupiedTiles(card.Team);
                        for (var i = 0; i < occupiedTiles.Count; i++)
                        {
                            var tile = occupiedTiles[i];
                            if (tile.Occupied)
                            {
                                if (card.data.limitBreak.cardIds.Contains(tile.Card.data.id))
                                {
                                    card.limitBreakValue++;
                                }
                            }
                        }
                        UpdateLimitBreak();
                        break;
                    }
                    case LimitBreakType.YOUR_MINIONS_HAVE_BEEN_DESTROYED:
                    {
                        var destroyedMinion = GameServer.State.GetCard(args.cardUid);

                        if (card.Team == destroyedMinion.Team)
                        {
                            card.limitBreakValue++;
                            UpdateLimitBreak();
                        }

                        break;
                    }
                }
            }
        }

        private void OnGainedArmor(IMessage message)
        {
            if (message.Data is OnGainedArmorEventArgs args)
            {
                var card = GameServer.State.GetCard(_cardUid);

                if (card == null)
                {
                    SetAsNa();
                    return;
                }

                if (card.Team == args.team)
                {
                    card.limitBreakValue += args.armorGained;
                    UpdateLimitBreak();
                }
            }
        }

        private void OnCharacterDamaged(IMessage message)
        {
            if (message.Data is OnCharacterDamagedEventArgs args)
            {
                var card = GameServer.State.GetCard(_cardUid);
                var damageCard = GameServer.State.GetCard(args.damageSourceUid);
                var targetCard = GameServer.State.GetCard(args.characterUid);

                switch (card.data.limitBreak.type)
                {
                    case LimitBreakType.DAMAGE_WITH_SPELLS:
                        if (damageCard?.data != null && damageCard.data.type == CardType.Spell && card.owner == damageCard.owner)
                        {
                            card.limitBreakValue += args.damageAmount;
                            UpdateLimitBreak();
                        }
                        break;
                    case LimitBreakType.FRIENDLY_MINIONS_DAMAGED_BY_YOU:
                        if (damageCard?.data != null && damageCard.owner == card.owner && targetCard.characterData.type == CardType.Minion)
                        {
                            card.limitBreakValue += args.damageAmount;
                            UpdateLimitBreak();
                        }
                        break;
                }
            }
        }

        private void OnCardPlayed(IMessage message)
        {
            var card = GameServer.State.GetCard(_cardUid);

            if (card == null)
            {
                SetAsNa();
                return;
            }

            if (message.Data is OnCardPlayedEventArgs args)
            {
                var playedCard = GameServer.State.GetCard(args.cardUid);
                if (playedCard == null) return;

                if (card.data.limitBreak.type == LimitBreakType.PLAY_CARD_BY_NAME)
                {
                    if ((args.isClientCard && card.Team == Team.Client) || (!args.isClientCard && card.Team == Team.Opponent))
                    {
                        if (playedCard.data.id == card.data.limitBreak.cardId)
                        {
                            card.limitBreakValue++;
                            UpdateLimitBreak();
                        }
                    }
                }
                else if (card.data.limitBreak.type == LimitBreakType.PLAY_CARD_FROM_CLASS)
                {
                    if ((args.isClientCard && card.Team == Team.Client) || (!args.isClientCard && card.Team == Team.Opponent))
                    {
                        if (playedCard.data.@class == card.data.limitBreak.@class)
                        {
                            card.limitBreakValue++;
                            UpdateLimitBreak();
                        }
                    }
                }
                else if (card.data.limitBreak.type == LimitBreakType.PLAY_CARD_FROM_ANOTHER_FACTION)
                {
                    if (playedCard.Team == card.Team && playedCard.data.faction != card.data.faction && playedCard.data.faction != Faction.Neutral)
                    {
                        card.limitBreakValue++;
                        UpdateLimitBreak();
                    }
                }
            }
        }

        private void OnCardDiscarded(IMessage message)
        {
            var card = GameServer.State.GetCard(_cardUid);

            if (card == null)
            {
                SetAsNa();
                return;
            }

            if (message.Data is OnCardDiscardedEventArgs args)
            {
                if (args.team == card.Team)
                {
                    card.limitBreakValue += args.discardCount;
                    UpdateLimitBreak();
                }
            }
        }

        private void OnCardAddedToDeck(IMessage message)
        {
            var card = GameServer.State.GetCard(_cardUid);

            if (card == null)
            {
                SetAsNa();
                return;
            }

            if (message.Data is OnCardAddedToDeckEventArgs args)
            {
                var addedCard = GameServer.State.GetCard(args.cardUid);
                if (addedCard == null) return;

                if (card.data.limitBreak.type == LimitBreakType.ADD_CARD_BY_NAME_TO_OPPONENTS_DECK &&
                    card.Team == args.addedByTeam &&
                    args.teamOfDeck != args.addedByTeam)
                {
                    if (addedCard.data.id == card.data.limitBreak.cardId)
                    {
                        card.limitBreakValue++;
                        UpdateLimitBreak();
                    }
                }
            }
        }

        private void OnMinionFrozen(IMessage message)
        {
            var card = GameServer.State.GetCard(_cardUid);

            if (card == null)
            {
                SetAsNa();
                return;
            }

            if (message.Data is OnMinionFrozenEventArgs args)
            {
                var playedCard = GameServer.State.GetCard(args.cardUid);

                if (playedCard == null)
                {
                    return;
                }

                if (card.data.limitBreak.type == LimitBreakType.MINIONS_ARE_FROZEN &&
                    playedCard.HasStatusEffect(StatusEffect.Frozen))
                {
                    card.limitBreakValue++;
                    UpdateLimitBreak();
                }
            }
        }

        public IEnumerator<float> BlitzBurstSpawn()
        {

            blitzBurstReadyLabelCanvas.alpha = 0;
            blitzBurstReady.SetActive(true);
            blitzBurstReadyAnim.Play("spawn");
            AudioController.PlaySound("battle_bliztburst", "BARDSFX");

            _readyText = "READY";

            blitzBurstReadyLabel.text = "<color=#E16647>" + _readyText + "</color>";

            while (blitzBurstReadyAnim.CurrentFrame <= 11)
            {
                yield return Timing.WaitForOneFrame;
                //blitzBurstReadyLabel.rectTransform.DOPunchPosition(Vector3.up, 0.5f);
            }

            while (blitzBurstReadyAnim.CurrentFrame <= 12)
            {
                blitzBurstReadyLabelCanvas.DOFade(1, 0.16f);
               // blitzBurstReadyLabel.rectTransform.DOPunchPosition(Vector3.up, 0.16f);
                yield return Timing.WaitForOneFrame;
            }

            while (blitzBurstReadyAnim.CurrentFrame <= 15)
            {
                blitzBurstReadyLabel.text = "<color=#F9A653>" + _readyText + "</color>";
                yield return Timing.WaitForOneFrame;
            }

            while (blitzBurstReadyAnim.CurrentFrame <= 16)
            {
                blitzBurstReadyLabel.text = "<color=#E6ECB5>" + _readyText + " </color>";
                yield return Timing.WaitForOneFrame;
            }

            while (blitzBurstReadyAnim.CurrentFrame <= 22)
            {
                blitzBurstReadyLabel.text = "<color=#F9A653>" + _readyText + " </color>";
                yield return Timing.WaitForOneFrame;
            }

            while (blitzBurstReadyAnim.CurrentFrame <= 23)
            {
                blitzBurstReadyLabelCanvas.DOFade(0, 0.08f);
                yield return Timing.WaitForOneFrame;
            }

            while (!blitzBurstReadyAnim.IsDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            blitzBurstReady.SetActive(false);
            _testBurst = true;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (string.IsNullOrEmpty(_cardUid))
            {
                return;
            }

            cardView.Show(_cardUid);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            cardView.Hide();
        }

        private float _timePointerDown;

        public void OnPointerClick(PointerEventData eventData)
        {
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _timePointerDown = Time.time;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            var timeDiff = Time.time - _timePointerDown;

            if (timeDiff > 0.3f || string.IsNullOrEmpty(_cardUid) || !GameServer.State.IsClientsTurn()) return;

            var heroPower = GameServer.State.GetBlitzBurst(Team.Client);

            // TODO: IMPLEMENT DRAGGING!!

            if (heroPower != null && heroPower.limitBreakValue >= heroPower.data.limitBreak.value)
            {
                cardView.Hide();
                heroPower.limitBreakValue = 0;
                UpdateLimitBreak();

                var command = new PlayCommand
                {
                    PlayerUid = GameServer.ClientPlayerState.Uid,
                    SourceUid = _cardUid
                };

                // var shouldSelectTarget = card.State.ShouldSelectTarget();
                //
                // if (shouldSelectTarget)
                // {
                //     if (_cardView == null) yield break;
                //     var gameCardView = _cardView.GetCardComponent<GameCardView>();
                //     if (gameCardView  == null) yield break;
                //     var gameCardState = gameCardView.State;
                //     if (gameCardState == null) yield break;
                //
                //     command.Selection = gameCardState;
                // }

                //Play Sound
                PlayBlitzBurstSound();

                var card = GameServer.State.GetCard(_cardUid);

                GameLogic.StartCommand(command);

                Events.Publish(this, EventType.OnCardPlayed, new OnCardPlayedEventArgs{ isClientCard = card.Team == Team.Client,cardUid = card.uid });
            }
        }

        private void PlayBlitzBurstSound()
        {
            //var heroUid = GameServer.ClientPlayerState.heroUid;
            //var hero = GameServer.State.GetCard(heroUid);
            //var jingle = "jingle-blitzburst-" + hero.characterData.name.ToLower();

            AudioController.PlaySound("battle_bliztburst", "BARDSFX", true, gameObject);

        }

    }
}