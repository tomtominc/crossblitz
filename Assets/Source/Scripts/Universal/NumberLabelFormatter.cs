using System;
using CrossBlitz.Audio;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace CrossBlitz.Universal
{
    public class NumberLabelFormatter : MonoBehaviour
    {

        public TextMeshProUGUI label;

        public bool formatWithCommas;
        public string prefix;

        public bool smoothlyIncrease;
        [ShowIf("smoothlyIncrease"),Indent]
        public float smoothTime=0.5f;

        public bool capsAtMaxValue;
        [ShowIf("capsAtMaxValue"), Indent]
        public int maxValue = 999;
        [ShowIf("capsAtMaxValue"), Indent]
        public bool usesObjectForPlusSymbol;
        [ShowIf("@this.capsAtMaxValue && usesObjectForPlusSymbol"), Indent]
        public GameObject plusSymbol;

        private float _currentAmount;
        private float _targetAmount;
        private float _currentSpeed;
        public bool soundOn = false;

        public bool IsFinishedAnimating => Mathf.Abs(_currentAmount - _targetAmount) <= float.Epsilon;

        public void RefreshLabel()
        {
            SetLabel();
        }

        private void Update()
        {
            if (Math.Abs(_currentAmount - _targetAmount) > float.Epsilon)
            {
                _currentAmount = Mathf.MoveTowards(_currentAmount, _targetAmount, _currentSpeed * Time.deltaTime);

                SetLabel();

                if(soundOn) AudioController.PlaySound("Text-Letter", "SFX", true).setPitch(2.5f);
            }
        }

        public void UpdateLabel(int amount)
        {
            if (Math.Abs(_targetAmount - amount) < float.Epsilon && smoothlyIncrease) return;

            _currentAmount = _targetAmount;
            _targetAmount = amount;

            if (capsAtMaxValue && _targetAmount >= maxValue)
            {
                _targetAmount = maxValue;
            }

            _currentSpeed = Mathf.Abs(_currentAmount - _targetAmount) / smoothTime;

            if (!smoothlyIncrease)
            {
                _currentAmount = _targetAmount;
                SetLabel();
            }

            //todo: figure out why meld button is decreasing at a weird rate.
            // if (_currentAmount < _targetAmount)
            // {
            //     _currentAmount = _targetAmount;
            // }
            //
            // if (_currentAmount == _targetAmount)
            // {
            //     SetLabel();
            // }
        }

        private void SetLabel()
        {
            var plus = string.Empty;

            if (capsAtMaxValue && _currentAmount >= maxValue)
            {
                _currentAmount = maxValue;

                if (usesObjectForPlusSymbol && plusSymbol)
                {
                    plusSymbol.SetActive(true);
                }
                else
                {
                    plus = "+";
                }
            }
            else if (capsAtMaxValue)
            {
                if (plusSymbol) plusSymbol.SetActive(false);
            }

            var intAmount = (int) _currentAmount;
            label.text = $"{prefix}{intAmount.ToString(formatWithCommas? "N0" : string.Empty)}{plus}";
        }
    }
}