using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.PlayFab.Authentication;
using GameDataEditor;
using Sirenix.OdinInspector;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Universal
{
    [RequireComponent(typeof(NumberLabelFormatter))]
    public class IngredientDisplay : MonoBehaviour
    {
        public string itemName;
        public TextMeshProUGUI itemNameLabel;
        public SpriteAnimation itemHolder;
        public SpriteAnimation itemIcon;
        public NumberLabelFormatter labelFormatter;
        public IngredientTooltip tooltip;

        [FoldoutGroup("Overlay Settings")]
        public bool usesOverlay;
        [FoldoutGroup("Overlay Settings"), ShowIf("usesOverlay")]
        public CanvasGroup overlay;
        [FoldoutGroup("Overlay Settings"), ShowIf("usesOverlay")]
        public Image overlayImg;
        [FoldoutGroup("Overlay Settings"), ShowIf("usesOverlay")]
        public bool darkensAtNoItemsInInventory;
        [FoldoutGroup("Overlay Settings"), ShowIf("@this.usesOverlay && darkensAtNoItemsInInventory"), Indent]
        public Color darkenColor = "#1A0B12".ToColor();
        [FoldoutGroup("Overlay Settings"), ShowIf("usesOverlay")]
        public bool blinks;
        [FoldoutGroup("Overlay Settings"), ShowIf("@this.usesOverlay && darkensAtNoItemsInInventory"), Indent]
        public float blinkSpeed = 10;
        [FoldoutGroup("Overlay Settings"), ShowIf("@this.usesOverlay && darkensAtNoItemsInInventory"), Indent]
        public Color blinkColor = "#E6DCD2".ToColor();

        private bool _usesCustomNumber;
        private int _customNumber;
        private bool _blink;

        public bool UsesCustomNumber => _usesCustomNumber;
        public int CustomNumber => _customNumber;

        private void Start()
        {
            blinkSpeed = 0.5f;
            SetItemId(itemName);
            Events.Subscribe(EventType.OnModifiedItemUses, OnModifiedItemUses);
        }

        private void Update()
        {
            if (_blink)
            {
                overlayImg.color = blinkColor;
                overlay.alpha = Mathf.PingPong(Time.time * blinkSpeed, 0.5f);
            }
        }

        public void SetItemId(string itemId, bool usesCustomNumber = false, int customNumber = 0, bool isManaShard = false)
        {
            if (_usesCustomNumber && !usesCustomNumber) return;

            if (!isManaShard)
            {
                itemName = itemId;
                var item = Db.ItemDatabase.GetItem(itemName);

                _usesCustomNumber = usesCustomNumber;
                _customNumber = customNumber;

                if (item != null)
                {
                    if (itemIcon) itemIcon.Play(item.ItemImageUrl);
                    if (itemNameLabel) itemNameLabel.text = item.DisplayName;
                    if (tooltip) tooltip.SetIngredient(new GDEItemData(itemName));
                    if (itemHolder) itemHolder.Play(Crafting.GetItemFaction(itemName).ToString().ToLower());

                    if (_usesCustomNumber)
                        UpdateLabel(_customNumber);
                    else
                        UpdateLabel(App.Inventory.GetItemAmount(itemName));
                }
            }
            else
            {
                if (itemIcon) itemIcon.Play("mana-shard");
                if (itemNameLabel) itemNameLabel.text = "MANA SHARDS";
                // What is the GDEItemDatabase id for manashards...Does it even have one? Is it even in that database? if not , need to search a different one
                if (tooltip)
                {
                    //tooltip.SetIngredient(new GDEItemData("ManaShards"));

                    tooltip.headerBanner.Play("chaos");
                    tooltip.headerTitle.text = "MANA SHARDS";
                    tooltip.factionTitle.text = $"TYPE: CORE";
                    tooltip.description.text = "These brilliant rock-bits are the remnants of crystalized mana sap!";
                }
                if (itemHolder) itemHolder.Play("mana-shard");
                UpdateLabel(App.Inventory.GetCurrencyAmount(Currency.MANA_SHARDS));
            }

        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnModifiedItemUses, OnModifiedItemUses);
        }

        private void UpdateLabel(int usesLeft)
        {
            labelFormatter.UpdateLabel(usesLeft);

            if (usesOverlay && overlay && darkensAtNoItemsInInventory && usesLeft <= 0)
            {
                overlay.SetActive(true);
                overlay.alpha = 0.5f;
                overlayImg.color = darkenColor;
            }
            else if (usesOverlay && overlay && !_blink)
            {
                overlay.SetActive(false);
            }
        }

        public void Blink(bool e)
        {
            _blink = e;
            overlay.SetActive(_blink);
        }

        private void OnModifiedItemUses(IMessage message)
        {
            if (_usesCustomNumber) return;

            if (message.Data is OnModifiedItemUsesEventArgs args)
            {
                if (args.ItemNames.Contains( itemName ))
                {
                    UpdateLabel(App.Inventory.GetItemAmount(itemName));
                }
            }
        }
    }
}