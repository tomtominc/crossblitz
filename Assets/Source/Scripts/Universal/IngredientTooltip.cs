using GameDataEditor;
using TMPro;
using UnityEngine;

namespace CrossBlitz.Universal
{
    public class IngredientTooltip : MonoBehaviour
    {
        public SpriteAnimation headerBanner;
        public TextMeshProUGUI headerTitle;
        public TextMeshProUGUI factionTitle;
        public TextMeshProUGUI description;

        public void SetIngredient(GDEItemData ingredient)
        {
            headerBanner.Play(ingredient.Faction.ToLower());
            headerTitle.text = ingredient.Name;
            factionTitle.text = $"TYPE: {ingredient.Faction}";
            description.text = ingredient.Description;
        }
    }
}