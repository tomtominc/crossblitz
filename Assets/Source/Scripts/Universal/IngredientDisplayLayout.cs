using System.Collections.Generic;
using CrossBlitz.Fables.Battle;
using UnityEngine;

namespace CrossBlitz.Universal
{
    public class IngredientDisplayLayout : MonoBehaviour
    {
        public RectTransform layout;
        public IngredientDisplay ingredientDisplayPrefab;
        [HideInInspector]
        public List<IngredientDisplay> ingredientDisplays;

        public void Clear()
        {
            layout.DestroyChildren();
        }

        public void DisplayIngredients(List<string> ingredients)
        {
            ingredientDisplays = new List<IngredientDisplay>();

            for (var i = 0; i < ingredients.Count; i++)
            {
                var display = Instantiate(ingredientDisplayPrefab, layout);
                display.SetItemId(ingredients[i]);
                ingredientDisplays.Add(display);
            }
        }

        public void DisplayIngredients(List<ItemValue> ingredients)
        {
            ingredientDisplays = new List<IngredientDisplay>();

            for (var i = 0; i < ingredients.Count; i++)
            {
                var display = Instantiate(ingredientDisplayPrefab, layout);
                display.SetItemId(ingredients[i].ItemUid, true, ingredients[i].Count);
                ingredientDisplays.Add(display);
            }
        }
    }
}