using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.PlayFab.Authentication;
using Sirenix.OdinInspector;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Universal
{
    [RequireComponent(typeof(NumberLabelFormatter))]
    public class CurrencyDisplay : MonoBehaviour
    {
        public string currencyCode;
        public SpriteAnimation currencyIcon;
        public TextMeshProUGUI nameLabel;
        public NumberLabelFormatter labelFormatter;

        [FoldoutGroup("Overlay Settings")]
        public bool usesOverlay;
        [FoldoutGroup("Overlay Settings"), ShowIf("usesOverlay")]
        public CanvasGroup overlay;
        [FoldoutGroup("Overlay Settings"), ShowIf("usesOverlay")]
        public Image overlayImg;
        [FoldoutGroup("Overlay Settings"), ShowIf("usesOverlay")]
        public bool darkensAtNoItemsInInventory;
        [FoldoutGroup("Overlay Settings"), ShowIf("@this.usesOverlay && darkensAtNoItemsInInventory"), Indent]
        public Color darkenColor = "#1A0B12".ToColor();
        [FoldoutGroup("Overlay Settings"), ShowIf("usesOverlay")]
        public bool blinks;
        [FoldoutGroup("Overlay Settings"), ShowIf("@this.usesOverlay && darkensAtNoItemsInInventory"), Indent]
        public float blinkSpeed = 10;
        [FoldoutGroup("Overlay Settings"), ShowIf("@this.usesOverlay && darkensAtNoItemsInInventory"), Indent]
        public Color blinkColor = "#E6DCD2".ToColor();

        private bool _usesCustomNumber;
        private int _customNumber;
        private bool _blink;

        public bool UsesCustomNumber => _usesCustomNumber;
        public int CustomNumber => _customNumber;

        public void Start()
        {
            SetCurrencyCode(currencyCode);
            Events.Subscribe(EventType.OnModifiedCurrency, OnModifyCurrency);

        }

        private void Update()
        {
            if (_blink)
            {
                overlayImg.color = blinkColor;
                overlay.alpha = Mathf.PingPong(Time.time * blinkSpeed, 0.5f);
            }
        }

        public void SetCurrencyCode(string code, bool usesCustomNumber = false, int customNumber = 0)
        {
            if (_usesCustomNumber && !usesCustomNumber) return;

            currencyCode = code;

            _usesCustomNumber = usesCustomNumber;
            _customNumber = customNumber;

            if (currencyIcon != null) currencyIcon.Play(Currency.GetIconName[currencyCode]);
            if (nameLabel != null) nameLabel.text = Currency.GetReadableName[currencyCode];
            UpdateLabel(_usesCustomNumber ? _customNumber : App.Inventory.GetCurrencyAmount(currencyCode));
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnModifiedCurrency, OnModifyCurrency);
        }

        private void UpdateLabel(int amount)
        {
            labelFormatter.UpdateLabel(amount);

            if (usesOverlay && overlay && darkensAtNoItemsInInventory && amount <= 0)
            {
                overlay.alpha = 0.25f;
                overlayImg.color = darkenColor;
            }
        }

        public void Blink(bool e)
        {
            _blink = e;
        }

        private void OnModifyCurrency(IMessage message)
        {
            if (_usesCustomNumber) return;

            if (message.Data is OnModifiedCurrencyEventArgs args)
            {
                if (args.CurrencyCode == currencyCode) UpdateLabel(App.Inventory.GetCurrencyAmount(currencyCode));
            }
        }
    }
}