using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.PlayFab.Authentication;
using GameDataEditor;
using Sirenix.OdinInspector;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Universal
{
    public class IngredientDisplayManaRecipe : MonoBehaviour
    {
        public string itemName;
        public TextMeshProUGUI ingredientCostLabel;
        public TextMeshProUGUI ingredientCostLabelShadow;
        public SpriteAnimation itemHolder;
        public SpriteAnimation itemIcon;
        public IngredientTooltip tooltip;

        private void Start()
        {
            //SetItemId(itemName);
            Events.Subscribe(EventType.OnModifiedItemUses, OnModifiedItemUses);
        }

        public void SetItemId(string itemId, int requiredAmt, bool isManaShard = false)
        {
            if (!isManaShard)
            {
                itemName = itemId;
                var item = Db.ItemDatabase.GetItem(itemName);

                if (item != null)
                {

                    if (itemIcon) itemIcon.Play(item.ItemImageUrl);
                    if (tooltip) tooltip.SetIngredient(new GDEItemData(itemName));
                    if (itemHolder) itemHolder.Play(Crafting.GetItemFaction(itemName).ToString().ToLower());

                    UpdateLabel(App.Inventory.GetItemAmount(itemName), requiredAmt);
                }
            }
            else
            {
                
                if (itemIcon) itemIcon.Play("mana-shard");
                if (itemHolder) itemHolder.Play("mana-shard");
                UpdateLabel(App.Inventory.GetCurrencyAmount(Currency.MANA_SHARDS), requiredAmt);
            }

        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnModifiedItemUses, OnModifiedItemUses);
        }

        private void UpdateLabel(int usesLeft, int usesRequired = 0)
        {
            var prefix = "<color=#E42F2F>";
            if (usesLeft >= usesRequired) prefix = "<color=#FFB2E4>";

            var shadowLabel = usesLeft.ToString() + "/" + usesRequired.ToString();
            var colorLabel = prefix + usesLeft.ToString() + "</color>" + "/" + usesRequired.ToString();
            ingredientCostLabel.text = colorLabel;
            ingredientCostLabelShadow.text = shadowLabel;
        }

        private void OnModifiedItemUses(IMessage message)
        {
            if (message.Data is OnModifiedItemUsesEventArgs args)
            {
                if (args.ItemNames.Contains( itemName ))
                {
                    UpdateLabel(App.Inventory.GetItemAmount(itemName));
                }
            }
        }
    }
}