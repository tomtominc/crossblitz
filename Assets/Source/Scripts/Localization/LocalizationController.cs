﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public static class LocalizationController
{
    public static Dictionary<string, string> LocalizedText;

    public static void Initialize()
    {
        // download text stuff
        LocalizedText = new Dictionary<string, string>();
    }

    public static void Localize(Transform transform)
    {
        // if (LocalizedText == null)
        // {
        //     Initialize();
        // }
        //
        // if (transform.childCount <= 0)
        // {
        //     TextMeshProUGUI text = transform.GetComponent<TextMeshProUGUI>();
        //
        //     if (text != null)
        //     {
        //         Localize(text);
        //     }
        //
        //     return;
        // }
        //
        // for (int i = 0; i < transform.childCount; i++)
        // {
        //     TextMeshProUGUI text = transform.GetComponent<TextMeshProUGUI>();
        //
        //     if (text != null)
        //     {
        //         Localize(text);
        //     }
        //
        //     Localize(transform.GetChild(i));
        // }
    }


    public static void Localize(TextMeshProUGUI text)
    {
        text.text = Localize(text.text);
    }

    public static string Localize(string key, string replacement = default)
    {
        if (LocalizedText == null)
        {
            Initialize();
        }

        if (LocalizedText.ContainsKey(key))
        {
            return LocalizedText[key];
        }

        return string.IsNullOrEmpty(replacement) ? key : replacement;
    }
}

public static class LocalizationKeys
{
    public const string EMAIL_BLANK_ERROR = "EMAIL_BLANK_ERROR";
    public const string PASSWORD_BLANK_ERROR = "PASSWORD_BLANK_ERROR";
    public const string PASSWORD_NOT_LONG_ENOUGH_ERROR = "PASSWORD_NOT_LONG_ENOUGH_ERROR";
    public const string EMAIL_FORMAT_ERROR = "EMAIL_FORMAT_ERROR";
    public const string USERNAME_BLANK_ERROR = "USERNAME_BLANK_ERROR";
    public const string PASSWORDS_DONT_MATCH_ERROR = "PASSWORDS_DONT_MATCH_ERROR";
    public const string USERNAME_TOOLTIP = "USERNAME_TOOLTIP";
    public const string PASSWORD_TOOLTIP = "PASSWORD_TOOLTIP";
    public const string ACCOUNT_NOT_FOUND = "ACCOUNT_NOT_FOUND";
}