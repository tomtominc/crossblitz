using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CrossBlitz.RewardScreenAPI
{
    public class PlatformGlowEffect : MonoBehaviour,
        IPointerEnterHandler,
        IPointerExitHandler,
        IDropHandler
    {
        public CanvasGroup canvasGroup;
        public RectTransform platformOutlineGlow;
        public List<ParticleSystem> platformGlowParticles;
        private float _alpha;
        private float _targetAlpha;
        private float _changeSpeed;
        private Tweener _glowFlicker;
        private float _flickerHeight;
        private void Start()
        {
            _targetAlpha = 0;
            _alpha = 0;
            _changeSpeed = 4;
            _flickerHeight = 0;
        }

        private void Update()
        {
            if (Math.Abs(_alpha - _targetAlpha) > float.Epsilon)
            {
                _alpha = Mathf.MoveTowards(_alpha, _targetAlpha, _changeSpeed * Time.deltaTime);

                canvasGroup.alpha = _alpha;

                for (var i = 0; i < platformGlowParticles.Count; i++)
                {
                    var main = platformGlowParticles[i].main;
                    main.startColor = new Color(1, 1, 1, _alpha);
                }
            }

            _flickerHeight = Mathf.PingPong(Time.time * 32f, 2);
            platformOutlineGlow.sizeDelta = new Vector2(280f,120f + _flickerHeight);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (RewardChestDragIcon.Current)
            {
                _targetAlpha = 1;
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _targetAlpha = 0;
        }

        public void OnDrop(PointerEventData eventData)
        {
            if (RewardChestDragIcon.Current)
            {
                RewardScreen.Instance.SummonChestBegin(RewardChestDragIcon.Current.ChestItem);
                RewardChestDragIcon.Current.Summon();
            }
        }
    }
}