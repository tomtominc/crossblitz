using System.Collections;
using CrossBlitz.Cursors;
using CrossBlitz.Items.Data;
using CrossBlitz.Utils;
using DG.Tweening;
using PlayFab.ClientModels;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CrossBlitz.RewardScreenAPI
{
    public class RewardChestDragIcon : MonoBehaviour,
        IPointerEnterHandler,
        IPointerExitHandler,
        IPointerDownHandler,
        IBeginDragHandler,
        IDragHandler,
        IEndDragHandler,
        ICursorSelectable
    {
        public static RewardChestDragIcon Current;
        public RectTransform chestObject;
        public SpriteAnimation chestAnimator;
        public TextMeshProUGUI chestCount;
        public SpriteAnimation chestDragAnimator;
        public RectTransform chestDragRect;
        public CanvasGroup chestDragCanvas;

        private bool _dragging;
        private bool _summoning;
        private ItemDataInstance _chestItem;
        public ItemDataInstance ChestItem => _chestItem;
        public void SetChestItem(ItemDataInstance chestItem)
        {
            _chestItem = chestItem;
            var chestName = "classic"; //_chestItem.ItemId.Replace("chest", "").ToLower();
            chestAnimator.Play(chestName);
            chestCount.text = $"x{_chestItem.RemainingUses}";
            chestDragAnimator.Play(chestName);
            chestDragAnimator.SetActive(false);
        }

        public IEnumerator AnimateIn()
        {
            chestCount.SetActive(false);
            chestObject.anchoredPosition = new Vector2(0, 68f);
            yield return  chestObject.DOAnchorPosY(0, 0.25f)
                .WaitForCompletion();
            chestObject.DOPunchScale(new Vector3(1, -1) * 0.32f, 1f);
            yield return new WaitForSeconds(0.25f);
            var anchoredPosition = chestCount.rectTransform.anchoredPosition;
            var originalChestCountPos = anchoredPosition;
            anchoredPosition -= Vector2.right * 20f;
            chestCount.rectTransform.anchoredPosition = anchoredPosition;
            chestCount.rectTransform.DOAnchorPos(originalChestCountPos, 0.125f).SetEase(Ease.OutBack);
            chestCount.SetActive(true);
            chestCount.text = $"x0";
            var currCount = 0;
            var target = _chestItem.RemainingUses;
            DOTween.To(() => currCount, count=> currCount = count, target, target)
                .SetSpeedBased(true).SetEase(Ease.Linear)
                .OnUpdate(() =>
                {
                    chestCount.text = $"x{currCount}";
                });
        }

        private void Update()
        {
            if (chestDragRect.IsActive())
            {
                if (_dragging && Current == this)
                {
                    chestDragRect.position =
                        (Vector2) Camera.main.ScreenToWorldPoint(Input.mousePosition) + new Vector2(0.1f, -0.1f);
                }
                else if (!_summoning && chestDragCanvas.alpha > 0)
                {
                    chestDragRect.anchoredPosition = Vector2.MoveTowards(
                        chestDragRect.anchoredPosition, Vector2.zero, 200 * Time.deltaTime);
                }
                else if (chestDragCanvas.alpha <= 0)
                {
                    chestDragRect.anchoredPosition = Vector2.zero;
                    chestDragCanvas.SetActive(false);
                    chestDragCanvas.alpha = 1;
                    _summoning = false;
                }
            }

            if (_dragging)
            {
                chestCount.text = $"x{_chestItem.RemainingUses-1}";
            }
        }

        public void Summon()
        {
            Current = null;
            _summoning = true;
            _chestItem.RemainingUses -= 1;
            chestDragCanvas.DOFade(0, 0.25f);
        }


        public void OnPointerEnter(PointerEventData eventData)
        {

        }

        public void OnPointerExit(PointerEventData eventData)
        {

        }

        public void OnPointerDown(PointerEventData eventData)
        {

        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (Current != null) return;

            Current = this;
            _dragging = true;
            chestDragAnimator.SetActive(true);
            chestDragCanvas.alpha = 1;
        }

        public void OnDrag(PointerEventData eventData)
        {
            _dragging = true;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            Current = null;
            _dragging = false;
            chestDragCanvas.DOFade(0, 0.25f);
        }

        public bool Interactable => false;
        public bool Grabbable => true;
        public bool InfoOnly => false;
        public bool Loading => false;
    }
}