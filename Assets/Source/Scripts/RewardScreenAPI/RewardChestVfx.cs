using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.Items.Data;
using CrossBlitz.ServerAPI.Models;
using DG.Tweening;
using MEC;
using PlayFab.ClientModels;
using UnityEngine;

namespace CrossBlitz.RewardScreenAPI
{
    public class RewardChestVfx : MonoBehaviour
    {
        public RectTransform chestRect;
        public SpriteAnimation chestAnimator;
        public SpriteAnimation chestShadow;
        public SpriteAnimation landFx;
        public SpriteAnimation openFx;
        public SpriteAnimation lightBeamFx;
        public ParticleSystem lightBeamParticles;
        public RectTransform parentPlatformGroup;

        public IEnumerator<float> SpawnAndOpen(ItemDataInstance chest, UnlockContainerResult result, GameObject cardObject)
        {
            chestRect.anchoredPosition= new Vector2(0,
                (RewardScreen.Instance.RectTransform().rect.height * 0.5f) +
                (chestRect.sizeDelta.y * 0.5f));

            lightBeamFx.Play("spawn");
            var lightBeamMain = lightBeamParticles.main;

            var lightBeamAlpha = 0;
            lightBeamMain.startColor = new Color(1,1,1,lightBeamAlpha );
            DOTween.To(() => lightBeamAlpha, alpha => lightBeamAlpha = alpha, 1,
                lightBeamFx.GetCurrentAnimationLength())
                .OnUpdate(() =>
                {
                    lightBeamMain.startColor = new Color(1,1,1,lightBeamAlpha);
                });

            while (!lightBeamFx.IsDone) yield return Timing.WaitForOneFrame;

            lightBeamFx.Play("idle");
            chestShadow.Play("land");

            var landingFrame = 4;
            var animTime = landingFrame * 0.04f;
            var chestName = "classic";//chest.ItemId.Replace("chest","").ToLower();

            chestRect.SetActive(true);
            chestAnimator.Play($"idle-{chestName}");

            yield return Timing.WaitUntilDone( chestRect.DOAnchorPosY(0, animTime)
                .WaitForCompletion(true));

            chestAnimator.Play($"land-{chestName}");
            landFx.Play("land");
            parentPlatformGroup.DOPunchAnchorPos(Vector2.down * 10f, 0.5f);

            yield return Timing.WaitForSeconds(0.4f);

            lightBeamFx.Play("despawn");
            DOTween.To(() => lightBeamAlpha, alpha => lightBeamAlpha = alpha, 0,
                    lightBeamFx.GetCurrentAnimationLength())
                .OnUpdate(() =>
                {
                    lightBeamMain.startColor = new Color(1,1,1,lightBeamAlpha);
                });

            while (!lightBeamFx.IsDone) yield return Timing.WaitForOneFrame;

            var shakeCount = 12;

            var pos = this.RectTransform().anchoredPosition;

            for (var i = 0; i < shakeCount; i++)
            {
                this.RectTransform().anchoredPosition =pos+ new Vector2(4 * (i%2==0?1:-1), 1 * (i%2==0?-1:1));
                yield return Timing.WaitForSeconds(0.06f);
            }

            this.RectTransform().anchoredPosition = pos;
            chestAnimator.Play($"open-{chestName}");

            while (!chestAnimator.IsDone) yield return Timing.WaitForOneFrame;

            for (var i = 0; i < result.GrantedItems.Count; i++)
            {
                var cardData = Db.CardDatabase.GetCard(result.GrantedItems[i].ItemId);
                chestAnimator.Play($"spawn-item-{chestName}");
                openFx.Play("open");
                openFx.image.color = "#FBF8DA".ToColor();
                //todo: add rarity colors.
                //openFx.image.color = ColorPalette.CrossBlitz.GetRarityColor(cardData.rarity);

                RewardScreen.Instance.FlashScreen(0.8f);
                parentPlatformGroup.DOPunchAnchorPos(Vector2.down * 10f, 0.5f);

                yield return Timing. WaitForSeconds(0.6f);
            }

            for (var i = 0; i < result.GrantedVirtualCurrencies.Count; i++)
            {
                chestAnimator.Play($"spawn-item-{chestName}");
                openFx.Play("open");
                openFx.image.color = "#FBF8DA".ToColor();

                RewardScreen.Instance.FlashScreen(0.8f);
                parentPlatformGroup.DOPunchAnchorPos(Vector2.down * 10f, 0.5f);

                yield return Timing. WaitForSeconds(0.6f);
            }
        }
    }
}