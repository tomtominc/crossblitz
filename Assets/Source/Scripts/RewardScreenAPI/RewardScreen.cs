using System;
using System.Collections;
using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Items.Data;
using CrossBlitz.PlayFab;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI.Models;
using DG.Tweening;
using MEC;
using PlayFab;
using PlayFab.ClientModels;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.RewardScreenAPI
{
    public class RewardScreen : global::CrossBlitz.ViewAPI.View
    {
        public static RewardScreen Instance;
        public ShakeContainer shakeContainer;
        public RectTransform headerBanner;
        public SpriteAnimation chestIconAnimator;
        public TextMeshProUGUI chestNameLabel;
        public CanvasGroup instructions;

        public UIBackground backgroundScroll;
        public RectTransform chestPlatform;
        public List<ParticleSystem> dustParticles;

        public CanvasGroup flashCanvas;
        public RewardChestVfx rewardChest;

        public CanvasGroup footer;
        public RectTransform chestLayout;
        public RewardChestDragIcon rewardChestDragIconPrefab;
        public Button actionButton;
        public CanvasGroup actionButtonCanvas;
        public GameObject loadingIcon;

        public CanvasGroup singleCardsContainer;
        public RectTransform singleCardsLayout;
        public CanvasGroup singleCardsBackground;

        private bool _continueOpeningRewards;
        private UnlockContainerResult _unlockContainerResult;

        private void Awake()
        {
            Instance = this;
            actionButton.onClick.AddListener(OnContinue);
        }

        private void OnContinue()
        {
            _continueOpeningRewards = true;
        }

        public override void Open()
        {
            StartCoroutine(AnimateOpen());
        }

        private IEnumerator AnimateOpen()
        {
            backgroundScroll.m_speed = 1;

            var platformOriginPosition = chestPlatform.anchoredPosition;
            chestPlatform.anchoredPosition = platformOriginPosition + new Vector2(400,-200);

            var originalHeaderPosition = headerBanner.anchoredPosition;
            headerBanner.anchoredPosition = new Vector2(0, 67f);

            footer.alpha = 0;

            yield return new WaitForSeconds(1);

            const float moveToPlatformDuration = 1f;
            const float stopDelay =moveToPlatformDuration * 0.6f;
            const float timeToStop = moveToPlatformDuration - stopDelay;

            chestPlatform.DOAnchorPos(platformOriginPosition, moveToPlatformDuration);
                //.SetEase(Ease.OutBack, 1.1f);

            yield return new WaitForSeconds(stopDelay/2f);

            headerBanner.DOAnchorPos(originalHeaderPosition, moveToPlatformDuration)
                .SetEase(Ease.OutBack);

            footer.DOFade(1, 0.25f);

            var chests = App.Inventory.GetItemsByType("Chest");

            for (var i = 0; i < chests.Count; i++)
            {
                var dragIcon = Instantiate(rewardChestDragIconPrefab, chestLayout);
                dragIcon.SetChestItem(chests[i]);
                StartCoroutine(dragIcon.AnimateIn());
                yield return  new WaitForSeconds(0.1f);
            }

            yield return new WaitForSeconds(stopDelay/2f);

            AnimateDust(timeToStop);
            DOTween.To(() => backgroundScroll.m_speed, speed => backgroundScroll.m_speed = speed,
                0.32f,
                timeToStop);

            for (var i = 0; i < dustParticles.Count; i++)
            {
                var emissionModule = dustParticles[i].emission;
                emissionModule.rateOverTime = 3;
            }
        }

        private void AnimateDust(float duration)
        {
            var dust1 = dustParticles[0];

            var velocityOverLifeTime = dust1.velocityOverLifetime;

            DOTween.To(() => velocityOverLifeTime.x.constant,
                    x => velocityOverLifeTime.x = x, -1, duration)
                ;//.SetEase(Ease.OutBack);
            DOTween.To(() => velocityOverLifeTime.y.constant,
                    y => velocityOverLifeTime.y = y, 0.5f, duration)
                ;//.SetEase(Ease.OutBack);

            var dust2 = dustParticles[1];

            var velocityOverLifeTime2 = dust2.velocityOverLifetime;

            DOTween.To(() => velocityOverLifeTime2.x.constant,
                    x => velocityOverLifeTime2.x = x, -1, duration)
                ;//.SetEase(Ease.OutBack);
            DOTween.To(() => velocityOverLifeTime2.y.constant,
                    y => velocityOverLifeTime2.y = y, 0.5f, duration)
                ;//.SetEase(Ease.OutBack);
        }

        private bool _summoningChestBegan;
        private float _serverWaitTime;
        private ItemDataInstance _currentChestItem;

        private void Update()
        {
            if (_summoningChestBegan)
            {
                _serverWaitTime += Time.deltaTime;

                if (_serverWaitTime > 2 && !loadingIcon.activeSelf)
                {
                    loadingIcon.SetActive(true);
                }
            }
        }

        public void SummonChestBegin(ItemDataInstance chestItem)
        {
            loadingIcon.SetActive(true);

            _currentChestItem = chestItem;

            footer.interactable = false;
            footer.blocksRaycasts = false;

            var unlockContainerRequest = new UnlockContainerRequest
            {
                ItemInstanceId = chestItem.ItemInstanceId,
            };

            CloudScript.OnUnlockContainerRequestComplete += OnUnlockContainerComplete;
            CloudScript.OnUnlockContainerRequestFailedInServer += OnUnlockContainerFailedInServer;
            CloudScript.OnUnlockContainerRequestFailedInPlayFab += OnUnlockContainerFailedInPlayFab;
            CloudScript.UnlockContainerRequest(unlockContainerRequest);

            FlashScreen(0.5f);
        }

        private void OnUnlockContainerFailedInServer(ExecuteCloudScriptResult result)
        {
            CloudScript.OnUnlockContainerRequestComplete -= OnUnlockContainerComplete;
            CloudScript.OnUnlockContainerRequestFailedInServer -= OnUnlockContainerFailedInServer;
            CloudScript.OnUnlockContainerRequestFailedInPlayFab -= OnUnlockContainerFailedInPlayFab;
        }

        private void OnUnlockContainerFailedInPlayFab(PlayFabError error)
        {
            CloudScript.OnUnlockContainerRequestComplete -= OnUnlockContainerComplete;
            CloudScript.OnUnlockContainerRequestFailedInServer -= OnUnlockContainerFailedInServer;
            CloudScript.OnUnlockContainerRequestFailedInPlayFab -= OnUnlockContainerFailedInPlayFab;
        }

        private void OnUnlockContainerComplete(UnlockContainerResult result)
        {
            CloudScript.OnUnlockContainerRequestComplete -= OnUnlockContainerComplete;
            CloudScript.OnUnlockContainerRequestFailedInServer -= OnUnlockContainerFailedInServer;
            CloudScript.OnUnlockContainerRequestFailedInPlayFab -= OnUnlockContainerFailedInPlayFab;

            loadingIcon.SetActive(false);

            if (_currentChestItem.ItemInstanceId != result.UnlockedItemInstance)
            {
                Debug.LogError($"Wrong item was unlocked? {result.UnlockedItemInstance} vs {_currentChestItem.ItemInstanceId}");
            }

            _summoningChestBegan = false;
            _serverWaitTime = 0;
            _unlockContainerResult = result;

            for (var i = 0; i < _unlockContainerResult.GrantedItems.Count; i++)
            {
                Debug.Log($"YOU GOT A CARD! {_unlockContainerResult.GrantedItems[i].DisplayName}");
            }

            foreach (var currency in _unlockContainerResult.GrantedVirtualCurrencies)
            {
                Debug.Log($"YOU GOT A CURRENCY! {currency.Key}:{currency.Value}");
            }

            Timing.RunCoroutine(AnimateChestSequence());
        }

        [HideInInspector]
        public bool _continueChestSequence;

        private IEnumerator<float> AnimateChestSequence()
        {
            var cardAddress = AddressableReferenceLoader.GetAsset("Card_Standard", true);
            yield return Timing.WaitUntilDone( cardAddress.AsCoroutine() );

            var cardObjectList = new List<CardView>();
            var createSettings = new CreateCardFactorySettings
            {
                CardPrefab = cardAddress.Result,
                Layout = singleCardsLayout,
                SortingOrder = 400,
                CardObjectOffsetPosition = new Vector2(0, this.RectTransform().rect.height),
                StartsDisabled = false,
                AdditionalComponents = new List<Type>{ typeof(DragRotator) }
            };

            for (var i = 0; i < _unlockContainerResult.GrantedItems.Count; i++)
            {
                var cardData = Db.CardDatabase.GetCard(_unlockContainerResult.GrantedItems[i].ItemId); //new CardData {id = _unlockContainerResult.GrantedItems[i].ItemId };
                createSettings.CardData = cardData;
                CardFactory.Create(createSettings, cardObjectList);
            }

            if (_unlockContainerResult.GrantedVirtualCurrencies.ContainsKey(Currency.GOLD) &&
                _unlockContainerResult.GrantedVirtualCurrencies[Currency.GOLD] > 0)
            {
                var cardData = new CardData {id = "gold", type = CardType.Resource, name = $"{_unlockContainerResult.GrantedVirtualCurrencies[Currency.GOLD]:N0}"};
                createSettings.CardData = cardData;
                CardFactory.Create(createSettings, cardObjectList);
            }

            if (_unlockContainerResult.GrantedVirtualCurrencies.ContainsKey(Currency.DAWN_DOLLARS) &&
                _unlockContainerResult.GrantedVirtualCurrencies[Currency.DAWN_DOLLARS] > 0)
            {
                var cardData = new CardData {id = "dollars", type = CardType.Resource, name = $"{_unlockContainerResult.GrantedVirtualCurrencies[Currency.DAWN_DOLLARS]:N0}"};
                createSettings.CardData = cardData;
                CardFactory.Create(createSettings, cardObjectList);
            }

            if (_unlockContainerResult.GrantedVirtualCurrencies.ContainsKey(Currency.MANA_SHARDS) &&
                _unlockContainerResult.GrantedVirtualCurrencies[Currency.MANA_SHARDS] > 0)
            {
                var cardData = new CardData {id = "mana-shards", type = CardType.Resource, name = $"{_unlockContainerResult.GrantedVirtualCurrencies[Currency.MANA_SHARDS]:N0}"};
                createSettings.CardData = cardData;
                CardFactory.Create(createSettings, cardObjectList);
            }

            _continueChestSequence = false;
            yield return Timing.WaitUntilDone( rewardChest.SpawnAndOpen(_currentChestItem, _unlockContainerResult, cardAddress.Result));

            for (var i = 0; i < cardObjectList.Count; i++)
            {
                var cardView = cardObjectList[i];
                cardView.cardObject.RectTransform().DOAnchorPosY(0, 0.5f);
                yield return Timing.WaitForSeconds(0.25f);
            }

            singleCardsBackground.SetActive(true);
            singleCardsBackground.DOFade(1, 0.5f);

            // do cards show here.
            // keep list of all the new cards so we can show them at the end.

            _currentChestItem = null;
            _unlockContainerResult = null;
            _continueChestSequence = false;
            _continueOpeningRewards = false;

            actionButton.SetActive(true);
            actionButtonCanvas.DOFade(1, 0.25f);
            actionButtonCanvas.interactable = true;
            actionButtonCanvas.blocksRaycasts = true;

            while (!_continueOpeningRewards) yield return Timing.WaitForOneFrame;

            _continueOpeningRewards = false;

            singleCardsContainer.DOFade(0, 0.25f)
                .OnComplete(() =>
                {
                    singleCardsLayout.DestroyChildren();
                    singleCardsBackground.SetActive(false);
                    singleCardsBackground.alpha = 0;
                    singleCardsContainer.alpha = 1;
                });

            footer.interactable = true;
            footer.blocksRaycasts = true;

            actionButtonCanvas.DOFade(0, 0.25f);
            actionButtonCanvas.interactable = false;
            actionButtonCanvas.blocksRaycasts = false;

        }

        public void FlashScreen(float alpha)
        {
            flashCanvas.DOFade(alpha, 0.06f)
                .OnComplete(() =>
                {
                    flashCanvas.DOFade(0, 0.32f);
                });

            shakeContainer.Shake(16, 0.8f);
        }
    }
}