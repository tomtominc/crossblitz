using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.UI.Widgets
{
    public struct ToggleSelectValue
    {
        public bool isOn;
        public string content;
    }

    [RequireComponent(typeof(Toggle))]
    [ExecuteInEditMode]
    public class ToggleButton : MonoBehaviour
    {
        public string content;
        public GameObject activeGroup;
        public GameObject inactiveGroup;
        [InfoBox("Animator needs 4 animations.\nToggle-True\nToggle-False\nToggle-Change-True\nToggle-Change-False")]
        public Animator animator;

        public bool useGroupList;
        [ShowIf("useGroupList")]
        public List<GameObject> activeGroups;
        [ShowIf("useGroupList")]
        public List<GameObject> inactiveGroups;

        private Toggle _toggle;
        public event Action<bool,string> OnValueChanged;
        public event Action<ToggleButton, bool> OnValueChangedIncludeToggle;

        public Toggle Toggle
        {
            get
            {
                if (_toggle == null)
                {
                    _toggle = GetComponent<Toggle>();
                    _toggle.onValueChanged.AddListener(ToggleValueChanged);
                    ChangeToggleState(_toggle.isOn, true);
                }

                return _toggle;
            }
        }

        private void Awake()
        {
            if (!Application.isPlaying) return;

            if (_toggle == null)
            {
                _toggle = GetComponent<Toggle>();
                _toggle.onValueChanged.AddListener(ToggleValueChanged);

                ChangeToggleState(_toggle.isOn, true);
            }
        }

        protected virtual void ToggleValueChanged( bool isOn )
        {
            OnValueChanged?.Invoke(isOn, content);
            OnValueChangedIncludeToggle?.Invoke(this, isOn);

            if (activeGroup)
            {
                activeGroup.SetActive(isOn);
            }

            if (inactiveGroup)
            {
                inactiveGroup.SetActive(!isOn);
            }

            if(useGroupList)
            {
                activeGroups.ForEach( g => g.SetActive(isOn));
                inactiveGroups.ForEach(g => g.SetActive(!isOn));
            }

            if (animator)
            {
                ChangeToggleState(isOn, false);
            }
        }

        public void ChangeToggleStateWithoutNotify(bool isOn)
        {
            _toggle.SetIsOnWithoutNotify(isOn);

            if (activeGroup)
            {
                activeGroup.SetActive(isOn);
            }

            if (inactiveGroup)
            {
                inactiveGroup.SetActive(!isOn);
            }

            if(useGroupList)
            {
                activeGroups.ForEach( g => g.SetActive(isOn));
                inactiveGroups.ForEach(g => g.SetActive(!isOn));
            }

            if (animator)
            {
                ChangeToggleState(isOn, false);
            }
        }

        private void Update()
        {
            //if (Application.isPlaying) return;

            if (_toggle == null)
                _toggle = GetComponent<Toggle>();

            if (activeGroup)
            {
                activeGroup.SetActive(_toggle.isOn);
            }

            if (inactiveGroup)
            {
                inactiveGroup.SetActive(!_toggle.isOn);
            }

            if(useGroupList)
            {
                activeGroups.ForEach( g => g.SetActive(_toggle.isOn));
                inactiveGroups.ForEach(g => g.SetActive(!_toggle.isOn));
            }
        }

        public virtual void SetContent(string c)
        {
            content = c;
        }

        public virtual void ChangeToggleState(bool isOn, bool forced)
        {
            if (!animator)
            {
                return;
            }

            if (forced)
            {
                animator.Play($"Toggle-{isOn}", -1, 0);
            }
            else
            {
                animator.Play($"Toggle-Change-{isOn}", -1, 0);
            }
        }
    }
}