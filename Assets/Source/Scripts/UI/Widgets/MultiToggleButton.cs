using System;
using System.Collections.Generic;
using CrossBlitz.ViewAPI;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.UI.Widgets
{
    public class MultiToggleButton : MonoBehaviour
    {
        public List<ToggleButton> toggleButtons;
        private ToggleButton _currentButton;
        public ToggleButton CurrentButton => _currentButton;
        public event Action<bool, string, MultiToggleButton> OnValueChanged;
        private void Start()
        {
            for (var i = 0; i < toggleButtons.Count; i++)
            {
                toggleButtons[i].OnValueChanged += ValueChanged;
                toggleButtons[i].SetActive(false);
            }

            SetCurrentButton(0);
        }

        private void OnDestroy()
        {
            for (var i = 0; i < toggleButtons.Count; i++)
            {
                toggleButtons[i].OnValueChanged -= ValueChanged;
            }
        }

        public void SetContents(List<ToggleSelectValue> values)
        {
            for (var i = 0; i < values.Count; i++)
            {
                if (toggleButtons.Count > i)
                {
                    toggleButtons[i].content = values[i].content;
                    toggleButtons[i].Toggle.SetIsOnWithoutNotify(values[i].isOn);
                }
            }
        }

        public void SetCurrentButton(int index)
        {
            if (index < toggleButtons.Count)
            {
                if (_currentButton == toggleButtons[index]) return;
                if (_currentButton != null) _currentButton.SetActive(false);

                _currentButton = toggleButtons[index];
                _currentButton.SetActive(true);
            }
        }

        public void SetCurrentButton(string content)
        {
            var toggleButton = toggleButtons.Find(tb => tb.content == content);
            if (toggleButton != null && toggleButton != _currentButton)
            {
                _currentButton.SetActive(false);
                _currentButton = toggleButton;
                _currentButton.SetActive(true);
            }
            else
            {
                Debug.LogError($"Could not find toggle with content {content}");
            }
        }


        private void ValueChanged(bool isOn, string content)
        {
            OnValueChanged?.Invoke(isOn, content, this);
        }

        [Button]
        public void AddChildButtonsToButtonList()
        {
            if (toggleButtons == null) toggleButtons = new List<ToggleButton>();

            toggleButtons.Clear();

            for (var i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i).GetComponent<ToggleButton>();
                if (child != null)
                {
                    toggleButtons.Add(child);
                }
            }
        }
    }
}