using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace CrossBlitz.UI.Widgets.TMP
{
    public class TMP_FontAssetAnimator : SerializedMonoBehaviour
    {
        private TextMeshProUGUI _label;
        public Dictionary<string, TMP_FontAsset> presets;

        private void Awake()
        {
            _label = GetComponent<TextMeshProUGUI>();
        }

        public void SetPreset(string presetName)
        {
            if (_label == null) _label = GetComponent<TextMeshProUGUI>();
            if (!presets.ContainsKey(presetName))
            {
                Debug.LogError($"No preset name with: {presetName}");
                return;
            }
            _label.font = presets[presetName];
        }
    }
}