using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace CrossBlitz.UI.Widgets.TMP
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class TMP_MaterialAnimator : SerializedMonoBehaviour
    {
        private TextMeshProUGUI _label;
        public Dictionary<string, Material> presets;

        private void Awake()
        {
            _label = GetComponent<TextMeshProUGUI>();
        }

        public void SetPreset(string presetName)
        {
            if (_label == null) _label = GetComponent<TextMeshProUGUI>();
            if (!presets.ContainsKey(presetName))
            {
                Debug.LogError($"No preset name with: {presetName}");
                return;
            }
            _label.fontSharedMaterial = presets[presetName];
        }
    }
}