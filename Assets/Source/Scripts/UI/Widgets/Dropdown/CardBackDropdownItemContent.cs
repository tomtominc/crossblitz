using CrossBlitz.Card;
using UnityEngine;

namespace CrossBlitz.UI.Widgets.Dropdown
{
    public class CardBackDropdownItemContent : DropdownItemContent
    {
        public override void SetContent(DropdownItemData content)
        {
            _content = content;

            var cardBackType = GameUtilities.ParseEnum(_content.name, CardBackFilter.All);

            switch (cardBackType)
            {
                case CardBackFilter.All:
                    var allColor = "#ae977a";
                    background.color = allColor.ToColor();
                    nameLabel.text = "ALL";
                    break;

                case CardBackFilter.Unlocked:
                    var unlockedColor = "#839735";
                    background.color = unlockedColor.ToColor();
                    nameLabel.text = "UNLOCKED";
                    break;

                case CardBackFilter.Locked:
                    var lockedColor = "#984943";
                    background.color = lockedColor.ToColor();
                    nameLabel.text = "LOCKED";
                    break;
            }
        }
    }
}