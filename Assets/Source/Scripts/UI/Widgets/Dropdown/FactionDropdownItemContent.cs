using CrossBlitz.Card;
using UnityEngine;

namespace CrossBlitz.UI.Widgets.Dropdown
{
    public class FactionDropdownItemContent : DropdownItemContent
    {
        public override void SetContent(DropdownItemData content)
        {
            _content = content;

            var faction = GameUtilities.ParseEnum(_content.name, Faction.All);

            nameLabel.text = LocalizationController.Localize(faction.ToString().ToUpper(), faction.ToString().ToUpper());
            background.color = ColorPalette.CrossBlitz.GetFactionColor(faction).ToColor();

            //todo:
            // Check if you have new cards in this faction and place them in a bubble with text.
        }
    }
}