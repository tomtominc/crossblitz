
using System.Linq;
using CrossBlitz.Fables.Data;
using CrossBlitz.UI.Widgets.Dropdown;
using Sirenix.Utilities;
using UnityEngine;

namespace CrossBlitz.UI.Widgets
{
    public class TileTypeDropdownItemContent : DropdownItemContent
    {
        public SpriteAnimation tileTypeAnimator;

        public override void SetContent(DropdownItemData content)
        {
            _content = content;
            var tileType = _content.value;
            tileTypeAnimator.UpdateAnimations();
            FableTileData.PlayTileAnimation(tileType, tileTypeAnimator);
        }
    }
}