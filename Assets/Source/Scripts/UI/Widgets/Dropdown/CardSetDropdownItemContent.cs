using CrossBlitz.Card;
using UnityEngine;

namespace CrossBlitz.UI.Widgets.Dropdown
{
    public class CardSetDropdownItemContent : DropdownItemContent
    {
        public SpriteAnimation iconAnimator;
        public override void SetContent(DropdownItemData content)
        {
            _content = content;

            var cardSet = GameUtilities.ParseEnum(_content.name, CardSet.Core);
            var newLabel = "";

            switch (cardSet)
            {
                case CardSet.Core:
                    newLabel = "CORE";
                    break;
                case CardSet.PiratesAhoy:
                    newLabel = "PIRATE'S PLOY";
                    break;
                case CardSet.StarscapeTwilight:
                    newLabel = "MUSICAL MEDDLING";
                    break;
                case CardSet.PerilousPilfering:
                    newLabel = "PERILOUS PILFERING";
                    break;
                case CardSet.MonksMantra:
                    newLabel = "MONK'S MONTRA";
                    break;
                case CardSet.SproutwoodGuardian:
                    newLabel = "REGENT'S RESOLVE";
                    break;
                default:
                    newLabel = "CORE";
                    break;
            }

            // nameLabel.text = LocalizationController.Localize(cardSet.ToString().ToUpper(), cardSet.ToString().ToUpper());
            nameLabel.text = LocalizationController.Localize(newLabel);
            background.color = ColorPalette.CrossBlitz.GetCardSetFilterColor(cardSet).ToColor();
            iconAnimator.Play(cardSet.ToString().ToLower());
        }
    }
}