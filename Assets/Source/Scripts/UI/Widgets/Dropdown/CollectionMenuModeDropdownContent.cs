using CrossBlitz.DeckEditAPI;
using CrossBlitz.UI.Widgets.Dropdown;
using UnityEngine;

namespace CrossBlitz.UI.Widgets
{
    public class CollectionMenuModeDropdownContent : DropdownItemContent
    {
        public SpriteAnimation buttonAnimator;
        public override void SetContent(DropdownItemData content)
        {
            _content = content;
            var menuMode = GameUtilities.ParseEnum(_content.value, CollectionMenuMode.Collection);

            switch (menuMode)
            {
                case CollectionMenuMode.Collection:
                {
                    var animName = contentType == ContentType.Nonactive ? "collection" : "collection-selected";
                    buttonAnimator.Play(animName);
                    break;
                }
                case CollectionMenuMode.CardBacks:
                {
                    var animName = contentType == ContentType.Nonactive ? "card-backs" : "card-backs-selected";
                    buttonAnimator.Play(animName);
                    break;
                }
                case CollectionMenuMode.ManaMeld:
                {
                    var animName = contentType == ContentType.Nonactive ? "mana-meld" : "mana-meld-selected";
                    buttonAnimator.Play(animName);
                    break;
                }
            }
        }
    }
}