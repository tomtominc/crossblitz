using CrossBlitz.Card;
using CrossBlitz.UI.Widgets.Dropdown;

namespace CrossBlitz.UI.Widgets
{
    public class KeywordFilterDropdownItemContent : DropdownItemContent
    {
        public SpriteAnimation iconAnimator;

        public override void SetContent(DropdownItemData content)
        {
            _content = content;

            var type = GameUtilities.ParseEnum(_content.value, KeywordFilter.All);
            var lowerName = type.ToString().Replace("_", "-").ToLower();
            var upperName = type.ToString().Replace("_", " ").ToUpper();
            nameLabel.text = LocalizationController.Localize(upperName, upperName);
            iconAnimator.Play(lowerName.ToLower());
        }
    }
}