using CrossBlitz.Card;
using UnityEngine;

namespace CrossBlitz.UI.Widgets.Dropdown
{
    public class RelicDropdownItemContent : DropdownItemContent
    {
        public override void SetContent(DropdownItemData content)
        {
            _content = content;

            var relicType = GameUtilities.ParseEnum(_content.name, RelicFilter.All);

            switch (relicType)
            {
                case RelicFilter.All:
                    var allColor = "#ae977a";
                    background.color = allColor.ToColor();
                    nameLabel.text = "ALL";
                    break;

                case RelicFilter.ElderRelics:
                    var relicColor = "#809499";
                    background.color = relicColor.ToColor();
                    nameLabel.text = "E. RELICS";
                    break;

                case RelicFilter.Relics:
                    var eRelicColor = "#3d4d57";
                    background.color = eRelicColor.ToColor();
                    nameLabel.text = "RELICS";
                    break;

            }
        }
    }
}