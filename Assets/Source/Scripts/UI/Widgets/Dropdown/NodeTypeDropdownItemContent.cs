using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Grid;
using Sirenix.Utilities;
using UnityEngine;

namespace CrossBlitz.UI.Widgets.Dropdown
{
    public class NodeTypeDropdownItemContent : DropdownItemContent
    {
        public SpriteAnimation markerAnimator;
        public SpriteAnimation nodeAnimator;
        public override void SetContent(DropdownItemData content)
        {
            _content = content;

            nameLabel.text = content.name.SplitPascalCase();
            var nodeType = GameUtilities.ParseEnum(content.value, NodeType.None);

            if (FableTileData.MarkerAnimations.ContainsKey(nodeType) &&
                !string.IsNullOrEmpty(FableTileData.MarkerAnimations[nodeType]))
            {
                var markerAnim = FableTileData.MarkerAnimations[nodeType];
                markerAnimator.Play(markerAnim);
            }
            else
            {
                markerAnimator.SetActive(false);
                switch (nodeType)
                {
                    case NodeType.None: Debug.LogError("Trying to display NONE.");
                        break;
                    case NodeType.Start:
                        nodeAnimator.Play("start");
                        break;
                }
            }


        }
    }
}