using CrossBlitz.Databases;
using CrossBlitz.Fables.Data;
using CrossBlitz.UI.Widgets.Dropdown;
using UnityEngine;

namespace CrossBlitz.UI.Widgets
{
    public class TileDecorationDropdownItemContent : DropdownItemContent
    {
        public SpriteAnimation decorationAnimator;

        public override void SetContent(DropdownItemData content)
        {
            _content = content;
            var animationName = content.name;

            if (string.IsNullOrEmpty(animationName))
            {
                Debug.LogError("Something went wrong, animation is null");
                return;
            }

            nameLabel.text = animationName.Replace("-", " ").ToUpper();

            var decorationData = Db.FablesDatabase.GetDecorationData(animationName);

            decorationAnimator.Play(animationName);

            if (decorationAnimator.image != null && decorationAnimator.image.sprite != null)
            {
                decorationAnimator.RectTransform().sizeDelta = new Vector2(
                    decorationAnimator.image.sprite.rect.width * 2,
                    decorationAnimator.image.sprite.rect.height * 2);
            }

            decorationAnimator.RectTransform().anchoredPosition = decorationData.PixelOffset;
        }
    }
}