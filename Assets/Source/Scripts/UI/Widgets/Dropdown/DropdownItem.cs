﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.UI.Widgets.Dropdown
{

    [RequireComponent(typeof(Button))]
    public class DropdownItem : MonoBehaviour
    {
        public DropdownItemContent inactiveContent;
        public DropdownItemContent selectedContent;

        [HideInInspector]
        public DropdownItemData data;
        private Button button;
        private bool selected;

        public event Action<DropdownItemData> Selected = delegate { };

        private void Awake()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(OnSelected);
        }

        public virtual void SetContent(DropdownItemData _data)
        {
            data = _data;

            inactiveContent.SetContent(data);
            selectedContent.SetContent(data);

            Unselect();
        }

        public virtual void OnSelected()
        {
            if (!selected)
            {
                Select();
                Selected(data);
            }
        }

        public virtual void Select()
        {
            selected = true;

            inactiveContent.SetActive(false);
            selectedContent.SetActive(true);
        }

        public virtual void Unselect()
        {
            //if (selected)
            {
                selected = false;
                inactiveContent.SetActive(true);
                selectedContent.SetActive(false);
            }
        }
    }
}