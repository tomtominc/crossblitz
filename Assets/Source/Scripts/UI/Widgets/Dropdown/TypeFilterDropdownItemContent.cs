using CrossBlitz.Card;
using CrossBlitz.UI.Widgets.Dropdown;

namespace CrossBlitz.UI.Widgets
{
    public class TypeFilterDropdownItemContent : DropdownItemContent
    {
        public SpriteAnimation iconAnimator;
        public override void SetContent(DropdownItemData content)
        {
            _content = content;

            var type = GameUtilities.ParseEnum(_content.name, TypeFilter.All);
            var upperName = type.ToString().ToUpper();
            nameLabel.text = LocalizationController.Localize(upperName, upperName);
            background.color = ColorPalette.CrossBlitz.GetTypeFilterColor(type).ToColor();
            iconAnimator.Play(upperName.ToLower());
        }
    }
}