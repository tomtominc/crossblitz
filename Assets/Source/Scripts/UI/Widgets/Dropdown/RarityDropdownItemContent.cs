using CrossBlitz.Card;
using UnityEngine;

namespace CrossBlitz.UI.Widgets.Dropdown
{
    public class RarityDropdownItemContent : DropdownItemContent
    {
        public SpriteAnimation iconAnimator;
        public override void SetContent(DropdownItemData content)
        {
            _content = content;

            var rarity = GameUtilities.ParseEnum(_content.name, Rarity.All);

            nameLabel.text = LocalizationController.Localize(rarity.ToString().ToUpper(), rarity.ToString().ToUpper());
            background.color = ColorPalette.CrossBlitz.GetRarityFilterColor(rarity).ToColor();
            iconAnimator.Play(rarity.ToString().ToLower());
        }
    }
}