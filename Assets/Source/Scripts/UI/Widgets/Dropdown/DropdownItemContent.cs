﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.UI.Widgets.Dropdown
{
    public class DropdownItemContent : MonoBehaviour
    {
        public enum ContentType
        {
            Nonactive,
            Selected,
        }

        public ContentType contentType;
        public TextMeshProUGUI nameLabel;
        public Image icon;
        public Image background;

        protected DropdownItemData _content;

        public virtual void SetContent(DropdownItemData content)
        {
            _content = content;

            if (nameLabel)
            {
                nameLabel.text = _content.name;
            }

            if (icon)
            {
                icon.sprite = _content.icon;
            }

            if (background)
            {
                background.color = _content.color;
            }
        }
    }
}
