using CrossBlitz.Card;
using UnityEngine;

namespace CrossBlitz.UI.Widgets.Dropdown
{
    public class ClassDropdownItemContent : DropdownItemContent
    {
        public SpriteAnimation iconAnimator;
        public override void SetContent(DropdownItemData content)
        {
            _content = content;

            var @class = GameUtilities.ParseEnum(_content.name, Class.All);

            nameLabel.text = LocalizationController.Localize(@class.ToString().ToUpper(), @class.ToString().ToUpper());
            background.color = ColorPalette.CrossBlitz.GetClassColor(@class).ToColor();
            iconAnimator.Play(@class.ToString().ToLower());

            //todo:
            // Check if you have new cards in this faction and place them in a bubble with text.
        }
    }
}