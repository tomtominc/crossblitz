﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.UI.Widgets.Dropdown
{
    [RequireComponent(typeof(CanvasGroup))]
    public class DropdownContainer : MonoBehaviour
    {
        public CanvasGroup canvasGroup;
        public DropdownItem itemPrefab;
        public RectTransform itemsLayout;

        private List<DropdownItem> items;
        private GridLayoutGroup _gridLayout;
        private int _maxRowCount;
        public event Action<DropdownItemData> ItemSelected = delegate { };

        public RectTransform Rect
        {
            get { return GetComponent<RectTransform>(); }
        }

        public void SetContents(List<DropdownItemData> contents, DropdownItem overrideItem = null)
        {
            if (_gridLayout == null)
            {
                _gridLayout = itemsLayout.GetComponent<GridLayoutGroup>();
                _maxRowCount = _gridLayout.constraintCount;
            }

            if (overrideItem)
            {
                itemPrefab = overrideItem;
            }

            RemoveContents();

            for (int i = 0; i < contents.Count; i++)
            {
                DropdownItem item = Instantiate(itemPrefab, itemsLayout, false);
                item.SetContent(contents[i]);
                item.Selected += OnItemSelected;
                items.Add(item);

                if (i == 0)
                {
                    item.Select();
                }
            }

            _gridLayout.constraintCount = Mathf.Clamp(contents.Count, 1, _maxRowCount);
        }

        public void RemoveContents()
        {
            itemsLayout.DestroyChildren();
            items = new List<DropdownItem>();
        }

        public void OnItemSelected(DropdownItemData data)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (data.name != items[i].data.name)
                {
                    items[i].Unselect();
                }
            }

            ItemSelected?.Invoke(data);
        }

        public void SetEnabled(bool enable)
        {
            canvasGroup.interactable = enable;
            canvasGroup.blocksRaycasts = enable;
            canvasGroup.alpha = enable ? 1 : 0;
            gameObject.SetActive(enable);
        }
    }
}
