using UnityEngine;

namespace CrossBlitz.UI.Widgets.Dropdown
{
    [System.Serializable]
    public class IDropdownData
    {
        public virtual string GetName()
        {
            return string.Empty;
        }

        public virtual Sprite GetIcon()
        {
            return null;
        }

        public virtual Color GetColor()
        {
            return Color.clear;
        }
    }
}