using CrossBlitz.UI.Widgets.Dropdown;

namespace CrossBlitz.UI.Widgets
{
    public class NpcTypeDropdownItemContent : DropdownItemContent
    {
        public SpriteAnimation npcTypeAnimator;
        private string _npcAnimation;

        public override void SetContent(DropdownItemData content)
        {
            _content = content;
            nameLabel.text = content.value.ToUpper();
            _npcAnimation = _content.value + "-march";
        }

        private void Update()
        {
            if (string.IsNullOrEmpty(_npcAnimation)) return;

            if (!npcTypeAnimator.playing)
            {
                npcTypeAnimator.Play(_npcAnimation);
            }
        }
    }
}