﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;
using Image = UnityEngine.UI.Image;

namespace CrossBlitz.UI.Widgets.Dropdown
{
    public class DropdownBox : MonoBehaviour
    {
        public enum DropdownItemType
        {
            EnumProperty,
            SpriteAnimationValues,
        }

        public bool setContentsOnStart = true;
        public bool closeWhenItemIsSelected;
        public bool dropdownIsAlwaysAlive;
        public DropdownItem overrideItem;

        public Button dropdownButton;
        public ToggleButton dropdownToggleButton;
        public MultiToggleButton multiDropdownToggleButton;
        public DropdownContainer container;
        public Modal dropdownModal;
        public TextMeshProUGUI title;
        public Image icon;
        public bool useMultiIconAnimators;
        [HideIf("useMultiIconAnimators")]
        public SpriteAnimation iconAnimator;
        [ShowIf("useMultiIconAnimators")]
        public List<SpriteAnimation> iconAnimators;
        public TextMeshProUGUI label;

        public DropdownItemType dropDownItemType;

        [InfoBox("If checked, this will only show the first instance of a repeated first name split by '-'.")]
        [ShowIf("@this.dropDownItemType == DropdownItemType.SpriteAnimationValues")]
        public bool onlyUniqueFirstNames;
        [InfoBox("If checked, this will only show the first instance of a repeated set if other names are appended with -2, -3 etc.")]
        [ShowIf("@this.dropDownItemType == DropdownItemType.SpriteAnimationValues")]
        public bool onlyNonVariants;
        [ShowIf("@this.dropDownItemType == DropdownItemType.SpriteAnimationValues")]
        public SpriteAnimationAsset assetProperty;
        [ShowIf("@this.dropDownItemType == DropdownItemType.EnumProperty")]
        public string enumProperty;
        public List<string> itemsToLeaveOut;
        public event Action<DropdownItemData> ItemSelected;
        public event Action<DropdownBox> OnClickedDropdown;
        private bool _dropDownIsEnabled;
        [HideInInspector]
        public List<DropdownItemData> contents;

        private void Start()
        {
            RefreshDropBox();
        }

        public void RefreshDropBox()
        {
            if (string.IsNullOrEmpty(enumProperty))
            {
                Debug.LogWarning($"Enum Prop has not been configured for this dropdown: {name}");
                return;
            }
            contents = new List<DropdownItemData>();

            List<string> values = new List<string>();

            if (dropDownItemType == DropdownItemType.EnumProperty)
            {
                var type = GameUtilities.GetEnumType(enumProperty);
                values = Enum.GetNames(type).ToList();
            }
            else if (dropDownItemType == DropdownItemType.SpriteAnimationValues)
            {
                values = assetProperty.animations.Select(anim => anim.name).ToList();

                if (onlyUniqueFirstNames)
                {
                    var uniqueNames = new List<string>();

                    for (var i = 0; i < values.Count; i++)
                    {
                        var currName = values[i].Split('-')[0];

                        if (!uniqueNames.Contains(currName))
                        {
                            uniqueNames.Add(currName);
                        }
                    }

                    values = uniqueNames;
                }
                else if (onlyNonVariants)
                {
                    var uniqueNames = new List<string>();

                    for (var i = 0; i < values.Count; i++)
                    {
                        var lastIndexOfSplit = values[i].LastIndexOf('-');
                        var currName = values[i].Remove(lastIndexOfSplit, values[i].Length-lastIndexOfSplit);

                        if (!uniqueNames.Contains(currName))
                        {
                            uniqueNames.Add(currName);
                        }
                    }

                    values = uniqueNames;
                }
            }
            else
            {
                Debug.LogError($"Dropdown Type {dropDownItemType} has not been correctly setup.");
            }

            for (int i = 0; i < values.Count; i++)
            {
                var value = values[i];

                if (string.IsNullOrEmpty(value) || value.Equals("NONE") || value.Equals("None") || itemsToLeaveOut.Contains(value)) continue;

                var readableValue = value.Replace("_", " ");

                var content = new DropdownItemData
                {
                    value = value,
                    name = readableValue,
                    iconAnimation = value.Replace("_", "-").ToLower()
                };

                contents.Add(content);
            }

            if (setContentsOnStart)
            {
                SetContents();
            }

            SetItemValues(contents[0]);
        }

        public void SetContents()
        {
            container.SetContents(contents,overrideItem);
        }

        private void OnEnable()
        {
            if (!dropdownModal)
            {
                dropdownModal = FindObjectOfType<Modal>();
            }

            if (dropdownModal)
                dropdownModal.Disabled += OnModalDisabled;

            if (dropdownButton)
                dropdownButton.onClick.AddListener(OnDropDownButton);

            if (dropdownToggleButton)
                dropdownToggleButton.OnValueChanged += OnDrownDownToggleButton;

            if (multiDropdownToggleButton)
                multiDropdownToggleButton.OnValueChanged += OnDrownDownToggleButton;

            container.ItemSelected += OnItemSelected;
        }

        private void OnDisable()
        {
            if (dropdownModal)
                dropdownModal.Disabled -= OnModalDisabled;

            if (dropdownButton)
                dropdownButton.onClick.RemoveListener(OnDropDownButton);

            if (dropdownToggleButton)
                dropdownToggleButton.OnValueChanged -= OnDrownDownToggleButton;

            if (multiDropdownToggleButton)
                multiDropdownToggleButton.OnValueChanged -= OnDrownDownToggleButton;

            container.ItemSelected -= OnItemSelected;
        }

        public void ResetDropdown()
        {
            SetItemValues(contents[0]);
        }

        private void OnModalItemsChanged()
        {

        }

        private void OnModalDisabled()
        {
            if (_dropDownIsEnabled)
            {
                OnDropDownButton();
            }
        }

        private void OnDropDownButton()
        {
            if (dropdownIsAlwaysAlive)
            {
                OnClickedDropdown?.Invoke(this);
                return;
            }

            if (!_dropDownIsEnabled)
            {
                _dropDownIsEnabled = true;
                container.SetEnabled(true);

                if (dropdownModal)
                {
                    dropdownModal.ParentObjectToModal(container.Rect);
                }

                OnClickedDropdown?.Invoke(this);
            }
            else
            {
                _dropDownIsEnabled = false;

                if (dropdownModal)
                {
                    container.Rect.SetParent(transform);
                }

                container.SetEnabled(false);

                if (dropdownToggleButton)
                {
                    dropdownToggleButton.Toggle.SetIsOnWithoutNotify(false);
                }

                if (multiDropdownToggleButton)
                {
                    multiDropdownToggleButton.CurrentButton.Toggle.SetIsOnWithoutNotify(false);
                }
            }
        }

        private void OnDrownDownToggleButton(bool isOn, string content, MultiToggleButton toggle)
        {
            _dropDownIsEnabled = !isOn;
            OnDropDownButton();
        }

        private void OnDrownDownToggleButton(bool isOn, string content)
        {
            _dropDownIsEnabled = !isOn;
            OnDropDownButton();
        }

        public void SetItemSelected(string itemName)
        {
            if (contents.Count <= 0)
                return;

            DropdownItemData data = contents.Find(x => x.name == itemName);

            if (data == null)
            {
                itemName = itemName.Replace("_", " ");
                data = contents.Find(x => x.name == itemName);
            }

            if (data == null)
            {
                Debug.LogErrorFormat("could not find item with name '{0}'", itemName);
                string itemsInArray = string.Empty;
                for (int i = 0; i < contents.Count; i++)
                {
                    itemsInArray += contents[i].name + ", ";
                }
                Debug.LogErrorFormat("size {0}; items {1}", contents.Count, itemsInArray);
                return;
            }

            SetItemValues(data);
        }

        private void SetItemValues(DropdownItemData data)
        {
            if (icon)
            {
                icon.sprite = data.icon;
            }

            if (iconAnimator)
            {
                iconAnimator.Play(data.iconAnimation);
            }

            if (useMultiIconAnimators)
            {
                iconAnimators.ForEach( e => e.Play(data.iconAnimation));
            }

            if (label)
            {
                label.text = data.name.ToUpper();
            }
        }

        private void OnItemSelected(DropdownItemData data)
        {
            SetItemValues(data);
            ItemSelected?.Invoke(data);

            if (multiDropdownToggleButton)
            {
                multiDropdownToggleButton.SetCurrentButton(data.name);
                multiDropdownToggleButton.CurrentButton.Toggle.SetIsOnWithoutNotify(true);
            }

            if (closeWhenItemIsSelected && dropdownModal)
            {
                dropdownModal.OnDisableButton();
            }
        }
    }
}
