using UnityEngine;

namespace CrossBlitz.UI.Widgets.Dropdown
{
    [System.Serializable]
    public class DropdownItemData : IDropdownData
    {
        public string value;
        public string name;
        public Sprite icon;
        public Color color;
        public string iconAnimation;

        public string GetName()
        {
            return name;
        }

        public Sprite GetIcon()
        {
            return icon;
        }

        public Color GetColor()
        {
            return color;
        }
    }
}