using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.UI.Widgets
{
    public class Sidebar : MonoBehaviour
    {
        public Button expandButton;
        public Button shrinkButton;

        public float shrinkSize = 194;
        public float expandSize = 700;

        public RectTransform buttonContainer;
        public RectTransform sizeRect;

        private List<SidebarButton> _sideBarButtons;

        private void Start()
        {
            if (expandButton) expandButton.onClick.AddListener(Expand);
            if (shrinkButton) shrinkButton.onClick.AddListener(Shrink);

            _sideBarButtons=new List<SidebarButton>();

            for (var i = 0; i < buttonContainer.childCount; i++)
            {
                _sideBarButtons.Add(buttonContainer.GetChild(i).GetComponent<SidebarButton>());
            }
        }

        public void Open()
        {

        }

        public void Expand()
        {
            DOTween.Kill(sizeRect, true);

            expandButton.SetActive(false);

            sizeRect.DOSizeDelta(new Vector2(expandSize, sizeRect.sizeDelta.y), 0.25f)
                .SetEase(Ease.OutBack)
                .OnComplete(() =>
                {
                    shrinkButton.SetActive(true);
                    _sideBarButtons.ForEach( button => button.OnFinishedExpand());
                });
        }

        public void Shrink()
        {
            DOTween.Kill(sizeRect, true);

            shrinkButton.SetActive(false);

            _sideBarButtons.ForEach( button => button.OnStartShrink());
            sizeRect.DOSizeDelta(new Vector2(shrinkSize, sizeRect.sizeDelta.y), 0.25f)
                .SetEase(Ease.InOutBack)
                .OnComplete(() =>
                {
                    expandButton.SetActive(true);
                });
        }
    }
}