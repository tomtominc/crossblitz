using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.UI.Widgets
{
    public class SidebarButton : MonoBehaviour
    {
        public ToggleButton button;
        public GameObject nameDisplay;
        public TextMeshProUGUI nameLabel;
        public TextMeshProUGUI descriptionLabel;

        /// <summary>
        /// When the button is fully expanded
        /// </summary>
        public void OnFinishedExpand()
        {
            if (nameDisplay) nameDisplay.SetActive(true);
        }

        /// <summary>
        /// When the button starts to shrink
        /// </summary>
        public void OnStartShrink()
        {
            if (nameDisplay) nameDisplay.SetActive(false);
        }
    }
}