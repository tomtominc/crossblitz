using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace CrossBlitz.UI.Widgets
{
    public class MultiSelectToggleButtonLayout : MonoBehaviour
    {
        public ToggleButton toggleButtonPrefab;
        public RectTransform toggleButtonLayout;

        public event Action<bool, string> OnContentValueChanged;

        public void Rebuild(List<string> contents)
        {
            toggleButtonLayout.DestroyChildren();

            for (var i = 0; i < contents.Count; i++)
            {
                var toggleButton = Instantiate(toggleButtonPrefab, toggleButtonLayout);
                toggleButton.content = contents[i];
                toggleButton.OnValueChanged += ContentValueChanged;
            }
        }

        public void Rebuild(List<ToggleSelectValue> contents)
        {
            toggleButtonLayout.DestroyChildren();

            for (var i = 0; i < contents.Count; i++)
            {
                var toggleButton = Instantiate(toggleButtonPrefab, toggleButtonLayout);
                toggleButton.content = contents[i].content;
                toggleButton.Toggle.isOn = contents[i].isOn;
                toggleButton.OnValueChanged += ContentValueChanged;

                var label = toggleButton.GetComponentInChildren<TextMeshProUGUI>();
                if (label != null)
                {
                    label.text = contents[i].content;
                }
            }
        }

        private void ContentValueChanged(bool isOn, string content)
        {
            OnContentValueChanged?.Invoke(isOn,content);
        }
    }
}