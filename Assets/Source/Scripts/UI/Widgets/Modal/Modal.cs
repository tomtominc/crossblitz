﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Modal : MonoBehaviour
{
    public CanvasGroup canvasGroup;
    public Button disableButton;

    public event Action Disabled = delegate { };
    // public event Action ModalItemsChanged = delegate { };

    private void Awake()
    {
        SetModalActive(false);
    }

    private void OnEnable()
    {
        disableButton.onClick.AddListener(OnDisableButton);
    }

    private void OnDisable()
    {
        disableButton.onClick.RemoveListener(OnDisableButton);
    }

    public void SetModalActive(bool active)
    {
        canvasGroup.interactable = active;
        canvasGroup.blocksRaycasts = active;
        canvasGroup.alpha = active ? 1 : 0;
    }

    public void ParentObjectToModal(RectTransform rt)
    {
        rt.SetParent(transform, true);
        SetModalActive(true);
    }

    public void OnDisableButton()
    {
        SetModalActive(false);
        Disabled();
    }
}
