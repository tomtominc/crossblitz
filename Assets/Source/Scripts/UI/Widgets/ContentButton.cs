using System;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.UI.Widgets
{
    [RequireComponent(typeof(Button))]
    public class ContentButton : MonoBehaviour
    {
        public event Action<ContentButton> OnClick;
        private Button _button;

        public Button button => _button;

        private void Start()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(Click);
        }

        private void Click()
        {
            OnClick?.Invoke(this);
        }
    }
}