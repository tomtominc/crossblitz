using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.UI.Widgets
{
    /// <summary>
    /// Best fucking dropdown implementation
    /// </summary>
    public class TakoDropdown : MonoBehaviour
    {
        public enum InteractType
        {
            Button,
            Toggle,
        }

        public InteractType interactType;
        [ShowIf("@this.interactType == InteractType.Button")]
        public Button button;
        [ShowIf("@this.interactType == InteractType.Toggle")]
        public Toggle toggle;


    }
}