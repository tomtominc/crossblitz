﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using CrossBlitz.Card;
using CrossBlitz.UI.Widgets.Dropdown;

public class AbilityItem : MonoBehaviour
{
    public Button removeButton;
    public Button addConditionButton;
    public ConditionItem conditionItemPrefab;

    [Header("DROPDOWNS")]
    public DropdownBox abilityKeyword;
    public DropdownBox triggerType;
    public DropdownBox triggerTarget;
    public DropdownBox effectTarget;
    public DropdownBox equationDropdown;
    public DropdownBox summonAllowedFieldDropdown;
    public DropdownBox summonPlacementDropdown;

    [Header("INPUT FIELDS")]
    public TMP_InputField amountInput;
    public TMP_InputField cardSelector;
    public TMP_InputField equationInput;
    public TMP_InputField summonCount;

    [Header("TITLES")]
    public TextMeshProUGUI amountTitle;

    [Header("LAYOUTS")]
    public GameObject equationLayout;
    public GameObject summonLayout;

    [Header("MISC")]
    public List<ConditionItem> conditions;

    public event Action<AbilityItem> RemoveItem = delegate { };

    //   [HideInInspector]
    public Ability ability;

    public Ability GetAbility()
    {
        List<AbilityCondition> abilityConditions = new List<AbilityCondition>();

        for (int i = 0; i < conditions.Count; i++)
        {
            abilityConditions.Add(conditions[i].condition);
        }

        ability.conditions = abilityConditions;

        return ability;
    }

    private void Start()
    {
        conditions = new List<ConditionItem>();
    }

    public void OnEnable()
    {
        removeButton.onClick.AddListener(OnRemoveItem);
        addConditionButton.onClick.AddListener(OnAddCondition);

        abilityKeyword.ItemSelected += OnAbilitySelected;
        triggerType.ItemSelected += OnTriggerTypeSelected;
        triggerTarget.ItemSelected += OnTriggerTargetSelected;
        effectTarget.ItemSelected += OnEffectTargetSelected;
        equationDropdown.ItemSelected += OnEquationSelected;
        summonAllowedFieldDropdown.ItemSelected += OnSummonAllowedFieldSelected;
        summonPlacementDropdown.ItemSelected += OnSummonPlacementSelected;

        amountInput.onValueChanged.AddListener(OnAmountSubmitted);
        cardSelector.onValueChanged.AddListener(OnCardSelected);
        equationInput.onValueChanged.AddListener(OnEquationInput);
        summonCount.onValueChanged.AddListener(OnSummonCountSubmit);

        ResetAbility();
    }

    private void OnDisable()
    {
        removeButton.onClick.RemoveListener(OnRemoveItem);
        addConditionButton.onClick.RemoveListener(OnAddCondition);

        abilityKeyword.ItemSelected -= OnAbilitySelected;
        triggerType.ItemSelected -= OnTriggerTypeSelected;
        triggerTarget.ItemSelected -= OnTriggerTargetSelected;
        effectTarget.ItemSelected -= OnEffectTargetSelected;
        equationDropdown.ItemSelected -= OnEquationSelected;
        summonAllowedFieldDropdown.ItemSelected -= OnSummonAllowedFieldSelected;
        summonPlacementDropdown.ItemSelected -= OnSummonPlacementSelected;

        amountInput.onValueChanged.RemoveListener(OnAmountSubmitted);
        cardSelector.onValueChanged.RemoveListener(OnCardSelected);
        equationInput.onValueChanged.RemoveListener(OnEquationInput);
        summonCount.onValueChanged.RemoveListener(OnSummonCountSubmit);
    }

    public void SetAbility(Ability _ability)
    {
        ability = _ability;

        OnKeywordChanged();
        OnSummonPlacementChanged();

        StartCoroutine(SetConditionsDelayed());
    }

    public IEnumerator SetConditionsDelayed()
    {
        for (int i = 0; i < ability.conditions.Count; i++)
        {
            ConditionItem condition = Instantiate(conditionItemPrefab, transform, false);
            condition.transform.SetSiblingIndex(conditions.Count + 1);
            conditions.Add(condition);

            yield return new WaitForEndOfFrame();

            condition.SetCondition(ability.conditions[i]);
        }

        yield return new WaitForEndOfFrame();

        abilityKeyword.SetItemSelected(ability.keyword.ToString());
        triggerType.SetItemSelected(ability.triggerType.ToString());
        // triggerTarget.SetItemSelected(ability.triggerTarget.ToString());
        // effectTarget.SetItemSelected(ability.effectTarget.ToString());
        // equationDropdown.SetItemSelected(ability.modifyEquation.ToString());
        summonPlacementDropdown.SetItemSelected(ability.summonMinionData.placement.ToString());

        amountInput.text = ability.GetValue().ToString();
        equationInput.text = amountInput.text;
        summonCount.text = ability.summonMinionData.count.ToString();

        if (ability.keyword == AbilityKeyword.SUMMON)
        {
            cardSelector.text = ability.summonMinionData.cardId;
        }
    }

    private void OnRemoveItem()
    {
        RemoveItem(this);
    }

    private void ResetAbility()
    {
        ability = new Ability();

        triggerType.SetActive(false);
        triggerTarget.SetActive(false);
        effectTarget.SetActive(false);
        equationDropdown.SetActive(false);
        summonAllowedFieldDropdown.SetActive(false);
        summonPlacementDropdown.SetActive(false);

        amountInput.SetActive(false);
        cardSelector.SetActive(false);
        equationInput.SetActive(false);
        summonCount.SetActive(false);

        equationLayout.SetActive(false);
        summonLayout.SetActive(false);
    }

    private void OnKeywordChanged()
    {
        if (AbilityUtilities.IsTriggerType(ability.keyword))
        {
            triggerType.SetActive(true);
            triggerType.title.text = AbilityUtilities.GetTriggerTitle(ability.keyword);
        }

        if (AbilityUtilities.HasFixedTriggerType(ability.keyword))
        {
            ability.triggerType = AbilityUtilities.GetFixedTriggerType(ability.keyword);
        }

        if (AbilityUtilities.HasTriggerTarget(ability.keyword))
        {
            triggerTarget.SetActive(true);
            triggerTarget.title.text = AbilityUtilities.GetTriggerTargetTitle(ability.keyword, ability.triggerType);
        }

        if (AbilityUtilities.HasEffectTarget(ability.keyword))
        {
            effectTarget.SetActive(true);
            effectTarget.title.text = AbilityUtilities.GetEffectTargetTitle(ability.keyword, ability.triggerType);
        }

        if (AbilityUtilities.KeywordHasValue(ability.keyword))
        {
            amountInput.SetActive(true);
            amountInput.text = string.Empty;
            amountTitle.text = AbilityUtilities.GetValueTitle(ability.keyword);
        }

        if (AbilityUtilities.UsesCardSelector(ability.keyword))
        {
            cardSelector.SetActive(true);
        }

        if (AbilityUtilities.UsesEquation(ability.keyword))
        {
            equationLayout.SetActive(true);
            equationDropdown.SetActive(true);
            equationInput.SetActive(true);
        }

        if (ability.keyword == AbilityKeyword.SUMMON)
        {
            summonLayout.SetActive(true);
            summonPlacementDropdown.SetActive(true);
            summonAllowedFieldDropdown.SetActive(true);
        }
    }

    private void OnSummonPlacementChanged()
    {
        if (ability.summonMinionData.placement == SummonPlacement.RANDOM)
        {
            summonCount.SetActive(true);
        }
        else
        {
            summonCount.SetActive(false);
        }
    }

    private void OnAbilitySelected(DropdownItemData data)
    {
        ResetAbility();

        ability.keyword = GameUtilities.ParseEnum(data.name.Replace(" ", "_"), AbilityKeyword.NONE);

        OnKeywordChanged();
    }

    private void OnTriggerTypeSelected(DropdownItemData data)
    {
        ability.triggerType = GameUtilities.ParseEnum(data.name.Replace(" ", "_"), TriggerType.NONE);
    }

    private void OnTriggerTargetSelected(DropdownItemData data)
    {
        //ability.triggerTarget = GameUtilities.ParseEnum(data.name.Replace(" ", "_"), TriggerTarget.NONE);
    }

    private void OnEffectTargetSelected(DropdownItemData data)
    {
        //ability.effectTarget = GameUtilities.ParseEnum(data.name.Replace(" ", "_"), TriggerTarget.NONE);
    }

    private void OnEquationSelected(DropdownItemData data)
    {
        //ability.modifyEquation = GameUtilities.ParseEnum(data.name.Replace(" ", "_"), ModifyEquation.NONE);
    }

    private void OnSummonAllowedFieldSelected(DropdownItemData data)
    {
      //  ability.summonMinionData.allowed = GameUtilities.ParseEnum(data.name.Replace(" ", "_"), AllowedField.ANY);
    }

    private void OnSummonPlacementSelected(DropdownItemData data)
    {
        ability.summonMinionData.placement = GameUtilities.ParseEnum(data.name.Replace(" ", "_"), SummonPlacement.NONE);

        OnSummonPlacementChanged();
    }

    private void OnEquationInput(string inputString)
    {
        ability.ResetValues();

        if (ability != null && int.TryParse(inputString, out int amount))
        {
            if (ability.keyword == AbilityKeyword.DAMAGE)
            {
                ability.damageValue = amount;
            }
        }
    }

    private void OnAmountSubmitted(string amountString)
    {
        ability.ResetValues();

        if (ability != null && int.TryParse(amountString, out int amount))
        {
            switch (ability.keyword)
            {
                case AbilityKeyword.DAMAGE:
                    ability.damageValue = amount;
                    break;
                case AbilityKeyword.HEAL:
                    ability.healAmount = amount;
                    break;
                case AbilityKeyword.DUAL_STRIKE:
                    //ability.multiAttackCount = amount;
                    break;
                case AbilityKeyword.ARMOR_BODY:
                    ability.armorBodyAmount = amount;
                    break;
                case AbilityKeyword.MODIFY_HEALTH:
                    ability.healthModifier = amount;
                    break;
                case AbilityKeyword.MODIFY_POWER:
                    ability.powerModifier = amount;
                    break;
                case AbilityKeyword.GAIN_ARMOR:
                    ability.armorModifier = amount;
                    break;
            }
        }
    }

    private void OnCardSelected(string cardId)
    {
        if (ability.keyword == AbilityKeyword.SUMMON)
        {
            ability.summonMinionData.cardId = cardId;
        }
    }

    private void OnSummonCountSubmit(string amountString)
    {
        if (ability != null && int.TryParse(amountString, out int amount))
        {
            ability.summonMinionData.count = amount;
        }
    }

    private void OnAddCondition()
    {
        ConditionItem condition = Instantiate(conditionItemPrefab, transform, false);
        condition.transform.SetSiblingIndex(conditions.Count + 1);
        conditions.Add(condition);
    }
}
