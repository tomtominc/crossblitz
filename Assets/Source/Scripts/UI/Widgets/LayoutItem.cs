using System;
using System.Collections.Generic;
using CrossBlitz.AssetManagement;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace CrossBlitz.UI.Widgets
{
    public class LayoutItemData
    {
        public string title;
        public string subTitle;
        public string sideIconAnimation;
        public string addressableIcon;
        public object data;
        public int indexInList;
        public int listCount;
    }
    public class LayoutItem : MonoBehaviour, IPointerClickHandler
    {
        [BoxGroup("Display Options")]
        public TextMeshProUGUI titleLabel;
        [BoxGroup("Display Options")]
        public TMP_InputField titleInputField;
        [BoxGroup("Display Options")]
        public TextMeshProUGUI subTitleLabel;
        [BoxGroup("Display Options")]
        public SpriteAnimation sideIcon;
        [BoxGroup("Display Options")]
        public Image addressableLoadedIcon;
        [BoxGroup("Display Options")]
        public List<TextMeshProUGUI> labels;
        [BoxGroup("Display Options")]
        public List<Text> textLabels;
        [BoxGroup("Display Options")]
        public List<InputField> inputFields;

        [BoxGroup("Buttons")]
        public ToggleButton mainButton;
        [BoxGroup("Buttons")]
        public Button rightButton;
        [BoxGroup("Buttons")]
        public Button infoButton;
        [BoxGroup("Buttons")]
        public Button deleteButton;
        [BoxGroup("Buttons")]
        public List<ContentButton> buttons;

        [BoxGroup("Toggles")]
        public Toggle rightToggle;
        [BoxGroup("Toggles")]
        public Toggle leftToggle;

        [BoxGroup("List Options")]
        public Button moveUpButton;
        [BoxGroup("List Options")]
        public Button moveDownButton;

        private LayoutItemData _data;
        public LayoutItemData Data => _data;
        public event Action<bool, LayoutItem> OnMainButtonValueChanged;
        public event Action<string, LayoutItem> OnTitleChanged;
        public event Action<LayoutItem> OnRightButtonClicked;
        public event Action<LayoutItem> OnInfoButtonClicked;
        public event Action<LayoutItem> OnDeleteButtonClicked;
        public event Action<bool, LayoutItem> OnRightToggleValueChanged;
        public event Action<bool, LayoutItem> OnLeftToggleValueChanged;
        public event Action<LayoutItem> OnMoveUpList;
        public event Action<LayoutItem> OnMoveDownList;
        public event Action<ContentButton, LayoutItem> OnButtonPressed;
        public event Action<LayoutItem> OnDoubleClick;

        public event Action<string, LayoutItem> OnInputFieldEndEdit;

        private float lastClickTime;

        private void Start()
        {
            if (titleInputField)
            {
                titleInputField.onSubmit.AddListener(TitleChanged);
            }

            if (mainButton)
            {
                mainButton.OnValueChanged += MainButtonValueChanged;
            }

            if (rightButton)
            {
                rightButton.onClick.AddListener(RightButtonClicked);
            }

            if (infoButton)
            {
                infoButton.onClick.AddListener(InfoButtonClicked);
            }

            if (deleteButton)
            {
                deleteButton.onClick.AddListener(DeleteButtonClicked);
            }

            if (rightToggle)
            {
                rightToggle.onValueChanged.AddListener(RightToggleValueChanged);
            }

            if (leftToggle)
            {
                leftToggle.onValueChanged.AddListener(LeftToggleValueChanged);
            }

            if (moveUpButton)
            {
                moveUpButton.onClick.AddListener(MoveUpList);
            }

            if (moveDownButton)
            {
                moveDownButton.onClick.AddListener(MoveDownList);
            }

            if (buttons != null && buttons.Count > 0)
            {
                for (var i = 0; i < buttons.Count; i++)
                {
                    if (buttons[i] != null)
                    {
                        buttons[i].OnClick += ButtonPressed;
                    }
                }
            }

            if (inputFields != null && inputFields.Count > 0)
            {
                for (var i = 0; i < inputFields.Count; i++)
                {
                    inputFields[i].onEndEdit.AddListener(OnInputEndEdit);
                }
            }
        }

        private void OnInputEndEdit(string text)
        {
            OnInputFieldEndEdit?.Invoke(text,this);
        }

        private void OnDestroy()
        {
            if (addressableLoadedIcon && addressableLoadedIcon.sprite &&
                _data != null && !string.IsNullOrEmpty(_data.addressableIcon))
            {
                addressableLoadedIcon.sprite.Unload(_data.addressableIcon);
            }
        }

        public async void SetLayoutItem(LayoutItemData data)
        {
            _data = data;

            if (titleLabel)
            {
                if (string.IsNullOrEmpty(_data.title))
                {
                    titleLabel.SetActive(false);
                }
                else
                {
                    titleLabel.SetActive(true);
                    titleLabel.text = _data.title;
                }
            }

            if (titleInputField)
            {
                titleInputField.text = _data.title;
            }

            if (subTitleLabel)
            {
                if (string.IsNullOrEmpty(_data.subTitle))
                {
                    subTitleLabel.SetActive(false);
                }
                else
                {
                    subTitleLabel.SetActive(true);
                    subTitleLabel.text = _data.subTitle;
                }
            }

            if (sideIcon)
            {
                if (string.IsNullOrEmpty(_data.sideIconAnimation))
                {
                    sideIcon.SetActive(false);
                }
                else
                {
                    sideIcon.SetActive(true);
                    sideIcon.Play(_data.sideIconAnimation);
                }
            }

            if (moveDownButton && moveUpButton)
            {
                moveUpButton.SetActive(_data.indexInList > 0);
                moveDownButton.SetActive(_data.indexInList < _data.listCount-1);
            }

            if (addressableLoadedIcon && !string.IsNullOrEmpty(_data.addressableIcon))
            {
                addressableLoadedIcon.sprite = await AddressableReferenceLoader.Load(_data.addressableIcon);
            }
        }

        public void ResetMoveButtons(LayoutItemData data)
        {
            _data = data;

            if (moveDownButton && moveUpButton)
            {
                moveUpButton.SetActive(_data.indexInList > 0);
                moveDownButton.SetActive(_data.indexInList < _data.listCount-1);
            }
        }

        private void MainButtonValueChanged(bool value, string content)
        {
            OnMainButtonValueChanged?.Invoke(value, this);
        }

        private void RightButtonClicked()
        {
            OnRightButtonClicked?.Invoke(this);
        }

        private void InfoButtonClicked()
        {
            OnInfoButtonClicked?.Invoke(this);
        }

        private void DeleteButtonClicked()
        {
            OnDeleteButtonClicked?.Invoke(this);
        }

        private void RightToggleValueChanged(bool isOn)
        {
            OnRightToggleValueChanged?.Invoke(isOn, this);
        }

        private void LeftToggleValueChanged(bool isOn)
        {
            OnLeftToggleValueChanged?.Invoke(isOn, this);
        }

        private void TitleChanged(string newTitle)
        {
            OnTitleChanged?.Invoke(newTitle,this);
        }

        private void MoveUpList()
        {
            OnMoveUpList?.Invoke(this);
        }

        private void MoveDownList()
        {
            OnMoveDownList?.Invoke(this);
        }

        private void ButtonPressed(ContentButton button)
        {
            OnButtonPressed?.Invoke(button, this);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.clickTime - lastClickTime <= 0.2f)
            {
                OnDoubleClick?.Invoke(this);
            }

            lastClickTime = eventData.clickTime;
        }
    }
}