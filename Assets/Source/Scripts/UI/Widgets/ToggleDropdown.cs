using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.UI.Widgets
{
    [RequireComponent(typeof(ToggleButton))]
    public class ToggleDropdown : MonoBehaviour
    {
        public GameObject dropdownToShow;
        public Button closeButton;

        private ToggleButton _button;

        private void Start()
        {
            _button = GetComponent<ToggleButton>();
            _button.OnValueChanged += OnToggled;

            if (closeButton)
                closeButton.onClick.AddListener(OnClose);
        }

        private void OnToggled(bool isOn, string content)
        {
            if (dropdownToShow != null)
            {
                dropdownToShow.SetActive(isOn);
            }
        }

        private void OnClose()
        {
            OnToggled(false, string.Empty);
        }
    }
}