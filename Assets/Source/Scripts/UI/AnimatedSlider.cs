using CrossBlitz.Audio;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.UI
{
    [RequireComponent(typeof(Slider))]
    public class AnimatedSlider : MonoBehaviour
    {
        public TextMeshProUGUI valueLabel;
        public float speed = 10;
        public float speed_denominator = 1f;
        public bool capped = false;
        public bool soundOn = false;

        public float Value
        {
            get => Slider.value;
            set
            {
                Slider.value = value;
                valueLabel.text = $" {(int)Slider.value}/{(int)Slider.maxValue}";
            }
        }
        public float TargetValue
        {
            get => m_targetValue;
        }

        private float m_initialValueDistance;
        private float m_targetValue;
        private Slider m_slider;
        private float m_expPitch;

        public Slider Slider
        {
            get
            {
                if (m_slider == null) m_slider = GetComponent<Slider>();
                return m_slider;
            }
        }

        private void Awake()
        {
            m_slider = GetComponent<Slider>();
            m_expPitch = 0f;
        }

        public void SetValues(int value, int minValue, int maxValue)
        {
            Slider.minValue = minValue;
            Slider.maxValue = maxValue;
            Value = value;
        }

        public void SetTargetValue(int targetValue)
        {
            m_targetValue = targetValue;
            m_initialValueDistance = Mathf.Abs(Value - TargetValue);
        }

        private void Update()
        {
            speed = m_initialValueDistance / speed_denominator;
            if (!capped)
            {
                Value = Mathf.MoveTowards(Value, TargetValue, speed * Time.deltaTime);
                if(soundOn)
                {
                    AudioController.PlaySound("Text-LetterC", "SFX", true).setPitch(m_expPitch);
                    m_expPitch += 0.005f;
                }
            }
        }
    }
}