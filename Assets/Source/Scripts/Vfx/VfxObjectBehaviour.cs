using System;
using CrossBlitz.AssetManagement;
using CrossBlitz.Databases;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.Vfx
{
    public class VfxObjectBehaviour : MonoBehaviour
    {
        public Canvas canvas;
        public CanvasGroup canvasGroup;
        public SpriteAnimation animator;

        private RectTransform m_rect;
        private VfxObject m_vfxObject;
        private int m_currentFrame;
        private bool m_isValid;

        public event Action<int> OnFrameChanged;
        public event Action OnComplete;
        public event Action OnLooped;
        public event Action<string, string> OnCustomEvent;
        public event Action<string> OnCreateNewVfxObject;

        public void Play(VfxObject vfxObject)
        {
            m_vfxObject = vfxObject;
            m_isValid = false;
            m_rect = this.RectTransform();

            if (string.IsNullOrEmpty(vfxObject.animator) || vfxObject.animations.Count <= 0)
            {
                Debug.LogError($"Trying to play an object without an animation: {vfxObject.animator}");
                return;
            }
            // get the animator
            var animatorAsset = AddressableReferenceLoader.GetAnimationAsset(vfxObject.animator);

            if (animatorAsset == null)
            {
                Debug.LogError($"Trying to play an object without an animation: {vfxObject.animator}");
                return;
            }

            // set the animator with the animation asset
            animator.animationAsset = animatorAsset;
            animator.UpdateAnimations();

            // play the animation
            var randAnimation = vfxObject.animations[Random.Range(0, vfxObject.animations.Count)];
            animator.Play(randAnimation);

            // keep track of the animation frame

            // when the animation changes frames start an event

            m_isValid = true;
            m_currentFrame = 0;

            HandleFrameEvent();
        }

        public void Update()
        {
            if (!m_isValid) return;
            if (m_currentFrame == animator.CurrentFrame) return;

            var animationData = animator.GetAnimationData(animator.CurrentAnimationName);
            if (animationData == null) return;

            m_currentFrame = animator.CurrentFrame;

            switch (animationData.loop)
            {
                case SpriteAnimationLoopMode.LOOPTOSTART:
                    if (m_currentFrame == 0)
                    {
                        OnLooped?.Invoke();
                    }
                    break;
                 case SpriteAnimationLoopMode.LOOPTOFRAME:
                     if (m_currentFrame == animationData.frameToLoop)
                     {
                         OnLooped?.Invoke();
                     }
                     break;
            }

            HandleFrameEvent();

        }

        private void HandleFrameEvent()
        {
            if (m_vfxObject.frames.Count >= m_currentFrame)
            {
                return;
            }

            var vfxFrame = m_vfxObject.frames[m_currentFrame];

            for (var i = 0; i < vfxFrame.events.Count; i++)
            {
                var e = vfxFrame.events[i];
                switch (e.eventType)
                {
                    case VfxFrameEvent.FrameEventType.Color:
                        animator.image.color = e.color.ToColor(e.alpha);
                        break;
                    case VfxFrameEvent.FrameEventType.Position:
                        m_rect.anchoredPosition = e.position;
                        break;
                    case VfxFrameEvent.FrameEventType.Rotation:
                        m_rect.eulerAngles = e.rotation;
                        break;
                    case VfxFrameEvent.FrameEventType.Scale:
                        m_rect.localScale = (Vector3Int)e.scale;
                        break;
                    case VfxFrameEvent.FrameEventType.CreateObject:
                        OnCreateNewVfxObject?.Invoke(e.objectToCreate);
                        break;
                    case VfxFrameEvent.FrameEventType.CustomEvent:
                        OnCustomEvent?.Invoke(e.customEventName, e.customEventArgs);
                        break;
                    case VfxFrameEvent.FrameEventType.SetSortingLayer:
                        canvas.sortingLayerName = e.sortingLayer;
                        canvas.sortingOrder = e.sortingOrder;
                        break;
                    case VfxFrameEvent.FrameEventType.Destroy:
                        break;
                }
            }
        }
    }
}