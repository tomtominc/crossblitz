using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Dialogue;
using CrossBlitz.InputAPI;
using CrossBlitz.Items.Data;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.Utils;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using TakoBoyStudios;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace CrossBlitz.Shop
{
    public class RecipeViewerMainCards : InputMode
    {
        [BoxGroup("Dialogue")] [TextArea] public List<string> enterDialogue;
        [BoxGroup("Dialogue")] [TextArea] public List<string> selectCardDialogue;
        [BoxGroup("Dialogue")] [TextArea] public List<string> purchaseDialogue;
        [BoxGroup("Dialogue")] [TextArea] public List<string> leaveDialogue;
        [BoxGroup("Dialogue")] [TextArea] public List<string> notEnoughCurrencyDialogue;

        [BoxGroup("Shop Pages")] public PageBehaviour shopPage;
        [BoxGroup("Shop Pages")] public ShopPageItem shopPageItem;
        [BoxGroup("Shop Pages")] public ToggleGroup toggleGroup;

        [BoxGroup("Shop Categories")] public Button cardsCategoryToggle;
        [BoxGroup("Shop Categories")] public FlashWhiteEffect cardCategoryOutline;

        [BoxGroup("Misc")] public CanvasGroup canvasGroup;
        [BoxGroup("Misc")] public RectTransform exitButtonContainer;
        [BoxGroup("Misc")] public Button exitButton;

        [BoxGroup("Animation")] public CanvasGroup background;
        [BoxGroup("Animation/Timings")] public float delayBeforeOpen=0.5f;
        [BoxGroup("Animation/Timings")] public float durationMultiplier = 1;
        [BoxGroup("Animation/Properties")] public Vector3 headerPunchRotationAxis = new Vector3(0,0,1);

        private bool _cardsCategoryOpen=true;
        private int _lastCardsPage;
        private int _lastRelicsPage;

        private ShopPageItem _current;
        private List<string> _itemList;
        private CoroutineHandle openHandle;

        private ClientBookData _bookData;

        private Vector2 shopHeaderOriginalPos;
        private Vector2 shopContainerOriginalPos;
        private Vector2 dawnDollarFundsOriginalPos;

        private float _newDialogueSpeechBubbleSine;

        private bool _init;

        private void Start()
        {



        }

        private void Init()
        {
            if (_init) return;

            _init = true;

            exitButton.onClick.RemoveAllListeners();
            exitButton.onClick.AddListener(OnExitButton);

            cardsCategoryToggle.onClick.RemoveAllListeners();
            cardsCategoryToggle.onClick.AddListener(OnCardsCategoryToggle);

            shopPage.OnPageChanged -= OnPageChanged;
            shopPage.OnPageChanged += OnPageChanged;
        }

        private void OnShopKeeperShowCustomDialogue()
        {

        }

        public IEnumerator<float> Open(List<string> items)
        {
            Init();

            // setup animation
            background.alpha = 0;

            exitButtonContainer.anchoredPosition = new Vector2(exitButtonContainer.anchoredPosition.x, exitButtonContainer.sizeDelta.y);

            gameObject.SetActive(true);
            _itemList = items;

            _bookData = App.FableData.GetCurrentBook();
            RefreshStore(_bookData,true);

            // ------ animation stuff! ------

            yield return Timing.WaitForSeconds(delayBeforeOpen);

            background.DOFade(1, 0.3f * durationMultiplier);


            yield return Timing.WaitForSeconds(0.4f* durationMultiplier);

            exitButtonContainer.DOAnchorPosY(-4, 0.24f).SetEase(Ease.OutBack);

        }

        private IEnumerator<float> Close()
        {
            exitButtonContainer.DOAnchorPosY(exitButtonContainer.sizeDelta.y, 0.24f)
                .SetEase(Ease.InBack);


            yield return Timing.WaitForSeconds(0.4f);

            background.DOFade(0, 0.3f * durationMultiplier);

            // figure out which mode to go to!
            // cutscenes can happen after leaving a shop.. see if phil implements this though.
            FablesInput.Instance.GoToMode(FablesInput.Mode.Explore);

            yield return Timing.WaitForSeconds(0.6f * durationMultiplier);

            gameObject.SetActive(false);
        }

        private void RefreshStore(ClientBookData bookData, bool resetItems=false)
        {
            _bookData = bookData;

            var shownItems = new List<ClientStoreItemData>();

            for (var i = 0; i < _bookData.StoreData.Items.Count; i++)
            {
                var item = _bookData.StoreData.Items[i];
                var card = Db.CardDatabase.GetCard(item.ItemId);

                if (_cardsCategoryOpen)
                {
                    if (card.type == CardType.Relic) continue;
                }
                else
                {
                    if (card.type != CardType.Relic) continue;
                }
            }

            var pageToDisplay = _cardsCategoryOpen ? _lastCardsPage : _lastRelicsPage;

            shopPage.InitializePages(shopPageItem, 4, shownItems.Cast<IPageItemData>().ToList(), pageToDisplay);

            for (var i = 0; i < shopPage.Items.Count; i++)
            {
                var item = (ShopPageItem)shopPage.Items[i];
                item.itemToggle.Toggle.group = toggleGroup;
                item.OnClicked += OnShopItemSelected;
            }
        }

        private void OnShopItemSelected(bool isOn, ShopPageItem itemClicked)
        {
            _current = null;

            for (var i = 0; i < shopPage.Items.Count; i++)
            {
                var item = (ShopPageItem)shopPage.Items[i];

                if (item.itemToggle.Toggle.isOn)
                {

                    _current = item;
                }
            }
        }


        private void OnPageChanged(int page)
        {
            if (_cardsCategoryOpen) _lastCardsPage = page;
            else _lastRelicsPage = page;
        }

        private void OnCardsCategoryToggle()
        {
            if (!_cardsCategoryOpen)
            {
                AnimateButtonAndFlash(cardsCategoryToggle, cardCategoryOutline);
                RefreshStore(_bookData);
            }
        }


        private void AnimateButtonAndFlash(Button button, FlashWhiteEffect effect)
        {
            effect.SetActive(true);
            effect.Flash();

            effect.RectTransform().DOKill();
            effect.RectTransform().localScale = new Vector3(0.4f, 0.4f, 1f);
            effect.RectTransform().DOScale(1, 0.4f).SetEase(Ease.OutElastic);

            button.RectTransform().DOKill();
            button.RectTransform().DOPunchAnchorPos(Vector2.up * 2f, 0.24f);
        }


        private void OnExitButton()
        {
            //todo: check for cutscenes and move into a cutscene if needed!

            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = true;

            Timing.RunCoroutine(Close());
        }

        public override void StartMode()
        {
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;
        }

        public override void UpdateMode()
        {

        }

        public override void EndMode()
        {
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
        }
    }
}