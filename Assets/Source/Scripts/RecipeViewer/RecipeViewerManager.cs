using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.PlayFab.Authentication;
using DG.Tweening;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using CrossBlitz.Utils;
using RenderMode = UnityEngine.RenderMode;

namespace CrossBlitz.DeckEditAPI
{
    public class RecipeViewerManager : MonoBehaviour, ISceneConfiguration
    {
        public int RecipeTestIndex = -1;

        private List<DeckData> Decks;
        private List<CardValue> mainCards;
        private List<CardValue> deckList;
        private List<CardValue> duplicateList;

        public GameObject mainCardsContainer;
        public GameObject playGuideContainer;
        public GameObject cardListContainer;

        public Canvas canvas;
        public CanvasGroup canvasContainer;

        public Canvas topButtonsCanvas;
        public CanvasGroup topButtonsCanvasGroup;

        public Button mainCardsButton;
        public Button playGuideButton;
        public Button cardListButton;
        public Button exitButton;

        public PageBehaviour mainCardsPage;
        public RecipeViewerCard mainCardsItem;
        public RecipeViewerCard blitzBurstCard;
        public RectTransform cardListLayout;
        public CardDeckViewItem cardListItem;
        public ScrollRect cardScrollRectangle;

        public SpriteAnimation heroPortrait;
        public SpriteAnimation heroBanner;
        public SpriteAnimation heroEmblem;

        public TextMeshProUGUI heroNameLabel;
        public TextMeshProUGUI deckNameLabel;
        public TextMeshProUGUI deckTypeLabel;
        public TextMeshProUGUI blitzBurstLabel;
        public TextMeshProUGUI overviewLabel;

        public TextMeshProUGUI playGuideLabel;

        public GameObject MissingCardsObject;

        private int _lastCardsPage;
        public float delayBeforeOpen = 0.05f;
        private bool m_opened;

        public event Action OnClosed;

        // Start is called before the first frame update
        void Start()
        {
            mainCardsButton.interactable = true;
            mainCardsButton.onClick.AddListener(OnMainCards);

            playGuideButton.interactable = true;
            playGuideButton.onClick.AddListener(OnPlayGuide);

            cardListButton.interactable = true;
            cardListButton.onClick.AddListener(OnCardListButton);

            exitButton.interactable = true;
            exitButton.onClick.AddListener(OnExitButton);

            duplicateList = new List<CardValue>();

            canvasContainer.alpha = 0;

            if (RecipeTestIndex >= 0 && RecipeTestIndex < Db.DeckRecipeDatabase.deckRecipes.Count && !m_opened)
            {
                // Get a deck recipe & Open for testing
                var recipe = Db.DeckRecipeDatabase.deckRecipes[RecipeTestIndex];
                Open(recipe);
            }
        }

        public void SetAsOverlay()
        {
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            topButtonsCanvas.renderMode = RenderMode.ScreenSpaceOverlay;
        }

        //public IEnumerator<float> Open(DeckRecipeData deckRecipeData)
        public void Open(DeckRecipeData deckRecipeData)
        {
            m_opened = true;

            // setup animation
            // background.alpha = 0;
            // exitButtonContainer.anchoredPosition = new Vector2(exitButtonContainer.anchoredPosition.x, exitButtonContainer.sizeDelta.y);

            // Retrieve label information from the deck recipe
            deckNameLabel.text = deckRecipeData.deckData.name.ToUpper();
            deckTypeLabel.text = deckRecipeData.deckType.ToString().ToUpper();
            overviewLabel.text = deckRecipeData.overview;
            playGuideLabel.text = deckRecipeData.howToPlayGuide;

            // Set changing Banner and Portrait icon
            SetBanner(deckRecipeData.deckData.faction.ToString().ToLower());

            // Retrieve deck information from the deck recipe
            mainCards = deckRecipeData.mainCards;
            deckList = deckRecipeData.deckData.cards;

            RefreshViewerNew(mainCards, deckList, duplicateList);

            // have main cards open first.
            OnMainCards();

            // ------ animation stuff! ------
            Timing.RunCoroutine(AnimateIn());

        }

        private IEnumerator<float> AnimateIn()
        {
            yield return Timing.WaitForSeconds(delayBeforeOpen);
            canvasContainer.DOFade(1, 0.2f);
            yield return Timing.WaitForSeconds(0.4f);
            //exitButton.RectTransform().DOAnchorPosY(-4, 0.24f).SetEase(Ease.OutBack);
        }

        private IEnumerator<float> AnimateOut()
        {
            OnClosed?.Invoke();
            canvasContainer.DOFade(0, 0.4f);
            topButtonsCanvasGroup.DOFade(0, 0.4f);
            yield return Timing.WaitForSeconds(0.4f);
            CloseScene();
        }

        private async void CloseScene()
        {
            await SceneController.Instance.UnloadScene("RecipeViewer");
        }

        private void RefreshViewerNew(List<CardValue> mainCards, List<CardValue> deckList, List<CardValue> duplicateList)
        {
            var shownCards = new List<RecipeViewerCardData>();

            // The Main Cards
            for (var i = 0; i < mainCards.Count; i++)
            {
                //var card = deckData.cards[i];
                var card = mainCards[i];
                var cardData = new RecipeViewerCardData();
                cardData.cardvalue = card;
                shownCards.Add(cardData);
            }

            // Get Owned Cards
            var ownedCardList = new List<CardValue>();
            var missingCardList = new List<CardValue>();
            for (var i = 0; i < deckList.Count; i++)
            {
                var cardData = Db.CardDatabase.GetCard(deckList[i].id);

                if (cardData == null) continue;

                var cardOwnedAmount = App.Inventory.GetItemAmount(cardData.id);
                var cardNeededAmount = deckList[i].count;
                var cardMissingAmount = 0;

                if (cardOwnedAmount >= cardNeededAmount) cardOwnedAmount = cardNeededAmount;
                else cardMissingAmount = cardNeededAmount - cardOwnedAmount;


                if (cardOwnedAmount >0) ownedCardList.Add(deckList[i]);

                if (cardMissingAmount > 0) missingCardList.Add(deckList[i]);

                //for (var j = 0; j < cardOwnedAmount; j++)
                //{
                //    ownedCardList.Add(deckList[i]);
                //}

                //for (var y = 0; y < cardMissingAmount; y++)
                //{
                //    missingCardList.Add(deckList[i]);
                //}
            }

            // The Card List
            deckList = deckList.OrderBy(deckCard => Db.CardDatabase.GetCard(deckCard.id).type == CardType.Power).ThenBy(deckCard => Db.CardDatabase.GetCard(deckCard.id).cost).ToList();


            for (var i = 0; i < ownedCardList.Count; i++)
            {
                var card_id = ownedCardList[i].id;
                var cardListData = Db.CardDatabase.GetCard(card_id);
                var cardListViewObject = Instantiate(cardListItem, cardListLayout, false);

                //If creating a deck recipe
                var cardOwnedAmount = 0;
                var cardNeededAmount = 0;
                var cardMissingAmount = 0;
                var deckRecipieCount = 0;

                cardOwnedAmount = App.Inventory.GetItemAmount(card_id);
                cardNeededAmount = deckList.Find(card => card.id == card_id).count;

                if (cardOwnedAmount >= cardNeededAmount) cardOwnedAmount = cardNeededAmount;
                else cardMissingAmount = cardNeededAmount - cardOwnedAmount;

                deckRecipieCount = cardOwnedAmount;

                var hoverCard = cardListViewObject.gameObject.AddComponent<HoverCard>();
                hoverCard.Initialize(cardListData, cardListViewObject.transform, 60);
                hoverCard.ForceToolTipsLeft = true;

                var dragCard = cardListViewObject.gameObject.AddComponent<DragCard>();

                if (dragCard)
                {
                    dragCard.SetIsInDeck(true);
                    dragCard.SetScrollRect(cardScrollRectangle);
                }

                cardListViewObject.Load(ownedCardList[i], true, deckRecipieCount, false);
                cardListViewObject.SetActive(true);

                // Set the Blitz Burst Card
                if (cardListData.type == CardType.Power)
                {
                    var blitzCardData = new RecipeViewerCardData();
                    blitzCardData.cardvalue = ownedCardList[i];
                    blitzBurstCard.SetData(blitzCardData, 0);
                    blitzBurstLabel.text = cardListData.name.ToUpper();
                }
            }

            if(missingCardList.Count >0) Instantiate(MissingCardsObject, cardListLayout, false);

            for (var i = 0; i < missingCardList.Count; i++)
            {
                var card_id = missingCardList[i].id;
                var cardListData = Db.CardDatabase.GetCard(card_id);
                var cardListViewObject = Instantiate(cardListItem, cardListLayout, false);

                //If creating a deck recipe
                var cardOwnedAmount = 0;
                var cardNeededAmount = 0;
                var cardMissingAmount = 0;
                var deckRecipieCount = 0;

                cardOwnedAmount = App.Inventory.GetItemAmount(card_id);
                cardNeededAmount = deckList.Find(card => card.id == card_id).count;

                if (cardOwnedAmount >= cardNeededAmount) cardOwnedAmount = cardNeededAmount;
                else cardMissingAmount = cardNeededAmount - cardOwnedAmount;

                deckRecipieCount = cardMissingAmount;

                var locationCard = cardListViewObject.gameObject.AddComponent<CardLocationComponent>();
                locationCard.Initialize(cardListData, cardListViewObject.gameObject.transform);

                var dragCard = cardListViewObject.gameObject.AddComponent<DragCard>();

                if (dragCard)
                {
                    dragCard.SetIsInDeck(true);
                    dragCard.SetScrollRect(cardScrollRectangle);
                }

                cardListViewObject.Load(missingCardList[i], true, deckRecipieCount, true);
                cardListViewObject.SetActive(true);

                // Set the Blitz Burst Card
                if (cardListData.type == CardType.Power)
                {
                    var blitzCardData = new RecipeViewerCardData();
                    blitzCardData.cardvalue = missingCardList[i];
                    blitzBurstCard.SetData(blitzCardData, 0);
                    blitzBurstLabel.text = cardListData.name.ToUpper();
                }
            }

            var pageToDisplay = _lastCardsPage;
            mainCardsPage.InitializePages(mainCardsItem, 4, shownCards.Cast<IPageItemData>().ToList(), pageToDisplay);
        }

        private void RefreshViewer(List<CardValue> mainCards, List<CardValue> deckList, List<CardValue> duplicateList)
        {
            var shownCards = new List<RecipeViewerCardData>();

            // The Main Cards
            for (var i = 0; i < mainCards.Count; i++)
            {
                //var card = deckData.cards[i];
                var card = mainCards[i];
                var cardData = new RecipeViewerCardData();
                cardData.cardvalue = card;
                shownCards.Add(cardData);
            }

            // The Card List
            deckList = deckList.OrderBy(deckCard => Db.CardDatabase.GetCard(deckCard.id).type == CardType.Power).ThenBy(deckCard => Db.CardDatabase.GetCard(deckCard.id).cost).ToList();

            for (var i = 0; i < deckList.Count; i++)
            {
                var card_id = deckList[i].id;
                var cardListData = Db.CardDatabase.GetCard(card_id);
                var cardListViewObject = Instantiate(cardListItem, cardListLayout, false);

                var hoverCard = cardListViewObject.gameObject.AddComponent<HoverCard>();
                hoverCard.Initialize(cardListData, cardListViewObject.transform, 60);
                hoverCard.ForceToolTipsLeft = true;

                var dragCard = cardListViewObject.gameObject.AddComponent<DragCard>();

                if (dragCard)
                {
                    dragCard.SetIsInDeck(true);
                    dragCard.SetScrollRect(cardScrollRectangle);
                }

                cardListViewObject.Load(deckList[i], false);
                cardListViewObject.SetActive(true);

                // Dictate if duplicate
                // var duplicateCard = duplicateList.Find(card => card.id == card_id);
                // if (duplicateCard != null)
                // {
                //     //Debug.Log("FOUND DUPLICATE CARD");
                //     cardListViewObject.AnimateDoubleCard(true);
                // }
                // duplicateList.Add(deckList[i]);
                //
                // //Debug.Log("CARD ID: " + card_id);
                // for (var x = 0; x < duplicateList.Count; x++)
                // {
                //   //  Debug.Log("DUPLICATE LIST " + x + ": " +duplicateList[x].id);
                // }

                // Set the Blitz Burst Card
                if (cardListData.type == CardType.Power)
                {
                    var blitzCardData = new RecipeViewerCardData();
                    blitzCardData.cardvalue = deckList[i];
                    blitzBurstCard.SetData(blitzCardData, 0);
                    blitzBurstLabel.text = cardListData.name.ToUpper();
                }
            }

            var pageToDisplay =  _lastCardsPage;
            mainCardsPage.InitializePages(mainCardsItem, 4, shownCards.Cast<IPageItemData>().ToList(), pageToDisplay);
        }


        private void SetBanner(String deckType)
        {
            heroBanner.Play(deckType);
            var deckName = "";

            switch(deckType)
            {
                case "balance":
                    deckName = "seto";
                    break;

                case "chaos":
                    deckName = "violet";
                    break;

                case "fortune":
                    deckName = "quill";
                    break;

                case "nature":
                    deckName = "mereena";
                    break;

                case "war":
                    deckName = "redcroft";
                    break;
            }

            heroPortrait.Play(deckName);
            heroEmblem.Play(deckType.ToLower());
            heroNameLabel.text = deckName.ToUpper();

        }

        private void OnMainCards()
        {
            mainCardsContainer.SetActive(true);
            playGuideContainer.SetActive(false);
            cardListContainer.SetActive(false);

            //mainCardsButton.interactable = false;
            //playGuideButton.interactable = true;
            //cardListButton.interactable = true;

            //Debug.Log("MAIN CARDS BUTTON");
        }

        private void OnPlayGuide()
        {
            mainCardsContainer.SetActive(false);
            playGuideContainer.SetActive(true);
            cardListContainer.SetActive(false);

            //mainCardsButton.interactable = true;
            //playGuideButton.interactable = false;
            //cardListButton.interactable = true;
            //Debug.Log("PLAY GUIDE BUTTON");
        }

        private void OnCardListButton()
        {
            mainCardsContainer.SetActive(false);
            playGuideContainer.SetActive(false);
            cardListContainer.SetActive(true);

            //mainCardsButton.interactable = true;
            //playGuideButton.interactable = true;
            //cardListButton.interactable = false;
            //Debug.Log("CARD LIST BUTTON");
        }

        private async void OnExitButton()
        {
            mainCardsButton.interactable = false;
            playGuideButton.interactable = false;
            cardListButton.interactable = false;
            exitButton.interactable = false;

            Timing.RunCoroutine(AnimateOut());
        }

        public Task OnCreated(SceneModel sceneModel)
        {
            return Task.CompletedTask;
        }

        public void OnFocused(bool hasFocus)
        {

        }

        public void OnSceneLoaded(SceneModel model)
        {

        }

        public void OnSceneUnLoaded(SceneModel model)
        {

        }
    }
}
