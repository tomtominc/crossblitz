using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CrossBlitz.Utils;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using System;

namespace CrossBlitz
{
    public class RecipeViewerCardList : PageItem
    {
        public RectTransform cardParentTransform;
        public GameObject cardStandardPrefab;

        private CardView m_cardStandard;

        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        public override void SetAsEmpty(int itemIndex)
        {
            if(m_cardStandard != null)
            {
                m_cardStandard.SetActive(false);
            }

        }
        public override void SetData(IPageItemData data, int itemIndex)
        {
            if (!Db.Initialized)
            { 
                Db.Initialize();
            }

            var recipieViewerCardDataList = (RecipeViewerCardDataList)data;
            var cardData = Db.CardDatabase.GetCard(recipieViewerCardDataList.cardvalue.id);

            if (m_cardStandard == null)
            {
                var settings = new CreateCardFactorySettings
                {
                    CardPrefab = cardStandardPrefab,
                    Layout = cardParentTransform,
                    AdditionalComponents = new List<Type>
                        {typeof(HoverCard)},
                    StartsDisabled = true
                };

                settings.CardData = cardData;

                m_cardStandard = CardFactory.Create(settings, null);
            }
            else
            {
                m_cardStandard.SetCardData(cardData);
            }

            m_cardStandard.SetActive(true);
        }
    }

    public class RecipeViewerCardDataList: IPageItemData
    {
        public CardValue cardvalue;
    }
}
