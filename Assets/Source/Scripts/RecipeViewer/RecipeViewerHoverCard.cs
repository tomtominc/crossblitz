using System;
using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Cursors;
using CrossBlitz.GameVfx;
using CrossBlitz.Plugins.TakoBoyStudios.Utilities;
using DG.Tweening;
using Source.Scripts.Card;
using UnityEngine;
using UnityEngine.EventSystems;
using Events = CrossBlitz.ClientAPI.GameLogic.EventSystem.Events;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz
{
    public class RecipeViewerHoverCard : MonoBehaviour,
        ICardComponent,
        IPointerEnterHandler,
        IPointerExitHandler,
        IPointerClickHandler
    {
        public void Initialize(CardView card)
        {
            
        }

        public void OnAfterCreation()
        {
            
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            
        }

        public void OnPointerExit(PointerEventData eventData)
        {
           
        }

        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
