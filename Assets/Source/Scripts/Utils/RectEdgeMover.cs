using System.Collections.Generic;
using UnityEngine;

namespace CrossBlitz.Utils
{
    public class RectEdgeMover : MonoBehaviour
    {
        public RectTransform rectEdge;
        public RectTransform rect;
        public int startingCorner;
        public float speed = 30;

        private int _currentCorner;
        private Vector3[] _rectCorners;

        private void OnEnable()
        {
            _rectCorners=new Vector3[4];

            rectEdge.GetLocalCorners(_rectCorners);

            _currentCorner = startingCorner;
            rect.anchoredPosition = _rectCorners[_currentCorner];
        }

        private void Update()
        {
            rect.anchoredPosition = Vector2.MoveTowards(
                rect.anchoredPosition, _rectCorners[_currentCorner], speed*Time.deltaTime );

            if (Vector2.Distance(rect.anchoredPosition, _rectCorners[_currentCorner]) <= 1f)
            {
                rect.anchoredPosition = _rectCorners[_currentCorner];
                _currentCorner++;

                if (_currentCorner >= _rectCorners.Length)
                {
                    _currentCorner = 0;
                }
            }
        }
    }
}