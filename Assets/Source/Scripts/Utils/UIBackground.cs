﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIBackground : MonoBehaviour
{
    public bool resetMaterialOffsetDuringPlayMode = true;

    [SerializeField]
    protected bool m_scroll;
    [SerializeField]
    public float m_speed;
    [SerializeField]
    protected Vector2 m_direction;

    protected Material m_materialRef;

    private bool _materialLoaded;

    protected Material m_material
    {
        get
        {
            if (m_materialRef == null)
            {
                Image image = GetComponent<Image>();
                SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();

                if (null != image)
                    m_materialRef = image.material;

                if (null != spriteRenderer)
                    m_materialRef = spriteRenderer.material;
            }

            return m_materialRef;
        }
    }

    private void OnEnable()
    {
        resetBackground();
    }

    private void OnDisable()
    {
        resetBackground();
    }

    public void fade(float p_from, float p_to, float p_duration)
    {
        Color fromColor = m_material.GetColor("_Color");
        fromColor.a = p_from;
        m_material.SetColor("_Color", fromColor);
        m_material.DOFade(p_to, p_duration);
    }

    private void Update()
    {
        if (!_materialLoaded)
        {
            resetBackground();
        }
        else if (m_scroll)
        {
            scrollBackground(m_direction * m_speed * Time.deltaTime);
        }
    }

    public void scrollBackground(Vector2 p_velocity)
    {
        m_material.mainTextureOffset += p_velocity;

        Vector2 offset = m_material.mainTextureOffset;

        if (offset.x <= -1)
        {
            offset.x = 0f;
        }

        if (offset.x >= 1)
        {
            offset.x = 0;
        }

        if (offset.y <= -1)
        {
            offset.y = 0f;
        }

        if (offset.y >= 1)
        {
            offset.y = 0;
        }

        m_material.mainTextureOffset = offset;
    }

    public void resetBackground()
    {
        if(!Application.isPlaying || resetMaterialOffsetDuringPlayMode)
        {
            m_material.mainTextureOffset = Vector2.zero;
        }
        _materialLoaded = m_material != null;
    }
}
