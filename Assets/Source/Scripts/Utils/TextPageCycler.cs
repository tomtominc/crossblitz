using TMPro;
using UnityEngine;

namespace CrossBlitz
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class TextPageCycler : MonoBehaviour
    {
        private const float CycleDelay = 5f;
        private TextMeshProUGUI m_text;
        private float m_cycleTimer;
        private int m_currentPage;
        private int m_maxPages;

        // Start is called before the first frame update
        void Start()
        {
            m_text = GetComponent<TextMeshProUGUI>();
            m_maxPages = m_text.textInfo.pageCount+1;
            m_currentPage = 1;
            m_text.pageToDisplay = m_currentPage;
        }

        // Update is called once per frame
        void Update()
        {
            m_cycleTimer += Time.deltaTime;
            if (m_cycleTimer >= CycleDelay)
            {
                m_cycleTimer = 0;
                m_currentPage++;
                if (m_currentPage > m_maxPages) m_currentPage = 1;
                m_text.pageToDisplay = m_currentPage;
            }
        }
    }
}
