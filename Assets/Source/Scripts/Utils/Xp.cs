using CrossBlitz.Fables.Battle;
using UnityEngine;

namespace CrossBlitz.Utils
{
    public static class Xp
    {
        public static int GetXpForLevel(int level)
        {
            return Mathf.CeilToInt((6 / 5f * Mathf.Pow(level, 3)) - (15 * Mathf.Pow(level, 2)) + (100 * level) - 140);
        }

        public static int GetXpGainedFromEnemy(BattleType battleType, int level, bool firstTime, int chapterNumber = 1)
        {
            var multiplier = (firstTime ? battleType == BattleType.Boss ? 3f : 2f : 1f) * (chapterNumber*2-1);
            return Mathf.CeilToInt((multiplier * 64 * level) / 4f);
        }

        public static int GetGainedLevelsFromXp(int currentLevel, int totalXp, out int xpLeft)
        {
            var levelsGained = 0;
            var nextLevel = currentLevel + 1;
            var tryLevelUp = true;

            xpLeft = totalXp;

            if (currentLevel == 30)
            {
                Debug.Log("MAX LEVEL REACHED!");
                tryLevelUp = false;
                totalXp = 0;
                xpLeft = 0;
            }

            while (tryLevelUp)
            {
                var xpForNextLevel = GetXpForLevel(nextLevel);
                var xpAfterLevelGain = totalXp - xpForNextLevel;
                if(nextLevel == 31)
                {
                    Debug.Log("MAX LEVEL REACHED!");
                    tryLevelUp = false;
                    xpLeft = 0;
                }
                else if (xpAfterLevelGain >= 0)
                {
                    levelsGained++;
                    nextLevel++;
                    totalXp = xpAfterLevelGain;
                }
                else
                {
                    tryLevelUp = false;
                    xpLeft = totalXp;
                }
            }

            return levelsGained;
        }
    }
}