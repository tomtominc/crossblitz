using UnityEngine;

namespace CrossBlitz.Utils
{
    public abstract class PageItem : MonoBehaviour
    {
        public abstract void SetAsEmpty(int itemIndex);
        public abstract void SetData(IPageItemData data, int itemIndex);
    }
}