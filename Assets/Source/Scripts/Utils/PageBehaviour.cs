using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Utils
{
    public class PageBehaviour : MonoBehaviour
    {
        public TextMeshProUGUI pageNumber;
        public RectTransform pageLayout;
        public Button previousButton;
        public Button nextButton;

        private int _currentPage;
        private int _maxNumItems;
        private List<IPageItemData> _data;
        private List<PageItem> _items;

        public event Action<int> OnPageChanged;

        public List<PageItem> Items => _items;
        public int CurrentPage => _currentPage;

        private void Start()
        {
            if (previousButton) previousButton.onClick.AddListener(OnPreviousButton);
            if (nextButton) nextButton.onClick.AddListener(OnNextButton);
        }

        public void InitializePages(PageItem prefab, int maxNumItems, List<IPageItemData> data, int setPage = 0)
        {
            pageLayout.DestroyChildren();

            _data = data;
            _items = new List<PageItem>();
            _maxNumItems = maxNumItems;

            for (var i = 0; i < _maxNumItems; i++)
            {
                var item = Instantiate(prefab, pageLayout, false);
                _items.Add(item);
            }

            GoToPage(setPage);
        }

        public void ChangeData(List<IPageItemData> data, int setPage = -1)
        {
            _data = data;
            GoToPage(setPage > -1 ? setPage : _currentPage);
        }

        private void GoToPage(int page)
        {
            _currentPage = page;

            var startingIndex = _maxNumItems * _currentPage;

            for (var i = startingIndex; i < _maxNumItems + startingIndex; i++)
            {
                if (i - startingIndex >= _items.Count)
                {
                    Debug.LogError($"Index={i-startingIndex} Count={_items.Count}");
                    continue;
                }

                if (_data.Count > i)
                {
                    _items[i-startingIndex].SetData(_data[i], i-startingIndex);
                }
                else
                {
                    _items[i-startingIndex].SetAsEmpty(i-startingIndex);
                }
            }

            RefreshButtons();

            OnPageChanged?.Invoke(_currentPage);
        }

        private void RefreshButtons()
        {
            if (pageNumber)
            {
                var pages = GetMaxNumberOfPages();
                pages = pages <= 0 ? 1 : pages;
                pageNumber.text = $"{_currentPage + 1}/{pages}";
            }

            if (nextButton)
            {
                nextButton.SetActive(_currentPage + 1 < GetMaxNumberOfPages());
            }

            if (previousButton)
            {
                previousButton.SetActive(_currentPage > 0);
            }
        }

        public int GetMaxNumberOfPages()
        {
            if (_data == null || _data.Count <= 0) return 0;
            return _data.Count / _maxNumItems + (_data.Count % _maxNumItems > 0 ? 1 : 0);
        }

        public void OnPreviousButton()
        {
            if (_currentPage > 0)
            {
                GoToPage(_currentPage - 1);
            }
        }

        public void OnNextButton()
        {
            if (_currentPage + 1 < GetMaxNumberOfPages())
            {
                GoToPage(_currentPage + 1);
            }
        }
    }
}