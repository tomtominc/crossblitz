using UnityEngine;

namespace Source.Scripts.Utils.ParticleSystem
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(UnityEngine.ParticleSystem))]
    public class ParticleSystemChangeRowAfterTime : MonoBehaviour
    {
        public float changeRowAtRemainingTime;
        public int changeToRow;

        private UnityEngine.ParticleSystem _ps;
        private UnityEngine.ParticleSystem.Particle[] _particles;

        private void Update()
        {
            if (_ps == null)
            {
                _ps = GetComponent<UnityEngine.ParticleSystem>();
            }

            _particles = new UnityEngine.ParticleSystem.Particle[_ps.particleCount];

            for (var i = 0; i < _ps.GetParticles(_particles); i++)
            {
                var particle = _particles[i];

                if (particle.remainingLifetime <= changeRowAtRemainingTime)
                {
                }
            }
        }
    }
}
