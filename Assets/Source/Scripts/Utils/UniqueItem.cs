using CrossBlitz.PlayFab.Data;
using CrossBlitz.ServerAPI;
using Newtonsoft.Json;
using Sirenix.OdinInspector;

namespace CrossBlitz.Utils
{
    [System.Serializable]
    public class UniqueItem
    {
        [ReadOnly]
        public string Uid;

        public virtual void UpdateIdentifier()
        {
            Uid = GUID.CreateUnique();
        }
        //#if UNITY_EDITOR
        public virtual string ToJson(bool shallow)
        {
            if (shallow)
            {
                return JsonConvert.SerializeObject(this);
            }

            return JsonConvert.SerializeObject(this,Server.JsonSettings);

        }

        public static T FromJson<T>(string json, bool shallow) where T : UniqueItem
        {
            if (shallow)
            {
                return JsonConvert.DeserializeObject<T>(json);
            }

            return JsonConvert.DeserializeObject<T>(json, Server.JsonSettings);
        }
        //#endif
    }
}