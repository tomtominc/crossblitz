using System;

namespace CrossBlitz.Utils
{
    public static class ExcelUtils
    {
        // public static void ExportToExcel(string filePath, string sheet, int row,  )
        // {
        //
        // }

        public static string GetExcelColumnName(int columnNumber)
        {
            string columnName = "";

            if (columnNumber < 0) columnNumber = 0;

            columnNumber++;

            while (columnNumber > 0)
            {
                int modulo = (columnNumber-1) % 26;
                columnName = Convert.ToChar('A' + modulo) + columnName;
                columnNumber = (columnNumber - modulo) / 26;
            }

            return columnName;
        }
    }
}