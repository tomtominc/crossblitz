using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz
{
    public class CrossBlitzTwitterGifCreator : MonoBehaviour
    {
        public float yOffset = 16f;
        public float duration = 1f;
        public float shadowFollowDelay = 0.04f;
        public Ease easeType = Ease.OutQuint;
        public RectTransform rect;
        public RectTransform shadowRect;
        public float backgroundSpeed;
        public Image background;
        private Material m_materialRef;

        protected Material m_material
        {
            get
            {
                if (m_materialRef == null)
                {
                    m_materialRef = background.material;
                }

                return m_materialRef;
            }
        }

        private void Start()
        {
            rect.DOAnchorPosY(yOffset, duration).SetEase(easeType).SetLoops(-1, LoopType.Yoyo);
            shadowRect.DOAnchorPosY(yOffset -4, duration).SetDelay(shadowFollowDelay).SetEase(easeType).SetLoops(-1, LoopType.Yoyo);

            rect.DOScale(new Vector3(.9f, 1.1f, 1f), duration).SetLoops(-1, LoopType.Yoyo);
            shadowRect.DOScale(new Vector3(.9f, 1.1f, 1f), duration).SetDelay(shadowFollowDelay).SetLoops(-1, LoopType.Yoyo);
        }

        private void Update()
        {
            Vector2 offset = m_material.mainTextureOffset;
            offset = Vector2.MoveTowards(offset, new Vector2(-1, 1), backgroundSpeed * Time.deltaTime);

            if (offset.x <= -1)
            {
                offset.x = 0f;
            }

            if (offset.x >= 1)
            {
                offset.x = 0;
            }

            if (offset.y <= -1)
            {
                offset.y = 0f;
            }

            if (offset.y >= 1)
            {
                offset.y = 0;
            }

            m_material.mainTextureOffset = offset;
        }
    }
}