using UnityEngine;
using System;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonEvents : MonoBehaviour
{
    public event Action<string> OnClick = delegate { };
    private Button _button;

    private void Start()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(Clicked);
    }

    private void Clicked()
    {
        OnClick(name);
    }
}
