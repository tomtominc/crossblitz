﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
[ExecuteInEditMode]
public class CopyText : MonoBehaviour
{
    public TextMeshProUGUI copy;

    private TextMeshProUGUI label;

    public void Update()
    {
        if (copy == null)
            return;

        if (label == null)
            label = GetComponent<TextMeshProUGUI>();

        if (label == null)
            return;

        label.text = copy.text;
    }
}
