﻿using System;
using CrossBlitz.ServerAPI;
using CrossBlitz.Transition;
using Source.Scripts.AssetManagement;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Source.Scripts.Utils
{
    public class AssetBundleUIAttachment : MonoBehaviour, IAssetLoader
    {
        public AssetReference reference;
        [HideInInspector]
        public GameObject referenceObject;
        public string AssetName => name;
        private bool _isBusy = true;
        public bool IsBusy => _isBusy;

        private void Start()
        {
            OpenAttachment();
            // TransitionController.AssetsToWaitFor.Add(this);
            // TransitionController.AssetsToUnload.Add(this);
        }

        private async void OpenAttachment()
        {
            _isBusy = true;

            try
            {
                referenceObject = await reference.InstantiateAsync(transform, false).Task;

                var windowRect = referenceObject.GetComponent<RectTransform>();
                windowRect.anchoredPosition = Vector2.zero;

                referenceObject.SetActive(true);
            }
            catch (Exception ex)
            {
                Debug.LogErrorFormat("Could not load attachment: {0}\n{1}", reference, ex.Message );
                throw;
            }

            _isBusy = false;
        }

        public void Unload()
        {
            Addressables.ReleaseInstance(referenceObject);

            if (reference.Asset !=null)
                reference.ReleaseAsset();

            Destroy(referenceObject);
        }
    }
}
