using UnityEditor;
using UnityEngine;

namespace CrossBlitz.Source.Scripts.Utils.Editor
{
    public class SpriteImporter : AssetPostprocessor // AssetPostprocessor gives us a bunch of tools we can use to modifiy assets on import
    {
        // This hooks in to texture Preprocessing. that means it happens *before* the texture is processed by unity so we can fiddle with its import settings!
        // This method will be run on ever asset as it is imported. For example, whenever a new png file is added to the project or if you right click -> reimport on a texture asset.
        void OnPreprocessTexture()
        {
            // if (assetPath.Contains("Source/Artwork")) // Optional filtering. We can set it to only effect files with certain keywords in the path. Uncomment if useful
            // {
            //     TextureImporter textureImporter = (TextureImporter)assetImporter;
            //
            //     if (textureImporter.textureType != TextureImporterType.Sprite)
            //     {
            //         textureImporter.textureType = TextureImporterType.Sprite;
            //         textureImporter.spriteImportMode = SpriteImportMode.Single;
            //
            //         textureImporter.mipmapEnabled = false;
            //
            //         textureImporter.filterMode = FilterMode.Point;
            //         textureImporter.spritePixelsPerUnit = 32;
            //
            //         textureImporter.textureCompression =TextureImporterCompression.Uncompressed;
            //     }
            // }
        }
    }
}
