﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CopyCanvas : MonoBehaviour
{
    public Canvas copy;
    public int offset;

    private Canvas canvas;
    public void Update()
    {
        if (copy == null)
            return;

        if (canvas == null)
            canvas = GetComponent<Canvas>();

        if (canvas == null)
            return;

        canvas.sortingOrder = copy.sortingOrder + offset;
        canvas.sortingLayerName = copy.sortingLayerName;
    }
}
