using System.Collections.Generic;
using CrossBlitz.PlayFab.Authentication;

public static class GUID
{
    public static List<int> guidInts;
    private static List<string> guids;

    public static void AddGuids(List<string> guidList)
    {
        guids.AddRange(guidList);
    }

    public static string CreateUnique()
    {
        if (guids == null)
        {
            guids = new List<string>();
        }

        string guid=string.Empty;

        while (string.IsNullOrEmpty(guid) || guids.Contains(guid))
        {
            if (App.AccountInfo != null)
            {
                guid = $"{System.Guid.NewGuid()}-{App.AccountInfo.GetPlayerGuid()}";
            }
            else
            {
                guid = $"{System.Guid.NewGuid()}";
            }
        }

        guids.Add(guid);
        return guid;
    }

    public static int CreateUniqueInt()
    {
        if (guidInts == null)
        {
            guidInts = new List<int>();
        }

        var guid=0;

        while (guid == 0 || guidInts.Contains(guid))
        {
            guid = System.Guid.NewGuid().GetHashCode();
        }

        guidInts.Add(guid);
        return guid;
    }
}
