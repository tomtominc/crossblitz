﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace CrossBlitz.Utils
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class TextColor : MonoBehaviour
    {
        public List<Color> colors;

        private TextMeshProUGUI _text;
        private const float SwitchDelay = 0.25f;
        private float _timer;
        private int _colorIndex;

        private void Start()
        {
            _text = GetComponent<TextMeshProUGUI>();
            _colorIndex = 0;
            _text.color = colors[_colorIndex];
        }

        public void Update()
        {
            _timer += Time.deltaTime;

            if (_timer > SwitchDelay)
            {
                _timer = 0;
                _colorIndex++;
                if (_colorIndex >= colors.Count)
                {
                    _colorIndex = 0;
                }

                _text.color = colors[_colorIndex];
            }
        }
    }
}
