using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Utils
{
    [RequireComponent(typeof(RawImage))]
    public class AutoScrollRawImage : MonoBehaviour
    {
        public float speed;
        public Vector2Int direction;

        public RawImage m_img;
        private RawImage Img
        {
            get
            {
                if (m_img == null) m_img = GetComponent<RawImage>();
                return m_img;
            }
        }

        private void Update()
        {
            var rect = Img.uvRect;
            rect.x += speed * direction.x * Time.deltaTime;
            rect.y += speed * direction.y * Time.deltaTime;
            Img.uvRect = rect;
        }
    }
}