using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Utils
{
    public class ColorCopier : MonoBehaviour
    {
        public Image copyFrom;
        public Image copyTo;

        private void Update()
        {
            copyTo.color = copyFrom.color;
        }
    }
}