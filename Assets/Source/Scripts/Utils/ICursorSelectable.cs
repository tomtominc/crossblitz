namespace CrossBlitz.Utils
{
    public interface ICursorSelectable
    {
        bool Interactable { get; }
        bool Grabbable { get; }
        bool InfoOnly { get; }
        bool Loading { get;  }
    }
}