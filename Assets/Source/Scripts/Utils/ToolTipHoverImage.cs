using System;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace CrossBlitz.Utils
{
    [System.Serializable]
    public class CanvasSortingLayer
    {
        public Canvas canvas;
        public int inActivateSortingOrder;
        public int activeSortingOrder;
    }

    [System.Serializable]
    public class ToolTipCustomData
    {
        public string data;
        public Vector2 overridePosition;
    }

    public class ToolTipHoverImage : MonoBehaviour,
        IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, ICursorSelectable
    {
        public static List<ToolTipHoverImage> ToolTips;

        public float fadeSpeed = 10;
        public float alphaHitTestMinimumThreshold;
        public bool clickToShow;
        public bool hoverToShow;
        public bool hoverToHighlight;

        [ShowIf("hoverToShow")]
        public float hoverDelay = 0.24f;
        public CanvasGroup backgroundCanvas;
        [ShowIf("clickToShow")]
        public Button hideBackgroundButton;
        [ShowIf("clickToShow")]
        public bool doHitEffect;
        [ShowIf("doHitEffect")]
        public RectTransform objectForHitEffect;
        public CanvasGroup toolTipCanvas;
        public RectTransform toolTipRect;
        public List<SpriteOutline> outlines;
        public bool animateToolTipIn = true;
        [ShowIf("animateToolTipIn")]
        public float animateDuration=1;
        [ShowIf("animateToolTipIn")]
        public Vector2 startToolTipOffset = new Vector2(0, -12);
        public bool usesCustomData;
        [ShowIf("usesCustomData")]
        public ToolTipCustomData toolTipCustomData;
        [ShowIf("usesCustomData")]
        public ToolTipBehaviour toolTipBehaviour;
        public bool showInFront = true;
        [ShowIf("showInFront")]
        public List<CanvasSortingLayer> sortingLayers;

        private bool _show;
        private bool _showing;
        private bool _hide;

        private bool _pointerHovering;
        private float _hoverTime;

        private Vector2 _toolTipOriginalPosition;
        private void Start()
        {
            ToolTips ??= new List<ToolTipHoverImage>();
            if (!ToolTips.Contains(this)) ToolTips.Add(this);

            if (alphaHitTestMinimumThreshold > 0)
            {
                GetComponent<Image>().alphaHitTestMinimumThreshold = alphaHitTestMinimumThreshold;
            }

            if (toolTipRect)
            {
                _toolTipOriginalPosition = toolTipRect.anchoredPosition;
            }
            else
            {
                Debug.LogWarning("Tool Tip Rect not setup!");
            }

            if (hideBackgroundButton)
            {
                hideBackgroundButton.onClick.AddListener(OnHideButton);
            }

            Hide();
        }

        private void OnDestroy()
        {
            ToolTips?.Remove(this);
        }

        private void Highlight()
        {
            for (var i = 0; i < outlines.Count; i++)
            {
                outlines[i].Regenerate();
            }
        }

        private void RemoveHighlight()
        {
            for (var i = 0; i < outlines.Count; i++)
            {
                outlines[i].Clear();
            }
        }

        private void RemoveInstantly()
        {
            _hide = false;

            if (backgroundCanvas)
            {
                backgroundCanvas.alpha = 0;
                backgroundCanvas.SetActive(false);
            }

            if (toolTipCanvas)
            {
                toolTipCanvas.alpha = 0;
                toolTipCanvas.SetActive(false);
            }
        }

        private void ShowInFront()
        {
            if (showInFront)
            {
                for (var i = 0; i < sortingLayers.Count; i++)
                {
                    sortingLayers[i].canvas.sortingOrder = sortingLayers[i].activeSortingOrder;
                }
            }
        }

        private void RemoveFromFront()
        {
            if (showInFront)
            {
                for (var i = 0; i < sortingLayers.Count; i++)
                {
                    sortingLayers[i].canvas.sortingOrder = sortingLayers[i].inActivateSortingOrder;
                }
            }
        }

        private void Show()
        {
            _show = true;
            _showing = false;
            _hide = false;

            if (toolTipCanvas)
            {
                toolTipCanvas.SetActive(true);
            }

            if (usesCustomData && toolTipBehaviour && toolTipCustomData!=null)
            {
                _toolTipOriginalPosition = toolTipCustomData.overridePosition;
                toolTipBehaviour.SetCustomData(toolTipCustomData);
            }

            if (animateToolTipIn && toolTipRect)
            {
                toolTipRect.anchoredPosition = _toolTipOriginalPosition + startToolTipOffset;
                toolTipRect.DOAnchorPosY(_toolTipOriginalPosition.y, animateDuration)
                    .SetEase(Ease.OutBack);
            }

            ShowInFront();
            Highlight();

            if (backgroundCanvas)
            {
                backgroundCanvas.SetActive(true);
            }
        }

        public void Hide()
        {
            _show = false;
            _hide = true;
            _showing = false;
            _hoverTime = 0;
            _pointerHovering = false;

            RemoveFromFront();
            RemoveHighlight();
            RemoveInstantly();
        }

        private void Update()
        {
            if (_pointerHovering && !_showing)
            {
                OnPointerHover();
            }

            if (_show)
            {
                if (backgroundCanvas)
                {
                    backgroundCanvas.alpha = Mathf.MoveTowards(backgroundCanvas.alpha, 1, fadeSpeed * Time.deltaTime);
                }

                if (toolTipCanvas)
                {
                    toolTipCanvas.alpha = Mathf.MoveTowards(toolTipCanvas.alpha, 1, fadeSpeed * Time.deltaTime);
                }

                if (toolTipCanvas && toolTipCanvas.alpha >= 1)
                {
                    _show = false;
                    _showing = true;
                }
            }
            else if (_hide)
            {
                if (backgroundCanvas)
                {
                    backgroundCanvas.alpha = Mathf.MoveTowards(backgroundCanvas.alpha, 0, fadeSpeed * Time.deltaTime);
                }

                if (toolTipCanvas)
                {
                    toolTipCanvas.alpha = Mathf.MoveTowards(toolTipCanvas.alpha, 0, fadeSpeed * Time.deltaTime);

                    if (toolTipCanvas.alpha <= 0)
                    {
                        _hide = false;
                        toolTipCanvas.SetActive(false);

                        if (backgroundCanvas)
                        {
                            backgroundCanvas.SetActive(false);
                        }
                    }
                }
            }
        }


        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!hoverToShow && !hoverToHighlight) return;

            if (hoverToHighlight)
            {
                Highlight();
                ShowInFront();
            }
            else if (hoverDelay <= 0)
            {
                Show();
            }
            else
            {
                _pointerHovering = true;
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (!hoverToShow && !hoverToHighlight) return;

            if (!hoverToShow && !_showing && !_show)
            {
                RemoveHighlight();
                RemoveFromFront();
            }
            else if (hoverToShow)
            {
                Hide();
            }
        }

        private void OnPointerHover()
        {
            if (_show) return;

            _hoverTime += Time.deltaTime;

            if (_hoverTime >= hoverDelay)
            {
                _hoverTime = 0;
                Show();
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (!clickToShow) return;

            if (doHitEffect)
            {
                DOTween.Kill(objectForHitEffect, true);
                objectForHitEffect.DOPunchAnchorPos(Vector2.down * 4, 0.16f);
            }

            Show();
        }

        private void OnHideButton()
        {
            Hide();
        }

        public static void HideAll()
        {
            if (ToolTips == null) return;

            for (var i = 0; i < ToolTips.Count; i++)
            {
                ToolTips[i].Hide();
            }
        }

        public bool Interactable => clickToShow;
        public bool Grabbable => false;
        public bool InfoOnly => hoverToShow;
        public bool Loading => false;
    }
}