using KennethDevelops.ProLibrary.DataStructures.Pool;
using KennethDevelops.ProLibrary.Managers;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.Pools
{
    public class AnimatorPoolObject : MonoBehaviour, IPoolObject
    {
        public SpriteAnimation animator;
        [ShowIf("animator")]
        public bool playAnimationOnStart;
        [ShowIf("playAnimationOnStart")]
        public string animationToPlay;
        [ShowIf("animator")]
        public bool disposeOnFinish;

        private PoolManager _poolManager;

        public void OnAcquire(PoolManager poolManager)
        {
            _poolManager = poolManager;

            if (playAnimationOnStart && !string.IsNullOrEmpty(animationToPlay))
            {
                if (disposeOnFinish)
                {
                    animator.Play(animationToPlay, Dispose);
                }
                else
                {
                    animator.Play(animationToPlay);
                }
            }
        }

        private void Dispose()
        {
            _poolManager.Dispose(this);
        }

        public void OnDispose()
        {
            animator.Stop();
        }
    }
}