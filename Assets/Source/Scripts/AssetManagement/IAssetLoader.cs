namespace Source.Scripts.AssetManagement
{
    public interface IAssetLoader
    {
        string AssetName { get; }
        bool IsBusy { get; }
        void Unload();

    }
}
