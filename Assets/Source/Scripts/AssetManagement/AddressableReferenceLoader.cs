using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asyncoroutine;
using CrossBlitz.AddressablesAPI;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.ResourceLocations;
using UnityEngine.U2D;
using Object = UnityEngine.Object;

namespace CrossBlitz.AssetManagement
{
    public static class AddressableReferenceLoader
    {
        private static bool initialized;
        private static Dictionary<string, GameObject> _loadingRefs;
        private static Dictionary<string, GameObject> _refs;
        private static Dictionary<string, AssetReferenceObject> _spriteRefs;
        private static List<GameObject> _alloc;
        private static Dictionary<string, Sprite> _spriteAlloc;
        private static Dictionary<string, Object> _objectRefs;

        // remove this and load them dynamically later?
        private static Dictionary<string, SpriteAnimationAsset> _animations;

        public static bool Initialized => initialized;

        public static async void Initialize()
        {
            _alloc = new List<GameObject>();
            _spriteAlloc = new Dictionary<string, Sprite>();
            _refs = new Dictionary<string, GameObject>();
            _loadingRefs = new Dictionary<string, GameObject>();
            _objectRefs = new Dictionary<string, Object>();
            _animations = new Dictionary<string, SpriteAnimationAsset>();

            initialized = true;

            LoadAllAnimations();

            await Load<SpriteAtlas>("cards-atlas");

            ShowAllAddressables();
        }

        public static async void LoadAllAnimations()
        {
            var animations = await Addressables.LoadAssetsAsync<SpriteAnimationAsset>("animation-asset", null).Task;

            for (var i = 0; i < animations.Count; i++)
            {
                if (animations[i] != null && !_animations.ContainsKey(animations[i].name))
                {
                    _animations.Add(animations[i].name, animations[i]);
                }
            }
        }

        public static SpriteAnimationAsset GetAnimationAsset(string key)
        {
            if (!_animations.ContainsKey(key))
            {
                return null;
            }

            return _animations[key];
        }

        public static Dictionary<string, SpriteAnimationAsset> GetAllAnimationAssets()
        {
            return _animations;
        }

        public static async Task<GameObject> Load(string key, Transform parent, bool startsInactive = false)
        {
            if (!initialized) Initialize();

            GameObject reference;

            if (!_refs.ContainsKey(key))
            {
                if (_loadingRefs.ContainsKey(key))
                {
                    reference = await Addressables.LoadAssetAsync<GameObject>(key).Task;
                }
                else
                {
                    _loadingRefs.Add(key, null);
                    reference = await Addressables.LoadAssetAsync<GameObject>(key).Task;
                    _refs.Add(key, reference);
                    _loadingRefs.Remove(key);
                }
            }

            reference = _refs[key];
            reference.SetActive(!startsInactive);
            return CreateGameObject(_refs[key], parent, startsInactive);
        }

        private static GameObject CreateGameObject(GameObject original, Transform parent, bool startsInactive = false)
        {
            if (!initialized) Initialize();

            var alloc = Object.Instantiate(original, parent);

            if (startsInactive)
            {
                alloc.SetActive(false);
            }

            _alloc.Add(alloc);

            return alloc;
        }

        public static async Task<GameObject> GetAsset(string key, bool startsInactive = false)
        {
            if (!initialized) Initialize();

            GameObject reference;

            if (!_refs.ContainsKey(key))
            {
                reference = await Addressables.LoadAssetAsync<GameObject>(key).Task;

                if (_refs.ContainsKey(key))
                {
                    Unload(reference);
                }
                else
                {
                    _refs.Add(key, reference);
                }
            }

            reference = _refs[key];
            reference.SetActive(!startsInactive);
            return reference;
        }

        public static async Task<Sprite> Load(string key)
        {
            if (!initialized) Initialize();

            if (!AddressableResourceExists(key, typeof(Sprite)))
            {
                //Debug.LogWarning($"Resource does not exist for {key}");
                return null;
            }

            if (_spriteAlloc.ContainsKey(key))
            {
                return _spriteAlloc[key];
            }

            var alloc = await Addressables.LoadAssetAsync<Sprite>(key).Task;

            if (alloc == null)
            {
                Debug.LogError($"No sprite with key {key} is an addressable");
            }
            else if (!_spriteAlloc.ContainsKey(key))
            {
                _spriteAlloc.Add(key, alloc);
            }

            return alloc;
        }

        public static void Unload(GameObject obj)
        {
            if (!initialized) Initialize();

            _alloc.Remove(obj);
            Addressables.ReleaseInstance(obj);
        }

        public static void Unload(this GameObject obj, bool immediate)
        {
            Unload(obj);
        }

        public static void Unload(string key, Sprite sprite)
        {
            if (!initialized) Initialize();

            _spriteAlloc.Remove(key);
            Addressables.Release(sprite);
        }

        public static void Unload(this Sprite sprite, string key)
        {
            if (!initialized) Initialize();
            Unload(key, sprite);
        }

        public static async Task<T> Load<T>(string key) where T : Object
        {
            if (!initialized)
            {
                Initialize();
            }

            if (!AddressableResourceExists(key, typeof(T)))
            {
                return null;
            }

            if (_objectRefs.ContainsKey(key))
            {
                return (T)_objectRefs[key];
            }

            var alloc = await Addressables.LoadAssetAsync<T>(key).Task;

            if (alloc == null)
            {
                Debug.LogError($"No sprite with key {key} is an addressable");
            }
            else if (!_objectRefs.ContainsKey(key))
            {
                _objectRefs.Add(key, alloc);
            }

            return alloc;
        }

        public static void UnloadAll()
        {
            if (!initialized) Initialize();

            for (var i = _alloc.Count - 1; i > -1; i--)
            {
                Unload(_alloc[i]);
            }

            foreach (var asset in _spriteAlloc)
            {
                Unload(asset.Key, asset.Value);
            }
        }

        public static void ShowAllAddressables()
        {
            var allLocations = new List<IResourceLocation>();

            foreach (var resourceLocator in Addressables.ResourceLocators)
            {
                Debug.Log($"{resourceLocator.LocatorId}");

                if (resourceLocator is ResourceLocationMap map)
                {
                    foreach (var locations in map.Locations.Values)
                    {
                        foreach (var loc in locations)
                        {
                            Debug.Log(
                                $"{loc.ProviderId} {loc.ResourceType} {loc.InternalId} {loc.Data} {loc.PrimaryKey}");
                        }

                        allLocations.AddRange(locations);
                    }
                }
            }
        }

        public static bool AddressableResourceExists(object key, Type type)
        {
            if (!initialized) Initialize();

            foreach (var l in Addressables.ResourceLocators)
            {
                IList<IResourceLocation> locs;

                if (l.Locate(key, type, out locs))
                {
                    return true;
                }
            }

            return false;
        }
    }
}