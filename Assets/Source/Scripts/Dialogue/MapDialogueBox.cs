﻿
using System.Collections.Generic;
using CrossBlitz.Fables.Grid;
using DG.Tweening;
using MEC;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Dialogue
{
    public class MapDialogueCustomProperties
    {
        public HexTile2D DialogueTile;
    }

    public class MapDialogueBox : DialogueBox
    {
        public ContentSizeFitter dialogueBoxSizeFitter;
        private MapDialogueCustomProperties _customProperties;
        private Camera _mainCamera;
        public override void InitDialogueBox(object customInfo)
        {
            _mainCamera = Camera.main;
            _customProperties = (MapDialogueCustomProperties) customInfo;

            base.InitDialogueBox(customInfo);
        }

        protected override Vector2 GetStartingSizeDialogueBox()
        {
            return new Vector2(34, 28);
        }

        protected override void SetupStartingPosition()
        {
            var pos = _customProperties.DialogueTile.GetDialogueTargetWorldPosition();
            var viewportPoint = _mainCamera.WorldToViewportPoint(pos);

            container.anchorMin = viewportPoint;
            container.anchorMax = viewportPoint;
        }

        public override IEnumerator<float> AnimateIn(DialogueInfo dialogueInfo)
        {
            dialogue.textAnimator.tmproText.text = dialogueInfo.Text;

            yield return Timing.WaitForOneFrame;
            yield return Timing.WaitForOneFrame;

            var boxRect = box.rect;
            var animateBoxTo = new Vector2(boxRect.width, boxRect.height);

            Debug.Log(animateBoxTo);

            dialogue.ShowText(string.Empty);
            dialogue.SetActive(false);

            box.sizeDelta = GetStartingSizeDialogueBox();

            yield return Timing.WaitForOneFrame;

            dialogueBoxSizeFitter.verticalFit = ContentSizeFitter.FitMode.Unconstrained;

            boxCanvas.DOFade(1, 0.125f);

            yield return Timing.WaitUntilDone( box.DOSizeDelta(animateBoxTo, 0.25f)
                .SetEase(Ease.OutBack)
                .WaitForCompletion(true));

            dialogue.SetActive(true);
            dialogueBoxSizeFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
            isReady = true;
        }

        public override IEnumerator<float> AnimateOut()
        {
            dialogueBoxSizeFitter.verticalFit = ContentSizeFitter.FitMode.Unconstrained;
            dialogue.ShowText(string.Empty);

            box.DOSizeDelta(GetStartingSizeDialogueBox(), 0.25f);
            yield return Timing.WaitForSeconds(0.15f);

            boxCanvas.DOFade(0, 0.24f);
            dialogueBoxSizeFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;

            isReady = false;
        }
    }
}