using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.Transition;
using CrossBlitz.Utils;
using DG.Tweening;
using Febucci.UI;
using FMOD.Studio;
using MEC;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Dialogue
{
    [System.Serializable]
    public class DialogueInfo : UniqueItem
    {
        public const string Empty = "empty";
        public const string Narrator = "narrator";
        public const string Illustration = "Illustration";

        public string Text;

        public string PortraitAnimation;
        public bool SkipPressedActions;

        public bool SkipAnimateIn;
    }

    public class DialogueBox : MonoBehaviour
    {

        [BoxGroup("Always Needed")] public RectTransform container;
        [BoxGroup("Always Needed")] public RectTransform box;
        [BoxGroup("Always Needed")] public CanvasGroup boxCanvas;
        [BoxGroup("Always Needed")] public TextAnimatorPlayer dialogue;
        [BoxGroup("Animation")] public bool useBobbingAnimation = true;
        [ShowIf("useBobbingAnimation")] [BoxGroup("Animation")]public float bobHeight=1;
        [ShowIf("useBobbingAnimation")] [BoxGroup("Animation")]public float bobSpeed=4;

        [BoxGroup("Can Be Null/Animation")] public RectTransform portraitFrame;
        [BoxGroup("Can Be Null")] public SpriteAnimation portrait;
        [BoxGroup("Can Be Null")] public CanvasGroup portraitCanvas;
        [BoxGroup("Can Be Null")] public SpriteAnimation illustrationFront;
        [BoxGroup("Can Be Null")] public SpriteAnimation illustrationBack;
        [BoxGroup("Can Be Null")] public CanvasGroup illustrationFrontCanvas;
        [BoxGroup("Can Be Null")] public CanvasGroup illustrationBackCanvas;
        [BoxGroup("Can Be Null")] public CanvasGroup illustrationLabelCanvas;
        [BoxGroup("Can Be Null")] public TextMeshProUGUI illustrationLabel;
        [BoxGroup("Can Be Null")] public RectTransform illustrationLabelTransform;
        [BoxGroup("Can Be Null")] public Button nextButton;
        [BoxGroup("Can Be Null")] public RectTransform boxArrow;
        [BoxGroup("Can Be Null")] public bool isIllustration;
        [BoxGroup("Can Be Null/Events")] public ShakeContainer menuShaker;

        private CanvasGroup dialogueCanvas;
        private DialogueArrow boxArrowObject;

        private EventInstance textLetterInstance;

        protected float BoxDelta;
        protected float DialogueDelta;
        protected float ArrowDelta;

        protected bool DialogueShowed;
        protected bool NextButtonPressed;

        protected bool isReady;
        protected Vector2 OriginalContainerPos;

        // used for bobbing animation--
        private float boxY;
        private float portraitY;
        private float arrowY;

        private string previousLabel = string.Empty;

        private bool m_skipped;
        private bool m_skippedToChoice;
        //private PlaySoundResult m_typewriterSound;

        protected string m_clickSound = "UI-SingleClick";
        private string m_characterPitch;
        private string m_characterBleep;
        private CutsceneData _cutsceneData;

        public bool IsReady
        {
            get => isReady;
            set => isReady = value;
        }

        public virtual void InitDialogueBox(object customInfo)
        {
            _cutsceneData = (CutsceneData)customInfo;
            dialogueCanvas = dialogue.GetComponent<CanvasGroup>();
            if(boxArrow) boxArrowObject = boxArrow.GetComponent<DialogueArrow>();

            isReady = false;
            isIllustration = false;
            BoxDelta = 0;
            DialogueDelta = 0;

            OriginalContainerPos = container.anchoredPosition;

            dialogue.onCharacterVisible.AddListener(OnCharacterShown);

            if (boxCanvas)
            {
                boxCanvas.alpha = 0;
            }

            if (boxArrow) boxArrow.SetActive(true);
            SetupStartingPosition();

            dialogue.onTextShowed.AddListener(OnDialogueShowed);
            dialogue.textAnimator.onEvent -= OnEvent;
            dialogue.textAnimator.onEvent += OnEvent;

            SetBleepPitchDefault();

            if (nextButton)
            {
                nextButton.onClick.AddListener(OnNextButton);
            }

            if (box)
            {
                boxY = box.anchoredPosition.y;
            }

            if (portrait)
            {
                portraitY = portrait.RectTransform().anchoredPosition.y;
            }

            if (boxArrow)
            {
                arrowY = boxArrow.anchoredPosition.y;
            }
        }

        protected virtual void FixDependencies()
        {
            if (nextButton)
            {
                nextButton.onClick.RemoveAllListeners();
                nextButton.onClick.AddListener(OnNextButton);
            }

            FixSound();
        }

        private void OnCharacterShown(char c)
        {
            float pitch = 1;

            switch (m_characterPitch)
            {
                case "1":
                    pitch = 0.5f;
                    break;

                case "2":
                    pitch = 0.75f;
                    break;
                case "3":
                    pitch = 1f;
                    break;

                case "4":
                    pitch = 1.25f;
                    break;

                case "5":
                    pitch = 1.5f;
                    break;
            }

            if (c == ' ') return;

            else if (c == '?' || c == '!')
            {
                AudioController.PlaySound("Text-Letter").setPitch(1.2f * pitch);
            }
            else if (c == '.')
            {
                AudioController.PlaySound("Text-Letter").setPitch(.8f * pitch);
            }
            else
            {
                var randomPitch = UnityEngine.Random.Range(0.9f, 1.1f);
                if (!AudioController.IsSoundPlaying(m_characterBleep)) AudioController.PlaySound(m_characterBleep, "SFX",false).setPitch(pitch*randomPitch);
            }
        }

        public virtual void OnSkipped()
        {
            m_skipped = true;
            DialogueShowed = true;

            FixSound();
        }

        public virtual void OnSkippedToChoice()
        {
            m_skippedToChoice = true;
            DialogueShowed = true;

            FixSound();
        }

        private void FixSound()
        {
            // if (m_typewriterSound != null && m_typewriterSound.ActingVariation != null)
            // {
            //     m_typewriterSound.ActingVariation.FadeOutNowAndStop(0.2f);
            // }

            AudioController.StopSound("Text-Letter");
            AudioController.StopSound("Text-LetterB");
            AudioController.StopSound("Text-LetterC");
        }

        protected virtual void OnEvent(string message)
        {
            if (message.Contains("screen-shake"))
            {
                if (m_skipped || m_skippedToChoice || App.Settings.GetScreenShake() == 0) return;

                var parameters = message.Split(':');
                var magnitude = 4;
                var duration = 0.5f;

                if (parameters.Length > 1)
                {
                    magnitude = int.Parse(parameters[1]);
                }

                if (parameters.Length > 2)
                {
                    duration = float.Parse(parameters[2]);
                }

                if (App.Settings.GetScreenShake() == 1)
                {
                    magnitude /= 2;
                    duration /= 2;
                }

                menuShaker.Shake(magnitude,duration);
            }
            else if (message.Equals("flash"))
            {
                if (m_skipped || m_skippedToChoice || App.Settings.GetScreenFlash() == 0) return;

                TransitionController.FlashScreen();
            }
            else if (message.Contains("stop-ambient"))
            {
                if (m_skipped || m_skippedToChoice) return;

                AmbientController.Stop();
            }
            else if (message.Contains("ambient"))
            {
                if (m_skipped || m_skippedToChoice) return;

                var audioId = message.Split(':');

                if (audioId.Length > 1)
                {
                    AmbientController.PlayAmbient(audioId[1]);
                }
            }
            else if (message.Contains("fade-music"))
            {
                if (m_skipped || m_skippedToChoice) return;

                var parameters = message.Split(':');
                var seconds = 1;

                if (parameters.Length > 1)
                {
                    seconds = int.Parse(parameters[1]);
                }
                AudioController.StopSong();
                //AudioController.FadePlaylistVolume(0f, seconds);
            }
            // else if (message.Contains("quest-step-change"))
            // {
            //     var parameters = message.Split(':');
            //     var parameterLength = parameters.Length;
            //     var isInt = int.TryParse(parameters[2], out var stepValue);
            //
            //     if (parameterLength > 2 &&
            //         isInt &&
            //         stepValue > 0 &&
            //         FablesMapController.Instance != null &&
            //         FablesMapController.Instance.ClientChapterData != null)
            //     {
            //         var chapter = FablesMapController.Instance.ClientChapterData;
            //         var fableQuestUid = Db.FableQuestDatabase.GetQuestUidFromName(parameters[1]);
            //
            //         if (!string.IsNullOrEmpty(fableQuestUid))
            //         {
            //             chapter.SetQuestStep(fableQuestUid, stepValue);
            //         }
            //         else
            //         {
            //             Debug.LogError($"Could not get quest from name: {parameters[1]}");
            //         }
            //     }
            // }
            else if (message.Contains("sfx"))
            {
                if (m_skipped || m_skippedToChoice) return;

                var audioId = message.Split(':');

                if (audioId.Length > 1)
                {
                    AudioController.PlaySound(audioId[1]);
                }
            }
            else if (message.Contains("music"))
            {
                if (m_skipped || m_skippedToChoice) return;

                var audioId = message.Split(':');

                if (audioId.Length > 1)
                {
                    AudioController.PlaySong(audioId[1]);
                    AudioController.FadePlaylistVolume(1f, 0.5f);
                }
            }
            else if (message.Contains("shake-char"))
            {
                if (m_skipped || m_skippedToChoice) return;

                var parameters = message.Split(':');

                var characterId = string.Empty;
                var magnitude = 4;
                var duration = 0.16f;

                if (parameters.Length > 1)
                {
                    characterId = parameters[1];
                }

                if (parameters.Length > 2)
                {
                    magnitude = int.Parse(parameters[2]);
                }

                if (parameters.Length > 3)
                {
                    duration = float.Parse(parameters[3]);
                }

                Events.Publish(this, EventType.OnShakeCharacter, new OnShakeCharacterEventArgs
                {
                    characterId = characterId,
                    magnitude = magnitude,
                    duration = duration
                });
            }
            else if (message.Contains("bounce-char"))
            {
                if (m_skipped || m_skippedToChoice) return;

                var characterId = message.Split(':');

                if (characterId.Length > 1)
                {
                    Events.Publish(this, EventType.OnBounceCharacter, new OnBounceCharacterEventArgs { characterId = characterId[1]});
                }
                else
                {
                    Events.Publish(this, EventType.OnBounceCharacter, new OnBounceCharacterEventArgs());
                }
            }
            else if (message.Contains("illustration-change-img"))
            {
                if (m_skipped || m_skippedToChoice) return;

                var characterId = message.Split(':');

                if (characterId.Length > 1)
                {
                   Timing.WaitUntilDone(IllustrationTransition(characterId[1]).CancelWith(gameObject));
                }
            }
            else if (message.Contains("illustration-change-char"))
            {
                if (m_skipped || m_skippedToChoice) return;

                var characterId = message.Split(':');

                if (characterId.Length > 1)
                {
                    Timing.WaitUntilDone(IllustrationLabel(characterId[1]).CancelWith(gameObject));
                }
                else
                {
                    Timing.WaitUntilDone(IllustrationLabel(string.Empty).CancelWith(gameObject));
                }
            }
        }

        protected virtual void SetupStartingPosition()
        {
            container.anchoredPosition= Vector2.zero;
        }

        protected virtual Vector2 GetStartingSizeDialogueBox()
        {
            return new Vector2(34, 28);
        }

        protected virtual void OnDestroy()
        {
            FixSound();
            //AudioController.SetPlaylistVolume(1);
        }

        public virtual IEnumerator<float> ShowText(DialogueInfo dialogueInfo)
        {
            FixDependencies();

            if(!isIllustration) GetTypewriterBleepSoundPitch(dialogueInfo);

            if (!isReady)
            {
                yield return Timing.WaitUntilDone(AnimateIn(dialogueInfo).CancelWith(gameObject));
            }

            DialogueShowed = false;
            NextButtonPressed = dialogueInfo.SkipPressedActions;

            if (boxArrow)
            {
                boxArrowObject.FadeOut(); // boxArrow.SetActive(false);
            }


           // var characterId = message.Split(':');
           // var character = Db.CharacterDatabase.GetCharacter(_cutsceneData.RightSideCharacters[info.CharacterIndex]);

            if (portrait != null && portrait.CurrentAnimationName != dialogueInfo.PortraitAnimation)
            {
                portrait.Play( dialogueInfo.PortraitAnimation );
            }

            dialogueCanvas.alpha = 1;

            //Process dialogue
            var processedText = ProcessText(dialogueInfo.Text);

            dialogue.ShowText(processedText);

           // if (!m_skipped)
          //  {
               // m_typewriterSound = GetTypewriterBleepSound(dialogueInfo);

                //AudioController.PlaySound(GetTypewriterBleepSound(dialogueInfo));
            //}

            while (!DialogueShowed)
            {
                if (m_skipped || m_skippedToChoice)
                {
                    FixSound();
                }
                yield return Timing.WaitForOneFrame;
            }

            //m_typewriterSound.ActingVariation.Stop();

            DialogueShowed = false;
            NextButtonPressed = dialogueInfo.SkipPressedActions;

            if (!dialogueInfo.SkipPressedActions)
            {
                if (boxArrow)
                {
                    boxArrow.SetActive(true);
                    boxArrowObject.FadeIn();
                }
            }

            while (!NextButtonPressed && !m_skipped && !m_skippedToChoice) yield return Timing.WaitForOneFrame;

            AudioController.PlaySound(m_clickSound);
            if (boxArrow) boxArrowObject.FadeOut(); //boxArrow.SetActive(false);
            dialogueCanvas.DOFade(0f, 0.1f);
            yield return Timing.WaitForSeconds(0.15f);


            NextButtonPressed = false;
            m_skippedToChoice = false;
        }

        public virtual IEnumerator<float> AnimateIn(DialogueInfo dialogueInfo)
        {
            if (portrait.CurrentAnimationName != dialogueInfo.PortraitAnimation)
            {
                portrait.Play( dialogueInfo.PortraitAnimation );
            }

            isReady = true;

            yield break;
        }

        public virtual IEnumerator<float> AnimateOut()
        {
            isReady = false;

            dialogue.ShowText(string.Empty);
            boxCanvas.DOFade(0, 0.24f);
            yield return Timing.WaitUntilDone( box.DOSizeDelta(new Vector2(32, 60f), 0.24f)
                .SetEase(Ease.OutBack).WaitForCompletion(true));

            var targetPosition = new Vector2(OriginalContainerPos.x,
                -(this.RectTransform().rect.height/2) - (container.sizeDelta.y * 2f));
            if (container.anchoredPosition.y > 0)
            {
                targetPosition = new Vector2(
                    OriginalContainerPos.x,
                    (this.RectTransform().rect.height/2) + container.sizeDelta.y * 2f);
            }

            yield return Timing.WaitUntilDone(container.DOAnchorPos(targetPosition, 0.6f)
                .SetEase(Ease.InBack).WaitForCompletion(true));

        }

        public virtual IEnumerator<float> IllustrationTransition(string newIllustration)
        {
            dialogue.StopShowingText();

            illustrationFrontCanvas.alpha = 0;
            illustrationFront.Play(newIllustration);
            illustrationFrontCanvas.DOFade(1, 1f);
            illustrationBackCanvas.DOFade(0, 1f);
            yield return Timing.WaitForSeconds(1f);
            illustrationBack.Play(newIllustration);
            illustrationBackCanvas.alpha = 1;
            illustrationFrontCanvas.alpha = 0;

            dialogue.StartShowingText();

            Debug.Log(newIllustration);
            yield return Timing.WaitForOneFrame;
        }

        public virtual IEnumerator<float> IllustrationLabel(string label)
        {
            dialogue.StopShowingText();

            if (label == string.Empty)
            {
                illustrationLabel.text = label.ToUpper();

                illustrationLabelCanvas.DOFade(0, 0.2f);
                illustrationLabelTransform.DOAnchorPosX(illustrationLabelTransform.anchoredPosition.x - 20, 0.2f).SetEase(Ease.InBack);
            }
            else if(label != previousLabel && previousLabel != String.Empty)
            {
                illustrationLabelCanvas.DOFade(0, 0.2f);
                illustrationLabelTransform.DOAnchorPosX(illustrationLabelTransform.anchoredPosition.x - 20, 0.2f).SetEase(Ease.InBack);

                yield return Timing.WaitForSeconds(0.4f);

                illustrationLabel.text = label.ToUpper();

                illustrationLabelCanvas.DOFade(1, 0.2f);
                illustrationLabelTransform.DOAnchorPosX(illustrationLabelTransform.anchoredPosition.x + 20, 0.2f).SetEase(Ease.OutBack);

                previousLabel = label;
            }
            else
            {
                illustrationLabel.text = label.ToUpper();

                illustrationLabelCanvas.DOFade(1, 0.2f);
                illustrationLabelTransform.DOAnchorPosX(illustrationLabelTransform.anchoredPosition.x + 20, 0.2f).SetEase(Ease.OutBack);
            }

            previousLabel = label;

            GetTypewriterBleepSoundPitchIllustration(label);

            yield return Timing.WaitForSeconds(0.2f);

            dialogue.StartShowingText();

            yield return Timing.WaitForOneFrame;
        }

        public void OnNextButton()
        {
            if (!isReady)
            {
                return;
            }

            NextButtonPressed = true;
            DialogueShowed = true;
            dialogue.SkipTypewriter();
        }

        private void OnDialogueShowed()
        {
            DialogueShowed = true;
        }

        protected  virtual void Update()
        {
            if (!isReady || !useBobbingAnimation) return;

            BoxDelta = Mathf.Sin(Time.time * bobSpeed) * bobHeight;
            DialogueDelta = Mathf.Sin((Time.time+90) * bobSpeed) * bobHeight;
            ArrowDelta = Mathf.Sin(Time.time * bobSpeed * 4) * bobHeight;

            if (box)
            {
                box.anchoredPosition = new Vector2(
                    box.anchoredPosition.x,
                    boxY + BoxDelta);
            }

            if (portraitFrame)
            {
                portraitFrame.anchoredPosition = new Vector2(
                    portraitFrame.anchoredPosition.x,
                    portraitY + DialogueDelta);
            }

            if (boxArrow)
            {
                boxArrow.anchoredPosition = new Vector2(
                    boxArrow.anchoredPosition.x,
                    arrowY + ArrowDelta);
            }
        }

        public string ProcessText(string text)
        {
            var match = Regex.Match(text, @"illustration-change-img(.*?(?=\>))>");
            if (match.Success)
            {
                var insertIndex = match.Value.Length + match.Index;
                text = text.Insert(insertIndex, " ");
            }
            match = Regex.Match(text, @"illustration-change-char(.*?(?=\>))>");
            if (text.Contains("<?illustration-change-char"))
            {
                var insertIndex = match.Value.Length + match.Index;
                text = text.Insert(insertIndex, " ");
            }
            return text;
        }


        protected virtual int GetTypewriterPitch(DialogueInfo dialogueInfo)
        {
            return 1;
        }

        //protected virtual string GetTypewriterBleepSound(DialogueInfo dialogueInfo)
        //{
        //    return "Text-LetterC";
        //}

        protected void GetTypewriterBleepSoundPitch(DialogueInfo dialogueInfo)
        {
            // Default Values
            m_characterBleep = "Text-LetterC";
            m_characterPitch = "3";

            if (_cutsceneData != null && dialogueInfo is CutsceneDialogueInfo info)
            {
                if (info.CharacterSide == CharacterSide.Left &&
                    _cutsceneData.LeftSideCharacters.Count > info.CharacterIndex)
                {
                    var character =
                        Db.CharacterDatabase.GetCharacter(_cutsceneData.LeftSideCharacters[info.CharacterIndex]);
                    if (character != null && !string.IsNullOrEmpty(character.speechBleep))
                    {
                        m_characterBleep =  character.speechBleep;
                        m_characterPitch = character.speechPitch;
                    }
                }
                else if (info.CharacterSide == CharacterSide.Right &&
                         _cutsceneData.RightSideCharacters.Count > info.CharacterIndex)
                {
                    var character =
                        Db.CharacterDatabase.GetCharacter(_cutsceneData.RightSideCharacters[info.CharacterIndex]);
                    if (character != null && !string.IsNullOrEmpty(character.speechBleep))
                    {
                        m_characterBleep = character.speechBleep;
                        m_characterPitch = character.speechPitch;
                    }
                }
            }
        }

        public void GetTypewriterBleepSoundPitchIllustration(String nextCharacter)
        {
            // Default Values
            m_characterBleep = "Text-LetterC";
            m_characterPitch = "3";

            if (nextCharacter != null && nextCharacter != "")
            {
                var alteredChar = char.ToUpper(nextCharacter[0]) + nextCharacter.Substring(1).ToLower();

                var character = Db.CharacterDatabase.GetCharacter(alteredChar);

                if (character != null && !string.IsNullOrEmpty(character.speechBleep))
                {
                    m_characterBleep = character.speechBleep;
                    m_characterPitch = character.speechPitch;
                }
            }
        }

        public void SetBleepPitchDefault()
        {
            m_characterBleep = "Text-LetterC";
            m_characterPitch = "3";
        }
    }
}
