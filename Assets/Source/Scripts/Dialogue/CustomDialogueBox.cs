using System.Collections.Generic;
using CrossBlitz.Cursors;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.Dialogue
{
    public class CustomDialogueBox : DialogueBox
    {
        public enum AnimationInDirection
        {
            Bottom,
            Left,
            Right,
            Top,
            Custom
        }

        [BoxGroup("Custom Properties")][InfoBox("Anchor point must be Middle/Center.")] public AnimationInDirection animationInDirection;
        [ShowIf("@this.animationInDirection == AnimationInDirection.Custom")] [BoxGroup("Custom Properties/Animation")] public Vector2 animationInCustomOffset;
        [BoxGroup("Custom Properties/Animation")] public float moveDuration;
        [BoxGroup("Custom Properties/Animation")] public Ease moveEase;
        [BoxGroup("Custom Properties/Animation")] public Vector2 speechBoxAnimateFromSize = new Vector2(84f, 72f);

        private Vector2 _originalPosition;
        private Vector2 _speechBoxOriginalSize;
        private RectTransform _rect;

        public override void InitDialogueBox(object customInfo)
        {
            if (_rect == null)
            {
                _rect = this.RectTransform();
                _originalPosition = _rect.anchoredPosition;
                _speechBoxOriginalSize = box.sizeDelta;
            }

            base.InitDialogueBox(customInfo);
        }

        private Vector2 GetStartingPosition()
        {
            var screenSize = CrossBlitzInputModule.Instance.GetNativeScreenSize();

            switch (animationInDirection)
            {
                case AnimationInDirection.Bottom:
                    return new Vector2(_originalPosition.x, -_rect.sizeDelta.y / 2f - (screenSize.y / 2f));
                case AnimationInDirection.Left:
                    return new Vector2(-_rect.sizeDelta.x / 2f - (screenSize.y / 2f), _originalPosition.y);
                case AnimationInDirection.Right:
                    return new Vector2(_rect.sizeDelta.x / 2f + (screenSize.y / 2f), _originalPosition.y);
                case AnimationInDirection.Top:
                    return new Vector2(_originalPosition.x, _rect.sizeDelta.y / 2f + (screenSize.y / 2f));
                case AnimationInDirection.Custom:
                    return _originalPosition + animationInCustomOffset;
            }

            return _originalPosition;
        }

        protected override void SetupStartingPosition()
        {
            _rect.anchoredPosition = GetStartingPosition();
            boxCanvas.alpha = 0;
            box.sizeDelta = speechBoxAnimateFromSize;
        }

        public override IEnumerator<float> AnimateIn(DialogueInfo dialogueInfo)
        {
            SetupStartingPosition();

            _rect.DOAnchorPos(_originalPosition, moveDuration).SetEase(moveEase);

            yield return Timing.WaitForSeconds(moveDuration);
            //todo: add custom variables for these values as well...
            boxCanvas.DOFade(1, 0.25f);
            box.DOSizeDelta(_speechBoxOriginalSize, 0.4f).SetEase(Ease.OutBack);
        }

        public override IEnumerator<float> AnimateOut()
        {
            box.DOSizeDelta(speechBoxAnimateFromSize, 0.4f).SetEase(Ease.InBack);
            boxCanvas.DOFade(0, 0.4f);
            yield return Timing.WaitForSeconds(0.4f);
            _rect.DOAnchorPos(GetStartingPosition(), 0.24f).SetEase(Ease.InBack);
        }

        public override IEnumerator<float> ShowText(DialogueInfo dialogueBaseInfo)
        {
            dialogue.ShowText(string.Empty);

            var dialogueInfo =  dialogueBaseInfo;

            if(!dialogueBaseInfo.SkipAnimateIn)
            {
                isReady = false;
                Debug.Log("Animate in???");
                yield return Timing.WaitUntilDone(AnimateIn(dialogueInfo));
            }

            isReady = true;
            DialogueShowed = false;
            NextButtonPressed = dialogueInfo.SkipPressedActions;

            if (boxArrow) boxArrow.SetActive(false);

            if (portrait != null && portrait.CurrentAnimationName != dialogueInfo.PortraitAnimation)
            {
                portrait.Play( dialogueInfo.PortraitAnimation );
            }

            dialogue.ShowText(dialogueInfo.Text);

            while (!DialogueShowed)
            {
                yield return Timing.WaitForOneFrame;
            }

            DialogueShowed = false;
            NextButtonPressed = dialogueInfo.SkipPressedActions;

            if (!dialogueInfo.SkipPressedActions)
            {
                if (boxArrow) boxArrow.SetActive(true);
            }

            while (!NextButtonPressed) yield return Timing.WaitForOneFrame;

            NextButtonPressed = false;
        }
    }
}