using System;
using System.Collections.Generic;
using CrossBlitz.Audio;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.Dialogue
{
    public class NarratorDialogueBox : DialogueBox
    {
        public SpriteAnimation scroll;
        public CanvasGroup dialogueFader;
        public CanvasGroup fadeBackground;

        public override void InitDialogueBox(object customInfo)
        {
            m_clickSound = "UI-SingleClick";
            boxCanvas.alpha = 0;
            box.anchoredPosition = new Vector2(box.anchoredPosition.x, box.anchoredPosition.y - 32f);
            dialogueFader.alpha = 0;
            fadeBackground.alpha = 0;
            scroll.Play("closed");
            base.InitDialogueBox(customInfo);
        }

        protected override void SetupStartingPosition()
        {

        }

        public override IEnumerator<float> AnimateIn(DialogueInfo dialogueInfo)
        {
            box.DOAnchorPosY(box.anchoredPosition.y + 32f, 0.24f).SetEase(Ease.OutBack);
            boxCanvas.DOFade(1, 0.24f);
            fadeBackground.DOFade(1, 0.24f);
            yield return Timing.WaitForSeconds(0.24f);
            scroll.Play("open");
            AudioController.PlaySound("battle_end_win_1", "BARDSFX", true);
            while (scroll.CurrentFrame < 7) yield return Timing.WaitForOneFrame;
            dialogueFader.DOFade(1, 0.08f);
            isReady = true;
        }

        public override IEnumerator<float> AnimateOut()
        {
            box.DOAnchorPosY(box.anchoredPosition.y + 10f, 0.12f);
            boxCanvas.DOFade(0, 0.12f);
            fadeBackground.DOFade(0, 0.12f);
            yield return Timing.WaitForSeconds(0.12f);
            box.anchoredPosition = new Vector2(box.anchoredPosition.x, box.anchoredPosition.y - 32f);
            isReady = false;
            boxArrow.SetActive(false);
            scroll.Play("closed");
            dialogueFader.alpha = 0;
        }

        public IEnumerator<float> SimplyFadeOut()
        {
            boxCanvas.DOFade(0, 0.5f);
            fadeBackground.DOFade(0, 0.5f);
            yield return Timing.WaitForSeconds(0.5f);
        }
    }
}