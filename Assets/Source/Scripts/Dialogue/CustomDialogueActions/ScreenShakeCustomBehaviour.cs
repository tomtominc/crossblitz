using CrossBlitz.Cameras;
using Febucci.UI.Core;
using UnityEngine;

namespace CrossBlitz.Dialogue.CustomDialogueActions
{
    [EffectInfo(tag: "screen-shake")]
    [UnityEngine.Scripting.Preserve]
    public class ScreenShakeCustomBehaviour : BehaviorBase
    {
        private float strength;
        private float duration;
        private bool hasShaken;

        public override void ApplyEffect(ref CharacterData data, int charIndex)
        {
            if (hasShaken) return;

            var cameraVariant = CameraVariant.GetCurrentCamera();

            if (cameraVariant == null)
            {
                Debug.LogError("Camera Variant is null!!");
            }
            else
            {
                hasShaken = true;
                Debug.LogError($"SHAKE! {cameraVariant.GetInstanceID()} {cameraVariant.name} {strength} {duration}");
                cameraVariant.Shake(strength,duration);
            }
        }

        public override void SetDefaultValues(BehaviorDefaultValues data)
        {
            strength = data.defaults.shakeStrength;
            duration = data.defaults.shakeDelay;
            hasShaken = false;
        }

        public override void SetModifier(string modifierName, string modifierValue)
        {

        }
    }
}