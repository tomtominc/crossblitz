using CrossBlitz.Transition;
using Febucci.UI.Core;

namespace CrossBlitz.Dialogue.CustomDialogueActions
{
    [EffectInfo(tag: "flash")]
    [UnityEngine.Scripting.Preserve]
    public class FlashCustomBehaviour : AppearanceBase
    {
        private bool executed;

        public override void ApplyEffect(ref CharacterData data, int charIndex)
        {
           if (executed) return;
           executed = true;
           TransitionController.FlashScreen();
        }

        public override void SetDefaultValues(AppearanceDefaultValues data)
        {
            executed = false;
        }
    }
}