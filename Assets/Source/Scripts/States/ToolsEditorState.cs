using System.Threading.Tasks;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Developer.Tools;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Grid;
using CrossBlitz.InputAPI;
using CrossBlitz.Transition;
using CrossBlitz.ViewAPI.Popups;
using MEC;
using TakoBoyStudios.Events;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.States
{
    public class ToolsEditorState : State
    {
        public enum ToolsState
        {
            ToolsNavigator,
            EditTileMap,
            EditTileMetadata // still within the tools navigator
        }

        //private ToolsState _state;

        public override void Enter()
        {
            //_state = ToolsState.ToolsNavigator;

            OpenTools();
        }

        public override void Exit()
        {
        }

        private async void OpenTools()
        {
            var toolsSceneLoad = new SceneLoadParams
            {
                SceneToLoad = "Tools"
            };

            await SceneController.Instance.LoadScene(toolsSceneLoad);

            Timing.RunCoroutine(TransitionController.TransitionOut(TransitionController.TransitionType.Fade));
        }

        private async void OnEditMap(FableChapterData chapterData, int mapIndex)
        {
            if (SceneController.IsLoaded("FablesMap"))
            {
                PopupController.Open(new PopupInfo
                {
                    style = PopupWindowStyle.Dialogue,
                    body = "There is already a map open, please exit that map before opening a new one."
                });

                return;
            }

            Timing.RunCoroutine(TransitionController.TransitionIn(TransitionController.TransitionType.Fade));
            await UnityAsync.Await.Seconds(0.4f);

            await SceneController.Instance.UnloadScene("Tools");

            var toolsSceneLoad = new SceneLoadParams
            {
                SceneToLoad = "FablesMap"
            };

            var toolsSceneModel = await SceneController.Instance.LoadScene(toolsSceneLoad);
            var fablesMapConfig = toolsSceneModel.GetComponent<FablesMapController>();
            fablesMapConfig.InitializeFable(FablesInput.Mode.Editor, chapterData, 0);

            Timing.RunCoroutine(TransitionController.TransitionOut(TransitionController.TransitionType.Fade));
        }

        private async void OnOpenNodeMetadataEditor(IMessage message)
        {
            if (message.Data is OpenNodeMetadataEditorEventArgs args)
            {
                if (SceneController.IsLoaded("Tools")) return;

                var toolsSceneLoad = new SceneLoadParams
                {
                    SceneToLoad = "Tools"
                };

                await SceneController.Instance.LoadScene(toolsSceneLoad);
            }
        }

        private void AddHexGridEditorListeners()
        {
            Events.Subscribe(EventType.OpenNodeMetadataEditor, OnOpenNodeMetadataEditor);
        }

        private void RemoveHexGridEditorListeners()
        {
            Events.Unsubscribe(EventType.OpenNodeMetadataEditor, OnOpenNodeMetadataEditor);
        }
    }
}