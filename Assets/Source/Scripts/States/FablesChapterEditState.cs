using System.Threading.Tasks;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Developer.Tools;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Grid;
using CrossBlitz.Fables.Tools;
using CrossBlitz.InputAPI;
using CrossBlitz.Transition;
using MEC;

namespace CrossBlitz.States
{
    public class FablesChapterEditState : State
    {
        // private FableChapterEditor fableChapterEditor;
        // public override void Enter()
        // {
        //     OpenFablesChapterEditor();
        // }
        //
        // private async void OpenFablesChapterEditor()
        // {
        //     //if (SceneController.IsLoaded("Tools")) return;
        //
        //     var toolsSceneLoad = new SceneLoadParams
        //     {
        //         SceneToLoad = "Tools"
        //     };
        //
        //     var toolsSceneModel = await SceneController.Instance.LoadScene(toolsSceneLoad);
        //     fableChapterEditor = toolsSceneModel.GetView<FableChapterEditor>("FableChapterEditor");
        //     fableChapterEditor.SetActive(true);
        //     fableChapterEditor.OnEditMap += OnEditMap;
        //     fableChapterEditor.Open(new DatabaseEditorOpenOptions());
        //
        //     Timing.RunCoroutine(TransitionController.FadeFromBlack());
        // }
        //
        // private async void OnEditMap(FableChapterData chapterData, int mapIndex)
        // {
        //     fableChapterEditor.OnEditMap -= OnEditMap;
        //
        //     Timing.RunCoroutine(TransitionController.FadeToBlack());
        //
        //     await Task.Delay(1000);
        //
        //     await SceneController.Instance.UnloadScene("Tools");
        //
        //     var toolsSceneLoad = new SceneLoadParams
        //     {
        //         SceneToLoad = "FablesMap"
        //     };
        //
        //     var toolsSceneModel = await SceneController.Instance.LoadScene(toolsSceneLoad);
        //     var fablesMapConfig = toolsSceneModel.GetComponent<FablesMapController>();
        //     fablesMapConfig.InitializeFable(FablesInput.Mode.Editor, chapterData, 0);
        //
        //     Timing.RunCoroutine(TransitionController.FadeFromBlack());
        //
        // }
    }
}