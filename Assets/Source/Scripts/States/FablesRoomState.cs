using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Grid;
using CrossBlitz.InputAPI;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.Transition;
using CrossBlitz.ViewAPI.Popups;
using MEC;
using TakoBoyStudios.Events;

namespace CrossBlitz.States
{
    public class FablesRoomState : State
    {
        private FableChapterData m_selectedChapter;

        public override void Enter()
        {
            Events.Subscribe(EventType.OnPlayBook, LoadFable);
            OpenFablesRoom();
        }

        public override void Exit()
        {
            Events.Unsubscribe(EventType.OnPlayBook, LoadFable);
        }

        private void LoadFable(IMessage message)
        {
            Timing.RunCoroutine(UnloadAndLoadFable());
        }

        private IEnumerator<float> UnloadAndLoadFable()
        {
            //yield return Timing.WaitUntilDone(TransitionController.TransitionIn(TransitionController.TransitionType.Hex));
            yield return Timing.WaitUntilDone(TransitionController.TransitionIn(TransitionController.TransitionType.Fade));
            yield return Timing.WaitUntilDone(SceneController.Instance.UnloadAllScenes().AsCoroutine());
            StateController.ChangeState(StateDefinition.FABLE_ENTER);
        }

        private async void OpenFablesRoom()
        {
            if (SceneController.IsLoaded("FablesMap"))
            {
                PopupController.Open(new PopupInfo
                {
                    style = PopupWindowStyle.Dialogue,
                    body = "There is already a map open, please exit that map before opening a new one."
                });

                return;
            }

            //Debug.Log("SAVEALL()");
            //App.SaveAll();

            m_selectedChapter = Db.FablesDatabase.GetChapterByChapterId(FableController.GetCurrentChapterId());

            var fableRoomLoadParams = new SceneLoadParams
            {
                SceneToLoad = "FablesMap"
            };

            var fableRoomScene = await SceneController.Instance.LoadScene(fableRoomLoadParams);
            var fablesMapConfig = fableRoomScene.GetComponent<FablesMapController>();
            fablesMapConfig.InitializeFable(FablesInput.Mode.Explore, m_selectedChapter);


            //Timing.RunCoroutine(TransitionController.TransitionOut(TransitionController.TransitionType.Fade));
            //Timing.RunCoroutine(TransitionOutFable());
        }

        private IEnumerator<float> TransitionOutFable()
        {
            yield return Timing.WaitForSeconds(0.15f);
           // yield return Timing.WaitUntilDone(TransitionController.TransitionOut(TransitionController.TransitionType.Hex));
            yield return Timing.WaitUntilDone(TransitionController.TransitionOut(TransitionController.TransitionType.Fade));
        }
    }
}