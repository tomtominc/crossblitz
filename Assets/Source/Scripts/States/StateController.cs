﻿using System;
using System.Collections.Generic;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.GameLogic.States;
using CrossBlitz.ClientAPI.States;
using CrossBlitz.States;
using Mono.CSharp;
using Sirenix.OdinInspector;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

public enum StateDefinition
{
    NONE,
    INITIALIZE,
    LOGIN,
    MAIN_MENU,
    GAME_LOBBY,
    GAME_MATCHMAKING,
    GAME_INITIALIZE,
    GAME_RO_SHAM_BO,
    GAME_MULLIGAN,
    GAME_START_GAME,
    GAME_PLAYER_START_TURN,
    GAME_PLAYER_UPDATE_TURN,
    GAME_WAIT_FOR_OPPONENT_TURN,
    GAME_COMBAT_PHASE,
    GAME_FINISHED,
    GAME_OPPONENT_START_TURN,
    GAME_OPPONENT_UPDATE_TURN,
    FABLES_CHAPTER_SELECT,
    FABLES_ROOM,
    FABLE_CHAPTER_EDIT,
    TOOLS_EDITOR,
    FABLE_ENTER,
    GAME_CUTSCENE
}

public class StateController : MonoBehaviour
{
    public static StateDefinition CurrentState;
    public static Dictionary<StateDefinition, State> States;

    public static StateDefinition NextState;

    private void Start()
    {
        States = new Dictionary<StateDefinition, State>
        {
            { StateDefinition.INITIALIZE, new AppInitializeState() },
            { StateDefinition.LOGIN, new LoginState() },
            { StateDefinition.MAIN_MENU, new MainMenuState() },
            { StateDefinition.GAME_LOBBY, new GameLobbyState() },
            { StateDefinition.GAME_MATCHMAKING, new GameStateMatchmaking() },
            { StateDefinition.GAME_INITIALIZE, new GameStateInitialize() },
            { StateDefinition.GAME_RO_SHAM_BO, new GameStateRoShamBo() },
            { StateDefinition.GAME_MULLIGAN, new GameStateMulligan() },
            { StateDefinition.GAME_PLAYER_START_TURN, new GameStateTurnStart() },
            { StateDefinition.GAME_PLAYER_UPDATE_TURN, new GameStatePlayerTurnUpdate() },
            { StateDefinition.GAME_COMBAT_PHASE, new GameCombatPhase() },
            { StateDefinition.GAME_OPPONENT_START_TURN, new GameOpponentStartTurn() },
            { StateDefinition.GAME_OPPONENT_UPDATE_TURN, new GameOpponentUpdateTurn() },
            { StateDefinition.GAME_FINISHED, new GameFinishedState() },
            { StateDefinition.GAME_CUTSCENE, new GameCutsceneState() },
            { StateDefinition.FABLES_ROOM, new FablesRoomState() },
            { StateDefinition.FABLE_CHAPTER_EDIT, new FablesChapterEditState() },
            { StateDefinition.TOOLS_EDITOR, new ToolsEditorState() },
            { StateDefinition.FABLE_ENTER, new FableEnterState() },
            { StateDefinition.FABLES_CHAPTER_SELECT, new FableChapterSelectState() },
            { StateDefinition.GAME_START_GAME, new GameStateStartMatch() }
        };

        ChangeState(StateDefinition.INITIALIZE);
    }

    private void OnDestroy()
    {
        if (States != null && States.ContainsKey(CurrentState))
        {
            States[CurrentState].Exit();
        }
    }

    private void Update()
    {
        if (States.ContainsKey(CurrentState))
        {
            States[CurrentState].Update();
        }
    }

    public static void ChangeState(StateDefinition state)
    {
        if (States.ContainsKey(CurrentState))
        {
            States[CurrentState].Exit();
        }

        CurrentState = state;

        Events.Publish(null, EventType.OnStateChanged, new OnStateChangedEventArgs { state = CurrentState });

        if (States.ContainsKey(CurrentState))
        {
            States[CurrentState].Enter();
        }
    }

    public static void GoToNextState()
    {
        if (NextState == StateDefinition.NONE)
        {
            Debug.LogError("You are trying to go to next state but it is NONE!");
            return;
        }

        ChangeState(NextState);
        NextState = StateDefinition.NONE;
    }

    public static T GetState<T>(StateDefinition stateDefinition) where T : State
    {
        return (T)States[stateDefinition];
    }

    [Button]
    public void ShowCurrentState()
    {
        Debug.Log(CurrentState);
    }
}