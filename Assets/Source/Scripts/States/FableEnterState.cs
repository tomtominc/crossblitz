using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Audio;
using CrossBlitz.Databases;
using CrossBlitz.Fables;
using CrossBlitz.Fables.Data;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.Transition;
using CrossBlitz.WorldMap;
using MEC;
using UnityEngine;

namespace CrossBlitz.States
{
    /// <summary>
    /// Enter this state after the player hits the play button in the fable screen
    /// The players book & chapter should already be set by this point.
    /// </summary>
    public class FableEnterState : State
    {
        private FableChapterData m_selectedChapter;
        //private ClientBookData m_clientBookData;
        // private ClientChapterData m_clientChapterData;
        private bool m_isAnimatingCutscene;
        private bool m_isAnimatingIllustration;
        private bool m_isExiting;

        public override void Enter()
        {
            if (!FableController.PlayerIsInAFable() || App.FableData.GetCurrentChapter() == null)
            {
                Debug.LogError($"No valid chapter was detected!!! {FableController.GetCurrentChapterId()}");
                CancelFromState();
                return;
            }

            m_isAnimatingCutscene = false;
            m_isAnimatingIllustration = false;
            m_isExiting = false;

            m_selectedChapter = Db.FablesDatabase.GetChapterByChapterId( FableController.GetCurrentChapterId() );
            //m_clientBookData = App.FableData.GetCurrentBook();
            // m_clientChapterData = m_clientBookData.Chapters.Find(chapter => chapter.ChapterNumber == m_selectedChapter.ChapterNumber);

            if (App.FableData.GetCurrentChapter().HasSeenIntro)
            {
                Timing.RunCoroutine(GoToFablesMapState());
            }
            else
            {
                Timing.RunCoroutine(OpenWorldMap());
            }
        }

        private async void CancelFromState()
        {
            await SceneController.Instance.UnloadAllScenes();

            StateController.ChangeState(StateDefinition.INITIALIZE);

            //await TransitionController.TransitionOut(TransitionController.TransitionType.Hex);
            await TransitionController.TransitionOut(TransitionController.TransitionType.Fade);
        }

        private IEnumerator<float> OpenWorldMap()
        {
            var worldMapLoadParams = new SceneLoadParams
            {
                SceneToLoad = "WorldMap"
            };

            var sceneLoadTask = SceneController.Instance.LoadScene(worldMapLoadParams);

            yield return Timing.WaitUntilDone(sceneLoadTask.AsCoroutine());

            // add events to react
            WorldMapController.Instance.OnFinishedAnimatingPath += OnFinishedAnimatingPath;
            WorldMapController.Instance.OnFinishedMovingPointerToDestination += OnFinishedMovingPointerToDestination;
            WorldMapController.Instance.ui.OnFinishedCutscene += OnFinishedCutscene;

            sceneLoadTask = SceneController.Instance.LoadScene(new SceneLoadParams {SceneToLoad = "Cutscene"});

            yield return Timing.WaitUntilDone(sceneLoadTask.AsCoroutine());

            // set the world map and animate in!
            WorldMapController.Instance.SetUpWorldMap(m_selectedChapter);

            //yield return Timing.WaitUntilDone(TransitionController.TransitionOut(TransitionController.TransitionType.Hex));
            yield return Timing.WaitUntilDone(TransitionController.TransitionOut(TransitionController.TransitionType.Fade));

            var cutscene = Db.CutsceneDatabase.GetCutscene(m_selectedChapter.IntroCutsceneUid);

            if (cutscene != null)
            {
                if (!string.IsNullOrEmpty(cutscene.StartingTrack) && cutscene.StartingTrack != "Keep Same")
                {
                    AudioController.PlaySong(cutscene.StartingTrack);
                }

                if (!string.IsNullOrEmpty(cutscene.StartingAmbient) && cutscene.StartingAmbient != "Keep Same")
                {
                    AmbientController.PlayAmbient(cutscene.StartingAmbient);
                }
            }


        }

        private IEnumerator<float> GoToFablesMapState()
        {
            if (m_isExiting)
            {
                yield break;
            }

            m_isExiting = true;
            App.FableData.GetCurrentChapter().SetHasSeenIntro(true);

            if ( SceneController.IsLoaded("WorldMap") )
            {
                //yield return Timing.WaitUntilDone(TransitionController.TransitionIn(TransitionController.TransitionType.Hex));
                yield return Timing.WaitUntilDone(TransitionController.TransitionIn(TransitionController.TransitionType.Fade));
                yield return Timing.WaitUntilDone(SceneController.Instance.UnloadScene("WorldMap").AsCoroutine());
            }

            if ( SceneController.IsLoaded("Cutscene") )
            {
                yield return Timing.WaitUntilDone(SceneController.Instance.UnloadScene("Cutscene").AsCoroutine());
            }

            // fables room will handle loading in the scene and transition.
            StateController.ChangeState(StateDefinition.FABLES_ROOM);
        }

        private void OnFinishedAnimatingPath()
        {
            WorldMapController.Instance.OnFinishedAnimatingPath -= OnFinishedAnimatingPath;

            if (!string.IsNullOrEmpty(m_selectedChapter.IntroCutsceneUid))
            {
                var cutscene = Db.CutsceneDatabase.GetCutscene(m_selectedChapter.IntroCutsceneUid);

                if (cutscene == null)
                {
                    Debug.LogError($"Cutscene {m_selectedChapter.IntroCutsceneUid} is null! skipping.");
                    return;
                }

                m_isAnimatingCutscene = true;

                WorldMapController.Instance.ui.PlayCutscene(cutscene);
            }
        }

        private void OnFinishedCutscene()
        {
            WorldMapController.Instance.ui.OnFinishedCutscene -= OnFinishedCutscene;

            m_isAnimatingCutscene = false;

            Timing.RunCoroutine(GoToFablesMapState());
        }

        private void OnFinishedMovingPointerToDestination()
        {
            WorldMapController.Instance.OnFinishedMovingPointerToDestination -= OnFinishedMovingPointerToDestination;

            if (m_isAnimatingCutscene) return;

            Timing.RunCoroutine(GoToFablesMapState());
        }
    }
}