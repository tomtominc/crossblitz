﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Asyncoroutine;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Databases;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI;
using CrossBlitz.Transition;
using GameDataEditor;
using MEC;
using UnityEngine;

namespace CrossBlitz.States
{
    public class LoginState : State
    {
        public override void Enter()
        {
            Timing.RunCoroutine(GoToLobbyState());
        }

        private IEnumerator<float> GoToLobbyState()
        {
            yield return Timing.WaitUntilDone(TransitionController.TransitionIn(TransitionController.TransitionType.Fade));

            if (SceneController.IsLoaded("Title"))
            {
                yield return Timing.WaitUntilDone(SceneController.Instance.UnloadScene("Title").AsCoroutine());
            }

            if (!App.Initialized)
            {
                yield return Timing.WaitUntilDone(App.InitWithDelays(0.2f));
            }

            StateController.GoToNextState();
        }
    }
}
