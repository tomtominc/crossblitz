﻿using CrossBlitz.AddressablesAPI;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Developer;
using CrossBlitz.Fables;
using CrossBlitz.ServerAPI;
using CrossBlitz.ViewAPI.TitleScreen;
using QFSW.QC.Actions;
using TakoBoyStudios.Events;
using UnityEngine;
using UnityEngine.SceneManagement;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.States
{
    public class AppInitializeState : State
    {
        private TitleScreen titleScreen;

        public override void Enter()
        {
            if (Server.ConnectionState != Server.State.CONNECTED)
            {
                Events.Subscribe(EventType.OnPlayBook, LoadFable);
                Server.OnServerConnectedAndReady += Server_OnServerConnectedAndReady;
                Server.Connect();
            }
            else
            {
                OpenTitleScreen();
            }

        }

        private void Server_OnServerConnectedAndReady(bool obj)
        {
            Debug.Log($"ON SERVER CONNECTED AND READY! {obj}");
            Server.OnServerConnectedAndReady -= Server_OnServerConnectedAndReady;

            if (obj)
            {
                OpenTitleScreen();
            }
        }

        private async void OpenTitleScreen()
        {
            await UnityAsync.Await.NextUpdate();

            var titleSceneLoadParams = new SceneLoadParams
            {
                SceneToLoad = "Title",
                LoadSceneMode = LoadSceneMode.Additive,
                Params = null
            };

            var titleScene = await SceneController.Instance.LoadScene(titleSceneLoadParams);

            titleScreen = titleScene.GetView<TitleScreen>("TitleScreen");
            titleScreen.OnContinueButton += ContinueToLogin;
            titleScreen.OnDeveloperToolsButton += ToolsEditor;

            //TerminalCommands.GetTotalRewardsFromBook("Redcroft", Faction.War);
        }

        private void ContinueToLogin()
        {
            RemoveListeners();
            StateController.NextState = StateDefinition.MAIN_MENU;
            StateController.ChangeState(StateDefinition.LOGIN);
        }

        private void ToolsEditor()
        {
            RemoveListeners();
            DeveloperTools.OpenToolMode.ChapterEditor = true;
            StateController.NextState = StateDefinition.TOOLS_EDITOR;
            StateController.ChangeState(StateDefinition.LOGIN);
        }

        private void LoadFable(IMessage message)
        {
            RemoveListeners();
            StateController.NextState = StateDefinition.FABLE_ENTER;
            StateController.ChangeState(StateDefinition.LOGIN);
        }

        public override void Exit()
        {
            base.Exit();
            RemoveListeners();
        }

        private void RemoveListeners()
        {
            if (titleScreen)
            {
                titleScreen.OnContinueButton -= ContinueToLogin;
                titleScreen.OnDeveloperToolsButton -= ToolsEditor;
            }

            Events.Unsubscribe(EventType.OnPlayBook, LoadFable);
        }
    }
}

