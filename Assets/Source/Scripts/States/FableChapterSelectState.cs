using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.Transition;
using MEC;
using TakoBoyStudios.Events;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.States
{
    public class FableChapterSelectState : State
    {
        public override void Enter()
        {
            AddListeners();
            LoadFableChapterSelectScene();
        }

        private async void LoadFableChapterSelectScene()
        {
            await SceneController.Instance.LoadScene(new SceneLoadParams{ SceneToLoad = "FableCharacterSelect"} );

            Events.Publish(this, EventType.OnHeroChanged,Db.HeroDatabase.GetHero(App.HeroData.GetCurrentHero()) );

            Timing.RunCoroutine(TransitionController.TransitionOut(TransitionController.TransitionType.Cross));
        }

        public override void Exit()
        {
            RemoveListeners();
        }

        private void LoadFable(IMessage message)
        {
            // Go Forward to Fables 
            StateController.NextState = StateDefinition.FABLE_ENTER;
            //Timing.RunCoroutine(GoToNextState(TransitionController.TransitionType.Hex));
            Timing.RunCoroutine(GoToNextState(TransitionController.TransitionType.Fade));
        }

        public override void ChangeStateFromEvent(IMessage message)
        {
            // Go Back to Main Menu
            if (message.Data is StateChangeEventArgs args)
            {
                StateController.NextState = args.state;
                Timing.RunCoroutine(GoToNextState(TransitionController.TransitionType.Fade));
            }
        }

        private IEnumerator<float> GoToNextState(TransitionController.TransitionType transitionType)
        {
            RemoveListeners();

            yield return Timing.WaitUntilDone(TransitionController.TransitionIn(transitionType));

            if (SceneController.IsLoaded("FableCharacterSelect"))
            {
                yield return Timing.WaitUntilDone(SceneController.Instance.UnloadScene("FableCharacterSelect").AsCoroutine());
            }

            StateController.GoToNextState();
        }

        private void AddListeners()
        {
            Events.Subscribe(EventType.ChangeState, ChangeStateFromEvent);
            Events.Subscribe(EventType.OnPlayBook, LoadFable);
        }

        private void RemoveListeners()
        {
            Events.Unsubscribe(EventType.ChangeState, ChangeStateFromEvent);
            Events.Unsubscribe(EventType.OnPlayBook, LoadFable);
        }
    }
}