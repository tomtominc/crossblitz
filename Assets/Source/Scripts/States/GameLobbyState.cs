﻿using CrossBlitz.AddressablesAPI;
using CrossBlitz.Transition;
using CrossBlitz.ViewAPI.Lobby;
using MEC;
using UnityEngine.SceneManagement;

namespace CrossBlitz.States
{
    public class GameLobbyState : State
    {
        private LobbyScreen _lobby;
        public override void Enter()
        {
            OpenLobbyScreen();
        }

        private async void OpenLobbyScreen()
        {
            var lobbyLoadParams = new SceneLoadParams
            {
                SceneToLoad = "GameLobby",
                LoadSceneMode = LoadSceneMode.Additive,
                Params = null
            };

            var lobbyScene = await SceneController.Instance.LoadScene(lobbyLoadParams);

            _lobby = lobbyScene.GetView<LobbyScreen>("GameLobbyScreen");
            _lobby.OnPlayGame += OnPlayGame;
            _lobby.Open();

            Timing.RunCoroutine(TransitionController.TransitionOut(TransitionController.TransitionType.Fade));
        }

        private void OnPlayGame()
        {
            _lobby.OnPlayGame -= OnPlayGame;
            StateController.ChangeState(StateDefinition.GAME_MATCHMAKING);
        }
    }
}
