using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Transition;
using MEC;
using TakoBoyStudios.Events;
using UnityEngine;
using UnityEngine.SceneManagement;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.States
{
    public class MainMenuState : State
    {
        public override void Enter()
        {
            AddListeners();
            OpenMainMenu();
        }

        private async void OpenMainMenu()
        {
            var mainMenuLoadParams = new SceneLoadParams
            {
                SceneToLoad = "MainMenu",
                LoadSceneMode = LoadSceneMode.Additive
            };

            var mainMenu = await SceneController.Instance.LoadScene(mainMenuLoadParams);

            Timing.RunCoroutine(TransitionController.TransitionOut(TransitionController.TransitionType.Fade));
        }

        public override void ChangeStateFromEvent(IMessage message)
        {
            if (message.Data is StateChangeEventArgs args)
            {
                Timing.RunCoroutine(GoToState(args.state));
            }
        }

        private IEnumerator<float> GoToState(StateDefinition state)
        {
            RemoveListeners();

            if (state == StateDefinition.INITIALIZE)
            {
                yield return Timing.WaitUntilDone(TransitionController.TransitionIn(TransitionController.TransitionType.Fade));
            }
            else
            {
                yield return Timing.WaitUntilDone(TransitionController.TransitionIn(TransitionController.TransitionType.Cross));
            }
            if (SceneController.IsLoaded("MainMenu"))
            {
                yield return Timing.WaitUntilDone(SceneController.Instance.UnloadScene("MainMenu").AsCoroutine());
            }

            StateController.ChangeState(state);
        }

        public override void Exit()
        {
            RemoveListeners();
        }

        private void AddListeners()
        {
            Events.Subscribe(EventType.ChangeState, ChangeStateFromEvent);
        }

        private void RemoveListeners()
        {
            Events.Unsubscribe(EventType.ChangeState, ChangeStateFromEvent);
        }
    }
}