﻿using TakoBoyStudios.Events;

public class State
{
    public virtual void Enter()
    {

    }

    public virtual void Update()
    {

    }

    public virtual void Exit()
    {

    }

    public virtual void ChangeStateFromEvent(IMessage message)
    {
    }
}
