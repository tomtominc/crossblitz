using System.Collections.Generic;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Databases;
using CrossBlitz.Developer.Tools;
using CrossBlitz.Items;
using CrossBlitz.Shop.Data;
using CrossBlitz.UI.Widgets;
using PlayFab.ClientModels;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Shop
{
    /// <summary>
    /// The shop details window populates a window with a layout group with the contents
    /// of the shop separated by categories.
    /// </summary>
    public class ShopDataInspector : MonoBehaviour
    {
        public RectTransform layout;
        public GameObject errorMessage;
        public LayoutItem categoryItemPrefab;
        public LayoutItem shopItemPrefab;
        public Button addItemButton;

        private bool _viewOnly;
        private ShopData _shopData;
        public ShopData ShopData => _shopData;
        private Dictionary<ShopCategory,LayoutItem> _createdCategories;
        private Dictionary<string, LayoutItem> _createdItems;

        private void Start()
        {
            if (addItemButton)
            {
                addItemButton.onClick.AddListener(OnAddItem);
            }
        }

        public void SetupShop(ShopData shopData, bool viewOnly, bool forceUpdate=false)
        {
            _viewOnly = viewOnly;
            _shopData = shopData;

            if (addItemButton)
            {
                addItemButton.SetActive(!viewOnly);
            }

            if (forceUpdate)
            {
                if (_createdCategories != null && _createdCategories.Count > 0)
                {
                    foreach (var category in _createdCategories)
                    {
                        Destroy(category.Value.gameObject);
                    }

                    _createdCategories = new Dictionary<ShopCategory, LayoutItem>();
                }

                if (_createdItems != null && _createdItems.Count > 0)
                {
                    foreach (var item in _createdItems)
                    {
                        Destroy(item.Value.gameObject);
                    }

                    _createdItems = new Dictionary<string, LayoutItem>();
                }
            }

            _createdCategories ??= new Dictionary<ShopCategory, LayoutItem>();
            _createdItems ??= new Dictionary<string, LayoutItem>();

            if (_shopData == null || string.IsNullOrEmpty(_shopData.Uid))
            {
                errorMessage.SetActive(true);
                return;
            }

            errorMessage.SetActive(false);

            // foreach (var shopCategory in _shopData.items)
            // {
            //     LayoutItem categoryItem;
            //
            //     if (_createdCategories.ContainsKey(shopCategory.category))
            //     {
            //         categoryItem = _createdCategories[shopCategory.category];
            //     }
            //     else
            //     {
            //         categoryItem =Instantiate(categoryItemPrefab, layout, false);
            //
            //         if (addItemButton != null && layout.childCount >= 2)
            //         {
            //             categoryItem.transform.SetSiblingIndex(layout.childCount-2);
            //         }
            //
            //         categoryItem.SetLayoutItem(
            //             new LayoutItemData
            //             {
            //                 title = $"{shopCategory.category} Category",
            //                 sideIconAnimation = $"{shopCategory.category.ToString().ToLower()}-light",
            //                 data = shopCategory.category
            //             });
            //
            //         if (!_viewOnly)
            //         {
            //             categoryItem.OnRightToggleValueChanged -= OnRightValueToggleChanged;
            //             categoryItem.OnRightToggleValueChanged += OnRightValueToggleChanged;
            //         }
            //     }
            //
            //     categoryItem.rightToggle.isOn = shopCategory.enabled;
            //     categoryItem.rightToggle.SetActive(!_viewOnly);
            //
            //     if(!_createdCategories.ContainsKey(shopCategory.category))
            //     {
            //         _createdCategories.Add(shopCategory.category, categoryItem);
            //     }
            //
            //     if (!shopCategory.enabled) continue;
            //
            //     for (var i = 0; i < shopCategory.items.Count; i++)
            //     {
            //         var itemId = shopCategory.items[i];
            //
            //         if (itemId == null || string.IsNullOrEmpty(itemId))
            //         {
            //             Debug.LogError($"An item in shop category {shopCategory} is not linked to a PlayFab item.");
            //             continue;
            //         }
            //
            //         if (Db.ItemDatabase.GetItem(itemId)==null)
            //         {
            //             Debug.LogError($"There is no item with ID {itemId} in the database.");
            //             continue;
            //         }
            //
            //         var playFabItem = Db.ItemDatabase.GetItem(itemId);
            //
            //         LayoutItem itemContainer;
            //
            //         if (_createdItems.ContainsKey(itemId))
            //         {
            //             itemContainer = _createdItems[itemId];
            //         }
            //         else
            //         {
            //             itemContainer=Instantiate(shopItemPrefab, categoryItem.transform, false);
            //
            //             itemContainer.SetLayoutItem(new LayoutItemData
            //             {
            //                 title = playFabItem.DisplayName,
            //                 subTitle = playFabItem.Description,
            //                 data = playFabItem
            //             });
            //
            //             itemContainer.OnInfoButtonClicked -= OnItemInfoClicked;
            //             itemContainer.OnInfoButtonClicked += OnItemInfoClicked;
            //
            //             if (!_viewOnly)
            //             {
            //                 itemContainer.OnRightButtonClicked -= OnItemDeleted;
            //                 itemContainer.OnRightButtonClicked += OnItemDeleted;
            //             }
            //         }
            //
            //         itemContainer.infoButton.SetActive(true);
            //         itemContainer.rightButton.SetActive(!_viewOnly);
            //
            //         if (!_createdItems.ContainsKey(itemId))
            //         {
            //             _createdItems.Add(itemId, itemContainer);
            //         }
            //     }
            //
            //     LayoutRebuilder.ForceRebuildLayoutImmediate(categoryItem.RectTransform());
            // }

            // if (addItemButton)
            // {
            //     addItemButton.transform.parent.parent.SetSiblingIndex(layout.childCount - 1);
            // }
        }

        private void OnRightValueToggleChanged(bool isOn, LayoutItem layoutItem)
        {
            // if (layoutItem.Data.data is ShopCategory category)
            // {
            //     var itemCategory = _shopData.items.Find(c => c.category == category);
            //
            //     if (itemCategory == null)
            //     {
            //         itemCategory = new ShopCategoryData {category = category, items = new List<string>()};
            //         _shopData.items.Add(itemCategory);
            //     }
            //
            //     itemCategory.enabled = true;
            //
            //     SetupShop(_shopData, _viewOnly);
            // }
        }

        private void OnItemInfoClicked(LayoutItem item)
        {
            Debug.Log("We need an inspector window!");
        }

        private void OnItemDeleted(LayoutItem item)
        {
            // if (item.Data.data is CatalogItem catalogItem)
            // {
            //     DestroyItemView(catalogItem.ItemId);
            //     Db.ShopsDatabase.DeleteItem( ShopData.Uid, ShopsDatabase.GetShopCategoryFromItemType[catalogItem.ItemClass], catalogItem.ItemId );
            //     SetupShop(ShopData, _viewOnly);
            // }
            // else
            // {
            //     Debug.LogError("Data set in item is not a catalog item!");
            // }
        }

        private void DestroyItemView(string itemId)
        {
            if (_createdItems.ContainsKey(itemId))
            {
                var itemView = _createdItems[itemId];
                _createdItems.Remove(itemId);

                if (itemView != null)
                {
                    Destroy(itemView.gameObject);
                }
            }
        }

        private void OnAddItem()
        {
            // ToolsNavigator.Instance.OpenItemDatabase(new DatabaseEditorOpenOptions { linkOptionEnabled = true, itemContainerLink = ShopData.Uid });
            // ToolsNavigator.Instance.OnItemAdded -= ItemLinked;
            // ToolsNavigator.Instance.OnItemAdded += ItemLinked;
        }

        private void ItemLinked(ShopCategory category, string playFabId)
        {
            // Db.ShopsDatabase.AddItem(ShopData.Uid,category, playFabId);
            // SetupShop(ShopData, _viewOnly);
            // Debug.Log($"item linked? {playFabId}");
        }
    }
}