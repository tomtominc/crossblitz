using CrossBlitz.Card;
using DG.Tweening;
using MEC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BlendMode = BlendModes.BlendMode;

namespace CrossBlitz
{
    public class ShopItemPurchaseCard : MonoBehaviour
    {
        public RectTransform cardLayoutRect;
        public CanvasGroup cardLayoutCanvas;

        public SpriteAnimation bigSparkleAnimation;

        private CardView _loadedCard;
        private Color _burstColor;

        public void Setup(CardView cardview)
        {
            _loadedCard = cardview;
            Timing.RunCoroutine(Play());
        }

        private IEnumerator<float> Play()
        {
            // Move Card Up
            //cardLayoutRect.DOAnchorPos(Vector2.up * 250, 0.4f).SetEase(Ease.InQuart);

            // Scale Card
            //cardLayoutRect.DOScale(1.25f, 0.4f);
            //yield return Timing.WaitForSeconds(0.2f);

            // Fade Card
            //cardLayoutCanvas.DOFade(0f, 0.2f); 
            //yield return Timing.WaitForSeconds(0.2f);

            // Tint card and spark

            //ColorUtility.TryParseHtmlString("#E4DCD", out _burstColor);
            //_loadedCard.TintThenUntintOverTime(_burstColor, BlendMode.Overlay, 0.15f,0.1f,0.15f);
            bigSparkleAnimation.SetActive(true);
            bigSparkleAnimation.Play("card-burst",() => bigSparkleAnimation.SetActive(false));

            yield return Timing.WaitForSeconds(2f);

            Destroy(gameObject);

        }
    }
}
