using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Utils;

namespace CrossBlitz.Shop.Data
{
    public enum ShopCategory
    {
        Specials,
        Cards,
        Expansions,
        Resources,
        Style
    }

    [System.Serializable]
    public class ShopData : UniqueItem
    {
        public string displayName;
        public string merchantId;
        public List<string> items;

        public void Init(){}

        public void SetupBaseValues()
        {
            UpdateIdentifier();

            if (string.IsNullOrEmpty(displayName)) displayName = $"NewShop#{Uid}";
            if (string.IsNullOrEmpty(merchantId)) merchantId = "shopkeeper";
            items ??= new List<string>();
        }

        public bool HasItem(string itemId)
        {
            return items.Find(item => item == itemId) != null;
        }
    }

}