using System.Collections.Generic;

namespace CrossBlitz.Shop.Data
{
    [System.Serializable]
    public class ShopCategoryData
    {
        public bool enabled;
        public ShopCategory category;
        public List<string> items;
    }
}