using System;
using CrossBlitz.Databases;
using CrossBlitz.Developer.Tools;

namespace CrossBlitz.Shop
{
    public class ShopEditor : DatabaseEditorView
    {
        public ShopDatabaseTable shopDatabaseTable;
        public event Action<string> OnLinked;
        private bool _viewOnly;
        private bool _showLinkOption;

        private void Start()
        {
            shopDatabaseTable.OnLinkedShop += ShopLinked;
        }

        private void OnDisable()
        {
            Db.ShopsDatabase.Save();
        }

        public override void Open()
        {
            _viewOnly = _options.viewOnly;
            _showLinkOption = _options.linkOptionEnabled;
            shopDatabaseTable.Init(string.Empty, _viewOnly, _showLinkOption);
            //popup.Open();
        }

        private void ShopLinked(string shopUid)
        {
            OnLinked?.Invoke(shopUid);
        }
    }
}