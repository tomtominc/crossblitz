using System;
using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.UI.Widgets;
using CrossBlitz.ViewAPI.Popups;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Shop
{
    public class ShopDatabaseTable : MonoBehaviour
    {
        public GameObject errorMessage;
        public RectTransform shopsLayout;
        public LayoutItem shopDatabaseItemPrefab;
        public Button addShopButton;
        public GameObject noSelectionWindow;
        public ShopDataInspector shopDataInspector;

        private bool _viewOnly;
        private bool _showLinkOption;
        private string _selectedShop;

        private Dictionary<string, LayoutItem> _createdLayoutItems;
        private ToggleGroup _toggleGroup;

        public event Action<string> OnLinkedShop;

        private void Start()
        {
            addShopButton.onClick.AddListener(AddShopClicked);
        }

        public void Init(string selectedShop, bool viewOnly, bool showLinkOption, bool forceUpdateShop = false)
        {
            _selectedShop = selectedShop;
            _viewOnly = viewOnly;
            _showLinkOption = showLinkOption;

            if (_toggleGroup == null) _toggleGroup = shopsLayout.GetComponent<ToggleGroup>();

            addShopButton.SetActive(!_viewOnly);
            noSelectionWindow.SetActive(string.IsNullOrEmpty(_selectedShop));
            shopDataInspector.SetActive(!string.IsNullOrEmpty(_selectedShop));

            _createdLayoutItems ??= new Dictionary<string, LayoutItem>();

            if (Db.ShopsDatabase.Shops.Count <= 0)
            {
                errorMessage.SetActive(true);
                return;
            }

            errorMessage.SetActive(false);

            foreach (var shop in Db.ShopsDatabase.Shops)
            {
                LayoutItem shopDatabaseItem;

                if (_createdLayoutItems.ContainsKey(shop.Uid))
                {
                    shopDatabaseItem = _createdLayoutItems[shop.Uid];
                }
                else
                {
                    shopDatabaseItem = Instantiate(shopDatabaseItemPrefab, shopsLayout, false);

                    if (shopsLayout.childCount >= 2)
                    {
                        shopDatabaseItem.transform.SetSiblingIndex(shopsLayout.childCount - 2);
                    }

                    shopDatabaseItem.mainButton.Toggle.group = _toggleGroup;
                    shopDatabaseItem.OnMainButtonValueChanged += OnShopToggled;

                    if (!_viewOnly)
                    {
                        shopDatabaseItem.OnDeleteButtonClicked += OnDeleteButton;
                    }

                    if (_showLinkOption)
                    {
                        shopDatabaseItem.OnRightButtonClicked += OnLinkButton;
                    }

                    shopDatabaseItem.OnTitleChanged += OnChangedStoreName;
                }

                shopDatabaseItem.SetLayoutItem( new LayoutItemData
                {
                    title = shop.displayName,
                    subTitle = shop.Uid,
                    data = shop.Uid
                });

                if (!string.IsNullOrEmpty( shop.Uid ) && shop.Uid == _selectedShop)
                {
                    shopDatabaseItem.mainButton.Toggle.isOn = true;
                    shopDataInspector.SetActive(true);
                    shopDataInspector.SetupShop(shop,_viewOnly, forceUpdateShop);
                }
                else
                {
                    shopDatabaseItem.mainButton.Toggle.isOn = false;
                }

                if (!_viewOnly)
                {
                    shopDatabaseItem.deleteButton.SetActive(true);
                }

                if (_showLinkOption)
                {
                    shopDatabaseItem.rightButton.SetActive(true);
                }

                if (!_createdLayoutItems.ContainsKey(shop.Uid))
                {
                    _createdLayoutItems.Add(shop.Uid, shopDatabaseItem);
                }
            }
        }

        private void OnDeleteButton(LayoutItem shopItem)
        {
            _selectedShop = (string)shopItem.Data.data;

            PopupController.Open(new PopupInfo
            {
                title = "Delete Shop?",
                body = "Are you sure you want to delete this shop? This action will be permanent after you save.",
                deny = "Keep Shop",
                confirm = "Delete Shop"
            });

            PopupController.OnPopupChoice += OnDeleteActionResponse;
        }

        private void OnDeleteActionResponse(bool delete)
        {
            if (delete)
            {
                Db.ShopsDatabase.DeleteShop(_selectedShop);
                Init(string.Empty, _viewOnly, _showLinkOption);
            }
        }

        private void OnLinkButton(LayoutItem shopItem)
        {
            OnLinkedShop?.Invoke((string)shopItem.Data.data);
        }

        private void AddShopClicked()
        {
            var shopData = Db.ShopsDatabase.CreateNewShop();
            _selectedShop = shopData.Uid;
            Init(_selectedShop, _viewOnly, _showLinkOption);
        }

        private void OnShopToggled(bool isOn, LayoutItem shopItem)
        {
            var shopUid = (string) shopItem.Data.data;
            var forceUpdate = isOn && !string.IsNullOrEmpty(shopUid) && shopUid != _selectedShop;

            _selectedShop = isOn ? shopUid : string.Empty;
            Init(_selectedShop, _viewOnly, _showLinkOption, forceUpdate);
        }

        private void OnChangedStoreName(string newName, LayoutItem shopItem)
        {
            var shopUid = (string) shopItem.Data.data;
            Db.ShopsDatabase.ChangeShopName(shopUid, newName);
            Init(_selectedShop, _viewOnly, _showLinkOption);
        }
    }
}