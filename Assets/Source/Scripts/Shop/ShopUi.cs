using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Dialogue;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.Fables.Data;
using CrossBlitz.InputAPI;
using CrossBlitz.Items.Data;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.Utils;
using CrossBlitz.ViewAPI.Popups;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using TakoBoyStudios;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace CrossBlitz.Shop
{
    public class ShopUi : InputMode
    {
        [BoxGroup("Dialogue")] [TextArea] public List<string> enterDialogue;
        [BoxGroup("Dialogue")] [TextArea] public List<string> selectCardDialogue;
        [BoxGroup("Dialogue")] [TextArea] public List<string> purchaseDialogue;
        [BoxGroup("Dialogue")] [TextArea] public List<string> leaveDialogue;
        [BoxGroup("Dialogue")] [TextArea] public List<string> notEnoughCurrencyDialogue;
        [BoxGroup("Dialogue")] [TextArea] public List<string> outOfStockDialogue;


        [BoxGroup("Shop Pages")] public PageBehaviour shopPage;
        [BoxGroup("Shop Pages")] public ShopPageItem shopPageItem;
        [BoxGroup("Shop Pages")] public ToggleGroup toggleGroup;

        [BoxGroup("Shop Categories")] public Button cardsCategoryToggle;
        [BoxGroup("Shop Categories")] public FlashWhiteEffect cardCategoryOutline;
        [BoxGroup("Shop Categories")] public Button relicsCategoryToggle;
        [BoxGroup("Shop Categories")] public FlashWhiteEffect relicsCategoryOutline;

        [BoxGroup("Funds")] public ShakeContainer fundsContainer;

        [BoxGroup("Purchase Window")] public GameObject selectItemInstructions;
        [BoxGroup("Purchase Window")] public GameObject costContainer;
        [BoxGroup("Purchase Window")] public TextMeshProUGUI costAmount;
        [BoxGroup("Purchase Window")] public Button buyButton;
        [BoxGroup("Purchase Window")] public SpriteAnimation buyButtonFlash;
        [BoxGroup("Purchase Window")] public SpriteAnimation buyButtonDollars;
        [BoxGroup("Purchase Window")] public CanvasGroup buyButtonDollarsCanvas;

        [BoxGroup("Shopkeeper Custom Dialogue")] public DialogueBox shopKeeperDialogue;
        [BoxGroup("Shopkeeper Custom Dialogue")] public RectTransform newDialogueSpeechBubble;
        [BoxGroup("Shopkeeper Custom Dialogue")] public Button shopKeeperPlayDialogueButton;

        [BoxGroup("Misc")] public CanvasGroup canvasGroup;
        [BoxGroup("Misc")] public RectTransform exitButtonContainer;
        [BoxGroup("Misc")] public Button exitButton;

        [BoxGroup("Animation")] public RectTransform shopHeader;
        [BoxGroup("Animation")] public SpriteAnimation shopHeaderSprite;
        [BoxGroup("Animation")] public RectTransform shopContainer;
        [BoxGroup("Animation")] public SpriteAnimation shopContainerSprite;
        [BoxGroup("Animation")] public RectTransform dawnDollarFundsContainer;
        [BoxGroup("Animation")] public CanvasGroup background;
        [BoxGroup("Animation/Timings")] public float delayBeforeOpen=0.5f;
        [BoxGroup("Animation/Timings")] public float durationMultiplier = 1;
        [BoxGroup("Animation/Properties")] public Vector3 headerPunchRotationAxis = new Vector3(0,0,1);

        private int _lastCardsPage;
        private int _lastRelicsPage;

        private ShopPageItem _current;
        private int _current_index;
        private List<ItemValue> _itemList;
        private CoroutineHandle openHandle;

        private ClientBookData _bookData;
        private ShopType m_shopType;

        private Vector2 shopHeaderOriginalPos;
        private Vector2 shopContainerOriginalPos;
        private Vector2 dawnDollarFundsOriginalPos;

        private float _newDialogueSpeechBubbleSine;

        private bool _init;
        private bool _canEscape;
        private string _shopkeeperName;
        private bool _purchaseBuffer;

        private void Init()
        {
            if (_init) return;

            _init = true;

            shopHeaderOriginalPos = shopHeader.anchoredPosition;
            shopContainerOriginalPos = shopContainer.anchoredPosition;
            dawnDollarFundsOriginalPos = dawnDollarFundsContainer.anchoredPosition;

            buyButton.onClick.RemoveAllListeners();
            buyButton.onClick.AddListener(OnBuyItem);

            //selectItemInstructions.SetActive(true);
            //costContainer.SetActive(false);
            buyButton.interactable = false;
            _purchaseBuffer = false;

            exitButton.onClick.RemoveAllListeners();
            exitButton.onClick.AddListener(OnExitButton);

            cardsCategoryToggle.onClick.RemoveAllListeners();
            cardsCategoryToggle.onClick.AddListener(OnCardsCategoryToggle);

            relicsCategoryToggle.onClick.RemoveAllListeners();
            relicsCategoryToggle.onClick.AddListener(OnRelicsCategoryToggle);

            shopKeeperPlayDialogueButton.onClick.RemoveAllListeners();
            shopKeeperPlayDialogueButton.onClick.AddListener(OnShopKeeperShowCustomDialogue);

            shopPage.OnPageChanged -= OnPageChanged;
            shopPage.OnPageChanged += OnPageChanged;
        }

        private void OnShopKeeperShowCustomDialogue()
        {

        }

        public IEnumerator<float> Open(List<ItemValue> items, ShopType shopType)
        {
            if (items == null)
            {
                items = new List<ItemValue>();
            }

            Init();

            _canEscape = false;

            App.FableData.OnEnteredShop();

            // setup animation
            background.alpha = 0;

            shopHeader.anchoredPosition = new Vector2(shopHeaderOriginalPos.x, shopHeader.sizeDelta.y);
            shopContainer.anchoredPosition = new Vector2(shopContainerOriginalPos.x, -400);
            dawnDollarFundsContainer.anchoredPosition = new Vector2(dawnDollarFundsOriginalPos.x-150, dawnDollarFundsOriginalPos.y);
            exitButtonContainer.anchoredPosition = new Vector2(exitButtonContainer.sizeDelta.x, exitButtonContainer.anchoredPosition.y);

            shopKeeperDialogue.SetActive(false);

            gameObject.SetActive(true);
            m_shopType = shopType;
            _itemList = items;
            _itemList.RemoveAll(item => string.IsNullOrEmpty(item.ItemUid));

            _bookData = App.FableData.GetCurrentBook();

            switch (m_shopType)
            {
                case ShopType.Card:
                    AudioController.PlaySong("cardinellas-shop");
                    cardsCategoryToggle.SetActive(false);
                    relicsCategoryToggle.SetActive(false);
                    relicsCategoryOutline.SetActive(false);

                    AnimateButtonAndFlash(cardsCategoryToggle, cardCategoryOutline);
                    shopKeeperDialogue.portrait.Play("cardinella");
                    shopContainerSprite.Play("cards");
                    shopHeaderSprite.Play("card-shop");

                    _shopkeeperName = "Cardinella";
                    break;

                case ShopType.Relic:
                    AudioController.PlaySong("shop");
                    cardsCategoryToggle.SetActive(false);
                    relicsCategoryToggle.SetActive(false);
                    cardCategoryOutline.SetActive(false);

                    AnimateButtonAndFlash(relicsCategoryToggle, relicsCategoryOutline);
                    shopKeeperDialogue.portrait.Play("scais");
                    shopContainerSprite.Play("relics");
                    shopHeaderSprite.Play("relic-shop");
                    shopKeeperDialogue.portrait.image.SetNativeSize();

                    _shopkeeperName = "scias";

                    break;
            }


            var card = Db.CharacterDatabase.GetCharacter(_shopkeeperName);
            enterDialogue.Clear();
            purchaseDialogue.Clear();
            notEnoughCurrencyDialogue.Clear();
            selectCardDialogue.Clear();
            leaveDialogue.Clear();
            outOfStockDialogue.Clear();

            // Set up ShopKeeper
            var snippetsShopEnter = card.dialogueSnippets.FindAll(d => d.Option == "Shop/Enter");
            if (snippetsShopEnter.Count > 0)
            {
                snippetsShopEnter.ForEach(d => enterDialogue.Add(d.Dialogue));
            }

            var snippetsShopPurchase = card.dialogueSnippets.FindAll(d => d.Option == "Shop/Purchase");
            if (snippetsShopPurchase.Count > 0)
            {
                snippetsShopPurchase.ForEach(d => purchaseDialogue.Add(d.Dialogue));
            }

            var snippetsFunds = card.dialogueSnippets.FindAll(d => d.Option == "Shop/Insufficent Funds");
            if (snippetsFunds.Count > 0)
            {
                snippetsFunds.ForEach(d => notEnoughCurrencyDialogue.Add(d.Dialogue));
            }

            var snippetsSelect = card.dialogueSnippets.FindAll(d => d.Option == "Shop/Item Click");
            if (snippetsSelect.Count > 0)
            {
                snippetsSelect.ForEach(d => selectCardDialogue.Add(d.Dialogue));
            }

            var snippetsLeave = card.dialogueSnippets.FindAll(d => d.Option == "Shop/Exit");
            if (snippetsLeave.Count > 0)
            {
                snippetsLeave.ForEach(d => leaveDialogue.Add(d.Dialogue));
            }

            var snippetsOutOfStock= card.dialogueSnippets.FindAll(d => d.Option == "Shop/OutOfStock");
            if (snippetsOutOfStock.Count > 0)
            {
                snippetsOutOfStock.ForEach(d => outOfStockDialogue.Add(d.Dialogue));
            }

            shopKeeperDialogue.portrait.image.SetNativeSize();
            _lastCardsPage = 0;

            RefreshStore(_bookData,-1);

            // ------ animation stuff! ------

            yield return Timing.WaitForSeconds(delayBeforeOpen);

            background.DOFade(1, 0.3f * durationMultiplier);

            shopHeader.DOAnchorPos(shopHeaderOriginalPos, 0.4f* durationMultiplier)
                .SetEase(Ease.OutBounce).OnComplete(() =>
                {
                    shopHeader.DOPunchRotation(headerPunchRotationAxis * -40f, 0.4f* durationMultiplier);
                });

            shopContainer.DOAnchorPos(shopContainerOriginalPos, 0.6f* durationMultiplier).SetEase(Ease.OutBack);
            AudioController.PlaySound("ui_window_open", "BARDSFX", true);

            yield return Timing.WaitForSeconds(0.4f* durationMultiplier);

            dawnDollarFundsContainer.DOAnchorPos(dawnDollarFundsOriginalPos, 0.4f* durationMultiplier).SetEase(Ease.OutBack);

            //yield return Timing.WaitForSeconds(0.4f* durationMultiplier);

            exitButtonContainer.DOAnchorPosX(-52, 0.4f * durationMultiplier).SetEase(Ease.OutBack);
            shopKeeperDialogue.SetActive(true);
            shopKeeperDialogue.InitDialogueBox(null);

            Timing.RunCoroutine( shopKeeperDialogue.ShowText(new DialogueInfo
            {
                SkipPressedActions = true,
                Text = enterDialogue[Random.Range(0, enterDialogue.Count)],
                SkipAnimateIn = false
            }));

            _canEscape = true;
        }

        private IEnumerator<float> Close()
        {
            App.FableData.OnExitShop();

            exitButtonContainer.DOAnchorPosX(exitButtonContainer.sizeDelta.x, 0.4f * durationMultiplier)
                .SetEase(Ease.InBack);

            dawnDollarFundsContainer.DOAnchorPosX(-100f, 0.4f * durationMultiplier).SetEase(Ease.InBack);

            //ShowText(leaveDialogue[Random.Range(0,leaveDialogue.Count)]);

            //yield return Timing.WaitForSeconds(0.4f);

            yield return Timing.WaitUntilDone(shopKeeperDialogue.AnimateOut());


            //yield return Timing.WaitForSeconds(0.2f* durationMultiplier);

            shopHeader.DOAnchorPosY(shopHeader.sizeDelta.y, 0.4f * durationMultiplier).SetEase(Ease.InBack);
            shopContainer.DOAnchorPosY(-400f, 0.6f* durationMultiplier).SetEase(Ease.InBack);
            AudioController.PlaySound("ui_window_close", "BARDSFX", true);

            background.DOFade(0, 0.3f * durationMultiplier);

            FablesInput.Instance.deckViewInput.RefreshNewCardsLabel();

            // figure out which mode to go to!
            // cutscenes can happen after leaving a shop.. see if phil implements this though.
            FablesInput.Instance.GoToMode(FablesInput.Mode.Explore);

            yield return Timing.WaitForSeconds(0.6f * durationMultiplier);

            gameObject.SetActive(false);
        }

        private void RefreshStore(ClientBookData bookData, int chosenCard)
        {
            _bookData = bookData;

            var shownItems = new List<ClientStoreItemData>();

            for (var i = 0; i < _itemList.Count; i++)
            {
                _bookData.StoreData.AddItem(_itemList[i].ItemUid);
            }

            for (var i = _bookData.StoreData.Items.Count-1; i >= 0; i--)
            {
                var item = _bookData.StoreData.Items[i];
                var card = Db.CardDatabase.GetCard(item.ItemId);

                if (card == null)
                {
                  //  Debug.LogError($"Cant find card {item.ItemId}");
                    continue;
                }

                switch (m_shopType)
                {
                    case ShopType.Card: if (card.type == CardType.Relic || card.type == CardType.Elder_Relic) continue;
                        break;
                    case ShopType.Relic: if (card.type != CardType.Relic && card.type != CardType.Elder_Relic) continue;
                        break;
                }



                shownItems.Add(item);
            }

            var pageToDisplay = _lastCardsPage;

            shopPage.InitializePages(shopPageItem, 3, shownItems.Cast<IPageItemData>().ToList(), pageToDisplay);

            for (var i = 0; i < shopPage.Items.Count; i++)
            {
                //Debug.Log("I: " + i);
                var item = (ShopPageItem)shopPage.Items[i];
                item.itemToggle.Toggle.group = toggleGroup;
                item.OnClicked += OnShopItemSelected;

              //  Timing.RunCoroutine(item.UpdateNewCardStatus(item.Data.isNew));

                if (i == chosenCard)
                {
                    item.itemToggle.Toggle.isOn = true;
                    OnShopItemSelected(false, item);
                }
            }
        }


        private void OnShopItemSelected(bool isOn, ShopPageItem itemClicked)
        {
            _current = null;
            _current_index = -1;

            for (var i = 0; i < shopPage.Items.Count; i++)
            {
                var item = (ShopPageItem)shopPage.Items[i];

                if (item.itemToggle.Toggle.isOn)
                {
                    var itemData = Db.ItemDatabase.GetItem(item.Data.ItemId);
                    var price = itemData.GetCurrencyPrice(Currency.DAWN_DOLLARS);
                    var funds = App.FableData.GetDawnDollarAmount();
                    //var prefixColor = price > funds ? $"<color={ColorPalette.CrossBlitz.FactionColor.WAR}>" : string.Empty;
                    var prefixColor = price > funds ? string.Empty : string.Empty;

                    costAmount.text = $"{prefixColor}{price:N0}";
                    _current = item;
                    _current_index = i;
                }
            }

            if (_current)
            {

                var cardData = Db.CardDatabase.GetCard(_current.Data.ItemId);
                var totalAmountInInventory = App.Inventory.GetItemAmount(_current.Data.ItemId);
                var amountToSell = DeckData.GetMaxCopies(cardData.rarity) - totalAmountInInventory;

                if (amountToSell <= 0 || (cardData.type == CardType.Relic && totalAmountInInventory >= 1))
                {
                    buyButton.interactable = false;
                    ShowText(outOfStockDialogue[Random.Range(0, outOfStockDialogue.Count)]);
                }
                else
                {
                    if (_current.Data.isNew)
                    {
                        Timing.RunCoroutine(_current.UpdateNewCardStatus(false));
                        _current.Data.isNew = false;
                    }

                    buyButton.interactable = true;
                    ShowText(selectCardDialogue[Random.Range(0, selectCardDialogue.Count)]);
                }

                //selectItemInstructions.SetActive(false);
                //costContainer.SetActive(true);

            }
            else
            {
                //selectItemInstructions.SetActive(true);
                //costContainer.SetActive(false);
                buyButton.interactable = false;
                if(_shopkeeperName == "scias") ShowText("Feel free to browse for as long as you'd like.");
                else if(_shopkeeperName == "Cardinella") ShowText("Take your time!");
            }
        }

        private void OnBuyItem()
        {
            if (_current != null && !_purchaseBuffer)
            {
                var purchasedItem = App.Inventory.PurchaseItem(_current.Data.ItemId, 1, "DD", new ItemAcquisitionData
                {
                    Receipt = ItemAcquisitionData.GetReceipt(),
                    Annotation = ItemAcquisitionData.AnnotationReason.PURCHASED,
                    Purchased = true,
                    PurchasedCurrency = Currency.DAWN_DOLLARS
                }, out var errorCode);

                switch (errorCode)
                {
                    case PurchaseErrorCode.NONE:
                    {
                        _bookData = App.FableData.GetCurrentBook();
                        _bookData.StoreData.PurchasedItem(_current.Data.ItemId);

                        for (var i = 0; i < shopPage.Items.Count; i++)
                        {
                            var item = (ShopPageItem)shopPage.Items[i];
                            var cardData = Db.CardDatabase.GetCard(item.Data.ItemId);
                            if (cardData != null)
                            {
                                var totalAmountInInventory = App.Inventory.GetItemAmount(item._itemData.ItemId);
                                var amountToSell = DeckData.GetMaxCopies(cardData.rarity) - totalAmountInInventory;
                                bool inStock = !(amountToSell <= 0 || (cardData.type == CardType.Relic && totalAmountInInventory >= 1) || (cardData.type == CardType.Elder_Relic && totalAmountInInventory >= 1));

                                if ((item._itemData.ItemId != _current._itemData.ItemId) && inStock)
                                {
                                    var itemData = Db.ItemDatabase.GetItem(item._itemData.ItemId);

                                    var price = itemData.GetCurrencyPrice(Currency.DAWN_DOLLARS);
                                    var funds = App.FableData.GetDawnDollarAmount();
                                    var prefixColor = price > funds ? $"<color={ColorPalette.CrossBlitz.FactionColor.WAR}>" : string.Empty;
                                    var font = price > funds ? item.ThicketOutline : item.ThicketDawnDollars;

                                    item.itemCost.text = $"{prefixColor}{price:N0}";
                                    item.itemCost.font = font;
                                }
                            }
                        }


                        Timing.RunCoroutine(_current.ReduceStock());

                        // Buy Button Animation
                        AudioController.PlaySound("ui_buy", "BARDSFX", true, gameObject);
                        buyButtonDollars.SetActive(true);
                        buyButtonDollarsCanvas.alpha = 1f;
                        buyButtonDollars.Play("burst");
                        buyButtonDollarsCanvas.DOFade(0f, 0.1f).SetDelay(0.05f);

                        buyButtonFlash.SetActive(true);
                        buyButtonFlash.Play("burst", () => { _purchaseBuffer = false; buyButtonFlash.SetActive(false); buyButtonDollars.SetActive(false); });
                        _purchaseBuffer = true;

                        //App.HandleItemGrant(purchasedItem,null, errorCode);
                        ShowText(purchaseDialogue[Random.Range(0,purchaseDialogue.Count)]);

                        //OnShopItemSelected(false, null);
                        break;
                    }
                    case PurchaseErrorCode.NOT_ENOUGH_CURRENCY:
                    {
                        //App.HandleItemGrant(purchasedItem,null, errorCode, false);
                        fundsContainer.Shake(2, 0.5f);
                        _current.shakeContainer.Shake(2, 0.5f);
                        ShowText(notEnoughCurrencyDialogue[Random.Range(0,notEnoughCurrencyDialogue.Count)]);
                        break;
                    }
                    default:
                    {
                        //App.HandleItemGrant(purchasedItem,null, errorCode);
                        break;
                    }
                }
            }
        }

        private void OnPageChanged(int page)
        {
            _lastCardsPage = page;
        }

        private void OnCardsCategoryToggle()
        {
             //if (!_cardsCategoryOpen)
             //{
             //    _cardsCategoryOpen = true;
             //    relicsCategoryOutline.SetActive(false);
             //    AnimateButtonAndFlash(cardsCategoryToggle, cardCategoryOutline);
             //    RefreshStore(_bookData);
             //}
        }

        private void OnRelicsCategoryToggle()
        {
            // if (_cardsCategoryOpen)
            // {
            //     _cardsCategoryOpen = false;
            //     cardCategoryOutline.SetActive(false);
            //     AnimateButtonAndFlash(relicsCategoryToggle, relicsCategoryOutline);
            //     RefreshStore(_bookData);
            // }
        }

        private void AnimateButtonAndFlash(Button button, FlashWhiteEffect effect)
        {
            effect.SetActive(true);
            effect.Flash();

            effect.RectTransform().DOKill();
            effect.RectTransform().localScale = new Vector3(0.4f, 0.4f, 1f);
            effect.RectTransform().DOScale(1, 0.4f).SetEase(Ease.OutElastic);

            button.RectTransform().DOKill();
            button.RectTransform().DOPunchAnchorPos(Vector2.up * 2f, 0.24f);
        }

        private void ShowText(string text)
        {
            shopKeeperDialogue.dialogue
                .textAnimator.tmproText.text = text;
        }

        private void OnExitButton()
        {
            //todo: check for cutscenes and move into a cutscene if needed!

            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = true;

            Timing.RunCoroutine(Close());
        }

        public override void StartMode()
        {
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;
        }

        public override void UpdateMode()
        {
            if (newDialogueSpeechBubble.IsActive())
            {
                _newDialogueSpeechBubbleSine = Mathf.PingPong(Time.time * 2, 1);
                var y = EasingFunction.EaseOutQuad(18, 24, _newDialogueSpeechBubbleSine);
                newDialogueSpeechBubble.anchoredPosition = new Vector2(newDialogueSpeechBubble.anchoredPosition.x, y);
            }
        }

        public override void EndMode()
        {
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (!PopupController.IsOpen && _canEscape)
                {
                    _canEscape = false;
                    OnExitButton();
                    return;
                }
            }
        }
    }
}