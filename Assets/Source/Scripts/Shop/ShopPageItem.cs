using System;
using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.UI.Widgets;
using CrossBlitz.Utils;
using CrossBlitz.Card.Vfx;
using CrossBlitz.GameVfx.Minion;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.Shop
{
    public class ShopPageItem : PageItem
    {
        public CanvasGroup canvas;
        public SpriteAnimation itemFrame;
        public CanvasGroup PriceTagCanvas;
        public RectTransform PriceTagTransform;
        public GameObject itemCostContainer;
        public TextMeshProUGUI itemCost;
        public TMP_FontAsset Thicket2x;
        public TMP_FontAsset ThicketDawnDollars;
        public TMP_FontAsset ThicketOutline;
        public GameObject DawnDollarIcon;
        public GameObject EachIcon;
        public GameObject itemAmountContainer;
        public TextMeshProUGUI itemAmount;
        public ShakeContainer shakeContainer;

        public ToggleButton itemToggle;

        public GameObject cardPrefab;
        public RectTransform cardContainer;

        public FlashWhiteEffect outlineFlash;

        [BoxGroup("Spawn Animation")] public float itemSpawnDelay = 0.06f;
        [BoxGroup("Spawn Animation")] public Vector3 startingScale = new Vector3(1.5f, 1.5f, 1.5f);
        [BoxGroup("Spawn Animation")] public float startingHeight = 10;
        [BoxGroup("Spawn Animation")] public float scaleToDuration = 0.4f;
        [BoxGroup("Spawn Animation")] public Ease scaleEase=Ease.Linear;
        [BoxGroup("Spawn Animation")] public Ease moveToEase=Ease.Linear;
        [BoxGroup("Rotation")] public Vector3 rotationAxis=new Vector3(0,0,1);
        [BoxGroup("Rotation")] public float rotationPower= 20;
        [BoxGroup("Rotation")] public float rotationDuration=0.24f;
        [BoxGroup("Rotation")] public int rotationVibration=20;
        [BoxGroup("Rotation")] public Ease rotationEase=Ease.Linear;

        [BoxGroup("Color Overlay")] public Color color;
        [BoxGroup("Color Overlay")] public Color soldOutColor;
        [BoxGroup("Color Overlay")] public BlendMode blend;

        private CardView _card;
        public ClientStoreItemData _itemData;
        private bool _soldOut = false;
        public event Action<bool, ShopPageItem> OnClicked;

        [BoxGroup("Purchase")] public ShopItemPurchaseCard PurchasedCardObject;

        public ClientStoreItemData Data => _itemData;

        private void Start()
        {
            itemToggle.OnValueChanged += Clicked;
        }

        private void OnClickCard(ClickableCard cardView)
        {
            itemToggle.Toggle.isOn = !itemToggle.Toggle.isOn;
        }

        private void Clicked(bool isOn, string content)
        {
            if (isOn)
            {
                itemFrame.RectTransform().DOKill();
                itemFrame.RectTransform().DOPunchRotation(rotationAxis * ((Random.value > 0.5f ? -1 : 1) * rotationPower), rotationDuration, rotationVibration).SetEase(rotationEase);
                itemFrame.RectTransform().DOPunchScale(Vector3.one * 0.16f, 0.24f);

               // _card.shadow.OffsetDistance = 4;
                _card.RectTransform().DOKill();
                _card.RectTransform().DOAnchorPos(Vector2.zero, 0.3f).SetEase(Ease.OutBack);
                if(!_soldOut) _card.TintThenUntintOverTime(color, blend, 0.06f, 0.06f, 0.06f);

                outlineFlash.Flash();
                outlineFlash.RectTransform().DOKill();
                outlineFlash.RectTransform().localScale = new Vector3(0.8f, 0.8f, 1);
                outlineFlash.RectTransform().DOScale(1, 0.4f).SetEase(Ease.OutElastic);
            }
            else
            {
               // _card.shadow.OffsetDistance = 0;
                _card.RectTransform().DOKill();
                _card.RectTransform().DOAnchorPos(new Vector2(-2, -2), 0.3f).SetEase(Ease.OutBounce);
            }

            OnClicked?.Invoke(isOn,this);
        }

        public override void SetAsEmpty(int itemIndex)
        {
            canvas.alpha = 0;
            //itemFrame.Play("empty");
            //itemAmountContainer.SetActive(false);
            itemToggle.Toggle.isOn = false;
            itemToggle.Toggle.interactable = false;
            //itemCostContainer.SetActive(false);

            if (_card)
            {
                _card.SetActive(false);
                _card.RectTransform().DOKill();
            }

          //  var seq = DOTween.Sequence();

           // itemFrame.RectTransform().DOKill();
            //itemFrame.RectTransform().localScale = startingScale;
           // itemFrame.RectTransform().anchoredPosition = new Vector2(0,startingHeight);


            //seq.AppendInterval(itemSpawnDelay * itemIndex);
            //seq.AppendCallback(() => canvas.alpha = 1);
            //seq.Append(itemFrame.RectTransform().DOScale(1, scaleToDuration).SetEase(scaleEase));
            //seq.Join(itemFrame.RectTransform().DOAnchorPos(Vector2.zero, scaleToDuration).SetEase(moveToEase));
           // seq.Append(itemFrame.RectTransform().DOPunchRotation(rotationAxis * ((Random.value > 0.5f ? -1 : 1) * rotationPower), rotationDuration, rotationVibration).SetEase(rotationEase));
        }

        public void SetAsSoldOut(CardData cardData, int itemIndex)
        {
            canvas.alpha = 0;
            itemToggle.Toggle.isOn = false;
            itemToggle.Toggle.interactable = false;
            _soldOut = true;

            if (_card == null)
            {
                _card = CardFactory.Create(new CreateCardFactorySettings
                {
                    Layout = cardContainer,
                    CardData = cardData,
                    AdditionalComponents = new List<Type> {/*typeof(HoverCard),*/typeof(ClickableCard)},
                    CardPrefab = cardPrefab,
                    RemoveCanvas = true,
                    StartsDisabled = true,
                    CardObjectOffsetPosition = Vector2.zero
                }, null);

                var hoverCard = _card.GetCardComponent<ClickableCard>();
                hoverCard.OnRightClick += OnClickCard;
                hoverCard.OnLeftClick += OnClickCard;
            }
            else
            {
                _card.SetCardData(cardData);
            }

            _card.SetActive(true);
            _card.RectTransform().anchoredPosition = new Vector2(-2, -2);

            var seq = DOTween.Sequence();

            _card.RectTransform().DOKill();
            PriceTagTransform.DOKill();
            _card.CanvasGroup.DOKill(true);
            PriceTagCanvas.DOKill(true);
            _card.CanvasGroup.alpha = 0f;
            PriceTagCanvas.alpha = 0;

            //_card.TintThenUntintOverTime(color, blend, 0.06f, 0.06f, 0.06f);
            _card.Tint(soldOutColor, blend);

            seq.AppendInterval(itemSpawnDelay * itemIndex);
            seq.AppendCallback(() => canvas.alpha = 1);

            seq.Join(_card.CanvasGroup.DOFade(1, 0.1f));
            seq.Join(PriceTagCanvas.DOFade(1, 0.1f));

            seq.Join(_card.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, 0.4f).SetEase(Ease.OutBack));
            seq.Join(PriceTagTransform.DOPunchAnchorPos(Vector2.up * 6f, 0.4f).SetEase(Ease.OutBack));

            itemAmount.text = $"<color=#ae977a>x0</color>";
            itemCost.text = $"<color=#684054>SOLD OUT</color>";
            itemCost.font = Thicket2x;

            itemCostContainer.RectTransform().anchoredPosition = new Vector2(itemCostContainer.RectTransform().anchoredPosition.x, 1);
            EachIcon.SetActive(false);
            DawnDollarIcon.SetActive(false);

        }

        public override void SetData(IPageItemData data, int itemIndex)
        {
            _itemData = (ClientStoreItemData) data;

            var cardData = Db.CardDatabase.GetCard(_itemData.ItemId);
            var totalAmountInInventory = App.Inventory.GetItemAmount(_itemData.ItemId);
            var amountToSell = DeckData.GetMaxCopies(cardData.rarity) - totalAmountInInventory;

            if (amountToSell <= 0 || (cardData.type == CardType.Relic && totalAmountInInventory >= 1) || (cardData.type == CardType.Elder_Relic && totalAmountInInventory >= 1))
            {
                SetAsSoldOut(cardData, itemIndex);
                return;
            }

            canvas.alpha = 0;

            if (_card == null)
            {
                _card = CardFactory.Create(new CreateCardFactorySettings
                {
                    Layout = cardContainer,
                    CardData = cardData,
                    AdditionalComponents = new List<Type> {/*typeof(HoverCard),*/typeof(ClickableCard),typeof(CardObtainedEffectComponent) },
                    CardPrefab = cardPrefab,
                    RemoveCanvas = true,
                    StartsDisabled = true,
                    CardObjectOffsetPosition = Vector2.zero
                }, null);

                var hoverCard = _card.GetCardComponent<ClickableCard>();
                hoverCard.OnRightClick += OnClickCard;
                hoverCard.OnLeftClick += OnClickCard;
            }
            else
            {
                _card.SetCardData(cardData);
            }


            var seq = DOTween.Sequence();

            _card.RectTransform().DOKill();
            PriceTagTransform.DOKill();
            _card.CanvasGroup.DOKill(true);
            PriceTagCanvas.DOKill(true);
            _card.CanvasGroup.alpha = 0f;
            PriceTagCanvas.alpha = 0;

            seq.AppendInterval(itemSpawnDelay * itemIndex);
            seq.AppendCallback(() => canvas.alpha = 1);

            seq.Join(_card.CanvasGroup.DOFade(1, 0.1f));
            seq.Join(PriceTagCanvas.DOFade(1, 0.1f));

            seq.Join(_card.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, 0.4f).SetEase(Ease.OutBack));
            seq.Join(PriceTagTransform.DOPunchAnchorPos(Vector2.up * 6f, 0.4f).SetEase(Ease.OutBack));

            itemToggle.Toggle.isOn = false;

            _card.SetActive(true);
            _card.RectTransform().anchoredPosition = new Vector2(-2, -2);

            itemToggle.Toggle.interactable = true;

            var itemData = Db.ItemDatabase.GetItem(_itemData.ItemId);

            var price = itemData.GetCurrencyPrice(Currency.DAWN_DOLLARS);
            var funds = App.FableData.GetDawnDollarAmount();
            var prefixColor = price > funds ? $"<color={ColorPalette.CrossBlitz.FactionColor.WAR}>" : string.Empty;
            var font = price > funds ? ThicketOutline : ThicketDawnDollars;

            itemCost.text = $"{prefixColor}{price:N0}";
            itemCost.font = font;

            if (cardData.type == CardType.Relic || cardData.type == CardType.Elder_Relic)
            {
               // itemAmountContainer.SetActive(true);
                itemAmount.text = "<color=#ae977a>x</color><color=#e7edb5>1</color>";
            }
            else
            {
                //itemAmountContainer.SetActive(true);
                itemAmount.text = $"<color=#ae977a>x</color><color=#e7edb5>{amountToSell}</color>";
            }

            itemCostContainer.RectTransform().anchoredPosition = new Vector2(itemCostContainer.RectTransform().anchoredPosition.x, 2);
            EachIcon.SetActive(true);
            DawnDollarIcon.SetActive(true);

            Timing.RunCoroutine(UpdateNewCardStatus(_itemData.isNew));

        }

        public IEnumerator<float> UpdateNewCardStatus(bool isNew)
        {
            var cardObtainedEffectComponent = _card.GetCardComponent<CardObtainedEffectComponent>();

            if (cardObtainedEffectComponent == null) yield break;

            while (cardObtainedEffectComponent.Effect == null)
            {
                yield return Timing.WaitForOneFrame;
            }

            var cardObtainedEffect = _card.GetVfx<CardObtainedEffectComponent, CardObtainedEffect>();
            cardObtainedEffect.SetupForShop(isNew);
        }


        public IEnumerator<float> ReduceStock()
        {

            // Launch the card upwards upon buying
            OnBoughtLaunchBought(_card);

            var totalAmountInInventory = App.Inventory.GetItemAmount(_itemData.ItemId);
            var amountToSell = DeckData.GetMaxCopies(_card.data.rarity) - totalAmountInInventory;

            if (amountToSell <= 0 || (_card.data.type == CardType.Relic && totalAmountInInventory >= 1) || (_card.data.type == CardType.Elder_Relic && totalAmountInInventory >= 1))
            //if (false)
            {
                Timing.RunCoroutine(SetAsSoldOutSimple());
                //SetAsSoldOutSimple();
                //yield return Timing.WaitUntilDone(OnBoughtLaunchRestock());
                yield break;
            }

            var itemData = Db.ItemDatabase.GetItem(_itemData.ItemId);

            var price = itemData.GetCurrencyPrice(Currency.DAWN_DOLLARS);
            var funds = App.FableData.GetDawnDollarAmount();
            var prefixColor = price > funds ? $"<color={ColorPalette.CrossBlitz.FactionColor.WAR}>" : string.Empty;
            var font = price > funds ? ThicketOutline : ThicketDawnDollars;

            itemCost.text = $"{prefixColor}{price:N0}";
            itemCost.font = font;

            if (_card.data.type == CardType.Relic || _card.data.type == CardType.Elder_Relic)
            {
                itemAmount.text = "<color=#ae977a>x</color><color=#e7edb5>1</color>";
            }
            else
            {
                itemAmount.text = $"<color=#ae977a>x</color><color=#e7edb5>{amountToSell}</color>";
            }

            var seq = DOTween.Sequence();

            _card.RectTransform().DOKill();
            PriceTagTransform.DOKill();
            _card.CanvasGroup.DOKill(true);
            PriceTagCanvas.DOKill(true);


            seq.AppendCallback(() => canvas.alpha = 1);

            _card.RectTransform().DOPunchAnchorPos(Vector2.up * 10f, 0.25f).SetEase(Ease.OutExpo);
            PriceTagTransform.DOPunchAnchorPos(Vector2.down * 10f, 0.25f).SetEase(Ease.OutExpo);
            _card.TintThenUntintOverTime(color, blend, 0.075f, 0.05f, 0.075f);

            //seq.Append(_card.CanvasGroup.DOFade(0, 0.25f));
            //seq.Join(_card.RectTransform().DOPunchAnchorPos(Vector2.up * 10f, 0.25f));


            // Launch the card upwards upon buying
            //yield return Timing.WaitUntilDone(OnBoughtLaunchRestock());

            //cardContainer.DOAnchorPos(Vector2.up * 250,0.5f).SetEase(Ease.OutQuart);

            //seq.Append(_card.CanvasGroup.DOFade(1, 0.25f));

            // seq.Join(_card.RectTransform().DOPunchAnchorPos(Vector2.up * 4f, 0.4f).SetEase(Ease.OutBack));
            // seq.Join(PriceTagTransform.DOPunchAnchorPos(Vector2.up * 6f, 0.4f).SetEase(Ease.OutBack));

        }

        public IEnumerator<float> SetAsSoldOutSimple()
        {
            //_card.RectTransform().DOPunchAnchorPos(Vector2.up * 10f, 0.25f).SetEase(Ease.OutExpo);
            //PriceTagTransform.DOPunchAnchorPos(Vector2.down * 10f, 0.25f).SetEase(Ease.OutExpo);
            //_card.TintThenUntintOverTime(color, blend, 0.075f, 0.05f, 0.075f);

            itemAmount.text = $"<color=#ae977a>x0</color>";
            itemCost.text = $"<color=#684054>SOLD OUT</color>";
            itemCost.font = Thicket2x;

            itemCostContainer.RectTransform().anchoredPosition = new Vector2(itemCostContainer.RectTransform().anchoredPosition.x, 1);
            EachIcon.SetActive(false);
            DawnDollarIcon.SetActive(false);

            _card.RectTransform().DOPunchAnchorPos(Vector2.up * 10f, 0.25f).SetEase(Ease.OutExpo);
            PriceTagTransform.DOPunchAnchorPos(Vector2.down * 10f, 0.25f).SetEase(Ease.OutExpo);
            _card.TintThenUntintOverTime(color, blend, 0.075f, 0.05f, 0.075f);


            itemToggle.Toggle.isOn = false;
            itemToggle.Toggle.interactable = false;
            _soldOut = true;

            yield return Timing.WaitForSeconds(0.25f);

            _card.SetActive(true);
            _card.TintOverTime(soldOutColor, blend, 0.5f);
        }


        public IEnumerator<float> OnBought()
        {

            _card.RectTransform().DOKill();
            PriceTagTransform.DOKill();
            _card.CanvasGroup.DOKill(true);
            PriceTagCanvas.DOKill(true);
            _card.CanvasGroup.alpha = 0f;
            PriceTagCanvas.alpha = 0;

            _card.CanvasGroup.DOFade(0, 0.25f);
            _card.RectTransform().DOPunchAnchorPos(Vector2.up * 10f, 0.5f).SetEase(Ease.OutBack);
            yield return Timing.WaitForSeconds(0.25f);
            _card.CanvasGroup.DOFade(1, 0.25f);
        }

        public void OnBoughtLaunchBought(CardView cardView)
        {
            var cardData = cardView.data;
            var newShopCard = Instantiate(PurchasedCardObject, cardContainer);
            newShopCard.transform.SetSiblingIndex(1);

            //var launchCard = CardFactory.Create(new CreateCardFactorySettings
            //{
            //    Layout = newShopCard.cardLayoutRect,
            //    CardData = cardData,
            //    AdditionalComponents = new List<Type> {/*typeof(HoverCard),*/typeof(ClickableCard), typeof(CardObtainedEffectComponent) },
            //    CardPrefab = cardPrefab,
            //    RemoveCanvas = true,
            //    StartsDisabled = true,
            //    CardObjectOffsetPosition = Vector2.zero
            //}, null);

            //launchCard.SetActive(true);
            newShopCard.Setup(cardView);

            //launchCard.TintThenUntintOverTime(color, blend, 0.1f, 0.5f, 0.1f);
            //launchCard.TintOverTime(color, blend, 0.4f);

            //PriceTagTransform.DOPunchScale(Vector2.down * 10, 0.2f);

        }

        public IEnumerator<float> OnBoughtLaunchRestock()
        {
            cardContainer.anchoredPosition = (Vector2.down * 320);
            cardContainer.DOAnchorPos(new Vector2(0f, 0f), 0.4f).SetEase(Ease.OutQuart);
            yield return Timing.WaitForSeconds(0.5f);
        }
    }
}