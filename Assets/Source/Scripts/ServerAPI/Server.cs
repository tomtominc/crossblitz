﻿using System;
using System.IO;
using System.Threading;
using CrossBlitz.AssetManagement;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.ClientAPI.GameLogic.Data;
using CrossBlitz.Databases;
using CrossBlitz.PlayFab;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using CrossBlitz.ServerAPI.Models;
using CrossBlitz.ViewAPI;
using Newtonsoft.Json;
using UnityEngine;

namespace CrossBlitz.ServerAPI
{
    public enum ClientErrorCode
    {
        None,
        TimedOut,
        BoardCardNotFound,
        BoardCardInGraveyard,
        BoardCardInDifferentLocation,
    }
    public class Server : MonoBehaviour
    {
        public enum State
        {
            NOT_CONNECTED,
            CONNECTED,
        }

        public enum MatchState
        {
            NOT_CONNECTED,
            SEARCHING,
            CONNECTED,
        }

        public const string GAME_PROPERTIES = "GP";

        public static Server Instance;
        public static bool ConnectedToPhotonMaster;
        public static State ConnectionState;
        public static MatchState ClientMatchState;
        public static string Environment;
        public static bool DownloadingBundles;
        public static GameMode PendingGameMode;
        public static JsonSerializerSettings JsonSettings;
        public static CommandResolver CommandResolver;
        public static Thread MainThread;

        public CustomGameOptions customGameOptions;
        //public CustomGameProperties customGameProperties;
        //public GameState currentGameState;

        public static event Action<bool> OnServerConnectedAndReady = delegate { };
        public static event Action OnLoggedInSuccess = delegate { };
        public static event Action OnLoggedInFailed = delegate { };

        private void Awake()
        {
            Instance = this;
            Environment = "internal";
            ConnectionState = State.NOT_CONNECTED;
            CommandResolver = new CommandResolver();

            JsonSettings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            };
        }

        private void Start()
        {
            MainThread = System.Threading.Thread.CurrentThread;
            App.Awake();

            // if (ES3.FileExists("SaveFile.es3"))
            // {
            //     //ES3.fil
            //     Debug.Log("File Exists!");
            //     ES3.DeleteFile(ES3Settings.defaultSettings);
            //     ES3.DeleteFile("SaveFile.es3", new ES3Settings { location = ES3.Location.Cache });
            //     ES3.DeleteFile("SaveFile.es3", ES3Settings.defaultSettings);
            // }
            // else
            // {
            //     Debug.LogError("No filed exists!!");
            // }
            //
            // var files = ES3.GetFiles(ES3Settings.defaultSettings);
            // for (var i = 0; i < files.Length; i++)
            // {
            //     Debug.Log($"#{i} {files[i]}");
            // }
            //
            // files = ES3.GetFiles("SaveFile.es3");
            // for (var i = 0; i < files.Length; i++)
            // {
            //     Debug.Log($"#{i} {files[i]} ({Path.GetFullPath(files[i])})");
            // }

            // var keys = ES3.GetKeys();
            //
            // for (var i = 0; i < keys.Length; i++)
            // {
            //     Debug.Log($"#{i+1}: {keys[i]}");
            // }
        }

        private void Update()
        {
//             if (StateController.CurrentState != StateDefinition.INITIALIZE ||
//                 StateController.CurrentState != StateDefinition.LOGIN)
//             {
//                 if (Input.GetKey(KeyCode.LeftControl))
//                 {
//                     if (Input.GetKeyDown(KeyCode.P))
//                     {
// #if UNITY_EDITOR
//                         UnityEditor.EditorApplication.isPaused = !UnityEditor.EditorApplication.isPaused ;
// #endif
//                     }
//                 }
//             }

            // if (GameServer.State != null)
            // {
            //     currentGameState = GameServer.State;
            // }

            App.OnUpdate();
        }

        public static bool IsOnMainThread()
        {
            return MainThread.Equals(Thread.CurrentThread);
        }

        private void OnApplicationQuit()
        {
            App.OnApplicationClose();
            GameServer.Dispose();
        }

        private void OnEnable()
        {
            GameServer.OnCreatedGameStateSuccess += OnCreatedGameState;
        }

        private void OnDisable()
        {
            GameServer.OnCreatedGameStateSuccess -= OnCreatedGameState;
        }
        public static void Connect()
        {
            ViewController.Initialize();
            AddressableReferenceLoader.Initialize();
            ConnectionState = State.CONNECTED;
            OnServerConnectedAndReady?.Invoke(true);
        }

        public static void Terminate()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        /// <summary>
        /// We call this to try and join a room with 1 player looking for a match,
        /// if it succeeds "OnJoinedRoom" gets called, these means we've joined someone elses game.
        /// if it fails it will call "OnJoinRoomFailed" which will then Create our own match
        /// when we create our own we'll specify the room we want to make using our photon id and playfab id so
        /// it's always unique.
        ///
        /// If the game type is local we don't join a room but just create a match without photon.
        /// </summary>
        /// <param name="gameMode"></param>
        public static void JoinOrCreateMatch(GameMode gameMode)
        {
            ClientMatchState = MatchState.SEARCHING;

            PendingGameMode = gameMode;

            if (PendingGameMode.ConnectionMode == ConnectionMode.LOCAL)
            {
                //PhotonNetwork.OfflineMode = true;
                CreateMatch(PendingGameMode);
            }
        }

        /// <summary>
        /// Creates a match for the client
        /// </summary>
        private static void CreateMatch(GameMode gameMode)
        {
            GameServer.CreateGameState(gameMode);
        }

        /// <summary>
        /// Callback. When we've successfully created a match this will return
        /// We still need to search for a player, but we'll create a room with our gamestate parameters
        /// </summary>
        /// <param name="gameState"></param>
        private static void OnCreatedGameState(GameState gameState)
        {
            if (GameServer.State.ConnectionMode == ConnectionMode.LOCAL)
            {
                GameServer.SendClientOpponentJoinedMessage();
            }
        }

        public static bool WaitingForExecuteGameLogicRpc;

        public void RemoteCallToExecuteGameLogic(CommandQueue commandQueue)
        {
            //Debug.LogError("STARTING REMOTE CALL!");

            WaitingForExecuteGameLogicRpc = true;

            if (GameServer.State == null)
            {
                Debug.LogWarning($"GameState is null!");
                return;
            }

            if (commandQueue == null)
            {
                Debug.LogWarning($"Command Queue is null!");
                return;
            }

            ExecuteVisualGameLogic(commandQueue);

            GameplayScreen.Instance.OnGameStateChanged();
        }

        public static void FinishedMulligan()
        {
            var command = new FinishedMulliganCommand
            {
                PlayerUid = GameServer.ClientPlayerState.Uid
            };

            global::GameLogic.StartCommand(command);

            if (GameServer.Mode.ConnectionMode == ConnectionMode.LOCAL)
            {
                AI.Mulligan();
            }
        }

        private void ExecuteVisualGameLogicFromRPC(string gameState, string commandQueue)
        {
            GameServer.SetState(GameState.FromJson(gameState));
            ExecuteVisualGameLogic(CommandQueue.FromJson(commandQueue));
        }

        private static void ExecuteVisualGameLogic(CommandQueue queue)
        {
            WaitingForExecuteGameLogicRpc = false;
            CommandResolver.Execute(queue);
        }

        public static void Log(string message)
        {
#if UNITY_EDITOR
            Debug.Log(message);
#endif
        }

        public static void LogWarning(string message)
        {
#if UNITY_EDITOR
            Debug.LogWarning(message);
#endif
        }

        public static void LogError(string message)
        {
#if UNITY_EDITOR
            Debug.LogError(message);
#endif
        }

        public static void HandleClientError(ClientResult result, bool isFatal)
        {
            LogError(result.GetErrorMessage());

            if (isFatal)
            {
                //todo: this is a REALLY bad error and we should not continue the game in this state - reload the game state!
            }
        }


    }
}
