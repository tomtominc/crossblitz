using CrossBlitz.PlayFab;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI.Models;
using CrossBlitz.ViewAPI.Popups;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.ServerAPI
{
    public class ServerAdmin : MonoBehaviour
    {
        [Button(ButtonSizes.Medium, ButtonStyle.Box, Expanded = true)]
        public void ConsumeItem(string itemId, int consumeCount)
        {
            var itemInstance = App.Inventory.GetItem(itemId);
            var consumeRequest = new ConsumeItemRequest
            {
                ItemInstanceId = itemInstance.ItemInstanceId,
                ConsumeCount = consumeCount
            };
            CloudScript.OnConsumeItemRequestComplete += OnConsumeItemComplete;
            CloudScript.ConsumeItemRequest(consumeRequest);
        }

        private void OnConsumeItemComplete(ConsumeItemResult result)
        {
            PopupController.Open( new PopupInfo
            {
                title = "Success",
                body = $"Consumed {result.ConsumedItemInstanceId} now only {result.ConsumedItemUsesRemaining} are left!",
                confirm = "OKAY"
            });
        }
    }
}