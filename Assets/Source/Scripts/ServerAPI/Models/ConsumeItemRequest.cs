using System.Collections.Generic;
using PlayFab.ClientModels;

namespace CrossBlitz.ServerAPI.Models
{
    public class ConsumeItemRequest : CloudScriptRequest
    {
        public string ItemInstanceId;
        public int ConsumeCount;

        // The chest type if this is a chest, null if not.
        public string ChestType;
    }

    public class ConsumeItemResult : CloudScriptResult
    {
        public string ConsumedItemInstanceId;
        public int ConsumedItemUsesRemaining;

        public List<string> Cards;
        public int GoldModifier;
        public int ManaShardModifier;
        public int KeystoneModifier;
    }

    public class UnlockContainerRequest : CloudScriptRequest
    {
        public string ItemInstanceId;
    }

    /// <summary>
    /// The result of the unlocked container, these items + virutal currency have already been awarded to the player
    /// these variables are just used for visualization.
    /// </summary>
    public class UnlockContainerResult : CloudScriptResult
    {
        public string UnlockedItemInstance;
        public List<ItemInstance> GrantedItems;
        public Dictionary<string, int> GrantedVirtualCurrencies;
    }
}