using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using UnityEngine;

namespace CrossBlitz.ServerAPI.Models
{
    public class GetBoardCardResult : ClientResult
    {
        // expected result
        public GameCardView boardCard;
        public Vector3 worldPositionIfNotFound;

        // error handling
        public string boardCardUid;
        public CardLocation errorLocation;

        public override string GetErrorMessage()
        {
            return $"{error}: Could not get board card uid: {boardCardUid} location: {errorLocation}";
        }
    }
}