using PlayFab.ClientModels;

namespace CrossBlitz.ServerAPI.Models
{
    public class PlayerLoggedInResult : CloudScriptResult
    {
        public GetUserDataResult PlayerData;
    }
}