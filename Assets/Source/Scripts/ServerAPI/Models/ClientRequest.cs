namespace CrossBlitz.ServerAPI.Models
{
    public class ClientRequest
    {
        public const int MaxRequestLoops = 100;
    }

    public class ClientResult
    {
        public ClientErrorCode error;

        public virtual string GetErrorMessage()
        {
            return $"{error}: Please override GetErrorMessage() to print a custom error.";
        }
    }
}