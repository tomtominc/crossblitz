using PlayFab;

namespace CrossBlitz.ServerAPI.Models
{
    public class ServerError
    {
        public string api;
        public object request;
        public object result;
        public PlayFabError error;
        public ApiError apiError;
    }

    public class ApiError
    {
        public int code;
        public string error;
        public int errorCode;
        public string errorDetails;
        public string errorMessage;
        public string status;
    }
}