﻿using System;
using System.Collections.Generic;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ViewAPI;
using CrossBlitz.ViewAPI.Accounts;
using MEC;
using UnityEngine;

public class AuthenticationController : MonoBehaviour
{
    public static bool AttemptingAuthentication;
    public static bool AuthenticationSuccessful;

    private static AccountScreen accountScreen;

    public static event Action<string> OnLoggedInSuccessful;
    public static event Action<string> OnLoginFailed;

    private void OnEnable()
    {
        PF_Authentication.OnLoginSuccess += HandleOnLoginSuccess;
        PF_Authentication.OnLoginFail += HandleOnLoginFail;
        PF_Authentication.OnAccountRecoverySuccess += HandleRecoveryEmailSuccess;
        PF_Authentication.OnAccountRecoveryFailed += HandleRecoveryEmailFailed;
    }

    private void OnDisable()
    {
        PF_Authentication.OnLoginSuccess -= HandleOnLoginSuccess;
        PF_Authentication.OnLoginFail -= HandleOnLoginFail;
        PF_Authentication.OnAccountRecoverySuccess -= HandleRecoveryEmailSuccess;
        PF_Authentication.OnAccountRecoveryFailed -= HandleRecoveryEmailFailed;
    }

    public static IEnumerator<float> AttemptLogin(string overrideEmail, string overridePassword)
    {
        if (App.CheckForStoredCredentials() ||
            !string.IsNullOrEmpty(overrideEmail) && !string.IsNullOrEmpty(overridePassword))
        {
            AttemptingAuthentication = true;
            AuthenticationSuccessful = false;

            if (!string.IsNullOrEmpty(overrideEmail) && !string.IsNullOrEmpty(overridePassword))
            {
                PF_Authentication.LoginWithEmail(overrideEmail, overridePassword);
            }
            else
            {
                StoredCredentials credentials = App.GetCredentials();
                PF_Authentication.LoginWithEmail(credentials.username, credentials.password);
            }

            while (AttemptingAuthentication)
            {
                yield return Timing.WaitForOneFrame;
            }
        }

        if (AuthenticationSuccessful == false)
        {
            AttemptingAuthentication = true;
        }

        if (AuthenticationSuccessful == false)
        {
            LoadAccountScreen();

            while (accountScreen == null)
            {
                yield return Timing.WaitForOneFrame;
            }

            while (accountScreen.IsActive())
            {
                yield return Timing.WaitForOneFrame;
            }
        }
    }

    private static async void LoadAccountScreen()
    {
        accountScreen = await ViewController.OpenAsync<AccountScreen>("AccountScreen", null, Camera.main);
    }

    private void HandleOnLoginSuccess(string details, MessageDisplayStyle style)
    {
        AttemptingAuthentication = false;
        AuthenticationSuccessful = true;

        if (accountScreen != null)
        {
            accountScreen.LoginSuccessful();
        }

        OnLoggedInSuccessful?.Invoke(details);

        Debug.Log($"Logged In As: {App.AccountInfo.GetUsername()}");
    }

    private void HandleOnLoginFail(string details, FieldErrorType fieldError, MessageDisplayStyle style)
    {
        AuthenticationSuccessful = false;

        if (accountScreen != null)
        {
            accountScreen.LoginFailed(details, fieldError);
        }
        else
        {
            AttemptingAuthentication = false;
        }

        OnLoginFailed?.Invoke(details);

    }

    private void HandleRecoveryEmailSuccess(string details, MessageDisplayStyle style)
    {
        if (accountScreen != null)
        {
            accountScreen.AccountRecoveryEmailSent();
        }
    }

    private void HandleRecoveryEmailFailed(string details, FieldErrorType fieldError, MessageDisplayStyle style)
    {
        if (accountScreen != null)
        {
            accountScreen.AccountRecoveryEmailFailed(details, fieldError);
        }
    }
}
