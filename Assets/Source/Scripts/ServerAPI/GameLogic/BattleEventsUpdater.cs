using System.Collections.Generic;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.PlayFab.Authentication;
using TakoBoyStudios.Events;
using UnityEngine;

namespace CrossBlitz.ServerAPI.GameLogic
{
    public class BattleEventsUpdater : GameEventUpdater
    {
        private BattleData m_battleData;
        private Dictionary<BattleEvent.BattleEventListener, int> EventCounts;
        private Dictionary<string, int> EventPlayedCounts;

        public bool IsPlaying => GameplayScreen.Instance.dialogueStage.Playing;

        public override void OnStartMatch(IMessage message)
        {
            TryStartCutscene(message, BattleEvent.BattleEventListener.OnStartMatch);
        }

        public override void OnMatchWon(IMessage message)
        {
            TryStartCutscene(message, BattleEvent.BattleEventListener.OnMatchWon);
        }

        public override void OnMatchLost(IMessage message)
        {
            TryStartCutscene(message, BattleEvent.BattleEventListener.OnMatchLost);
        }

        public override void OnMatchDraw(IMessage message)
        {
            TryStartCutscene(message, BattleEvent.BattleEventListener.OnMatchDraw);
        }

        public override void OnTurnStart(IMessage message)
        {
            TryStartCutscene(message, BattleEvent.BattleEventListener.OnTurnStart);
        }

        public override void OnTurnEnd(IMessage message)
        {
            TryStartCutscene(message, BattleEvent.BattleEventListener.OnTurnEnd);
        }

        public override void OnMatchDisposed(IMessage message)
        {
            TryStartCutscene(message, BattleEvent.BattleEventListener.OnMatchDisposed);
        }

        public override void OnCardDrawn(IMessage message)
        {
            TryStartCutscene(message, BattleEvent.BattleEventListener.OnCardDrawn);
        }

        public override void OnMinionSummoned(IMessage message)
        {
            TryStartCutscene(message, BattleEvent.BattleEventListener.OnMinionSummoned);
        }

        public override void OnCardChangedLocation(IMessage message)
        {
            TryStartCutscene(message, BattleEvent.BattleEventListener.OnCardChangedLocation);
        }

        public override void OnCharacterDamaged(IMessage message)
        {
            TryStartCutscene(message, BattleEvent.BattleEventListener.OnCharacterDamaged);
        }

        public override void OnCharacterStatsModified(IMessage message)
        {
            TryStartCutscene(message, BattleEvent.BattleEventListener.OnCharacterStatsModified);
        }

        public override void OnMinionDestroyed(IMessage message)
        {
            TryStartCutscene(message, BattleEvent.BattleEventListener.OnMinionDestroyed);
        }

        public override void OnCardPlayed(IMessage message)
        {
            TryStartCutscene(message, BattleEvent.BattleEventListener.OnCardPlayed);
        }

        public override void OnModifiedMana(IMessage message)
        {
            TryStartCutscene(message, BattleEvent.BattleEventListener.OnModifiedMana);
        }

        public override void OnCardAddedToDeck(IMessage message)
        {
            TryStartCutscene(message, BattleEvent.BattleEventListener.OnCardAddedToDeck);
        }

        public override void OnMinionFrozen(IMessage message)
        {
            TryStartCutscene(message, BattleEvent.BattleEventListener.OnMinionFrozen);
        }

        public override void OnCardTrackedAndAddedToHand(IMessage message)
        {
            TryStartCutscene(message, BattleEvent.BattleEventListener.OnCardTrackedAndAddedToHand);
        }

        private void TryStartCutscene(IMessage message, BattleEvent.BattleEventListener listener)
        {
            if (string.IsNullOrEmpty(GameServer.Mode.BattleUid))
            {
                return;
            }

            m_battleData ??= Db.BattleDatabase.GetBattleData(GameServer.Mode.BattleUid);

            if (m_battleData?.BattleEvents == null || m_battleData.BattleEvents?.Count <= 0)
            {
                return;
            }

            EventCounts ??= new Dictionary<BattleEvent.BattleEventListener, int>();
            EventPlayedCounts ??= new Dictionary<string, int>();

            if (TryGetBattleEvent(message, m_battleData, listener, out var battleEvent))
            {
                if (EventCounts.ContainsKey(listener))
                {
                    EventCounts[listener]++;
                }
                else
                {
                    EventCounts.Add(listener, 1);
                }

                if (battleEvent.skipUntil)
                {
                    if(battleEvent.skips >= EventCounts[listener] || !GameServer.State.IsClientsTurn()) return;
                }

                if (EventPlayedCounts.ContainsKey(battleEvent.Uid))
                {
                    EventPlayedCounts[battleEvent.Uid]++;
                }
                else
                {
                    EventPlayedCounts.Add(battleEvent.Uid, 0);
                }

                var cutsceneUid = battleEvent.cutsceneUids[Random.Range(0, battleEvent.cutsceneUids.Count)];
                var cutscene = Db.CutsceneDatabase.GetCutscene(cutsceneUid);
                var currentChapter = App.FableData.GetCurrentChapter();

                if (currentChapter != null && currentChapter.HasSeenCutscene(cutsceneUid))
                {
                    return;
                }

                if (cutscene == null)
                {
                    return;
                }

                if (currentChapter != null)
                {
                    currentChapter.MarkCutsceneAsSeen(cutsceneUid);
                }
                // else // return if the current chapter is null, if you want to test the cutscenes within the battle editor, comment out this else statement.
                // {
                //     return;
                // }

                switch (battleEvent.frequency)
                {
                    case BattleEvent.EventFrequency.Once when EventPlayedCounts[battleEvent.Uid] <= 0:
                    {
                        GameplayScreen.Instance.dialogueStage.PlayCutscene(cutscene);
                        break;
                    }
                    case BattleEvent.EventFrequency.Random when Random.value <= battleEvent.randomFrequency:
                    {
                        GameplayScreen.Instance.dialogueStage.PlayCutscene(cutscene);
                        break;
                    }
                    case BattleEvent.EventFrequency.Always:
                    {
                        GameplayScreen.Instance.dialogueStage.PlayCutscene(cutscene);
                        break;
                    }
                }
            }
        }


        private bool TryGetBattleEvent(IMessage message, BattleData battle, BattleEvent.BattleEventListener listener, out BattleEvent battleEvent)
        {
            // check args for custom finding for event
            if (message.Data is OnCardPlayedEventArgs cardPlayedArgs)
            {
                battleEvent = battle.BattleEvents.Find(e =>
                    e.battleEventListener == listener &&
                    (string.IsNullOrEmpty(e.cardId) || e.cardId == GameServer.State.GetCard(cardPlayedArgs.cardUid).data.id));
            }
            // else covers just finding the battle event by listener
            else
            {
                battleEvent = battle.BattleEvents.Find(e => e.battleEventListener == listener);
            }

            if (battleEvent?.cutsceneUids == null || battleEvent.cutsceneUids.Count <= 0)
            {
                return false;
            }

            return true;
        }


    }
}