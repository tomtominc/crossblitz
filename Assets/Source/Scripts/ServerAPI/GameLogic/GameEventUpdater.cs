using System.Collections.Generic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using TakoBoyStudios.Events;

namespace CrossBlitz.ServerAPI.GameLogic
{
    [System.Serializable]
    public class GameEventUpdater
    {
        public List<string> Targets;

        public virtual void Initialize()
        {
        }

        public virtual void Kill()
        {
        }

        public virtual void OnStartMatch(IMessage message){}
        public virtual void OnMatchWon(IMessage message){}
        public virtual void OnMatchLost(IMessage message){}
        public virtual void OnMatchDraw(IMessage message){}
        public virtual void OnTurnStart(IMessage message){}
        public virtual void OnTurnEnd(IMessage message){}
        public virtual void OnMatchDisposed(IMessage message){}
        public virtual void OnCardDrawn(IMessage message){}
        public virtual void OnMinionSummoned(IMessage message){}
        public virtual void OnCardChangedLocation(IMessage message){}
        public virtual void OnCharacterDamaged(IMessage message){}
        public virtual void OnCharacterStatsModified(IMessage message){}
        public virtual void OnMinionDestroyed(IMessage message){}
        public virtual void OnCardPlayed(IMessage message){}
        public virtual void OnCardTransformed(GameCardState cardState){}
        public virtual void OnModifiedMana(IMessage message){}
        public virtual void OnCardAddedToDeck(IMessage message){}
        public virtual void OnMinionFrozen(IMessage message){}
        public virtual void OnCardTrackedAndAddedToHand(IMessage message){}
        public virtual void OnCardDiscarded(IMessage message){}

        public virtual void OnTrapTriggered(IMessage message){}


        public virtual void AddTarget(string uid)
        {
            if (Targets == null) Targets = new List<string>();

            Targets.Add(uid);
        }
    }
}