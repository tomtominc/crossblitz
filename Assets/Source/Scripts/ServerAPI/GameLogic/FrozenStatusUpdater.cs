using System.Collections.Generic;
using System.Linq;
using Asyncoroutine;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ServerAPI.GameLogic.Data;
using MEC;
using TakoBoyStudios.Events;
using UnityEngine;

namespace CrossBlitz.ServerAPI.GameLogic
{
    [System.Serializable]
    public class FrozenStatusUpdater : GameEventUpdater
    {
        public const int TurnsUntilFrozenFinishes = 1;

        public override void OnCardTransformed(GameCardState cardState)
        {
            if (cardState == null)
            {
                return;
            }

            if (Targets == null)
            {
                return;
            }

            if (Targets.Contains(cardState.uid))
            {
                Targets.Remove(cardState.uid);
            }
        }

        public override async void OnTurnStart(IMessage message)
        {
            var unfreezeTargets = new List<string>();

            if (Targets == null)
            {
                return;
            }

            for (var i = Targets.Count-1; i > -1; i--)
            {
                var card = GameServer.State.GetCard(Targets[i]);
                var currentPlayer = GameServer.GetPlayerState(GameServer.State.CurrentPlayer);

                if (card != null && card.Team != currentPlayer.GetTeam() && card.HasStatusEffect(StatusEffect.Frozen))
                {
                    card.RemoveStatusEffect(StatusEffect.Frozen);
                    Targets.RemoveAt(i);

                    if (card.location == CardLocation.Board)
                    {
                        unfreezeTargets.Add(card.uid);
                    }
                }
            }

            if (unfreezeTargets.Count > 0)
            {
                for (var i = 0; i < unfreezeTargets.Count; i++)
                {
                    var getBoardCardResult = await GameBoard.GetBoardCard(unfreezeTargets[i]);

                    if (getBoardCardResult.error != ClientErrorCode.None)
                    {
                        Server.HandleClientError(getBoardCardResult, true);
                    }

                    if (getBoardCardResult.boardCard)
                    {
                        getBoardCardResult.boardCard.Events.UnFrozen();
                    }
                }
            }
        }
    }
}