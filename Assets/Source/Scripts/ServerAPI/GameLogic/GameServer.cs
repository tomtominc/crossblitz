using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Data;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using Source.Scripts.Server.GameLogic.Data;
using TakoBoyStudios.Events;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ServerAPI.GameLogic
{
    public static class GameServer
    {
        public static GameMode Mode;
        public static Rules Rules;
        public static int ClientPlayerIndex=-1;
        public static int OpponentPlayerIndex=-1;

        public static Team LocalPlayerTeam = Team.Client;

        /// <summary>
        /// Returns either the "pending state" if set, or the main state.
        /// </summary>
        public static GameState State => m_pendingState ?? m_state;
        /// <summary>
        /// Always returns the main state, even if the pending state has been setup
        /// </summary>
        public static GameState MainState => m_state;

        // ==========================================================================
        // EVENTS TO HOOK TO
        // ==========================================================================

        public static event Action<GameState> OnCreatedGameStateSuccess = delegate { };
        public static event Action OnCreatedGameSuccess = delegate { };

        private static GameState m_pendingState;
        private static GameState m_state;

        public static void Kill()
        {
            m_pendingState = null;
            m_state = null;

            Mode = null;
            Rules = null;
            ClientPlayerIndex = -1;
            OpponentPlayerIndex = -1;
        }

        // ==========================================================================
        // MESSAGE SYSTEM TO CLIENT
        // ==========================================================================

        public static void CreateGameState(GameMode gameMode)
        {
            Server.CommandResolver.Dispose();
            Server.CommandResolver = new CommandResolver();

            Mode = gameMode;
            Rules = new Rules();

            m_pendingState = null;
            m_state = new GameState
            {
                RoomId = GenerateRandomRoomId(),
                GameType = Mode.GameType,
                ConnectionMode = Mode.ConnectionMode,
                PlayerTurnCount = 0,
                OpponentTurnCount = 0,
                History = new List<Command>(),
                Board = new BoardState(),
                Cards =  new List<GameCardState>(),
                StartTime = DateTime.UtcNow.Ticks,
                HistoryDisplays = new List<string>()
            };

            State.FreezeUpdater = new FrozenStatusUpdater();
            State.FreezeUpdater.Initialize();

            State.gameStats = new GameStats();
            State.gameStats.Initialize();

            State.AuraUpdater = new AuraUpdater();
            State.AuraUpdater.Initialize();

            State.BattleEventsUpdater = new BattleEventsUpdater();
            State.BattleEventsUpdater.Initialize();

            State.GameEventUpdaters = new List<GameEventUpdater>
            {
                State.FreezeUpdater,
                State.gameStats,
                State.AuraUpdater,
                State.BattleEventsUpdater
            };

            State.Players = GeneratePlayers(gameMode);

            ClientPlayerIndex = 0;
            OpponentPlayerIndex = 1;

            // INITIALIZING WITH CLIENT PLAYER BUT THIS CHANGES AFTER ROSHAMBO
            State.SetCurrentPlayer( ClientPlayerState.Uid );
            State.Board.GenerateBoard();

            Events.Subscribe(EventType.OnStartMatch, OnStartMatch);
            Events.Subscribe(EventType.OnTurnStart, OnTurnStart);
            Events.Subscribe(EventType.OnTurnEnd, OnTurnEnd);
            Events.Subscribe(EventType.OnCardDrawn, OnCardDrawn);
            Events.Subscribe(EventType.OnMinionSummoned, OnMinionSummoned);
            Events.Subscribe(EventType.OnCardChangedLocation, OnCardChangedLocation);
            Events.Subscribe(EventType.OnCharacterDamaged, OnCharacterDamaged);
            Events.Subscribe(EventType.OnCharacterStatsModified, OnCharacterStatsModified);
            Events.Subscribe(EventType.OnMinionDestroyed, OnMinionDestroyed);
            Events.Subscribe(EventType.OnMatchWon, OnMatchWon);
            Events.Subscribe(EventType.OnMatchLost, OnMatchLost);
            Events.Subscribe(EventType.OnMatchDraw, OnMatchDraw);
            Events.Subscribe(EventType.OnMatchDisposed, OnMatchDisposed);
            Events.Subscribe(EventType.OnCardPlayed, OnCardPlayed);
            Events.Subscribe(EventType.OnCardTrackedAndAddedToHand, OnCardTrackedAndAddedToHand);
            Events.Subscribe(EventType.OnModifiedMana, OnModifiedMana);
            Events.Subscribe(EventType.OnCardAddedToDeck, OnCardAddedToDeck);
            Events.Subscribe(EventType.OnMinionFrozen, OnMinionFrozen);
            Events.Subscribe(EventType.OnCardDiscarded, OnCardDiscarded);
            Events.Subscribe(EventType.OnTrapTriggered, OnTrapTriggered);

            OnCreatedGameStateSuccess?.Invoke(State);
        }

        public static void PrintGameState()
        {
            var matchFile = new MatchStateFile();
            matchFile.Rules = Rules;
            matchFile.GameMode = Mode;
            matchFile.GameState = m_state;
            matchFile.SaveToDisk();
        }

        public static bool CheckForMatchFinished()
        {
            if (StateController.CurrentState == StateDefinition.GAME_FINISHED)
            {
                return false;
            }

            if (IsGameFinished())
            {
                if (IsClientWinner())
                {
                    Events.Publish(null, EventType.OnMatchWon, null);
                }
                else if (IsOpponentWinner())
                {
                    Events.Publish(null, EventType.OnMatchLost, null);
                }
                else
                {
                    Events.Publish(null, EventType.OnMatchDraw, null);
                }

                return true;
            }

            return false;
        }

        public static void SetState(GameState state)
        {
            m_state = state;
        }

        public static void SetPendingState(GameState pendingState)
        {
            m_pendingState = pendingState;
        }

        public static bool IsGamePendingFinished()
        {
            if (State == null) return true;
            if (State.Players.Count < 2) return true;

            var opponentHero = State.GetCard(OpponentPlayerState.heroUid);
            var playerHero = State.GetCard(ClientPlayerState.heroUid);

            return opponentHero.GetPendingHealth() <= 0 || playerHero.GetPendingHealth() <= 0;
        }

        public static bool IsGameFinished()
        {
            if (State == null) return true;
            if (State.Players.Count < 2) return true;

            var opponentHero = State.GetCard(OpponentPlayerState.heroUid);
            var playerHero = State.GetCard(ClientPlayerState.heroUid);

            return opponentHero.GetHealth() <= 0 || playerHero.GetHealth() <= 0;
        }

        public static bool IsClientWinner()
        {
            if (State == null) return true;
            if (State.Players.Count < 2) return true;

            var playerHero = State.GetCard(ClientPlayerState.heroUid);
            return playerHero.GetHealth() > 0;
        }

        public static bool IsOpponentWinner()
        {
            if (State == null) return false;
            if (State.Players.Count < 2) return false;

            var opponentHero = State.GetCard(OpponentPlayerState.heroUid);
            return opponentHero.GetHealth() > 0;
        }

        public static void ModifyManaFromPlayedCard(Team team, GameCardState card)
        {
            if (State != MainState)
            {
                Debug.LogError("Main state is not equal to the state!");
                return;
            }

            var playerState = GetPlayerState(team);
            var cardCost = card.GetCost();
            var costsHealthInsteadOfMana = State.AuraUpdater.CostsHealthInsteadOfMana(playerState.Uid, out var sourceUid, out var ability);

            if (costsHealthInsteadOfMana)
            {
                var hero = State.GetCard(playerState.heroUid);

                var damageCommand = new DamageCommand
                {
                    PlayerUid = playerState.Uid,
                    Amount = cardCost,
                    SourceUid = sourceUid,
                    TargetUids = new List<string> { hero.uid }
                };

                global::GameLogic.ExecuteCommand(damageCommand);

                var sourceCard = State.GetCard(sourceUid);

                // use the ability, in case this is a "once per turn" ability.
                sourceCard.UseAbility( ability );
            }
            else
            {
                playerState.currentMana = Mathf.Clamp(playerState.currentMana - card.GetCost(), 0, 10);

                if (!AI.Thinking)
                {
                    Events.Publish(null, EventType.OnModifiedMana, new OnModifiedManaEventArgs
                    {
                        team = team,
                        currentMana = playerState.currentMana,
                        totalMana = playerState.totalMana,
                        goldMana = false
                    });
                }
            }
        }

        public static void SetMana(Team team, int mana, bool goldMana = false, bool newTurn = false)
        {
            if (State != MainState)
            {
                Debug.LogError("Main state is not equal to the state!");
                return;
            }

            if (team == Team.Client)
            {
                ClientPlayerState.currentMana = Mathf.Clamp(mana, 0, 10);

                Events.Publish(null, EventType.OnModifiedMana, new OnModifiedManaEventArgs
                {
                    team = team,
                    currentMana = ClientPlayerState.currentMana,
                    totalMana = ClientPlayerState.totalMana,
                    goldMana = goldMana,
                    newTurn = newTurn
                });
            }
            else if (team == Team.Opponent)
            {
                OpponentPlayerState.currentMana = Mathf.Clamp(mana, 0, 10);

                Events.Publish(null, EventType.OnModifiedMana, new OnModifiedManaEventArgs
                {
                    team = team,
                    currentMana = OpponentPlayerState.currentMana,
                    totalMana = OpponentPlayerState.totalMana,
                    goldMana = goldMana,
                    newTurn = newTurn
                });
            }
        }

        private static List<PlayerBattleState> GeneratePlayers(GameMode mode)
        {
            List<PlayerBattleState> players = new List<PlayerBattleState>();

            switch (mode.ConnectionMode)
            {
                case ConnectionMode.LOCAL:

                    ClientPlayerIndex = 0;
                    OpponentPlayerIndex = 1;

                    var clientState = new PlayerBattleState();
                    clientState.UpdateIdentifier();
                    clientState.turnOrderSelection = PlayerBattleState.TurnOrderCardType.None;
                    clientState.controlMode = PlayerBattleState.ControlMode.Player;

                    var currentDeck = string.IsNullOrEmpty(mode.DeckUid) ?
                        App.PlayerDecks.GetDeckBasedOnGameState()  :
                        App.PlayerDecks.GetDeck(mode.DeckUid);
                    var battle = Db.BattleDatabase.GetBattleData(mode.BattleUid);
                    var shuffle = true;

                    if (battle.CustomBattleOptions != null &&
                        battle.CustomBattleOptions.Enabled &&
                        battle.CustomBattleOptions.PlayerDeckOverride != null &&
                        battle.CustomBattleOptions.PlayerDeckOverride.Enabled &&
                        battle.CustomBattleOptions.PlayerDeckOverride.OverrideDeck != null)
                    {
                        currentDeck = battle.CustomBattleOptions.PlayerDeckOverride.OverrideDeck;

                    }

                    if (battle.CustomBattleOptions != null &&
                        battle.CustomBattleOptions.Enabled &&
                        battle.CustomBattleOptions.PlayerDeckOverride != null &&
                        battle.CustomBattleOptions.PlayerDeckOverride.Enabled &&
                        battle.CustomBattleOptions.PlayerDeckOverride.DrawInOrder)
                    {
                        shuffle = false;
                    }

                    clientState.name = currentDeck.GetHeroData().name;
                    clientState.CreateGameDeck(currentDeck,shuffle,battle.CustomBattleOptions?.RemoveCardsFromPlayersDeck);

                    // ====== opponent stuff ======

                    var opponentState = new PlayerBattleState();
                    opponentState.UpdateIdentifier();
                    opponentState.turnOrderSelection = PlayerBattleState.TurnOrderCardType.None;
                    opponentState.controlMode = PlayerBattleState.ControlMode.Bot;
                    opponentState.name = battle.Name;

                    var opponentDeck = battle.Deck;
                    shuffle = true;

                    if (battle.CustomBattleOptions != null &&
                        battle.CustomBattleOptions.Enabled &&
                        battle.CustomBattleOptions.OpponentDeckOverride != null &&
                        battle.CustomBattleOptions.OpponentDeckOverride.Enabled &&
                        battle.CustomBattleOptions.OpponentDeckOverride.OverrideDeck != null)
                    {
                        opponentDeck = battle.CustomBattleOptions.OpponentDeckOverride.OverrideDeck;
                    }

                    if (battle.CustomBattleOptions != null &&
                        battle.CustomBattleOptions.Enabled &&
                        battle.CustomBattleOptions.OpponentDeckOverride != null &&
                        battle.CustomBattleOptions.OpponentDeckOverride.Enabled &&
                        battle.CustomBattleOptions.OpponentDeckOverride.DrawInOrder)
                    {
                        shuffle = false;
                    }

                    opponentDeck.hero = battle.CardId;
                    opponentState.CreateGameDeck(opponentDeck,shuffle);

                    players.Add(clientState);
                    players.Add(opponentState);

                    break;
                case ConnectionMode.ONLINE: // figure out online
                    break;
            }

            return players;
        }

        // ==========================================================================
        // MESSAGE SYSTEM TO CLIENT
        // ==========================================================================

        public static void SendClientOpponentJoinedMessage()
        {
            OnCreatedGameSuccess?.Invoke();
        }

        // ==========================================================================
        // CLIENT INFORMATION HELPERS
        // ==========================================================================

        // A real game server would know which client is calling and provide the server info based on
        // client and opponent but since this lives locally we'll just pretend.

        public static PlayerBattleState ClientPlayerState => State.Players[ClientPlayerIndex];
        public static PlayerBattleState OpponentPlayerState => State.Players[OpponentPlayerIndex];

        public static PlayerBattleState GetPlayerState(Team team)
        {
            return team == Team.Client ? ClientPlayerState : OpponentPlayerState;
        }

        public static PlayerBattleState GetPlayerState(string uid)
        {
            return ClientPlayerState.Uid == uid ? ClientPlayerState : OpponentPlayerState;
        }

        public static PlayerBattleState GetCurrentPlayerState()
        {
            return GetPlayerState(State.CurrentPlayer);
        }

        public static PlayerBattleState GetLocalPlayerState()
        {
            return LocalPlayerTeam == Team.Client ? ClientPlayerState : OpponentPlayerState;
        }

        public static PlayerBattleState GetRemotePlayerState()
        {
            return LocalPlayerTeam == Team.Client ? OpponentPlayerState : ClientPlayerState;
        }

        public static int GetRemotePlayerIndex()
        {
            return LocalPlayerTeam == Team.Client ? OpponentPlayerIndex : ClientPlayerIndex;
        }

        public static Team GetOpposingTeam()
        {
            return GameServer.GetLocalPlayerState().GetTeam() == Team.Client ? Team.Opponent : Team.Client;
        }

        public static bool IsClientGoingFirst()
        {
            if (ClientPlayerState.turnOrderSelection == PlayerBattleState.TurnOrderCardType.Rock &&
                OpponentPlayerState.turnOrderSelection == PlayerBattleState.TurnOrderCardType.Scissors)
            {
                return true;
            }

            if (ClientPlayerState.turnOrderSelection == PlayerBattleState.TurnOrderCardType.Paper &&
                OpponentPlayerState.turnOrderSelection == PlayerBattleState.TurnOrderCardType.Rock)
            {
                return true;
            }

            if (ClientPlayerState.turnOrderSelection == PlayerBattleState.TurnOrderCardType.Scissors &&
                OpponentPlayerState.turnOrderSelection == PlayerBattleState.TurnOrderCardType.Paper)
            {
                return true;
            }

            return false;
        }

        public static bool IsRunning()
        {
            return Mode != null && Rules != null && State != null && ClientPlayerIndex > -1 && OpponentPlayerIndex > -1;
        }

        public static void Dispose()
        {
            Debug.LogError("Disposed Game Server!");

            Events.Unsubscribe(EventType.OnStartMatch, OnStartMatch);
            Events.Unsubscribe(EventType.OnMatchWon, OnMatchWon);
            Events.Unsubscribe(EventType.OnMatchLost, OnMatchLost);
            Events.Unsubscribe(EventType.OnMatchDraw, OnMatchDraw);
            Events.Unsubscribe(EventType.OnTurnStart, OnTurnStart);
            Events.Unsubscribe(EventType.OnTurnEnd, OnTurnEnd);
            Events.Unsubscribe(EventType.OnCardDrawn, OnCardDrawn);
            Events.Unsubscribe(EventType.OnMinionSummoned, OnMinionSummoned);
            Events.Unsubscribe(EventType.OnCardChangedLocation, OnCardChangedLocation);
            Events.Unsubscribe(EventType.OnCharacterDamaged, OnCharacterDamaged);
            Events.Unsubscribe(EventType.OnCharacterStatsModified, OnCharacterStatsModified);
            Events.Unsubscribe(EventType.OnMinionDestroyed, OnMinionDestroyed);
            Events.Unsubscribe(EventType.OnMatchDisposed, OnMatchDisposed);
            Events.Unsubscribe(EventType.OnCardPlayed, OnCardPlayed);
            Events.Unsubscribe(EventType.OnCardTrackedAndAddedToHand, OnCardTrackedAndAddedToHand);
            Events.Unsubscribe(EventType.OnModifiedMana, OnModifiedMana);
            Events.Unsubscribe(EventType.OnCardAddedToDeck, OnCardAddedToDeck);
            Events.Unsubscribe(EventType.OnMinionFrozen, OnMinionFrozen);
            Events.Unsubscribe(EventType.OnCardDiscarded, OnCardDiscarded);
            Events.Unsubscribe(EventType.OnTrapTriggered, OnTrapTriggered);

            Server.CommandResolver.Dispose();
            AI.Dispose();

            m_state?.Dispose();
            m_pendingState?.Dispose();

            m_state = null;
            m_pendingState = null;

            Rules?.Dispose();
            Rules = null;

            Mode?.Dispose();
            Mode = null;

            ClientPlayerIndex = -1;
            OpponentPlayerIndex = -1;
        }

        // ==========================================================================
        // GAME EVENTS
        // ==========================================================================
        public static void OnStartMatch(IMessage message)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnStartMatch(message);
            }
        }

        public static void OnMatchWon(IMessage message)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnMatchWon(message);
            }
        }

        public static void OnMatchLost(IMessage message)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnMatchLost(message);
            }
        }
        public static void OnMatchDraw(IMessage message)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnMatchDraw(message);
            }
        }

        public static void OnTurnStart(IMessage message)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnTurnStart(message);
            }
        }

        public static void OnTurnEnd(IMessage message)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnTurnEnd(message);
            }
        }
        public static void OnMatchDisposed(IMessage message)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnMatchDisposed(message);
            }
        }

        public static void OnCardDrawn(IMessage message)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnCardDrawn(message);
            }
        }
        public static void OnMinionSummoned(IMessage message)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnMinionSummoned(message);
            }
        }

        public static void OnCardChangedLocation(IMessage message)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnCardChangedLocation(message);
            }
        }

        public static void OnCharacterDamaged(IMessage message)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnCharacterDamaged(message);
            }
        }

        public static void OnCharacterStatsModified(IMessage message)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnCharacterStatsModified(message);
            }
        }

        public static void OnMinionDestroyed(IMessage message)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnMinionDestroyed(message);
            }
        }
        public static void OnCardPlayed(IMessage message)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnCardPlayed(message);
            }
        }

        public static void OnCardTransformed(GameCardState cardState)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnCardTransformed(cardState);
            }
        }

        public static void OnModifiedMana(IMessage message)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnModifiedMana(message);
            }
        }

        public static void OnCardAddedToDeck(IMessage message)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnCardAddedToDeck(message);
            }
        }

        public static void OnMinionFrozen(IMessage message)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnMinionFrozen(message);
            }
        }

        public static void OnCardTrackedAndAddedToHand(IMessage message)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnCardTrackedAndAddedToHand(message);
            }
        }

        public static void OnCardDiscarded(IMessage message)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnCardDiscarded(message);
            }
        }

        public static void OnTrapTriggered(IMessage message)
        {
            for (var i = 0; i < State?.GameEventUpdaters.Count; i++)
            {
                State?.GameEventUpdaters[i].OnTrapTriggered(message);
            }
        }

        public static bool AbleToPlayCards()
        {
            return StateController.CurrentState == StateDefinition.GAME_PLAYER_UPDATE_TURN &&
                   (GameplayScreen.Instance == null || GameplayScreen.Instance.ScreenState == GameplayScreen.ScreenStateType.NormalUpdate);
        }


        public static void ChangeGameState(StateDefinition stateDefinition)
        {
            if (StateController.CurrentState == StateDefinition.GAME_FINISHED)
            {
                return;
            }

            StateController.ChangeState(stateDefinition);
        }

        // ==========================================================================
        // GENERAL HELPERS
        // ==========================================================================

        private static string GenerateRandomRoomId()
        {
            return $"{App.AccountInfo.GetPlayerGuid()}.{Guid.NewGuid()}.{"PHOTON_AUTH_TOKEN_GOES_HERE"}";
        }
    }
}
