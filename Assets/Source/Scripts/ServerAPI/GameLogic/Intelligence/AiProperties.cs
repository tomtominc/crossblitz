using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.ServerAPI.GameLogic.Intelligence
{
    [System.Serializable]
    public class AiProperties
    {
        /// <summary>
        /// Helps determine when to play this or with which cards to play this with.
        /// </summary>
        public enum StrategyType
        {
            /// <summary>
            /// Used in the early game
            /// </summary>
            Early,
            Combo,
            Meta,
            Tempo,
        }
        /// <summary>
        /// The order this card will try to be played.
        /// </summary>
        public enum Order
        {
            /// <summary>
            /// Will be played after cards using the "First" order
            /// </summary>
            AnyOrder,
            /// <summary>
            /// Will try to be played first, with other cards that are first.
            /// </summary>
            First,
            /// <summary>
            /// Will try to be played last, with other cards that are last.
            /// </summary>
            Last
        }

        // ============================================================================================================
        //
        // ============================================================================================================

        /// <summary>
        /// Where on the board the card will try to be played, Default for best strategy.
        /// </summary>
        public enum BoardPlacement
        {
            /// <summary>
            /// Will be played on the front if melee or to block when necessary.
            /// Will be played on the back if arcane or to block when necessary.
            /// Will be played in best row in either the front or back if arcane.
            /// </summary>
            Default,
            /// <summary>
            /// Will always be played in front, won't be played otherwise.
            /// </summary>
            FrontAlways,
            /// <summary>
            /// Will always be played in the back, won't be played otherwise.
            /// </summary>
            BackAlways,

            /// <summary>
            /// Will try and be placed behind a specific card.
            /// </summary>
            Behind_Specific_Card,
        }

        [Flags]
        public enum PlacementBehavior
        {
            /// <summary>
            /// Will be played on the front if melee or to block when necessary.
            /// Will be played on the back if arcane or to block when necessary.
            /// Will be played in best row in either the front or back if arcane.
            /// </summary>
            [Tooltip("Will be played on the front if melee or to block when necessary. \nWill be played on the back if arcane or to block when necessary.\nWill be played in best row in either the front or back if arcane.")]
            DEFAULT = 1 << 1,
            /// <summary>
            /// Strict will make it so the behavior is ALWAYS applied and wont be summoned otherwise.
            /// </summary>
            [Tooltip("Strict will make it so the behavior is ALWAYS applied and wont be summoned otherwise.")]
            STRICT = 1 << 2,
            /// <summary>
            /// Will always try the front first, then the back, if STRICT is on, then i will only summon in front
            /// </summary>
            [Tooltip("Will always try the front first, then the back, if STRICT is on, then i will only summon in front")]
            FRONT = 1 << 3,
            /// <summary>
            /// Will always try the back first, then the front, if STRICT is on, then i will only summon in back
            /// </summary>
            [Tooltip("Will always try the back first, then the front, if STRICT is on, then i will only summon in back")]
            BACK = 1 << 4,
            /// <summary>
            /// Trys to summon by a specific minion (specified by FRONT/BACK) e.g. BACK + SPECIFIC_MINION will try and summon in the BACK BEHIND THE MINION. If strict, wont summon without minion
            /// </summary>
            [Tooltip("Trys to summon by a specific minion (specified by FRONT/BACK) e.g. BACK + SPECIFIC_MINION will try and summon in the BACK BEHIND THE MINION. If strict, wont summon without minion")]
            SPECIFIC_MINION = 1 << 5,
            /// <summary>
            /// Trys to summon in front when the back is empty (probably to summon something). Strict will only summon when back is empty.
            /// </summary>
            [Tooltip("Trys to summon in front when the back is empty (probably to summon something). Strict will only summon when back is empty.")]
            EMPTY_BACK = 1 << 6,
            /// <summary>
            /// Trys to summon in back when the front is empty (probably to summon something). Strict will only summon when back is empty.
            /// </summary>
            [Tooltip("Trys to summon in back when the front is empty (probably to summon something). Strict will only summon when back is empty.")]
            EMPTY_FRONT = 1 << 7,
            /// <summary>
            /// After being placed, the ai will also think this is occupying in front. Used for things that hae an end of turn summon effect.
            /// </summary>
            [Tooltip("After being placed, the ai will also think this is occupying in front. Used for things that hae an end of turn summon effect.")]
            OCCUPY_FRONT = 1 << 8,
            /// <summary>
            /// After being placed, the ai will also think this is occupying in back. Used for things that have an end of turn summon effect
            /// </summary>
            [Tooltip("After being placed, the ai will also think this is occupying in back. Used for things that hae an end of turn summon effect.")]
            OCCUPY_BACK = 1 << 10,
            /// <summary>
            /// Only for back row minions. Makes it so they won't spawn unless the front tile is occupied.
            /// </summary>
            [Tooltip("Only for back row minions. Makes it so they won't spawn unless the front tile is occupied.")]
            FRONT_OCCUPIED = 1 << 11,
            /// <summary>
            /// Only for front row minions. Makes it so they won't spawn unless the back tile is occupied.
            /// </summary>
            [Tooltip("Only for front row minions. Makes it so they won't spawn unless the back tile is occupied.")]
            BACK_OCCUPIED = 1 << 12,

            EMPTY_RIGHT = 1 << 13,
            EMPTY_LEFT = 1 << 14,

        }

        /// <summary>
        /// The spaces this card "occupies" other than the space they're on.
        /// This is used in cases where you don't want things to be played on a tile because
        /// some effect might be messed up.
        /// (e.g.) Jelly Dancer (OccupyFront) - she summons a minion in front of her so to get the most
        /// value out of her, you shouldn't play a minion in front of her.
        /// </summary>
        public enum BoardOccupyLimit
        {
            /// <summary>
            /// Only occupies the tile its on.
            /// </summary>
            None,
            /// <summary>
            /// Occupies the tile its on and the tile in front of it.
            /// </summary>
            OccupyFront,
            /// <summary>
            /// Occupied the tile its on and the tile in back of it.
            /// </summary>
            OccupyBack
        }

        /// <summary>
        /// The order this card will try to be played.
        /// </summary>
        public Order order;
        public PlacementBehavior placementBehavior;
        /// <summary>
        /// Where on the board the card will try to be played, Default for best strategy.
        /// </summary>
        public BoardPlacement placement;
        /// <summary>
        /// The spaces this card "occupies" other than the space they're on.
        /// This is used in cases where you don't want things to be played on a tile because
        /// some effect might be messed up.
        /// (e.g.) Jelly Dancer (OccupyFront) - she summons a minion in front of her so to get the most
        /// value out of her, you shouldn't play a minion in front of her.
        /// </summary>
        public BoardOccupyLimit occupyLimit;
        /// <summary>
        /// Helps this card in determining how to behave.
        /// </summary>
        public AiStrategy strategy;
        /// <summary>
        /// As the Ai they are limited in what pool of cards they're able to grab from Redeem or other
        /// effects that generate cards, to keep the ai more predictable and simple.
        /// </summary>
        public List<string> cardPool;

        [ShowIf("@this.placementBehavior.HasFlag(AiProperties.PlacementBehavior.SPECIFIC_MINION)")]
        [ValueDropdown("GetAllCards")]
        public string cardId;

        public void Init()
        {
            order = Order.AnyOrder;
            placement = BoardPlacement.Default;
            occupyLimit = BoardOccupyLimit.None;
            cardPool = new List<string>();
            strategy = new AiStrategy
            {
                type = AiStrategy.Type.Early,
                targetConditions = new List<TargetConditions>(),
                targetScoreConditions = new List<TargetScoreCondition>(),
            };
        }

#if UNITY_EDITOR
        public static ValueDropdownList<string> cards;

        private static System.Collections.IEnumerable GetAllCards()
        {
            if (cards != null) return cards;

            cards = new ValueDropdownList<string>();

            if (!Db.Initialized)
            {
                Db.ForceInit();
            }

            for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
            {
                var cardData = Db.CardDatabase.Cards[i];
                cards.Add($"{cardData.faction}/{cardData.name}", cardData.id);
            }


            return cards;
        }
#endif
    }



    [System.Serializable]
    public class AiStrategy
    {
        /// <summary>
        /// Helps determine when and how to play this card.
        /// </summary>
        public enum Type
        {
            /// <summary>
            /// Used in the early game with few restrictions.
            /// </summary>
            Early,
            /// <summary>
            /// Used in combination with other cards
            /// </summary>
            Combo,
            /// <summary>
            /// Try to use whenever possible, its a key card for the strategy
            /// </summary>
            Meta,
            /// <summary>
            /// Used when you're higher on tempo
            /// [When you are 1 or more health stages above the opponent or you're at the same health stage and have a better board score.]
            /// </summary>
            Tempo,
            /// <summary>
            /// Played defensively when needed.
            /// </summary>
            Defensive,
        }
        /// <summary>
        /// Determines what this card targets (if applicable)
        /// </summary>
        public enum Targets
        {
            /// <summary>
            /// no targets set, if this has a targeted ability it will target randomly
            /// </summary>
            None,
            /// <summary>
            /// Targets the opponent hero
            /// </summary>
            TargetOpponentHero,
            /// <summary>
            /// Targets our hero (healing?)
            /// </summary>
            TargetPlayerHero,
            /// <summary>
            /// Targets the opponents minions
            /// </summary>
            TargetOpponentMinion,
            /// <summary>
            /// Targets our own minions (buffs?)
            /// </summary>
            TargetPlayerMinion,
            /// <summary>
            ///
            /// </summary>
            TargetDoesNotHaveStatusEffect
        }
        /// <summary>
        /// What kind of "scoring" method is used for spells.
        /// </summary>
        public enum TargetScoreMethod
        {
            None,
            DamageRemovedFromOpponentsSide,
            MinionsOccupyingOpponentsSide,
            MinimumTargets,
            MinimumDamageScore,
            MinimumStatAmount,
            RiffActivator,
            MinimumCardsOfTypeInHand,
            MinimumCardsPlayedThisTurn,
        }

        public Type type;
        public bool selectionRequired;
        public bool disableTriggerDuringAiThink;
        public int priority;
        public int overrideCost = -1;
        public List<TargetConditions> targetConditions;
        public List<TargetScoreCondition> targetScoreConditions;
    }

    [System.Serializable]
    public class TargetConditions
    {
        public FilterCondition triggerCondition;
        public FilterCondition targetCondition;
        public FilterConditionValues targetConditionValues;
    }

    [Serializable]
    public class TargetScoreCondition
    {
        public AiStrategy.TargetScoreMethod targetScoreMethod;
        public int checkAbilityAtIndex;
        [ShowIf("@this.targetScoreMethod == AiStrategy.TargetScoreMethod.MinimumStatAmount")]
        public FilterConditionValues.Stat stat;
        [ShowIf("@this.targetScoreMethod == AiStrategy.TargetScoreMethod.MinimumStatAmount")]
        public FilterConditionValues.StatFormula formula;
        public int minimumScore;
    }
}