﻿using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Data;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using Mono.CSharp;
using Newtonsoft.Json;
using Source.Scripts.Server.GameLogic.Data;
using UnityEngine;
using Class = CrossBlitz.Card.Class;

namespace CrossBlitz.ServerAPI.GameLogic.Data
{
    [System.Serializable]
    public class GameState : IDisposable
    {
        /// <summary>
        /// The game mode tells us where we initiated the game
        /// </summary>
        public GameType GameType;

        /// <summary>
        /// tells us if we're online or offline
        /// </summary>
        public ConnectionMode ConnectionMode;

        /// <summary>
        /// The room id is the id of the Photon room we either joined or created.
        /// </summary>
        public string RoomId;

        /// <summary>
        /// The state of the board, this can have readable data since both
        /// players see this at all times.
        /// </summary>
        public BoardState Board;

        /// <summary>
        /// The player who's turn it is
        /// Uses PF_PlayerData.PlayerId
        /// </summary>
        public string CurrentPlayer;

        /// <summary>
        /// The start time of the game
        /// </summary>
        public long StartTime;

        /// <summary>
        /// The start time of the turn, this is used to determine how much time is left
        /// in a turn.
        /// </summary>
        public long TurnStartTime;

        /// <summary>
        /// The turn count for the client player
        /// </summary>
        public int PlayerTurnCount;

        /// <summary>
        /// The turn count for the opponent player
        /// </summary>
        public int OpponentTurnCount;

        /// <summary>
        /// A list of player infos, this data is readable by the other player
        /// so things like cards in the players hand should never be available in here.
        /// </summary>
        public List<PlayerBattleState> Players;

        /// <summary>
        /// All cards, this is the only place where the 'GameCardState' class is
        /// serialized, use Uids and State.GetCard() calls to get these never store them.
        /// </summary>
        public List<GameCardState> Cards;

        public Dictionary<string, GameCardState> CardsMap;

        /// <summary>
        /// shared history about the game, this should only be filled in with history when
        /// a player takes his/her turn. Otherwise, there will be duplicates
        /// </summary>
        public List<Command> History;

        /// <summary>
        /// History displays used for debugs.
        /// </summary>
        public List<string> HistoryDisplays;

        /// <summary>
        /// Updates frozen minion states
        /// </summary>
        public FrozenStatusUpdater FreezeUpdater;

        /// <summary>
        /// Updates all game analytics and disposing of the game
        /// </summary>
        public GameStats gameStats;

        /// <summary>
        /// Checks for and updates all auras in the game.
        /// </summary>
        public AuraUpdater AuraUpdater;

        /// <summary>
        /// Updates cutscene events
        /// </summary>
        public BattleEventsUpdater BattleEventsUpdater;

        public List<GameEventUpdater> GameEventUpdaters;

        public bool IsClientsTurn()
        {
            return CurrentPlayer == GameServer.ClientPlayerState.Uid;
        }
        
        public bool IsLocalPlayersTurn()
        {
            return CurrentPlayer == GameServer.GetLocalPlayerState().Uid;
        }

        public Team CurrentTeamTurn()
        {
            return IsClientsTurn() ? Team.Client : Team.Opponent;
        }

        public void ChangeTurns()
        {
            if (IsClientsTurn())
            {
                SetCurrentPlayer(GameServer.OpponentPlayerState.Uid);
            }
            else
            {
                SetCurrentPlayer(GameServer.ClientPlayerState.Uid);
            }
        }

        public int GetTurnCount(Team team)
        {
            return team == Team.Client ? PlayerTurnCount : OpponentTurnCount;
        }

        public void IncreaseTurnCounter(Team team)
        {
            if (team == Team.Client)
            {
                PlayerTurnCount++;
            }
            else
            {
                OpponentTurnCount++;
            }
        }

        public void SetCurrentPlayer(string uid)
        {
            CurrentPlayer = uid;
        }

        public string GetCurrentPlayer()
        {
            return CurrentPlayer;
        }

        public void AddCard(GameCardState card)
        {
            Cards ??= new List<GameCardState>();
            CardsMap ??= new Dictionary<string, GameCardState>();

            if (Cards.Contains(card))
            {
                Debug.LogError($"You are trying to add a card twice! This is not allowed! {card.uid}");
                return;
            }

            Cards.Add(card);
            CardsMap.Add( card.uid, card );
        }

        public void ReleaseCard(string uid)
        {
            var card = GetCard(uid);

            if (card == null) return;

            CardsMap.Remove(uid);
            Cards.Remove(card);
        }

        public void InitializeRelics()
        {
            var commands = new List<Command>();

            for (var i = 0; i < 2; i++)
            {
                var team = (Team)i;
                var player = GameServer.GetPlayerState(team);

                for (var j = 0; j < player.relics.Count; j++)
                {
                    var relic = GetCard(player.relics[j]);

                    if (relic != null)
                    {
                        if (relic.HasAbility(AbilityKeyword.AURA))
                        {
                            var aura = relic.GetAbility(AbilityKeyword.AURA);
                            var auraIndex = relic.GetAbilityIndex(aura);

                            var auraCmd = new AuraCommand
                            {
                                SourceUid = relic.uid,
                                abilityIndex = auraIndex,
                            };

                            commands.Add(auraCmd);
                        }
                        else if (relic.HasTrigger(TriggerType.GAME_START))
                        {
                            var ability = relic.GetAbility(TriggerType.GAME_START);
                            var cmd = global::GameLogic.CreateCommandFromAbility(relic, ability, null, null);
                            commands.AddRange(cmd);
                        }
                    }
                }
            }

            global::GameLogic.StartCommands(commands);
        }

        public void OnCardTransformed(GameCardState card)
        {

        }

        public bool FactionExitsOnBoard(Faction faction, string playerGuid = null)
        {
            return Cards.Exists(card => card.location == CardLocation.Board && card.data?.faction == faction && (string.IsNullOrEmpty(playerGuid) || card.owner == playerGuid));
        }

        public bool ClassExistsOnBoard(Class @class, string playerGuid = null)
        {
            return Cards.Exists(card => card.location == CardLocation.Board && card.data?.@class == @class && (string.IsNullOrEmpty(playerGuid) || card.owner == playerGuid));
        }

        /// <summary>
        /// Adds a trick card to the board and writes it's history.
        /// </summary>
        /// <param name="command"></param>
        // public void SetTrap(PlayCommand command)
        // {
        //     var card = GetCard( command.SourceUid );
        //
        //     if (card.data.type != CardType.Trick)
        //     {
        //         Debug.LogWarningFormat("Card you're trying to add is not a trick!");
        //         return;
        //     }
        //
        //     if (!AI.Thinking)
        //     {
        //         if (!command.IsClientCommand())
        //         {
        //             if (card.GetCost() <= GameServer.OpponentPlayerState.currentMana)
        //             {
        //                 GameServer.ModifyMana(Team.Opponent, -card.GetCost());
        //             }
        //             else
        //             {
        //                 Debug.LogWarningFormat("Opponent doesn't have enough mana.");
        //             }
        //         }
        //         else
        //         {
        //             if (card.GetCost() <= GameServer.ClientPlayerState.currentMana)
        //             {
        //                 GameServer.ModifyMana(Team.Opponent, -card.GetCost());
        //             }
        //             else
        //             {
        //                 Debug.LogWarningFormat("I don't have enough mana.");
        //             }
        //         }
        //
        //         // move this card to the board
        //         card.SetCardLocation(CardLocation.Board);
        //
        //         AddHistory(command);
        //     }
        // }

        public void PlaySpell(PlayCommand command)
        {
            var card = GetCard( command.SourceUid );

            if (card == null || GameCardState.IsNullSpell(card))
            {
                Debug.LogWarningFormat("Spell you're trying to play is null!");
                return;
            }

            if (!AI.Thinking)
            {
                if (!command.AutoCast)
                {
                    if (!PayCostToPlayCard(card)) return;
                }
            }

            card.Use();

            var player = command.IsClientCommand() ? GameServer.ClientPlayerState : GameServer.OpponentPlayerState;

            if (player.CardIsInHand(card.uid))
            {
                player.RemoveHandCard(card.uid, CardLocation.Null);
            }
        }

        /// <summary>
        /// Summons a minion to a specific position onto the board
        /// </summary>
        /// <param name="command"></param>
        /// <param name="aiOnly">Doesn't update hand or mana, just updates the board</param>
        /// <param name="summonedMinions"></param>
        public bool SummonMinion(SummonCommand command, out List<string> summonedMinions)
        {
            bool summonSuccessful = false;

            summonedMinions = new List<string>();

            switch (command.SummonMethod)
            {
                case SummonCommand.Method.PlayedFromHand:
                    summonSuccessful = SummonMinion( GetCard( command.SourceUid ), command, command.Position);

                    if (summonSuccessful)
                    {
                        summonedMinions.Add( command.SourceUid );
                    }

                    break;
                case SummonCommand.Method.PlayedWithAbilityFromHand:
                case SummonCommand.Method.PlayedWithAbility:

                    if (command.TargetUids == null || command.TargetUids.Count <= 0)
                    {
                        command.TargetUids = new List<string> { command.SourceUid };
                    }

                    if (command.Positions == null || command.Positions.Count <= 0)
                    {
                        command.Positions = new List<Vector2Int> { command.Position };
                    }

                    for (var i = 0; i < command.Positions.Count; i++)
                    {
                        var success = SummonMinion(GetCard( command.TargetUids[i] ), command, command.Positions[i]);

                       if (success)
                       {
                           summonedMinions.Add(command.TargetUids[i]);
                       }

                       summonSuccessful = summonSuccessful || success;
                    }
                    break;
            }

            return summonSuccessful;
        }

        private bool SummonMinion(GameCardState card, SummonCommand command, Vector2Int position)
        {
            if (card == null)
            {
                Debug.LogError("[SUMMON FAILED] CARD NULL");
                return false;
            }

            if (GameCardState.IsNullOrDeadCanBeInGraveyard(card))
            {
                Debug.LogError($"[SUMMON FAILED] CARD IS DEAD! {card.characterData.name} {card.characterData.GetHealth()}");
                return false;
            }

            if (card.characterData.type == CardType.Spell)
            {
                Debug.LogError("[SUMMON FAILED] CARD IS A SPELL! CAN'T SUMMON SPELLS.");
                return false;
            }

            var tile = GameServer.State.Board.GetTile(position);
            var opponentCommand = !command.IsClientCommand();

            if (!AI.Thinking)
            {
                if (tile.Occupied && tile.Card.uid != card.uid && !GameCardState.IsPendingNullOrDead( tile.Card ))
                {
                    if (opponentCommand)
                    {
                        AI.FailedSummonAttempt = true;
                    }

                    Debug.LogError($"[FAILED] This tile is occupied. Occupied By: ({tile.Position}) {tile.Card.data.id} Trying to Summon: {card.data.id}");
                    return false;
                }
            }

            if (command.SummonMethod == SummonCommand.Method.PlayedFromHand && !AI.Thinking)
            {
                if (!PayCostToPlayCard(card)) return false;
            }

            card.owner = position.y >= 2 ? GameServer.OpponentPlayerState.Uid : GameServer.ClientPlayerState.Uid;
            card.turnSummonedOnBoard = Mathf.Clamp(GameServer.State.GetTurnCount( card.Team ), 1, int.MaxValue);

            var abilities = card.GetAbilities();

            for (var i = 0; i < abilities.Count; i++)
            {
                switch (abilities[i].keyword)
                {
                    case AbilityKeyword.TOUGH:
                        card.AddStatusEffect(StatusEffect.Tough);
                        break;
                    case AbilityKeyword.BARRIER:
                        card.AddStatusEffect(StatusEffect.Barrier);
                        break;
                }
            }

            //Debug.Log($"Setting tile {position} - {card.characterData.name}");

            Board.SetTile(position, card.uid);

            if ( (command.SummonMethod == SummonCommand.Method.PlayedFromHand || command.SummonMethod == SummonCommand.Method.PlayedWithAbilityFromHand) && !AI.Thinking)
            {
                if (!opponentCommand)
                {
                    GameServer.ClientPlayerState.RemoveHandCard(card.uid, CardLocation.Null);
                }
                else
                {
                    GameServer.OpponentPlayerState.RemoveHandCard(card.uid, CardLocation.Null);
                }
            }

            return true;
        }

        public static bool CanPayCostOfCard(GameCardState card)
        {
            var playerState = GameServer.GetPlayerState(card.Team);
            var costsHealthInsteadOfMana = GameServer.State.AuraUpdater.CostsHealthInsteadOfMana(playerState.Uid, out _, out _);

            if (costsHealthInsteadOfMana)
            {
                var hero = GameServer.State.GetCard(playerState.heroUid);

                if (card.GetCost() > hero.GetHealth())
                {
                    return false;
                }
            }
            else if (card.GetCost() > playerState.currentMana)
            {
                return false;
            }

            return true;
        }

        public bool PayCostToPlayCard( GameCardState card )
        {
            if (!CanPayCostOfCard(card))
            {
                return false;
            }

            GameServer.ModifyManaFromPlayedCard(card.Team, card);

            return true;
        }

        /// <summary>
        /// Gets the last summoned minion, this uses the history of summons
        /// This is probably only used when a minion is triggered by summoning so it should work fine for that.
        /// </summary>
        /// <returns></returns>
        public List<string> GetLastSummonMinions()
        {
            if (!(History.FindLast(x => x.Type == CommandType.SUMMON) is SummonCommand snippet))
            {
                Debug.LogWarningFormat("could not find any 'Summon' history snippets.");
                return null;
            }

            if (snippet.SummonMethod == SummonCommand.Method.PlayedFromHand)
            {
                return new List<string> { snippet.SourceUid };
            }

            return snippet.TargetUids;
        }

        /// <summary>
        /// Get the hero of a certain team.
        /// </summary>
        /// <param name="team">The team of the hero you get.</param>
        /// <returns></returns>
        public GameCardState GetHero(Team team)
        {
            if (team == Team.Opponent)
            {
                return GetCard(GameServer.OpponentPlayerState.heroUid);
            }

            return GetCard(GameServer.ClientPlayerState.heroUid);
        }

        /// <summary>
        /// Get a card by uid
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public GameCardState GetCard(string uid)
        {
            if (string.IsNullOrEmpty(uid))
            {
                return null;
            }

            if (CardsMap.ContainsKey(uid))
            {
                return CardsMap[uid];
            }

            return Cards.Find(card => card?.uid == uid);
        }

        public GameCardState GetCard(Vector2Int position)
        {
            return Cards.Find(card => card?.position == position);
        }

        public List<GameCardState> GetCardsWithId(string cardId)
        {
            Cards ??= new List<GameCardState>();
            return Cards.FindAll(card => card?.characterData?.id == cardId);
        }

        public List<GameCardState> GetCardsInLocation(CardLocation location)
        {
            return Cards.FindAll(c => c.location == location);
        }

        public List<GameCardState> GetCards(Predicate<GameCardState> filter)
        {
            return Cards.FindAll(filter);
        }

        // public GameCardState GetCardByName(string name)
        // {
        //     return Cards.Find(card => card?.characterData?.name == name);
        // }
        //
        // public GameCardState GetCardById(string id)
        // {
        //     return Cards.Find(card => card?.characterData?.id == id);
        // }

        public GameCardState GetBlitzBurst(Team team)
        {
            var player = GameServer.GetPlayerState(team);
            return GetCard(player.blitzBurst);
        }

        /// <summary>
        /// Adds history to the game (for later lookup and or other operations)
        /// </summary>
        /// <param name="command"></param>
        public void AddHistory(Command command)
        {
            if (!History.Contains(command))
            {
                History.Add(command);
            }

            var display = command.GetLabelDisplay();
            HistoryDisplays.Add(display);
        }

        public Command GetHistory(int historyId)
        {
            return History.Find(h => h.Guid == historyId);
        }

        public List<Command> GetCardHistories(string uid)
        {
            if (History == null) return new List<Command>();
            return History.FindAll(history => history != null && !string.IsNullOrEmpty(history.SourceUid) && history.TargetUids != null && (history.SourceUid == uid || history.TargetUids.Contains(uid)));
        }

        public List<Command> GetHistories(CommandType type)
        {
            var histories = new List<Command>();
            if (History == null) return histories;

            for (var i = History.Count - 1; i > -1; i--)
            {
                if (History[i].Type == type) histories.Add(History[i]);
            }

            return histories;
        }

        public Command GetLastHistoryOf(CommandType type)
        {
            if (History == null) return Command.Empty();
            return History.FindLast(c => c.Type == type);
        }

        /// <summary>
        /// Converts the game state into a json object for the server.
        /// </summary>
        /// <returns></returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Server.JsonSettings);
        }

        public static GameState FromJson(string json)
        {
            return JsonConvert.DeserializeObject<GameState>(json, Server.JsonSettings);
        }

        public GameState Copy()
        {
            var copiedState = FromJson(ToJson());
            // Debug.Log($"x Card List Count = { Cards.Count } Cards Dict Count = { CardsMap.Count }");
            // Debug.Log($"Card List Count = { copiedState.Cards.Count } Cards Dict Count = { copiedState.CardsMap.Count }");
            return copiedState;
        }

        public void Dispose()
        {
            for (var i = 0; i < GameEventUpdaters.Count; i++)
            {
                GameEventUpdaters[i].Kill();
                GameEventUpdaters[i] = null;
            }

            GameEventUpdaters = null;
        }
    }
}