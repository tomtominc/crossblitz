using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Hero;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI.GameLogic.Intelligence;
using Newtonsoft.Json;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ServerAPI.GameLogic.Data
{
    /// <summary>
    /// The game card state is an object that holds some card data
    /// Cards are everything in Cross Blitz - Minions, Spells, Tricks, Heroes, Powers
    /// It's easier to filter effects if everything is the same type of object
    /// </summary>
    [System.Serializable]
    public class GameCardState
    {
        /// <summary>
        /// The unique id specific for this match
        /// </summary>
        public string uid;

        /// <summary>
        /// the owner id, this is just the username of the player
        /// </summary>
        public string owner;

        /// <summary>
        /// The last location the card was in.
        /// </summary>
        public CardLocation lastLocation;

        /// <summary>
        /// Where is this card right now?
        /// </summary>
        public CardLocation location;

        /// <summary>
        /// An easy way to get certain character data that's
        /// similar in both heroes and cards
        /// </summary>
        [JsonIgnore] public CharacterData characterData => IsHero ? heroData : (CharacterData)data;

        public string characterId;

        /// <summary>
        /// The card data, this is all read only stuff
        /// </summary>
        [JsonIgnore] public CardData data => Db.CardDatabase.GetCard(characterId);

        /// <summary>
        /// The hero data (this is if the card is a hero, null otherwise)
        /// </summary>
        public HeroData heroData;

        /// <summary>
        /// The history of this specific card
        /// </summary>
        public List<CardHistory> history;

        /// <summary>
        /// This is applied history. This is so the view doesn't update the stats until we apply it in the view
        /// </summary>
        public List<CardHistory> pendingHistory;

        /// <summary>
        /// Armor can only be had on a hero
        /// </summary>
        public int armor;

        /// <summary>
        /// This is zero unless it's on the board.
        /// </summary>
        public Vector2Int lastPosition;

        /// <summary>
        /// This is zero unless it's on the board.
        /// </summary>
        public Vector2Int position;

        //public List<Ability> abilities;
        /// <summary>
        /// Abilities have been given to us, we can't just add these to the
        /// CardData since we treat the card data as read only while playing.
        /// </summary>
        public List<Ability> gameAbilities;

        /// <summary>
        /// The current value of focus cards.
        /// </summary>
        public List<CardAbilityState> abilityStates;

        /// <summary>
        /// The cards current status effect, this variable can contain multiple effects at once.
        /// </summary>
        public StatusEffect statusEffect;

        /// <summary>
        /// The amount of turns this minion has been frozen.
        /// Will unfreeze after FrozenStatusUpdater.TurnsUntilFrozenFinishes
        /// </summary>
        public int turnFrozen;

        /// <summary>
        /// This will be true, if the player has taken this hero power and created a card from it
        /// will be false otherwise.
        /// </summary>
        public bool powerHasBeenTaken;

        /// <summary>
        /// The current value of the limit break, should be incremented whenever
        /// conditions are met.
        /// </summary>
        public int limitBreakValue;

        /// <summary>
        /// The turn this card was added to the players hand.
        /// If this card was never added to hand, it will be -1
        /// If added to hand during the mulligan phase, this is 0.
        /// </summary>
        public int turnAddedToHand = -1;

        /// <summary>
        /// If this card was added to hand by a card's effect,
        /// the Id of the card would be filled in, otherwise, this is empty.
        /// </summary>
        public string addedToHandByCard;

        /// <summary>
        /// turns this card has been alive on board, if 0, they were summoned this turn.
        /// </summary>
        public int turnSummonedOnBoard;

        /// <summary>
        /// Was this card revealed to the opponent by some means?
        /// When cards are added to a players deck or stolen to a player, usually its been revealed
        /// </summary>
        public bool revealedToTheOpponent;

        //todo:
        // remove armor modifiers and just use a variable
        public int armorAmount;

        /// <summary>
        /// If the cost of this minion has been overridden from its database stats, otherwise,
        /// cards use their database stats for cost + modifiers
        /// </summary>
        public int overrideCost=-1;

        /// <summary>
        /// If the power of this minion has been overridden from its database stats, otherwise,
        /// cards use their database stats for power + modifiers
        /// </summary>
        public int overridePower=-1;

        /// <summary>
        /// If the health of this minion has been overridden from its database stats, otherwise,
        /// cards use their database stats for health + modifiers
        /// </summary>
        public int overrideMaximumHealth=-1;

        private const int ForceRefreshStat = -1000;

        public int m_health=ForceRefreshStat;
        public int m_pendingHealth=ForceRefreshStat;

        public int m_power = ForceRefreshStat;
        public int m_pendingPower = ForceRefreshStat;

        [JsonIgnore] public bool IsHero => heroData != null;

        // used for focus and other modifier stuff.
        public int modifierIncrease;
        // used to increase thorns
        public int thornsIncrease;
        /// <summary>
        /// The identifier of the card that destroyed this card.
        /// </summary>
        public string destroyedByCardUid;

        /// <summary>
        /// The team this card is on, checks the owner so it's dynamic between both clients
        /// </summary>

        [JsonIgnore] public Team Team => owner == GameServer.ClientPlayerState.Uid ? Team.Client : Team.Opponent;

        public void Init(bool asNew = true)
        {
            uid = GUID.CreateUnique();
            if (asNew) history = new List<CardHistory>();
            if (asNew) pendingHistory = new List<CardHistory>();
            GameServer.State.AddCard(this);
        }

        public void ChangeOwners(Team team)
        {
            owner = team == Team.Client ? GameServer.ClientPlayerState.Uid : GameServer.OpponentPlayerState.Uid;
            turnSummonedOnBoard = GameServer.State.GetTurnCount(team);
        }

        /// <summary>
        /// transform this card into a new card
        /// </summary>
        /// <param name="transformInto">The card database id of the card to transform into.</param>
        /// <param name="keepStatsAndUpgradesOfPreviousMinion">if true, the card will not be silenced when transforming.</param>
        public string TransformCard(string transformInto, bool keepStatsAndUpgradesOfPreviousMinion)
        {
            // silence the card..
            if (!keepStatsAndUpgradesOfPreviousMinion)
            {
                Silence();
            }

            var transformHistory = new CardHistory();
            transformHistory.UpdateIdentifier();
            transformHistory.CardUid = uid;
            transformHistory.TransformedInto = transformInto;
            pendingHistory.Add(transformHistory);

            return transformHistory.Uid;
        }

        public void ExecutePendingTransformation(string transformHistoryId, bool keepStatsAndUpgradesOfPreviousMinion)
        {
            var pendingTransform = pendingHistory.Find(h => h.Uid == transformHistoryId);

            if (pendingTransform == null)
            {
                Debug.LogError($"No pending transform with id: {transformHistoryId}");
                return;
            }

            if (string.IsNullOrEmpty(pendingTransform.TransformedInto))
            {
                Debug.LogError("Transformed into has not been set.");
                return;
            }

            if (pendingTransform.Applied)
            {
                Debug.LogError("This transform has already been applied");
                return;
            }

            silenced = false;
            turnSummonedOnBoard = GameServer.State.GetTurnCount(Team);

            if (!keepStatsAndUpgradesOfPreviousMinion)
            {
                // keep pending transforms in case we transform multiple times.
                var pendingTransforms = pendingHistory.FindAll(h =>
                    h.Uid != pendingTransform.Uid && !string.IsNullOrEmpty(h.TransformedInto));

                history = new List<CardHistory>();
                pendingHistory = new List<CardHistory>();
                gameAbilities = new List<Ability>();
                abilityStates = new List<CardAbilityState>();
                pendingHistory.AddRange( pendingTransforms );

                RemoveAllStatusEffects();
            }
            else
            {
                history.RemoveAll(h => h.DamageAmount > 0 || h.RemoveBarrier || h.HealthModifier > 0 || h.PowerModifier > 0);
                pendingHistory.RemoveAll(h => h.DamageAmount > 0 || h.RemoveBarrier || h.HealthModifier > 0 || h.PowerModifier > 0);

                var modifier = new CardHistory
                {
                    AppliedBy = uid,
                    HealthModifier = GetHealth(),
                    PowerModifier = GetPower()
                };

                modifier.UpdateIdentifier();

                AddPendingHistory( modifier );
                ApplyHistory(modifier.Uid);

                var previousMinionAbilities = new List<Ability>(gameAbilities);
                gameAbilities = null;
                characterId = pendingTransform.TransformedInto;

                GetAbilities();
                AddAbilities(previousMinionAbilities, 0);
            }

            characterId = pendingTransform.TransformedInto;

            RefreshStats();
            GameServer.State.OnCardTransformed(this);
        }

        public void RefreshStats()
        {
            m_power = ForceRefreshStat;
            m_health = ForceRefreshStat;
            m_pendingPower = ForceRefreshStat;
            m_pendingHealth = ForceRefreshStat;
        }

        /// <summary>
        /// Gets the cost of this minion with all the modifiers
        /// </summary>
        /// <returns>The cost of this minion</returns>
        public int GetCost()
        {
            var costReduction = GetAbility(AbilityKeyword.COST_REDUCED_BY_CONDITION);
            var databaseCost = GetDatabaseCost();
            var costReductionFromHistory = history.Sum(h => h.CostModifier);
            var costReducedBy = 0;
            var costAura = GameServer.State.AuraUpdater.GetCostAura(uid);

            if (costReduction != null)
            {
                switch (costReduction.abilityCondition.condition)
                {
                    case Condition.ARMOR_AMOUNT:
                    {
                        var hero = GameServer.State.GetCard(GameServer.GetPlayerState(Team).heroUid);
                        costReducedBy = hero.GetArmor();
                        break;
                    }
                    case Condition.SPELLS_PLAYED_THIS_TURN:
                    {
                        var hero = GameServer.GetPlayerState(Team);
                        costReducedBy = hero.spellsCastThisTurn;
                        break;
                    }
                    case Condition.NUMBER_OF_SPECIFIC_CARDS_IN_DECK:
                    {
                        if (costReduction.abilityCondition.filters.HasFlag(FilterCondition.PLAYER))
                        {
                            var player = GameServer.GetPlayerState(Team);
                            costReducedBy = player.GetNumberOfCardsWithIdInDeck(costReduction.abilityCondition.cardId);
                        }
                        else if (costReduction.abilityCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            var player = GameServer.GetPlayerState(Team == Team.Client ? Team.Opponent : Team.Client);
                            costReducedBy = player.GetNumberOfCardsWithIdInDeck(costReduction.abilityCondition.cardId);
                        }
                        break;
                    }
                    case Condition.TRAPS_SET:
                    {
                        List<TileState> occupiedTiles;

                        if (costReduction.abilityCondition.filters.HasFlag(FilterCondition.PLAYER) &&
                            costReduction.abilityCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles(  );
                        }
                        else  if (costReduction.abilityCondition.filters.HasFlag(FilterCondition.PLAYER))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles( Team  );
                        }
                        else if (costReduction.abilityCondition.filters.HasFlag(FilterCondition.OPPONENT))
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles( Team == Team.Client ? Team.Opponent : Team.Client  );
                        }
                        else
                        {
                            occupiedTiles = GameServer.State.Board.GetOccupiedTiles(  );
                        }

                        var traps = 0;

                        for (var i = 0; i < occupiedTiles.Count; i++)
                        {
                            var card = GameServer.State.GetCard(occupiedTiles[i].CardId);
                            if(card.characterData.type == CardType.Trick) traps++;
                        }

                        costReducedBy = traps * 3;
                        break;
                    }
                    case Condition.NUMBER_OF_CARDS_PLAYED_FROM_DIFFERENT_FACTIONS:
                    {
                        var num = 0;
                        var playCommands = GameServer.State.GetHistories(CommandType.PLAY);

                        for (var i = 0; i < playCommands.Count; i++)
                        {
                            var command = playCommands[i];
                            var source = GameServer.State.GetCard(command.SourceUid);
                            var player = GameServer.GetPlayerState(command.PlayerUid);

                            if (costReduction.abilityCondition.filters.HasFlag(FilterCondition.PLAYER) && player.GetTeam() != source.Team)
                            {
                                continue;
                            }

                            if (costReduction.abilityCondition.filters.HasFlag(FilterCondition.OPPONENT) && player.GetTeam() == source.Team)
                            {
                                continue;
                            }

                            if (source.characterData != null && source.characterData.faction != Faction.Neutral && source.characterData.faction != player.faction)
                            {
                                num++;
                            }
                        }

                        var minionSummons = GameServer.State.GetHistories(CommandType.SUMMON);

                        for (var i = 0; i < minionSummons.Count; i++)
                        {
                            var command = (SummonCommand)minionSummons[i];

                            if (command.SummonMethod != SummonCommand.Method.PlayedFromHand) continue;

                            var source = GameServer.State.GetCard(command.SourceUid);
                            var player = GameServer.GetPlayerState(command.PlayerUid);

                            if (costReduction.abilityCondition.filters.HasFlag(FilterCondition.PLAYER) && player.GetTeam() != source.Team)
                            {
                                continue;
                            }

                            if (costReduction.abilityCondition.filters.HasFlag(FilterCondition.OPPONENT) && player.GetTeam() == source.Team)
                            {
                                continue;
                            }

                            if (source.characterData.faction != Faction.Neutral && source.characterData.faction != player.faction)
                            {
                                num++;
                            }
                        }

                        costReducedBy = num;
                        break;
                    }
                    case Condition.SPECIFIC_MINION_DIED_THIS_GAME:
                    {
                        var count = 0;
                        var cardId = costReduction.abilityCondition.cardId;

                        for (var i = 0; i < GameServer.State.Cards.Count; i++)
                        {
                            var card = GameServer.State.Cards[i];

                            if (card == null || card.characterData == null) continue;

                            var deadCard = card.characterData.id;

                            if (deadCard== "DevilcoreGroupie" && cardId == "BandWorshiper")
                            {
                                deadCard = cardId;
                            }

                            if (card.IsDead() && deadCard == cardId)
                            {
                                count++;
                            }
                        }

                        costReducedBy = count;
                        break;
                    }
                }
            }


            return Mathf.Clamp( databaseCost + costReductionFromHistory - costReducedBy + costAura, 0, 99);
        }

        public int GetDatabaseCost()
        {
            if (overrideCost >= 0) return overrideCost;
            return characterData.GetCost();
        }

        public CardHistory ModifyCost(string source, int amount)
        {
            var snippet = new CardHistory
            {
                CardUid = uid,
                AppliedBy = source,
                CostModifier = amount
            };

            snippet.UpdateIdentifier();

            AddPendingHistory(snippet);

            return snippet;
        }

        /// <summary>
        /// Deals damage to this minion
        /// </summary>
        /// <param name="source">The card that's dealing the damage</param>
        /// <param name="damageAmount">The amount of damage this card deals</param>
        public List<CardHistory> DealDamage(string source, int damageAmount)
        {
            List<CardHistory> snippets = new List<CardHistory>();
            CardHistory snippet;

            var guarded = false;

            if (IsHero)
            {
                if (damageAmount > 1)
                {
                    var occupyingMinions = GameServer.State.Board.GetOccupiedTiles(Team);

                    for (var i = 0; i < occupyingMinions.Count; i++)
                    {
                        var card = GameServer.State.GetCard(occupyingMinions[i].CardId);

                        if (card.HasAbility(AbilityKeyword.GUARD))
                        {
                            guarded = true;
                            damageAmount = (int)(damageAmount * 0.5f);
                            break;
                        }
                    }
                }

                var totalArmor = GetPendingArmor();

                if (totalArmor > 0)
                {
                    if (totalArmor < damageAmount)
                    {
                        damageAmount -= totalArmor;

                        if(Team == Team.Client && App.Settings.GetUnlimitedPlayerHP()) damageAmount = 0;

                        snippet = new CardHistory
                        {
                            CardUid = uid,
                            AppliedBy = source,
                            ArmorRemoval = totalArmor,
                            Guarded = guarded
                        };

                        snippet.UpdateIdentifier();
                        snippets.Add(snippet);
                    }
                    else
                    {
                        if (Team == Team.Client && App.Settings.GetUnlimitedPlayerHP()) damageAmount = 0;

                        snippet = new CardHistory
                        {
                            CardUid = uid,
                            AppliedBy = source,
                            ArmorRemoval = damageAmount,
                            ArmorBreak = true,
                            Guarded = guarded
                        };

                        snippet.UpdateIdentifier();
                        snippets.Add(snippet);

                        damageAmount = 0;
                    }
                }
            }

            if (damageAmount > 0)
            {
                var removeBarrier = false;

                if (HasBarrier()) // check barrier separately so we can check if we're removing it already
                {
                    removeBarrier = true;
                    damageAmount = 0;
                }

                if (HasStatusEffect(StatusEffect.Tough))
                {
                    damageAmount = Mathf.Clamp(damageAmount - 1, 0, damageAmount);
                }

                if (Team == Team.Client && App.Settings.GetUnlimitedCardHP()) damageAmount = 0;

                snippet = new CardHistory
                {
                    CardUid = uid,
                    AppliedBy = source,
                    DamageAmount = damageAmount,
                    Guarded = guarded,
                    RemoveBarrier = removeBarrier
                };
                snippet.UpdateIdentifier();
                snippets.Add(snippet);
            }

            if (snippets.Count > 0)
            {
                for (var i = 0; i < snippets.Count; i++)
                {
                    AddPendingHistory(snippets[i]);
                }
            }

            return snippets;
        }

        public CardHistory GetHistory(string Uid)
        {
            var snippet = history.Find(h => h.Uid == Uid);
            return snippet ?? pendingHistory.Find(h => h.Uid == Uid);
        }

        public bool ApplyAllPendingHistories()
        {
            var appliedAny = false;

            for (var i = 0; i < pendingHistory.Count; i++)
            {
                var pending = pendingHistory[i];

                if (!pending.Applied)
                {
                    ApplyHistory(pending.Uid);
                    appliedAny = true;
                }
            }

            return appliedAny;
        }

        public void ApplyHistory(string Uid)
        {
            var snippet = pendingHistory.Find(h => h.Uid == Uid);

            if (snippet == null || snippet.Applied)
            {
                if (snippet == null) Debug.LogError($"Trying to Apply History but there is no snippet with Uid = {Uid}");
                //else Debug.LogError($"[{characterData.name}] Snippet has already been applied!\n{snippet.AppliedStackTrace}");
                return;
            }

            snippet.Applied = true;
            snippet.AppliedStackTrace = Environment.StackTrace;

            if (snippet.ArmorModifier > 0)
            {
                armorAmount += snippet.ArmorModifier;
            }
            else if (snippet.ArmorRemoval > 0)
            {
                armorAmount -= snippet.ArmorRemoval;
            }

            if (snippet.RemoveBarrier)
            {
                RemoveStatusEffect(StatusEffect.Barrier);
                pendingHistory.Remove(snippet);
            }

            armorAmount = Mathf.Clamp(armorAmount, 0, 99);

            history.Add(snippet);

            OnStatsChanged();

            if (AI.Thinking == false)
            {
                if (snippet.DamageAmount > 0)
                {
                    Events.Publish(this, EventType.OnCharacterDamaged, new OnCharacterDamagedEventArgs
                    {
                        characterUid = uid,
                        damageSourceUid = snippet.AppliedBy,
                        damageAmount = snippet.DamageAmount,
                        isClientCharacter = Team == Team.Client
                    });
                }
                else if (snippet.PowerModifier > 0 || snippet.HealthModifier > 0)
                {
                    Events.Publish(this, EventType.OnCharacterStatsModified, new OnCharacterStatsModifiedEventArgs
                    {
                        characterUid = uid,
                        appliedBy = snippet.AppliedBy,
                        powerAmount = snippet.PowerModifier,
                        healthAmount = snippet.HealthModifier,
                        isClientCharacter = Team == Team.Client
                    });
                }
            }
        }

        public void ApplyHistoryByCardGuid(string cardGuid)
        {
            var snippets = pendingHistory.FindAll(x => !x.Applied && x.AppliedBy == cardGuid);

            for (var i = 0; i < snippets.Count; i++)
            {
                var snippet = snippets[i];
                ApplyHistory(snippet.Uid);
            }
        }

        public void AddPendingHistory(CardHistory h)
        {
            pendingHistory.Add(h);

            OnStatsChanged();
        }

        private void OnStatsChanged()
        {
            m_health = ForceRefreshStat;
            m_power = ForceRefreshStat;

            m_pendingHealth = ForceRefreshStat;
            m_pendingPower = ForceRefreshStat;
        }

        public void AddStatusEffect(StatusEffect status)
        {
            statusEffect |= status;
        }

        public void RemoveStatusEffect(StatusEffect status)
        {
            statusEffect &= ~status;
        }

        public void RemoveAllStatusEffects()
        {
            statusEffect = 0;
            turnFrozen = 0;
        }

        public bool HasBarrier()
        {
            var hasStatusEffect = HasStatusEffect(StatusEffect.Barrier);

            if (!hasStatusEffect)
            {
                return false;
            }

            for (var i = 0; i < pendingHistory.Count; i++)
            {
                if (pendingHistory[i].RemoveBarrier)
                {
                    hasStatusEffect = false;
                    break;
                }
            }

            return hasStatusEffect;
        }

        public bool HasStatusEffect(StatusEffect statusCheck)
        {
            return statusEffect.HasFlag(statusCheck);
        }

        /// <summary>
        /// Gets the health of this minion with all of it's modifiers that have been applied (does not take into account modifiers that haven't been applied by the view).
        /// </summary>
        /// <returns>The health of the minion</returns>
        public int GetHealth()
        {
            if (m_health <= ForceRefreshStat)
            {
                m_health = GetHealth(history);
            }

            return m_health;
        }

        /// <summary>
        /// Gets the health of this minion with modifiers that haven't been applied to the view yet.
        /// </summary>
        /// <returns>The health of the minion</returns>
        public int GetPendingHealth()
        {
            if (m_pendingHealth <= ForceRefreshStat)
            {
                m_pendingHealth = GetHealth(pendingHistory);
            }

            return m_pendingHealth;
        }

        public int GetDatabaseMaxHealth()
        {
            if (overrideMaximumHealth > 0) return overrideMaximumHealth;
            return characterData.GetHealth();
        }

        public int GetMaxHealthWithBuffs()
        {
            if (overrideMaximumHealth > 0) return overrideMaximumHealth + pendingHistory.Sum(x => x.HealthModifier);
            return characterData.GetHealth() + pendingHistory.Sum(x => x.HealthModifier);
        }



        public int GetPendingHealth(List<string> historyExcludes)
        {
            var pendingHistoryWithExcludes = new List<CardHistory>(pendingHistory);
            pendingHistoryWithExcludes.RemoveAll(p => historyExcludes.Contains(p.Uid));
            return GetHealth(pendingHistoryWithExcludes);
        }

        private int GetHealth(IList<CardHistory> histories)
        {
            var healthModifiers = histories.Sum(x => x.HealthModifier);
            var damageAndHealing = histories.Sum(x => x.HealAmount) + healthModifiers - histories.Sum(x => x.DamageAmount);

            if (!AI.Thinking)
            {
                healthModifiers += GameServer.State.AuraUpdater.GetHealthAura(uid);
            }

            return Mathf.Clamp(GetDatabaseMaxHealth() + damageAndHealing, 0,  GetDatabaseMaxHealth() + healthModifiers);
        }

        public CardHistory Heal(string source, int amount)
        {
            var truncatedAmount = Mathf.Clamp(amount, 0, GetDatabaseMaxHealth() + pendingHistory.Sum(x => x.HealthModifier) - GetPendingHealth());

            var snippet = new CardHistory
            {
                CardUid = uid,
                AppliedBy = source,
                HealAmount = truncatedAmount
            };

            snippet.UpdateIdentifier();
            AddPendingHistory(snippet);

            return snippet;
        }

        public CardHistory ModifyHealth(string source, int amount)
        {
            var snippet = new CardHistory
            {
                CardUid = uid,
                AppliedBy = source,
                HealthModifier = amount
            };
            snippet.UpdateIdentifier();

            AddPendingHistory(snippet);

            return snippet;
        }

        public CardHistory ModifyPower(string source, int amount)
        {
            var snippet = new CardHistory
            {
                CardUid = uid,
                AppliedBy = source,
                PowerModifier = amount
            };
            snippet.UpdateIdentifier();

            AddPendingHistory(snippet);

            return snippet;
        }

        public CardHistory ModifyHealthAndPower(string source, int health, int power)
        {
            var snippet = new CardHistory
            {
                CardUid = uid,
                AppliedBy = source,
                HealthModifier = health,
                PowerModifier = power,
            };
            snippet.UpdateIdentifier();
            AddPendingHistory(snippet);

            return snippet;
        }

        public bool IsSlow()
        {
            return turnSummonedOnBoard >= GameServer.State.GetTurnCount(Team) && !HasAbility(AbilityKeyword.RUSH) && characterData.type != CardType.Trick;
        }

        /// <summary>
        /// Gets the power of this minion with all of it's modifiers
        /// </summary>
        /// <returns>The power of this minion</returns>
        public int GetPower()
        {
            if (m_power <= ForceRefreshStat)
            {
                m_power = GetPower(history);
            }

            return m_power;
        }

        public int GetPendingPower()
        {
            if (m_pendingPower <= ForceRefreshStat)
            {
                m_pendingPower = GetPower(pendingHistory);
            }

            return m_pendingPower;
        }

        private int GetPower(IEnumerable<CardHistory> histories)
        {
            var powerAura = GameServer.State.AuraUpdater.GetPowerAura(uid);
            var thornsActive = Team != GameServer.State.CurrentTeamTurn();

            if (thornsActive)
            {
                var thorns = GetAbility(AbilityKeyword.THORNS);

                if (thorns != null)
                {
                    powerAura += thorns.damageValue + thornsIncrease;
                }

                if (IsHero)
                {
                    var player = GameServer.GetPlayerState(Team);

                    for (var i = 0; i < player.relics.Count; i++)
                    {
                        var relic = GameServer.State.GetCard(player.relics[i]);
                        thorns = relic.GetAbility(AbilityKeyword.THORNS);

                        if (thorns != null)
                        {
                            powerAura += thorns.damageValue;
                        }
                    }
                }
            }

            return GetDatabasePower() + histories.Sum(x => x.PowerModifier) + powerAura;
        }

        public int GetDatabasePower()
        {
            if (overridePower > 0) return overridePower;
            return characterData.GetPower();
        }

        public CardHistory GrantArmor(string source, int amount)
        {
            if (!IsHero)
            {
                Debug.LogError("You're trying to give armor to something that is not a hero!");
                return null;
            }

            var snippet = new CardHistory
            {
                CardUid = uid,
                AppliedBy = source,
                ArmorModifier = amount
            };

            snippet.UpdateIdentifier();
            AddPendingHistory(snippet);

            return snippet;
        }

        public int GetArmor()
        {
            return armorAmount;
            //return GetArmor(history);
        }

        public int GetPendingArmor()
        {
            return GetArmor(pendingHistory);
        }

        private int GetArmor(IList<CardHistory> histories)
        {
            if (IsHero)
            {
                //return armorAmount;
                return Mathf.Clamp(histories.Sum(x => x.ArmorModifier) -
                                   histories.Sum(x => x.ArmorRemoval), 0, int.MaxValue);
            }

            return 0;
        }

        /// <summary>
        /// Use is only for Trick cards and spells.
        /// </summary>
        public void Use()
        {
            SetCardLocation(CardLocation.Graveyard);
        }

        public void BindTile(Vector2Int tilePosition)
        {
            SetCardLocation(CardLocation.Board);
            position = tilePosition;
        }

        public void UnbindTile(CardLocation newLocation)
        {
            SetCardLocation(newLocation);
            lastPosition = position;
            position = Vector2Int.zero;
        }

        /// <summary>
        /// Discards this card from the players hand.
        /// </summary>
        /// <returns>The index where the card was before.</returns>
        public int Discard()
        {
            var player = GameServer.GetPlayerState(Team);
            return player.DiscardHandCard(uid);
        }

        public void Flux(int indexToInsert)
        {
            var player = GameServer.GetPlayerState(Team);
            player.AddHandCard( uid, indexToInsert );
        }

        public void SetCardLocation(CardLocation newLocation, bool sendEvent = true)
        {
            if (location == newLocation)
            {
                return;
            }

            var cardChangeEventArgs = new OnCardChangedLocationEventArgs
            {
                cardUid = uid, oldLocation = location, newLocation = newLocation
            };

            if (location == CardLocation.Deck && newLocation == CardLocation.Graveyard)
            {
                var player = GameServer.GetPlayerState(owner);
                if (player != null) player.RemoveCardFromDeck(uid);
            }

            if (location == CardLocation.Deck && newLocation == CardLocation.Hand)
            {
                turnAddedToHand = GameServer.State.GetTurnCount( Team );
            }

            lastLocation = location;
            location = newLocation;

            if (lastLocation != location)
            {
                global::GameLogic.ResolveTriggeredCards(this, null, TriggerType.CARD_CHANGED_LOCATIONS);
            }

            if (sendEvent && Server.IsOnMainThread())
            {
                Events.Publish(this, EventType.OnCardChangedLocation, cardChangeEventArgs);
            }
        }

        public void ClearHistory()
        {
            history = new List<CardHistory>();
            pendingHistory = new List<CardHistory>();
        }

        public void OverrideStats(int cost, int power, int health)
        {
            ClearHistory();
            overrideCost = cost;
            overridePower = power;
            overrideMaximumHealth = health;
        }

        public void Destroy()
        {
            UnbindTile(CardLocation.Graveyard);
        }

        public bool AbleToAttack(int localRow)
        {
            if (IsHero) return false;
            if (location != CardLocation.Board) return false;
            if (data.attackType == AttackType.Immobile) return false;
            if (HasAbility(AbilityKeyword.CANT_ATTACK)) return false;
            if (IsSlow()) return false;
            if (GetPower() <= 0) return false;
            if (HasStatusEffect(StatusEffect.Frozen)) return false;
            if (data.attackType == AttackType.Arcane) return true;
            if (data.attackType == AttackType.Melee && localRow == 0) return true;
            if (data.attackType == AttackType.Ranged && localRow == 1) return true;

            return false;
        }

        public bool silenced;
        public void Silence()
        {
            silenced = true;
            gameAbilities = new List<Ability>();
            abilityStates = new List<CardAbilityState>();

            RemoveAllStatusEffects();
            GameServer.State.AuraUpdater.RemoveAurasCreatedByCard(uid);

            for (var i = 0; i < history.Count; i++)
            {
                history[i].HealthModifier = 0;
                history[i].PowerModifier = 0;
            }

            m_health = ForceRefreshStat;
            m_power = ForceRefreshStat;
            m_pendingHealth = ForceRefreshStat;
            m_pendingPower = ForceRefreshStat;
        }

        /// <summary>
        /// Set the hand index of the card, this should be used instead of just doing handIndex = index so it's easier to debug
        /// </summary>
        /// <param name="index"></param>
        // public void SetHandIndex(int index)
        // {
        //     handIndex = index;
        // }

        public List<Ability> GetAbilities()
        {
            if (gameAbilities == null)
            {
                gameAbilities = new List<Ability>();
                abilityStates = new List<CardAbilityState>();
            }

            if (IsHero)
            {
                return gameAbilities;
            }

            if (data == null)
            {
                return gameAbilities;
            }

            var abilities = data.GetAbilities();

            if (!silenced && gameAbilities.Count < abilities.Count)
            {
                try
                {
                    // lets add our abilities to the game abilities
                    gameAbilities = new List<Ability>(abilities);
                    abilityStates.Clear();

                    for (var i = 0; i < gameAbilities.Count; i++)
                    {
                        abilityStates.Add( new CardAbilityState() );
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError($"Could not fill the list with abilities {characterData.name}. ({e})");
                }
            }

            return gameAbilities;
        }

        public List<Ability> GetAbilities(AbilityKeyword keyword)
        {
            return  GetAbilities().FindAll( ability => ability.keyword == keyword);
        }

        public List<Ability> GetAbilities(List<AbilityKeyword> keywords)
        {
            return GetAbilities().FindAll( ability => keywords.Contains(ability.keyword));
        }

        public List<Ability> GetAbilities(TriggerType trigger)
        {
            return GetAbilities().FindAll(ability => ability.triggerType == trigger);
        }

        public Ability GetAbility(AbilityKeyword keyword)
        {
            return GetAbilities().Find(ability => ability.keyword == keyword);
        }

        public Ability GetAbility(TriggerType trigger)
        {
            return GetAbilities().Find(ability => ability.triggerType == trigger);
        }

        public bool HasAbility(AbilityKeyword keyword)
        {
            return GetAbilities().Exists(ability => ability.keyword == keyword);
        }

        public bool HasTrigger(TriggerType trigger)
        {
            return GetAbilities().Exists(ability => ability.triggerType == trigger);
        }

        public bool HasTargetCondition(FilterCondition filterCondition)
        {
            return GetAbilities().Exists(ability => ability.targetConditions.HasFlag(filterCondition));
        }

        public CardAbilityState GetAbilityState(Ability ability)
        {
            return abilityStates[gameAbilities.IndexOf(ability)];
        }

        public int GetIncreasedSpellDamageForAbility(Ability ability)
        {
            var increasedStats = 0;
            var abilityState = GetAbilityState(ability);

            if (abilityState != null)
            {
                increasedStats = abilityState.spellDamageIncrease;
            }

            switch (ability.abilityCondition.condition)
            {
                case Condition.SPECIFIC_MINION_DIED_THIS_GAME:
                {
                    var count = 0;
                    var cardId = ability.abilityCondition.cardId;

                    for (var i = 0; i < GameServer.State.Cards.Count; i++)
                    {
                        var card = GameServer.State.Cards[i];

                        if (card == null || card.characterData == null) continue;

                        var deadCard = card.characterData.id;

                        if (deadCard == "DevilcoreGroupie" && cardId == "BandWorshiper")
                        {
                            deadCard = cardId;
                        }

                        if (card.IsDead() && deadCard == cardId)
                        {
                            count++;
                        }
                    }

                    increasedStats += count;
                    break;
                }
            }

            return Mathf.Clamp(increasedStats, 0, 999);
        }

        public List<Ability> SwapToConditionalAbilities()
        {
            if (data == null) return new List<Ability>();

            gameAbilities = new List<Ability>(data.GetConditionalAbilities());

            return gameAbilities;
        }

        public void ReplaceAbility(int index, Ability ability)
        {
            if (gameAbilities.Count <= index) return;

            // I need to initialize the abilities again just in case.
            GetAbilities();

            gameAbilities[index] = ability;
            abilityStates[index] = new CardAbilityState();
        }

        public void AddAbilities(List<Ability> abilitiesToAdd, int givenByHistoryId)
        {
            // I need to initialize the abilities again just in case.
            GetAbilities();

            for (var i = 0; i < abilitiesToAdd.Count; i++)
            {
                gameAbilities.Add(abilitiesToAdd[i]);
                abilityStates.Add( new CardAbilityState { giveFromHistoryId = givenByHistoryId });

                switch (abilitiesToAdd[i].keyword)
                {
                    case AbilityKeyword.TOUGH:
                        AddStatusEffect(StatusEffect.Tough);
                        break;
                    case AbilityKeyword.BARRIER:
                        AddStatusEffect(StatusEffect.Barrier);
                        break;
                }
            }
        }

        public void UseAbility(Ability ability)
        {
            // I need to initialize the abilities again just in case.
            GetAbilities();

            if (ability.oncePerTurn)
            {
                abilityStates[gameAbilities.IndexOf(ability)].usedThisTurn = true;
            }
        }

        public void ClearUsedAbilities()
        {
            // I need to initialize the abilities again just in case.
            GetAbilities();

            for (var i = 0; i < abilityStates.Count; i++)
            {
                abilityStates[i].usedThisTurn = false;
            }
        }

        public void ExhaustAbility(Ability ability)
        {
            // I need to initialize the abilities again just in case.
            GetAbilities();
            abilityStates[gameAbilities.IndexOf(ability)].exhausted = true;
        }

        public bool IsAbilityExhausted(Ability ability)
        {
            // I need to initialize the abilities again just in case.
            GetAbilities();

            var index = GetAbilityIndex(ability);
            if (index >= abilityStates.Count || index < 0) return false;
            var state = abilityStates[gameAbilities.IndexOf(ability)];
            return state.exhausted || state.usedThisTurn;
        }

        public int GetAbilityIndex(Ability ability)
        {
            return gameAbilities.IndexOf(ability);
        }

        /// <summary>
        /// Is this card usable with the current state of the game?
        /// </summary>
        /// <returns>If the card is usable</returns>
        public bool IsCardUsableFromHand()
        {
            if (location != CardLocation.Hand)
            {
                return false;
            }

            if (GameState.CanPayCostOfCard(this) && GameServer.State.IsClientsTurn() && GameServer.AbleToPlayCards())
            {
                if (data.type == CardType.Minion || data.type == CardType.Trick)
                {
                    return true;
                }

                if (data.type == CardType.Spell)
                {
                    if (GetAbilities().Count <= 0)
                    {
                        return false;
                    }

                    if (GetAbilities()[0].targetConditions == FilterCondition.NONE)
                    {
                        return true;
                    }

                    if (global::GameLogic.GetTargets(this, GetAbilities()[0],null, null, false).Count > 0)
                    {
                        return true;
                    }

                    // don't feel like testing this yet, so I'll just leave it here for later!
                    // if (HasTargetCondition(FilterCondition.ALL_BUT_ONE))
                    // {
                    //     return global::GameLogic.GetTargets(this, GetAbilities()[0], null, false).Count > 2;
                    // }

                    return false;
                }

                Debug.LogError($"Unknown card type {data.type}");
                return false;
            }

            return false;
        }

        public bool ConditionalAbilitiesHaveBeenMet(string targetUid)
        {
            var abilities = GetAbilities();

            if (abilities.Count <= 0 || !abilities[0].hasConditionalAbility) return false;

            var passesConditions = global::GameLogic.PassesAllConditions(owner, abilities[0], GameServer.State.GetCard(targetUid));

            return passesConditions;
        }

        /// <summary>
        /// Checks to see if this minion is a battlecry minion
        /// </summary>
        /// <returns></returns>
        public bool IsBattlecry()
        {
            return GetAbilities().Exists(ability => ability.triggerType == TriggerType.BATTLECRY);
        }

        /// <summary>
        /// Checks to see how many "attacks" this minion has. Usually 1 but some abilities allow for multiple.
        /// </summary>
        /// <returns></returns>
        public int GetAttackCount()
        {
            var multi = GetAbilities().Find(ability => ability.keyword == AbilityKeyword.DUAL_STRIKE);
            return multi == null ? 1 : 2;
        }

        /// <summary>
        /// Figure out if the card should select a target when being played.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSelectTarget(TileState summoningOn)
        {
            return GetSelectTargets(summoningOn).Count > 0;
        }

        /// <summary>
        /// Figure out if the card should select a target when being played.
        /// </summary>
        /// <returns></returns>
        public List<string> GetSelectTargets()
        {
            var selectAbility = GetAbilities().Find(ability => ability.keyword.HasFlag(AbilityKeyword.TAUNT) || ability.targetConditions.HasFlag(FilterCondition.PLAYER_SELECTED));

            if (selectAbility == null) return new List<string>();

            var targets = global::GameLogic.GetTargets(this, selectAbility, null, null,false);
            return targets;
        }

        /// <summary>
        /// Figure out if the card should select a target when being played.
        /// </summary>
        /// <returns></returns>
        public List<string> GetSelectTargets(TileState summoningOn)
        {
            var selectAbility = GetAbilities().Find(ability => ability.keyword.HasFlag(AbilityKeyword.TAUNT) || ability.targetConditions.HasFlag(FilterCondition.PLAYER_SELECTED));

            if (selectAbility == null) return new List<string>();

            var targets = global::GameLogic.GetTargets(this, selectAbility, null,null, false, summoningOn);
            return targets;
        }

        /// <summary>
        /// Figure out if the card should select a target when being played.
        /// </summary>
        /// <returns></returns>
        public List<string> GetSelectTargets(TileState summoningOn, TargetConditions overrideTargetConditions)
        {
            var selectAbility = GetAbilities().Find(ability => ability.keyword.HasFlag(AbilityKeyword.TAUNT) || ability.targetConditions.HasFlag(FilterCondition.PLAYER_SELECTED));

            if (selectAbility == null)
            {
                return new List<string>();
            }

            var targets = global::GameLogic.GetTargets(this, selectAbility, null,null, false, summoningOn, overrideTargetConditions);
            return targets;
        }

        public bool IsDamaged()
        {
            return GetHealth() < GetDatabaseMaxHealth();
        }

        /// <summary>
        /// Evaluates a card and determines if its dead.
        /// The first condition (location == CardLocation.Graveyard && GetPendingHealth() > 0) is to check if the minion is dead but still on the board (probably due to a destroy effect)
        /// The second checks if the minion was just destroyed through regular means.
        /// </summary>
        /// <returns>If the minion is considered DEAD</returns>
        public bool IsDead()
        {
            return location == CardLocation.Graveyard && GetPendingHealth() > 0 || GetHealth() <= 0 && GetPendingHealth() <= 0;
        }

        /// <summary>
        /// Use this method to check if there's a valid minion in this state or if it's dead (health less than 1)
        /// </summary>
        /// <param name="card">The card to be checked</param>
        /// <returns>true if null or dead, otherwise false</returns>
        public static bool IsNullOrDead(GameCardState card)
        {
            if (card == null)
            {
                return true;
            }

            if (card.characterData == null)
            {
                return true;
            }

            if (card.characterData.type == CardType.Elder_Relic || card.characterData.type == CardType.Relic)
            {
                return card.location != CardLocation.Board;
            }

            if (card.characterData.type == CardType.Spell)
            {
                return card.location == CardLocation.Graveyard || card.data == null;
            }

            return card.location == CardLocation.Graveyard || card.data == null || card.GetHealth() <= 0;
        }

        /// <summary>
        /// Use this method to check if there's a valid minion in this state or if it's dead (health less than 1)
        /// </summary>
        /// <param name="card">The card to be checked</param>
        /// <returns>true if null or dead, otherwise false</returns>
        public static bool IsNullOrDeadCanBeInGraveyard(GameCardState card)
        {
            if (card == null)
            {
                return true;
            }

            if (card.characterData == null)
            {
                return true;
            }

            if (card.characterData.type == CardType.Elder_Relic || card.characterData.type == CardType.Relic)
            {
                return card.location != CardLocation.Board;
            }

            if (card.GetHealth() <= 0)
            {
                Debug.LogError($"Card Health is 0!! {card.GetDatabaseMaxHealth()}");
            }

            return card.data == null || card.GetHealth() <= 0;
        }

        public bool isPendingDeath;
        public void SetAsPendingDead(bool value, string destroyedBy = default)
        {
            isPendingDeath = value;

            if (isPendingDeath)
            {
                destroyedByCardUid = destroyedBy;
            }
        }

        /// <summary>
        /// Use this method to check if there's a valid minion in this state or if it's dead (health less than 1)
        /// </summary>
        /// <param name="card">The card to be checked</param>
        /// <returns>true if null or dead, otherwise false</returns>
        public static bool IsPendingNullOrDead(GameCardState card)
        {
            if (card == null)
            {
                return true;
            }

            if (card.characterData.type == CardType.Elder_Relic || card.characterData.type == CardType.Relic)
            {
                return card.location != CardLocation.Board;
            }

            return card.location == CardLocation.Graveyard || card.isPendingDeath || card.data == null || card.GetPendingHealth() <= 0;
        }

        // <summary>
        /// Use this method to check if there's a valid minion in this state or if it's dead (health less than 1)
        /// </summary>
        /// <param name="card">The card to be checked</param>
        /// <returns>true if null or dead, otherwise false</returns>
        public static bool IsNullSpell(GameCardState card)
        {
            return card == null || card.location == CardLocation.Graveyard || card.data == null;
        }

        public static GameCardState AsCopy(GameCardState card, bool setToFullHealth = false)
        {
            var copy = JsonConvert.DeserializeObject<GameCardState>(JsonConvert.SerializeObject(card));
            copy.Init(false);
            copy.ApplyAllPendingHistories();

            copy.m_power = ForceRefreshStat;
            copy.m_health = ForceRefreshStat;
            copy.m_pendingPower = ForceRefreshStat;
            copy.m_pendingHealth = ForceRefreshStat;

            if (setToFullHealth)
            {
                for (var i = 0; i < copy.history.Count; i++)
                {
                    copy.history[i].DamageAmount = 0;
                }

                for (var i = 0; i < copy.pendingHistory.Count; i++)
                {
                    copy.pendingHistory[i].DamageAmount = 0;
                }

                copy.m_power = ForceRefreshStat;
                copy.m_health = ForceRefreshStat;
                copy.m_pendingPower = ForceRefreshStat;
                copy.m_pendingHealth = ForceRefreshStat;
            }

            return copy;
        }

        public string GetDebugName()
        {
            return $"{characterData?.name} ({uid})";
        }
    }
}