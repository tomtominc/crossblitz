namespace CrossBlitz.ServerAPI.GameLogic.Data
{
    [System.Serializable]
    public class CardAbilityState
    {
        public bool usedThisTurn;
        public bool exhausted;
        public int focusValue;
        public int spellDamageIncrease;
        public int giveFromHistoryId;
    }
}