using System.Collections.Generic;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Visuals;
using Newtonsoft.Json;
using UnityEngine;

namespace CrossBlitz.ServerAPI.GameLogic.Data
{
    [System.Serializable]
    public class TileState
    {
        public int Index;
        public string CardId;
        public List<string> CardHistory;
        public Vector2Int Position;
        public Vector2Int LocalPosition;

        [JsonIgnore]
        public GameCardState Card
        {
            get
            {
                if (Occupied)
                {
                    return GameServer.State.GetCard(CardId);
                }

                return null;
            }
        }

        [JsonIgnore] public bool Occupied => !string.IsNullOrEmpty(CardId);

        public GameTile GetView() => GameplayScreen.Instance.GetBoardTile(Position);
    }
}
