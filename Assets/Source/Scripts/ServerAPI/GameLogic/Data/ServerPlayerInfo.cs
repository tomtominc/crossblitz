using CrossBlitz.PlayFab.Authentication;

namespace Source.Scripts.Server.GameLogic.Data
{
    /// <summary>
    /// The Game Player Info is used to keep track of various infos about the player.
    /// Only certain information can be placed in here, we need to keep this safe because both players
    /// will have this on their phone.
    /// </summary>
    // [System.Serializable]
    // public class ServerPlayerInfo
    // {
    //     public string id;
    //     public string username;
    //     public DeckData deck;
    //     public int currentMana;
    //     public int totalMana;
    //     public int totalHandCards;
    //     public bool finishedDrawingInitialCards;
    //     public bool finishedMulligan;
    //
    //     public bool IsClient => id == App.AccountInfo.PlayerGuid;
    // }
}
