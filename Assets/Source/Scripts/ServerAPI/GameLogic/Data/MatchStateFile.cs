using CrossBlitz.ServerAPI.GameLogic.GameModes;

namespace CrossBlitz.ServerAPI.GameLogic.Data
{
    public class MatchStateFile
    {
        public const string Extension = ".cbgs";

        public GameMode GameMode;
        public Rules Rules;
        public GameState GameState;

        public void SaveToDisk()
        {
            // todo: actually save to json format.
            // todo: create an editor (in game) that allows you to load and edit the file.
        }
    }
}