using System.Collections.Generic;
using CrossBlitz.Utils;
using Newtonsoft.Json;

namespace CrossBlitz.ServerAPI.GameLogic.Data
{
    [System.Serializable]
    public class CardHistory : UniqueItem
    {
        public bool Applied;
        public string AppliedBy;
        public string CardUid;
        public int PowerModifier;
        public int HealthModifier;
        public int CostModifier;
        public int DamageAmount;
        public int HealAmount;
        public int ArmorModifier;
        public int ArmorRemoval;
        public bool ArmorBreak;
        public bool RemoveBarrier;
        public bool Guarded;
        public string TransformedInto;
        public string AppliedStackTrace;

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
