using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ServerAPI.GameLogic.Data;
using UnityEngine;
using UnityEngine.UIElements;

namespace Source.Scripts.Server.GameLogic.Data
{
    [System.Serializable]
    public class BoardState
    {
        public const int COLUMN_COUNT = 4;
        public const int ROW_COUNT = 4;

        /// <summary>
        /// Input a tile position (TileState.Position) and it outputs the location on a grid
        /// that goes from top left to bottom left
        /// 0,0 | 1,0 | 2,0 | 3,0
        /// 0,1 | 1,1 | 2,1 | 3,1
        /// 0,2 | 1,2 | 2,2 | 3,2
        /// 0,3 | 1,3 | 2,3 | 3,3
        /// You can do calculations on this grid and then convert it back so its easy to do some maths.
        /// </summary>
        ///

        public static readonly Dictionary<Vector2Int, Vector2Int> BoardPositionToSimpleMap = new Dictionary<Vector2Int, Vector2Int>
        {
            {new Vector2Int(3,3), new Vector2Int(0,0)},
            {new Vector2Int(2,3), new Vector2Int(1,0)},
            {new Vector2Int(1,3), new Vector2Int(2,0)},
            {new Vector2Int(0,3), new Vector2Int(3,0)},

            {new Vector2Int(3,2), new Vector2Int(0,1)},
            {new Vector2Int(2,2), new Vector2Int(1,1)},
            {new Vector2Int(1,2), new Vector2Int(2,1)},
            {new Vector2Int(0,2), new Vector2Int(3,1)},

            {new Vector2Int(0,0), new Vector2Int(0,2)},
            {new Vector2Int(1,0), new Vector2Int(1,2)},
            {new Vector2Int(2,0), new Vector2Int(2,2)},
            {new Vector2Int(3,0), new Vector2Int(3,2)},

            {new Vector2Int(0,1), new Vector2Int(0,3)},
            {new Vector2Int(1,1), new Vector2Int(1,3)},
            {new Vector2Int(2,1), new Vector2Int(2,3)},
            {new Vector2Int(3,1), new Vector2Int(3,3)},

        };

        /// <summary>
        /// Input a simple map position and it outputs a location on the board (TileState.Position)
        /// 3,3 | 2,3 | 1,3 | 0,3
        /// 3,2 | 2,2 | 1,2 | 0,2
        /// 0,0 | 1,0 | 2,0 | 3,0
        /// 0,1 | 1,1 | 2,1 | 3,1
        /// </summary>
        public static readonly Dictionary<Vector2Int, Vector2Int> SimpleMapToBoardPosition = new Dictionary<Vector2Int, Vector2Int>
        {
            {new Vector2Int(0,0), new Vector2Int(3,3)},
            {new Vector2Int(1,0), new Vector2Int(2,3)},
            {new Vector2Int(2,0), new Vector2Int(1,3)},
            {new Vector2Int(3,0), new Vector2Int(0,3)},

            {new Vector2Int(0,1), new Vector2Int(3,2)},
            {new Vector2Int(1,1), new Vector2Int(2,2)},
            {new Vector2Int(2,1), new Vector2Int(1,2)},
            {new Vector2Int(3,1), new Vector2Int(0,2)},

            {new Vector2Int(0,2), new Vector2Int(0,0)},
            {new Vector2Int(1,2), new Vector2Int(1,0)},
            {new Vector2Int(2,2), new Vector2Int(2,0)},
            {new Vector2Int(3,2), new Vector2Int(3,0)},

            {new Vector2Int(0,3), new Vector2Int(0,1)},
            {new Vector2Int(1,3), new Vector2Int(1,1)},
            {new Vector2Int(2,3), new Vector2Int(2,1)},
            {new Vector2Int(3,3), new Vector2Int(3,1)},
        };

        /// <summary>
        /// All tiles on the board
        /// </summary>
        public List<TileState> tiles;
        //public Dictionary<Vector2Int, int> tilePositionMap;

        public TileState GetTile(Vector2Int position)
        {
            // if (!tilePositionMap.ContainsKey(position)) return null;
            // return tiles[tilePositionMap[position]];

            return tiles.Find(t => t.Position == position);
        }

        public TileState GetTile(string cardUid)
        {
            return tiles.Find(t => t.CardId == cardUid);
        }

        public TileState GetTileWithCardOrHistoryOfCard(string cardUid)
        {
            return tiles.Find(t => t.CardId == cardUid || t.CardHistory.Contains(cardUid));
        }

        public List<TileState> GetOccupiedTiles()
        {
            return tiles.FindAll(x => x.Occupied);
        }

        public List<TileState> GetUnOccupiedTiles()
        {
            return tiles.FindAll(x => !x.Occupied);
        }

        public List<TileState> GetUnOccupiedTiles(Team team)
        {
            return tiles.FindAll(x => !x.Occupied && (team == Team.Client ? x.Position.y < 2 : x.Position.y >= 2));
        }

        public List<TileState> GetUnOccupiedTilesInRow(int row)
        {
            return GetUnOccupiedTiles().FindAll(t => !t.Occupied && t.Position.y == row);
        }

        public List<TileState> GetOccupiedTiles(Team team)
        {
            return tiles.FindAll(x => x.Occupied && (team == Team.Client ? x.Position.y < 2 : x.Position.y >= 2));
        }

        public List<TileState> GetOccupiedTilesInSameRow(int row)
        {
            return tiles.FindAll(x => x.Occupied && x.Position.y == row);
        }

        public List<TileState> GetOccupiedTilesInColumn(int column)
        {
            var occupiedTiles = new List<TileState>();
            occupiedTiles.AddRange(GetOccupiedTilesInColumn(Team.Client, column));
            occupiedTiles.AddRange(GetOccupiedTilesInColumn(Team.Opponent, column));
            return occupiedTiles;
        }

        public List<TileState> GetUnOccupiedTilesInColumn(int column)
        {
            return GetUnOccupiedTiles().FindAll(t => !t.Occupied && BoardPositionToSimpleMap[t.Position ].x == column);
        }

        public List<TileState> GetTilesInRow(int row)
        {
            return tiles.FindAll(x => x.Position.y == row);
        }

        public List<TileState> GetOccupiedTilesInColumn(Team team, int column)
        {
            return tiles.FindAll(x => x.Occupied && x.Position.x == column && (team == Team.Client ? x.Position.y < 2 : x.Position.y >= 2));
        }

        /// <summary>
        /// TopRight = X = +1 Y = -1
        /// TopLeft = X = -1 Y = -1
        /// BottomRight = X = +1 Y = +1
        /// BottomLeft  = X = -1 Y = +1
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public List<TileState> GetOccupiedTilesInDiagonalSpaces(Vector2Int position)
        {
            var diagonalTiles = new List<TileState>();

            if (!SimpleMapToBoardPosition.ContainsKey(position))
            {
                return diagonalTiles;
            }

            var simplePosition = BoardPositionToSimpleMap[position];
            var topRightPosition = new Vector2Int(simplePosition.x + 1, simplePosition.y - 1);
            var topLeftPosition = new Vector2Int(simplePosition.x - 1, simplePosition.y - 1);
            var bottomRightPosition = new Vector2Int(simplePosition.x + 1, simplePosition.y + 1);
            var bottomLeftPosition = new Vector2Int(simplePosition.x - 1, simplePosition.y + 1);
            //
            // Debug.Log(position);
            // Debug.Log(simplePosition);
            // Debug.Log(topRightPosition);
            // Debug.Log(topLeftPosition);
            // Debug.Log(bottomRightPosition);
            // Debug.Log(bottomLeftPosition);

            if (SimpleMapToBoardPosition.ContainsKey(topRightPosition))
            {
                var tileTR = GetTile(SimpleMapToBoardPosition[topRightPosition]);
                if (tileTR != null && tileTR.Occupied) diagonalTiles.Add(tileTR);
            }

            if (SimpleMapToBoardPosition.ContainsKey(topLeftPosition))
            {
                var tileTL = GetTile(SimpleMapToBoardPosition[topLeftPosition]);
                if (tileTL != null && tileTL.Occupied) diagonalTiles.Add(tileTL);
            }

            if (SimpleMapToBoardPosition.ContainsKey(bottomRightPosition))
            {
                var tileBR = GetTile(SimpleMapToBoardPosition[bottomRightPosition]);
                if (tileBR != null && tileBR.Occupied) diagonalTiles.Add(tileBR);
            }

            if (SimpleMapToBoardPosition.ContainsKey(bottomLeftPosition))
            {
                var tileBL = GetTile(SimpleMapToBoardPosition[bottomLeftPosition]);
                if (tileBL != null && tileBL.Occupied) diagonalTiles.Add(tileBL);
            }

            return diagonalTiles;
        }


        /// <summary>
        /// get a tile based on team
        /// </summary>
        /// <param name="position"></param>
        /// <param name="team"></param>
        /// <returns></returns>
        public TileState GetTile(Vector2Int position, Team team)
        {
            if (team == Team.Opponent)
            {
                if (position.y < 2) position.y += 2;
            }

            return GetTile(position);
        }

        public Vector2Int InvertPosition(Vector2Int position)
        {
            return new Vector2Int(COLUMN_COUNT - 1 - position.x, position.y);
        }

        /// <summary>
        /// Converts the position to another position directly opposite in the same column.
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public Vector2Int OppositeSide(Vector2Int position)
        {
            var mirrored = MirroredPosition(position);
            return new Vector2Int(COLUMN_COUNT - 1 - position.x, mirrored.y);
        }

        /// <summary>
        /// Converts the position to the mirrored position on the other side.
        /// </summary>
        /// <param name="position">The position to invert</param>
        /// <returns></returns>
        public Vector2Int MirroredPosition(Vector2Int position)
        {
            if (position.y >= 2)
            {
                return new Vector2Int(position.x, position.y - 2);
            }

            return new Vector2Int(position.x, position.y + 2);
        }

        public bool IsOpenColumn(int col)
        {
            for (var row = 0; row < ROW_COUNT; row++)
            {
                var position = new Vector2Int(col, row);
                var tile = GetTile(position);

                if (tile.Occupied)
                    return false;
            }

            return true;
        }

        public bool IsOpenColumn(int col, Team team)
        {
            var startingRow = team == Team.Opponent ? 2 : 0;

            for (var row = startingRow; row < startingRow + 2; row++)
            {
                var position = new Vector2Int(col, row);
                var tile = GetTile(position);

                if (tile.Occupied)
                    return false;
            }

            return true;
        }

        public bool IsFullColumn(int col)
        {
            for (var row = 0; row < ROW_COUNT; row++)
            {
                var position = new Vector2Int(col, row);
                var tile = GetTile(position);

                if (!tile.Occupied)
                    return false;
            }

            return true;
        }

        public bool IsFullColumn(int col, Team team)
        {
            var startingRow = team == Team.Opponent ? 2 : 0;

            for (var row = startingRow; row < startingRow + 2; row++)
            {
                var position = new Vector2Int(col, row);
                var tile = GetTile(position);

                if (!tile.Occupied)
                    return false;
            }

            return true;
        }

        public void GenerateBoard()
        {
            tiles = new List<TileState>();
            //tilePositionMap = new Dictionary<Vector2Int, int>();

            var index = 0;

            for (var col = 0; col < COLUMN_COUNT; col++)
            {
                for (var row = 0; row < ROW_COUNT; row++)
                {
                    var tile = new TileState
                    {
                        Index = index,
                        Position = new Vector2Int(col, row),
                        LocalPosition = new Vector2Int(col, row > 1 ? row - 2 : row),
                        CardHistory = new List<string>()
                    };

                    tiles.Add(tile);
                    //tilePositionMap.Add(tile.Position, tile.Index);
                    index++;
                }
            }
        }

        // public void SetTile(Team team, Vector2Int position, string uid)
        // {
        //     var tile = GetTile(position, team);
        //
        //     if (tile.Occupied)
        //     {
        //         DestroyCardFromTile(tile.Card.uid);
        //     }
        //
        //     tile.CardId = uid;
        //     tile.Card?.BindTile(tile.Position);
        // }

        public void SetTile(Vector2Int position, string cardId)
        {
            var tile = GetTile(position);

            if (tile.Occupied)
            {
                DestroyCardFromTile(tile.Card.uid);
            }

            tile.CardId = cardId;
            tile.Card?.BindTile(tile.Position);
        }

        public void RemoveCardFromTile(string cardId, CardLocation location)
        {
            var tile = GetTile(cardId);

            if (tile != null && tile.Occupied)
            {
                tile.Card.UnbindTile(location);
                tile.CardHistory.Add( tile.CardId );
                tile.CardId = string.Empty;
            }
        }

        public void DestroyCardFromTile(string cardId)
        {
            var tile = GetTile(cardId);

            if (tile != null && tile.Occupied)
            {
                tile.Card.Destroy();
                tile.CardHistory.Add( tile.CardId );
                tile.CardId = string.Empty;
            }
        }

        public bool IsOccupied(Vector2Int position, Team team)
        {
            var tile = GetTile(position, team);

            if (tile == null)
            {
                return true;
            }

            return !string.IsNullOrEmpty(tile.CardId);
        }

        public bool IsPlayerField(Vector2Int position)
        {
            return position.y > 2;
        }

        public bool IsOpponentField(Vector2Int position)
        {
            return position.y <= 2;
        }
    }
}
