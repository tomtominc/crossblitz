using System;

namespace CrossBlitz.ServerAPI.GameLogic.GameModes
{
    [System.Serializable]
    public class GameMode : IDisposable
    {
        public GameType GameType;
        public ConnectionMode ConnectionMode;
        public SceneEnvironment SceneEnvironment;
        public string SceneToReturnTo;
        public bool ShowResultsAfterMatch;
        public string BattleUid;
        public string DeckUid;

        public void Dispose()
        {

        }
    }


    public enum ConnectionMode
    {
        NULL = -1,
        ONLINE,
        LOCAL
    }

    public enum GameType
    {
        NULL  = -1,
        COLISEUM,
        SKIRMISH,
        HOLLOW_HUNT,
        STORY

    }

    public enum SceneEnvironment
    {
        PurpleWood,
        BrownWood,
        CaveWood,
        Grass,
        StarscapeGrass,
        Sand,
        Stone,
        TownStone,
        MineStone,
        RedCarpet,
        PurpleCarpet,
        Ocean,
        PrisonStone,
        PierStone,
        CasinoFloor,
        ManorFloor

    }
}
