using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.ServerAPI.GameLogic.Intelligence;
using TakoBoyStudios.Events;
using UnityEngine;

namespace CrossBlitz.ServerAPI.GameLogic
{
    [System.Serializable]
    public class AuraUpdaterData
    {
        public string SourceUid;
        public Ability auraAbility;
        public List<Ability> givenAbilities;
    }

    [System.Serializable]
    public class AuraUpdater : GameEventUpdater
    {
        public int m_instanceId;
        public List<AuraUpdaterData> m_auraData;

        public override void Initialize()
        {
            base.Initialize();

            m_instanceId = GUID.CreateUniqueInt();
            m_auraData = new List<AuraUpdaterData>();
        }

        public bool AuraExists(AuraData.AuraEffect aura)
        {
            return m_auraData.Exists(a => a.auraAbility.auraData.auraEffect == aura);
        }

        public void AddAura(GameCardState card, List<Ability> auraAbilities, List<Ability> givenAbilities)
        {
            for (var i = 0; i < auraAbilities.Count; i++)
            {
                var aura = auraAbilities[i];

                if ( aura.keyword == AbilityKeyword.AURA )
                {
                    m_auraData.Add(new AuraUpdaterData
                    {
                        SourceUid = card.uid,
                        auraAbility = aura,
                        givenAbilities = givenAbilities
                    });

                    //Debug.Log($"Added new Aura ({card.characterData.name}): {aura.auraData.auraEffect} Aura Count Now = {m_auraData.Count}");
                }
            }
        }

        public List<AuraUpdaterData> GetAurasCreatedByCard(string cardId)
        {
            return m_auraData.FindAll(a => a.SourceUid == cardId);
        }

        public void RemoveAurasCreatedByCard(string cardId)
        {
            m_auraData.RemoveAll(a => a.SourceUid == cardId && (a.auraAbility.auraData.removeWhenMinionDies ||
                                                                a.auraAbility.auraData.removeWhenTurnEnds));
        }

        public override void OnMinionSummoned(IMessage message)
        {
            if (message.Data is OnMinionSummonedEventArgs args)
            {
                //==================================================
                // GIVE ABILITY
                //==================================================
                {
                    var giveAbilityAuras = m_auraData.FindAll(a => a.auraAbility.auraData.auraEffect == AuraData.AuraEffect.Give_Ability);

                    if (giveAbilityAuras.Count > 0)
                    {
                        var card = GameServer.State.GetCard(args.cardUid);

                        if (card?.data == null) return;

                        for (var i = 0; i < giveAbilityAuras.Count; i++)
                        {
                            var sourceCard = GameServer.State.GetCard(giveAbilityAuras[i].SourceUid);
                            var giveAbilityCommand = new GiveAbilityCommand();
                            giveAbilityCommand.PlayerUid = sourceCard.owner;
                            giveAbilityCommand.SourceUid = sourceCard.uid;
                            giveAbilityCommand.AbilityIndex = giveAbilityAuras[i].auraAbility.usesCustomEffect ? 0 : -1;
                            giveAbilityCommand.TargetUids = new List<string> { card.uid };
                            giveAbilityCommand.AbilitiesToGive = new List<Ability>( giveAbilityAuras[i].givenAbilities );

                            global::GameLogic.StartCommand(giveAbilityCommand);
                        }
                    }
                }

                //==================================================
                // TRANSFORM AURA
                //==================================================
                {
                    var transformAuras = m_auraData.FindAll(a => a.auraAbility.auraData.auraEffect == AuraData.AuraEffect.Transform_Summoned_Minions);

                    if (transformAuras.Count > 0)
                    {
                        var card = GameServer.State.GetCard(args.cardUid);

                        if (card?.data == null) return;

                        for (var i = 0; i < transformAuras.Count; i++)
                        {
                            var auraUpdater = transformAuras[i];
                            var aura = auraUpdater.auraAbility;
                            var sourceCard = GameServer.State.GetCard(transformAuras[i].SourceUid);

                            if (aura.auraData.minionToTransform != card.data.id)
                            {
                                continue;
                            }

                            var auraSource = GameServer.State.GetCard(auraUpdater.SourceUid);

                            if (auraSource.owner != card.owner &&
                                aura.auraData.targets.HasFlag(FilterCondition.PLAYER) &&
                                !aura.auraData.targets.HasFlag(FilterCondition.OPPONENT))
                            {
                                continue;
                            }

                            if (auraSource.owner == card.owner &&
                                aura.auraData.targets.HasFlag(FilterCondition.OPPONENT) &&
                                !aura.auraData.targets.HasFlag(FilterCondition.PLAYER))
                            {
                                continue;
                            }

                            var transformCardsCommand = new TransformCardsCommand();
                            transformCardsCommand.PlayerUid = sourceCard.owner;
                            transformCardsCommand.SourceUid = sourceCard.uid;
                            transformCardsCommand.AbilityIndex = transformAuras[i].auraAbility.usesCustomEffect ? 0 : -1;
                            transformCardsCommand.TargetUids = new List<string> { card.uid };
                            transformCardsCommand.TransformCardData = new TransformCardData
                                { cardId = aura.auraData.minionToTransformInto };

                            global::GameLogic.StartCommand(transformCardsCommand);
                        }
                    }
                }
            }
        }

        public override void OnMinionDestroyed(IMessage message)
        {
            if (message.Data is OnMinionDestroyedEventArgs args)
            {
                var card = GameServer.State.GetCard(args.cardUid);

                if (card.HasAbility(AbilityKeyword.AURA))
                {
                    var auraRemovals = m_auraData.RemoveAll(a => a.auraAbility.auraData.removeWhenMinionDies && a.SourceUid == args.cardUid);
                }
            }
        }

        public override void OnCardTransformed(GameCardState cardState)
        {
            if (cardState.HasAbility(AbilityKeyword.AURA))
            {
                var auraRemovals = m_auraData.RemoveAll(a => a.auraAbility.auraData.removeWhenMinionDies && a.SourceUid == cardState.uid);
                Debug.Log($"Removed: {auraRemovals} Auras");
            }
        }

        public override void OnTurnEnd(IMessage message)
        {
            if (message.Data is OnTurnEndEventArgs args)
            {
                for (var i = 0; i < GameServer.State.Cards.Count; i++)
                {
                    var card = GameServer.State.Cards[i];

                    if (card.HasAbility(AbilityKeyword.AURA))
                    {
                        var auraRemovals = m_auraData.RemoveAll(a => a.auraAbility.auraData.removeWhenTurnEnds && a.SourceUid == card.uid);
                        //Debug.Log($"Removed: {auraRemovals} Auras");
                    }
                }
            }
        }

        public int GetSpellDamageForSchool(string owner, Class school)
        {
            var spellDamages = m_auraData.FindAll(a => a.auraAbility.auraData.auraEffect == AuraData.AuraEffect.SpellDamage);
            var damageModifier = 0;

            if (spellDamages.Count > 0)
            {
                for (var i = 0; i < spellDamages.Count; i++)
                {
                    var aura = spellDamages[i].auraAbility;

                    if (aura.damageSchool != Class.All && aura.damageSchool != school)
                    {
                        continue;
                    }

                    var sourceCard = GameServer.State.GetCard(spellDamages[i].SourceUid);

                    if (sourceCard.owner!=owner && aura.auraData.targets.HasFlag(FilterCondition.PLAYER) && !aura.auraData.targets.HasFlag(FilterCondition.OPPONENT))
                    {
                        continue;
                    }

                    if (sourceCard.owner==owner && aura.auraData.targets.HasFlag(FilterCondition.OPPONENT) && !aura.auraData.targets.HasFlag(FilterCondition.PLAYER))
                    {
                        continue;
                    }

                    damageModifier += aura.auraData.modifyStatAmount + sourceCard.GetIncreasedSpellDamageForAbility( aura );
                }
            }

            return damageModifier;
        }

        public int GetAdditionalDamage(string cardUid)
        {
            var additionalDamage = 0;

            if (AuraEffectIsActive(AuraData.AuraEffect.StatModifier, out var statAuras))
            {
                var card = GameServer.State.GetCard(cardUid);

                for (var i = 0; i < statAuras.Count; i++)
                {
                    var statAura = statAuras[i];

                    if (statAura.auraAbility.auraData.stat != FilterConditionValues.Stat.Damage)
                    {
                        continue;
                    }

                    var sourceCard = GameServer.State.GetCard(statAura.SourceUid);

                    if (statAura.auraAbility.auraData.turnRestricted)
                    {
                        if (statAura.auraAbility.auraData.turnAllowed == FilterCondition.PLAYER && GameServer.State.GetCurrentPlayer() != sourceCard.owner)
                        {
                            continue;
                        }

                        if (statAura.auraAbility.auraData.turnAllowed == FilterCondition.OPPONENT && GameServer.State.GetCurrentPlayer() == sourceCard.owner)
                        {
                            continue;
                        }
                    }

                    if (!string.IsNullOrEmpty(statAura.auraAbility.auraData.advancedFilters.specificTargetCard))
                    {
                        if (card.characterData.GetIdForSpecificCardAbility() == statAura.auraAbility.auraData.advancedFilters.specificTargetCard)
                        {
                            additionalDamage += statAura.auraAbility.auraData.modifyStatAmount;
                        }
                    }
                    else
                    {
                        var targets = global::GameLogic.GetTargets(sourceCard, statAura.auraAbility, null, null, true,
                            overrideTargetConditions: new TargetConditions
                            {
                                targetCondition = statAura.auraAbility.auraData.targets,
                                targetConditionValues = statAura.auraAbility.auraData.advancedFilters
                            });

                        if (targets.Contains(card.uid))
                        {
                            additionalDamage += statAura.auraAbility.auraData.modifyStatAmount;
                        }
                    }
                }
            }

            return additionalDamage;
        }

        public bool CostsHealthInsteadOfMana (string owner, out string sourceUid, out Ability ability)
        {
            sourceUid = string.Empty;
            ability = null;

            if (m_auraData.Exists(a => a.auraAbility.auraData.auraEffect == AuraData.AuraEffect.Cards_Cost_Health_Instead_Of_Mana))
            {
                var aura = m_auraData.Find(a => a.auraAbility.auraData.auraEffect == AuraData.AuraEffect.Cards_Cost_Health_Instead_Of_Mana);
                var sourceCard = GameServer.State.GetCard(aura.SourceUid);
                sourceUid = sourceCard.uid;
                ability = sourceCard.GetAbility(AbilityKeyword.AURA);

                if (!sourceCard.IsAbilityExhausted( ability ))
                {
                    if (sourceCard.owner == owner && aura.auraAbility.auraData.targets.HasFlag(FilterCondition.PLAYER))
                    {
                        return true;
                    }

                    if (sourceCard.owner != owner &&
                        aura.auraAbility.auraData.targets.HasFlag(FilterCondition.OPPONENT))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public int GetBattlecryCount(string owner)
        {
            if (m_auraData.Exists(a => a.auraAbility.auraData.auraEffect == AuraData.AuraEffect.Double_Battlecry))
            {
                var doubleBc = m_auraData.Find(a => a.auraAbility.auraData.auraEffect == AuraData.AuraEffect.Double_Battlecry);
                var sourceCard = GameServer.State.GetCard(doubleBc.SourceUid);

                if (sourceCard.owner==owner && doubleBc.auraAbility.auraData.targets.HasFlag(FilterCondition.PLAYER))
                {
                    return 2;
                }

                if (sourceCard.owner!=owner && doubleBc.auraAbility.auraData.targets.HasFlag(FilterCondition.OPPONENT))
                {
                    return 2;
                }
            }

            return 1;
        }

        public int GetPlunderCount(string owner)
        {
            if (m_auraData.Exists(a => a.auraAbility.auraData.auraEffect == AuraData.AuraEffect.Double_Plunder))
            {
                var doubleBc = m_auraData.Find(a => a.auraAbility.auraData.auraEffect == AuraData.AuraEffect.Double_Plunder);
                var sourceCard = GameServer.State.GetCard(doubleBc.SourceUid);

                if (sourceCard.owner==owner && doubleBc.auraAbility.auraData.targets.HasFlag(FilterCondition.PLAYER))
                {
                    return 2;
                }

                if (sourceCard.owner!=owner && doubleBc.auraAbility.auraData.targets.HasFlag(FilterCondition.OPPONENT))
                {
                    return 2;
                }
            }

            return 1;
        }

        public int GetDeathrattleCount(string owner)
        {
            if (m_auraData.Exists(a => a.auraAbility.auraData.auraEffect == AuraData.AuraEffect.Double_Deathrattle))
            {
                var doubleDr = m_auraData.Find(a => a.auraAbility.auraData.auraEffect == AuraData.AuraEffect.Double_Deathrattle);
                var sourceCard = GameServer.State.GetCard(doubleDr.SourceUid);

                if (sourceCard.owner==owner && doubleDr.auraAbility.auraData.targets.HasFlag(FilterCondition.PLAYER))
                {
                    return 2;
                }

                if (sourceCard.owner!=owner && doubleDr.auraAbility.auraData.targets.HasFlag(FilterCondition.OPPONENT))
                {
                    return 2;
                }
            }

            return 1;
        }

        public int GetDrawCount(string owner)
        {
            if (AuraEffectIsActive(AuraData.AuraEffect.DrawModifier, out var drawModifiers))
            {
                return 1 + drawModifiers.Sum(mod =>
                {
                    var sourceCard = GameServer.State.GetCard(mod.SourceUid);

                    if (sourceCard.owner == owner && mod.auraAbility.auraData.targets.HasFlag(FilterCondition.PLAYER) ||
                        sourceCard.owner != owner && mod.auraAbility.auraData.targets.HasFlag(FilterCondition.OPPONENT))
                    {
                        return mod.auraAbility.auraData.modifyDrawAmount;
                    }

                    return 0;
                });
            }

            return 1;
        }

        /// <summary>
        /// This feels too expensive so we don't allow stat altering auras
        /// </summary>
        /// <param name="cardUid"></param>
        /// <returns></returns>
        public int GetPowerAura(string cardUid)
        {
            var power = 0;

            if (AuraEffectIsActive(AuraData.AuraEffect.StatModifier, out var statAuras))
            {
                var card = GameServer.State.GetCard(cardUid);

                for (var i = 0; i < statAuras.Count; i++)
                {
                    var statAura = statAuras[i];

                    if (statAura.auraAbility.auraData.stat != FilterConditionValues.Stat.Power) continue;

                    var sourceCard = GameServer.State.GetCard(statAura.SourceUid);

                    if (statAura.auraAbility.auraData.turnRestricted)
                    {
                        if (statAura.auraAbility.auraData.turnAllowed == FilterCondition.PLAYER && GameServer.State.GetCurrentPlayer() != sourceCard.owner)
                        {
                            continue;
                        }

                        if (statAura.auraAbility.auraData.turnAllowed == FilterCondition.OPPONENT && GameServer.State.GetCurrentPlayer() == sourceCard.owner)
                        {
                            continue;
                        }
                    }

                    var multiplier = 1;

                    var targets = global::GameLogic.GetTargets(sourceCard, statAura.auraAbility, null, null, false,
                        overrideTargetConditions: new TargetConditions
                        {
                            targetCondition = statAura.auraAbility.auraData.targets,
                            targetConditionValues = statAura.auraAbility.auraData.advancedFilters
                        });

                    if (statAura.auraAbility.auraData.checkRelativeBoardSpot && card.uid != statAura.SourceUid)
                    {
                        continue;
                    }

                    if (statAura.auraAbility.auraData.checkRelativeBoardSpot && card.uid == statAura.SourceUid)
                    {
                        multiplier = 0;

                        var position = sourceCard.position;

                        if (statAura.auraAbility.auraData.relativeBoardSpot.HasFlag(AuraData.RelativeBoardSpace.Up))
                        {
                            var target = targets.Find(id =>
                                GameServer.State.GetCard(id).position == position + Vector2Int.down );
                            multiplier += string.IsNullOrEmpty(target) ? 0 : 1;
                        }

                        if (statAura.auraAbility.auraData.relativeBoardSpot.HasFlag(AuraData.RelativeBoardSpace.Down))
                        {
                            var target = targets.Find(id =>
                                GameServer.State.GetCard(id).position == position + Vector2Int.up );
                            multiplier += string.IsNullOrEmpty(target) ? 0 : 1;
                        }

                        if (statAura.auraAbility.auraData.relativeBoardSpot.HasFlag(AuraData.RelativeBoardSpace.Left))
                        {
                            var target = targets.Find(id =>
                                GameServer.State.GetCard(id).position == position + Vector2Int.left );
                            multiplier += string.IsNullOrEmpty(target) ? 0 : 1;
                        }

                        if (statAura.auraAbility.auraData.relativeBoardSpot.HasFlag(AuraData.RelativeBoardSpace.Right))
                        {
                            var target = targets.Find(id =>
                                GameServer.State.GetCard(id).position == position + Vector2Int.right );
                            multiplier += string.IsNullOrEmpty(target) ? 0 : 1;
                        }
                    }

                    if (targets.Contains(card.uid))
                    {
                        power += statAura.auraAbility.auraData.modifyStatAmount * multiplier;
                    }
                }

                // Debug.LogError($"power = {power}");
            }

            return power;
        }

        /// <summary>
        /// This feels too expensive so we don't allow stat altering auras
        /// </summary>
        /// <param name="cardUid"></param>
        /// <returns></returns>
        public int GetHealthAura(string cardUid)
        {
            var health = 0;

            if (AuraEffectIsActive(AuraData.AuraEffect.StatModifier, out var statAuras))
            {
                var card = GameServer.State.GetCard(cardUid);

                for (var i = 0; i < statAuras.Count; i++)
                {
                    var statAura = statAuras[i];

                    if (statAura.auraAbility.auraData.stat != FilterConditionValues.Stat.Health) continue;

                    var sourceCard = GameServer.State.GetCard(statAura.SourceUid);

                    if (statAura.auraAbility.auraData.turnRestricted)
                    {
                        if (statAura.auraAbility.auraData.turnAllowed == FilterCondition.PLAYER && GameServer.State.GetCurrentPlayer() != sourceCard.owner)
                        {
                            continue;
                        }

                        if (statAura.auraAbility.auraData.turnAllowed == FilterCondition.OPPONENT && GameServer.State.GetCurrentPlayer() == sourceCard.owner)
                        {
                            continue;
                        }
                    }

                    var targets = global::GameLogic.GetTargets(sourceCard, statAura.auraAbility, null, null, false,
                        overrideTargetConditions: new TargetConditions
                        {
                            targetCondition = statAura.auraAbility.auraData.targets,
                            targetConditionValues = statAura.auraAbility.auraData.advancedFilters
                        });

                    if (targets.Contains(card.uid))
                    {
                        health += statAura.auraAbility.auraData.modifyStatAmount;
                    }
                }
            }

            return health;
        }

        public int GetCostAura(string cardUid)
        {
            var cost = 0;

            if (AuraEffectIsActive(AuraData.AuraEffect.StatModifier, out var statAuras))
            {
                var card = GameServer.State.GetCard(cardUid);

                for (var i = 0; i < statAuras.Count; i++)
                {
                    var statAura = statAuras[i];

                    if (statAura.auraAbility.auraData.stat != FilterConditionValues.Stat.ManaCost) continue;

                    var sourceCard = GameServer.State.GetCard(statAura.SourceUid);

                    if (statAura.auraAbility.auraData.turnRestricted)
                    {
                        if (statAura.auraAbility.auraData.turnAllowed == FilterCondition.PLAYER && GameServer.State.GetCurrentPlayer() != sourceCard.owner)
                        {
                            continue;
                        }

                        if (statAura.auraAbility.auraData.turnAllowed == FilterCondition.OPPONENT && GameServer.State.GetCurrentPlayer() == sourceCard.owner)
                        {
                            continue;
                        }
                    }

                    var targets = global::GameLogic.GetTargets(sourceCard, statAura.auraAbility, null, null, false,
                        overrideTargetConditions: new TargetConditions
                        {
                            targetCondition = statAura.auraAbility.auraData.targets,
                            targetConditionValues = statAura.auraAbility.auraData.advancedFilters
                        });

                    if (targets.Contains(card.uid))
                    {
                        cost += statAura.auraAbility.auraData.modifyStatAmount;
                    }
                }
            }

            return cost;
        }

        public bool AuraEffectIsActive(AuraData.AuraEffect auraEffect, out List<AuraUpdaterData> auras)
        {
            auras = m_auraData.FindAll(a => a.auraAbility.auraData.auraEffect == auraEffect);
            return auras.Count > 0;
        }

        public override void Kill()
        {
            base.Kill();

            m_auraData = null;
            m_instanceId = 0;
        }
    }
}