using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Asyncoroutine;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.ServerAPI.GameLogic.Intelligence;
using FMOD;
using GameDataEditor;
using MEC;
using Source.Scripts.Server.GameLogic.Data;
using UnityEngine;
using Debug = UnityEngine.Debug;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;
using Random = UnityEngine.Random;

namespace CrossBlitz.ServerAPI.GameLogic
{
    public static class AI
    {
        public static bool Thinking;
        public static int FailedAiAttempts;
        public static bool FailedSummonAttempt;
        private static bool Disposed;

        public class GetAiScoreParameters
        {
        }

        public class Decision
        {
            public List<Command> commands;
            public int score;

            public Decision()
            {
                commands = new List<Command>();
            }

            public int GetManaRequiredForThisDecision()
            {
                var manaRequirement = 0;

                for (var i = 0; i < commands.Count; i++)
                {
                    if (commands[i] is SummonCommand summonCommand)
                    {
                        var minion = GameServer.State.GetCard(summonCommand.SourceUid);
                        manaRequirement += minion.GetCost();
                    }
                    else if (commands[i] is PlayCommand playCommand)
                    {
                        var playedCard = GameServer.State.GetCard(playCommand.SourceUid);
                        manaRequirement += playedCard.GetCost();
                    }
                }

                return manaRequirement;
            }
        }

        public static void DrawMulliganCards(string battleUid)
        {
            var draw = new DrawCardCommand
            {
                PlayerUid = GameServer.OpponentPlayerState.Uid,
                CardCountBeforeDraw = 0,
                DrawCount = GameServer.Rules.StartingDrawCount(GameServer.OpponentPlayerIndex),
                Mulligan = true
            };

            var battleData = Db.BattleDatabase.GetBattleData(battleUid);

            if (battleData != null &&
                battleData.CustomBattleOptions.Enabled &&
                battleData.CustomBattleOptions.OpponentAdditionalCards.Enabled)
            {
                for (var i = 0;
                     i < battleData.CustomBattleOptions.OpponentAdditionalCards.OverrideDeck.cards.Count;
                     i++)
                {
                    var card = battleData.CustomBattleOptions.OpponentAdditionalCards.OverrideDeck.cards[i];
                    var indexOfCardInDeck = -1;

                    for (var j = 0; j < GameServer.OpponentPlayerState.deck.Count; j++)
                    {
                        var cardUidInsideDeck = GameServer.OpponentPlayerState.deck[j];
                        var cardStateInsideDeck = GameServer.State.GetCard(cardUidInsideDeck);

                        if (cardStateInsideDeck.characterData.id == card.id)
                        {
                            indexOfCardInDeck = j;
                        }
                    }

                    // this card was already in the position we wanted it.
                    if (indexOfCardInDeck == i)
                    {
                        continue;
                    }

                    // not inside the deck, so we can't swap the card, add this card to the deck
                    if (indexOfCardInDeck < 0)
                    {
                        Debug.LogError(
                            $"No card {card.id} inside deck, if you want to override it, you need to add at least 1 copy to the deck.");
                        continue;
                    }

                    var cardAtSwapIndex = GameServer.OpponentPlayerState.deck[i];
                    var cardToSwapIndex = GameServer.OpponentPlayerState.deck[indexOfCardInDeck];

                    GameServer.OpponentPlayerState.deck[indexOfCardInDeck] = cardAtSwapIndex;
                    GameServer.OpponentPlayerState.deck[i] = cardToSwapIndex;
                }
            }

            global::GameLogic.StartCommand(draw);
        }

        public static void Mulligan()
        {
            //TODO: Have the AI actually figure out which cards to keep (actually mulligan)

            var command = new FinishedMulliganCommand
            {
                PlayerUid = GameServer.OpponentPlayerState.Uid
            };

            global::GameLogic.StartCommand(command);
        }

        public static void StartTurn()
        {
            Disposed = false;

            var turnStartCommand = new TurnStartCommand
            {
                PlayerUid = GameServer.OpponentPlayerState.Uid,
                Team = Team.Opponent
            };

            global::GameLogic.StartCommand(turnStartCommand);

            Timing.RunCoroutine(UpdateTurn(AIStrategy.DoubloonCheat), CommandResolver.ResolvingTag);
        }

        public static IEnumerator<float> UpdateTurn(AIStrategy strategy)
        {
            FailedAiAttempts = 0;
            Disposed = false;

            yield return Timing.WaitUntilDone(
                DoubloonCheat(Team.Opponent).CancelWith(GameplayScreen.Instance.gameObject), CommandResolver.ResolvingTag);

            if (GameplayScreen.Instance.IsShowingTargets())
            {
                GameplayScreen.Instance.HideTargets();
            }

            while (Thinking || Server.CommandResolver.Executing)
            {
                yield return Timing.WaitForOneFrame;
            }

            EndTurn();
        }


        private static bool CanPlayAtLeastOneCard(Team team)
        {
            // get the player dependant on team
            var playerState = GameServer.GetPlayerState(team);

            // get the mana we have + the cards that increase our mana output
            var totalMana = playerState.currentMana;
            var totalManaWithManaModifyingSpells =
                TotalManaWithManaModifyingSpells(team, totalMana, out var manaModifyingSpells);

            // get valid cards dependant on mana restrictions
            var validCards = GetOrderedValidCards(team, totalManaWithManaModifyingSpells, manaModifyingSpells);
            var unoccupiedTiles = GameServer.State.Board.GetUnOccupiedTiles(team);

            // checks to make sure there's at least 1 valid card and 2 valid unoccupied tiles (1 that's melee and 1 that's ranged)
            // this way there's no infinite loops.
            return validCards.Count > 0 && unoccupiedTiles.Count > 1 &&
                   unoccupiedTiles.Exists(t => t.LocalPosition.y == 0) &&
                   unoccupiedTiles.Exists(t => t.LocalPosition.y == 1);
        }

        public static IEnumerator<float> DoubloonCheat(Team team)
        {
            while (Server.CommandResolver.Executing)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (Disposed) yield break;

            var player = GameServer.GetPlayerState(team);

            if (GameServer.State.CurrentPlayer != player.Uid)
            {
                Debug.LogError("Not my turn anymore, stop updating!");
                yield break;
            }


            var opponent = GameServer.GetPlayerState(team == Team.Client ? Team.Opponent : Team.Client);
            var opponentHero = GameServer.State.GetCard(opponent.heroUid);
            var boardDamage = GetTotalBoardDamage(team, GameServer.State, false);

            if (boardDamage >= opponentHero.GetPendingHealth())
            {
                yield break;
            }

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            Thinking = true;

            // show the hand while thinking..
            GameplayScreen.Instance.opponentHandView.Show();

            var task = Task.Run(() => GetAiDecision(team));
            yield return Timing.WaitUntilDone(task.AsCoroutine());

            if (Disposed) yield break;

            while (GameServer.State.BattleEventsUpdater.IsPlaying)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (Disposed) yield break;

            var handDecision = task.Result;
            task.Dispose();

            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;

            Thinking = false;

            if (handDecision.commands.Count <= 0 && CanPlayAtLeastOneCard(team) && FailedAiAttempts <= 1)
            {
                FailedAiAttempts++;
                yield return Timing.WaitUntilDone(DoubloonCheat(team).CancelWith(GameplayScreen.Instance.gameObject),
                    CommandResolver.ResolvingTag);
                yield break;
            }

            if (Disposed) yield break;

            var decisionReport = "<color=red>" +
                                 $"Doubloon Cheat Decision Commands ({handDecision.commands.Count}) " +
                                 $"Score: {handDecision.score} " +
                                 $"Milliseconds: ({ts.Milliseconds})</color>\n";

            for (var i = 0; i < handDecision.commands.Count; i++)
            {
                decisionReport += $"[{i}] = {handDecision.commands[i].GetLabelDisplay()}\n";
            }

            Debug.Log(decisionReport);


            while (Server.CommandResolver.Executing)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (Disposed) yield break;

            for (var i = 0; i < handDecision.commands.Count; i++)
            {
                if (Disposed) yield break;

                if (GameServer.IsGamePendingFinished())
                {
                    yield break;
                }

                var command = handDecision.commands[i];


                if (command is SummonCommand summonCommand)
                {
                    GameServer.SetPendingState(null);
                    global::GameLogic.StartCommand(summonCommand);
                    yield return Timing.WaitForSeconds(2);
                }
                else if (command is PlayCommand playCommand)
                {
                    var playedCard = GameServer.State.GetCard(playCommand.SourceUid);

                    if (playedCard == null)
                    {
                        var cardUidByIndex = player.GetHandCard(playCommand.HandIndex);
                        playCommand.SourceUid = cardUidByIndex;
                        playedCard = GameServer.State.GetCard(playCommand.SourceUid);
                    }

                    if (playedCard == null)
                    {
                        continue;
                    }

                    if (playedCard.characterData.type == CardType.Power)
                    {
                        playedCard.limitBreakValue = 0;
                        GameplayScreen.Instance.heroSideBarContainer.GetHeroView(team).heroLimitBreak
                            .UpdateLimitBreak();
                    }

                    global::GameLogic.StartCommand(playCommand);

                    Events.Publish(null, EventType.OnCardPlayed,
                        new OnCardPlayedEventArgs
                            { isClientCard = playedCard.Team == Team.Client, cardUid = playedCard.uid });

                    yield return Timing.WaitForSeconds(2);
                }

                while (Server.CommandResolver.Executing)
                {
                    yield return Timing.WaitForOneFrame;
                }

                yield return Timing.WaitForSeconds(2);
            }

            while (Server.CommandResolver.Executing)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (Disposed) yield break;

            var playerState = GameServer.GetPlayerState(team);

            if (handDecision.commands.Count > 0)
            {
                // get the mana we have + the cards that increase our mana output
                var totalMana = playerState.currentMana;
                var totalManaWithManaModifyingSpells =
                    TotalManaWithManaModifyingSpells(team, totalMana, out var manaModifyingSpells);

                if (totalManaWithManaModifyingSpells > 0)
                {
                    //DoubloonCheat(team);`
                    yield return Timing.WaitUntilDone(
                        DoubloonCheat(team).CancelWith(GameplayScreen.Instance.gameObject), CommandResolver.ResolvingTag);
                    yield break;
                }
            }

            if (Disposed) yield break;

            var blitzBurst = GameServer.State.GetCard(playerState.blitzBurst);

            if (blitzBurst != null && blitzBurst.limitBreakValue >= blitzBurst.data.limitBreak.value)
            {
                var totalMana = playerState.currentMana;
                var totalManaWithManaModifyingSpells =
                    TotalManaWithManaModifyingSpells(team, totalMana, out var manaModifyingSpells);

                if (blitzBurst.data.aiProperties.strategy.overrideCost <= totalManaWithManaModifyingSpells &&
                    blitzBurst.limitBreakValue >= blitzBurst.data.limitBreak.value)
                {
                    yield return Timing.WaitUntilDone(
                        DoubloonCheat(team).CancelWith(GameplayScreen.Instance.gameObject), CommandResolver.ResolvingTag);
                    yield break;
                }
            }

            if (Disposed) yield break;

            while (Server.CommandResolver.Executing)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (Disposed) yield break;

            if (FailedSummonAttempt)
            {
                FailedSummonAttempt = false;
                yield return Timing.WaitUntilDone(DoubloonCheat(team).CancelWith(GameplayScreen.Instance.gameObject), CommandResolver.ResolvingTag);
                yield break;
            }
        }


        // public static async Task<Decision> GetAiDecisionNew(Team team)
        // {
        //     // 1. Lethal?
        //     // 2. Dead next turn?
        //     // -> Heal
        //     // -> Removal (Board or Single)
        //     // -> Block
        //     // 3. How are we playing?
        //     // -> Defensive? Look at resources to check
        //     // ->
        //     // Priority
        //     // -> Minion Summon ( Power + Health + Ability Priority )
        //     //  -> Positioning.
        //     // 4. How to summon minions.
        //     // -> Positioning
        //     // -> Aggressive Deck: Face is high priority
        // }

        public static async Task<Decision> GetAiDecision(Team team)
        {
            var decisions = new List<Decision>();
            var random = new System.Random();

            // get the player dependant on team
            var playerState = GameServer.GetPlayerState(team);
            var opponentState = GameServer.GetPlayerState(team == Team.Client ? Team.Opponent : Team.Client);

            // get the mana we have + the cards that increase our mana output
            var totalMana = playerState.currentMana;
            var totalManaWithManaModifyingSpells = TotalManaWithManaModifyingSpells(team, totalMana, out var manaModifyingSpells);

            // get valid cards dependant on mana restrictions
            var validCards = GetOrderedValidCards(team, totalManaWithManaModifyingSpells, manaModifyingSpells);
            var unoccupiedTiles = GameServer.State.Board.GetUnOccupiedTiles(team);
            var occupiedTiles = GameServer.State.Board.GetOccupiedTiles(team);
            var bestDecision = new Decision { score = -100 };
            var getAiScoreParams = new GetAiScoreParameters();
            var battleData = Db.BattleDatabase.GetBattleData(GameServer.Mode.BattleUid);

            // start gold otk here...
            // - Play every Doubloon
            // - Play every Shiny Loot
            // - Play every Doubloon
            // - Check if I have enough mana to play eve (7 but sometimes 1)
            //     - If not, break;
            // - Check card count >= opponent health
            //     - If not, check bb is ready
            //                           - If so, play bb and eve
            //     - If not, break;
            // - If so, play eve

            // check to see if you're playing an OTK deck..
            //if (battleData != null && battleData.Strategy == AIStrategy.DoubloonOtk)
            {
                // remove all doubloons, shiny loots and eves from valid cards if this doesn't workout.
                // var otkActive = false;
                var eve = validCards.Find(c => c.data.id == GDEItemKeys.Card_Eve);

                if (eve != null)
                {
                    var decision = new Decision();
                    var playedCards = 0;
                    var playedDoubloons = 0;

                    // reset the state back to the main state
                    var stateCopy = GameServer.MainState.Copy();
                    GameServer.SetPendingState(stateCopy);

                    var currPlayerState = GameServer.GetPlayerState(team);
                    var currTotalManaWithManaModifyingSpells = totalManaWithManaModifyingSpells;
                    var currValidCards = new List<GameCardState>( validCards );
                    var currentMana = currPlayerState.currentMana;

                    var doubloons = new List<GameCardState>(manaModifyingSpells);
                    var shinyLoots = currValidCards.FindAll(c => c.characterData.id == "GoldenManaShard");
                    var loopCount = 0;
                    const int MaxNumberOfLoops = 200;

                    while (doubloons.Count > 0 || shinyLoots.Count > 0)
                    {
                        var maxDoubloonsToPlay = Mathf.Min(doubloons.Count, 10 - currentMana);

                        for (var i = 0; i < maxDoubloonsToPlay; i++)
                        {
                            var doubloon = doubloons[i];
                            var playCommand = GetPlayCardCommand(playerState.Uid, doubloon);

                            if (playCommand == null)
                            {
                                continue;
                            }

                            global::GameLogic.StartCommand(playCommand, false);
                            decision.commands.Add(playCommand);
                            playedCards++;
                            currentMana++;
                            playedDoubloons++;
                        }

                        for (var i = 0; i < shinyLoots.Count; i++)
                        {
                            var shinyLoot = shinyLoots[i];

                            if (/*currPlayerState.hand.Count > 8 ||*/ currentMana < shinyLoot.GetCost())
                            {
                                break;
                            }

                            var playCommand = GetPlayCardCommand(playerState.Uid, shinyLoot);

                            if (playCommand == null)
                            {
                                continue;
                            }

                            global::GameLogic.StartCommand(playCommand, false);
                            decision.commands.Add(playCommand);
                            playedCards++;
                            currentMana -= shinyLoot.GetCost();
                        }

                        currTotalManaWithManaModifyingSpells = TotalManaWithManaModifyingSpells(team, currentMana, out var currManaModifyingSpells);
                        currValidCards = GetOrderedValidCards(team, currTotalManaWithManaModifyingSpells, currManaModifyingSpells);
                        doubloons = new List<GameCardState>(currManaModifyingSpells);
                        shinyLoots = currValidCards.FindAll(c => c.characterData.id == "GoldenManaShard");

                        if (doubloons.Count > 0 && shinyLoots.Count <= 0 && currentMana >= 10)
                        {
                            break;
                        }

                        if (shinyLoots.Count > 0 /*&& currPlayerState.hand.Count > 8*/ && doubloons.Count <= 0)
                        {
                            break;
                        }

                        loopCount++;

                        if (loopCount >= MaxNumberOfLoops)
                        {
                            break;
                        }
                    }

                    var opponentHero = GameServer.State.GetCard(opponentState.heroUid);
                    var battlecryCount = GameServer.State.AuraUpdater.GetBattlecryCount(currPlayerState.Uid);
                    playedCards *= battlecryCount;

                    if (currentMana >= eve.GetCost())
                    {
                        var blitzBurstActive = false;

                        if (!string.IsNullOrEmpty(playerState.blitzBurst))
                        {
                            var blitzBurst = GameServer.State.GetCard(playerState.blitzBurst);

                            if (blitzBurst.limitBreakValue + playedDoubloons >= blitzBurst.data.limitBreak.value)
                            {
                                var playCommand = GetPlayCardCommand(playerState.Uid, blitzBurst);

                                if (playCommand != null)
                                {
                                    global::GameLogic.StartCommand(playCommand, false);
                                    decision.commands.Add(playCommand);
                                    blitzBurstActive = true;
                                }
                            }
                        }

                        if (playedCards >= opponentHero.GetPendingHealth() || blitzBurstActive)
                        {
                            var frontTiles = unoccupiedTiles.FindAll(t => t.LocalPosition.y == 0);
                            var tile = frontTiles[random.Next(0, frontTiles.Count)];
                            var summonCommand = GetSummonCommandForCard(playerState.Uid, tile, eve,
                                new Vector2Int(-1, -1), opponentHero.uid);
                            global::GameLogic.StartCommand(summonCommand, false);
                            decision.commands.Add(summonCommand);

                            GameServer.SetPendingState(null);
                            return decision;
                        }
                    }
                }

                for (var i = validCards.Count-1; i > -1; i--)
                {
                    var card = validCards[i];
                    if (card.characterData.id == "Eve" || card.characterData.id == "Doubloon" || card.characterData.id == "GoldenManaShard")
                    {
                        validCards.RemoveAt(i);
                    }
                }

                GameServer.SetPendingState(null);
            }
            // end gold otk here...

            var allCardsInAllOrders = GameUtilities.GetAllPermutations(validCards);
            var startingPlaysCount = allCardsInAllOrders.Count;

            // groups that exceed your total mana should not be evaluated
            for (var i = allCardsInAllOrders.Count - 1; i > -1; i--)
            {
                var requiredMana = 0;

                for (var j = 0; j < allCardsInAllOrders[i].Count; j++)
                {
                    requiredMana += allCardsInAllOrders[i][j].GetCost();
                }

                if (requiredMana > totalManaWithManaModifyingSpells)
                {
                    allCardsInAllOrders.RemoveAt(i);
                }
            }

            var playsAfterManaEval = allCardsInAllOrders.Count;

            var disposals = new List<int>();

            // remove all permutations that have duplicate orders of cards with the same name.
            for (var i = 0; i < allCardsInAllOrders.Count; i++)
            {
                for (var j = i+1; j < allCardsInAllOrders.Count; j++)
                {
                    if (disposals.Contains(j)) continue;

                    var list1 = allCardsInAllOrders[i].ConvertAll( card => card.data.id);
                    var list2 = allCardsInAllOrders[j].ConvertAll( card => card.data.id);

                    var equal = list1.AsEnumerable().SequenceEqual(list2.AsEnumerable());

                    if (equal)
                    {
                        disposals.Add(j);
                    }
                }
            }

            for (var i = disposals.Count-1; i > -1; i--)
            {
                allCardsInAllOrders.RemoveAt(i);
            }

            var playsAfterDuplicateEval = allCardsInAllOrders.Count;

#if UNITY_EDITOR
            var listOfDecisions = "LIST OF DECISIONS:\n";
            var listOfCardOrders = $"LIST OF CARD ORDERS [MANA={totalManaWithManaModifyingSpells}] [TOTAL PLAYS: {startingPlaysCount}, AFTER MANA RESTRICTION: {playsAfterManaEval}, AFTER DUP EVAL: {playsAfterDuplicateEval}]:\n";
            var listCount = 0;

            foreach (var list in allCardsInAllOrders)
            {
                listOfCardOrders += $"\n#{listCount}\n";

                foreach (var cardState in list)
                {
                    listOfCardOrders += $"({cardState.GetCost()}) {cardState.characterData.name}\n";
                }

                listCount++;
            }

            Debug.Log(listOfCardOrders);
#endif


            //var stopAiIfScoreIsEqualToOrHigherThanThis = 10;

            for (var i = 0; i < allCardsInAllOrders.Count; i++)
            {
                var decision = new Decision();

                // reset the state back to the main state
                var stateCopy = GameServer.MainState.Copy();
                GameServer.SetPendingState(stateCopy);

                var historyCountBeforeExecution = GameServer.State.History.Count;
                var unoccupiedTilesUpdate = new List<TileState>(unoccupiedTiles);
                var unoccupiedTilesDic = new Dictionary<Vector2Int, TileState>();
                var occupiedTilesDic = new Dictionary<Vector2Int, TileState>();

                for (var j = 0; j < unoccupiedTilesUpdate.Count; j++)
                {
                    unoccupiedTilesDic.Add(unoccupiedTilesUpdate[j].LocalPosition, unoccupiedTilesUpdate[j]);
                }

                for (var j = 0; j < occupiedTiles.Count; j++)
                {
                    occupiedTilesDic.Add(occupiedTiles[j].LocalPosition, occupiedTiles[j]);
                }

                for (var j = 0; j < allCardsInAllOrders[i].Count; j++)
                {
                    var card = allCardsInAllOrders[i][j];

                    // =================================================================================
                    // MINIONS START HERE!!!
                    // =================================================================================
                    if (card.data.type == CardType.Minion || card.data.type == CardType.Trick)
                    {
                        // todo: go through every valid tile and get the best tile to summon on.
                        // todo: for targeted effects, make sure we have a target.
                        // todo: for redeem or generated effects make sure we set the generated card.

                        if (unoccupiedTilesUpdate.Count <= 0)
                        {
                            continue;
                        }

                        var tilesToCheck = new List<TileState>();

                        for (var k = 0; k < unoccupiedTilesUpdate.Count; k++)
                        {
                            var tile = unoccupiedTilesUpdate[k];

                            if (card.data.aiProperties.placementBehavior.HasFlag(AiProperties.PlacementBehavior
                                    .FRONT) && tile.LocalPosition.y != 0)
                            {
                                continue;
                            }

                            if (card.data.aiProperties.placementBehavior.HasFlag(AiProperties.PlacementBehavior.BACK) && tile.LocalPosition.y != 1)
                            {
                                continue;
                            }

                            if (card.data.type == CardType.Minion)
                            {
                                if (card.data.attackType == AttackType.Melee && tile.LocalPosition.y != 0)
                                {
                                    continue;
                                }

                                if (card.data.attackType == AttackType.Ranged && tile.LocalPosition.y != 1)
                                {
                                    continue;
                                }

                                if (card.data.type == CardType.Trick && tile.LocalPosition.y != 1)
                                {
                                    continue;
                                }
                            }

                            var backTilePosition = new Vector2Int(tile.LocalPosition.x, tile.LocalPosition.y + 1);

                            if (card.data.aiProperties.placementBehavior.HasFlag(AiProperties.PlacementBehavior
                                    .EMPTY_BACK) && !unoccupiedTilesDic.ContainsKey(backTilePosition))
                            {
                                continue;
                            }

                            var frontTilePosition = new Vector2Int(tile.LocalPosition.x, tile.LocalPosition.y - 1);

                            if (card.data.aiProperties.placementBehavior.HasFlag(AiProperties.PlacementBehavior
                                    .EMPTY_FRONT) && !unoccupiedTilesDic.ContainsKey(frontTilePosition))
                            {
                                continue;
                            }

                            var leftTilePosition = new Vector2Int(tile.LocalPosition.x - 1, tile.LocalPosition.y);

                            if (card.data.aiProperties.placementBehavior.HasFlag(AiProperties.PlacementBehavior
                                    .EMPTY_LEFT) && !unoccupiedTilesDic.ContainsKey(leftTilePosition))
                            {
                                continue;
                            }

                            var rightTilePosition = new Vector2Int(tile.LocalPosition.x + 1, tile.LocalPosition.y);

                            if (card.data.aiProperties.placementBehavior.HasFlag(AiProperties.PlacementBehavior
                                    .EMPTY_RIGHT) && !unoccupiedTilesDic.ContainsKey(rightTilePosition))
                            {
                                continue;
                            }


                            // check for specific minions spawning

                            if (card.data.aiProperties.placementBehavior.HasFlag(AiProperties.PlacementBehavior.SPECIFIC_MINION))
                            {
                                if (card.data.aiProperties.placementBehavior.HasFlag(AiProperties.PlacementBehavior.FRONT))
                                {
                                    if (!occupiedTilesDic.ContainsKey(backTilePosition))
                                    {
                                        continue;
                                    }

                                    var backTile = occupiedTilesDic[backTilePosition];

                                    if (backTile.Card.characterData.id != card.data.aiProperties.cardId)
                                    {
                                        continue;
                                    }
                                }

                                if (card.data.aiProperties.placementBehavior.HasFlag(AiProperties.PlacementBehavior.BACK))
                                {
                                    if (!occupiedTilesDic.ContainsKey(frontTilePosition))
                                    {
                                        continue;
                                    }

                                    var frontTile = occupiedTilesDic[frontTilePosition];

                                    if (frontTile.Card.characterData.id != card.data.aiProperties.cardId)
                                    {
                                        continue;
                                    }
                                }
                            }

                            //check to see if the minion needs "cover"

                            if (card.data.aiProperties.placementBehavior.HasFlag(AiProperties.PlacementBehavior.FRONT_OCCUPIED) &&
                                !occupiedTilesDic.ContainsKey(frontTilePosition))
                            {
                                continue;
                            }

                            if (card.data.aiProperties.placementBehavior.HasFlag(AiProperties.PlacementBehavior.BACK_OCCUPIED) &&
                                !occupiedTilesDic.ContainsKey(backTilePosition))
                            {
                                continue;
                            }

                            // check for occupied tiles that are "occupying" the front or back of this.

                            if (occupiedTilesDic.ContainsKey(frontTilePosition))
                            {
                                var frontTile = occupiedTilesDic[frontTilePosition];

                                if (frontTile.Card.data.aiProperties.placementBehavior.HasFlag(AiProperties.PlacementBehavior.OCCUPY_BACK))
                                {
                                    continue;
                                }
                            }

                            if (occupiedTilesDic.ContainsKey(backTilePosition))
                            {
                                var backTile = occupiedTilesDic[backTilePosition];

                                if (backTile.Card.data.aiProperties.placementBehavior.HasFlag(AiProperties
                                        .PlacementBehavior.OCCUPY_FRONT))
                                {
                                    continue;
                                }
                            }

                            tilesToCheck.Add(tile);
                        }

                        if (tilesToCheck.Count <= 0)
                        {
                            continue;
                        }

                        tilesToCheck.Shuffle(random);

                        SummonCommand bestSummonCommand = null;
                        TileState summonedTile = null;

                        var tileCheckState = GameServer.State.Copy();
                        var historyCountBeforeAction = tileCheckState.History.Count;
                        var bestDecisionScore = -100;
                        var selections = card.GetSelectTargets();

                        if (card.data.aiProperties.strategy.selectionRequired && selections.Count <= 0)
                        {
                            Debug.LogWarning(
                                $"{card.characterData?.name} failed check because a selection target is required.");
                            continue;
                        }

                        // =================================================================================
                        // TILE CHECKS START HERE!!
                        // =================================================================================
                        for (var k = 0; k < tilesToCheck.Count; k++)
                        {
                            GameServer.SetPendingState(tileCheckState.Copy());

                            var selection = new Vector2Int(-1, -1);
                            var selectionHeroUid = string.Empty;
                            var tile = tilesToCheck[k];

                            if ((selection.x < 0 || !string.IsNullOrEmpty(selectionHeroUid)) && selections.Count > 0)
                            {
                                var targetMethods = card.data.aiProperties.strategy.targetConditions;

                                if (targetMethods.Count > 0)
                                {
                                    var foundTarget = false;

                                    for (var l = 0; l < targetMethods.Count; l++)
                                    {
                                        var targets = card.GetSelectTargets(tile, targetMethods[l]);

                                        if (targets.Count > 0)
                                        {
                                            foundTarget = true;
                                            selections = targets;
                                            break;
                                        }
                                    }

                                    // this makes it so, if you have a target override and can't find a target
                                    // the card will NEVER be played. Be careful with overrides or fix this behaviour.
                                    if (!foundTarget)
                                    {
                                        Debug.LogWarning($"{card.characterData?.name} failed override check.");
                                        break;
                                    }
                                }

                                var randomSelection = selections[random.Next(0, selections.Count)];
                                var selectionCard = GameServer.State.GetCard(randomSelection);

                                if (selectionCard.IsHero)
                                {
                                    selectionHeroUid = randomSelection;
                                }
                                else
                                {
                                    selection = selectionCard.position;
                                }
                            }

                            var summonCommand = GetSummonCommandForCard(playerState.Uid, tile, card, selection,
                                selectionHeroUid);

                            //Debug.Log($"SUMMON TRY {card.characterData.name}");
                            if (summonCommand == null)
                            {
                                continue; // we couldn't summon this card anywhere
                            }

                            //Debug.Log($"summon worked {card.characterData.name}");
                            global::GameLogic.StartCommand(summonCommand, false);

                            // change the priority of this minion to make the ai summon it more often.
                            var decisionScore = card.data.aiProperties.strategy.priority;
                            var gridPosition = BoardState.BoardPositionToSimpleMap[tile.Position];

                            // give arcane minions an increased score if they are summoned on the back row
                            // if (card.data.attackType == AttackType.Arcane && gridPosition.y == 0 || gridPosition.y == 3)
                            // {
                            //     decisionScore += 2;
                            // }

                            if (card.data.type == CardType.Trick)
                            {
                                decisionScore += 2;
                            }

                            if (gridPosition.y == 1 || gridPosition.y == 2)
                            {
                                var tilePos = new Vector2Int(gridPosition.x, gridPosition.y + gridPosition.y == 2 ? 1 : -1);
                                var backTile = GameServer.State.Board.GetTile(tilePos);
                                decisionScore += backTile.Occupied ? 2 : 0;
                            }

                            var passedMinimumConditions = true;

                            var abilities = card.GetAbilities();

                            if (abilities.Count > 0)
                            {
                                for (var l = 0; l < card.data.aiProperties.strategy.targetScoreConditions.Count; l++)
                                {
                                    var targetScoreCondition = card.data.aiProperties.strategy.targetScoreConditions[l];

                                    switch (targetScoreCondition.targetScoreMethod)
                                    {
                                        case AiStrategy.TargetScoreMethod.MinimumCardsPlayedThisTurn:
                                        {
                                            passedMinimumConditions = playerState.cardsPlayedThisTurn >=
                                                                      targetScoreCondition.minimumScore;
                                            break;
                                        }
                                        case AiStrategy.TargetScoreMethod.MinimumTargets:
                                        {
                                            var checkIndex = abilities.Count > targetScoreCondition.checkAbilityAtIndex
                                                ? targetScoreCondition.checkAbilityAtIndex
                                                : abilities.Count - 1;
                                            var targets = global::GameLogic.GetTargets(card, abilities[checkIndex],
                                                null, null, true);

                                            if (targets.Count < targetScoreCondition.minimumScore)
                                            {
                                                passedMinimumConditions = false;
                                            }

                                            break;
                                        }
                                        case AiStrategy.TargetScoreMethod.MinimumStatAmount:
                                        {
                                            var statValue = 0;
                                            switch (targetScoreCondition.stat)
                                            {
                                                case FilterConditionValues.Stat.Health:
                                                    statValue = card.GetPendingHealth();
                                                    break;
                                                case FilterConditionValues.Stat.Power:
                                                    statValue = card.GetPendingPower();
                                                    break;
                                                case FilterConditionValues.Stat.ManaCost:
                                                    statValue = card.GetCost();
                                                    break;
                                            }

                                            switch (targetScoreCondition.formula)
                                            {
                                                case FilterConditionValues.StatFormula.Equal:
                                                    passedMinimumConditions =
                                                        statValue == targetScoreCondition.minimumScore;
                                                    break;
                                                case FilterConditionValues.StatFormula.GreaterThan:
                                                    passedMinimumConditions =
                                                        statValue > targetScoreCondition.minimumScore;
                                                    break;
                                                case FilterConditionValues.StatFormula.GreaterThanOrEqualTo:
                                                    passedMinimumConditions =
                                                        statValue >= targetScoreCondition.minimumScore;
                                                    break;
                                                case FilterConditionValues.StatFormula.LessThan:
                                                    passedMinimumConditions =
                                                        statValue < targetScoreCondition.minimumScore;
                                                    break;
                                                case FilterConditionValues.StatFormula.LessThanOrEqualTo:
                                                    passedMinimumConditions =
                                                        statValue <= targetScoreCondition.minimumScore;
                                                    break;
                                            }

                                            break;
                                        }
                                    }
                                }

                                if (!passedMinimumConditions)
                                {
                                    break;
                                }
                            }

                            if (!passedMinimumConditions)
                            {
                                continue;
                            }

                            for (var l = historyCountBeforeAction; l < GameServer.State.History.Count; l++)
                            {
                                var history = GameServer.State.History[l];

                                switch (history.Type)
                                {
                                    case CommandType.DAMAGE when history is DamageCommand damageCommand:
                                        var targetScoreCondition =
                                            card.data.aiProperties.strategy.targetScoreConditions.Find(n =>
                                                n.targetScoreMethod == AiStrategy.TargetScoreMethod.MinimumDamageScore);

                                        if (targetScoreCondition != null &&
                                            damageCommand.Amount < targetScoreCondition.minimumScore &&
                                            (damageCommand.Amounts == null || damageCommand.Amounts.Sum(a => a) <
                                                targetScoreCondition.minimumScore))
                                        {
                                            passedMinimumConditions = false;
                                            continue;
                                        }

                                        break;
                                }

                                if (!passedMinimumConditions)
                                {
                                    break;
                                }

                                decisionScore += history.GetAiScore(getAiScoreParams);
                            }

                            if (!passedMinimumConditions)
                            {
                                continue;
                            }

                            var opponentDestroyed = GameServer.State.GetCard(team == Team.Client
                                ? GameServer.OpponentPlayerState.heroUid
                                : GameServer.ClientPlayerState.heroUid).GetPendingHealth() <= 0
                                ? 10000
                                : 0;

                            var potentialDamageFromSummon = GetTotalBoardDamage(team, GameServer.State, true);
                            var potentialDamageSavedFromSummon =
                                GetTotalBoardDamage(team == Team.Client ? Team.Opponent : Team.Client, GameServer.State,
                                    true);

                            // the score = any score increases from loop + total board damage + the amount of commands (moves) this decision has.
                            decisionScore += potentialDamageFromSummon - potentialDamageSavedFromSummon;
                            decisionScore += opponentDestroyed;
                            var overrideScore = decisionScore == bestDecisionScore && random.Next(0, 2) == 0;

                            if (decisionScore > bestDecisionScore || overrideScore)
                            {
                                bestDecisionScore = decisionScore;
                                bestSummonCommand = summonCommand;
                                summonedTile = tilesToCheck[k];
                            }
                        }
                        // tile checks end here...

                        if (bestSummonCommand != null)
                        {
                            decision.commands.Add(bestSummonCommand);
                            unoccupiedTilesUpdate.Remove(summonedTile);
                        }
                    }
                    // minions end here...

                    // =================================================================================
                    // SPELLS AND BLITZ BURST START HERE!!
                    // =================================================================================
                    else if (card.data.type == CardType.Spell || card.data.type == CardType.Power)
                    {
                        var potentialOpponentDamage = 0;

                        if (card.HasTargetCondition(FilterCondition.PLAYER_SELECTED) && !card.ShouldSelectTarget(null))
                        {
                            continue;
                        }

                        var abilities = card.GetAbilities();

                        if (abilities.Count <= 0)
                        {
                            continue;
                        }

                        var passesAllStrategyConditions = true;

                        TargetConditions overrideTargetConditions = null;

                        if (card.data.aiProperties.strategy.targetConditions.Count > 0)
                        {
                            overrideTargetConditions = card.data.aiProperties.strategy.targetConditions[0];
                        }

                        for (var k = 0; k < card.data.aiProperties.strategy.targetScoreConditions.Count; k++)
                        {
                            var targetScoreCondition = card.data.aiProperties.strategy.targetScoreConditions[k];

                            switch (targetScoreCondition.targetScoreMethod)
                            {
                                case AiStrategy.TargetScoreMethod.MinionsOccupyingOpponentsSide:
                                {
                                    var minionsOnOpponentsSide =
                                        GameServer.State.Board.GetOccupiedTiles(team == Team.Client
                                            ? Team.Opponent
                                            : Team.Client);

                                    if (minionsOnOpponentsSide.Count < targetScoreCondition.minimumScore)
                                    {
                                        passesAllStrategyConditions = false;
                                    }

                                    break;
                                }
                                case AiStrategy.TargetScoreMethod.MinimumTargets:
                                {
                                    var checkIndex = abilities.Count > targetScoreCondition.checkAbilityAtIndex ? targetScoreCondition.checkAbilityAtIndex : abilities.Count - 1;
                                    var targets = global::GameLogic.GetTargets(card, abilities[checkIndex],
                                        null, null, false,
                                        overrideTargetConditions: overrideTargetConditions );

                                    if (targets.Count < targetScoreCondition.minimumScore)
                                    {
                                        passesAllStrategyConditions = false;
                                    }

                                    break;
                                }
                                case AiStrategy.TargetScoreMethod.MinimumStatAmount:
                                {
                                    var checkIndex = abilities.Count > targetScoreCondition.checkAbilityAtIndex ? targetScoreCondition.checkAbilityAtIndex : abilities.Count - 1;
                                    var targets = global::GameLogic.GetTargets(card, abilities[checkIndex],
                                        null, null, false,
                                        overrideTargetConditions: overrideTargetConditions );
                                    var passes = false;

                                    for (var l = 0; l < targets.Count; l++)
                                    {
                                        var statValue = 0;
                                        var target = GameServer.State.GetCard(targets[l]);

                                        if (target == null)
                                        {
                                            continue;
                                        }

                                        switch (targetScoreCondition.stat)
                                        {
                                            case FilterConditionValues.Stat.Health:
                                                statValue = target.GetPendingHealth();
                                                break;
                                            case FilterConditionValues.Stat.Power:
                                                statValue = target.GetPendingPower();
                                                break;
                                            case FilterConditionValues.Stat.ManaCost:
                                                statValue = target.GetCost();
                                                break;
                                        }

                                        switch (targetScoreCondition.formula)
                                        {
                                            case FilterConditionValues.StatFormula.Equal:
                                                passes = statValue == targetScoreCondition.minimumScore;
                                                break;
                                            case FilterConditionValues.StatFormula.GreaterThan:
                                                passes = statValue > targetScoreCondition.minimumScore;
                                                break;
                                            case FilterConditionValues.StatFormula.GreaterThanOrEqualTo:
                                                passes = statValue >= targetScoreCondition.minimumScore;
                                                break;
                                            case FilterConditionValues.StatFormula.LessThan:
                                               passes = statValue < targetScoreCondition.minimumScore;
                                                break;
                                            case FilterConditionValues.StatFormula.LessThanOrEqualTo:
                                                passes = statValue <= targetScoreCondition.minimumScore;
                                                break;
                                        }

                                        if (passes)
                                        {
                                            passesAllStrategyConditions = true;
                                            break;
                                        }
                                    }

                                    if (!passes)
                                    {
                                        passesAllStrategyConditions = false;
                                    }

                                    break;
                                }
                            }

                            if (!passesAllStrategyConditions)
                            {
                                break;
                            }
                        }

                        if (!passesAllStrategyConditions)
                        {
                            continue;
                        }

                        var playCommand = GetPlayCardCommand(playerState.Uid, card);

                        if (playCommand == null)
                        {
                            Debug.LogWarning($"could not get play command for {card.characterData.name}");
                            continue;
                        }

                        global::GameLogic.StartCommand(playCommand, false);

                        decision.commands.Add(playCommand);
                    }
                }

                var scoreFactor = "breakdown:\n";

                for (var j = historyCountBeforeExecution; j < GameServer.State.History.Count; j++)
                {
                    var cardId = GameServer.State.History[j].SourceUid;
                    var card = GameServer.State.GetCard(cardId);
                    var score = GameServer.State.History[j].GetAiScore(getAiScoreParams) + 1;

                    if (card != null && card.data != null)
                    {
                        score += card.data.aiProperties.strategy.priority+1;
                        scoreFactor += $"{card.data.name} (+{card.data.aiProperties.strategy.priority}) ";
                    }

                    decision.score += score;
                    scoreFactor += $"{GameServer.State.History[j]}: {score}\n";
                }

                var potentialDamage = GetTotalBoardDamage(team, GameServer.State, true);
                var potentialDamageSaved = GetTotalBoardDamage(team == Team.Client ? Team.Opponent : Team.Client, GameServer.State, true);
                var damageScore = potentialDamage - potentialDamageSaved;

#if UNITY_EDITOR
                scoreFactor += $"Damage Score: {damageScore} ({potentialDamage}-{potentialDamageSaved})";
#endif
                // the score = any score increases from loop + total board damage + the amount of commands (moves) this decision has.
                decision.score += damageScore;
                decisions.Add(decision);

#if UNITY_EDITOR
                listOfDecisions += $"\n#{i} Score={decision.score} Count={decision.commands.Count}\n{scoreFactor}";
#endif
            }

            // order the decisions by cost so we always try to fill our mana
            //decisions = decisions.OrderByDescending(d => d.GetManaRequiredForThisDecision()).ToList();

            for (var i = 0; i < decisions.Count; i++)
            {
                var decision = decisions[i];
                //var randomEquals = decision.score == bestDecision.score;

                if (decision.score > bestDecision.score)
                {
                    var manaRequired = decision.GetManaRequiredForThisDecision();

                    if (manaRequired <= totalMana) // if we have enough mana, this decision is valid, we continue
                    {
                        bestDecision = decision;
                        continue;
                    }

                    var manaModifiers = 0;

                    // we can get enough mana using spells we should add them to the decision
                    if (manaRequired <= totalManaWithManaModifyingSpells)
                    {
                        bestDecision = decision;

                        // lets add our mana modifying spells to the list so we can play this combo
                        for (var j = 0; j < manaModifyingSpells.Count; j++)
                        {
                            var abilities = manaModifyingSpells[j].GetAbilities();
                            var cost = manaModifyingSpells[j].GetCost();

                            for (var k = 0; k < abilities.Count; k++)
                            {
                                var ability = abilities[k];

                                if (ability.manaModifier > 0 && !ability.modifyMaximumManaOnly && cost <= 0)
                                {
                                    var playCommand = GetPlayCardCommand(playerState.Uid, manaModifyingSpells[j]);
                                    if (playCommand == null) continue;
                                    decision.commands.Push(playCommand);
                                    manaModifiers += ability.manaModifier;

                                    if (manaRequired <= totalMana + manaModifiers) break;
                                }
                            }

                            if (manaRequired <= totalMana + manaModifiers) break;
                        }
                    }
                }
            }
#if UNITY_EDITOR
            listOfDecisions +=
                $"Best Decision: Num Commands: {bestDecision.commands.Count} Score: ({bestDecision.score})";

            Debug.LogWarning(listOfCardOrders);
            Debug.LogWarning(listOfDecisions);
#endif
            // set the pending state to null so that the original state is updated again!
            GameServer.SetPendingState(null);

            return bestDecision;
        }


        private static int TotalManaWithManaModifyingSpells(Team team, int totalMana, out List<GameCardState> manaModifyingSpells)
        {
            var totalManaWithManaModifyingSpells = totalMana;
            manaModifyingSpells = GetValidSpells(team, totalManaWithManaModifyingSpells, AbilityKeyword.MODIFY_MANA_CRYSTALS);
            manaModifyingSpells.RemoveAll(mms => mms.GetAbility(AbilityKeyword.MODIFY_MANA_CRYSTALS).modifyMaximumManaOnly);

            for (var i = 0; i < manaModifyingSpells.Count; i++)
            {
                var abilities = manaModifyingSpells[i].GetAbilities();
                for (var j = 0; j < abilities.Count; j++)
                {
                    var ability = abilities[j];
                    if (ability.manaModifier > 0 && !ability.modifyMaximumManaOnly && manaModifyingSpells[i].GetCost() <= 0)
                    {
                        totalManaWithManaModifyingSpells += ability.manaModifier;
                    }
                }
            }

            return totalManaWithManaModifyingSpells;
        }

        public static int GetTotalBoardDamage(Team team, GameState State, bool returnPotentialDamage)
        {
            var totalDamage = 0;
            var opponentTeam = team == Team.Client ? Team.Opponent : Team.Client;

            var occupiedTiles = State.Board.GetOccupiedTiles(team);
            var occupiedTilesOpponentTeam = State.Board.GetOccupiedTiles(opponentTeam);
            var halfDamage = false;

            for (var i = 0; i < occupiedTilesOpponentTeam.Count; i++)
            {
                var card = occupiedTilesOpponentTeam[i].Card;

                if (card.HasAbility(AbilityKeyword.GUARD))
                {
                    halfDamage = true;
                    break;
                }
            }

            for (var i = 0; i < occupiedTiles.Count; i++)
            {
                var tile = occupiedTiles[i];

                if (returnPotentialDamage || tile.Card.AbleToAttack(tile.LocalPosition.y))
                {
                    for (var j = 0; j < tile.Card.GetAttackCount(); j++)
                    {
                        var invertedPosition = State.Board.InvertPosition(tile.LocalPosition);
                        if (!State.Board.IsOccupied(new Vector2Int(invertedPosition.x, 0), opponentTeam) &&
                            !State.Board.IsOccupied(new Vector2Int(invertedPosition.x, 1), opponentTeam))
                        {
                            var multiplier = tile.Card.HasAbility(AbilityKeyword.DUAL_STRIKE) ? 2 : 1;
                            totalDamage += tile.Card.GetPendingPower() * multiplier;
                        }
                    }
                }
            }

            if (halfDamage)
            {
                totalDamage = (int)(totalDamage * 0.5f);
            }

            return totalDamage;
        }

        public static int GetTotalHandDamage(Team team)
        {
            var totalDamage = 0;
            var opponentTeam = team == Team.Client ? Team.Opponent : Team.Client;
            var playerState = GameServer.GetPlayerState(team);
            var potentialMana = playerState.currentMana;

            // check for specific cards??????????????
            return totalDamage;
        }

        public static IEnumerator<float> UpdateDoubloonOtk()
        {
            var totalMana = GameServer.OpponentPlayerState.currentMana;
            var totalManaWithManaModifyingSpells = totalMana;
            var spells = GetValidSpells(Team.Opponent, totalMana, AbilityKeyword.MODIFY_MANA_CRYSTALS);

            for (var i = 0; i < spells.Count; i++)
            {
                var abilities = spells[i].GetAbilities();
                for (var j = 0; j < abilities.Count; j++)
                {
                    var ability = abilities[j];
                    if (ability.manaModifier > 0 && !ability.modifyMaximumManaOnly && spells[i].GetCost() <= 0)
                    {
                        totalManaWithManaModifyingSpells++;
                    }
                }
            }

            // Check for win condition

            // 1. check if we have the cards we need for the OTK
            // 2. if we can win using the otk then do it.
            // 3. check for possible board damage
            // 4. if enough to win, do it
            // 5. check for any other damage to add for a possible win, does not have to be the exact OTK
            // 6. if enough to win, do it
            // 7. at this point we have another turn--
            // 8. Check for losing conditions and block or remove minions based on priority. (Will need to add some priority information on minions).
            // 9. Checking the players hand / next drawn card is also fine (even if its cheating a bit I think the AI will be better for it).
            // 10.

            // for this win condition to work I need certain things:
            // 1. at least 8 mana for the full combo!
            // 2. 3 slots of board space
            // 3. 5 total cards in my hand (3 after dropping mistmancer and eve)
            // 4. I need a card to play this turn (save a pirate recruit?)

            var validCards = GetValidCards(Team.Opponent, totalManaWithManaModifyingSpells);
            const int neededManaForFullCombo = 8;
            const int neededBoardSlotsForFullCombo = 3;
            const int maxCardsInHandForFullCombo = 3;
            var eveCount = 0;
            var mistmancerJinnCount = 0;
            var eldaCount = 0;
            var pirateRecruits = 0;
            var doubloons = 0;
            var cardsInHand = GameServer.OpponentPlayerState.hand.Count;
            var unoccupiedTiles = GameServer.State.Board.GetUnOccupiedTiles(Team.Opponent);
            var hero = GameServer.State.GetCard(GameServer.ClientPlayerState.heroUid);
            var clientHealthLeft = hero.GetHealth();
            //var maxDamage = 0;

            for (var i = 0; i < validCards.Count; i++)
            {
                var validCard = validCards[i];

                if (validCard.data.id == GDEItemKeys.Card_Eve) eveCount++;
                if (validCard.data.id == GDEItemKeys.Card_MistmancerJinn) mistmancerJinnCount++;
                if (validCard.data.id == GDEItemKeys.Card_SparkleFiendElda) eldaCount++;
                if (validCard.data.id == GDEItemKeys.Card_PirateRecruit) pirateRecruits++;
                if (validCard.data.id == GDEItemKeys.Card_Doubloon) doubloons++;
            }

            if (totalManaWithManaModifyingSpells < neededManaForFullCombo)
            {
                // not enough mana, lets just continue playing as normal by being very defensive
            }
            else if ((cardsInHand - doubloons - pirateRecruits - eveCount - mistmancerJinnCount) >
                     maxCardsInHandForFullCombo)
            {
                // we have too many cards in hand to setup the full combo, this might still work depending on how much health the player has.
            }
            else if (unoccupiedTiles.Count >= neededBoardSlotsForFullCombo)
            {
            }

            if (totalManaWithManaModifyingSpells >= neededManaForFullCombo && (pirateRecruits > 0 || doubloons > 0) &&
                eveCount > 1 && mistmancerJinnCount > 0 && eldaCount > 0 &&
                (cardsInHand - doubloons - pirateRecruits - eveCount - mistmancerJinnCount) <=
                maxCardsInHandForFullCombo &&
                unoccupiedTiles.Count >= neededBoardSlotsForFullCombo)
            {
            }

            var possibleDamageFromBoard = GetPossibleDamageFromMinions(totalManaWithManaModifyingSpells);


            yield break;
        }

        public static IEnumerator<float> UpdateNoneStrategy()
        {
            var totalMana = GameServer.OpponentPlayerState.currentMana;

            while (totalMana > 0)
            {
                yield return Timing.WaitForSeconds(1);

                var cards = GetValidCards(Team.Opponent, totalMana);

                if (cards.Count <= 0)
                {
                    EndTurn();
                    break;
                }

                var randCard = cards[Random.Range(0, cards.Count)];

                if (randCard.data.type == CardType.Spell)
                {
                    var playCommand = new PlayCommand
                    {
                        PlayerUid = GameServer.OpponentPlayerState.Uid,
                        SourceUid = randCard.uid
                    };

                    totalMana -= randCard.GetCost();

                    global::GameLogic.StartCommand(playCommand);

                    Events.Publish(null, EventType.OnCardPlayed,
                        new OnCardPlayedEventArgs
                            { isClientCard = randCard.Team == Team.Client, cardUid = randCard.uid });

                    //global::GameLogic.ResolveTriggeredCards(randCard, null, TriggerType.PLAYED_CARD);

                    yield return Timing.WaitForSeconds(1);
                }
                else if (randCard.data.type == CardType.Minion)
                {
                    var unoccupiedTiles = GameServer.State.Board.GetUnOccupiedTiles(Team.Opponent);

                    if (unoccupiedTiles.Count <= 0)
                    {
                        EndTurn();
                        break;
                    }

                    var summonedCard = GetSummonCommandForCard(GameServer.OpponentPlayerState.Uid, unoccupiedTiles,
                        randCard, out var summonedTile, out var summonCommand);
                    var summonedTileView = GameplayScreen.Instance.gameBoard.GetTile(summonedTile.Position);

                    totalMana -= summonedCard.GetCost();

                    global::GameLogic.StartCommand(summonCommand);

                    Events.Publish(null, EventType.OnCardPlayed,
                        new OnCardPlayedEventArgs
                            { isClientCard = summonedCard.Team == Team.Client, cardUid = summonedCard.uid });

                    //global::GameLogic.ResolveTriggeredCards(randMinion, null, TriggerType.PLAYED_CARD);

                    yield return Timing.WaitUntilDone(SummonBoardCard(summonedTileView, summonedCard,
                        summonCommand.SummonMethod), CommandResolver.ResolvingTag);
                }

                while (Server.CommandResolver.Executing) yield return Timing.WaitForOneFrame;
                yield return Timing.WaitForSeconds(1);

                if (totalMana <= 0)
                {
                    EndTurn();
                    break;
                }
            }

            while (Server.CommandResolver.Executing) yield return Timing.WaitForOneFrame;
        }

        private static SummonCommand GetSummonCommandForCard(string playerUid, TileState summonOnTile,
            GameCardState minionToSummon, Vector2Int selection, string selectionHeroUid)
        {
            var summonCommand = new SummonCommand
            {
                PlayerUid = playerUid,
                SummonMethod = SummonCommand.Method.PlayedFromHand,
                Position = summonOnTile.Position,
                SourceUid = minionToSummon.uid,
                Selection = selection,
                SelectedHeroUid = selectionHeroUid,
            };

            return summonCommand;
        }

        private static GameCardState GetSummonCommandForCard(string playerUid, List<TileState> unoccupiedTiles,
            GameCardState minionToSummon, out TileState summonedOnTile, out SummonCommand summonCommand)
        {
            var frontTiles = unoccupiedTiles.FindAll(tile => tile.LocalPosition.y == 0);
            var backTiles = unoccupiedTiles.FindAll(tile => tile.LocalPosition.y == 1);

            if (minionToSummon.data.attackType == AttackType.Melee && frontTiles.Count > 0)
            {
                summonedOnTile = frontTiles[Random.Range(0, frontTiles.Count)];
            }
            else if (backTiles.Count > 0)
            {
                summonedOnTile = backTiles[Random.Range(0, backTiles.Count)];
            }
            else
            {
                summonedOnTile = null;
            }

            if (summonedOnTile == null)
            {
                summonCommand = null;
                return null;
            }

            summonCommand = new SummonCommand
            {
                PlayerUid = playerUid,
                SummonMethod = SummonCommand.Method.PlayedFromHand,
                Position = summonedOnTile.Position,
                SourceUid = minionToSummon.uid,
                Selection = Vector2Int.zero,
            };

            return minionToSummon;
        }

        public static PlayCommand GetPlayCardCommand(string playerUid, GameCardState cardToPlay)
        {
            var playCommand = new PlayCommand
            {
                PlayerUid = playerUid,
                SourceUid = cardToPlay.uid,
                HandIndex = GameServer.GetPlayerState(playerUid).GetHandCardIndex(cardToPlay.uid)
            };

            List<string> selections = null;

            if (cardToPlay.data.aiProperties.strategy.targetConditions.Count > 0)
            {
                for (var i = 0; i < cardToPlay.data.aiProperties.strategy.targetConditions.Count; i++)
                {
                    var temp = cardToPlay.GetSelectTargets(null,
                        cardToPlay.data.aiProperties.strategy.targetConditions[i]);

                    if (temp.Count > 0)
                    {
                        selections = temp;
                    }
                }

                if (selections == null || selections.Count <= 0)
                {
                    return null;
                }
            }
            else
            {
                selections = cardToPlay.GetSelectTargets(null);
            }

            if (selections != null && selections.Count > 0)
            {
                playCommand.Selection =
                    GameServer.State.GetCard(selections[new System.Random().Next(0, selections.Count)]);
            }

            return playCommand;
        }

        private static int GetPossibleDamageFromMinions(int mana)
        {
            return 0;
        }

        public static IEnumerator<float> SummonBoardCard(GameTile tile, GameCardState cardState,
            SummonCommand.Method method)
        {
            var getBoardCardPrefabTask = AddressableReferenceLoader.GetAsset("Card_Board", true);
            yield return Timing.WaitUntilDone(getBoardCardPrefabTask.AsCoroutine());

            var settings = new CreateCardFactorySettings
            {
                CardPrefab = getBoardCardPrefabTask.Result,
                CardState = cardState,
                Layout = tile.RectTransform(),
                StartsDisabled = true,
                AdditionalComponents = GameVfxComponent.GetRequiredVfxComponents(cardState)
            };

            var boardCard = CardFactory.Create(settings, null);

            while (!boardCard.AllVfxComponentsLoaded()) yield return Timing.WaitForOneFrame;

            var boardCardGameView = boardCard.GetCardComponent<GameCardView>();

            boardCardGameView.View.GetVfx<AttackEffectComponent, AttackEffect>().SetupDirection(cardState.Team);
            boardCard.SetActive(true);

            yield return Timing.WaitUntilDone(boardCardGameView.AnimateSummonOnTile(tile, cardState,
                SummonMinionData.SummonVisualEffect.FromAbility, method), CommandResolver.ResolvingTag);
            GameplayScreen.Instance.SetBoardTile(boardCardGameView, tile.Position);
        }

        public static void EndTurn()
        {
            var endTurnCommand = new EndTurnCommand
            {
                PlayerUid = GameServer.OpponentPlayerState.Uid,
                Team = Team.Opponent
            };

            global::GameLogic.StartCommand(endTurnCommand);
        }

        private static List<GameCardState> GetValidCards(Team team, int mana)
        {
            var playerState = GameServer.GetPlayerState(team);
            var validCards = new List<GameCardState>();

            if (!string.IsNullOrEmpty(playerState.blitzBurst))
            {
                var blitzBurst = GameServer.State.GetCard(playerState.blitzBurst);

                if (blitzBurst.data.aiProperties.strategy.overrideCost <= mana &&
                    blitzBurst.limitBreakValue >= blitzBurst.data.limitBreak.value)
                {
                    validCards.Add(blitzBurst);
                }
            }

            for (var i = 0; i < playerState.hand.Count; i++)
            {
                var card = GameServer.State.GetCard(playerState.hand[i]);

                if (card.data.aiProperties.strategy.overrideCost > card.GetCost())
                {
                    if (mana >= card.data.aiProperties.strategy.overrideCost)
                    {
                        validCards.Add(card);
                    }
                }
                else
                {
                    if (mana >= card.GetCost())
                    {
                        validCards.Add(card);
                    }
                }
            }

            return validCards;
        }

        private static List<GameCardState> GetOrderedValidCards(Team team, int totalManaWithManaModifyingSpells,
            List<GameCardState> removals)
        {
            var validCards = GetValidCards(team, totalManaWithManaModifyingSpells);
            validCards.RemoveAll(c => removals.Exists(c1 => c1.uid == c.uid));
            validCards = validCards
                .OrderBy(c => c.data.aiProperties.order == AiProperties.Order.First ? 0 : 1)
                .ThenBy(c => c.data.aiProperties.order == AiProperties.Order.AnyOrder ? 0 : 1)
                .ToList();
            return validCards;
        }

        private static List<GameCardState> GetHandCards(Team team)
        {
            var playerState = GameServer.GetPlayerState(team);
            var validCards = new List<GameCardState>();

            for (var i = 0; i < playerState.hand.Count; i++)
            {
                var card = GameServer.State.GetCard(playerState.hand[i]);
                validCards.Add(card);
            }

            return validCards;
        }

        private static List<GameCardState> GetValidMinions(Team team, int mana)
        {
            var validCards = GetValidCards(team, mana);
            return validCards.FindAll(card => card.data.type == CardType.Minion);
        }

        private static List<GameCardState> GetValidMinions(Team team, int mana, AbilityKeyword keyword)
        {
            var validCards = GetValidCards(team, mana);
            return validCards.FindAll(card =>
                card.data.type == CardType.Minion && card.GetAbilities().Exists(ability => ability.keyword == keyword));
        }

        private static List<GameCardState> GetMinionsWithKeyword(Team team, AbilityKeyword keyword)
        {
            var cards = GetHandCards(team);
            return cards.FindAll(card =>
                card.data.type == CardType.Minion && card.GetAbilities().Exists(ability => ability.keyword == keyword));
        }

        private static List<GameCardState> GetValidSpells(Team team, int mana)
        {
            var validCards = GetValidCards(team, mana);
            return validCards.FindAll(x => x.data.type == CardType.Spell);
        }

        private static List<GameCardState> GetValidSpells(Team team, int mana, AbilityKeyword keyword)
        {
            var validSpells = GetValidSpells(team, mana);
            return validSpells.FindAll(card =>
                card.data.type == CardType.Spell && card.GetAbilities().Exists(ability => ability.keyword == keyword));
        }

        private static TileState FindBestPlacement(GameCardState minion, List<TileState> occupiedPlayerTiles)
        {
            // best placement in order
            // minion can be destroyed by this minion this turn; if multiple the one with the highest attack
            // minion with the highest attack
            // win condition

            return null;
        }

        public static string RedeemCard(List<string> cards)
        {
            return cards[Random.Range(0, cards.Count)];
        }

        public static void Dispose()
        {
            Thinking = false;
            Disposed = true;
        }
    }
}