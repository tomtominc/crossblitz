using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Items.Data;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using CrossBlitz.Utils;
using TakoBoyStudios.Events;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.ServerAPI.GameLogic
{
    [System.Serializable]
    public class GameStats : GameEventUpdater
    {
        public int TurnsTaken;
        public int TotalClientHeroDamageTaken; //
        public int TotalOpponentHeroDamageTaken; //
        public int TotalClientMinionDamageTaken; //
        public int TotalOpponentMinionDamageTaken; //
        public int TotalDamageYouDidToYourOwnMinions;
        public int TotalMinionsDestroyed; //
        public int TotalClientMinionsDestroyed; //
        public int TotalOpponentMinionsDestroyed; //
        public int TotalCardsPlayed; //
        public int TotalClientCardsPlayed; //
        public int TotalOpponentCardsPlayed; //
        public int TotalClientCardsPlayedThisTurn; //
        public int TotalOpponentCardsPlayedThisTurn; //
        public int TotalClientMinionCardsPlayed; //
        public int TotalClientSpellCardsPlayed; //
        public int TotalClientTrickCardsPlayed; //
        public int TotalOpponentMinionCardsPlayed; //
        public int TotalOpponentSpellCardsPlayed; //
        public int TotalOpponentTrickCardsPlayed; //
        public int TotalClientTimesOverdrawn;
        public int TotalOpponentTimesOverdrawn;
        public int TotalBattlecryMinionsActivated;
        public int TotalDeathrattleMinionsActivated;
        public int TotalCardsPlayedInASingleTurn;
        public int TotalClientMinionsBuffed; //
        public int TotalOpponentMinionsBuffed; //
        public int TotalClientCardsDrawn; //
        public int TotalOpponentCardsDrawn; //
        public int TotalClientMinionsSummoned; //
        public int TotalOpponentMinionsSummoned;
        public int TrapsTriggered;
        public int TotalMinionsFrozen;//
        public int AdmiralBrassWasSavedOnTurn;
        public List<ItemValue> DestroyedCardsByTurnDestroyed;
        public CardType LastDamageCardType;
        public List<string> DiscardedCards;

        public override void OnTurnStart(IMessage message)
        {
            TotalClientCardsPlayedThisTurn = 0;
            TotalOpponentCardsPlayedThisTurn = 0;
            AdmiralBrassWasSavedOnTurn=-1;
            DestroyedCardsByTurnDestroyed = new List<ItemValue>();
            GameServer.ClientPlayerState.cardsPlayedThisTurn = 0;
            GameServer.OpponentPlayerState.cardsPlayedThisTurn = 0;
            GameServer.ClientPlayerState.spellsCastThisTurn = 0;
            GameServer.OpponentPlayerState.spellsCastThisTurn = 0;
        }

        public override void OnMinionFrozen(IMessage message)
        {
            TotalMinionsFrozen++;
        }

        public override void OnCharacterDamaged(IMessage message)
        {
            if (message.Data is OnCharacterDamagedEventArgs args)
            {
                var cardState = GameServer.State.GetCard(args.characterUid);
                var damagingCard = GameServer.State.GetCard(args.damageSourceUid);

                if (cardState.IsHero)
                {
                    if (args.isClientCharacter)
                    {
                        TotalClientHeroDamageTaken += args.damageAmount;
                    }
                    else
                    {
                        TotalOpponentHeroDamageTaken += args.damageAmount;
                    }

                    LastDamageCardType = damagingCard.characterData.type;
                }
                else
                {
                    if (args.isClientCharacter)
                    {
                        TotalClientMinionDamageTaken += args.damageAmount;
                    }
                    else
                    {
                        TotalOpponentMinionDamageTaken += args.damageAmount;
                    }
                }

                if (cardState.data!=null && cardState.data.type == CardType.Minion && cardState.Team == Team.Client && damagingCard.owner == cardState.owner)
                {
                    TotalDamageYouDidToYourOwnMinions += args.damageAmount;

                    var tile = Db.MapDatabase.GetTileByBattle(GameServer.Mode.BattleUid);

                    if (tile != null)
                    {
                        var clientTileData = App.FableData.GetTile(tile.uid);
                        var battleData = Db.BattleDatabase.GetBattleData(GameServer.Mode.BattleUid);

                        // checking accolades
                        for (var i = 0; i < battleData.Accolades.Count; i++)
                        {
                            var accolade = battleData.Accolades[i];
                            var savedAccolade = clientTileData.BattleData.Accolades[i];

                            if (accolade.Type == AccoladeType.DamageYourOwnMinions)
                            {
                                if (!savedAccolade.IsComplete && !savedAccolade.IsPending && TotalDamageYouDidToYourOwnMinions >= accolade.Score)
                                {
                                    savedAccolade.IsPending = true;
                                    Events.Publish(this, EventType.OnAccoladeComplete, new OnAccoladeCompleteArgs
                                    {
                                        accoladeID = accolade.Uid
                                    });
                                }
                            }
                        }
                    }

                }
            }
        }

        public override void OnCharacterStatsModified(IMessage message)
        {
            if (message.Data is OnCharacterStatsModifiedEventArgs args)
            {
                if (args.isClientCharacter)
                {
                    var character = GameServer.State.GetCard(args.characterUid);
                    if (!character.IsHero) TotalClientMinionsBuffed++;
                }
                else
                {
                    var character = GameServer.State.GetCard(args.characterUid);
                    if (!character.IsHero) TotalOpponentMinionsBuffed++;
                }
            }
        }

        public override void OnTrapTriggered(IMessage message)
        {
            if (message.Data is OnTrapTriggeredEventArgs args)
            {
                TrapsTriggered++;

                if (string.IsNullOrEmpty(GameServer.Mode.BattleUid))
                {
                    return;
                }

                var battleData = Db.BattleDatabase.GetBattleData(GameServer.Mode.BattleUid);

                for (var i = 0; i < battleData.Accolades.Count; i++)
                {
                    var accolade = battleData.Accolades[i];

                    if (accolade.Type == AccoladeType.TriggerTraps && TrapsTriggered >= accolade.Score)
                    {
                        var tile = Db.MapDatabase.GetTileByBattle(GameServer.Mode.BattleUid);
                        var clientTileData = App.FableData.GetTile(tile.uid);
                        var savedAccolade = clientTileData.BattleData.Accolades[i];

                        savedAccolade.IsPending = true;

                        Events.Publish(this, EventType.OnAccoladeComplete, new OnAccoladeCompleteArgs
                        {
                            accoladeID = accolade.Uid
                        });
                    }
                }
            }
        }

        public override void OnMinionDestroyed(IMessage message)
        {
            if (message.Data is OnMinionDestroyedEventArgs args)
            {
                if (args.isClientCard)
                {
                    TotalClientMinionsDestroyed++;
                }
                else
                {
                    TotalOpponentMinionsDestroyed++;
                }

                TotalMinionsDestroyed++;

                DestroyedCardsByTurnDestroyed.Add(new ItemValue
                {
                    ItemUid = args.cardUid,
                    Count = GameServer.State.GetTurnCount(Team.Client)
                });

                var tile = Db.MapDatabase.GetTileByBattle(GameServer.Mode.BattleUid);

                if (tile != null)
                {
                    var clientTileData = App.FableData.GetTile(tile.uid);
                    var battleData = Db.BattleDatabase.GetBattleData(GameServer.Mode.BattleUid);

                    // checking accolades
                    for (var i = 0; i < battleData.Accolades.Count; i++)
                    {
                        var accolade = battleData.Accolades[i];
                        var savedAccolade = clientTileData.BattleData.Accolades[i];

                        if (savedAccolade.IsComplete || savedAccolade.IsPending) continue;

                        switch (accolade.Type)
                        {
                            case AccoladeType.DestroyedOpponentMinions:
                            {
                                if (TotalOpponentMinionsDestroyed >= accolade.Score)
                                {
                                    savedAccolade.IsPending = true;
                                    Events.Publish(this, EventType.OnAccoladeComplete, new OnAccoladeCompleteArgs
                                    {
                                        accoladeID = accolade.Uid
                                    });
                                }

                                break;
                            }
                            case AccoladeType.DestroySpecificMinionInASpecificNumberOfTurns:
                            {
                                for (var j = 0; j < DestroyedCardsByTurnDestroyed.Count; j++)
                                {
                                    var dp = DestroyedCardsByTurnDestroyed[j];
                                    var card = GameServer.State.GetCard(dp.ItemUid);

                                    if (card?.data != null && card.data.id == accolade.CardId && dp.Count <= accolade.Score)
                                    {
                                        savedAccolade.IsPending = true;
                                        Events.Publish(this, EventType.OnAccoladeComplete, new OnAccoladeCompleteArgs
                                        {
                                            accoladeID = accolade.Uid
                                        });
                                    }
                                }

                                break;
                            }
                        }
                    }
                }
            }
        }

        public override void OnCardPlayed(IMessage message)
        {
            if (message.Data is OnCardPlayedEventArgs args)
            {
                var card = GameServer.State.GetCard(args.cardUid);

                //  Debug.Log($"Card played! {card.data.name}");

                if (card.Team == Team.Client)
                {
                    TotalClientCardsPlayed++;
                    TotalClientCardsPlayedThisTurn++;
                    GameServer.ClientPlayerState.cardsPlayedThisTurn++;
                    if (card.data.type == CardType.Minion) TotalClientMinionCardsPlayed++;
                    else if (card.data.type == CardType.Spell)
                    {
                        GameServer.ClientPlayerState.spellsCastThisTurn++;
                        TotalClientSpellCardsPlayed++;
                    }
                    else if (card.data.type == CardType.Trick) TotalClientTrickCardsPlayed++;

                    var tile = Db.MapDatabase.GetTileByBattle(GameServer.Mode.BattleUid);

                    if (tile != null)
                    {
                        var clientTileData = App.FableData.GetTile(tile.uid);
                        var battleData = Db.BattleDatabase.GetBattleData(GameServer.Mode.BattleUid);

                        // checking accolades
                        for (var i = 0; i < battleData.Accolades.Count; i++)
                        {
                            var accolade = battleData.Accolades[i];
                            var savedAccolade = clientTileData.BattleData.Accolades[i];

                            if (accolade.Type == AccoladeType.PlayMinions)
                            {
                                if (!savedAccolade.IsComplete && !savedAccolade.IsPending && TotalClientMinionCardsPlayed >= accolade.Score)
                                {
                                    savedAccolade.IsPending = true;
                                    Events.Publish(this, EventType.OnAccoladeComplete, new OnAccoladeCompleteArgs
                                    {
                                        accoladeID = accolade.Uid
                                    });
                                }
                            }

                            if (accolade.Type == AccoladeType.PlaySpells)
                            {
                                if (!savedAccolade.IsComplete && !savedAccolade.IsPending && TotalClientSpellCardsPlayed >= accolade.Score)
                                {
                                    savedAccolade.IsPending = true;
                                    Events.Publish(this, EventType.OnAccoladeComplete, new OnAccoladeCompleteArgs
                                    {
                                        accoladeID = accolade.Uid
                                    });
                                }
                            }
                        }
                    }
                }
                else
                {
                    TotalOpponentCardsPlayed++;
                    TotalOpponentCardsPlayedThisTurn++;
                    GameServer.OpponentPlayerState.cardsPlayedThisTurn++;
                    if (card.data.type == CardType.Minion) TotalOpponentMinionCardsPlayed++;
                    else if (card.data.type == CardType.Spell)
                    {
                        GameServer.OpponentPlayerState.spellsCastThisTurn++;
                        TotalOpponentSpellCardsPlayed++;
                    }
                    else if (card.data.type == CardType.Trick) TotalOpponentTrickCardsPlayed++;
                }

                TotalCardsPlayed++;
            }
        }

        public override void OnCardDrawn(IMessage message)
        {
            if (message.Data is OnCardDrawnEventArgs args)
            {
                if (args.team == Team.Client)
                {
                    TotalClientCardsDrawn++;
                }
                else
                {
                    TotalOpponentCardsDrawn++;
                }
            }
        }

        public override void OnMinionSummoned(IMessage message)
        {
            if (message.Data is OnMinionSummonedEventArgs args)
            {
                if (args.isClientCard)
                {
                    TotalClientMinionsSummoned++;
                }
                else
                {
                    TotalOpponentMinionsSummoned++;
                }
            }
        }

        public override void OnMatchWon(IMessage message)
        {
            AI.Dispose();
            Server.CommandResolver.Dispose();

            App.BattleResults = new BattleResults();
            App.BattleResults.SetInitialized(true);
            App.BattleResults.SetMatchResult(BattleResults.Result.Won);

            Events.Publish(this, EventType.OnMatchDisposed, null);
        }

        public override void OnMatchLost(IMessage message)
        {
            AI.Dispose();
            Server.CommandResolver.Dispose();

            App.BattleResults = new BattleResults();
            App.BattleResults.SetInitialized(true);
            App.BattleResults.SetMatchResult(BattleResults.Result.Lost);

            Events.Publish(this, EventType.OnMatchDisposed, null);
        }

        public override void OnMatchDraw(IMessage message)
        {
            AI.Dispose();
            Server.CommandResolver.Dispose();

            App.BattleResults = new BattleResults();
            App.BattleResults.SetInitialized(true);
            App.BattleResults.SetMatchResult(BattleResults.Result.Draw);

            Events.Publish(this, EventType.OnMatchDisposed, null);
        }

        public override void OnMatchDisposed(IMessage message)
        {
            TurnsTaken = GameServer.State.GetTurnCount(Team.Client);

            var deck = App.PlayerDecks.GetDeckBasedOnGameState();
            var hero = App.HeroData.GetHero(deck.hero);

            if (App.BattleResults == null)
            {
                App.BattleResults = new BattleResults();
                App.BattleResults.SetInitialized(true);
                App.BattleResults.SetMatchResult(BattleResults.Result.Draw);
            }

            App.BattleResults.SetLastGameMode(new GameMode
            {
                GameType = GameServer.Mode.GameType,
                ConnectionMode = GameServer.Mode.ConnectionMode,
                SceneEnvironment = GameServer.Mode.SceneEnvironment,
                SceneToReturnTo = GameServer.Mode.SceneToReturnTo,
                ShowResultsAfterMatch = GameServer.Mode.ShowResultsAfterMatch,
                BattleUid = GameServer.Mode.BattleUid,
                DeckUid = GameServer.Mode.DeckUid,
            });

            App.BattleResults.SetPendingMapVisuals(true);
            App.BattleResults.SetDeckId(deck.uid);
            App.BattleResults.SetStats(GameUtilities.Copy(this));

            App.BattleResults.SetHeroLevelBeforeBattleEnd(hero.level);
            App.BattleResults.SetExperienceBeforeBattleEnd(hero.xp);
            App.BattleResults.SetBattleUid(GameServer.Mode.BattleUid);

            var tile = Db.MapDatabase.GetTileByBattle(GameServer.Mode.BattleUid);
            var clientTileData = App.FableData.GetTile(tile.uid);
            var battleData = Db.BattleDatabase.GetBattleData(GameServer.Mode.BattleUid);

            var chapter = Db.FablesDatabase.GetChapterByBattle(GameServer.Mode.BattleUid);
            var clientChapterData = App.FableData.GetChapter(chapter.Uid);
            var clientBookData = App.FableData.GetBook(chapter.ChapterHero, chapter.BookNumber);

            clientTileData.BattleData ??= new ClientBattleData();
            clientTileData.BattleData.FixDependencies(battleData);

            App.BattleResults.SetCompletedAccoladeIds(new List<string>());
            App.BattleResults.SetCardsRewarded(new List<string>());
            App.BattleResults.SetChestsObtained(new List<ItemValue>());
            App.BattleResults.SetIngredientsEarned(new List<ItemValue>());

            var completedAccoladeIds = App.BattleResults.GetCompletedAccoladeIds();
            var matchResult = App.BattleResults.GetMatchResult();

            for (var i = 0; i < battleData.Accolades.Count; i++)
            {
                var accolade = battleData.Accolades[i];
                var savedAccolade = clientTileData.BattleData.Accolades[i];

                if (savedAccolade.IsComplete)
                {
                    continue;
                }

                switch (accolade.Type)
                {
                    case AccoladeType.Win:

                        if (matchResult == BattleResults.Result.Won)
                        {
                            savedAccolade.IsComplete = true;
                            completedAccoladeIds.Add(accolade.Uid);
                        }
                        break;

                    case AccoladeType.PlayMinions:

                        if (savedAccolade.IsPending)
                        {
                            savedAccolade.IsComplete = true;
                            completedAccoladeIds.Add(accolade.Uid);
                        }
                        break;


                    case AccoladeType.PlaySpells:
                        if (savedAccolade.IsPending)
                        {
                            savedAccolade.IsComplete = true;
                            completedAccoladeIds.Add(accolade.Uid);
                        }
                        break;


                    case AccoladeType.UnderTurns:
                        if (matchResult == BattleResults.Result.Won)
                        {
                            if (GameServer.State.GetTurnCount(Team.Client) < accolade.Score)
                            {
                                savedAccolade.IsComplete = true;
                                completedAccoladeIds.Add(accolade.Uid);
                            }
                        }
                        break;

                    case AccoladeType.OverTurns:
                        if (matchResult == BattleResults.Result.Won)
                        {
                            if (GameServer.State.GetTurnCount(Team.Client) > accolade.Score)
                            {
                                savedAccolade.IsComplete = true;
                                completedAccoladeIds.Add(accolade.Uid);
                            }
                        }
                        break;

                    case AccoladeType.BeatWithSpellDamage:
                        if (matchResult == BattleResults.Result.Won)
                        {
                            if (LastDamageCardType == CardType.Spell)
                            {
                                savedAccolade.IsComplete = true;
                                completedAccoladeIds.Add(accolade.Uid);
                            }
                        }
                        break;
                    case AccoladeType.WinUsingBlitzBurst:
                        if (matchResult == BattleResults.Result.Won)
                        {
                            if (LastDamageCardType == CardType.Power)
                            {
                                savedAccolade.IsComplete = true;
                                completedAccoladeIds.Add(accolade.Uid);
                            }
                        }
                        break;

                    case AccoladeType.BeatWithMinionDamage:
                        if (matchResult == BattleResults.Result.Won)
                        {
                            if (LastDamageCardType == CardType.Minion)
                            {
                                savedAccolade.IsComplete = true;
                                completedAccoladeIds.Add(accolade.Uid);
                            }
                        }
                        break;

                    case AccoladeType.DestroyedOpponentMinions:
                        if (savedAccolade.IsPending)
                        {
                            savedAccolade.IsComplete = true;
                            completedAccoladeIds.Add(accolade.Uid);
                        }
                        break;

                    case AccoladeType.TakeLessThanXDamage:
                        if (matchResult == BattleResults.Result.Won)
                        {
                            if (TotalClientHeroDamageTaken < accolade.Score)
                            {
                                savedAccolade.IsComplete = true;
                                completedAccoladeIds.Add(accolade.Uid);
                            }
                        }
                        break;

                    case AccoladeType.DamageYourOwnMinions:
                        if (savedAccolade.IsPending)
                        {
                            savedAccolade.IsComplete = true;
                            completedAccoladeIds.Add(accolade.Uid);
                        }
                        break;

                    case AccoladeType.WinWithAtLeastXNumberOfCardCopiesInYourDeck:
                        if (matchResult == BattleResults.Result.Won)
                        {
                            var cards = GameServer.ClientPlayerState.deck.FindAll(c => c == accolade.CardId);
                            if (cards.Count >= accolade.Score)
                            {
                                savedAccolade.IsComplete = true;
                                completedAccoladeIds.Add(accolade.Uid);
                            }
                        }
                        break;

                    case AccoladeType.DealDamageToEnemiesThroughEffects:
                        break;

                    case AccoladeType.GetAdmiralBrassBack:
                        if (matchResult == BattleResults.Result.Won)
                        {
                            if (AdmiralBrassWasSavedOnTurn <= accolade.Score)
                            {
                                savedAccolade.IsComplete = true;
                                completedAccoladeIds.Add(accolade.Uid);
                            }
                        }
                        break;
                    case AccoladeType.DestroySpecificMinionInASpecificNumberOfTurns:
                        if (savedAccolade.IsPending)
                        {
                            savedAccolade.IsComplete = true;
                            completedAccoladeIds.Add(accolade.Uid);
                        }
                        break;
                }
            }

            for (var i = 0; i < completedAccoladeIds.Count; i++)
            {
                var accolade = battleData.Accolades.Find(a => a.Uid == completedAccoladeIds[i]);
                App.BattleResults.SetDawnDollarsAcquired(App.BattleResults.GetDawnDollarsAcquired() + accolade.DawnDollarReward);
            }

            if (App.BattleResults.GetMatchResult() == BattleResults.Result.Won)
            {
                App.BattleResults.SetIngredientsEarned(battleData.GetIngredientsFromWin(deck.faction, !clientTileData.BattleData.IsComplete));
                App.BattleResults.SetCardsRewarded(battleData.GetCardRewardsForWin(deck.faction, chapter.Uid, !clientTileData.BattleData.IsComplete));
                App.BattleResults.SetManaShardsAcquired(battleData.GetManaShardsFromWin(!clientTileData.BattleData.IsComplete));

                if (App.Settings.GetNoBattleRewards())
                {
                    Debug.LogError("Battle rewards off...");
                    completedAccoladeIds.Clear();
                    App.BattleResults.GetCardsRewarded().Clear();
                    App.BattleResults.GetChestsObtained().Clear();
                    App.BattleResults.GetIngredientsEarned().Clear();
                    App.BattleResults.SetExperienceGained(0);
                    App.BattleResults.SetDawnDollarsAcquired(0);
                }

                if (hero.level == 1 && hero.xp == 0)
                {
                    App.BattleResults.SetExperienceGained(7);
                }
                if(hero.level == 30)
                {
                    App.BattleResults.SetExperienceGained(0);
                }
                else
                {
                    App.BattleResults.SetExperienceGained(Xp.GetXpGainedFromEnemy(battleData.BattleType,
                        battleData.Level, !clientTileData.BattleData.IsComplete, chapter.ChapterNumber));
                }

                clientTileData.BattleData.SetCompleteStatus(true);
                clientChapterData.SetTileAsPendingComplete(clientTileData.TileUid);

                if (App.BattleResults.GetExperienceGained() > 0)
                {
                    App.HeroData.IncreaseXp(hero.id, App.BattleResults.GetExperienceGained());

                    hero = App.HeroData.GetHero(hero.id);

                    App.BattleResults.SetHeroLevelsGained(Xp.GetGainedLevelsFromXp(hero.level, hero.xp, out var xpLeft));

                    App.HeroData.SetXp(hero.id, xpLeft);

                    HeroDatabase.ModifyHeroLevel(hero.id, hero.level + App.BattleResults.GetHeroLevelsGained());
                }

                if (App.BattleResults.GetManaShardsAcquired() > 0)
                {
                    App.Inventory.AddCurrency(Currency.MANA_SHARDS, App.BattleResults.GetManaShardsAcquired());
                }
            }

            clientBookData.AddDawnDollars(App.BattleResults.GetDawnDollarsAcquired());

            GameServer.ChangeGameState(StateDefinition.GAME_FINISHED);
        }
    }
}