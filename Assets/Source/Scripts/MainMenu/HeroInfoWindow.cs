using System;
using System.Linq;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables;
using CrossBlitz.Fables.Grid;
using CrossBlitz.Hero;
using CrossBlitz.InputAPI;
using CrossBlitz.LevelGrid;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.UI;
using CrossBlitz.Utils;
using CrossBlitz.ViewAPI;
using DG.Tweening;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.MainMenu
{
    public class HeroInfoWindow : ToolTipBehaviour
    {
        public TextMeshProUGUI nameLabel;
        public TextMeshProUGUI subTitleLabel;
        public SpriteAnimation factionIcon;
        public TextMeshProUGUI factionLabel;
        public SpriteAnimation levelIcon;
        public TextMeshProUGUI levelLabel;
        public TMPSpriteFont hpLabel;
        public RectTransform levelContainer;
        public TextMeshProUGUI xpLabel;
        public HeroMainMenuSlot heroSlot;
        public Image xpBar;
        public AnimatedSlider xpBarSlider;
        public Button levelTreeButton;
        public float levelLabelOffsetX = -56f;
        public event Action OnLevelButtonPressed;

        public MainMenuController mainMenuController;

        private string _heroId;

        private void Start()
        {
            if (levelTreeButton)
            {
                levelTreeButton.onClick.AddListener(OnLevelTreeButtonPressed);
            }

            Events.Subscribe(EventType.OnHeroLevelChanged, OnHeroLevelChanged);
            Events.Subscribe(EventType.OnHeroVisuallyLeveledUp, OnHeroVisuallyLeveledUp);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnHeroLevelChanged, OnHeroLevelChanged);
            Events.Unsubscribe(EventType.OnHeroVisuallyLeveledUp, OnHeroVisuallyLeveledUp);
        }

        public override void SetCustomData(ToolTipCustomData customData)
        {
            _heroId = customData.data;

            var heroData = Db.HeroDatabase.GetHero(_heroId);

            SetHeroData(heroData);

        }

        public void SetHeroData(HeroData heroData)
        {
            if (heroData == null) return;

            _heroId = heroData.id;

            if (nameLabel) nameLabel.text = heroData.name.ToUpper();
            if (subTitleLabel) subTitleLabel.text = heroData.title.ToUpper();
            if (factionIcon) factionIcon.Play(heroData.faction.ToString().ToLower());
            if (factionLabel) factionLabel.text = heroData.faction.ToString().ToUpper();

            RefreshLevelData();
        }

        public void EraseHeroData()
        {
            // not sure we need this?
        }

        public void RefreshLevelData()
        {
            var heroData = App.HeroData.GetHero(_heroId);

            if (heroData == null)
            {
                Debug.LogError($"Can't find hero data {_heroId}");
                return;
            }

            if (levelIcon)
            {
                levelIcon.Play(heroData.GetData().faction.ToString().ToLower());
            }

            if (levelLabel)
            {
                levelLabel.text = heroData.level.ToString();
            }

            if (xpLabel)
            {
                if(heroData.level == 30)
                {
                    xpLabel.text = "MAX LEVEL!";
                }
                else
                {
                    xpLabel.text = $"{heroData.xp}/{Xp.GetXpForLevel(heroData.level + 1)}";
                }
            }

            if (xpBar)
            {
                if (heroData.level == 30)
                {
                    xpBar.fillAmount = 1;
                }
                else
                {
                    xpBar.fillAmount = heroData.xp / (float)Xp.GetXpForLevel(heroData.level + 1);
                }
            }

            if(xpBarSlider)
            {
                xpBarSlider.SetValues(heroData.xp, 0, Xp.GetXpForLevel(heroData.level + 1));
            }

            if (heroSlot)
            {
                heroSlot.SetHeroId(heroData.id);
            }

            if (hpLabel)
            {
                hpLabel.text = heroData.health.ToString();
            }

            if (levelTreeButton)
            {
                if (heroData.level <= 1)
                {
                    levelTreeButton.SetActive(false);
                }
                else
                {
                    levelTreeButton.SetActive(true);
                    var notify = heroData.levelPoints > 0;
                    levelTreeButton.GetComponent<ButtonContainer>().Notify(notify);
                }
            }

            if (levelLabel)
            {
                var offset = levelLabel.text.Count(c => c.Equals('1'));

                if (heroData.level < 10)
                {
                    levelContainer.anchoredPosition = new Vector2(4 + offset + levelLabelOffsetX, levelContainer.anchoredPosition.y);
                }
                else if (heroData.level < 100)
                {
                    levelContainer.anchoredPosition = new Vector2(0 + offset+ levelLabelOffsetX, levelContainer.anchoredPosition.y);
                }
                else
                {
                    levelContainer.anchoredPosition = new Vector2(-3 + offset+ levelLabelOffsetX, levelContainer.anchoredPosition.y);
                }
            }
        }

        private void OnLevelTreeButtonPressed()
        {
            if(mainMenuController != null) mainMenuController._canEscape = false;

            AudioController.SetMusicParameter("map_cutscene", 1);

            ToolTipHoverImage.HideAll();
            LevelTreeController.OnClosed -= OnLevelTreeClosed;
            LevelTreeController.OnClosed += OnLevelTreeClosed;
            LevelTreeController.Open(_heroId);
            OnLevelButtonPressed?.Invoke();
        }

        private void OnLevelTreeClosed()
        {
            RefreshLevelData();
            if (mainMenuController != null) mainMenuController._canEscape = true;
        }

        private void OnHeroLevelChanged(IMessage message)
        {
            var eventArgs = (OnHeroLevelChangedEventArgs) message.Data;


            if (eventArgs.heroId == _heroId)
            {
                RefreshLevelData();
            }

        }

        private void OnHeroVisuallyLeveledUp(IMessage message)
        {
            var eventArgs = (OnHeroVisuallyLeveledUpEventArgs) message.Data;

            if (eventArgs.heroId == _heroId)
            {
                RefreshLevelData();
            }
        }
    }
}