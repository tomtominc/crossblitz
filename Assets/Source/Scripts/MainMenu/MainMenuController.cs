using System.Collections.Generic;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.Settings;
using CrossBlitz.ViewAPI;
using CrossBlitz.ViewAPI.Popups;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.MainMenu
{
    public class MainMenuController : MonoBehaviour
    {
        public ButtonContainer playButton;
        public ButtonContainer collectionButton;
        public ButtonContainer settingsButton;
        public ButtonContainer backButton;

        [BoxGroup("Animation")] public RectTransform menuHeaderRect;
        [BoxGroup("Animation")] public SpriteAnimation menuHeader;
        [BoxGroup("Animation")] public RectTransform cardsButtonRect;
        [BoxGroup("Animation")] public RectTransform cardsButtonBackDropRect;
        [BoxGroup("Animation")] public RectTransform questsButtonRect;
        [BoxGroup("Animation")] public List<RectTransform> topButtons;
        [BoxGroup("Animation/Game Mode")] public RectTransform gameModeBg;
        [BoxGroup("Animation/Game Mode")] public CanvasGroup gameModeBgShadow;
        [BoxGroup("Animation/Game Mode")] public CanvasGroup gameModeOverlay;
        [BoxGroup("Animation/Game Mode")] public RectTransform selectGameModeHeaderRect;
        [BoxGroup("Animation/Game Mode")] public RectTransform gameModeInstructionsRect;
        [BoxGroup("Animation/Tako Boy")] public RectTransform takoBoyAccountRect;
        [BoxGroup("Animation/Hero Slots")] public CanvasGroup heroSlotsCanvas;
        [BoxGroup("Animation/Hero Slots")] public List<HeroMainMenuSlot> heroMainMenuSlots;
        [BoxGroup("Animation/Play")] public RectTransform playButtonRect;

        [BoxGroup("Animation/Timing")] public float waitTime = .5f;
        [BoxGroup("Animation/Timing")]public float gameModeBgSizeDuration = 1;
        [BoxGroup("Animation/Timing")]public float wait1 = 0.65f;
        [BoxGroup("Animation/Timing")]public float headerStartY=40;
        [BoxGroup("Animation/Timing")]public float headerInDur = .6f;
        [BoxGroup("Animation/Timing")]public float wait2 = .24f;
        [BoxGroup("Animation/Timing")]public float startRot = -80;
        [BoxGroup("Animation/Timing")]public float rotateToDur = .3f;
        [BoxGroup("Animation/Timing")]public Ease bgOpenEase;
        [BoxGroup("Animation/Timing")]public Ease headerEase;
        [BoxGroup("Animation/Timing")]public Ease instructionEase;

        [BoxGroup("Animation/Sfx")] public string gameModeRevealSfx = "HeroAbility-Flash";
        [BoxGroup("Animation/Sfx")] public string buttonMoveInSfx = "Hero_Appear-Whoosh";
        [BoxGroup("Animation/Sfx")] public string revealHeader = "UI-Battle_RevealCard";
        [BoxGroup("Animation/Sfx")] public string removeHeader = "UI-ButtonFlip";
        [BoxGroup("Animation/Sfx")] public string revealHero = "Battle_PhysAttack01";

        private bool _isMovingToOtherMenu;
        public bool _canEscape;

        private void Start()
        {
            playButton.SetInteractable(true);
            playButton.SetAlpha(1);
            playButton.Button.onClick.AddListener(OnPlayButton);
            collectionButton.Button.onClick.AddListener(OnCollectionClicked);
            settingsButton.SetInteractable(true);
            settingsButton.SetAlpha(1);
            settingsButton.Button.onClick.AddListener(OnSettingsButton);

            backButton.SetInteractable(true);
            backButton.SetAlpha(1);
            backButton.Button.onClick.AddListener(OnBackButtonPressed);

            Timing.RunCoroutine( MoveToMainMenu().CancelWith(gameObject) );
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (backButton.IsActive() && backButton.Interactable)
                {
                    if (!PopupController.IsOpen && _canEscape && !SceneManager.GetSceneByName("OptionsWindow").IsValid()) OnBackButtonPressed();
                    return;
                }
            }
        }

        [Button]
        private void MoveToMainMenuButton()
        {
            Timing.RunCoroutine( MoveToMainMenu() );
        }

        private void OnPlayButton()
        {
            AudioController.PlaySound("UI-ColiseumStart-NoFX");
            AudioController.StopSong();

            playButton.SetInteractable(false);

            Events.Publish(this, EventType.ChangeState, new StateChangeEventArgs
            {
                state = StateDefinition.FABLES_CHAPTER_SELECT
            });
        }

        private async void OnSettingsButton()
        {
            //playButton.SetInteractable(false);

            if (_isMovingToOtherMenu) return;

            _isMovingToOtherMenu = true;

            if (SceneManager.GetSceneByName("OptionsWindow").isLoaded) return;

            var sceneModel = await SceneController.Instance.LoadScene(new SceneLoadParams
            {
                SceneToLoad = "OptionsWindow"
            });

            var settingsWindow = sceneModel.GetView<SettingsWindow>("SettingsWindow");
            settingsWindow.Initialize(SettingsContext.Default);

            _isMovingToOtherMenu = false;
        }


        private void OnBackButtonPressed()
        {
            AudioController.PlaySound("TMP-UI-SelectLockedItem");
            AudioController.StopSong();

            backButton.SetInteractable(false);

            Events.Publish(this, EventType.ChangeState, new StateChangeEventArgs
            {
                state = StateDefinition.INITIALIZE
            });
        }

        private async void OnCollectionClicked()
        {
            if (_isMovingToOtherMenu) return;

            _isMovingToOtherMenu = true;
            _canEscape = false;

            if (SceneManager.GetSceneByName("Collection").isLoaded)
            {
                Debug.LogError("Collection is already loaded!!");
                return;
            }

            Timing.RunCoroutine(MoveToOtherMenu("cards").CancelWith(gameObject));

            Timing.RunCoroutine(WaitForCollection().CancelWith(gameObject));
        }

        private IEnumerator<float> WaitForCollection()
        {
            yield return Timing.WaitForSeconds(0.75f);

            LoadCollection();

            _isMovingToOtherMenu = false;
        }

        private async void LoadCollection()
        {
            var sceneModel = await SceneController.Instance.LoadScene(new SceneLoadParams
            {
                SceneToLoad = "Collection"
            });

            var collectionManager = sceneModel.GetView<DeckEditMenu>("CollectionManager");

            collectionManager.OnCollectionClosed += OnCollectionManagerClosed;
            collectionManager.OpenCollectionManagerWithSettings(new DeckEditSettings
            {
                Mode = DeckEditMode.Edit,
                OpenMode = DeckOpenMode.Collection,
                EquipContext = DeckEquipContext.Pvp,
                FilterCardSettings = new FilterCardSettings
                { ownedCardsOnly = true, faction = Faction.All, @class = Class.All, rarity = Rarity.All, typeFilter = TypeFilter.All, relicFilter = RelicFilter.All, cardBackFilter = CardBackFilter.All },
            });
        }

        private void OnCollectionManagerClosed()
        {
            Timing.RunCoroutine(MoveToMainMenu(false).CancelWith(gameObject));
        }

        // animate another menu in, removing various things from the main menu
        private IEnumerator<float> MoveToOtherMenu(string menuHeaderName)
        {
            for (var i = 0; i < topButtons.Count; i++)
            {
                topButtons[i].DOAnchorPosY(  32, 0.24f).SetEase(Ease.InBack);
            }

            selectGameModeHeaderRect.DOAnchorPosY(headerStartY, 0.12f).SetEase(Ease.InBack);
            gameModeInstructionsRect.DOLocalRotate(new Vector3(startRot, 0, 0), 0.24f).SetEase(Ease.InBack);

            heroSlotsCanvas.DOFade(0, 0.32f);

            Timing.RunCoroutine(AnimateNewHeaderIn(menuHeaderName).CancelWith(gameObject));

            playButtonRect.DOAnchorPosY(-58f, 0.24f).SetEase(Ease.OutBack);
            takoBoyAccountRect.DOAnchorPosX(140, 0.24f).SetEase(Ease.OutBack);

            yield return Timing.WaitForSeconds(0.24f);

            gameModeInstructionsRect.SetActive(false);
            selectGameModeHeaderRect.SetActive(false);

            cardsButtonRect.DOAnchorPosX(-400, 0.24f).SetEase(Ease.InOutQuint).SetDelay(0.025f);
            cardsButtonBackDropRect.DOAnchorPosX(-400, 0.24f).SetEase(Ease.InOutQuint);
            questsButtonRect.DOAnchorPosX(400, 0.24f).SetEase(Ease.OutBack);

            gameModeBg.DOSizeDelta(new Vector2(gameModeBg.sizeDelta.x, 26f), 0.4f).SetEase(Ease.InBack);
            gameModeBgShadow.DOFade(0, 0.24f);

        }

        // main menu animations
         private IEnumerator<float> MoveToMainMenu(bool reset = true)
        {
            // setup

            gameModeBg.sizeDelta = new Vector2(gameModeBg.sizeDelta.x, 26f);

            gameModeBgShadow.alpha = 0;

            gameModeOverlay.SetActive(true);
            gameModeOverlay.alpha = 1;

            selectGameModeHeaderRect.anchoredPosition = new Vector2(0, headerStartY);
            selectGameModeHeaderRect.SetActive(false);

            gameModeInstructionsRect.localEulerAngles = new Vector3(startRot, 0, 0);
            gameModeInstructionsRect.SetActive(false);

            cardsButtonRect.anchoredPosition = new Vector2(-400, cardsButtonRect.anchoredPosition.y);
            cardsButtonBackDropRect.anchoredPosition = new Vector2(-400, cardsButtonBackDropRect.anchoredPosition.y);
            questsButtonRect.anchoredPosition = new Vector2(400, questsButtonRect.anchoredPosition.y);

            heroSlotsCanvas.alpha = 0;

            takoBoyAccountRect.anchoredPosition = new Vector2(140, takoBoyAccountRect.anchoredPosition.y);

            playButtonRect.SetActive(false);
            playButtonRect.anchoredPosition = new Vector2(0, -58f);
            playButtonRect.localEulerAngles = new Vector3(80, 0, 0);

            if (reset)
            {
                menuHeaderRect.anchoredPosition = new Vector2(0, 36);
            }

            for (var i = 0; i < topButtons.Count; i++)
            {
                topButtons[i].anchoredPosition = new Vector2(topButtons[i].anchoredPosition.x, 32);
            }

            AudioController.PlaySong("main-menu");

            yield return Timing.WaitForSeconds(waitTime);

            // animation
            AudioController.PlaySound(gameModeRevealSfx);
            gameModeBg.DOSizeDelta(new Vector2(gameModeBg.sizeDelta.x, 130f), gameModeBgSizeDuration).SetEase(bgOpenEase);

            yield return Timing.WaitForSeconds(wait1);

            Timing.RunCoroutine(AnimateNewHeaderIn("main-menu").CancelWith(gameObject));

            gameModeOverlay.DOFade(0, 0.24f);

            selectGameModeHeaderRect.SetActive(true);
            selectGameModeHeaderRect.DOAnchorPosY(80f, headerInDur).SetEase(headerEase);

            yield return Timing.WaitForSeconds(wait2);

            AudioController.PlaySound(buttonMoveInSfx);

            cardsButtonRect.DOAnchorPosX(-275f, 0.24f).SetEase(Ease.InOutQuint).SetDelay(0.025f);
            cardsButtonBackDropRect.DOAnchorPosX(-275f, 0.24f).SetEase(Ease.InOutQuint);
            questsButtonRect.DOAnchorPosX(255f, 0.24f).SetEase(Ease.OutBack);

            gameModeInstructionsRect.SetActive(true);
            gameModeInstructionsRect.DOLocalRotate(Vector3.zero, rotateToDur).SetEase(instructionEase);

            Timing.RunCoroutine(AnimateHeroSlotsIn().CancelWith(gameObject));

            for (var i = 0; i < topButtons.Count; i++)
            {
                topButtons[i].DOAnchorPosY(-1, 0.24f).SetEase(Ease.OutBack)
                    .SetDelay(Random.Range(0.04f, 0.12f));
            }

            yield return Timing.WaitForSeconds(0.1f);


            AudioController.PlaySound(buttonMoveInSfx);
           // MasterAudio.PlaySound(buttonMoveInSfx);

            playButtonRect.SetActive(true);
            playButtonRect.anchoredPosition = new Vector2(0, 0f);
            playButtonRect.DOLocalRotate(Vector3.zero, 0.3f).SetEase(Ease.OutBack);

            gameModeBgShadow.DOFade(1, 0.24f);
            takoBoyAccountRect.DOAnchorPosX(0, 0.24f);

            _canEscape = true;
        }

        private IEnumerator<float> AnimateNewHeaderIn(string newMenu)
        {
            menuHeaderRect.DOKill(true);

            if (newMenu.Equals("main-menu"))
            {
               // MasterAudio.PlaySound(revealHeader);
                AudioController.PlaySound(revealHeader);

                menuHeader.Play(newMenu);
                menuHeaderRect.localEulerAngles = new Vector3(-90, 0, 0);

                menuHeaderRect.DOLocalRotate(Vector3.zero, 1.7f).SetEase(Ease.OutElastic);
                menuHeaderRect.DOAnchorPosY(0, 0.24f).SetEase(Ease.OutQuart);
            }
            else
            {
                yield return Timing.WaitUntilDone(AnimateHeaderOut().CancelWith(gameObject));
            }
        }

        private IEnumerator<float> AnimateHeaderOut()
        {
            //MasterAudio.PlaySound(removeHeader);
            AudioController.PlaySound(removeHeader);
            menuHeaderRect.DOAnchorPosY(36f, 0.24f).SetEase(Ease.OutBack);
            yield return Timing.WaitForSeconds(0.24f);
        }

        private IEnumerator<float> AnimateHeroSlotsIn()
        {
            for (var i = 0; i < heroMainMenuSlots.Count; i++)
            {
                heroMainMenuSlots[i].SetActive(false);
            }

            heroSlotsCanvas.DOFade(1, 0.24f);

            yield return Timing.WaitForSeconds(0.12f);

            for (var i = 0; i < heroMainMenuSlots.Count; i++)
            {
                heroMainMenuSlots[i].SetActive(true);
                //MasterAudio.PlaySound(revealHero);
                Timing.RunCoroutine(heroMainMenuSlots[i].PlayLevelUpAnimation().CancelWith(gameObject));
                yield return Timing.WaitForSeconds(0.24f);
            }
        }



    }
}