using System;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Hero;
using CrossBlitz.LevelGrid;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.UI;
using CrossBlitz.Utils;
using CrossBlitz.ViewAPI;
using DG.Tweening;
using GameDataEditor;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.MainMenu
{
    public class HexInfoWindow : MonoBehaviour
    {
        public CanvasGroup canvas;
        public RectTransform container;
        public SpriteAnimation title;
        public RectTransform levelContainer;
        public SpriteAnimation levelUpIcon;
        public SpriteAnimation levelIcon;
        public TextMeshProUGUI levelLabel;
        public TextMeshProUGUI description;

        public void SetData(string heroId, int levelGridIndex, int level, Vector3 position)
        {
            canvas.DOKill();
            container.DOKill();

            canvas.alpha = 0;
            container.position = position - Vector3.up;

            // Adjust for cropping offscreen
            if (container.position.x > 4)
            {
                container.position -= new Vector3(0.4f, 0, 0);
                position -= new Vector3(0.4f, 0, 0);
            }

            var clientHero = App.HeroData.GetHero(heroId);
            var heroData = clientHero.GetData();
            var clientLevelRewardData = clientHero.levelGridData[levelGridIndex];

            levelIcon.Play(heroData.faction.ToString().ToLower());
            levelLabel.text = (level+1).ToString();

            title.SetActive(true);
            canvas.DOFade(1, .1f);
            container.DOMove(position, .24f).SetEase(Ease.OutBack);

            if (clientLevelRewardData.RewardUid==0)
            {
                levelUpIcon.SetActive(true);
                levelUpIcon.Play(heroData.faction.ToString().ToLower());

                if ((clientHero.level-1-clientHero.levelPoints) == levelGridIndex)
                {
                    title.SetActive(false);
                    description.text = "Spend 1 lv.p to select a level card.";
                }
                else
                {
                    title.SetActive(true);
                    title.Play("pending");
                    description.text = "Activate the preceding node(s) to access this.";
                }
                return;
            }

            levelUpIcon.SetActive(false);
            var levelRewardData = heroData.GetLevelReward(clientLevelRewardData.RewardUid);

            // switch (clientLevelRewardData.Reward.type)
            // {
            //     case LevelReward.EType.HeathUp:
            //         title.Play("hp-up");
            //         description.text = $"maximum hp increased by {levelRewardData.healthPoints} points.";
            //         break;
            //     case LevelReward.EType.Card:
            //         var card = Db.CardDatabase.GetCard(levelRewardData.cardId);
            //         switch (card.type)
            //         {
            //             case CardType.Minion:
            //                 title.Play("card-minion");
            //                 break;
            //             case CardType.Spell:
            //                 title.Play("card-spell");
            //                 break;
            //             case CardType.Trick:
            //                 title.Play("card-trick");
            //                 break;
            //             case CardType.Power:
            //                 title.Play("blitz-burst");
            //                 break;
            //             default:
            //                 title.Play("mystery");
            //                 break;
            //         }
            //         description.text = $"a new card was added to your collection!";
            //         break;
            //     case LevelReward.EType.ManaShards:
            //         title.Play("mana-shards");
            //         description.text = $"received {levelRewardData.manaShards}.";
            //         break;
            //     case LevelReward.EType.IngredientPouch:
            //         title.Play("ingredients");
            //         var ingredient = new GDEItemData(levelRewardData.ingredient);
            //         description.text = $"received {Crafting.GetLevelRewardIngredientAmountForIngredient(levelRewardData.ingredient)} {ingredient.Name}s, use these to craft new cards!";
            //         break;
            //     case LevelReward.EType.TreasureChest:
            //         title.Play("treasure-chest");
            //         break;
            //     default:
            //         title.Play("mystery");
            //         description.text = "Unknown reward.";
            //         break;
            // }

        }

        public void Close()
        {
            canvas.DOFade(0, .1f);
        }

    }
}