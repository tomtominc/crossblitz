using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.UI.Widgets;
using CrossBlitz.Utils;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.MainMenu
{
    public class HeroMainMenuSlot : MonoBehaviour
    {
        [InfoBox("The Hero Id is determined by the objects name. Don't change the name!")]
        public bool setHeroDynamically;
        public Canvas heroCanvas;
        public RectTransform slotShadow;
        public RectTransform slotContainer;
        public SpriteAnimation heroEmblem;
        public SpriteAnimation levelIcon;
        public TextMeshProUGUI levelNumber;
        public RectTransform levelContainer;
        public SpriteAnimation levelUpEffect;

        [BoxGroup("Selectable")] public bool invokeToggleActions;
        [BoxGroup("Selectable")][ShowIf("invokeToggleActions")] public ToggleButton toggleButton;
        [BoxGroup("Selectable")] [ShowIf("invokeToggleActions")] public SpriteOutline spriteOutline;
        [BoxGroup("Selectable")] [ShowIf("invokeToggleActions")] public CanvasSortingLayer sorting;

        private string _heroId;
        public event Action<bool, HeroMainMenuSlot> OnToggled;

        private void Start()
        {
            Events.Subscribe(EventType.OnHeroLevelChanged, OnHeroLevelChanged);
            Events.Subscribe(EventType.OnHeroVisuallyLeveledUp, OnHeroVisuallyLeveledUp);
            Events.Subscribe(EventType.OnHeroesReset, OnHeroesReset);

            if (!setHeroDynamically)
            {
                SetHeroId(name);
            }

            if (invokeToggleActions && toggleButton)
            {
                toggleButton.OnValueChanged += Toggled;
            }
        }

        private void Toggled(bool isOn, string context)
        {
            OnToggled?.Invoke(isOn, this);

            if (spriteOutline)
            {
                if (isOn)
                {
                    //toggleButton.Toggle.interactable = false;
                    spriteOutline.Regenerate();
                    sorting.canvas.sortingOrder = sorting.activeSortingOrder;
                    Timing.RunCoroutine(PlayHeadScale());
                }
                else
                {
                    spriteOutline.Clear();
                    sorting.canvas.sortingOrder = sorting.inActivateSortingOrder;
                }
            }
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnHeroLevelChanged, OnHeroLevelChanged);
            Events.Unsubscribe(EventType.OnHeroVisuallyLeveledUp, OnHeroVisuallyLeveledUp);
            Events.Unsubscribe(EventType.OnHeroesReset, OnHeroesReset);
        }

        private void OnHeroesReset(IMessage message)
        {
            UpdateHeroView();
        }

        public void SetHeroId(string heroId, int customLevel =-1 )
        {
            _heroId = heroId;
            UpdateHeroView(customLevel);
        }

        public void UpdateHeroView(int customLevel =-1 )
        {
            if (!App.Initialized)
            {
                App.Init();
            }

            if (!Db.Initialized)
            {
                Db.Initialize();
            }

            var heroData = App.HeroData.GetHero(_heroId);
            var level = customLevel > 0 ? customLevel : heroData.level;

            heroEmblem.Play(heroData.id.ToLower());
            levelIcon.Play(heroData.GetData().faction.ToString().ToLower());
            levelNumber.text = level.ToString();

            var offset = levelNumber.text.Count(c => c.Equals('1'));

            if (level < 10)
            {
                levelContainer.anchoredPosition = new Vector2(4 + offset, levelContainer.anchoredPosition.y);
            }
            else if (level < 100)
            {
                levelContainer.anchoredPosition = new Vector2(0 + offset,levelContainer.anchoredPosition.y);
            }
            else
            {
                levelContainer.anchoredPosition=new Vector2(-3 + offset,levelContainer.anchoredPosition.y);
            }
        }



        public IEnumerator<float> PlayLevelUpAnimation()
        {
            if (levelContainer)
            {
                levelContainer.DOPunchAnchorPos(Vector2.up * 4f, 0.24f);
            }

            Timing.RunCoroutine(PlayHeadBounce().CancelWith(gameObject));

            if (levelUpEffect)
            {
                levelUpEffect.SetActive(true);
                levelUpEffect.Play("number-burst");
                levelUpEffect.RectTransform().DOPunchAnchorPos(Vector2.down * 8f, 0.5f);
                while (!levelUpEffect.IsDone) yield return Timing.WaitForOneFrame;
                levelUpEffect.SetActive(false);
            }

            if (slotContainer.GetComponent<ToolTipHoverImage>())
            {
                slotContainer.GetComponent<ToolTipHoverImage>().Hide();
            }
        }

        private IEnumerator<float> PlayHeadBounce()
        {
            if (slotContainer)
            {
                slotContainer.DOPunchAnchorPos(Vector2.down * 8f, 0.5f);
            }

            if (slotShadow)
            {
                slotShadow.DOPunchAnchorPos(Vector2.down * 8f, 0.5f);
            }

            if (heroEmblem)
            {
                var startY = heroEmblem.RectTransform().anchoredPosition.y;
                yield return Timing.WaitUntilDone(heroEmblem.RectTransform().DOAnchorPosY(startY + 8, 0.2f)
                    .SetEase(Ease.Linear).WaitForCompletion(true));
                heroEmblem.RectTransform().DOAnchorPosY(startY, 0.4f).SetEase(Ease.OutBounce);
            }

        }

        private IEnumerator<float> PlayHeadScale()
        {

            yield return Timing.WaitUntilDone(transform.DOPunchScale(Vector3.one * -0.2f, 0.4f).WaitForCompletion(true));
        }

        private void OnHeroLevelChanged(IMessage message)
        {
            if (message.Data is OnHeroLevelChangedEventArgs args)
            {
                if (args.heroId != _heroId) return;
                UpdateHeroView(args.level);
            }
        }

        private void OnHeroVisuallyLeveledUp(IMessage message)
        {
            if (message.Data is OnHeroVisuallyLeveledUpEventArgs args)
            {
                if (args.heroId != _heroId) return;

                UpdateHeroView(args.newLevel);
                Timing.RunCoroutine(PlayLevelUpAnimation().CancelWith(gameObject));
            }
        }
    }
}