using UnityEngine;

namespace CrossBlitz.MainMenu
{
    public class SlowMapPan : MonoBehaviour
    {
        public float speed;
        private Vector2 direction;

        public float scroll_speed = 1;

        public void Start()
        {
            direction = new Vector2(scroll_speed, scroll_speed);

            // Random Start to scrolling
            for (int i = 0; i<2; i++)
            {
                int m_random = Random.Range(0, 100);
                if(m_random > 50)
                {
                    direction[i] = -direction[i];
                }
            }
            
        }
        public void FixedUpdate()
        {

            transform.Translate(direction*speed*Time.fixedDeltaTime);

            var curr_x = transform.position.x;
            var curr_y = transform.position.y;

            if(curr_x <= -60)
            {
                direction[0] = scroll_speed;
            }
            if (curr_x >= 60)
            {
                direction[0] = -scroll_speed;
            }

            if (curr_y <= -30)
            {
                direction[1] = scroll_speed;
            }
            if (curr_y >= 30)
            {
                direction[1] = -scroll_speed;
            }
        }
    }
}