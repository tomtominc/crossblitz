using System.Collections.Generic;
using CrossBlitz.Shop.Data;
using UnityEngine;

namespace CrossBlitz.Databases
{
    [CreateAssetMenu]
    public class ShopsDatabase : ScriptableObject
    {
        public List<ShopData> Shops;

        public static readonly Dictionary<string, ShopCategory> GetShopCategoryFromItemType =
            new Dictionary<string, ShopCategory>
            {
                {"Card", ShopCategory.Cards},
                {"Chest", ShopCategory.Specials},
                {"Ingredient", ShopCategory.Resources}
            };

#if UNITY_EDITOR

        public const string AssetPath = "Assets/Source/Database/ShopsDatabase.asset";
        public static ShopsDatabase GetOrCreateDatabase()
        {
            var database = UnityEditor.AssetDatabase.LoadAssetAtPath<ShopsDatabase>(AssetPath);

            if (database == null)
            {
                database = CreateInstance<ShopsDatabase>();
                database.name = "ShopsDatabase";
                UnityEditor.AssetDatabase.CreateAsset(database, AssetPath);
                UnityEditor.EditorUtility.SetDirty(database);
                UnityEditor.AssetDatabase.SaveAssets();
                UnityEditor.AssetDatabase.Refresh();
            }

            return database;
        }
#endif

        public void Init()
        {

        }
        public ShopData GetShopByUid(string uid)
        {
            return Shops.Find(shop => shop.Uid == uid);
        }

        public ShopData GetShopByName(string shopName)
        {
            return Shops.Find(shop => shop.displayName == shopName);
        }

        public ShopData CreateNewShop()
        {
            var shopData = new ShopData();
            shopData.SetupBaseValues();
            Shops.Add(shopData);
            Save();
            return shopData;
        }

        public void ChangeShopName(string uid, string newName)
        {
            var shop = GetShopByUid(uid);

            if (shop != null)
            {
                shop.displayName = newName;
                Save();
            }
        }

        public void AddItem(string uid, string itemId)
        {
            var shop = GetShopByUid(uid);

            if (shop != null)
            {
                var item = shop.items.Find(i => i == itemId);

                if (item == null)
                {
                    shop.items.Add(itemId);
                }
                Save();
            }
        }

        public void DeleteItem(string uid, string itemId)
        {
            var shop = GetShopByUid(uid);

            if (shop != null)
            {
                if (shop.items.Remove(itemId))
                {
                    Save();
                }

            }
        }

        public void Save()
        {
            #if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
            #endif
        }

        public void DeleteShop(string uid)
        {
            var shop = GetShopByUid(uid);

            if (shop != null)
            {
                Shops.Remove(shop);
            }
        }
    }
}