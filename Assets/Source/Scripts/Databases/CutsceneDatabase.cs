using System;
using System.Collections.Generic;
using CrossBlitz.Fables.Cutscene.Data;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.Databases
{
    public class CutsceneDatabasePlayFab
    {
        public Dictionary<string, CutsceneData> Table;
    }
    [CreateAssetMenu]
    public class CutsceneDatabase : ScriptableObject
    {
        public List<string> Folders;
        public List<CutsceneData> Cutscenes;

        public void Init()
        {

        }

        [Button]
        public void UpdateQuestSteps()
        {
            for (var i = 0; i < Cutscenes.Count; i++)
            {
                var cutscene = Cutscenes[i];

                for (var j = 0; j < cutscene.DialogueInfo.Count; j++)
                {
                    var dialogue = cutscene.DialogueInfo[j];
                    if (dialogue.Text.Contains("quest-step-change"))
                    {
                        var parameters = dialogue.Text.Split(':');
                        var isInt = int.TryParse(parameters[2][0].ToString(), out var stepValue);
                        var fableQuestUid = Db.FableQuestDatabase.GetQuestUidFromName(parameters[1]);
                        cutscene.QuestUid = fableQuestUid;
                        cutscene.QuestStepChange = stepValue;
                    }
                }
            }

            Save();
        }

        [Button]
        public void FixAndUpdateUniqueIdentifiers()
        {
            var uids = new Dictionary<string, int>();

            for (var i = 0; i < Cutscenes.Count; i++)
            {
                var cutscene = Cutscenes[i];

                if (uids.ContainsKey(cutscene.Uid))
                {
                    Debug.LogError($"Cutscene {cutscene.EventTitle} does not have a unique identifier.");
                    continue;
                }

                uids.Add(cutscene.Uid, 0);
            }
            
            Save();
        }

        [Button]
        public void FixApostrophes()
        {
            for (var i = 0; i < Cutscenes.Count; i++)
            {
                for (var j = 0; j < Cutscenes[i].DialogueInfo.Count; j++)
                {
                    Cutscenes[i].DialogueInfo[j].Text = Cutscenes[i].DialogueInfo[j].Text.Replace("’", "'");
                }
            }

            Save();
        }

        public CutsceneData CreateNew(string folder)
        {
            var cutscene = new CutsceneData();
            cutscene.UpdateIdentifier();
            cutscene.EventTitle = "New Cutscene";
            cutscene.RightSideCharacters = new List<string>();
            cutscene.LeftSideCharacters = new List<string>();
            cutscene.RightCharacterOverrideNames = new List<string>();
            cutscene.LeftCharacterOverrideNames = new List<string>();
            cutscene.DialogueInfo = new List<CutsceneDialogueInfo>();
            AddToFolder(folder, cutscene);
            Cutscenes.Add(cutscene);
            return cutscene;
        }

        public bool TryCreateNewFolder(string folderPath, out string folderName)
        {
            folderName = string.Empty;

            var newFolderName = string.IsNullOrEmpty(folderPath) ? "New Folder" : $"{folderPath}/New Folder";

            if (Folders.Contains(newFolderName))
            {
                Debug.LogError(newFolderName);
                return false;
            }

            folderName = newFolderName;
            Folders.Add(newFolderName);
            return true;
        }

        public CutsceneData AddToFolder(string folderName, CutsceneData cutsceneData)
        {
            cutsceneData.Folder = folderName;

            for (var i = 0; i < cutsceneData.DialogueInfo.Count; i++)
            {
                for (var j = 0; j < cutsceneData.DialogueInfo[i].Choices.Count; j++)
                {
                    var choiceDialogue = GetCutscene( cutsceneData.DialogueInfo[i].Choices[j].CutsceneUid);

                    if (choiceDialogue != null)
                    {
                        choiceDialogue.Folder = folderName;
                    }
                }
            }
            return cutsceneData;
        }

        public void DeleteFolder(string folderName)
        {
            var index = Folders.IndexOf(folderName);

            if (index < 0)
            {
                Debug.Log($"Could not find old folder {folderName}");
                return;
            }

            Folders.RemoveAt(index);
        }

        public void ChangeFolderName(string oldFolderName, string newFolderName)
        {
            var index = Folders.IndexOf(oldFolderName);

            if (index < 0)
            {
                Debug.Log($"Could not find old folder {oldFolderName}");
                return;
            }

            var cutscenesToChange = GetCutscenesInFolder(oldFolderName);

            for (var i = 0; i < cutscenesToChange.Count; i++)
            {
                cutscenesToChange[i].Folder=cutscenesToChange[i].Folder.Replace(oldFolderName, newFolderName);
            }

            var childFolders = GetFoldersInFolder(oldFolderName);

            for (var i = 0; i < childFolders.Count; i++)
            {
                var childIndex = Folders.IndexOf(childFolders[i]);
                if (childIndex > 0) Folders[childIndex] = childFolders[i].Replace(oldFolderName, newFolderName);
            }

            Folders[index] = newFolderName;
        }

        public List<CutsceneData> GetCutscenesInFolder(string folderName, bool includeChildren=false)
        {
            if (string.IsNullOrEmpty(folderName))
            {
                return Cutscenes.FindAll(cutsceneData => string.IsNullOrEmpty(cutsceneData.Folder));
            }

            if (includeChildren)
            {
                return Cutscenes.FindAll(cutsceneData => cutsceneData.Folder.Contains(folderName));
            }

            return Cutscenes.FindAll(cutsceneData => cutsceneData.Folder == folderName);
        }

        public List<string> GetFoldersInFolder(string folderPath)
        {
            if (string.IsNullOrEmpty(folderPath))
            {
                return Folders.FindAll(folder => !folder.Contains("/"));
            }

            return Folders.FindAll(folder => folder.Contains(folderPath + "/"));
        }

        public CutsceneData GetCutscene(string uid)
        {
            if (string.IsNullOrEmpty(uid)) return null;
            return Cutscenes.Find(cutscene => cutscene.Uid == uid);
        }

        public CutsceneData GetCutsceneByEventTitle(string eventTitle)
        {
            return Cutscenes.Find(cutscene => cutscene.EventTitle == eventTitle);
        }

        public CutsceneData GetCutsceneFromDialogueUid(string dialogueUid)
        {
            CutsceneData cutscene = null;

            for (var i = 0; i < Cutscenes.Count; i++)
            {
                for (var j = 0; j < Cutscenes[i].DialogueInfo.Count; j++)
                {
                    if (Cutscenes[i].DialogueInfo[j].Uid == dialogueUid)
                    {
                        cutscene = Cutscenes[i];
                        break;
                    }
                }
            }

            return cutscene;
        }

        public CutsceneDialogueInfo GetDialogue(string uid)
        {
            CutsceneDialogueInfo dialogue = null;

            for (var i = 0; i < Cutscenes.Count; i++)
            {
                for (var j = 0; j < Cutscenes[i].DialogueInfo.Count; j++)
                {
                    if (Cutscenes[i].DialogueInfo[j].Uid == uid)
                    {
                        dialogue = Cutscenes[i].DialogueInfo[j];
                        break;
                    }
                }
            }

            return dialogue;
        }

        public void Delete(string uid, bool save = true)
        {
            if (string.IsNullOrEmpty(uid)) return;

            var cutscene = GetCutscene(uid);

            if (cutscene != null)
            {
                Cutscenes.Remove(cutscene);

                for (var i = 0; i < cutscene.DialogueInfo.Count; i++)
                {
                    for (var j = 0; j < cutscene.DialogueInfo[i].Choices.Count; j++)
                    {
                        Delete(cutscene.DialogueInfo[i].Choices[j].CutsceneUid,false);
                    }
                }

                if (save) Save();
            }
        }

        public void MoveUp(string uid, bool save = true)
        {
            if (string.IsNullOrEmpty(uid)) return;

            var cutscene = GetCutscene(uid);

            if (cutscene != null)
            {
                Cutscenes.MoveUp(cutscene);
                if (save) Save();
            }
        }

        public void MoveDown(string uid, bool save = true)
        {
            if (string.IsNullOrEmpty(uid)) return;

            var cutscene = GetCutscene(uid);

            if (cutscene != null)
            {
                Cutscenes.MoveDown(cutscene);
                if (save) Save();
            }
        }

        public void Save()
        {
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }

#if UNITY_EDITOR

        public const string AssetPath = "Assets/Source/Database/CutsceneDatabase.asset";
        public static CutsceneDatabase GetOrCreateDatabase()
        {
            var database = UnityEditor.AssetDatabase.LoadAssetAtPath<CutsceneDatabase>(AssetPath);

            if (database == null)
            {
                database = CreateInstance<CutsceneDatabase>();
                database.name = "CutsceneDatabase";
                UnityEditor.AssetDatabase.CreateAsset(database, AssetPath);
                UnityEditor.EditorUtility.SetDirty(database);
                UnityEditor.AssetDatabase.SaveAssets();
                UnityEditor.AssetDatabase.Refresh();
            }

            return database;
        }
#endif
    }
}