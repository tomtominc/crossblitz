using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.Utils;
using Sirenix.Utilities;
using UnityEngine;

namespace CrossBlitz.Databases
{
    [CreateAssetMenu]
    public class DeckRecipeDatabase : ScriptableObject
    {
        public List<DeckRecipeData> deckRecipes;

        public void Init()
        {

        }

        public void Save()
        {
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }

        public DeckRecipeData CreateNewRecipe()
        {
            var deckRecipe = new DeckRecipeData();
            deckRecipe.UpdateIdentifier();
            deckRecipe.deckType = DeckRecipeData.DeckType.Aggro;
            deckRecipe.mainCards = new List<CardValue>();
            deckRecipe.deckData = new DeckData();
            deckRecipe.deckData.name = "New Deck";
            deckRecipes.Add(deckRecipe);
            return deckRecipe;
        }

        public DeckRecipeData GetRecipe(string uid)
        {
            return deckRecipes.Find(d => d.Uid == uid);
        }

        public DeckRecipeData GetRecipeByName(string recipeName)
        {
            return deckRecipes.Find(d => d.deckData.name == recipeName);
        }

        public int GetRecipeIndex(string uid)
        {
            var recipe = GetRecipe(uid);
            return deckRecipes.IndexOf(recipe);
        }

        public void MoveRecipeDown(string uid)
        {
            var recipeIndex = GetRecipeIndex(uid);

            if (recipeIndex < deckRecipes.Count - 1)
            {
                (deckRecipes[recipeIndex], deckRecipes[recipeIndex + 1]) =
                    (deckRecipes[recipeIndex + 1], deckRecipes[recipeIndex]);
            }
        }

        public void MoveRecipeUp(string uid)
        {
            var recipeIndex = GetRecipeIndex(uid);

            if (recipeIndex > 0)
            {
                (deckRecipes[recipeIndex], deckRecipes[recipeIndex - 1]) =
                    (deckRecipes[recipeIndex - 1], deckRecipes[recipeIndex]);
            }
        }

        public string GetArchetypeName(string uid)
        {
            if (!string.IsNullOrEmpty(uid))
            {
                var deckArchetype = GetRecipe(uid);
                return deckArchetype.archetypeDisplayName;
            }

            return string.Empty;
        }

        public void RemoveRecipe(string uid)
        {
            deckRecipes.RemoveAll(d => d.Uid == uid);
        }

        public List<DeckRecipeData> GetCollectableDecksByFaction(Faction faction)
        {
            return deckRecipes.FindAll(d => d.collectable && d.deckData.faction == faction);
        }

        public DeckRecipeData GetClosestArchetype(DeckData deckData)
        {
            var closest = 0;
            DeckRecipeData recipe = null;
            var recipesByFaction = deckRecipes.FindAll(r => r.deckData.faction == deckData.faction);

            for (var i = 0; i < recipesByFaction.Count; i++)
            {
                var recipeCards = recipesByFaction[i].deckData.cards;
                var count = 0;

                for (var j = 0; j < recipeCards.Count; j++)
                {
                    for (var k = 0; k < deckData.cards.Count; k++)
                    {
                        if (deckData.cards[k].id == recipeCards[j].id)
                        {
                            count++;
                        }
                    }
                }

                if (count > closest)
                {
                    closest = count;
                    recipe = recipesByFaction[i];
                }
            }

            return recipe;
        }
    }

    [System.Serializable]
    public class DeckRecipeData : UniqueItem
    {
        public enum DeckType
        {
            Aggro,
            Midrange,
            Control,
            Combo
        }

        public bool collectable;
        public DeckType deckType;
        public string archetype;
        public string archetypeDisplayName;
        public DeckData deckData;
        public string overview;
        public string howToPlayGuide;
        public List<CardValue> mainCards;
    }
}