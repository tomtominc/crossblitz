using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Hero;
using CrossBlitz.PlayFab.Authentication;
using GameDataEditor;
using Newtonsoft.Json;
using QFSW.QC;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Databases
{
    public class HeroDatabasePlayFab
    {
        public List<HeroData> Heroes;
    }

    [CreateAssetMenu]
    public class HeroDatabase : ScriptableObject
    {
        public List<HeroData> Heroes;

        public void Init(bool refreshFromExcel = true)
        {
            if (refreshFromExcel)
            {
                var gdeHeroes = GDEDataManager.GetAllItems<GDEHeroData>();

                for (var i = 0; i < gdeHeroes.Count; i++)
                {
                    var gdeHero = gdeHeroes[i];
                    var hero = Heroes.Find(h => h.id == gdeHero.Key);

                    if (hero == null)
                    {
                        var newHero = new HeroData {id = gdeHero.Key};
                        newHero.ConvertData();
                        Heroes.Add(newHero);
                    }
                }
            }

            FixDependencies();
        }

        public void FixDependencies()
        {
            // const int healthPoints2 = 2;
            // const int manaShardsMedium = 100;
            // var resetLevelTree = false;
            // var dirty = false;
            //
            // for (var i = 0; i < Heroes.Count; i++)
            // {
            //     var hero = Heroes[i];
            //
            //     if (resetLevelTree)
            //     {
            //         hero.levelTree = null;
            //     }
            //
            //     if (hero.levelTree == null)
            //     {
            //         hero.levelTree = new LevelTree();
            //         hero.levelTree.levels = new List<LevelReward>();
            //         /*2 */hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.HeathUp, healthPoints = healthPoints2 });
            //         /*2 */hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.IngredientPouch, ingredients = new List<string> { GDEItemKeys.Item_SilverChip }});
            //         /*2 */hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.Card, cardId = GDEItemKeys.Card_ArmorFairy });
            //         /*3 */hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.Card, cardId = GDEItemKeys.Card_RedwingCannon });
            //         /*4 */hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.ManaShards, manaShards = manaShardsMedium });
            //         /*5 */hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.Card, cardId = GDEItemKeys.Card_RedwingRaid });
            //         /*6 */hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.HeathUp, healthPoints = healthPoints2 });
            //         /*7 */hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.IngredientPouch, ingredients = new List<string> { GDEItemKeys.Item_SaltConch }});
            //         /*8 */hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.Card, cardId = GDEItemKeys.Card_AbandonShip });
            //         /*9 */hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.Card, cardId = GDEItemKeys.Card_SupplyShip });
            //         /*10*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.HeathUp, healthPoints = healthPoints2  });
            //         /*11*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.IngredientPouch, ingredients = new List<string> { GDEItemKeys.Item_DragonNail }});
            //         /*12*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.HeathUp, healthPoints = healthPoints2 });
            //         /*13*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.Card, cardId = GDEItemKeys.Card_ThePeddler });
            //         /*14*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.IngredientPouch, ingredients = new List<string> { GDEItemKeys.Item_ShineWire }});
            //         /*15*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.ManaShards, manaShards = manaShardsMedium });
            //         /*16*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.IngredientPouch, ingredients = new List<string> { GDEItemKeys.Item_SilverChip }});
            //         /*17*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.Card, cardId = GDEItemKeys.Card_GunpowderPelican });
            //         /*18*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.IngredientPouch, ingredients = new List<string> { GDEItemKeys.Item_SaltConch }});
            //         /*19*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.ManaShards, manaShards = manaShardsMedium });
            //         /*20*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.IngredientPouch, ingredients = new List<string> { GDEItemKeys.Item_DragonNail }});
            //         /*21*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.HeathUp, healthPoints = healthPoints2 });
            //         /*22*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.Card, cardId = GDEItemKeys.Card_Blunderbuss });
            //         /*23*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.HeathUp, healthPoints = healthPoints2 });
            //         /*24*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.IngredientPouch, ingredients = new List<string> { GDEItemKeys.Item_ShineWire }});
            //         /*25*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.Card, cardId = GDEItemKeys.Card_WorldTournament });
            //         /*26*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.IngredientPouch, ingredients = new List<string> { GDEItemKeys.Item_DragonNail }});
            //         /*27*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.HeathUp, healthPoints = healthPoints2 });
            //         /*28*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.ManaShards, manaShards = manaShardsMedium });
            //         /*29*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.HeathUp, healthPoints = healthPoints2 });
            //         /*30*/hero.levelTree.levels.Add(new LevelReward { uid = GUID.CreateUniqueInt(), type = LevelReward.EType.Card, cardId = GDEItemKeys.Card_GunwallBlast });
            //
            //         dirty = true;
            //     }
            // }
            //
            // if (dirty)
            // {
            //     Save();
            // }
        }

        public void DeleteHero(string characterId)
        {
            var hero = GetHero(characterId);
            if (hero!=null)
            {
                Heroes.Remove(hero);
            }
        }

        public void Save()
        {
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }

        public HeroData GetHeroByName(string heroName)
        {
            return Heroes.Find(hero => hero.name == heroName);
        }

        public bool IsHeroById(string id)
        {
            return Heroes.Exists(hero => hero.id == id);
        }

        public bool IsHero(string heroName)
        {
            return GetHeroByName(heroName) != null;
        }

        public HeroData GetHero(string heroId)
        {
            return Heroes.Find(hero => hero.id == heroId);
        }

        public HeroData GetHeroByFaction(Faction faction)
        {
            switch (faction)
            {
                case Faction.War: return GetHero(GDEItemKeys.Hero_Redcroft);
                case Faction.Balance: return GetHero(GDEItemKeys.Hero_Seto);
                case Faction.Chaos: return GetHero(GDEItemKeys.Hero_Violet);
                case Faction.Fortune: return GetHero(GDEItemKeys.Hero_Quill);
                case Faction.Nature: return GetHero(GDEItemKeys.Hero_Mereena);
            }
            return GetHero(GDEItemKeys.Hero_Redcroft);
        }



        [Command("modify-hero-level")]
        public static void ModifyHeroLevel(string heroId, int newLevel)
        {
            if (!App.Initialized) App.Init();
            App.HeroData.ChangeHeroLevel(heroId, newLevel);
            Events.Publish(null, EventType.OnHeroLevelChanged, new OnHeroLevelChangedEventArgs {heroId = heroId, level = newLevel });
        }

        [Command("level-up")]
        public static void LevelUpHero(string heroId)
        {
            if (!App.Initialized)
            {
                App.Init();
            }

            var hero = App.HeroData.GetHero(heroId);

            if (hero != null)
            {
                App.HeroData.ChangeHeroLevel(heroId, hero.level + 1);

                Events.Publish(null, EventType.OnHeroLevelChanged, new OnHeroLevelChangedEventArgs
                {
                    heroId = heroId,
                    level = App.HeroData.GetHero(heroId).level
                });
            }
        }

        [Command("grant-exp")]
        public static void GrantExperience(string heroId, int expAmt)
        {
            if (!App.Initialized)
            {
                App.Init();
            }

            var hero = App.HeroData.GetHero(heroId);
            App.HeroData.SetXp(hero.id, expAmt);
        }

        [Command("change-current-hero")]
        public static void ChangeCurrentHero(string newHero)
        {
            var heroData = Db.HeroDatabase.GetHero(newHero);

            if (heroData != null)
            {
                Events.Publish(null, EventType.OnHeroChanged, heroData);
            }
            else
            {
                Debug.LogError($"Hero {newHero} is not a valid hero.");
            }
        }
    }
}