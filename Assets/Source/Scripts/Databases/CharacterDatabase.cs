using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.Hero;
using UnityEngine;

namespace CrossBlitz.Databases
{
    [CreateAssetMenu()]
    public class CharacterDatabase : ScriptableObject
    {
        public List<CharacterData> Characters;

        public void Init() {}

        public bool IsCharacter(string id)
        {
            return Characters.Exists(c => c.id == id);
        }

        public CharacterData GetCharacterByName(string characterName)
        {
            var character = Characters.Find(c => c.name == characterName);

            if (character == null)
            {
                character = Db.HeroDatabase.IsHero(characterName)
                    ? Db.HeroDatabase.GetHero(characterName)
                    : (CharacterData)Db.CardDatabase.GetCardByName(characterName);
            }

            return character;
        }

        public void DeleteCharacter(string characterId)
        {
            var character = Characters.Find(c => c.id == characterId);
            if (character != null)
            {
                Characters.Remove(character);
            }
        }

        public void DeleteCharacterFromAllDbs(string characterId)
        {
            var character = Characters.Find(c => c.id == characterId);

            if (character != null)
            {
                Characters.Remove(character);
                return;
            }

            if (Db.HeroDatabase.IsHeroById(characterId))
            {
                Db.HeroDatabase.DeleteHero(characterId);
                return;
            }

            if (Db.CardDatabase.GetCard(characterId) != null)
            {
                Db.CardDatabase.DeleteCard(characterId);
            }
        }

        public CharacterData GetCharacter(string id)
        {
            var character = Characters.Find(c => c.id == id);

            if (character == null)
            {
                character = Db.HeroDatabase.IsHeroById(id)
                    ? Db.HeroDatabase.GetHero(id)
                    : (CharacterData)Db.CardDatabase.GetCard(id);
            }

            return character;
        }

        public bool CreateCharacter(string id, out CharacterData characterData)
        {
            if (string.IsNullOrEmpty(id) || id.Contains(" "))
            {
                characterData = null;
                return false;
            }

            var copiedCharacter = GetCharacter(id);

            if (copiedCharacter != null)
            {
                characterData = null;
                return false;
            }

            characterData = new CharacterData();
            characterData.id = id;
            characterData.name = id;
            characterData.type = CardType.Character;
            characterData.faction = Faction.Neutral;

            Characters.Add(characterData);

            return true;
        }

        public List<CharacterData> GetAllCharacters(Predicate<CharacterData> match=null)
        {
            var characterList = new List<CharacterData>();
            characterList.AddRange(Characters.OrderBy(c => c.name));
            characterList.AddRange(Db.HeroDatabase.Heroes.ConvertAll( c => c as CharacterData).OrderBy(c => c.name));
            characterList.AddRange(Db.CardDatabase.Cards.ConvertAll(c => c as CharacterData).OrderBy(c=> c.name));
            if (match!=null) characterList.RemoveAll(match);
            return characterList;
        }

        public Dictionary<string, List<CharacterData>> GetAllCharactersByType()
        {
            return new Dictionary<string, List<CharacterData>>
            {
                {"Character", Characters },
                {"Hero", Db.HeroDatabase.Heroes.ConvertAll( c => c as CharacterData) },
                {"Card", Db.CardDatabase.Cards.ConvertAll(c => c as CharacterData) }
            };
        }

        public void Save()
        {
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }
    }
}