using UnityEngine;

namespace CrossBlitz.Databases
{
    [CreateAssetMenu]
    public class Economy : ScriptableObject
    {
        public const int IngredientPointsPerWin = 4;
        public const int MatchPointsPerWin = 8;
    }
}