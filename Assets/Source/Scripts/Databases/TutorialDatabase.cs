using System.Collections.Generic;
using CrossBlitz.ServerAPI;
using CrossBlitz.Utils;
using Newtonsoft.Json;
using UnityEngine;

namespace CrossBlitz.Databases
{
    [System.Serializable]
    public class TutorialData : UniqueItem
    {
        public bool enabled;
        public string title;
        public List<TutorialPage> pages;

        public string AddPage()
        {
            var page = new TutorialPage
            {
                header = "New Page",
                images = new List<string>()
            };
            page.UpdateIdentifier();
            pages.Add( page );
            return page.Uid;
        }

        public bool DeletePage(string uid)
        {
            var page = Get(uid);
            return pages.Remove(page);
        }

        public TutorialPage Get(string uid)
        {
            return pages.Find(p => p.Uid == uid);
        }

        public bool MoveUp(string uid)
        {
            var page = Get(uid);
            if (page != null)
            {
                var index = pages.IndexOf(page);
                if (index > 0)
                {
                    (pages[index], pages[index - 1]) = (pages[index - 1], pages[index]);
                    return true;
                }
            }

            return false;
        }

        public bool MoveDown(string uid)
        {
            var page = Get(uid);
            if (page != null)
            {
                var index = pages.IndexOf(page);
                if (index < pages.Count-1)
                {
                    (pages[index], pages[index + 1]) = (pages[index + 1], pages[index]);
                    return true;
                }
            }

            return false;
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Server.JsonSettings);
        }

        public static TutorialData FromJson(string json)
        {
            return JsonConvert.DeserializeObject<TutorialData>(json, Server.JsonSettings);
        }
    }

    [System.Serializable]
    public class TutorialPage : UniqueItem
    {
        public string header;
        public string text;

        /// <summary>
        /// images are loaded by asset bundle reference, when importing a tutorial image, make sure it's an asset bundle.
        /// example: Tutorial/Image_01
        /// </summary>
        public List<string> images;

        public void AddImage(string imageUrl)
        {
            images.Add(imageUrl);
        }

        public bool DeleteImage(string imageUrl)
        {
            return images.Remove(imageUrl);
        }

        public void MoveUp(string imageUrl)
        {
            var index = images.IndexOf(imageUrl);
            if (index > 0)
            {
                (images[index], images[index - 1]) = (images[index - 1], images[index]);
            }
        }

        public void MoveDown(string imageUrl)
        {
            var index = images.IndexOf(imageUrl);
            if (index < images.Count-1 && index > -1)
            {
                (images[index], images[index + 1]) = (images[index + 1], images[index]);
            }
        }
    }

    [CreateAssetMenu()]
    public class TutorialDatabase : ScriptableObject
    {
        public List<TutorialData> tutorials;

        public void Init()
        {

        }

        public TutorialData GetByName(string tutorialName)
        {
            return tutorials.Find(t => t.title == tutorialName);
        }

        public TutorialData Get(string uid)
        {
            return tutorials.Find(t => t.Uid == uid);
        }

        public TutorialData Add(string tutorialName)
        {
            var tutorialData = new TutorialData
            {
                enabled = true,
                title = tutorialName,
                pages =  new List<TutorialPage>()
            };

            tutorialData.UpdateIdentifier();
            tutorialData.AddPage();

            tutorials.Add( tutorialData );

            return tutorialData;
        }

        public bool Delete(string uid)
        {
            var tutorial = Get(uid);

            if (tutorial != null)
            {
                return tutorials.Remove(tutorial);
            }

            return false;
        }

        public bool MoveUp(string uid)
        {
            var tutorial = Get(uid);

            if (tutorial != null)
            {
                var index = tutorials.IndexOf(tutorial);

                if (index > 0)
                {
                    (tutorials[index], tutorials[index - 1]) = (tutorials[index - 1], tutorials[index]);
                    return true;
                }
            }

            return false;
        }

        public bool MoveDown(string uid)
        {
            var tutorial = Get(uid);

            if (tutorial != null)
            {
                var index = tutorials.IndexOf(tutorial);

                if (index < tutorials.Count-1)
                {
                    (tutorials[index], tutorials[index + 1]) = (tutorials[index + 1], tutorials[index]);
                    return true;
                }
            }

            return false;
        }

        public string Copy(string uid)
        {
            var tutorial = Get(uid);

            if (tutorial != null)
            {
                return tutorial.ToJson();
            }

            return string.Empty;
        }

        public TutorialData Paste(string json)
        {
            var tutorialData = TutorialData.FromJson(json);
            tutorialData.UpdateIdentifier();

            tutorials.Add( tutorialData );

            return tutorialData;
        }

        public void Save()
        {
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }
    }
}