using System.Collections.Generic;
using CrossBlitz.Utils;
using UnityEngine;

namespace CrossBlitz.Databases
{
    [CreateAssetMenu]
    public class GameStateDatabase : ScriptableObject
    {
        //public List<CustomGameStateData> GameStates;

        public void Init()
        {

        }

        public void Save()
        {
#if UNITY_EDITOR

            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }
    }
}