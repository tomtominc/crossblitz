﻿using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Developer.D90;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Data;
using CrossBlitz.Hero;
using GameDataEditor;
using KennethDevelops.ProLibrary.Util.Serialization;
using Microsoft.Win32;
using Mono.CSharp;
using PlayFab;
using PlayFab.ClientModels;
using QFSW.QC;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;

namespace CrossBlitz.Databases
{
    public class Db : MonoBehaviour
    {
        public bool saveOnAwake;
        public CharacterDatabase _characterDatabase;
        public CardDatabase _cardDatabase;
        public AbilityDatabase _abilityDatabase;
        public HeroDatabase _heroDatabase;
        public DeckDatabase _deckDatabase;
        public ShopsDatabase _shopsDatabase;
        public ItemDatabase _itemDatabase;
        public FablesDatabase _fablesDatabase;
        public CutsceneDatabase _cutsceneDatabase;
        public MapDatabase _mapDatabase;
        public BattleDatabase _battleDatabase;
        public FableQuestDatabase _fableQuestDatabase;
        public FableBackgroundPalettePresetsDatabase _fableBackgroundPalettePresetsDatabase;
        public DeckRecipeDatabase _deckRecipeDatabase;
        public GameStateDatabase _gameStateDatabase;
        public CardBackDatabase _cardBackDatabase;
        public AchievementDatabase _achievements;
        public VfxDatabase _vfxDatabase;
        public CardPoolsDatabase _cardPoolsDatabase;
        public TutorialDatabase _tutorialDatabase;

        public static CharacterDatabase CharacterDatabase => Instance._characterDatabase;
        public static CardDatabase CardDatabase=> Instance._cardDatabase;

        public static AbilityDatabase AbilityDatabase
        {
            get
            {
                if (Instance == null)
                {
                    ForceInit();
                }

                return Instance._abilityDatabase;
            }
        }
        public static HeroDatabase HeroDatabase=>Instance._heroDatabase;
        public static DeckDatabase DeckDatabase => Instance._deckDatabase;
        public static ShopsDatabase ShopsDatabase => Instance._shopsDatabase;
        public static ItemDatabase ItemDatabase => Instance._itemDatabase;
        public static FablesDatabase FablesDatabase => Instance._fablesDatabase;
        public static CutsceneDatabase CutsceneDatabase => Instance._cutsceneDatabase;
        public static BattleDatabase BattleDatabase => Instance._battleDatabase;
        public static FableBackgroundPalettePresetsDatabase FableBackgroundPalettePresetsDatabase =>
            Instance._fableBackgroundPalettePresetsDatabase;
        public static FableQuestDatabase FableQuestDatabase => Instance._fableQuestDatabase;
        public static MapDatabase MapDatabase => Instance._mapDatabase;
        public static DeckRecipeDatabase DeckRecipeDatabase => Instance._deckRecipeDatabase;
        public static GameStateDatabase GameStateDatabase => Instance._gameStateDatabase;
        public static CardBackDatabase CardBackDatabase => Instance._cardBackDatabase;
        public static AchievementDatabase AchievementDatabase => Instance._achievements;
        public static VfxDatabase VfxDatabase => Instance._vfxDatabase;

        public static CardPoolsDatabase CardPoolsDatabase => Instance._cardPoolsDatabase;
        public static TutorialDatabase TutorialDatabase => Instance._tutorialDatabase;

        public static bool IsBusy;
        public static bool Initialized;
        private static Db Instance;

        public void Awake()
        {
            Instance = this;
        }

        public static void ForceInit()
        {
            Instance = FindObjectOfType<Db>();
            Initialize();
        }

        public static void Initialize()
        {
            if (!GDEDataManager.Init("gde_data"))
            {
                Debug.LogError("Error! GDE could not be loaded.");
            }

            InitializeDatabase();
        }

        [Command("save-databases")][Button]
        public static void Save()
        {
            CharacterDatabase.Save();
            CardDatabase.Save();
            AbilityDatabase.Save();
            HeroDatabase.Save();
            ShopsDatabase.Save();
            ItemDatabase.Save();
            FablesDatabase.Save();
            CutsceneDatabase.Save();
            MapDatabase.Save();
            FableQuestDatabase.Save();
            FableBackgroundPalettePresetsDatabase.Save();
            BattleDatabase.Save();
            DeckRecipeDatabase.Save();
            GameStateDatabase.Save();
            CardBackDatabase.Save();
            AchievementDatabase.Save();
            VfxDatabase.Save();
            CardPoolsDatabase.Save();
            TutorialDatabase.Save();
        }

        /// <summary>
        /// Gets all the cards from firebase, it's event on success places them inside a dictionary
        /// </summary>
        public static void InitializeDatabase()
        {
            CharacterDatabase.Init();
            CardDatabase.Init();
            AbilityDatabase.Init();
            HeroDatabase.Init();
            DeckDatabase.Init();
            ShopsDatabase.Init();
            CutsceneDatabase.Init();
            FablesDatabase.Init();
            ItemDatabase.Init();
            FableBackgroundPalettePresetsDatabase.Init();
            MapDatabase.Init();
            BattleDatabase.Init();
            FableQuestDatabase.Init();
            DeckRecipeDatabase.Init();
            GameStateDatabase.Init();
            CardBackDatabase.Init();
            AchievementDatabase.Init();
            VfxDatabase.Init();
            CardPoolsDatabase.Init();
            TutorialDatabase.Init();

            IsBusy = false;
            Initialized = true;
        }


    }
}
