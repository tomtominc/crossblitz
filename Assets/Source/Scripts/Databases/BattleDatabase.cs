using System.Collections.Generic;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Data;
using CrossBlitz.GameVfx;
using GameDataEditor;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.Databases
{
    [CreateAssetMenu]
    public class BattleDatabase : ScriptableObject
    {
        [Searchable]
        public List<GameVfxData> VfxData;
        [Searchable]
        public List<BattleData> Battles;

        public void Init()
        {
            // if (Battles == null) Battles = new List<BattleData>();
            //
            // Battles.Clear();
            //
            // var shouldSave = false;
            //
            // for (var i = 0; i < Db.MapDatabase.MapData.Count; i++)
            // {
            //     for (var j = 0; j < Db.MapDatabase.MapData[i].Rooms.Count; j++)
            //     {
            //         for (var k = 0; k < Db.MapDatabase.MapData[i].Rooms[j].Tiles.Count; k++)
            //         {
            //             var tile = Db.MapDatabase.MapData[i].Rooms[j].Tiles[k];
            //
            //             if (tile.battleData != null && tile.NodeType == NodeType.Battle)
            //             {
            //                 if (tile.battleData.Uid == string.Empty) tile.battleData.UpdateIdentifier();
            //
            //                 tile.battleUid = tile.battleData.Uid;
            //                 tile.battleData.BookHero = GDEItemKeys.Hero_Redcroft;
            //                 tile.battleData.BookNumber = 1;
            //                 tile.battleData.ChapterNumber = 1;
            //                 tile.battleData.BattleNumber = Battles.Count + 1;
            //
            //                 Battles.Add(tile.battleData);
            //                 shouldSave = true;
            //             }
            //         }
            //     }
            // }
            //
            // if (shouldSave)
            // {
            //     Save();
            // }

            var saveAccoladeData = false;

            for (var i = 0; i < Battles.Count; i++)
            {
                for (var j = 0; j < Battles[i].Accolades.Count; j++)
                {
                    if (string.IsNullOrEmpty(Battles[i].Accolades[j].Uid))
                    {
                        Battles[i].Accolades[j].UpdateIdentifier();
                        saveAccoladeData = true;
                    }
                }
            }

            if(saveAccoladeData)
            {
                Debug.LogError("Updated Accolade Identifiers.");
                Save();
            }
        }

        [Button]
        public void FixDependencies()
        {
            var identifiers = new List<string>();

            for (var i = 0; i < Battles.Count; i++)
            {
                if(!identifiers.Contains(Battles[i].Uid))
                {
                    identifiers.Add(Battles[i].Uid);
                }
                else
                {
                    Debug.LogError($"Identifier is not unique: {Battles[i].Uid} [{i}]");
                }
            }
        }

        public void Save()
        {
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }

        public GameVfxData GetBattleVfx(string uid)
        {
            VfxData ??= new List<GameVfxData>();
            return VfxData.Find(vfx => vfx.Uid == uid);
        }

        public GameVfxData GetBattleVfxByName(string vfxName)
        {
            VfxData ??= new List<GameVfxData>();
            return VfxData.Find(vfx => vfx.vfxName == vfxName);
        }

        public BattleData GetNewBattleData()
        {
            var battleData = new BattleData();
            battleData.Init();
            Battles.Add(battleData);
            return battleData;
        }
        public BattleData GetBattleData(string uid)
        {
            if (string.IsNullOrEmpty(uid)) return null;
            return Battles.Find(x => x.Uid == uid);
        }

        public BattleData GetBattleDataByName(string battleName)
        {
            return Battles.Find(x => x.BattleName == battleName);
        }

        public void DeleteBattle(string uid)
        {
            var battle = GetBattleData(uid);
            if (battle != null)
            {
                Battles.Remove(battle);
                Save();
            }
        }
    }
}