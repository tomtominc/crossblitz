using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Utils;
using UnityEngine;

namespace CrossBlitz.Databases
{
    public enum AchievementType
    {
        None,
        Battle,
    }

    public enum AchievementScoreType
    {
        None,
        Played,
        StatusInflict,
        Destroy,
        Damage,
    }

    [CreateAssetMenu()]
    public class AchievementDatabase : ScriptableObject
    {
        public List<AchievementData> achievements;

        public void Init()
        {

        }

        public AchievementData CreateAchievement()
        {
            var achievement = new AchievementData();
            achievement.UpdateIdentifier();
            achievement.name = "New Achievement";
            achievement.description = "Nothing here.";
            achievement.type = AchievementType.Battle;

            achievements.Add(achievement);

            return achievement;
        }

        public AchievementData GetAchievement(string achievementId)
        {
            return achievements.Find(achievement => achievement.Uid == achievementId);
        }

        public void Save()
        {
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }
    }

    [Serializable]
    public class AchievementData : UniqueItem
    {
        public AchievementType type;
        public string name;
        public string description;
        public string rewardId;

        public AchievementScoreType scoreType;
        public FilterCondition targetFilters;
        public FilterConditionValues advancedFilters;
        public int targetScore;
    }
}