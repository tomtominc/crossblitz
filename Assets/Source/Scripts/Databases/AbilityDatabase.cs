using System.Collections.Generic;
using CrossBlitz.Card;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.Databases
{
    [CreateAssetMenu]
    public class AbilityDatabase : ScriptableObject
    {
        public List<Ability> AbilitiesList;
        public List<Ability> GiveAbilitiesList;
        public List<Ability> ConditionalAbilitiesList;

        private Dictionary<string, List<Ability>> Abilities;
        private Dictionary<string, List<Ability>> GivenAbilities;
        private Dictionary<string, List<Ability>> ConditionalAbilities;
        private bool dirty = true;

        public void Init()
        {
            Abilities = new Dictionary<string, List<Ability>>();
            GivenAbilities = new Dictionary<string, List<Ability>>();
            ConditionalAbilities = new Dictionary<string, List<Ability>>();

            for (var i = 0; i < AbilitiesList.Count; i++)
            {
                var ability = AbilitiesList[i];

                if (!Abilities.ContainsKey(ability.linkedToCardId))
                {
                    Abilities.Add(ability.linkedToCardId, new List<Ability> { ability });
                }
                else
                {
                    Abilities[ability.linkedToCardId].Add( ability );
                }
            }

            for (var i = 0; i < GiveAbilitiesList.Count; i++)
            {
                var ability = GiveAbilitiesList[i];

                if (!GivenAbilities.ContainsKey(ability.linkedToCardId))
                {
                    GivenAbilities.Add(ability.linkedToCardId, new List<Ability> { ability });
                }
                else
                {
                    GivenAbilities[ability.linkedToCardId].Add( ability );
                }
            }

            for (var i = 0; i < ConditionalAbilitiesList.Count; i++)
            {
                var ability = ConditionalAbilitiesList[i];

                if (!ConditionalAbilities.ContainsKey(ability.linkedToCardId))
                {
                    ConditionalAbilities.Add(ability.linkedToCardId, new List<Ability> { ability });
                }
                else
                {
                    ConditionalAbilities[ability.linkedToCardId].Add( ability );
                }
            }

            dirty = false;
        }

        public void Save()
        {
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }

        public List<Ability> GetAbilities(string cardId)
        {
            if (Abilities == null || dirty)
            {
                Init();
            }

            if (!Abilities.ContainsKey(cardId))
            {
                return new List<Ability>();
            }

            return Abilities[cardId];
        }

        public List<Ability> GetGivenAbilities(string cardId)
        {
            if (GivenAbilities == null || dirty)
            {
                Init();
            }

            if (!GivenAbilities.ContainsKey(cardId))
            {
                return new List<Ability>();
            }

            return GivenAbilities[cardId];
        }

        public List<Ability> GetConditionalAbilities(string cardId)
        {
            if (ConditionalAbilities == null || dirty)
            {
                Init();
            }

            if (!ConditionalAbilities.ContainsKey(cardId))
            {
                return new List<Ability>();
            }

            return ConditionalAbilities[cardId];
        }

        public void AddNewAbility(List<Ability> abilityList,string cardId, Ability ability = null)
        {
            if (ability == null)
            {
                ability = new Ability { linkedToCardId = cardId };
            }

            abilityList.Add( ability );
            dirty = true;
        }

        public string CopyToJson(List<Ability> abilityList, string cardId, int index)
        {
            var curr = 0;

            for (var i = 0; i < abilityList.Count; i++)
            {
                var ability = abilityList[i];

                if (ability.linkedToCardId == cardId)
                {
                    if (curr == index)
                    {
                        return ability.ToJson();
                    }

                    curr++;
                }
            }

            return string.Empty;
        }

        public void PasteFromJson(List<Ability> abilityList, string cardId, string json, int index = -1)
        {
            if (string.IsNullOrEmpty(json))
            {
                Debug.LogError("Failed to paste, nothing on the clipboard.");
                return;
            }

            var ability = Ability.FromJson(json);

            if (ability == null)
            {
                Debug.LogError("Failed to paste, string is not json.");
                return;
            }

            ability.linkedToCardId = cardId;

            if (index < 0)
            {
                AddNewAbility(abilityList,cardId, ability);
            }
            else
            {
                var curr = 0;

                for (var i = 0; i < abilityList.Count; i++)
                {
                    if (abilityList[i].linkedToCardId == cardId)
                    {
                        if (curr == index)
                        {
                            abilityList[i] = ability;
                        }

                        curr++;
                    }
                }
            }

            dirty = true;
        }

        public void DeleteAbility(List<Ability> abilityList, string cardId, int index)
        {
            var curr = 0;
            var removeAt = -1;

            for (var i = 0; i < abilityList.Count; i++)
            {
                var ability = abilityList[i];

                if (ability.linkedToCardId == cardId)
                {
                    if (curr == index)
                    {
                        removeAt = i;
                        break;
                    }

                    curr++;
                }
            }

            if (removeAt > -1)
            {
                abilityList.RemoveAt(removeAt);
                dirty = true;
            }
        }

        public void MoveAbilityDown(List<Ability> abilityList, string cardId, int index)
        {
            if (abilityList == null || index == 0)
            {
                return;
            }

            var curr = 0;

            var swapAIndex = -1;
            var swapBIndex = -1;

            for (var i = 0; i < abilityList.Count; i++)
            {
                var ability = abilityList[i];

                if (ability.linkedToCardId == cardId)
                {
                    if (curr == index)
                    {
                        swapBIndex = i;
                    }

                    if (curr == index + 1)
                    {
                        swapAIndex = i;
                        break;
                    }

                    curr++;
                }
            }

            if (swapAIndex > -1 && swapBIndex > -1 && swapAIndex != swapBIndex)
            {
                (abilityList[swapAIndex], abilityList[swapBIndex]) =
                    (abilityList[swapBIndex], abilityList[swapAIndex]);

                dirty = true;
            }
        }

        public void MoveAbilityUp(List<Ability> abilityList, string cardId, int index)
        {
            if (abilityList == null || index == 0)
            {
                return;
            }

            var curr = 0;

            var swapAIndex = -1;
            var swapBIndex = -1;

            for (var i = 0; i < abilityList.Count; i++)
            {
                var ability = abilityList[i];

                if (ability.linkedToCardId == cardId)
                {
                    if (curr == index)
                    {
                        swapBIndex = i;
                        break;
                    }

                    if (curr == index - 1)
                    {
                        swapAIndex = i;
                    }

                    curr++;
                }
            }

            if (swapAIndex > -1 && swapBIndex > -1 && swapAIndex != swapBIndex)
            {
                (abilityList[swapAIndex], abilityList[swapBIndex]) =
                    (abilityList[swapBIndex], abilityList[swapAIndex]);

                dirty = true;
            }
        }
    }
}