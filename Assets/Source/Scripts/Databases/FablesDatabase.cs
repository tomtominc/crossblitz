using System;
using System.Collections.Generic;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Fables;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Tools;
using CrossBlitz.Hero;
using Newtonsoft.Json;
using QFSW.QC;
using Sirenix.OdinInspector;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;


namespace CrossBlitz.Databases
{
    [CreateAssetMenu]
    public class FablesDatabase : ScriptableObject
    {
        public List<FableBookData> Books;
        //public List<FableChapterData> Chapters;
        public List<TileDecorationData> DecorationData;
        public List<TileTypeData> TileTypeData;

        public void Init()
        {

        }

        [Button]
        private void RefactorBooksIntoChapters()
        {
            //Chapters = new List<FableChapterData>();

        }

        public void Save()
        {
#if UNITY_EDITOR

            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }

        public TileDecorationData GetDecorationData(string id)
        {
            var deco = DecorationData.Find(d => d.Id == id);

            if (deco == null)
            {
                Debug.LogError($"Decoration {id} needed to be added. This should only happen once.");
                deco = new TileDecorationData { Id = id, PixelOffset = Vector2Int.zero};
                DecorationData.Add(deco);
                Save();
            }

            return deco;
        }

        public TileTypeData GetTileTypeData(string id)
        {
            var tileType = TileTypeData.Find(t => t.TileId == id);

            if (tileType == null)
            {
                tileType = new TileTypeData();
                tileType.TileId = id;
                TileTypeData.Add(tileType);
                Save();
            }

            return tileType;
        }


        public FableChapterData AddChapter(CreateNewFableParameters createParams)
        {
            var fableChapterData = new FableChapterData();
            fableChapterData.UpdateIdentifier();
            fableChapterData.Enabled = true;
            fableChapterData.ChapterHero = createParams.hero;
            fableChapterData.ChapterNumber = createParams.chapterNumber;
            fableChapterData.ChapterFaction = HeroData.GetFactionForHero(createParams.hero);
            fableChapterData.ChapterName = $"{fableChapterData.ChapterHero} Book {fableChapterData.BookNumber} Chapter {fableChapterData.ChapterNumber}";
            fableChapterData.ChapterId = $"{fableChapterData.ChapterHero}-{fableChapterData.BookNumber}-{fableChapterData.ChapterNumber}";
            fableChapterData.mapId = string.Empty;
            fableChapterData.AvailableSets = HeroData.GetAvailableSetsForHero(createParams.hero);
            fableChapterData.PrizeCardPool = new List<string>();
            fableChapterData.ExcludedPrizeCards = new List<string>();

            var book = GetOrCreateBook(fableChapterData.ChapterHero, createParams.bookNumber);
            book.Chapters.Add(fableChapterData);

            Save();

            return fableChapterData;
        }

        public FableBookData GetBookByUid(string uid)
        {
            return Books.Find(book => book.Uid == uid);
        }
        public FableBookData GetBook(string bookId)
        {
            return Books.Find(book => book.BookId == bookId);
        }

        public FableBookData GetBook(string chapterHero, int bookNumber)
        {
            return Books.Find(book => book.BookHero == chapterHero && book.BookNumber == bookNumber);
        }

        public FableBookData GetBookByTile(FableTileData tile)
        {
            FableBookData book = null;

            for (var i = 0; i < Books.Count; i++)
            {
                for (var j = 0; j < Books[i].Chapters.Count; j++)
                {
                    if (string.IsNullOrEmpty(Books[i].Chapters[j].mapId)) continue;

                    var map = Db.MapDatabase.GetMap(Books[i].Chapters[j].mapId);

                    for (var k = 0; k < map.Rooms.Count; k++)
                    {
                        for (var l = 0; l < map.Rooms[k].Tiles.Count; l++)
                        {
                            if (map.Rooms[k].Tiles[l].uid == tile.uid)
                            {
                                book = Books[i];
                                break;
                            }
                        }
                    }
                }
            }

            if (book == null)
            {
                for (var i = 0; i < Db.MapDatabase.MapData.Count; i++)
                {
                    var map = Db.MapDatabase.MapData[i];

                    if (!string.IsNullOrEmpty(map.BookLinkUid))
                    {
                        for (var j = 0; j < map.Rooms.Count; j++)
                        {
                            for (var k = 0; k < map.Rooms[j].Tiles.Count; k++)
                            {
                                if (map.Rooms[j].Tiles[k].uid == tile.uid)
                                {
                                    book = GetBook(map.BookLinkUid);
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            return book;
        }

        public FableChapterData GetChapterByBattle(string battleUid)
        {
            var battle = Db.BattleDatabase.GetBattleData(battleUid);
            return battle.GetChapter();
        }

        public FableBookData GetOrCreateBook(string chapterHero, int bookNumber)
        {
            var book = GetBook(chapterHero, bookNumber);

            if (book == null)
            {
                book = new FableBookData();
                book.UpdateIdentifier();
                book.Enabled = true;
                book.BookId = $"{chapterHero}-{bookNumber}";
                book.BookName = $"{chapterHero} Book {bookNumber}";
                book.BookNumber = bookNumber;
                book.BookHero = chapterHero;
                book.Chapters = new List<FableChapterData>();
                Books.Add(book);

                Debug.LogWarning($"Needed to create a book {chapterHero} {bookNumber}");
            }

            return book;
        }

        public FableBookData AddNewBook(string bookHero)
        {
            var book = GetOrCreateBook(bookHero, Books.Count + 2);

            AddChapter(new CreateNewFableParameters
            {
                hero = bookHero,
                bookNumber = book.BookNumber,
                chapterNumber = 1,
            });

            return book;
        }

        public void DeleteBook(string bookHero, int bookNumber)
        {
            var book = GetBook(bookHero, bookNumber);

            if (book != null)
            {
                Books.Remove(book);
            }
        }

        public int GetBookCountByHero(string bookHero)
        {
            return Books.FindAll(book => book.BookHero == bookHero).Count;
        }

        public void DeleteChapter(string chapterUid)
        {
            FableChapterData chapter = null;

            for (var i = 0; i < Books.Count; i++)
            {
                chapter = Books[i].Chapters.Find(c => c.Uid == chapterUid);

                if (chapter != null)
                {
                    Books[i].Chapters.Remove(chapter);
                    break;
                }
            }

            Save();
        }

        public List<string> GetBookDirectories()
        {
            var directories = new List<string>();

            for (var i = 0; i < Books.Count; i++)
            {
                for (var j = 0; j < Books[i].Chapters.Count; j++)
                {
                    directories.Add($"{Books[i].BookHero}/Book {Books[i].BookNumber}/Chapter {Books[i].Chapters[j].ChapterNumber}");
                }
            }

            return directories;
        }

        public FableChapterData GetChapter(string chapterUid)
        {
            FableChapterData chapter = null;

            for (var i = 0; i < Books.Count; i++)
            {
                chapter = Books[i].Chapters.Find(c => c.Uid == chapterUid);
                if (chapter != null) break;
            }

            return chapter;
        }

        public FableChapterData GetChapterByChapterId(string chapterId)
        {
            FableChapterData chapter = null;

            for (var i = 0; i < Books.Count; i++)
            {
                chapter = Books[i].Chapters.Find(c => c.ChapterId == chapterId);
                if (chapter != null) break;
            }

            return chapter;
        }

        public FableChapterData GetChapterByRoomUid(string roomUid)
        {
            FableChapterData chapter = null;

            for (var i = 0; i < Books.Count; i++)
            {
                for (var j = 0; j < Books[i].Chapters.Count; j++)
                {
                    if (string.IsNullOrEmpty(Books[i].Chapters[j].mapId)) continue;

                    var map = Db.MapDatabase.GetMap(Books[i].Chapters[j].mapId);

                    for (var k = 0; k < map.Rooms.Count; k++)
                    {
                        if (map.Rooms[k].Uid == roomUid)
                        {
                            chapter = Books[i].Chapters[j];
                            break;
                        }
                    }
                }
            }

            return chapter;
        }



#if UNITY_EDITOR

        public const string AssetPath = "Assets/Source/Database/FablesDatabase.asset";
        public static FablesDatabase GetOrCreateDatabase()
        {
            var database = UnityEditor.AssetDatabase.LoadAssetAtPath<FablesDatabase>(AssetPath);

            if (database == null)
            {
                database = CreateInstance<FablesDatabase>();
                database.name = "FablesDatabase";
                UnityEditor.AssetDatabase.CreateAsset(database, AssetPath);
                UnityEditor.AssetDatabase.SaveAssets();
                UnityEditor.AssetDatabase.Refresh();
            }

            return database;
        }
#endif

        [Command("open-fable")]
        public static void OpenFable(string bookName, int bookNumber, int chapterNumber)
        {
            FableController.SetChapter(bookName, bookNumber, chapterNumber);
            Events.Publish(null, EventType.OnPlayBook, null);
        }

        [Command("list-fables")]
        public static void ListFables()
        {
            foreach (var fable in Db.FablesDatabase.GetBookDirectories())
            {
                Debug.Log(fable);
            }
        }
    }
}