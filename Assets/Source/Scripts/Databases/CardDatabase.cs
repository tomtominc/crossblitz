using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Intelligence;
using QFSW.QC.Utilities;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.Databases
{
    [System.Serializable]
    public class CardSortingData
    {
        public enum SortBy
        {
            Faction,
            Faction2,
            Class,
            Cost,
            Power,
            Health,
            AbilityKeyword,
            ModifyStatsConditionIsNotNone,
            CardTypes,
            HasText,
            AbilityTargetCondition,
            AbilityTargetConditionGrouped,
            AbilityTriggerCondition,
            AbilityTriggerConditionGrouped,
            AbilityTargetAndTriggerConditions,
            AbilityTargetAndTriggerConditionsGrouped,
            AbilityTriggerType,
            Set,
            ByChapterSeen,
            Priority,
            Rarity,
            CreationDate,
            HasAnyConditions,
            UsesCustomEffect,
            ByCustomEffectPrefab
        }

        public SortBy sortBy;
        [ShowIf("@this.sortBy == SortBy.HasText")]
        public string sortText;

        [ShowIf("@this.sortBy == SortBy.ByChapterSeen")]
        public FablesDatabase fableDatabase;
        [ShowIf("@this.sortBy == SortBy.ByChapterSeen")]
        public MapDatabase mapDatabase;
        [ShowIf("@this.sortBy == SortBy.ByChapterSeen")]
        public BattleDatabase battleDatabase;
    }
    [CreateAssetMenu]
    public class CardDatabase : ScriptableObject
    {
        public const string AssetPath = "Assets/Source/Database/CardDatabase.asset";

        public CardSortingData SortingData;

        [Searchable]
        public List<CardData> Cards;

        public Dictionary<string, CardData> CardsById;
        public Dictionary<string, CardData> CardsByName;

        public void Init(bool refreshFromExcel = true)
        {
            if (refreshFromExcel)
            {
                // var gdeCards = GDEDataManager.GetAllItems<GDECardData>();
                //
                // for (var i = 0; i < gdeCards.Count; i++)
                // {
                //     var gdeCard = gdeCards[i];
                //     var card = Cards.Find(h => h.id == gdeCard.Key);
                //
                //     if (card == null)
                //     {
                //         var newCard = new CardData {id = gdeCard.Key};
                //         newCard.ConvertData();
                //         Cards.Add(newCard);
                //     }
                //     else if (!Application.isPlaying)
                //     {
                //         card.ConvertData();
                //     }
                // }
            }

            Cards = Cards.OrderBy(c => c.name).ToList();
            Cards = Cards.DistinctBy(c => c.id).ToList();

            CardsById = new Dictionary<string, CardData>();
            CardsByName = new Dictionary<string, CardData>();

            for (var i = 0; i < Cards.Count; i++)
            {
                CardsById.Add(Cards[i].id, Cards[i]);
                CardsByName.Add(Cards[i].id, Cards[i]);
            }
        }

        #if UNITY_EDITOR

        [Button]
        public void SetupInitialCreationDates()
        {
            // for (var i = 0; i < Cards.Count; i++)
            // {
            //     if (Cards[i].changelogs == null || Cards[i].changelogs.Count <= 0)
            //     {
            //         Cards[i].changelogs = new List<CardData.Changelog>
            //         {
            //             new CardData.Changelog { time = new DateTime(2022, 7, 5).Ticks, comment = "Created" }
            //         };
            //     }
            // }
        }

        [Button]
        public void FixAnyNameAbbreviations()
        {
            for (var i = 0; i < Cards.Count; i++)
            {
                if (string.IsNullOrEmpty( Cards[i].nameAbbrev ))
                {
                    Cards[i].nameAbbrev = Cards[i].name;
                }
            }
        }

        public static CardDatabase GetOrCreateDatabase()
        {
            var cardDatabase = UnityEditor.AssetDatabase.LoadAssetAtPath<CardDatabase>(AssetPath);

            // if (cardDatabase == null)
            // {
            //     cardDatabase = CreateInstance<CardDatabase>();
            //     cardDatabase.name = "CardDatabase.asset";
            //     UnityEditor.AssetDatabase.CreateAsset(cardDatabase, AssetPath);
            //     UnityEditor.EditorUtility.SetDirty(cardDatabase);
            //     UnityEditor.AssetDatabase.SaveAssets();
            //     UnityEditor.AssetDatabase.Refresh();
            // }

            return cardDatabase;
        }
        #endif

        public void Save()
        {
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();

#endif
        }

        public void DeleteCard(string cardId)
        {
            var card = GetCard(cardId);

            if (card != null)
            {
                Cards.Remove(card);
                CardsById.Remove(card.id);
                CardsByName.Remove(card.name);
            }
        }

        public Dictionary<string, CardData> GetCardDictionaryMap()
        {
            var cardDic = new Dictionary<string, CardData>();

            for (var i = 0; i < Cards.Count; i++)
            {
                cardDic.Add(Cards[i].id, Cards[i]);
            }

            return cardDic;
        }

        [Button]
        public void SetupAbilityConditions()
        {
            // for (var i = 0; i < Cards.Count; i++)
            // {
            //     var card = Cards[i];
            //
            //     for (var j = 0; j < card.abilities.Count; j++)
            //     {
            //         var ability = card.abilities[j];
            //
            //         switch (ability.keyword)
            //         {
            //             case AbilityKeyword.MODIFY_POWER:
            //             case AbilityKeyword.MODIFY_HEALTH_AND_POWER:
            //             case AbilityKeyword.HEAL:
            //             case AbilityKeyword.MODIFY_HEALTH:
            //             case AbilityKeyword.MODIFY_COST:
            //             case AbilityKeyword.COST_REDUCED_BY_CONDITION:
            //                 ability.abilityCondition = new AbilityCondition();
            //                 ability.abilityCondition.condition = ability.modifyStatsCondition.condition;
            //                 ability.abilityCondition.filters = ability.modifyStatsCondition.targetConditions;
            //                 ability.abilityCondition.filterValues = ability.modifyStatsCondition.targetAdvancedValues;
            //                 ability.abilityCondition.faction = ability.modifyStatsCondition.faction;
            //                 ability.abilityCondition.@class = ability.modifyStatsCondition.@class;
            //                 ability.abilityCondition.cardId = ability.modifyStatsCondition.cardId;
            //                 break;
            //         }
            //     }
            // }

            Save();
        }

        [Button]
        public void AddIngredientsToCards(bool forceUpdate)
        {
            for (var i = 0; i < Cards.Count; i++)
            {
                // if (!Cards[i].craftingData.craftable)
                // {
                //     continue;
                // }

                // if (Cards[i].set == CardSet.Token || Cards[i].set == CardSet.Deprecated)
                // {
                //     Cards[i].craftingData.craftable = false;
                //     continue;
                // }
                //
                // if (Cards[i].type == CardType.Relic || Cards[i].type == CardType.Elder_Relic)
                // {
                //     Cards[i].craftingData.craftable = false;
                //     continue;
                // }

                AddIngredientsToCard(Cards[i], forceUpdate);
            }

            Save();
        }

        public void AddIngredientsToCard(CardData card, bool forceUpdate=true)
        {
            if (forceUpdate || card.craftingData.ingredients == null || card.craftingData.ingredients.Count <= 0)
            {
                var ingredients = Crafting.GetIngredientsForCard(card);

                card.craftingData = new CardCraftingData();
                card.craftingData.craftable = true;
                card.craftingData.ingredients = new List<ItemValue>();
                card.craftingData.scraps = new List<ItemValue>();
                card.craftingData.cost = Crafting.GetCraftingCost(card.rarity);

                for (var j = 0; j < ingredients.Count; j++)
                {
                    card.craftingData.ingredients.Add(ingredients[j]);

                    var scrapsData = new ItemValue()
                    {
                        Count = (int) (ingredients[j].Count * 0.25f),
                        ItemUid = ingredients[j].ItemUid
                    };

                    card.craftingData.scraps.Add(scrapsData);
                }
            }
        }

        [Button]
        public void PrintCardList(bool withDescriptions)
        {
            var print = string.Empty;
            var cards = new List<CardData>(Cards);
            cards = cards.OrderBy(c => c.faction).ThenBy(c => c.type != CardType.Power).ThenBy(c => c.cost).ThenBy(c => c.name).ToList();

            var currFaction = Faction.All;

            for (var i = 0; i < cards.Count; i++)
            {
                var card = cards[i];

                if (currFaction != card.faction)
                {
                    currFaction = card.faction;
                    print += $"// {currFaction} ({cards.FindAll(c => c.faction == currFaction).Count})\n";
                }

                var additionalInfo = withDescriptions ? " " + card.description : string.Empty;
                print += $"({card.cost}) {card.name}{additionalInfo}\n";
            }

            Debug.Log(print);
        }

        public bool CreateCard(string id, out CardData cardData)
        {
            if (string.IsNullOrEmpty(id) || id.Contains(" "))
            {
                cardData = null;
                return false;
            }

            var copiedCharacter = GetCard(id);

            if (copiedCharacter != null)
            {
                cardData = null;
                return false;
            }

            cardData = new CardData();
            cardData.id = id;
            cardData.name = id;
            cardData.type = CardType.Minion;
            cardData.faction = Faction.Neutral;
            // cardData.abilities = new List<Ability>();
            // cardData.givenAbilities = new List<Ability>();
            // cardData.conditionalAbilities = new List<Ability>();
            cardData.limitBreak = new LimitBreakData();
            cardData.limitBreak.cardIds = new List<string>();
            cardData.dialogueSnippets = new List<DialogueSnippetData>();
            cardData.aiProperties = new AiProperties();
            cardData.aiProperties.Init();

            Cards.Add(cardData);
            CardsById.Add(cardData.id, cardData);
            CardsByName.Add(cardData.name, cardData);

            // if (Db.AbilityDatabase != null)
            // {
            //     Db.AbilityDatabase.Abilities.Add( cardData.id, new List<Ability>());
            //     Db.AbilityDatabase.GivenAbilities.Add( cardData.id, new List<Ability>());
            //     Db.AbilityDatabase.ConditionalAbilities.Add( cardData.id, new List<Ability>());
            // }

            AddIngredientsToCard(cardData);

            return true;
        }

        public CardData GetCard(string cardId)
        {
            if (string.IsNullOrEmpty(cardId))
            {
                return null;
            }

            if (CardsById.ContainsKey(cardId))
            {
                return CardsById[cardId];
            }

            return Cards.Find(c => c.id == cardId);
        }

        public CardData GetCardByName(string cardName)
        {
            if (CardsByName.ContainsKey(cardName))
            {
                return CardsByName[cardName];
            }

            return Cards.Find(card => card.name == cardName);
        }

        public string GetCardIdFromName(string cardName)
        {
            return GetCardByName(cardName)?.id;
        }

        public int GetCollectableCardCount()
        {
            return Cards.FindAll(card => card.set != CardSet.Deprecated && card.set != CardSet.Token).Count;
        }


        public List<CardData> GetCollectableCards()
        {
            return Cards.FindAll(card => card.set != CardSet.Deprecated && card.set != CardSet.Token);
        }

        public List<CardData> GetValidGameCards()
        {
            return Cards.FindAll(card =>
                card.set != CardSet.Deprecated &&
                card.set != CardSet.Token &&
                card.type != CardType.Power &&
                card.type != CardType.Relic &&
                card.GetAbilities().Count > 0);
        }

        public List<CardData> GetRedeemCards(CardFilter filter, Faction clientFaction, Faction opponentFaction)
        {
            var allCards = Cards.FindAll(card => card.type != CardType.Power &&
                                                 card.type != CardType.Relic &&
                                                 card.set != CardSet.Token &&
                                                 card.set != CardSet.Deprecated);

            if (filter.limitToSameFaction)
            {
                allCards.RemoveAll(c => c.faction != clientFaction && c.faction != Faction.Neutral);
            }

            if (filter.fromOpponentsFaction)
            {
                allCards.RemoveAll(c => c.faction != opponentFaction);
            }

            if (filter.fromClass != Class.None)
            {
                allCards.RemoveAll(c => c.@class != filter.fromClass);
            }

            if (filter.fromType != CardType.None)
            {
                allCards.RemoveAll( c => c.type != filter.fromType);
            }

            if (filter.fromRarity != Rarity.All)
            {
                allCards.RemoveAll(c => c.rarity != filter.fromRarity);
            }

            if (filter.keyword != AbilityKeyword.NONE)
            {
                allCards.RemoveAll(c => !c.GetAbilities().Exists( k => k.keyword == filter.keyword));
            }

            if (filter.trigger != TriggerType.NONE)
            {
                allCards.RemoveAll(c => !c.GetAbilities().Exists( k => k.triggerType == filter.trigger));
            }

            if (filter.withStat != FilterConditionValues.Stat.None && filter.formula != FilterConditionValues.StatFormula.None)
            {
                allCards.RemoveAll(c =>
                {
                    var score = 0;

                    switch (filter.withStat)
                    {
                        case FilterConditionValues.Stat.Health:
                            score = c.health;
                            break;
                        case FilterConditionValues.Stat.Power:
                            score = c.power;
                            break;
                        case FilterConditionValues.Stat.ManaCost:
                            score = c.GetCost();
                            break;
                    }

                    switch (filter.formula)
                    {
                        case FilterConditionValues.StatFormula.LessThanOrEqualTo:
                            return score > filter.statAmount;
                        case FilterConditionValues.StatFormula.LessThan:
                            return score >= filter.statAmount;
                        case FilterConditionValues.StatFormula.GreaterThanOrEqualTo:
                            return score < filter.statAmount;
                        case FilterConditionValues.StatFormula.GreaterThan:
                            return score <= filter.statAmount;
                        case FilterConditionValues.StatFormula.Equal:
                            return score != filter.statAmount;
                    }

                    return true;
                });
            }

            if (filter.fromSameSpellSchoolAsLastPlayedSpell)
            {
                var cardIds = new List<string>();
                var history = GameServer.State.GetLastHistoryOf(CommandType.PLAY);
                var lastSpell = GameServer.State.GetCard(history.SourceUid);
                var spellSchool = lastSpell == null ? Class.None : lastSpell.data.@class;

                for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
                {
                    var card = Db.CardDatabase.Cards[i];

                    if (card.type == CardType.Spell && card.@class == spellSchool)
                    {
                        cardIds.Add(card.id);
                    }
                }

                allCards.RemoveAll(c => !cardIds.Contains(c.id));
            }

            if (filter.fromDiscardedCardsThisGame)
            {
                var cardIds = new List<string>();

                for (var i = 0; i < GameServer.State.History.Count; i++)
                {
                    var history = GameServer.State.History[i];

                    if (history is DiscardCardCommand discard)
                    {
                        for (var j = 0; j < discard.TargetUids.Count; j++)
                        {
                            var card = GameServer.State.GetCard(discard.TargetUids[j]);
                            if (card?.data != null)
                            {
                                cardIds.Add(card.data.id);
                            }
                        }
                    }
                }

                allCards.RemoveAll(c => !cardIds.Contains(c.id));
            }

            if (filter.fromAnotherFaction)
            {
                allCards.RemoveAll(c => c.faction == clientFaction || c.faction == Faction.Neutral);
            }

            return allCards;
        }

        public List<CardData> GetCardsByFaction(Faction limitedFaction)
        {
            return Cards.FindAll(card => card.faction == limitedFaction &&
                                         card.set != CardSet.Token &&
                                         card.set != CardSet.Deprecated &&
                                         card.type != CardType.Power &&
                                         card.type != CardType.Relic);
        }

        public List<CardData> GetCards(Predicate<CardData> predicate)
        {
            return Cards.FindAll(predicate);
        }

        public List<CardData> GetCardsByClass(Class @class, Faction limitedFaction)
        {
            return GetCardsByClass(Cards, @class, limitedFaction);
        }

        public List<CardData> GetCardsByClass(List<CardData> cards, Class @class, Faction limitedFaction)
        {
            if (limitedFaction == Faction.All || limitedFaction == Faction.None)
                return cards.FindAll(card => card.@class == @class);

            return cards.FindAll(card => card.@class == @class &&
                                         card.faction == limitedFaction &&
                                         card.set != CardSet.Token &&
                                         card.set != CardSet.Deprecated &&
                                         card.type != CardType.Power &&
                                         card.type != CardType.Relic);
        }

        public List<CardData> GetCardsByType(CardType type, Faction limitedFaction)
        {
            return GetCardsByType(Cards, type, limitedFaction);
        }

        public List<CardData> GetCardsByType(List<CardData> cards, CardType type, Faction limitedFaction)
        {
            if (limitedFaction == Faction.All || limitedFaction == Faction.None)
                return cards.FindAll(card => card.type == type);

            return cards.FindAll(card => card.type == type &&
                                         card.faction == limitedFaction &&
                                         card.set != CardSet.Token &&
                                         card.set != CardSet.Deprecated &&
                                         card.type != CardType.Power &&
                                         card.type != CardType.Relic);
        }


        public List<CardData> GetCardsByStatFormula(FilterConditionValues.Stat stat, FilterConditionValues.StatFormula formula, int statAmount, Faction limitedFaction)
        {
            return GetCardsByStatFormula(Cards, stat, formula, statAmount,limitedFaction);
        }

        public List<CardData> GetCardsByStatFormula(List<CardData> cards, FilterConditionValues.Stat stat, FilterConditionValues.StatFormula formula, int statAmount, Faction limitedFaction)
        {
            return cards.FindAll(card =>
            {
                if (limitedFaction != Faction.None && limitedFaction != Faction.All && card.faction != limitedFaction)
                {
                    return false;
                }

                var statCurrent = 0;

                switch (stat)
                {
                    case FilterConditionValues.Stat.Health:
                        statCurrent = card.GetHealth();
                        break;
                    case FilterConditionValues.Stat.Power:
                        statCurrent = card.GetPower();
                        break;
                    case FilterConditionValues.Stat.ManaCost:
                        statCurrent = card.GetCost();
                        break;
                }

                switch (formula)
                {
                    case FilterConditionValues.StatFormula.Equal: return statCurrent == statAmount;
                    case FilterConditionValues.StatFormula.GreaterThan: return statCurrent > statAmount;
                    case FilterConditionValues.StatFormula.GreaterThanOrEqualTo: return statCurrent >= statAmount;
                    case FilterConditionValues.StatFormula.LessThan: return statCurrent < statAmount;
                    case FilterConditionValues.StatFormula.LessThanOrEqualTo: return statCurrent <= statAmount;
                }

                return true;
            });
        }

        public List<ItemValue> GetFullCollection(Faction faction)
        {
            var itemList = new List<ItemValue>();

            for (var i = 0; i < Cards.Count; i++)
            {
                if (Cards[i].set != CardSet.Token && Cards[i].set != CardSet.Deprecated &&
                     (faction == Faction.All || faction == Cards[i].faction || Cards[i].faction == Faction.Neutral))
                {
                    itemList.Add(new ItemValue
                    {
                        ItemUid = Cards[i].id,
                        Count = DeckData.GetMaxCopies(Cards[i].rarity)
                    });
                }
            }

            return itemList;
        }

        public Dictionary<string, ItemValue> GetFullCollectionAsDictionary(Faction faction)
        {
            var itemList = new Dictionary<string, ItemValue> ();

            for (var i = 0; i < Cards.Count; i++)
            {
                if (Cards[i].set != CardSet.Token && Cards[i].set != CardSet.Deprecated &&
                     (faction == Faction.All || faction == Cards[i].faction || Cards[i].faction == Faction.Neutral))
                {
                    itemList.Add(Cards[i].id, new ItemValue
                    {
                        ItemUid = Cards[i].id,
                        Count = DeckData.GetMaxCopies(Cards[i].rarity)
                    });
                }
            }

            return itemList;
        }

        public int GetFullCollectionCardCountWithDuplicates(Faction faction)
        {
            var count = 0;

            for (var i = 0; i < Cards.Count; i++)
            {
                if (Cards[i].set != CardSet.Token && Cards[i].set != CardSet.Deprecated &&
                    (faction == Faction.All || faction == Cards[i].faction || Cards[i].faction == Faction.Neutral))
                {
                    count += DeckData.GetMaxCopies(Cards[i].rarity);
                }
            }

            return count;
        }

        public int GetFullCollectionCardCountWithDuplicates(Faction faction, CardSet expansion)
        {
            var count = 0;

            for (var i = 0; i < Cards.Count; i++)
            {
                if ((expansion == CardSet.Core || Cards[i].set == CardSet.Core || Cards[i].set == expansion) &&
                    (faction == Faction.All || faction == Cards[i].faction || Cards[i].faction == Faction.Neutral))
                {
                    count += DeckData.GetMaxCopies(Cards[i].rarity);
                }
            }

            return count;
        }

    }
}