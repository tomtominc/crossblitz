using System.Collections.Generic;
using CrossBlitz.Items.Data;
using CrossBlitz.PlayFab.Data;
using GameDataEditor;
using QFSW.QC;
using Selectionator;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.Databases
{
    public class ItemDatabasePlayFab
    {
        public Dictionary<string, ItemData> Items;
    }
    [CreateAssetMenu]
    public class ItemDatabase : ScriptableObject
    {
        public List<ItemData> Items;
        public List<DropTable> DropTables;

        public void Init()
        {
            FixItemDependencies();
        }

        public ItemData GetItem(string itemId)
        {
            if (string.IsNullOrEmpty(itemId)) return null;
            return Items.Find(item => item.ItemId == itemId);
        }

        public ItemData GetItemByName(string itemName)
        {
            if (string.IsNullOrEmpty(itemName)) return null;
            return Items.Find(item => item.DisplayName == itemName);
        }

        public List<ItemData> GetItemsOfType(string itemType)
        {
            return Items.FindAll(item => item.ItemClass == itemType);
        }

        [Button]
        public void FixItemDependencies()
        {
            var save = false;
            for (var i = 0; i < Db.DeckRecipeDatabase.deckRecipes.Count; i++)
            {
                var recipe = Db.DeckRecipeDatabase.deckRecipes[i];
                var item = GetItem(recipe.Uid);

                if (item == null)
                {
                    item = new ItemData
                    {
                        ItemId = recipe.Uid,
                        DisplayName = recipe.deckData.name,
                        ItemClass = ItemClassType.Recipe,
                        Description = recipe.overview
                    };
                    Items.Add(item);
                    save = true;
                }

                if (item.DisplayName != recipe.deckData.name)
                {
                    item.DisplayName = recipe.deckData.name;
                    save = true;
                }
            }

            for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
            {
                var card = Db.CardDatabase.Cards[i];
                var item = GetItem(card.id);

                if (item == null)
                {
                    item = new ItemData
                    {
                        ItemId = card.id,
                        ItemClass = ItemClassType.Card,
                        ItemImageUrl = card.portraitUrl,
                        DisplayName = card.name,
                        Description = card.description,
                        CatalogVersion = "1",
                        Consumable = new ItemConsumableInfo
                        {
                            UsageCount = 1
                        },
                        IsStackable = true
                    };

                    Items.Add(item);

                    save = true;
                }

                if (item.DisplayName != card.name)
                {
                    item.DisplayName = card.name;
                    save = true;
                }
            }

            var ingredients = GDEDataManager.GetAllItems<GDEItemData>();

            for (var i = 0; i < ingredients.Count; i++)
            {
                var item = ingredients[i];
                if (item.ItemType == ItemClassType.Ingredient)
                {
                    var ing = GetItem(item.Key);

                    if (ing != null) continue;

                    ing = new ItemData
                    {
                        ItemId = item.Key,
                        ItemClass = ItemClassType.Ingredient,
                        ItemImageUrl = item.Icon,
                        DisplayName = item.Name,
                        Description = item.Description,
                        CatalogVersion = "1",
                        Consumable = new ItemConsumableInfo
                        {
                            UsageCount = 1
                        },
                        IsStackable = true
                    };
                    Items.Add(ing);

                    save = true;
                }
            }

            for (var i = Items.Count-1; i > -1; i--)
            {
                var item = Items[i];

                if (item.ItemClass == ItemClassType.Recipe)
                {
                    var recipe = Db.DeckRecipeDatabase.GetRecipe(item.ItemId);

                    if (recipe == null)
                    {
                        Items.RemoveAt(i);
                        save = true;
                    }
                }

                if (item.ItemClass == ItemClassType.Card)
                {
                    var card = Db.CardDatabase.GetCard(item.ItemId);
                    if (card == null)
                    {
                        Items.RemoveAt(i);
                        save = true;
                    }
                }

                if (item.ItemClass == ItemClassType.Ingredient)
                {
                    var ing = new GDEItemData(item.ItemId);
                    if (string.IsNullOrEmpty(ing.Key))
                    {
                        Items.RemoveAt(i);
                        save = true;
                        Debug.Log($"Removed {item.ItemId}!");
                    }
                }
            }

            if (save)
            {
                Save();
            }
        }

        /// <summary>
        /// Creates an item instance from an "item" these should have no bundle or container data.
        /// </summary>
        /// <param name="itemId">The item id to grant.</param>
        /// <param name="acquisition">How this player obtained the item.</param>
        /// <returns></returns>
        public ItemDataInstance CreateItemInstance(string itemId, ItemAcquisitionData acquisition)
        {
            var item = GetItem(itemId);

            if (item == null)
            {
                Debug.LogError($"Item with item Id {itemId} is null!");
                return null;
            }

            return CreateItemInstance(item, acquisition);
        }

        public ItemDataInstance CreateItemInstance(ItemData item, ItemAcquisitionData acquisition)
        {
            if (item.IsBundle())
            {
                Debug.LogError($"Item with item Id {item.ItemId} is a BUNDLE! Recursive bundles are not supported!");
                return null;
            }

            return ItemDataInstance.Convert(item,acquisition);
        }

        public DropTable GetDropTable(string dropTableId)
        {
            return DropTables.Find(droptable => droptable.TableId == dropTableId);
        }

        public ItemDataInstance EvaluateDropTable(string dropTableId, ItemAcquisitionData acquisition)
        {
            var dropTable = GetDropTable(dropTableId);

            if (dropTable == null)
            {
                Debug.LogError($"Drop Table with value {dropTableId} is null!");
                return null;
            }

            var selector = new Selector();
            var dropTableNode = selector.Single(dropTable.Nodes);

            if (dropTableNode.DropTableNodeType == DropTableNodeType.ItemId)
            {
                return CreateItemInstance(dropTableNode.ResultItem, acquisition);
            }

            return EvaluateDropTable(dropTableNode.ResultItem, acquisition);
        }

        public void Save()
        {
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }

        [Command("fix-item-image-urls")]
        public static void FixAndSaveImagePaths()
        {
            var itemDatabase = Db.ItemDatabase;

            for (var i = 0; i < itemDatabase.Items.Count; i++)
            {
                var itemName = itemDatabase.Items[i].DisplayName;
                var itemImage = itemName.Replace(" ", "-");
                itemDatabase.Items[i].ItemImageUrl = itemImage.ToLower();
            }

            itemDatabase.Save();
        }


#if UNITY_EDITOR

        public const string AssetPath = "Assets/Source/Database/ItemDatabase.asset";
        public static ItemDatabase GetOrCreateDatabase()
        {
            var database = UnityEditor.AssetDatabase.LoadAssetAtPath<ItemDatabase>(AssetPath);

            if (database == null)
            {
                Debug.LogError("Creating new item database!");
                database = CreateInstance<ItemDatabase>();
                database.name = "ItemDatabase";
                UnityEditor.AssetDatabase.CreateAsset(database, AssetPath);
                UnityEditor.AssetDatabase.SaveAssets();
                UnityEditor.AssetDatabase.Refresh();
            }

            return database;
        }
#endif
    }
}