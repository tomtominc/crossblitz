using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using UnityEngine;

namespace CrossBlitz.Databases
{
    [CreateAssetMenu]
    public class DeckDatabase : ScriptableObject
    {
        public List<DeckData> decks;

        public List<DeckData> GetDecksByFaction(Faction faction)
        {
            return decks.FindAll(deck => deck.faction == faction).ToList();
        }

        public void Init()
        {
        }
    }
}