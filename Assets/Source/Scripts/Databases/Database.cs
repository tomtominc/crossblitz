using Newtonsoft.Json;
using UnityEngine;

#if ENABLE_PLAYFABADMIN_API
using PlayFab;
using PlayFab.AdminModels;
#endif

namespace CrossBlitz.Databases
{
    public class Database
    {
        public static string CutsceneDatabaseKey = "CutsceneDatabase";

        public virtual string Key { get;  }
        public virtual void Init(){}

        public virtual void Save()
        {
#if ENABLE_PLAYFABADMIN_API
            var setTitleData = new SetTitleDataRequest
            {
                Key = Key,
                Value = JsonConvert.SerializeObject(this)
            };

            PlayFabAdminAPI.SetTitleData(setTitleData, OnSaveSuccessful, OnSaveFailed);
#endif
        }

#if ENABLE_PLAYFABADMIN_API
        protected virtual void OnSaveSuccessful(SetTitleDataResult result)
        {
            Debug.Log($"Successfully saved database {Key}.");
        }
#endif

#if ENABLE_PLAYFABADMIN_API
        protected virtual void OnSaveFailed(PlayFabError error)
        {
            Debug.LogError($"Failed to save database {Key} ({error.Error})");
        }
#endif


    }
}