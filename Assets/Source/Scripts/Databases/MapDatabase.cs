using System.Collections.Generic;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Grid;
using CrossBlitz.PlayFab.Data;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.Databases
{
    [CreateAssetMenu()]
    public class MapDatabase : ScriptableObject
    {
        public List<string> Folders;
        public List<FableMapData> MapData;

        public void Init()
        {
            bool save = false;
            for (var i = 0; i < MapData.Count; i++)
            {
                if (MapData[i].Rooms == null || MapData[i].Rooms.Count <= 0)
                {
                    var roomData = new FableRoomData();
                    roomData.UpdateIdentifier();
                    roomData.DisplayName = $"new room #{roomData.Uid}";
                    roomData.Tiles = new List<FableTileData>();

                    MapData[i].Rooms = new List<FableRoomData>();
                    MapData[i].Rooms.Add(roomData);
                    save = true;
                }
            }

            if (save)
            {
                Save();
            }
        }

        public List<FableMapData> GetAllMapsForChapter(string chapterUid)
        {
            var chapterData = Db.FablesDatabase.GetChapter(chapterUid);
            var map = Db.MapDatabase.GetMap(chapterData.mapId);
            var addedMaps = new List<FableMapData>();

            if (map == null) return addedMaps;

            var maps = new Queue<FableMapData>();
            maps.Enqueue(map);

            while (maps.Count > 0)
            {
                var nextMap = maps.Dequeue();
                addedMaps.Add(nextMap);

                for (var j = 0; j < nextMap.Rooms.Count; j++)
                {
                    for (var k = 0; k < nextMap.Rooms[j].Tiles.Count; k++)
                    {
                        var tile = nextMap.Rooms[j].Tiles[k];

                        if (tile.entranceTileUid != 0)
                        {
                            var mapData = Db.MapDatabase.GetMapByTileUid(tile.entranceTileUid);

                            //Debug.Log($"Tile: {tile.displayName} Tile Entrance:{ Db.MapDatabase.GetTile( tile.entranceTileUid ).displayName } Has Map: {mapData.Uid}");
                            if (!addedMaps.Contains(mapData))
                            {
                                maps.Enqueue(mapData);
                            }
                        }
                    }
                }
            }

            return addedMaps;
        }

        [Button("Fix Dependencies")]
        public void FixDependencies()
        {
            var battleUid = new Dictionary<string, List<string>>();

            for (var i = 0; i < MapData.Count; i++)
            {
                for (var j = 0; j < MapData[i].Rooms.Count; j++)
                {
                    for (var k = 0; k < MapData[i].Rooms[j].Tiles.Count; k++)
                    {
                        var tile = MapData[i].Rooms[j].Tiles[k];

                        if(string.IsNullOrEmpty(tile.battleUid))
                        {
                            continue;
                        }

                        // if (tile.NodeType != NodeType.Battle)
                        // {
                        //     //tile.battleUid = string.Empty;
                        //     Debug.LogError($"Non-battle tile has Battle Uid: {tile.battleUid}:{tile.NodeType}");
                        //     continue;
                        // }

                        if (!battleUid.ContainsKey(tile.battleUid))
                        {
                            battleUid.Add(tile.battleUid, new List<string> { $"{MapData[i].DisplayName}[{i}] - {MapData[i].Rooms[j].DisplayName}[{j}] - {tile.displayName}[{k}]"});
                        }
                        else
                        {
                            battleUid[tile.battleUid].Add( $"{MapData[i].DisplayName}[{i}] - {MapData[i].Rooms[j].DisplayName}[{j}] - {tile.displayName}[{k}]" );

                            var indicies = $"Shared Battle ID: ({tile.battleUid}) ";

                            foreach (var tileValue in battleUid[tile.battleUid])
                            {
                                indicies += $"{tileValue},";
                            }

                            Debug.LogError(indicies);
                        }
                        // if (tile.NodeType == NodeType.OtherThing)
                        // {
                        //     tile.NodeType = NodeType.Battle;
                        //
                        //     BattleData battleData;
                        //
                        //     if (!string.IsNullOrEmpty(tile.battleUid))
                        //     {
                        //         battleData = Db.BattleDatabase.GetBattleData(tile.battleUid);
                        //     }
                        //     else
                        //     {
                        //         battleData = Db.BattleDatabase.GetNewBattleData();
                        //     }
                        //
                        //     tile.battleUid = battleData.Uid;
                        //     battleData.BattleType = BattleType.Boss;
                        // }

                        // for (var l = 0; l < tile.connectingPathRooms.Count; l++)
                        // {
                        //     var connectedTile = GetTile(tile.connectingPathRooms[l]);
                        //
                        //     if (!connectedTile.connectingPathRooms.Contains(tile.uid))
                        //     {
                        //         connectedTile.connectingPathRooms.Add(tile.uid);
                        //         Debug.Log($"Added {tile.uid} -> {connectedTile.uid}");
                        //     }
                        // }
                    }
                }
            }
        }

        public FableMapData CreateNew(string folder)
        {
            var mapData = new FableMapData();
            mapData.UpdateIdentifier();
            mapData.DisplayName = "New Map";
            mapData.Rooms = new List<FableRoomData>();
            var roomData = new FableRoomData();
            roomData.UpdateIdentifier();
            roomData.DisplayName = $"new room #{roomData.Uid}";
            roomData.Tiles = new List<FableTileData>();
            mapData.Rooms.Add(roomData);

            AddToFolder(folder, mapData);
            MapData.Add(mapData);
            return mapData;
        }

        public bool TryCreateNewFolder(string folderPath, out string folderName)
        {
            folderName = string.Empty;

            var newFolderName = string.IsNullOrEmpty(folderPath) ? "New Folder" : $"{folderPath}/New Folder";

            if (Folders.Contains(newFolderName))
            {
                Debug.LogError(newFolderName);
                return false;
            }

            folderName = newFolderName;
            Folders.Add(newFolderName);
            return true;
        }

        public FableMapData AddToFolder(string folderName, FableMapData mapData)
        {
            mapData.Folder = folderName;
            return mapData;
        }

        public void ChangeFolderName(string oldFolderName, string newFolderName)
        {
            var index = Folders.IndexOf(oldFolderName);

            if (index < 0)
            {
                Debug.Log($"Could not find old folder {oldFolderName}");
                return;
            }

            var cutscenesToChange = GetMapsInFolder(oldFolderName);

            for (var i = 0; i < cutscenesToChange.Count; i++)
            {
                cutscenesToChange[i].Folder=cutscenesToChange[i].Folder.Replace(oldFolderName, newFolderName);
            }

            var childFolders = GetFoldersInFolder(oldFolderName);

            for (var i = 0; i < childFolders.Count; i++)
            {
                var childIndex = Folders.IndexOf(childFolders[i]);
                if (childIndex > 0) Folders[childIndex] = childFolders[i].Replace(oldFolderName, newFolderName);
            }

            Folders[index] = newFolderName;
        }

        public List<FableMapData> GetMapsInFolder(string folderName, bool includeChildren=false)
        {
            if (string.IsNullOrEmpty(folderName))
            {
                return MapData.FindAll(cutsceneData => string.IsNullOrEmpty(cutsceneData.Folder));
            }

            if (includeChildren)
            {
                return MapData.FindAll(cutsceneData => cutsceneData.Folder.Contains(folderName));
            }

            return MapData.FindAll(cutsceneData => cutsceneData.Folder == folderName);
        }

        public List<string> GetFoldersInFolder(string folderPath)
        {
            if (string.IsNullOrEmpty(folderPath))
            {
                return Folders.FindAll(folder => !folder.Contains("/"));
            }

            return Folders.FindAll(folder => folder.Contains(folderPath + "/"));
        }

        public FableMapData GetMap(string uid)
        {
            return MapData.Find(m => m.Uid == uid);
        }

        public FableMapData GetMapByRoomUid(string roomUid)
        {
            FableMapData map = null;

            for (var i = 0; i < MapData.Count; i++)
            {
                for (var j = 0; j < MapData[i].Rooms.Count; j++)
                {
                    if (MapData[i].Rooms[j].Uid == roomUid)
                    {
                        map = MapData[i];
                        break;
                    }
                }
            }

            return map;
        }

        public FableMapData GetMapByTileUid(int tileUid)
        {
            FableMapData map = null;

            for (var i = 0; i < MapData.Count; i++)
            {
                for (var j = 0; j < MapData[i].Rooms.Count; j++)
                {
                    for (var k = 0; k < MapData[i].Rooms[j].Tiles.Count; k++)
                    {
                        if (MapData[i].Rooms[j].Tiles[k].uid == tileUid)
                        {
                            map = MapData[i];
                            break;
                        }
                    }
                }
            }

            return map;
        }

        public FableMapData GetMapByName(string mapName)
        {
            return MapData.Find(m => m.DisplayName == mapName);
        }

        public void DeleteMap(string uid, bool save = true)
        {
            if (string.IsNullOrEmpty(uid)) return;

            var map = GetMap(uid);

            if (map != null)
            {
                for (var i = 0; i < map.Rooms.Count; i++)
                {
                    DeleteRoom(map.Rooms[i].Uid);
                }

                MapData.Remove(map);

                if (save) Save();
            }
        }

        public bool DeleteRoom(string roomUid)
        {
            for (var i = 0; i < MapData.Count; i++)
            {
                for (var j = 0; j < MapData[i].Rooms.Count; j++)
                {
                    if (MapData[i].Rooms[j].Uid == roomUid)
                    {
                        if (MapData[i].Rooms.Remove(MapData[i].Rooms[j]))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public void DeleteFolder(string folderName)
        {
            var index = Folders.IndexOf(folderName);

            if (index < 0)
            {
                Debug.Log($"Could not find old folder {folderName}");
                return;
            }

            Folders.RemoveAt(index);
        }

        public List<FableRoomData> GetRoomsWithTileType(NodeType nodeType)
        {
            var rooms = new List<FableRoomData>();

            for (var i = 0; i < MapData.Count; i++)
            {
                for (var j = 0; j < MapData[i].Rooms.Count; j++)
                {
                    if (MapData[i].Rooms[j].HasTileOfType(nodeType))
                    {
                        rooms.Add(MapData[i].Rooms[j]);
                    }
                }
            }

            return rooms;
        }

        public FableTileData GetTile(int tileUid)
        {
            FableTileData tile = null;

            for (var j = 0; j < MapData.Count; j++)
            {
                var map = MapData[j];

                for (var k = 0; k < map.Rooms.Count; k++)
                {
                    for (var l = 0; l < map.Rooms[k].Tiles.Count; l++)
                    {
                        if (map.Rooms[k].Tiles[l].uid == tileUid)
                        {
                            tile = map.Rooms[k].Tiles[l];
                        }
                    }
                }
            }

            return tile;
        }

        public void SetTile(int tileUid, FableTileData tileData)
        {
            for (var j = 0; j < MapData.Count; j++)
            {
                var map = MapData[j];

                for (var k = 0; k < map.Rooms.Count; k++)
                {
                    for (var l = 0; l < map.Rooms[k].Tiles.Count; l++)
                    {
                        if (map.Rooms[k].Tiles[l].uid == tileUid)
                        {
                            map.Rooms[k].Tiles[l] = tileData;
                        }
                    }
                }
            }
        }

        public void PasteTile(int sourceTileUid, FableTileData tileToCopy)
        {
            var source = GetTile(sourceTileUid);
            var tile = DataModel.FromJson<FableTileData>(tileToCopy.ToJson(false), false);
            tile.uid = source.uid;
            tile.Height = source.Height;
            tile.hex = DataModel.FromJson<Hex>(source.hex.ToJson(false), false);
            tile.connectingPathRooms = new List<int>();
            SetTile(sourceTileUid, tile);
        }

        public void ClearTile(int sourceTileUid)
        {
            var source = GetTile(sourceTileUid);
            var tile = new FableTileData();
            tile.Init(source.uid);
            tile.tileType = source.tileType;
            tile.Height = source.Height;
            tile.hex = DataModel.FromJson<Hex>(source.hex.ToJson(false), false);
            SetTile(sourceTileUid, tile);
        }

        public FableTileData GetTileByName(string tileName)
        {
            FableTileData tile = null;

            for (var j = 0; j < MapData.Count; j++)
            {
                var map = MapData[j];

                for (var k = 0; k < map.Rooms.Count; k++)
                {
                    for (var l = 0; l < map.Rooms[k].Tiles.Count; l++)
                    {
                        if (map.Rooms[k].Tiles[l].displayName == tileName)
                        {
                            tile = map.Rooms[k].Tiles[l];
                            break;
                        }
                    }
                }
            }

            return tile;
        }

        public FableTileData GetTileByDirectory(string mapName, string roomName, string tileName)
        {
            FableTileData tile = null;

            for (var j = 0; j < MapData.Count; j++)
            {
                var map = MapData[j];
                if (map.DisplayName != mapName) continue;

                for (var k = 0; k < map.Rooms.Count; k++)
                {
                    if (map.Rooms[k].DisplayName != roomName) continue;

                    for (var l = 0; l < map.Rooms[k].Tiles.Count; l++)
                    {
                        if (map.Rooms[k].Tiles[l].displayName == tileName)
                        {
                            tile = map.Rooms[k].Tiles[l];
                            break;
                        }
                    }
                }
            }

            return tile;
        }

        public FableTileData GetTileByBattle(string battleUid)
        {
            FableTileData tile = null;

            for (var j = 0; j < MapData.Count; j++)
            {
                var map = MapData[j];

                for (var k = 0; k < map.Rooms.Count; k++)
                {
                    for (var l = 0; l < map.Rooms[k].Tiles.Count; l++)
                    {
                        if (map.Rooms[k].Tiles[l].battleUid == battleUid)
                        {
                            tile = map.Rooms[k].Tiles[l];
                            break;
                        }
                    }
                }
            }

            return tile;
        }

        public FableTileData GetTileByCutscene(string cutsceneUid)
        {
            FableTileData tile = null;

            for (var j = 0; j < MapData.Count; j++)
            {
                var map = MapData[j];

                for (var k = 0; k < map.Rooms.Count; k++)
                {
                    for (var l = 0; l < map.Rooms[k].Tiles.Count; l++)
                    {
                        if (map.Rooms[k].Tiles[l].cutsceneUid == cutsceneUid)
                        {
                            tile = map.Rooms[k].Tiles[l];
                            break;
                        }
                    }
                }
            }

            return tile;
        }

        public List<FableTileData> GetTiles()
        {
            var tiles = new List<FableTileData>();
            for (var j = 0; j < MapData.Count; j++)
            {
                var map = MapData[j];

                for (var k = 0; k < map.Rooms.Count; k++)
                {
                    for (var l = 0; l < map.Rooms[k].Tiles.Count; l++)
                    {
                        var tile = map.Rooms[k].Tiles[l];

                        if (tile.NodeType != NodeType.None && tile.NodeType != NodeType.Path)
                            tiles.Add(map.Rooms[k].Tiles[l]);
                    }
                }
            }

            return tiles;
        }

        public List<FableTileData> GetTilesByNodeType(NodeType nodeType)
        {
            var tiles = new List<FableTileData>();
            for (var j = 0; j < MapData.Count; j++)
            {
                var map = MapData[j];

                for (var k = 0; k < map.Rooms.Count; k++)
                {
                    for (var l = 0; l < map.Rooms[k].Tiles.Count; l++)
                    {
                        var tile = map.Rooms[k].Tiles[l];

                        if (tile.NodeType == nodeType)
                            tiles.Add(map.Rooms[k].Tiles[l]);
                    }
                }
            }

            return tiles;
        }

        public List<string> GetAllTileDirectories()
        {
            var directories = new List<string>();

            for (var j = 0; j < MapData.Count; j++)
            {
                var map = MapData[j];

                for (var k = 0; k < map.Rooms.Count; k++)
                {
                    for (var l = 0; l < map.Rooms[k].Tiles.Count; l++)
                    {
                        var tile = map.Rooms[k].Tiles[l];

                        if (tile.NodeType != NodeType.None && tile.NodeType != NodeType.Path)
                        {
                            directories.Add($"{map.DisplayName}/{map.Rooms[k].DisplayName}/{tile.displayName}");
                        }
                    }
                }
            }

            return directories;
        }

        public string GetDirectoryNameForTile(int tileUid)
        {
            var directory = string.Empty;

            for (var j = 0; j < MapData.Count; j++)
            {
                var map = MapData[j];

                for (var k = 0; k < map.Rooms.Count; k++)
                {
                    for (var l = 0; l < map.Rooms[k].Tiles.Count; l++)
                    {
                        var tile = map.Rooms[k].Tiles[l];

                        if (tile.uid == tileUid)
                        {
                            directory = $"{map.DisplayName}/{map.Rooms[k].DisplayName}/{tile.displayName}";
                            break;
                        }
                    }
                }
            }

            return directory;
        }

        public FableRoomData GetRoom(string roomUid)
        {
            FableRoomData room = null;

            for (var j = 0; j < MapData.Count; j++)
            {
                var map = MapData[j];

                for (var k = 0; k < map.Rooms.Count; k++)
                {
                    if (map.Rooms[k].Uid == roomUid)
                    {
                        room = map.Rooms[k];
                        break;
                    }
                }
            }

            return room;
        }

        public FableRoomData GetRoomByTileUid(int tileUid)
        {
            FableRoomData room = null;

            for (var j = 0; j < MapData.Count; j++)
            {
                var map = MapData[j];

                for (var k = 0; k < map.Rooms.Count; k++)
                {
                    for (var l = 0; l < map.Rooms[k].Tiles.Count; l++)
                    {
                        if (map.Rooms[k].Tiles[l].uid == tileUid)
                        {
                            room = map.Rooms[k];
                            break;
                        }
                    }
                }
            }

            return room;
        }

        public int GetRoomIndex(string roomUid)
        {
            int roomIndex = -1;

            for (var j = 0; j < MapData.Count; j++)
            {
                var map = MapData[j];

                for (var k = 0; k < map.Rooms.Count; k++)
                {
                    if (map.Rooms[k].Uid == roomUid)
                    {
                        roomIndex = k;
                        break;
                    }
                }
            }

            return roomIndex;
        }

        public string GetRoomNameByUid(string roomUid)
        {
            string roomName = string.Empty;

            for (var j = 0; j < MapData.Count; j++)
            {
                var map = MapData[j];

                for (var k = 0; k < map.Rooms.Count; k++)
                {
                    if (map.Rooms[k].Uid == roomUid)
                    {
                        roomName = map.Rooms[k].DisplayName;
                        break;
                    }
                }
            }

            return roomName;
        }

        public string GetRoomUidByName(string roomName)
        {
            string uid = string.Empty;

            for (var j = 0; j < MapData.Count; j++)
            {
                var map = MapData[j];

                for (var k = 0; k < map.Rooms.Count; k++)
                {
                    if (map.Rooms[k].DisplayName == roomName)
                    {
                        uid = map.Rooms[k].Uid;
                        break;
                    }
                }
            }

            return uid;
        }


        public void Save()
        {
#if UNITY_EDITOR

            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }

    }
}