using System;
using System.Collections.Generic;
using CrossBlitz.Utils;
using UnityEngine;

namespace CrossBlitz.Databases
{
    [System.Serializable]
    public class VfxData : UniqueItem
    {
        public string displayName;
        public List<VfxObject> objects;

        public VfxObject GetObject(string uid)
        {
            return objects.Find(vfxObject => vfxObject.Uid == uid);
        }

        public VfxObject CreateNewObject()
        {
            var vfxObject = new VfxObject();
            vfxObject.UpdateIdentifier();
            vfxObject.displayName = "New Object";
            vfxObject.parentType = VfxObject.ParentType.Source;
            vfxObject.animator = string.Empty;
            vfxObject.createOnAwake = true;
            vfxObject.animations = new List<string>();
            vfxObject.frames = new List<VfxFrame>();
            objects.Add(vfxObject);
            return vfxObject;
        }

        public void Delete(string uid)
        {
            var obj = GetObject(uid);
            objects.Remove(obj);
        }

        public void MoveObjUp(string objUid)
        {
            var obj = GetObject(objUid);
            var objIndex = objects.IndexOf(obj);

            if (objIndex > 0)
            {
                var objTemp = objects[objIndex - 1];
                objects[objIndex] = objTemp;
                objects[objIndex - 1] = obj;
            }
        }

        public void MoveObjDown(string objUid)
        {
            var obj = GetObject(objUid);
            var objIndex = objects.IndexOf(obj);

            if (objIndex < objects.Count-1)
            {
                var objTemp = objects[objIndex + 1];
                objects[objIndex] = objTemp;
                objects[objIndex + 1] = obj;
            }
        }
    }

    [System.Serializable]
    public class VfxObject : UniqueItem
    {
        public enum ParentType
        {
            Source, // source creates this object on top of the source supplied
            Target, // will create this object on top of EVERY target supplied
            World // will create this object with no parent / just under the canvas
        }

        public string displayName;
        /// <summary>
        /// What this attaches to, there's other logic for this too.
        /// </summary>
        public ParentType parentType;
        /// <summary>
        /// The animator supplies the art but their can be more "frames" after the animator is added.
        /// The animator will play like normal, either looping or staying on the last frame (non-looped)
        /// </summary>
        public string animator;
        /// <summary>
        /// The animation to play, this is a list so you can play a random animation;
        /// </summary>
        public List<string> animations;
        /// <summary>
        /// When this vfx data is created this object is also created, otherwise, you'll ahve
        /// to manually create the object via a "CreateObject" event.
        /// </summary>
        public bool createOnAwake;
        /// <summary>
        /// Various things to do during every "frame"
        /// </summary>
        public List<VfxFrame> frames;

        public VfxFrame GetFrame(string uid)
        {
            return frames.Find(frame => frame.Uid == uid);
        }

        public VfxFrame CreateNewFrame()
        {
            var frame = new VfxFrame();
            frame.UpdateIdentifier();
            frame.events = new List<VfxFrameEvent>();
            frame.CreateNewEvent();
            frames.Add(frame);
            return frame;
        }

        public void DeleteFrame(string frameUid)
        {
            var frame = GetFrame(frameUid);
            var index = frames.IndexOf(frame);
            if (index <= 0) return;
            frames.Remove(frame);
        }
    }

    [System.Serializable]
    public class VfxFrame : UniqueItem
    {
        public List<VfxFrameEvent> events;

        public VfxFrameEvent GetEvent(string uid)
        {
            return events.Find(e => e.Uid == uid);
        }

        public VfxFrameEvent CreateNewEvent()
        {
            var e = new VfxFrameEvent();
            e.UpdateIdentifier();
            e.eventType = VfxFrameEvent.FrameEventType.None;
            e.targetType = VfxFrameEvent.TargetType.Self;
            e.alpha = 255;
            e.sortingLayer = "Particles";
            e.customEventArgs = "{}";
            events.Add(e);
            return e;
        }

        public void DeleteEvent(string uid)
        {
            var e = GetEvent(uid);
            events.Remove(e);
        }
    }

    [System.Serializable]
    public class VfxFrameEvent : UniqueItem
    {
        public enum FrameEventType
        {
            None,
            Color,
            Position,
            Rotation,
            Scale,
            CreateObject,
            SetSortingLayer,
            CustomEvent,
            Destroy,
        }

        public enum TargetType
        {
            Self,
            Parent,
        }

        public FrameEventType eventType;
        public TargetType targetType;
        public int skipUntil; // skips the entire event until X time.
        public string color;
        public int alpha;
        public Vector2Int position;
        public Vector3Int rotation;
        public Vector2Int scale;
        public string objectToCreate;
        public string sortingLayer;
        public int sortingOrder;
        public string customEventName;
        public string customEventArgs;

    }

    [CreateAssetMenu]
    public class VfxDatabase : ScriptableObject
    {
        public List<VfxData> vfxData;

        public void Init()
        {
        }

        public VfxData GetVfxData(string vfxUid)
        {
            return vfxData.Find(vfx => vfx.Uid == vfxUid);
        }

        public VfxData CreateNewItem()
        {
            var vfxItem = new VfxData();
            vfxItem.UpdateIdentifier();
            vfxItem.displayName = "New Vfx";
            vfxItem.objects = new List<VfxObject>();
            vfxData.Add(vfxItem);
            return vfxItem;
        }

        public void MoveUp( string vfxUid )
        {
            var vfx = GetVfxData(vfxUid);
            var index = vfxData.IndexOf(vfx);
            if (index <= 0) return;
            var tempVfx = vfxData[index - 1];
            vfxData[index] = tempVfx;
            vfxData[index - 1] = vfx;
        }

        public void MoveDown( string vfxUid )
        {
            var vfx = GetVfxData(vfxUid);
            var index = vfxData.IndexOf(vfx);
            if (index >= vfxData.Count-1) return;
            var tempVfx = vfxData[index + 1];
            vfxData[index] = tempVfx;
            vfxData[index + 1] = vfx;
        }

        public void Delete( string vfxUid )
        {
            var vfx = GetVfxData(vfxUid);
            vfxData.Remove(vfx);
        }

        public void Save()
        {
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }
    }
}