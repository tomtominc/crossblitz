using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Fables.Data;
using CrossBlitz.Utils;
using UnityEngine;

namespace CrossBlitz.Databases
{
    // [System.Serializable]
    // public class PoolItem
    // {
    //     public string poolItemId;
    //     public int count;
    //     public float weight;
    // }

    [System.Serializable]
    public class CardPool : UniqueItem
    {
        public enum Location
        {
            None,
            StarterDeck,
            CardShop,
            RelicShop,
            Recipe,
            LevelTree,
            Battle,
            Cutscene,
            Chest,
        }

        public Location location;
        public string name;
        public string hero;
        public int chapter;
        public List<string> cards;
    }

    public struct CardPoolLocation
    {
        public CardPool.Location location;

        public string hero;
        public string cardId;
        public int chapter;
        public string cardIcon;
        public string cardChapter;
        public string cardLocation;
        public string cardHint;
    }

    [CreateAssetMenu]
    public class CardPoolsDatabase : ScriptableObject
    {
        public List<CardPool> cardPools;
        [NonSerialized] public Dictionary<string, CardPool> cardPoolDic;
        [NonSerialized] public Dictionary<string, CardPoolLocation> cardLocations;

        public void Init()
        {
            cardPools = new List<CardPool>();

            Generate();

            cardPoolDic = new Dictionary<string, CardPool>();
            cardLocations = new Dictionary<string, CardPoolLocation>();

            for (var i = 0; i < cardPools.Count; i++)
            {
                cardPoolDic.Add(cardPools[i].Uid, cardPools[i]);
            }

            for (var i = 0; i < cardPools.Count; i++)
            {
                var pool = cardPools[i];

                for (var j = 0; j < pool.cards.Count; j++)
                {
                    if (!cardLocations.ContainsKey(pool.cards[j]))
                    {
                        var cardLocation = new CardPoolLocation();
                        cardLocation.location = pool.location;
                        cardLocation.cardId = pool.cards[j];
                        cardLocation.hero = pool.hero;
                        cardLocation.chapter = pool.chapter;
                        cardLocation.cardChapter = GetFormattedChapterText(cardLocation.hero, cardLocation.chapter);
                        cardLocation.cardLocation = GetFormattedLocationText(cardLocation.location);
                        cardLocation.cardIcon = GetAnimationIconName(cardLocation.location);
                        cardLocation.cardHint = GetFormattedLocationHint(cardLocation.location);
                        cardLocations.Add(cardLocation.cardId, cardLocation);
                    }
                }
            }
        }

        private string GetFormattedChapterText(string hero, int chapter)
        {
            if (chapter <= 0)
            {
                return $"<color=#8b6c67>{hero}</color>";
            }

            return $"<color=#8b6c67>{hero}: Ch.{chapter}</color>";
        }

        private string GetFormattedLocationText(CardPool.Location location)
        {
            switch (location)
            {
                case CardPool.Location.CardShop: return "<color=#e16647>Cardinella's Shop</color>";
                case CardPool.Location.Recipe: return "<color=#7838d3>Mawlder's Shop</color>";
                case CardPool.Location.LevelTree: return "<color=#5e859c>Level Tree</color>";
                case CardPool.Location.Battle: return "<color=#ab3b5c>Battle</color>";
                case CardPool.Location.StarterDeck:return "<color=#ab3b5c>Starter Deck</color>";
                case CardPool.Location.RelicShop:return "<color=#ab3b5c>Scai's Shop</color>";
                case CardPool.Location.Cutscene:return "<color=#ab3b5c>Cutscene</color>";
                case CardPool.Location.Chest:return "<color=#ab3b5c>Chest</color>";
            }

            return "Unknown";
        }

        private string GetAnimationIconName(CardPool.Location location)
        {
            switch (location)
            {
                case CardPool.Location.CardShop: return "cardinellas-shop";
                case CardPool.Location.Recipe: return "mawlders-shop";
                case CardPool.Location.LevelTree: return "level-grid";
                case CardPool.Location.Battle: return "battle";
            }

            return string.Empty;
        }

        private string GetFormattedLocationHint(CardPool.Location location)
        {
            switch (location)
            {
                case CardPool.Location.Battle: return "<color=#ae977a>Acquire</color> <color=#8b6c67>Card</color> <color=#ae977a>Recipe</color>";
            }

            return string.Empty;
        }

        public CardPool CreateNew(string poolName)
        {
            var cardPool = new CardPool();
            cardPool.UpdateIdentifier();
            cardPool.name = poolName;
            cardPool.cards = new List<string>();
            cardPools.Add(cardPool);

            return cardPool;
        }

        public List<CardPool> GetCardPools(Predicate<CardPool> predicate)
        {
            return cardPools.FindAll(predicate);
        }

        public List<string> GetPoolsForCard(string cardId)
        {
            var pools = new List<string>();

            for (var i = 0; i < cardPools.Count; i++)
            {
                if (cardPools[i].cards.Exists(c => c == cardId))
                {
                    pools.Add(cardPools[i].Uid);
                }
            }

            return pools;
        }

        public List<CardPool> GetPoolsForHero(string hero)
        {
            var pools = new List<CardPool>();

            for (var i = 0; i < cardPools.Count; i++)
            {
                if (cardPools[i].hero == hero)
                {
                    pools.Add(cardPools[i]);
                }
            }

            return pools;
        }

        public void Delete(string uid)
        {
            if (cardPoolDic.ContainsKey(uid))
            {
                var cardPool = cardPoolDic[uid];
                cardPools.Remove(cardPool);
                cardPoolDic.Remove(cardPool.Uid);
            }
        }

        public CardPool GetCardPool(string uid)
        {
            CardPool cardPool = null;

            if ( cardPoolDic != null && cardPoolDic.ContainsKey(uid))
            {
                cardPool = cardPoolDic[uid];
            }

            return cardPool;
        }

        public CardPool GetCardPoolByName(string poolName)
        {
            return cardPools.Find(p => p.name == poolName);
        }

        public CardPool GetOrCreatePoolByName(string poolName)
        {
            var cardPool = GetCardPoolByName(poolName);

            if (cardPool == null)
            {
                cardPool = CreateNew(poolName);
            }

            return cardPool;
        }

        public void MoveUp(string uid)
        {
            var cardPool = GetCardPool(uid);

            if (cardPool != null)
            {
                var index = cardPools.IndexOf(cardPool);

                if (index > 0)
                {
                    var temp = cardPools[index - 1];
                    cardPools[index] = temp;
                    cardPools[index - 1] = cardPool;
                }
            }
        }

        public void MoveDown(string uid)
        {
            var cardPool = GetCardPool(uid);

            if (cardPool != null)
            {
                var index = cardPools.IndexOf(cardPool);

                if (index < cardPools.Count - 1)
                {
                    var temp = cardPools[index + 1];
                    cardPools[index] = temp;
                    cardPools[index + 1] = cardPool;
                }
            }
        }

        public string GetCardLocationIcon(string cardId)
        {
            if (cardLocations.TryGetValue(cardId, out var location))
            {
                if (location.location == CardPool.Location.Recipe)
                {
                    // I need to check if the player has the recipe, if they don't, then I need to show formatted battle stuff.
                }

                return location.cardIcon;
            }

            return string.Empty;
        }

        public string GetCardLocationChapter(string cardId)
        {
            if (cardLocations.TryGetValue(cardId, out var location))
            {
                if (location.location == CardPool.Location.Recipe)
                {
                    // I need to check if the player has the recipe, if they don't, then I need to show formatted battle stuff.
                }

                return location.cardChapter;
            }

            // gusto
            // quick books
            // quick books update
            // w9s
            // costa rica
            // ny filing
            // ---
            // Tutorial Popup Data
            // - title
            // - Tutorial Page
            // -- sub-text
            // -- text
            // -- list of images
            // Battle


            return string.Empty;
        }

        public string GetCardLocation(string cardId)
        {
            if (cardLocations.TryGetValue(cardId, out var location))
            {
                if (location.location == CardPool.Location.Recipe)
                {
                    // I need to check if the player has the recipe, if they don't, then I need to show formatted battle stuff.
                }

                return location.cardLocation;
            }

            return string.Empty;
        }

        public string GetCardLocationHint(string cardId)
        {
            if (cardLocations.TryGetValue(cardId, out var location))
            {
                if (location.location == CardPool.Location.Recipe)
                {
                    // I need to check if the player has the recipe, if they don't, then I need to show formatted battle stuff.
                }

                return location.cardHint;
            }

            return string.Empty;
        }

        public void Generate()
        {
            var heroes = new List<string> { "Redcroft", "Violet", "Seto", "Quill", "Mereena" };

            for (var i = 0; i < heroes.Count; i++)
            {
                var chapterHero = Db.HeroDatabase.GetHero(heroes[i]);
                var book = Db.FablesDatabase.GetBook(chapterHero.id, 1);

                var starterRecipePool = GetOrCreatePoolByName($"{chapterHero.id} Starter Deck");
                var levelTreePool = GetOrCreatePoolByName($"{chapterHero.id} Level Tree");

                starterRecipePool.location = CardPool.Location.StarterDeck;
                levelTreePool.location = CardPool.Location.LevelTree;

                starterRecipePool.hero = levelTreePool.hero = chapterHero.id;

                // ==========================================================
                // ALL CHAPTERS - SHOPS - BATTLE PRIZE - CUTSCENE - CHESTS
                // ==========================================================

                for (var j = 0; j < book.Chapters.Count; j++)
                {
                    var chapter = book.Chapters[j];
                    var maps = Db.MapDatabase.GetAllMapsForChapter(chapter.Uid);

                    var cardShopPool = GetOrCreatePoolByName($"{chapterHero.id} Card Shops Ch.{chapter.ChapterNumber}");
                    var relicShopPool = GetOrCreatePoolByName($"{chapterHero.id} Relic Shops Ch.{chapter.ChapterNumber}");
                    var recipesPool = GetOrCreatePoolByName($"{chapterHero.id} Melding Recipes Ch.{chapter.ChapterNumber}");
                    var cutscenePool = GetOrCreatePoolByName($"{chapterHero.id} Cutscenes Ch.{chapter.ChapterNumber}");
                    var chestsPool = GetOrCreatePoolByName($"{chapterHero.id} Chests Ch.{chapter.ChapterNumber}");

                    cardShopPool.location = CardPool.Location.CardShop;
                    relicShopPool.location = CardPool.Location.RelicShop;
                    recipesPool.location = CardPool.Location.Recipe;
                    cutscenePool.location = CardPool.Location.Cutscene;
                    chestsPool.location = CardPool.Location.Chest;


                    cardShopPool.cards.Clear();
                    relicShopPool.cards.Clear();
                    recipesPool.cards.Clear();
                    cutscenePool.cards.Clear();
                    chestsPool.cards.Clear();
                    starterRecipePool.cards.Clear();
                    levelTreePool.cards.Clear();

                    cardShopPool.hero = relicShopPool.hero = recipesPool.hero = cutscenePool.hero = chestsPool.hero = chapterHero.id;
                    cardShopPool.chapter = relicShopPool.chapter = recipesPool.chapter = cutscenePool.chapter = chestsPool.chapter = chapter.ChapterNumber;

                    var mapLog = $"{chapterHero.id} Ch.{chapter.ChapterNumber} Maps:\n";

                    for (var k = 0; k < maps.Count; k++)
                    {
                        mapLog += $"{maps[k].DisplayName}\n";

                        for (var l = 0; l < maps[k].Rooms.Count; l++)
                        {
                            for (var m = 0; m < maps[k].Rooms[l].Tiles.Count; m++)
                            {
                                var tile = maps[k].Rooms[l].Tiles[m];

                                if (tile.NodeType == NodeType.Shop)
                                {
                                    for (var n = 0; n < tile.items.Count; n++)
                                    {
                                        var card = Db.CardDatabase.GetCard(tile.items[n].ItemUid);

                                        if (card != null)
                                        {
                                            if (tile.ShopType == ShopType.Card)
                                            {
                                                cardShopPool.cards.AddDistinct(card.id);
                                            }
                                            else
                                            {
                                                relicShopPool.cards.AddDistinct(card.id);
                                            }
                                        }
                                    }
                                }

                                if (tile.NodeType == NodeType.Chest)
                                {
                                    for (var n = 0; n < tile.items.Count; n++)
                                    {
                                        var card = Db.CardDatabase.GetCard(tile.items[n].ItemUid);

                                        if (card != null)
                                        {
                                            chestsPool.cards.AddDistinct(card.id);
                                        }
                                    }
                                }

                                if (tile.NodeType == NodeType.Battle)
                                {
                                    var battle = Db.BattleDatabase.GetBattleData(tile.battleUid);

                                    if (battle != null)
                                    {
                                        for (var n = 0; n < battle.PrizeCards.Count; n++)
                                        {
                                            var card = Db.CardDatabase.GetCard(battle.PrizeCards[n]);
                                            if (card != null)
                                            {
                                                recipesPool.cards.AddDistinct(card.id);
                                            }
                                        }
                                    }
                                }

                                //if (tile.NodeType == NodeType.Event1 || tile.NodeType == NodeType.Event2)
                                {
                                    var cutscenes = new List<string>();

                                    if (!string.IsNullOrEmpty(tile.cutsceneUid))
                                    {
                                        cutscenes.Add(tile.cutsceneUid);
                                    }

                                    if (!string.IsNullOrEmpty(tile.introCutsceneUid))
                                    {
                                        cutscenes.Add(tile.introCutsceneUid);
                                    }

                                    if (!string.IsNullOrEmpty(tile.outroCutsceneUid))
                                    {
                                        cutscenes.Add(tile.outroCutsceneUid);
                                    }

                                    for (var n = 0; n < cutscenes.Count; n++)
                                    {
                                        var cutscene = Db.CutsceneDatabase.GetCutscene(cutscenes[n]);
                                        if (cutscene != null)
                                        {
                                            for (var o = 0; o < cutscene.RewardItems.Count; o++)
                                            {
                                                var reward = cutscene.RewardItems[o];
                                                var card = Db.CardDatabase.GetCard(reward.ItemUid);

                                                if (card != null)
                                                {
                                                    cutscenePool.cards.AddDistinct(card.id);
                                                }
                                            }

                                            for (var o = 0; o < cutscene.DialogueInfo.Count; o++)
                                            {
                                                for (var p = 0; p < cutscene.DialogueInfo[o].Choices.Count; p++)
                                                {
                                                    var choice = cutscene.DialogueInfo[o].Choices[p];
                                                    var choiceScene = Db.CutsceneDatabase.GetCutscene(choice.CutsceneUid);

                                                    if (choiceScene != null)
                                                    {
                                                        for (var q = 0; q < choiceScene.RewardItems.Count; q++)
                                                        {
                                                            var reward = choiceScene.RewardItems[q];
                                                            var card = Db.CardDatabase.GetCard(reward.ItemUid);

                                                            if (card != null)
                                                            {
                                                                cutscenePool.cards.AddDistinct(card.id);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // ==========================================================
                // STARTER DECK
                // ==========================================================

                var deck = Db.DeckRecipeDatabase.GetRecipeByName($"{chapterHero.id} Starter");
                if (deck == null) deck = Db.DeckRecipeDatabase.GetRecipeByName($"{chapterHero.id} Starter Deck");

                if (deck != null)
                {
                    for (var j = 0; j < deck.deckData.cards.Count; j++)
                    {
                        var card = Db.CardDatabase.GetCard( deck.deckData.cards[j].id );
                        starterRecipePool.cards.AddDistinct(card.id);
                    }
                }

                // ==========================================================
                // LEVEL TREE
                // ==========================================================

                for (var j = 0; j < chapterHero.levelTrees.Count; j++)
                {
                    for (var k = 0; k < chapterHero.levelTrees[j].levels.Count; k++)
                    {
                        var reward = chapterHero.levelTrees[j].levels[k];

                        if (reward.type == LevelReward.EType.Card)
                        {
                            if (string.IsNullOrEmpty(reward.cardId)) continue;
                            var card = Db.CardDatabase.GetCard(reward.cardId);
                            if (card == null) continue;
                            levelTreePool.cards.AddDistinct(card.id);
                        }
                    }
                }
            }
        }

        public void Save()
        {
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }
    }
}