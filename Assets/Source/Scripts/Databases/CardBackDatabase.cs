
using System;
using System.Collections.Generic;
using CrossBlitz.Utils;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.Databases
{
    [CreateAssetMenu]
    public class CardBackDatabase : SerializedScriptableObject
    {
        public SpriteAnimationAsset cardBackAnimator;
        public List<CardBackData> cardBacks;
        [HideInInspector]
        public Dictionary<string, CardBackData> cardBackMap;
        [HideInInspector]
        public Dictionary<string, CardBackData> cardBackByPortraitUrlMap;

        public void Init()
        {
            var dirty = false;

            if (cardBacks == null)
            {
                cardBacks = new List<CardBackData>();
            }

            // loop through all animations in the card back animator, if an animation exists but card back data does not, add it. This will make sure that no card back is left out.
            for (var i = 0; i < cardBackAnimator.animations.Count; i++)
            {
                if (cardBacks.Find( cardBack => cardBack.portraitUrl == cardBackAnimator.animations[i].name ) == null)
                {
                    dirty = true;
                    var cardBack = new CardBackData();
                    cardBack.UpdateIdentifier();
                    cardBack.displayName = cardBackAnimator.animations[i].name;
                    cardBack.portraitUrl = cardBackAnimator.animations[i].name;
                    cardBack.description = "Nothing here.";
                    cardBacks.Add(cardBack);
                }
            }

            for (var i = cardBacks.Count-1; i > -1; i--)
            {
                if (!cardBackAnimator.animations.Exists(c => c.name == cardBacks[i].portraitUrl))
                {
                    cardBacks.RemoveAt(i);
                    dirty = true;
                }
            }

            if (cardBackMap == null)
            {
                cardBackMap = new Dictionary<string, CardBackData>();
            }

            if (cardBackByPortraitUrlMap == null)
            {
                cardBackByPortraitUrlMap = new Dictionary<string, CardBackData>();
            }

            for (var i = 0; i < cardBacks.Count; i++)
            {
                // new card backs added via the unity editor will not have an id, lets make sure
                // its setup.
                if (string.IsNullOrEmpty(cardBacks[i].Uid))
                {
                    cardBacks[i].UpdateIdentifier();
                    dirty = true;
                }

                if (!cardBackMap.ContainsKey(cardBacks[i].Uid))
                {
                    cardBackMap.Add(cardBacks[i].Uid, cardBacks[i]);
                }

                if (!cardBackByPortraitUrlMap.ContainsKey(cardBacks[i].portraitUrl))
                {
                    cardBackByPortraitUrlMap.Add(cardBacks[i].portraitUrl, cardBacks[i]);
                }
            }

            if (dirty)
            {
                Save();
            }

        }

        public CardBackData GetCardBackDataCanBeNull(string uid)
        {
            if (!cardBackMap.ContainsKey(uid))
            {
                return null;
            }

            return cardBackMap[uid];
        }

        public CardBackData GetCardBackData(string uid)
        {
            if (string.IsNullOrEmpty(uid))
            {
                return GetCardBackByPortraitUrl("default");
            }

            if (!cardBackMap.ContainsKey(uid))
            {
                return GetCardBackByPortraitUrl("default");
            }

            return cardBackMap[uid];
        }

        public CardBackData GetCardBackByPortraitUrl(string portraitUrl)
        {
            if (string.IsNullOrEmpty(portraitUrl))
            {
                return cardBackByPortraitUrlMap["default"];
            }

            if (!cardBackByPortraitUrlMap.ContainsKey(portraitUrl))
            {
                return cardBackByPortraitUrlMap["default"];
            }

            return cardBackByPortraitUrlMap[portraitUrl];
        }

        public void Save()
        {
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }
    }


    [Serializable]
    public class CardBackData : UniqueItem
    {
        public string displayName;
        public string portraitUrl;
        public string description;
        public string achievementId;
    }
}


public abstract class UnitySerializedDictionary<TKey, TValue> : Dictionary<TKey, TValue>, ISerializationCallbackReceiver
{
    [SerializeField, HideInInspector]
    private List<TKey> keyData = new List<TKey>();

    [SerializeField, HideInInspector]
    private List<TValue> valueData = new List<TValue>();

    void ISerializationCallbackReceiver.OnAfterDeserialize()
    {
        this.Clear();
        for (int i = 0; i < this.keyData.Count && i < this.valueData.Count; i++)
        {
            this[this.keyData[i]] = this.valueData[i];
        }
    }

    void ISerializationCallbackReceiver.OnBeforeSerialize()
    {
        this.keyData.Clear();
        this.valueData.Clear();

        foreach (var item in this)
        {
            this.keyData.Add(item.Key);
            this.valueData.Add(item.Value);
        }
    }
}