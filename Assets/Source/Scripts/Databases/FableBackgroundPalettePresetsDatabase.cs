using System.Collections.Generic;
using BlendModes;
using UnityEngine;

namespace CrossBlitz.Databases
{
    [CreateAssetMenu(fileName = "FableBackgroundPalettePresetsDatabase", menuName = "Background Palette Presets Database", order = 0)]
    public class FableBackgroundPalettePresetsDatabase : ScriptableObject
    {
        public List<FableBackgroundPalettePreset> Palettes;

        public void Init()
        {

        }

        public FableBackgroundPalettePreset GetPalette(string paletteName)
        {
            return Palettes.Find(p => p.presetName == paletteName);
        }

        public int GetPaletteIndex(FableBackgroundPalettePreset palette)
        {
            return Palettes.IndexOf(palette);
        }

        public bool HasPalette(string paletteName)
        {
            return Palettes.Find(p => p.presetName == paletteName) != null;
        }

        public int AddNewPalette(FableBackgroundPalettePreset palette)
        {
            Palettes.Add(palette);
            return Palettes.IndexOf(palette);
        }

        public void Save()
        {
#if UNITY_EDITOR

            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }
    }

    [System.Serializable]
    public class FableBackgroundPalettePreset
    {
        public string presetName;
        public string foregroundColor1;
        public string backgroundColor1;
        public string backgroundColor2;
        public string backgroundColor3;
        public string backgroundColor4;
        public string backgroundColor5;
        public string backgroundColor6;
        public string cornerLayerColor1;
        public string cornerLayerColor2;
        public string diamondPatternColor;

        public float foregroundAlpha1;
        public float backgroundAlpha1;
        public float backgroundAlpha2;
        public float backgroundAlpha3;
        public float backgroundAlpha4;
        public float backgroundAlpha5;
        public float backgroundAlpha6;
        public float cornerLayerAlpha1;
        public float cornerLayerAlpha2;
        public float diamondPatternAlpha;

        public BlendMode foregroundBlend;

        public float scrollSpeed;
    }
}