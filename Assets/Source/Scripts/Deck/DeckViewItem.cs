using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Hero;
using CrossBlitz.PlayFab.Authentication;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using CrossBlitz.DeckEditAPI;

namespace CrossBlitz.Deck
{
    public class DeckViewItem : MonoBehaviour,IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
    {
        public SpriteAnimation heroPortrait;
        public TextMeshProUGUI deckNameLabel;
        public SpriteAnimation background;
        public Button deckButton;
        public CanvasGroup canvasGroup;
        public TextMeshProUGUI deckCount;
        public Button deckOptionsButton;
        public CanvasGroup optionsControlIcon;

        public bool isHeader = false;
        //public SpriteAnimation iconAnimation;

        [BoxGroup("Equipped")] public List<RectTransform> equippedIcons;
        [BoxGroup("Selections")] public SpriteAnimation selectedDeck;

        [BoxGroup("Animate In")] public ShakeContainer shake;
        [BoxGroup("Animate In")] public CanvasGroup plopOverlay;
        [BoxGroup("Animate In")] public GameObject actionDrips;
        [BoxGroup("Animate In")] public List<SpriteAnimation> dripAnimators;

        [BoxGroup("Animate Parameters")] public bool rotateOnHover;
        [BoxGroup("Animate Parameters")] public float rotatePower = 40;
        [BoxGroup("Animate Parameters")] public int rotateVibrateTimes = 4;
        [BoxGroup("Animate Parameters")] public float rotateElasticity = 1;
        [BoxGroup("Animate Parameters")] public float hoverDuration = 0.24f;


        private DeckData m_deckData;
        private RectTransform rect;

        private Tweener _rotateTween;
        private bool _isHovering;

        public DeckData MDeckData => m_deckData;
        public event Action<DeckViewItem> OnDeckSelected;

        private void Start()
        {
            if (deckButton)
            {
                //deckButton.onClick.AddListener(DeckSelected);
            }

            if (deckOptionsButton)
            {
                //deckOptionsButton.onClick.AddListener(DeckOptionsButton);
            }

            _isHovering = false;
        }

        public void Update()
        {


        }

        public virtual void OnPointerEnter(PointerEventData e)
        {
            plopOverlay.SetActive(true);
            plopOverlay.alpha = 0;
            plopOverlay.DOFade(.5f, 0.2f);

            if (rotateOnHover)
            {
                if (_rotateTween == null || !_rotateTween.IsActive())
                {
                    shake.RectTransform().DOKill();
                    shake.RectTransform().localScale = Vector3.one;
                    shake.RectTransform().localEulerAngles = Vector3.zero;

                    _rotateTween = shake.RectTransform().DOPunchRotation(Vector3.right * rotatePower, hoverDuration,
                        rotateVibrateTimes,
                        rotateElasticity);
                }
            }

            optionsControlIcon.DOFade(1, 0.1f);
            _isHovering = true;
        }

        public virtual void OnPointerExit(PointerEventData e)
        {
            plopOverlay.SetActive(true);
            plopOverlay.alpha = .5f;
            plopOverlay.DOFade(0, 0.2f);

            optionsControlIcon.DOFade(0, 0.1f);
            _isHovering = false;
        }

        public void OnDeckSelectedForEdit()
        {
            selectedDeck.SetActive(true);
            selectedDeck.Play("play");
        }

        public void OnDeckUnselectedForEdit()
        {
            selectedDeck.SetActive(false);
        }

        public void Clear(string labelName, Faction faction)
        {
            m_deckData = null;
            deckNameLabel.text = labelName;
            deckNameLabel.color = ColorPalette.CrossBlitz.GetFactionColorLight(faction).ToColor();
            background.Play(faction.ToString().ToLower());

            if (heroPortrait)
            {
                heroPortrait.Play( HeroData.GetHeroFromFaction(faction).ToLower());
            }
        }

        public void SetDeck(DeckData deckData)
        {
            m_deckData = deckData;

            deckNameLabel.text = m_deckData.name;
            deckNameLabel.color = ColorPalette.CrossBlitz.GetFactionColorLight(m_deckData.faction).ToColor();

            background.Play(m_deckData.faction.ToString().ToLower());

            if (deckCount)
            {
                var cardCount = m_deckData.GetCardCount();
                deckCount.text = $"{cardCount}/{DeckData.MaxCardsPerDeck}";
                deckCount.SetActive(cardCount < DeckData.MaxCardsPerDeck);
            }

            if (heroPortrait)
            {
                heroPortrait.Play( m_deckData.hero.ToLower()+"-idle");
            }

            var currentDeck = App.PlayerDecks.GetDeckBasedOnGameState();

            if (App.PlayerDecks != null && currentDeck?.uid == deckData.uid)
            {
                for (var i = 0; i < equippedIcons.Count; i++) equippedIcons[i].SetActive(true);
            }
            else
            {
                for (var i = 0; i < equippedIcons.Count; i++) equippedIcons[i].SetActive(false);
            }
        }

        public void PunchSelect()
        {
            shake.RectTransform().DOKill(true);
            transform.GetChild(0).DOPunchScale(new Vector3(-0.25f, 1, 0) * 0.25f, 0.25f);
           // shake.Shake(3, 0.25f);
            actionDrips.SetActive(true);
            dripAnimators.ForEach( drip => drip.Play("spawn-particle"));
            plopOverlay.SetActive(true);
            plopOverlay.alpha = 1;
            plopOverlay.DOFade(0, 0.25f)
                .OnComplete(() =>
                {
                    plopOverlay.alpha = 1;
                    plopOverlay.DOFade(0, 0.12f)
                        .SetLoops(3, LoopType.Yoyo)
                        .OnComplete(() =>
                        {
                            actionDrips.SetActive(false);
                            plopOverlay.SetActive(false);
                        });
                });
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            //Debug.Log("POINTER CLICKED");
            switch (eventData.button)
            {
                case PointerEventData.InputButton.Right:
                    DeckOptionsButton();
                    break;
                case PointerEventData.InputButton.Left:
                    if(!DeckEditMenu.Instance.deckOptions.isActiveAndEnabled  && !isHeader) DeckSelected();
                    break;
                case PointerEventData.InputButton.Middle:
                    //OnMiddleClick?.Invoke(this);
                    break;
            }
        }

        private void DeckSelected()
        {
            OnDeckSelected?.Invoke(this);
        }

        private void DeckOptionsButton()
        {
            if (DeckEditMenu.Instance.deckOptions.isActiveAndEnabled)
            {
                DeckEditMenu.Instance.deckOptions.SetActive(false);
                //iconAnimation.Play("magnifying-lens");
            }
            else
            {
                DeckEditMenu.Instance.deckOptions.Setup(m_deckData, this);
                DeckEditMenu.Instance.deckOptions.SetActive(true);
                //iconAnimation.Play("magnifying-lens-close");
            }

        }
    }
}