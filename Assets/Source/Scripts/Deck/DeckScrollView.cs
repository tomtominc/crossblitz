using System;
using System.Collections.Generic;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI;
using CrossBlitz.Transition;
using CrossBlitz.UI.Widgets.Dropdown;
using Source.Scripts.AssetManagement;
using UnityEngine;
using UnityEngine.UI;

namespace Source.Scripts.Deck
{
    public class DeckScrollView : MonoBehaviour, IAssetLoader
    {
        private const string DeckPrefab = "DeckView";

        public int decksPerPage;
        public ToggleGroup toggleGroup;
        public RectTransform layout;
        public DropdownBox factionDropdown;
        public Button nextButton;
        public Button previousButton;
        public string AssetName => name;
        public event Action<DeckViewOld> OnDeckSelected;

        private int _page;
        private Faction _filter;
        private DeckViewOld _current;
        private List<DeckData> _decks;
        private List<GameObject> _assetsToUnload;
        private bool _isBusy=true;
        public bool IsBusy => _isBusy;

        private void Start()
        {
            if (decksPerPage <= 0)
                decksPerPage = 4;

            factionDropdown.ItemSelected += FilterSelected;
            nextButton.onClick.AddListener(NextButtonPressed);
            previousButton.onClick.AddListener(PreviousButtonPressed);

            FilterSelected(factionDropdown.contents[0]);

            // TransitionController.AssetsToUnload.Add(this);

            _assetsToUnload=new List<GameObject>();
        }

        private void FilterSelected(DropdownItemData item)
        {
            var filter = (Faction)Enum.Parse(typeof(Faction), item.name.ToCamelCase());
            LoadPage(0, filter);
        }

        private void NextButtonPressed()
        {
            LoadPage(_page + 1, _filter);
        }

        private void PreviousButtonPressed()
        {
            LoadPage(_page - 1, _filter);
        }

        private void LoadPage(int page, Faction filter)
        {
            _page = page;
            _filter = filter;

            previousButton.SetActive(false);
            nextButton.SetActive(false);

            layout.DestroyChildren();

            if (_filter == Faction.All)
            {
                _decks = App.PlayerDecks.GetDecks();
            }
            else
            {
                _decks = App.PlayerDecks.GetDecks()
                    .FindAll(x => x.faction == _filter);
            }

            if (_decks.Count > 0)
            {
                CreateDeckLayout();
            }

            previousButton.SetActive(_page > 0);
            nextButton.SetActive(_page < (int)(_decks.Count / (float)decksPerPage));
        }

        private async void CreateDeckLayout()
        {
            _isBusy = true;

            for (var i = _page * decksPerPage; i < (_page * decksPerPage) + decksPerPage; i++)
            {
                if (i >= _decks.Count)
                    break;

                var deckViewObj = await AddressableReferenceLoader.Load(DeckPrefab, layout);// Instantiate(deckViewPrefab, layout);
                var deckView = deckViewObj.GetComponent<DeckViewOld>();

                deckView.SetDeck(_decks[i]);
                deckView.toggle.group = toggleGroup;

                if (i == _page * decksPerPage)
                {
                    _current = deckView;
                    _current.toggle.isOn = true;

                    // we need to change the selection here
                    DeckChangedSelection(_current, true);
                }

                deckView.OnChangedSelectionValue += DeckChangedSelection;

                _assetsToUnload.Add(deckViewObj);
                //deckView.AnimateDropIn();
            }

            _isBusy = false;
        }

        public void Unload()
        {
            for (var i = 0; i < _assetsToUnload.Count; i++)
            {
                AddressableReferenceLoader.Unload(_assetsToUnload[i]);
            }
        }

        private void DeckChangedSelection(DeckViewOld deckViewOld, bool isOn)
        {
            if (isOn)
            {
                _current = deckViewOld;

                OnDeckSelected?.Invoke(_current);
            }
        }
    }
}
