using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DeckViewOld : MonoBehaviour
{
    public Image deckImage;
    public Toggle toggle;
    public TextMeshProUGUI deckName;
    public Image characterIcon;
    public List<Sprite> deckSprites;

    private DeckData _deckData;
    public DeckData DeckData => _deckData;

    public event Action<DeckViewOld, bool> OnChangedSelectionValue;

    private void Start()
    {
        toggle.onValueChanged.
            AddListener(ChangedSelectionValue);
    }

    public void SetDeck(DeckData deckData)
    {
        _deckData = deckData;
        deckName.text = _deckData.name;
        characterIcon.SetActive(false);
        deckImage.sprite = deckSprites.
            Find(x => x.name == $"deck-{deckData.faction.ToString().ToLower()}");
    }

    private void ChangedSelectionValue(bool isOn)
    {
       OnChangedSelectionValue?.Invoke(this,isOn);
    }
}
