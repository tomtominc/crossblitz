using CrossBlitz.Card;
using CrossBlitz.Databases;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace CrossBlitz.Deck
{
    public class DeckView : MonoBehaviour
    {
        public SpriteAnimation deckAnimator;
        public SpriteAnimation deckIcon;
        public TextMeshProUGUI deckName;
        public GameObject deckCountContainer;
        public TextMeshProUGUI deckCount;

        //public GameObject newView;
        public GameObject lockedView;
        public GameObject disableOverlay;
        public GameObject selectedView;
        public GameObject outline;
        public CanvasGroup overlay;

        private RectTransform m_rect;
        private DeckData m_deckData;
        public DeckData DeckData => m_deckData;

        private void Start()
        {
            m_rect = transform as RectTransform;
        }

        public void SetDeck(DeckData deckData)
        {
            m_deckData = deckData;

            if (deckName)
            {
                deckName.text = m_deckData.name.ToUpper();
                deckName.SetActive(true);
            }

            if (deckAnimator)
            {
                deckAnimator.Play(deckData.faction.ToString().ToLower());
            }

            if (deckIcon)
            {
                var recipe = Db.DeckRecipeDatabase.GetClosestArchetype(m_deckData);
                if (recipe == null || !deckIcon.Play(recipe.archetype)) // if the recipe is null or I can't play the archetype icon, just play the starter icon.
                {
                    deckIcon.Play($"{deckData.hero.ToLower()}-starter");
                }
            }

            if (deckCount && deckCountContainer)
            {
                var cardCount = m_deckData.GetCardCount();
                var countColor = cardCount < DeckData.MaxCardsPerDeck ? "Red" : "Yellow";
                deckCount.text = $"<sprite=0><font=HopeGold-Outlined-Brown-{countColor}>{cardCount}</font>/{DeckData.MaxCardsPerDeck}";
                deckCountContainer.SetActive(cardCount < DeckData.MaxCardsPerDeck);
            }
        }

        public void SetIsNew(bool isNew)
        {
            //newView.SetActive(isNew);
        }

        public void SetIsLocked(bool isLocked)
        {
            lockedView.SetActive(isLocked);
            disableOverlay.SetActive(isLocked);
        }

        public void SetIsSelected(bool isSelected)
        {
            selectedView.SetActive(isSelected);
        }

        public bool ToggleIsSelected()
        {
            selectedView.SetActive(!selectedView.activeSelf);
            return selectedView.activeSelf;
        }

        public void Punch()
        {
            m_rect.DOKill();
            m_rect.DOPunchScale(new Vector3(0.125f,-0.125f, 0), 0.5f);
            m_rect.DOAnchorPosY(3, 0.25f).SetEase(Ease.OutBack);

            overlay.DOKill();
            overlay.alpha = 1;
            overlay.DOFade(0, 0.25f);
        }

        public void SetIsOutlined(bool isOutlined)
        {
            outline.SetActive(isOutlined);
        }

        private Color GetNameColorFromFaction(Faction faction)
        {
            switch (faction)
            {
                case Faction.War: return "#C26665".ToColor();
                case Faction.Balance: return "#679CCF".ToColor();
                case Faction.Chaos: return "#895DB1".ToColor();
                case Faction.Fortune: return "#EFAC29".ToColor();
                case Faction.Nature: return "#91BC4D".ToColor();
            }

            return "#7A908A".ToColor();
        }

        public void SetDeckAsCustom(DeckData deckData)
        {
            m_deckData = deckData;

            if (deckName)
            {
                deckName.SetActive(true);
                deckName.text = "CUSTOM\nDECK";
            }

            if (deckAnimator)
            {
                deckAnimator.Play("basic");
            }

            if (deckIcon)
            {
                deckIcon.Play("custom-deck");
            }

            if (deckCountContainer)
            {
                deckCountContainer.SetActive(false);
            }
        }

        public void SetDeckAsWild(DeckData deckData)
        {
            m_deckData = deckData;
            if (deckName)
            {
                deckName.SetActive(false);
            }

            if (deckAnimator)
            {
                deckAnimator.Play("wild");
            }

            if (deckIcon)
            {
                deckIcon.SetActive(false);
            }

            if (deckCount)
            {
                deckCount.SetActive(false);
            }
        }
    }
}