using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CrossBlitz.Source.Scripts.Developer
{
    public static class ImageHelpers
    {
        public static Texture2D AlphaBlend2(List<Texture2D> textures, List<Vector2Int> offsets, List<Rect> drawingBounds, Vector2Int finalImageSize)
        {
            // store the sizes of each texture to use when iterating over the pixels
            var sizes = new List<Vector2Int>();
            // store all the colors of each texture so we can layer them
            var colors = new List<Color[]>();

            for (var i = 0; i < textures.Count; i++)
            {
                sizes.Add(new Vector2Int(textures[i].width, textures[i].height));
                colors.Add(textures[i].GetPixels());
            }

            // the final pixels of the merged texture
            var finalPixels = new Color[finalImageSize.x * finalImageSize.y];
            var finalSize = finalImageSize;
            var bottomTexture = textures[0];
            var pixelCount = 0;

            for (var row = 0; row < finalSize.y; row++)
            {
                for (var col = 0; col < finalSize.x; col++)
                {
                    var nextPixel = bottomTexture.GetPixel(col, row);

                    if (!IsWithinSpaceOrNoSpaceSpecified(new Vector2(col, row), drawingBounds[0], null))
                    {
                        nextPixel = Color.clear;
                    }

                    for (var i = 1; i < textures.Count; i++)
                    {
                        var offset = offsets[i];
                        var nextPixelX = col - offset.x;
                        var nextPixelY = row - offset.y;
                        var nextTexture = textures[i];
                        var nextSize = sizes[i];
                        var pixel = nextTexture.GetPixel(nextPixelX, nextPixelY);

                        if (pixel.a <= 0.5f ||
                            col < offset.x ||
                            row < offset.y ||
                            nextPixelX >= nextSize.x ||
                            nextPixelY >= nextSize.y ||
                            !IsWithinSpaceOrNoSpaceSpecified(new Vector2(col, row), drawingBounds[i],null))
                        {
                            continue;
                        }

                        nextPixel = nextTexture.GetPixel(nextPixelX, nextPixelY);
                    }

                    finalPixels[pixelCount] = nextPixel;
                    pixelCount++;
                }
            }

            var res = new Texture2D(finalSize.x, finalSize.y);
            res.filterMode = FilterMode.Point;
            res.SetPixels(finalPixels);
            res.Apply();
            return res;
        }
        public static Texture2D AlphaBlend(this Texture2D aBottom,
            Texture2D aTop,
            Vector2Int offset,
            Rect drawInRect,
            List<Rect> cutOuts,
            string forceColor = default,
            bool flipX = false,
            bool forceOnlyDrawOnTransparentPixels = false)
        {
            var bCoords = new Vector2Int(aBottom.width, aBottom.height);
            var tCoords = new Vector2Int(aTop.width, aTop.height);

            var bData = aBottom.GetPixels();

            int count = bData.Length;
            var rData = new Color[count];
            var p = 0;
            var forcedColorRaw = Color.white;

            if (!string.IsNullOrEmpty(forceColor))
            {
                forcedColorRaw = forceColor.ToColor();
            }

            for (var row = 0; row < bCoords.y; row++)
            {
                for (var col = 0; col < bCoords.x; col++)
                {
                    var bP = aBottom.GetPixel(col, row);
                    var cX = flipX? (tCoords.x - 1 - col) + offset.x : col - offset.x;
                    var cY = row - offset.y;
                    var tP = aTop.GetPixel(cX, cY);

                    if (forceOnlyDrawOnTransparentPixels)
                    {
                        if (bP.a > 0)
                        {
                            rData[p] = bP;
                        }
                        else
                        {
                            if (tP.a > 0.5 &&
                                col >= offset.x &&
                                row >= offset.y &&
                                cX < tCoords.x &&
                                cY < tCoords.y &&
                                IsWithinSpaceOrNoSpaceSpecified(new Vector2(col, row), drawInRect, cutOuts))
                            {
                                if (!string.IsNullOrEmpty(forceColor))
                                    rData[p] = forcedColorRaw;
                                else
                                    rData[p] = tP;
                            }
                            else
                            {
                                rData[p] = bP;
                            }
                        }
                    }
                    else
                    {
                        if (tP.a > 0.5 &&
                            col >= offset.x &&
                            row >= offset.y &&
                            cX < tCoords.x &&
                            cY < tCoords.y &&
                            IsWithinSpaceOrNoSpaceSpecified(new Vector2(col, row), drawInRect, cutOuts))
                        {
                            if (!string.IsNullOrEmpty(forceColor))
                                rData[p] = forcedColorRaw;
                            else
                                rData[p] = tP;
                        }
                        else
                        {
                            rData[p] = bP;
                        }
                    }

                    p++;
                }
            }

            var res = new Texture2D(aBottom.width, aBottom.height);
            res.filterMode = FilterMode.Point;
            res.SetPixels(rData);
            res.Apply();
            return res;
        }

        public static bool IsWithinSpaceOrNoSpaceSpecified(
            Vector2 point,
            Rect drawInRect,
            List<Rect> cutOuts)
        {
            if (drawInRect.size == Vector2.zero) return true;
            if (!drawInRect.Contains(point)) return false;
            if (cutOuts == null) return true;
            if (cutOuts.Any(rect => rect.Contains(point))) return false;

            return true;
        }

        public static Texture2D DuplicateTexture(this Texture2D source)
        {
            if (source == null) return Texture2D.blackTexture;
            byte[] pix = source.GetRawTextureData();
            Texture2D readableText = new Texture2D(source.width, source.height, source.format, false);
            readableText.filterMode = FilterMode.Point;
            readableText.LoadRawTextureData(pix);
            readableText.Apply();
            return readableText;
        }

        public static Texture2D DuplicateTexture(this Texture2D source, Rect cutout, string color = default)
        {
            if (source == null) return Texture2D.blackTexture;
            var sourceDuplicate = source.DuplicateTexture();
            var colors = sourceDuplicate.GetPixels((int)cutout.x, (int)cutout.y, (int)cutout.width, (int)cutout.height);
            Texture2D readableText = new Texture2D((int)cutout.width, (int)cutout.height, sourceDuplicate.format, false);
            readableText.filterMode = FilterMode.Point;
            readableText.SetPixels(colors);
            readableText.Apply();
            return readableText;
        }
    }
}