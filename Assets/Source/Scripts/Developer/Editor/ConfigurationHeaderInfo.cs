using Sirenix.OdinInspector;

namespace CrossBlitz.Source.Scripts.Developer
{
    public class ConfigurationHeaderInfo
    {
        [ReadOnly]
        public string info;
    }
}