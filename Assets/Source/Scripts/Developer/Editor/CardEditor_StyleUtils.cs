using UnityEditor;
using UnityEngine;

namespace Source.Scripts.Developer.Editor
{
    public partial class CardEditor
    {

        private GUIStyle LabelButtonStyle()
        {
            return new GUIStyle(GUI.skin.label) {padding = new RectOffset(8, 0, 0, 0)};
        }

        private GUIStyle LabelButtonSelectedStyle()
        {
            var style = new GUIStyle(GUI.skin.box)
                {alignment = TextAnchor.MiddleLeft, padding = new RectOffset(8, 0, 0, 0)};
            style.normal.textColor = Color.white;
            return style;
        }

        private void DrawVerticalSpacer(int width)
        {
            GUILayout.Box(GUIContent.none, GUIStyle.none, GUILayout.ExpandHeight(true), GUILayout.Width(width));
        }

        public void DrawUiLine(Color color, int thickness = 1, int padding = 10)
        {
            Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
            r.height = thickness;
            r.y += padding / 2;
            r.x -= 2;
            r.width += 6;
            EditorGUI.DrawRect(r, color);
        }

        public void DrawUiLineVertical(Color color, int thickness = 1, float width = 120, int padding = 10)
        {
            EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(false));
            Rect r = new Rect(width, EditorGUIUtility.singleLineHeight, thickness, Screen.height);
            EditorGUI.DrawRect(r, color);
            EditorGUILayout.EndHorizontal();
        }
    }
}
