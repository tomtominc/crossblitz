using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.ServerAPI;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Source.Scripts.Developer.Editor
{
    public partial class CardEditor
    {
        private ReorderableList _deckList;
        private DeckData _deckData;
        private Rect _deckStatsRect;

        private DeckData DrawDeck(DeckData deckData, bool renewList)
        {
            if (renewList || _deckList == null)
            {
                _deckList = new ReorderableList(deckData.cards,typeof(string), true,
                    true,true,true);
                _deckList.drawHeaderCallback += DrawHeaderCallback;
                _deckList.drawElementCallback += DrawCardProperty;
                _deckList.onAddCallback += AddCardToDeck;
                _deckList.onRemoveCallback += DeleteCardFromDeck;
                _deckList.onSelectCallback += SelectCardInDeck;
                _deckList.drawFooterCallback += DrawFooter;
            }

            deckData.name = EditorGUILayout.TextField("Name", deckData.name);
            deckData.hero = DrawHeroPopup("Hero", deckData.hero);
            deckData.faction =(Faction)EditorGUILayout.EnumPopup("Faction", deckData.faction);
            EditorGUILayout.PrefixLabel("Info");
            deckData.info = EditorGUILayout.TextArea(deckData.info);

            _deckList.DoLayoutList();

            deckData.cards = (List<CardValue>) _deckList.list;

            _deckData = deckData;

            return deckData;
        }

        private void AddCardToDeck(ReorderableList list)
        {
            list.list.Add(cards.Keys.ToList()[0]);
            list.index = list.count - 1;
            SelectCardInDeck(list);
        }

        private void SelectCardInDeck(ReorderableList list)
        {
            Repaint();
        }

        private void DrawHeaderCallback(Rect rect)
        {
            float nameSize = rect.width;
            float currentX = rect.x + 10;
            float currentY = rect.y + 2;

            EditorGUI.LabelField(new Rect(currentX,currentY,nameSize, rect.height ),
                "Deck List");

        }

        private  void DrawCardProperty(Rect rect, int index, bool isActive, bool isFocused)
        {
            if (index >= _deckList.count || index < 0)
            {
                return;
            }

            string[] contents = ((string) _deckList.list[index]).Split(',');
            string cardId =contents[0];
            int count = 0;

            if (contents.Length > 1)
                count = Int32.Parse( contents[1]);

            float nameSize = rect.width-120;
            float propSize = 120;
            float currentX = rect.x;

            var cardIdsAll = cards.Keys.ToList();
            var cardIdOrganizedList = new List<string>();
            for (var i = 0; i < cardIdsAll.Count; i++)
            {
                var card = GetCard(cardIdsAll[i]);
                cardIdOrganizedList.Add($"{card.faction}/{cardIdsAll[i]}");
            }
            int curr = EditorGUI.Popup(new Rect(rect.x,rect.y,nameSize, rect.height ),
                    cards.Keys.ToList().IndexOf(cardId), cardIdOrganizedList.ToArray());
            if (curr < 0 || curr > cards.Keys.Count)
            {
                curr = 0;
            }

            cardId = cards.Keys.ToList()[curr];

            currentX += nameSize;

            if (GUI.Button(new Rect(currentX, rect.y, propSize/3f, rect.height),
                EditorGUIUtility.IconContent("d_Toolbar Minus@2x"), EditorStyles.miniButtonLeft))
            {
                count--;
                if (count < 0) count = 0;
            }

            currentX += propSize / 3f;
            count =  EditorGUI.IntField(new Rect(currentX, rect.y, propSize/3f, rect.height),
                count, EditorStyles.miniButtonMid);
            currentX += propSize / 3f;

            if (GUI.Button(new Rect(currentX, rect.y, propSize/3f, rect.height),
                EditorGUIUtility.IconContent("d_Toolbar Plus@2x"), EditorStyles.miniButtonRight))
            {
                count++;
                if (count > 2) count = 2;
            }

            _deckList.list[index] = cardId + "," + count;
        }

        private void DeleteCardFromDeck(ReorderableList list)
        {
            ReorderableList.defaultBehaviours.DoRemoveButton(list);

            Repaint();
        }

        private void DrawFooter(Rect rect)
        {
            _deckStatsRect = new Rect(rect.x, rect.y+(rect.height/2f), rect.width, rect.height*6);
            ReorderableList.defaultBehaviours.DrawFooter(rect,_deckList);
        }

        private void DrawDeckStats(DeckData deckData)
        {
            var stats = "Deck Stats\n";

            // get stats here
            float totalCost = 0;
            float totalCount = 0;
            for (var i = 0; i < deckData.cards.Count; i++)
            {
                var cardId = deckData.GetCardId(i);
                if (string.IsNullOrEmpty(cardId)) continue;
                var cardCount = deckData.GetCardAtIndexCount(i);
                if (cardCount <= 0) continue;
                var card = GetCard(cardId);
                if (card ==null) continue;

                totalCost += card.cost * cardCount;
                totalCount += cardCount;
            }

            var average = totalCost / totalCount;
            stats += $"Card Count: {totalCount}/{DeckData.MaxCardsPerDeck}\n";
            stats += $"Average Cost: {average}";

            EditorGUILayout.HelpBox(stats, MessageType.Info);
        }
    }
}
