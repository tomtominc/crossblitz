using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.Hero;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Source.Scripts.Developer.Editor
{
    public partial class CardEditor
    {
        private ReorderableList _heroPowerList;
        private ReorderableList _heroLevelList;
        private List<float> _levelHeights;
        private int _activeLevelIndex;
        private ReorderableList _heroLevelRewardList;
        private bool _changedHero;
        private bool _heroInfoFoldout;
        private bool _heroPowersFoldout;
        private bool _heroLevelTreeFoldout;

        private string DrawHeroPopup(string label, string selected)
        {
            if (cards == null || cards.Count <= 0)
                return string.Empty;

            var heroIds = heroes.Keys.ToList();
            var index = EditorGUILayout.Popup(label,
                heroIds.IndexOf(selected),
                heroIds.ToArray());

            if (index >= 0 && index < heroIds.Count)
            {
                selected = heroIds[index];
            }

            return selected;
        }

        private HeroData DrawHero(HeroData hero, bool newHero)
        {
            EditorGUILayout.Space();
            var cName = hero == null ? "Unknown" : hero.name;
            EditorGUILayout.LabelField(cName, GetTitleLabelStyle(), GUILayout.Height(_titleFontSize * 1.25f));
            DrawUiLine(new Color(0.5f, 0.5f, 0.5f, 1));
            _heroInfoFoldout =
                EditorGUILayout.Foldout(_heroInfoFoldout, "Basic Information", GetFoldoutStyle());
            if (_heroInfoFoldout)
            {
                EditorGUI.indentLevel++;
                if (hero != null)
                {
                    hero.name = EditorGUILayout.TextField("Name", hero.name);
                    EditorGUILayout.BeginHorizontal();
                    hero.portraitUrl = EditorGUILayout.TextField("Portrait Url", hero.portraitUrl);
                    if (GUILayout.Button("Fix", GUILayout.Width(24)))
                    {
                        hero.portraitUrl = hero.name.ToLower().Replace(" ", "-");
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.LabelField("Description", EditorStyles.largeLabel);
                    hero.description = EditorGUILayout.TextArea(hero.description, EditorStyles.textArea,
                        GUILayout.Height(48));
                    hero.faction = (Faction)EditorGUILayout.EnumPopup("Faction", hero.faction);
                    //hero.skin = EditorGUILayout.IntField("Skin", hero.skin);

                    EditorGUILayout.LabelField("Portrait Offset", EditorStyles.largeLabel);
                    EditorGUILayout.BeginHorizontal(GUILayout.Width(240));
                    hero.standardOffset = EditorGUILayout.Vector2IntField(GUIContent.none, hero.standardOffset);
                    hero.zoomedOffset = EditorGUILayout.Vector2IntField(GUIContent.none, hero.zoomedOffset);
                    EditorGUILayout.EndHorizontal();
                }
                EditorGUI.indentLevel--;
            }

            DrawUiLine(new Color(0.5f, 0.5f, 0.5f, 1));

            _heroPowersFoldout =
                EditorGUILayout.Foldout(_heroLevelTreeFoldout, "Powers", GetFoldoutStyle());

            if (_heroPowersFoldout)
            {
                EditorGUI.indentLevel++;

                if (hero != null)
                {
                    // if (hero.powers == null)
                    // {
                    //     hero.powers = new List<string>();
                    // }

                    if (newHero || _heroPowerList == null)
                    {
                        // _heroPowerList = new ReorderableList(hero.powers, typeof(string),
                        //     true, true,
                        //     true, true);
                        _heroPowerList.drawHeaderCallback += (r) =>
                        {
                            EditorGUI.LabelField(r, "Hero Powers");
                        };
                        _heroPowerList.drawElementCallback += (r, index, active, focused) =>
                        {
                            if (index >= _heroPowerList.list.Count || index < 0) return;
                            _heroPowerList.list[index] =
                                DrawCardPopup(r, $"Power[{index}]", (string)_heroPowerList.list[index]);
                        };
                        _heroPowerList.onAddCallback += list =>
                        {
                            list.list.Add(string.Empty);
                            list.index = list.count - 1;
                        };
                        // _heroPowerList.onRemoveCallback += RemoveLevelFromHero;
                        // _heroPowerList.onSelectCallback += SelectLevelFromHero;
                        //_heroLevelList.elementHeightCallback += DrawHeroLevelHeight;
                    }

                    _heroPowerList.DoLayoutList();

                    //hero.powers = (List<string>)_heroPowerList.list;
                }

                EditorGUI.indentLevel--;
            }

            DrawUiLine(new Color(0.5f, 0.5f, 0.5f, 1));

            _heroLevelTreeFoldout =
                EditorGUILayout.Foldout(_heroLevelTreeFoldout, "Level Tree", GetFoldoutStyle());

            if (_heroLevelTreeFoldout)
            {
                EditorGUI.indentLevel++;

                if (hero != null)
                {
                    //if (hero.levels == null || hero.levels.Count <= 0)
                    //{
                    //    hero.levels = GetGenericLevelProgress();
                    //}

                    //if (newHero || _heroLevelList == null)
                    //{
                    //    _heroLevelList = new ReorderableList(hero.levels, typeof(HeroLevelData),
                    //        true,true,
                    //        true,true);
                    //    _heroLevelList.drawHeaderCallback += DrawHeroListHeader;
                    //    _heroLevelList.drawElementCallback += DrawHeroListElement;
                    //    _heroLevelList.onAddCallback += AddLevelToHero;
                    //    _heroLevelList.onRemoveCallback += RemoveLevelFromHero;
                    //    _heroLevelList.onSelectCallback += SelectLevelFromHero;
                    //    //_heroLevelList.elementHeightCallback += DrawHeroLevelHeight;
                    //}

                    //_heroLevelList.DoLayoutList();

                    //hero.levels = (List<HeroLevelData>)_heroLevelList.list;
                }

                EditorGUI.indentLevel--;
            }


            return hero;
        }

        private void DrawHeroListHeader(Rect rect)
        {
            EditorGUI.LabelField(rect, "Level Progress");
        }

        private void DrawHeroListElement(Rect rect, int index, bool isActive, bool isFocused)
        {
            if (index >= _heroLevelList.count || index < 0) return;

            if (_levelHeights == null)
            {
                _levelHeights = new List<float>();
            }

            if (_levelHeights.Count <= index)
            {
                _levelHeights.Add(EditorGUIUtility.singleLineHeight);
            }

            var height = EditorGUIUtility.singleLineHeight;

            //var levelData = (LevelData)_heroLevelList.list[index];

            var elementWidth = rect.width / 3;
            var elementX = rect.x;

            EditorGUI.LabelField(
                new Rect(elementX, rect.y, elementWidth, height),
                $"Level {index + 1}");

            elementX += elementWidth;

            // levelData.health = EditorGUI.IntField(
            //     new Rect(elementX, rect.y, elementWidth + elementWidth, height),
            //     "Health", levelData.health);
            //
            // if (isActive)
            // {
            //     if (_activeLevelIndex != index || _heroLevelRewardList == null)
            //     {
            //         _heroLevelRewardList = new ReorderableList(levelData.rewards, typeof(Reward), true,
            //             true, true, true);
            //         _heroLevelRewardList.drawHeaderCallback += (r) =>
            //         {
            //             EditorGUI.LabelField(r, $"Rewards (Level{index + 1})");
            //         };
            //
            //         _heroLevelRewardList.drawElementCallback += DrawRewardElement;
            //         _heroLevelRewardList.onAddCallback += AddReward;
            //         // _heroLevelRewardList.onRemoveCallback += DeleteReward;
            //         // _heroLevelRewardList.onSelectCallback += SelectReward;
            //         // _heroLevelRewardList.elementHeightCallback += DrawRewardHeight;
            //
            //         _activeLevelIndex = index;
            //     }
            //
            //     _heroLevelRewardList.DoLayoutList();
            //     levelData.rewards = (List<Reward>)_heroLevelRewardList.list;
            //
            //     height += _heroLevelRewardList.GetHeight();
            // }

            _levelHeights[index] = height;
        }

        private void DrawRewardElement(Rect rect, int index, bool isActive, bool isFocused)
        {
            if (index >= _heroLevelRewardList.count || index < 0) return;

            var reward = (Reward)_heroLevelRewardList.list[index];

            if (reward.type == RewardType.Card)
            {
                var elementWidth = rect.width / 3;
                var elementX = rect.x;
                reward.type = (RewardType)EditorGUI.EnumPopup(
                    new Rect(elementX, rect.y, elementWidth, rect.height),
                    reward.type);
                elementX += elementWidth;
                reward.reward = DrawCardPopup(
                    new Rect(elementX, rect.y, elementWidth, rect.height),
                    string.Empty, reward.reward);
                elementX += elementWidth;
                reward.amount = EditorGUI.IntField(
                    new Rect(elementX, rect.y, elementWidth, rect.height),
                    reward.amount);
            }
            else if (reward.type == RewardType.Gold || reward.type == RewardType.Keystones)
            {
                var elementWidth = rect.width / 2;
                var elementX = rect.x;
                reward.type = (RewardType)EditorGUI.EnumPopup(
                    new Rect(elementX, rect.y, elementWidth, rect.height),
                    reward.type);
                elementX += elementWidth;
                reward.amount = EditorGUI.IntField(
                    new Rect(elementX, rect.y, elementWidth, rect.height),
                    reward.amount);
            }
            else if (reward.type == RewardType.Power)
            {
                var elementWidth = rect.width;
                var elementX = rect.x;
                reward.type = (RewardType)EditorGUI.EnumPopup(
                    new Rect(elementX, rect.y, elementWidth, rect.height),
                    reward.type);
            }
            else if (reward.type == RewardType.Chest)
            {
                var elementWidth = rect.width / 2;
                var elementX = rect.x;
                reward.type = (RewardType)EditorGUI.EnumPopup(
                    new Rect(elementX, rect.y, elementWidth, rect.height),
                    reward.type);
                elementX += elementWidth;
                // reward.chest = (ChestType)EditorGUI.EnumPopup(
                //     new Rect(elementX, rect.y, elementWidth, rect.height),
                //     reward.chest);
            }
        }

        private void AddReward(ReorderableList list)
        {
            list.list.Add(new Reward() { type = RewardType.Gold });
            list.index = list.count - 1;
        }

        private void AddLevelToHero(ReorderableList list)
        {
            //list.list.Add(new LevelData());
            list.index = list.count - 1;
            SelectLevelFromHero(list);
        }

        private void SelectLevelFromHero(ReorderableList list)
        {
            Repaint();
        }

        private void RemoveLevelFromHero(ReorderableList list)
        {
            ReorderableList.defaultBehaviours.DoRemoveButton(list);
            Repaint();
        }

        private float DrawHeroLevelHeight(int index)
        {
            if (_levelHeights.Count <= index)
                return EditorGUIUtility.singleLineHeight;

            return _levelHeights[index];
        }

        // private List<LevelData> GetGenericLevelProgress()
        // {
        //     var levels = new List<LevelData>();
        //     //
        //     // // level 1 (Starting level, no rewards)
        //     // var level = new LevelData { health = 20 };
        //     // levels.Add(level);
        //     // // level 2
        //     // level = new LevelData { health = 20 };
        //     // level.rewards = new List<Reward>
        //     // {
        //     //     new Reward {type = RewardType.Card, amount = 1},
        //     //     new Reward {type = RewardType.Gold, amount = 100},
        //     //     new Reward {type = RewardType.Keystones, amount = 10},
        //     // };
        //     // levels.Add(level);
        //     // // level 3
        //     // level = new LevelData { health = 22 };
        //     // level.rewards = new List<Reward>
        //     // {
        //     //     new Reward {type = RewardType.Gold, amount = 100},
        //     //     new Reward {type = RewardType.Keystones, amount = 10},
        //     // };
        //     // levels.Add(level);
        //     // // level 4
        //     // level = new LevelData { health = 22 };
        //     // level.rewards = new List<Reward>
        //     // {
        //     //     new Reward {type = RewardType.Card, amount = 1},
        //     //     new Reward {type = RewardType.Gold, amount = 100},
        //     //     new Reward {type = RewardType.Keystones, amount = 10},
        //     // };
        //     // levels.Add(level);
        //     // // level 5
        //     // level = new LevelData { health = 25 };
        //     // level.rewards = new List<Reward>
        //     // {
        //     //     new Reward {type = RewardType.Chest, chest = ChestType.ClassicChest, amount = 1},
        //     //     new Reward {type = RewardType.Gold, amount = 100},
        //     //     new Reward {type = RewardType.Keystones, amount = 10},
        //     // };
        //     // levels.Add(level);
        //     // // level 6
        //     // level = new LevelData { health = 25 };
        //     // level.rewards = new List<Reward>
        //     // {
        //     //     new Reward {type = RewardType.Card, amount = 1},
        //     //     new Reward {type = RewardType.Gold, amount = 100},
        //     //     new Reward {type = RewardType.Keystones, amount = 10},
        //     // };
        //     // levels.Add(level);
        //     // // level 7
        //     // level = new LevelData { health = 25 };
        //     // level.rewards = new List<Reward>
        //     // {
        //     //     new Reward {type = RewardType.Gold, amount = 100},
        //     //     new Reward {type = RewardType.Keystones, amount = 10},
        //     // };
        //     // levels.Add(level);
        //     // // level 8
        //     // level = new LevelData { health = 28 };
        //     // level.rewards = new List<Reward>
        //     // {
        //     //     new Reward {type = RewardType.Card, amount = 1},
        //     //     new Reward {type = RewardType.Gold, amount = 100},
        //     //     new Reward {type = RewardType.Keystones, amount = 10},
        //     // };
        //     // levels.Add(level);
        //     // // level 9
        //     // level = new LevelData { health = 28 };
        //     // level.rewards = new List<Reward>
        //     // {
        //     //     new Reward {type = RewardType.Gold, amount = 100},
        //     //     new Reward {type = RewardType.Keystones, amount = 10},
        //     // };
        //     // levels.Add(level);
        //     // // level 10
        //     // level = new LevelData { health = 30 };
        //     // level.rewards = new List<Reward>
        //     // {
        //     //     new Reward {type = RewardType.Chest, chest = ChestType.ClassicChest, amount = 1},
        //     //     new Reward {type = RewardType.Power },
        //     //     new Reward {type = RewardType.Gold, amount = 100},
        //     //     new Reward {type = RewardType.Keystones, amount = 10},
        //     // };
        //     // levels.Add(level);
        //     // // level 11
        //     // level = new LevelData { health = 30 };
        //     // level.rewards = new List<Reward>
        //     // {
        //     //     new Reward {type = RewardType.Card, amount = 1},
        //     //     new Reward {type = RewardType.Gold, amount = 100},
        //     //     new Reward {type = RewardType.Keystones, amount = 10},
        //     // };
        //     // levels.Add(level);
        //     // // level 12
        //     // level = new LevelData { health = 32 };
        //     // level.rewards = new List<Reward>
        //     // {
        //     //     new Reward {type = RewardType.Gold, amount = 100},
        //     //     new Reward {type = RewardType.Keystones, amount = 10},
        //     // };
        //     // levels.Add(level);
        //     // // level 13
        //     // level = new LevelData { health = 32 };
        //     // level.rewards = new List<Reward>
        //     // {
        //     //     new Reward {type = RewardType.Gold, amount = 100},
        //     //     new Reward {type = RewardType.Keystones, amount = 10},
        //     // };
        //     // levels.Add(level);
        //     // // level 14
        //     // level = new LevelData { health = 32 };
        //     // level.rewards = new List<Reward>
        //     // {
        //     //     new Reward {type = RewardType.Gold, amount = 100},
        //     //     new Reward {type = RewardType.Keystones, amount = 10},
        //     // };
        //     // levels.Add(level);
        //     // // level 15
        //     // level = new LevelData { health = 35 };
        //     // level.rewards = new List<Reward>
        //     // {
        //     //     new Reward {type = RewardType.Card, amount = 1},
        //     //     new Reward {type = RewardType.Gold, amount = 100},
        //     //     new Reward {type = RewardType.Keystones, amount = 10},
        //     // };
        //     // levels.Add(level);
        //     // // level 16
        //     // level = new LevelData { health = 35 };
        //     // level.rewards = new List<Reward>
        //     // {
        //     //     new Reward {type = RewardType.Gold, amount = 100},
        //     //     new Reward {type = RewardType.Keystones, amount = 10},
        //     // };
        //     // levels.Add(level);
        //     // // level 17
        //     // level = new LevelData { health = 35 };
        //     // level.rewards = new List<Reward>
        //     // {
        //     //     new Reward {type = RewardType.Gold, amount = 100},
        //     //     new Reward {type = RewardType.Keystones, amount = 10},
        //     // };
        //     // levels.Add(level);
        //     // // level 18
        //     // level = new LevelData { health = 38 };
        //     // level.rewards = new List<Reward>
        //     // {
        //     //     new Reward {type = RewardType.Gold, amount = 100},
        //     //     new Reward {type = RewardType.Keystones, amount = 10},
        //     // };
        //     // levels.Add(level);
        //     // // level 19
        //     // level = new LevelData { health = 38 };
        //     // level.rewards = new List<Reward>
        //     // {
        //     //     new Reward {type = RewardType.Gold, amount = 100},
        //     //     new Reward {type = RewardType.Keystones, amount = 10},
        //     // };
        //     // levels.Add(level);
        //     // // level 20
        //     // level = new LevelData { health = 40 };
        //     // level.rewards = new List<Reward>
        //     // {
        //     //     new Reward {type = RewardType.Chest, chest = ChestType.ClassicChest, amount = 1},
        //     //     new Reward {type = RewardType.Power },
        //     //     new Reward {type = RewardType.Gold, amount = 100},
        //     //     new Reward {type = RewardType.Keystones, amount = 10},
        //     // };
        //     // levels.Add(level);
        //
        //     return levels;
        // }
    }
}
