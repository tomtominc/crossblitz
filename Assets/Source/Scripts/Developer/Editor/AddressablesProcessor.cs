using System.IO;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEngine;

namespace CrossBlitz.Source.Scripts.Developer
{
    public class AddressablesProcessor
    {
        [MenuItem("Assets/Addressable Tools/Add Effect Object", false, 0)]
        public static void AddAsAnEffectObject()
        {
            var guids = Selection.assetGUIDs;

            if (null == guids || guids.Length == 0)
                return;

            for (var i = 0; i < guids.Length; i++)
            {
                var guid = guids[i];
                var path = AssetDatabase.GUIDToAssetPath(guid);
                var asset = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(path);

                if (asset == null) continue;

                SpriteAnimationAsset sprAsset=null;
                
                if (asset is Texture2D t2d)
                {
                    // we need to find the animator
                    var assetName = Path.GetFileNameWithoutExtension(path);
                    var animationAssetName = assetName.Split('@')[0];

                    path = $"{SpriteAnimationAssetCreation.ANIMATION_SAVE_PATH}{animationAssetName}.asset";
                    sprAsset = AssetDatabase.LoadAssetAtPath<SpriteAnimationAsset>(path);
                    guid = AssetDatabase.AssetPathToGUID(path);
                    
                    Debug.Log($"Sprite Asset is still null ? {sprAsset==null} Path={path}");

                }
                else if (asset is SpriteAnimationAsset spriteAnimationAsset)
                {
                    sprAsset = spriteAnimationAsset;
                }
                
                if(sprAsset == null)
                {
                    continue;
                }
                
                AddressableAssetSettingsDefaultObject.Settings.CreateOrMoveEntry(guid, AddressableAssetSettingsDefaultObject.Settings.DefaultGroup, false, true);
                var assetEntry = AddressableAssetSettingsDefaultObject.Settings.FindAssetEntry(guid);
                assetEntry.address = "animations/" + sprAsset.name;
                assetEntry.SetLabel("animation-asset", true, true);
            }

            AssetDatabase.Refresh();
        }

        [MenuItem("Assets/Addressable Tools/Create Addressable", false, 0)]
        public static void CreateAddressable()
        {
            var guids = Selection.assetGUIDs;

            if (null == guids || guids.Length == 0)
                return;

            for (var i = 0; i < guids.Length; i++)
            {
                var guid = guids[i];
                var path = AssetDatabase.GUIDToAssetPath(guid);
                var assetName = Path.GetFileNameWithoutExtension(path);
                AddressableAssetSettingsDefaultObject.Settings.CreateOrMoveEntry(guid, AddressableAssetSettingsDefaultObject.Settings.DefaultGroup, false, true);
                AddressableAssetSettingsDefaultObject.Settings.FindAssetEntry(guid).address = assetName;
            }

            AssetDatabase.Refresh();
        }
    }
}