using System;
using CrossBlitz.Card;
using UnityEditor;
using UnityEngine;

namespace Source.Scripts.Developer.Editor
{
    public class CardCreationWindow : EditorWindow
    {
        public static string cardId;
        public static string cardName;
        public static CardType cardType;
        public static Faction faction;

        public static Action<string, string, CardType> Created;
        public static Action Canceled;

        public static void ShowWindow()
        {
            var window = GetWindow<CardCreationWindow>();
            window.titleContent = new GUIContent("Create Card");
            window.Show();
        }

        private void OnGUI()
        {
            cardId = EditorGUILayout.TextField("GDE ID", cardId);
            cardName = EditorGUILayout.TextField("Name", cardName);

            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Create"))
            {
                Created(cardId, cardName, cardType);
                Close();
            }

            if (GUILayout.Button("Cancel"))
            {
                Canceled();
                Close();
            }
            EditorGUILayout.EndHorizontal();
        }
    }
}
