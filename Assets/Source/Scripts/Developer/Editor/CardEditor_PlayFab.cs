using System.Collections.Generic;
using PlayFab;
#if ENABLE_PLAYFABADMIN_API
using PlayFab.AdminModels;
using UnityEngine;

#endif

namespace Source.Scripts.Developer.Editor
{
    public partial class CardEditor
    {


        public void PF_UpdateCardsInCatalog()
        {
#if ENABLE_PLAYFABADMIN_API
            loading = true;

            var catalogItems = new List<CatalogItem>();

            foreach (var pair in cards)
            {
                var card = pair.Value;

                var item = new CatalogItem
                {
                    ItemId = card.id,
                    DisplayName = card.name,
                    Description = card.description,
                    IsStackable = true,
                    ItemClass = "Card",
                    Consumable = new CatalogItemConsumableInfo {UsageCount = 1}
                };

                catalogItems.Add(item);
            }

            var request = new UpdateCatalogItemsRequest
            {
                Catalog = catalogItems,
                CatalogVersion = null
            };

            PlayFabAdminAPI.UpdateCatalogItems(request,
                UpdateCatalogItemsSuccess,
                UpdateCatalogItemsFailed);
#endif
        }

        public void PF_UpdateHeroesInCatalog()
        {
#if ENABLE_PLAYFABADMIN_API
            loading = true;

            var catalogItems = new List<CatalogItem>();

            foreach (var pair in heroes)
            {
                var hero = pair.Value;

                var item = new CatalogItem
                {
                    ItemId = hero.id,
                    DisplayName = hero.name,
                    Description = hero.description,
                    IsStackable = true,
                    ItemClass = "Hero"
                };

                catalogItems.Add(item);
            }

            var request = new UpdateCatalogItemsRequest
            {
                Catalog = catalogItems,
                CatalogVersion = null
            };

            PlayFabAdminAPI.UpdateCatalogItems(request,
                UpdateCatalogItemsSuccess,
                UpdateCatalogItemsFailed);
#endif
        }
#if ENABLE_PLAYFABADMIN_API
        private void UpdateCatalogItemsSuccess(UpdateCatalogItemsResult result)
        {
            Debug.Log("Updated Store....OK");
            loading = false;
            Focus();
        }
#endif
#if ENABLE_PLAYFABADMIN_API
        private void UpdateCatalogItemsFailed(PlayFabError error)
        {
            Debug.LogError("Updated Store....FAILED");
            loading = false;
            Focus();
        }
#endif
    }
}
