using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using GameDataEditor;
using UnityEditor;
using UnityEngine;

namespace Source.Scripts.Developer.Editor
{
    public partial class CardEditor
    {

        private void DrawToolbar()
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);

            if (GUILayout.Button("GDE Load",  EditorStyles.toolbarButton))
            {
                GDELoad();
            }

            GUILayout.Space(5);

            if (GUILayout.Toggle(mode == EditMode.CARD,"Card Editor",  EditorStyles.toolbarButton))
            {
                mode = EditMode.CARD;
            }

            GUILayout.Space(5);

            if (GUILayout.Toggle(mode == EditMode.HERO,"Hero Editor",  EditorStyles.toolbarButton))
            {
                mode = EditMode.HERO;
            }

            GUILayout.Space(5);

            if (GUILayout.Toggle(mode == EditMode.DECK,"Deck Editor",  EditorStyles.toolbarButton))
            {
                mode = EditMode.DECK;
            }

            if (GUILayout.Button("Update Containers",  EditorStyles.toolbarButton))
            {
                PF_UpdateCardsInCatalog();
            }

            GUILayout.FlexibleSpace();

            searchText = EditorGUILayout.TextField(searchText, EditorStyles.toolbarSearchField);

            EditorGUILayout.EndHorizontal();
        }

        private void DrawCardListSideBar(int scrollWidth)
        {
            EditorGUILayout.BeginVertical(GUILayout.Width(scrollWidth));
            labelScrollPosition = EditorGUILayout.BeginScrollView(labelScrollPosition, GUILayout.Width(scrollWidth));

            if (cards != null)
            {
                List<string> keys = cards.Keys.ToList();

                for (int i = 0; i < keys.Count; i++)
                {
                    var style = GUIStyle.none;

                    if (keys[i] .Equals( currentCard) )
                    {
                        style = LabelButtonSelectedStyle();
                    }
                    else
                    {
                        style = LabelButtonStyle();
                    }

                    if (GUILayout.Button(cards[keys[i]].name,style, GUILayout.Width(scrollWidth)))
                    {
                        currentCard = keys[i];
                    }
                }
            }

            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();
        }

        private void DrawHeroListSideBar(int scrollWidth)
        {
            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(scrollWidth));
            labelScrollPosition = EditorGUILayout.BeginScrollView(labelScrollPosition, GUILayout.Width(scrollWidth));

            if (heroes != null)
            {
                List<string> keys = heroes.Keys.ToList();

                for (var i = 0; i < keys.Count; i++)
                {
                    var style = GUIStyle.none;

                    if (keys[i] .Equals( currentHero) )
                    {
                        style = LabelButtonSelectedStyle();
                    }
                    else
                    {
                        style = LabelButtonStyle();
                    }

                    if (GUILayout.Button(heroes[keys[i]].name,style, GUILayout.Width(scrollWidth)))
                    {
                        currentHero = keys[i];
                        _changedHero = true;
                    }
                }
            }

            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();
        }

        private void DrawDeckListSideBar(int scrollWidth)
        {
            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(scrollWidth));
            labelScrollPosition = EditorGUILayout.BeginScrollView(labelScrollPosition, GUILayout.Width(scrollWidth));

            if (decks != null)
            {
                var keys = decks.Keys.ToList();

                for (var i = 0; i < keys.Count; i++)
                {
                    var style = keys[i] .Equals( currentDeck) ? LabelButtonSelectedStyle() : LabelButtonStyle();

                    if (GUILayout.Button(decks[keys[i]].name,style, GUILayout.Width(scrollWidth)))
                    {
                        currentDeck = keys[i];
                        changedDeck = true;
                    }
                }
            }

            EditorGUILayout.EndScrollView();

            if (decks != null &&
                GUILayout.Button(EditorGUIUtility.IconContent("d_Toolbar Plus@2x"), EditorStyles.miniButton))
            {
                var newDeck = new DeckData
                {
                    name = "New Deck",
                    faction = Faction.War,
                    hero = GDEItemKeys.Hero_Redcroft
                };

                decks.Add(newDeck.uid, newDeck);
            }
            EditorGUILayout.EndVertical();
        }

    }
}
