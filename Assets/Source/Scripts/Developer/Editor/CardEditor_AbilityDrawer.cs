using System.Text.RegularExpressions;
using CrossBlitz.Card;
using UnityEditor;
using UnityEngine;

namespace Source.Scripts.Developer.Editor
{
    public partial class CardEditor
    {
        public Ability DrawAbility( Ability ability)
        {
            if (ability == null)
            {
                ability = new Ability();
            }

            EditorGUI.indentLevel++;

            var foldout = EditorPrefs.GetBool(ability.GetHashCode().ToString(), false);
            foldout = EditorGUILayout.Foldout(foldout, "Ability " + ability.keyword);
            EditorPrefs.SetBool(ability.GetHashCode().ToString(), foldout);

            if (foldout)
            {
                EditorGUI.indentLevel++;

                ability.keyword = (AbilityKeyword) EditorGUILayout.EnumPopup("Keyword", ability.keyword);
                ability.triggerType = (TriggerType) EditorGUILayout.EnumPopup("TriggerType", ability.triggerType);
                ability.triggerConditions =
                    (FilterCondition) EditorGUILayout.EnumFlagsField("Trigger Conditions", ability.triggerConditions);
                ability.targetConditions =
                    (FilterCondition) EditorGUILayout.EnumFlagsField("Filter Conditions", ability.targetConditions);
                ability.usesGenericIcon = EditorGUILayout.Toggle("Uses Generic Icon", ability.usesGenericIcon);

                if (ability.keyword == AbilityKeyword.HEAL)
                {
                    ability.healAmount = EditorGUILayout.IntField("Heal Amount", ability.healAmount);
                }
                else if (ability.keyword == AbilityKeyword.DUAL_STRIKE)
                {
                    //ability.multiAttackCount = EditorGUILayout.IntField("Multi-Attack Count", ability.multiAttackCount);
                }
                else if (ability.keyword == AbilityKeyword.DAMAGE)
                {
                    EditorGUI.indentLevel++;

                    var damageFoldout = EditorPrefs.GetBool("AbilityDrawerDamage", true);
                    damageFoldout = EditorGUILayout.BeginFoldoutHeaderGroup(damageFoldout, "Damage");
                    EditorPrefs.SetBool("AbilityDrawerDamage", damageFoldout);

                    ability.damageValue = EditorGUILayout.IntField("Damage Value", ability.damageValue);
                    ability.targetCountBasedOnConditions = EditorGUILayout.Toggle("Target Count Based On Condition",
                        ability.targetCountBasedOnConditions);

                    if (!ability.targetCountBasedOnConditions)
                    {
                        ability.targetCount = EditorGUILayout.IntField("Target Count", ability.targetCount);
                    }
                    else
                    {
                        var dfoldout = EditorPrefs.GetBool(ability.GetHashCode() + "dfoldout", false);
                        dfoldout = EditorGUILayout.Foldout(dfoldout, "Target Count Condition");
                        EditorPrefs.SetBool(ability.GetHashCode() + "dfoldout", dfoldout);

                        if (dfoldout)
                        {
                            EditorGUI.indentLevel++;

                            ability.targetCountCondition = DrawCondition(ability.targetCountCondition);

                            EditorGUI.indentLevel--;
                        }
                    }

                    var exp = "if(any_minion is Damaged) GainStats(1,1)";

                    EditorGUILayout.EndFoldoutHeaderGroup();
                    EditorGUI.indentLevel--;

                }
                else if (ability.keyword == AbilityKeyword.THORNS)
                {
                    ability.damageValue = EditorGUILayout.IntField("Thorns Value", ability.damageValue);
                }
                else if (ability.keyword == AbilityKeyword.ARMOR_BODY)
                {
                    ability.armorBodyAmount = EditorGUILayout.IntField("Armor Body Amount", ability.armorBodyAmount);
                }
                else if (ability.keyword == AbilityKeyword.MODIFY_HEALTH)
                {
                    ability.healthModifier = EditorGUILayout.IntField("Health Modifier", ability.healthModifier);
                }
                else if (ability.keyword == AbilityKeyword.MODIFY_POWER)
                {
                    ability.powerModifier = EditorGUILayout.IntField("Power Modifier", ability.powerModifier);
                }
                else if (ability.keyword == AbilityKeyword.GIVE_ABILITY)
                {
                    //ability.givenAbility = DrawAbility(ability.givenAbility);
                }
                else if (ability.keyword == AbilityKeyword.SUMMON)
                {
                    var sfoldout = EditorPrefs.GetBool(ability.GetHashCode() + "sfoldout", false);
                    sfoldout = EditorGUILayout.Foldout(sfoldout, "Summon Properties");
                    EditorPrefs.SetBool(ability.GetHashCode() + "sfoldout", sfoldout);

                    if (sfoldout)
                    {
                        EditorGUI.indentLevel++;
                        ability.summonMinionData.cardId =Regex.Replace(ability.summonMinionData.cardId, "[^0-9a-zA-Z]+", "");
                        ability.summonMinionData.cardId =
                            EditorGUILayout.TextField("Minion To Summon", ability.summonMinionData.cardId);
                        ability.summonMinionData.placement =
                            (SummonPlacement) EditorGUILayout.EnumFlagsField("Placement Options",
                                ability.summonMinionData.placement);
                        ability.summonMinionData.count =
                            EditorGUILayout.IntField("Count", ability.summonMinionData.count);

                        EditorGUI.indentLevel--;
                    }
                }
                else if (ability.keyword == AbilityKeyword.MODIFY_DAMAGE)
                {
                    ability.maxDamageTakenDuringHit =
                        EditorGUILayout.IntField("Max Damage", ability.maxDamageTakenDuringHit);
                }
                else if (ability.keyword == AbilityKeyword.GAIN_ARMOR)
                {
                    ability.armorModifier = EditorGUILayout.IntField("Armor Modifier", ability.armorModifier);
                }
                else if (ability.keyword == AbilityKeyword.DRAW || ability.keyword == AbilityKeyword.DRAW_FROM_BOTTOM)
                {
                    ability.drawCount = EditorGUILayout.IntField("Draw Count", ability.drawCount);
                }
                else if (ability.keyword == AbilityKeyword.ADD_CARD_TO_HAND)
                {
                    // ability.cardToAddToHand = Regex.Replace(ability.cardToAddToHand, "[^0-9a-zA-Z]+", "");
                    // ability.cardToAddToHand = DrawCardPopup("Card To Add", ability.cardToAddToHand);
                }

                ability.hasConditionalAbility =
                    EditorGUILayout.Toggle("Has Conditional Ability", ability.hasConditionalAbility);

                if (ability.hasConditionalAbility)
                {
                    var cfoldout = EditorPrefs.GetBool(ability.GetHashCode() + "cfoldout", false);
                    cfoldout = EditorGUILayout.Foldout(cfoldout, "Conditional Ability");
                    EditorPrefs.SetBool(ability.GetHashCode() + "cfoldout", cfoldout);

                    if (cfoldout)
                    {
                        EditorGUI.indentLevel++;

                        cfoldout = EditorPrefs.GetBool(ability.GetHashCode() + "c1foldout", false);
                        cfoldout = EditorGUILayout.Foldout(cfoldout, "Condition");
                        EditorPrefs.SetBool(ability.GetHashCode() + "c1foldout", cfoldout);

                        if (cfoldout)
                        {
                            EditorGUI.indentLevel++;

                            //ability.conditionForAltAbility = DrawCondition(ability.conditionForAltAbility);

                            EditorGUI.indentLevel--;
                        }

                        cfoldout = EditorPrefs.GetBool(ability.GetHashCode() + "c2foldout", false);
                        cfoldout = EditorGUILayout.Foldout(cfoldout, "Ability");
                        EditorPrefs.SetBool(ability.GetHashCode() + "c2foldout", cfoldout);

                        if (cfoldout)
                        {
                            EditorGUI.indentLevel++;

                            //ability.conditionalAbility = DrawAbility(ability.conditionalAbility);

                            EditorGUI.indentLevel--;
                        }

                        EditorGUI.indentLevel--;
                    }
                }

                EditorGUI.indentLevel--;
            }

            EditorGUI.indentLevel--;

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Delete"))
            {
                ability = null;
            }
            EditorGUILayout.EndHorizontal();

            return ability;
        }

        private static AbilityCondition DrawCondition(AbilityCondition condition)
        {
            // if (condition == null)
            // {
            //     condition = new AbilityCondition();
            // }
            //
            // condition.condition =
            //     (Condition) EditorGUILayout.EnumPopup("Condition",
            //         condition.condition);
            //
            // if (condition.condition == Condition.FACTION_ON_FIELD)
            // {
            //     condition.faction =
            //         (Faction) EditorGUILayout.EnumPopup("Faction",
            //             condition.faction);
            // }
            //
            // else if (condition.condition == Condition.CLASS_ON_FIELD)
            // {
            //     condition.@class =
            //         (Class) EditorGUILayout.EnumPopup("Class", condition.@class);
            // }
            // else if (condition.condition == Condition.DAMAGED)
            // {
            //     // nothing here?
            // }
            // else if (condition.condition == Condition.TARGET_FROZEN)
            // {
            //     // nothing here?
            // }

            return condition;
        }
    }
}
