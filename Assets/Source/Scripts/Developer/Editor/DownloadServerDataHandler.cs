using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.Hero;
using GameDataEditor;
using Newtonsoft.Json;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;
using CrossBlitz.Card.Data;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.Items.Data;
// using PlayFab;
// using PlayFab.AdminModels;
using Sirenix.OdinInspector;

namespace CrossBlitz.Source.Scripts.Developer
{
    public class DownloadServerDataHandler
    {
        [FoldoutGroup("Item Configuration")]
        public bool updateIngredientsWhenPullingGdeExcelData;
        [FoldoutGroup("Item Configuration")]
        public bool updateCardsWhenPullingGdeExcelData;


        [FoldoutGroup("Force Updates")]
        public bool forceUpdatePlayFabDatabase;
        [FoldoutGroup("Force Updates")]
        public bool shouldUpdateTree;

        [FoldoutGroup("Raw Databases")]
        public HeroDatabase heroDatabase;
        [FoldoutGroup("Raw Databases")]
        public CardDatabase cardDatabase;
        [FoldoutGroup("Raw Databases")]
        public DeckDatabase deckDatabase;
        [FoldoutGroup("Raw Databases")]
        public ShopsDatabase shopsDatabase;
        [FoldoutGroup("Raw Databases")]
        public ItemDatabase itemDatabase;
        [FoldoutGroup("Raw Databases")]
        public FablesDatabase fablesDatabase;
        [FoldoutGroup("Raw Databases")]
        public CutsceneDatabase cutsceneDatabase;

        public void DrawHeader()
        {

        }


        public void DrawGui()
        {
            SirenixEditorGUI.Title("Import & Export Settings", "A streamlined way to import/export data.",
                TextAlignment.Left, false);
            //SirenixEditorGUI.BeginHorizontalToolbar();
            {
                if (GUILayout.Button("Save All"))
                {
                    if (!Db.Initialized)
                    {
                        Db.ForceInit();
                    }

                    Db.Save();

                    // heroDatabase.Save();
                    // cardDatabase.Save();
                    // shopsDatabase.Save();
                    // itemDatabase.Save();
                    // fablesDatabase.Save();
                    // cutsceneDatabase.Save();
                }
            }

            SirenixEditorGUI.BeginHorizontalToolbar();
            {
                EditorGUILayout.LabelField("Import Cards From File");
                GUILayout.FlexibleSpace();

                if (SirenixEditorGUI.ToolbarButton(EditorGUIUtility.IconContent("CollabPull")))
                {
                    var cardFile = EditorUtility.OpenFilePanel("Open Bulk Card Import", Application.dataPath, "txt");

                    if (!string.IsNullOrEmpty(cardFile))
                    {
                        var debug = "Processed {0} Cards:\n";
                        var cardCount = 0;

                        var cards = File.ReadAllText(cardFile);
                        var cardsByLine = cards.Split('\n');
                        for (var i = 0; i < cardsByLine.Length; i++)
                        {
                            if (cardsByLine[i].StartsWith("//")) continue;

                            var cardProperties = cardsByLine[i].Split('$');
                            var cardName = cardProperties[0];

                            try
                            {
                                var cardId = cardName.Replace(" ", "");
                                var cardType = (CardType)Enum.Parse(typeof(CardType), cardProperties[1]);
                                var cardDescription = cardProperties.Length > 2 ? cardProperties[2] : string.Empty;
                                var rarity = cardProperties.Length > 3 ? (Rarity)Enum.Parse(typeof(Rarity), cardProperties[3]) : Rarity.Common;
                                var cardFaction = cardProperties.Length > 4 ? (Faction)Enum.Parse(typeof(Faction), cardProperties[4]) : Faction.Neutral;
                                var classType = cardProperties.Length > 5 ? (Class)Enum.Parse(typeof(Class), cardProperties[5]) : Class.None;
                                var set =cardProperties.Length > 6 ? (CardSet)Enum.Parse(typeof(CardSet), cardProperties[6]) : CardSet.Core;
                                var attackType = cardProperties.Length > 7 ? (AttackType)Enum.Parse(typeof(AttackType), cardProperties[7]) : AttackType.Melee;
                                var cost = cardProperties.Length > 8 ? int.Parse(cardProperties[8]) : 0;
                                var power =cardProperties.Length > 9 ? int.Parse(cardProperties[9]) : 0;
                                var health =cardProperties.Length > 10 ? int.Parse(cardProperties[10]) : 0;
                                var bbReward = cardProperties.Length > 11 ? cardProperties[11] : string.Empty;

                                var created = false;
                                var card = cardDatabase.GetCard(cardId);

                                if (card == null && cardDatabase.CreateCard(cardId, out var createdCard))
                                {
                                    created = true;
                                    card = createdCard;
                                }

                                if (card != null)
                                {
                                    card.name = cardName;
                                    card.nameAbbrev = cardName;
                                    card.type = cardType;
                                    card.rarity = rarity;
                                    card.description = cardDescription;
                                    card.faction = cardFaction;
                                    card.type = cardType;
                                    card.@class = classType;
                                    card.set = set;
                                    card.attackType = attackType;
                                    card.cost = cost;
                                    card.power = power;
                                    card.health = health;
                                    card.burstRewardDescription = bbReward;

                                    cardCount++;
                                    debug += $"{card.name} ({created})\n";

                                    Db.CardDatabase.AddIngredientsToCard(card);
                                }
                            }
                            catch (Exception e)
                            {
                                Debug.Log($"Failed on {cardName}. {e}");
                            }
                        }

                        Debug.LogFormat(debug, cardCount);
                    }
                }
            }
            SirenixEditorGUI.EndHorizontalToolbar();

            SirenixEditorGUI.BeginHorizontalToolbar();
            {
                EditorGUILayout.LabelField("PlayFab Data");
                GUILayout.FlexibleSpace();

                if (SirenixEditorGUI.ToolbarButton(EditorGUIUtility.IconContent("CollabPull")))
                {
                    DownloadServerData();
                }
            }
            SirenixEditorGUI.EndHorizontalToolbar();

            SirenixEditorGUI.BeginIndentedVertical(SirenixGUIStyles.ToggleGroupBackground);
            {
                if (cardDatabase != null)
                {
                    SirenixEditorGUI.BeginHorizontalToolbar();
                    {
                        EditorGUILayout.LabelField("Card Data");
                        GUILayout.FlexibleSpace();

                        if (SirenixEditorGUI.IconButton(EditorGUIUtility.IconContent("CollabPush").image))
                        {
                            //SaveCardDatabase();
                        }
                    }
                    SirenixEditorGUI.EndHorizontalToolbar();

                    SirenixEditorGUI.BeginHorizontalToolbar();
                    {
                        EditorGUILayout.LabelField("Upload Chests + Drop Tables");
                        GUILayout.FlexibleSpace();

                        if (SirenixEditorGUI.IconButton(EditorGUIUtility.IconContent("CollabPush").image))
                        {
                            //SaveCardDroptables();
                        }
                    }
                    SirenixEditorGUI.EndHorizontalToolbar();
                }

                if (heroDatabase != null)
                {

                    SirenixEditorGUI.BeginHorizontalToolbar();
                    {
                        EditorGUILayout.LabelField("Hero Data");
                        GUILayout.FlexibleSpace();

                        if (SirenixEditorGUI.IconButton(EditorGUIUtility.IconContent("CollabPush").image))
                        {
                            //SaveHeroDatabase();
                        }
                    }
                    SirenixEditorGUI.EndHorizontalToolbar();
                }

                if (deckDatabase != null)
                {
                    SirenixEditorGUI.BeginHorizontalToolbar();
                    {
                        EditorGUILayout.LabelField("Deck Data");
                        GUILayout.FlexibleSpace();

                        if (SirenixEditorGUI.IconButton(EditorGUIUtility.IconContent("CollabPush").image))
                        {
                            //SaveDeckDatabase();
                        }
                    }
                    SirenixEditorGUI.EndHorizontalToolbar();
                }

                if (itemDatabase != null)
                {
                    SirenixEditorGUI.BeginHorizontalToolbar();
                    {
                        EditorGUILayout.LabelField("Item Data");
                        GUILayout.FlexibleSpace();

                        if (SirenixEditorGUI.IconButton(EditorGUIUtility.IconContent("CollabPush").image))
                        {
                            //SaveItemDatabase();
                        }
                    }
                    SirenixEditorGUI.EndHorizontalToolbar();
                }
            }
            SirenixEditorGUI.EndIndentedVertical();
        }

        public void ImportExcelData()
        {
            GDEExcelManager.DoImport();

            if (updateIngredientsWhenPullingGdeExcelData)
            {
                var ing = GDEDataManager.GetAllItems<GDEItemData>();

                for (var i = 0; i < ing.Count; i++)
                {
                    var ingredient = ing[i];
                    var item = itemDatabase.GetItem(ingredient.Key);

                    if (item == null)
                    {
                        item = new ItemData();
                        item.ItemId = ingredient.Key;
                        item.DisplayName = ingredient.Name;
                        item.Description = ingredient.Description;
                        item.ItemClass = ingredient.ItemType;

                        Debug.Log($"(ItemDatabase) Successfully added a new item: {item.ItemId}");
                    }
                    else
                    {
                        item.DisplayName = ingredient.Name;
                        item.Description = ingredient.Description;
                        item.ItemClass = ingredient.ItemType;

                        Debug.Log($"(ItemDatabase) Successfully modified item: {item.ItemId}");
                    }
                }

                updateIngredientsWhenPullingGdeExcelData = false;
                shouldUpdateTree = true;
            }

            if (updateCardsWhenPullingGdeExcelData)
            {
                var cards = GDEDataManager.GetAllItems<GDECardData>();

                for (var i = 0; i < cards.Count; i++)
                {
                    var card = cards[i];
                    var item = itemDatabase.GetItem(card.Key);

                    if (item == null)
                    {
                        item = new ItemData();
                        item.ItemId = card.Key;
                        item.DisplayName = card.Name;
                        item.Description = card.Description;
                        item.IsStackable = true;
                        item.ItemClass = "Card";
                        item.Consumable = new ItemConsumableInfo();
                        item.Consumable.UsageCount = 1;
                        itemDatabase.Items.Add(item);

                        Debug.Log($"(ItemDatabase) Successfully added a new card item: {item.ItemId}");
                    }
                    else
                    {
                        item.DisplayName = card.Name;
                        item.Description = card.Description;
                        item.IsStackable = true;
                        item.ItemClass = "Card";
                        item.Consumable = new ItemConsumableInfo();
                        item.Consumable.UsageCount = 1;

                        Debug.Log($"(ItemDatabase) Successfully modified card item: {item.ItemId}");
                    }
                }

                updateCardsWhenPullingGdeExcelData = false;
                shouldUpdateTree = true;
            }
        }

        public void DownloadServerData()
        {
            if (!GDEDataManager.Init("gde_data"))
            {
                Debug.LogError("There was a problem loading GDE Data from excel.");
                return;
            }

            GetDatabases();
        }

        public void GetDatabasesLocally()
        {
            if (!GDEDataManager.Init("gde_data"))
            {
                Debug.LogError("There was a problem loading GDE Data from excel.");
                return;
            }

            cardDatabase = CardDatabase.GetOrCreateDatabase();
            // //heroDatabase = HeroDatabase.GetOrCreateDatabase();
            // deckDatabase = DeckDatabase.GetOrCreateDatabase();
            // shopsDatabase = ShopsDatabase.GetOrCreateDatabase();
            // itemDatabase = ItemDatabase.GetOrCreateDatabase();
            // fablesDatabase = FablesDatabase.GetOrCreateDatabase();
            // cutsceneDatabase = CutsceneDatabase.GetOrCreateDatabase();
            //
            // // heroDatabase.Init();
            cardDatabase.Init();
            // deckDatabase.Init();
            // shopsDatabase.Init();
            // itemDatabase.Init();
            // fablesDatabase.Init();
            // cutsceneDatabase.Init();
            //
            shouldUpdateTree = true;
        }

        /// <summary>
        /// Gets all the cards from firebase, it's event on success places them inside a dictionary
        /// </summary>
        private void GetDatabases()
        {
            Debug.LogError("this is no longer supported!");
            // var getTitleDataRequest = new GetTitleDataRequest();
            // getTitleDataRequest.Keys = new List<string> {"CardDatabase", "HeroDatabase", "DeckDatabase", "ShopsDatabase", "CutsceneDatabase", "FablesDatabase" };
            // PlayFabAdminAPI.GetTitleData(getTitleDataRequest, GetDatabaseSuccess, OnSetDatabaseFailure);
        }

        // private void GetDatabaseSuccess(GetTitleDataResult result)
        // {
        //     cardDatabase = CardDatabase.GetOrCreateDatabase();
        //     //heroDatabase = HeroDatabase.GetOrCreateDatabase();
        //     itemDatabase = ItemDatabase.GetOrCreateDatabase();
        //     fablesDatabase = FablesDatabase.GetOrCreateDatabase();
        //     cutsceneDatabase = CutsceneDatabase.GetOrCreateDatabase();
        //
        //     if (cardDatabase.Cards== null || cardDatabase.Cards.Count <= 0 || forceUpdatePlayFabDatabase)
        //     {
        //         var temp = JsonConvert.DeserializeObject<CardDatabasePlayFab>(result.Data[ "CardDatabase" ]);
        //         cardDatabase.Cards = new List<CardData>(temp.Cards);
        //     }
        //
        //     if (heroDatabase.Heroes == null ||heroDatabase.Heroes.Count <= 0|| forceUpdatePlayFabDatabase)
        //     {
        //         var temp = JsonConvert.DeserializeObject<HeroDatabasePlayFab>(result.Data["HeroDatabase"]);
        //         heroDatabase.Heroes = new List<HeroData>(temp.Heroes);
        //     }
        //
        //     if (cutsceneDatabase.Cutscenes==null||cutsceneDatabase.Cutscenes.Count <= 0|| forceUpdatePlayFabDatabase)
        //     {
        //         var temp = JsonConvert.DeserializeObject<CutsceneDatabasePlayFab>(result.Data["CutsceneDatabase"]);
        //         cutsceneDatabase.Cutscenes = new List<CutsceneData>();
        //         foreach (var cutscene in temp.Table) cutsceneDatabase.Cutscenes.Add(cutscene.Value);
        //     }
        //
        //     // heroDatabase.Init();
        //     cardDatabase.Init();
        //     fablesDatabase.Init();
        //     cutsceneDatabase.Init();
        //     itemDatabase.Init();
        //
        //     // heroDatabase.Save();
        //     cardDatabase.Save();
        //     fablesDatabase.Save();
        //     cutsceneDatabase.Save();
        //     itemDatabase.Save();
        //
        //     shouldUpdateTree = true;
        //
        //
        //     // if (itemDatabase.Items != null && itemDatabase.Items.Count > 0 && !forceUpdatePlayFabDatabase)
        //     // {
        //     //     itemDatabase.Init();
        //     //     shouldUpdateTree = true;
        //     //     forceUpdatePlayFabDatabase = false;
        //     // }
        //     // else
        //     // {
        //     //     PlayFabAdminAPI.GetCatalogItems(new GetCatalogItemsRequest(), OnGetCatalogItemsSuccess,OnGetCatalogItemsFailed);
        //     // }
        // }

        // private void OnGetCatalogItemsSuccess(GetCatalogItemsResult result)
        // {
        //     itemDatabase = ItemDatabase.GetOrCreateDatabase();
        //
        //     var newItems = new List<ItemData>();
        //
        //     foreach (var catalogueItem in result.Catalog)
        //     {
        //         var item = ItemData.Convert(catalogueItem);
        //         newItems.Add(item);
        //     }
        //
        //     itemDatabase.Items = new List<ItemData>(newItems);
        //
        //     PlayFabAdminAPI.GetRandomResultTables(new GetRandomResultTablesRequest(), GetRandomResultsTables, OnGetRandomResultsTablesFailed);
        // }
        //
        // private static void OnGetCatalogItemsFailed(PlayFabError error)
        // {
        //     Debug.LogError($"Get Catalog Items....FAILED\n{error.GenerateErrorReport()}");
        // }

        // private void GetRandomResultsTables(GetRandomResultTablesResult result)
        // {
        //     var newDropTables = new List<DropTable>();
        //
        //     foreach (var table in result.Tables)
        //     {
        //         newDropTables.Add( DropTable.Convert(table.Value) );
        //     }
        //
        //     itemDatabase.DropTables = new List<DropTable>(newDropTables);
        //     itemDatabase.Init();
        //     itemDatabase.Save();
        //
        //     shouldUpdateTree = true;
        //     forceUpdatePlayFabDatabase = false;
        //
        //
        // }
        //
        // private static void OnGetRandomResultsTablesFailed(PlayFabError error)
        // {
        //     Debug.LogError($"Get Drop Tables....FAILED\n{error.GenerateErrorReport()}");
        // }

        // private void SaveCardDatabase()
        // {
        //     var setTitleDataRequest = new SetTitleDataRequest();
        //     setTitleDataRequest.Key = "CardDatabase";
        //     setTitleDataRequest.Value = JsonConvert.SerializeObject(cardDatabase);
        //     PlayFabAdminAPI.SetTitleData(setTitleDataRequest, OnSetDatabaseSuccess, OnSetDatabaseFailure);
        //
        //     var updateCatalogItemsRequest = new UpdateCatalogItemsRequest();
        //     updateCatalogItemsRequest.Catalog = new List<CatalogItem>();
        //
        //     for (var i = 0; i < cardDatabase.Cards.Count; i++)
        //     {
        //         var card = cardDatabase.Cards[i];
        //         var catalogItem = new CatalogItem();
        //         catalogItem.ItemId = card.id;
        //         catalogItem.DisplayName = card.name;
        //         catalogItem.Description = card.description;
        //         catalogItem.IsStackable = true;
        //         catalogItem.ItemClass = "Card";
        //         catalogItem.Consumable = new CatalogItemConsumableInfo();
        //         catalogItem.Consumable.UsageCount = 1;
        //         updateCatalogItemsRequest.Catalog.Add(catalogItem);
        //     }
        //     PlayFabAdminAPI.UpdateCatalogItems(updateCatalogItemsRequest, OnUpdateCatalogItemsSuccess, OnUpdateCatalogItemsFailed);
        // }

        // private void SaveCardDroptables()
        // {
        //     var updateDropTables = new UpdateRandomResultTablesRequest();
        //     updateDropTables.Tables = new List<RandomResultTable>();
        //
        //     var splitCollections = cardDatabase.Cards.GroupBy(card => card.set);
        //     foreach (var collection in splitCollections)
        //     {
        //         if (collection.Key == CardSet.Token || collection.Key == CardSet.Any) continue;
        //
        //         // The drop table that chests use "normally"
        //         var commonDropTable = new RandomResultTable();
        //         commonDropTable.TableId = $"{collection.Key}-Commons";
        //         commonDropTable.Nodes = new List<ResultTableNode>();
        //         updateDropTables.Tables.Add(commonDropTable);
        //
        //         var commonsAndRaresDropTable = new RandomResultTable();
        //         commonsAndRaresDropTable.TableId = $"{collection.Key}-CommonsAndRares";
        //         commonsAndRaresDropTable.Nodes = new List<ResultTableNode>();
        //         updateDropTables.Tables.Add(commonsAndRaresDropTable);
        //
        //         var raresOrEpicsDropTable = new RandomResultTable();
        //         raresOrEpicsDropTable.TableId = $"{collection.Key}-RaresOrEpics";
        //         raresOrEpicsDropTable.Nodes = new List<ResultTableNode>();
        //         updateDropTables.Tables.Add(raresOrEpicsDropTable);
        //
        //         var raresOrBetterDropTable = new RandomResultTable();
        //         raresOrBetterDropTable.TableId = $"{collection.Key}-RaresOrBetter";
        //         raresOrBetterDropTable.Nodes = new List<ResultTableNode>();
        //         updateDropTables.Tables.Add(raresOrBetterDropTable);
        //
        //         var legendaryDropTable = new RandomResultTable();
        //         legendaryDropTable.TableId = $"{collection.Key}-Legendaries";
        //         legendaryDropTable.Nodes = new List<ResultTableNode>();
        //         updateDropTables.Tables.Add(legendaryDropTable);
        //
        //         foreach (var card in collection)
        //         {
        //             // if (card.dropTableData != null &&
        //             //     card.dropTableData.dropLocations.HasFlag(DropLocations.Chests))
        //             // {
        //             //     var tableNode = new ResultTableNode();
        //             //     tableNode.ResultItemType = ResultTableNodeType.ItemId;
        //             //     tableNode.ResultItem = card.id;
        //             //     tableNode.Weight = card.dropTableData.dropPercentage;
        //             //
        //             //     switch (card.rarity)
        //             //     {
        //             //         case Rarity.Common:
        //             //             commonDropTable.Nodes.Add(tableNode);
        //             //             commonsAndRaresDropTable.Nodes.Add(tableNode);
        //             //             break;
        //             //         case Rarity.Rare:
        //             //             commonsAndRaresDropTable.Nodes.Add(tableNode);
        //             //             raresOrEpicsDropTable.Nodes.Add(tableNode);
        //             //             raresOrBetterDropTable.Nodes.Add(tableNode);
        //             //             break;
        //             //         case Rarity.Mythic:
        //             //             raresOrBetterDropTable.Nodes.Add(tableNode);
        //             //             raresOrEpicsDropTable.Nodes.Add(tableNode);
        //             //             break;
        //             //         case Rarity.Legendary:
        //             //             raresOrBetterDropTable.Nodes.Add(tableNode);
        //             //             legendaryDropTable.Nodes.Add(tableNode);
        //             //             break;
        //             //     }
        //             // }
        //         }
        //     }
        //     PlayFabAdminAPI.UpdateRandomResultTables(updateDropTables, OnUpdateDropTablesSuccess, OnUpdateDropTablesFailed);
        // }
        //
        // private void OnUpdateDropTablesSuccess(UpdateRandomResultTablesResult result)
        // {
        //     Debug.Log("Set Drop Tables....OK");
        //
        //     var chests = GDEDataManager.GetAllItems<GDEChestData>();
        //
        //     var updateCatalogItemsRequest = new UpdateCatalogItemsRequest();
        //     updateCatalogItemsRequest.Catalog = new List<CatalogItem>();
        //
        //     for (var i = 0; i < chests.Count; i++)
        //     {
        //         // create 2 chests per chest, 1 for normal opening and the other to guarantee a legendary
        //         for (var j = 0; j < 2; j++)
        //         {
        //             var chest = chests[i];
        //             var catalogItem = new CatalogItem();
        //
        //             catalogItem.ItemId =j == 0? chest.Key: $"{chest.Key}-Legendary";
        //             catalogItem.DisplayName = j == 0? chest.ChestName : $"{chest.ChestName} (Legendary)";
        //             catalogItem.Description = $"Legendary variant of the {chest.ChestName}.\n\n{chest.Description}";
        //             catalogItem.IsStackable = true;
        //             catalogItem.ItemClass = "Chest";
        //             catalogItem.Consumable = new CatalogItemConsumableInfo();
        //             catalogItem.Consumable.UsageCount = 1;
        //             catalogItem.Container = new CatalogItemContainerInfo();
        //             catalogItem.Container.ResultTableContents = new List<string>();
        //             catalogItem.Container.ResultTableContents.Add($"{chest.Set}-Commons");
        //             catalogItem.Container.ResultTableContents.Add($"{chest.Set}-Commons");
        //             if (j == 0)
        //             {
        //                 catalogItem.Container.ResultTableContents.Add($"{chest.Set}-Commons");
        //                 catalogItem.Container.ResultTableContents.Add($"{chest.Set}-CommonsAndRares");
        //                 catalogItem.Container.ResultTableContents.Add($"{chest.Set}-RaresOrBetter");
        //             }
        //             else
        //             {
        //                 catalogItem.Container.ResultTableContents.Add($"{chest.Set}-CommonsAndRares");
        //                 catalogItem.Container.ResultTableContents.Add($"{chest.Set}-RaresOrEpics");
        //                 catalogItem.Container.ResultTableContents.Add($"{chest.Set}-Legendaries");
        //             }
        //             catalogItem.Container.VirtualCurrencyContents = new Dictionary<string, uint>();
        //
        //             if (chest.Gold > 0)
        //                 catalogItem.Container.VirtualCurrencyContents.Add(Currency.GOLD, (uint) chest.Gold);
        //             if (chest.Keystones > 0)
        //                 catalogItem.Container.VirtualCurrencyContents.Add(Currency.KEYSTONES, (uint) chest.Keystones);
        //
        //             catalogItem.VirtualCurrencyPrices = new Dictionary<string, uint>();
        //
        //             if (chest.GoldCost > 0) catalogItem.VirtualCurrencyPrices.Add(Currency.GOLD, (uint) chest.GoldCost);
        //             if (chest.KeystoneCost > 0)
        //                 catalogItem.VirtualCurrencyPrices.Add(Currency.KEYSTONES, (uint) chest.KeystoneCost);
        //
        //             updateCatalogItemsRequest.Catalog.Add(catalogItem);
        //         }
        //     }
        //
        //     PlayFabAdminAPI.UpdateCatalogItems(updateCatalogItemsRequest, OnUpdateCatalogItemsSuccess, OnUpdateCatalogItemsFailed);
        // }
        //
        // private void OnUpdateDropTablesFailed(PlayFabError error)
        // {
        //     Debug.LogError($"Set Drop Tables....FAILED\n{error.ErrorMessage}");
        // }
        //
        // private void SaveHeroDatabase()
        // {
        //     var setTitleDataRequest = new SetTitleDataRequest();
        //     setTitleDataRequest.Key = "HeroDatabase";
        //     setTitleDataRequest.Value = JsonConvert.SerializeObject(heroDatabase);
        //     PlayFabAdminAPI.SetTitleData(setTitleDataRequest, OnSetDatabaseSuccess, OnSetDatabaseFailure);
        //
        //     var updateCatalogItemsRequest = new UpdateCatalogItemsRequest();
        //     updateCatalogItemsRequest.Catalog = new List<CatalogItem>();
        //
        //     for (var i = 0; i < heroDatabase.Heroes.Count; i++)
        //     {
        //         var hero = heroDatabase.Heroes[i];
        //         var catalogItem = new CatalogItem();
        //         catalogItem.ItemId = hero.id;
        //         catalogItem.DisplayName = hero.name;
        //         catalogItem.Description = hero.description;
        //         catalogItem.IsStackable = false;
        //         catalogItem.ItemClass = "Hero";
        //         catalogItem.ItemImageUrl = hero.portraitUrl;
        //         updateCatalogItemsRequest.Catalog.Add(catalogItem);
        //     }
        //     PlayFabAdminAPI.UpdateCatalogItems(updateCatalogItemsRequest, OnUpdateCatalogItemsSuccess, OnUpdateCatalogItemsFailed);
        // }
        //
        // private void SaveDeckDatabase()
        // {
        //     var setTitleDataRequest = new SetTitleDataRequest();
        //     setTitleDataRequest.Key = "DeckDatabase";
        //     setTitleDataRequest.Value = JsonConvert.SerializeObject(deckDatabase);
        //     PlayFabAdminAPI.SetTitleData(setTitleDataRequest, OnSetDatabaseSuccess, OnSetDatabaseFailure);
        // }
        //
        // private void SaveItemDatabase()
        // {
        //     var updateCatalogItemsRequest = new UpdateCatalogItemsRequest();
        //     updateCatalogItemsRequest.Catalog = new List<CatalogItem>();
        //
        //     for (var i = 0; i < itemDatabase.Items.Count; i++)
        //     {
        //         var item = itemDatabase.Items[i];
        //         if (item.IsBundle()) continue;
        //         if (item.IsContainer()) continue;
        //
        //         var catalogItem = new CatalogItem();
        //         catalogItem.ItemId = item.ItemId;
        //         catalogItem.DisplayName = item.DisplayName;
        //         catalogItem.Description = item.Description;
        //         catalogItem.IsStackable = true;
        //         catalogItem.ItemClass = item.ItemClass;
        //         catalogItem.ItemImageUrl = item.ItemImageUrl;
        //         catalogItem.Consumable = new CatalogItemConsumableInfo
        //         {
        //             UsageCount = 1
        //         };
        //         updateCatalogItemsRequest.Catalog.Add(catalogItem);
        //     }
        //     PlayFabAdminAPI.UpdateCatalogItems(updateCatalogItemsRequest, OnUpdateCatalogItemsSuccess, OnUpdateCatalogItemsFailed);
        // }
        //
        // private void OnUpdateCatalogItemsSuccess(UpdateCatalogItemsResult result)
        // {
        //     //saveNeeded = false;
        //     Debug.Log("Set Catalog Items....OK");
        // }
        //
        // private void OnUpdateCatalogItemsFailed(PlayFabError error)
        // {
        //     Debug.LogError($"Set Catalog Items....FAILED\n{error.ErrorMessage}");
        // }
        //
        // private void OnSetDatabaseSuccess(SetTitleDataResult result)
        // {
        //     //saveNeeded = false;
        //     Debug.Log("Set Database....OK");
        // }
        //
        // private void OnSetDatabaseFailure(PlayFabError error)
        // {
        //     Debug.LogError($"Set Database....FAILED\n{error.ErrorMessage}");
        // }
        //
        // private void DeleteCard(CardData card)
        // {
        //     cardDatabase.Cards.Remove(card);
        //     SaveCardDatabase();
        // }
        //
        // private void DeleteHero(HeroData hero)
        // {
        //     heroDatabase.Heroes.Remove(hero);
        //     SaveHeroDatabase();
        // }
        //
        // private void DeleteDeck(DeckData deckData)
        // {
        //     deckDatabase.decks.Remove(deckData);
        //     SaveDeckDatabase();
        // }
    }
}