using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Hero;
using Newtonsoft.Json;
using UnityEngine;
using CrossBlitz.ServerAPI;
using GameDataEditor;
// #if ENABLE_PLAYFABADMIN_API
// using PlayFab;
// using PlayFab.AdminModels;
// #endif

namespace Source.Scripts.Developer.Editor
{
    public partial class CardEditor
    {

        private void GDELoad()
        {
            GDEDataManager.Init("gde_data");
            GetDatabases();
        }

        /// <summary>
        /// Gets all the cards from firebase, it's event on success places them inside a dictionary
        /// </summary>
        private void GetDatabases()
        {
            loading = true;

            // #if ENABLE_PLAYFABADMIN_API
            //
            // var getTitleDataRequest = new GetTitleDataRequest();
            // getTitleDataRequest.Keys = new List<string> {"CardDatabase", "HeroDatabase", "DeckDatabase"};
            // PlayFabAdminAPI.GetTitleData(getTitleDataRequest, GetDatabaseSuccess,OnSetDatabaseFailure);
            //
            // #endif
        }

        // private void GetDatabaseSuccess(GetTitleDataResult result)
        // {
        //     cards = new Dictionary<string, CardData>();
        //     heroes = new Dictionary<string, HeroData>();
        //     decks = new Dictionary<string, DeckData>();
        //
        //     var deckDatabase = JsonConvert.DeserializeObject<DeckDatabase>(result.Data["DeckDatabase"]);
        //
        //     for (var i = 0; i < deckDatabase.decks.Count; i++)
        //     {
        //         var deckData = deckDatabase.decks[i];
        //         decks.Add(deckData.uid, deckData);
        //         if (i == 0 && string.IsNullOrEmpty(currentDeck)) currentDeck = deckData.uid;
        //     }
        //
        //     GDERefresh();
        // }

        private void GDERefresh()
        {
            // var gde_cards = GDEDataManager.GetAllItems<GDECardData>();
            //
            // for (var i = 0; i < gde_cards.Count; i++)
            // {
            //     var gde_card = gde_cards[i];
            //
            //     if (!cards.ContainsKey(gde_card.Key))
            //     {
            //         CardData cardData = new CardData{id = gde_card.Key};
            //         cardData.ConvertData();
            //         cards.Add(cardData.id, cardData);
            //     }
            // }

            var gde_heroes = GDEDataManager.GetAllItems<GDEHeroData>();

            for (var i = 0; i < gde_heroes.Count; i++)
            {
                var gde_hero = gde_heroes[i];

                if (!heroes.ContainsKey(gde_hero.Key))
                {
                    HeroData heroData = new HeroData {id = gde_hero.Key};
                    heroData.ConvertData();
                    heroes.Add(heroData.id, heroData);
                }
            }

            loading = false;
            Focus();
        }

        private void SaveCardDatabase()
        {
            // loading = true;
            //
            // #if ENABLE_PLAYFABADMIN_API
            //
            // var cardDatabase = new CardDatabase();
            // cardDatabase.Cards = cards.Values.ToList();
            // cardDatabase.Init();
            //
            // var setTitleDataRequest = new SetTitleDataRequest();
            // setTitleDataRequest.Key = "CardDatabase";
            // setTitleDataRequest.Value = JsonConvert.SerializeObject(cardDatabase);
            // PlayFabAdminAPI.SetTitleData(setTitleDataRequest, OnSetDatabaseSuccess,OnSetDatabaseFailure);
            //
            // #endif

        }

        private void SaveHeroDatabase()
        {
            // loading = true;
            //
            // #if ENABLE_PLAYFABADMIN_API
            //
            // var heroDatabase = new HeroDatabase();
            // heroDatabase.Heroes = heroes.Values.ToList();
            //
            // var setTitleDataRequest = new SetTitleDataRequest();
            // setTitleDataRequest.Key = "HeroDatabase";
            // setTitleDataRequest.Value = JsonConvert.SerializeObject(heroDatabase);
            // PlayFabAdminAPI.SetTitleData(setTitleDataRequest, OnSetDatabaseSuccess,OnSetDatabaseFailure);
            //
            // #endif
        }

        private void SaveDeckDatabase()
        {
            // loading = true;
            //
            // #if ENABLE_PLAYFABADMIN_API
            //
            // var deckDatabase = new DeckDatabase();
            // deckDatabase.decks = decks.Values.ToList();
            //
            // var setTitleDataRequest = new SetTitleDataRequest();
            // setTitleDataRequest.Key = "DeckDatabase";
            // setTitleDataRequest.Value = JsonConvert.SerializeObject(deckDatabase);
            // PlayFabAdminAPI.SetTitleData(setTitleDataRequest, OnSetDatabaseSuccess,OnSetDatabaseFailure);
            //
            // #endif
        }

        // private void OnSetDatabaseSuccess(SetTitleDataResult result)
        // {
        //     // loading = false;
        //     // Focus();
        //     // Debug.Log("Set Database....OK");
        // }
        //
        // private void OnSetDatabaseFailure(PlayFabError error)
        // {
        //     Debug.LogError($"Set Database....FAILED\n{error.ErrorMessage}");
        // }

        private void DeleteCard(CardData card)
        {
            if (!cards.ContainsKey(card.id)) return;

            loading = true;
            cards.Remove(card.id);
            SaveCardDatabase();
        }

        private void DeleteHero(HeroData hero)
        {
            if (!heroes.ContainsKey(hero.id)) return;

            loading = true;
            heroes.Remove(hero.id);
            SaveHeroDatabase();
        }

        private void DeleteDeck(DeckData deckData)
        {
            if (!decks.ContainsKey(deckData.uid)) return;

            loading = true;
            decks.Remove(deckData.uid);
            SaveDeckDatabase();

        }
    }
}
