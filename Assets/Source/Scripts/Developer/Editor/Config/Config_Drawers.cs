
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Hero;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace CrossBlitz.Source.Scripts.Developer.Config
{
    public partial class ConfigurationEditor
    {
        private const int StandardCardWidth = 88;
        private const int StandardCardHeight = 100;

        private const int ZoomedCardWidth = 168;
        private const int ZoomedCardHeight = 202;

        private const int BoardCardWidth = 84;
        private const int BoardCardHeight = 80;

        private const int DeckCardWidth = 170;
        private const int DeckCardHeight = 34;

        [HideInInspector]
        public int standardCardZoomLevel = 2;
        [HideInInspector]
        public int zoomedCardZoomLevel = 2;
        [HideInInspector]
        public int boardCardZoomLevel = 2;
        [HideInInspector]
        public int deckCardZoomLevel = 2;


        private void DrawBegin()
        {
            OdinMenuTreeSelection selected = MenuTree.Selection;

            SirenixEditorGUI.BeginHorizontalToolbar();
            {
                GUILayout.FlexibleSpace();

                if (selected.SelectedValue is HeroData heroData)
                {
                    if (SirenixEditorGUI.ToolbarButton("Add New Level"))
                    {
                        //heroData.levelTree.levels.Add(new LevelReward());
                        ForceMenuTreeRebuild();
                    }
                    else if (selected.SelectedValue is DownloadServerDataHandler dw)
                    {
                        dw.DrawHeader();
                    }
                }
            }
            SirenixEditorGUI.EndHorizontalToolbar();

            if (selected.SelectedValue is DownloadServerDataHandler dwr)
            {
                dwr.DrawGui();
            }
        }

        private void DrawUpdate()
        {
            if (_downloadHandler.shouldUpdateTree)
            {
                _downloadHandler.shouldUpdateTree = false;
                ForceMenuTreeRebuild();
            }


        }

        private void DrawEnd()
        {
            //DrawOffsetTools();
        }

        private void DrawOffsetTools()
        {
            var selected = MenuTree.Selection;

            if (selected.SelectedValue is CardData cardData)
            {
                var controlRect = EditorGUILayout.GetControlRect();

                const float padding = 2;

                var boxRect = new Rect(controlRect.x + padding, controlRect.y, controlRect.width - padding, position.height-controlRect.y );
                EditorGUI.DrawRect(boxRect, SirenixGUIStyles.BoxBackgroundColor);

                var headerRect = new Rect(controlRect.x + padding, controlRect.y, controlRect.width - padding, 22);
                EditorGUI.DrawRect(headerRect, SirenixGUIStyles.HeaderBoxBackgroundColor);

                var labelRect = new Rect(controlRect.x + 10, controlRect.y, controlRect.width, 22);
                EditorGUI.LabelField(labelRect, "Offset Tools");

                var standardCardRect = DrawStandardCard(cardData, controlRect);
                var zoomedCardRect = DrawZoomedCard(cardData, controlRect, standardCardRect);
                var boardCardRect = DrawBoardCard(cardData, controlRect, zoomedCardRect);
                DrawDeckCard(cardData, controlRect, boardCardRect);
            }
        }

        private Rect DrawStandardCard(CardData cardData, Rect controlRect)
        {
            if (!_standardCardPreviews.ContainsKey(cardData.id))
            {
                var textures = new List<Texture2D>();
                var offsets = new List<Vector2Int>();
                var drawingBounds = new List<Rect>();

                var cardBackground = GetCardBackground(cardData);
                var portrait = GetCardPortrait(cardData);
                var cardFront = GetStandardCardFrontTexture(cardData);
                var classIcon = GetCardClassIcon(cardData);

                if (cardBackground)
                {
                    textures.Add(cardBackground);
                    offsets.Add( Vector2Int.zero);
                    drawingBounds.Add(new Rect(23, 25, 41, 55));
                }

                if (portrait)
                {
                    textures.Add(portrait);
                    offsets.Add(cardData.standardOffset);
                    drawingBounds.Add(new Rect(23, 25, 41, 55));
                }

                if (cardFront)
                {
                    textures.Add(cardFront);
                    offsets.Add( Vector2Int.zero);
                    drawingBounds.Add(Rect.zero);
                }

                if (classIcon)
                {
                    textures.Add(classIcon);
                    offsets.Add( new Vector2Int(7, 28));
                    drawingBounds.Add(Rect.zero);
                }

                var finalTexture = ImageHelpers.AlphaBlend2(textures, offsets, drawingBounds, new Vector2Int(cardFront.width, cardFront.height));

                if (_cardFont == null)
                {
                    _cardFont = GetCardFont();
                }

                finalTexture = DrawTextOverTexture(
                    cardData.nameAbbrev.ToUpper(),
                    new Vector2Int(13,15),
                    61, _cardFont,  finalTexture);

                if (_standardCardPreviews.ContainsKey(cardData.id))
                {
                    _standardCardPreviews[cardData.id] = finalTexture;
                }
                else
                {
                    _standardCardPreviews.Add(cardData.id, finalTexture);
                }
            }

            var width = StandardCardWidth * standardCardZoomLevel;
            var height = StandardCardHeight * standardCardZoomLevel;

            var titleBarRect = new Rect(controlRect.x + 22, controlRect.y + 44, width, 22);
            EditorGUI.DrawRect(titleBarRect, SirenixGUIStyles.HeaderBoxBackgroundColor);

            var labelRect = new Rect(titleBarRect.x + 10, titleBarRect.y, titleBarRect.width, titleBarRect.height);
            EditorGUI.LabelField(labelRect, "Standard Card");

            const float toolSize = 16;

            var totalZoomToolRect = new Rect(titleBarRect.x + titleBarRect.width - 66, titleBarRect.y + ((22-toolSize)/2), 66, titleBarRect.height);

            GUI.DrawTexture(new Rect(totalZoomToolRect.x, totalZoomToolRect.y, toolSize, toolSize), EditorIcons.MagnifyingGlass.Raw);

            if (SirenixEditorGUI.IconButton(new Rect(totalZoomToolRect.x+22, totalZoomToolRect.y, toolSize, toolSize),
                EditorIcons.Minus))
            {
                standardCardZoomLevel--;
                if (standardCardZoomLevel <= 0) standardCardZoomLevel = 1;
            }

            if (SirenixEditorGUI.IconButton(new Rect(totalZoomToolRect.x+44, totalZoomToolRect.y, toolSize, toolSize),
                EditorIcons.Plus))
            {
                standardCardZoomLevel++;
                if (standardCardZoomLevel > 3) standardCardZoomLevel = 3;
            }

            var standardCardRect = new Rect(titleBarRect.x, titleBarRect.y + 22, width, height);
            EditorGUI.DrawRect(standardCardRect, SirenixGUIStyles.BorderColor);
            GUI.DrawTexture(standardCardRect, _standardCardPreviews[cardData.id]);

            var toolBoxRect = new Rect(standardCardRect.x, standardCardRect.y + standardCardRect.height, standardCardRect.width, 32);
            EditorGUI.DrawRect(toolBoxRect, SirenixGUIStyles.HeaderBoxBackgroundColor);
            var offsetRect = new Rect(toolBoxRect.x + 10, toolBoxRect.y + 5, toolBoxRect.width-20, 22);
            cardData.standardOffset = EditorGUI.Vector2IntField(offsetRect, string.Empty, cardData.standardOffset);

            return standardCardRect;
        }

        private Rect DrawZoomedCard(CardData cardData, Rect controlRect, Rect standardCardRect)
        {
            if (!_zoomedCardPreviews.ContainsKey(cardData.id))
            {
                var textures = new List<Texture2D>();
                var offsets = new List<Vector2Int>();
                var drawingBounds = new List<Rect>();

                var cardBackground = GetCardBackground(cardData);
                var portrait = GetCardPortrait(cardData);
                var cardFront = GetZoomedCardFrontTexture(cardData);
                var classIcon = GetCardClassIcon(cardData);

                if (cardBackground)
                {
                    textures.Add(cardBackground);
                    offsets.Add( Vector2Int.zero);
                    drawingBounds.Add(new Rect(42, 80, 84, 91));
                }

                if (portrait)
                {
                    textures.Add(portrait);
                    offsets.Add(cardData.zoomedOffset);
                    drawingBounds.Add(new Rect(42, 80, 84, 91));
                }

                if (cardFront)
                {
                    textures.Add(cardFront);
                    offsets.Add( Vector2Int.zero);
                    drawingBounds.Add(Rect.zero);
                }

                if (classIcon)
                {
                    textures.Add(classIcon);
                    offsets.Add( new Vector2Int(28, 93));
                    drawingBounds.Add(Rect.zero);
                }

                var finalTexture = ImageHelpers.AlphaBlend2(textures, offsets, drawingBounds, new Vector2Int(cardFront.width, cardFront.height));


                if (_cardFont == null)
                {
                    _cardFont = GetCardFont();
                }

                finalTexture = DrawTextOverTexture(
                    cardData.nameAbbrev.ToUpper(),
                    new Vector2Int(16,80),
                    133, _cardFont,  finalTexture);

                if (_zoomedCardPreviews.ContainsKey(cardData.id))
                {
                    _zoomedCardPreviews[cardData.id] = finalTexture;
                }
                else
                {
                    _zoomedCardPreviews.Add(cardData.id, finalTexture);
                }
            }

            var width = ZoomedCardWidth*zoomedCardZoomLevel;
            var height = ZoomedCardHeight*zoomedCardZoomLevel;

            var titleBarRect = new Rect(standardCardRect.x + standardCardRect.width + 22, standardCardRect.y-22, width, 22);
            EditorGUI.DrawRect(titleBarRect, SirenixGUIStyles.HeaderBoxBackgroundColor);

            var labelRect = new Rect(titleBarRect.x + 10, titleBarRect.y, titleBarRect.width, titleBarRect.height);
            EditorGUI.LabelField(labelRect, "Zoomed Card");

            const float toolSize = 16;

            var totalZoomToolRect = new Rect(titleBarRect.x + titleBarRect.width - 66, titleBarRect.y + ((22-toolSize)/2), 66, titleBarRect.height);

            GUI.DrawTexture(new Rect(totalZoomToolRect.x, totalZoomToolRect.y, toolSize, toolSize), EditorIcons.MagnifyingGlass.Raw);

            if (SirenixEditorGUI.IconButton(new Rect(totalZoomToolRect.x+22, totalZoomToolRect.y, toolSize, toolSize),
                EditorIcons.Minus))
            {
                zoomedCardZoomLevel--;
                if (zoomedCardZoomLevel <= 0) zoomedCardZoomLevel = 1;
            }

            if (SirenixEditorGUI.IconButton(new Rect(totalZoomToolRect.x+44, totalZoomToolRect.y, toolSize, toolSize),
                EditorIcons.Plus))
            {
                zoomedCardZoomLevel++;
                if (zoomedCardZoomLevel > 3) zoomedCardZoomLevel = 3;
            }

            var zoomedCardRect = new Rect(titleBarRect.x, titleBarRect.y + 22 , width, height);
            EditorGUI.DrawRect(zoomedCardRect, SirenixGUIStyles.BorderColor);
            GUI.DrawTexture(zoomedCardRect, _zoomedCardPreviews[cardData.id]);

            var toolBoxRect = new Rect(zoomedCardRect.x, zoomedCardRect.y + zoomedCardRect.height, zoomedCardRect.width, 32);
            EditorGUI.DrawRect(toolBoxRect, SirenixGUIStyles.HeaderBoxBackgroundColor);
            var offsetRect = new Rect(toolBoxRect.x + 10, toolBoxRect.y + 5, toolBoxRect.width-20, 22);
            cardData.zoomedOffset = EditorGUI.Vector2IntField(offsetRect, string.Empty, cardData.zoomedOffset);

            return zoomedCardRect;
        }

        private Rect DrawBoardCard(CardData cardData, Rect controlRect, Rect zoomedCardRect)
        {
            if (!_boardCardPreviews.ContainsKey(cardData.id))
            {
                var textures = new List<Texture2D>();
                var offsets = new List<Vector2Int>();
                var drawingBounds = new List<Rect>();

                var cardBackground = GetCardBackground(cardData);
                var portrait = GetCardPortrait(cardData);
                var cardFront = GetBoardCardFrontTexture(cardData);
                //var classIcon = GetCardClassIcon(cardData);

                if (cardBackground)
                {
                    textures.Add(cardBackground);
                    offsets.Add( Vector2Int.zero);
                    drawingBounds.Add(new Rect(21,27,42,41));
                }

                if (portrait)
                {
                    textures.Add(portrait);
                    offsets.Add(cardData.boardOffset);
                    drawingBounds.Add(new Rect(21,27,42,41));
                }

                if (cardFront)
                {
                    textures.Add(cardFront);
                    offsets.Add( Vector2Int.zero);
                    drawingBounds.Add(Rect.zero);
                }

                var finalTexture = ImageHelpers.AlphaBlend2(textures, offsets, drawingBounds, new Vector2Int(cardFront.width, cardFront.height));

                if (_boardCardPreviews.ContainsKey(cardData.id))
                {
                    _boardCardPreviews[cardData.id] = finalTexture;
                }
                else
                {
                    _boardCardPreviews.Add(cardData.id, finalTexture);
                }
            }

            var width = BoardCardWidth*boardCardZoomLevel;
            var height = BoardCardHeight*boardCardZoomLevel;

            var titleBarRect = new Rect(zoomedCardRect.x + zoomedCardRect.width + 22, zoomedCardRect.y-22, width, 22);
            EditorGUI.DrawRect(titleBarRect, SirenixGUIStyles.HeaderBoxBackgroundColor);

            var labelRect = new Rect(titleBarRect.x + 10, titleBarRect.y, titleBarRect.width, titleBarRect.height);
            EditorGUI.LabelField(labelRect, "Board Card");

            const float toolSize = 16;

            var totalZoomToolRect = new Rect(titleBarRect.x + titleBarRect.width - 66, titleBarRect.y + ((22-toolSize)/2), 66, titleBarRect.height);

            GUI.DrawTexture(new Rect(totalZoomToolRect.x, totalZoomToolRect.y, toolSize, toolSize), EditorIcons.MagnifyingGlass.Raw);

            if (SirenixEditorGUI.IconButton(new Rect(totalZoomToolRect.x+22, totalZoomToolRect.y, toolSize, toolSize),
                EditorIcons.Minus))
            {
                boardCardZoomLevel--;
                if (boardCardZoomLevel <= 0) boardCardZoomLevel = 1;
            }

            if (SirenixEditorGUI.IconButton(new Rect(totalZoomToolRect.x+44, totalZoomToolRect.y, toolSize, toolSize),
                EditorIcons.Plus))
            {
                boardCardZoomLevel++;
                if (boardCardZoomLevel > 3) boardCardZoomLevel = 3;
            }

            var boardCardRect = new Rect(titleBarRect.x, titleBarRect.y + 22 , width, height);

            if (cardData.type == CardType.Minion)
            {
                EditorGUI.DrawRect(boardCardRect, SirenixGUIStyles.BorderColor);
                GUI.DrawTexture(boardCardRect, _boardCardPreviews[cardData.id]);

                var toolBoxRect = new Rect(boardCardRect.x, boardCardRect.y + boardCardRect.height, boardCardRect.width,
                    32);
                EditorGUI.DrawRect(toolBoxRect, SirenixGUIStyles.HeaderBoxBackgroundColor);
                var offsetRect = new Rect(toolBoxRect.x + 10, toolBoxRect.y + 5, toolBoxRect.width - 20, 22);
                cardData.boardOffset = EditorGUI.Vector2IntField(offsetRect, string.Empty, cardData.boardOffset);
            }

            return boardCardRect;
        }

        private void DrawDeckCard(CardData cardData, Rect controlRect, Rect boardCardRect)
        {
            if (!_deckCardPreviews.ContainsKey(cardData.id))
            {
                var finalTexture = GetDeckCardFrontTexture(cardData);
                var portrait = GetCardPortrait(cardData);

                if (portrait != null)
                {
                    finalTexture = finalTexture.AlphaBlend(portrait,
                        cardData.deckOffset, new Rect(4,4,34,26), null,flipX:false);
                }

                if (_cardFont == null)
                {
                    _cardFont = GetCardFont();
                }

                // finalTexture = DrawTextOverTexture(
                //     cardData.nameAbbrev.ToUpper(),
                //     new Vector2Int(16,80),
                //     133, _cardFont,  finalTexture);

                if (_deckCardPreviews.ContainsKey(cardData.id))
                {
                    _deckCardPreviews[cardData.id] = finalTexture;
                }
                else
                {
                    _deckCardPreviews.Add(cardData.id, finalTexture);
                }
            }

            var width = DeckCardWidth*deckCardZoomLevel;
            var height = DeckCardHeight*deckCardZoomLevel;

            var titleBarRect = new Rect(boardCardRect.x, boardCardRect.y+boardCardRect.height+44, width, 22);
            EditorGUI.DrawRect(titleBarRect, SirenixGUIStyles.HeaderBoxBackgroundColor);

            var labelRect = new Rect(titleBarRect.x + 10, titleBarRect.y, titleBarRect.width, titleBarRect.height);
            EditorGUI.LabelField(labelRect, "Deck Card");

            const float toolSize = 16;

            var totalZoomToolRect = new Rect(titleBarRect.x + titleBarRect.width - 66, titleBarRect.y + ((22-toolSize)/2), 66, titleBarRect.height);

            GUI.DrawTexture(new Rect(totalZoomToolRect.x, totalZoomToolRect.y, toolSize, toolSize), EditorIcons.MagnifyingGlass.Raw);

            if (SirenixEditorGUI.IconButton(new Rect(totalZoomToolRect.x+22, totalZoomToolRect.y, toolSize, toolSize),
                EditorIcons.Minus))
            {
                deckCardZoomLevel--;
                if (deckCardZoomLevel <= 0) deckCardZoomLevel = 1;
            }

            if (SirenixEditorGUI.IconButton(new Rect(totalZoomToolRect.x+44, totalZoomToolRect.y, toolSize, toolSize),
                EditorIcons.Plus))
            {
                deckCardZoomLevel++;
                if (deckCardZoomLevel > 3) deckCardZoomLevel = 3;
            }

            var deckCardRect = new Rect(titleBarRect.x, titleBarRect.y + 22 , width, height);
            EditorGUI.DrawRect(deckCardRect, SirenixGUIStyles.BorderColor);
            GUI.DrawTexture(deckCardRect, _deckCardPreviews[cardData.id]);

            var toolBoxRect = new Rect(deckCardRect.x, deckCardRect.y + deckCardRect.height, deckCardRect.width, 57);

            EditorGUI.DrawRect(toolBoxRect, SirenixGUIStyles.HeaderBoxBackgroundColor);
            var offsetRect = new Rect(toolBoxRect.x + 10, toolBoxRect.y + 5, toolBoxRect.width-20, 22);
            cardData.deckOffset = EditorGUI.Vector2IntField(offsetRect, string.Empty, cardData.deckOffset);

        }
    }
}