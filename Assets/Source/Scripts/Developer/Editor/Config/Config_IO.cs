using System.Collections.Generic;
using CrossBlitz.Card;
using TMPro;
using UnityEditor;
using UnityEngine;

namespace CrossBlitz.Source.Scripts.Developer.Config
{
    public partial class ConfigurationEditor
    {
        private const string StandardCardFrontAssetPath = "Assets/Source/Addressables/artwork/standard-card-builds/card-fronts/card-front@{0}-{1}.png";
        private const string BoardCardFrontAssetPath = "Assets/Source/Addressables/artwork/standard-card-builds/card-fronts-on-board/cob-card-front@{0}-{1}.png";
        private const string DeckCardFrontAssetPath = "Assets/Source/Artwork/Main Menu - Cards/Card-Slot/deck-card-slot@{0}-{1}.png";
        private const string ZoomedCardFrontAssetPath = "Assets/Source/Addressables/artwork/zoomed-card-builds/card-fronts/zc-card-front@{0}-{1}.png";
        private const string CardPortraitPath = "Assets/Source/Addressables/card-artwork/Color-Outlines/{0}-outlined.png";
        private const string CardClassIconPath = "Assets/Source/Addressables/artwork/standard-card-builds/class-icons/class-icon@{0}.png";
        private const string CardFontAssetPath = "Assets/Source/Fonts/HopeGold/HopeGold_Modified/HopeGold.asset";
        private const string CardBackgroundPath = "Assets/Source/Addressables/artwork/card-backgrounds/card-bg@{0}.png";

        private Texture2D GetStandardCardFrontTexture(CardData cardData)
        {
            if (_standardCardFronts != null && _standardCardFronts.ContainsKey(cardData.id))
                return _standardCardFronts[cardData.id];

            var txt = AssetDatabase.
                LoadAssetAtPath<Texture2D>(string.Format
                (StandardCardFrontAssetPath,cardData.faction.ToString()
                    .ToLower(),cardData.type.ToString().ToLower()))
                .DuplicateTexture();

            if (txt != null ) _standardCardFronts?.Add(cardData.id, txt);

            return txt;
        }

        private Texture2D GetZoomedCardFrontTexture(CardData cardData)
        {
            if (_zoomedCardFronts != null && _zoomedCardFronts.ContainsKey(cardData.id))
                return _zoomedCardFronts[cardData.id];

            var txt =  AssetDatabase.
                LoadAssetAtPath<Texture2D>(string.Format
                (ZoomedCardFrontAssetPath,cardData.faction.ToString()
                    .ToLower(),cardData.type.ToString().ToLower()))
                .DuplicateTexture();

            if (txt != null) _zoomedCardFronts?.Add(cardData.id, txt);

            return txt;
        }

        private Texture2D GetBoardCardFrontTexture(CardData cardData)
        {
            if (_boardCardFronts != null && _boardCardFronts.ContainsKey(cardData.id))
                return _boardCardFronts[cardData.id];

            var txt =  AssetDatabase.
                LoadAssetAtPath<Texture2D>(string.Format
                (BoardCardFrontAssetPath,cardData.faction.ToString()
                    .ToLower(),cardData.type.ToString().ToLower()))
                .DuplicateTexture();

            if (txt != null) _boardCardFronts?.Add(cardData.id, txt);

            return txt;
        }

        private Texture2D GetDeckCardFrontTexture(CardData cardData)
        {
            if (_deckCardFronts != null && _deckCardFronts.ContainsKey(cardData.id))
                return _deckCardFronts[cardData.id];

            var txt =  AssetDatabase.
                LoadAssetAtPath<Texture2D>(string.Format
                (DeckCardFrontAssetPath,cardData.faction.ToString()
                    .ToLower(),cardData.type.ToString().ToLower()))
                .DuplicateTexture();

            if (txt != null) _deckCardFronts?.Add(cardData.id, txt);

            return txt;
        }

        private Texture2D GetCardPortrait(CardData cardData)
        {
            if (_cardPortraits != null && _cardPortraits.ContainsKey(cardData.id))
                return _cardPortraits[cardData.id];

            var txt = AssetDatabase.LoadAssetAtPath<Texture2D>(string.Format(CardPortraitPath, cardData.portraitUrl));
            if (txt)
            {
                var dup = txt.DuplicateTexture();
                _cardPortraits?.Add(cardData.id, dup);
                return dup;
            }

            return null;
        }

        private Texture2D GetCardBackground(CardData cardData)
        {
            var background = $"{cardData.faction}-{cardData.type}";

            if (_cardBackgrounds != null && _cardBackgrounds.ContainsKey(background))
                return _cardBackgrounds[background];

            if (_cardBackgrounds == null)
            {
                _cardBackgrounds = new Dictionary<string, Texture2D>();
            }

            var txt = AssetDatabase.LoadAssetAtPath<Texture2D>(string.Format(CardBackgroundPath, background));

            if (txt)
            {
                var dup = txt.DuplicateTexture();
                _cardBackgrounds?.Add(background, dup);
                return dup;
            }

            return null;
        }

        private Texture2D GetCardClassIcon(CardData cardData)
        {
            if (cardData.@class == Class.None) return null;

            var classLower = cardData.@class.ToString().ToLower();

            if (_classIconTextures != null && _classIconTextures.ContainsKey(classLower))
                return _classIconTextures[classLower];

            var txt =  AssetDatabase.
                LoadAssetAtPath<Texture2D>(string.Format(CardClassIconPath, cardData.@class.ToString().ToLower())).
                DuplicateTexture();

            if (txt != null)
            {
                _classIconTextures?.Add(classLower, txt);
            }

            return txt;
        }

        private TMP_FontAsset GetCardFont()
        {
            if (_cardFont != null) return _cardFont;

            return AssetDatabase.LoadAssetAtPath<TMP_FontAsset>(CardFontAssetPath);
        }
    }
}