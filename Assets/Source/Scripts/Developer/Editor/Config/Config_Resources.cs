using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using TMPro;
using UnityEditor;
using UnityEngine;

namespace CrossBlitz.Source.Scripts.Developer.Config
{
    public partial class ConfigurationEditor
    {
        private DownloadServerDataHandler _downloadHandler;
        private TMP_FontAsset _cardFont;
        private Dictionary<string, Sprite> _classSprites;
        private Dictionary<string, Sprite> _storeSprites;
        private Dictionary<string, Texture2D> _standardCardFronts;
        private Dictionary<string, Texture2D> _zoomedCardFronts;
        private Dictionary<string, Texture2D> _boardCardFronts;
        private Dictionary<string, Texture2D> _deckCardFronts;
        private Dictionary<string, Texture2D> _standardCardPreviews;
        private Dictionary<string, Texture2D> _zoomedCardPreviews;
        private Dictionary<string, Texture2D> _boardCardPreviews;
        private Dictionary<string, Texture2D> _deckCardPreviews;
        private Dictionary<string, Sprite> _heroEmblems;
        private Dictionary<string, Texture2D> _cardPortraits;
        private Dictionary<string, Texture2D> _cardBackgrounds;
        private Dictionary<string, Texture2D> _classIconTextures;
        private List<Dictionary<string, Texture2D>> _resourceBank;

        private readonly List<Rect> _standardCardCutOuts = new List<Rect>
        {
            //top left
            new Rect(23, 99 - 26, 6, 7),
            new Rect(23, 99 - 27, 5, 1),
            new Rect(29, 99 - 25, 1, 6),
            new Rect(30, 99 - 24, 1, 5),

            //top right
            new Rect(58, 99 - 24, 6, 5),
            new Rect(59, 99 - 25, 5, 6),
            new Rect(60, 99 - 26, 4, 1),
            new Rect(61, 99 - 27, 3, 1),
            new Rect(57, 99 - 23, 1, 4),

            //top middle
            new Rect(35, 99 - 22, 18, 3),
            new Rect(34, 99 - 21, 1, 3),
            new Rect(33, 99 - 20, 1, 1),
            new Rect(53, 99 - 21, 1, 2),
            new Rect(54, 99 - 20, 1, 1),

            //bottom
            new Rect(23, 99 - 74, 1, 3),
            new Rect(63, 99 - 74, 1, 3),

        };

        private void InitResources()
        {
            if (_downloadHandler == null)
            {
                _downloadHandler = new DownloadServerDataHandler();
                _downloadHandler.GetDatabasesLocally();
            }

            if (_classSprites == null)
            {
                _classSprites=new Dictionary<string, Sprite>();

                var names = Enum.GetNames(typeof(Class));
                for (var i = 0; i < names.Length; i++)
                {
                    var path =
                        $"Assets/Source/Addressables/artwork/standard-card-builds/class-icons/class-icon@{names[i].ToLower()}.png";
                    _classSprites.Add(names[i], AssetDatabase.LoadAssetAtPath<Sprite>(path));
                }
            }

            if (_storeSprites == null)
            {
                _storeSprites = new Dictionary<string, Sprite>();
                var names = new []{ "cards-light", "expansions-light", "resources-light", "specials-light", "style-light"};
                for (var i = 0; i < names.Length; i++)
                {
                    var path =
                        $"Assets/Source/Artwork/Main Menu - Shop/Category-Icons/shop-category-icon@{names[i].ToLower()}.png";
                    _storeSprites.Add(names[i], AssetDatabase.LoadAssetAtPath<Sprite>(path));
                }
            }

            if (_standardCardFronts == null)
            {
                _standardCardFronts = new Dictionary<string, Texture2D>();
            }

            if (_zoomedCardFronts == null)
            {
                _zoomedCardFronts = new Dictionary<string, Texture2D>();
            }

            if (_boardCardFronts == null)
            {
                _boardCardFronts = new Dictionary<string, Texture2D>();
            }

            if (_deckCardFronts == null)
            {
                _deckCardFronts = new Dictionary<string, Texture2D>();
            }

            if (_heroEmblems == null)
            {
                _heroEmblems = new Dictionary<string, Sprite>();
            }

            if (_cardPortraits == null)
            {
                _cardPortraits =new Dictionary<string, Texture2D>();
            }

            if (_classIconTextures == null)
            {
                _classIconTextures = new Dictionary<string, Texture2D>();
            }

            if (_standardCardPreviews == null)
            {
                _standardCardPreviews = new Dictionary<string, Texture2D>();
            }

            if (_zoomedCardPreviews == null)
            {
                _zoomedCardPreviews = new Dictionary<string, Texture2D>();
            }

            if (_boardCardPreviews == null)
            {
                _boardCardPreviews = new Dictionary<string, Texture2D>();
            }

            if (_deckCardPreviews == null)
            {
                _deckCardPreviews = new Dictionary<string, Texture2D>();
            }

            if (_resourceBank == null)
            {
                _resourceBank = new List<Dictionary<string, Texture2D>>();
                _resourceBank.Add(_standardCardFronts);
                _resourceBank.Add(_zoomedCardFronts);
                _resourceBank.Add(_boardCardFronts);
                _resourceBank.Add(_deckCardFronts);
                _resourceBank.Add(_cardPortraits);
                _resourceBank.Add(_classIconTextures);
                _resourceBank.Add(_standardCardPreviews);
                _resourceBank.Add(_zoomedCardPreviews);
                _resourceBank.Add(_boardCardPreviews);
                _resourceBank.Add(_deckCardPreviews);
            }
        }

        private void CleanUp()
        {
            if (_resourceBank != null)
            {
                for (var i = 0; i < _resourceBank.Count; i++)
                {
                    if (_resourceBank[i] != null)
                    {
                        var textures = _resourceBank[i].Values.ToList();

                        for (var j = textures.Count - 1; j > -1; j--)
                        {
                            DestroyImmediate(textures[j]);
                        }

                        _resourceBank[i] = null;
                    }
                }

                _resourceBank = null;
            }
        }
    }
}