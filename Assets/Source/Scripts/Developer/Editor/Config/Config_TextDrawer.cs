using System;
using TMPro;
using UnityEngine;

namespace CrossBlitz.Source.Scripts.Developer.Config
{
    public partial class ConfigurationEditor
    {
         private Texture2D DrawTextOverTexture(string text, Vector2Int characterDisplayOffset, int totalSpace, TMP_FontAsset font, Texture2D finalTexture)
        {
            var textTotalSpaceUsed = 0;

            for (var i = 0; i < text.Length; i++)
            {
                var character = text[i];

                if (Char.IsWhiteSpace(character))
                {
                    textTotalSpaceUsed += 2;
                    continue;
                }

                var unicode = (uint) character;

                if (font.characterLookupTable.ContainsKey(unicode))
                {
                    var characterTmp = font.characterLookupTable[unicode];
                    var glyphInfo = font.glyphLookupTable[characterTmp.glyphIndex];
                    textTotalSpaceUsed += glyphInfo.glyphRect.width + 1;
                }
            }

            characterDisplayOffset.x += (totalSpace - textTotalSpaceUsed) / 2;

            for (var i = 0; i < text.Length; i++)
            {
                var character = text[i];

                if (Char.IsWhiteSpace(character))
                {
                    characterDisplayOffset.x += 2;
                    continue;
                }

                var unicode = (uint) character;

                if (font.characterLookupTable.ContainsKey(unicode))
                {
                    var characterTmp = font.characterLookupTable[unicode];
                    var glyphInfo = font.glyphLookupTable[characterTmp.glyphIndex];
                    var glyphRect = new Rect(glyphInfo.glyphRect.x, glyphInfo.glyphRect.y,
                        glyphInfo.glyphRect.width,
                        glyphInfo.glyphRect.height);
                    var atlasTexture = font.atlasTexture.DuplicateTexture(glyphRect);

                    finalTexture = finalTexture.AlphaBlend(atlasTexture, characterDisplayOffset, Rect.zero, null,
                        "#7a4c4e");

                    characterDisplayOffset.x += (int) glyphInfo.metrics.width + 1;
                }
                else
                {
                    Debug.LogError($"Unicode {unicode} of character {character} is not in the character table!");
                }
            }

            return finalTexture;
        }
    }
}