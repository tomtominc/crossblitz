﻿using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Developer.Tools;
using CrossBlitz.Hero;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEditor.Searcher;
using UnityEngine;
#if ENABLE_PLAYFABADMIN_API

#endif

namespace CrossBlitz.Source.Scripts.Developer.Config
{
    public partial class ConfigurationEditor : OdinMenuEditorWindow
    {
        [MenuItem("Window/Cross Blitz/Configuration")]
        private static void OpenMenu()
        {
            var window = GetWindow<Config.ConfigurationEditor>("Configuration");
            window.Show();
        }

        private void OnFocus()
        {
            if (_downloadHandler != null)
            {
                _downloadHandler.GetDatabasesLocally();
            }
        }

        private void OnValidate()
        {
            InitResources();
        }

        protected override void OnBeginDrawEditors()
        {
            if (MenuTree == null) return;

            MenuTree.DrawSearchToolbar();
            DrawBegin();
        }

        protected override void OnEndDrawEditors()
        {
            if (MenuTree == null)
            {
                return;
            }

            DrawEnd();
        }

        private void OnInspectorUpdate()
        {
            DrawUpdate();
        }

        protected override OdinMenuTree BuildMenuTree()
        {
            var tree = new OdinMenuTree();

            tree.Selection.SupportsMultiSelect = false;

            BuildImportExport(ref tree);
            BuildHeroes(ref tree);
            BuildCards(ref tree);
            BuildItems(ref tree);
            BuildDropTables(ref tree);
            BuildFonts(ref tree);

            tree.EnumerateTree().AddThumbnailIcons();

            for (var i = 0; i < tree.MenuItems.Count; i++)
            {
                AddRightClickEvent(tree.MenuItems[i]);
            }

            return tree;
        }

        private void AddRightClickEvent(OdinMenuItem item)
        {
            item.OnRightClick -= OnRightClickMenuItem;
            item.OnRightClick += OnRightClickMenuItem;

            for (var i = 0; i < item.ChildMenuItems.Count; i++)
            {
                AddRightClickEvent(item.ChildMenuItems[i]);
            }
        }

        private void OnRightClickMenuItem(OdinMenuItem item)
        {
            if (item.Name == "Abilities" || item.Name == "Given Abilities" || item.Name == "Conditional Abilities")
            {
                GenericMenu menu = new GenericMenu();

                menu.AddItem(new GUIContent("Add New Ability"), false, () =>
                {
                    var cardName = item.Parent.Name;
                    var card = _downloadHandler.cardDatabase.GetCardByName(cardName);

                    if (item.Name == "Abilities")
                    {
                        Db.AbilityDatabase.AddNewAbility(Db.AbilityDatabase.AbilitiesList,card.id);
                    }
                    else if (item.Name == "Given Abilities")
                    {
                        Db.AbilityDatabase.AddNewAbility(Db.AbilityDatabase.GiveAbilitiesList,card.id);
                    }
                    else
                    {
                        Db.AbilityDatabase.AddNewAbility(Db.AbilityDatabase.ConditionalAbilitiesList,card.id);
                    }
                });

                menu.ShowAsContext();

                return;
            }

            if (item.Name.StartsWith("["))
            {
                GenericMenu menu = new GenericMenu();

                menu.AddItem(new GUIContent("Delete"), false, () =>
                {
                    var cardName = item.Parent.Parent.Name;
                    var abilityListName = item.Parent.Name;
                    var abilityIndex = int.Parse(item.Name[1].ToString());
                    var card = _downloadHandler.cardDatabase.GetCardByName(cardName);

                    if (abilityListName == "Abilities")
                    {
                        Db.AbilityDatabase.DeleteAbility(Db.AbilityDatabase.AbilitiesList, card.id, abilityIndex);
                    }
                    else if (abilityListName == "Given Abilities")
                    {
                        Db.AbilityDatabase.DeleteAbility(Db.AbilityDatabase.GiveAbilitiesList, card.id, abilityIndex);
                    }
                    else
                    {
                        Db.AbilityDatabase.DeleteAbility(Db.AbilityDatabase.ConditionalAbilitiesList, card.id, abilityIndex);
                    }
                });

                menu.AddItem(new GUIContent("Move Up"), false, () =>
                {
                    var cardName = item.Parent.Parent.Name;
                    var abilityListName = item.Parent.Name;
                    var abilityIndex = int.Parse(item.Name[1].ToString());
                    var card = _downloadHandler.cardDatabase.GetCardByName(cardName);

                    if (abilityListName == "Abilities")
                    {
                        Db.AbilityDatabase.MoveAbilityUp(Db.AbilityDatabase.AbilitiesList, card.id, abilityIndex);
                    }
                    else if (abilityListName == "Given Abilities")
                    {
                        Db.AbilityDatabase.MoveAbilityUp(Db.AbilityDatabase.GiveAbilitiesList, card.id, abilityIndex);
                    }
                    else
                    {
                        Db.AbilityDatabase.MoveAbilityUp(Db.AbilityDatabase.ConditionalAbilitiesList, card.id, abilityIndex);
                    }
                });

                menu.AddItem(new GUIContent("Move Down"), false, () =>
                {
                    var cardName = item.Parent.Parent.Name;
                    var abilityListName = item.Parent.Name;
                    var abilityIndex = int.Parse(item.Name[1].ToString());
                    var card = _downloadHandler.cardDatabase.GetCardByName(cardName);

                    if (abilityListName == "Abilities")
                    {
                        Db.AbilityDatabase.MoveAbilityDown(Db.AbilityDatabase.AbilitiesList, card.id, abilityIndex);
                    }
                    else if (abilityListName == "Given Abilities")
                    {
                        Db.AbilityDatabase.MoveAbilityDown(Db.AbilityDatabase.GiveAbilitiesList, card.id, abilityIndex);
                    }
                    else
                    {
                        Db.AbilityDatabase.MoveAbilityDown(Db.AbilityDatabase.ConditionalAbilitiesList, card.id, abilityIndex);
                    }
                });

                menu.AddItem(new GUIContent("Copy"), false, () =>
                {
                    var cardName = item.Parent.Parent.Name;
                    var abilityListName = item.Parent.Name;
                    var abilityIndex = int.Parse(item.Name[1].ToString());
                    var card = _downloadHandler.cardDatabase.GetCardByName(cardName);

                    if (abilityListName == "Abilities")
                    {
                        GUIUtility.systemCopyBuffer =  Db.AbilityDatabase.CopyToJson(Db.AbilityDatabase.AbilitiesList, card.id, abilityIndex);
                    }
                    else if (abilityListName == "Given Abilities")
                    {
                        GUIUtility.systemCopyBuffer = Db.AbilityDatabase.CopyToJson(Db.AbilityDatabase.GiveAbilitiesList, card.id, abilityIndex);
                    }
                    else
                    {
                        GUIUtility.systemCopyBuffer = Db.AbilityDatabase.CopyToJson(Db.AbilityDatabase.ConditionalAbilitiesList, card.id, abilityIndex);
                    }
                });

                menu.AddItem(new GUIContent("Paste"), false, () =>
                {
                    var cardName = item.Parent.Parent.Name;
                    var abilityListName = item.Parent.Name;
                    var abilityIndex = int.Parse(item.Name[1].ToString());
                    var card = _downloadHandler.cardDatabase.GetCardByName(cardName);

                    if (abilityListName == "Abilities")
                    {
                        Db.AbilityDatabase.PasteFromJson(Db.AbilityDatabase.AbilitiesList, card.id, GUIUtility.systemCopyBuffer, abilityIndex);
                    }
                    else if (abilityListName == "Given Abilities")
                    {
                        Db.AbilityDatabase.PasteFromJson(Db.AbilityDatabase.GiveAbilitiesList, card.id, GUIUtility.systemCopyBuffer, abilityIndex);
                    }
                    else
                    {
                        Db.AbilityDatabase.PasteFromJson(Db.AbilityDatabase.ConditionalAbilitiesList, card.id,GUIUtility.systemCopyBuffer, abilityIndex);
                    }
                });

                menu.ShowAsContext();
            }

            var card = _downloadHandler.cardDatabase.GetCardByName(item.Name);

            if (card != null)
            {
                GenericMenu menu = new GenericMenu();

                menu.AddItem(new GUIContent("Delete"), false, () =>
                {
                    _downloadHandler.cardDatabase.DeleteCard(card.id);
                });

                menu.AddItem(new GUIContent("Open In Explorer"), false, () =>
                {
                    var desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    var folder = CardGenerationTool.ZoomedCardFolderName;
                    var cardName = card.name.Replace(" ", "-").ToLower();
                    var fullPath = $"{desktopPath}/{folder}/{cardName}.png";
                    Debug.Log(fullPath);
                    EditorUtility.RevealInFinder(fullPath);
                });

                menu.ShowAsContext();
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            CleanUp();
        }
    }
}
