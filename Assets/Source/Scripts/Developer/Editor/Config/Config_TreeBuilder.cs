using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Items;
using NUnit.Framework;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using TMPro;
using UnityEditor;
using UnityEngine;

namespace CrossBlitz.Source.Scripts.Developer.Config
{
    public partial class ConfigurationEditor
    {
        private void BuildImportExport(ref OdinMenuTree tree)
        {
            if (_downloadHandler == null) _downloadHandler = new DownloadServerDataHandler();

            if (tree != null)
            {
                tree.Add("Import & Export", _downloadHandler,
                    EditorGUIUtility.IconContent("Collab").image);
                tree.AddAllAssetsAtPath("Import & Export/PlayFab", "Assets/PlayFabEditorExtensions/Editor/Resources/",
                    typeof(ScriptableObject), true, true);
                tree.AddAllAssetsAtPath("Import & Export/PlayFab", "Assets/PlayFabSdk/Shared/Public/Resources/",
                    typeof(ScriptableObject), true, true);
            }
        }

        private void BuildHeroes(ref OdinMenuTree tree)
        {
            // if (tree == null) return;
            // if (_downloadHandler == null) return;
            // if (_storeSprites != null)
            // {
            //     tree.Add($"Heroes", null, _storeSprites["specials-light"]);
            // }
            //
            // if (_downloadHandler.heroDatabase != null)
            // {
            //     foreach (var heroData in _downloadHandler.heroDatabase.Heroes)
            //     {
            //         if (_heroEmblems == null)
            //         {
            //             _heroEmblems = new Dictionary<string, Sprite>();
            //         }
            //
            //         if (!_heroEmblems.ContainsKey(heroData.id))
            //         {
            //             var emblemPath = $"Assets/Source/Addressables/emblems/player-emblem@{heroData.id.ToLower()}.png";
            //             var emblem = AssetDatabase.LoadAssetAtPath<Sprite>(emblemPath);
            //             if (!emblem) Debug.LogError($"Could not load emblem {emblemPath}");
            //             _heroEmblems.Add(heroData.id, emblem);
            //         }
            //
            //
            //         tree.Add($"Heroes/{heroData.id}", heroData, _heroEmblems[heroData.id]);
            //         tree.Add($"Heroes/{heroData.name}/Level Tree", null, EditorIcons.List);
            //
            //         for (var i = 0; i < heroData.levelTree.levels.Count; i++)
            //         {
            //             tree.Add($"Heroes/{heroData.name}/Level Tree/Level {i+1}", heroData.levelTree.levels[i], EditorIcons.Tree);
            //         }
            //     }
            // }
        }

        private struct CardTreeProperty
        {
            public string cardDataId;
            public List<string> paths;
            public bool isUsingEditorIcon;
            public EditorIcon editorIcon;
            public Sprite spriteIcon;
        }

        private void BuildCards(ref OdinMenuTree tree)
        {
            if (tree == null) return;
            if (_downloadHandler == null) return;

            if (_downloadHandler.cardDatabase != null)
            {
                if (_downloadHandler.cardDatabase.SortingData == null)
                {
                    _downloadHandler.cardDatabase.SortingData = new CardSortingData();
                    _downloadHandler.cardDatabase.SortingData.sortBy = CardSortingData.SortBy.Faction;
                }

                if (_storeSprites != null)
                {
                    tree.Add($"Cards", _downloadHandler.cardDatabase.SortingData, _storeSprites["cards-light"]);
                }
                else
                {
                    tree.Add("Cards", _downloadHandler.cardDatabase.SortingData);
                }

                var treeProperties = new List<CardTreeProperty>();

                if (_downloadHandler.cardDatabase.SortingData.sortBy == CardSortingData.SortBy.ByChapterSeen)
                {
                    if (_downloadHandler.cardDatabase.SortingData.battleDatabase != null)
                    {
                        var cardsAdded = new List<string>();

                        for (var i = 0; i < _downloadHandler.cardDatabase.SortingData.battleDatabase.Battles.Count; i++)
                        {
                            var battle = _downloadHandler.cardDatabase.SortingData.battleDatabase.Battles[i];

                            for (var j = 0; j < battle.Deck.cards.Count; j++)
                            {
                                if (cardsAdded.Contains(battle.Deck.cards[j].id))
                                {
                                    continue;
                                }

                                cardsAdded.Add(battle.Deck.cards[j].id);

                                var cardData = _downloadHandler.cardDatabase.GetCard( battle.Deck.cards[j].id );
                                var abilities = cardData.GetAbilities();

                                Sprite classSprite = null;

                                if (_classSprites != null && _classSprites.ContainsKey(cardData.@class.ToString()))
                                {
                                    classSprite = _classSprites[cardData.@class.ToString()];
                                }

                                var abilityIsSet = !string.IsNullOrEmpty(cardData.description) && abilities?.Count > 0;
                                var cardPaths = new List<string>();
                                var battleNameSplit = battle.BattleName.Split('-');

                                if (battleNameSplit.Length > 0)
                                {
                                    var battleChapter =  battleNameSplit[0];
                                    cardPaths.Add($"Cards/{battleChapter}/{cardData.name}");

                                    treeProperties.Add(new CardTreeProperty
                                    {
                                        cardDataId = cardData.id,
                                        paths = cardPaths,
                                        isUsingEditorIcon = abilityIsSet || classSprite == null,
                                        editorIcon = abilityIsSet ? EditorIcons.Checkmark : EditorIcons.X,
                                        spriteIcon = classSprite
                                    });
                                }
                            }


                        }
                    }
                }
                else
                {
                    foreach (var cardData in _downloadHandler.cardDatabase.Cards)
                    {
                        Sprite classSprite = null;

                        if (_classSprites != null && _classSprites.ContainsKey(cardData.@class.ToString()))
                        {
                            classSprite = _classSprites[cardData.@class.ToString()];
                        }

                        var abilities = cardData.GetAbilities();
                        var abilityIsSet = !string.IsNullOrEmpty(cardData.description) && abilities?.Count > 0;
                        var cardPaths = new List<string>();

                        switch (_downloadHandler.cardDatabase.SortingData.sortBy)
                        {
                            case CardSortingData.SortBy.Faction:
                                if (cardData.type == CardType.Relic || cardData.type == CardType.Elder_Relic)
                                {
                                    cardPaths.Add($"Cards/{cardData.type}/{cardData.name}");
                                }
                                else
                                {
                                    cardPaths.Add($"Cards/{cardData.faction}/{cardData.name}");
                                }
                                break;
                            case CardSortingData.SortBy.Faction2:
                                cardPaths.Add($"Cards/{cardData.faction2}/{cardData.name}");
                                break;
                            case CardSortingData.SortBy.Priority:
                                cardPaths.Add($"Cards/{cardData.aiProperties.strategy.priority}/{cardData.name}");
                                break;
                            case CardSortingData.SortBy.Class:
                                cardPaths.Add($"Cards/{cardData.@class}/{cardData.name}");
                                break;
                            case CardSortingData.SortBy.Cost:
                                cardPaths.Add($"Cards/Cost: {cardData.cost}/{cardData.name}");
                                break;
                            case CardSortingData.SortBy.Power:
                                cardPaths.Add($"Cards/Power: {cardData.power}/{cardData.name}");
                                break;
                            case CardSortingData.SortBy.Health:
                                cardPaths.Add($"Cards/Health: {cardData.health}/{cardData.name}");
                                break;
                            case CardSortingData.SortBy.Set:
                                if (cardData.type == CardType.Relic || cardData.type == CardType.Elder_Relic)
                                {
                                    cardPaths.Add($"Cards/{cardData.type}/{cardData.name}");
                                }
                                else
                                {
                                    cardPaths.Add($"Cards/{cardData.set}/{cardData.faction}/{cardData.name}");
                                }
                                break;
                            case CardSortingData.SortBy.Rarity:
                                cardPaths.Add($"Cards/{cardData.rarity}/{cardData.faction}/{cardData.name}");
                                break;
                            case CardSortingData.SortBy.UsesCustomEffect:
                                if (cardData.usesCustomEffect || cardData.GetAbilities().Exists( a => a.usesCustomEffect))
                                {
                                    cardPaths.Add(
                                        $"Cards/{cardData.faction}/{cardData.name}");
                                }
                                break;
                            case CardSortingData.SortBy.ByCustomEffectPrefab:
                                if (cardData.usesCustomEffect || cardData.GetAbilities().Exists( a => a.usesCustomEffect))
                                {
                                    if (cardData.usesCustomEffect)
                                    {
                                        cardPaths.Add($"Cards/{cardData.effectName}/{cardData.name}");
                                    }
                                    else
                                    {
                                        var customAbilityEffect = cardData.GetAbilities().Find(a => a.usesCustomEffect);

                                        if (customAbilityEffect.usesBoardEffect)
                                        {
                                            cardPaths.Add($"Cards/{customAbilityEffect.boardEffectPrefab}/{cardData.name}");
                                        }
                                        else
                                        {
                                            cardPaths.Add($"Cards/{customAbilityEffect.customEffectComponent}/{cardData.name}");
                                        }
                                    }
                                }
                                break;
                            case CardSortingData.SortBy.AbilityKeyword:
                            {
                                if (abilities?.Count > 0)
                                {
                                    for (var i = 0; i < abilities?.Count; i++)
                                    {
                                        cardPaths.Add($"Cards/{abilities[i].keyword}/{cardData.name}");
                                    }
                                }
                                else
                                {
                                    cardPaths.Add($"Cards/NONE/{cardData.name}");
                                }

                                break;
                            }
                            case CardSortingData.SortBy.HasAnyConditions:
                            {
                                var modifyCondition = Condition.NONE;

                                if (abilities != null)
                                {
                                    foreach (var ability in abilities)
                                    {
                                        if (ability.abilityCondition != null &&
                                            ability.abilityCondition.condition != Condition.NONE)
                                        {
                                            modifyCondition = ability.abilityCondition.condition;
                                            cardPaths.Add($"Cards/{modifyCondition}/{cardData.name}");
                                        }

                                        if (ability.damageAmountCondition != null &&
                                            ability.damageAmountCondition.condition != Condition.NONE)
                                        {
                                            modifyCondition = ability.damageAmountCondition.condition;
                                            cardPaths.Add($"Cards/{modifyCondition}/{cardData.name}");
                                        }

                                        if (ability.armorCondition != null &&
                                            ability.armorCondition.condition != Condition.NONE)
                                        {
                                            modifyCondition = ability.armorCondition.condition;
                                            cardPaths.Add($"Cards/{modifyCondition}/{cardData.name}");
                                        }

                                        if (ability.drawCondition != null &&
                                            ability.drawCondition.condition != Condition.NONE)
                                        {
                                            modifyCondition = ability.drawCondition.condition;
                                            cardPaths.Add($"Cards/{modifyCondition}/{cardData.name}");
                                        }

                                        if (ability.hasConditionalAbility)
                                        {
                                            for (var i = 0; i < ability.conditions.Count; i++)
                                            {
                                                modifyCondition = ability.conditions[i].condition;
                                                cardPaths.Add($"Cards/{modifyCondition}/{cardData.name}");
                                            }
                                        }
                                    }
                                }
                                break;
                            }
                            case CardSortingData.SortBy.CardTypes:
                                cardPaths.Add($"Cards/{cardData.type}/{cardData.name}");
                                break;
                            case CardSortingData.SortBy.HasText:
                                if (!string.IsNullOrEmpty(_downloadHandler.cardDatabase.SortingData.sortText) &&
                                    cardData.description.Contains(_downloadHandler.cardDatabase.SortingData.sortText))
                                {
                                    cardPaths.Add($"Cards/{cardData.faction}/{cardData.name}");
                                }

                                break;
                            case CardSortingData.SortBy.AbilityTargetCondition:
                                if (abilities?.Count > 0)
                                {
                                    for (var i = 0; i < abilities?.Count; i++)
                                    {
                                        foreach (var filterConditionValue in Enum.GetValues(typeof(FilterCondition)))
                                        {
                                            var filterCondition = (FilterCondition)filterConditionValue;

                                            if (abilities[i].targetConditions.HasFlag(filterCondition))
                                            {
                                                cardPaths.Add($"Cards/{filterCondition}/{cardData.name}");
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    cardPaths.Add($"Cards/NONE/{cardData.name}");
                                }

                                break;
                            case CardSortingData.SortBy.AbilityTargetConditionGrouped:
                                if (abilities?.Count > 0)
                                {
                                    for (var i = 0; i < abilities?.Count; i++)
                                    {
                                        cardPaths.Add(
                                            $"Cards/{abilities[i].targetConditions}/{cardData.name}");
                                    }
                                }
                                else
                                {
                                    cardPaths.Add($"Cards/NONE/{cardData.name}");
                                }

                                break;
                            case CardSortingData.SortBy.AbilityTriggerCondition:
                                if (abilities?.Count > 0)
                                {
                                    for (var i = 0; i < abilities?.Count; i++)
                                    {
                                        foreach (var filterConditionValue in Enum.GetValues(typeof(FilterCondition)))
                                        {
                                            var filterCondition = (FilterCondition)filterConditionValue;

                                            if (abilities[i].triggerConditions.HasFlag(filterCondition))
                                            {
                                                cardPaths.Add($"Cards/{filterCondition}/{cardData.name}");
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    cardPaths.Add($"Cards/NONE/{cardData.name}");
                                }

                                break;
                            case CardSortingData.SortBy.AbilityTriggerConditionGrouped:
                                if (abilities?.Count > 0)
                                {
                                    for (var i = 0; i < abilities?.Count; i++)
                                    {
                                        cardPaths.Add(
                                            $"Cards/{abilities[i].triggerConditions}/{cardData.name}");
                                    }
                                }
                                else
                                {
                                    cardPaths.Add($"Cards/NONE/{cardData.name}");
                                }

                                break;
                            case CardSortingData.SortBy.AbilityTargetAndTriggerConditions:
                                if (abilities?.Count > 0)
                                {
                                    for (var i = 0; i < abilities?.Count; i++)
                                    {
                                        foreach (var filterConditionValue in Enum.GetValues(typeof(FilterCondition)))
                                        {
                                            var filterCondition = (FilterCondition)filterConditionValue;

                                            if (abilities[i].triggerConditions.HasFlag(filterCondition))
                                            {
                                                cardPaths.Add($"Cards/{filterCondition}/{cardData.name}");
                                            }

                                            if (abilities[i].targetConditions.HasFlag(filterCondition))
                                            {
                                                cardPaths.Add($"Cards/{filterCondition}/{cardData.name}");
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    cardPaths.Add($"Cards/NONE/{cardData.name}");
                                }

                                break;
                            case CardSortingData.SortBy.AbilityTargetAndTriggerConditionsGrouped:
                                if (abilities?.Count > 0)
                                {
                                    for (var i = 0; i < abilities?.Count; i++)
                                    {
                                        cardPaths.Add(
                                            $"Cards/{abilities[i].targetConditions}/{cardData.name}");
                                        cardPaths.Add(
                                            $"Cards/{abilities[i].triggerConditions}/{cardData.name}");
                                    }
                                }
                                else
                                {
                                    cardPaths.Add($"Cards/NONE/{cardData.name}");
                                }

                                break;
                            case CardSortingData.SortBy.AbilityTriggerType:
                                if (abilities?.Count > 0)
                                {
                                    for (var i = 0; i < abilities?.Count; i++)
                                    {
                                        cardPaths.Add($"Cards/{abilities[i].triggerType}/{cardData.name}");
                                    }
                                }
                                else
                                {
                                    cardPaths.Add($"Cards/NONE/{cardData.name}");
                                }

                                break;
                            case CardSortingData.SortBy.ByChapterSeen:
                            {
                                break;
                            }
                            default:
                                cardPaths.Add($"Cards/{cardData.faction}/{cardData.name}");
                                break;
                        }

                        if (cardPaths.Count > 0)
                        {
                            treeProperties.Add(new CardTreeProperty
                            {
                                cardDataId = cardData.id,
                                paths = cardPaths,
                                isUsingEditorIcon = abilityIsSet || classSprite == null,
                                editorIcon = abilityIsSet ? EditorIcons.Checkmark : EditorIcons.X,
                                spriteIcon = classSprite
                            });
                        }

                        // cards.Add(cardData);
                        // paths.Add(cardPaths);
                        // isUsingEditorIcon.Add( abilityIsSet || classSprite == null );
                        // editorIcons.Add(abilityIsSet ? EditorIcons.Checkmark : EditorIcons.X);
                        // spriteIcons.Add(classSprite);

                        // for (var i = 0; i < cardPaths.Count; i++)
                        // {
                        //     var cardPath = cardPaths[i];
                        //
                        //     if (abilityIsSet)
                        //     {
                        //         tree.Add(cardPath, cardData, EditorIcons.Checkmark);
                        //     }
                        //     else if (classSprite)
                        //     {
                        //         tree.Add(cardPath, cardData, classSprite);
                        //     }
                        //     else
                        //     {
                        //         tree.Add(cardPath, cardData, EditorIcons.X);
                        //     }
                        // }
                    }
                }

                treeProperties = treeProperties.OrderBy(t => t.paths[0]).ToList();

                for (var i = 0; i < treeProperties.Count; i++)
                {
                    var card = _downloadHandler.cardDatabase.GetCard(treeProperties[i].cardDataId);

                    for (var j = 0; j < treeProperties[i].paths.Count; j++)
                    {
                        // if (treeProperties[i].isUsingEditorIcon)
                        // {
                        //     tree.Add(treeProperties[i].paths[j], card, treeProperties[i].editorIcon);
                        // }
                        // else
                        // {
                        //
                        // }

                        tree.Add(treeProperties[i].paths[j], card, treeProperties[i].spriteIcon);

                        var abilities = card.GetAbilities();
                        var conditionalAbilities = card.GetConditionalAbilities();
                        var givenAbilities = card.GetGivenAbilities();

                        tree.Add(treeProperties[i].paths[j] + $"/Abilities/", null);

                        for (var k = 0; k < abilities.Count; k++)
                        {
                            var ability = abilities[k];
                            tree.Add(treeProperties[i].paths[j] + $"/Abilities/[{k}] {ability.keyword}", ability);
                        }

                        tree.Add(treeProperties[i].paths[j] + $"/Conditional Abilities/", null);

                        for (var k = 0; k < conditionalAbilities.Count; k++)
                        {
                            var ability = conditionalAbilities[k];
                            tree.Add(treeProperties[i].paths[j] + $"/Conditional Abilities/[{k}] {ability.keyword}", ability);
                        }

                        tree.Add(treeProperties[i].paths[j] + $"/Given Abilities/", null);

                        for (var k = 0; k < givenAbilities.Count; k++)
                        {
                            var ability = givenAbilities[k];
                            tree.Add(treeProperties[i].paths[j] + $"/Given Abilities/[{k}] {ability.keyword}", ability);
                        }
                    }
                }
            }
        }

        private void BuildItems(ref OdinMenuTree tree)
        {
            // if (tree == null) return;
            // if (_downloadHandler == null) return;
            //
            // if (_storeSprites != null)
            // {
            //     tree.Add($"Items", null, _storeSprites["resources-light"]);
            // }
            // else
            // {
            //     tree.Add("Items",null);
            // }
            //
            // if (_downloadHandler.itemDatabase != null)
            // {
            //     foreach (var item in _downloadHandler.itemDatabase.Items)
            //     {
            //         if (item.ItemClass != "Card")
            //         {
            //             tree.Add($"Items/{item.ItemClass}/{item.DisplayName}", item);
            //         }
            //     }
            // }
        }

        private void BuildDropTables(ref OdinMenuTree tree)
        {
            // if (tree == null) return;
            // if (_downloadHandler == null) return;
            // if (_downloadHandler.itemDatabase != null)
            // {
            //     foreach (var item in _downloadHandler.itemDatabase.DropTables)
            //     {
            //         tree.Add($"Drop Tables/{item.TableId}", item);
            //     }
            // }
        }

        private void BuildFonts(ref OdinMenuTree tree)
        {
            //tree.AddAllAssetsAtPath("Fonts/TMP Font Assets", "Assets/Source/Fonts", typeof(TMP_FontAsset), true, true);
            //tree.AddAllAssetsAtPath("Fonts/TMP Sprite Assets", "Assets/Source/Fonts", typeof(TMP_SpriteAsset), true, true);
        }
    }
}