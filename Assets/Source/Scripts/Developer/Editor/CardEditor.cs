using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Hero;
using UnityEditor;
using UnityEngine;

namespace Source.Scripts.Developer.Editor
{
    public partial class CardEditor : EditorWindow
    {
        public enum EditMode
        {
            CARD,
            DECK,
            HERO
        }

        private EditMode mode;

        public Dictionary<string, CardData> cards;
        public Dictionary<string, HeroData> heroes;
        public Dictionary<string, DeckData> decks;

        public string currentCard;
        public string currentHero;
        public string currentDeck;
        public bool changedDeck;

        private Vector2 labelScrollPosition;
        private Vector2 cardViewScrollPosition;
        private Vector2 deckViewScrollPosition;
        private Vector2 _heroViewScrollPosition;
        private string searchText;
        private bool loading;

        [MenuItem("Window/Cross Blitz/Card Editor")]
        private static void ShowWindow()
        {
            var window = GetWindow<CardEditor>();
            window.titleContent = new GUIContent("Card Editor");
            window.Show();
        }

        private void OnEnable()
        {
            loading = false;
        }

        private void OnGUI()
        {
            EditorGUI.BeginDisabledGroup(loading);
            DrawToolbar();

            EditorGUILayout.BeginHorizontal();

            const int width = 180;

            if (mode == EditMode.CARD)
            {
                DrawCardListSideBar(width);
            }
            else if (mode == EditMode.HERO)
            {
                DrawHeroListSideBar(width);
            }
            else if (mode == EditMode.DECK)
            {
                DrawDeckListSideBar(width);
            }

            DrawUiLineVertical("212121FF".ToColor(),width:width);

            EditorGUILayout.BeginVertical();

            if (mode == EditMode.CARD)
            {
                cardViewScrollPosition = EditorGUILayout.BeginScrollView(cardViewScrollPosition);

                if (cards != null && !string.IsNullOrEmpty(currentCard))
                {

                    cards[currentCard] = DrawCard(cards[currentCard]);
                }
                else
                {
                    DrawCard(null);
                }
            }
            else if (mode == EditMode.HERO)
            {
                _heroViewScrollPosition = EditorGUILayout.BeginScrollView(_heroViewScrollPosition);

                if (heroes != null && !string.IsNullOrEmpty(currentHero))
                {
                    heroes[currentHero] = DrawHero(heroes[currentHero],_changedHero);
                    _changedHero = false;
                }
                else
                {
                    DrawHero(null, false);
                }
            }
            else if (mode == EditMode.DECK)
            {
                deckViewScrollPosition = EditorGUILayout.BeginScrollView(deckViewScrollPosition);

                if (decks != null && !string.IsNullOrEmpty(currentDeck))
                {
                    decks[currentDeck] = DrawDeck( decks[currentDeck] , changedDeck);
                    DrawDeckStats(decks[currentDeck]);
                    changedDeck = false;
                }
            }

            EditorGUILayout.EndScrollView();

            GUILayout.FlexibleSpace();

            EditorGUILayout.BeginHorizontal();

            GUILayout.FlexibleSpace();

            EditorGUI.BeginDisabledGroup(cards == null || string.IsNullOrEmpty(currentCard));

            if (GUILayout.Button(EditorGUIUtility.IconContent("TreeEditor.Trash"), GUILayout.Width(32)))
            {
                if (mode == EditMode.CARD)
                {
                    DeleteCard(cards?[currentCard]);
                }
                else if (mode == EditMode.HERO)
                {
                    DeleteHero(heroes?[currentHero]);
                }
                else if (mode == EditMode.DECK)
                {
                    DeleteDeck(decks?[currentDeck]);
                }
            }

            if (GUILayout.Button(EditorGUIUtility.IconContent("d_Asset Store"), GUILayout.Width(32)))
            {
                if (mode == EditMode.CARD)
                {
                    PF_UpdateCardsInCatalog();
                }
                else if (mode == EditMode.HERO)
                {
                    PF_UpdateHeroesInCatalog();
                }
                else if (mode == EditMode.DECK)
                {
                    Debug.LogWarning("Decks don't use store items!");
                }
            }

            if (GUILayout.Button(EditorGUIUtility.IconContent("SaveActive"), GUILayout.Width(32)))
            {
                if (mode == EditMode.CARD)
                {
                    SaveCardDatabase();
                }
                else if (mode == EditMode.HERO)
                {
                    SaveHeroDatabase();
                }
                else if (mode == EditMode.DECK)
                {
                    SaveDeckDatabase();
                }
            }

            EditorGUI.EndDisabledGroup();

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();

            EditorGUILayout.EndHorizontal();

            EditorGUI.EndDisabledGroup();
        }

        private void OnCreateNewCard(object obj)
        {
            loading = true;

            CardCreationWindow.ShowWindow();
            CardCreationWindow.cardType = (CardType) obj;
            CardCreationWindow.Created += CreationSuccess;
            CardCreationWindow.Canceled += CreationCanceled;
        }

        private void CreateNewHero(object obj)
        {
            loading = true;

            CardCreationWindow.ShowWindow();
            CardCreationWindow.cardType = CardType.Hero;
            CardCreationWindow.faction = (Faction)obj;
            CardCreationWindow.Created += CreationSuccess;
            CardCreationWindow.Canceled += CreationCanceled;
        }

        public CardData GetCard(string id)
        {
            if (cards != null && cards.ContainsKey(id))
            {
                return cards[id];
            }

            return null;
        }

        private void CreationSuccess(string cardId, string cardName, CardType cardType)
        {
            loading = false;

            if (cardType == CardType.Hero)
            {
                var hero = new HeroData
                {
                    id = cardId,
                    name = cardName,
                    faction = CardCreationWindow.faction
                };

                hero.ConvertData();

                if (heroes.ContainsKey(hero.id))
                {
                    heroes[hero.id] = hero;
                }
                else
                {
                    heroes.Add(hero.id, hero);
                }
            }
            else
            {
                var data = new CardData
                {
                    id = cardId,
                    name = cardName,
                    type = cardType
                };

                //data.ConvertData();

                if (cards.ContainsKey(data.id))
                {
                    cards[data.id] = data;
                }
                else
                {
                    cards.Add(data.id, data);
                }
            }


            Focus();
        }

        private void CreationCanceled()
        {
            loading = false;

            Focus();
        }
    }
}
