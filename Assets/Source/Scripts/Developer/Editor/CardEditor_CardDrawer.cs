using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using UnityEditor;
using UnityEngine;

namespace Source.Scripts.Developer.Editor
{
    public partial class CardEditor
    {
        public bool basicInfoFoldout;
        public bool abilityFoldout;
        public bool previewFoldout;

        private int _titleFontSize = 24;

        private string DrawCardPopup(string label, string selected)
        {
            if (cards == null || cards.Count <= 0)
                return string.Empty;

            var cardIds = cards.Keys.ToList();
            var index = EditorGUILayout.Popup(label, cardIds.IndexOf(selected) , cardIds.ToArray() );

            if (index >= 0)
            {
                selected = cardIds[index];
            }

            return selected;
        }

        private string DrawCardPopup(Rect rect,string label, string selected)
        {
            if (cards == null || cards.Count <= 0)
                return string.Empty;

            var cardIds = cards.Keys.ToList();
            var index = string.IsNullOrEmpty(label) ?
                EditorGUI.Popup(rect,
                    cardIds.IndexOf(selected) ,
                    cardIds.ToArray() ) :
                EditorGUI.Popup(rect,label,
                    cardIds.IndexOf(selected) ,
                    cardIds.ToArray() );

            if (index >= 0)
            {
                selected = cardIds[index];
            }

            return selected;
        }
        private CardData DrawCard(CardData cardData)
        {
            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            var cName = cardData == null ? "Unknown" : cardData.name;

            EditorGUILayout.LabelField(cName, GetTitleLabelStyle(), GUILayout.Height(_titleFontSize * 1.5f));

            if (GUILayout.Button("Edit"))
            {
                Application.OpenURL("https://docs.google.com/spreadsheets/d/1xORKqT01LX95BymA3XOD0je3Q6ielfOvhvZ4JhjTmEA/edit?usp=sharing");
            }
            EditorGUILayout.EndHorizontal();
            // DrawHorizontalSpacer(0);
           DrawUiLine(new Color ( 0.5f,0.5f,0.5f, 1 ));

            basicInfoFoldout =
                EditorGUILayout.Foldout(basicInfoFoldout, "Basic Information", GetFoldoutStyle());

            if (basicInfoFoldout)
            {
                EditorGUI.indentLevel++;

                if (cardData != null)
                {
                    EditorGUI.BeginDisabledGroup(true);
                    EditorGUILayout.TextField("ID", cardData.id);

                    cardData.name = EditorGUILayout.TextField("Name", cardData.name);
                    EditorGUILayout.BeginHorizontal();
                    cardData.portraitUrl = EditorGUILayout.TextField("Portrait Url", cardData.portraitUrl);
                    if (GUILayout.Button("Fix",GUILayout.Width(64)))
                    {
                        cardData.portraitUrl = cardData.name.ToLower().Replace(" ", "-");
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.LabelField("Description", EditorStyles.largeLabel);
                    cardData.description = EditorGUILayout.TextArea(cardData.description, EditorStyles.textArea,
                        GUILayout.Height(48));
                    cardData.rarity = (Rarity)EditorGUILayout.EnumPopup("Rarity", cardData.rarity);
                    cardData.type = (CardType)EditorGUILayout.EnumPopup("Card Type", cardData.type);
                    cardData.faction = (Faction) EditorGUILayout.EnumPopup("Faction", cardData.faction);

                    if (cardData.type == CardType.Minion)
                    {
                        cardData.@class = (Class)EditorGUILayout.EnumPopup("Class", cardData.@class);
                        cardData.attackType = (AttackType)EditorGUILayout.EnumPopup("Attack Type", cardData.attackType);
                    }

                    cardData.cost = EditorGUILayout.IntField("Cost", cardData.cost);

                    if (cardData.type == CardType.Minion)
                    {
                        cardData.power = EditorGUILayout.IntField("Power", cardData.power);
                        cardData.health = EditorGUILayout.IntField("Health", cardData.health);
                    }

                    EditorGUILayout.LabelField("Portrait Offset", EditorStyles.largeLabel);
                    EditorGUILayout.BeginHorizontal(GUILayout.Width(240));
                    cardData.standardOffset = EditorGUILayout.Vector2IntField(GUIContent.none, cardData.standardOffset);
                    cardData.zoomedOffset = EditorGUILayout.Vector2IntField(GUIContent.none, cardData.zoomedOffset);
                    cardData.boardOffset = EditorGUILayout.Vector2IntField(GUIContent.none, cardData.boardOffset);
                    EditorGUILayout.EndHorizontal();
                    EditorGUI.EndDisabledGroup();
                }
                EditorGUI.indentLevel--;
            }

            DrawUiLine(new Color ( 0.5f,0.5f,0.5f, 1 ));

            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);

            abilityFoldout =
                EditorGUILayout.Foldout(abilityFoldout, "Abilities");

            if (GUILayout.Button(EditorGUIUtility.IconContent("d_P4_AddedRemote"),EditorStyles.toolbarButton, GUILayout.Width(32)))
            {
                // if (cardData != null && abilities == null)
                // {
                //     cardData.abilities = new List<Ability>();
                // }
                //
                // cardData?.abilities?.Add(new Ability());
            }

            EditorGUILayout.EndHorizontal();
;
            if (abilityFoldout)
            {
                // if (cardData != null && cardData.abilities != null && cardData.abilities?.Count > 0)
                // {
                //     for (int i = 0; i < cardData.abilities.Count; i++)
                //     {
                //       cardData.abilities[i]=  DrawAbility(cardData.abilities[i]);
                //
                //       if (cardData.abilities[i] == null)
                //       {
                //           cardData.abilities.RemoveAt(i);
                //       }
                //     }
                // }
            }

            return cardData;
        }

        public GUIStyle GetTitleLabelStyle()
        {
            return  new GUIStyle(EditorStyles.largeLabel) { font = EditorStyles.boldFont, fontSize = _titleFontSize };
        }

        public GUIStyle GetFoldoutStyle()
        {
            return new GUIStyle(EditorStyles.foldout) { font = EditorStyles.boldFont, fontSize =  12};
        }
    }
}
