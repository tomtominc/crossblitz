﻿using System.Collections;
using System.Collections.Generic;
using CrossBlitz.Card;
using UnityEngine;
using UnityEngine.UI;

public class AbilityEditor : MonoBehaviour
{
    public Button addButton;
    public AbilityItem itemPrefab;
    public List<AbilityItem> items;

    private void Start()
    {
        items = new List<AbilityItem>();
        addButton.onClick.AddListener(OnAddButton);
    }

    public void OnAddButton()
    {
        AbilityItem item = Instantiate(itemPrefab, transform, false);
        item.RemoveItem += OnRemoveItem;

        items.Add(item);
    }

    private void OnRemoveItem(AbilityItem item)
    {
        items.Remove(item);
        Destroy(item.gameObject);
    }

    public void SetAbilities(List<Ability> abilities)
    {
        ResetEditor();
        StartCoroutine(SetAbilitiesDelayed(abilities));
    }

    public IEnumerator SetAbilitiesDelayed(List<Ability> abilities)
    {
        for (int i = 0; i < abilities.Count; i++)
        {
            AbilityItem item = Instantiate(itemPrefab, transform, false);
            item.RemoveItem += OnRemoveItem;
            items.Add(item);

            yield return new WaitForEndOfFrame();

            item.SetAbility(abilities[i]);
        }
    }

    public void ResetEditor()
    {
        for (int i = items.Count - 1; i > -1; i--)
        {
            Destroy(items[i].gameObject);
        }

        items = new List<AbilityItem>();
    }

    public List<Ability> GetAbilities()
    {
        List<Ability> abilities = new List<Ability>();

        for (int i = 0; i < items.Count; i++)
        {
            abilities.Add(items[i].GetAbility());
        }

        return abilities;
    }
}
