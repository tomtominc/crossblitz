using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.Fables;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Grid;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using CrossBlitz.Settings;
using CrossBlitz.Utils;
using QFSW.QC;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.ResourceLocations;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;
using Random = UnityEngine.Random;

namespace CrossBlitz.Developer
{
    public static class TerminalCommands
    {
        [Command("show-resolved-commands")]
        public static void ShowResolvedCommands()
        {
            if (!GameServer.IsRunning()) return;

            for (var i = 0; i < Server.CommandResolver.resolvedCommands.Count; i++)
            {
                Debug.Log($"{i+1}: {Server.CommandResolver.resolvedCommands[i]}");
            }
        }

        [Command("modify-dawn-dollars")]
        public static void ModifyDawnDollars(int amount)
        {
            var currentBook = App.FableData.GetCurrentBook();

            if (currentBook != null)
            {
                currentBook.AddDawnDollars(amount);
            }
        }

        [Command("test-column-utils")]
        public static void TestColumnUtils()
        {
            var rows = string.Empty;
            for (var i = 0; i < 99; i++)
            {
                var rand = i;// Random.Range(0, 100);
                rows += $"{rand}, {ExcelUtils.GetExcelColumnName(rand)}\n";
            }
            Debug.Log(rows);
        }

        [Command("clear-seen-tutorials")]
        public static void ClearSeenTutorials()
        {
            App.TutorialData.ClearSeenTutorials();
        }

        [Command("grant-all-deck-recipes-no-cards")]
        public static void UnlockAllDeckRecipesNoCards()
        {
            var items = new List<string>();

            for (var i = 0; i < Db.DeckRecipeDatabase.deckRecipes.Count; i++)
            {
                var deck = Db.DeckRecipeDatabase.deckRecipes[i];

                if (deck == null)
                {
                    continue;
                }
                items.Add( deck.Uid );
            }

            try
            {
                ClientInventory.GrantAndDisplayItems(items, false);
            }
            catch (Exception e)
            {
                Debug.LogError($"Could not grant item. {e}");
            }

            App.SaveAll();
        }

        [Command("grant-all-deck-recipes")]
        public static void UnlockAllDeckRecipes()
        {
            var items = new List<string>();

            for (var i = 0; i < Db.DeckRecipeDatabase.deckRecipes.Count; i++)
            {
                var deck = Db.DeckRecipeDatabase.deckRecipes[i];

                if (deck == null)
                {
                    continue;
                }

                for (var j = 0; j < deck.deckData.cards.Count; j++)
                {
                    var cardValue = deck.deckData.cards[j];

                    for (var k = 0; k < cardValue.count; k++)
                    {
                        items.Add(cardValue.id);
                    }
                }

                items.Add( deck.Uid );
            }

            try
            {
                ClientInventory.GrantAndDisplayItems(items, false);
            }
            catch (Exception e)
            {
                Debug.LogError($"Could not grant item. {e}");
            }

        }

        [Command("grant-items")]
        public static void GrantItems(List<string> items)
        {
            var addedItems = new List<string>();

            // set item ids if item names were entered
            for (var i = 0; i < items.Count; i++)
            {
                var item = Db.ItemDatabase.GetItem(items[i]);

                if (item == null)
                {
                    item = Db.ItemDatabase.GetItemByName(items[i]);

                    if (item != null)
                    {
                        items[i] = item.ItemId;
                        Debug.Log($"Replaced with {item.ItemId}");
                    }
                }

                if (item == null)
                {
                    continue;
                }

                var deck = Db.DeckRecipeDatabase.GetRecipeByName($"{item.DisplayName} Starter");

                if (deck == null)
                {
                    deck = Db.DeckRecipeDatabase.GetRecipeByName($"{item.DisplayName} Starter Deck");
                }

                if (deck == null)
                {
                    continue;
                }

                for (var j = 0; j < deck.deckData.cards.Count; j++)
                {
                    var cardValue = deck.deckData.cards[j];

                    for (var k = 0; k < cardValue.count; k++)
                    {
                        addedItems.Add(cardValue.id);
                    }
                }

                addedItems.Add( deck.Uid );

                switch (item.DisplayName)
                {
                    case "Redcroft":
                        App.PlayerDecks.AddDeck(deck, DeckEquipContext.Redcroft_Fable);
                        break;
                    case "Violet":
                        App.PlayerDecks.AddDeck(deck, DeckEquipContext.Violet_Fable);
                        break;
                    case "Seto":
                        App.PlayerDecks.AddDeck(deck, DeckEquipContext.Seto_Fable);
                        break;
                    case "Quill":
                        App.PlayerDecks.AddDeck(deck, DeckEquipContext.Quill_Fable);
                        break;
                    case "Mereena":
                        App.PlayerDecks.AddDeck(deck, DeckEquipContext.Mereena_Fable);
                        break;
                }
            }

            items.AddRange(addedItems);

            try
            {
                ClientInventory.GrantAndDisplayItems(items, false);
            }
            catch (Exception e)
            {
                Debug.LogError($"Could not grant item. {e}");
            }
        }

        [Command("check-blitz-burst")]
        public static void CheckBlitzOffset(string id)
        {
            var card = Db.CharacterDatabase.GetCharacter(id);
            Debug.Log($"{card.heroPowerCinematicOffset}");
        }

        [Command("grant-all-cards")]
        public static void GrantAllCards()
        {
            var cardItems = new List<ItemValue>();

            for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
            {
                var card = Db.CardDatabase.Cards[i];
                var value = 0;

                switch (card.rarity)
                {
                    case Rarity.Common: value = 8;
                        break;
                    case Rarity.Rare: value = 4;
                        break;
                    case Rarity.Mythic: value = 2;
                        break;
                    case Rarity.Legendary: value = 1;
                        break;
                }

                cardItems.Add(new ItemValue { ItemUid = card.id, Count = value});

            }

            ClientInventory.GrantAndDisplayItems(cardItems, null);
        }

        [Command("check-board-damage")]
        public static void CheckBoardDamage(Team team, bool potentialDamage)
        {
            var damage = AI.GetTotalBoardDamage(team, GameServer.State, potentialDamage);
            Debug.Log($"Current Total damage for {team} = {damage}");
        }

        [Command("open-scene")]
        public static async void OpenScene(string sceneName)
        {
            await SceneController.Instance.UnloadAllScenes();
            await SceneController.Instance.LoadScene(new SceneLoadParams
            {
                SceneToLoad = sceneName,
                LoadSceneMode = LoadSceneMode.Additive
            });
        }

        [Command("change-character")]
        public static void ChangeCharacter(string heroName)
        {
            var heroData = Db.HeroDatabase.GetHero(heroName);
            Events.Publish(null, EventType.OnHeroChanged, heroData);
        }


       [Command("simple-permutations-test")]
        public static void SimplePermutationsTest()
        {
            var combos = GameUtilities.GetAllPermutations(new List<int> {1, 2, 3});
            var comboList = string.Empty;
            for (var i = 0; i < combos.Count; i++)
            {
                comboList += "{";

                for (var j = 0; j < combos[i].Count; j++)
                {
                    comboList += $"{combos[i][j]}";

                    if (j < combos[i].Count - 1)
                    {
                        comboList += ",";
                    }
                }

                comboList += "},";
            }
            Debug.Log(comboList);
        }

        [Command("simple-permutations-test-all-orders-2")]
        public static void SimplePermutationsInAllOrdersTest2()
        {
            var combos = GameUtilities.GetAllPermutationsInAllOrders(new List<int> {1, 2, 3} );
            var comboLog = string.Empty;

            for (var i = 0; i < combos.Count; i++)
            {
                var comboList = combos[i];
                comboLog += "{";

                for (var j = 0; j < comboList.Count; j++)
                {
                    comboLog += $"{comboList[j]}" + (j < comboList.Count-1 ? "," : string.Empty);
                }

                comboLog += "},";
            }

            Debug.Log(comboLog);
        }

        [Command("reset-all-saved-data")]
        public static void ResetAllSavedData()
        {
            SettingsManager.ResetAllSavedData();
        }

        [Command("level-hero-from-battle")]
        public static void LevelHeroFromBattle(string heroId, BattleType battleType, int enemyLevel, bool firstTime)
        {
            var hero = App.HeroData.GetHero(heroId);

            if (hero != null)
            {
                var levelBefore = hero.level;
                var xpBefore = hero.xp;
                var gainedXp = Xp.GetXpGainedFromEnemy(battleType, enemyLevel, firstTime);

                App.HeroData.IncreaseXp(hero.id, gainedXp);
                var levelsGained = Xp.GetGainedLevelsFromXp(hero.level, hero.xp, out var xpLeft);
                App.HeroData.SetXp(hero.id, xpLeft);

                HeroDatabase.ModifyHeroLevel(hero.id, hero.level + levelsGained);

                Debug.LogError(
                    $"{hero.id}: Levels: {levelBefore}->{hero.level} (+{levelsGained}) Xp; {xpBefore}->{hero.xp} (+{gainedXp})");
            }
        }

        [Command("show-inventory")]
        public static void ShowAllInventory()
        {
            if (!App.Initialized)
            {
                App.Init();
            }

            if (!Db.Initialized)
            {
                Db.Initialize();
            }

            foreach (var item in App.Inventory.GetItems())
            {
                Debug.Log($"{item.Key} x{item.Value.RemainingUses}");
            }

            foreach (var currency in App.Inventory.GetCurrency())
            {
                Debug.Log($"{currency.Key}: {currency.Value}");
            }
        }

        [Command("simple-permutations-test-all-orders")]
        public static void SimplePermutationsInAllOrdersTest()
        {
            var combos = GameUtilities.GetAllPermutations(new List<int> {1, 2, 3});
            var comboLog = string.Empty;

            for (var i = 0; i < combos.Count; i++)
            {
                var comboList = combos[i];

                if (comboList.Count <= 1) // does not need to get anymore permutations
                {
                    comboLog += "{";

                    for (var j = 0; j < comboList.Count; j++)
                    {
                        comboLog += $"{comboList[j]}" + (j < comboList.Count-1 ? "," : string.Empty);
                    }

                    comboLog += "},";
                }
                else // needs to get even more permutations
                {
                    var combosPerm = GameUtilities.GetPermutationsAtFixedSize(comboList).ToList();
                    for (var j = 0; j < combosPerm.Count; j++)
                    {
                        var comboPermList = combosPerm[j].ToList();

                        comboLog += "{";

                        for (var k = 0; k < comboPermList.Count; k++)
                        {
                            comboLog += $"{comboPermList[k]}" + (k < comboPermList.Count-1 ? "," : string.Empty);;
                        }

                        comboLog += "},";
                    }
                }
            }

            Debug.Log(comboLog);
        }

        [Command("get-hand-damage-doubloon-cheat")]
        public static void GetHandDamageDoubloonCheat(Team team)
        {
            // Stopwatch stopWatch = new Stopwatch();
            // stopWatch.Start();
            //
            // var decision = AI.GetHandDecisionDoubloonCheat(team, new List<string>());
            //
            // stopWatch.Stop();
            // var milliseconds = stopWatch.ElapsedMilliseconds;
            //
            // if (decision == null)
            // {
            //     Debug.LogError("Decision is null!");
            //     return;
            // }
            //
            // var log = $"Decision ({decision.score}) = [";
            //
            // for (var i = 0; i < decision.commands.Count; i++)
            // {
            //     log += $"{decision.commands[i].Type},";
            // }
            //
            // log += $"] Score: {decision.score} {milliseconds} ms";
            //
            // Debug.Log(log);
        }

        [Command("fire-event")]
        public static async void FireEvent(int eventIndex)
        {
            Events.Publish(null, (EventType)eventIndex, null);
        }

        [Command("load-match-finished")]
        public static async void LoadMatchFinishedResults()
        {
            await SceneController.Instance.LoadScene(new SceneLoadParams {SceneToLoad = "MatchFinished"});
        }

        [Command("finish-match")]
        public static void FinishMatch(string outcome)
        {
            if (outcome == "win")
            {
                Events.Publish(null, EventType.OnMatchWon, null);
            }
            else if (outcome == "lose")
            {
                Events.Publish(null, EventType.OnMatchLost, null);
            }
            else if (outcome == "draw")
            {
                Events.Publish(null, EventType.OnMatchDraw, null);
            }
        }

        [Command("draw-cards")]
        public static void DrawCards(int count, Team team)
        {
            GameLogic.StartCommand(new DrawCardCommand
            {
                PlayerUid = GameServer.GetPlayerState(team).Uid,
                CardCountBeforeDraw = GameServer.GetPlayerState(team).GetHandCount(),
                DrawCount = count
            });
        }

        [Command("mana-meld")]
        public static async void LoadManaMeld()
        {
            var sceneModel = await SceneController.Instance.LoadScene(new SceneLoadParams
            {
                SceneToLoad = "ManaMeld"
            });

            var collectionManager = sceneModel.GetView<ManaMeldEditMenu>("CollectionManager");

            collectionManager.OnCollectionClosed += OnCollectionManagerClosed;
            collectionManager.OpenCollectionManagerWithSettings(new DeckEditSettings
            {
                Mode = DeckEditMode.Edit,
                OpenMode = DeckOpenMode.Collection,
                MenuMode = CollectionMenuMode.ManaMeld,
                DeckSettings = new DeckSettings
                {
                    deckData = App.PlayerDecks.GetDeckBasedOnGameState()
                },
                EquipContext = App.PlayerDecks.GetContextBasedOnGameState(),
                FilterCardSettings = new FilterCardSettings
                {
                    ownedCardsOnly = false,
                    //forcedFaction = chapterData.ChapterFaction,
                    faction = Faction.All,
                    @class = Class.All,
                    rarity = Rarity.All
                },
            });
        }

        private static async void OnCollectionManagerClosed()
        {
            await System.Threading.Tasks.Task.Delay(400);
            SceneManager.UnloadSceneAsync("ManaMeld");
        }


        [Command("modify-mana")]
        public static void ModifyMana(Card.Team team, int amount)
        {
            if (GameServer.State != null)
            {
                var player = GameServer.GetPlayerState(team);
                if (player != null) player.currentMana = amount;
            }


            GameServer.SetMana(team, amount, false, false);
        }

        [Command("show-all-key-binding-codes")]
        public static void ShowAllKeyBindings()
        {
            var keys = Enum.GetNames(typeof(KeyCode));
            var keyDisplay = string.Empty;
            for (var i = 0; i < keys.Length; i++)
            {
                if (keys[i] == "None") continue;
                keyDisplay += $"{keys[i]},";
            }
            Debug.Log(keyDisplay);
        }

        [Command("show-all-key-bound-commands")]
        public static void ShowAllKeyBoundCommands()
        {
            var keys = Enum.GetNames(typeof(KeyCode));
            for (var i = 0; i < keys.Length; i++)
            {
                var key = $"bound-command-from-{keys[i]}";
                if (PlayerPrefs.HasKey(key))
                {
                    var command = PlayerPrefs.GetString(key);
                    Debug.Log($"{keys[i]} = '{command}'");
                }
            }
        }

        [Command("remove-all-bound-commands")]
        public static void RemoveAllBoundCommands()
        {
            var keys = Enum.GetNames(typeof(KeyCode));

            for (var i = 0; i < keys.Length; i++)
            {
                var key = $"bound-command-from-{keys[i]}";

                if (PlayerPrefs.HasKey(key))
                {
                    PlayerPrefs.DeleteKey(key);
                }
            }

            PlayerPrefs.Save();

            Debug.Log("All bound commands have been removed.");
        }

        [Command("reset-chest-tiles-on-current-map")]
        public static void ResetChestTilesOnCurrentMap()
        {
            if (!FableController.PlayerIsInAFable())
            {
                Debug.LogError("Player is not in a fable.");
                return;
            }

            if (!FablesMapController.Instance)
            {
                Debug.LogError("Fable Map Controller is null.");
                return;
            }

            var roomId = App.FableData.GetCurrentChapter().CurrentRoomUid;

            if (string.IsNullOrEmpty(roomId))
            {
                Debug.LogError("No current room id.");
                return;
            }


            var room = App.FableData.GetCurrentChapter().Rooms[roomId];

            foreach (var tileState in room.TileData)
            {
                var tile = Db.MapDatabase.GetTile(tileState.Key);

                if (tile != null && tile.NodeType == NodeType.Chest)
                {
                    tileState.Value.Complete = false;
                    tileState.Value.PendingComplete = false;
                    tileState.Value.Reset();
                }
            }

            for (var i = 0; i < HexGrid2D.Map.Count; i++)
            {
                var tile = HexGrid2D.Map[i];
                tile.RefreshHexData();
            }
        }

        [Command("bind-command")]
        public static void BindCommand(KeyCode key, string command)
        {
            if (key == KeyCode.None) return;

            PlayerPrefs.SetString($"bound-command-from-{key}", command);
            PlayerPrefs.Save();

            Debug.Log($"Command '{command}' was bound to the {key} key!");
        }

        [Command("reset-heroes")]
        public static void ResetHeroes()
        {
            App.HeroData.ResetHeroes();
        }

        [Command("restart-battle")]
        public static void RestartBattle()
        {
            if (GameServer.Mode == null)
            {
                Debug.LogError("You're not currently in a game!");
                return;
            }

            Events.Publish(null, EventType.StartBattle, new StartBattleEventArgs
            {
                gameMode = new GameMode
                {
                    GameType = GameServer.Mode.GameType,
                    ConnectionMode = GameServer.Mode.ConnectionMode,
                    SceneEnvironment = GameServer.Mode.SceneEnvironment,
                    SceneToReturnTo = GameServer.Mode.SceneToReturnTo,
                    ShowResultsAfterMatch = GameServer.Mode.ShowResultsAfterMatch,
                    BattleUid = GameServer.Mode.BattleUid,
                    DeckUid = App.PlayerDecks.GetDeckBasedOnGameState().uid,
                }
            });
        }




        [Command("open-tools")]
        public static async void OpenTools()
        {
            await SceneController.Instance.UnloadAllScenes();
            DeveloperTools.OpenToolMode.ChapterEditor = true;
            StateController.NextState = StateDefinition.TOOLS_EDITOR;
            StateController.ChangeState(StateDefinition.LOGIN);
        }

        [Command("show-current-room-saved-data")]
        public static void ShowCurrentRoomSavedData()
        {
            if (FablesMapController.Instance.ClientChapterData != null)
            {
                var currentMapUid = FablesMapController.Instance.ClientChapterData.CurrentMapUid;
                var currentRoomIndex = FablesMapController.Instance.ClientChapterData.CurrentRoomIndex;
                var map = Db.MapDatabase.GetMap(currentMapUid);
                for (var i = 0; i < map.Rooms[currentRoomIndex].Tiles.Count; i++)
                {
                    var tile = map.Rooms[currentRoomIndex].Tiles[i];

                    if (tile.IsValidNodeType())
                    {
                        var clientTile = FablesMapController.Instance.ClientChapterData.GetTile(tile.uid);
                        Debug.Log($"Tile: {tile.displayName} is complete? {clientTile.Complete}");
                    }
                    else if (tile.NodeType == NodeType.Path)
                    {
                        var clientTile = FablesMapController.Instance.ClientChapterData.GetTile(tile.uid);
                        Debug.Log($"Path is complete? {clientTile.Complete}");
                    }

                }
            }
        }

        [Command("show-all-saved-rooms")]
        public static void ShowAllSavedRooms()
        {
            foreach (var chapter in App.FableData.Chapters)
            {
                foreach (var room in chapter.Value.Rooms)
                {
                    Debug.LogError($"Room = {room.Value.RoomUid}");
                }
            }
        }

        [Command("test-fable-save")]
        public static void TestFableSave()
        {
            var stopWatch = new System.Diagnostics.Stopwatch();
            stopWatch.Start();

            App.FableData.Save();
            App.SaveAll();

            stopWatch.Stop();
            Debug.Log($"Save Time: {stopWatch.Elapsed.Milliseconds}");
        }

        [Command("up-time")]
        public static void UpTime()
        {
            App.SaveAll(); // save to get updated values

            var uptime = "============ UP TIME ============\n";
            uptime += $"Total Game Time {new TimeSpan(0,0,0, (int)App.AccountInfo.GetTimePlayedTotal()):g}\n\n";

            foreach (var fableBook in Db.FablesDatabase.Books)
            {
                var book = App.FableData.GetBook(fableBook.BookId);

                foreach (var chapter in fableBook.Chapters)
                {

                    var chap = App.FableData.GetChapter(chapter.Uid);
                    var chapterPlayTime = App.FableData.GetChapterPlayTime(book.BookHero, book.BookNumber, chap.ChapterNumber);

                    uptime += $"{book.BookHero} Book #{book.BookNumber} Ch.{chap.ChapterNumber}:\n";
                    uptime += $"Total Chapter Time:      { new TimeSpan(0,0,0, (int)chapterPlayTime.TotalChapterPlayTime):g}\n";
                    uptime += $"Total Time In Battles:   { new TimeSpan(0,0,0, (int)chapterPlayTime.TotalTimeInBattles ):g}\n";
                    uptime += $"Total Time In Cutscenes: { new TimeSpan(0,0,0, (int)chapterPlayTime.TotalTimeWatchingCutscenes ):g}\n";
                    uptime += $"Total Time Deck Editing: { new TimeSpan(0,0,0, (int)chapterPlayTime.TotalTimeDeckEditing ):g}\n";
                    uptime += $"Total Time Melding:      { new TimeSpan(0,0,0, (int)chapterPlayTime.TotalTimeMelding ):g}\n";
                    uptime += $"Total Time In Shops:     { new TimeSpan(0,0,0, (int)chapterPlayTime.TotalTimeInShops ):g}\n";
                }
            }

            Debug.Log(uptime);
        }

        [Command("print-deck")]
        public static void PrintDeck()
        {
            if (!GameServer.IsRunning()) return;
            var deckPrint = $"Deck ({GameServer.ClientPlayerState.deck.Count})\n";
            for (var i = 0; i < GameServer.ClientPlayerState.deck.Count; i++)
            {
                var card = GameServer.State.GetCard(GameServer.ClientPlayerState.deck[i]);

                if (card!=null)
                {
                    deckPrint += $"{card.data.name}\n";
                }
            }
            Debug.Log(deckPrint);
        }

        [Command("print-enemy-hand")]
        public static void PrintEnemyHand()
        {
            if (!GameServer.IsRunning()) return;
            var deckPrint = $"Opp Hand ({GameServer.OpponentPlayerState.hand.Count})\n";
            for (var i = 0; i < GameServer.OpponentPlayerState.hand.Count; i++)
            {
                var card = GameServer.State.GetCard(GameServer.OpponentPlayerState.hand[i]);

                if (card!=null)
                {
                    deckPrint += $"{card.data.name} ({card.GetCost()}): {card.data.description}\n";
                }
            }
            Debug.Log(deckPrint);
        }

        [Command("grant-prize-pool-cards")]
        public static void GrantPrizePoolCards(string battleName)
        {
            if (!Db.Initialized)
            {
                Db.Initialize();
            }

            if (!App.Initialized)
            {
                App.Init();
            }

            var battle = Db.BattleDatabase.GetBattleDataByName(battleName);

            if (battle == null)
            {
                battle = Db.BattleDatabase.GetBattleData(battleName);
            }

            if (battle == null)
            {
                Debug.LogError($"Could not find battle {battleName}!");
                return;
            }

            var chapterData = Db.FablesDatabase.GetChapterByBattle(battle.Uid);
            var prizes = battle.GetCardRewardsForWin(Faction.War, chapterData.Uid, true);
            GrantItems(prizes);
        }

        public struct TerminalCommandChapterData
        {
            public int level;
            public int dawnDollars;
            public int manaShards;
            public int shopTotalDawnDollars;
            public int exp;
            public int battles;
            public List<ItemValue> cardList;
            public List<ItemValue> itemsList;
            public List<string> shopItems;
        }

        public static TerminalCommandChapterData GetRewardsFromSingleChapter(string bookName, int chapter, Faction playedAs, TerminalCommandChapterData importData)
        {
            var chapterName = $"{bookName}-1-{chapter}";
            var chapterData = Db.FablesDatabase.GetChapterByChapterId(chapterName);
            var mapData = Db.MapDatabase.GetMap(chapterData.mapId);

            if (mapData == null)
            {
                Debug.LogError("Map data is null!!");
                return new TerminalCommandChapterData();
            }

            var battleTiles = new List<FableTileData>();
            var chestTiles = new List<FableTileData>();
            var shopTiles = new List<FableTileData>();

            var addedMaps = new List<FableMapData>();
            var maps = new Queue<FableMapData>();
            maps.Enqueue(mapData);

            var startingCollection = "\nSTARTING COLLECTION:\n";

            for (var i = 0; i <  importData.cardList.Count; i++)
            {
                startingCollection += $"{ importData.cardList[i].ItemUid} x{ importData.cardList[i].Count}\n";
            }

            var startingIngredients = "\nSTARTING INGREDIENTS:\n";

            for (var i = 0; i <  importData.itemsList.Count; i++)
            {
                startingIngredients += $"{ importData.itemsList[i].ItemUid} x{ importData.itemsList[i].Count}\n";
            }

            //var startingCardList = new List<ItemValue>( importData.cardList );
            //importData.cardList = new List<ItemValue>();
            importData.itemsList = new List<ItemValue>();

            while (maps.Count > 0)
            {
                var nextMap = maps.Dequeue();

                if (nextMap == null)
                {
                    Debug.LogError("Next Map is null!!");
                    break;
                }

                addedMaps.Add(nextMap);

                for (var j = 0; j < nextMap.Rooms.Count; j++)
                {
                    if (nextMap.Rooms[j] == null || nextMap.Rooms[j].Tiles == null)
                    {
                        continue;
                    }

                    for (var k = 0; k < nextMap.Rooms[j].Tiles.Count; k++)
                    {
                        if (nextMap.Rooms[j].Tiles[j] == null)
                        {
                            continue;
                        }

                        var tile = nextMap.Rooms[j].Tiles[k];

                        if (tile.NodeType == NodeType.Battle)
                        {
                            var battle = Db.BattleDatabase.GetBattleData(tile.battleUid);

                            if (battle == null)
                            {
                                continue;
                            }

                            battleTiles.Add(tile);
                        }
                        else if (tile.NodeType == NodeType.Chest)
                        {
                            chestTiles.Add(tile);
                        }
                        else if (tile.NodeType == NodeType.Shop)
                        {
                            shopTiles.Add(tile);
                        }
                        else if (tile.entranceTileUid != 0)
                        {
                            var newMapData = Db.MapDatabase.GetMapByTileUid(tile.entranceTileUid);

                            if (!addedMaps.Contains(newMapData))
                            {
                                maps.Enqueue(newMapData);
                            }
                        }
                    }
                }
            }

            var battleBreakdown = "\nBATTLES BREAKDOWN\n";
            var currentLevel = importData.level;
            var xp = 0;
            var earnedDawnDollars = 0;

            for (var i = 0; i < battleTiles.Count; i++)
            {
                var tile = battleTiles[i];

                var battle = Db.BattleDatabase.GetBattleData(tile.battleUid);

                if (battle == null)
                {
                    continue;
                }

                var manaShardsGained = battle.GetManaShardsFromWin(true);
                var expGained = Xp.GetXpGainedFromEnemy(battle.BattleType, battle.Level, true, chapter);
                var levelsGained = Xp.GetGainedLevelsFromXp(currentLevel, expGained + xp, out int xpLeft);

                importData.manaShards += manaShardsGained;
                importData.exp += expGained;

                battleBreakdown += $"\nBattle [{importData.battles+1}] {battle.BattleName}\n" +
                                   $"Mana Shards Gained: {manaShardsGained}\n" +
                                   $"Exp Earned: {expGained}\n" +
                                   $"Levels Gained: {levelsGained}\n" +
                                   $"Enemy Level:{battle.Level}\n" +
                                   $"Hero Level (Starting Battle): {currentLevel}\n" +
                                   $"Hero Level (Ending Battle): {currentLevel+levelsGained}\n";

                currentLevel += levelsGained;
                xp = xpLeft;

                var cardListAsDic = importData.cardList.ToDictionary(keySelector: cr => cr.ItemUid);
                var cards = battle.GetCardRewardsForWin(playedAs, chapterData.Uid, true, cardListAsDic);
                var ingredients = battle.GetIngredientsFromWin(playedAs, true);

                battleBreakdown += $"Obtained Cards:\n";

                for (var k = 0; k < cards.Count; k++)
                {
                    var item = importData. cardList.Find(it => it.ItemUid == cards[k]);

                    if (item == null)
                    {
                        item = new ItemValue { ItemUid = cards[k] };
                        importData.cardList.Add(item);
                    }

                    item.Count++;
                    battleBreakdown += $"{item.ItemUid} x{item.Count}\n";
                }

                for (var k = 0; k < ingredients.Count; k++)
                {
                    var item =  importData.itemsList.Find(it => it.ItemUid == ingredients[k].ItemUid);

                    if (item!=null)
                    {
                        item.Count+=ingredients[k].Count;
                    }
                    else
                    {
                        importData.itemsList.Add(ingredients[k]);
                    }
                }

                if (battle.Accolades == null )
                {
                    continue;
                }

                var dawnDollarsGained = 0;

                for (var k = 0; k < battle.Accolades.Count; k++)
                {
                    var accolade = battle.Accolades[k];
                    dawnDollarsGained += accolade.DawnDollarReward;
                }

                earnedDawnDollars += dawnDollarsGained;
                importData.dawnDollars += dawnDollarsGained;

                battleBreakdown += $"Dawn Dollars Gained: {dawnDollarsGained}\n";

                importData.battles++;
            }

            for (var i = 0; i < chestTiles.Count; i++)
            {
                importData.manaShards += Crafting.ManaShardsPerChest;
            }

            for (var i = 0; i < shopTiles.Count; i++)
            {
                for (var j = 0; j < shopTiles[i].items.Count; j++)
                {
                    if (string.IsNullOrEmpty(shopTiles[i].items[j].ItemUid))
                    {
                        continue;
                    }

                    var cardItem = Db.ItemDatabase.GetItem(shopTiles[i].items[j].ItemUid);
                    var card = Db.CardDatabase.GetCard(shopTiles[i].items[j].ItemUid);

                    if (card == null)
                    {
                        Debug.LogError($"Card is null!! name='{shopTiles[i].items[j].ItemUid}'");
                        continue;
                    }

                    var inventoryCard = importData.cardList.Find(c => c.ItemUid == card.id);
                    var purchaseCount =
                        inventoryCard == null ? DeckData.GetMaxCopies(card.rarity) : DeckData.GetMaxCopies(card.rarity) - inventoryCard.Count;

                    for (var k = 0; k < purchaseCount; k++)
                    {
                        var price = cardItem.GetCurrencyPrice(Currency.DAWN_DOLLARS);

                        if (importData.dawnDollars >= price)
                        {
                            importData.dawnDollars -= price;

                            if (importData.cardList.Exists(c => c.ItemUid == card.id))
                            {
                                importData.cardList.Find(c => c.ItemUid == card.id).Count++;
                            }
                            else
                            {
                                importData.cardList.Add(new ItemValue { ItemUid = card.id, Count = 1} );
                            }
                        }
                    }

                    if (importData.shopItems.Contains(shopTiles[i].items[j].ItemUid))
                    {
                        continue;
                    }

                    importData.shopItems.Add(shopTiles[i].items[j].ItemUid);

                    for (var k = 0; k < DeckData.GetMaxCopies(card.rarity); k++)
                    {
                        var price =cardItem.GetCurrencyPrice(Currency.DAWN_DOLLARS);

                        if (importData.dawnDollars >= price)
                        {
                            importData.dawnDollars -= price;

                            if (importData.cardList.Exists(c => c.ItemUid == card.id))
                            {
                                importData.cardList.Find(c => c.ItemUid == card.id).Count++;
                            }
                            else
                            {
                                importData.cardList.Add(new ItemValue { ItemUid = card.id, Count = 1} );
                            }
                        }

                        importData.shopTotalDawnDollars += cardItem.GetCurrencyPrice(Currency.DAWN_DOLLARS);
                    }
                }
            }

            var levels = Xp.GetGainedLevelsFromXp( importData.level,  importData.exp, out var expLeft);
            var rewardPrint = $"REWARDS FOR {bookName.ToUpper()} CHAPTER {chapter}:\n";

            rewardPrint += $"\nMAPS TRAVELED:\n";

            for (var i = 0; i < addedMaps.Count; i++)
            {
                rewardPrint += $"{addedMaps[i].DisplayName}\n";
            }

            rewardPrint += $"\nXP GAINED: { importData.exp}\n";
            rewardPrint += $"STARTING LEVEL { importData.level}\n";
            rewardPrint += $"ENDING LEVEL { importData.level + levels}\n";
            rewardPrint += $"LEVELS GAINED: {levels}\n";

            importData.level += levels;

            rewardPrint += $"\nEARNED DAWN DOLLARS THIS CHAPTER: {earnedDawnDollars}\n";
            rewardPrint += $"\nDAWN DOLLARS LEFT (AFTER SPENDING): { importData.dawnDollars}\n";
            rewardPrint += $"TOTAL SHOP COST (BOOK TOTAL SO FAR): { importData.shopTotalDawnDollars}\n";
            rewardPrint += $"MANA SHARDS (BOOK TOTAL SO FAR): { importData.manaShards}\n";



            for (var i = 0; i < chestTiles.Count; i++)
            {
                rewardPrint += $"\nCHEST ({i+1}) {chestTiles[i].displayName}:\n";
                rewardPrint += $"MANA SHARDS ({Crafting.ManaShardsPerChest})\n";

                for (var j = 0; j < chestTiles[i].items.Count; j++)
                {
                    var reward = chestTiles[i].items[j];
                    rewardPrint += $"[{j}] = x{reward.Count} {reward.ItemUid}:\n";
                }
            }

            var expansion = CardSet.Core;

            if (bookName == "Redcroft")
            {
                expansion = CardSet.PiratesAhoy;
            }
            else if (bookName == "Violet")
            {
                expansion = CardSet.StarscapeTwilight;
            }
            else if (bookName == "Seto")
            {
                expansion = CardSet.MonksMantra;
            }
            else if (bookName == "Quill")
            {
                expansion = CardSet.PerilousPilfering;
            }
            else if (bookName == "Mereena")
            {
                expansion = CardSet.SproutwoodGuardian;
            }

            var collectionTotal = Db.CardDatabase.GetFullCollectionCardCountWithDuplicates(Faction.All);
            var collectionFactionTotal =  Db.CardDatabase.GetFullCollectionCardCountWithDuplicates( playedAs );
            var collectionFactionExpansionTotal =  Db.CardDatabase.GetFullCollectionCardCountWithDuplicates( playedAs, expansion );
            var collectionNumCardsOwned = 0f;

            for (var i = 0; i < importData. cardList.Count; i++)
            {
                var card = Db.CardDatabase.GetCard( importData.cardList[i].ItemUid);

                if (card != null)
                {
                    collectionNumCardsOwned += DeckData.GetMaxCopies(card.rarity);
                }
            }

            var collectionCompletePercent = collectionNumCardsOwned / (float)collectionTotal;
            var collectionCompleteFactionPercent =collectionNumCardsOwned / (float)collectionFactionTotal;
            var collectionCompleteExpansionPercent =collectionNumCardsOwned / (float)collectionFactionExpansionTotal;

            rewardPrint += $"\nCOLLECTION COMPLETENESS: {collectionCompletePercent:P0}";
            rewardPrint += $"\nFACTION COMPLETENESS: {collectionCompleteFactionPercent:P0}";
            rewardPrint += $"\nEXPANSION (FACTION) COMPLETENESS: {collectionCompleteExpansionPercent:P0}";
            rewardPrint += $"\nBATTLE COUNT: { importData.battles}\n{battleBreakdown}";
            rewardPrint += startingCollection;
            rewardPrint += $"\nCARD REWARDS ({collectionNumCardsOwned}):\n";

            // var fullCollectionOfCards = Db.CardDatabase.GetFullCollectionAsDictionary( Faction.All );
            // var fullCollectionOfFaction = Db.CardDatabase.GetFullCollectionAsDictionary( playedAs );

            var fullCollectionMinusOwned = Db.CardDatabase.GetFullCollectionAsDictionary( Faction.All );
            var fullCollectionOfFactionMinusOwned = Db.CardDatabase.GetFullCollectionAsDictionary( playedAs );

            for (var i = 0; i <  importData.cardList.Count; i++)
            {
                var cardItem = importData.cardList[i];
                rewardPrint += $"{ cardItem.ItemUid} x{ cardItem.Count}\n";

                if (fullCollectionMinusOwned.ContainsKey( cardItem.ItemUid))
                {
                    fullCollectionMinusOwned[ cardItem.ItemUid].Count-=cardItem.Count;

                    if (fullCollectionMinusOwned[ cardItem.ItemUid].Count <= 0)
                    {
                        fullCollectionMinusOwned.Remove( cardItem.ItemUid);
                    }
                }

                if (fullCollectionOfFactionMinusOwned.ContainsKey(cardItem.ItemUid))
                {
                    fullCollectionOfFactionMinusOwned[cardItem.ItemUid].Count-=cardItem.Count;

                    if (fullCollectionOfFactionMinusOwned[ cardItem.ItemUid].Count <= 0)
                    {
                        fullCollectionOfFactionMinusOwned.Remove( cardItem.ItemUid);
                    }
                }
            }

            rewardPrint += $"\nMISSING CARDS ({collectionFactionTotal-collectionNumCardsOwned}):\n";

            foreach (var missingCard in fullCollectionOfFactionMinusOwned)
            {
                var missingCardData = Db.CardDatabase.GetCard(missingCard.Key);

                if (missingCardData!=null)
                {
                    rewardPrint += $"{missingCard.Value.ItemUid} x{missingCard.Value.Count}\n";
                }
            }

            var inventoryDictionary = new Dictionary<string, ItemValue>();
            inventoryDictionary.Add(Currency.MANA_SHARDS,
                new ItemValue { ItemUid = Currency.MANA_SHARDS, Count = importData. manaShards });

            rewardPrint += startingIngredients;
            rewardPrint += "\nINGREDIENT REWARDS:\n";

            for (var i = 0; i <  importData.itemsList.Count; i++)
            {
                rewardPrint += $"{ importData.itemsList[i].ItemUid} x{ importData.itemsList[i].Count}\n";
                inventoryDictionary.Add( importData.itemsList[i].ItemUid,  importData.itemsList[i]);
            }

            rewardPrint += "\nCRAFTED CARDS:\n";

            var additionalCardsAdded = new Dictionary<string, ItemValue>();

            foreach (var missingCard in fullCollectionOfFactionMinusOwned)
            {
                var missingCardData = Db.CardDatabase.GetCard(missingCard.Key);
                var craftCount = 0;

                for (var i = 0; i < missingCard.Value.Count; i++)
                {
                    if (missingCardData != null && Crafting.CanCraftCardCustomInventory(missingCardData, inventoryDictionary, out var itemsUsed))
                    {
                        foreach (var itemUsed in itemsUsed)
                        {
                            inventoryDictionary[itemUsed.Key].Count -= itemUsed.Value.Count;

                            if (itemUsed.Key == Currency.MANA_SHARDS)
                            {
                                importData.manaShards -= itemUsed.Value.Count;
                            }

                            var item = importData.itemsList.Find( item => item.ItemUid == itemUsed.Key);

                            if (item!=null)
                            {
                                item.Count -= itemUsed.Value.Count;
                            }
                        }

                        craftCount++;
                        collectionNumCardsOwned++;

                        if (additionalCardsAdded.ContainsKey(missingCardData.id))
                        {
                            additionalCardsAdded[missingCardData.id].Count++;
                        }
                        else
                        {
                            additionalCardsAdded.Add(missingCardData.id, new ItemValue { ItemUid = missingCardData.id, Count = 1});
                        }
                    }
                }

                if (craftCount > 0) rewardPrint += $"{missingCardData.name} x{craftCount}\n";
            }

            collectionCompleteExpansionPercent = collectionNumCardsOwned / (float)collectionFactionExpansionTotal;

            rewardPrint += $"\nEXPANSION (FACTION) COMPLETENESS AFTER MELD: {collectionCompleteExpansionPercent:P0}";
            rewardPrint += $"\nCOLLECTION FINISHED:\n";

            for (var i = 0; i < importData.cardList.Count; i++)
            {
                rewardPrint += $"{ importData.cardList[i].ItemUid} x{ importData.cardList[i].Count}\n";
            }

            rewardPrint += $"\n\n===============================================================";
            rewardPrint += $"\nCHAPTER {chapter} COMPLETE";
            rewardPrint += $"\n===============================================================";

            Debug.Log(rewardPrint);

            return importData;
        }

        [Command("get-total-rewards-from-book")]
        public static void GetTotalRewardsFromBook(string bookName, Faction playedAs)
        {
            if (!Db.Initialized)
            {
                Db.Initialize();
            }

            if (!App.Initialized)
            {
                App.Init();
            }
            //
            // var chapterName = $"{bookName}-1-{chapter}";
            // var chapterData = Db.FablesDatabase.GetChapterByChapterId(chapterName);
            // var mapData = Db.MapDatabase.GetMap(chapterData.mapId);

            var importData = new TerminalCommandChapterData
            {
                level = 1,
                cardList = new List<ItemValue>(),
                itemsList = new List<ItemValue>(),
                shopItems = new List<string>()
            };

            var heroStarterDeck = Db.DeckRecipeDatabase.GetRecipeByName($"{bookName} Starter Deck");

            if (heroStarterDeck?.deckData != null)
            {
                for (var i = 0; i < heroStarterDeck.deckData.cards.Count; i++)
                {
                    importData.cardList.Add( new ItemValue { ItemUid = heroStarterDeck.deckData.cards[i].id, Count = heroStarterDeck.deckData.cards[i].count });
                }
            }

            var chapter = 1;

            for (var i = 0; i < 3; i++)
            {
                Debug.Log($"Getting for {bookName}, {chapter}");
                importData = GetRewardsFromSingleChapter(bookName, chapter, playedAs, importData );
                chapter++;
            }
        }

        [Command("get-total-rewards-from-chapter")]
        public static void GetTotalRewardsFromChapter(string bookName, int chapter, int startingLevel, Faction playedAs)
        {
            if (!Db.Initialized)
            {
                Db.Initialize();
            }

            if (!App.Initialized)
            {
                App.Init();
            }

            var chapterName = $"{bookName}-1-{chapter}";
            var chapterData = Db.FablesDatabase.GetChapterByChapterId(chapterName);
            var mapData = Db.MapDatabase.GetMap(chapterData.mapId);

            var dawnDollars = 0;
            var shopTotalDawnDollars = 0;
            var manaShards = 0;
            var exp = 0;
            var battles = 0;
            var cardList = new List<ItemValue>();
            var itemsList = new List<ItemValue>();

            // replace with single!

            var battleTiles = new List<FableTileData>();
            var chestTiles = new List<FableTileData>();
            var shopTiles = new List<FableTileData>();
            var heroStarterDeck = Db.DeckRecipeDatabase.GetRecipeByName($"{bookName} Starter Deck");

            if (heroStarterDeck != null && heroStarterDeck.deckData != null)
            {
                for (var i = 0; i < heroStarterDeck.deckData.cards.Count; i++)
                {
                    cardList.Add( new ItemValue { ItemUid = heroStarterDeck.deckData.cards[i].id, Count = heroStarterDeck.deckData.cards[i].count });
                }
            }

            var addedMaps = new List<FableMapData>();
            var maps = new Queue<FableMapData>();
            maps.Enqueue(mapData);

            while (maps.Count > 0)
            {
                var nextMap = maps.Dequeue();
                addedMaps.Add(nextMap);

                for (var j = 0; j < nextMap.Rooms.Count; j++)
                {
                    if (nextMap.Rooms[j] == null || nextMap.Rooms[j].Tiles == null)
                    {
                        continue;
                    }

                    for (var k = 0; k < nextMap.Rooms[j].Tiles.Count; k++)
                    {
                        if (nextMap.Rooms[j].Tiles[j] == null)
                        {
                            continue;
                        }

                        var tile = nextMap.Rooms[j].Tiles[k];

                        if (tile.NodeType == NodeType.Battle)
                        {
                            var battle = Db.BattleDatabase.GetBattleData(tile.battleUid);

                            if (battle == null)
                            {
                                continue;
                            }

                            battleTiles.Add(tile);
                        }
                        else if (tile.NodeType == NodeType.Chest)
                        {
                            chestTiles.Add(tile);
                        }
                        else if (tile.NodeType == NodeType.Shop)
                        {
                            shopTiles.Add(tile);
                        }
                        else if (tile.entranceTileUid != 0)
                        {
                            var newMapData = Db.MapDatabase.GetMapByTileUid(tile.entranceTileUid);

                            if (!addedMaps.Contains(newMapData))
                            {
                                maps.Enqueue(newMapData);
                            }
                        }
                    }
                }
            }

            for (var i = 0; i < battleTiles.Count; i++)
            {
                var tile = battleTiles[i];

                var battle = Db.BattleDatabase.GetBattleData(tile.battleUid);

                if (battle == null)
                {
                    continue;
                }

                manaShards += battle.GetManaShardsFromWin(true);
                exp += Xp.GetXpGainedFromEnemy(battle.BattleType, battle.Level, true, chapter);

                var cards = battle.GetCardRewardsForWin(playedAs, chapterData.Uid, true);
                var ingredients = battle.GetIngredientsFromWin(playedAs, true);

                for (var k = 0; k < cards.Count; k++)
                {
                    var item = cardList.Find(it => it.ItemUid == cards[k]);

                    if (item!=null)
                    {
                        item.Count++;
                    }
                    else
                    {
                        cardList.Add(new ItemValue { ItemUid = cards[k], Count = 1});
                    }
                }

                for (var k = 0; k < ingredients.Count; k++)
                {
                    var item = itemsList.Find(it => it.ItemUid == ingredients[k].ItemUid);

                    if (item!=null)
                    {
                        item.Count+=ingredients[k].Count;
                    }
                    else
                    {
                        itemsList.Add(ingredients[k]);
                    }
                }

                if (battle.Accolades == null ) continue;

                for (var k = 0; k < battle.Accolades.Count; k++)
                {
                    var accolade = battle.Accolades[k];
                    dawnDollars += accolade.DawnDollarReward;
                }

                battles++;
            }

            for (var i = 0; i < chestTiles.Count; i++)
            {
                manaShards += Crafting.ManaShardsPerChest;
            }

            var overlapItems = new List<string>();
            for (var i = 0; i < shopTiles.Count; i++)
            {
                for (var j = 0; j < shopTiles[i].items.Count; j++)
                {
                    if (overlapItems.Contains(shopTiles[i].items[j].ItemUid)) continue;

                    overlapItems.Add(shopTiles[i].items[j].ItemUid);

                    var cardItem = Db.ItemDatabase.GetItem(shopTiles[i].items[j].ItemUid);
                    var card = Db.CardDatabase.GetCard(shopTiles[i].items[j].ItemUid);

                    for (var k = 0; k < DeckData.GetMaxCopies(card.rarity); k++)
                    {
                        shopTotalDawnDollars += cardItem.GetCurrencyPrice(Currency.DAWN_DOLLARS);
                    }
                }
            }

            var levels = Xp.GetGainedLevelsFromXp(startingLevel, exp, out var expLeft);
            var rewardPrint = $"REWARDS FOR {bookName.ToUpper()} CHAPTER {chapter}:\n";

            rewardPrint += $"\nMAPS TRAVELED:\n";

            for (var i = 0; i < addedMaps.Count; i++)
            {
                rewardPrint += $"{addedMaps[i].DisplayName}\n";
            }

            rewardPrint += $"\nXP GAINED: {exp}\n";
            rewardPrint += $"STARTING LEVEL {startingLevel}\n";
            rewardPrint += $"ENDING LEVEL {startingLevel + levels}\n";
            rewardPrint += $"LEVELS GAINED: {levels}\n";

            rewardPrint += $"\nDAWN DOLLARS: {dawnDollars}\n";
            rewardPrint += $"DAWN DOLLARS SPENT: {shopTotalDawnDollars}\n";
            rewardPrint += $"MANA SHARDS: {manaShards}\n";

            rewardPrint += $"\nBATTLE COUNT: {battles}\n";

            for (var i = 0; i < chestTiles.Count; i++)
            {
                rewardPrint += $"\nCHEST ({i+1}) {chestTiles[i].displayName}:\n";
                rewardPrint += $"MANA SHARDS ({Crafting.ManaShardsPerChest})\n";

                for (var j = 0; j < chestTiles[i].items.Count; j++)
                {
                    var reward = chestTiles[i].items[j];
                    rewardPrint += $"[{j}] = x{reward.Count} {reward.ItemUid}:\n";
                }
            }

            var expansion = CardSet.Core;

            if (bookName == "Redcroft")
            {
                expansion = CardSet.PiratesAhoy;
            }
            else if (bookName == "Violet")
            {
                expansion = CardSet.StarscapeTwilight;
            }
            else if (bookName == "Seto")
            {
                expansion = CardSet.MonksMantra;
            }
            else if (bookName == "Quill")
            {
                expansion = CardSet.PerilousPilfering;
            }
            else if (bookName == "Mereena")
            {
                expansion = CardSet.SproutwoodGuardian;
            }

            var collectionTotal = Db.CardDatabase.GetFullCollectionCardCountWithDuplicates(Faction.All);
            var collectionFactionTotal =  Db.CardDatabase.GetFullCollectionCardCountWithDuplicates( playedAs );
            var collectionFactionExpansionTotal =  Db.CardDatabase.GetFullCollectionCardCountWithDuplicates( playedAs, expansion );
            var collectionNumCardsOwned = 0f;

            for (var i = 0; i < cardList.Count; i++)
            {
                var card = Db.CardDatabase.GetCard(cardList[i].ItemUid);

                if (card != null)
                {
                    collectionNumCardsOwned += DeckData.GetMaxCopies(card.rarity);
                }
            }

            var collectionCompletePercent = collectionNumCardsOwned / (float)collectionTotal;
            var collectionCompleteFactionPercent =collectionNumCardsOwned / (float)collectionFactionTotal;
            var collectionCompleteExpansionPercent =collectionNumCardsOwned / (float)collectionFactionExpansionTotal;

            rewardPrint += $"\nCOLLECTION COMPLETENESS: {collectionCompletePercent:P0}";
            rewardPrint += $"\nFACTION COMPLETENESS: {collectionCompleteFactionPercent:P0}";
            rewardPrint += $"\nEXPANSION (FACTION) COMPLETENESS: {collectionCompleteExpansionPercent:P0}";
            rewardPrint += $"\nCARD REWARDS ({collectionNumCardsOwned}):\n";

            // var fullCollectionOfCards = Db.CardDatabase.GetFullCollectionAsDictionary( Faction.All );
            // var fullCollectionOfFaction = Db.CardDatabase.GetFullCollectionAsDictionary( playedAs );

            var fullCollectionMinusOwned = Db.CardDatabase.GetFullCollectionAsDictionary( Faction.All );
            var fullCollectionOfFactionMinusOwned = Db.CardDatabase.GetFullCollectionAsDictionary( playedAs );

            for (var i = 0; i < cardList.Count; i++)
            {
                rewardPrint += $"{cardList[i].ItemUid} x{cardList[i].Count}\n";

                if (fullCollectionMinusOwned.ContainsKey(cardList[i].ItemUid))
                {
                    fullCollectionMinusOwned[cardList[i].ItemUid].Count--;

                    if (fullCollectionMinusOwned[cardList[i].ItemUid].Count <= 0)
                    {
                        fullCollectionMinusOwned.Remove(cardList[i].ItemUid);
                    }
                }

                if (fullCollectionOfFactionMinusOwned.ContainsKey(cardList[i].ItemUid))
                {
                    fullCollectionOfFactionMinusOwned[cardList[i].ItemUid].Count--;

                    if (fullCollectionOfFactionMinusOwned[cardList[i].ItemUid].Count <= 0)
                    {
                        fullCollectionOfFactionMinusOwned.Remove(cardList[i].ItemUid);
                    }
                }
            }

            rewardPrint += $"\nMISSING CARDS ({collectionFactionTotal-collectionNumCardsOwned}):\n";

            foreach (var missingCard in fullCollectionOfFactionMinusOwned)
            {
                var missingCardData = Db.CardDatabase.GetCard(missingCard.Key);

                if (missingCardData!=null)
                {
                    rewardPrint += $"{missingCard.Value.ItemUid} x{missingCard.Value.Count}\n";
                }
            }

            var inventoryDictionary = new Dictionary<string, ItemValue>();
            inventoryDictionary.Add(Currency.MANA_SHARDS,
                new ItemValue { ItemUid = Currency.MANA_SHARDS, Count = manaShards });
            rewardPrint += "\nINGREDIENT REWARDS:\n";

            for (var i = 0; i < itemsList.Count; i++)
            {
                rewardPrint += $"{itemsList[i].ItemUid} x{itemsList[i].Count}\n";
                inventoryDictionary.Add(itemsList[i].ItemUid, itemsList[i]);
            }

            rewardPrint += "\nPOSSIBLE CARDS MELDED:\n";

            foreach (var missingCard in fullCollectionOfFactionMinusOwned)
            {
                var missingCardData = Db.CardDatabase.GetCard(missingCard.Key);
                var craftCount = 0;
                //rewardPrint += $"{missingCardData.name}:\n";

                for (var i = 0; i < missingCard.Value.Count; i++)
                {
                    if (missingCardData != null &&
                        Crafting.CanCraftCardCustomInventory(missingCardData, inventoryDictionary, out var itemsUsed))
                    {
                        craftCount++;

                        foreach (var itemUsed in itemsUsed)
                        {
                            inventoryDictionary[itemUsed.Key].Count -= itemUsed.Value.Count;

                            //rewardPrint += $"\t({craftCount}) {itemUsed.Value.ItemUid} x{itemUsed.Value.Count}";
                        }

                        collectionNumCardsOwned++;
                    }
                }

                rewardPrint += $"{missingCardData.name} x{craftCount}\n";
            }

            collectionCompleteExpansionPercent = collectionNumCardsOwned / (float)collectionFactionExpansionTotal;

            rewardPrint += $"\nEXPANSION (FACTION) COMPLETENESS AFTER MELD: {collectionCompleteExpansionPercent:P0}";
            rewardPrint += $"\n\n===============================================================";
            rewardPrint += $"\nCHAPTER {chapter} COMPLETE";
            rewardPrint += $"\n===============================================================";

            Debug.Log(rewardPrint);
        }
        [Command("show-all-addressables")]
        public static void ShowAllAddressables()
        {
            #if UNITY_EDITOR
            var addressableList = new List<string>();
            var settings = UnityEditor.AddressableAssets.AddressableAssetSettingsDefaultObject.Settings;

            foreach (var group in settings.groups)
            {
                foreach (var entry in group.entries)
                {
                    addressableList.Add( entry.address );
                    // addressableGUIDName newitem = new addressableGUIDName();
                    // newitem.name = entry.address.ToString();
                    // newitem.guid = entry.guid.ToString();
                    // addressableListObject.GuidNameData.Add(newitem);
                }
            }

            Debug.Log($"Addressable Count = {addressableList.Count}");

            for (var i = 0; i < addressableList.Count; i++)
            {
                Debug.Log(addressableList[i]);
            }
            #endif
        }

        [Command("start-battle")]
        public static async void StartBattle(string battleName)
        {
            if (!Db.Initialized)
            {
                Db.Initialize();
            }

            if (!App.Initialized)
            {
                App.Init();
            }

            var battle = Db.BattleDatabase.GetBattleDataByName(battleName);

            if (battle == null)
            {
                battle = Db.BattleDatabase.GetBattleData(battleName);
            }

            if (battle == null)
            {
                Debug.LogError($"Could not find battle {battleName}!");
                return;
            }

            var tileByBattle = Db.MapDatabase.GetTileByBattle(battle.Uid);
            var sceneEnvironment = SceneEnvironment.Ocean;
            var sceneToReturnTo = SceneManager.GetActiveScene().name;

            if (tileByBattle != null)
            {
                sceneEnvironment = tileByBattle.GetSceneEnvironment();
            }

            Events.Publish(null, EventType.StartBattle, new StartBattleEventArgs
            {
                gameMode = new GameMode
                {
                    GameType = GameType.STORY,
                    ConnectionMode = ConnectionMode.LOCAL,
                    SceneEnvironment = sceneEnvironment,
                    SceneToReturnTo = sceneToReturnTo,
                    ShowResultsAfterMatch = false,
                    BattleUid = battle.Uid,
                    DeckUid = App.PlayerDecks.GetDeckBasedOnGameState().uid,
                }
            });

            await SceneController.Instance.UnloadAllScenes();
        }
    }
}