using System.Collections.Generic;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Cutscene;
using CrossBlitz.Hero;
using UnityEngine;

namespace CrossBlitz.Developer
{
    public class DisplayHeroAndCardsTwitterTemplate : MonoBehaviour
    {
        public Faction displayFaction;
        public float maxXRotation = 20;
        public float rotationSpeed = 2;
        public SpriteAnimation background;
        public SpriteAnimation header;
        public SpriteAnimation heroBackground;
        public HeroCutscenePortraitAnimator heroPortrait;
        public RectTransform cardsLayout;
        public GameObject zoomedCardPrefab;
        public CardDatabase cardDatabase;

        public List<float> rotationDisplacements;
        public List<string> cardToDisplay;


        private List<CardView> m_cards;
        private bool m_rotateCards;

        private void Start()
        {
            var factionLower = displayFaction.ToString().ToLower();
            background.Play(factionLower);
            header.Play(factionLower);
            heroBackground.Play(factionLower);
            heroPortrait.SetHeroId(HeroData.GetHeroFromFaction(displayFaction));


            m_cards = new List<CardView>();
            DisplayCards();
        }

        private void DisplayCards()
        {
            var settings = new CreateCardFactorySettings
            {
                CardPrefab = zoomedCardPrefab,
                Layout = cardsLayout
            };

            for (var i = 0; i < cardToDisplay.Count; i++)
            {
                settings.CardData = cardDatabase.GetCard(cardToDisplay[i]);
                var card = CardFactory.Create(settings, m_cards);
                card.SetActive(true);
                rotationDisplacements.Add(Random.Range(0, 2));
            }

            m_rotateCards = true;
        }

        private void Update()
        {
            if (m_rotateCards)
            {


                for (var i = 0; i < m_cards.Count; i++)
                {
                    rotationDisplacements[i] += Time.deltaTime * rotationSpeed;
                    var rotateAngle = maxXRotation * (Mathf.PingPong(rotationDisplacements[i],2) - 1);
                    m_cards[i].RectTransform().eulerAngles = new Vector3(0, rotateAngle, 0);
                }
            }
        }
    }
}