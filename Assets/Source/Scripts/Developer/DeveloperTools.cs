namespace CrossBlitz.Developer
{
    public static class DeveloperTools
    {
        public static class OpenToolMode
        {
            public static bool ChapterEditor;
            public static bool CutsceneEditor;
        }
    }
}