using System;
using QFSW.QC;
using UnityEngine;

namespace CrossBlitz.Developer
{
    public class BindingCommandsUpdater : MonoBehaviour
    {
        private KeyCode[] _keys;
        private bool _updateKeys;
        private bool _quantumConsoleReady;

        private void Start()
        {
            _updateKeys = true;
            _keys = (KeyCode[])Enum.GetValues(typeof(KeyCode));
        }

        private void OnDestroy()
        {
            if (_quantumConsoleReady)
            {
                QuantumConsole.Instance.OnActivate -= OnConsoleActivate;
                QuantumConsole.Instance.OnDeactivate -= OnConsoleDeactivate;
            }
        }

        private void OnConsoleActivate()
        {
            _updateKeys = false;
        }

        private void OnConsoleDeactivate()
        {
            _updateKeys = true;
        }

        private void Update()
        {
            if (!_quantumConsoleReady)
            {
                if (QuantumConsole.Instance != null)
                {
                    QuantumConsole.Instance.OnActivate += OnConsoleActivate;
                    QuantumConsole.Instance.OnDeactivate += OnConsoleDeactivate;
                    _quantumConsoleReady = true;
                }

                return;
            }

            if (!_updateKeys) return;
            if (!Input.anyKey) return;

            var current = KeyCode.None;

            for (var i = 0; i < _keys.Length; i++)
            {
                if (Input.GetKeyDown(_keys[i]))
                {
                    current = _keys[i];
                }
            }

            if (current != KeyCode.None && PlayerPrefs.HasKey($"bound-command-from-{current}"))
            {
                var command = PlayerPrefs.GetString($"bound-command-from-{current}");

                if(!string.IsNullOrEmpty(command))
                {
                    QuantumConsole.Instance.InvokeCommand(command);
                }
            }
        }
    }
}