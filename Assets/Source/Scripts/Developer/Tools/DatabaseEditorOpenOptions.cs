using CrossBlitz.Fables.Data;

namespace CrossBlitz.Developer.Tools
{
    public class DatabaseEditorOpenOptions
    {
        public bool viewOnly;
        public bool linkOptionEnabled;
        public string itemContainerLink;
        public FableTileData tile;
    }
}