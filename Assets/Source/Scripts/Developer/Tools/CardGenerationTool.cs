using System;
using System.Collections;
using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using GameDataEditor;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.U2D;
using Random = UnityEngine.Random;

namespace CrossBlitz.Developer.Tools
{
    public class CardGenerationTool : MonoBehaviour
    {
        public const string ZoomedCardFolderName = "Cross_Blitz_Zoomed_Cards";
        public const string StandardCardFolderName = "Cross_Blitz_Standard_Cards";
        public static CardGenerationTool Instance;

        // public const int Width = 168;
        // public const int Height = 202;

        [BoxGroup("Zoomed Cards")] public RectTransform zoomedCardContainer;
        [BoxGroup("Zoomed Cards")] public GameObject zoomedCardPrefab;
        [BoxGroup("Standard Cards")] public RectTransform standardCardContainer;
        [BoxGroup("Standard Cards")] public GameObject standardCardPrefab;
        [BoxGroup("Misc")] public CardDatabase cardDatabase;
        [BoxGroup("Misc")] public SpriteAtlas cardAtlas;

        private bool m_generatingCards;

        private void Start()
        {
            Instance = this;
        }
        // =====================================================================
        // ZOOMED CARDS
        // =====================================================================

        [Button]
        public void GenerateCards()
        {
            StartCoroutine(GenerateAllCards());
        }

        private IEnumerator GenerateAllCards()
        {
            if (m_generatingCards)
            {
                Debug.LogError("Cards already being generated!");
                yield break;
            }

            m_generatingCards = true;
            cardDatabase.Init(false);

            for (var i = 0; i < cardDatabase.Cards.Count; i++)
            {
                yield return StartCoroutine(GenerateCard(cardDatabase.Cards[i].id, zoomedCardPrefab,
                    zoomedCardContainer, 168, 202, ZoomedCardFolderName));
                yield return new WaitForEndOfFrame();
            }

            m_generatingCards = false;
        }

        private IEnumerator GenerateCard(string id, GameObject prefab, RectTransform parent, int width, int height, string folderName)
        {
            var settings = new CreateCardFactorySettings
            {
                CardPrefab = prefab,
                Layout = parent,
                AdditionalComponents = new List<Type>()
            };

            settings.CardData = cardDatabase.GetCard(id);
            var cardView = CardFactory.Create(settings, null);
            cardView.SetInteractable(false);
            if (cardView.cardOutline) cardView.cardOutline.RemoveOutline();
            if (cardView.shadow) cardView.shadow.SetActive(false);
            if (cardView.front3D) cardView.front3D.SetActive(false);
            cardView.RectTransform().anchoredPosition = Vector2.zero;
            cardView.SetActive(true);

            while (cardView.isDirty || cardView.loadingData) yield return null;

            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();

            var screenshot = new Texture2D(width, height) {filterMode = FilterMode.Point};
            screenshot.ReadPixels(new Rect(0,0, width,height), 0,0 );
            screenshot.Apply();

            var bytes = screenshot.EncodeToPNG();
            Destroy(screenshot);
            var desktopPath = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            var writePath = $"{desktopPath}/{folderName}";

            if (!System.IO.Directory.Exists(writePath))
            {
                System.IO.Directory.CreateDirectory(writePath);
            }

            var fileName = cardView.data.name.Replace(" ", "-").ToLower();
            var assetPath = $"{writePath}/{fileName}.png";

            System.IO.File.WriteAllBytes(assetPath,bytes);
            cardView.SetActive(false);
        }

        // =====================================================================
        // STANDARD CARDS
        // =====================================================================

        [Button]
        public void GenerateStandardCards()
        {
            StartCoroutine(GenerateAllStandardCards());
        }

        private IEnumerator GenerateAllStandardCards()
        {
            if (m_generatingCards)
            {
                Debug.LogError("Cards already being generated!");
                yield break;
            }

            m_generatingCards = true;

            var cards = GDEDataManager.GetAllItems<GDECardData>();
            cardDatabase.Init(false);

            for (var i = 0; i < cards.Count; i++)
            {
                yield return StartCoroutine(GenerateCard(cards[i].Key, standardCardPrefab,
                    standardCardContainer, 88, 100,StandardCardFolderName ));
                yield return new WaitForEndOfFrame();
            }

            m_generatingCards = false;
        }
    }
}