using System.Collections;
using System.Collections.Generic;
using CrossBlitz.ViewAPI;
using UnityEngine;
//using UltimateClean;

namespace CrossBlitz.Developer.Tools
{
    public class DatabaseEditorView : View
    {
        //public Popup popup;
        protected DatabaseEditorOpenOptions _options;

        public void Open(DatabaseEditorOpenOptions options)
        {
            _options = options;
            gameObject.SetActive(true);
            //popup.Open();
            Open();
        }

        public override void Open()
        {

        }

        public override IEnumerator<float> Close()
        {
            Debug.LogError("Database Editor Views should not use this close method, use ClosePopup() instead.");
            yield break;
        }

        public virtual void ClosePopup()
        {
            // if (popup.IsActive())
            // {
            //     popup.Close();
            // }
        }
    }
}