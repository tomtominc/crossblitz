using System;
using System.Collections;
using System.Collections.Generic;
using CrossBlitz.Fables.Cutscene.Tools;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Tools;
using CrossBlitz.Items;
using CrossBlitz.Shop;
using CrossBlitz.Shop.Data;
using CrossBlitz.UI.Widgets;
using CrossBlitz.ViewAPI;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.Tools
{
    public class ToolsNavigator : View
    {
        public static ToolsNavigator Instance;
        [BoxGroup("Buttons")] public ToggleButton gameButton;
        [BoxGroup("Buttons")] public ToggleButton chapterButton;
        [BoxGroup("Buttons")] public ToggleButton tileMetadataButton;
        [BoxGroup("Buttons")] public ToggleButton cutsceneButton;
        [BoxGroup("Buttons")] public ToggleButton itemDatabaseButton;
        [BoxGroup("Buttons")] public ToggleButton recipeButton;
        [BoxGroup("Buttons")] public ToggleButton shopButton;
        [BoxGroup("Buttons")] public Button exitButton;
        [BoxGroup("Editors")] public FableChapterEditor chapterEditor;
        [BoxGroup("Editors")] public CutsceneEditor cutsceneEditor;
        [BoxGroup("Editors")] public ShopEditor shopEditor;
        [BoxGroup("Editors")] public TileMetadataEditor tileMetadataEditor;
        [BoxGroup("Editors")] public ItemDatabaseList itemDatabase;
        [BoxGroup("Misc")] public GameObject loadingOverlay;
        public event Action OnGameButton;
        public event Action<string> OnLinked;
        public event Action<ShopCategory, string> OnItemAdded;
        public event Action<FableChapterData, int> OnEditMap;

        public event Action OnExit;

        private DatabaseEditorView _openMenu;

        private DatabaseEditorOpenOptions _options;

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            gameButton.OnValueChanged += GameButtonClicked;
            chapterButton.OnValueChanged += OpenChapterEditMenu;
            cutsceneButton.OnValueChanged += OpenCutsceneEditMenu;
            shopButton.OnValueChanged += OpenShopEditMenu;
            tileMetadataButton.OnValueChanged += OpenTileMetadata;
            itemDatabaseButton.OnValueChanged += OpenItemDatabase;

            cutsceneEditor.OnLinkedCutscene += Linked;
            shopEditor.OnLinked += Linked;
            itemDatabase.OnItemLinked += ItemAdded;
            chapterEditor.OnEditMap += EditMap;

            exitButton.onClick.AddListener(OnExitButton);
        }

        private void OnDestroy()
        {
            gameButton.OnValueChanged -= GameButtonClicked;
            chapterButton.OnValueChanged -= OpenChapterEditMenu;
            cutsceneButton.OnValueChanged -= OpenCutsceneEditMenu;
            shopButton.OnValueChanged -= OpenShopEditMenu;
            tileMetadataButton.OnValueChanged -= OpenTileMetadata;
            itemDatabaseButton.OnValueChanged -= OpenItemDatabase;

            cutsceneEditor.OnLinkedCutscene -= Linked;
            shopEditor.OnLinked -= Linked;
            itemDatabase.OnItemLinked -= ItemAdded;
            chapterEditor.OnEditMap -= EditMap;
        }

        public override void Open()
        {
            // background.interactable = true;
            // background.blocksRaycasts = true;
            // background.SetActive(true);
            // background.DOFade(1, 0.25f);
            // sidePanel.DOAnchorPosX(0, 0.5f).SetEase(Ease.OutBack);
        }

        public void OpenChapterEditMenu(DatabaseEditorOpenOptions options)
        {
            if (_openMenu == chapterEditor) return;
            if (_openMenu) _openMenu.ClosePopup();

            _options = options;
            _openMenu = chapterEditor;
            chapterEditor.Open(_options);
        }

        public void OpenCutsceneEditMenu(DatabaseEditorOpenOptions options)
        {
            if (_openMenu == cutsceneEditor) return;
            if (_openMenu) _openMenu.ClosePopup();

            _options = options;
            _openMenu = cutsceneEditor;
            cutsceneEditor.Open(_options);
        }

        public void OpenShopEditMenu(DatabaseEditorOpenOptions options)
        {
            if (_openMenu == shopEditor) return;
            if (_openMenu) _openMenu.ClosePopup();

            _options = options;
            _openMenu = shopEditor;
            shopEditor.Open(_options);
        }

        public bool OpenTileMetadata(DatabaseEditorOpenOptions options)
        {
            if (_openMenu == tileMetadataEditor) return false;
            if (_openMenu) _openMenu.ClosePopup();

            _options = options;
            _openMenu = tileMetadataEditor;
            tileMetadataEditor.Open(_options);
            return true;
        }

        public void OpenItemDatabase(DatabaseEditorOpenOptions options)
        {
            if (_openMenu == itemDatabase) return;
            if (_openMenu) _openMenu.ClosePopup();

            _options = options;
            _openMenu = itemDatabase;
            itemDatabase.Open(_options);
        }

        private void GameButtonClicked(bool isOn, string content)
        {
            if (!isOn) return;
            OnGameButton?.Invoke();
        }

        private void OpenChapterEditMenu(bool isOn, string content)
        {
            if (!isOn) return;
            if (_options == null) _options = new DatabaseEditorOpenOptions();
            OpenChapterEditMenu(_options);
        }

        private void OpenCutsceneEditMenu(bool isOn, string content)
        {
            if (!isOn) return;
            if (_options == null) _options = new DatabaseEditorOpenOptions();
            OpenCutsceneEditMenu(_options);
        }

        private void OpenShopEditMenu(bool isOn, string content)
        {
            if (!isOn) return;
            if (_options == null) _options = new DatabaseEditorOpenOptions();
            OpenShopEditMenu(_options);
        }

        private void OpenTileMetadata(bool isOn, string content)
        {
            if (!isOn) return;
            if (_options == null) _options = new DatabaseEditorOpenOptions();
            OpenTileMetadata(_options);
        }

        private void OpenItemDatabase(bool isOn, string content)
        {
            if (!isOn) return;

            if (_options == null) _options = new DatabaseEditorOpenOptions();
            OpenItemDatabase(_options);
        }

        private void Linked(string uid)
        {
            OnLinked?.Invoke(uid);
        }

        private void ItemAdded(ShopCategory category, string item)
        {
            OnItemAdded?.Invoke(category, item);
        }

        private void EditMap(FableChapterData chapterData, int mapIndex)
        {
            OnEditMap?.Invoke(chapterData, mapIndex);
        }

        private void OnExitButton()
        {
            OnExit?.Invoke();
        }

        // private void CloseMenu()
        // {
        //     background.DOFade(0, 0.25f);
        //     sidePanel.DOAnchorPosX(-194f, 0.5f).SetEase(Ease.OutBack);
        //     background.interactable = false;
        //     background.blocksRaycasts = false;
        // }

        public override IEnumerator<float> Close()
        {
            yield break;
        }
    }
}