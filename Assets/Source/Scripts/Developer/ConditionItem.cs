﻿using System.Collections;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.UI.Widgets.Dropdown;
using UnityEngine;

public class ConditionItem : MonoBehaviour
{
    public DropdownBox conditionDropdown;
    public DropdownBox classDropdown;
    public DropdownBox factionDropdown;

    public AbilityCondition condition;

    public void OnEnable()
    {
        ResetCondition();

        conditionDropdown.ItemSelected += OnConditionSelected;
        classDropdown.ItemSelected += OnClassSelected;
        factionDropdown.ItemSelected += OnFactionSelected;
    }

    private void OnDisable()
    {
        conditionDropdown.ItemSelected -= OnConditionSelected;
        classDropdown.ItemSelected -= OnClassSelected;
        factionDropdown.ItemSelected -= OnFactionSelected;
    }

    private void ResetCondition()
    {
        classDropdown.SetActive(false);
        factionDropdown.SetActive(false);
    }

    public void SetCondition(AbilityCondition _condition)
    {
        // condition = _condition;
        // conditionDropdown.SetItemSelected(condition.condition.ToString());
        //
        // if (condition.condition == Condition.CLASS_ON_FIELD)
        // {
        //     classDropdown.SetActive(true);
        //     classDropdown.SetItemSelected(condition.@class.ToString());
        // }
        // else if (condition.condition == Condition.FACTION_ON_FIELD)
        // {
        //     factionDropdown.SetActive(true);
        //     factionDropdown.SetItemSelected(condition.faction.ToString());
        // }
    }

    private void OnConditionSelected(DropdownItemData data)
    {
        ResetCondition();

        condition.condition = GameUtilities.ParseEnum(data.name.Replace(" ", "_"), Condition.NONE);

        Debug.LogFormat("condition = {0}", condition.condition);

        if (condition.condition == Condition.CLASS_ON_FIELD)
        {
            classDropdown.SetActive(true);
            Debug.Log("hereeee");
        }
        else if (condition.condition == Condition.FACTION_ON_FIELD)
        {
            factionDropdown.SetActive(true);
        }
    }

    private void OnFactionSelected(DropdownItemData data)
    {
        condition.faction = GameUtilities.ParseEnum(data.name.Replace(" ", "_"), Faction.Neutral);
    }

    private void OnClassSelected(DropdownItemData data)
    {
        condition.@class = GameUtilities.ParseEnum(data.name.Replace(" ", "_"), Class.None);
    }

}
