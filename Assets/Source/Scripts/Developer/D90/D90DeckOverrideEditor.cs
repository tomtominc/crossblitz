using System;
using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Hero;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90DeckOverrideEditor : MonoBehaviour
    {
        public Toggle enabledToggle;
        public Toggle drawOrderToggle;
        public CanvasGroup disableGroup;
        public D90DatabaseDropdown overrideHeroDropdown;
        public D90DeckInspector deckInspector;

        private CustomDeckOverride m_deckOverride;
        public event Action<CustomDeckOverride> OnDeckOverrideChanged;

        public void SetDeck(CustomDeckOverride deckOverride)
        {
            m_deckOverride = deckOverride;

            enabledToggle.onValueChanged.RemoveAllListeners();
            enabledToggle.onValueChanged.AddListener(OnEnabledToggled);
            enabledToggle.SetIsOnWithoutNotify(m_deckOverride.Enabled);

            drawOrderToggle.onValueChanged.RemoveAllListeners();
            drawOrderToggle.onValueChanged.AddListener(OnDrawOrderToggled);
            drawOrderToggle.SetIsOnWithoutNotify(m_deckOverride.DrawInOrder);

            disableGroup.interactable = enabledToggle.isOn;
            disableGroup.blocksRaycasts = enabledToggle.isOn;

            m_deckOverride.OverrideDeck ??= new DeckData();
            m_deckOverride.OverrideDeck.cards ??= new List<CardValue>();

            deckInspector.OnCardsChanged -= OnCardsChanged;
            deckInspector.OnCardsChanged += OnCardsChanged;
            deckInspector.SetupCards( m_deckOverride.OverrideDeck.cards );

            overrideHeroDropdown.OnValueChanged -= OnOverrideHeroChanged;
            overrideHeroDropdown.OnValueChanged += OnOverrideHeroChanged;

            var hero = Db.CharacterDatabase.GetCharacter(m_deckOverride.OverrideDeck.hero);
            if (hero != null) overrideHeroDropdown.SetDropdownWithoutNotify(hero.name);
            else overrideHeroDropdown.Init();


        }

        private void OnOverrideHeroChanged(D90DatabaseDropdown dropdown, int value)
        {
            if (value <= 0)
            {
                m_deckOverride.OverrideDeck.hero = HeroData.GetHeroFromFaction(m_deckOverride.OverrideDeck.faction);
            }
            else
            {
                var hero = Db.CharacterDatabase.GetCharacterByName(dropdown.dropdown.options[value].text);
                if (hero != null)
                {
                    m_deckOverride.OverrideDeck.hero = hero.id;
                }
                else
                {
                    m_deckOverride.OverrideDeck.hero = HeroData.GetHeroFromFaction(m_deckOverride.OverrideDeck.faction);
                }
            }

        }

        private void OnEnabledToggled(bool e)
        {
            m_deckOverride.Enabled = e;
            disableGroup.interactable = enabledToggle.isOn;
            disableGroup.blocksRaycasts = enabledToggle.isOn;

            OnDeckOverrideChanged?.Invoke(m_deckOverride);
        }

        private void OnDrawOrderToggled(bool e)
        {
            m_deckOverride.DrawInOrder = e;
            OnDeckOverrideChanged?.Invoke(m_deckOverride);
        }

        private void OnCardsChanged(List<CardValue> cards)
        {
            m_deckOverride.OverrideDeck.cards = cards;
            OnDeckOverrideChanged?.Invoke(m_deckOverride);
        }

    }
}