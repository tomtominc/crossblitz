using System;
using System.Collections.Generic;
using UnityEngine;

namespace CrossBlitz.Developer.D90
{
    public class D90ToggleGroup : MonoBehaviour
    {
        public List<D90ToggleButton> toggles;
        private D90ToggleButton _currentlyOnToggle;
        public event Action<D90ToggleButton> OnToggle;

        private void Start()
        {
            if(toggles != null)
            {
                toggles.RemoveAll(t => t == null);

                for (var i = 0; i < toggles.Count; i++)
                {
                    if(toggles[i] == null) continue;
                    toggles[i].OnToggle += OnToggled;
                    toggles[i].SetIsOn(i==0);
                }
            }
        }

        private void OnToggled(D90ToggleButton toggle, bool isOn)
        {
            if (isOn)
            {
                _currentlyOnToggle = toggle;
                OnToggle?.Invoke(_currentlyOnToggle);
            }
        }

        private void Update()
        {
            if (toggles == null || toggles.Count == 0)
            {
                if(Application.isPlaying)
                    Debug.LogError("No toggled configured!!");
                return;
            }

            if (_currentlyOnToggle == null)
            {
                _currentlyOnToggle = toggles.Find(toggle => toggle.isOn);
            }

            if (_currentlyOnToggle != null)
            {
                for (var i = 0; i < toggles.Count; i++)
                {
                    if (toggles[i] == null) continue;
                    toggles[i].SetIsOn( toggles[i] == _currentlyOnToggle);
                }
            }
        }
    }
}