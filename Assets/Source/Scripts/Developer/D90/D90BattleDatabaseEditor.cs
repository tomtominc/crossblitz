using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.GameVfx;
using CrossBlitz.UI.Widgets;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90BattleDatabaseEditor : MonoBehaviour
    {
        public D90ContentLayout battlesList;
        public LayoutItem battleTextItemPrefab;

        public Button addButton;
        public Button moveUpButton;
        public Button moveDownButton;

        public D90BattleDataEditor battleDataEditor;
        public D90BattleEffectEditor battleEffectEditor;
        public D90ContentLayout battleEffectsLayout;
        public LayoutItem battleEffectItemPrefab;
        public Button addEffectButton;
        private string m_lastEffectSelected;

        private string m_battleUid;

        public void Open(string battleUid)
        {
            m_battleUid = battleUid;

            if (string.IsNullOrEmpty(m_battleUid))
            {
                m_battleUid = Db.BattleDatabase.Battles[0].Uid;
            }

            battlesList.Clear();
            battlesList.OnDelete -= OnDeleteBattle;
            battlesList.OnDelete += OnDeleteBattle;

            for (var i = 0; i < Db.BattleDatabase.Battles.Count; i++)
            {
                var battle = Db.BattleDatabase.Battles[i];

                var battleItem = Instantiate(battleTextItemPrefab, battlesList.contentLayout, false);
                battleItem.mainButton.content = battle.Uid;
                battleItem.mainButton.OnValueChanged -= OnBattleItemChanged;
                battleItem.mainButton.OnValueChanged += OnBattleItemChanged;
                battleItem.mainButton.Toggle.SetIsOnWithoutNotify( m_battleUid == battle.Uid);

                var battleDisplayName = battle.GetDatabaseDisplayName(out var invalid);

                if (invalid)
                {
                    battleItem.textLabels[0].fontStyle = FontStyle.Italic;
                }
                else
                {
                    battleItem.textLabels[0].fontStyle = FontStyle.Normal;
                }

                battleItem.textLabels[0].text = battleDisplayName;
            }

            addButton.onClick.RemoveAllListeners();
            addButton.onClick.AddListener(OnAddBattleButton);

            moveUpButton.onClick.RemoveAllListeners();
            moveUpButton.onClick.AddListener(OnMoveBattleUpButton);

            moveDownButton.onClick.RemoveAllListeners();
            moveDownButton.onClick.AddListener(OnMoveBattleDownButton);

            battleDataEditor.RefreshView(m_battleUid);

            RefreshEffectList();
        }

        private void RefreshEffectList()
        {
            battleEffectsLayout.Clear();
            battleEffectsLayout.OnDelete -= OnDeleteEffect;
            battleEffectsLayout.OnDelete += OnDeleteEffect;

            Db.BattleDatabase.VfxData ??= new List<GameVfxData>();

            if (!string.IsNullOrEmpty(m_lastEffectSelected) &&
                !Db.BattleDatabase.VfxData.Exists( vfx => vfx.Uid == m_lastEffectSelected))
            {
                m_lastEffectSelected = string.Empty;
            }

            for (var i = 0; i < Db.BattleDatabase.VfxData.Count; i++)
            {
                var vfxItem = Db.BattleDatabase.VfxData[i];

                if (string.IsNullOrEmpty(m_lastEffectSelected) && i == 0)
                {
                    m_lastEffectSelected = vfxItem.Uid;
                }

                var cutsceneItem = Instantiate(battleEffectItemPrefab, battleEffectsLayout.contentLayout, false);
                cutsceneItem.mainButton.content = vfxItem.Uid;
                cutsceneItem.textLabels[0].text = $"{vfxItem.vfxName}";
                cutsceneItem.mainButton.OnValueChanged -= OnVfxItemClicked;
                cutsceneItem.mainButton.OnValueChanged += OnVfxItemClicked;
                cutsceneItem.mainButton.Toggle.SetIsOnWithoutNotify( vfxItem.Uid == m_lastEffectSelected );
                battleEffectsLayout.AddItem(cutsceneItem);
            }

            addEffectButton.onClick.RemoveAllListeners();
            addEffectButton.onClick.AddListener(OnAddEffect);

            battleEffectEditor.Init(m_lastEffectSelected);
        }

        private void OnVfxItemClicked(bool isOn, string context)
        {
            if (isOn)
            {
                m_lastEffectSelected = context;
                battleEffectEditor.Init(m_lastEffectSelected);
            }
        }

        private void OnAddEffect()
        {
            var vfx = new GameVfxData { vfxName = "New Vfx" };
            vfx.UpdateIdentifier();
            Db.BattleDatabase.VfxData.Add(vfx);
            RefreshEffectList();
        }

        private void OnDeleteEffect(LayoutItem layoutItem)
        {
            var vfx = Db.BattleDatabase.GetBattleVfx(layoutItem.mainButton.content);

            if (vfx != null)
            {
                Db.BattleDatabase.VfxData.Remove(vfx);
                RefreshEffectList();
            }
        }

        private void OnBattleItemChanged(bool isOn, string context)
        {
            if (isOn)
            {
                Open(context);
            }
        }

        private void OnAddBattleButton()
        {
            var battle = Db.BattleDatabase.GetNewBattleData();
            Open(battle.Uid);
        }

        private void OnMoveBattleUpButton()
        {
            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            var index = Db.BattleDatabase.Battles.IndexOf(battleData);

            if (index > 0)
            {
                var temp = Db.BattleDatabase.Battles[index - 1];
                Db.BattleDatabase.Battles[index - 1] = battleData;
                Db.BattleDatabase.Battles[index] = temp;
                Open(m_battleUid);
            }
        }

        private void OnMoveBattleDownButton()
        {
            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            var index = Db.BattleDatabase.Battles.IndexOf(battleData);

            if (index >= 0 && index < Db.BattleDatabase.Battles.Count-1)
            {
                var temp = Db.BattleDatabase.Battles[index + 1];
                Db.BattleDatabase.Battles[index + 1] = battleData;
                Db.BattleDatabase.Battles[index] = temp;
                Open(m_battleUid);
            }
        }

        private void OnDeleteBattle(LayoutItem item)
        {
            D90Tools.DisplayConfirmation("Are you sure you want to delete this battle?", OnBattleDeleted);
        }

        private void OnBattleDeleted(bool delete)
        {
            if (delete)
            {
                Db.BattleDatabase.DeleteBattle(m_battleUid);
                Open(string.Empty);
            }
        }
    }
}