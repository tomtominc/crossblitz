using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Data;
using CrossBlitz.UI.Widgets;
using GameDataEditor;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90BattleDataEditor : MonoBehaviour
    {
        [BoxGroup("Battles")] public InputField battleId;
        [BoxGroup("Battles")] public InputField battleDisplayName;
        [BoxGroup("Battles")] public D90DatabaseDropdown enemyCardId;
        [BoxGroup("Battles")] public Dropdown battleType;
        [BoxGroup("Battles")] public InputField enemyName;
        [BoxGroup("Battles")] public Dropdown enemyFaction;
        [BoxGroup("Battles")] public InputField enemyLevel;
        [BoxGroup("Battles")] public InputField enemyHealth;
        [BoxGroup("Battles")] public D90DatabaseDropdown enemyDeckArchetype;
        [BoxGroup("Battles")] public Dropdown enemyStrategyDropdown;
        [BoxGroup("Battles")] public Dropdown rewardTier;
        [BoxGroup("Battles")] public Dropdown weather;
        [BoxGroup("Battles")] public D90DatabaseDropdown recommendedDeck;
        [BoxGroup("Battles/Deck")] public D90DeckInspector deckInspector;
        [BoxGroup("Battles/Deck")] public D90DeckInspector excludedCards;
        [BoxGroup("Battles/Deck")] public D90DeckInspector prizeCards;
        [BoxGroup("Battles/Deck")] public Dropdown cardBack;
        [BoxGroup("Battles/Cutscene")] public D90DatabaseDropdown matchStartCutsceneDropdown;
        [BoxGroup("Battles/Cutscene")] public D90DatabaseDropdown matchEndCutsceneDropdown;
        [BoxGroup("Battles/Accolades")] public D90ContentLayout accoladesContentLayout;
        [BoxGroup("Battles/Accolades")] public LayoutItem accoladeTextPrefab;
        [BoxGroup("Battles/Accolades")] public Button addAccoladeButton;
        [BoxGroup("Battles/Accolades")] public Button deleteAccoladeButton;
        [BoxGroup("Battles/Accolades")] public D90AccoladeInspector accoladeInspector;
        [BoxGroup("Battles/Audio")] public D90DatabaseDropdown musicDatabase;
        [BoxGroup("Battles/Custom")] public D90CustomBattleOptionsEditor customBattleOptions;
        [BoxGroup("Battles/Misc")] public Button saveButton;
        [BoxGroup("Battles/Misc")] public Button battleButton;
        [BoxGroup("Battles/Misc")] public Dropdown todoStatusDropdown;
        [BoxGroup("Battles/Events")] public D90ContentLayout battleEventContentLayout;
        [BoxGroup("Battles/Events")] public LayoutItem battleEventItemPrefab;
        [BoxGroup("Battles/Events")] public Button battleEventAddButton;
        [BoxGroup("Battles/Events")] public D90BattleEventEditor battleEventEditor;

        private string m_battleUid;
        private string m_accoladeUid;

        public void RefreshView(string battleUid)
        {
            m_battleUid = battleUid;

            if (string.IsNullOrEmpty(m_battleUid))
            {
                m_battleUid = Db.BattleDatabase.Battles[0].Uid;
            }

            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            battleId.SetTextWithoutNotify(battleData.Uid);

            battleDisplayName.SetTextWithoutNotify(battleData.BattleName);
            battleDisplayName.onEndEdit.RemoveAllListeners();
            battleDisplayName.onEndEdit.AddListener(OnBattleDisplayNameChanged);

            enemyCardId.Init();
            enemyCardId.SetDropdownWithoutNotify(Db.CardDatabase.GetCard(battleData.CardId)?.name);
            enemyCardId.OnValueChanged -= OnEnemyCardIdChanged;
            enemyCardId.OnValueChanged += OnEnemyCardIdChanged;

            battleType.ClearOptions();
            battleType.AddOptions(Enum.GetNames(typeof(BattleType)).ToList());
            battleType.SetValueWithoutNotify((int)battleData.BattleType);
            battleType.onValueChanged.RemoveAllListeners();
            battleType.onValueChanged.AddListener(OnBattleTypeChanged);

            enemyName.SetTextWithoutNotify(battleData.Name);
            enemyName.onEndEdit.RemoveAllListeners();
            enemyName.onEndEdit.AddListener(OnEnemyNameChanged);

            enemyFaction.ClearOptions();
            enemyFaction.AddOptions(Enum.GetNames(typeof(Faction)).ToList());
            enemyFaction.SetValueWithoutNotify((int)battleData.Faction);
            enemyFaction.onValueChanged.RemoveAllListeners();
            enemyFaction.onValueChanged.AddListener(OnEnemyFactionChanged);

            enemyLevel.SetTextWithoutNotify(battleData.Level.ToString());
            enemyLevel.onEndEdit.RemoveAllListeners();
            enemyLevel.onEndEdit.AddListener(OnEnemyLevelChanged);

            enemyHealth.SetTextWithoutNotify(battleData.Health.ToString());
            enemyHealth.onEndEdit.RemoveAllListeners();
            enemyHealth.onEndEdit.AddListener(OnEnemyHealthValueChanged);

            enemyDeckArchetype.Init();
            enemyDeckArchetype.SetDropdownWithoutNotify( battleData.DeckArchetype );
            enemyDeckArchetype.OnValueChanged -= OnArchetypeChanged;
            enemyDeckArchetype.OnValueChanged += OnArchetypeChanged;

            enemyStrategyDropdown.ClearOptions();
            enemyStrategyDropdown.AddOptions(Enum.GetNames(typeof(AIStrategy)).ToList());
            enemyStrategyDropdown.SetValueWithoutNotify((int)battleData.Strategy);
            enemyStrategyDropdown.onValueChanged.RemoveAllListeners();
            enemyStrategyDropdown.onValueChanged.AddListener(OnStrategyChanged);

            battleData.Deck ??= new DeckData();
            battleData.Deck.cards ??= new List<CardValue>();

            deckInspector.OnCardsChanged -= OnCardsChanged;
            deckInspector.OnCardsChanged += OnCardsChanged;
            deckInspector.SetupCards( battleData.Deck.cards );

            battleData.PrizeCards ??= new List<string>();

            prizeCards.OnCardsChanged -= OnPrizeCardsChanged;
            prizeCards.OnCardsChanged += OnPrizeCardsChanged;
            prizeCards.SetupCards( battleData.PrizeCards.Select( c => new CardValue { id = c, count = 1 }).ToList() );

            rewardTier.onValueChanged.RemoveAllListeners();
            rewardTier.onValueChanged.AddListener(OnRewardTierChanged);
            rewardTier.ClearOptions();
            rewardTier.AddOptions(Enum.GetNames(typeof(BattleData.RewardTierType)).ToList() );
            rewardTier.SetValueWithoutNotify( (int)battleData.RewardTier);

            recommendedDeck.Init();
            recommendedDeck.SetDropdownWithoutNotify(Db.DeckRecipeDatabase.GetRecipe(battleData.RecommendedDeck)?.deckData?.name);
            recommendedDeck.OnValueChanged -= OnRecommendedDeckChanged;
            recommendedDeck.OnValueChanged += OnRecommendedDeckChanged;

            todoStatusDropdown.onValueChanged.RemoveAllListeners();
            todoStatusDropdown.onValueChanged.AddListener(TodoStatusChanged);
            todoStatusDropdown.ClearOptions();
            todoStatusDropdown.AddOptions( Enum.GetNames(typeof(TodoStatus)).ToList());

            // battleData.ExcludedCards ??= new List<string>();
            //
            // rewardPoolInspector.OnCardsChanged -= OnRewardPoolChanged;
            // rewardPoolInspector.OnCardsChanged += OnRewardPoolChanged;
            // rewardPoolInspector.SetupCards( battleData.ExcludedCards.Select( c => new CardValue { id = c, count = 1}).ToList());

            if (string.IsNullOrEmpty(m_accoladeUid) && battleData.Accolades.Count > 0)
            {
                m_accoladeUid = battleData.Accolades[0].Uid;
            }

            accoladesContentLayout.Clear();
            accoladesContentLayout.OnDelete -= OnDeleteAccolade;
            accoladesContentLayout.OnDelete += OnDeleteAccolade;

            for (var i = 0; i < battleData.Accolades.Count; i++)
            {
                var accolade = battleData.Accolades[i];
                var accoladeItem = Instantiate(accoladeTextPrefab, accoladesContentLayout.contentLayout, false);
                accoladeItem.mainButton.content = accolade.Uid;
                accoladeItem.mainButton.OnValueChanged -= OnAccoladeChanged;
                accoladeItem.mainButton.OnValueChanged += OnAccoladeChanged;
                accoladeItem.textLabels[0].text = $"{accolade.Type} ({accolade.Score}) ${accolade.DawnDollarReward:N0} ";
                accoladeItem.mainButton.Toggle.SetIsOnWithoutNotify( m_accoladeUid == accolade.Uid );

                if (accolade.ItemRewards != null && accolade.ItemRewards.Count > 0)
                {
                    for (var j = 0; j < accolade.ItemRewards.Count; j++)
                    {
                        accoladeItem.textLabels[0].text +=
                            $"{accolade.ItemRewards[j].ItemUid} x{accolade.ItemRewards[j].Count}, ";
                    }
                }

                accoladesContentLayout.AddItem(accoladeItem);
            }

            accoladeInspector.OnBattleChanged -= OnBattleChanged;
            accoladeInspector.OnBattleChanged += OnBattleChanged;
            accoladeInspector.SetAccolade(m_battleUid, m_accoladeUid);

            addAccoladeButton.onClick.RemoveAllListeners();
            addAccoladeButton.onClick.AddListener(OnAddAccolade);

            if (battleData.CustomBattleOptions == null)
            {
                battleData.CustomBattleOptions = new CustomBattleOptions
                {
                    PlayerDeckOverride = new CustomDeckOverride(),
                    OpponentDeckOverride = new CustomDeckOverride(),
                    PlayerAdditionalCards = new CustomDeckOverride(),
                    OpponentAdditionalCards = new CustomDeckOverride(),
                    RemoveCardsFromPlayersDeck = new List<string>(),
                    ForcedPrizeCards = new List<string>(),
                    GameBoardOptions = new CustomGameBoardOptions
                    {
                        Board = new List<CustomGameCardState>()
                    }
                };
            }

            battleData.CustomBattleOptions.ForcedPrizeCards ??= new List<string>();
            battleData.CustomBattleOptions.RemoveCardsFromPlayersDeck ??= new List<string>();
            battleData.CustomBattleOptions.PlayerAdditionalCards ??= new CustomDeckOverride();
            battleData.CustomBattleOptions.OpponentAdditionalCards ??= new CustomDeckOverride();

            customBattleOptions.OnOptionsChanged -= OnOptionsChanged;
            customBattleOptions.OnOptionsChanged += OnOptionsChanged;
            customBattleOptions.RefreshData( battleData.CustomBattleOptions );

            excludedCards.OnCardsChanged -= OnExcludedCardsChanged;
            excludedCards.OnCardsChanged += OnExcludedCardsChanged;
            excludedCards.SetupCards( battleData.CustomBattleOptions.RemoveCardsFromPlayersDeck.Select( c => new CardValue { id = c, count = 1 }).ToList() );

            musicDatabase.Init();
            musicDatabase.OnValueChanged -= OnMusicValueChanged;
            musicDatabase.OnValueChanged += OnMusicValueChanged;
            musicDatabase.SetDropdownWithoutNotify( battleData.Music );

            var cardBackOptions = Db.CardBackDatabase.cardBacks.Select( cb => cb.portraitUrl).ToList();
            cardBack.ClearOptions();
            cardBack.AddOptions(cardBackOptions);
            cardBack.SetValueWithoutNotify(cardBackOptions.IndexOf(Db.CardBackDatabase.GetCardBackData( battleData.Deck.cardBackUid ).portraitUrl ));
            cardBack.onValueChanged.RemoveAllListeners();
            cardBack.onValueChanged.AddListener(OnCardBackChanged);


            weather.ClearOptions();
            weather.AddOptions( Enum.GetNames(typeof(FableRoomData.RoomWeather)).ToList() );
            weather.onValueChanged.RemoveAllListeners();
            weather.onValueChanged.AddListener(OnWeatherChanged);
            weather.SetValueWithoutNotify( (int)battleData.Weather );

            saveButton.onClick.RemoveAllListeners();
            saveButton.onClick.AddListener(OnSave);

            battleButton.onClick.RemoveAllListeners();
            battleButton.onClick.AddListener(OnBattle);

            battleEventContentLayout.Clear();

            battleData.BattleEvents ??= new List<BattleEvent>();

            if (!string.IsNullOrEmpty(m_lastBattleEventSelected) &&
                !battleData.BattleEvents.Exists(battleEvent => battleEvent.Uid == m_lastBattleEventSelected))
            {
                m_lastBattleEventSelected = string.Empty;
            }

            for (var i = 0; i < battleData.BattleEvents.Count; i++)
            {
                var battleEvent = battleData.BattleEvents[i];

                if (string.IsNullOrEmpty(m_lastBattleEventSelected) && i == 0)
                {
                    m_lastBattleEventSelected = battleEvent.Uid;
                }

                var battleEventItem = Instantiate(battleEventItemPrefab, battleEventContentLayout.contentLayout, false);
                battleEventItem.mainButton.content = battleEvent.Uid;
                battleEventItem.textLabels[0].text = $"{battleEvent.displayName}";
                battleEventItem.mainButton.OnValueChanged -= OnBattleEventChanged;
                battleEventItem.mainButton.OnValueChanged += OnBattleEventChanged;
                battleEventItem.mainButton.Toggle.SetIsOnWithoutNotify( battleEvent.Uid == m_lastBattleEventSelected );
                battleEventContentLayout.AddItem(battleEventItem);
            }

            battleEventContentLayout.OnDelete -= OnDeleteBattleEvent;
            battleEventContentLayout.OnDelete += OnDeleteBattleEvent;

            battleEventAddButton.onClick.RemoveAllListeners();
            battleEventAddButton.onClick.AddListener(OnAddBattleEvent);

            if (!string.IsNullOrEmpty(m_lastBattleEventSelected))
            {
                battleEventEditor.SetActive(true);

                var battleEvent = battleData.BattleEvents.Find(e => e.Uid == m_lastBattleEventSelected);

                if (battleEvent != null)
                {
                    battleEventEditor.Init(m_battleUid, battleData.BattleEvents.IndexOf(battleEvent));
                }
            }
            else
            {
                battleEventEditor.SetActive(false);
            }

        }

        private void OnBattleChanged(string battleUid)
        {
            RefreshView(battleUid);
        }

        private void TodoStatusChanged(int index)
        {
            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);
            if (battle == null)
            {
                return;
            }

            var status = (TodoStatus)index;
            battle.TodoStatus = status;
        }

        private void OnAddAccolade()
        {
            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);
            if (battle == null)
            {
                return;
            }

            var newAccolade = battle.CreateNewAccolade();
            m_accoladeUid = newAccolade.Uid;

            RefreshView(m_battleUid);
        }

        private void OnDeleteAccolade(LayoutItem item)
        {
            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);

            if (battle == null)
            {
                return;
            }

            var accolade = battle.GetAccolade(item.mainButton.content);

            if (accolade == null)
            {
                return;
            }

            battle.Accolades.Remove(accolade);
            m_accoladeUid = string.Empty;

            RefreshView(m_battleUid);
        }

        private void OnAccoladeChanged(bool isOn, string context)
        {
            if (isOn)
            {
                m_accoladeUid = context;
                RefreshView(m_battleUid);
            }
        }

        private string m_lastBattleEventSelected;

        private void OnBattleEventChanged(bool isOn, string context)
        {
            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);
            if (battle == null)
            {
                return;
            }

            m_lastBattleEventSelected = context;

            var battleEvent = battle.BattleEvents.Find(e => e.Uid == m_lastBattleEventSelected);

            if (battleEvent != null)
            {
                var index = battle.BattleEvents.IndexOf(battleEvent);
                battleEventEditor.Init(m_battleUid, index);
            }
        }

        private void OnDeleteBattleEvent(LayoutItem item)
        {
            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);
            if (battle == null)
            {
                return;
            }
            var battleEvent = battle.BattleEvents.Find(e => e.Uid == item.mainButton.content);

            if (battleEvent != null)
            {
                battle.BattleEvents.Remove(battleEvent);
                RefreshView(m_battleUid);
            }
        }

        private void OnAddBattleEvent()
        {
            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);
            if (battle == null)
            {
                return;
            }

            var battleEvent = new BattleEvent
            {
                displayName = "New Battle Event",
            };
            battleEvent.UpdateIdentifier();
            battle.BattleEvents.Add(battleEvent);

            RefreshView(m_battleUid);
        }

        private void OnWeatherChanged(int index)
        {
            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            battleData.Weather = (FableRoomData.RoomWeather) index;
        }

        private void OnRewardTierChanged(int index)
        {
            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            battleData.RewardTier = (BattleData.RewardTierType) index;
        }

        private void OnBattle()
        {
            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            TerminalCommands.StartBattle(battleData.Uid);
        }

        private void OnRewardPoolChanged(List<CardValue> cardValues)
        {
            // var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            // battleData.ExcludedCards = cardValues.Select(v => v.id).ToList();
        }

        private void OnCardBackChanged(int index)
        {
            var cardBackKey = cardBack.options[index].text;
            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            battleData.Deck.cardBackUid = Db.CardBackDatabase.GetCardBackByPortraitUrl(cardBackKey).Uid;
        }

        private void OnMusicValueChanged(D90DatabaseDropdown dropdown, int value)
        {
            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            battleData.Music = dropdown.dropdown.options[value].text;
        }

        private void OnOptionsChanged(CustomBattleOptions battleOptions)
        {
            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            battleData.CustomBattleOptions = battleOptions;
        }

        private void OnBattleDisplayNameChanged(string newName)
        {
            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            battleData.BattleName = newName;
        }

        private void OnEnemyCardIdChanged(D90DatabaseDropdown dropdown, int value)
        {
            var cardName = dropdown.dropdown.options[value].text;
            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            battleData.CardId = Db.CardDatabase.GetCardIdFromName(cardName);
            battleData.Deck.hero = battleData.CardId;
        }

        private void OnArchetypeChanged(D90DatabaseDropdown dropdown, int value)
        {
            var deckRecipeName = dropdown.dropdown.options[value].text;
            var deckRecipe = Db.DeckRecipeDatabase.GetRecipeByName(deckRecipeName);
            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            battleData.DeckArchetype = deckRecipe.Uid;
        }

        private void OnRecommendedDeckChanged(D90DatabaseDropdown dropdown, int value)
        {
            var deckName = dropdown.dropdown.options[value].text;
            var deckRecipe = Db.DeckRecipeDatabase.GetRecipeByName(deckName);
            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            battleData.RecommendedDeck = deckRecipe.Uid;
        }

        private void OnBattleTypeChanged(int value)
        {
            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            battleData.BattleType = (BattleType) value;
        }

        private void OnStrategyChanged(int value)
        {
            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            battleData.Strategy = (AIStrategy)value;
        }

        private void OnEnemyNameChanged(string newName)
        {
            var battleData =  Db.BattleDatabase.GetBattleData(m_battleUid);
            battleData.Name = newName;
        }

        private void OnEnemyFactionChanged(int value)
        {
            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            battleData.Faction = (Faction) value;
        }

        private void OnEnemyLevelChanged(string newLevel)
        {
            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            battleData.Level = int.Parse(newLevel);
        }

        private void OnEnemyHealthValueChanged(string newHealth)
        {
            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            battleData.Health = int.Parse(newHealth);
        }

        private void OnPrizeCardsChanged(List<CardValue> cards)
        {
            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            battleData.PrizeCards = cards.Select( c => c.id).ToList();
        }

        private void OnExcludedCardsChanged(List<CardValue> cards)
        {
            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            battleData.CustomBattleOptions.RemoveCardsFromPlayersDeck = cards.Select( c => c.id).ToList();
        }

        private void OnCardsChanged(List<CardValue> cards)
        {
            var battleData = Db.BattleDatabase.GetBattleData(m_battleUid);
            battleData.Deck.cards = cards;
        }

        private void OnSave()
        {
            Db.BattleDatabase.Save();
        }
    }
}