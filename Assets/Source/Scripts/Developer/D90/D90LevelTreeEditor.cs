using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.UI.Widgets;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90LevelTreeEditor : MonoBehaviour
    {
        public GameObject disabledOverlay;
        public D90DatabaseDropdown recipe;
        public D90ContentLayout levelDataLayout;
        public LayoutItem levelRewardItemPrefab;

        public InputField readOnlyId;
        public Dropdown rewardType;
        public D90DatabaseDropdown cardDropdown;
        public InputField healthPointsIncrease;
        public InputField manaShardsIncrease;
        public D90DatabaseDropdown ingredientDropdown;
        public InputField levelReport;

        public Button moveUpButton;
        public Button moveDownButton;
        public Button addButton;
        public Button deleteButton;
        public Button resetButton;

        private string m_heroId;
        private int m_rewardId;
        private int m_index;

        public void Init(string heroId, int index)
        {
            if (string.IsNullOrEmpty(heroId))
            {
                disabledOverlay.SetActive(true);
                return;
            }

            m_heroId = heroId;
            m_index = index;

            var hero = Db.HeroDatabase.GetHero(m_heroId);

            if (hero == null)
            {
                disabledOverlay.SetActive(true);
                return;
            }

            if (hero.levelTrees.Count <= m_index)
            {
                hero.levelTrees.Add(new LevelTree { levels =  new List<LevelReward>() });
            }

            var levelTree = hero.levelTrees[m_index];

            if (m_rewardId == 0 && levelTree.levels.Count > 0)
            {
                m_rewardId = levelTree.levels[0].uid;
            }

            levelDataLayout.Clear();

            var recipeData = Db.DeckRecipeDatabase.GetRecipe(levelTree.archetype);

            recipe.Init();
            recipe.SetDropdownWithoutNotify( recipeData != null ? recipeData.deckData.name :string.Empty);
            recipe.OnValueChanged += OnRecipeChanged;

            for (var i = 0; i < levelTree.levels.Count; i++)
            {
                var reward = levelTree.levels[i];
                AddLevelTreeItemToLayout(reward, i);
            }

            if (m_rewardId != 0)
            {
                SetRewardLayout(m_rewardId);
            }

            if (moveUpButton)
            {
                moveUpButton.onClick.RemoveAllListeners();
                moveUpButton.onClick.AddListener(OnMoveUp);
            }

            if (moveDownButton)
            {
                moveDownButton.onClick.RemoveAllListeners();
                moveDownButton.onClick.AddListener(OnMoveDown);
            }

            if (addButton)
            {
                addButton.onClick.RemoveAllListeners();
                addButton.onClick.AddListener(OnAddButton);
            }

            if (deleteButton)
            {
                deleteButton.onClick.RemoveAllListeners();
                deleteButton.onClick.AddListener(OnDeleteButton);
            }

            if (resetButton)
            {
                resetButton.onClick.RemoveAllListeners();
                resetButton.onClick.AddListener(ResetLevelTree);
            }

            disabledOverlay.SetActive(false);
        }

        private void OnRecipeChanged(D90DatabaseDropdown dropdown, int index)
        {
            var hero = Db.HeroDatabase.GetHero(m_heroId);

            if (hero == null)
            {
                return;
            }

            var rec = Db.DeckRecipeDatabase.GetRecipeByName(dropdown.dropdown.options[index].text);
            hero.levelTrees[m_index].archetype = rec.Uid;

            Init(m_heroId,m_index);
        }

        private void OnAddButton()
        {
            var hero = Db.HeroDatabase.GetHero(m_heroId);

            if (hero == null)
            {
                return;
            }

            var newReward = hero.GetRandomNewPrize();
            m_rewardId = newReward.uid;
            hero.levelTrees[m_index].levels.Add(newReward);
            Init(m_heroId, m_index);
        }

        private void OnDeleteButton()
        {
            var hero = Db.HeroDatabase.GetHero(m_heroId);

            if (hero == null)
            {
                return;
            }

            var reward = hero.levelTrees[m_index].levels.Find(r => r.uid == m_rewardId);

            if (reward == null)
            {
                return;
            }

            hero.levelTrees[m_index].levels.Remove(reward);
            m_rewardId = 0;
            Init(m_heroId,m_index);
        }

        private void OnMoveUp()
        {
            var hero = Db.HeroDatabase.GetHero(m_heroId);

            if (hero == null)
            {
                return;
            }

            var reward = hero.levelTrees[m_index].levels.Find(r => r.uid == m_rewardId);

            if (reward == null)
            {
                return;
            }

            var index = hero.levelTrees[m_index].levels.IndexOf(reward);

            if (index > 0)
            {
                (hero.levelTrees[m_index].levels[index - 1], hero.levelTrees[m_index].levels[index]) =
                    (hero.levelTrees[m_index].levels[index], hero.levelTrees[m_index].levels[index - 1]);

                Init(m_heroId, m_index);
            }
        }

        private void OnMoveDown()
        {
            var hero = Db.HeroDatabase.GetHero(m_heroId);

            if (hero == null)
            {
                return;
            }

            var reward = hero.levelTrees[m_index].levels.Find(r => r.uid == m_rewardId);

            if (reward == null)
            {
                return;
            }

            var index = hero.levelTrees[m_index].levels.IndexOf(reward);

            if (index >= 0 && index < hero.levelTrees[m_index].levels.Count-1)
            {
                (hero.levelTrees[m_index].levels[index + 1], hero.levelTrees[m_index].levels[index]) =
                    (hero.levelTrees[m_index].levels[index], hero.levelTrees[m_index].levels[index + 1]);

                Init(m_heroId,m_index);
            }
        }

        private void ResetLevelTree()
        {
            var hero = Db.HeroDatabase.GetHero(m_heroId);

            if (hero == null)
            {
                return;
            }

            for (var i = 0; i < hero.levelTrees[m_index].levels.Count; i++)
            {
                var level = hero.levelTrees[m_index].levels[i];

                switch (i)
                {
                    case 0:
                        level.type = LevelReward.EType.HeathUp;
                        level.healthPoints = 2;
                        level.cardId = string.Empty;
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 1:
                        level.type = LevelReward.EType.Card;
                        level.healthPoints = 0;
                        //level.cardId = string.Empty; *don't reset the cards*
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 2:
                        level.type = LevelReward.EType.Card;
                        level.healthPoints = 0;
                        //level.cardId = string.Empty; *don't reset the cards*
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 3:
                        level.type = LevelReward.EType.Card;
                        level.healthPoints = 0;
                        //level.cardId = string.Empty; *don't reset the cards*
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 4:
                        level.type = LevelReward.EType.HeathUp;
                        level.healthPoints = 2;
                        level.cardId = string.Empty;
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 5:
                        level.type = LevelReward.EType.Card;
                        level.healthPoints = 0;
                        //level.cardId = string.Empty; *don't reset the cards*
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 6:
                        level.type = LevelReward.EType.Card;
                        level.healthPoints = 0;
                        //level.cardId = string.Empty; *don't reset the cards*
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 7:
                        level.type = LevelReward.EType.HeathUp;
                        level.healthPoints = 2;
                        level.cardId = string.Empty;
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 8:
                        level.type = LevelReward.EType.Card;
                        level.healthPoints = 0;
                        //level.cardId = string.Empty; *don't reset the cards*
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 9:
                        level.type = LevelReward.EType.Card;
                        level.healthPoints = 0;
                        //level.cardId = string.Empty; *don't reset the cards*
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 10:
                        level.type = LevelReward.EType.Card;
                        level.healthPoints = 0;
                        //level.cardId = string.Empty; *don't reset the cards*
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 11:
                        level.type = LevelReward.EType.ManaShards;
                        level.healthPoints = 0;
                        level.cardId = string.Empty;
                        level.ingredient = string.Empty;
                        level.manaShards = 5;
                        break;
                    case 12:
                        level.type = LevelReward.EType.Card;
                        level.healthPoints = 0;
                        //level.cardId = string.Empty; *don't reset the cards*
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 13:
                        level.type = LevelReward.EType.HeathUp;
                        level.healthPoints = 2;
                        level.cardId = string.Empty;
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 14:
                        level.type = LevelReward.EType.Card;
                        level.healthPoints = 0;
                        //level.cardId = string.Empty; *don't reset the cards*
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 15:
                        level.type = LevelReward.EType.Card;
                        level.healthPoints = 0;
                        //level.cardId = string.Empty; *don't reset the cards*
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 16:
                        level.type = LevelReward.EType.Card;
                        level.healthPoints = 0;
                        //level.cardId = string.Empty; *don't reset the cards*
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 17:
                        level.type = LevelReward.EType.Card;
                        level.healthPoints = 0;
                        //level.cardId = string.Empty; *don't reset the cards*
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 18:
                        level.type = LevelReward.EType.HeathUp;
                        level.healthPoints = 2;
                        level.cardId = string.Empty;
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 19:
                        level.type = LevelReward.EType.Card;
                        level.healthPoints = 0;
                        //level.cardId = string.Empty; *don't reset the cards*
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 20:
                        level.type = LevelReward.EType.HeathUp;
                        level.healthPoints = 2;
                        level.cardId = string.Empty;
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 21:
                        level.type = LevelReward.EType.ManaShards;
                        level.healthPoints = 0;
                        level.cardId = string.Empty;
                        level.ingredient = string.Empty;
                        level.manaShards = 5;
                        break;
                    case 22:
                        level.type = LevelReward.EType.HeathUp;
                        level.healthPoints = 2;
                        level.cardId = string.Empty;
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 23:
                        level.type = LevelReward.EType.ManaShards;
                        level.healthPoints = 0;
                        level.cardId = string.Empty;
                        level.ingredient = string.Empty;
                        level.manaShards = 5;
                        break;
                    case 24:
                        level.type = LevelReward.EType.Card;
                        level.healthPoints = 0;
                        //level.cardId = string.Empty; *don't reset the cards*
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 25:
                        level.type = LevelReward.EType.HeathUp;
                        level.healthPoints = 2;
                        level.cardId = string.Empty;
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 26:
                        level.type = LevelReward.EType.Card;
                        level.healthPoints = 0;
                        //level.cardId = string.Empty; *don't reset the cards*
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 27:
                        level.type = LevelReward.EType.Card;
                        level.healthPoints = 0;
                        //level.cardId = string.Empty; *don't reset the cards*
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 28:
                        level.type = LevelReward.EType.HeathUp;
                        level.healthPoints = 2;
                        level.cardId = string.Empty;
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                    case 29:
                        level.type = LevelReward.EType.ManaShards;
                        level.healthPoints = 0;
                        level.cardId = string.Empty;
                        level.ingredient = string.Empty;
                        level.manaShards = 5;
                        break;
                    case 30:
                        level.type = LevelReward.EType.HeathUp;
                        level.healthPoints = 2;
                        level.cardId = string.Empty;
                        level.ingredient = string.Empty;
                        level.manaShards = 0;
                        break;
                }


            }
        }

        public void SetRewardLayout(int rewardId)
        {
            m_rewardId = rewardId;

            var hero = Db.HeroDatabase.GetHero(m_heroId);

            if (hero == null)
            {
                return;
            }

            var reward = hero.levelTrees[m_index].levels.Find(r => r.uid == m_rewardId);

            if (reward == null)
            {
                return;
            }

            readOnlyId.text = reward.uid.ToString();

            rewardType.ClearOptions();
            rewardType.AddOptions( System.Enum.GetNames( typeof( LevelReward.EType ) ).ToList() );
            rewardType.onValueChanged.RemoveAllListeners();
            rewardType.onValueChanged.AddListener(OnRewardTypeChanged);
            rewardType.SetValueWithoutNotify((int)reward.type);

            cardDropdown.SetActive(false);
            healthPointsIncrease.SetActive(false);
            manaShardsIncrease.SetActive(false);
            ingredientDropdown.SetActive(false);

            switch (reward.type)
            {
                case LevelReward.EType.Card:
                    cardDropdown.SetActive(true);
                    break;
                case LevelReward.EType.HeathUp:
                    healthPointsIncrease.SetActive(true);
                    break;
                case LevelReward.EType.IngredientPouch:
                    ingredientDropdown.SetActive(true);
                    break;
                case LevelReward.EType.ManaShards:
                    manaShardsIncrease.SetActive(true);
                    break;
            }

            cardDropdown.OnValueChanged -= OnCardRewardChanged;
            cardDropdown.OnValueChanged += OnCardRewardChanged;
            if (!string.IsNullOrEmpty(reward.cardId))
            {
                var card = Db.CardDatabase.GetCard(reward.cardId);
                if (card != null) cardDropdown.SetDropdownWithoutNotify(card.name);
            }
            else
            {
                cardDropdown.SetDropdownWithoutNotify(string.Empty);
            }

            healthPointsIncrease.onValueChanged.RemoveAllListeners();
            healthPointsIncrease.onValueChanged.AddListener(OnHealthPointsChanged);
            healthPointsIncrease.SetTextWithoutNotify( reward.healthPoints.ToString() );

            manaShardsIncrease.onValueChanged.RemoveAllListeners();
            manaShardsIncrease.onValueChanged.AddListener(OnManaShardsChanged);
            manaShardsIncrease.SetTextWithoutNotify( reward.manaShards.ToString() );

            ingredientDropdown.OnValueChanged -= OnIngredientChanged;
            ingredientDropdown.OnValueChanged += OnIngredientChanged;
            if (!string.IsNullOrEmpty(reward.ingredient))
            {
                var item = Db.ItemDatabase.GetItem(reward.ingredient);
                if (item != null) ingredientDropdown.SetDropdownWithoutNotify(item.DisplayName);
            }
            else
            {
                ingredientDropdown.SetDropdownWithoutNotify(string.Empty);
            }

            levelReport.text = hero.levelTrees[m_index].GetLevelReport();
            levelReport.RectTransform().sizeDelta = new Vector2(levelReport.RectTransform().sizeDelta.x, 240);
        }

        private void OnHealthPointsChanged(string value)
        {
            if (int.TryParse(value, out var hp))
            {
                var hero = Db.HeroDatabase.GetHero(m_heroId);

                if (hero == null)
                {
                    return;
                }

                var reward = hero.levelTrees[m_index].levels.Find(r => r.uid == m_rewardId);

                if (reward == null)
                {
                    return;
                }

                reward.healthPoints = hp;
            }
        }

        private void OnManaShardsChanged(string value)
        {
            if (int.TryParse(value, out var ms))
            {
                var hero = Db.HeroDatabase.GetHero(m_heroId);

                if (hero == null)
                {
                    return;
                }

                var reward = hero.levelTrees[m_index].levels.Find(r => r.uid == m_rewardId);

                if (reward == null)
                {
                    return;
                }

                reward.manaShards = ms;
            }
        }

        private void OnIngredientChanged(D90DatabaseDropdown dropdown, int index)
        {
            var hero = Db.HeroDatabase.GetHero(m_heroId);

            if (hero == null)
            {
                return;
            }

            var reward = hero.levelTrees[m_index].levels.Find(r => r.uid == m_rewardId);

            if (reward == null)
            {
                return;
            }

            var ingredient = Db.ItemDatabase.GetItemByName(dropdown.dropdown.options[index].text);

            if (ingredient != null)
            {
                reward.ingredient = ingredient.ItemId;
            }
        }

        private void OnRewardTypeChanged(int index)
        {
            var hero = Db.HeroDatabase.GetHero(m_heroId);

            if (hero == null)
            {
                return;
            }

            var reward = hero.levelTrees[m_index].levels.Find(r => r.uid == m_rewardId);

            if (reward == null)
            {
                return;
            }

            reward.type = (LevelReward.EType)index;

            Init(m_heroId,m_index);
            //SetRewardLayout(m_rewardId);
        }

        private void OnCardRewardChanged(D90DatabaseDropdown dropdown, int index)
        {
            var hero = Db.HeroDatabase.GetHero(m_heroId);

            if (hero == null)
            {
                return;
            }

            var reward = hero.levelTrees[m_index].levels.Find(r => r.uid == m_rewardId);

            if (reward == null)
            {
                return;
            }

            var card = Db.CardDatabase.GetCardByName(dropdown.dropdown.options[index].text);

            if (card != null)
            {
                reward.cardId = card.id;
            }

            Init(m_heroId,m_index);
        }

        private void AddLevelTreeItemToLayout(LevelReward reward, int index)
        {
            if (!Db.Initialized)
            {
                Db.Initialize();
            }


            var rewardItem = Instantiate(levelRewardItemPrefab, levelDataLayout.contentLayout, false);
            rewardItem.mainButton.content = reward.uid.ToString();
            rewardItem.mainButton.Toggle.SetIsOnWithoutNotify( m_rewardId == reward.uid );
            rewardItem.mainButton.OnValueChanged -= OnRewardToggled;
            rewardItem.mainButton.OnValueChanged += OnRewardToggled;
            var levelSeen = index < 2 ? 2 : index;
            var content = $"Lv.{levelSeen} ";

            LevelReward selectedReward = null;

            var hero = Db.HeroDatabase.GetHero(m_heroId);

            if (hero != null)
            {
                selectedReward = hero.levelTrees[m_index].levels.Find(r => r.uid == m_rewardId);
            }

            var highlight = selectedReward != null && selectedReward.type == reward.type ? "<b><color=red>" : "<color=black>";
            var prefix = selectedReward != null && selectedReward.type == reward.type ? "</color></b>" : "</color>";

            switch (reward.type)
            {
                case LevelReward.EType.Card:
                    var card = Db.CardDatabase.GetCard(reward.cardId);
                    if (card!=null) content += $"{highlight}Card: {card.name} ({card.rarity}){prefix}";
                    break;
                case LevelReward.EType.HeathUp:
                    content += $"{highlight}Health Up +{reward.healthPoints}{prefix}";
                    break;
                case LevelReward.EType.IngredientPouch:
                    content += $"{highlight}Ingredient: {reward.ingredient}{prefix}";
                    break;
                case LevelReward.EType.ManaShards:
                    content += $"{highlight}Mana Shards +{reward.manaShards}{prefix}";
                    break;
                case LevelReward.EType.RelicSlot:
                    content += $"{highlight}Relic Slot{prefix}";
                    break;
                case LevelReward.EType.ElderRelicSlot:
                    content += $"{highlight}Elder Relic Slot{prefix}";
                    break;
            }

            rewardItem.textLabels[0].text = content;

            levelDataLayout.AddItem(rewardItem);
        }

        private void OnRewardToggled(bool isOn, string context)
        {
            if (isOn)
            {
                m_rewardId = int.Parse(context);
                Init(m_heroId,m_index);
            }
        }
    }
}