using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Data;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90ChapterEditor : MonoBehaviour
    {
        public InputField chapterName;
        public D90DatabaseDropdown chapterIntroCutscene;
        public D90DatabaseDropdown chapterIllustrationCutscene;
        public D90DatabaseDropdown mapDropdown;
        public D90DatabaseDropdown mainQuestDropdown;
        public InputField chapterPrologue;
        public InputField chapterDescription;
        public D90DeckInspector rewardPoolInspector;
        public D90DeckInspector excludedCardsInspector;
        public Button configureRewardPool;
        public Button saveButton;
        public Button printCardsButton;

        private string m_chapterUid;
        //private FableChapterData m_chapter;

        private void OnChapterNameChanged(string newChapterName)
        {
            var chapter = Db.FablesDatabase.GetChapter(m_chapterUid);
            chapter.ChapterName = newChapterName;
        }

        private void OnChapterPrologueChanged(string newPrologue)
        {
            var chapter = Db.FablesDatabase.GetChapter(m_chapterUid);
            chapter.ChapterPrologue = newPrologue;
        }

        private void OnMapChanged(D90DatabaseDropdown dropdown, int value)
        {
            var chapter = Db.FablesDatabase.GetChapter(m_chapterUid);
            var mapName = dropdown.dropdown.options[value].text;
            var map = Db.MapDatabase.GetMapByName(mapName);
            chapter.mapId = map != null ? map.Uid : string.Empty;
        }

        private void OnIntroCutsceneChanged(D90DatabaseDropdown dropdown, int value)
        {
            var chapter = Db.FablesDatabase.GetChapter(m_chapterUid);
            var eventTitle = dropdown.dropdown.options[value].text;
            var cutscene = Db.CutsceneDatabase.GetCutsceneByEventTitle(eventTitle);
            chapter.IntroCutsceneUid = cutscene.Uid;
        }

        private void OnIntroIllustrationChanged(D90DatabaseDropdown dropdown, int value)
        {
            var chapter = Db.FablesDatabase.GetChapter(m_chapterUid);
            var eventTitle = dropdown.dropdown.options[value].text;
            var cutscene = Db.CutsceneDatabase.GetCutsceneByEventTitle(eventTitle);
            chapter.IntroIllustrationUid = cutscene.Uid;
        }

        private void OnMainQuestChanged(D90DatabaseDropdown dropdown, int value)
        {
            var chapter = Db.FablesDatabase.GetChapter(m_chapterUid);
            var mainQuestName = dropdown.dropdown.options[value].text;
            var quest = Db.FableQuestDatabase.GetQuestUidFromName(mainQuestName);
            chapter.MainQuest = quest;
        }

        private void OnRewardPoolChanged(List<CardValue> cardValues)
        {
            var chapter = Db.FablesDatabase.GetChapter(m_chapterUid);
            chapter.PrizeCardPool = cardValues.Select(v => v.id).ToList();
        }

        private void OnExcludedCardsChanged(List<CardValue> cardValues)
        {
            var chapter = Db.FablesDatabase.GetChapter(m_chapterUid);
            chapter.ExcludedPrizeCards = cardValues.Select(v => v.id).ToList();
        }

        private void OnChapterDescriptionChanged(string text)
        {
            var chapter = Db.FablesDatabase.GetChapter(m_chapterUid);
            chapter.ChapterDescription = text;
        }

        private void OnConfigureRewardPool()
        {
            // everything in the set
            // - cards in shops
            // - cards obtained by dialogue
            // - cards from level up rewards

            ConfigureRewardPool();
            Open(m_chapterUid);
        }

        private void OnPrintCardsButton()
        {
            ConfigureRewardPool();

            var chapterForHero = Db.FablesDatabase.GetChapter(m_chapterUid);
            var chapterHero = Db.HeroDatabase.GetHero(chapterForHero.ChapterHero);
            var book = Db.FablesDatabase.GetBook(chapterForHero.ChapterHero, 1);
            var allCardsPrint = "ALL CARDS IN CHAPTER\n\n";
            var cardsInLevelTree = new List<string>();
            var cardsInShop = new List<string>();
            var cardsInBattle = new List<string>();
            var cardsInCutscenes = new List<string>();

            var prizePoolInFaction = new List<string>();
            var prizePoolOthers = new List<string>();
            var excludedInFaction = new List<string>();
            var excludedOthers = new List<string>();


            for (var i = 0; i < book.Chapters.Count; i++)
            {
                var chapter = book.Chapters[i];
                var maps = Db.MapDatabase.GetAllMapsForChapter(chapter.Uid);

                for (var k = 0; k < maps.Count; k++)
                {
                    for (var l = 0; l < maps[k].Rooms.Count; l++)
                    {
                        for (var m = 0; m < maps[k].Rooms[l].Tiles.Count; m++)
                        {
                            var tile = maps[k].Rooms[l].Tiles[m];

                            if (tile.NodeType == NodeType.Shop)
                            {
                                for (var n = 0; n < tile.items.Count; n++)
                                {
                                    var card = Db.CardDatabase.GetCard(tile.items[n].ItemUid);
                                    cardsInShop.AddDistinct(card.name);
                                }
                            }

                            if (tile.NodeType == NodeType.Battle)
                            {
                                var battle = Db.BattleDatabase.GetBattleData(tile.battleUid);

                                if (battle != null)
                                {
                                    for (var n = 0; n < battle.PrizeCards.Count; n++)
                                    {
                                        var card = Db.CardDatabase.GetCard(battle.PrizeCards[n]);
                                        cardsInBattle.AddDistinct(card.name);
                                    }
                                }
                            }

                            if (tile.NodeType == NodeType.Event1 || tile.NodeType == NodeType.Event2)
                            {
                                var cutscenes = new List<string>
                                    { tile.cutsceneUid, tile.introCutsceneUid, tile.outroCutsceneUid };

                                for (var n = 0; n < cutscenes.Count; n++)
                                {
                                    var cutscene = Db.CutsceneDatabase.GetCutscene(cutscenes[n]);
                                    if (cutscene != null)
                                    {
                                        for (var j = 0; j < cutscene.RewardItems.Count; j++)
                                        {
                                            var reward = cutscene.RewardItems[j];
                                            var card = Db.CardDatabase.GetCard(reward.ItemUid);

                                            if (card != null)
                                            {
                                                cardsInCutscenes.AddDistinct(card.name);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                for (var k = 0; k < chapter.PrizeCardPool.Count; k++)
                {
                    var card = Db.CardDatabase.GetCard(chapter.PrizeCardPool[k]);
                    if (card.faction == chapterHero.faction)
                    {
                        prizePoolInFaction.AddDistinct(card.name);
                    }
                }

                for (var k = 0; k < chapter.PrizeCardPool.Count; k++)
                {
                    var card = Db.CardDatabase.GetCard(chapter.PrizeCardPool[k]);
                    if (card.faction != chapterHero.faction)
                    {
                        prizePoolOthers.AddDistinct($"{card.name} ({card.faction})");
                    }
                }

                for (var k = 0; k < chapter.ExcludedPrizeCards.Count; k++)
                {
                    var card = Db.CardDatabase.GetCard(chapter.ExcludedPrizeCards[k]);

                    if (card.faction == chapterHero.faction &&
                        !cardsInLevelTree.Contains(card.name) &&
                        !cardsInShop.Contains(card.name) &&
                        !cardsInBattle.Contains(card.name) &&
                        !cardsInCutscenes.Contains(card.name))
                    {
                        excludedInFaction.AddDistinct( $"{card.name} ({card.set})" );
                    }
                }

                for (var k = 0; k < chapter.ExcludedPrizeCards.Count; k++)
                {
                    var card = Db.CardDatabase.GetCard(chapter.ExcludedPrizeCards[k]);

                    if (card.faction != chapterHero.faction &&
                        !cardsInLevelTree.Contains(card.name) &&
                        !cardsInShop.Contains(card.name) &&
                        !cardsInBattle.Contains(card.name) &&
                        !cardsInCutscenes.Contains(card.name))
                    {
                        excludedOthers.AddDistinct( $"{card.name} ({card.set})" );
                    }
                }
            }

            // ==========================================================
            // LEVEL TREE
            // ==========================================================
            allCardsPrint += "\nLEVEL TREE\n";


            // for (var j = 0; j < chapterHero.levelTree.levels.Count; j++)
            // {
            //     var reward = chapterHero.levelTree.levels[j];
            //
            //     if (reward.type == LevelReward.EType.Card)
            //     {
            //         var card = Db.CardDatabase.GetCard(reward.cardId);
            //         allCardsPrint += $"{card.name}\n";
            //         cardsInLevelTree.Add(card.name);
            //     }
            // }

            // ==========================================================
            // SHOPS
            // ==========================================================
            allCardsPrint += "\nSHOPS\n";

            for (var i = 0; i < cardsInShop.Count; i++)
            {
                allCardsPrint += $"{cardsInShop[i]}\n";
            }

            // ==========================================================
            // BATTLES
            // ==========================================================
            allCardsPrint += "\nBATTLE\n";

            for (var i = 0; i < cardsInBattle.Count; i++)
            {
                allCardsPrint += $"{cardsInBattle[i]}\n";
            }

            // ==========================================================
            // CUTSCENES
            // ==========================================================
            allCardsPrint += "\nCUTSCENES\n";

            for (var i = 0; i < cardsInCutscenes.Count; i++)
            {
                allCardsPrint += $"{cardsInCutscenes[i]}\n";
            }

            // ==========================================================
            // PRIZE POOL (RANDOM FROM BATTLE)
            // ==========================================================
            allCardsPrint += "\nPRIZE POOL IN FACTION (RANDOM BATTLE)\n";

            for (var k = 0; k < prizePoolInFaction.Count; k++)
            {
                allCardsPrint += $"{prizePoolInFaction[k]}\n";
            }

            // ==========================================================
            // PRIZE POOL (RANDOM FROM BATTLE)
            // ==========================================================
            allCardsPrint += "\nPRIZE POOL (RANDOM BATTLE)\n";

            for (var k = 0; k < prizePoolOthers.Count; k++)
            {
                allCardsPrint += $"{prizePoolOthers[k]}\n";
            }

            // ==========================================================
            // EXCLUDED (IN FACTION)
            // ==========================================================
            allCardsPrint += "\nEXCLUDED (IN FACTION)\n";

            for (var k = 0; k < excludedInFaction.Count; k++)
            {
                allCardsPrint += $"{excludedInFaction[k]}\n";
            }

            // ==========================================================
            // EXCLUDED (OTHERS)
            // ==========================================================
            allCardsPrint += "\nEXCLUDED (OTHERS)\n";

            for (var k = 0; k < excludedOthers.Count; k++)
            {
                allCardsPrint += $"{excludedOthers[k]}\n";
            }

            Debug.Log(allCardsPrint);
        }

        public static void ConfigureRewardPool()
        {
            var cardsThatWillBeExcluded = new List<string>();

            // ==========================================================
            // CUTSCENES (GLOBAL)
            // ==========================================================
            for (var i = 0; i < Db.CutsceneDatabase.Cutscenes.Count; i++)
            {
                var cutscene = Db.CutsceneDatabase.Cutscenes[i];

                for (var j = 0; j < cutscene.RewardItems.Count; j++)
                {
                    var reward = cutscene.RewardItems[j];
                    var card = Db.CardDatabase.GetCard(reward.ItemUid);

                    if (card != null)
                    {
                        cardsThatWillBeExcluded.Add(card.id);
                    }
                }
            }

            // ==========================================================
            // LEVEL TREE (GLOBAL)
            // ==========================================================
            // for (var i = 0; i < Db.HeroDatabase.Heroes.Count; i++)
            // {
            //     var hero = Db.HeroDatabase.Heroes[i];
            //     for (var j = 0; j < hero.levelTree.levels.Count; j++)
            //     {
            //         var reward = hero.levelTree.levels[j];
            //
            //         if (reward.type == LevelReward.EType.Card)
            //         {
            //             cardsThatWillBeExcluded.Add(reward.cardId);
            //         }
            //     }
            // }

            // ==========================================================
            // ALL BOOKS
            // ==========================================================
            for (var i = 0; i < Db.FablesDatabase.Books.Count; i++)
            {
                for (var j = 0; j < Db.FablesDatabase.Books[i].Chapters.Count; j++)
                {
                    var chapter = Db.FablesDatabase.Books[i].Chapters[j];

                    // ==========================================================
                    // EXCLUDE NON-AVAILABLE SETS
                    // ==========================================================

                    for (var k = 0; k < Db.CardDatabase.Cards.Count; k++)
                    {
                        var card = Db.CardDatabase.Cards[k];

                        if (card.set == CardSet.Deprecated || card.set == CardSet.Token)
                        {
                            cardsThatWillBeExcluded.AddDistinct(card.id);
                        }
                    }

                    var maps = Db.MapDatabase.GetAllMapsForChapter(chapter.Uid);

                    for (var k = 0; k < maps.Count; k++)
                    {
                        for (var l = 0; l < maps[k].Rooms.Count; l++)
                        {
                            for (var m = 0; m < maps[k].Rooms[l].Tiles.Count; m++)
                            {
                                var tile = maps[k].Rooms[l].Tiles[m];

                                // ==========================================================
                                // EXCLUDE CARDS IN SHOPS
                                // ==========================================================
                                if (tile.NodeType == NodeType.Shop)
                                {
                                    for (var n = 0; n < tile.items.Count; n++)
                                    {
                                        cardsThatWillBeExcluded.AddDistinct(tile.items[n].ItemUid);
                                    }
                                }

                                // ==========================================================
                                // EXCLUDE CARDS THAT ARE PRIZES
                                // ==========================================================
                                if (tile.NodeType == NodeType.Battle)
                                {
                                    var battle = Db.BattleDatabase.GetBattleData(tile.battleUid);

                                    if (battle != null)
                                    {
                                        for (var n = 0; n < battle.PrizeCards.Count; n++)
                                        {
                                            var card = Db.CardDatabase.GetCard( battle.PrizeCards[n] );
                                            card.craftingData.craftable = false;
                                            cardsThatWillBeExcluded.AddDistinct(card.id);
                                        }
                                    }
                                }

                                // ==========================================================
                                // EXCLUDE CARDS THAT ARE IN CUTSCENES
                                // ==========================================================

                                if (tile.NodeType == NodeType.Event1 || tile.NodeType == NodeType.Event2)
                                {
                                    var cutscenes = new List<string>
                                        { tile.cutsceneUid, tile.introCutsceneUid, tile.outroCutsceneUid };

                                    for (var n = 0; n < cutscenes.Count; n++)
                                    {
                                        var cutscene = Db.CutsceneDatabase.GetCutscene(cutscenes[n]);
                                        if (cutscene != null)
                                        {
                                            for (var o = 0; o < cutscene.RewardItems.Count; o++)
                                            {
                                                var reward = cutscene.RewardItems[o];
                                                var card = Db.CardDatabase.GetCard(reward.ItemUid);

                                                if (card != null)
                                                {
                                                    card.craftingData.craftable = false;
                                                    cardsThatWillBeExcluded.AddDistinct(card.id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                for (var j = 0; j < Db.FablesDatabase.Books[i].Chapters.Count; j++)
                {
                    var chapter = Db.FablesDatabase.Books[i].Chapters[j];
                    chapter.PrizeCardPool = new List<string>();

                    // add all excluded cards from the various places that you can get them in the game.
                    chapter.ExcludedPrizeCards = new List<string>(cardsThatWillBeExcluded);

                    for (var k = 0; k < Db.CardDatabase.Cards.Count; k++)
                    {
                        var card = Db.CardDatabase.Cards[k];

                        // if its already excluded than it cannot be a prize card.
                        if (chapter.ExcludedPrizeCards.Contains(card.id))
                        {
                            continue;
                        }

                        // prize cards have to either be neutral or part of your faction.
                        if (card.faction != Faction.Neutral && card.faction != chapter.ChapterFaction)
                        {
                            continue;
                        }

                        // prize cards cannot be relics, elder relics or powers
                        if (card.type == CardType.Relic || card.type == CardType.Elder_Relic || card.type == CardType.Power)
                        {
                            continue;
                        }

                        // prize cards need to be craftable.
                        if (!card.craftingData.craftable)
                        {
                            continue;
                        }

                        chapter.PrizeCardPool.Add(card.id);
                    }

                    chapter.ExcludedPrizeCards = chapter.ExcludedPrizeCards
                        .OrderBy(c => (int)Db.CardDatabase.GetCard(c).faction)
                        .ThenBy(c => Db.CardDatabase.GetCard(c).cost).ToList();
                    chapter.PrizeCardPool = chapter.PrizeCardPool.OrderBy(c => (int)Db.CardDatabase.GetCard(c).faction)
                        .ThenBy(c => Db.CardDatabase.GetCard(c).cost).ToList();
                }
            }
        }

        private void Save()
        {
            OnPrintCardsButton();
            Db.FablesDatabase.Save();
        }

        public void Open(string chapterUid)
        {
            m_chapterUid = chapterUid;
            var chapter = Db.FablesDatabase.GetChapter(m_chapterUid);

            if (chapter == null)
            {
                Debug.LogError($"Chapter is null? {chapterUid}");
                return;
            }

            chapterName.SetTextWithoutNotify(chapter.ChapterName);
            chapterName.onEndEdit.RemoveAllListeners();
            chapterName.onEndEdit.AddListener(OnChapterNameChanged);

            chapterPrologue.SetTextWithoutNotify(chapter.ChapterPrologue);
            chapterPrologue.onEndEdit.RemoveAllListeners();
            chapterPrologue.onEndEdit.AddListener(OnChapterPrologueChanged);

            var cutscene = Db.CutsceneDatabase.GetCutscene(chapter.IntroCutsceneUid);
            chapterIntroCutscene.SetDropdownWithoutNotify(cutscene == null ? "None" : cutscene.EventTitle);
            chapterIntroCutscene.OnValueChanged -= OnIntroCutsceneChanged;
            chapterIntroCutscene.OnValueChanged += OnIntroCutsceneChanged;

            var illustration = Db.CutsceneDatabase.GetCutscene(chapter.IntroIllustrationUid);
            chapterIllustrationCutscene.SetDropdownWithoutNotify(
                illustration == null ? "None" : illustration.EventTitle);
            chapterIllustrationCutscene.OnValueChanged -= OnIntroIllustrationChanged;
            chapterIllustrationCutscene.OnValueChanged += OnIntroIllustrationChanged;

            var map = Db.MapDatabase.GetMap(chapter.mapId);
            mapDropdown.SetDropdownWithoutNotify(map == null ? "None" : map.DisplayName);
            mapDropdown.OnValueChanged -= OnMapChanged;
            mapDropdown.OnValueChanged += OnMapChanged;

            var mainQuest = Db.FableQuestDatabase.GetQuest(chapter.MainQuest);
            mainQuestDropdown.SetDropdownWithoutNotify(mainQuest == null ? "None" : mainQuest.Name);
            mainQuestDropdown.OnValueChanged -= OnMainQuestChanged;
            mainQuestDropdown.OnValueChanged += OnMainQuestChanged;

            rewardPoolInspector.OnCardsChanged -= OnRewardPoolChanged;
            rewardPoolInspector.OnCardsChanged += OnRewardPoolChanged;
            rewardPoolInspector.SetupCards(chapter.PrizeCardPool.Select(c => new CardValue { id = c, count = 1 })
                .ToList());

            excludedCardsInspector.OnCardsChanged -= OnExcludedCardsChanged;
            excludedCardsInspector.OnCardsChanged += OnExcludedCardsChanged;
            excludedCardsInspector.SetupCards(chapter.ExcludedPrizeCards
                .Select(c => new CardValue { id = c, count = 1 }).ToList());

            saveButton.onClick.RemoveAllListeners();
            saveButton.onClick.AddListener(Save);

            chapterDescription.SetTextWithoutNotify(chapter.ChapterDescription);
            chapterDescription.onEndEdit.RemoveAllListeners();
            chapterDescription.onEndEdit.AddListener(OnChapterDescriptionChanged);

            configureRewardPool.onClick.RemoveAllListeners();
            configureRewardPool.onClick.AddListener(OnConfigureRewardPool);
        }
    }
}