using System;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90IntegerInput : MonoBehaviour
    {
        public InputField inputField;
        public Button upButton;
        public Button downButton;

        private int _value;
        public int Value => _value;

        public event Action<int> OnChanged;

        public bool interactable
        {
            get => inputField.interactable && upButton.interactable && downButton.interactable ;
            set
            {
                inputField.interactable = value;
                upButton.interactable = value;
                downButton.interactable = value;
            }
        }

        public void Start()
        {
            inputField.contentType = InputField.ContentType.IntegerNumber;

            if (string.IsNullOrEmpty(inputField.text))
            {
                inputField.text = "0";
            }

            inputField.onValueChanged.RemoveAllListeners();
            inputField.onValueChanged.AddListener(OnValueChanged);

            upButton.onClick.RemoveAllListeners();
            upButton.onClick.AddListener(OnUpButton);

            downButton.onClick.RemoveAllListeners();
            downButton.onClick.AddListener(OnDownButton);
        }

        public void SetTextWithoutNotify(string text)
        {
            if (string.IsNullOrEmpty(text)) text = "0";

            inputField.SetTextWithoutNotify(text);

            if (int.TryParse(text, out var num))
            {
                _value = num;
            }
        }

        public void SetTextWithoutNotify(int integer)
        {
            SetTextWithoutNotify(integer.ToString());
        }

        private void OnValueChanged(string text)
        {
            if (string.IsNullOrEmpty(text)) text = "0";

            inputField.SetTextWithoutNotify(text);

            if (int.TryParse(text, out var num))
            {
                _value = num;
                OnChanged?.Invoke(num);
            }
        }

        private void OnUpButton()
        {
            if (string.IsNullOrEmpty(inputField.text)) inputField.text = "0";
            var x = int.Parse(inputField.text);
            x++;
            inputField.text = x.ToString();
            var num = int.Parse(inputField.text);
            _value = num;
            OnChanged?.Invoke(num);
        }

        private void OnDownButton()
        {
            if (string.IsNullOrEmpty(inputField.text)) inputField.text = "0";
            var x = int.Parse(inputField.text);
            x--;
            inputField.text = x.ToString();
            var num = int.Parse(inputField.text);
            _value = num;
            OnChanged?.Invoke(num);
        }
    }
}