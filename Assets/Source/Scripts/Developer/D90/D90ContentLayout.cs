using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.UI.Widgets;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90ContentLayout : MonoBehaviour
    {
        public RectTransform contentLayout;
        public ToggleGroup toggleGroup;
        public bool deleteHandledOutsideOfClass;
        public Button deleteButton;

        public event Action<LayoutItem> OnDelete;

        private string _currentItem;
        private Dictionary<string, LayoutItem> _items;

        public LayoutItem CurrentItem
        {
            get
            {
                if (string.IsNullOrEmpty(_currentItem))
                {
                    return null;
                }

                if (!_items.ContainsKey(_currentItem))
                {
                    return null;
                }

                return _items[_currentItem];
            }
        }

        private void Start()
        {
            _items ??= new Dictionary<string, LayoutItem>();

            if (deleteButton)
            {
                deleteButton.onClick.RemoveAllListeners();
                deleteButton.onClick.AddListener(Delete);
            }
        }

        public void Clear()
        {
            _currentItem = string.Empty;
            _items = new Dictionary<string, LayoutItem>();
            contentLayout.DestroyChildren();
        }

        private void Delete()
        {
            if (!deleteHandledOutsideOfClass)
            {
                D90Tools.DisplayConfirmation("Are you sure?", DeleteConfirmation);
            }
            else
            {
                if (!string.IsNullOrEmpty(_currentItem) && _items != null && _items.ContainsKey(_currentItem))
                {
                    var item = _items[_currentItem];
                    OnDelete?.Invoke(item);
                }
            }
        }


        private void DeleteConfirmation(bool result)
        {
            if (result)
            {
                if (!string.IsNullOrEmpty(_currentItem) && _items != null &&
                    _items.ContainsKey(_currentItem))
                {
                    var item = _items[_currentItem];

                    OnDelete?.Invoke(item);
                    RemoveItem(item);
                }
            }

        }
        private void Update()
        {
            if (deleteButton && !deleteHandledOutsideOfClass)
            {
                deleteButton.interactable = !string.IsNullOrEmpty(_currentItem) &&
                                            _items != null &&
                                            _items.ContainsKey(_currentItem);
            }

            // if (Input.GetAxis("Vertical") > 0)
            // {
            //     if (CurrentItem != null && EventSystem.current.currentSelectedGameObject && EventSystem.current.currentSelectedGameObject == CurrentItem.gameObject)
            //     {
            //         var item = contentLayout.Find(CurrentItem.name);
            //         var index = item.GetSiblingIndex();
            //
            //         Debug.Log($"Up {index} {EventSystem.current.currentSelectedGameObject.name}");
            //
            //         if (index > 0)
            //         {
            //             index--;
            //             var newItem = contentLayout.GetChild(index);
            //             newItem.GetComponent<LayoutItem>().mainButton.Toggle.isOn = true;
            //
            //             Debug.LogWarning($"Up {index} {EventSystem.current.currentSelectedGameObject.name} {newItem.name}");
            //
            //             //EventSystem.current.SetSelectedGameObject(newItem.gameObject);
            //         }
            //     }
            //     else
            //     {
            //         Debug.LogError($"Up Current Item: {CurrentItem} {_currentItem} Event System {EventSystem.current.currentSelectedGameObject}");
            //     }
            //     //
            // }
            // if (Input.GetAxis("Vertical") < 0)
            // {
            //     if (CurrentItem != null && EventSystem.current.currentSelectedGameObject &&
            //         EventSystem.current.currentSelectedGameObject == CurrentItem.gameObject)
            //     {
            //         var item = contentLayout.Find(CurrentItem.name);
            //         var index = item.GetSiblingIndex();
            //
            //         Debug.Log($"Down {index} {EventSystem.current.currentSelectedGameObject.name}");
            //
            //         if (index < contentLayout.childCount - 1)
            //         {
            //             index++;
            //             var newItem = contentLayout.GetChild(index);
            //             newItem.GetComponent<LayoutItem>().mainButton.Toggle.isOn = true;
            //             //EventSystem.current.SetSelectedGameObject(newItem.gameObject);
            //
            //             Debug.LogWarning($"Down {index} {EventSystem.current.currentSelectedGameObject.name} {newItem.name}");
            //         }
            //     }
            //     else
            //     {
            //         Debug.LogError($"Down Current Item: {CurrentItem} {_currentItem} Event System {EventSystem.current.currentSelectedGameObject}");
            //     }
            // }
        }

        public void AddItem(LayoutItem item)
        {
            _items ??= new Dictionary<string, LayoutItem>();

            if (_items.ContainsKey(item.mainButton.content)) return;

            item.mainButton.Toggle.group = toggleGroup;
            item.transform.SetParent(contentLayout, false);
            item.mainButton.OnValueChanged += OnValueChanged;
            _items.Add(item.mainButton.content, item);
        }

        public void InsertItem(int index, LayoutItem item)
        {
            _items ??= new Dictionary<string, LayoutItem>();
            item.mainButton.Toggle.group = toggleGroup;
            item.transform.SetParent(contentLayout, false);
            item.transform.SetSiblingIndex(index);
            item.mainButton.OnValueChanged += OnValueChanged;
            _items.Add(item.mainButton.content, item);
        }

        public void RemoveItem(LayoutItem item)
        {
            if (!string.IsNullOrEmpty(item.mainButton.content) &&
                _items != null &&
                _items.ContainsKey(item.mainButton.content))
            {
                _items.Remove(item.mainButton.content);
            }

            Destroy(item.gameObject);
        }

        public LayoutItem GetItem(string contentName)
        {
            if (string.IsNullOrEmpty(contentName)) return null;
            if (!_items.ContainsKey(contentName)) return null;

            return _items[contentName];
        }

        public LayoutItem GetItemByIndex(int index)
        {
            if (index >= contentLayout.childCount) return null;
            return contentLayout.GetChild(index).GetComponent<LayoutItem>();
        }

        public void OnValueChanged(bool isOn, string content)
        {
            if (isOn) _currentItem = content;
        }
    }
}