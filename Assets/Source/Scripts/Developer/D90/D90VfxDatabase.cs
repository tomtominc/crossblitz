using CrossBlitz.Databases;
using CrossBlitz.UI.Widgets;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90VfxDatabase : MonoBehaviour
    {
        public InputField displayName;
        public LayoutItem itemPrefab;
        public D90ContentLayout vfxList;
        public D90VfxObjectMenu vfxObjectMenu;
        public Button addButton;
        public Button moveUpButton;
        public Button moveDownButton;
        public Button saveButton;

        private string m_vfxUid;

        public void Open()
        {
            addButton.onClick.RemoveAllListeners();
            addButton.onClick.AddListener(OnAddButton);

            saveButton.onClick.RemoveAllListeners();
            saveButton.onClick.AddListener(OnSaveButton);

            RefreshMenu();
        }

        public void RefreshMenu()
        {
            if (Db.VfxDatabase.vfxData == null || Db.VfxDatabase.vfxData.Count <= 0)
            {
                vfxList.Clear();
                vfxObjectMenu.ClearMenu();
                return;
            }

            if (string.IsNullOrEmpty(m_vfxUid))
            {
                m_vfxUid = Db.VfxDatabase.vfxData[0].Uid;
            }

            var vfxData = Db.VfxDatabase.GetVfxData(m_vfxUid);

            if (vfxData == null)
            {
                Debug.LogError($"Null vfx data? --{m_vfxUid}--");
                return;
            }

            moveUpButton.onClick.RemoveAllListeners();
            moveUpButton.onClick.AddListener(OnMoveUp);

            moveDownButton.onClick.RemoveAllListeners();
            moveDownButton.onClick.AddListener(OnMoveDown);

            vfxList.Clear();
            vfxList.OnDelete -= OnDeleteVfxList;
            vfxList.OnDelete += OnDeleteVfxList;

            for (var i = 0; i < Db.VfxDatabase.vfxData.Count; i++)
            {
                var vfx = Db.VfxDatabase.vfxData[i];
                var vfxItem = Instantiate(itemPrefab, vfxList.contentLayout, false);
                vfxItem.mainButton.content = vfx.Uid;
                vfxItem.textLabels[0].text = vfx.displayName;
                vfxItem.mainButton.OnValueChanged -= OnVfxChanged;
                vfxItem.mainButton.OnValueChanged += OnVfxChanged;

                if (vfx.Uid == m_vfxUid)
                {
                    vfxItem.mainButton.Toggle.SetIsOnWithoutNotify(true);
                }

                vfxList.AddItem(vfxItem);
            }

            displayName.SetTextWithoutNotify( vfxData.displayName );
            displayName.onEndEdit.RemoveAllListeners();
            displayName.onEndEdit.AddListener(OnDisplayNameChanged);

            vfxObjectMenu.RefreshMenu(m_vfxUid);
        }

        private void OnAddButton()
        {
            var newVfxItem = Db.VfxDatabase.CreateNewItem();
            m_vfxUid = newVfxItem.Uid;
            RefreshMenu();
        }

        private void OnMoveUp()
        {
            Db.VfxDatabase.MoveUp( m_vfxUid );
            RefreshMenu();
        }

        private void OnMoveDown()
        {
            Db.VfxDatabase.MoveDown( m_vfxUid );
            RefreshMenu();
        }

        private void OnDeleteVfxList(LayoutItem item)
        {
            Db.VfxDatabase.Delete( m_vfxUid );
            m_vfxUid = string.Empty;
            RefreshMenu();
        }

        private void OnVfxChanged(bool isOn, string context)
        {
            if (isOn)
            {
                m_vfxUid = context;
                RefreshMenu();
            }
        }

        private void OnDisplayNameChanged(string text)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            vfx.displayName = text;
            RefreshMenu();
        }

        private void OnSaveButton()
        {
            Db.VfxDatabase.Save();
        }
    }
}