// using System;
// using System.Collections.Generic;
// using System.Linq;
// using CrossBlitz.Databases;
// using CrossBlitz.ServerAPI.GameLogic.Intelligence;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90AiPropertiesInspector: MonoBehaviour
    {
//         public Dropdown strategyTypeDropdown;
//         public Dropdown playOrderDropdown;
//         public Dropdown targetMethodDropdown;
//         public Dropdown spellScoreMethodDropdown;
//         public InputField minimumSpellScoreInput;
//         public Dropdown boardPlacementDropdown;
//         public Dropdown boardOccupyLimitDropdown;
//         public D90DeckInspector cardPool;
//
//         private string m_cardId;
//
//         public void Init( string cardId )
//         {
//             m_cardId = cardId;
//
//             var card = Db.CardDatabase.GetCard(m_cardId);
//
//             if (card == null)
//             {
//                 return;
//             }
//
//             if (card.aiProperties == null)
//             {
//                 card.aiProperties = new AiProperties();
//                 card.aiProperties.Init();
//             }
//
//             card.aiProperties.strategy ??= new AiStrategy();
//             card.aiProperties.cardPool ??= new List<string>();
//
//             strategyTypeDropdown.onValueChanged.RemoveAllListeners();
//             strategyTypeDropdown.onValueChanged.AddListener(OnStrategyTypeChanged);
//             strategyTypeDropdown.ClearOptions();
//             strategyTypeDropdown.AddOptions(Enum.GetNames(typeof(AiStrategy.Type)).ToList());
//             strategyTypeDropdown.SetValueWithoutNotify((int)card.aiProperties.strategy.type);
//
//             playOrderDropdown.onValueChanged.RemoveAllListeners();
//             playOrderDropdown.onValueChanged.AddListener(OnPlayOrderChanged);
//             playOrderDropdown.ClearOptions();
//             playOrderDropdown.AddOptions(Enum.GetNames(typeof(AiProperties.Order)).ToList());
//             playOrderDropdown.SetValueWithoutNotify((int)card.aiProperties.order);
//
//             targetMethodDropdown.onValueChanged.RemoveAllListeners();
//             targetMethodDropdown.onValueChanged.AddListener(OnTargetMethodChanged);
//             targetMethodDropdown.ClearOptions();
//             targetMethodDropdown.AddOptions(Enum.GetNames(typeof(AiStrategy.Targets)).ToList());
//             targetMethodDropdown.SetValueWithoutNotify((int)card.aiProperties.strategy.targets);
//
//             spellScoreMethodDropdown.onValueChanged.RemoveAllListeners();
//             spellScoreMethodDropdown.onValueChanged.AddListener(OnSpellScoreMethodChanged);
//             spellScoreMethodDropdown.ClearOptions();
//             spellScoreMethodDropdown.AddOptions(Enum.GetNames(typeof(AiStrategy.TargetScoreMethod)).ToList());
//             spellScoreMethodDropdown.SetValueWithoutNotify((int)card.aiProperties.strategy.targetScoreMethod);
//
//             minimumSpellScoreInput.onValueChanged.RemoveAllListeners();
//             minimumSpellScoreInput.onValueChanged.AddListener(OnMinimumSpellScoreChanged);
//             minimumSpellScoreInput.SetTextWithoutNotify(card.aiProperties.strategy.minimumTargetScore.ToString());
//
//             boardPlacementDropdown.onValueChanged.RemoveAllListeners();
//             boardPlacementDropdown.onValueChanged.AddListener(OnBoardPlacementChanged);
//             boardPlacementDropdown.ClearOptions();
//             boardPlacementDropdown.AddOptions(Enum.GetNames(typeof(AiProperties.BoardPlacement)).ToList());
//             boardPlacementDropdown.SetValueWithoutNotify((int)card.aiProperties.placement);
//
//             boardOccupyLimitDropdown.onValueChanged.RemoveAllListeners();
//             boardOccupyLimitDropdown.onValueChanged.AddListener(OnBoardOccupyLimitChanged);
//             boardOccupyLimitDropdown.ClearOptions();
//             boardOccupyLimitDropdown.AddOptions(Enum.GetNames(typeof(AiProperties.BoardOccupyLimit)).ToList());
//             boardOccupyLimitDropdown.SetValueWithoutNotify((int)card.aiProperties.occupyLimit);
//
//             cardPool.SetupCards(card.aiProperties.cardPool.ConvertAll(id => new CardValue { id = id, count = 1 }).ToList());
//             cardPool.OnCardsChanged -= OnCardsChanged;
//             cardPool.OnCardsChanged += OnCardsChanged;
//         }
//
//         private void OnStrategyTypeChanged(int value)
//         {
//             var card = Db.CardDatabase.GetCard(m_cardId);
//
//             if (card == null)
//             {
//                 return;
//             }
//
//             card.aiProperties.strategy.type = (AiStrategy.Type)value;
//         }
//
//         private void OnPlayOrderChanged(int value)
//         {
//             var card = Db.CardDatabase.GetCard(m_cardId);
//
//             if (card == null)
//             {
//                 return;
//             }
//
//             card.aiProperties.order = (AiProperties.Order)value;
//         }
//
//         private void OnTargetMethodChanged(int value)
//         {
//             var card = Db.CardDatabase.GetCard(m_cardId);
//
//             if (card == null)
//             {
//                 return;
//             }
//
//             //card.aiProperties.strategy.targets = (AiStrategy.Targets)value;
//         }
//
//         private void OnSpellScoreMethodChanged(int value)
//         {
//             var card = Db.CardDatabase.GetCard(m_cardId);
//
//             if (card == null)
//             {
//                 return;
//             }
//
//             card.aiProperties.strategy.targetScoreMethod = (AiStrategy.TargetScoreMethod)value;
//         }
//
//         private void OnMinimumSpellScoreChanged(string value)
//         {
//             var card = Db.CardDatabase.GetCard(m_cardId);
//
//             if (card == null)
//             {
//                 return;
//             }
//
//             if (int.TryParse(value, out var result))
//             {
//                 card.aiProperties.strategy.minimumTargetScore = result;
//             }
//         }
//
//         private void OnBoardPlacementChanged(int value)
//         {
//             var card = Db.CardDatabase.GetCard(m_cardId);
//
//             if (card == null)
//             {
//                 return;
//             }
//
//             card.aiProperties.placement = (AiProperties.BoardPlacement)value;
//         }
//
//         private void OnBoardOccupyLimitChanged(int value)
//         {
//             var card = Db.CardDatabase.GetCard(m_cardId);
//
//             if (card == null)
//             {
//                 return;
//             }
//
//             card.aiProperties.occupyLimit = (AiProperties.BoardOccupyLimit)value;
//         }
//
//         private void OnCardsChanged(List<CardValue> cards)
//         {
//             var card = Db.CardDatabase.GetCard(m_cardId);
//
//             if (card == null)
//             {
//                 return;
//             }
//
//             card.aiProperties.cardPool = cards.ConvertAll(cardValue => cardValue.id);
//         }
    }
}