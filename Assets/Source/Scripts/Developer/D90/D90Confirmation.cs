using System.Linq;
using CrossBlitz.UI.Widgets;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



namespace CrossBlitz.Developer.D90
{
    public class D90Confirmation : MonoBehaviour
    {
        public Button YesButton;
        public Button NoButton;
        public bool m_result;
        public bool is_clicked;

        public void Start()
        {
            YesButton.onClick.RemoveAllListeners();
            YesButton.onClick.AddListener(OnYesButton);

            NoButton.onClick.RemoveAllListeners();
            NoButton.onClick.AddListener(OnNoButton);
        }
        public void Open()
        {
            gameObject.SetActive(true);
        }

        public void Close()
        {
            is_clicked = false;
            m_result = false;
            gameObject.SetActive(false);
        }

        private void OnYesButton()
        {
            is_clicked = true;
            m_result = true;
        }

        private void OnNoButton()
        {
            is_clicked = true;
            m_result = false;
        }

        public bool IsClicked()
        {
            return is_clicked;
        }

        private void Update()
        {
            // playButton.interactable = !_isPlaying;
            // stopButton.interactable = _isPlaying;
        }
    }
}