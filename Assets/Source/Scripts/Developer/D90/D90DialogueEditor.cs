using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Cutscene.Data;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90DialogueEditor : MonoBehaviour
    {
        // public InputField speakingCharacter;
        // public InputField dialogue;
        // public Toggle hasChoices;
        // public InputField choice1;
        // public InputField choice2;
        // public D90DatabaseDropdown cutsceneChoice1;
        // public D90DatabaseDropdown cutsceneChoice2;
        //
        // public Text emotionLabelLeft1;
        // public Dropdown emotionDropdownLeft1;
        // public Text emotionLabelLeft2;
        // public Dropdown emotionDropdownLeft2;
        // public Text emotionLabelRight1;
        // public Dropdown emotionDropdownRight1;
        // public Text emotionLabelRight2;
        // public Dropdown emotionDropdownRight2;
        //
        // public InputField dawnDollarReward;
        // public InputField goldReward;
        //
        // public List<D90DatabaseDropdown> itemRewards;
        //
        // private CutsceneData _cutscene;
        // private CutsceneDialogueInfo _dialogue;
        //
        // private void OnDialogueChanged(string newDialogue)
        // {
        //     _dialogue.Text = newDialogue;
        //     Save();
        // }
        //
        // private void OnHasChoicesChanged(bool isOn)
        // {
        //     _dialogue.HasChoices = isOn;
        //     Save();
        // }
        //
        // private void OnChoice1Changed(string newChoice)
        // {
        //     _dialogue.Choices[0].Choice = newChoice;
        //     Save();
        // }
        //
        // private void OnChoice2Changed(string newChoice)
        // {
        //     _dialogue.Choices[1].Choice = newChoice;
        //     Save();
        // }
        //
        // private void OnCutsceneChoice1Changed(D90DatabaseDropdown dropdown, int value)
        // {
        //     var eventTitle = dropdown.dropdown.options[value].text;
        //     var cutscene = Db.CutsceneDatabase.GetCutsceneByEventTitle(eventTitle);
        //     _dialogue.Choices[0].CutsceneUid = cutscene == null ? string.Empty : cutscene.Uid;
        //     Save();
        // }
        // private void OnCutsceneChoice2Changed(D90DatabaseDropdown dropdown, int value)
        // {
        //     var eventTitle = dropdown.dropdown.options[value].text;
        //     var cutscene = Db.CutsceneDatabase.GetCutsceneByEventTitle(eventTitle);
        //     _dialogue.Choices[1].CutsceneUid = cutscene == null ? string.Empty : cutscene.Uid;
        //     Save();
        // }
        //
        // private void OnEmotionLeft1Changed(int value)
        // {
        //     _dialogue.EmoteIds[0] = emotionDropdownLeft1.options[value].text;
        //     Save();
        // }
        //
        // private void OnEmotionLeft2Changed(int value)
        // {
        //     _dialogue.EmoteIds[1] = emotionDropdownLeft2.options[value].text;
        //     Save();
        // }
        //
        // private void OnEmotionRight1Changed(int value)
        // {
        //     _dialogue.EmoteIds[2] = emotionDropdownRight1.options[value].text;
        //     Save();
        // }
        // private void OnEmotionRight2Changed(int value)
        // {
        //     _dialogue.EmoteIds[3] = emotionDropdownRight2.options[value].text;
        //     Save();
        // }
        //
        // private void OnDawnDollarRewardChanged(string dd)
        // {
        //     _dialogue.DawnDollarReward = int.Parse(dd);
        //     Save();
        // }
        //
        // private void OnGoldRewardChanged(string gd)
        // {
        //     _dialogue.GoldReward = int.Parse(gd);
        //     Save();
        // }
        //
        // private void OnItemRewardChanged(D90DatabaseDropdown dropdown, int value)
        // {
        //     int index = itemRewards.IndexOf(dropdown);
        //
        //     if (index >= _dialogue.RewardItems.Count)
        //     {
        //         // for (var i = _dialogue.RewardItems.Count; i < index; i++)
        //         // {
        //         //     _dialogue.RewardItems.Add("None" );
        //         // }
        //     }
        //
        //     //_dialogue.RewardItems[index] = dropdown.dropdown.options[value].text;
        //     Save();
        // }
        //
        // public void Open(string dialogueUid)
        // {
        //     _cutscene = Db.CutsceneDatabase.GetCutsceneFromDialogueUid(dialogueUid);
        //     _dialogue = Db.CutsceneDatabase.GetDialogue(dialogueUid);
        //
        //     //speakingCharacter.text = _dialogue.Obsolete_Character_Id_Property;
        //
        //     dialogue.onEndEdit.RemoveAllListeners();
        //     dialogue.onEndEdit.AddListener(OnDialogueChanged);
        //     dialogue.SetTextWithoutNotify( _dialogue.Text );
        //
        //     hasChoices.onValueChanged.RemoveAllListeners();
        //     hasChoices.onValueChanged.AddListener(OnHasChoicesChanged);
        //     hasChoices.SetIsOnWithoutNotify( _dialogue.HasChoices );
        //
        //     if (_dialogue.Choices == null || _dialogue.Choices.Count < 2)
        //     {
        //         _dialogue.Choices = new List<CutsceneChoice> {new CutsceneChoice(), new CutsceneChoice()};
        //         Save();
        //     }
        //
        //     choice1.onValueChanged.RemoveAllListeners();
        //     choice1.onValueChanged.AddListener(OnChoice1Changed);
        //     choice1.SetTextWithoutNotify(_dialogue.Choices[0].Choice);
        //
        //     choice2.onValueChanged.RemoveAllListeners();
        //     choice2.onValueChanged.AddListener(OnChoice2Changed);
        //     choice2.SetTextWithoutNotify(_dialogue.Choices[1].Choice);
        //
        //     var choice1Cutscene = Db.CutsceneDatabase.GetCutscene(_dialogue.Choices[0].CutsceneUid);
        //     cutsceneChoice1.SetDropdown(choice1Cutscene ==null ? "None" : choice1Cutscene.EventTitle);
        //     cutsceneChoice1.OnValueChanged -= OnCutsceneChoice1Changed;
        //     cutsceneChoice1.OnValueChanged += OnCutsceneChoice1Changed;
        //
        //     var choice2Cutscene = Db.CutsceneDatabase.GetCutscene(_dialogue.Choices[1].CutsceneUid);
        //     cutsceneChoice2.SetDropdown(choice2Cutscene ==null ? "None" : choice2Cutscene.EventTitle);
        //     cutsceneChoice2.OnValueChanged -= OnCutsceneChoice2Changed;
        //     cutsceneChoice2.OnValueChanged += OnCutsceneChoice2Changed;
        //
        //     if (_dialogue.EmoteIds == null || _dialogue.EmoteIds.Count < 4)
        //     {
        //         _dialogue.EmoteIds = new List<string>
        //         {
        //             "talking", "talking", "talking", "talking"
        //         };
        //     }
        //
        //     var emotes = new List<string>{"talking", "angry", "bored", "confused", "determined", "embarrassed", "flirt", "joy", "nervous", "sad", "shocked"};
        //
        //     emotionLabelLeft1.text = $"{_cutscene.Characters[0]} Emotion";
        //     emotionDropdownLeft1.ClearOptions();
        //     emotionDropdownLeft1.AddOptions(emotes);
        //     emotionDropdownLeft1.SetValueWithoutNotify(emotes.IndexOf(_dialogue.EmoteIds[0]));
        //     emotionDropdownLeft1.onValueChanged.RemoveAllListeners();
        //     emotionDropdownLeft1.onValueChanged.AddListener(OnEmotionLeft1Changed);
        //
        //     emotionLabelLeft2.text = $"{_cutscene.Characters[1]} Emotion";
        //     emotionDropdownLeft2.ClearOptions();
        //     emotionDropdownLeft2.AddOptions(emotes);
        //     emotionDropdownLeft2.SetValueWithoutNotify(emotes.IndexOf(_dialogue.EmoteIds[1]));
        //     emotionDropdownLeft2.onValueChanged.RemoveAllListeners();
        //     emotionDropdownLeft2.onValueChanged.AddListener(OnEmotionLeft2Changed);
        //
        //     emotionLabelRight1.text = $"{_cutscene.Characters[2]} Emotion";
        //     emotionDropdownRight1.ClearOptions();
        //     emotionDropdownRight1.AddOptions(emotes);
        //     emotionDropdownRight1.SetValueWithoutNotify(emotes.IndexOf(_dialogue.EmoteIds[2]));
        //     emotionDropdownRight1.onValueChanged.RemoveAllListeners();
        //     emotionDropdownRight1.onValueChanged.AddListener(OnEmotionRight1Changed);
        //
        //     emotionLabelRight2.text = $"{_cutscene.Characters[3]} Emotion";
        //     emotionDropdownRight2.ClearOptions();
        //     emotionDropdownRight2.AddOptions(emotes);
        //     emotionDropdownRight2.SetValueWithoutNotify(emotes.IndexOf(_dialogue.EmoteIds[3]));
        //     emotionDropdownRight2.onValueChanged.RemoveAllListeners();
        //     emotionDropdownRight2.onValueChanged.AddListener(OnEmotionRight2Changed);
        //
        //     dawnDollarReward.SetTextWithoutNotify(_dialogue.DawnDollarReward.ToString());
        //     dawnDollarReward.onEndEdit.RemoveAllListeners();
        //     dawnDollarReward.onEndEdit.AddListener(OnDawnDollarRewardChanged);
        //
        //     goldReward.SetTextWithoutNotify(_dialogue.GoldReward.ToString());
        //     goldReward.onEndEdit.RemoveAllListeners();
        //     goldReward.onEndEdit.AddListener(OnGoldRewardChanged);
        //
        //     for (var i = 0; i < itemRewards.Count; i++)
        //     {
        //         // var itemId = _dialogue.RewardItems.Count > i ? _dialogue.RewardItems[i] : "None";
        //         // var item = Db.ItemDatabase.GetItem(itemId);
        //         // itemRewards[i].SetDropdown(item == null ? "None" : item.DisplayName);
        //         // itemRewards[i].OnValueChanged -= OnItemRewardChanged;
        //         // itemRewards[i].OnValueChanged += OnItemRewardChanged;
        //     }
        // }
        //
        // private void Save()
        // {
        //     Db.CutsceneDatabase.Save();
        // }
    }
}