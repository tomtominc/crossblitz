using System.Collections.Generic;
using CrossBlitz.ClientAPI;
using CrossBlitz.Databases;
using MEC;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90HeroPowerCinematic : MonoBehaviour
    {
        public HeroPowerCinematic cinematic;
        public D90IntegerInput xOffset;
        public D90IntegerInput yOffset;
        public D90IntegerInput xEyeFlashOffset;
        public D90IntegerInput yEyeFlashOffset;
        public Dropdown emotionDropdown;
        public Toggle showEyeFlash;
        public Button playButton;

        private bool m_isPlaying;
        private string m_characterId;

        public void SetHero(string characterId)
        {
            m_characterId = characterId;

            if (string.IsNullOrEmpty(m_characterId))
            {
                return;
            }

            var character = Db.CharacterDatabase.GetCharacter(m_characterId);

            xOffset.OnChanged -= OnXOffsetChanged;
            xOffset.OnChanged += OnXOffsetChanged;
            xOffset.SetTextWithoutNotify( character.heroPowerCinematicOffset.x.ToString() );

            yOffset.OnChanged -= OnYOffsetChanged;
            yOffset.OnChanged += OnYOffsetChanged;
            yOffset.SetTextWithoutNotify( character.heroPowerCinematicOffset.y.ToString() );

            xEyeFlashOffset.OnChanged -= OnXEyeFlashChanged;
            xEyeFlashOffset.OnChanged += OnXEyeFlashChanged;
            xEyeFlashOffset.SetTextWithoutNotify( character.heroPowerCinematicEyeOffset.x.ToString() );

            yEyeFlashOffset.OnChanged -= OnYEyeFlashChanged;
            yEyeFlashOffset.OnChanged += OnYEyeFlashChanged;
            yEyeFlashOffset.SetTextWithoutNotify( character.heroPowerCinematicEyeOffset.y.ToString() );

            emotionDropdown.ClearOptions();
            var emotions = new List<string>
            {
                "idle", "angry", "bored", "confused", "determined",
                "embarrassed", "flirt", "joy", "nervous", "sad",
                "shocked"
            };
            emotionDropdown.AddOptions(emotions);
            emotionDropdown.onValueChanged.RemoveAllListeners();
            emotionDropdown.onValueChanged.AddListener(OnEmotionChanged);
            emotionDropdown.SetValueWithoutNotify(string.IsNullOrEmpty(character.heroPowerCinematicEmotion) ? 0 : emotions.IndexOf(character.heroPowerCinematicEmotion));

            showEyeFlash.onValueChanged.RemoveAllListeners();
            showEyeFlash.onValueChanged.AddListener(OnShowEyeFlash);
            showEyeFlash.SetIsOnWithoutNotify(character.heroPowerCinematicShowEyeFlash);

            playButton.onClick.RemoveAllListeners();
            playButton.onClick.AddListener(OnPlayButton);

            cinematic.SetHero(character, true);
        }

        private void OnShowEyeFlash(bool isOn)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.heroPowerCinematicShowEyeFlash = isOn;

            if (!m_isPlaying)
            {
                cinematic.SetHero(character, true);
            }

        }

        private void OnXOffsetChanged(int x)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.heroPowerCinematicOffset = new Vector2Int(x, character.heroPowerCinematicOffset.y);

            if (!m_isPlaying)
            {
                cinematic.SetHero(character, true);
            }
        }

        private void OnYOffsetChanged(int y)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.heroPowerCinematicOffset = new Vector2Int(character.heroPowerCinematicOffset.x,y);

            if (!m_isPlaying)
            {
                cinematic.SetHero(character, true);
            }
        }

        private void OnXEyeFlashChanged(int x)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.heroPowerCinematicEyeOffset = new Vector2Int(x, character.heroPowerCinematicEyeOffset.y);

            if (!m_isPlaying)
            {
                cinematic.SetHero(character, true);
            }
        }

        private void OnYEyeFlashChanged(int y)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.heroPowerCinematicEyeOffset = new Vector2Int(character.heroPowerCinematicEyeOffset.x,y);

            if (!m_isPlaying)
            {
                cinematic.SetHero(character, true);
            }
        }

        private void OnEmotionChanged(int index)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.heroPowerCinematicEmotion = emotionDropdown.options[index].text;

            if (!m_isPlaying)
            {
                cinematic.SetHero(character, true);
            }
        }

        private void OnPlayButton()
        {
            if (m_isPlaying || string.IsNullOrEmpty(m_characterId)) return;

            Timing.RunCoroutine(Play());
        }

        private void Update()
        {
            playButton.enabled = !m_isPlaying;
        }

        private IEnumerator<float> Play()
        {
            m_isPlaying = true;
            yield return Timing.WaitUntilDone(cinematic.PlayCinematic(m_characterId, null));
            m_isPlaying = false;

            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            cinematic.SetHero(character, true);
        }
    }
}