using System.Collections.Generic;
using System.IO;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Data;
using CrossBlitz.UI.Widgets;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{


    public class D90MapDatabase : MonoBehaviour
    {
        [BoxGroup("Maps")] public LayoutItem itemPrefab;
        [BoxGroup("Maps")] public D90ContentLayout mapList;
        [BoxGroup("Maps")] public Button mapListBackButton;
        [BoxGroup("Maps")] public Button mapListNewButton;
        [BoxGroup("Maps")] public Button mapListItemDeleteButton;
        [BoxGroup("Maps")] public Button mapListNewFolderButton;
        [BoxGroup("Maps")] public Button mapListDeleteButton;

        [BoxGroup("Folder")] public GameObject folderInspector;
        [BoxGroup("Folder")] public InputField folderNameField;
        [BoxGroup("Folder")] public Button folderSaveButton;

        [BoxGroup("Maps Inspector")] public InputField mapName;
        [BoxGroup("Maps Inspector")] public D90DatabaseDropdown bookLink;
        [BoxGroup("Maps Inspector")] public Dropdown folder;
        [BoxGroup("Maps Inspector")] public Button mapSaveButton;
        [BoxGroup("Maps Inspector")] public Button mapMoveUpButton;
        [BoxGroup("Maps Inspector")] public Button mapMoveDownButton;
        [BoxGroup("Maps Inspector")] public D90ContentLayout roomList;
        [BoxGroup("Maps Inspector")] public D90RoomPage roomPage;

        [BoxGroup("Search Page")] public D90MapAdvancedSearchPage searchPage;

        private FableMapData _map;
        private bool _isFolderSelected;
        private string _currentFolderPath;
        private string _currentFolderSelected;
        private string _currentRoomUid;

        public void Open()
        {
            var map = Db.MapDatabase.MapData[0];

            _isFolderSelected = false;
            _currentFolderPath = string.Empty;
            _currentFolderSelected = string.Empty;

            RefreshMapList(map.Uid,string.Empty);

            folderSaveButton.onClick.RemoveAllListeners();
            folderSaveButton.onClick.AddListener(OnFolderSaved);

            roomList.OnDelete -= OnDeleteRoom;
            roomList.OnDelete += OnDeleteRoom;

            mapMoveDownButton.onClick.RemoveAllListeners();
            mapMoveDownButton.onClick.AddListener(OnRoomMoveDown);

            mapMoveUpButton.onClick.RemoveAllListeners();
            mapMoveUpButton.onClick.AddListener(OnRoomMoveUp);
        }

        private void RefreshMapList(string mapUid, string folderPath)
        {
            _map = null;

            if (!string.IsNullOrEmpty(mapUid))
            {
                _map = Db.MapDatabase.GetMap(mapUid);
            }

            _isFolderSelected = _map == null;
            _currentFolderPath = folderPath;

            var folders = Db.MapDatabase.GetFoldersInFolder(folderPath);
            var maps = Db.MapDatabase.GetMapsInFolder(folderPath);

            folderInspector.SetActive(false);
            _currentFolderSelected = string.Empty;

            if (_isFolderSelected && folders.Count > 0)
            {
                folderInspector.SetActive(true);
                _currentFolderSelected = folders[0];
            }

            mapList.Clear();

            for (var i = 0; i < folders.Count; i++)
            {
                var folder = Instantiate(itemPrefab, mapList.contentLayout, false);
                folder.mainButton.content = folders[i];
                folder.textLabels[0].text = Path.GetFileName(folders[i]);
                folder.OnDoubleClick -= OnFolderChanged;
                folder.OnDoubleClick += OnFolderChanged;
                folder.mainButton.OnValueChanged -= OnFolderSelected;
                folder.mainButton.OnValueChanged += OnFolderSelected;
                if (_isFolderSelected && folders[i] == _currentFolderSelected)
                {
                    folder.mainButton.Toggle.SetIsOnWithoutNotify(true);
                }
                folder.sideIcon.Play("folder");
                mapList.AddItem(folder);
            }

            for (var i = 0; i < maps.Count; i++)
            {
                var mapItem = Instantiate(itemPrefab, mapList.contentLayout, false);
                mapItem.mainButton.content = maps[i].Uid;
                mapItem.textLabels[0].text = maps[i].DisplayName;
                mapItem.mainButton.OnValueChanged -= OnMapChanged;
                mapItem.mainButton.OnValueChanged += OnMapChanged;
                if (_map != null && maps[i].Uid == _map.Uid)
                {
                    mapItem.mainButton.Toggle.SetIsOnWithoutNotify(true);
                }
                mapItem.sideIcon.Play("map");
                mapList.AddItem(mapItem);
            }

            mapListBackButton.onClick.RemoveAllListeners();
            mapListBackButton.onClick.AddListener(OnMapListBackButton);

            mapListNewButton.onClick.RemoveAllListeners();
            mapListNewButton.onClick.AddListener(OnMapListNewButton);

            mapListItemDeleteButton.onClick.RemoveAllListeners();
            mapListItemDeleteButton.onClick.AddListener(OnMapListDeleteButtonConfirmation);

            mapListNewFolderButton.onClick.RemoveAllListeners();
            mapListNewFolderButton.onClick.AddListener(OnMapListNewFolderButton);

            searchPage.RefreshSearch();

            RefreshMapInspector();
        }

        private void RefreshMapInspector()
        {
            mapSaveButton.onClick.RemoveAllListeners();
            mapSaveButton.onClick.AddListener(Save);

            mapName.SetTextWithoutNotify(_map?.DisplayName);

            bookLink.Init();

            if (!string.IsNullOrEmpty(_map?.BookLinkUid))
            {
                var book = Db.FablesDatabase.GetBook(_map?.BookLinkUid);

                if (book !=null)
                {
                    bookLink.SetDropdownWithoutNotify(book.BookId);
                }
            }

            bookLink.OnValueChanged -= OnBookLinkChanged;
            bookLink.OnValueChanged += OnBookLinkChanged;

            folder.ClearOptions();
            var folders = new List<string> {"None"};
            folders.AddRange(Db.MapDatabase.Folders);
            var folderValue = string.IsNullOrEmpty(_map?.Folder) ? 0 :
                folders.IndexOf(_map?.Folder) < 0 ? 0 : folders.IndexOf(_map?.Folder);
            folder.AddOptions(folders);
            folder.SetValueWithoutNotify( folderValue );

            roomList.Clear();

            for (var i = 0; i < _map?.Rooms.Count; i++)
            {
                var roomItem = Instantiate(itemPrefab, roomList.contentLayout, false);
                roomItem.mainButton.content = _map.Rooms[i].Uid;
                roomItem.textLabels[0].text = _map.Rooms[i].DisplayName;
                roomItem.mainButton.OnValueChanged -= OnRoomChanged;
                roomItem.mainButton.OnValueChanged += OnRoomChanged;

                if (_currentRoomUid == _map.Rooms[i].Uid)
                {
                    roomItem.mainButton.Toggle.SetIsOnWithoutNotify(true);
                }

                roomList.AddItem(roomItem);
            }

            // fix the current room uid if there's nothing here
            if (string.IsNullOrEmpty(_currentRoomUid) && _map?.Rooms.Count > 0)
            {
                _currentRoomUid = _map.Rooms[0].Uid;
            }

            // still can't find a room, set this inactive
            if (string.IsNullOrEmpty(_currentRoomUid))
            {
                roomPage.SetActive(false);
            }
            else
            {
                roomPage.SetActive(true);
                roomPage.SetRoom(_currentRoomUid);
            }
        }

        private void OnBookLinkChanged(D90DatabaseDropdown dropdown, int index)
        {
            var bookId = dropdown.dropdown.options[index].text;
            var book = Db.FablesDatabase.GetBook(bookId);
            var map = Db.MapDatabase.GetMap(_map.Uid);
            map.BookLinkUid = book.BookId;
        }

        private void OnFolderChanged(LayoutItem folderItem)
        {
            if (_currentFolderPath != folderItem.mainButton.content)
            {
                _currentFolderSelected = string.Empty;
                RefreshMapList(_map?.Uid, folderItem.mainButton.content);
            }
        }

        private void OnFolderSelected(bool isOn, string folderName)
        {
            if (isOn)
            {
                _isFolderSelected = true;
                _currentFolderSelected = folderName;
                folderInspector.SetActive(true);
                folderNameField.SetTextWithoutNotify(_currentFolderSelected);
            }
        }

        private void OnFolderSaved()
        {
            if (!string.IsNullOrEmpty(_currentFolderSelected))
            {
                Db.MapDatabase.ChangeFolderName(_currentFolderSelected, folderNameField.text);
                _currentFolderSelected = folderNameField.text;
                RefreshMapList(_map?.Uid, _currentFolderPath);

            }
        }

        private void OnMapChanged(bool isOn, string mapUid)
        {
            if (isOn && (_map == null || _map.Uid != mapUid))
            {
                folderInspector.SetActive(false);
                RefreshMapList(mapUid, _currentFolderPath);
            }
        }

        private void OnMapListBackButton()
        {
            if (string.IsNullOrEmpty(_currentFolderPath)) return;

            if (!_currentFolderPath.Contains("/"))
            {
                _currentFolderPath = string.Empty;
                RefreshMapList(string.Empty, string.Empty);
            }
            else
            {
                var lastIndex = _currentFolderPath.LastIndexOf('/');
                _currentFolderPath = _currentFolderPath.Remove(lastIndex, _currentFolderPath.Length - lastIndex);
                Debug.Log($"Current Folder = {_currentFolderPath}");
                RefreshMapList(string.Empty, _currentFolderPath);
            }
        }

        private void OnMapListNewButton()
        {
            var map = Db.MapDatabase.CreateNew(_currentFolderPath);
            RefreshMapList(map.Uid, _currentFolderPath);
        }

        private void OnMapListDeleteButtonConfirmation()
        {
            D90Tools.DisplayConfirmation("Are you sure?", OnMapListDeleteButton);
        }

        private void OnMapListDeleteButton(bool result)
        {
            if (result)
            {
                var folders = Db.MapDatabase.GetFoldersInFolder(_currentFolderSelected);
                var maps = Db.MapDatabase.GetMapsInFolder(_currentFolderSelected);

                // Delete Folder
                if (folders.Count == 0 && maps.Count == 0)
                {
                    if (folderInspector.activeInHierarchy)
                    {
                        Debug.Log("DELETE THE FOLDER NOW");
                        Db.MapDatabase.DeleteFolder(_currentFolderSelected);
                        _currentFolderSelected = string.Empty;
                        RefreshMapList(string.Empty, _currentFolderPath);
                    }
                }

                // Delete Map
                else
                {
                    if (_map != null && !folderInspector.activeInHierarchy)
                    {
                        Debug.Log("DELETE THE MAP NOW");
                        Db.MapDatabase.DeleteMap(_map.Uid);
                        RefreshMapList(string.Empty, _currentFolderPath);
                    }
                }
            }
        }

        private void OnMapListNewFolderButton()
        {
            if (Db.MapDatabase.TryCreateNewFolder(_currentFolderPath, out var folderName))
            {
                _currentFolderPath = folderName;
                RefreshMapList(_map?.Uid, _currentFolderPath);
                Save();
            }
        }



        private void OnRoomChanged(bool isOn, string context)
        {
            if (isOn)
            {
                _currentRoomUid = context;
                roomPage.SetActive(true);
                roomPage.SetRoom(_currentRoomUid);
            }
        }

        private void OnRoomMoveUp()
        {
            if (_map !=null && !string.IsNullOrEmpty(_currentRoomUid))
            {
                if (_map.MoveRoomUp(_currentRoomUid))
                {
                    RefreshMapInspector();
                }
            }
        }

        private void OnRoomMoveDown()
        {
            if (_map !=null && !string.IsNullOrEmpty(_currentRoomUid))
            {
                if (_map.MoveRoomDown(_currentRoomUid))
                {
                    RefreshMapInspector();
                }
            }
        }

        private void OnDeleteRoom(LayoutItem item)
        {
            if (item && !string.IsNullOrEmpty(item.mainButton.content) )
            {
                if (Db.MapDatabase.DeleteRoom(item.mainButton.content))
                {
                    Debug.Log("Successfully deleted room.");
                }
            }
        }

        private void Save()
        {
            Db.MapDatabase.Save();

            if (_map != null)
            {
                _map.DisplayName = mapName.text;

                if (folder.options.Count <= 1 || folder.value == 0)
                {
                    _map.Folder = string.Empty;
                }
                else
                {
                    _map.Folder = folder.options[folder.value].text;
                }

                RefreshMapList(_map?.Uid, _currentFolderPath);
            }
        }
    }
}