using System;
using float_oat.Desktop90;
using UnityEngine;

namespace CrossBlitz.Developer.D90
{
    public class D90ToggleButton : MonoBehaviour
    {
        public bool isOn;
        public GameObject page;
        //public D90ToggleGroup toggleGroup;
        public event Action<D90ToggleButton, bool> OnToggle;

        private D90Button _button;

        public D90Button Button
        {
            get
            {
                if (_button == null) _button = GetComponent<D90Button>();
                return _button;
            }
        }

        private void Start()
        {
            Button.onClick.AddListener(OnButtonPressed);
        }

        public void SetIsOn(bool on, bool notify=false)
        {
            isOn = on;
            if(notify) OnToggle?.Invoke(this,isOn);
        }

        private void Update()
        {
            Button.interactable = !isOn;

            if (page != null)
            {
                page.SetActive(isOn);
            }
        }

        private void OnButtonPressed()
        {
            isOn = !isOn;
            OnToggle?.Invoke(this,isOn);
        }
    }
}