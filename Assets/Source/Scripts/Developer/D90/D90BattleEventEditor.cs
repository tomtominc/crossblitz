using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.UI.Widgets;
using UnityEngine;
using UnityEngine.UI;
using Enum = System.Enum;

namespace CrossBlitz.Developer.D90
{
    public class D90BattleEventEditor : MonoBehaviour
    {
        public InputField displayName;
        public Dropdown battleEventListener;
        public Dropdown eventFrequency;
        public Toggle skipUntilToggle;
        public D90IntegerInput skipCounter;
        public D90DatabaseDropdown cardDropdown;
        public D90ContentLayout cutscenesLayout;
        public LayoutItem cutsceneItemPrefab;
        public D90DatabaseDropdown cutsceneDropdown;
        public Button addCutsceneButton;

        private string m_battleUid;
        private int m_index;
        private string m_lastCutsceneItemUid;

        public void Init(string battleUid, int index)
        {
            m_battleUid = battleUid;
            m_index = index;

            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);

            if (battle == null)
            {
                return;
            }

            displayName.onEndEdit.RemoveAllListeners();
            displayName.onEndEdit.AddListener(OnDisplayNameChanged);
            displayName.SetTextWithoutNotify(battle.BattleEvents[m_index].displayName);

            battleEventListener.onValueChanged.RemoveAllListeners();
            battleEventListener.onValueChanged.AddListener(OnBattleEventListenerChanged);
            battleEventListener.ClearOptions();
            battleEventListener.AddOptions(Enum.GetNames(typeof(BattleEvent.BattleEventListener)).ToList());
            battleEventListener.SetValueWithoutNotify((int)battle.BattleEvents[m_index].battleEventListener);

            eventFrequency.onValueChanged.RemoveAllListeners();
            eventFrequency.onValueChanged.AddListener(OnEventFrequencyChanged);
            eventFrequency.ClearOptions();
            eventFrequency.AddOptions(Enum.GetNames(typeof(BattleEvent.EventFrequency)).ToList());
            eventFrequency.SetValueWithoutNotify((int)battle.BattleEvents[m_index].frequency);

            skipUntilToggle.onValueChanged.RemoveAllListeners();
            skipUntilToggle.onValueChanged.AddListener(OnSkipUntilToggle);
            skipUntilToggle.SetIsOnWithoutNotify(battle.BattleEvents[m_index].skipUntil);

            skipCounter.OnChanged -= OnSkipCounterChanged;
            skipCounter.OnChanged += OnSkipCounterChanged;
            skipCounter.SetTextWithoutNotify(battle.BattleEvents[m_index].skips.ToString());

            cardDropdown.Init();
            cardDropdown.OnValueChanged -= OnCardDropdownChanged;
            cardDropdown.OnValueChanged += OnCardDropdownChanged;
            cardDropdown.SetDropdownWithoutNotify(battle.BattleEvents[m_index].cardId);

            cutscenesLayout.Clear();

            battle.BattleEvents[m_index].cutsceneUids ??= new List<string>();

            if (!string.IsNullOrEmpty(m_lastCutsceneItemUid) &&
                !battle.BattleEvents[m_index].cutsceneUids.Contains(m_lastCutsceneItemUid))
            {
                m_lastCutsceneItemUid = string.Empty;
            }

            for (var i = 0; i < battle.BattleEvents[m_index].cutsceneUids.Count; i++)
            {
                var cutsceneUid = battle.BattleEvents[m_index].cutsceneUids[i];
                var cutscene = Db.CutsceneDatabase.GetCutscene(cutsceneUid);

                if (cutscene == null) continue;

                if (string.IsNullOrEmpty(m_lastCutsceneItemUid) && i == 0)
                {
                    m_lastCutsceneItemUid = cutsceneUid;
                }

                var cutsceneItem = Instantiate(cutsceneItemPrefab, cutscenesLayout.contentLayout, false);
                cutsceneItem.mainButton.content = cutsceneUid;
                cutsceneItem.textLabels[0].text = $"{cutscene.EventTitle}";
                cutsceneItem.mainButton.OnValueChanged -= OnCutsceneItemClicked;
                cutsceneItem.mainButton.OnValueChanged += OnCutsceneItemClicked;
                cutsceneItem.mainButton.Toggle.SetIsOnWithoutNotify( cutsceneUid == m_lastCutsceneItemUid );
                cutscenesLayout.AddItem(cutsceneItem);
            }

            cutsceneDropdown.Init();

            cutscenesLayout.OnDelete -= OnDeleteCutscene;
            cutscenesLayout.OnDelete += OnDeleteCutscene;

            addCutsceneButton.onClick.RemoveAllListeners();
            addCutsceneButton.onClick.AddListener(OnAddCutscene);

            // cutsceneDropdown.OnAddButton -= OnAddCutscene;
            // cutsceneDropdown.OnAddButton += OnAddCutscene;
        }

        private void OnDisplayNameChanged(string text)
        {
            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);

            if (battle == null)
            {
                return;
            }

            battle.BattleEvents[m_index].displayName = text;

        }

        private void OnCutsceneItemClicked(bool isOn, string context)
        {
            if (isOn)
            {
                m_lastCutsceneItemUid = context;
            }
        }

        private void OnDeleteCutscene(LayoutItem item)
        {
            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);

            if (battle == null)
            {
                return;
            }

            battle.BattleEvents[m_index].cutsceneUids.Remove(item.mainButton.content);
            Init(m_battleUid, m_index);
        }

        private void OnAddCutscene()
        {
            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);

            if (battle == null)
            {
                return;
            }

            var dropdown = cutsceneDropdown;
            var itemName = dropdown.dropdown.options[dropdown.dropdown.value].text;
            var cutscene = Db.CutsceneDatabase.GetCutsceneByEventTitle(itemName);
            battle.BattleEvents[m_index].cutsceneUids.Add(cutscene.Uid);
            Init(m_battleUid, m_index);
        }

        private void Update()
        {
            skipCounter.interactable = skipUntilToggle.isOn;
        }

        private void OnBattleEventListenerChanged(int index)
        {
            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);

            if (battle == null)
            {
                return;
            }

            battle.BattleEvents[m_index].battleEventListener = (BattleEvent.BattleEventListener)index;
        }

        private void OnEventFrequencyChanged(int index)
        {
            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);

            if (battle == null)
            {
                return;
            }

            battle.BattleEvents[m_index].frequency = (BattleEvent.EventFrequency)index;
        }

        private void OnSkipUntilToggle(bool isOn)
        {
            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);

            if (battle == null)
            {
                return;
            }

            battle.BattleEvents[m_index].skipUntil = isOn;
        }

        private void OnSkipCounterChanged(int count)
        {
            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);

            if (battle == null)
            {
                return;
            }

            battle.BattleEvents[m_index].skips = count;
        }

        private void OnCardDropdownChanged(D90DatabaseDropdown dropdown, int index)
        {
            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);

            if (battle == null)
            {
                return;
            }

            var cardByName = Db.CardDatabase.GetCardByName(dropdown.dropdown.options[index].text);

            if (cardByName == null)
            {
                return;
            }

            battle.BattleEvents[m_index].cardId = cardByName.id;
        }
    }
}