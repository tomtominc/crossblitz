using System;
using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.UI.Widgets;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90DeckInspector : MonoBehaviour
    {
        public D90ContentLayout cardLayout;
        public LayoutItem cardTextPrefab;
        public D90DatabaseDropdown addCardDropdown;
        public Button copyButton;
        public Button pasteButton;
        public Button moveUpButton;
        public Button moveDownButton;
        private List<CardValue> m_cards;
        private string m_currentCardId;
        public event Action<List<CardValue>> OnCardsChanged;

        public void SetupCards(List<CardValue> cards)
        {
            m_cards = cards;

            cardLayout.Clear();
            cardLayout.deleteHandledOutsideOfClass = true;
            cardLayout.OnDelete -= OnCardDeleted;
            cardLayout.OnDelete += OnCardDeleted;

            for (var i = 0; i < cards.Count; i++)
            {
                AddCardToCardLayout(cards[i]);
            }

            addCardDropdown.InitSoft();
            addCardDropdown.OnAddButton -= OnAddCardFromDropdown;
            addCardDropdown.OnAddButton += OnAddCardFromDropdown;

            copyButton.onClick.RemoveAllListeners();
            copyButton.onClick.AddListener(OnCopyDeck);

            pasteButton.onClick.RemoveAllListeners();
            pasteButton.onClick.AddListener(OnPasteDeck);

            if (moveUpButton)
            {
                moveUpButton.onClick.RemoveAllListeners();
                moveUpButton.onClick.AddListener(OnMoveUp);
            }

            if (moveDownButton)
            {
                moveDownButton.onClick.RemoveAllListeners();
                moveDownButton.onClick.AddListener(OnMoveDown);
            }
        }

        private void OnMoveUp()
        {
            var cardItem = cardLayout.CurrentItem;
            if (cardItem == null) return;

            var cardId = cardItem.mainButton.content;
            var index = m_cards.IndexOf( m_cards.Find(c => c.id == cardId) );

            if (index > 0)
            {
                (m_cards[index-1], m_cards[index]) = (m_cards[index], m_cards[index-1]);

                SetupCards(m_cards);
                OnCardsChanged?.Invoke(m_cards);
            }
        }

        private void OnMoveDown()
        {
            var cardItem = cardLayout.CurrentItem;

            if (cardItem == null) return;

            var cardId = cardItem.mainButton.content;
            var index = m_cards.IndexOf( m_cards.Find(c => c.id == cardId) );

            if (index >= 0 && index < m_cards.Count-1)
            {
                (m_cards[index+1], m_cards[index]) = (m_cards[index], m_cards[index+1]);

                SetupCards(m_cards);
                OnCardsChanged?.Invoke(m_cards);
            }
        }

        private void OnCardDeleted(LayoutItem cardItem)
        {
            var cardId = cardItem.mainButton.content;
            var card = m_cards.Find(c => c.id == cardId);

            if (card != null)
            {
                card.count--;

                if (card.count <= 0)
                {
                    m_cards.Remove(card);
                }
            }

            SetupCards(m_cards);
            OnCardsChanged?.Invoke(m_cards);
        }

        private void OnAddCardFromDropdown(D90DatabaseDropdown dropdown)
        {
            var cardName = dropdown.dropdown.options[ dropdown.dropdown.value ].text;
            var cardId = Db.CardDatabase.GetCardIdFromName(cardName);

            if (string.IsNullOrEmpty(cardId))
            {
                cardId = Db.ItemDatabase.GetItemByName(cardName).ItemId;
            }

            if (!string.IsNullOrEmpty(cardId))
            {
                var cardData = m_cards.Find(c => c.id == cardId);
                var cardIndex = m_cards.IndexOf(cardData);
                var cardItem = cardLayout.GetItem(cardId);

                if (cardData != null && cardItem != null)
                {
                    cardData.count++;
                    var count = cardData.count;
                    cardItem.textLabels[1].text = $"x{count}";
                    m_cards[cardIndex] = cardData;
                }
                else
                {
                    var cardValue = new CardValue {id = cardId, count = 1};
                    m_cards.Add(cardValue);
                    //AddCardToCardLayout(cardValue);
                }

                OnCardsChanged?.Invoke(m_cards);
                SetupCards(m_cards);
            }
        }

        private void AddCardToCardLayout(CardValue value)
        {
            if (!Db.Initialized)
            {
                Db.Initialize();
            }

            var card = Db.CardDatabase.GetCard(value.id);

            if (card == null)
            {
                var item = Db.ItemDatabase.GetItem(value.id);

                if (item == null)
                {
                    return;
                }

                if (string.IsNullOrEmpty(m_currentCardId))
                {
                    m_currentCardId = value.id;
                }

                var itemText = Instantiate(cardTextPrefab, cardLayout.contentLayout);
                itemText.mainButton.content = value.id;
                itemText.mainButton.Toggle.SetIsOnWithoutNotify( m_currentCardId == value.id );
                itemText.mainButton.OnValueChanged -= OnCardToggled;
                itemText.mainButton.OnValueChanged += OnCardToggled;
                itemText.textLabels[0].text = $"∆ {item.DisplayName}";
                itemText.textLabels[1].text = $"x{value.count}";

                cardLayout.AddItem(itemText);

                if (cardLayout.CurrentItem == null && !string.IsNullOrEmpty(m_currentCardId))
                {
                    cardLayout.OnValueChanged(true, m_currentCardId);
                }

                return;
            }

            if (string.IsNullOrEmpty(m_currentCardId))
            {
                m_currentCardId = card.id;
            }

            var cardText = Instantiate(cardTextPrefab, cardLayout.contentLayout);
            cardText.mainButton.content = value.id;
            cardText.mainButton.Toggle.SetIsOnWithoutNotify( m_currentCardId == card.id );
            cardText.mainButton.OnValueChanged -= OnCardToggled;
            cardText.mainButton.OnValueChanged += OnCardToggled;
            cardText.textLabels[0].text = $"({card.cost}) {card.name}";
            cardText.textLabels[1].text = $"x{value.count}";

            cardLayout.AddItem(cardText);

            if (cardLayout.CurrentItem == null && !string.IsNullOrEmpty(m_currentCardId))
            {
                cardLayout.OnValueChanged(true, m_currentCardId);
            }
        }

        private void OnCardToggled(bool isOn, string context)
        {
            if (isOn)
            {
                m_currentCardId = context;
                var card = Db.CardDatabase.GetCard(m_currentCardId);

                if (card != null)
                {
                    addCardDropdown.SetDropdownWithoutNotify(card.name);
                }
                else
                {
                    var item = Db.ItemDatabase.GetItem(m_currentCardId);

                    if (item != null)
                    {
                        addCardDropdown.SetDropdownWithoutNotify(item.DisplayName);
                    }
                }
            }
        }

        private void OnCopyDeck()
        {
            var newDeck = new DeckData();

            for (var i = 0; i < m_cards.Count; i++)
            {
                var cardValue = m_cards[i];

                for (var j = 0; j < cardValue.count; j++)
                {
                    newDeck.AddCard(cardValue.id);
                }
            }

            GUIUtility.systemCopyBuffer = newDeck.ToRecipe();
        }

        private void OnPasteDeck()
        {
            if (!string.IsNullOrEmpty(GUIUtility.systemCopyBuffer))
            {
                var deck = DeckData.FromRecipe(GUIUtility.systemCopyBuffer);

                if (deck != null)
                {
                    SetupCards(deck.cards);
                    OnCardsChanged?.Invoke(m_cards);
                }
            }
        }
    }
}