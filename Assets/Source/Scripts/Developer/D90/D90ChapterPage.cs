using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Data;
using float_oat.Desktop90;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Developer.D90
{
    public class D90ChapterPage : MonoBehaviour
    {
        public InputField chapterName;
        public InputField chapterPrologue;
        public D90DatabaseDropdown questDropdown;
        public D90Button editButton;

        private string _chapterUid;

        private void OnChapterNameChanged(string newName)
        {
            var chapter = Db.FablesDatabase.GetChapter(_chapterUid);
            chapter.ChapterName = newName;
            Save();
        }

        private void OnChapterPrologueChanged(string newPrologue)
        {
            var chapter = Db.FablesDatabase.GetChapter(_chapterUid);
            chapter.ChapterPrologue = newPrologue;
            Save();
        }

        private void OnEditButton()
        {
            Events.Publish(this, EventType.OpenChapterInfo, new OnOpenChapterInfoEventArgs
            {
                chapterUid = _chapterUid
            });
        }

        private void OnMainQuestChanged(D90DatabaseDropdown dropdown, int value)
        {
            var chapter = Db.FablesDatabase.GetChapter(_chapterUid);

            if (value == 0)
            {
                chapter.MainQuest = string.Empty;
                Save();
            }
            else
            {
                chapter.MainQuest = Db.FableQuestDatabase.GetQuestUidFromName( dropdown.dropdown.options[value].text );
                Save();
            }
        }

        public void SetChapterData(string chapterUid)
        {
            _chapterUid = chapterUid;

            var chapter = Db.FablesDatabase.GetChapter(_chapterUid);

            chapterName.SetTextWithoutNotify(chapter.ChapterName);
            chapterName.onEndEdit.RemoveAllListeners();
            chapterName.onEndEdit.AddListener(OnChapterNameChanged);

            chapterPrologue.SetTextWithoutNotify(chapter.ChapterPrologue);
            chapterPrologue.onEndEdit.RemoveAllListeners();
            chapterPrologue.onEndEdit.AddListener(OnChapterPrologueChanged);

            editButton.onClick.RemoveAllListeners();
            editButton.onClick.AddListener(OnEditButton);

            questDropdown.Init();
            questDropdown.SetDropdownWithoutNotify( Db.FableQuestDatabase.GetQuest( chapter.MainQuest )?.Name );
            questDropdown.OnValueChanged -= OnMainQuestChanged;
            questDropdown.OnValueChanged += OnMainQuestChanged;
        }

        private void Save()
        {
            Db.FablesDatabase.Save();
        }
    }
}