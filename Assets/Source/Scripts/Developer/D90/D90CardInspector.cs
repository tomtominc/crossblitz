using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Developer.Tools;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Hero;
using CrossBlitz.UI.Widgets;
using GameDataEditor;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90CardInspector : MonoBehaviour
    {
        [BoxGroup("Database")] public D90ContentLayout cardDatabaseLayout;
        [BoxGroup("Database")] public LayoutItem cardLayoutItemPrefab;
        [BoxGroup("Database")] public Button saveButton;

        [BoxGroup("Character Creator")] public InputField characterIdField;
        [BoxGroup("Character Creator")] public Button addAsCharacter;
        [BoxGroup("Character Creator")] public Button addAsCard;

        [BoxGroup("Info")] public InputField id;
        [BoxGroup("Info")] public InputField cardName;
        [BoxGroup("Info")] public InputField cardAbbrev;
        [BoxGroup("Info")] public Dropdown cardType;
        [BoxGroup("Info")] public Dropdown cardFaction;
        [BoxGroup("Info")] public Dropdown cardClass;
        [BoxGroup("Info")] public Dropdown cardRarity;
        [BoxGroup("Info")] public InputField manaCost;
        [BoxGroup("Info")] public InputField power;
        [BoxGroup("Info")] public InputField health;
        [BoxGroup("Info")] public Dropdown attackType;
        [BoxGroup("Info")] public Dropdown set;
        [BoxGroup("Info")] public InputField description;
        [BoxGroup("Info")] public InputField burstReward;
        [BoxGroup("Info")] public InputField portraitUrl;
        [BoxGroup("Info")] public Image cardImage;
        [BoxGroup("Info")] public Image cardErrorImage;
        [BoxGroup("Info")] public Dropdown characterSpeechBleep;
        [BoxGroup("Info")] public Dropdown characterSpeechPitch;
        [BoxGroup("Info")] public Button convertToCardButton;
        [BoxGroup("Info")] public Button deleteCharacterButton;
        [BoxGroup("Info/Dialogue")] public D90DialogueSnippetEditor snippetEditor;
        // [BoxGroup("Info/Rewards")] public InputField storyModeManaRewards;
        // [BoxGroup("Info/Rewards")] public D90ItemValueEditor storyModeRewards;
        [BoxGroup("Info/Lore")] public InputField loreDescription;
        [BoxGroup("Info/Lore")] public InputField quote;
        [BoxGroup("Info/Lore")] public InputField likes;
        [BoxGroup("Info/Lore")] public InputField dislikes;
        [BoxGroup("Info/Lore")] public InputField height;
        [BoxGroup("Info/Lore")] public InputField weight;
        [BoxGroup("Info/Lore")] public Dropdown race1;
        [BoxGroup("Info/Lore")] public Dropdown race2;
        [BoxGroup("Info/Lore")] public Toggle craftable;
        [BoxGroup("Info/Lore")] public InputField craftingCost;
        [BoxGroup("Info/Lore")] public D90ItemValueEditor ingredientEditor;
        [BoxGroup("Info/Lore")] public D90ItemValueEditor scrapsEditor;

        [BoxGroup("Offsets")] public D90EventScene eventScene;
        [BoxGroup("Offsets")] public RectTransform standardCardLayout;
        [BoxGroup("Offsets")] public RectTransform zoomedCardLayout;
        [BoxGroup("Offsets")] public RectTransform boardCardLayout;
        [BoxGroup("Offsets")] public RectTransform deckCardLayout;
        [BoxGroup("Offsets")] public GameObject cardStandard;
        [BoxGroup("Offsets")] public GameObject cardZoomed;
        [BoxGroup("Offsets")] public GameObject cardBoard;
        [BoxGroup("Offsets")] public CardDeckViewItem cardDeck;
        [BoxGroup("Offsets")] public D90IntegerInput standardOffsetX;
        [BoxGroup("Offsets")] public D90IntegerInput zoomedOffsetX;
        [BoxGroup("Offsets")] public D90IntegerInput boardOffsetX;
        [BoxGroup("Offsets")] public D90IntegerInput deckOffsetX;
        [BoxGroup("Offsets")] public D90IntegerInput standardOffsetY;
        [BoxGroup("Offsets")] public D90IntegerInput zoomedOffsetY;
        [BoxGroup("Offsets")] public D90IntegerInput boardOffsetY;
        [BoxGroup("Offsets")] public D90IntegerInput deckOffsetY;

        [BoxGroup("Offsets")] public D90IntegerInput burstTopOffsetY;
        [BoxGroup("Offsets")] public D90IntegerInput burstTopWidth;
        [BoxGroup("Offsets")] public D90IntegerInput burstTopHeight;
        [BoxGroup("Offsets")] public D90IntegerInput burstBottomOffsetY;
        [BoxGroup("Offsets")] public D90IntegerInput burstBottomWidth;
        [BoxGroup("Offsets")] public D90IntegerInput burstBottomHeight;
        [BoxGroup("Offsets")] public D90IntegerInput burstBannerOffsetY;

        [BoxGroup("Offsets")] public D90IntegerInput descriptionOffsetX;
        [BoxGroup("Offsets")] public D90IntegerInput descriptionOffsetY;
        [BoxGroup("Offsets")] public D90IntegerInput descriptionSizeX;
        [BoxGroup("Offsets")] public D90IntegerInput descriptionSizeY;
        [BoxGroup("Offsets")] public D90HeroPowerCinematic heroPowerCinematic;

        [BoxGroup("Locations")] public D90CardLocationsEditor locationsEditor;
        [BoxGroup("Level Tree")] public List<D90LevelTreeEditor> levelTrees;
        [BoxGroup("Ai")] public D90AiPropertiesInspector aiProperties;
        [BoxGroup("Abilities")] public D90CardAbilitiesInspector abilitiesInspector;

        private CardView _standardCardView;
        private CardView _zoomedCardView;
        private CardView _boardCardView;
        private CardDeckViewItem _deckCardView;

        private string m_characterId;

        public void Open(OpenCardEditorInstanceEventArgs args)
        {
            if (args == null)
            {
                args = new OpenCardEditorInstanceEventArgs { cardId = GDEItemKeys.Card_AbraxasAzureAncient };
            }

            AddListeners();

            eventScene.Init();

            SetCardId(args.cardId);
            CreateDatabaseLayout();
        }

        private void CreateDatabaseLayout()
        {
            cardDatabaseLayout.Clear();
            var characters = Db.CharacterDatabase.GetAllCharacters();

            for (var i = 0; i < characters.Count; i++)
            {
                var character = characters[i];
                var characterItem = Instantiate(cardLayoutItemPrefab, cardDatabaseLayout.contentLayout, false);
                characterItem.mainButton.content = character.id;
                characterItem.textLabels[0].text = character.name;
                characterItem.mainButton.OnValueChanged += OnChangedCard;
                characterItem.mainButton.Toggle.isOn = m_characterId == character.id;
                cardDatabaseLayout.AddItem(characterItem);
            }
        }

        private void OnChangedCard(bool isOn, string content)
        {
            if (isOn)
            {
                SetCardId(content);
            }
        }



        private async void SetCardId(string cardId)
        {
            m_characterId = cardId;
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);

            if (character == null)
            {
                Debug.LogError($"Could not find character with Id {m_characterId}");
                return;
            }

            id.SetTextWithoutNotify(character.id);
            cardName.SetTextWithoutNotify(character.name);
            cardType.SetValueWithoutNotify((int) character.type);
            cardFaction.SetValueWithoutNotify((int) character.faction);
            description.SetTextWithoutNotify(character.description);
            loreDescription.SetTextWithoutNotify(character.loreDescription);
            portraitUrl.SetTextWithoutNotify(character.portraitUrl);
            heroPowerCinematic.SetHero(character.id);
            characterSpeechBleep.ClearOptions();
            var speechBleeps = new List<string> {"Text-Letter", "Text-LetterB", "Text-LetterC"};
            characterSpeechBleep.AddOptions(speechBleeps);
            var indexBleeps = speechBleeps.IndexOf(character.speechBleep);
            characterSpeechBleep.SetValueWithoutNotify(indexBleeps < 0 ? 2 : indexBleeps);
            characterSpeechPitch.ClearOptions();
            var speechPitch = new List<string> { "1", "2", "3", "4", "5" };
            characterSpeechPitch.AddOptions(speechPitch);
            var indexPitch = speechPitch.IndexOf(character.speechPitch);
            characterSpeechPitch.SetValueWithoutNotify(indexPitch < 0 ? 2 : indexPitch);
            eventScene.SetCard(m_characterId);
            burstReward.SetActive(false);
            locationsEditor.Init(m_characterId);

            for (var i = 0; i < levelTrees.Count; i++)
            {
                levelTrees[i].Init(m_characterId, i);
            }

            var dialogueCategories = new List<string>();
            dialogueCategories.AddRange(CharacterData.GetAllDialogueSnippetOptions());

            snippetEditor.Init(character.id, dialogueCategories);
            cardErrorImage.SetActive(true);

            // storyModeManaRewards.onEndEdit.RemoveAllListeners();
            // storyModeManaRewards.onEndEdit.AddListener(OnStoryModeManaRewardsChanged);
            // storyModeManaRewards.SetTextWithoutNotify(character.storyModeManaRewards.ToString());
            //
            // storyModeRewards.OnItemsChanged -= OnStoryModeRewardsChanged;
            // storyModeRewards.OnItemsChanged += OnStoryModeRewardsChanged;
            // storyModeRewards.Setup(character.storyModeRewards);

            quote.onEndEdit.RemoveAllListeners();
            quote.onEndEdit.AddListener(OnQuoteChanged);
            quote.SetTextWithoutNotify(character.quote);

            likes.onEndEdit.RemoveAllListeners();
            likes.onEndEdit.AddListener(OnLikesChanged);
            likes.SetTextWithoutNotify(character.likes);

            dislikes.onEndEdit.RemoveAllListeners();
            dislikes.onEndEdit.AddListener(OnDislikesChanged);
            dislikes.SetTextWithoutNotify(character.dislikes);

            height.onEndEdit.RemoveAllListeners();
            height.onEndEdit.AddListener(OnHeightChanged);
            height.SetTextWithoutNotify(character.height.ToString());

            weight.onEndEdit.RemoveAllListeners();
            weight.onEndEdit.AddListener(OnWeightChanged);
            weight.SetTextWithoutNotify(character.weight.ToString());

            race1.ClearOptions();
            race1.AddOptions(Enum.GetNames(typeof(Race)).ToList());
            race1.onValueChanged.RemoveAllListeners();
            race1.onValueChanged.AddListener(OnRace1Changed);
            race1.SetValueWithoutNotify((int)character.race1);

            race2.ClearOptions();
            race2.AddOptions(Enum.GetNames(typeof(Race)).ToList());
            race2.onValueChanged.RemoveAllListeners();
            race2.onValueChanged.AddListener(OnRace2Changed);
            race2.SetValueWithoutNotify((int)character.race2);

                //aiProperties.Init(m_characterId);
            abilitiesInspector.Init(m_characterId, 0,0,0);

            if (!string.IsNullOrEmpty(character.portraitUrl))
            {
                var atlas = CardGenerationTool.Instance != null
                    ? CardGenerationTool.Instance.cardAtlas
                    : await AddressableReferenceLoader.Load<SpriteAtlas>("cards-atlas");

                cardImage.sprite = atlas.GetSprite(character.portraitUrl);
                cardErrorImage.SetActive(cardImage.sprite == null);
                cardImage.SetActive(cardImage.sprite != null);
            }
            else
            {
                cardImage.SetActive(false);
            }

            if (Db.CharacterDatabase.IsCharacter(m_characterId) || Db.HeroDatabase.IsHeroById(m_characterId))
            {
                cardAbbrev.SetActive(false);
                cardClass.SetActive(false);
                cardRarity.SetActive(false);
                manaCost.SetActive(false);
                power.SetActive(false);
                health.SetActive(false);
                attackType.SetActive(false);
                set.SetActive(false);
                power.SetActive(false);
                health.SetActive(false);
                attackType.SetActive(false);

                if (_standardCardView) _standardCardView.SetActive(false);
                if (_zoomedCardView) _zoomedCardView.SetActive(false);
                if (_boardCardView) _boardCardView.SetActive(false);
                if (_deckCardView) _deckCardView.SetActive(false);

                standardOffsetX.SetActive(false);
                standardOffsetY.SetActive(false);
                zoomedOffsetX.SetActive(false);
                zoomedOffsetY.SetActive(false);
                boardOffsetX.SetActive(false);
                boardOffsetY.SetActive(false);
                deckOffsetX.SetActive(false);
                deckOffsetY.SetActive(false);

                convertToCardButton.SetActive(true);
            }
            else
            {
                var card = Db.CardDatabase.GetCard(m_characterId);

                cardAbbrev.SetActive(true);
                cardClass.SetActive(true);
                cardRarity.SetActive(true);
                manaCost.SetActive(true);
                power.SetActive(true);
                health.SetActive(true);
                attackType.SetActive(true);
                set.SetActive(true);

                convertToCardButton.SetActive(false);

                cardAbbrev.SetTextWithoutNotify(card.nameAbbrev);
                cardClass.SetValueWithoutNotify((int) card.@class);
                cardRarity.SetValueWithoutNotify((int) card.rarity);
                manaCost.SetTextWithoutNotify(card.cost.ToString());
                power.SetTextWithoutNotify(card.power.ToString());
                health.SetTextWithoutNotify(card.health.ToString());
                attackType.SetValueWithoutNotify((int) card.attackType);
                set.SetValueWithoutNotify((int) card.set);
                burstReward.SetTextWithoutNotify(card.burstRewardDescription);

                power.SetActive(card.type == CardType.Minion);
                health.SetActive(card.type == CardType.Minion || card.type == CardType.Trick);
                attackType.SetActive(card.type == CardType.Minion);
                burstReward.SetActive(card.type == CardType.Power);

                craftable.onValueChanged.RemoveAllListeners();
                craftable.onValueChanged.AddListener(OnCraftableChanged);
                craftable.SetIsOnWithoutNotify(card.craftingData.craftable);

                craftingCost.onEndEdit.RemoveAllListeners();
                craftingCost.onEndEdit.AddListener(OnCraftingCostChanged);
                craftingCost.SetTextWithoutNotify(card.craftingData.cost.ToString());

                ingredientEditor.OnItemsChanged -= OnCraftingIngredientsChanged;
                ingredientEditor.OnItemsChanged += OnCraftingIngredientsChanged;
                ingredientEditor.Setup( card.craftingData.ingredients );

                scrapsEditor.OnItemsChanged -= OnCraftingScrapsChanged;
                scrapsEditor.OnItemsChanged += OnCraftingScrapsChanged;
                scrapsEditor.Setup( card.craftingData.scraps );

                if (_standardCardView == null)
                {
                    _standardCardView = CardFactory.Create(new CreateCardFactorySettings
                    {
                        CardData = card,
                        CardPrefab = cardStandard,
                        Layout = standardCardLayout,
                        StartsDisabled = false
                    }, null);
                }

                _standardCardView.SetActive(true);
                _standardCardView.SetCardData(card);

                if (_zoomedCardView == null)
                {
                    _zoomedCardView = CardFactory.Create(new CreateCardFactorySettings
                    {
                        CardData = card,
                        CardPrefab = cardZoomed,
                        Layout = zoomedCardLayout,
                        StartsDisabled = false
                    }, null);
                }

                _zoomedCardView.SetActive(true);
                _zoomedCardView.SetCardData(card);

                if (_boardCardView == null)
                {
                    _boardCardView = CardFactory.Create(new CreateCardFactorySettings
                    {
                        CardData = card,
                        CardPrefab = cardBoard,
                        Layout = boardCardLayout,
                        StartsDisabled = false
                    }, null);
                }

                _boardCardView.SetCardData(card);
                _boardCardView.SetActive(card.type == CardType.Minion || card.type == CardType.Trick);

                if (_deckCardView == null)
                {
                    _deckCardView = Instantiate(cardDeck, deckCardLayout, false);
                    _deckCardView.RectTransform().anchoredPosition = Vector2.zero;
                    _deckCardView.SetActive(true);
                    var drag = _deckCardView.GetComponentInChildren<DragRotator>();
                    if (drag) drag.enabled = false;
                }

                _deckCardView.SetActive(true);
                _deckCardView.Load(new CardValue { id = card.id, count = 1 }, false);

                standardOffsetX.SetActive(true);
                standardOffsetY.SetActive(true);
                zoomedOffsetX.SetActive(true);
                zoomedOffsetY.SetActive(true);
                boardOffsetX.SetActive(true);
                boardOffsetY.SetActive(true);
                deckOffsetX.SetActive(true);
                deckOffsetY.SetActive(true);

                standardOffsetX.SetTextWithoutNotify(card.standardOffset.x.ToString());
                standardOffsetY.SetTextWithoutNotify(card.standardOffset.y.ToString());

                zoomedOffsetX.SetTextWithoutNotify(card.zoomedOffset.x.ToString());
                zoomedOffsetY.SetTextWithoutNotify(card.zoomedOffset.y.ToString());

                boardOffsetX.SetTextWithoutNotify(card.boardOffset.x.ToString());
                boardOffsetY.SetTextWithoutNotify(card.boardOffset.y.ToString());

                deckOffsetX.SetTextWithoutNotify(card.deckOffset.x.ToString());
                deckOffsetY.SetTextWithoutNotify(card.deckOffset.y.ToString());

                burstTopOffsetY.SetTextWithoutNotify(card.topDescriptionOffset.ToString());
                burstTopWidth.SetTextWithoutNotify(card.topDescriptionWidth.ToString());
                burstTopHeight.SetTextWithoutNotify(card.topDescriptionHeight.ToString());

                burstBottomOffsetY.SetTextWithoutNotify(card.bottomDescriptionOffset.ToString());
                burstBottomWidth.SetTextWithoutNotify(card.bottomDescriptionWidth.ToString());
                burstBottomHeight.SetTextWithoutNotify(card.bottomDescriptionHeight.ToString());

                burstBannerOffsetY.SetTextWithoutNotify(card.burstBannerOffsetY.ToString());

                descriptionOffsetX.SetTextWithoutNotify(card.descriptionOffset.x.ToString());
                descriptionOffsetY.SetTextWithoutNotify(card.descriptionOffset.y.ToString());
                descriptionSizeX.SetTextWithoutNotify(card.descriptionSize.x.ToString());
                descriptionSizeY.SetTextWithoutNotify(card.descriptionSize.y.ToString());
            }
        }

        private void AddListeners()
        {
            cardName.onEndEdit.RemoveAllListeners();
            cardName.onEndEdit.AddListener(OnCardNameChanged);

            cardAbbrev.onEndEdit.RemoveAllListeners();
            cardAbbrev.onEndEdit.AddListener(OnCardAbbrevChanged);

            cardType.ClearOptions();
            cardType.AddOptions(Enum.GetNames(typeof(CardType)).ToList());
            cardType.onValueChanged.RemoveAllListeners();
            cardType.onValueChanged.AddListener(OnCardTypeChanged);

            cardFaction.ClearOptions();
            cardFaction.AddOptions(Enum.GetNames(typeof(Faction)).ToList());
            cardFaction.onValueChanged.RemoveAllListeners();
            cardFaction.onValueChanged.AddListener(OnCardFactionChanged);

            cardClass.ClearOptions();
            cardClass.AddOptions(Enum.GetNames(typeof(Class)).ToList());
            cardClass.onValueChanged.RemoveAllListeners();
            cardClass.onValueChanged.AddListener(OnCardClassChanged);

            cardRarity.ClearOptions();
            cardRarity.AddOptions(Enum.GetNames(typeof(Rarity)).ToList());
            cardRarity.onValueChanged.RemoveAllListeners();
            cardRarity.onValueChanged.AddListener(OnCardRarityChanged);

            manaCost.onEndEdit.RemoveAllListeners();
            manaCost.onEndEdit.AddListener(OnManaCostChanged);

            power.onEndEdit.RemoveAllListeners();
            power.onEndEdit.AddListener(OnPowerChanged);

            health.onEndEdit.RemoveAllListeners();
            health.onEndEdit.AddListener(OnHealthChanged);

            portraitUrl.onEndEdit.RemoveAllListeners();
            portraitUrl.onEndEdit.AddListener(OnPortraitUrlChanged);

            attackType.ClearOptions();
            attackType.AddOptions(Enum.GetNames(typeof(AttackType)).ToList());
            attackType.onValueChanged.RemoveAllListeners();
            attackType.onValueChanged.AddListener(OnAttackTypeChanged);

            set.ClearOptions();
            set.AddOptions(Enum.GetNames(typeof(CardSet)).ToList());
            set.onValueChanged.RemoveAllListeners();
            set.onValueChanged.AddListener(OnSetChanged);

            description.onEndEdit.RemoveAllListeners();
            description.onEndEdit.AddListener(OnDescriptionChanged);

            loreDescription.onEndEdit.RemoveAllListeners();
            loreDescription.onEndEdit.AddListener(OnLoreDescriptionChanged);

            burstReward.onEndEdit.RemoveAllListeners();
            burstReward.onEndEdit.AddListener(OnBurstRewardDescriptionChanged);

            saveButton.onClick.RemoveAllListeners();
            saveButton.onClick.AddListener(OnSave);

            standardOffsetX.OnChanged -= OnStandardOffsetXChanged;
            standardOffsetX.OnChanged += OnStandardOffsetXChanged;

            standardOffsetY.OnChanged -= OnStandardOffsetYChanged;
            standardOffsetY.OnChanged += OnStandardOffsetYChanged;

            zoomedOffsetX.OnChanged -= OnZoomedOffsetXChanged;
            zoomedOffsetX.OnChanged += OnZoomedOffsetXChanged;

            zoomedOffsetY.OnChanged -= OnZoomedOffsetYChanged;
            zoomedOffsetY.OnChanged += OnZoomedOffsetYChanged;

            boardOffsetX.OnChanged -= OnBoardOffsetXChanged;
            boardOffsetX.OnChanged += OnBoardOffsetXChanged;

            boardOffsetY.OnChanged -= OnBoardOffsetYChanged;
            boardOffsetY.OnChanged += OnBoardOffsetYChanged;

            deckOffsetX.OnChanged -= OnDeckOffsetXChanged;
            deckOffsetX.OnChanged += OnDeckOffsetXChanged;

            deckOffsetY.OnChanged -= OnDeckOffsetYChanged;
            deckOffsetY.OnChanged += OnDeckOffsetYChanged;

            burstTopOffsetY.OnChanged -= OnBurstTopOffsetYChanged;
            burstTopWidth.OnChanged -= OnBurstTopWidthChanged;
            burstTopHeight.OnChanged -= OnBurstTopHeightChanged;

            burstTopOffsetY.OnChanged += OnBurstTopOffsetYChanged;
            burstTopWidth.OnChanged += OnBurstTopWidthChanged;
            burstTopHeight.OnChanged += OnBurstTopHeightChanged;

            burstBottomOffsetY.OnChanged -= OnBurstBottomOffsetYChanged;
            burstBottomWidth.OnChanged -= OnBurstBottomWidthChanged;
            burstBottomHeight.OnChanged -= OnBurstBottomHeightChanged;

            burstBottomOffsetY.OnChanged += OnBurstBottomOffsetYChanged;
            burstBottomWidth.OnChanged += OnBurstBottomWidthChanged;
            burstBottomHeight.OnChanged += OnBurstBottomHeightChanged;

            burstBannerOffsetY.OnChanged -= OnBurstBannerOffsetChanged;
            burstBannerOffsetY.OnChanged += OnBurstBannerOffsetChanged;

            descriptionOffsetX.OnChanged -= OnDescriptionOffsetXChanged;
            descriptionOffsetY.OnChanged -= OnDescriptionOffsetYChanged;
            descriptionSizeX.OnChanged -= OnDescriptionSizeXChanged;
            descriptionSizeY.OnChanged -= OnDescriptionSizeYChanged;

            descriptionOffsetX.OnChanged += OnDescriptionOffsetXChanged;
            descriptionOffsetY.OnChanged += OnDescriptionOffsetYChanged;
            descriptionSizeX.OnChanged += OnDescriptionSizeXChanged;
            descriptionSizeY.OnChanged += OnDescriptionSizeYChanged;

            addAsCard.onClick.RemoveAllListeners();
            addAsCard.onClick.AddListener(AddNewCard);

            addAsCharacter.onClick.RemoveAllListeners();
            addAsCharacter.onClick.AddListener(AddNewCharacter);

            characterSpeechBleep.onValueChanged.RemoveAllListeners();
            characterSpeechBleep.onValueChanged.AddListener(OnSpeechBleepChanged);

            characterSpeechPitch.onValueChanged.RemoveAllListeners();
            characterSpeechPitch.onValueChanged.AddListener(OnSpeechPitchChanged);

            description.onEndEdit.RemoveAllListeners();
            description.onEndEdit.AddListener(OnDescriptionChanged);
            convertToCardButton.onClick.RemoveAllListeners();
            convertToCardButton.onClick.AddListener(ConvertToCard);

            deleteCharacterButton.onClick.RemoveAllListeners();
            deleteCharacterButton.onClick.AddListener(DeleteCharacter);
        }

        private void OnCraftableChanged(bool isOn)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);

            if (card != null)
            {
                card.craftingData.craftable = isOn;
            }
        }

        private void OnCraftingCostChanged(string text)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);

            if (card != null)
            {
                card.craftingData.cost = int.Parse(text);
            }
        }

        private void OnCraftingIngredientsChanged(List<ItemValue> items)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);

            if (card != null)
            {
                card.craftingData.ingredients = items;
            }
        }

        private void OnCraftingScrapsChanged(List<ItemValue> items)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);

            if (card != null)
            {
                card.craftingData.scraps = items;
            }
        }

        private void OnQuoteChanged(string text)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.quote = text;
        }

        private void OnLikesChanged(string text)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.likes = text;
        }

        private void OnDislikesChanged(string text)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.dislikes = text;
        }

        private void OnHeightChanged(string text)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.height = int.Parse(text);
        }

        private void OnWeightChanged(string text)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.weight = int.Parse(text);
        }

        private void OnRace1Changed(int index)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.race1 = (Race)index;
        }

        private void OnRace2Changed(int index)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.race2 = (Race)index;
        }

        private void OnStoryModeManaRewardsChanged(string text)
        {
            // var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            // character.storyModeManaRewards = int.Parse(text);
        }

        private void OnStoryModeRewardsChanged(List<ItemValue> items)
        {
            // var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            // character.storyModeRewards = items;
        }

        private void OnDescriptionOffsetXChanged(int offset)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);

            if (card != null)
            {
                card.descriptionOffset = new Vector2Int(offset, card.descriptionOffset.y);
                _zoomedCardView.OnBurstOffsetChanged();
            }
        }

        private void OnDescriptionOffsetYChanged(int offset)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);

            if (card != null)
            {
                card.descriptionOffset = new Vector2Int(card.descriptionOffset.x, offset);
                _zoomedCardView.OnBurstOffsetChanged();
            }
        }

        private void OnDescriptionSizeXChanged(int offset)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);

            if (card != null)
            {
                card.descriptionSize = new Vector2Int(offset, card.descriptionSize.y);
                _zoomedCardView.OnBurstOffsetChanged();
            }
        }

        private void OnDescriptionSizeYChanged(int offset)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);

            if (card != null)
            {
                card.descriptionSize = new Vector2Int(card.descriptionSize.x, offset);
                _zoomedCardView.OnBurstOffsetChanged();
            }
        }

        private void OnBurstTopOffsetYChanged(int offset)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);

            if (card != null)
            {
                card.topDescriptionOffset = offset;
                _zoomedCardView.OnBurstOffsetChanged();
            }
        }

        private void OnBurstTopWidthChanged(int offset)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);

            if (card != null)
            {
                card.topDescriptionWidth = offset;
                _zoomedCardView.OnBurstOffsetChanged();
            }
        }

        private void OnBurstTopHeightChanged(int offset)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);

            if (card != null)
            {
                card.topDescriptionHeight = offset;
                _zoomedCardView.OnBurstOffsetChanged();
            }
        }

        private void OnBurstBottomOffsetYChanged(int offset)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);

            if (card != null)
            {
                card.bottomDescriptionOffset = offset;
                _zoomedCardView.OnBurstOffsetChanged();
            }
        }

        private void OnBurstBottomWidthChanged(int offset)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);

            if (card != null)
            {
                card.bottomDescriptionWidth = offset;
                _zoomedCardView.OnBurstOffsetChanged();
            }
        }

        private void OnBurstBottomHeightChanged(int offset)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);

            if (card != null)
            {
                card.bottomDescriptionHeight = offset;
                _zoomedCardView.OnBurstOffsetChanged();
            }
        }

        private void OnBurstBannerOffsetChanged(int offset)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);

            if (card != null)
            {
                card.burstBannerOffsetY = offset;
                _zoomedCardView.OnBurstOffsetChanged();
            }
        }

        private void OnSpeechBleepChanged(int index)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.speechBleep = characterSpeechBleep.options[index].text;
        }

        private void OnSpeechPitchChanged(int index)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.speechPitch = characterSpeechPitch.options[index].text;
        }

        private void DeleteCharacter()
        {
            D90Tools.DisplayConfirmation($"Are you sure you want to delete {m_characterId} this cannot be undone.", OnDeleteCharacter);
        }

        private void OnDeleteCharacter(bool delete)
        {
            if (delete)
            {
                Db.CharacterDatabase.DeleteCharacterFromAllDbs(m_characterId);
                SetCardId(GDEItemKeys.Card_AbraxasAzureAncient);
                CreateDatabaseLayout();
            }
        }

        private void AddNewCard()
        {
            if (Db.CardDatabase.CreateCard(characterIdField.text, out var newCard))
            {
                SetCardId(newCard.id);
                CreateDatabaseLayout();
            }
        }

        private void AddNewCharacter()
        {
            if (Db.CharacterDatabase.CreateCharacter(characterIdField.text, out var newCharacter))
            {
                SetCardId(newCharacter.id);
                CreateDatabaseLayout();
            }
        }

        private void ConvertToCard()
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            if (Db.CardDatabase.CreateCard(character.id, out var newCard))
            {
                newCard.name = character.name;
                newCard.description = character.description;
                newCard.loreDescription = character.loreDescription;
                newCard.portraitUrl = character.portraitUrl;
                newCard.faction = character.faction;
                newCard.standardOffset = character.standardOffset;
                newCard.zoomedOffset = character.zoomedOffset;
                newCard.eventDialogueOffset = character.eventDialogueOffset;
                newCard.eventDialogueShadowOffset = character.eventDialogueShadowOffset;
                newCard.eventDialogueShadowType = character.eventDialogueShadowType;
                newCard.type = CardType.Minion;
                newCard.rightFacing = character.rightFacing;
                newCard.speechBleep = character.speechBleep;
                newCard.speechPitch = character.speechPitch;
                newCard.dialogueSnippets = character.dialogueSnippets;

                Db.CharacterDatabase.DeleteCharacter(character.id);
                SetCardId(newCard.id);
                CreateDatabaseLayout();
            }
        }

        private void OnStandardOffsetXChanged(int x)
        {
            var card = Db.CharacterDatabase.GetCharacter(m_characterId);
            card.standardOffset = new Vector2Int(x, card.standardOffset.y);
            _standardCardView.OnOffsetChanged();
        }

        private void OnStandardOffsetYChanged(int y)
        {
            var card = Db.CharacterDatabase.GetCharacter(m_characterId);
            card.standardOffset = new Vector2Int(card.standardOffset.x, y);
            _standardCardView.OnOffsetChanged();
        }

        private void OnZoomedOffsetXChanged(int x)
        {
            var card = Db.CharacterDatabase.GetCharacter(m_characterId);
            card.zoomedOffset = new Vector2Int(x, card.zoomedOffset.y);
            _zoomedCardView.OnOffsetChanged();
        }

        private void OnZoomedOffsetYChanged(int y)
        {
            var card = Db.CharacterDatabase.GetCharacter(m_characterId);
            card.zoomedOffset = new Vector2Int(card.zoomedOffset.x, y);
            _zoomedCardView.OnOffsetChanged();
        }

        private void OnBoardOffsetXChanged(int x)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);
            card.boardOffset = new Vector2Int(x, card.boardOffset.y);
            _boardCardView.OnOffsetChanged();
        }

        private void OnBoardOffsetYChanged(int y)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);
            card.boardOffset = new Vector2Int(card.boardOffset.x, y);
            _boardCardView.OnOffsetChanged();
        }

        private void OnDeckOffsetXChanged(int x)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);
            card.deckOffset = new Vector2Int(x, card.deckOffset.y);
            _deckCardView.OnOffsetChanged();
        }

        private void OnDeckOffsetYChanged(int y)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);
            card.deckOffset = new Vector2Int(card.deckOffset.x, y);
            _deckCardView.OnOffsetChanged();
        }

        private void OnCardNameChanged(string text)
        {
            var card = Db.CharacterDatabase.GetCharacter(m_characterId);
            card.name = text;
        }

        private void OnCardAbbrevChanged(string text)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);
            card.nameAbbrev = text;
        }

        private void OnCardTypeChanged(int type)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);
            card.type = (CardType) type;
        }

        private void OnCardFactionChanged(int faction)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.faction = (Faction) faction;
        }

        private void OnCardClassChanged(int @class)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);
            card.@class = (Class) @class;
        }

        private void OnCardRarityChanged(int rarity)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);
            card.rarity = (Rarity) rarity;
        }

        private void OnManaCostChanged(string text)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);
            card.cost = int.Parse(text);
        }

        private void OnPowerChanged(string text)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);
            card.power = int.Parse(text);
        }

        private void OnHealthChanged(string text)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);
            card.health = int.Parse(text);
        }

        private void OnAttackTypeChanged(int aType)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);
            card.attackType = (AttackType) aType;
        }

        private void OnSetChanged(int setChange)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);
            card.set = (CardSet) setChange;
        }

        private void OnDescriptionChanged(string text)
        {
            var card = Db.CharacterDatabase.GetCharacter(m_characterId);
            card.description = text;
        }

        private void OnLoreDescriptionChanged(string text)
        {
            var card = Db.CharacterDatabase.GetCharacter(m_characterId);
            card.loreDescription = text;
        }

        private void OnBurstRewardDescriptionChanged(string text)
        {
            var card = Db.CardDatabase.GetCard(m_characterId);
            card.burstRewardDescription = text;
        }

        private void OnPortraitUrlChanged(string text)
        {
            var card = Db.CharacterDatabase.GetCharacter(m_characterId);
            card.portraitUrl = text;
        }

        private void OnSave()
        {
            Db.Save();
            CreateDatabaseLayout();
        }
    }
}