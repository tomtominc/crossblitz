using System.Collections.Generic;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Databases;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.Tutorial;
using CrossBlitz.UI.Widgets;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.ResourceLocations;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90TutorialEditor : MonoBehaviour
    {
        // tutorials list
        public LayoutItem textLayoutItemPrefab;
        public D90ContentLayout tutorialsList;
        public Button addTutorialButton;
        public Button moveUpTutorialButton;
        public Button moveDownTutorialButton;

        // tutorial contents
        public Toggle enabledToggle;
        public InputField title;
        public D90ContentLayout pageList;
        public Button addPageButton;
        public Button moveUpPageButton;
        public Button moveDownPageButton;

        // page contents
        public InputField pageHeader;
        public InputField pageText;
        public D90ContentLayout imageList;
        public Dropdown imageChoiceDropdown;
        public Button addImageButton;
        public Button moveUpImageButton;
        public Button moveDownImageButton;

        // misc
        public Button previewButton;
        public Button saveButton;

        private string m_currentTutorial;
        private string m_currentPage;
        private string m_currentImage;

        public void Open(string tutorialUid)
        {
            m_currentPage = m_currentTutorial != tutorialUid ? string.Empty : m_currentPage;
            m_currentImage = m_currentTutorial != tutorialUid ? string.Empty : m_currentImage;

            m_currentTutorial = tutorialUid;

            addTutorialButton.onClick.RemoveAllListeners();
            addTutorialButton.onClick.AddListener(AddTutorialButton);

            moveUpTutorialButton.onClick.RemoveAllListeners();
            moveUpTutorialButton.onClick.AddListener(MoveUpTutorialButton);

            moveDownTutorialButton.onClick.RemoveAllListeners();
            moveDownTutorialButton.onClick.AddListener(MoveDownTutorialButton);

            tutorialsList.Clear();
            tutorialsList.OnDelete -= OnDeleteTutorial;
            tutorialsList.OnDelete += OnDeleteTutorial;

            for (var i = 0; i < Db.TutorialDatabase.tutorials.Count; i++)
            {
                var tutorial = Db.TutorialDatabase.tutorials[i];
                if (string.IsNullOrEmpty(m_currentTutorial)) m_currentTutorial = tutorial.Uid;
                var text = Instantiate(textLayoutItemPrefab, tutorialsList.contentLayout);
                text.mainButton.content = tutorial.Uid;
                text.textLabels[0].text = tutorial.title;
                text.mainButton.Toggle.SetIsOnWithoutNotify( m_currentTutorial == tutorial.Uid );
                text.mainButton.OnValueChanged += OnTutorialChanged;
                tutorialsList.AddItem( text );
            }

            if (string.IsNullOrEmpty(m_currentTutorial)) return;

            var currentTutorial = Db.TutorialDatabase.Get(m_currentTutorial);

            enabledToggle.onValueChanged.RemoveAllListeners();
            enabledToggle.onValueChanged.AddListener(OnTutorialEnabled);
            enabledToggle.SetIsOnWithoutNotify( currentTutorial.enabled );

            title.onEndEdit.RemoveAllListeners();
            title.onEndEdit.AddListener( OnTitleChanged );
            title.SetTextWithoutNotify( currentTutorial.title );

            addPageButton.onClick.RemoveAllListeners();
            addPageButton.onClick.AddListener(AddPageButton);

            moveUpPageButton.onClick.RemoveAllListeners();
            moveUpPageButton.onClick.AddListener(MoveUpPageButton);

            moveDownPageButton.onClick.RemoveAllListeners();
            moveDownPageButton.onClick.AddListener(MoveDownPageButton);

            pageList.Clear();
            pageList.OnDelete -= OnDeletePage;
            pageList.OnDelete += OnDeletePage;

            for (var i = 0; i < currentTutorial.pages.Count; i++)
            {
                var page = currentTutorial.pages[i];
                if (string.IsNullOrEmpty(m_currentPage)) m_currentPage = page.Uid;
                var text = Instantiate(textLayoutItemPrefab, pageList.contentLayout);
                text.mainButton.content = page.Uid;
                text.textLabels[0].text = $"Page {i+1} ({page.header})";
                text.mainButton.Toggle.SetIsOnWithoutNotify( m_currentPage == page.Uid );
                text.mainButton.OnValueChanged += OnPageChanged;
                pageList.AddItem( text );
            }

            if (string.IsNullOrEmpty(m_currentPage)) return;

            var currentPage = currentTutorial.Get(m_currentPage);

            pageHeader.onEndEdit.RemoveAllListeners();
            pageHeader.onEndEdit.AddListener(OnPageHeaderChanged);
            pageHeader.SetTextWithoutNotify(currentPage.header);

            pageText.onEndEdit.RemoveAllListeners();
            pageText.onEndEdit.AddListener(OnPageTextChanged);
            pageText.SetTextWithoutNotify( currentPage.text );

            imageList.Clear();
            imageList.OnDelete -= OnDeleteImage;
            imageList.OnDelete += OnDeleteImage;

            for (var i = 0; i < currentPage.images.Count; i++)
            {
                var image = currentPage.images[i];
                if (string.IsNullOrEmpty(m_currentImage)) m_currentImage = image;
                var text = Instantiate(textLayoutItemPrefab, imageList.contentLayout);
                text.mainButton.content = image;
                text.textLabels[0].text = $"{image.Split('/')[2]}";
                text.mainButton.Toggle.SetIsOnWithoutNotify( m_currentImage == image );
                text.mainButton.OnValueChanged += OnImageChanged;
                imageList.AddItem( text );
            }

            var addressableList = GetAddressablesList();

            imageChoiceDropdown.ClearOptions();
            imageChoiceDropdown.AddOptions( addressableList );

            addImageButton.onClick.RemoveAllListeners();
            addImageButton.onClick.AddListener(OnAddImage);

            moveUpImageButton.onClick.RemoveAllListeners();
            moveUpImageButton.onClick.AddListener( OnMoveUpImage );

            moveDownImageButton.onClick.RemoveAllListeners();
            moveDownImageButton.onClick.AddListener( OnMoveDownImage );

            previewButton.onClick.RemoveAllListeners();
            previewButton.onClick.AddListener(OnPreviewButton);

            saveButton.onClick.RemoveAllListeners();
            saveButton.onClick.AddListener(OnSaveButton);

        }

        private void AddTutorialButton()
        {
            var tutorial = Db.TutorialDatabase.Add("New Tutorial");
            Open(tutorial.Uid);
        }

        private void MoveUpTutorialButton()
        {
            if (Db.TutorialDatabase.MoveUp(m_currentTutorial))
            {
                Open(m_currentTutorial);
            }
        }

        private void MoveDownTutorialButton()
        {
            if (Db.TutorialDatabase.MoveDown(m_currentTutorial))
            {
                Open(m_currentTutorial);
            }
        }

        private void OnDeleteTutorial(LayoutItem item)
        {
            var deleteUid = item.mainButton.content;
            if (Db.TutorialDatabase.Delete(deleteUid))
            {
                m_currentTutorial = string.Empty;
                Open(m_currentTutorial);
            }
        }

        private void OnTutorialChanged(bool isOn, string context)
        {
            if (isOn)
            {
                Open(context);
            }
        }

        private void OnTutorialEnabled(bool isOn)
        {
            var tutorial = Db.TutorialDatabase.Get(m_currentTutorial);
            tutorial.enabled = isOn;
        }

        private void OnTitleChanged(string text)
        {
            var tutorial = Db.TutorialDatabase.Get(m_currentTutorial);
            tutorial.title = text;
        }

        private void AddPageButton()
        {
            var tutorial = Db.TutorialDatabase.Get(m_currentTutorial);
            m_currentPage = tutorial.AddPage();
            Open(m_currentTutorial);
        }

        private void MoveUpPageButton()
        {
            var tutorial = Db.TutorialDatabase.Get(m_currentTutorial);

            if (tutorial.MoveUp(m_currentPage))
            {
                Open(m_currentTutorial);
            }
        }

        private void MoveDownPageButton()
        {
            var tutorial = Db.TutorialDatabase.Get(m_currentTutorial);

            if (tutorial.MoveDown(m_currentPage))
            {
                Open(m_currentTutorial);
            }
        }

        private void OnDeletePage(LayoutItem item)
        {
            var tutorial = Db.TutorialDatabase.Get(m_currentTutorial);
            var deleteUid = item.mainButton.content;

            if (tutorial.DeletePage(deleteUid))
            {
                m_currentPage = string.Empty;
                Open(m_currentTutorial);
            }
        }

        private void OnPageChanged(bool isOn, string context)
        {
            m_currentPage = context;
            Open(m_currentTutorial);
        }

        private void OnPageHeaderChanged(string text)
        {
            var tutorial = Db.TutorialDatabase.Get(m_currentTutorial);
            var page = tutorial.Get(m_currentPage);
            page.header = text;
        }

        private void OnPageTextChanged(string text)
        {
            var tutorial = Db.TutorialDatabase.Get(m_currentTutorial);
            var page = tutorial.Get(m_currentPage);
            page.text = text;
        }

        private void OnImageChanged(bool isOn, string context)
        {
            if (isOn)
            {
                m_currentImage = context;
            }
        }

        private void OnDeleteImage(LayoutItem item)
        {
            var image = item.mainButton.content;
            var tutorial = Db.TutorialDatabase.Get(m_currentTutorial);
            var page = tutorial.Get(m_currentPage);

            if (page.DeleteImage(image))
            {
                Open(m_currentTutorial);
            }
        }

        private void OnAddImage()
        {
            var addressablesList = GetAddressablesList();
            var index = imageChoiceDropdown.value;
            var tutorial = Db.TutorialDatabase.Get(m_currentTutorial);
            var page = tutorial.Get(m_currentPage);
            page.AddImage( addressablesList[index] );
            Open(m_currentTutorial);
        }

        private void OnMoveUpImage()
        {
            var tutorial = Db.TutorialDatabase.Get(m_currentTutorial);
            var page = tutorial.Get(m_currentPage);
            page.MoveUp( m_currentImage );
            Open( m_currentTutorial );
        }

        private void OnMoveDownImage()
        {
            var tutorial = Db.TutorialDatabase.Get(m_currentTutorial);
            var page = tutorial.Get(m_currentPage);
            page.MoveDown( m_currentImage );
            Open( m_currentTutorial );
        }

        private void OnSaveButton()
        {
            Db.TutorialDatabase.Save();
        }

        private async void OnPreviewButton()
        {
            var sceneModel = await SceneController.Instance.LoadScene(new SceneLoadParams
            {
                SceneToLoad = "TutorialPopup",
                LoadSceneMode = LoadSceneMode.Additive
            });

            var tutorialMenu = sceneModel.GetSceneData<TutorialMenu>();

            if (tutorialMenu == null)
            {
                Debug.LogError("Error!");
                return;
            }

            tutorialMenu.SetAsOverlay();
            tutorialMenu.Open( m_currentTutorial );
        }

        private List<string> GetAddressablesList()
        {
            var addressableList = new List<string>();

#if UNITY_EDITOR

            var settings = UnityEditor.AddressableAssets.AddressableAssetSettingsDefaultObject.Settings;

            foreach (var group in settings.groups)
            {
                foreach (var entry in group.entries)
                {
                    if (!string.IsNullOrEmpty(entry.address) && entry.address.Contains("tutorial-images/en"))
                    {
                        addressableList.Add(entry.address);
                    }
                }
            }
#endif
            return addressableList;
        }
    }
}