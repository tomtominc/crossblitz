using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.Hero;
using CrossBlitz.UI.Widgets;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90DeckRecipeEditor : MonoBehaviour
    {
        public D90ContentLayout deckRecipesList;
        public LayoutItem textLayoutItemPrefab;
        public InputField deckRecipeUidInput;
        public InputField recipeDisplayName;
        public Dropdown deckTypeDropdown;
        public Dropdown factionDropdown;
        public InputField overviewInput;
        public InputField playGuideInput;
        public D90DeckInspector mainCards;
        public D90DeckInspector deckList;
        public Button addButton;
        public Button saveButton;
        public Button openDeckRecipeUiButton;
        public Toggle collectableToggle;
        public SpriteAnimationAsset archetypeAnimationAsset;
        public Dropdown archetypeIconName;
        public InputField archetypeDisplayName;
        public Button moveUpButton;
        public Button moveDownButton;

        private string m_deckRecipeUid;

        public void Open(string deckRecipeUid)
        {
            if (!Db.Initialized)
            {
                Db.Initialize();
            }

            m_deckRecipeUid = deckRecipeUid;

            if (string.IsNullOrEmpty(m_deckRecipeUid) && Db.DeckRecipeDatabase.deckRecipes.Count > 0)
            {
                m_deckRecipeUid = Db.DeckRecipeDatabase.deckRecipes[0].Uid;
            }

            deckRecipesList.Clear();
            deckRecipesList.OnDelete -= OnDeleteRecipe;
            deckRecipesList.OnDelete += OnDeleteRecipe;

            addButton.onClick.RemoveAllListeners();
            addButton.onClick.AddListener(OnAddButton);

            saveButton.onClick.RemoveAllListeners();
            saveButton.onClick.AddListener(OnSaveButton);

            moveDownButton.onClick.RemoveAllListeners();
            moveDownButton.onClick.AddListener(OnMoveDownButton);

            moveUpButton.onClick.RemoveAllListeners();
            moveUpButton.onClick.AddListener(OnMoveUpButton);

            for (var i = 0; i < Db.DeckRecipeDatabase.deckRecipes.Count; i++)
            {
                var recipe = Db.DeckRecipeDatabase.deckRecipes[i];
                var cardText = Instantiate(textLayoutItemPrefab, deckRecipesList.contentLayout);
                cardText.mainButton.content = recipe.Uid;
                cardText.textLabels[0].text = recipe.deckData.name;
                cardText.mainButton.Toggle.SetIsOnWithoutNotify( recipe.Uid == m_deckRecipeUid );
                cardText.mainButton.OnValueChanged += OnDeckChanged;
                deckRecipesList.AddItem(cardText);
            }

            if (!string.IsNullOrEmpty(m_deckRecipeUid))
            {
                var recipe = Db.DeckRecipeDatabase.GetRecipe(m_deckRecipeUid);

                deckRecipeUidInput.SetTextWithoutNotify(recipe.Uid);

                collectableToggle.onValueChanged.RemoveAllListeners();
                collectableToggle.onValueChanged.AddListener(OnCollectableChanged);
                collectableToggle.SetIsOnWithoutNotify(recipe.collectable);

                var archetypeAnimationNames = archetypeAnimationAsset.animations.Select(anim => anim.name).ToList();
                archetypeIconName.ClearOptions();
                archetypeIconName.AddOptions( archetypeAnimationNames );
                archetypeIconName.onValueChanged.RemoveAllListeners();
                archetypeIconName.onValueChanged.AddListener( OnArchetypeIconChanged );
                archetypeIconName.SetValueWithoutNotify( archetypeAnimationNames.IndexOf( recipe.archetype ) );

                archetypeDisplayName.onEndEdit.RemoveAllListeners();
                archetypeDisplayName.onEndEdit.AddListener(OnArchetypeDisplayNameChanged);
                archetypeDisplayName.SetTextWithoutNotify( recipe.archetypeDisplayName );

                recipeDisplayName.onEndEdit.RemoveAllListeners();
                recipeDisplayName.onEndEdit.AddListener(OnRecipeNameChanged);
                recipeDisplayName.SetTextWithoutNotify( recipe.deckData.name );

                overviewInput.onEndEdit.RemoveAllListeners();
                overviewInput.onEndEdit.AddListener(OverviewChanged);
                overviewInput.SetTextWithoutNotify( recipe.overview );

                playGuideInput.onEndEdit.RemoveAllListeners();
                playGuideInput.onEndEdit.AddListener(PlayGuideChanged);
                playGuideInput.SetTextWithoutNotify( recipe.howToPlayGuide );

                factionDropdown.ClearOptions();
                factionDropdown.AddOptions(Enum.GetNames(typeof(Faction)).ToList());
                factionDropdown.onValueChanged.RemoveAllListeners();
                factionDropdown.onValueChanged.AddListener(OnDeckFactionChanged);
                factionDropdown.SetValueWithoutNotify((int)recipe.deckData.faction);

                deckTypeDropdown.ClearOptions();
                deckTypeDropdown.AddOptions(Enum.GetNames(typeof(DeckRecipeData.DeckType)).ToList());
                deckTypeDropdown.onValueChanged.RemoveAllListeners();
                deckTypeDropdown.onValueChanged.AddListener(OnDeckTypeChanged);
                deckTypeDropdown.SetValueWithoutNotify((int)recipe.deckType);

                mainCards.OnCardsChanged -= OnMainCardsChanged;
                mainCards.OnCardsChanged += OnMainCardsChanged;
                mainCards.SetActive(true);
                mainCards.SetupCards(recipe.mainCards);

                deckList.OnCardsChanged -= OnDeckCardsChanged;
                deckList.OnCardsChanged += OnDeckCardsChanged;
                deckList.SetActive(true);
                deckList.SetupCards(recipe.deckData.cards);

                openDeckRecipeUiButton.onClick.RemoveAllListeners();
                openDeckRecipeUiButton.onClick.AddListener(OpenRecipe);
            }
            else
            {
                deckRecipeUidInput.SetTextWithoutNotify(string.Empty);

                recipeDisplayName.onEndEdit.RemoveAllListeners();
                recipeDisplayName.SetTextWithoutNotify( string.Empty );

                overviewInput.onEndEdit.RemoveAllListeners();
                overviewInput.SetTextWithoutNotify( string.Empty );

                playGuideInput.onEndEdit.RemoveAllListeners();
                playGuideInput.SetTextWithoutNotify( string.Empty );

                factionDropdown.ClearOptions();
                factionDropdown.AddOptions(Enum.GetNames(typeof(Faction)).ToList());
                factionDropdown.onValueChanged.RemoveAllListeners();
                factionDropdown.SetValueWithoutNotify((int)Faction.None);

                deckTypeDropdown.ClearOptions();
                deckTypeDropdown.AddOptions(Enum.GetNames(typeof(DeckRecipeData.DeckType)).ToList());
                deckTypeDropdown.onValueChanged.RemoveAllListeners();
                deckTypeDropdown.SetValueWithoutNotify((int)DeckRecipeData.DeckType.Aggro);

                mainCards.OnCardsChanged -= OnMainCardsChanged;
                mainCards.SetActive(false);

                deckList.OnCardsChanged -= OnDeckCardsChanged;
                deckList.SetActive(false);
            }
        }

        private void OnCollectableChanged(bool isOn)
        {
            var recipe = Db.DeckRecipeDatabase.GetRecipe(m_deckRecipeUid);

            if (recipe == null)
            {
                return;
            }

            recipe.collectable = isOn;
        }

        private void OnArchetypeIconChanged(int index)
        {
            var recipe = Db.DeckRecipeDatabase.GetRecipe(m_deckRecipeUid);

            if (recipe == null)
            {
                return;
            }

            var archetypeAnimationNames = archetypeAnimationAsset.animations.Select(anim => anim.name).ToList();
            recipe.archetype = archetypeAnimationNames[index];
        }

        private void OnArchetypeDisplayNameChanged(string text)
        {
            var recipe = Db.DeckRecipeDatabase.GetRecipe(m_deckRecipeUid);

            if (recipe == null)
            {
                return;
            }

            recipe.archetypeDisplayName = text;
        }

        private async void OpenRecipe()
        {
            var recipe = Db.DeckRecipeDatabase.GetRecipe(m_deckRecipeUid);

            if (recipe == null)
            {
                return;
            }

            var sceneModel = await SceneController.Instance.LoadScene(new SceneLoadParams
            {
                SceneToLoad = "RecipeViewer",
                LoadSceneMode = LoadSceneMode.Additive
            });

            var recipeViewer = sceneModel.GetSceneData<RecipeViewerManager>();
            recipeViewer.OnClosed += OnRecipeViewerClosed;
            recipeViewer.SetAsOverlay();
            recipeViewer.Open( recipe );
        }

        private void OnRecipeViewerClosed()
        {

        }

        private void OnDeckChanged(bool isOn, string context)
        {
            if (isOn)
            {
                Open(context);
            }
        }

        private void OnRecipeNameChanged(string text)
        {
            var recipe = Db.DeckRecipeDatabase.GetRecipe(m_deckRecipeUid);
            if (recipe != null)
            {
                recipe.deckData.name = text;
                //Open(m_deckRecipeUid);
            }
        }

        private void OverviewChanged(string text)
        {
            var recipe = Db.DeckRecipeDatabase.GetRecipe(m_deckRecipeUid);
            if (recipe != null)
            {
                recipe.overview = text;
                //Open(m_deckRecipeUid);
            }
        }

        private void PlayGuideChanged(string text)
        {
            var recipe = Db.DeckRecipeDatabase.GetRecipe(m_deckRecipeUid);
            if (recipe != null)
            {
                recipe.howToPlayGuide = text;
                //Open(m_deckRecipeUid);
            }
        }

        private void OnDeckFactionChanged(int index)
        {
            var recipe = Db.DeckRecipeDatabase.GetRecipe(m_deckRecipeUid);
            if (recipe != null)
            {
                recipe.deckData.faction = (Faction)index;
                recipe.deckData.hero = Db.HeroDatabase.GetHeroByFaction(recipe.deckData.faction).id;
                //Open(m_deckRecipeUid);
            }
        }

        private void OnDeckTypeChanged(int index)
        {
            var recipe = Db.DeckRecipeDatabase.GetRecipe(m_deckRecipeUid);
            if (recipe != null)
            {
                recipe.deckType = (DeckRecipeData.DeckType) index;
                //Open(m_deckRecipeUid);
            }
        }

        private void OnMainCardsChanged(List<CardValue> changedCards)
        {
            var recipe = Db.DeckRecipeDatabase.GetRecipe(m_deckRecipeUid);
            if (recipe != null)
            {
                recipe.mainCards = changedCards;
                //Open(m_deckRecipeUid);
            }
        }

        private void OnDeckCardsChanged(List<CardValue> changedCards)
        {
            var recipe = Db.DeckRecipeDatabase.GetRecipe(m_deckRecipeUid);
            if (recipe != null)
            {
                recipe.deckData.cards = changedCards;
                //Open(m_deckRecipeUid);
            }
        }

        private void OnAddButton()
        {
            var newRecipe = Db.DeckRecipeDatabase.CreateNewRecipe();
            Open(newRecipe.Uid);
        }

        private void OnMoveUpButton()
        {
            Db.DeckRecipeDatabase.MoveRecipeUp(m_deckRecipeUid);
            Open(m_deckRecipeUid);
        }

        private void OnMoveDownButton()
        {
            Db.DeckRecipeDatabase.MoveRecipeDown(m_deckRecipeUid);
            Open(m_deckRecipeUid);
        }

        private void OnDeleteRecipe(LayoutItem item)
        {
            var recipeUid = item.mainButton.content;
            Db.DeckRecipeDatabase.RemoveRecipe(recipeUid);
            Open(string.Empty);
        }

        private void OnSaveButton()
        {
            Db.DeckRecipeDatabase.Save();
        }
    }
}