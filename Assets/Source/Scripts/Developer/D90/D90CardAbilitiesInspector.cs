using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using UnityEngine;

namespace CrossBlitz.Developer.D90
{
    public class D90CardAbilitiesInspector : MonoBehaviour
    {
        public D90AbilityEditor abilityEditorPrefab;
        public D90Pages abilitiesPages;
        public D90Pages givenAbilitiesPages;
        public D90Pages conditionalAbilitiesPages;

        private string m_cardId;

        public void Init(string cardId, int abilityPageIndex=-1, int giveAbilityPageIndex=-1, int conditionalAbilityPageIndex=-1)
        {
            m_cardId = cardId;

            var card = Db.CardDatabase.GetCard(m_cardId);

            if (card == null)
            {
                return;
            }

            if (abilityPageIndex >= 0)
            {
                abilitiesPages.OnDeleteButton -= OnDeleteAbility;
                abilitiesPages.OnDeleteButton += OnDeleteAbility;

                abilitiesPages.OnPageChanged -= OnPageChangedAbility;
                abilitiesPages.OnPageChanged += OnPageChangedAbility;

                abilitiesPages.OnAddButton -= OnAddAbility;
                abilitiesPages.OnAddButton += OnAddAbility;

                abilitiesPages.Clear();
                var abilities = card.GetAbilities();

                for (var i = 0; i < abilities.Count; i++)
                {
                    var abilityPage = Instantiate(abilityEditorPrefab, abilitiesPages.pageLayout, false);
                    abilityPage.Init(abilities, i);
                    abilitiesPages.AddPage(abilityPage.gameObject);
                }

                abilitiesPages.SetPageIndex(abilityPageIndex);
            }

            if (giveAbilityPageIndex >= 0)
            {
                givenAbilitiesPages.OnDeleteButton -= OnDeleteGivenAbility;
                givenAbilitiesPages.OnDeleteButton += OnDeleteGivenAbility;

                givenAbilitiesPages.OnPageChanged -= OnPageChangedGivenAbility;
                givenAbilitiesPages.OnPageChanged += OnPageChangedGivenAbility;

                givenAbilitiesPages.OnAddButton -= OnAddGivenAbility;
                givenAbilitiesPages.OnAddButton += OnAddGivenAbility;

                givenAbilitiesPages.Clear();

                var givenAbilities = card.GetGivenAbilities();

                for (var i = 0; i < givenAbilities.Count; i++)
                {
                    var abilityPage = Instantiate(abilityEditorPrefab, givenAbilitiesPages.pageLayout, false);
                    abilityPage.Init(givenAbilities, i);
                    givenAbilitiesPages.AddPage(abilityPage.gameObject);
                }

                givenAbilitiesPages.SetPageIndex(abilityPageIndex);
            }

            if (conditionalAbilityPageIndex >= 0)
            {
                conditionalAbilitiesPages.OnDeleteButton -= OnDeleteConditionalAbility;
                conditionalAbilitiesPages.OnDeleteButton += OnDeleteConditionalAbility;

                conditionalAbilitiesPages.OnPageChanged -= OnPageChangedConditionalAbility;
                conditionalAbilitiesPages.OnPageChanged += OnPageChangedConditionalAbility;

                conditionalAbilitiesPages.OnAddButton -= OnAddConditionalAbility;
                conditionalAbilitiesPages.OnAddButton += OnAddConditionalAbility;

                conditionalAbilitiesPages.Clear();

                var conditionalAbilities = card.GetConditionalAbilities();

                for (var i = 0; i < conditionalAbilities.Count; i++)
                {
                    var abilityPage = Instantiate(abilityEditorPrefab, conditionalAbilitiesPages.pageLayout, false);
                    abilityPage.Init(conditionalAbilities, i);
                    conditionalAbilitiesPages.AddPage(abilityPage.gameObject);
                }

                conditionalAbilitiesPages.SetPageIndex(abilityPageIndex);
            }
        }

        private void OnDeleteAbility(int index)
        {
            var card = Db.CardDatabase.GetCard(m_cardId);

            if (card == null)
            {
                return;
            }

            // if (card.abilities.Count > index)
            // {
            //     card.abilities.RemoveAt(index);
            //     Init(m_cardId, abilityPageIndex:card.abilities.Count > index?index:card.abilities.Count-1);
            // }
        }

        private void OnPageChangedAbility(int index)
        {

        }

        private void OnAddAbility()
        {
            // var card = Db.CardDatabase.GetCard(m_cardId);
            //
            // if (card == null)
            // {
            //     return;
            // }
            //
            // card.abilities.Add( new Ability() );
            // Init(m_cardId, abilityPageIndex:card.abilities.Count-1);
        }


        private void OnDeleteConditionalAbility(int index)
        {
            // var card = Db.CardDatabase.GetCard(m_cardId);
            //
            // if (card == null)
            // {
            //     return;
            // }
            //
            // if (card.conditionalAbilities.Count > index)
            // {
            //     card.conditionalAbilities.RemoveAt(index);
            //     Init(m_cardId, conditionalAbilityPageIndex:card.conditionalAbilities.Count > index?index:card.conditionalAbilities.Count-1);
            // }
        }

        private void OnPageChangedConditionalAbility(int index)
        {

        }

        private void OnAddConditionalAbility()
        {
            // var card = Db.CardDatabase.GetCard(m_cardId);
            //
            // if (card == null)
            // {
            //     return;
            // }
            //
            // card.conditionalAbilities.Add( new Ability() );
            // Init(m_cardId, conditionalAbilityPageIndex:card.conditionalAbilities.Count-1);
        }

        private void OnDeleteGivenAbility(int index)
        {
            // var card = Db.CardDatabase.GetCard(m_cardId);
            //
            // if (card == null)
            // {
            //     return;
            // }
            //
            // if (card.givenAbilities.Count > index)
            // {
            //     card.givenAbilities.RemoveAt(index);
            //     Init(m_cardId, giveAbilityPageIndex:card.givenAbilities.Count > index?index:card.givenAbilities.Count-1);
            // }
        }

        private void OnPageChangedGivenAbility(int index)
        {

        }

        private void OnAddGivenAbility()
        {
            // var card = Db.CardDatabase.GetCard(m_cardId);
            //
            // if (card == null)
            // {
            //     return;
            // }
            //
            // card.givenAbilities.Add( new Ability() );
            // Init(m_cardId, giveAbilityPageIndex:card.givenAbilities.Count-1);
        }
    }
}