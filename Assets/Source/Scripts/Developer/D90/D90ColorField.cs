using System;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90ColorField : MonoBehaviour
    {
        public InputField hexInput;
        public D90IntegerInput alphaField;
        public Image image;

        /// <summary>
        /// Returns the new hex code, alpha and color (with alpha applied)
        /// </summary>
        public event Action<string, float, Color> OnColorChanged;

        private string _hex;
        private float _alpha;

        public string Hex => _hex;
        public float Alpha => _alpha;

        public void Start()
        {
            hexInput.onEndEdit.RemoveAllListeners();
            hexInput.onEndEdit.AddListener(ChangedColor);

            alphaField.OnChanged -= OnAlphaChanged;
            alphaField.OnChanged += OnAlphaChanged;
        }

        public void SetColorWithoutNotify(string hex, float alpha=255f)
        {
            _hex = FormatAndCheckColor(hex);
            hexInput.SetTextWithoutNotify(_hex);

            _alpha = Mathf.Clamp(alpha, 0, 255);
            alphaField.SetTextWithoutNotify(_alpha.ToString());

            var c = _hex.ToColor(_alpha / 255f);
            image.color = c;
        }

        private void ChangedColor(string color)
        {
            _hex = FormatAndCheckColor(color);
            hexInput.SetTextWithoutNotify(_hex);

            var c = _hex.ToColor(_alpha / 255f);
            image.color = c;

            OnColorChanged?.Invoke(_hex, _alpha, c);
        }

        private void OnAlphaChanged(int alpha)
        {
            _alpha = Mathf.Clamp(alpha, 0, 255);
            alphaField.SetTextWithoutNotify(_alpha.ToString());

            var c = _hex.ToColor(_alpha / 255f);
            image.color = c;

            OnColorChanged?.Invoke(_hex, _alpha, c );
        }

        private string FormatAndCheckColor(string hex)
        {
            if (string.IsNullOrEmpty(hex))
            {
                hex = "#FFFFFF";
            }

            var hexR = hex.Replace("#", string.Empty);

            if (!int.TryParse(hexR, System.Globalization.NumberStyles.HexNumber, System.Globalization.CultureInfo.InvariantCulture, out var res))
            {
                hex = "#FFFFFF";
            }

            hex = hex.ToUpper();

            if (!hex.StartsWith("#"))
            {
                hex = "#" + hex;
            }

            return hex;
        }
    }
}