using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90AccoladeInspector : MonoBehaviour
    {
        public Dropdown accoladeType;
        public InputField dawnDollarAmount;
        public InputField description;
        public InputField score;
        public D90DatabaseDropdown cardDatabase;
        public D90DeckInspector rewards;
        public Button regenerateDescriptionButton;
        public Button copyButton;
        public Button pasteButton;

        private string m_battleUid;
        private string m_accoladeUid;

        public event Action<string> OnBattleChanged;

        public void SetAccolade(string battleUid, string accoladeUid)
        {
            m_battleUid = battleUid;
            m_accoladeUid = accoladeUid;
            cardDatabase.SetActive(false);

            if (string.IsNullOrEmpty(m_battleUid) || string.IsNullOrEmpty(m_accoladeUid))
            {
                return;
            }

            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);

            if (battle == null)
            {
                return;
            }

            var accolade = battle.GetAccolade(m_accoladeUid);

            if (accolade == null)
            {
                return;
            }

            var accoladeTypes = Enum.GetNames(typeof(AccoladeType)).ToList();
            accoladeType.ClearOptions();
            accoladeType.AddOptions(accoladeTypes);
            accoladeType.onValueChanged.RemoveAllListeners();
            accoladeType.onValueChanged.AddListener(OnAccoladeTypeChanged);
            accoladeType.SetValueWithoutNotify((int)accolade.Type);

            dawnDollarAmount.SetTextWithoutNotify(accolade.DawnDollarReward.ToString());
            dawnDollarAmount.onEndEdit.RemoveAllListeners();
            dawnDollarAmount.onEndEdit.AddListener(OnEditDawnDollarAmount);

            description.SetTextWithoutNotify(accolade.Description);
            description.onEndEdit.RemoveAllListeners();
            description.onEndEdit.AddListener(OnEditDescription);

            score.SetTextWithoutNotify(accolade.Score.ToString());
            score.onEndEdit.RemoveAllListeners();
            score.onEndEdit.AddListener(OnScoreChanged);

            if (accolade.ItemRewards == null)
            {
                accolade.ItemRewards = new List<ItemValue>();
            }

            if (rewards)
            {
                rewards.OnCardsChanged -= OnRewardsChanged;
                rewards.OnCardsChanged += OnRewardsChanged;
                rewards.SetupCards(accolade.ItemRewards.Select(i => new CardValue { id = i.ItemUid, count = i.Count })
                    .ToList());
            }

            regenerateDescriptionButton.onClick.RemoveAllListeners();
            regenerateDescriptionButton.onClick.AddListener(RegenerateDescription);

            if (accolade.Type == AccoladeType.WinWithAtLeastXNumberOfCardCopiesInYourDeck || accolade.Type == AccoladeType.DestroySpecificMinionInASpecificNumberOfTurns)
            {
                cardDatabase.SetActive(true);
                cardDatabase.Init();
                var card = Db.CardDatabase.GetCard(accolade.CardId);
                cardDatabase.SetDropdownWithoutNotify( card == null ? string.Empty : card.id );
                cardDatabase.OnValueChanged -= OnCardIdChanged;
                cardDatabase.OnValueChanged += OnCardIdChanged;
            }

            copyButton.onClick.RemoveAllListeners();
            copyButton.onClick.AddListener(OnCopy);

            pasteButton.onClick.RemoveAllListeners();
            pasteButton.onClick.AddListener(OnPaste);
        }

        private void OnCopy()
        {
            if (string.IsNullOrEmpty(m_battleUid) || string.IsNullOrEmpty(m_accoladeUid))
            {
                return;
            }

            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);

            if (battle == null)
            {
                return;
            }

            GUIUtility.systemCopyBuffer = battle.ToJson(true);
        }

        private void OnPaste()
        {
            if (string.IsNullOrEmpty(GUIUtility.systemCopyBuffer))
            {
                return;
            }

            if (string.IsNullOrEmpty(m_battleUid) || string.IsNullOrEmpty(m_accoladeUid))
            {
                return;
            }

            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);

            if (battle == null)
            {
                return;
            }


            BattleData copiedBattleData;

            try
            {
                copiedBattleData = UniqueItem.FromJson<BattleData>(GUIUtility.systemCopyBuffer, true);
            }
            catch
            {
                return;
            }

            battle.Accolades = new List<AccoladeData>();

            for (var i = 0; i < copiedBattleData.Accolades.Count; i++)
            {
                var accolade = GameUtilities.Copy( copiedBattleData.Accolades[i] );
                accolade.UpdateIdentifier();
                battle.Accolades.Add(accolade);

                if (i == 0)
                {
                    m_accoladeUid = accolade.Uid;
                }
            }

            OnBattleChanged?.Invoke(m_battleUid);
        }

        private void OnCardIdChanged(D90DatabaseDropdown dropdown, int index)
        {
            var cardName = dropdown.dropdown.options[index];
            var card = Db.CardDatabase.GetCardByName(cardName.text);

            if (card != null)
            {
                if (string.IsNullOrEmpty(m_battleUid) || string.IsNullOrEmpty(m_accoladeUid))
                {
                    return;
                }

                var battle = Db.BattleDatabase.GetBattleData(m_battleUid);

                if (battle == null)
                {
                    return;
                }

                var accolade = battle.GetAccolade(m_accoladeUid);
                accolade.CardId = card.id;
            }
        }

        private void RegenerateDescription()
        {
            if (string.IsNullOrEmpty(m_battleUid) || string.IsNullOrEmpty(m_accoladeUid))
            {
                return;
            }

            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);

            if (battle == null)
            {
                return;
            }

            var accolade = battle.GetAccolade(m_accoladeUid);

            if (accolade == null)
            {
                return;
            }

            accolade.RegenerateDescription();

            SetAccolade(m_battleUid, m_accoladeUid);
        }

        private void OnScoreChanged(string text)
        {
            if (string.IsNullOrEmpty(m_battleUid) || string.IsNullOrEmpty(m_accoladeUid))
            {
                return;
            }

            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);

            if (battle == null)
            {
                return;
            }

            var accolade = battle.GetAccolade(m_accoladeUid);

            if (int.TryParse(text, out var value))
            {
                accolade.Score = value;
            }
        }

        private void OnRewardsChanged(List<CardValue> cardValue)
        {
            if (string.IsNullOrEmpty(m_battleUid) || string.IsNullOrEmpty(m_accoladeUid))
            {
                return;
            }

            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);

            if (battle == null)
            {
                return;
            }

            var accolade = battle.GetAccolade(m_accoladeUid);

            accolade.ItemRewards = cardValue.Select(i => new ItemValue { ItemUid = i.id, Count = i.count }).ToList();
        }

        private void OnAccoladeTypeChanged(int value)
        {
            if (string.IsNullOrEmpty(m_battleUid) || string.IsNullOrEmpty(m_accoladeUid))
            {
                return;
            }

            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);

            if (battle == null)
            {
                return;
            }

            var accolade = battle.GetAccolade(m_accoladeUid);

            accolade.Type = (AccoladeType) value;
            accolade.RegenerateDescription();
            SetAccolade(m_battleUid, m_accoladeUid);
        }

        private void OnEditDawnDollarAmount(string newAmount)
        {
            if (string.IsNullOrEmpty(m_battleUid) || string.IsNullOrEmpty(m_accoladeUid))
            {
                return;
            }

            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);

            if (battle == null)
            {
                return;
            }

            var accolade = battle.GetAccolade(m_accoladeUid);

            if (int.TryParse(newAmount, out var amount))
            {
                accolade.DawnDollarReward = amount;
            }
        }

        private void OnEditDescription(string newDescription)
        {
            if (string.IsNullOrEmpty(m_battleUid) || string.IsNullOrEmpty(m_accoladeUid))
            {
                return;
            }

            var battle = Db.BattleDatabase.GetBattleData(m_battleUid);

            if (battle == null)
            {
                return;
            }

            var accolade = battle.GetAccolade(m_accoladeUid);

            accolade.Description = newDescription;
        }

        private void Save()
        {
            Db.FablesDatabase.Save();
        }
    }
}