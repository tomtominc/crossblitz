using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Tools;
using CrossBlitz.UI.Widgets;
using float_oat.Desktop90;
using GameDataEditor;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90BookEditor : MonoBehaviour
    {
        public D90ToggleGroup categoryToggles;
        public InputField bookName;
        public D90DatabaseDropdown bookPrologue;
        public Text bookPage;
        public D90Button addBookButton;
        public D90Button deleteBookButton;
        public D90Button nextBookButton;
        public D90Button previousBookButton;
        public D90Pages chaptersPage;
        public D90ChapterPage chapterPagePrefab;
        public D90ContentLayout itemContentLayout;
        public LayoutItem itemTextPrefab;
        public D90DatabaseDropdown addItemDropdown;
        public Button saveButton;

        private FableBookData _currentBook;

        private void OnCategoryToggled(D90ToggleButton toggle)
        {
            if (toggle.name.Contains(GDEItemKeys.Hero_Redcroft))
            {
                SetupBook(GDEItemKeys.Hero_Redcroft, 1);
            }
            else if (toggle.name.Contains(GDEItemKeys.Hero_Mereena))
            {
                SetupBook(GDEItemKeys.Hero_Mereena, 1);
            }
            else if (toggle.name.Contains(GDEItemKeys.Hero_Quill))
            {
                SetupBook(GDEItemKeys.Hero_Quill, 1);
            }
            else if (toggle.name.Contains(GDEItemKeys.Hero_Seto))
            {
                SetupBook(GDEItemKeys.Hero_Seto, 1);
            }
            else if (toggle.name.Contains(GDEItemKeys.Hero_Violet))
            {
                SetupBook(GDEItemKeys.Hero_Violet, 1);
            }
        }

        private void SetupBook(string bookHero, int bookNumber)
        {
            _currentBook = Db.FablesDatabase.GetOrCreateBook(bookHero, bookNumber);

            if (_currentBook.Chapters.Count == 0)
            {
                Db.FablesDatabase.AddChapter(new CreateNewFableParameters
                {
                    hero = bookHero,
                    bookNumber = bookNumber,
                    chapterNumber = 1
                });
            }

            bookName.SetTextWithoutNotify(_currentBook.BookName);
            bookPrologue.SetDropdownWithoutNotify(_currentBook.BookPrologue);

            // if (_currentBook.ShopItems == null)
            // {
            //     _currentBook.ShopItems = new List<ItemValue>();
            // }
            //
            // for (var i = 0; i < _currentBook.ShopItems.Count; i++)
            // {
            //     var itemValue = _currentBook.ShopItems[i];
            //     AddItemReward(itemValue);
            // }

            chaptersPage.Clear();

            for (var i = 0; i < _currentBook.Chapters.Count; i++)
            {
                var chapterPage = Instantiate(chapterPagePrefab, chaptersPage.pageLayout);
                chapterPage.SetChapterData(_currentBook.Chapters[i].Uid);
                chaptersPage.AddPage(chapterPage.gameObject);
            }

            chaptersPage.SetPageIndex(0);
        }

        private void AddItemReward(ItemValue itemValue)
        {
            var item = Db.ItemDatabase.GetItem(itemValue.ItemUid);
            var itemContent = Instantiate(itemTextPrefab, itemContentLayout.contentLayout);
            itemContent.mainButton.content = itemValue.ItemUid;
            itemContent.textLabels[0].text = item.DisplayName;
            itemContent.textLabels[1].text = $"x{itemValue.Count}";
            itemContentLayout.AddItem(itemContent);
        }

        private void OnBookNameChanged(string newName)
        {
            _currentBook.BookName = newName;
        }

        private void OnBookPrologueChanged(D90DatabaseDropdown dropdown, int value)
        {
            var cutscene = Db.CutsceneDatabase.GetCutsceneByEventTitle(dropdown.dropdown.options[value].text);
            _currentBook.BookPrologue = cutscene.Uid;
        }

        private void OnBookAdded()
        {
            var book = Db.FablesDatabase.AddNewBook(_currentBook.BookHero);
            SetupBook(book.BookHero, book.BookNumber);
        }

        private void OnBookDeleted()
        {
            Db.FablesDatabase.DeleteBook(_currentBook.BookHero, _currentBook.BookNumber);
            SetupBook(_currentBook.BookHero, 1);
        }

        private void OnNextBook()
        {
            SetupBook(_currentBook.BookHero, _currentBook.BookNumber+1);
        }

        private void OnPreviousBook()
        {
            SetupBook(_currentBook.BookHero, _currentBook.BookNumber-1);
        }

        private void OnAddNewChapter()
        {
            Db.FablesDatabase.AddChapter(new CreateNewFableParameters
            {
                hero = _currentBook.BookHero,
                bookNumber = _currentBook.BookNumber,
                chapterNumber = _currentBook.Chapters.Count + 1
            });

            var chapterPage = Instantiate(chapterPagePrefab, chaptersPage.pageLayout);
            chapterPage.SetChapterData(_currentBook.Chapters[_currentBook.Chapters.Count-1].Uid);
            chaptersPage.AddPage(chapterPage.gameObject);
        }

        private void OnDeleteChapter(int index)
        {
            Db.FablesDatabase.DeleteChapter(_currentBook.Chapters[index].Uid);
        }

        public void Open()
        {
            SetupBook(GDEItemKeys.Hero_Redcroft, 1);

            categoryToggles.OnToggle -= OnCategoryToggled;
            categoryToggles.OnToggle += OnCategoryToggled;

            bookName.onEndEdit.RemoveAllListeners();
            bookName.onEndEdit.AddListener(OnBookNameChanged);

            bookPrologue.OnValueChanged -= OnBookPrologueChanged;
            bookPrologue.OnValueChanged += OnBookPrologueChanged;

            addBookButton.onClick.RemoveAllListeners();
            addBookButton.onClick.AddListener(OnBookAdded);

            deleteBookButton.onClick.RemoveAllListeners();
            deleteBookButton.onClick.AddListener(OnBookDeleted);

            nextBookButton.onClick.RemoveAllListeners();
            nextBookButton.onClick.AddListener(OnNextBook);

            previousBookButton.onClick.RemoveAllListeners();
            previousBookButton.onClick.AddListener(OnPreviousBook);

            chaptersPage.OnAddButton -= OnAddNewChapter;
            chaptersPage.OnAddButton += OnAddNewChapter;

            chaptersPage.OnDeleteButton -= OnDeleteChapter;
            chaptersPage.OnDeleteButton += OnDeleteChapter;

            saveButton.onClick.RemoveAllListeners();
            saveButton.onClick.AddListener(Save);
        }

        private void Update()
        {
            if (_currentBook != null)
            {
                nextBookButton.enabled = _currentBook.BookNumber - 1 < Db.FablesDatabase.GetBookCountByHero(_currentBook.BookHero) - 2;
                previousBookButton.enabled = _currentBook.BookNumber - 1 > 0;
                deleteBookButton.enabled = Db.FablesDatabase.GetBookCountByHero(_currentBook.BookHero) > 1;
                chaptersPage.deleteButton.enabled = _currentBook.Chapters.Count > 1;
                bookPage.text = $"{chaptersPage.CurrentIndex + 1}/{_currentBook.Chapters.Count}";
            }
        }

        private void Save()
        {
            Db.FablesDatabase.Save();
        }
    }
}