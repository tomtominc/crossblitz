using System;
using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.UI.Widgets;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90ItemValueEditor : MonoBehaviour
    {
        public D90ContentLayout itemLayout;
        public LayoutItem itemTextPrefab;
        public D90DatabaseDropdown addItemDropdown;
        public Button copyButton;
        public Button pasteButton;
        public Button moveUpButton;
        public Button moveDownButton;
        private List<ItemValue> m_items;
        private string m_currentItemId;
        public event Action<List<ItemValue>> OnItemsChanged;

        public void Setup(List<ItemValue> items)
        {
            m_items = items;
            m_items ??= new List<ItemValue>();

            itemLayout.Clear();
            itemLayout.deleteHandledOutsideOfClass = true;
            itemLayout.OnDelete -= ItemDeleted;
            itemLayout.OnDelete += ItemDeleted;

            for (var i = 0; i < m_items.Count; i++)
            {
                AddItem(m_items[i]);
            }

            if (addItemDropdown)
            {
                addItemDropdown.InitSoft();
                addItemDropdown.OnAddButton -= AddItemFromDropdown;
                addItemDropdown.OnAddButton += AddItemFromDropdown;
            }

            copyButton.onClick.RemoveAllListeners();
            copyButton.onClick.AddListener(OnCopyItems);

            pasteButton.onClick.RemoveAllListeners();
            pasteButton.onClick.AddListener(OnPasteItems);

            if (moveUpButton)
            {
                moveUpButton.onClick.RemoveAllListeners();
                moveUpButton.onClick.AddListener(OnMoveUp);
            }

            if (moveDownButton)
            {
                moveDownButton.onClick.RemoveAllListeners();
                moveDownButton.onClick.AddListener(OnMoveDown);
            }
        }

        private void OnMoveUp()
        {
            var item = itemLayout.CurrentItem;
            if (item == null) return;

            var itemId = item.mainButton.content;
            var index = m_items.IndexOf( m_items.Find(c => c.ItemUid == itemId) );

            if (index > 0)
            {
                (m_items[index-1], m_items[index]) = (m_items[index], m_items[index-1]);

                Setup(m_items);
                OnItemsChanged?.Invoke(m_items);
            }
        }

        private void OnMoveDown()
        {
            var item = itemLayout.CurrentItem;

            if (item == null) return;

            var itemId = item.mainButton.content;
            var index = m_items.IndexOf( m_items.Find(c => c.ItemUid == itemId) );

            if (index >= 0 && index < m_items.Count-1)
            {
                (m_items[index+1], m_items[index]) = (m_items[index], m_items[index+1]);

                Setup(m_items);
                OnItemsChanged?.Invoke(m_items);
            }
        }

        private void ItemDeleted(LayoutItem cardItem)
        {
            var itemId = cardItem.mainButton.content;
            var item = m_items.Find(i => i.ItemUid == itemId);

            if (item !=null)
            {
                item.Count--;

                if (item.Count <= 0)
                {
                    m_items.Remove(item);
                }
            }

            Setup(m_items);
            OnItemsChanged?.Invoke(m_items);
        }

        private void AddItemFromDropdown(D90DatabaseDropdown dropdown)
        {
            var itemDisplayName = dropdown.dropdown.options[ dropdown.dropdown.value ].text;
            var itemData = Db.ItemDatabase.GetItemByName(itemDisplayName);

            if (itemData != null)
            {
                var itemValue = m_items.Find(i => i.ItemUid == itemData.ItemId);

                if (itemValue != null && itemValue.ItemUid == itemData.ItemId)
                {
                    itemValue.Count++;
                }
                else
                {
                    itemValue = new ItemValue
                    {
                        ItemUid = itemData.ItemId,
                        Count = 1
                    };

                    m_items.Add(itemValue);
                }

                OnItemsChanged?.Invoke(m_items);
                Setup(m_items);
            }
        }

        private void AddItem(ItemValue value)
        {
            if (!Db.Initialized)
            {
                Db.Initialize();
            }

            var item = Db.ItemDatabase.GetItem(value.ItemUid);

            if (item == null)
            {
                return;
            }

            if (string.IsNullOrEmpty(m_currentItemId))
            {
                m_currentItemId = item.ItemId;
            }

            var itemText = Instantiate(itemTextPrefab, itemLayout.contentLayout);
            itemText.mainButton.content = value.ItemUid;
            itemText.mainButton.Toggle.SetIsOnWithoutNotify( m_currentItemId == item.ItemId );
            itemText.mainButton.OnValueChanged -= OnItemToggled;
            itemText.mainButton.OnValueChanged += OnItemToggled;
            itemText.textLabels[0].text = item.DisplayName;
            itemText.textLabels[1].text = $"x{value.Count}";

            itemLayout.AddItem(itemText);

            if (itemLayout.CurrentItem == null && !string.IsNullOrEmpty(m_currentItemId))
            {
                itemLayout.OnValueChanged(true, m_currentItemId);
            }
        }

        private void OnItemToggled(bool isOn, string context)
        {
            if (isOn)
            {
                m_currentItemId = context;
            }
        }

        private void OnCopyItems()
        {
            var newItemCopyPasteData = new ItemCopyPasteData();
            newItemCopyPasteData.items = new List<ItemValue>(m_items);

            GUIUtility.systemCopyBuffer = newItemCopyPasteData.ToJson();
        }

        private void OnPasteItems()
        {
            if (!string.IsNullOrEmpty(GUIUtility.systemCopyBuffer))
            {
                try
                {
                    var itemData = ItemCopyPasteData.FromJson(GUIUtility.systemCopyBuffer);
                    if (itemData != null)
                    {
                        Setup(itemData.items);
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError($"Could not copy items: {e}");
                }
            }
        }
    }

    [Serializable]
    public class ItemCopyPasteData
    {
        public List<ItemValue> items;

        public string ToJson()
        {
            return JsonUtility.ToJson(this);
        }

        public static ItemCopyPasteData FromJson(string json)
        {
            return JsonUtility.FromJson<ItemCopyPasteData>(json);
        }
    }
}