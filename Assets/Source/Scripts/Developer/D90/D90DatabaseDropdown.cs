using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.AssetManagement;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Data;
using CrossBlitz.Items.Data;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Developer.D90
{
    public enum DatabaseType
    {
        CardDatabase,
        HeroDatabase,
        DeckDatabase,
        ShopsDatabase,
        ItemDatabase,
        FablesDatabase,
        CutsceneDatabase,
        FableTiles,
        CharacterDatabase, // contains cards and heroes
        AudioDatabase,
        MapDatabase,
        FableQuestDatabase,
        AmbientDatabase,
        ShopRoomsDatabase,
        BattleDatabase,
        IngredientDatabase,
        IngredientsAndRecipesDatabase,
        BookDatabase,
        CardsAndItemsDatabase,
        Animator,
        CardPools
    }
    public class D90DatabaseDropdown : MonoBehaviour
    {
        public DatabaseType databaseType;
        public Dropdown dropdown;
        public Button editButton;
        public bool createNewOpensMenu;
        public Button createNewButton;
        public Button addButton;

        public event Action OnNewButton;
        public event Action<D90DatabaseDropdown> OnAddButton;
        public event Action<D90DatabaseDropdown, int> OnValueChanged;
        private bool _init;

        private void Start()
        {
            if (!_init)
            {
                Init();
            }
        }

        public void InitSoft()
        {
            if (!_init)
            {
                Init();
            }
        }

        public void Init()
        {
            if (editButton)
            {
                editButton.onClick.RemoveAllListeners();
                editButton.onClick.AddListener(EditButtonPressed);
            }

            if (addButton)
            {
                addButton.onClick.RemoveAllListeners();
                addButton.onClick.AddListener(AddButtonPressed);
            }

            if (createNewButton)
            {
                createNewButton.onClick.RemoveAllListeners();
                createNewButton.onClick.AddListener(CreateNewPressed);
            }

            if (dropdown)
            {
                dropdown.onValueChanged.RemoveAllListeners();
                dropdown.onValueChanged.AddListener(DropdownValueChanged);
            }

            OnDatabaseSetup();

            _init = true;
        }

        private void DropdownValueChanged(int value)
        {
            OnValueChanged?.Invoke(this, value);
        }

        public void SetDropdownWithoutNotify(string value)
        {
            if (!_init) Init();

            var index = dropdown.options.FindIndex(option => option.text == value);
            if (index < 0) index = 0;
            dropdown.SetValueWithoutNotify(index);
        }

        private void Update()
        {
            if (addButton)
            {
                addButton.interactable = dropdown.options != null &&
                                         dropdown.value > 0 &&
                                         dropdown.options.Count > dropdown.value ;
            }
        }

        private void AddButtonPressed()
        {
            OnAddButton?.Invoke(this);
        }

        private void CreateNewPressed()
        {
            OnNewButton?.Invoke();

            if (createNewOpensMenu)
            {
                switch (databaseType)
                {
                    case DatabaseType.CardDatabase:
                        break;
                    case DatabaseType.HeroDatabase:
                        break;
                    case DatabaseType.DeckDatabase:
                        break;
                    case DatabaseType.ShopsDatabase:
                        break;
                    case DatabaseType.ItemDatabase:
                        break;
                    case DatabaseType.FablesDatabase:
                        break;
                    case DatabaseType.CutsceneDatabase:
                        Events.Publish(this, EventType.OpenCutsceneEditor,
                            new OpenCutsceneEditorEventArgs {createNewCutscene = true});
                        break;
                    case DatabaseType.FableTiles:
                        break;
                }
            }
        }

        private void EditButtonPressed()
        {
            switch (databaseType)
            {
                case DatabaseType.CardDatabase:
                    if (dropdown.value > 0 && dropdown.value < dropdown.options.Count)
                    {
                        var value = dropdown.options[dropdown.value];
                        var cardId = Db.CardDatabase.GetCardIdFromName(value.text);
                        Events.Publish(this, EventType.OpenCardEditorInstance, new OpenCardEditorInstanceEventArgs{ cardId = cardId });
                    }
                    else
                    {
                        Events.Publish(this, EventType.OnError, new OnErrorEventArgs
                        {
                            errorTitle = "Card Id Not Valid",
                            errorMessage = "The card that you are trying to edit could not be found."
                        });
                    }
                    break;
                case DatabaseType.HeroDatabase:
                    break;
                case DatabaseType.CharacterDatabase:
                    if (dropdown.value > 0 && dropdown.value < dropdown.options.Count)
                    {
                        var value = dropdown.options[dropdown.value];
                        var character = Db.CharacterDatabase.GetCharacterByName(value.text);
                        if (character != null)
                        {
                            Events.Publish(this, EventType.OpenCardEditorInstance,
                                new OpenCardEditorInstanceEventArgs {cardId = character.id});
                        }
                    }
                    else
                    {
                        Events.Publish(this, EventType.OnError, new OnErrorEventArgs
                        {
                            errorTitle = "Card Id Not Valid",
                            errorMessage = "The card that you are trying to edit could not be found."
                        });
                    }
                    break;
                case DatabaseType.DeckDatabase:
                    break;
                case DatabaseType.ShopsDatabase:
                    break;
                case DatabaseType.ItemDatabase:
                    break;
                case DatabaseType.FablesDatabase:
                    break;
                case DatabaseType.AudioDatabase:
                {
                    D90Tools.Instance.OpenAudioDatabase();
                    break;
                }
                case DatabaseType.CutsceneDatabase:
                    if (dropdown.value > 0 && dropdown.value < dropdown.options.Count)
                    {
                        var value = dropdown.options[dropdown.value];
                        var cutscene = Db.CutsceneDatabase.GetCutsceneByEventTitle(value.text);
                        Events.Publish(this, EventType.OpenCutsceneEditor, new OpenCutsceneEditorEventArgs{ cutsceneUid = cutscene.Uid });
                    }
                    else
                    {
                        Events.Publish(this, EventType.OnError, new OnErrorEventArgs
                        {
                            errorTitle = "Cutscene Id Not Valid",
                            errorMessage = "The cutscene that you are trying to edit could not be found."
                        });
                    }
                    break;
                case DatabaseType.FableTiles:
                    if (dropdown.value > 0 && dropdown.value < dropdown.options.Count)
                    {
                        var value = dropdown.options[dropdown.value];
                        var tileSplit = value.text.Split('/');

                        if (tileSplit.Length >= 4)
                        {
                            var tileName = tileSplit[3];
                            var tile = Db.MapDatabase.GetTileByName(tileName);
                            var tileUid = tile?.uid ?? 0;

                            if (tileUid != 0)
                            {
                                Events.Publish(this, EventType.OpenTileEditor,
                                    new OpenTileEditorEventArgs() {tileUid = tileUid});
                            }
                        }

                    }
                    else
                    {
                        Events.Publish(this, EventType.OnError, new OnErrorEventArgs
                        {
                            errorTitle = "Tile Id Not Valid",
                            errorMessage = "The tile that you are trying to edit could not be found."
                        });
                    }
                    break;
                case DatabaseType.MapDatabase:
                    D90Tools.Instance.OpenMapDatabase();
                    break;
                case DatabaseType.BattleDatabase:
                    if (dropdown.value > 0 && dropdown.value < dropdown.options.Count)
                    {
                        var value = dropdown.options[dropdown.value].text;
                        var battle = Db.BattleDatabase.GetBattleDataByName(value);
                        if (battle != null)
                        {
                            D90Tools.Instance.OpenBattleDatabase(battle.Uid);
                        }
                    }

                    break;
            }
        }

        public void OnDatabaseSetup()
        {
            switch (databaseType)
            {
                case DatabaseType.CardDatabase:
                {
                    var cards = Db.CardDatabase.Cards.OrderBy(card => card.name).ToList();
                    var cardNames = new List<string> {"None"};
                    cardNames.AddRange(cards.Select(card => card.name));
                    dropdown.ClearOptions();
                    dropdown.AddOptions(cardNames);
                    break;
                }
                case DatabaseType.CardsAndItemsDatabase:
                {
                    var cards = Db.CardDatabase.Cards.OrderBy(card => card.name).ToList();
                    var cardNames = new List<string> {"None"};
                    cardNames.AddRange(cards.Select(card => card.name));

                    var ingredients = Db.ItemDatabase.GetItemsOfType(ItemClassType.Ingredient);
                    var recipes = Db.ItemDatabase.GetItemsOfType(ItemClassType.Recipe);
                    var itemsByName = new List<string> {"None"};
                    itemsByName.AddRange(ingredients.Select(item => item.DisplayName));
                    itemsByName.AddRange(recipes.Select(item => item.DisplayName));

                    dropdown.ClearOptions();
                    dropdown.AddOptions(cardNames);
                    dropdown.AddOptions(itemsByName);

                    break;
                }
                case DatabaseType.HeroDatabase:
                {
                    var heroes = Db.HeroDatabase.Heroes;
                    var heroNames = new List<string> {"None"};
                    heroNames.AddRange(heroes.Select(hero => hero.name));
                    dropdown.ClearOptions();
                    dropdown.AddOptions(heroNames);
                    break;
                }
                case DatabaseType.CharacterDatabase:
                {
                    var characters = Db.CharacterDatabase.GetAllCharacters(c =>
                        c.type == CardType.Power || c.type == CardType.Relic || c.type == CardType.Resource ||
                        c.type == CardType.Spell || c.type == CardType.Trick);
                    var characterNames = new List<string> {"None"};
                    characterNames.AddRange(characters.Select(c => c.name));
                    dropdown.ClearOptions();
                    dropdown.AddOptions(characterNames);
                    break;
                }
                case DatabaseType.DeckDatabase:
                {
                    var decks = Db.DeckRecipeDatabase.deckRecipes;
                    var decksByName = new List<string> { "None" };
                    decksByName.AddRange( decks.Select( deck => deck.deckData.name ));
                    dropdown.ClearOptions();
                    dropdown.AddOptions(decksByName);
                    break;
                }
                case DatabaseType.ShopsDatabase:
                {
                    var shops = Db.ShopsDatabase.Shops;
                    var shopsByName = new List<string> {"None"};
                    shopsByName.AddRange(shops.Select(shop => shop.displayName));
                    dropdown.ClearOptions();
                    dropdown.AddOptions(shopsByName);
                    break;
                }
                case DatabaseType.ItemDatabase:
                {
                    var items = Db.ItemDatabase.Items;
                    var itemsByName = new List<string> {"None"};
                    itemsByName.AddRange(items.Select(item => item.DisplayName).OrderBy(item => item));
                    dropdown.ClearOptions();
                    dropdown.AddOptions(itemsByName);
                    break;
                }
                case DatabaseType.FablesDatabase:
                {
                    var chaptersByName = new List<string> {"None"};
                    chaptersByName.AddRange(Db.FablesDatabase.GetBookDirectories());
                    dropdown.ClearOptions();
                    dropdown.AddOptions(chaptersByName);
                    break;
                }
                case DatabaseType.BookDatabase:
                {
                    var booksByName = new List<string> { "None" };
                    booksByName.AddRange( Db.FablesDatabase.Books.Select(b => b.BookId));
                    dropdown.ClearOptions();
                    dropdown.AddOptions(booksByName);
                    break;
            }
                case DatabaseType.CutsceneDatabase:
                {
                    var cutscenes = Db.CutsceneDatabase.Cutscenes;
                    var cutsceneNames = new List<string> {"None"};
                    cutsceneNames.AddRange(cutscenes.Select(cutscene => cutscene.EventTitle));
                    dropdown.ClearOptions();
                    dropdown.AddOptions(cutsceneNames);
                    break;
                }
                case DatabaseType.FableTiles:
                {
                    var fableDirectories = new List<string> {"None"};
                    fableDirectories.AddRange(Db.MapDatabase.GetAllTileDirectories());
                    dropdown.ClearOptions();
                    dropdown.AddOptions(fableDirectories);
                    break;
                }
                case DatabaseType.AudioDatabase:
                {
                    var allAudio = AudioController.GetAllAudioByCategory();
                    var musicAudio = allAudio["MUSIC"];
                    var musicNames = new List<string> { "None" };
                    musicNames.AddRange(musicAudio);

                    dropdown.ClearOptions();
                    dropdown.AddOptions(musicNames);

                    break;
                }
                case DatabaseType.AmbientDatabase:
                {
                    var allAudio = AudioController.GetAllAudioByCategory();
                    var musicAudio = allAudio["AMBIENCE"];
                    var musicNames = new List<string> { "None" };
                    musicNames.AddRange(musicAudio);

                    dropdown.ClearOptions();
                    dropdown.AddOptions(musicNames);
                    break;
                }
                case DatabaseType.MapDatabase:
                {
                    var maps = Db.MapDatabase.MapData.Select( m => m.DisplayName).ToList();
                    var mapNames = new List<string> {"None"};
                    mapNames.AddRange(maps);

                    dropdown.ClearOptions();
                    dropdown.AddOptions(mapNames);

                    break;
                }
                case DatabaseType.FableQuestDatabase:
                {
                    var quests = Db.FableQuestDatabase.Quests.Select(q => q.Name).ToList();
                    var questNames = new List<string> {"None"};
                    questNames.AddRange(quests);
                    dropdown.ClearOptions();
                    dropdown.AddOptions(questNames);
                    break;
                }
                case DatabaseType.ShopRoomsDatabase:
                {
                    var shopRooms = Db.MapDatabase.GetRoomsWithTileType(NodeType.Shop);
                    var shopRoomNames = new List<string> {"None"};
                    shopRoomNames.AddRange(shopRooms.Select(r => r.DisplayName));
                    dropdown.ClearOptions();
                    dropdown.AddOptions(shopRoomNames);
                    break;
                }
                case DatabaseType.BattleDatabase:
                {
                    var battles = Db.BattleDatabase.Battles.Select(b => b.BattleName);
                    var battleNames = new List<string> {"None"};
                    battleNames.AddRange(battles);
                    dropdown.ClearOptions();
                    dropdown.AddOptions(battleNames);
                    break;
                }
                case DatabaseType.IngredientDatabase:
                {
                    var items = Db.ItemDatabase.GetItemsOfType(ItemClassType.Ingredient);
                    var itemsByName = new List<string> {"None"};
                    itemsByName.AddRange(items.Select(item => item.DisplayName));
                    dropdown.ClearOptions();
                    dropdown.AddOptions(itemsByName);
                    break;
                }
                case DatabaseType.IngredientsAndRecipesDatabase:
                {
                    var ingredients = Db.ItemDatabase.GetItemsOfType(ItemClassType.Ingredient);
                    var recipes = Db.ItemDatabase.GetItemsOfType(ItemClassType.Recipe);
                    var itemsByName = new List<string> {"None"};
                    itemsByName.AddRange(ingredients.Select(item => item.DisplayName));
                    itemsByName.AddRange(recipes.Select(item => item.DisplayName));
                    dropdown.ClearOptions();
                    dropdown.AddOptions(itemsByName);
                    break;
                }
                case DatabaseType.Animator:
                {
                    var animators = AddressableReferenceLoader.GetAllAnimationAssets();
                    var itemsByName = new List<string> {"None"};
                    itemsByName.AddRange(animators.Select(anim => anim.Key));
                    dropdown.ClearOptions();
                    dropdown.AddOptions(itemsByName);
                    break;
                }
                case DatabaseType.CardPools:
                {
                    var pools = Db.CardPoolsDatabase.cardPools;
                    var poolsByName = new List<string> { "None" };
                    poolsByName.AddRange( pools.Select( p => p.name ));
                    dropdown.ClearOptions();
                    dropdown.AddOptions(poolsByName);
                    break;
                }
            }
        }
    }
}