using System;
using System.Linq;
using CrossBlitz.AssetManagement;
using CrossBlitz.Databases;
using CrossBlitz.UI.Widgets;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90VfxFrameMenu : MonoBehaviour
    {
        public LayoutItem itemPrefab;
        public D90ContentLayout vfxFramesList;
        public D90ContentLayout vfxEventList;

        public Button addButton;
        public Dropdown eventType;
        public Dropdown targetType;
        public D90IntegerInput skipUntil;
        public D90ColorField colorField;
        public D90IntegerInput positionX;
        public D90IntegerInput positionY;
        public D90IntegerInput scaleX;
        public D90IntegerInput scaleY;
        public D90IntegerInput rotationX;
        public D90IntegerInput rotationY;
        public D90IntegerInput rotationZ;
        public InputField objectToCreate;
        public InputField sortingLayer;
        public D90IntegerInput sortingOrder;
        public InputField customEvent;
        public InputField customEventArgs;
        public Button addEventButton;

        private string m_vfxUid;
        private string m_vfxObjectUid;
        private string m_vfxFrameUid;
        private string m_vfxEventUid;

        public void RefreshMenu(string vfxUid, string vfxObjectUid)
        {
            m_vfxUid = vfxUid;
            m_vfxObjectUid = vfxObjectUid;

            addButton.onClick.RemoveAllListeners();
            addButton.onClick.AddListener(OnAddButton);

            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);

            if (vfxObject.frames.Count <= 0)
            {
                ClearMenu();
                return;
            }

            addEventButton.onClick.RemoveAllListeners();
            addEventButton.onClick.AddListener(OnAddEventButton);

            if (string.IsNullOrEmpty(m_vfxFrameUid))
            {
                m_vfxFrameUid = vfxObject.frames[0].Uid;
            }

            var vfxFrame = vfxObject.GetFrame(m_vfxFrameUid);

            if (string.IsNullOrEmpty(m_vfxEventUid))
            {
                m_vfxEventUid = vfxFrame.events[0].Uid;
            }

            var vfxEvent = vfxFrame.GetEvent(m_vfxEventUid);

            vfxFramesList.Clear();
            vfxFramesList.OnDelete -= OnDeleteFrame;
            vfxFramesList.OnDelete += OnDeleteFrame;

            if (!string.IsNullOrEmpty(vfxObject.animator))
            {
                var animator = AddressableReferenceLoader.GetAnimationAsset(vfxObject.animator);
                if (animator != null)
                {
                    for (var i = 0; i < vfxObject.animations.Count; i++)
                    {
                        var animation =animator.animations.Find( anim => anim.name ==   vfxObject.animations[i]);

                        if (animation == null) continue;

                        for (var j = vfxObject.frames.Count; j < animation.frameDatas.Count; j++)
                        {
                            vfxObject.CreateNewFrame();
                        }
                    }

                }
            }

            for (var i = 0; i < vfxObject.frames.Count; i++)
            {
                var frame = vfxObject.frames[i];
                var frameItem = Instantiate(itemPrefab, vfxFramesList.contentLayout, false);
                frameItem.mainButton.content = frame.Uid;
                frameItem.textLabels[0].text = $"[{i}] Frame";
                frameItem.mainButton.OnValueChanged -= OnFrameChanged;
                frameItem.mainButton.OnValueChanged += OnFrameChanged;

                if (frame.Uid == m_vfxFrameUid)
                {
                    frameItem.mainButton.Toggle.SetIsOnWithoutNotify(true);
                }

                vfxFramesList.AddItem(frameItem);
            }

            vfxEventList.Clear();
            vfxEventList.OnDelete -= OnDeleteEvent;
            vfxEventList.OnDelete += OnDeleteEvent;

            for (var i = 0; i < vfxFrame.events.Count; i++)
            {
                var e = vfxFrame.events[i];
                var eventItem = Instantiate(itemPrefab, vfxEventList.contentLayout, false);
                eventItem.mainButton.content = e.Uid;
                eventItem.textLabels[0].text = e.eventType.ToString();
                eventItem.mainButton.OnValueChanged -= OnEventChanged;
                eventItem.mainButton.OnValueChanged += OnEventChanged;

                if (e.Uid == m_vfxEventUid)
                {
                    eventItem.mainButton.Toggle.SetIsOnWithoutNotify(true);
                }

                vfxEventList.AddItem(eventItem);
            }

            colorField.SetActive(false);
            positionX.SetActive(false);
            scaleX.SetActive(false);
            rotationX.SetActive(false);
            objectToCreate.SetActive(false);
            sortingLayer.SetActive(false);
            sortingOrder.SetActive(false);
            customEvent.SetActive(false);
            customEventArgs.SetActive(false);

            if (vfxEvent != null)
            {
                eventType.ClearOptions();
                eventType.AddOptions(Enum.GetNames(typeof(VfxFrameEvent.FrameEventType)).ToList());
                eventType.SetValueWithoutNotify((int)vfxEvent.eventType);
                eventType.onValueChanged.RemoveAllListeners();
                eventType.onValueChanged.AddListener(OnEventTypeChanged);

                targetType.ClearOptions();
                targetType.AddOptions(Enum.GetNames(typeof(VfxFrameEvent.TargetType)).ToList());
                targetType.SetValueWithoutNotify((int)vfxEvent.targetType);
                targetType.onValueChanged.RemoveAllListeners();
                targetType.onValueChanged.AddListener(OnTargetTypeChanged);

                skipUntil.OnChanged -= OnSkipUntilChanged;
                skipUntil.OnChanged += OnSkipUntilChanged;
                skipUntil.SetTextWithoutNotify(vfxEvent.skipUntil);

                colorField.OnColorChanged -= OnColorChanged;
                colorField.OnColorChanged += OnColorChanged;
                colorField.SetColorWithoutNotify(vfxEvent.color, vfxEvent.alpha);

                positionX.OnChanged -= OnPositionXChanged;
                positionX.OnChanged += OnPositionXChanged;
                positionX.SetTextWithoutNotify(vfxEvent.position.x);

                positionY.OnChanged -= OnPositionYChanged;
                positionY.OnChanged += OnPositionYChanged;
                positionY.SetTextWithoutNotify(vfxEvent.position.y);

                scaleX.OnChanged -= OnScaleXChanged;
                scaleX.OnChanged += OnScaleXChanged;
                scaleX.SetTextWithoutNotify(vfxEvent.scale.x);

                scaleY.OnChanged -= OnScaleYChanged;
                scaleY.OnChanged += OnScaleYChanged;
                scaleY.SetTextWithoutNotify(vfxEvent.scale.y);

                rotationX.OnChanged -= OnRotationXChanged;
                rotationX.OnChanged += OnRotationXChanged;
                rotationX.SetTextWithoutNotify(vfxEvent.rotation.x);

                rotationY.OnChanged -= OnRotationYChanged;
                rotationY.OnChanged += OnRotationYChanged;
                rotationY.SetTextWithoutNotify(vfxEvent.rotation.y);

                rotationZ.OnChanged -= OnRotationZChanged;
                rotationZ.OnChanged += OnRotationZChanged;
                rotationZ.SetTextWithoutNotify(vfxEvent.rotation.z);

                objectToCreate.onEndEdit.RemoveAllListeners();
                objectToCreate.onEndEdit.AddListener(OnObjectToCreateChanged);
                objectToCreate.SetTextWithoutNotify(vfxEvent.objectToCreate);

                sortingLayer.onEndEdit.RemoveAllListeners();
                sortingLayer.onEndEdit.AddListener(OnSortingLayerChanged);
                sortingLayer.SetTextWithoutNotify(vfxEvent.sortingLayer);

                sortingOrder.OnChanged -= OnSortingOrderChanged;
                sortingOrder.OnChanged += OnSortingOrderChanged;
                sortingOrder.SetTextWithoutNotify(vfxEvent.sortingOrder);

                customEvent.onEndEdit.RemoveAllListeners();
                customEvent.onEndEdit.AddListener(OnCustomEventChanged);
                customEvent.SetTextWithoutNotify(vfxEvent.customEventName);

                customEventArgs.onEndEdit.RemoveAllListeners();
                customEventArgs.onEndEdit.AddListener(OnCustomEventArgsChanged);
                customEventArgs.SetTextWithoutNotify(vfxEvent.customEventArgs);


                switch (vfxEvent.eventType)
                {
                    case VfxFrameEvent.FrameEventType.Color:
                    {
                        colorField.SetActive(true);
                        break;
                    }
                    case VfxFrameEvent.FrameEventType.Position:
                    {
                        positionX.SetActive(true);
                        break;
                    }
                    case VfxFrameEvent.FrameEventType.Scale:
                    {
                        scaleX.SetActive(true);
                        break;
                    }
                    case VfxFrameEvent.FrameEventType.Rotation:
                    {
                        rotationX.SetActive(true);
                        break;
                    }
                    case VfxFrameEvent.FrameEventType.CreateObject:
                    {
                        positionX.SetActive(true);
                        scaleX.SetActive(true);
                        rotationX.SetActive(true);
                        objectToCreate.SetActive(true);
                        break;
                    }
                    case VfxFrameEvent.FrameEventType.CustomEvent:
                    {
                        customEvent.SetActive(true);
                        customEventArgs.SetActive(true);
                        break;
                    }
                    case VfxFrameEvent.FrameEventType.SetSortingLayer:
                    {
                        sortingLayer.SetActive(true);
                        sortingOrder.SetActive(true);
                        break;
                    }
                }
            }
        }

        public void ClearMenu()
        {
        }

        private void OnCustomEventChanged(string customEventName)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            var vfxFrame = vfxObject.GetFrame(m_vfxFrameUid);
            var vfxEvent = vfxFrame.GetEvent(m_vfxEventUid);
            vfxEvent.customEventName = customEventName;
            RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }

        private void OnCustomEventArgsChanged(string customEventArg)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            var vfxFrame = vfxObject.GetFrame(m_vfxFrameUid);
            var vfxEvent = vfxFrame.GetEvent(m_vfxEventUid);
            vfxEvent.customEventArgs = customEventArg;
            RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }

        private void OnAddButton()
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            var vfxFrame = vfxObject.CreateNewFrame();
            m_vfxFrameUid = vfxFrame.Uid;
            RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }

        private void OnAddEventButton()
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            var vfxFrame = vfxObject.GetFrame(m_vfxFrameUid);
            var vfxEvent = vfxFrame.CreateNewEvent();
            m_vfxEventUid = vfxEvent.Uid;
            RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }

        private void OnDeleteFrame(LayoutItem item)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);

            if (vfxObject.frames.Count <= 1)
            {
                return;
            }

            var animation = AddressableReferenceLoader.GetAnimationAsset(vfxObject.animator);

            if (animation)
            {
                if (vfxObject.frames.Count <= animation.animations.Count)
                {
                    return;
                }

                vfxObject.DeleteFrame(m_vfxFrameUid);
            }
            else
            {
                vfxObject.DeleteFrame(m_vfxFrameUid);
            }

            RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }

        private void OnFrameChanged(bool isOn, string context)
        {
            if (isOn)
            {
                m_vfxFrameUid = context;
                RefreshMenu(m_vfxUid, m_vfxObjectUid);
            }
        }

        private void OnDeleteEvent(LayoutItem item)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            var vfxFrame = vfxObject.GetFrame(m_vfxFrameUid);
            vfxFrame.DeleteEvent(m_vfxEventUid);
            RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }

        private void OnEventChanged(bool isOn, string context)
        {
            if (isOn)
            {
                m_vfxEventUid = context;
                RefreshMenu(m_vfxUid, m_vfxObjectUid);
            }
        }

        private void OnEventTypeChanged(int type)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            var vfxFrame = vfxObject.GetFrame(m_vfxFrameUid);
            var vfxEvent = vfxFrame.GetEvent(m_vfxEventUid);
            vfxEvent.eventType = (VfxFrameEvent.FrameEventType)type;
            RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }

        private void OnTargetTypeChanged(int type)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            var vfxFrame = vfxObject.GetFrame(m_vfxFrameUid);
            var vfxEvent = vfxFrame.GetEvent(m_vfxEventUid);
            vfxEvent.targetType = (VfxFrameEvent.TargetType)type;
            RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }

        private void OnSkipUntilChanged(int i)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            var vfxFrame = vfxObject.GetFrame(m_vfxFrameUid);
            var vfxEvent = vfxFrame.GetEvent(m_vfxEventUid);
            vfxEvent.skipUntil = i;
            RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }

        private void OnColorChanged(string hex, float alpha, Color color)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            var vfxFrame = vfxObject.GetFrame(m_vfxFrameUid);
            var vfxEvent = vfxFrame.GetEvent(m_vfxEventUid);
            vfxEvent.color = hex;
            vfxEvent.alpha = (int)alpha;
            RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }

        private void OnPositionXChanged(int x)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            var vfxFrame = vfxObject.GetFrame(m_vfxFrameUid);
            var vfxEvent = vfxFrame.GetEvent(m_vfxEventUid);
            vfxEvent.position.x = x;
            RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }

        private void OnPositionYChanged(int y)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            var vfxFrame = vfxObject.GetFrame(m_vfxFrameUid);
            var vfxEvent = vfxFrame.GetEvent(m_vfxEventUid);
            vfxEvent.position.y = y;
            RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }

        private void OnScaleXChanged(int x)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            var vfxFrame = vfxObject.GetFrame(m_vfxFrameUid);
            var vfxEvent = vfxFrame.GetEvent(m_vfxEventUid);
            vfxEvent.scale.x = x;
            RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }

        private void OnScaleYChanged(int y)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            var vfxFrame = vfxObject.GetFrame(m_vfxFrameUid);
            var vfxEvent = vfxFrame.GetEvent(m_vfxEventUid);
            vfxEvent.scale.y = y;
            RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }

        private void OnRotationXChanged(int x)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            var vfxFrame = vfxObject.GetFrame(m_vfxFrameUid);
            var vfxEvent = vfxFrame.GetEvent(m_vfxEventUid);
            vfxEvent.rotation.x = x;
            RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }

        private void OnRotationYChanged(int y)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            var vfxFrame = vfxObject.GetFrame(m_vfxFrameUid);
            var vfxEvent = vfxFrame.GetEvent(m_vfxEventUid);
            vfxEvent.rotation.y = y;
            RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }

        private void OnRotationZChanged(int z)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            var vfxFrame = vfxObject.GetFrame(m_vfxFrameUid);
            var vfxEvent = vfxFrame.GetEvent(m_vfxEventUid);
            vfxEvent.rotation.z = z;
            RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }

        private void OnObjectToCreateChanged(string objectCreated)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            var vfxFrame = vfxObject.GetFrame(m_vfxFrameUid);
            var vfxEvent = vfxFrame.GetEvent(m_vfxEventUid);
            vfxEvent.objectToCreate = objectCreated;
            RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }

        private void OnSortingLayerChanged(string sortLayer)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            var vfxFrame = vfxObject.GetFrame(m_vfxFrameUid);
            var vfxEvent = vfxFrame.GetEvent(m_vfxEventUid);
            vfxEvent.sortingLayer = sortLayer;
            RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }

        private void OnSortingOrderChanged(int sortOrder)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            var vfxFrame = vfxObject.GetFrame(m_vfxFrameUid);
            var vfxEvent = vfxFrame.GetEvent(m_vfxEventUid);
            vfxEvent.sortingOrder = sortOrder;
            RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }
    }
}