using System;
using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90CustomBattleOptionsEditor : MonoBehaviour
    {
        public Toggle enabledToggle;
        public CanvasGroup disableGroup;
        public Toggle forcePlayerFirst;
        public Toggle forceOpponentFirst;
        public InputField forcePlayerMana;
        public InputField forceOpponentMana;
        public D90DeckOverrideEditor playerOverrideDeckEditor;
        public D90DeckOverrideEditor opponentOverrideDeckEditor;
        public D90DeckOverrideEditor playerAdditionalCards;
        public D90DeckOverrideEditor opponentAdditionalCards;

        public List<D90DatabaseDropdown> boardSpots;

        private CustomBattleOptions m_customBattleOptions;
        public event Action<CustomBattleOptions> OnOptionsChanged;

        public void RefreshData(CustomBattleOptions customOptions)
        {
            m_customBattleOptions = customOptions;

            enabledToggle.onValueChanged.RemoveAllListeners();
            enabledToggle.onValueChanged.AddListener(OnEnabledToggled);
            enabledToggle.SetIsOnWithoutNotify( m_customBattleOptions.Enabled );

            disableGroup.interactable = m_customBattleOptions.Enabled;
            disableGroup.blocksRaycasts = m_customBattleOptions.Enabled;

            playerOverrideDeckEditor.OnDeckOverrideChanged -= OnPlayerDeckChanged;
            playerOverrideDeckEditor.OnDeckOverrideChanged += OnPlayerDeckChanged;
            playerOverrideDeckEditor.SetDeck(m_customBattleOptions.PlayerDeckOverride);

            opponentOverrideDeckEditor.OnDeckOverrideChanged -= OnOpponentDeckChanged;
            opponentOverrideDeckEditor.OnDeckOverrideChanged += OnOpponentDeckChanged;
            opponentOverrideDeckEditor.SetDeck(m_customBattleOptions.OpponentDeckOverride);

            playerAdditionalCards.OnDeckOverrideChanged -= OnPlayerAdditionalCardsChanged;
            playerAdditionalCards.OnDeckOverrideChanged += OnPlayerAdditionalCardsChanged;
            playerAdditionalCards.SetDeck(m_customBattleOptions.PlayerAdditionalCards);

            opponentAdditionalCards.OnDeckOverrideChanged -= OnOpponentAdditionalCardsChanged;
            opponentAdditionalCards.OnDeckOverrideChanged += OnOpponentAdditionalCardsChanged;
            opponentAdditionalCards.SetDeck(m_customBattleOptions.OpponentAdditionalCards);

            forcePlayerFirst.onValueChanged.RemoveAllListeners();
            forcePlayerFirst.onValueChanged.AddListener(OnForcePlayerFirst);
            forcePlayerFirst.SetIsOnWithoutNotify(m_customBattleOptions.ForcePlayerStartsFirst);

            forceOpponentFirst.onValueChanged.RemoveAllListeners();
            forceOpponentFirst.onValueChanged.AddListener(OnForceOpponentFirst);
            forceOpponentFirst.SetIsOnWithoutNotify(m_customBattleOptions.ForceOpponentStartsFirst);

            forcePlayerMana.onEndEdit.RemoveAllListeners();
            forcePlayerMana.onEndEdit.AddListener(OnForcePlayerManaChanged);
            forcePlayerMana.SetTextWithoutNotify( m_customBattleOptions.PlayerStartingMana.ToString() );

            forceOpponentMana.onEndEdit.RemoveAllListeners();
            forceOpponentMana.onEndEdit.AddListener(OnForceOpponentManaChanged);
            forceOpponentMana.SetTextWithoutNotify( m_customBattleOptions.OpponentStartingMana.ToString() );

            if (m_customBattleOptions.GameBoardOptions.Board == null || m_customBattleOptions.GameBoardOptions.Board.Count < 16)
            {
                m_customBattleOptions.GameBoardOptions = new CustomGameBoardOptions();
                m_customBattleOptions.GameBoardOptions.Board = new List<CustomGameCardState>();

                for (var i = 0; i < 16; i++)
                {
                    m_customBattleOptions.GameBoardOptions.Board.Add( new CustomGameCardState());
                }

                OnOptionsChanged?.Invoke(m_customBattleOptions);
            }

            for (var i = 0; i < m_customBattleOptions.GameBoardOptions.Board?.Count; i++)
            {
                boardSpots[i].Init();

                if (!string.IsNullOrEmpty(m_customBattleOptions.GameBoardOptions.Board[i].CardId))
                {
                    var card = Db.CardDatabase.GetCard(m_customBattleOptions.GameBoardOptions.Board[i].CardId);
                    boardSpots[i].SetDropdownWithoutNotify(card != null ? card.name : string.Empty);
                }
                else
                {
                    boardSpots[i].SetDropdownWithoutNotify(string.Empty);
                }

                boardSpots[i].OnValueChanged -= OnBoardSpotChanged;
                boardSpots[i].OnValueChanged += OnBoardSpotChanged;
            }
        }

        private void OnPlayerAdditionalCardsChanged(CustomDeckOverride deckOverride)
        {
            m_customBattleOptions.PlayerAdditionalCards = deckOverride;
            OnOptionsChanged?.Invoke(m_customBattleOptions);
        }

        private void OnOpponentAdditionalCardsChanged(CustomDeckOverride deckOverride)
        {
            m_customBattleOptions.OpponentAdditionalCards = deckOverride;
            OnOptionsChanged?.Invoke(m_customBattleOptions);
        }

        private void OnBoardSpotChanged(D90DatabaseDropdown dropdown, int value)
        {
            var indexOfBoardSpot = boardSpots.IndexOf(dropdown);

            if(value > 0)
            {
                m_customBattleOptions.GameBoardOptions.Board[indexOfBoardSpot].CardId =
                    Db.CardDatabase.GetCardByName(dropdown.dropdown.options[value].text).id;
            }
            else
            {
                m_customBattleOptions.GameBoardOptions.Board[indexOfBoardSpot].CardId = string.Empty;
            }

            OnOptionsChanged?.Invoke(m_customBattleOptions);
        }

        private void OnForcePlayerFirst(bool isOn)
        {
            m_customBattleOptions.ForcePlayerStartsFirst = isOn;

            if (m_customBattleOptions.ForcePlayerStartsFirst)
            {
                m_customBattleOptions.ForceOpponentStartsFirst = false;
                forceOpponentFirst.SetIsOnWithoutNotify( false );
            }

            OnOptionsChanged?.Invoke(m_customBattleOptions);
        }

        private void OnForceOpponentFirst(bool isOn)
        {
            m_customBattleOptions.ForceOpponentStartsFirst = isOn;

            if (m_customBattleOptions.ForceOpponentStartsFirst)
            {
                m_customBattleOptions.ForcePlayerStartsFirst = false;
                forcePlayerFirst.SetIsOnWithoutNotify( false );
            }

            OnOptionsChanged?.Invoke(m_customBattleOptions);
        }

        private void OnForcePlayerManaChanged(string text)
        {
            m_customBattleOptions.PlayerStartingMana = int.Parse(text);
            OnOptionsChanged?.Invoke(m_customBattleOptions);
        }

        private void OnForceOpponentManaChanged(string text)
        {
            m_customBattleOptions.OpponentStartingMana = int.Parse(text);
            OnOptionsChanged?.Invoke(m_customBattleOptions);
        }

        private void OnEnabledToggled(bool isOn)
        {
            m_customBattleOptions.Enabled = isOn;
            disableGroup.interactable = m_customBattleOptions.Enabled;
            disableGroup.blocksRaycasts = m_customBattleOptions.Enabled;
            OnOptionsChanged?.Invoke(m_customBattleOptions);
        }

        private void OnPlayerDeckChanged(CustomDeckOverride deckOverride)
        {
            m_customBattleOptions.PlayerDeckOverride = deckOverride;
            OnOptionsChanged?.Invoke(m_customBattleOptions);
        }

        private void OnOpponentDeckChanged(CustomDeckOverride deckOverride)
        {
            m_customBattleOptions.OpponentDeckOverride = deckOverride;
            OnOptionsChanged?.Invoke(m_customBattleOptions);
        }

    }
}