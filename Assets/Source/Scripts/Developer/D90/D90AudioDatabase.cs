using CrossBlitz.Audio;
using CrossBlitz.UI.Widgets;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90AudioDatabase : MonoBehaviour
    {
        public D90ContentLayout audioList;
        public LayoutItem audioItemPrefab;
        public Button playButton;
        public Button stopButton;

        private string m_currentCategory;
        private string m_currentAudio;

        public void Open()
        {
            foreach (var audioCategory in AudioController.GetAllAudioByCategory())
            {
                foreach (var audioEvent in audioCategory.Value)
                {
                    var audioItem = Instantiate(audioItemPrefab, audioList.contentLayout, false);
                    audioItem.mainButton.content = $"{audioCategory.Key}/{audioEvent}";
                    audioItem.textLabels[0].text = $"{audioEvent} [{audioCategory.Key}]";
                    audioItem.mainButton.OnValueChanged -= OnMusicClicked;
                    audioItem.mainButton.OnValueChanged += OnMusicClicked;
                    audioItem.mainButton.Toggle.isOn = m_currentAudio == audioEvent;
                    audioList.AddItem(audioItem);
                }
            }

            playButton.onClick.RemoveAllListeners();
            playButton.onClick.AddListener(OnPlayButton);

            stopButton.onClick.RemoveAllListeners();
            stopButton.onClick.AddListener(OnStopButton);
        }

        private void OnMusicClicked(bool isOn, string content)
        {
            if (isOn)
            {
                var categorySplit = content.Split('/');
                m_currentCategory = categorySplit[0];
                m_currentAudio = categorySplit[1];
            }
        }

        private void OnPlayButton()
        {
            if (m_currentCategory == "MUSIC")
            {
                AudioController.PlaySong(m_currentAudio);
            }
            else if (m_currentCategory == "AMBIENCE")
            {
                AmbientController.PlayAmbient(m_currentAudio);
            }
            else if (!string.IsNullOrEmpty(m_currentAudio))
            {
                AudioController.PlaySound(m_currentAudio, m_currentCategory);
            }
        }

        private void OnStopButton()
        {

        }

        private void OnSfxFinishedPlaying()
        {

        }

        private void Update()
        {

        }
    }
}