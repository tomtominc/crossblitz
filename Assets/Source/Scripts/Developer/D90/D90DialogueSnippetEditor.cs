using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.UI.Widgets;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90DialogueSnippetEditor : MonoBehaviour
    {
        public D90ContentLayout contentLayout;
        public Dropdown optionDropdown;
        public RectTransform snippetInspector;
        public Dropdown emoteDropdown;
        public InputField dialogue;
        public D90DatabaseDropdown roomDropdown;
        public D90DatabaseDropdown soundDropdown;
        public Button addButton;
        public Button deleteButton;
        public LayoutItem textItemPrefab;

        private string m_currentOption;
        private string m_currentSnippetId;
        private string m_characterId;
        private List<string> m_options;
        private List<DialogueSnippetData> m_snippets;

        public void Init(string characterId, List<string> options)
        {
            m_options = options;

            if (m_characterId != characterId)
            {
                m_characterId = characterId;
                m_currentSnippetId = string.Empty;
            }

            if (string.IsNullOrEmpty(m_currentOption))
            {
                m_currentOption = m_options[0];
            }

            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            if (character.dialogueSnippets == null)
            {
                character.dialogueSnippets = new List<DialogueSnippetData>();
            }
            m_snippets = character.dialogueSnippets.FindAll(s => s.Option == m_currentOption);

            if (string.IsNullOrEmpty(m_currentSnippetId) && m_snippets.Count > 0)
            {
                m_currentSnippetId = m_snippets[0].Uid;
            }

            snippetInspector.SetActive(!string.IsNullOrEmpty(m_currentSnippetId));
            optionDropdown.ClearOptions();
            optionDropdown.AddOptions(m_options);
            optionDropdown.onValueChanged.RemoveAllListeners();
            optionDropdown.onValueChanged.AddListener(OnOptionChanged);
            optionDropdown.SetValueWithoutNotify(m_options.IndexOf(m_currentOption));

            addButton.onClick.RemoveAllListeners();
            addButton.onClick.AddListener(OnAddButton);

            deleteButton.onClick.RemoveAllListeners();
            deleteButton.onClick.AddListener(OnDeleteButton);

            contentLayout.Clear();

            DialogueSnippetData snippetData = null;

            for (var i = 0; i < m_snippets.Count; i++)
            {
                var textItem = Instantiate(textItemPrefab, contentLayout.contentLayout, false);
                textItem.mainButton.content = m_snippets[i].Uid;
                textItem.textLabels[0].text = m_snippets[i].Dialogue;
                textItem.mainButton.OnValueChanged += OnDialogueItemSelected;
                if (m_currentSnippetId == m_snippets[i].Uid)
                {
                    textItem.mainButton.Toggle.SetIsOnWithoutNotify( true );
                    snippetData = m_snippets[i];
                }
                else
                {
                    textItem.mainButton.Toggle.SetIsOnWithoutNotify( false );
                }
                contentLayout.AddItem(textItem);
            }

            if (snippetData != null)
            {
                if (m_currentOption.Contains("Shop"))
                {
                    roomDropdown.SetActive(true);
                    roomDropdown.Init();
                    roomDropdown.OnValueChanged -= OnRoomChanged;
                    roomDropdown.OnValueChanged += OnRoomChanged;
                    roomDropdown.SetDropdownWithoutNotify( Db.MapDatabase.GetRoomNameByUid(snippetData.RoomUid));
                }
                else
                {
                    roomDropdown.SetActive(false);
                }

                var emotes = CutsceneData.GetCutsceneEmotions();

                emoteDropdown.ClearOptions();
                emoteDropdown.AddOptions(emotes);
                emoteDropdown.onValueChanged.RemoveAllListeners();
                emoteDropdown.onValueChanged.AddListener(OnEmoteChanged);
                if (string.IsNullOrEmpty(snippetData.Emotion))
                {
                    snippetData.Emotion = "talking";
                }
                var emoteIndex = emotes.IndexOf(snippetData.Emotion);
                emoteDropdown.SetValueWithoutNotify( emoteIndex );

                dialogue.onEndEdit.RemoveAllListeners();
                dialogue.onEndEdit.AddListener(OnDialogueChanged);
                dialogue.SetTextWithoutNotify( snippetData.Dialogue );

                soundDropdown.Init();
                soundDropdown.OnValueChanged -= OnSoundChanged;
                soundDropdown.OnValueChanged += OnSoundChanged;
                soundDropdown.SetDropdownWithoutNotify(snippetData.SoundFx);
            }

        }

        private void OnOptionChanged(int option)
        {
            m_currentOption = optionDropdown.options[option].text;
            m_currentSnippetId = string.Empty;
            Init(m_characterId, m_options);
        }

        private void OnDialogueItemSelected(bool isOn, string snippetId)
        {
            if (isOn)
            {
                m_currentSnippetId = snippetId;
                Init(m_characterId, m_options);
            }
        }

        private void OnEmoteChanged(int index)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            var snippet = character.dialogueSnippets.Find(s => s.Uid == m_currentSnippetId);
            snippet.Emotion = emoteDropdown.options[index].text;
        }

        private void OnRoomChanged(D90DatabaseDropdown dropdown, int index)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            var snippet = character.dialogueSnippets.Find(s => s.Uid == m_currentSnippetId);
            snippet.RoomUid = index <= 0 ? string.Empty : Db.MapDatabase.GetRoomUidByName(dropdown.dropdown.options[index].text);
        }

        private void OnDialogueChanged(string text)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            var snippet = character.dialogueSnippets.Find(s => s.Uid == m_currentSnippetId);
            snippet.Dialogue = text;
        }

        private void OnSoundChanged(D90DatabaseDropdown dropdown, int index)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            var snippet = character.dialogueSnippets.Find(s => s.Uid == m_currentSnippetId);
            snippet.SoundFx = index <= 0 ? string.Empty : dropdown.dropdown.options[index].text;
        }

        private void OnAddButton()
        {
            var snippet = new DialogueSnippetData();
            snippet.UpdateIdentifier();
            snippet.Emotion = "talking";
            snippet.Option = m_currentOption;

            var character =  Db.CharacterDatabase.GetCharacter(m_characterId);
            character.dialogueSnippets.Add(snippet);

            Init(m_characterId, m_options);
        }

        private void OnDeleteButton()
        {
            if (!string.IsNullOrEmpty(m_currentSnippetId))
            {
                var character = Db.CharacterDatabase.GetCharacter(m_characterId);
                var snippet = character.dialogueSnippets.Find(s => s.Uid == m_currentSnippetId);
                D90Tools.DisplayConfirmation($"Do you want to delete the dialogue snippet: '{snippet.Dialogue}'", OnDeleteConfirmed);
            }
        }

        private void OnDeleteConfirmed(bool delete)
        {
            if (delete)
            {
                var character = Db.CharacterDatabase.GetCharacter(m_characterId);
                character.dialogueSnippets.RemoveAll(s => s.Uid == m_currentSnippetId);
                Init(m_characterId, m_options);
            }
        }
    }
}