using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Hero;
using CrossBlitz.UI.Widgets;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90CardPoolEditor : MonoBehaviour
    {

        public D90ContentLayout poolsList;
        public D90DeckInspector cardList;
        public CanvasGroup cardListCanvas;
        public D90ContentLayout cardFullList;
        public D90ContentLayout poolsCardBelongsToList;
        public Dropdown showOnlyFaction;

        public LayoutItem textLayoutItemPrefab;

        public InputField poolNameField;
        public Dropdown factionDropdown;
        public Button addPoolButton;
        public Button movePoolUp;
        public Button movePoolDown;
        public Button saveButton;
        public Button regenerateButton;

        public Text tooltip;

        private string m_currentCardPool;
        private string m_currentCard;
        private int m_currentFaction;

        public void Open(string poolUid)
        {
            Db.CardPoolsDatabase.Generate();
            Refresh(poolUid);
        }

        private void Refresh(string poolUid)
        {
            m_currentCardPool = poolUid;

            addPoolButton.onClick.RemoveAllListeners();
            addPoolButton.onClick.AddListener(OnAddPoolButton);

            movePoolUp.onClick.RemoveAllListeners();
            movePoolUp.onClick.AddListener(OnMovePoolUp);

            movePoolDown.onClick.RemoveAllListeners();
            movePoolDown.onClick.AddListener(OnMovePoolDown);

            saveButton.onClick.RemoveAllListeners();
            saveButton.onClick.AddListener(OnSaveButton);

            regenerateButton.onClick.RemoveAllListeners();
            regenerateButton.onClick.AddListener(OnRegenerate);

            poolsList.Clear();
            poolsList.OnDelete -= OnDeletePool;
            poolsList.OnDelete += OnDeletePool;

            var cardPools = Db.CardPoolsDatabase.cardPools;

            if (m_currentFaction > 0 && m_currentFaction < 7)
            {
                var heroName = HeroData.GetHeroFromFaction((Faction)m_currentFaction);
                cardPools = Db.CardPoolsDatabase.GetPoolsForHero(heroName);

                if (!string.IsNullOrEmpty(m_currentCardPool))
                {
                    var currentPool = Db.CardPoolsDatabase.GetCardPool(m_currentCardPool);

                    if (currentPool.hero != heroName)
                    {
                        m_currentCardPool = string.Empty;
                    }
                }
            }

            for (var i = 0; i < cardPools.Count; i++)
            {
                var pool = cardPools[i];
                if (string.IsNullOrEmpty(m_currentCardPool)) m_currentCardPool = pool.Uid;
                var cardText = Instantiate(textLayoutItemPrefab, poolsList.contentLayout);
                cardText.mainButton.content = pool.Uid;
                cardText.textLabels[0].text = pool.name;
                cardText.mainButton.Toggle.SetIsOnWithoutNotify( pool.Uid == m_currentCardPool );
                cardText.mainButton.OnValueChanged += OnCardPoolChanged;
                poolsList.AddItem(cardText);
            }

            var cardPool = Db.CardPoolsDatabase.GetCardPool(m_currentCardPool);

            poolNameField.onEndEdit.RemoveAllListeners();
            poolNameField.onEndEdit.AddListener(OnPoolNameChanged);
            poolNameField.SetTextWithoutNotify(cardPool.name);

            factionDropdown.onValueChanged.RemoveAllListeners();
            factionDropdown.onValueChanged.AddListener(OnPoolFactionChanged);
            factionDropdown.ClearOptions();
            factionDropdown.AddOptions( Enum.GetNames( typeof(Faction)).ToList() );
            factionDropdown.SetValueWithoutNotify((int)HeroData.GetFactionForHero( cardPool.hero ));

            showOnlyFaction.onValueChanged.RemoveAllListeners();
            showOnlyFaction.onValueChanged.AddListener(OnShowOnlyFactionChanged);
            showOnlyFaction.ClearOptions();
            showOnlyFaction.AddOptions( Enum.GetNames( typeof(Faction)).ToList() );
            showOnlyFaction.SetValueWithoutNotify((int)m_currentFaction);

            cardList.OnCardsChanged -= OnPoolCardsChanged;
            cardList.OnCardsChanged += OnPoolCardsChanged;
            cardList.SetupCards( cardPool.cards.Select( c => new CardValue { id = c, count = 1 }).ToList() );
            cardListCanvas.interactable = true; //!cardPool.disableEdit;

            cardFullList.Clear();

            // cardFullList.OnDelete -= OnDeletePool;
            // cardFullList.OnDelete += OnDeletePool;

            var cards = Db.CardDatabase.GetCollectableCards();
            var heroForFaction = string.Empty;

            if (m_currentFaction > 0 && m_currentFaction < 7)
            {
                heroForFaction =HeroData.GetHeroFromFaction((Faction) m_currentFaction);
                cards = Db.CardDatabase.GetCards(card => (card.faction == (Faction)m_currentFaction || card.faction ==Faction.Neutral) &&
                                                      card.set != CardSet.Token &&
                                                      card.set != CardSet.Deprecated &&
                                                      card.type != CardType.Relic &&
                                                      card.type != CardType.Elder_Relic );

                if (!string.IsNullOrEmpty(m_currentCard))
                {
                    var card = Db.CardDatabase.GetCard(m_currentCard);

                    if (card.faction != (Faction)m_currentFaction && card.faction != Faction.Neutral)
                    {
                        m_currentCard = string.Empty;
                    }
                }
            }

            if (!string.IsNullOrEmpty(m_currentCard))
            {
                tooltip.text = $"{Db.CardPoolsDatabase.GetCardLocationChapter(m_currentCard)}\n" +
                               $"{Db.CardPoolsDatabase.GetCardLocation(m_currentCard)}\n" +
                               $"{Db.CardPoolsDatabase.GetCardLocationHint(m_currentCard)}\n" +
                               $"{Db.CardPoolsDatabase.GetCardLocationIcon(m_currentCard)}";
            }

            cards = cards.OrderBy(card => card.faction).ThenBy(card => card.cost).ThenBy(card => card.name).ToList();

            for (var i = 0; i < cards.Count; i++)
            {
                var card = cards[i];
                if (string.IsNullOrEmpty(m_currentCard)) m_currentCard = card.id;
                var currentPools = Db.CardPoolsDatabase.GetPoolsForCard(card.id);
                var isInCardPools = currentPools.Count > 0;

                if (!string.IsNullOrEmpty(heroForFaction))
                {
                    currentPools.RemoveAll(p => Db.CardPoolsDatabase.GetCardPool(p).hero != heroForFaction);
                }

                var color = currentPools.Count <= 0 && !isInCardPools ? "#FF0000" : currentPools.Count <= 0 && isInCardPools ? "#FF8C00" : "#000000";
                var cardText = Instantiate(textLayoutItemPrefab, cardFullList.contentLayout);
                cardText.mainButton.content = card.id;
                cardText.sideIcon.SetActive(false);
                cardText.textLabels[0].text = $"<color={color}>({card.cost}) {card.name} [{card.type}] [{card.faction}]</color>";
                cardText.mainButton.Toggle.SetIsOnWithoutNotify( card.id == m_currentCard );
                cardText.mainButton.OnValueChanged += OnCardChanged;
                cardFullList.AddItem(cardText);
            }

            poolsCardBelongsToList.Clear();

            var pools = Db.CardPoolsDatabase.GetPoolsForCard(m_currentCard);

            //if (!string.IsNullOrEmpty(heroForFaction)) pools.RemoveAll(p => Db.CardPoolsDatabase.GetCardPool(p).hero != heroForFaction);

            for (var i = 0; i < pools.Count; i++)
            {
                var pool = Db.CardPoolsDatabase.GetCardPool(pools[i]);
                var cardText = Instantiate(textLayoutItemPrefab, poolsCardBelongsToList.contentLayout);
                cardText.mainButton.content = pool.Uid;
                cardText.textLabels[0].text = pool.name;
                // cardText.mainButton.Toggle.SetIsOnWithoutNotify( card.id == m_currentCard );
                // cardText.mainButton.OnValueChanged += OnCardChanged;
                poolsCardBelongsToList.AddItem(cardText);
            }

        }

        private void OnRegenerate()
        {
            Db.CardPoolsDatabase.Generate();
        }

        private void OnAddPoolButton()
        {
            var pool = Db.CardPoolsDatabase.CreateNew("New Card Pool");
            Refresh(pool.Uid);
        }

        private void OnMovePoolUp()
        {
            Db.CardPoolsDatabase.MoveUp(m_currentCardPool);
            Refresh(m_currentCardPool);
        }

        private void OnMovePoolDown()
        {
            Db.CardPoolsDatabase.MoveDown(m_currentCardPool);
            Refresh(m_currentCardPool);
        }

        private void OnSaveButton()
        {
            Db.CardPoolsDatabase.Save();
            PrintStats(m_currentFaction);
        }

        private void OnDeletePool(LayoutItem item)
        {
            var id = item.mainButton.content;
            Db.CardPoolsDatabase.Delete(id);
            Refresh(string.Empty);
        }

        private void OnCardPoolChanged(bool isOn, string uid)
        {
            if (isOn)
            {
                Refresh(uid);
            }
        }

        private void OnPoolNameChanged(string newName)
        {
            var pool = Db.CardPoolsDatabase.GetCardPool(m_currentCardPool);
            pool.name = newName;
        }

        private void OnPoolFactionChanged(int faction)
        {
            var pool = Db.CardPoolsDatabase.GetCardPool(m_currentCardPool);
            var hero = HeroData.GetHeroFromFaction((Faction)faction);
            pool.hero = hero;
        }


        private void OnShowOnlyFactionChanged(int faction)
        {
            m_currentFaction = faction;
            Refresh(m_currentCardPool);
        }

        private void OnPoolCardsChanged(List<CardValue> cardValue)
        {
            var pool = Db.CardPoolsDatabase.GetCardPool(m_currentCardPool);
            pool.cards = cardValue.Select(c => c.id).ToList();
            Refresh(m_currentCardPool);
        }

        private void OnCardChanged(bool isOn, string uid)
        {
            m_currentCard = uid;
            Refresh(m_currentCardPool);
        }

        public static void PrintStats(int currentFaction)
        {
            var meldingPools =
                Db.CardPoolsDatabase.GetCardPools(p =>
                {
                    if (p.name.Contains("Melding") || p.name.Contains("Mana"))
                    {
                        if (currentFaction > 0 && currentFaction < 7)
                        {
                            var heroForFaction = HeroData.GetHeroFromFaction((Faction)currentFaction);
                            return heroForFaction == p.hero;
                        }

                        return true;
                    }

                    return false;
                });

            var totalManaInPools = 0;
            var ingredientsInPool = new List<ItemValue>();

            for (var i = 0; i < meldingPools.Count; i++)
            {
                for (var j = 0; j < meldingPools[i].cards.Count; j++)
                {
                    var card = Db.CardDatabase.GetCard(meldingPools[i].cards[j]);

                    totalManaInPools += card.cost;
                    Debug.Log(card.name + ": " + card.cost);

                    for (var k = 0; k < card.craftingData.ingredients.Count; k++)
                    {
                        var ing = card.craftingData.ingredients[k];
                        if (ingredientsInPool.Exists(t => t.ItemUid == ing.ItemUid))
                        {
                            var ingInP = ingredientsInPool.Find(t => t.ItemUid == ing.ItemUid);
                            ingInP.Count += ing.Count;
                        }
                        else
                        {
                            var ingInP = new ItemValue();
                            ingInP.ItemUid = ing.ItemUid;
                            ingInP.Count = ing.Count;
                            ingredientsInPool.Add(ingInP);
                        }
                    }
                }
            }

            var log = $"TOTAL MELDING IN CHAPTER:\nMANA SHARDS: {totalManaInPools}\n";

            for (var i = 0; i < ingredientsInPool.Count; i++)
            {
                log += $"{ingredientsInPool[i].ItemUid}: {ingredientsInPool[i].Count}\n";
            }

            Debug.Log(log);
        }

    }
}