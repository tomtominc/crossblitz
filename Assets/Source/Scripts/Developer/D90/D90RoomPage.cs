using System;
using System.Linq;
using BlendModes;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Data;
using CrossBlitz.UI.Widgets;
using float_oat.Desktop90;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Developer.D90
{
    public class D90RoomPage : MonoBehaviour
    {
        public Toggle isStartingRoom;
        public InputField roomName;
        public D90ContentLayout tilesLayout;
        public ToggleGroup tileToggleGroup;
        public LayoutItem tilePrefab;
        public D90Button editTileButton;
        public D90Button deleteTileButton;
        public D90Button roomEditorButton;
        public D90IntegerInput roomPositionX;
        public D90IntegerInput roomPositionY;
        public Dropdown weatherDropdown;
        public D90DatabaseDropdown audioDropdown;
        public D90DatabaseDropdown ambientDropdown;

        [BoxGroup("Background")] public Dropdown palettePreset;
        [BoxGroup("Background")] public Button createNewPaletteButton;
        [BoxGroup("Background")] public Button cancelNewPaletteButton;
        [BoxGroup("Background")] public Button saveNewPaletteButton;
        [BoxGroup("Background")] public InputField newPaletteName;
        [BoxGroup("Background")] public D90ColorField foregroundColor1;
        [BoxGroup("Background")] public D90ColorField backgroundColor1;
        [BoxGroup("Background")] public D90ColorField backgroundColor2;
        [BoxGroup("Background")] public D90ColorField backgroundColor3;
        [BoxGroup("Background")] public D90ColorField backgroundColor4;
        [BoxGroup("Background")] public D90ColorField backgroundColor5;
        [BoxGroup("Background")] public D90ColorField backgroundColor6;
        [BoxGroup("Background")] public D90ColorField cornerLayerColor1;
        [BoxGroup("Background")] public D90ColorField cornerLayerColor2;
        [BoxGroup("Background")] public D90ColorField diamondPatternColor;
        [BoxGroup("Background")] public Dropdown foregroundBlendMode;
        [BoxGroup("Background")] public D90IntegerInput scrollSpeed;
        [BoxGroup("Background")] public Button saveButton;

        [BoxGroup("Background Preview")] public BlendModeEffect foregroundBlend;
        [BoxGroup("Background Preview")] public Image foreground1;
        [BoxGroup("Background Preview")] public Image backgroundGradient1;
        [BoxGroup("Background Preview")] public Image backgroundGradient2;
        [BoxGroup("Background Preview")] public Image backgroundGradient3;
        [BoxGroup("Background Preview")] public Image backgroundGradient4;
        [BoxGroup("Background Preview")] public Image backgroundGradient5;
        [BoxGroup("Background Preview")] public Image backgroundGradient6;
        [BoxGroup("Background Preview")] public Image cornerLayer1a;
        [BoxGroup("Background Preview")] public Image cornerLayer2a;
        [BoxGroup("Background Preview")] public Image cornerLayer1b;
        [BoxGroup("Background Preview")] public Image cornerLayer2b;
        [BoxGroup("Background Preview")] public Image diamondPattern;
        [BoxGroup("Background Preview")] public UIBackground diamondScroller;

        private FableRoomData _room;
        private int _currentTile;

        private bool _creatingNewPalette;

        private void OnRoomNameChanged(string newName)
        {
            _room.DisplayName = newName;
            Save();
        }

        private void OnIsStartingRoomToggled(bool isOn)
        {
            _room.IsStartingRoom = isOn;
            Save();
        }

        private void OnTileSelected(bool isOn, string tileUid)
        {
            if (isOn)
            {
                _currentTile = int.Parse(tileUid);
            }
        }

        private void OnEditTile()
        {
            if (_currentTile != 0)
            {
                var tile = Db.MapDatabase.GetTile(_currentTile);

                if (tile != null)
                {
                    Events.Publish(this, EventType.OpenTileEditor, new OpenTileEditorEventArgs
                    {
                        tileUid = tile.uid
                    });
                }
            }
        }

        private void OnDeleteTile()
        {
            D90Tools.DisplayConfirmation("Are you sure?", OnDeleteTileConfirmation);
        }

        private void OnDeleteTileConfirmation(bool result)
        {
            if (result)
            {
                if (_currentTile != 0)
                {
                    var tile = Db.MapDatabase.GetTile(_currentTile);

                    if (tile != null)
                    {
                        tile.Decoration = string.Empty;
                        tile.displayName = string.Empty;
                        tile.Npc = string.Empty;
                        tile.NodeType = NodeType.None;
                    }
                }

                SetRoom(_room.Uid);
            }

        }


        private void Save()
        {
            Db.FablesDatabase.Save();
        }

        public void SetRoom(string roomId)
        {
            _room = Db.MapDatabase.GetRoom(roomId);

            isStartingRoom.SetIsOnWithoutNotify(_room.IsStartingRoom);
            isStartingRoom.onValueChanged.RemoveAllListeners();
            isStartingRoom.onValueChanged.AddListener(OnIsStartingRoomToggled);

            roomName.SetTextWithoutNotify(_room.DisplayName);
            roomName.onEndEdit.RemoveAllListeners();
            roomName.onEndEdit.AddListener(OnRoomNameChanged);

            editTileButton.onClick.RemoveAllListeners();
            editTileButton.onClick.AddListener(OnEditTile);

            deleteTileButton.onClick.RemoveAllListeners();
            deleteTileButton.onClick.AddListener(OnDeleteTile);

            roomEditorButton.onClick.RemoveAllListeners();
            roomEditorButton.onClick.AddListener(OnRoomEditor);

            tilesLayout.Clear();

            for (var i = 0; i < _room.Tiles.Count; i++)
            {
                var tile = _room.Tiles[i];
                if (tile.NodeType != NodeType.None && tile.NodeType != NodeType.Path)
                {
                    var tileContent = Instantiate(tilePrefab, tilesLayout.contentLayout);
                    tileContent.mainButton.content = tile.uid.ToString();
                    tileContent.mainButton.Toggle.group = tileToggleGroup;
                    tileContent.textLabels[0].text = GetTextForTile(tile);
                    tileContent.mainButton.OnValueChanged -= OnTileSelected;
                    tileContent.mainButton.OnValueChanged += OnTileSelected;
                    tilesLayout.AddItem(tileContent);

                    if (_currentTile == 0) _currentTile = tile.uid;
                }
            }

            // background

            roomPositionX.SetTextWithoutNotify(_room.Position.x.ToString());
            roomPositionX.OnChanged -= OnRoomPositionXChanged;
            roomPositionX.OnChanged += OnRoomPositionXChanged;

            roomPositionY.SetTextWithoutNotify(_room.Position.y.ToString());
            roomPositionY.OnChanged -= OnRoomPositionYChanged;
            roomPositionY.OnChanged += OnRoomPositionYChanged;

            weatherDropdown.ClearOptions();
            weatherDropdown.AddOptions(Enum.GetNames(typeof(FableRoomData.RoomWeather)).ToList());
            weatherDropdown.onValueChanged.RemoveAllListeners();
            weatherDropdown.onValueChanged.AddListener(OnWeatherChanged);
            weatherDropdown.SetValueWithoutNotify((int)_room.Weather);

            palettePreset.ClearOptions();
            palettePreset.AddOptions(Db.FableBackgroundPalettePresetsDatabase.Palettes.Select( p => p.presetName).ToList());
            palettePreset.onValueChanged.RemoveAllListeners();
            palettePreset.onValueChanged.AddListener(OnBackgroundPaletteChanged);

            createNewPaletteButton.onClick.RemoveAllListeners();
            createNewPaletteButton.onClick.AddListener(OnCreateNewPalette);

            cancelNewPaletteButton.onClick.RemoveAllListeners();
            cancelNewPaletteButton.onClick.AddListener(OnCancelNewPalette);

            saveNewPaletteButton.onClick.RemoveAllListeners();
            saveNewPaletteButton.onClick.AddListener(OnSaveNewPalette);

            foregroundColor1.OnColorChanged -= OnForegroundColor1Changed;
            foregroundColor1.OnColorChanged += OnForegroundColor1Changed;

            backgroundColor1.OnColorChanged -= OnBackgroundColor1Changed;
            backgroundColor1.OnColorChanged += OnBackgroundColor1Changed;

            backgroundColor2.OnColorChanged -= OnBackgroundColor2Changed;
            backgroundColor2.OnColorChanged += OnBackgroundColor2Changed;

            backgroundColor3.OnColorChanged -= OnBackgroundColor3Changed;
            backgroundColor3.OnColorChanged += OnBackgroundColor3Changed;

            backgroundColor4.OnColorChanged -= OnBackgroundColor4Changed;
            backgroundColor4.OnColorChanged += OnBackgroundColor4Changed;

            backgroundColor5.OnColorChanged -= OnBackgroundColor5Changed;
            backgroundColor5.OnColorChanged += OnBackgroundColor5Changed;

            backgroundColor6.OnColorChanged -= OnBackgroundColor6Changed;
            backgroundColor6.OnColorChanged += OnBackgroundColor6Changed;

            cornerLayerColor1.OnColorChanged -= OnCornerLayerColor1Changed;
            cornerLayerColor1.OnColorChanged += OnCornerLayerColor1Changed;

            cornerLayerColor2.OnColorChanged -= OnCornerLayerColor2Changed;
            cornerLayerColor2.OnColorChanged += OnCornerLayerColor2Changed;

            diamondPatternColor.OnColorChanged -= OnDiamondPatternColorChanged;
            diamondPatternColor.OnColorChanged += OnDiamondPatternColorChanged;

            scrollSpeed.OnChanged -= OnScrollSpeedChanged;
            scrollSpeed.OnChanged += OnScrollSpeedChanged;

            foregroundBlendMode.ClearOptions();
            foregroundBlendMode.AddOptions(Enum.GetNames(typeof(BlendMode)).ToList());
            foregroundBlendMode.onValueChanged.RemoveAllListeners();
            foregroundBlendMode.onValueChanged.AddListener(OnForegroundBlendModeChanged);

            RecolorBackgroundPalette();

            var palette = Db.FableBackgroundPalettePresetsDatabase.GetPalette(_room.BackgroundPalette);

            palettePreset.SetValueWithoutNotify(Db.FableBackgroundPalettePresetsDatabase.GetPaletteIndex(palette));

            SetPaletteProperties(palette);

            audioDropdown.OnValueChanged -= OnMapAudioChanged;
            audioDropdown.OnValueChanged += OnMapAudioChanged;
            audioDropdown.SetDropdownWithoutNotify(_room.Music);

            ambientDropdown.OnValueChanged -= OnMapAmbientChanged;
            ambientDropdown.OnValueChanged += OnMapAmbientChanged;
            ambientDropdown.SetDropdownWithoutNotify(_room.Ambient);

            saveButton.onClick.RemoveAllListeners();
            saveButton.onClick.AddListener(OnSaveBackgroundPalette);
        }

        private void OnMapAudioChanged(D90DatabaseDropdown dropdown, int value)
        {
            if (_room != null)
            {
                if (value == 0)
                {
                    _room.Music = string.Empty;
                }
                else
                {
                    _room.Music = dropdown.dropdown.options[value].text;
                }
            }
        }

        private void OnMapAmbientChanged(D90DatabaseDropdown dropdown, int value)
        {
            if (_room != null)
            {
                if (value == 0)
                {
                    _room.Ambient = string.Empty;
                }
                else
                {
                    _room.Ambient = dropdown.dropdown.options[value].text;
                }
            }
        }

        private void SetPaletteProperties(FableBackgroundPalettePreset palette)
        {
            foregroundColor1.SetColorWithoutNotify(palette.foregroundColor1, palette.foregroundAlpha1);
            backgroundColor1.SetColorWithoutNotify(palette.backgroundColor1, palette.backgroundAlpha1);
            backgroundColor2.SetColorWithoutNotify(palette.backgroundColor2, palette.backgroundAlpha2);
            backgroundColor3.SetColorWithoutNotify(palette.backgroundColor3, palette.backgroundAlpha3);
            backgroundColor4.SetColorWithoutNotify(palette.backgroundColor4, palette.backgroundAlpha4);
            backgroundColor5.SetColorWithoutNotify(palette.backgroundColor5, palette.backgroundAlpha5);
            backgroundColor6.SetColorWithoutNotify(palette.backgroundColor6, palette.backgroundAlpha6);
            cornerLayerColor1.SetColorWithoutNotify(palette.cornerLayerColor1, palette.cornerLayerAlpha1);
            cornerLayerColor2.SetColorWithoutNotify(palette.cornerLayerColor2, palette.cornerLayerAlpha2);
            diamondPatternColor.SetColorWithoutNotify(palette.diamondPatternColor, palette.diamondPatternAlpha);
            foregroundBlendMode.SetValueWithoutNotify((int)palette.foregroundBlend);
            scrollSpeed.SetTextWithoutNotify(palette.scrollSpeed.ToString());
        }

        private void OnForegroundBlendModeChanged(int index)
        {
            if (_creatingNewPalette) return;

            var palette = Db.FableBackgroundPalettePresetsDatabase.GetPalette(_room.BackgroundPalette);
            palette.foregroundBlend = (BlendMode)index;

            foregroundBlend.BlendMode = palette.foregroundBlend;
        }

        private void OnForegroundColor1Changed(string hex, float alpha, Color color)
        {
            // we dont want to change the current palette if we are editing a new one
            if (_creatingNewPalette) return;

            var palette = Db.FableBackgroundPalettePresetsDatabase.GetPalette(_room.BackgroundPalette);
            palette.foregroundColor1 = hex;
            palette.foregroundAlpha1 = alpha;

            foreground1.color = palette.foregroundColor1.ToColor(palette.foregroundAlpha1 / 255f);

        }

        private void OnBackgroundColor1Changed(string hex, float alpha, Color color)
        {
            // we dont want to change the current palette if we are editing a new one
            if (_creatingNewPalette) return;

            var palette = Db.FableBackgroundPalettePresetsDatabase.GetPalette(_room.BackgroundPalette);
            palette.backgroundColor1 = hex;
            palette.backgroundAlpha1 = alpha;

            backgroundGradient1.color = palette.backgroundColor1.ToColor(palette.backgroundAlpha1 / 255f);
        }

        private void OnBackgroundColor2Changed(string hex, float alpha, Color color)
        {
            // we dont want to change the current palette if we are editing a new one
            if (_creatingNewPalette) return;

            var palette = Db.FableBackgroundPalettePresetsDatabase.GetPalette(_room.BackgroundPalette);
            palette.backgroundColor2 = hex;
            palette.backgroundAlpha2 = alpha;

            backgroundGradient2.color = palette.backgroundColor2.ToColor(palette.backgroundAlpha2 / 255f);
        }

        private void OnBackgroundColor3Changed(string hex, float alpha, Color color)
        {
            // we dont want to change the current palette if we are editing a new one
            if (_creatingNewPalette) return;

            var palette = Db.FableBackgroundPalettePresetsDatabase.GetPalette(_room.BackgroundPalette);
            palette.backgroundColor3 = hex;
            palette.backgroundAlpha3 = alpha;

            backgroundGradient3.color = palette.backgroundColor3.ToColor(palette.backgroundAlpha3 / 255f);
        }

        private void OnBackgroundColor4Changed(string hex, float alpha, Color color)
        {
            // we dont want to change the current palette if we are editing a new one
            if (_creatingNewPalette) return;

            var palette = Db.FableBackgroundPalettePresetsDatabase.GetPalette(_room.BackgroundPalette);
            palette.backgroundColor4 = hex;
            palette.backgroundAlpha4 = alpha;

            backgroundGradient4.color = palette.backgroundColor4.ToColor(palette.backgroundAlpha4 / 255f);
        }

        private void OnBackgroundColor5Changed(string hex, float alpha, Color color)
        {
            // we dont want to change the current palette if we are editing a new one
            if (_creatingNewPalette) return;

            var palette = Db.FableBackgroundPalettePresetsDatabase.GetPalette(_room.BackgroundPalette);
            palette.backgroundColor5 = hex;
            palette.backgroundAlpha5 = alpha;

            backgroundGradient5.color = palette.backgroundColor5.ToColor(palette.backgroundAlpha5 / 255f);
        }

        private void OnBackgroundColor6Changed(string hex, float alpha, Color color)
        {
            // we dont want to change the current palette if we are editing a new one
            if (_creatingNewPalette) return;

            var palette = Db.FableBackgroundPalettePresetsDatabase.GetPalette(_room.BackgroundPalette);
            palette.backgroundColor6 = hex;
            palette.backgroundAlpha6 = alpha;

            backgroundGradient6.color = palette.backgroundColor6.ToColor(palette.backgroundAlpha6 / 255f);
        }

        private void OnCornerLayerColor1Changed(string hex, float alpha, Color color)
        {
            // we dont want to change the current palette if we are editing a new one
            if (_creatingNewPalette) return;

            var palette = Db.FableBackgroundPalettePresetsDatabase.GetPalette(_room.BackgroundPalette);
            palette.cornerLayerColor1 = hex;
            palette.cornerLayerAlpha1 = alpha;

            cornerLayer1a.color = palette.cornerLayerColor1.ToColor(palette.cornerLayerAlpha1 / 255f);
            cornerLayer1b.color = palette.cornerLayerColor1.ToColor(palette.cornerLayerAlpha1 / 255f);
        }

        private void OnCornerLayerColor2Changed(string hex, float alpha, Color color)
        {
            // we dont want to change the current palette if we are editing a new one
            if (_creatingNewPalette) return;

            var palette = Db.FableBackgroundPalettePresetsDatabase.GetPalette(_room.BackgroundPalette);
            palette.cornerLayerColor2 = hex;
            palette.cornerLayerAlpha2 = alpha;

            cornerLayer2a.color = palette.cornerLayerColor2.ToColor(palette.cornerLayerAlpha2 / 255f);
            cornerLayer2b.color = palette.cornerLayerColor2.ToColor(palette.cornerLayerAlpha2 / 255f);
        }

        private void OnDiamondPatternColorChanged(string hex, float alpha, Color color)
        {
            // we dont want to change the current palette if we are editing a new one
            if (_creatingNewPalette) return;

            var palette = Db.FableBackgroundPalettePresetsDatabase.GetPalette(_room.BackgroundPalette);
            palette.diamondPatternColor = hex;
            palette.diamondPatternAlpha = alpha;

            diamondPattern.color = palette.diamondPatternColor.ToColor(palette.diamondPatternAlpha / 255f);
        }

        private void OnScrollSpeedChanged(int speed)
        {
            // we dont want to change the current palette if we are editing a new one
            if (_creatingNewPalette) return;

            var palette = Db.FableBackgroundPalettePresetsDatabase.GetPalette(_room.BackgroundPalette);
            palette.scrollSpeed = speed;

            diamondScroller.m_speed = palette.scrollSpeed * 0.01f;
        }

        private void OnCreateNewPalette()
        {
            _creatingNewPalette = true;
        }

        private void OnCancelNewPalette()
        {
            _creatingNewPalette = false;
        }

        private void OnSaveNewPalette()
        {
            if (!_creatingNewPalette)
            {
                D90Tools.DisplayError("To create a new palette, click the 'New Palette' button.");
                return;
            }

            if (string.IsNullOrEmpty(newPaletteName.text))
            {
                D90Tools.DisplayError("The new palette needs a name to be valid!");
                return;
            }

            if (Db.FableBackgroundPalettePresetsDatabase.HasPalette(newPaletteName.text))
            {
                D90Tools.DisplayError($"Palette with name: {newPaletteName.text} already exists, change the name and try saving again.");
                return;
            }

            var palette = new FableBackgroundPalettePreset
            {
                presetName = newPaletteName.text,

                foregroundColor1 = foregroundColor1.Hex,
                foregroundAlpha1 = foregroundColor1.Alpha,

                backgroundColor1 = backgroundColor1.Hex,
                backgroundAlpha1 = backgroundColor1.Alpha,

                backgroundColor2 = backgroundColor2.Hex,
                backgroundAlpha2 = backgroundColor2.Alpha,

                backgroundColor3 = backgroundColor3.Hex,
                backgroundAlpha3 = backgroundColor3.Alpha,

                backgroundColor4 = backgroundColor4.Hex,
                backgroundAlpha4 = backgroundColor4.Alpha,

                backgroundColor5 = backgroundColor5.Hex,
                backgroundAlpha5 = backgroundColor5.Alpha,

                backgroundColor6 = backgroundColor6.Hex,
                backgroundAlpha6 = backgroundColor6.Alpha,

                cornerLayerColor1 = cornerLayerColor1.Hex,
                cornerLayerAlpha1 = cornerLayerColor1.Alpha,

                cornerLayerColor2 = cornerLayerColor2.Hex,
                cornerLayerAlpha2 = cornerLayerColor2.Alpha,

                diamondPatternColor = diamondPatternColor.Hex,
                diamondPatternAlpha = diamondPatternColor.Alpha,

                scrollSpeed = scrollSpeed.Value,

                foregroundBlend = (BlendMode)foregroundBlendMode.value
            };

            var index = Db.FableBackgroundPalettePresetsDatabase.AddNewPalette(palette);

            palettePreset.ClearOptions();
            palettePreset.AddOptions(Db.FableBackgroundPalettePresetsDatabase.Palettes.Select( p => p.presetName).ToList());

            OnBackgroundPaletteChanged(index);

            _creatingNewPalette = false;

            OnSaveBackgroundPalette();

        }

        private void OnRoomPositionXChanged(int x)
        {
            _room.Position = new Vector2Int(x, _room.Position.y);
            Save();
        }

        private void OnRoomPositionYChanged(int y)
        {
            _room.Position = new Vector2Int(_room.Position.x, y);
            Save();
        }

        private void OnWeatherChanged(int index)
        {
            _room.Weather = (FableRoomData.RoomWeather) index;
            Save();
        }

        private void OnBackgroundPaletteChanged(int index)
        {
            _room.BackgroundPalette = Db.FableBackgroundPalettePresetsDatabase.Palettes[index].presetName;
            SetPaletteProperties(Db.FableBackgroundPalettePresetsDatabase.Palettes[index]);
            RecolorBackgroundPalette();
        }

        private void RecolorBackgroundPalette()
        {
            if (string.IsNullOrEmpty(_room.BackgroundPalette))
            {
                OnBackgroundPaletteChanged(0);
                return;
            }

            var palette = Db.FableBackgroundPalettePresetsDatabase.GetPalette(_room.BackgroundPalette);

            foreground1.color = palette.foregroundColor1.ToColor(palette.foregroundAlpha1 / 255f);
            backgroundGradient1.color = palette.backgroundColor1.ToColor(palette.backgroundAlpha1 / 255f);
            backgroundGradient2.color = palette.backgroundColor2.ToColor(palette.backgroundAlpha2 / 255f);
            backgroundGradient3.color = palette.backgroundColor3.ToColor(palette.backgroundAlpha3 / 255f);
            backgroundGradient4.color = palette.backgroundColor4.ToColor(palette.backgroundAlpha4 / 255f);
            backgroundGradient5.color = palette.backgroundColor5.ToColor(palette.backgroundAlpha5 / 255f);
            backgroundGradient6.color = palette.backgroundColor6.ToColor(palette.backgroundAlpha6 / 255f);
            cornerLayer1a.color = palette.cornerLayerColor1.ToColor(palette.cornerLayerAlpha1 / 255f);
            cornerLayer2a.color = palette.cornerLayerColor2.ToColor(palette.cornerLayerAlpha2 / 255f);
            cornerLayer1b.color = palette.cornerLayerColor1.ToColor(palette.cornerLayerAlpha1 / 255f);
            cornerLayer2b.color = palette.cornerLayerColor2.ToColor(palette.cornerLayerAlpha2 / 255f);
            diamondPattern.color = palette.diamondPatternColor.ToColor(palette.diamondPatternAlpha / 255f);
            diamondScroller.m_speed = palette.scrollSpeed * 0.01f;
            foregroundBlend.BlendMode = palette.foregroundBlend;
        }

        private void Update()
        {
            editTileButton.interactable = _currentTile != 0;

            if (_creatingNewPalette)
            {
                saveNewPaletteButton.SetActive(true);
                cancelNewPaletteButton.SetActive(true);
                newPaletteName.SetActive(true);
            }
            else
            {
                saveNewPaletteButton.SetActive(false);
                cancelNewPaletteButton.SetActive(false);
                newPaletteName.SetActive(false);
            }
        }

        private void OnRoomEditor()
        {
            PlayerPrefs.SetString("_last_editor_room", _room.Uid);
            PlayerPrefs.Save();

            Events.Publish(this, EventType.OpenRoomEditor, new OpenRoomEditorEventArgs
            {
                roomUid = _room.Uid
            });
        }

        public static string GetTextForTile(FableTileData tile)
        {
            var display = tile.displayName;
            var color = "#000000";

            switch (tile.NodeType)
            {
                case NodeType.Battle:
                //case NodeType.BossBattle:
                {
                    color = string.IsNullOrEmpty(tile.battleUid) || Db.BattleDatabase.GetBattleData(tile.battleUid) == null ? "#E03131": "#000000";

                    display = $"{tile.displayName} ({tile.NodeType})";
                    break;
                }
                case NodeType.Chest:
                {
                    color = tile.items == null || tile.items.Count <= 0
                        ? "#E03131"
                        : "#000000";
                    display = $"{tile.displayName} (Chest)";
                    break;
                }
                case NodeType.Event1:
                case NodeType.Event2:
                {
                    color = string.IsNullOrEmpty(tile.cutsceneUid) && string.IsNullOrEmpty(tile.introCutsceneUid) ? "#E03131" : "#000000";
                    display = $"{tile.displayName} (Event)";
                    break;
                }
                case NodeType.Shop:
                {
                    color = tile.items == null || tile.items.Count <= 0 ? "#E03131" : "#000000";
                    display = $"{tile.displayName} (Shop)";
                    break;
                }
                case NodeType.ExitDown:
                case NodeType.ExitLeft:
                case NodeType.ExitRight:
                case NodeType.ExitUp:
                {
                    display = $"{tile.displayName} (Passage)";
                    break;
                }
            }

            if (!string.IsNullOrEmpty(tile.questId))
            {
                var quest = Db.FableQuestDatabase.GetQuest(tile.questId);
                display += $" (Quest({tile.questStep}): {quest.Name}";
            }

            display = $"<color={color}>{display}</color>";

            return display;
        }



        private void OnSaveBackgroundPalette()
        {
            if (_creatingNewPalette)
            {
                OnSaveNewPalette();
                return;
            }

            Save();
            Db.FableBackgroundPalettePresetsDatabase.Save();
        }
    }
}