using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Grid;
using CrossBlitz.Fables.Treasure;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.UI.Widgets;
using Mono.CSharp;
using Sirenix.OdinInspector;
using TakoBoyStudios.Events;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Developer.D90
{
    public class D90TileEditorWindow : MonoBehaviour
    {
        [BoxGroup("Disabled")] public GameObject disabledOverlay;
        [BoxGroup("Info")] public InputField nameLabel;
        [BoxGroup("Info")] public HexTileDisplayView tileAnimator;
        [BoxGroup("Info")] public D90DatabaseDropdown linkedMap;
        [BoxGroup("Battles")] public D90DatabaseDropdown linkedBattle;
        [BoxGroup("Cutscenes")] public D90DatabaseDropdown introCutsceneDropdown;
        [BoxGroup("Cutscenes")] public D90DatabaseDropdown outroCutsceneDropdown;
        [BoxGroup("Chest")] public Dropdown chestType;
        [BoxGroup("Shop")] public Dropdown shopType;
        [BoxGroup("Shop")] public D90DeckInspector shopItems;
        [BoxGroup("Passage")] public D90DatabaseDropdown connectedTileForPassageDropdown;
        [BoxGroup("Passage")] public Dropdown passageEnterDirection;
        [BoxGroup("Quests")] public D90DatabaseDropdown connectedQuest;
        [BoxGroup("Quests")] public InputField questStep;
        [BoxGroup("Other")] public Button copyButton;
        [BoxGroup("Other")] public Button pasteButton;
        [BoxGroup("Other")] public Button deleteButton;
        [BoxGroup("Other")]public InputField uidField;
        [BoxGroup("Other")]public Button resetUidButton;
        [BoxGroup("Other")]public D90DatabaseDropdown cardPoolDropdown;

        private FableTileData _tile;

        public event Action<int> OnTileChanged;

        private void Start()
        {
            Events.Subscribe(EventType.OnFableTileDataChanged, OnFableTileDataChanged);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnFableTileDataChanged, OnFableTileDataChanged);
        }

        private void OnFableTileDataChanged(IMessage message)
        {

        }

        private void OnTileNameChanged(string newName)
        {
            _tile.displayName = newName;
            Save();
        }

        private void OnLinkedBattleChanged(D90DatabaseDropdown dropdown, int value)
        {
            if (value == 0)
            {
                _tile.battleUid = string.Empty;
            }
            else
            {
                var linkedBattleName = dropdown.dropdown.options[value].text;
                var battle = Db.BattleDatabase.GetBattleDataByName(linkedBattleName);
                if (battle != null)
                {
                    _tile.battleUid = battle.Uid;
                }
            }

            Save();
        }

        private void OnIntroCutsceneChanged(int cutsceneValue)
        {
            var eventTitle = introCutsceneDropdown.dropdown.options[cutsceneValue].text;
            var cutscene = Db.CutsceneDatabase.GetCutsceneByEventTitle(eventTitle);
            var uid = cutscene == null ? string.Empty : cutscene.Uid;
            _tile.introCutsceneUid = uid;
            Save();
        }

        private void OnOutroCutsceneChanged(int cutsceneValue)
        {
            var eventTitle = outroCutsceneDropdown.dropdown.options[cutsceneValue].text;
            var cutscene = Db.CutsceneDatabase.GetCutsceneByEventTitle(eventTitle);
            var uid = cutscene == null ? string.Empty : cutscene.Uid;
            _tile.outroCutsceneUid = uid;
            Save();
        }

        private void OnEntranceChanged(D90DatabaseDropdown dropdown, int value)
        {
            var passageSplit = dropdown.dropdown.options[value].text.Split('/');

            if (passageSplit.Length < 3)
            {
                _tile.entranceTileUid = 0;
            }
            else
            {
                var passageTile = Db.MapDatabase.GetTileByDirectory(passageSplit[0], passageSplit[1], passageSplit[2]);
                _tile.entranceTileUid = passageTile?.uid ?? 0;
            }

            Save();

        }

        private void OnPassageEnterDirectionChanged(int value)
        {
            _tile.entranceDirection = (FableRoomData.RoomDirection) value;
            Save();
        }

        private void Update()
        {
            if (Time.frameCount % 120 == 0) // slow down how much this is called
            {
                if (pasteButton)
                {
                    pasteButton.interactable = TryGetTileDataOnClipboard(out var tileCopy);
                }
            }
        }

        private void Save()
        {
            // Db.MapDatabase.Save();
            // Db.FablesDatabase.Save();
            // Db.BattleDatabase.Save();
        }

        private void OnCopy()
        {
            GUIUtility.systemCopyBuffer = _tile.ToJson(true);
        }

        private void OnPaste()
        {
            if (TryGetTileDataOnClipboard(out var tileCopy))
            {
                 Db.MapDatabase.PasteTile(_tile.uid, tileCopy);
                 Open(_tile.uid);
                 OnTileChanged?.Invoke(_tile.uid);
            }
        }

        private void OnDelete()
        {
            Db.MapDatabase.ClearTile(_tile.uid);
            Open(_tile.uid);
            OnTileChanged?.Invoke(_tile.uid);
        }

        private bool TryGetTileDataOnClipboard(out FableTileData tileCopy)
        {
            if (string.IsNullOrEmpty(GUIUtility.systemCopyBuffer))
            {
                tileCopy = null;
                return false;
            }

            if (!string.IsNullOrEmpty(GUIUtility.systemCopyBuffer))
            {
                try
                {
                    tileCopy = DataModel.FromJson<FableTileData>(GUIUtility.systemCopyBuffer, true);

                    if (tileCopy == null)
                    {
                        return false;
                    }

                    return true;
                }
                catch
                {
                    tileCopy = null;
                    return false;
                }
            }

            tileCopy = null;
            return false;
        }

        public void Open(int tileUid)
        {
            if (tileUid == 0 && disabledOverlay != null)
            {
                disabledOverlay.SetActive(true);
                return;
            }
            if (disabledOverlay) disabledOverlay.SetActive(false);

            _tile = Db.MapDatabase.GetTile(tileUid);

            if (string.IsNullOrEmpty(_tile.battleUid) &&
                _tile.NodeType == NodeType.Battle)
            {
                var newBattleData = Db.BattleDatabase.GetNewBattleData();
                _tile.battleUid = newBattleData.Uid;
            }

            nameLabel.SetTextWithoutNotify(_tile.displayName);
            nameLabel.onEndEdit.RemoveAllListeners();
            nameLabel.onEndEdit.AddListener(OnTileNameChanged);
            tileAnimator.SetData(_tile);

            var battleData = Db.BattleDatabase.GetBattleData(_tile.battleUid);

            if (linkedBattle)
            {
                linkedBattle.OnValueChanged -= OnLinkedBattleChanged;
                linkedBattle.OnValueChanged += OnLinkedBattleChanged;

                if (battleData != null)
                {
                    linkedBattle.SetDropdownWithoutNotify(battleData.BattleName);
                }
                else
                {
                    linkedBattle.SetDropdownWithoutNotify("None");
                }
            }

            if (copyButton)
            {
                copyButton.onClick.RemoveAllListeners();
                copyButton.onClick.AddListener(OnCopy);
            }

            if (pasteButton)
            {
                pasteButton.onClick.RemoveAllListeners();
                pasteButton.onClick.AddListener(OnPaste);
            }

            if (deleteButton)
            {
                deleteButton.onClick.RemoveAllListeners();
                deleteButton.onClick.AddListener(OnDelete);
            }

            introCutsceneDropdown.SetDropdownWithoutNotify(Db.CutsceneDatabase.GetCutscene(_tile.introCutsceneUid)?.EventTitle);
            introCutsceneDropdown.dropdown.onValueChanged.RemoveAllListeners();
            introCutsceneDropdown.dropdown.onValueChanged.AddListener(OnIntroCutsceneChanged);

            outroCutsceneDropdown.SetDropdownWithoutNotify(Db.CutsceneDatabase.GetCutscene(_tile.outroCutsceneUid)?.EventTitle);
            outroCutsceneDropdown.dropdown.onValueChanged.RemoveAllListeners();
            outroCutsceneDropdown.dropdown.onValueChanged.AddListener(OnOutroCutsceneChanged);

            if (shopItems)
            {
                shopItems.OnCardsChanged -= OnShopItemsChanged;
                shopItems.OnCardsChanged += OnShopItemsChanged;
                shopItems.SetupCards(_tile.items.Select(item => new CardValue { id = item.ItemUid, count = item.Count })
                    .ToList());
            }

            if (shopType)
            {
                shopType.SetActive(_tile.NodeType == NodeType.Shop);
                shopType.ClearOptions();
                shopType.AddOptions(System.Enum.GetNames(typeof(ShopType)).ToList());
                shopType.SetValueWithoutNotify( (int)_tile.ShopType );
                shopType.onValueChanged.RemoveAllListeners();
                shopType.onValueChanged.AddListener(OnShopTypeChanged);
            }

            if (chestType)
            {
                chestType.SetActive(_tile.NodeType == NodeType.Chest);
                chestType.ClearOptions();
                chestType.AddOptions(System.Enum.GetNames(typeof(ChestType)).ToList());
                chestType.SetValueWithoutNotify( (int)_tile.ChestType );
                chestType.onValueChanged.RemoveAllListeners();
                chestType.onValueChanged.AddListener(OnChestTypeChanged);
            }


            if (connectedTileForPassageDropdown)
            {
                //connectedTileForPassageDropdown.SetActive(false);
                connectedTileForPassageDropdown.Init();
                var directory = Db.MapDatabase.GetDirectoryNameForTile(_tile.entranceTileUid);

                connectedTileForPassageDropdown.SetDropdownWithoutNotify(directory);
                connectedTileForPassageDropdown.OnValueChanged -= OnEntranceChanged;
                connectedTileForPassageDropdown.OnValueChanged += OnEntranceChanged;
            }

            if (passageEnterDirection)
            {
                passageEnterDirection.ClearOptions();
                passageEnterDirection.AddOptions(new List<string> { "Up", "Right", "Down", "Left" });
                passageEnterDirection.onValueChanged.RemoveAllListeners();
                passageEnterDirection.onValueChanged.AddListener(OnPassageEnterDirectionChanged);
            }

            if (linkedMap)
            {
                linkedMap.OnValueChanged -= OnLinkedMapChanged;
                linkedMap.OnValueChanged += OnLinkedMapChanged;

                var map = Db.MapDatabase.GetMap(_tile.linkedMapUid);
                linkedMap.SetDropdownWithoutNotify(map?.DisplayName);
            }

            if (connectedQuest)
            {
                connectedQuest.OnValueChanged -= OnConnectedQuestChanged;
                connectedQuest.OnValueChanged += OnConnectedQuestChanged;

                if (!string.IsNullOrEmpty(_tile.questId))
                {
                    var quest = Db.FableQuestDatabase.GetQuest(_tile.questId);
                    connectedQuest.SetDropdownWithoutNotify(quest.Name);
                }
                else
                {
                    connectedQuest.SetDropdownWithoutNotify("None");
                }
            }

            if (cardPoolDropdown)
            {
                cardPoolDropdown.OnValueChanged -= OnCardPoolChanged;
                cardPoolDropdown.OnValueChanged += OnCardPoolChanged;

                if (!string.IsNullOrEmpty(_tile.cardPool))
                {
                    var cardPool = Db.CardPoolsDatabase.GetCardPool(_tile.cardPool);
                    cardPoolDropdown.SetDropdownWithoutNotify(cardPool.name);
                }
                else
                {
                    cardPoolDropdown.SetDropdownWithoutNotify("None");
                }
            }

            if (questStep)
            {
                questStep.SetTextWithoutNotify(_tile.questStep.ToString());
                questStep.onValueChanged.RemoveAllListeners();
                questStep.onValueChanged.AddListener(OnQuestStepChanged);
            }

            if (uidField)
            {
                uidField.SetTextWithoutNotify(_tile.uid.ToString());
            }

            if (resetUidButton)
            {
                resetUidButton.onClick.RemoveAllListeners();
                resetUidButton.onClick.AddListener(OnResetUid);
            }
        }

        private void OnResetUid()
        {
            _tile.uid = GUID.CreateUniqueInt();
            uidField.SetTextWithoutNotify(_tile.uid.ToString());
            OnTileChanged?.Invoke(_tile.uid);
        }

        private void OnShopTypeChanged(int index)
        {
            _tile.ShopType = (ShopType)index;
            tileAnimator.SetData(_tile);
            OnTileChanged?.Invoke(_tile.uid);
            Save();
        }

        private void OnChestTypeChanged(int index)
        {
            _tile.ChestType = (ChestType)index;
            tileAnimator.SetData(_tile);
            OnTileChanged?.Invoke(_tile.uid);
            Save();
        }

        private void OnShopItemsChanged(List<CardValue> changedCards)
        {
            _tile.items = changedCards.Select(card => new ItemValue { ItemUid = card.id, Count = card.count })
                .ToList();
            Save();
        }
        private void OnConnectedQuestChanged(D90DatabaseDropdown dropdown, int value)
        {
            Debug.Log($"connected? {dropdown.dropdown.options[value].text}");

            var questUid = Db.FableQuestDatabase.GetQuestUidFromName(dropdown.dropdown.options[value].text);
            _tile.questId = questUid;
            OnTileChanged?.Invoke(_tile.uid);
            Save();

            Debug.Log($"Uid {questUid}");
        }

        private void OnCardPoolChanged(D90DatabaseDropdown dropdown, int value)
        {
            var cardPool = Db.CardPoolsDatabase.GetCardPoolByName(dropdown.dropdown.options[value].text);
            _tile.cardPool = cardPool.Uid;
            OnTileChanged?.Invoke(_tile.uid);
            Save();
        }
        private void OnQuestStepChanged(string text)
        {
            if (int.TryParse(text, out var value))
            {
                _tile.questStep = value;
                Save();
            }
        }

        private void OnCopyDeck()
        {
            var battleData = Db.BattleDatabase.GetBattleData(_tile.battleUid);

            if (battleData?.Deck != null)
            {
                GUIUtility.systemCopyBuffer = battleData.Deck.ToRecipe();
            }
        }

        private void OnPasteDeck()
        {
            if (!string.IsNullOrEmpty(GUIUtility.systemCopyBuffer))
            {
                var battleData = Db.BattleDatabase.GetBattleData(_tile.battleUid);

                if (battleData == null)
                {
                    battleData = Db.BattleDatabase.GetNewBattleData();
                    _tile.battleUid = battleData.Uid;
                }
                battleData.Deck = DeckData.FromRecipe(GUIUtility.systemCopyBuffer);

                Open(_tile.uid);

                Save();
            }
        }

        private void OnLinkedMapChanged(D90DatabaseDropdown dropdown, int index)
        {
            if (index <= 0)
            {
                _tile.linkedMapUid = string.Empty;
            }
            else
            {
                var map = Db.MapDatabase.GetMapByName(dropdown.dropdown.options[index].text);
                if (map!=null) _tile.linkedMapUid = map.Uid;
            }
        }

        // private void AddCardToCardLayout(CardValue value)
        // {
        //     var card = Db.CardDatabase.GetCard(value.id);
        //     if (card == null) return;
        //     var cardText = Instantiate(cardTextPrefab, cardLayout.contentLayout);
        //     cardText.mainButton.content = value.id;
        //     cardText.textLabels[0].text = card.name;
        //     cardText.textLabels[1].text = $"x{value.count}";
        //     cardLayout.AddItem(cardText);
        // }
        //
        // private void AddAccoladeToBattle(AccoladeData accoladeData)
        // {
        //     var accoladeInspector = Instantiate(accoladeInspectorPrefab, accoladePages.pageLayout);
        //     accoladeInspector.SetAccolade(accoladeData);
        //     accoladePages.AddPage(accoladeInspector.gameObject);
        // }
    }
}