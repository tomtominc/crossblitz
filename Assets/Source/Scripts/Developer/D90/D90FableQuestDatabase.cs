using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.UI.Widgets;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90FableQuestDatabase : MonoBehaviour
    {
        public D90ContentLayout questList;
        public LayoutItem questItemPrefab;

        public InputField questName;
        public InputField questSteps;
        public InputField questDescription;

        public Toggle sideQuestToggle;
        public D90Pages questObjectivePages;
        public LayoutItem questObjectivePagePrefab;

        public Button addButton;
        public Button saveButton;

        private string m_currentQuestUid;

        public void Open()
        {
            if (string.IsNullOrEmpty(m_currentQuestUid) && Db.FableQuestDatabase.Quests?.Count > 0)
            {
                m_currentQuestUid = Db.FableQuestDatabase.Quests[0].Uid;
            }

            questList.Clear();
            questObjectivePages.Clear();

            for (var i = 0; i < Db.FableQuestDatabase.Quests?.Count; i++)
            {
                var quest = Db.FableQuestDatabase.Quests[i];
                var questItem = Instantiate(questItemPrefab, questList.contentLayout, false);
                questItem.mainButton.content = quest.Uid;
                questItem.textLabels[0].text = quest.Name + $" ({quest.QuestSteps})";
                questItem.mainButton.OnValueChanged -= OnQuestClicked;
                questItem.mainButton.OnValueChanged += OnQuestClicked;
                questItem.mainButton.Toggle.SetIsOnWithoutNotify(quest.Uid == m_currentQuestUid);
                questList.AddItem(questItem);
            }

            questList.OnDelete -= OnDeleteQuest;
            questList.OnDelete += OnDeleteQuest;

            addButton.onClick.RemoveAllListeners();
            addButton.onClick.AddListener(OnAddQuest);

            saveButton.onClick.RemoveAllListeners();
            saveButton.onClick.AddListener(OnSave);

            if (!string.IsNullOrEmpty(m_currentQuestUid) && Db.FableQuestDatabase.Quests?.Count > 0)
            {
                var quest = Db.FableQuestDatabase.Quests.
                    Find(q => q.Uid == m_currentQuestUid);

                questName.SetTextWithoutNotify(quest.Name);
                questSteps.SetTextWithoutNotify(quest.QuestSteps.ToString());
                sideQuestToggle.SetIsOnWithoutNotify(quest.IsSideQuest);

                questDescription.SetTextWithoutNotify( quest.QuestDescription );
                questDescription.onEndEdit.RemoveAllListeners();
                questDescription.onEndEdit.AddListener(OnQuestDescriptionChanged);

                if (quest.QuestObjectives == null)
                {
                    quest.QuestObjectives = new List<string>();
                }

                if (quest.QuestObjectives.Count < quest.QuestSteps)
                {
                    for (var i = quest.QuestObjectives.Count; i < quest.QuestSteps; i++)
                    {
                        quest.QuestObjectives.Add(string.Empty);
                    }
                }

                for (var i = 0; i < quest.QuestObjectives.Count; i++)
                {
                    var questObjectivePage = Instantiate(questObjectivePagePrefab,questObjectivePages.pageLayout, false);
                    questObjectivePage.name = i.ToString();
                    questObjectivePage.inputFields[0].SetTextWithoutNotify(quest.QuestObjectives[i]);
                    questObjectivePage.OnInputFieldEndEdit += OnObjectiveChanged;
                    questObjectivePages.AddPage(questObjectivePage.gameObject);
                }
            }
        }

        private void OnQuestDescriptionChanged(string text)
        {
            var quest = Db.FableQuestDatabase.Quests.
                Find(q => q.Uid == m_currentQuestUid);
            quest.QuestDescription = text;
        }

        private void OnObjectiveChanged(string text, LayoutItem item)
        {
            var index = int.Parse(item.name);
            var quest = Db.FableQuestDatabase.Quests.Find(q => q.Uid == m_currentQuestUid);
            if (quest == null) return;
            quest.QuestObjectives[index] = text;
        }

        private void OnQuestClicked(bool isOn, string context)
        {
            m_currentQuestUid = context;
            Open();
        }

        private void OnDeleteQuest(LayoutItem item)
        {
            var questUid = item.mainButton.content;

            if (m_currentQuestUid != questUid)
            {
                return;
            }

            Db.FableQuestDatabase.Quests.RemoveAll(q => q.Uid == questUid);
            m_currentQuestUid = string.Empty;
            Open();
        }

        private void OnAddQuest()
        {
            var newQuest = Db.FableQuestDatabase.CreateNewQuest();
            m_currentQuestUid = newQuest.Uid;
            Open();
        }

        private void OnSave()
        {
            if (string.IsNullOrEmpty(m_currentQuestUid))
            {
                Debug.LogError("current quest is null!");
                return;
            }

            var quest = Db.FableQuestDatabase.Quests.Find(q => q.Uid == m_currentQuestUid);

            if (quest == null)
            {
                Debug.LogError($"could not find quest {m_currentQuestUid}!");
                return;
            }

            quest.Name = questName.text;
            quest.QuestSteps = int.Parse(questSteps.text);
            quest.IsSideQuest = sideQuestToggle.isOn;


            Db.FableQuestDatabase.Save();

            Open();
        }
    }
}