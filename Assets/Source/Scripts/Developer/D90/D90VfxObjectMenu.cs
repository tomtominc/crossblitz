using System;
using System.Linq;
using CrossBlitz.AssetManagement;
using CrossBlitz.Databases;
using CrossBlitz.UI.Widgets;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90VfxObjectMenu : MonoBehaviour
    {
        public LayoutItem itemPrefab;
        public D90ContentLayout vfxObjectList;
        public D90VfxFrameMenu vfxFrameMenu;

        public InputField displayName;
        public Dropdown parentType;
        public D90DatabaseDropdown animatorDropdown;
        public Dropdown animationDropdown;
        public Button animationAddButton;
        public D90ContentLayout animationsList;
        public Toggle createOnAwake;
        public Button addButton;
        public Button moveUpObjButton;
        public Button moveDownObjButton;

        private string m_vfxUid;
        private string m_vfxObjectUid;
        private string m_currentAnimationName;
        private string m_currentSelectedAnimation;

        public void RefreshMenu( string vfxUid )
        {
            m_vfxUid = vfxUid;

            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);

            addButton.onClick.RemoveAllListeners();
            addButton.onClick.AddListener(OnAddButton);

            if (vfx.objects.Count <= 0)
            {
                ClearMenu();
                return;
            }

            if (string.IsNullOrEmpty(m_vfxObjectUid))
            {
                m_vfxObjectUid = vfx.objects[0].Uid;
            }

            var vfxObject = vfx.GetObject(m_vfxObjectUid);

            displayName.onEndEdit.RemoveAllListeners();
            displayName.onEndEdit.AddListener(OnDisplayNameChanged);
            displayName.SetTextWithoutNotify(vfxObject.displayName);

            parentType.ClearOptions();
            parentType.AddOptions( Enum.GetNames(typeof(VfxObject.ParentType) ).ToList());
            parentType.SetValueWithoutNotify( (int)vfxObject.parentType );
            parentType.onValueChanged.RemoveAllListeners();
            parentType.onValueChanged.AddListener(OnParentChanged);

            animatorDropdown.SetDropdownWithoutNotify( vfxObject.animator );
            animatorDropdown.OnValueChanged -= OnAnimatorChanged;
            animatorDropdown.OnValueChanged += OnAnimatorChanged;

            createOnAwake.onValueChanged.RemoveAllListeners();
            createOnAwake.onValueChanged.AddListener(OnCreateOnAwakeChanged);
            createOnAwake.SetIsOnWithoutNotify( vfxObject.createOnAwake );

            moveUpObjButton.onClick.RemoveAllListeners();
            moveUpObjButton.onClick.AddListener(OnMoveObjUp);

            moveDownObjButton.onClick.RemoveAllListeners();
            moveDownObjButton.onClick.AddListener(OnMoveObjDown);

            vfxObjectList.Clear();
            vfxObjectList.OnDelete -= OnDelete;
            vfxObjectList.OnDelete += OnDelete;

            for (var i = 0; i < vfx.objects.Count; i++)
            {
                var vfxObj = vfx.objects[i];
                var vfxItem = Instantiate(itemPrefab, vfxObjectList.contentLayout, false);
                vfxItem.mainButton.content = vfxObj.Uid;
                vfxItem.textLabels[0].text = vfxObj.displayName;
                vfxItem.mainButton.OnValueChanged -= OnVfxObjectChanged;
                vfxItem.mainButton.OnValueChanged += OnVfxObjectChanged;

                if (vfxObj.Uid == m_vfxObjectUid)
                {
                    vfxItem.mainButton.Toggle.SetIsOnWithoutNotify(true);
                }

                vfxObjectList.AddItem(vfxItem);
            }

            animationDropdown.ClearOptions();

            animationsList.Clear();
            animationsList.OnDelete -= OnDelete;
            animationsList.OnDelete += OnDelete;

            if (!string.IsNullOrEmpty(vfxObject.animator))
            {
                animationsList.SetActive(true);

                for (var i = 0; i < vfxObject.animations.Count; i++)
                {
                    var animName = vfxObject.animations[i];
                    var animItem = Instantiate(itemPrefab, vfxObjectList.contentLayout, false);
                    animItem.mainButton.content = animName;
                    animItem.textLabels[0].text = animName;
                    animItem.mainButton.OnValueChanged -= OnAnimationClicked;
                    animItem.mainButton.OnValueChanged += OnAnimationClicked;

                    if (animName == m_currentSelectedAnimation)
                    {
                        animItem.mainButton.Toggle.SetIsOnWithoutNotify(true);
                    }

                    animationsList.AddItem(animItem);
                }

                animationDropdown.SetActive(true);
                animationDropdown.onValueChanged.RemoveAllListeners();
                animationDropdown.onValueChanged.AddListener(OnAnimationChanged);

                animationAddButton.onClick.RemoveAllListeners();
                animationAddButton.onClick.AddListener(OnAnimationAdd);

                var animator = AddressableReferenceLoader.GetAnimationAsset(vfxObject.animator);

                if (animator != null && animator.animations != null && animator.animations.Count > 0)
                {
                    animationDropdown.AddOptions( animator.animations.Select( a => a.name ).ToList() );

                    if (string.IsNullOrEmpty(m_currentAnimationName) && vfxObject.animations.Count > 0)
                    {
                        m_currentAnimationName = animator.animations[0].name;
                    }

                    var currentAnim = animator.animations.Find(a => a.name == m_currentAnimationName);
                    animationDropdown.SetValueWithoutNotify( animator.animations.IndexOf( currentAnim ));
                }
            }
            else
            {
                animationDropdown.SetActive(false);
                animationsList.SetActive(false);
            }

            vfxFrameMenu.RefreshMenu(m_vfxUid, m_vfxObjectUid);
        }

        private void OnAnimationAdd()
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);

            if (string.IsNullOrEmpty(vfxObject.animator))
            {
                return;
            }

            var animator = AddressableReferenceLoader.GetAnimationAsset(vfxObject.animator);

            if (animator == null)
            {
                Debug.LogError($"Could not find {vfxObject.animator}");
                return;
            }

            var frameCount = vfxObject.frames.Count;
            var animationWithName = animator.animations.Find(anim => anim.name == m_currentAnimationName);

            if (animationWithName == null)
            {
                Debug.LogError($"Could not animation: {m_currentAnimationName}");
                return;
            }

            for (var i = frameCount; i < animationWithName.frameDatas.Count; i++)
            {
                vfxObject.CreateNewFrame();
            }

            vfxObject.animations.Add( m_currentAnimationName );

            RefreshMenu(m_vfxUid);
        }

        private void OnAnimationClicked(bool isOn, string context)
        {
            if (isOn) m_currentSelectedAnimation = context;
        }

        private void OnMoveObjUp()
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            vfx.MoveObjUp(m_vfxObjectUid);
            RefreshMenu(m_vfxUid);
        }

        private void OnMoveObjDown()
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            vfx.MoveObjDown(m_vfxObjectUid);
            RefreshMenu(m_vfxUid);
        }

        private void OnAnimationChanged(int animationIndex)
        {
            m_currentAnimationName = animationDropdown.options[animationIndex].text;
        }

        private void OnDisplayNameChanged(string text)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            vfxObject.displayName = text;
            RefreshMenu(m_vfxUid);
        }

        private void OnParentChanged(int pType)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            vfxObject.parentType = (VfxObject.ParentType)pType;
            RefreshMenu(m_vfxUid);
        }

        private void OnAnimatorChanged(D90DatabaseDropdown dropdown, int animatorIndex)
        {
            var animatorName = dropdown.dropdown.options[animatorIndex].text;
            var animator = AddressableReferenceLoader.GetAnimationAsset(animatorName);
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            vfxObject.animator = animatorName;

            // var frameCount = vfxObject.frames.Count;
            // for (var i = frameCount; i < animator.animations.Count; i++)
            // {
            //     vfxObject.CreateNewFrame();
            // }

            RefreshMenu(m_vfxUid);
        }

        private void OnCreateOnAwakeChanged(bool isOn)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.GetObject(m_vfxObjectUid);
            vfxObject.createOnAwake = isOn;
            RefreshMenu(m_vfxUid);
        }

        private void OnDelete(LayoutItem item)
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            vfx.Delete(m_vfxObjectUid);
            m_vfxObjectUid = string.Empty;
            RefreshMenu(m_vfxUid);
        }

        private void OnVfxObjectChanged(bool isOn, string context)
        {
            if (isOn)
            {
                m_vfxObjectUid = context;
                RefreshMenu(m_vfxUid);
            }
        }

        private void OnAddButton()
        {
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObject = vfx.CreateNewObject();
            m_vfxObjectUid = vfxObject.Uid;
            RefreshMenu(m_vfxUid);
        }

        public void ClearMenu()
        {

        }
    }
}