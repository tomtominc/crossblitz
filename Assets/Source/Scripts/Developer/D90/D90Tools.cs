using System;
using System.Collections.Generic;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Grid;
using CrossBlitz.InputAPI;
using CrossBlitz.States;
using CrossBlitz.Transition;
using CrossBlitz.ViewAPI.Popups;
using float_oat.Desktop90;
using MEC;
using Sirenix.OdinInspector;
using TakoBoyStudios.Events;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Developer.D90
{
    public class D90Tools : MonoBehaviour
    {
        public static D90Tools Instance;
        public DesktopIcon itemDbIcon;
        public DesktopIcon cardDbIcon;
        public DesktopIcon cutsceneDbIcon;
        public DesktopIcon mapsDbIcon;
        public DesktopIcon fablesDbIcon;
        public DesktopIcon audioDbIcon;
        public DesktopIcon deckRecipeIcon;
        public DesktopIcon battleDatabaseIcon;
        public DesktopIcon vfxEditorIcon;
        public DesktopIcon cardPoolIcon;
        public DesktopIcon tutorialIcon;

        public RectTransform desktopLayout;
        public D90BookEditor bookEditorPrefab;
        public D90ChapterEditor chapterEditorPrefab;
        public D90TileEditorWindow tileEditorPrefab;
        public D90CutsceneEditor cutsceneEditorPrefab;
        public D90DialogueEditor dialogueEditorPrefab;
        public D90CardInspector cardEditorPrefab;
        public D90AudioDatabase audioDatabase;
        public D90MapDatabase mapDatabase;
        public D90FableQuestDatabase questDatabase;
        public D90DeckRecipeEditor deckRecipe;
        public D90BattleDatabaseEditor battleDatabaseEditor;
        public D90VfxDatabase vfxEditor;
        public D90CardPoolEditor cardPoolEditor;
        public D90TutorialEditor tutorialEditor;

        [BoxGroup("Quick Actions")] public Button mapEditorButton;

        public WindowController alertWindow;
        public D90Confirmation confirmationWindow;
        public Text alertMessage;

        private void Awake()
        {
            Instance = this;

            Events.Subscribe(EventType.OpenBookEditor, OnOpenBookEditor);
            Events.Subscribe(EventType.OpenChapterInfo, OnOpenChapterInfo);
            Events.Subscribe(EventType.OpenTileEditor, OnOpenTileInfo);
            Events.Subscribe(EventType.OpenCutsceneEditor, OnOpenCutsceneEditor);
            Events.Subscribe(EventType.OpenDialogueEditor, OnOpenDialogueEditor);
            Events.Subscribe(EventType.OpenCardEditorInstance, OnOpenCardEditor);
            Events.Subscribe(EventType.OpenItemEditor, OnOpenItemEditor);
            Events.Subscribe(EventType.OpenTileInfo, OpenTileInfo);
            Events.Subscribe(EventType.OnMenuStatusChanged, OnMenuStatusChanged);

            itemDbIcon.OnDoubleClick.AddListener(OpenItemDb);
            cardDbIcon.OnDoubleClick.AddListener(OpenCardDb);
            mapsDbIcon.OnDoubleClick.AddListener(OpenMapDatabase);
            fablesDbIcon.OnDoubleClick.AddListener(OpenFables);
            cutsceneDbIcon.OnDoubleClick.AddListener(OpenCutsceneEditor);
            audioDbIcon.OnDoubleClick.AddListener(OpenAudioDatabase);
            deckRecipeIcon.OnDoubleClick.AddListener(OpenDeckRecipes);
            battleDatabaseIcon.OnDoubleClick.AddListener(OpenBattleDatabase);
            vfxEditorIcon.OnDoubleClick.AddListener(OpenVfxEditor);
            cardPoolIcon.OnDoubleClick.AddListener(OpenCardPoolEditor);
            tutorialIcon.OnDoubleClick.AddListener(OpenTutorialEditor);

            // actions--
            mapEditorButton.onClick.AddListener(OnMapEditorButton);
        }

        private void OnMenuStatusChanged(IMessage message)
        {
            if (message.Data is OnMenuStatusChangedEventArgs args)
            {

                if (args.Status == OnMenuStatusChangedEventArgs.MenuStatus.Opened)
                {
                    GetComponent<Canvas>().enabled = false;
                }
                else if (args.Status == OnMenuStatusChangedEventArgs.MenuStatus.Closed)
                {
                    GetComponent<Canvas>().enabled = true;
                }
            }
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OpenBookEditor, OnOpenBookEditor);
            Events.Unsubscribe(EventType.OpenChapterInfo, OnOpenChapterInfo);
            Events.Unsubscribe(EventType.OpenTileEditor, OnOpenTileInfo);
            Events.Unsubscribe(EventType.OpenCutsceneEditor, OnOpenCutsceneEditor);
            Events.Unsubscribe(EventType.OpenDialogueEditor, OnOpenDialogueEditor);
            Events.Unsubscribe(EventType.OpenCardEditorInstance, OnOpenCardEditor);
            Events.Unsubscribe(EventType.OpenItemEditor, OnOpenItemEditor);
            Events.Unsubscribe(EventType.OpenTileInfo, OpenTileInfo);
            Events.Unsubscribe(EventType.OnMenuStatusChanged, OnMenuStatusChanged);
        }

        public void OnMapEditorButton()
        {
            var roomUid = string.Empty;

            if (PlayerPrefs.HasKey("_last_editor_room"))
            {
                roomUid = PlayerPrefs.GetString("_last_editor_room");
            }
            else
            {
                roomUid = Db.MapDatabase.MapData[0].Rooms[0].Uid;
            }

            Events.Publish(this, EventType.OpenRoomEditor, new OpenRoomEditorEventArgs
            {
                roomUid = roomUid
            });
        }

        public void OpenTutorialEditor()
        {
            var cp = Instantiate(tutorialEditor, desktopLayout,false);
            cp.Open(string.Empty);
        }

        public void OpenCardPoolEditor()
        {
            var cp = Instantiate(cardPoolEditor, desktopLayout,false);
            cp.Open(string.Empty);
        }

        public void OpenVfxEditor()
        {
            var battleDatabase = Instantiate(vfxEditor, desktopLayout,false);
            battleDatabase.Open();
        }

        public void OpenBattleDatabase()
        {
            var battleDatabase = Instantiate(battleDatabaseEditor, desktopLayout,false);
            battleDatabase.Open(string.Empty);
        }

        public void OpenBattleDatabase(string battleUid)
        {
            var battleDatabase = Instantiate(battleDatabaseEditor, desktopLayout,false);
            battleDatabase.Open(battleUid);
        }

        public void OpenDeckRecipes()
        {
            var deckRecipeDb = Instantiate(deckRecipe, desktopLayout,false);
            deckRecipeDb.Open(string.Empty);
        }

        public void OpenAudioDatabase()
        {
            var audioDb = Instantiate(audioDatabase, desktopLayout,false);
            audioDb.Open();
        }

        private void OpenItemDb()
        {
            var questDb = Instantiate(questDatabase, desktopLayout,false);
            questDb.Open();
        }

        private void OpenCardDb()
        {
            Events.Publish(this, EventType.OpenCardEditorInstance, null);
        }

        public void OpenMapDatabase()
        {
            var mapDb = Instantiate(mapDatabase, desktopLayout,false);
            mapDb.Open();
        }

        private void OpenFables()
        {
            Events.Publish(this, EventType.OpenBookEditor, null);
        }

        private void OpenCutsceneEditor()
        {
            Events.Publish(this, EventType.OpenCutsceneEditor, new OpenCutsceneEditorEventArgs());
        }

        private void OnOpenCardEditor(IMessage message)
        {
            var cardEditor = Instantiate(cardEditorPrefab, desktopLayout, false);
            cardEditor.Open((OpenCardEditorInstanceEventArgs)message.Data);
        }

        private void OnOpenBookEditor(IMessage message)
        {
            var bookEditor = Instantiate(bookEditorPrefab, desktopLayout,false);
            bookEditor.Open();
        }

        private void OnOpenItemEditor(IMessage message)
        {
            Debug.LogError("Not implemented yet.");
        }

        private void OnOpenChapterInfo(IMessage message)
        {
            if (message.Data is OnOpenChapterInfoEventArgs args)
            {
                var chapterEditor = Instantiate(chapterEditorPrefab, desktopLayout,false);
                chapterEditor.Open(args.chapterUid);
            }
        }

        private void OnOpenTileInfo(IMessage message)
        {
            if (message.Data is OpenTileEditorEventArgs args)
            {
                var tileEditor = Instantiate(tileEditorPrefab, desktopLayout, false);
                tileEditor.Open(args.tileUid);
            }
        }

        private void OpenTileInfo(IMessage message)
        {
            if (message.Data is OpenTileInfoEventArgs args)
            {
                var tileEditor = Instantiate(tileEditorPrefab, desktopLayout, false);
                if (args.tileData != null) tileEditor.Open(args.tileData.uid);
            }
        }

        private void OnOpenCutsceneEditor(IMessage message)
        {
            if (message.Data is OpenCutsceneEditorEventArgs args)
            {
                var cutsceneEditor = Instantiate(cutsceneEditorPrefab, desktopLayout, false);
                cutsceneEditor.Open(args);
            }
        }

        private void OnOpenDialogueEditor(IMessage message)
        {

        }

        public static void DisplayError(string errorMessage)
        {
            Instance.alertWindow.Open();
            Instance.alertMessage.text = errorMessage;
        }

        public static void DisplayConfirmation (string message, Action<bool> action)
        {
            Timing.RunCoroutine(DisplayConfirmationCoroutine(message, action));
        }

        private static IEnumerator<float> DisplayConfirmationCoroutine(string message, Action<bool> action)
        {
            Instance.confirmationWindow.Open();
            yield return Timing.WaitUntilTrue(() => Instance.confirmationWindow.is_clicked);
            action?.Invoke(Instance.confirmationWindow.m_result);
            Instance.confirmationWindow.Close();
        }

    }
}