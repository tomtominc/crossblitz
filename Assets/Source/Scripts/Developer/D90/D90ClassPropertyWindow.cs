using float_oat.Desktop90;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90ClassPropertyWindow : MonoBehaviour
    {
        public Text titleLabel;
        public Text buttonLabel;
        public D90Button openButton;
        public WindowController window;

        private void Start()
        {
            openButton.onClick.AddListener(OpenWindow);
        }

        private void OpenWindow()
        {
            window.SetActive(true);
        }
    }
}