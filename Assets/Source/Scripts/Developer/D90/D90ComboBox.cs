using System;
using System.Collections.Generic;
using CrossBlitz.UI.Widgets;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90ComboBox : MonoBehaviour
    {
        public Text title;
        public LayoutItem optionTextPrefab;
        public D90ContentLayout optionsLayout;
        public Dropdown dropdown;
        public Button addButton;

        public event Action<int> OnAddedFlag;
        public event Action<int> OnRemovedFlag;

        // ADD value = value | other;
        // REMOVE value = value & ~other;
        // IS return value & other;

        private int m_value;
        private List<OptionValue> m_options;
        private int m_dropdownValue;
        private string m_selectedOption;

        public void SetValueWithoutNotify(int value, List<OptionValue> options)
        {
            m_value = value;
            m_options = options;

            dropdown.ClearOptions();
            dropdown.AddOptions(options.ConvertAll( op => op.name ));
            dropdown.onValueChanged.RemoveAllListeners();
            dropdown.onValueChanged.AddListener(OnDropdownValueChanged);
            dropdown.SetValueWithoutNotify(m_dropdownValue);

            optionsLayout.Clear();

            for (var i = 0; i < options.Count; i++)
            {
                //Debug.Log($"Checking option: {options[i].name} {m_value} & {options[i].value} = {m_value & options[i].value}");
                if ((m_value & options[i].value) > 0)
                {
                    var optionText = Instantiate(optionTextPrefab, optionsLayout.contentLayout, false);
                    optionText.mainButton.content = options[i].name;
                    optionText.textLabels[0].text = options[i].name;
                    if (string.IsNullOrEmpty(m_selectedOption) && i == 0) m_selectedOption = options[i].name;
                    optionText.mainButton.Toggle.SetIsOnWithoutNotify( options[i].name == m_selectedOption );
                    optionText.mainButton.OnValueChanged += OnSelectedOption;
                    optionsLayout.AddItem(optionText);
                   // Debug.LogError($"Adding option {options[i].name}");
                }
            }

            optionsLayout.OnDelete -= OnDeletedOption;
            optionsLayout.OnDelete += OnDeletedOption;

            addButton.onClick.RemoveAllListeners();
            addButton.onClick.AddListener(OnAddButton);
        }

        private void OnDeletedOption(LayoutItem item)
        {
            var option = m_options.Find(op => op.name == item.mainButton.content);
            if (!string.IsNullOrEmpty(option.name) && m_selectedOption == option.name)
            {
                m_value &= ~option.value;
                SetValueWithoutNotify(m_value, m_options);
            }
        }

        private void OnAddButton()
        {
            var option = m_options[m_dropdownValue];

            m_value |= option.value;
            SetValueWithoutNotify(m_value, m_options);
        }

        private void OnSelectedOption(bool isOn, string context)
        {
            m_selectedOption = context;
        }

        public void OnAdd(int value)
        {

        }

        public void OnDropdownValueChanged(int value)
        {
            m_dropdownValue = value;
        }

        public struct OptionValue
        {
            public string name;
            public int value;
        }

    }
}