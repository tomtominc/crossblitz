using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Data;
using CrossBlitz.UI.Widgets;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90CardLocationsEditor  : MonoBehaviour
    {
        public GameObject disabledOverlay;
        public D90ContentLayout cardLocationsLayout;
        public LayoutItem locationItemPrefab;
        public Button generateCardLocationsButton;

        private string m_cardId;
        private string m_currentItem;

        public void Init(string cardId)
        {
            m_cardId = cardId;

            if (!Db.Initialized)
            {
                Db.Initialize();
            }

            if (string.IsNullOrEmpty(m_cardId))
            {
                disabledOverlay.SetActive(true);
                return;
            }

            var card = Db.CardDatabase.GetCard(m_cardId);

            if (card == null)
            {
                disabledOverlay.SetActive(true);
                return;
            }

            // if (string.IsNullOrEmpty(m_currentItem) && card.locations.Count > 0)
            // {
            //     m_currentItem = card.locations[0].Uid;
            // }

            cardLocationsLayout.Clear();

            // for (var i = 0; i < card.locations.Count; i++)
            // {
            //     var location = card.locations[i];
            //     var locationItem = Instantiate(locationItemPrefab, cardLocationsLayout.contentLayout, false);
            //     locationItem.mainButton.content = location.Uid;
            //     locationItem.mainButton.Toggle.SetIsOnWithoutNotify( m_currentItem  == location.Uid);
            //     locationItem.mainButton.OnValueChanged -= OnLocationToggled;
            //     locationItem.mainButton.OnValueChanged += OnLocationToggled;
            //
            //     var content = $"[{location.location}] ";
            //
            //     switch (location.location)
            //     {
            //         case CardLocationData.Location.StartingDeck:
            //             content += $"{location.startingDeckHeroId}";
            //             break;
            //         case CardLocationData.Location.CardPrizePool:
            //             content += $"{location.chapterNamePrizePool}";
            //             break;
            //         case CardLocationData.Location.LevelReward:
            //             content += $"{location.levelRewardHeroId}";
            //             break;
            //         case CardLocationData.Location.Chest:
            //         case CardLocationData.Location.Shop:
            //             content += $"{Db.MapDatabase.GetDirectoryNameForTile(location.tileUid)}";
            //             break;
            //         case CardLocationData.Location.Cutscene:
            //             var cutscene = Db.CutsceneDatabase.GetCutscene(location.cutsceneUid);
            //             if (cutscene != null) content += $"\"{cutscene.EventTitle}\"";
            //             break;
            //         case CardLocationData.Location.Battle:
            //             var battle = Db.BattleDatabase.GetBattleData(location.battleUid);
            //             if (battle != null) content += $"\"{battle.BattleName}\"";
            //             break;
            //     }
            //
            //     locationItem.textLabels[0].text = content;
            // }

            if (generateCardLocationsButton)
            {
                generateCardLocationsButton.onClick.RemoveAllListeners();
                generateCardLocationsButton.onClick.AddListener(GenerateCardLocations);
            }

            disabledOverlay.SetActive(false);
        }

        private void OnLocationToggled(bool isOn, string context)
        {
            m_currentItem = context;

            Init(m_cardId);
        }

        private void GenerateCardLocations()
        {
            UpdateCardLocationData();

            Init(m_cardId);
        }
        public static void UpdateCardLocationData()
        {
            D90ChapterEditor.ConfigureRewardPool();

            // for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
            // {
            //     Db.CardDatabase.Cards[i].locations = new List<CardLocationData>();
            // }

            var cardDic = Db.CardDatabase.GetCardDictionaryMap();

        //     for (var i = 0; i < Db.HeroDatabase.Heroes.Count; i++)
        //     {
        //         var hero = Db.HeroDatabase.Heroes[i];
        //         var heroStarterDeck = Db.DeckRecipeDatabase.GetRecipeByName($"{hero.id} Starter Deck");
        //
        //         if (heroStarterDeck?.deckData != null)
        //         {
        //             for (var j = 0; j < heroStarterDeck.deckData.cards.Count; j++)
        //             {
        //                 var cardId = heroStarterDeck.deckData.cards[j].id;
        //
        //                 if (!string.IsNullOrEmpty(cardId) && cardDic.TryGetValue(cardId, out var card))
        //                 {
        //                     var location = new CardLocationData
        //                     {
        //                         location = CardLocationData.Location.StartingDeck,
        //                         startingDeckHeroId = hero.id
        //                     };
        //
        //                     location.UpdateIdentifier();
        //                     card.locations.Add( location );
        //                 }
        //             }
        //         }
        //
        //         if (hero.levelTree?.levels != null)
        //         {
        //             for (var j = 0; j < hero.levelTree.levels.Count; j++)
        //             {
        //                 var level = hero.levelTree.levels[j];
        //
        //                 if (level.type != LevelReward.EType.Card) continue;
        //
        //                 var cardId = level.cardId;
        //
        //                 if (!string.IsNullOrEmpty(cardId) && cardDic.TryGetValue(cardId, out var card))
        //                 {
        //                     var location = new CardLocationData
        //                     {
        //                         location = CardLocationData.Location.LevelReward,
        //                         levelRewardHeroId = hero.id,
        //                         levelRewardUid = level.uid
        //                     };
        //
        //                     location.UpdateIdentifier();
        //                     card.locations.Add(location);
        //                 }
        //             }
        //         }
        //     }
        //
        //     var excludedCards = new List<string>();
        //
        //     for (var i = 0; i < Db.FablesDatabase.Books.Count; i++)
        //     {
        //         for (var j = 0; j < Db.FablesDatabase.Books[i].Chapters.Count; j++)
        //         {
        //             var chapter = Db.FablesDatabase.Books[i].Chapters[j];
        //
        //             for (var k = 0; k < chapter.ExcludedPrizeCards.Count; k++)
        //             {
        //                 excludedCards.AddDistinct(chapter.ExcludedPrizeCards[k]);
        //             }
        //
        //             for (var k = 0; k < chapter.PrizeCardPool.Count;k++)
        //             {
        //                 var cardId = chapter.PrizeCardPool[k];
        //
        //                 if (!string.IsNullOrEmpty(cardId) && cardDic.TryGetValue(cardId, out var card))
        //                 {
        //                     var location = new CardLocationData
        //                     {
        //                         location = CardLocationData.Location.CardPrizePool,
        //                         chapterNamePrizePool = $"{chapter.ChapterHero}-{chapter.BookNumber}-{chapter.ChapterNumber}"
        //                     };
        //
        //                     location.UpdateIdentifier();
        //                     card.locations.Add(location);
        //                 }
        //             }
        //         }
        //     }
        //
        //     for (var i = 0; i < Db.CutsceneDatabase.Cutscenes.Count; i++)
        //     {
        //         var cutscene = Db.CutsceneDatabase.Cutscenes[i];
        //         // var tile = Db.MapDatabase.GetTileByCutscene(cutscene.Uid);
        //         // if (tile == null) continue;
        //         // var room = Db.MapDatabase.GetRoomByTileUid(tile.uid);
        //         // var map = Db.MapDatabase.GetMapByRoomUid(room.Uid);
        //
        //         for (var j = 0; j < cutscene.RewardItems.Count; j++)
        //         {
        //             var cardId = cutscene.RewardItems[j].ItemUid;
        //
        //             Debug.Log($"Cutscene reward = {cardId}");
        //
        //             if (!string.IsNullOrEmpty(cardId) && cardDic.TryGetValue(cardId, out var card))
        //             {
        //                 var location = new CardLocationData
        //                 {
        //                     location = CardLocationData.Location.Cutscene,
        //                     // mapUid = map.Uid,
        //                     // roomUid = room.Uid,
        //                     // tileUid = tile.uid,
        //                     cutsceneUid = cutscene.Uid,
        //                 };
        //
        //                 location.UpdateIdentifier();
        //                 card.locations.Add(location);
        //             }
        //         }
        //     }
        //
        //     for (var i = 0; i < Db.MapDatabase.MapData.Count; i++)
        //     {
        //         var map = Db.MapDatabase.MapData[i];
        //
        //         for (var j = 0; j < map.Rooms.Count; j++)
        //         {
        //             var room = map.Rooms[j];
        //
        //             for (var k = 0; k < room.Tiles.Count; k++)
        //             {
        //                 var tile = room.Tiles[k];
        //
        //                 switch (tile.NodeType)
        //                 {
        //                     case NodeType.Battle:
        //                     {
        //                         var battle = Db.BattleDatabase.GetBattleData(tile.battleUid);
        //
        //                         for (var l = 0; l < battle?.Deck?.cards?.Count; l++)
        //                         {
        //                             var cardId = battle.Deck.cards[l].id;
        //
        //                             if (excludedCards.Contains(cardId))
        //                             {
        //                                 continue;
        //                             }
        //
        //                             if (battle.ExcludedCards.Contains(cardId))
        //                             {
        //                                 continue;
        //                             }
        //
        //                             if (!string.IsNullOrEmpty(cardId) && cardDic.TryGetValue(cardId, out var card))
        //                             {
        //                                 var location = new CardLocationData
        //                                 {
        //                                     location = CardLocationData.Location.Battle,
        //                                     mapUid = map.Uid,
        //                                     roomUid = room.Uid,
        //                                     tileUid = tile.uid,
        //                                     battleUid = battle.Uid,
        //                                 };
        //
        //                                 location.UpdateIdentifier();
        //                                 card.locations.Add(location);
        //                             }
        //                         }
        //
        //                         break;
        //                     }
        //                     case NodeType.Chest:
        //                     {
        //                         for (var l = 0; l < tile.items.Count; l++)
        //                         {
        //                             var cardId = tile.items[l].ItemUid;
        //
        //                             if (!string.IsNullOrEmpty(cardId) && cardDic.TryGetValue(cardId, out var card))
        //                             {
        //                                 var location = new CardLocationData
        //                                 {
        //                                     location = CardLocationData.Location.Chest,
        //                                     mapUid = map.Uid,
        //                                     roomUid = room.Uid,
        //                                     tileUid = tile.uid,
        //                                     chestTileUid = tile.uid
        //                                 };
        //
        //                                 location.UpdateIdentifier();
        //                                 card.locations.Add(location);
        //                             }
        //                         }
        //
        //                         break;
        //                     }
        //                     case NodeType.Shop:
        //                     {
        //                         for (var l = 0; l < tile.items?.Count; l++)
        //                         {
        //                             var item = tile.items[l];
        //
        //                             if (!string.IsNullOrEmpty(item.ItemUid) && cardDic.TryGetValue(item.ItemUid, out var card))
        //                             {
        //                                 var location = new CardLocationData
        //                                 {
        //                                     location = CardLocationData.Location.Shop,
        //                                     mapUid = map.Uid,
        //                                     roomUid = room.Uid,
        //                                     tileUid = tile.uid,
        //                                     shopTileUid = tile.uid
        //                                 };
        //
        //                                 location.UpdateIdentifier();
        //                                 card.locations.Add(location);
        //                             }
        //                         }
        //
        //                         break;
        //                     }
        //                 }
        //             }
        //         }
        //     }
        }
        
    }
}