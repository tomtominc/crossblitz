using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Cutscene;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.InputAPI;
using CrossBlitz.UI.Widgets;
using Sirenix.OdinInspector;
using UnityAsync;
using UnityEngine;
using UnityEngine.UI;
using MEC;
using CrossBlitz.Audio;

namespace CrossBlitz.Developer.D90
{
    public class D90CutsceneEditor : MonoBehaviour
    {
        public D90ContentLayout cutscenesList;
        public LayoutItem cutsceneLayoutPrefab;
        public D90ContentLayout dialogueList;
        public InputField cutsceneName;
        public Dropdown cutsceneType;

        public Dropdown speakingCharacterDropdown;
        public InputField speakingCharacterDialogue;
        public Toggle hasChoices;
        public Toggle usesThoughtBubble;
        public InputField choice1;
        public InputField choice2;
        public D90DatabaseDropdown cutsceneChoice1;
        public D90DatabaseDropdown cutsceneChoice2;
        public D90Confirmation confirmationWindow;
        public Button newCutsceneButton;
        public Button deleteCutsceneButton;
        public Button playButton;
        public Button playIndexButton;
        public Dropdown cutsceneLocation;
        public SpriteAnimationAsset eventSceneLocationAnimator;

        [BoxGroup("Dialogue")] public CanvasGroup disableChoiceCanvas;
        [BoxGroup("Dialogue")] public Button addDialogueButton;
        [BoxGroup("Dialogue")] public Button saveCurrentDialogue;
        [BoxGroup("Dialogue")] public Button deleteCurrentDialogue;
        [BoxGroup("Dialogue")] public Button moveUpButton;
        [BoxGroup("Dialogue")] public Button moveDownButton;

        [BoxGroup("Dialogue")] public InputField dawnDollarReward;
        [BoxGroup("Dialogue")] public InputField goldReward;
        [BoxGroup("Dialogue")] public D90DatabaseDropdown itemDatabaseDropdown;
        [BoxGroup("Dialogue")] public LayoutItem itemContentPrefab;
        [BoxGroup("Dialogue")] public D90ContentLayout itemLayout;

        [BoxGroup("Dialogue")] public Button TxtThoughtsButton;
        [BoxGroup("Dialogue")] public Button TxtPersonButton;
        [BoxGroup("Dialogue")] public Button TxtPlaceButton;
        [BoxGroup("Dialogue")] public Button TxtItemButton;
        [BoxGroup("Dialogue")] public Button TxtShakeButton;
        [BoxGroup("Dialogue")] public Button TxtWiggleButton;
        [BoxGroup("Dialogue")] public Button TxtWaveButton;
        [BoxGroup("Dialogue")] public Button TxtSizeButton;
        [BoxGroup("Dialogue")] public Button TxtScreenShakeLyButton;
        [BoxGroup("Dialogue")] public Button TxtScreenShakeHButton;
        [BoxGroup("Dialogue")] public Button TxtCharacterShakeLButton;
        [BoxGroup("Dialogue")] public Button TxtCharacterShakeHButton;
        [BoxGroup("Dialogue")] public Button TxtBounceButton;
        [BoxGroup("Dialogue")] public Button TxtScreenFlashButton;
        [BoxGroup("Dialogue")] public Button TxtCharacterFadeButton;
        [BoxGroup("Dialogue")] public Button SFXDeterminedButton;
        [BoxGroup("Dialogue")] public Button SFXDingButton;
        [BoxGroup("Dialogue")] public Button SFXQuestionButton;
        [BoxGroup("Dialogue")] public Button SFXSmack1Button;
        [BoxGroup("Dialogue")] public Button SFXSmack2Button;
        [BoxGroup("Dialogue")] public Button SFXSmack3Button;
        [BoxGroup("Dialogue")] public Button MusicPlayButton;
        [BoxGroup("Dialogue")] public Button MusicFadeButton;
        [BoxGroup("Dialogue")] public Button AmbiencePlayButton;
        [BoxGroup("Dialogue")] public Button AmbienceStopButton;

        [BoxGroup("Dialogue/Emotes")] public D90ContentLayout emoteList;
        [BoxGroup("Dialogue/Emotes")] public Dropdown emoteDropdown;
        [BoxGroup("Dialogue/Emotes")] public Button emoteSetButton;
        [BoxGroup("Dialogue/Emotes")] public Button emoteSetTalkingButton;
        [BoxGroup("Dialogue/Emotes")] public Button emoteSetAngryButton;
        [BoxGroup("Dialogue/Emotes")] public Button emoteSetBoredButton;
        [BoxGroup("Dialogue/Emotes")] public Button emoteSetConfusedButton;
        [BoxGroup("Dialogue/Emotes")] public Button emoteSetDeterminedButton;
        [BoxGroup("Dialogue/Emotes")] public Button emoteSetEmbarassedButton;
        [BoxGroup("Dialogue/Emotes")] public Button emoteSetFlirtButton;
        [BoxGroup("Dialogue/Emotes")] public Button emoteSetJoyButton;
        [BoxGroup("Dialogue/Emotes")] public Button emoteSetNervousButton;
        [BoxGroup("Dialogue/Emotes")] public Button emoteSetSadButton;
        [BoxGroup("Dialogue/Emotes")] public Button emoteSetShockedButton;

        [BoxGroup("Dialogue/Quests")] public D90DatabaseDropdown questDropdown;
        [BoxGroup("Dialogue/Quests")] public InputField questStepChange;
        [BoxGroup("Dialogue/Quests")] public Toggle incrementQuestInstead;

        [BoxGroup("Dialogue/Play Modes")] public SceneLoadInputMode eventSceneInputMode;
        [BoxGroup("Dialogue/Play Modes")] public SceneLoadInputMode cutsceneInputMode;

        [BoxGroup("Character")] public D90ContentLayout rightCharacters;
        [BoxGroup("Character")] public D90DatabaseDropdown rightCharacterDropdown;
        [BoxGroup("Character")] public InputField rightOverrideName;
        [BoxGroup("Character")] public Button rightOverrideNameSetButton;
        [BoxGroup("Character")] public Button rightOverrideNameRemoveButton;
        [BoxGroup("Character")] public Button rightCopyButton;
        [BoxGroup("Character")] public Button rightPasteButton;

        [BoxGroup("Character")] public D90ContentLayout leftCharacters;
        [BoxGroup("Character")] public D90DatabaseDropdown leftCharacterDropdown;
        [BoxGroup("Character")] public InputField leftOverrideName;
        [BoxGroup("Character")] public Button leftOverrideNameSetButton;
        [BoxGroup("Character")] public Button leftOverrideNameRemoveButton;
        [BoxGroup("Character")] public Button leftCopyButton;
        [BoxGroup("Character")] public Button leftPasteButton;

        [BoxGroup("Folders")] public Button newFolderButton;
        [BoxGroup("Folders")] public Button folderMoveUpButton;
        [BoxGroup("Folders")] public Button folderMoveDownButton;
        [BoxGroup("Folders")] public Button backButton;
        [BoxGroup("Folders")] public Dropdown addToFolderDropdown;
        [BoxGroup("Folders")] public GameObject folderInspector;
        [BoxGroup("Folders")] public InputField folderNameInput;
        [BoxGroup("Folders")] public Button folderNameSaveButton;
        [BoxGroup("Folders")] public Button copyCutsceneButton;
        [BoxGroup("Folders")] public Button pasteCutsceneButton;

        [BoxGroup("Audio")] public D90DatabaseDropdown startingMusicDropdown;
        [BoxGroup("Audio")] public D90DatabaseDropdown startingAmbientDropdown;

        private CutsceneData m_currentCutscene;
        private CutsceneDialogueInfo m_currentDialogue;
        private string _currentItemUid;
        private SceneLoadInputMode _currentInputMode;
        private string _currentFolder;
        private string _selectedFolder;

        private string m_rightSideCharacter;
        private string m_leftSideCharacter;
        private string m_emoteCharacter;

        private List<String> copyCharacters = new List<String>();
        private List <String> copyCharacterOverrideNames = new List<String>();
        private CutsceneData copyCutscene = new CutsceneData();

        private void OnCutsceneNameChanged(string newName)
        {
            m_currentCutscene.EventTitle = newName;
            ResetCutsceneList();
            Save();
        }

        private void OnCutsceneTypeChanged(int value)
        {
            m_currentCutscene.CutsceneType = (CutsceneType) value;
            ChangeLocationOptions();
            Save();
        }

        private void OnCutsceneLocationChanged(int value)
        {
            switch (m_currentCutscene.CutsceneType)
            {
                case CutsceneType.HexMapDialogue:
                    break;
                case CutsceneType.EventScene:
                    m_currentCutscene.EventStageType = cutsceneLocation.options[value].text;
                    break;
                case CutsceneType.Cutscene:
                    m_currentCutscene.EventLocation = cutsceneLocation.options[value].text;
                    break;
            }
        }

        private void OnNewCutscene()
        {
            m_currentCutscene = Db.CutsceneDatabase.CreateNew(_currentFolder);
            m_currentCutscene.DialogueInfo.Add(new CutsceneDialogueInfo
            {
                CharacterSide = CharacterSide.Right,
                Text = "",
                RightSideEmotes = new List<string>(),
                LeftSideEmotes = new List<string>(),
                Choices = new List<CutsceneChoice> {new CutsceneChoice{ Choice = "choice 1" }, new CutsceneChoice {Choice = "choice 2"} },
            });

            m_currentDialogue = m_currentCutscene.DialogueInfo[0];
            m_currentDialogue.UpdateIdentifier();

            SetupCutsceneWindow(m_currentCutscene.Uid);
            Save();
        }
        

        private void OnCopyCutscene()
        {
            if (m_currentCutscene != null)
            {
                copyCutscene = m_currentCutscene;
            }
        }

        private void OnPasteCutscene()
        {

            if (copyCutscene != null)
            {
                m_currentCutscene = Db.CutsceneDatabase.CreateNew(_currentFolder);
                

                m_currentCutscene.CutsceneType = copyCutscene.CutsceneType;
                m_currentCutscene.DialogueInfo = new List<CutsceneDialogueInfo>(copyCutscene.DialogueInfo);
                m_currentCutscene.EventLocation = copyCutscene.EventLocation;
                m_currentCutscene.EventStageType = copyCutscene.EventStageType;
                m_currentCutscene.EventTitle = copyCutscene.EventTitle;
                m_currentCutscene.IncrementQuestStepInstead = copyCutscene.IncrementQuestStepInstead;
                m_currentCutscene.QuestStepChange = copyCutscene.QuestStepChange;
                m_currentCutscene.QuestUid = copyCutscene.QuestUid;
                m_currentCutscene.RewardItems = copyCutscene.RewardItems;
                m_currentCutscene.RightCharacterOverrideNames = new List<String>(copyCutscene.RightCharacterOverrideNames);
                m_currentCutscene.RightSideCharacters = new List<String>(copyCutscene.RightSideCharacters);
                m_currentCutscene.LeftCharacterOverrideNames = new List<String>(copyCutscene.LeftCharacterOverrideNames);
                m_currentCutscene.LeftSideCharacters = new List<String>(copyCutscene.LeftSideCharacters);
                m_currentCutscene.StartingAmbient = copyCutscene.StartingAmbient;
                m_currentCutscene.StartingTrack = copyCutscene.StartingTrack;

                m_currentDialogue.UpdateIdentifier();

                SetupCutsceneWindow(m_currentCutscene.Uid);
                Save();
            }
        }

        private void OnDeleteCutscene()
        {

                D90Tools.DisplayConfirmation("Are you sure?", OnDeleteCutsceneConfirmation);

        }

        private void OnDeleteCutsceneConfirmation(bool result)
        {
            if (result)
            {
                var folders =  Db.CutsceneDatabase.GetFoldersInFolder(_selectedFolder);
                var cutscenes = Db.CutsceneDatabase.GetCutscenesInFolder(_selectedFolder);

                // Delete Folder
                if (folders.Count == 0  && cutscenes.Count == 0)
                {
                    if (folderInspector.activeInHierarchy)
                    {
                       // Debug.Log("DELETE THE FOLDER NOW");
                        Db.CutsceneDatabase.DeleteFolder(_selectedFolder);
                        _selectedFolder = string.Empty;
                        ResetCutsceneList();
                    }

                }

                // Delete Cutscene
                else
            {
                    if (m_currentCutscene != null && !folderInspector.activeInHierarchy)
                    {
                      //  Debug.Log("DELETE THE CUTSCENE NOW");
                        Db.CutsceneDatabase.Delete(m_currentCutscene.Uid);
                        SetupCutsceneWindow(null);
                    }
                }
            }

        }


        private void OnAddDialogue()
        {
            if (m_currentDialogue != null)
            {
                var currentIndex = m_currentCutscene.DialogueInfo.IndexOf(m_currentDialogue);
                var lastDialogue = m_currentDialogue;

                m_currentDialogue = new CutsceneDialogueInfo
                {
                    CharacterSide = CharacterSide.Right,
                    Text = string.Empty,
                    RightSideEmotes = new List<string> ( lastDialogue.RightSideEmotes ),
                    LeftSideEmotes = new List<string> ( lastDialogue.LeftSideEmotes ),
                    Choices = new List<CutsceneChoice> { new CutsceneChoice{ Choice = "choice 1" }, new CutsceneChoice {Choice = "choice 2"}  },
                };

                m_currentDialogue.UpdateIdentifier();
                m_currentCutscene.DialogueInfo.Insert(currentIndex + 1, m_currentDialogue);

                OnSaveDialogue();
            }
        }

        private void OnDeleteDialogue()
        {
            if (m_currentDialogue != null)
            {
                D90Tools.DisplayConfirmation("Are you sure?", OnDeleteDialogueConfirmation);
            }
        }

        private void OnDeleteDialogueConfirmation(bool result)
        {
            if (result)
            {
                var currentIndex = m_currentCutscene.DialogueInfo.IndexOf(m_currentDialogue);

                m_currentCutscene.DialogueInfo.Remove(m_currentDialogue);

                for (var i = 0; i < m_currentDialogue.Choices.Count; i++)
                {
                    Db.CutsceneDatabase.Delete(m_currentDialogue.Choices[i].CutsceneUid, false);
                }

                if (m_currentCutscene.DialogueInfo.Count > currentIndex)
                {
                    m_currentDialogue = m_currentCutscene.DialogueInfo[currentIndex];
                }
                else if (m_currentCutscene.DialogueInfo.Count > 0)
                {
                    m_currentDialogue = m_currentCutscene.DialogueInfo[0];
                }
                else
                {
                    m_currentDialogue = null;
                }

                ResetDialogueList(m_currentDialogue == null ? string.Empty : m_currentDialogue.Uid);
                Save();
            }

        }

        private void OnMoveDialogueUp()
        {
            if (m_currentDialogue != null)
            {
                m_currentCutscene.DialogueInfo.MoveDown(m_currentDialogue);
                ResetDialogueList(m_currentDialogue.Uid);
                Save();
            }
        }

        private void OnMoveDialogueDown()
        {
            if (m_currentDialogue != null)
            {
                m_currentCutscene.DialogueInfo.MoveUp(m_currentDialogue);
                ResetDialogueList(m_currentDialogue.Uid);
                Save();
            }
        }

        private void OnMoveFolderUp()
        {
           // Debug.Log("MOVE FOLDER OBJ UP");
            if (m_currentCutscene != null && !folderInspector.activeSelf && !m_currentDialogue.HasChoices)
            {
                Db.CutsceneDatabase.MoveDown(m_currentCutscene.Uid);
                SetupCutsceneWindow(m_currentCutscene.Uid);
            }
        }

        private void OnMoveFolderDown()
        {
          //  Debug.Log("MOVE FOLDER OBJ DOWN");
            if (m_currentCutscene != null && !folderInspector.activeSelf && !m_currentDialogue.HasChoices)
            {
                Db.CutsceneDatabase.MoveUp(m_currentCutscene.Uid);
                SetupCutsceneWindow(m_currentCutscene.Uid);
            }
        }

        private void OnCutsceneChanged(bool isOn, string cutsceneUid)
        {
            if (isOn && (m_currentCutscene == null  || m_currentCutscene.Uid != cutsceneUid))
            {
                SetupCutsceneWindow(cutsceneUid);

                // var folders = Db.CutsceneDatabase.GetFoldersInFolder(_selectedFolder);
                // var cutscenes = Db.CutsceneDatabase.GetCutscenesInFolder(_selectedFolder);
                // Debug.Log("CUTSCENE SELECTED");
                // Debug.Log("FOLDERS: " + folders.Count);
                // Debug.Log("CUTSCENES: " + cutscenes.Count);

            }
        }

        private void OnFolderChanged(LayoutItem layoutItem)
        {
            if (_currentFolder !=  layoutItem.mainButton.content)
            {
                _currentFolder = layoutItem.mainButton.content;
                SetupCutsceneWindow(null);
            }
        }

        private void OnFolderSelected(bool isOn, string folderName)
        {
            if (isOn)
            {
                _selectedFolder = folderName;
                folderInspector.SetActive(true);
                folderNameInput.SetTextWithoutNotify(folderName);

                // var folders = Db.CutsceneDatabase.GetFoldersInFolder(_selectedFolder);
                // var cutscenes = Db.CutsceneDatabase.GetCutscenesInFolder(_selectedFolder);
                // Debug.Log("FOLDER SELECTED");
                // Debug.Log("FOLDERS: " + folders.Count);
                // Debug.Log("CUTSCENES: " + cutscenes.Count);

            }
        }

        private void OnDialogueEdit(bool isOn, string dialogueUid)
        {
            if (isOn && m_currentDialogue.Uid != dialogueUid)
            {
                ResetDialogueList(dialogueUid);
            }
        }

        private void OnSpeakingCharacterChanged(int value)
        {
            if (speakingCharacterDropdown.options.Count <= value)
            {
                return;
            }

            var characterName = speakingCharacterDropdown.options[value].text;
            var character = Db.CharacterDatabase.GetCharacterByName(characterName);

            if (character == null) return;

            if (value >= m_currentCutscene.LeftSideCharacters.Count)
            {
                var rightIndex = m_currentCutscene.RightSideCharacters.IndexOf(character.id);
                if (rightIndex < 0) return;
                m_currentDialogue.CharacterSide = CharacterSide.Right;
                m_currentDialogue.CharacterIndex = rightIndex;
            }
            else
            {
                var leftIndex = m_currentCutscene.LeftSideCharacters.IndexOf(character.id);
                if (leftIndex < 0) return;
                m_currentDialogue.CharacterSide = CharacterSide.Left;
                m_currentDialogue.CharacterIndex = leftIndex;
            }
        }

        private void OnSpeakingCharacterDialogueChanged(string newDialogue)
        {
            m_currentDialogue.Text = newDialogue;
        }

        private void OnHasChoicesChanged(bool isOn)
        {
            m_currentDialogue.HasChoices = isOn;
        }

        private void OnUsesThoughtBubbleChanged(bool isOn)
        {
            m_currentDialogue.UsesThoughtBubble = isOn;
        }

        private void OnChoice1Changed(string choiceText)
        {
            m_currentDialogue.Choices[0].Choice = choiceText;
        }

        private void OnChoice2Changed(string choiceText)
        {
            m_currentDialogue.Choices[1].Choice = choiceText;
        }

        private void OnCutsceneChoice1Changed(D90DatabaseDropdown dropdown, int value)
        {
            var eventTitle = dropdown.dropdown.options[value].text;
            var cutscene = Db.CutsceneDatabase.GetCutsceneByEventTitle(eventTitle);
            m_currentDialogue.Choices[0].CutsceneUid = cutscene == null ? string.Empty : cutscene.Uid;
        }

        private void OnCutsceneChoice2Changed(D90DatabaseDropdown dropdown, int value)
        {
            var eventTitle = dropdown.dropdown.options[value].text;
            var cutscene = Db.CutsceneDatabase.GetCutsceneByEventTitle(eventTitle);
            m_currentDialogue.Choices[1].CutsceneUid = cutscene == null ? string.Empty : cutscene.Uid;
        }

        private void OnNewChoice1CutsceneCreated()
        {
            if (m_currentDialogue != null)
            {
                var newCutscene = Db.CutsceneDatabase.CreateNew(_currentFolder);
                newCutscene.EventTitle = $"({choice1.text}) - Answer";
                newCutscene.RightSideCharacters = new List<string>(m_currentCutscene.RightSideCharacters);
                newCutscene.LeftSideCharacters = new List<string>(m_currentCutscene.LeftSideCharacters);
                newCutscene.RightCharacterOverrideNames = new List<string>(m_currentCutscene.RightCharacterOverrideNames);
                newCutscene.LeftCharacterOverrideNames = new List<string>(m_currentCutscene.LeftCharacterOverrideNames);
                newCutscene.CutsceneType = m_currentCutscene.CutsceneType;
                newCutscene.EventLocation = m_currentCutscene.EventLocation;

                if (string.IsNullOrEmpty(choice1.text))
                {
                    choice1.SetTextWithoutNotify("choice 1");
                }

                if (string.IsNullOrEmpty(choice2.text))
                {
                    choice2.SetTextWithoutNotify("choice 2");
                }

                m_currentDialogue.HasChoices = true;
                m_currentDialogue.Choices[0].Choice = choice1.text;
                m_currentDialogue.Choices[1].Choice = choice2.text;
                m_currentDialogue.Choices[0].CutsceneUid = newCutscene.Uid;

                m_currentCutscene = newCutscene;
                m_currentCutscene.DialogueInfo.Add(new CutsceneDialogueInfo
                {
                    CharacterSide = CharacterSide.Right,
                    Text = "",
                    RightSideEmotes = new List<string>(),
                    LeftSideEmotes = new List<string>(),
                    Choices = new List<CutsceneChoice> { new CutsceneChoice{ Choice = "choice 1" }, new CutsceneChoice {Choice = "choice 2"} },
                });

                m_currentDialogue = m_currentCutscene.DialogueInfo[0];
                m_currentDialogue.UpdateIdentifier();

                SetupCutsceneWindow(m_currentCutscene.Uid);
                Save();
            }
        }

        private void OnNewChoice2CutsceneCreated()
        {
            if (m_currentDialogue != null)
            {
                var newCutscene = Db.CutsceneDatabase.CreateNew(_currentFolder);
                newCutscene.EventTitle = $"({choice2.text}) - Answer";
                newCutscene.RightSideCharacters = new List<string>(m_currentCutscene.RightSideCharacters);
                newCutscene.LeftSideCharacters = new List<string>(m_currentCutscene.LeftSideCharacters);
                newCutscene.RightCharacterOverrideNames = new List<string>(m_currentCutscene.RightCharacterOverrideNames);
                newCutscene.LeftCharacterOverrideNames = new List<string>(m_currentCutscene.LeftCharacterOverrideNames);
                newCutscene.CutsceneType = m_currentCutscene.CutsceneType;
                newCutscene.EventLocation = m_currentCutscene.EventLocation;

                if (string.IsNullOrEmpty(choice1.text))
                {
                    choice1.SetTextWithoutNotify("choice 1");
                }

                if (string.IsNullOrEmpty(choice2.text))
                {
                    choice2.SetTextWithoutNotify("choice 2");
                }

                m_currentDialogue.HasChoices = true;
                m_currentDialogue.Choices[0].Choice = choice1.text;
                m_currentDialogue.Choices[1].Choice = choice2.text;
                m_currentDialogue.Choices[1].CutsceneUid = newCutscene.Uid;

                m_currentCutscene = newCutscene;
                m_currentCutscene.DialogueInfo.Add(new CutsceneDialogueInfo
                {
                    CharacterSide = CharacterSide.Right,
                    Text = "",
                    RightSideEmotes = new List<string>(),
                    LeftSideEmotes = new List<string>(),
                    Choices = new List<CutsceneChoice> { new CutsceneChoice{ Choice = "choice 1" }, new CutsceneChoice {Choice = "choice 2"} },
                });

                m_currentDialogue = m_currentCutscene.DialogueInfo[0];
                m_currentDialogue.UpdateIdentifier();


                SetupCutsceneWindow(m_currentCutscene.Uid);
                Save();
            }
        }

        // private void OnDawnDollarRewardChanged(string dd)
        // {
        //     if (string.IsNullOrEmpty(dd)) return;
        //
        //     if (int.TryParse(dd, out var value))
        //     {
        //         m_currentDialogue.DawnDollarReward = value;
        //     }
        // }
        //
        // private void OnGoldRewardChanged(string gd)
        // {
        //     if (string.IsNullOrEmpty(gd)) return;
        //
        //     if (int.TryParse(gd, out var value))
        //     {
        //         m_currentDialogue.GoldReward = value;
        //     }
        // }

        private void OnSaveDialogue()
        {
            if (m_currentDialogue != null)
            {
                OnSpeakingCharacterChanged(speakingCharacterDropdown.value);
                OnSpeakingCharacterDialogueChanged(speakingCharacterDialogue.text);
                OnHasChoicesChanged(hasChoices.isOn);
                OnUsesThoughtBubbleChanged(usesThoughtBubble.isOn);
                OnChoice1Changed(choice1.text);
                OnChoice2Changed(choice2.text);
                OnCutsceneChoice1Changed(cutsceneChoice1, cutsceneChoice1.dropdown.value);
                OnCutsceneChoice2Changed(cutsceneChoice2, cutsceneChoice2.dropdown.value);
                // OnDawnDollarRewardChanged(dawnDollarReward.text);
                // OnGoldRewardChanged(goldReward.text);
                ResetDialogueList(m_currentDialogue.Uid);
                Save();
            }
        }

        private void OnItemChanged(bool isOn, string content)
        {
            if (isOn)
            {
                _currentItemUid = content;
            }
        }

        private void OnItemAdded(D90DatabaseDropdown dropdown)
        {
            var item = Db.ItemDatabase.GetItemByName(dropdown.dropdown.options[dropdown.dropdown.value].text);

            if (item != null)
            {
                AddNewItem(item.ItemId);
            }
        }

        private void OnItemDeleted(LayoutItem item)
        {
            RemoveItem(item.mainButton.content);
        }

        public void Open(OpenCutsceneEditorEventArgs args)
        {
            SetupCutsceneWindow(string.IsNullOrEmpty(args.cutsceneUid)
                ? Db.CutsceneDatabase.Cutscenes[0].Uid
                : args.cutsceneUid);
        }

        private void SetupCutsceneWindow(string cutsceneUid)
        {
            if (string.IsNullOrEmpty(cutsceneUid))
            {
                var cutscenesInFolder = Db.CutsceneDatabase.GetCutscenesInFolder(_currentFolder);

                if (cutscenesInFolder.Count <= 0)
                {
                    m_currentCutscene = Db.CutsceneDatabase.Cutscenes[0];
                }
                else
                {
                    m_currentCutscene = cutscenesInFolder[0];
                }
            }
            else
            {
                m_currentCutscene = Db.CutsceneDatabase.GetCutscene(cutsceneUid);
            }

            ResetCutsceneList();

            if (m_currentCutscene!= null) cutsceneName.SetTextWithoutNotify(m_currentCutscene.EventTitle);
            cutsceneName.onEndEdit.RemoveAllListeners();
            cutsceneName.onEndEdit.AddListener(OnCutsceneNameChanged);

            cutsceneType.ClearOptions();
            cutsceneType.AddOptions(Enum.GetNames(typeof(CutsceneType)).ToList());
            if (m_currentCutscene!= null) cutsceneType.SetValueWithoutNotify((int) m_currentCutscene.CutsceneType);
            cutsceneType.onValueChanged.RemoveAllListeners();
            cutsceneType.onValueChanged.AddListener(OnCutsceneTypeChanged);

            addDialogueButton.onClick.RemoveAllListeners();
            addDialogueButton.onClick.AddListener(OnAddDialogue);

            saveCurrentDialogue.onClick.RemoveAllListeners();
            saveCurrentDialogue.onClick.AddListener(OnSaveDialogue);

            deleteCurrentDialogue.onClick.RemoveAllListeners();
            deleteCurrentDialogue.onClick.AddListener(OnDeleteDialogue);

            moveUpButton.onClick.RemoveAllListeners();
            moveUpButton.onClick.AddListener(OnMoveDialogueUp);

            moveDownButton.onClick.RemoveAllListeners();
            moveDownButton.onClick.AddListener(OnMoveDialogueDown);

            newCutsceneButton.onClick.RemoveAllListeners();
            newCutsceneButton.onClick.AddListener(OnNewCutscene);

            deleteCutsceneButton.onClick.RemoveAllListeners();
            deleteCutsceneButton.onClick.AddListener(OnDeleteCutscene);

            copyCutsceneButton.onClick.RemoveAllListeners();
            copyCutsceneButton.onClick.AddListener(OnCopyCutscene);

            pasteCutsceneButton.onClick.RemoveAllListeners();
            pasteCutsceneButton.onClick.AddListener(OnPasteCutscene);

            playButton.onClick.RemoveAllListeners();
            playButton.onClick.AddListener(() => OnPlayButton(false));

            playIndexButton.onClick.RemoveAllListeners();
            playIndexButton.onClick.AddListener(() => OnPlayButton(true));

            cutsceneLocation.onValueChanged.RemoveAllListeners();
            cutsceneLocation.onValueChanged.AddListener(OnCutsceneLocationChanged);

            backButton.onClick.RemoveAllListeners();
            backButton.onClick.AddListener(OnBackButton);

            newFolderButton.onClick.RemoveAllListeners();
            newFolderButton.onClick.AddListener(OnNewFolder);

            folderMoveUpButton.onClick.RemoveAllListeners();
           folderMoveUpButton.onClick.AddListener(OnMoveFolderUp);

            folderMoveDownButton.onClick.RemoveAllListeners();
            folderMoveDownButton.onClick.AddListener(OnMoveFolderDown);

            addToFolderDropdown.ClearOptions();
            var folders = new List<string> {string.Empty};
            folders.AddRange(Db.CutsceneDatabase.Folders);
            addToFolderDropdown.AddOptions(folders);
            if (m_currentCutscene!=null) addToFolderDropdown.SetValueWithoutNotify(string.IsNullOrEmpty(m_currentCutscene.Folder) ? 0 : folders.IndexOf(m_currentCutscene.Folder));
            addToFolderDropdown.onValueChanged.RemoveAllListeners();
            addToFolderDropdown.onValueChanged.AddListener(OnCutsceneAddedToFolder);

            folderInspector.SetActive(false);
            folderNameInput.SetTextWithoutNotify(_currentFolder);
            folderNameSaveButton.onClick.RemoveAllListeners();
            folderNameSaveButton.onClick.AddListener(OnSaveFolderName);

            startingMusicDropdown.Init();
            startingMusicDropdown.OnValueChanged -= OnStartingMusicChanged;
            startingMusicDropdown.OnValueChanged += OnStartingMusicChanged;

            startingAmbientDropdown.Init();
            startingAmbientDropdown.OnValueChanged -= OnStartingAmbientChanged;
            startingAmbientDropdown.OnValueChanged += OnStartingAmbientChanged;

            if (m_currentCutscene != null)
            {
                startingMusicDropdown.SetDropdownWithoutNotify(m_currentCutscene.StartingTrack);
                startingAmbientDropdown.SetDropdownWithoutNotify(m_currentCutscene.StartingAmbient);
            }

            if (m_currentCutscene != null)
            {
                for (var i = 0; i < m_currentCutscene.DialogueInfo.Count; i++)
                {
                    for (var j = 0; j < m_currentCutscene.DialogueInfo[i].Choices.Count; j++)
                    {
                        var choiceDialogue =
                            Db.CutsceneDatabase.GetCutscene(m_currentCutscene.DialogueInfo[i].Choices[j].CutsceneUid);

                        if (choiceDialogue != null)
                        {
                            for (var k = 0; k < m_currentCutscene.RightSideCharacters.Count; k++)
                            {
                                if (choiceDialogue.RightSideCharacters.Count > k)
                                {
                                    choiceDialogue.RightSideCharacters[k] = m_currentCutscene.RightSideCharacters[k];
                                }
                                else
                                {
                                    choiceDialogue.RightSideCharacters.Add(m_currentCutscene.RightSideCharacters[k]);
                                }
                            }

                            for (var k = 0; k < m_currentCutscene.LeftSideCharacters.Count; k++)
                            {
                                if (choiceDialogue.LeftSideCharacters.Count > k)
                                {
                                    choiceDialogue.LeftSideCharacters[k] = m_currentCutscene.LeftSideCharacters[k];
                                }
                                else
                                {
                                    choiceDialogue.LeftSideCharacters.Add(m_currentCutscene.LeftSideCharacters[k]);
                                }
                            }
                        }
                    }
                }
            }

            ChangeLocationOptions();

            ResetDialogueList(string.Empty);
        }

        private void SetupCharacterInspector()
        {
            rightCharacters.Clear();
            leftCharacters.Clear();

            for (var i = 0; i < m_currentCutscene.RightSideCharacters.Count; i++)
            {
                var characterId = m_currentCutscene.RightSideCharacters[i];
                if (i >= m_currentCutscene.RightCharacterOverrideNames.Count)
                {
                    m_currentCutscene.RightCharacterOverrideNames.Add(string.Empty);
                }
                var characterOverrideName = m_currentCutscene.RightCharacterOverrideNames[i];
                var character = Db.CharacterDatabase.GetCharacter(characterId);
                if (character == null) continue;
                var factionColor = $"<color={ColorPalette.CrossBlitz.GetFactionColor(character.faction)}>";
                var characterContent = Instantiate(cutsceneLayoutPrefab, rightCharacters.contentLayout, false);

                characterContent.mainButton.content = characterId;
                characterContent.textLabels[0].text = string.IsNullOrEmpty(characterOverrideName) ?
                    $"{factionColor}{character.name}</color>" :
                    $"{factionColor}{character.name} ({characterOverrideName})</color>";
                characterContent.mainButton.OnValueChanged -= OnRightSideCharacterSelected;
                characterContent.mainButton.OnValueChanged += OnRightSideCharacterSelected;
                characterContent.mainButton.Toggle.isOn = m_rightSideCharacter == characterId;
                rightCharacters.AddItem(characterContent);
            }

            for (var i = 0; i < m_currentCutscene.LeftSideCharacters.Count; i++)
            {
                var characterId = m_currentCutscene.LeftSideCharacters[i];
                if (i >= m_currentCutscene.LeftCharacterOverrideNames.Count)
                {
                    m_currentCutscene.LeftCharacterOverrideNames.Add(string.Empty);
                }
                var characterOverrideName = m_currentCutscene.LeftCharacterOverrideNames[i];
                var character = Db.CharacterDatabase.GetCharacter(characterId);
                if (character == null) continue;
                var factionColor = $"<color={ColorPalette.CrossBlitz.GetFactionColor(character.faction)}>";
                var characterContent = Instantiate(cutsceneLayoutPrefab, leftCharacters.contentLayout, false);

                characterContent.mainButton.content = characterId;
                characterContent.textLabels[0].text = string.IsNullOrEmpty(characterOverrideName) ?
                    $"{factionColor}{character.name}</color>" :
                    $"{factionColor}{character.name} ({characterOverrideName})</color>";
                characterContent.mainButton.OnValueChanged -= OnLeftSideCharacterSelected;
                characterContent.mainButton.OnValueChanged += OnLeftSideCharacterSelected;
                characterContent.mainButton.Toggle.isOn = m_leftSideCharacter == characterId;
                leftCharacters.AddItem(characterContent);
            }

            if (!string.IsNullOrEmpty(m_rightSideCharacter))
            {
                var characterIndex = m_currentCutscene.RightSideCharacters.IndexOf(m_rightSideCharacter);
                if (characterIndex >= 0)
                {
                    rightOverrideName.SetTextWithoutNotify(m_currentCutscene.RightCharacterOverrideNames[characterIndex]);
                }
                else
                {
                    rightOverrideName.SetTextWithoutNotify(string.Empty);
                }
            }
            else
            {
                rightOverrideName.SetTextWithoutNotify(string.Empty);
            }

            if (!string.IsNullOrEmpty(m_leftSideCharacter))
            {
                var characterIndex = m_currentCutscene.LeftSideCharacters.IndexOf(m_leftSideCharacter);
                if (characterIndex >= 0)
                {
                    leftOverrideName.SetTextWithoutNotify(m_currentCutscene.LeftCharacterOverrideNames[characterIndex]);
                }
                else
                {
                    leftOverrideName.SetTextWithoutNotify(string.Empty);
                }
            }
            else
            {
                leftOverrideName.SetTextWithoutNotify(string.Empty);
            }

            rightOverrideNameSetButton.onClick.RemoveAllListeners();
            rightOverrideNameSetButton.onClick.AddListener(OnSetRightOverrideName);

            leftOverrideNameSetButton.onClick.RemoveAllListeners();
            leftOverrideNameSetButton.onClick.AddListener(OnSetLeftOverrideName);

            rightOverrideNameRemoveButton.onClick.RemoveAllListeners();
            rightOverrideNameRemoveButton.onClick.AddListener(OnRemoveRightOverrideName);

            leftOverrideNameRemoveButton.onClick.RemoveAllListeners();
            leftOverrideNameRemoveButton.onClick.AddListener(OnRemoveLeftOverrideName);

            rightCopyButton.onClick.RemoveAllListeners();
            rightCopyButton.onClick.AddListener(OnCopyRightCharacters);

            leftCopyButton.onClick.RemoveAllListeners();
            leftCopyButton.onClick.AddListener(OnCopyLeftCharacters);

            rightPasteButton.onClick.RemoveAllListeners();
            rightPasteButton.onClick.AddListener(OnPasteRightCharacters);

            leftPasteButton.onClick.RemoveAllListeners();
            leftPasteButton.onClick.AddListener(OnPasteLeftCharacters);

            leftCharacters.OnDelete -= OnDeleteLeftCharacter;
            rightCharacters.OnDelete -= OnDeleteRightCharacter;

            leftCharacters.OnDelete += OnDeleteLeftCharacter;
            rightCharacters.OnDelete += OnDeleteRightCharacter;

            leftCharacterDropdown.OnAddButton -= OnAddLeftCharacter;
            rightCharacterDropdown.OnAddButton -= OnAddRightCharacter;

            leftCharacterDropdown.OnAddButton += OnAddLeftCharacter;
            rightCharacterDropdown.OnAddButton += OnAddRightCharacter;
        }

        private void OnSetRightOverrideName()
        {
            if (!string.IsNullOrEmpty(m_rightSideCharacter))
            {
                var characterIndex = m_currentCutscene.RightSideCharacters.IndexOf(m_rightSideCharacter);

                if (characterIndex >= 0)
                {
                    m_currentCutscene.RightCharacterOverrideNames[characterIndex] = rightOverrideName.text;
                    SetupCharacterInspector();
                    Save();
                }
            }
        }

        private void OnSetLeftOverrideName()
        {
            if (!string.IsNullOrEmpty(m_leftSideCharacter))
            {
                var characterIndex = m_currentCutscene.LeftSideCharacters.IndexOf(m_leftSideCharacter);

                if (characterIndex >= 0)
                {
                    m_currentCutscene.LeftCharacterOverrideNames[characterIndex] = leftOverrideName.text;
                    ResetDialogueList(m_currentDialogue?.Uid);
                    Save();
                }
            }
        }

        private void OnRemoveRightOverrideName()
        {
            if (!string.IsNullOrEmpty(m_rightSideCharacter))
            {
                var characterIndex = m_currentCutscene.RightSideCharacters.IndexOf(m_rightSideCharacter);

                if (characterIndex >= 0)
                {
                    m_currentCutscene.RightCharacterOverrideNames[characterIndex] = string.Empty;
                    ResetDialogueList(m_currentDialogue?.Uid);
                    Save();
                }
            }
        }

        private void OnRemoveLeftOverrideName()
        {
            if (!string.IsNullOrEmpty(m_leftSideCharacter))
            {
                var characterIndex = m_currentCutscene.LeftSideCharacters.IndexOf(m_leftSideCharacter);

                if (characterIndex >= 0)
                {
                    m_currentCutscene.LeftCharacterOverrideNames[characterIndex] = string.Empty;
                    ResetDialogueList(m_currentDialogue?.Uid);
                    Save();
                }
            }
        }

        private void OnDeleteLeftCharacter(LayoutItem item)
        {
            var leftCharacter = item.mainButton.content;

            if (!string.IsNullOrEmpty(leftCharacter))
            {
                var characterIndex = m_currentCutscene.LeftSideCharacters.IndexOf(leftCharacter);

                if (characterIndex >= 0)
                {
                    m_currentCutscene.LeftSideCharacters.RemoveAt(characterIndex);
                    m_currentCutscene.LeftCharacterOverrideNames.RemoveAt(characterIndex);
                    ResetDialogueList(m_currentDialogue?.Uid);
                    Save();
                }
            }
        }

        private void OnDeleteRightCharacter(LayoutItem item)
        {
            var rightCharacter = item.mainButton.content;

            if (!string.IsNullOrEmpty(rightCharacter))
            {
                var characterIndex = m_currentCutscene.RightSideCharacters.IndexOf(rightCharacter);

                if (characterIndex >= 0)
                {
                    m_currentCutscene.RightSideCharacters.RemoveAt(characterIndex);
                    m_currentCutscene.RightCharacterOverrideNames.RemoveAt(characterIndex);
                    ResetDialogueList(m_currentDialogue?.Uid);
                    Save();
                }
            }
        }

        private void OnAddLeftCharacter(D90DatabaseDropdown dropdown)
        {
            var characterName = dropdown.dropdown.options[dropdown.dropdown.value].text;
            var character = Db.CharacterDatabase.GetCharacterByName(characterName);

            if (character != null)
            {
                m_currentCutscene.LeftSideCharacters.Add(character.id);
                m_currentCutscene.LeftCharacterOverrideNames.Add(string.Empty);
                m_leftSideCharacter = character.id;
                ResetDialogueList(m_currentDialogue?.Uid);
                Save();
            }
        }

        private void OnAddRightCharacter(D90DatabaseDropdown dropdown)
        {
            var characterName = dropdown.dropdown.options[dropdown.dropdown.value].text;
            var character = Db.CharacterDatabase.GetCharacterByName(characterName);

            if (character != null)
            {
                m_currentCutscene.RightSideCharacters.Add(character.id);
                m_currentCutscene.RightCharacterOverrideNames.Add(string.Empty);
                m_rightSideCharacter = character.id;
                ResetDialogueList(m_currentDialogue?.Uid);
                Save();
            }
        }


        private void OnCopyRightCharacters()
        {
            copyCharacters.Clear();
            copyCharacterOverrideNames.Clear();

            for (var i = 0; i < m_currentCutscene.RightSideCharacters.Count; i++)
            {
                var characterId = m_currentCutscene.RightSideCharacters[i];
                copyCharacters.Add(characterId);

                if (i >= m_currentCutscene.RightCharacterOverrideNames.Count)
                {
                    copyCharacterOverrideNames.Add(string.Empty);
                }

                var characterOverrideName = m_currentCutscene.RightCharacterOverrideNames[i];
                copyCharacterOverrideNames.Add(characterOverrideName);
            }
        }

        private void OnCopyLeftCharacters()
        {
            copyCharacters.Clear();
            copyCharacterOverrideNames.Clear();

            for (var i = 0; i < m_currentCutscene.LeftSideCharacters.Count; i++)
            {
                var characterId = m_currentCutscene.LeftSideCharacters[i];
                copyCharacters.Add(characterId);

                if (i >= m_currentCutscene.LeftCharacterOverrideNames.Count)
                {
                    copyCharacterOverrideNames.Add(string.Empty);
                }

                var characterOverrideName = m_currentCutscene.LeftCharacterOverrideNames[i];
                copyCharacterOverrideNames.Add(characterOverrideName);
            }
        }

        private void OnPasteRightCharacters()
        {
            for (var i = 0; i < copyCharacters.Count; i++)
            {
                var characterId = copyCharacters[i];

                if (characterId != null)
                {
                    m_currentCutscene.RightSideCharacters.Add(characterId);
                    m_currentCutscene.RightCharacterOverrideNames.Add(string.Empty);
                    m_rightSideCharacter = characterId;

                    ResetDialogueList(m_currentDialogue?.Uid);
                    Save();
                }
            }
        }

        private void OnPasteLeftCharacters()
        {
            for (var i = 0; i < copyCharacters.Count; i++)
            {
                var characterId = copyCharacters[i];

                if (characterId != null)
                {
                    m_currentCutscene.LeftSideCharacters.Add(characterId);
                    m_currentCutscene.LeftCharacterOverrideNames.Add(string.Empty);
                    m_leftSideCharacter = characterId;

                    ResetDialogueList(m_currentDialogue?.Uid);
                    Save();
                }
            }
        }

        private void OnRightSideCharacterSelected(bool isOn, string context)
        {
            if (isOn)
            {
                m_rightSideCharacter = context;
            }
        }

        private void OnLeftSideCharacterSelected(bool isOn, string context)
        {
            if (isOn)
            {
                m_leftSideCharacter = context;
            }
        }

        private void OnStartingMusicChanged(D90DatabaseDropdown dropdown, int value)
        {
            if (value == 0)
            {
                m_currentCutscene.StartingTrack = string.Empty;
            }
            else
            {
                m_currentCutscene.StartingTrack = dropdown.dropdown.options[value].text;
            }
        }

        private void OnStartingAmbientChanged(D90DatabaseDropdown dropdown, int value)
        {
            if (value == 0)
            {
                m_currentCutscene.StartingAmbient = string.Empty;
            }
            else
            {
                m_currentCutscene.StartingAmbient = dropdown.dropdown.options[value].text;
            }
        }

        private void OnBackButton()
        {
            if (string.IsNullOrEmpty(_currentFolder)) return;
            if (!_currentFolder.Contains("/"))
            {
                _currentFolder = string.Empty;
                SetupCutsceneWindow(null);
            }
            else
            {
                var lastIndex = _currentFolder.LastIndexOf('/');
                _currentFolder = _currentFolder.Remove(lastIndex, _currentFolder.Length - lastIndex);
                SetupCutsceneWindow(_currentFolder);
            }
        }

        private void OnNewFolder()
        {
            if (Db.CutsceneDatabase.TryCreateNewFolder(_currentFolder, out var newFolderName))
            {
                _currentFolder = newFolderName;
                SetupCutsceneWindow(null);
            }
        }

        private void OnCutsceneAddedToFolder(int index)
        {
            if (m_currentCutscene == null) return;


            var folderName = index <= 0 ? string.Empty : Db.CutsceneDatabase.Folders[index-1];
            Db.CutsceneDatabase.AddToFolder(folderName, m_currentCutscene);
            _currentFolder = folderName;
            SetupCutsceneWindow(m_currentCutscene.Uid);
        }

        private void OnSaveFolderName()
        {
            var newFolderName = folderNameInput.text;

            if (string.IsNullOrEmpty(_selectedFolder) || _selectedFolder == newFolderName) return;

            Db.CutsceneDatabase.ChangeFolderName(_selectedFolder, newFolderName);
            _currentFolder = newFolderName;
            SetupCutsceneWindow(null);
        }

        private void ChangeLocationOptions()
        {
            if (m_currentCutscene == null) return;

            cutsceneLocation.ClearOptions();

            switch (m_currentCutscene.CutsceneType)
            {
                case CutsceneType.HexMapDialogue:
                    cutsceneLocation.SetActive(false);
                    break;
                case CutsceneType.EventScene:
                    cutsceneLocation.SetActive(true);
                    cutsceneLocation.AddOptions(eventSceneLocationAnimator.animations.Select(anim => anim.name).ToList());
                    if (string.IsNullOrEmpty(m_currentCutscene.EventStageType))
                    {
                        m_currentCutscene.EventStageType = "grasslands";
                    }
                    var anim = eventSceneLocationAnimator.animations.Find(anim =>
                        anim.name == m_currentCutscene.EventStageType);
                    var index = eventSceneLocationAnimator.animations.IndexOf(anim);
                    cutsceneLocation.SetValueWithoutNotify(index);
                    break;
                case CutsceneType.Cutscene:
                    cutsceneLocation.SetActive(true);
#if UNITY_EDITOR
                    var settings = UnityEditor.AddressableAssets.AddressableAssetSettingsDefaultObject.Settings;
                    var locations = new List<string>();
                    foreach (var group in settings.groups)
                    {
                        foreach (var entry in group.entries)
                        {
                            if (entry.address.Contains("CutsceneLocation"))
                            {
                                var location = entry.address.Split('-');

                                if (location.Length > 1)
                                {
                                    locations.Add(location[1]);
                                }
                            }
                        }
                    }
                    cutsceneLocation.AddOptions(locations);

                    if (string.IsNullOrEmpty(m_currentCutscene.EventLocation))
                    {
                        m_currentCutscene.EventLocation = "Beach";
                    }

                    var option = cutsceneLocation.options.Find(op => op.text == m_currentCutscene.EventLocation);
                    var locationIndex = cutsceneLocation.options.IndexOf(option);
                    cutsceneLocation.SetValueWithoutNotify(locationIndex);
#endif
                    break;
            }




        }

        private void ResetCutsceneList()
        {
            cutscenesList.Clear();

            var folders = Db.CutsceneDatabase.GetFoldersInFolder(_currentFolder);

            for (var i = 0; i < folders.Count; i++)
            {
                var folderContent = Instantiate(cutsceneLayoutPrefab, cutscenesList.contentLayout, false);
                folderContent.mainButton.content = folders[i];
                folderContent.textLabels[0].text = Path.GetFileName(folders[i]);
                folderContent.OnDoubleClick -= OnFolderChanged;
                folderContent.OnDoubleClick += OnFolderChanged;
                folderContent.mainButton.OnValueChanged -= OnFolderSelected;
                folderContent.mainButton.OnValueChanged += OnFolderSelected;
                folderContent.sideIcon.Play("folder");
                cutscenesList.AddItem(folderContent);
            }

            var cutscenes = Db.CutsceneDatabase.GetCutscenesInFolder(_currentFolder);
            //Debug.LogError($"Cutscene count = {cutscenes.Count} Current Folder = {_currentFolder}");

            for (var i = 0; i < cutscenes.Count; i++)
            {
                var cutscene = cutscenes[i];
                var alreadyInsideList = cutscenesList.GetItem(cutscene.Uid) != null;
                if (alreadyInsideList) continue;

                var cutsceneContent = Instantiate(cutsceneLayoutPrefab, cutscenesList.contentLayout, false);
                cutsceneContent.mainButton.content = cutscene.Uid;
                cutsceneContent.textLabels[0].text = cutscene.EventTitle;
                cutsceneContent.mainButton.OnValueChanged -= OnCutsceneChanged;
                cutsceneContent.mainButton.OnValueChanged += OnCutsceneChanged;
                if (m_currentCutscene!=null) cutsceneContent.mainButton.Toggle.isOn = m_currentCutscene.Uid == cutscene.Uid;
                cutsceneContent.sideIcon.Play("chat");
                cutscenesList.AddItem(cutsceneContent);

                AddChoices(cutscene.DialogueInfo, 0);
            }
        }

        private void AddChoices(CutsceneDialogueInfo dialogue, int tabCount)
        {
            if (!dialogue.HasChoices) return;

            for (var k = 0; k < dialogue.Choices.Count; k++)
            {
                var choice = dialogue.Choices[k];

                if (string.IsNullOrEmpty(choice.CutsceneUid)) continue;
                if (cutscenesList.GetItem(choice.CutsceneUid) != null) continue;

                var tabs = string.Empty;
                for (var l = 0; l < tabCount; l++) tabs += "\t";
                tabs += "∟";
                var choiceCutscene = Db.CutsceneDatabase.GetCutscene(choice.CutsceneUid);
                if (choiceCutscene == null) continue;
                var choiceCutsceneContent = Instantiate(cutsceneLayoutPrefab, cutscenesList.contentLayout, false);
                choiceCutsceneContent.mainButton.content = choiceCutscene.Uid;
                choiceCutsceneContent.textLabels[0].text = $"{tabs}{choiceCutscene.EventTitle}";
                choiceCutsceneContent.mainButton.OnValueChanged -= OnCutsceneChanged;
                choiceCutsceneContent.mainButton.OnValueChanged += OnCutsceneChanged;
                choiceCutsceneContent.mainButton.Toggle.isOn = m_currentCutscene.Uid == choiceCutscene.Uid;
                cutscenesList.AddItem(choiceCutsceneContent);
                AddChoices(choiceCutscene.DialogueInfo, tabCount + 1);
            }
        }

        private void AddChoices(List<CutsceneDialogueInfo> next, int tabCount)
        {
            for (var j = 0; j < next.Count; j++)
            {
                var dialogue = next[j];
                AddChoices(dialogue, tabCount);
            }
        }

        private void ResetDialogueList(string dialogueUid)
        {
            if (m_currentCutscene?.DialogueInfo == null)
            {
                return;
            }

            dialogueList.Clear();

            if (m_currentCutscene.DialogueInfo.Count > 0)
            {
                if (string.IsNullOrEmpty(dialogueUid))
                {
                    m_currentDialogue = m_currentCutscene.DialogueInfo[0];
                }
                else
                {
                    m_currentDialogue = m_currentCutscene.DialogueInfo.Find(dialogue => dialogue.Uid == dialogueUid);
                }
            }

            if (m_currentDialogue == null)
            {
                return;
            }

            SetupCharacterInspector();

            for (var i = 0; i < m_currentCutscene.DialogueInfo.Count; i++)
            {
                var dialogue = m_currentCutscene.DialogueInfo[i];

                if (string.IsNullOrEmpty(dialogue.Uid))
                {
                    dialogue.UpdateIdentifier();
                }

                if (dialogue.CharacterIndex < 0)
                {
                    dialogue.CharacterIndex = 0;
                }

                if (dialogue.CharacterSide == CharacterSide.Right && m_currentCutscene.RightSideCharacters.Count <= dialogue.CharacterIndex)
                {
                    dialogue.CharacterIndex = m_currentCutscene.RightSideCharacters.Count - 1;
                }

                if (dialogue.CharacterSide == CharacterSide.Left && m_currentCutscene.LeftSideCharacters.Count <= dialogue.CharacterIndex)
                {
                    dialogue.CharacterIndex = m_currentCutscene.LeftSideCharacters.Count - 1;
                }

                if (dialogue.CharacterIndex < 0) continue;

                var characterId = dialogue.CharacterSide == CharacterSide.Right ?
                    m_currentCutscene.RightSideCharacters[dialogue.CharacterIndex] :
                    m_currentCutscene.LeftSideCharacters[dialogue.CharacterIndex];
                var characterOverrideName =dialogue.CharacterSide == CharacterSide.Right ?
                    m_currentCutscene.RightCharacterOverrideNames[dialogue.CharacterIndex] :
                    m_currentCutscene.LeftCharacterOverrideNames[dialogue.CharacterIndex];

                var character = Db.CharacterDatabase.GetCharacter(characterId);

                if (character == null)
                {
                    continue;
                }

                var text = dialogue.Text;
                var factionColor = $"<color={ColorPalette.CrossBlitz.GetFactionColor(character.faction)}>";
                var dialogueContent = Instantiate(cutsceneLayoutPrefab, dialogueList.contentLayout, false);
                dialogueContent.mainButton.content = dialogue.Uid;
                dialogueContent.textLabels[0].text = !string.IsNullOrEmpty(characterOverrideName) ?
                    $"{i+1} {factionColor}{characterOverrideName}:</color> {text}":
                    $"{i+1} {factionColor}{character.name}:</color> {text}";
                dialogueContent.mainButton.OnValueChanged -= OnDialogueEdit;
                dialogueContent.mainButton.OnValueChanged += OnDialogueEdit;
                dialogueContent.mainButton.Toggle.isOn = m_currentDialogue.Uid == dialogue.Uid;
                dialogueList.AddItem(dialogueContent);

                if (!dialogue.HasChoices)
                {
                    continue;
                }

                for (var j = 0; j < dialogue.Choices.Count; j++)
                {
                    var choice = dialogue.Choices[j];
                    dialogueContent.textLabels[0].text += $"\n\t{j + 1}. {choice.Choice}";
                }
            }

            //setup all the other properties.
            var characterNames = new List<string>();

            for (var i = 0; i < m_currentCutscene.LeftSideCharacters.Count; i++)
            {
                var character = Db.CharacterDatabase.GetCharacter(m_currentCutscene.LeftSideCharacters[i]);

                if (character != null)
                {
                    characterNames.Add(character.name);
                }
            }

            for (var i = 0; i < m_currentCutscene.RightSideCharacters.Count; i++)
            {
                var character = Db.CharacterDatabase.GetCharacter(m_currentCutscene.RightSideCharacters[i]);

                if (character != null)
                {
                    characterNames.Add(character.name);
                }
            }

            speakingCharacterDropdown.ClearOptions();
            speakingCharacterDropdown.AddOptions(characterNames);

            if (m_currentDialogue.CharacterIndex >= 0 &&
                (m_currentDialogue.CharacterSide == CharacterSide.Right && m_currentDialogue.CharacterIndex < m_currentCutscene.RightSideCharacters.Count ||
                 m_currentDialogue.CharacterSide == CharacterSide.Left && m_currentDialogue.CharacterIndex < m_currentCutscene.LeftSideCharacters.Count))
            {
                var currentSide = m_currentDialogue.CharacterSide;
                var speakingCharacterId = currentSide== CharacterSide.Left
                    ? m_currentCutscene.LeftSideCharacters[m_currentDialogue.CharacterIndex]
                    : m_currentCutscene.RightSideCharacters[m_currentDialogue.CharacterIndex];

                var speakingCharacter = Db.CharacterDatabase.GetCharacter(speakingCharacterId);

                if (speakingCharacter != null)
                {
                    var index = -1;
                    var startingIndex = currentSide == CharacterSide.Right
                        ? m_currentCutscene.LeftSideCharacters.Count
                        : 0;
                    for (var i = startingIndex; i < characterNames.Count; i++)
                    {
                        if (speakingCharacter.name == characterNames[i])
                        {
                            index = i;
                            break;
                        }
                    }
                    speakingCharacterDropdown.SetValueWithoutNotify(index < 0 ? 0 : index);
                }
            }

            speakingCharacterDialogue.SetTextWithoutNotify(m_currentDialogue.Text);

            hasChoices.SetIsOnWithoutNotify(m_currentDialogue.HasChoices);

            usesThoughtBubble.SetIsOnWithoutNotify(m_currentDialogue.UsesThoughtBubble);

            if (m_currentDialogue.Choices == null || m_currentDialogue.Choices.Count < 2)
            {
                m_currentDialogue.Choices = new List<CutsceneChoice> { new CutsceneChoice{ Choice = "choice 1" }, new CutsceneChoice {Choice = "choice 2"}  };
                Save();
            }

            choice1.SetTextWithoutNotify(m_currentDialogue.Choices[0].Choice);
            choice2.SetTextWithoutNotify(m_currentDialogue.Choices[1].Choice);

            var choice1Cutscene = Db.CutsceneDatabase.GetCutscene(m_currentDialogue.Choices[0].CutsceneUid);
            cutsceneChoice1.Init();
            cutsceneChoice1.SetDropdownWithoutNotify(choice1Cutscene == null ? "None" : choice1Cutscene.EventTitle);

            cutsceneChoice1.OnNewButton -= OnNewChoice1CutsceneCreated;
            cutsceneChoice1.OnNewButton += OnNewChoice1CutsceneCreated;

            var choice2Cutscene = Db.CutsceneDatabase.GetCutscene(m_currentDialogue.Choices[1].CutsceneUid);
            cutsceneChoice2.Init();
            cutsceneChoice2.SetDropdownWithoutNotify(choice2Cutscene == null ? "None" : choice2Cutscene.EventTitle);

            cutsceneChoice2.OnNewButton -= OnNewChoice2CutsceneCreated;
            cutsceneChoice2.OnNewButton += OnNewChoice2CutsceneCreated;

            if (m_currentDialogue.RightSideEmotes.Count < m_currentCutscene.RightSideCharacters.Count)
            {
                for (var i = m_currentDialogue.RightSideEmotes.Count; i < m_currentCutscene.RightSideCharacters.Count; i++)
                {
                    m_currentDialogue.RightSideEmotes.Add( "talking" );
                }
            }
            else if (m_currentDialogue.RightSideEmotes.Count > m_currentCutscene.RightSideCharacters.Count)
            {
                var difference = m_currentDialogue.RightSideEmotes.Count - m_currentCutscene.RightSideCharacters.Count;
                m_currentDialogue.RightSideEmotes.RemoveRange(m_currentDialogue.RightSideEmotes.Count-difference-1, difference);
            }

            if (m_currentDialogue.LeftSideEmotes.Count < m_currentCutscene.LeftSideCharacters.Count)
            {
                for (var i = m_currentDialogue.LeftSideEmotes.Count; i < m_currentCutscene.LeftSideCharacters.Count; i++)
                {
                    m_currentDialogue.LeftSideEmotes.Add( "talking" );
                }
            }
            else if (m_currentDialogue.LeftSideEmotes.Count > m_currentCutscene.LeftSideCharacters.Count)
            {
                var difference = m_currentDialogue.LeftSideEmotes.Count - m_currentCutscene.LeftSideCharacters.Count;
                m_currentDialogue.LeftSideEmotes.RemoveRange(m_currentDialogue.LeftSideEmotes.Count-difference-1, difference);
            }

            var emotes = CutsceneData.GetCutsceneEmotions();

            emoteDropdown.ClearOptions();
            emoteDropdown.AddOptions(emotes);

            if (!string.IsNullOrEmpty(m_emoteCharacter))
            {
                var characterIndex = m_currentCutscene.LeftSideCharacters.IndexOf(m_emoteCharacter);

                if (characterIndex >= 0)
                {
                    emoteDropdown.SetValueWithoutNotify(
                        emotes.IndexOf(m_currentDialogue.LeftSideEmotes[characterIndex]));
                }

                characterIndex = m_currentCutscene.RightSideCharacters.IndexOf(m_emoteCharacter);

                if (characterIndex >= 0)
                {
                    emoteDropdown.SetValueWithoutNotify(
                        emotes.IndexOf(m_currentDialogue.RightSideEmotes[characterIndex]));
                }
            }

            emoteSetButton.onClick.RemoveAllListeners();
            emoteSetButton.onClick.AddListener(OnEmoteSet);
            
            emoteSetTalkingButton.onClick.RemoveAllListeners();
            emoteSetTalkingButton.onClick.AddListener(() => OnEmoteSetButton("talking"));
           
            emoteSetAngryButton.onClick.RemoveAllListeners();
            emoteSetAngryButton.onClick.AddListener(() => OnEmoteSetButton("angry"));
            
            emoteSetBoredButton.onClick.RemoveAllListeners();
            emoteSetBoredButton.onClick.AddListener(() => OnEmoteSetButton("bored"));
            
            emoteSetConfusedButton.onClick.RemoveAllListeners();
            emoteSetConfusedButton.onClick.AddListener(() => OnEmoteSetButton("confused"));
            
            emoteSetDeterminedButton.onClick.RemoveAllListeners();
            emoteSetDeterminedButton.onClick.AddListener(() => OnEmoteSetButton("determined"));
           
            emoteSetEmbarassedButton.onClick.RemoveAllListeners();
            emoteSetEmbarassedButton.onClick.AddListener(() => OnEmoteSetButton("embarassed"));
           
            emoteSetFlirtButton.onClick.RemoveAllListeners();
            emoteSetFlirtButton.onClick.AddListener(() => OnEmoteSetButton("flirt"));
           
            emoteSetJoyButton.onClick.RemoveAllListeners();
            emoteSetJoyButton.onClick.AddListener(() => OnEmoteSetButton("joy"));
            
            emoteSetNervousButton.onClick.RemoveAllListeners();
            emoteSetNervousButton.onClick.AddListener(() => OnEmoteSetButton("nervous"));
            
            emoteSetSadButton.onClick.RemoveAllListeners();
            emoteSetSadButton.onClick.AddListener(() => OnEmoteSetButton("sad"));
            
            emoteSetShockedButton.onClick.RemoveAllListeners();
            emoteSetShockedButton.onClick.AddListener(() => OnEmoteSetButton("shocked"));

            emoteList.Clear();


            TxtThoughtsButton.onClick.RemoveAllListeners();
            TxtThoughtsButton.onClick.AddListener(() => OnTxtSetButton("<color=#9D7D68></color>"));

            TxtPersonButton.onClick.RemoveAllListeners();
            TxtPersonButton.onClick.AddListener(() => OnTxtSetButton("<color=#C74D4D></color>"));

            TxtPlaceButton.onClick.RemoveAllListeners();
            TxtPlaceButton.onClick.AddListener(() => OnTxtSetButton("<color=#466DCE></color>"));

            TxtItemButton.onClick.RemoveAllListeners();
            TxtItemButton.onClick.AddListener(() => OnTxtSetButton("<color=#63982F></color>"));

            TxtShakeButton.onClick.RemoveAllListeners();
            TxtShakeButton.onClick.AddListener(() => OnTxtSetButton("<shake a=0.25></shake>"));

            TxtWiggleButton.onClick.RemoveAllListeners();
            TxtWiggleButton.onClick.AddListener(() => OnTxtSetButton("<wiggle a=0.15 w=0.15></wiggle>"));

            TxtWaveButton.onClick.RemoveAllListeners();
            TxtWaveButton.onClick.AddListener(() => OnTxtSetButton("<wave a=0.25></wave>"));

            TxtSizeButton.onClick.RemoveAllListeners();
            TxtSizeButton.onClick.AddListener(() => OnTxtSetButton("{size}{/size}"));

            TxtScreenShakeLyButton.onClick.RemoveAllListeners();
            TxtScreenShakeLyButton.onClick.AddListener(() => OnTxtSetButton("<?screen-shake:12:4>"));

            TxtScreenShakeHButton.onClick.RemoveAllListeners();
            TxtScreenShakeHButton.onClick.AddListener(() => OnTxtSetButton("<?screen-shake:16:6>"));

            TxtCharacterShakeLButton.onClick.RemoveAllListeners();
            TxtCharacterShakeLButton.onClick.AddListener(() => OnTxtSetButton("<?shake-char:character-name>:5:0.3>"));

            TxtCharacterShakeHButton.onClick.RemoveAllListeners();
            TxtCharacterShakeHButton.onClick.AddListener(() => OnTxtSetButton("<?shake-char:character-name>:5:0.3>"));

            TxtBounceButton.onClick.RemoveAllListeners();
            TxtBounceButton.onClick.AddListener(() => OnTxtSetButton("<?bounce-char:{CHARACTER_NAME}>"));

            TxtScreenFlashButton.onClick.RemoveAllListeners();
            TxtScreenFlashButton.onClick.AddListener(() => OnTxtSetButton("<?flash>"));

            TxtCharacterFadeButton.onClick.RemoveAllListeners();
            TxtCharacterFadeButton.onClick.AddListener(() => OnTxtSetButton("<?slow-exit>"));

            SFXDeterminedButton.onClick.RemoveAllListeners();
            SFXDeterminedButton.onClick.AddListener(() => OnTxtSetButton("<?sfx:CutsceneSFX-Determination>"));

            SFXDingButton.onClick.RemoveAllListeners();
            SFXDingButton.onClick.AddListener(() => OnTxtSetButton("<?sfx:CutsceneSFX-Ding>"));

            SFXQuestionButton.onClick.RemoveAllListeners();
            SFXQuestionButton.onClick.AddListener(() => OnTxtSetButton("<?sfx:CutsceneSFX-Question>"));

            SFXSmack1Button.onClick.RemoveAllListeners();
            SFXSmack1Button.onClick.AddListener(() => OnTxtSetButton("<?sfx:CutsceneSFX-Smack1>"));

            SFXSmack2Button.onClick.RemoveAllListeners();
            SFXSmack2Button.onClick.AddListener(() => OnTxtSetButton("<?sfx:CutsceneSFX-Smack2>"));

            SFXSmack3Button.onClick.RemoveAllListeners();
            SFXSmack3Button.onClick.AddListener(() => OnTxtSetButton("<?sfx:CutsceneSFX-Smack3>"));

            MusicPlayButton.onClick.RemoveAllListeners();
            MusicPlayButton.onClick.AddListener(() => OnTxtSetButton("<?music:NAME>"));

            MusicFadeButton.onClick.RemoveAllListeners();
            MusicFadeButton.onClick.AddListener(() => OnTxtSetButton("<?fade-music>"));

            AmbiencePlayButton.onClick.RemoveAllListeners();
            AmbiencePlayButton.onClick.AddListener(() => OnTxtSetButton("<?ambient:NAME>"));

            AmbienceStopButton.onClick.RemoveAllListeners();
            AmbienceStopButton.onClick.AddListener(() => OnTxtSetButton("<?stop-ambient>"));


            for (var i = 0; i < m_currentCutscene.RightSideCharacters.Count; i++)
            {
                var characterId = m_currentCutscene.RightSideCharacters[i];
                var emote = m_currentDialogue.RightSideEmotes[i];
                var character = Db.CharacterDatabase.GetCharacter(characterId);
                if (character == null) continue;
                var factionColor = $"<color={ColorPalette.CrossBlitz.GetFactionColor(character.faction)}>";
                var characterContent = Instantiate(cutsceneLayoutPrefab, emoteList.contentLayout, false);

                characterContent.mainButton.content = characterId;
                characterContent.textLabels[0].text = $"{factionColor}{character.name} ({emote})</color>";
                characterContent.mainButton.OnValueChanged -= OnEmoteCharacterSelected;
                characterContent.mainButton.OnValueChanged += OnEmoteCharacterSelected;
                characterContent.mainButton.Toggle.isOn = m_emoteCharacter == characterId;
                emoteList.AddItem(characterContent);
            }

            for (var i = 0; i < m_currentCutscene.LeftSideCharacters.Count; i++)
            {
                var characterId = m_currentCutscene.LeftSideCharacters[i];
                var emote = m_currentDialogue.LeftSideEmotes[i];
                var character = Db.CharacterDatabase.GetCharacter(characterId);
                if (character == null) continue;
                var factionColor = $"<color={ColorPalette.CrossBlitz.GetFactionColor(character.faction)}>";
                var characterContent = Instantiate(cutsceneLayoutPrefab, emoteList.contentLayout, false);

                characterContent.mainButton.content = characterId;
                characterContent.textLabels[0].text = $"{factionColor}{character.name} ({emote})</color>";
                characterContent.mainButton.OnValueChanged -= OnEmoteCharacterSelected;
                characterContent.mainButton.OnValueChanged += OnEmoteCharacterSelected;
                characterContent.mainButton.Toggle.isOn = m_emoteCharacter == characterId;
                emoteList.AddItem(characterContent);
            }

            // dawnDollarReward.SetTextWithoutNotify(m_currentDialogue.DawnDollarReward.ToString());
            // goldReward.SetTextWithoutNotify(m_currentDialogue.GoldReward.ToString());

            itemDatabaseDropdown.OnAddButton -= OnItemAdded;
            itemDatabaseDropdown.OnAddButton += OnItemAdded;

            itemLayout.OnDelete -= OnItemDeleted;
            itemLayout.OnDelete += OnItemDeleted;

            questDropdown.OnValueChanged -= OnQuestChanged;
            questDropdown.OnValueChanged += OnQuestChanged;
            var quest = Db.FableQuestDatabase.GetQuest(m_currentCutscene.QuestUid);
            questDropdown.SetDropdownWithoutNotify(quest?.Name);

            questStepChange.onEndEdit.RemoveAllListeners();
            questStepChange.onEndEdit.AddListener(OnQuestStepChanged);
            questStepChange.SetTextWithoutNotify(m_currentCutscene.QuestStepChange.ToString());

            incrementQuestInstead.onValueChanged.RemoveAllListeners();
            incrementQuestInstead.onValueChanged.AddListener(OnIncrementQuestStepInsteadChanged);
            incrementQuestInstead.SetIsOnWithoutNotify(m_currentCutscene.IncrementQuestStepInstead);

            RefreshItems();
        }

        private void OnIncrementQuestStepInsteadChanged(bool isOn)
        {
            m_currentCutscene.IncrementQuestStepInstead = isOn;
        }

        private void OnQuestChanged(D90DatabaseDropdown dropdown, int value)
        {
            var quest = Db.FableQuestDatabase.GetQuestUidFromName(dropdown.dropdown.options[value].text);
            m_currentCutscene.QuestUid = quest;
        }

        private void OnQuestStepChanged(string text)
        {
            if (int.TryParse(text, out var step))
            {
                m_currentCutscene.QuestStepChange = step;
            }
        }

        private void OnTxtSetButton(string str)
        {
            GUIUtility.systemCopyBuffer = str;
        }

        private void OnEmoteSetButton(string emote)
        {

            if (!string.IsNullOrEmpty(emote) && !string.IsNullOrEmpty(m_emoteCharacter))
            {
                var characterIndex = m_currentCutscene.LeftSideCharacters.IndexOf(m_emoteCharacter);

                if (characterIndex >= 0)
                {
                    m_currentDialogue.LeftSideEmotes[characterIndex] = emote;
                }

                characterIndex = m_currentCutscene.RightSideCharacters.IndexOf(m_emoteCharacter);

                if (characterIndex >= 0)
                {
                    m_currentDialogue.RightSideEmotes[characterIndex] = emote;
                }

                ResetDialogueList(m_currentDialogue?.Uid);
                Save();
            }
        }

        private void OnEmoteSet()
        {
            var emote = emoteDropdown.options[emoteDropdown.value].text;

            if (!string.IsNullOrEmpty(emote) && !string.IsNullOrEmpty(m_emoteCharacter))
            {
                var characterIndex = m_currentCutscene.LeftSideCharacters.IndexOf(m_emoteCharacter);

                if (characterIndex >= 0)
                {
                    m_currentDialogue.LeftSideEmotes[characterIndex] = emote;
                }

                characterIndex = m_currentCutscene.RightSideCharacters.IndexOf(m_emoteCharacter);

                if (characterIndex >= 0)
                {
                    m_currentDialogue.RightSideEmotes[characterIndex] = emote;
                }

                ResetDialogueList(m_currentDialogue?.Uid);
                Save();
            }
        }

        private void OnEmoteCharacterSelected(bool isOn, string context)
        {
            if (isOn)
            {
                m_emoteCharacter = context;
            }
        }

        private void AddNewItem(string itemUid)
        {
            if (m_currentCutscene != null)
            {
                if (m_currentCutscene.RewardItems == null) m_currentCutscene.RewardItems = new List<ItemValue>();

                var itemValue = m_currentCutscene.RewardItems.Find(item => item.ItemUid == itemUid);

                if (itemValue != null)
                {
                    itemValue.Count++;
                }
                else
                {
                    m_currentCutscene.RewardItems.Add(new ItemValue {ItemUid = itemUid, Count = 1});
                }

                _currentItemUid = itemUid;

                RefreshItems();
            }
        }

        private void RemoveItem(string itemUid)
        {
            if (m_currentCutscene != null)
            {
                if (m_currentCutscene.RewardItems == null) m_currentCutscene.RewardItems = new List<ItemValue>();

                var itemValue = m_currentCutscene.RewardItems.Find(item => item.ItemUid == itemUid);

                if (itemValue != null)
                {
                    itemValue.Count--;

                    if (itemValue.Count <= 0)
                    {
                        m_currentCutscene.RewardItems.Remove(itemValue);
                    }
                }

                RefreshItems();
            }
        }

        private void RefreshItems()
        {
            itemLayout.Clear();

            //Debug.Log($"Reward Items = {m_currentCutscene.RewardItems}");
            m_currentCutscene.RewardItems ??= new List<ItemValue>();

            for (var i = 0; i < m_currentCutscene.RewardItems.Count; i++)
            {
                var item = Db.ItemDatabase.GetItem(m_currentCutscene.RewardItems[i].ItemUid);
                var itemValue = m_currentCutscene.RewardItems[i];
                if (item == null) continue;
                var itemContent = Instantiate(itemContentPrefab, itemLayout.contentLayout, false);
                itemContent.mainButton.content = item.ItemId;
                itemContent.textLabels[0].text = item.DisplayName;
                itemContent.textLabels[1].text = $"x{itemValue.Count}";
                itemContent.textLabels[1].SetActive(true);
                itemContent.mainButton.OnValueChanged -= OnItemChanged;
                itemContent.mainButton.OnValueChanged += OnItemChanged;
                itemContent.mainButton.Toggle.isOn = _currentItemUid == item.ItemId;
                itemLayout.AddItem(itemContent);
            }
        }

        private void Update()
        {
            if (speakingCharacterDropdown.options.Count <= 0) return;
            if (speakingCharacterDropdown.value >= speakingCharacterDropdown.options.Count) return;

            var speakingCharacter = speakingCharacterDropdown.options[speakingCharacterDropdown.value].text;

            addDialogueButton.interactable = !string.IsNullOrEmpty(speakingCharacter) && speakingCharacter != "None" &&
                                             !string.IsNullOrEmpty(speakingCharacterDialogue.text);

            if (hasChoices.isOn)
            {
                addDialogueButton.interactable = !string.IsNullOrEmpty(choice1.text) &&
                                                 !string.IsNullOrEmpty(choice2.text);
            }

            disableChoiceCanvas.interactable = hasChoices.isOn;

            if(_currentInputMode) _currentInputMode.UpdateMode();

            //var folders = Db.CutsceneDatabase.GetFoldersInFolder(_selectedFolder);
            //var cutscenes = Db.CutsceneDatabase.GetCutscenesInFolder(_selectedFolder);
            //Debug.Log("FOLDERS: " + folders.Count);
            //Debug.Log("CUTSCENES: " + cutscenes.Count);
            //Debug.Log("");
        }

        private async void OnPlayButton(bool atIndex)
        {
            if (m_currentCutscene == null) return;

            var currentIndex = atIndex ? m_currentCutscene.DialogueInfo.IndexOf(m_currentDialogue) : 0;

            ResetDialogueList(m_currentDialogue == null ? string.Empty : m_currentDialogue.Uid);

            switch (m_currentCutscene.CutsceneType)
            {
                case CutsceneType.HexMapDialogue:
                    Debug.LogError("Map Dialogue not supported!");
                    break;
                case CutsceneType.EventScene:
                    D90Tools.Instance.GetComponent<Canvas>().enabled = false;
                    eventSceneInputMode.StartMode();
                    while (eventSceneInputMode.EventSceneInput == null) await Await.NextUpdate();
                    _currentInputMode = eventSceneInputMode;
                    _currentInputMode.EventSceneInput.OnFinished -= OnFinishedCutscene;
                    _currentInputMode.EventSceneInput.OnFinished += OnFinishedCutscene;
                    _currentInputMode.EventSceneInput.stage.skipToDialogueIndex = currentIndex;
                    _currentInputMode.EventSceneInput.StartEvent(m_currentCutscene);
                    break;
                case CutsceneType.Cutscene:
                    D90Tools.Instance.GetComponent<Canvas>().enabled = false;
                    cutsceneInputMode.StartMode();
                    while (cutsceneInputMode.CutsceneInput == null) await Await.NextUpdate();
                    _currentInputMode = cutsceneInputMode;
                    _currentInputMode.CutsceneInput.OnFinishedCutscene -= OnFinishedCutscene;
                    _currentInputMode.CutsceneInput.OnFinishedCutscene += OnFinishedCutscene;
                    _currentInputMode.CutsceneInput.stage.skipToDialogueIndex = currentIndex;
                    _currentInputMode.CutsceneInput.StartEvent(m_currentCutscene, new EventOptions { dimBackground = true, usesSkipButton = true, usesPortraitBackground = true, usesTitleContainer = true, usesDialogueBox = true} );
                    break;
            }
        }



        // var currentIndex = m_currentCutscene.DialogueInfo.IndexOf(m_currentDialogue);
        private void OnFinishedCutscene()
        {
            if (_currentInputMode)
            {
                AudioController.StopSong();
                _currentInputMode.EndMode();
                _currentInputMode = null;
                D90Tools.Instance.GetComponent<Canvas>().enabled = true;
            }
        }
        private void Save()
        {
            Db.CutsceneDatabase.Save();
        }
    }
}