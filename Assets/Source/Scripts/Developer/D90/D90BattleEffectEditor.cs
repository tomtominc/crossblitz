using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card.Vfx;
using CrossBlitz.Databases;
using CrossBlitz.GameVfx;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90BattleEffectEditor : MonoBehaviour
    {
        public InputField nameField;
        public Dropdown vfxTypeDropdown;
        public InputField prefabNameField;

        private string m_effectUid;
        private List<string> vfxTypes;

        public List<string> GetAllGameVfxTypes()
        {
            var gameVfxComponentNames = new List<string>();

            var q = typeof(IGameVfx).Assembly.GetTypes()
                .Where(x => !x.IsAbstract)
                .Where(x => !x.IsGenericTypeDefinition)
                .Where(x => typeof(IGameVfx).IsAssignableFrom(x))
                .ToList();

            for (var i = 0; i < q.Count; i++)
            {
                gameVfxComponentNames.Add(q[i].AssemblyQualifiedName);
            }

            return gameVfxComponentNames;
        }

        public void Init(string effectUid)
        {
            m_effectUid = effectUid;

            if (string.IsNullOrEmpty(m_effectUid))
            {
                //gameObject.SetActive(false);
                return;
            }

            var effect = Db.BattleDatabase.GetBattleVfx(m_effectUid);

            if (effect == null)
            {
                //gameObject.SetActive(false);
                return;
            }

            gameObject.SetActive(true);

            nameField.onValueChanged.RemoveAllListeners();
            nameField.onValueChanged.AddListener(OnNameChanged);
            nameField.SetTextWithoutNotify(effect.vfxName);

            prefabNameField.onValueChanged.RemoveAllListeners();
            prefabNameField.onValueChanged.AddListener(OnPrefabNameChanged);
            prefabNameField.SetTextWithoutNotify(effect.vfxPrefab);

            vfxTypeDropdown.onValueChanged.RemoveAllListeners();
            vfxTypeDropdown.onValueChanged.AddListener(OnVfxTypeChanged);
            vfxTypeDropdown.ClearOptions();

            if (vfxTypes == null)
            {
                vfxTypes= new List<string> { "NONE" };
                vfxTypes.AddRange(GetAllGameVfxTypes());
            }

            vfxTypeDropdown.AddOptions(vfxTypes);
            var nameIndex = string.IsNullOrEmpty(effect.vfxType) ? 0 : vfxTypes.IndexOf(effect.vfxType);
            vfxTypeDropdown.SetValueWithoutNotify(nameIndex);
        }

        private void OnNameChanged(string value)
        {
            var effect = Db.BattleDatabase.GetBattleVfx(m_effectUid);

            if (effect == null)
            {
                return;
            }

            effect.vfxName = value;
        }

        private void OnVfxTypeChanged(int index)
        {
            var effect = Db.BattleDatabase.GetBattleVfx(m_effectUid);

            if (effect == null)
            {
                return;
            }

            effect.vfxType = vfxTypeDropdown.options[index].text;
            var typeNamesWithAssemblies = effect.vfxType.Split('.');
            var typeNames = typeNamesWithAssemblies[typeNamesWithAssemblies.Length - 1].Split(',');
            prefabNameField.text=typeNames[0];
        }

        private void OnPrefabNameChanged(string value)
        {
            var effect = Db.BattleDatabase.GetBattleVfx(m_effectUid);

            if (effect == null)
            {
                return;
            }

            effect.vfxPrefab = value;
        }
    }
}