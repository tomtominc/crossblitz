using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Data;
using CrossBlitz.UI.Widgets;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Developer.D90
{
    public class D90MapDatabaseAdvancedSearch
    {
        public NodeType nodeType;
    }
    public class D90MapAdvancedSearchPage : MonoBehaviour
    {
        public D90ContentLayout tilesLayout;
        public LayoutItem tilePrefab;
        public Dropdown nodeTypeSearchDropdown;
        public Button openTileEditorButton;
        public Button saveButton;
        public Button forceUpdateButton;

        private D90MapDatabaseAdvancedSearch m_search;
        private int m_currentTile;

        public void RefreshSearch()
        {
            if (m_search == null)
            {
                m_search = new D90MapDatabaseAdvancedSearch();
            }

            tilesLayout.Clear();

            nodeTypeSearchDropdown.ClearOptions();
            nodeTypeSearchDropdown.AddOptions(System.Enum.GetNames(typeof(NodeType)).ToList());
            nodeTypeSearchDropdown.SetValueWithoutNotify((int)m_search.nodeType);
            nodeTypeSearchDropdown.onValueChanged.RemoveAllListeners();
            nodeTypeSearchDropdown.onValueChanged.AddListener(OnNodeTypeChanged);

            if (m_search.nodeType == NodeType.None)
            {
                return;
            }

            openTileEditorButton.onClick.RemoveAllListeners();
            openTileEditorButton.onClick.AddListener(OnEditTile);

            forceUpdateButton.onClick.RemoveAllListeners();
            forceUpdateButton.onClick.AddListener(OnUpdateButton);

            var tiles = Db.MapDatabase.GetTilesByNodeType(m_search.nodeType);
            tiles = tiles.OrderBy(tile => Db.MapDatabase.GetDirectoryNameForTile(tile.uid)).ToList();

            for (var i = 0; i < tiles.Count; i++)
            {
                var tileView = Instantiate(tilePrefab, tilesLayout.contentLayout);
                tileView.mainButton.content = tiles[i].uid.ToString();
                tileView.mainButton.Toggle.SetIsOnWithoutNotify( m_currentTile == tiles[i].uid );
                tileView.mainButton.OnValueChanged -= OnTileToggled;
                tileView.mainButton.OnValueChanged += OnTileToggled;
                tileView.textLabels[0].text = Db.MapDatabase.GetDirectoryNameForTile(tiles[i].uid);

                switch (tiles[i].NodeType)
                {
                    case NodeType.Chest:
                    case NodeType.Shop:
                        for (var j = 0; j < tiles[i].items.Count; j++)
                        {
                            var item = Db.ItemDatabase.GetItem(tiles[i].items[j].ItemUid);

                            if (item == null)
                            {
                                Db.ItemDatabase.FixItemDependencies();
                            }

                            if (item == null) continue;

                            var comma = j == tiles[i].items.Count - 1 ? string.Empty : ",";
                            tileView.textLabels[0].text += $" {item.DisplayName}{comma}";

                        }
                        break;
                    case NodeType.Battle:
                        if (!string.IsNullOrEmpty(tiles[i].battleUid))
                        {
                            var battle = Db.BattleDatabase.GetBattleData(tiles[i].battleUid);
                            if (battle != null)
                            {
                                tileView.textLabels[0].text += battle.GetDatabaseDisplayName(out _);
                            }
                        }
                        break;
                    case NodeType.Event2:
                        if (!string.IsNullOrEmpty(tiles[i].cutsceneUid))
                        {
                            var cutscene = Db.CutsceneDatabase.GetCutscene(tiles[i].cutsceneUid);
                            tileView.textLabels[0].text += $" {cutscene.EventTitle} ";
                        }
                        break;
                }

                tilesLayout.AddItem(tileView);
            }

            saveButton.onClick.RemoveAllListeners();
            saveButton.onClick.AddListener(Save);
        }

        private void OnNodeTypeChanged(int choice)
        {
            m_search.nodeType = (NodeType)choice;
            RefreshSearch();
        }

        private void OnTileToggled(bool isOn, string context)
        {
            if (isOn && int.TryParse(context, out var value))
            {
                m_currentTile = value;
            }
        }

        private void OnEditTile()
        {
            if (m_currentTile != 0)
            {
                var tile = Db.MapDatabase.GetTile(m_currentTile);

                if (tile != null)
                {
                    Events.Publish(this, EventType.OpenTileEditor, new OpenTileEditorEventArgs
                    {
                        tileUid = tile.uid
                    });
                }
            }
        }

        private void OnUpdateButton()
        {
            RefreshSearch();

            D90ChapterEditor.ConfigureRewardPool();
        }

        private void Save()
        {
            Db.MapDatabase.Save();
            Db.FablesDatabase.Save();
            Db.BattleDatabase.Save();
        }
    }
}