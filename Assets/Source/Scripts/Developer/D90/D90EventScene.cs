using System.Collections.Generic;
using System.Linq;
using CrossBlitz.AssetManagement;
using CrossBlitz.Databases;
using CrossBlitz.Developer.Tools;
using CrossBlitz.Hero;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90EventScene : MonoBehaviour
    {
        public List<Image> portraitImages;
        public List<SpriteAnimation> shadows;
        public Dropdown shadowTypeDropdown;
        public SpriteAnimation environment;
        public Dropdown environmentDropdown;
        public D90IntegerInput xOffset;
        public D90IntegerInput yOffset;
        public D90IntegerInput xShadowOffset;
        public D90IntegerInput yShadowOffset;
        public Toggle flipX;

        private string m_characterId;

        public void Init()
        {
            xOffset.OnChanged -= OnXOffsetChanged;
            xOffset.OnChanged += OnXOffsetChanged;

            yOffset.OnChanged -= OnYOffsetChanged;
            yOffset.OnChanged += OnYOffsetChanged;

            xShadowOffset.OnChanged -= OnXShadowOffsetChanged;
            xShadowOffset.OnChanged += OnXShadowOffsetChanged;

            yShadowOffset.OnChanged -= OnYShadowOffsetChanged;
            yShadowOffset.OnChanged += OnYShadowOffsetChanged;

            shadowTypeDropdown.ClearOptions();
            shadowTypeDropdown.AddOptions(shadows[0].animationAsset.animations.Select(anim => anim.name).ToList());
            shadowTypeDropdown.onValueChanged.RemoveAllListeners();
            shadowTypeDropdown.onValueChanged.AddListener(OnShadowChanged);


            environmentDropdown.ClearOptions();
            environmentDropdown.AddOptions(environment.animationAsset.animations.Select(anim => anim.name).ToList());
            environmentDropdown.onValueChanged.RemoveAllListeners();
            environmentDropdown.onValueChanged.AddListener(OnEnvironmentChanged);

            flipX.onValueChanged.RemoveAllListeners();
            flipX.onValueChanged.AddListener(OnFlipX);
        }

        private void OnFlipX(bool flip)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.rightFacing = flip;

            for (var i = 0; i < portraitImages.Count; i++)
            {
                if (i == 0)
                {
                    portraitImages[i].RectTransform().localScale = new Vector3(character.rightFacing ? -1 : 1, 1, 1);
                }
                else
                {
                    portraitImages[i].RectTransform().localScale = new Vector3(character.rightFacing ? 1 : -1, 1, 1);
                }
            }
        }

        private void OnXOffsetChanged(int x)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.eventDialogueOffset = new Vector2Int(x, character.eventDialogueOffset.y);

            for (var i = 0; i < portraitImages.Count; i++)
            {
                if (i == 0)
                {
                    portraitImages[i].RectTransform().anchoredPosition = character.eventDialogueOffset;
                }
                else
                {
                    portraitImages[i].RectTransform().anchoredPosition =
                        new Vector2(-character.eventDialogueOffset.x, character.eventDialogueOffset.y);
                }
            }
        }

        private void OnYOffsetChanged(int y)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.eventDialogueOffset = new Vector2Int(character.eventDialogueOffset.x, y);

            for (var i = 0; i < portraitImages.Count; i++)
            {
                if (i == 0)
                {
                    portraitImages[i].RectTransform().anchoredPosition = character.eventDialogueOffset;
                }
                else
                {
                    portraitImages[i].RectTransform().anchoredPosition =
                        new Vector2(-character.eventDialogueOffset.x, character.eventDialogueOffset.y);
                }
            }
        }

        private void OnXShadowOffsetChanged(int x)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.eventDialogueShadowOffset = new Vector2Int(x, character.eventDialogueShadowOffset.y);
            for (var i = 0; i < shadows.Count; i++)
            {
                if (i == 0)
                {
                    shadows[i].RectTransform().anchoredPosition = character.eventDialogueShadowOffset;
                }
                else
                {
                    shadows[i].RectTransform().anchoredPosition =
                        new Vector2(-character.eventDialogueShadowOffset.x, character.eventDialogueShadowOffset.y);
                }
            }
        }

        private void OnYShadowOffsetChanged(int y)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.eventDialogueShadowOffset = new Vector2Int(character.eventDialogueShadowOffset.x, y);

            for (var i = 0; i < shadows.Count; i++)
            {
                if (i == 0)
                {
                    shadows[i].RectTransform().anchoredPosition = character.eventDialogueShadowOffset;
                }
                else
                {
                    shadows[i].RectTransform().anchoredPosition =
                        new Vector2(-character.eventDialogueShadowOffset.x, character.eventDialogueShadowOffset.y);
                }
            }
        }

        private void OnShadowChanged(int shadowIndex)
        {
            var character = Db.CharacterDatabase.GetCharacter(m_characterId);
            character.eventDialogueShadowType = shadows[0].animationAsset.animations[shadowIndex].name;

            for (var i = 0; i < shadows.Count; i++)
            {
                shadows[i].Play(character.eventDialogueShadowType);
            }
        }

        private void OnEnvironmentChanged(int environmentIndex)
        {
            var environmentAnimName = environment.animationAsset.animations[environmentIndex].name;
            environment.Play(environmentAnimName);
        }

        public async void SetCard(string cardId)
        {
            m_characterId = cardId;

            var character = Db.CharacterDatabase.GetCharacter(m_characterId);

            if (string.IsNullOrEmpty(character.eventDialogueShadowType))
            {
                character.eventDialogueShadowType = "medium";
            }

            for (var i = 0; i < shadows.Count; i++)
            {
                shadows[i].Play(character.eventDialogueShadowType);
            }

            Sprite cardSprite = null;

            if (!string.IsNullOrEmpty(character.portraitUrl))
            {
                var atlas = CardGenerationTool.Instance != null
                    ? CardGenerationTool.Instance.cardAtlas
                    : await AddressableReferenceLoader.Load<SpriteAtlas>("cards-atlas");

                cardSprite = atlas.GetSprite(character.portraitUrl);
            }

            for (var i = 0; i < portraitImages.Count; i++)
            {
                portraitImages[i].sprite = cardSprite;
                portraitImages[i].SetActive(portraitImages[i].sprite != null);
            }

            for (var i = 0; i < portraitImages.Count; i++)
            {
                if (i == 0)
                {
                    portraitImages[i].RectTransform().anchoredPosition = character.eventDialogueOffset;
                }
                else
                {
                    portraitImages[i].RectTransform().anchoredPosition =
                        new Vector2(-character.eventDialogueOffset.x, character.eventDialogueOffset.y);
                }
            }

            for (var i = 0; i < shadows.Count; i++)
            {
                if (i == 0)
                {
                    shadows[i].RectTransform().anchoredPosition = character.eventDialogueShadowOffset;
                }
                else
                {
                    shadows[i].RectTransform().anchoredPosition =
                        new Vector2(-character.eventDialogueShadowOffset.x, character.eventDialogueShadowOffset.y);
                }
            }

            var shadowAnim = shadows[0].animationAsset.animations
                .Find(anim => anim.name == character.eventDialogueShadowType);

            shadowTypeDropdown.SetValueWithoutNotify(shadows[0].animationAsset.animations.IndexOf(shadowAnim));
            flipX.SetIsOnWithoutNotify(character.rightFacing);

            xOffset.SetTextWithoutNotify(character.eventDialogueOffset.x.ToString());
            yOffset.SetTextWithoutNotify(character.eventDialogueOffset.y.ToString());

            xShadowOffset.SetTextWithoutNotify(character.eventDialogueShadowOffset.x.ToString());
            yShadowOffset.SetTextWithoutNotify(character.eventDialogueShadowOffset.y.ToString());


            OnFlipX(character.rightFacing);
        }
    }
}