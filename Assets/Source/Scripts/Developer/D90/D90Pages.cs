using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90Pages : MonoBehaviour
    {
        public Transform pageLayout;
        public bool displayPageNumberAsZeroIndexed;
        public Text pageNumber;

        public Button addButton;
        public Button deleteButton;

        public Button previousButton;
        public Button nextButton;

        private int _currentIndex;
        private List<GameObject> _pages;

        public int CurrentIndex => _currentIndex;

        public event Action OnAddButton;
        public event Action<int> OnDeleteButton;

        public event Action<int> OnPageChanged;

        private void Start()
        {
            if (previousButton) previousButton.onClick.AddListener(PreviousButton);
            if (nextButton) nextButton.onClick.AddListener(NextButton);

            if (addButton) addButton.onClick.AddListener(AddButton);
            if (deleteButton) deleteButton.onClick.AddListener(DeleteButton);
        }

        public void AddButton()
        {
            OnAddButton?.Invoke();
        }

        public void DeleteButton()
        {
            OnDeleteButton?.Invoke(_currentIndex);
            RemovePage(_currentIndex);
        }

        public void SetPageIndex(int index)
        {
            _currentIndex = index;
            GoToCurrentPage();
        }

        public void Clear()
        {
            _currentIndex = 0;
            _pages = new List<GameObject>();
            pageLayout.DestroyChildren();
        }

        private void Update()
        {
            if (_pages == null) return;

            if (previousButton) previousButton.interactable = _currentIndex > 0;
            if (nextButton) nextButton.interactable = _currentIndex < _pages.Count - 1;
            if (deleteButton) deleteButton.interactable = _currentIndex > -1;
            if (pageNumber)
            {
                if (displayPageNumberAsZeroIndexed) pageNumber.text = $"{_currentIndex}/{_pages.Count-1}";
                else pageNumber.text = $"{_currentIndex + 1}/{_pages.Count}";
            }
        }

        public void AddPage(GameObject page)
        {
            if (_pages == null) _pages = new List<GameObject>();

            _pages.Add(page);
            _currentIndex = _pages.Count - 1;

            page.transform.SetParent(pageLayout, false);
            page.RectTransform().anchoredPosition = Vector2.zero;

            GoToCurrentPage();
        }

        public void RemovePage(int index)
        {
            if (_pages == null) _pages = new List<GameObject>();

            if (_pages.Count > index)
            {
                _pages.RemoveAt(index);
                if (_currentIndex >= _pages.Count) _currentIndex = _pages.Count-1;
                GoToCurrentPage();
            }
        }

        private void PreviousButton()
        {
            _currentIndex--;
            OnPageChanged?.Invoke(_currentIndex);
            GoToCurrentPage();
        }

        private void NextButton()
        {
            _currentIndex++;
            OnPageChanged?.Invoke(_currentIndex);
            GoToCurrentPage();
        }

        private void GoToCurrentPage()
        {
            if (_pages == null) _pages = new List<GameObject>();

            for (var i = 0; i < _pages.Count; i++)
            {
                _pages[i].SetActive(i == _currentIndex);
            }
        }
    }
}