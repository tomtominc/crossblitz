using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.Vfx;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Developer.D90
{
    public class D90VfxPreview : MonoBehaviour
    {
        public VfxObjectBehaviour vfxObjectPrefab;
        public RectTransform worldCanvas;
        public RectTransform sourceCanvas;
        public List<RectTransform> targetCanvases;

        public Dropdown previewType;
        public Button play;
        public Button pause;

        private bool m_playing;
        private string m_vfxUid;
        private List<VfxObjectBehaviour> m_objects;

        private void Start()
        {
            m_objects = new List<VfxObjectBehaviour>();
        }

        public void RefreshMenu(string vfxUid)
        {
            m_vfxUid = vfxUid;
            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
        }

        public void Update()
        {
            if (string.IsNullOrEmpty(m_vfxUid))
            {
                return;
            }

            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
        }

        public void Play()
        {
            if (string.IsNullOrEmpty(m_vfxUid))
            {
                return;
            }

            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);

            for (var i = 0; i < vfx.objects.Count; i++)
            {
                var vfxObject = Instantiate(vfxObjectPrefab, worldCanvas, false);
                vfxObject.Play( vfx.objects[i] );

                vfxObject.OnFrameChanged += OnVfxObjectFrameChanged;
                vfxObject.OnComplete += OnVfxObjectAnimationLooped;
                vfxObject.OnLooped += OnVfxObjectAnimationCompleted;
                vfxObject.OnCustomEvent += OnVfxCustomEvent;
                vfxObject.OnCreateNewVfxObject += OnVfxCreateNewObject;

                m_objects.Add(vfxObject);
            }

            m_playing = true;
        }

        private void OnVfxObjectFrameChanged(int frame)
        {

        }

        private void OnVfxObjectAnimationLooped()
        {

        }

        private void OnVfxObjectAnimationCompleted()
        {

        }

        private void OnVfxCustomEvent(string customEventName, string customEventArgs)
        {

        }

        private void OnVfxCreateNewObject(string newObjectName)
        {
            if (string.IsNullOrEmpty(m_vfxUid))
            {
                return;
            }

            var vfx = Db.VfxDatabase.GetVfxData(m_vfxUid);
            var vfxObj = vfx.GetObject(newObjectName);

            var vfxObject = Instantiate(vfxObjectPrefab, worldCanvas, false);
            vfxObject.Play( vfxObj );

            vfxObject.OnFrameChanged += OnVfxObjectFrameChanged;
            vfxObject.OnComplete += OnVfxObjectAnimationLooped;
            vfxObject.OnLooped += OnVfxObjectAnimationCompleted;
            vfxObject.OnCustomEvent += OnVfxCustomEvent;
            vfxObject.OnCreateNewVfxObject += OnVfxCreateNewObject;

            m_objects.Add(vfxObject);
        }

        public void Pause()
        {

        }

        private void ClearMenu()
        {

        }
    }
}