using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Sirenix.Utilities;
using Component = UnityEngine.Component;

namespace CrossBlitz.Developer.D90
{
    public class D90PropertyGenerator : MonoBehaviour
    {
        public enum CustomEditor
        {
            NotSet,
            AbilityEditor
        }

        public CustomEditor customEditor;
        public string className;
        public bool createScript;
        [ShowIf("createScript")] public string scriptNamespace;
        [ShowIf("createScript")]  public string scriptName;
        public Transform windowLayout;
        public Transform propertiesLayoutWindowPrefab;
        public InputField inputFieldPrefab;
        public Toggle togglePrefab;
        public Dropdown dropdownPrefab;
        public D90ComboBox comboBoxPrefab;
        public D90ClassPropertyWindow classPropertyWindowPrefab;

        public D90DatabaseDropdown databaseDropdownPrefab;

        [Button]
        public void CreateProperties()
        {
            var classType = Type.GetType(className);
            var windowLayouts = new Dictionary<string, Transform>();
            var categoryCounts = new Dictionary<string, int>();
            var categoryCurrItem = new Dictionary<string, int>();


            var usings = "using System;" +
                         "\nusing System.Collections.Generic;" +
                         "\nusing CrossBlitz.Card.Vfx;" +
                         "\nusing System.Linq;" +
                         "\nusing CrossBlitz.Card;" +
                         "\nusing Sirenix.OdinInspector;" +
                         "\nusing Sirenix.Utilities;" +
                         "\nusing UnityEngine;" +
                         "\nusing UnityEngine.UI;";

            var script =
                $"{usings}\nnamespace {scriptNamespace}\n{{\n\tpublic class {scriptName} : MonoBehaviour\n\t{{";
            // each property fills this with:
            // 1. field declaration
            // 2. initialization
            // 3. event functions
            var propertyStrings = new List<string[]>();

            if (classType == null)
            {
                Debug.LogError($"Class {className} could not be found.");
                return;
            }

            var properties = classType.GetFields();

            for (var i = 0; i < properties.Length; i++)
            {
                var prop = properties[i];
                var attributes = prop.GetCustomAttributes(true);

                for (var j = 0; j < attributes.Length; j++)
                {
                    if (attributes[j] is FoldoutGroupAttribute foldoutGroupAttribute)
                    {
                        if (categoryCounts.ContainsKey(foldoutGroupAttribute.GroupName))
                        {
                            categoryCounts[foldoutGroupAttribute.GroupName]++;
                        }
                        else
                        {
                            categoryCounts.Add(foldoutGroupAttribute.GroupName, 1);
                        }
                    }
                }
            }

            for (var i = 0; i < properties.Length; i++)
            {
                var prop = properties[i];
                var attributes = prop.GetCustomAttributes(true);

                Transform window = null;

                for (var j = 0; j < attributes.Length; j++)
                {
                    if (attributes[j] is FoldoutGroupAttribute foldoutGroupAttribute)
                    {
                        if (windowLayouts.ContainsKey(foldoutGroupAttribute.GroupName))
                        {
                            window = windowLayouts[foldoutGroupAttribute.GroupName];
                            categoryCurrItem[foldoutGroupAttribute.GroupName]++;
                        }
                        else
                        {
                            var childWindow = windowLayout.Find(foldoutGroupAttribute.GroupName);

                            if (childWindow)
                            {
                                window = childWindow;
                            }
                            else
                            {
                                window = Instantiate(propertiesLayoutWindowPrefab, windowLayout, false);
                            }

                            window.name = foldoutGroupAttribute.GroupName;
                            windowLayouts.Add(foldoutGroupAttribute.GroupName, window);
                            categoryCurrItem.Add(foldoutGroupAttribute.GroupName, 0);
                        }
                    }
                }

                if (window == null)
                {
                    Debug.LogError($"No Foldout Group detected, please add the attribute to draw this property: {className}");
                    continue;
                }

                var windowRect = window.RectTransform();
                var columns = windowRect.childCount;
                if (columns == 0)
                {
                    propertyStrings.Add(AddFields(prop, windowRect, window.name, string.Empty));
                }
                else
                {
                    var rows = categoryCounts[window.name] / columns;
                    rows = rows < 3 ? 3 : rows;
                    var curr = (int)(categoryCurrItem[window.name] / (float)rows);

                    Debug.Log(
                        $"({columns},{rows}), {categoryCurrItem[window.name]} / {rows} = {curr} {categoryCurrItem[window.name]}/{categoryCounts[window.name]} ");
                    var childRect = windowRect.GetChild(curr).RectTransform();

                    propertyStrings.Add(AddFields(prop, childRect, window.name, string.Empty));
                }
            }

            for (var i = 0; i < propertyStrings.Count; i++)
            {
                script += propertyStrings[i][0];
            }

            switch (customEditor)
            {
                case CustomEditor.AbilityEditor:
                    script += "\n\t\tprivate List<Ability> m_abilities;" +
                              "\n\t\tprivate int m_index;";
                    script += $"\n\t\tpublic void Init(List<Ability> abilities, int index)" +
                              $"\n\t\t{{" +
                              $"\n\t\t\tm_abilities = abilities;" +
                              $"\n\t\t\tm_index = index;" ;
                    break;
                default: script += $"\n\t\tpublic void Init(/* todo */)\n\t\t{{";
                    break;
            }


            for (var i = 0; i < propertyStrings.Count; i++)
            {
                script += propertyStrings[i][1] + "\n";
            }

            script += "\n\t\t}";

            for (var i = 0; i < propertyStrings.Count; i++)
            {
                script += propertyStrings[i][2] + "\n";
            }

            script += "\n\t}\n}";

            var desktopPath = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            var writePath = $"{desktopPath}/{scriptName}.cs";

            File.WriteAllText(writePath, script);
            //Debug.Log(script);
        }

        private T FindOrInstantiate<T>(T original, Transform parent, string originalName) where T : Component
        {
            var copyTransform = parent.Find(originalName);

            try
            {
                var copy = copyTransform == null
                    ? Instantiate(original, parent, false)
                    : copyTransform.GetComponent<T>();
                ;

                copy.name = originalName;

                return copy;
            }
            catch (Exception e)
            {
                Debug.LogError($"Error! {originalName} {e} {typeof(T)}");
            }

            Debug.Log(originalName);

            return null;

        }

        private string[] AddFields(FieldInfo prop, RectTransform windowRect, string groupName, string prependProperty)
        {
            var scr = new string[3];

            if (prop.FieldType.IsEnum)
            {
                if (prop.FieldType.GetCustomAttributes(typeof(FlagsAttribute), false).Length > 0)
                {
                    CreateComboBox(prop, windowRect, groupName, prependProperty, scr);
                }
                else
                {
                    CreateDropdown(prop, windowRect, groupName, prependProperty, scr);
                }

            }
            else if (prop.FieldType == typeof(string))
            {
                if (prop.GetCustomAttributes(typeof(ValueDropdownAttribute), false).Length > 0)
                {
                    CreateDropdown(prop, windowRect, groupName, prependProperty, scr);
                }
                else
                {
                    CreateTextField(prop, windowRect, groupName, prependProperty, scr);
                }
            }
            else if (prop.FieldType == typeof(int))
            {
                CreateTextField(prop, windowRect, groupName, prependProperty, scr);
            }
            else if (prop.FieldType == typeof(bool))
            {
                CreateToggle(prop, windowRect, groupName, prependProperty, scr);
            }
            else if (prop.FieldType.IsClass)
            {
                var classPropertyWindow = FindOrInstantiate(classPropertyWindowPrefab, windowRect, prop.GetPrettyName());
                classPropertyWindow.titleLabel.text = $"{prop.GetPrettyName()}";
                classPropertyWindow.buttonLabel.text =$"Open {prop.GetPrettyName()}";

                var properties = prop.FieldType.GetFields();
                var fieldInfos = new List<string[]>();

                for (var i = 0; i < properties.Length; i++)
                {
                    var childProp = properties[i];

                    if (childProp.FieldType == prop.FieldType)
                    {
                        Debug.LogError($"Cannot add recursive properties, it will cause an infinite loop: {prop.FieldType}");
                        continue;
                    }

                    fieldInfos.Add(AddFields(childProp, classPropertyWindow.window.RectTransform(), groupName, prop.Name));
                }

                for (var i = 0; i < fieldInfos.Count; i++)
                {
                    scr[0] += fieldInfos[i][0];
                    scr[1] += fieldInfos[i][1] + "\n";
                    scr[2] += fieldInfos[i][2] + "\n";
                }
            }
            else
            {
                Debug.LogError($"Filed Type {prop.FieldType} could not be generated. Please add this type.");
            }

            return scr;
        }

        private void CreateTextField(FieldInfo prop, RectTransform windowRect, string groupName, string prependProperty,
            string[] scr)
        {
            var isInt = prop.FieldType == typeof(int);
            var inputField = FindOrInstantiate(inputFieldPrefab, windowRect, prop.GetPrettyName());
            inputField.contentType = isInt ? InputField.ContentType.IntegerNumber : InputField.ContentType.Standard;
            inputField.RectTransform().GetChild(2).GetComponent<Text>().text = prop.GetPrettyName();

            string propAddress;
            var inputFieldPropertyName = $"{prop.Name}InputField";

            if (prop.Name == "class")
            {
                propAddress = $"@{prop.Name}";
            }
            else
            {
                propAddress = $"{prop.Name}";
            }

            if (!string.IsNullOrEmpty(prependProperty))
            {
                if (prop.Name == "class")
                {
                    propAddress = $"{prependProperty}.@{prop.Name}";
                }
                else
                {
                    propAddress = $"{prependProperty}.{prop.Name}";
                }

                inputFieldPropertyName = $"{prependProperty}{prop.Name.ToCamelCase()}InputField";
            }

            scr[0] =
                $"\n\t\t[BoxGroup(\"{groupName}\")] public InputField {inputFieldPropertyName};";

            scr[1] =
                $"\n\t\t\t{inputFieldPropertyName}.onValueChanged.RemoveAllListeners();" +
                $"\n\t\t\t{inputFieldPropertyName}.onValueChanged.AddListener(On{inputFieldPropertyName.ToCamelCase()}Changed);";

            switch (customEditor)
            {
                case CustomEditor.AbilityEditor:

                    scr[1] += $"\n\t\t\t{inputFieldPropertyName}.SetTextWithoutNotify(m_abilities[m_index].{propAddress}.ToString());";

                    if (isInt)
                    {
                        scr[2] =
                            $"\n\t\tprivate void On{inputFieldPropertyName.ToCamelCase()}Changed(string input)" +
                            $"\n\t\t{{" +
                            $"\n\t\t\tif (int.TryParse(input, out var value))" +
                            $"\n\t\t\t{{" +
                            $"\n\t\t\t\tm_abilities[m_index].{propAddress} = value;" +
                            $"\n\t\t\t\tInit(m_abilities, m_index);" +
                            $"\n\t\t\t}}" +
                            $"\n\t\t}}";
                    }
                    else
                    {
                        scr[2] =
                            $"\n\t\tprivate void On{inputFieldPropertyName.ToCamelCase()}Changed(string input)" +
                            $"\n\t\t{{" +
                            $"\n\t\t\tm_abilities[m_index].{propAddress} = input;" +
                            $"\n\t\t\tInit(m_abilities, m_index);" +
                            $"\n\t\t}}";
                    }
                    break;
                default:
                    scr[1] += $"\n\t\t\t{inputFieldPropertyName}.SetTextWithoutNotify(/* todo */);";
                    scr[2] =
                        $"\n\t\tprivate void On{inputFieldPropertyName.ToCamelCase()}Changed(string input)" +
                        $"\n\t\t{{" +
                        $"\n\t\t\t//todo: add code" +
                        $"\n\t\t}}";
                    break;
            }


        }

        private void CreateToggle(FieldInfo prop, RectTransform windowRect, string groupName, string prependProperty,
            string[] scr)
        {
            var toggle = FindOrInstantiate(togglePrefab, windowRect, prop.GetPrettyName());
            toggle.RectTransform().GetChild(1).GetComponent<Text>().text = prop.GetPrettyName();

            string propAddress;
            var inputFieldPropertyName = $"{prop.Name}Toggle";

            if (prop.Name == "class")
            {
                propAddress = $"@{prop.Name}";
            }
            else
            {
                propAddress = $"{prop.Name}";
            }

            if (!string.IsNullOrEmpty(prependProperty))
            {
                if (prop.Name == "class")
                {
                    propAddress = $"{prependProperty}.@{prop.Name}";
                }
                else
                {
                    propAddress = $"{prependProperty}.{prop.Name}";
                }

                inputFieldPropertyName = $"{prependProperty}{prop.Name.ToCamelCase()}Toggle";
            }

            scr[0] =
                $"\n\t\t[BoxGroup(\"{groupName}\")] public Toggle {inputFieldPropertyName};";

            scr[1] =
                $"\n\t\t\t{inputFieldPropertyName}.onValueChanged.RemoveAllListeners();" +
                $"\n\t\t\t{inputFieldPropertyName}.onValueChanged.AddListener(On{inputFieldPropertyName.ToCamelCase()}Changed);";

            switch (customEditor)
            {
                case CustomEditor.AbilityEditor:
                    scr[1] += $"\n\t\t\t{inputFieldPropertyName}.SetIsOnWithoutNotify(m_abilities[m_index].{propAddress});";
                    scr[2] =
                        $"\n\t\tprivate void On{inputFieldPropertyName.ToCamelCase()}Changed(bool isOn)" +
                        $"\n\t\t{{" +
                        $"\n\t\t\tm_abilities[m_index].{propAddress} = isOn;" +
                        $"\n\t\t\tInit(m_abilities, m_index);" +
                        $"\n\t\t}}";
                    break;
                default:
                    scr[1] += $"\n\t\t\t{inputFieldPropertyName}.SetIsOnWithoutNotify(/* todo */);";
                    scr[2] =
                        $"\n\t\tprivate void On{inputFieldPropertyName.ToCamelCase()}Changed(bool isOn)" +
                        $"\n\t\t{{" +
                        $"\n\t\t\t//todo: add code" +
                        $"\n\t\t}}";
                    break;
            }
        }

        private void CreateComboBox(FieldInfo prop, RectTransform windowRect, string groupName, string prependProperty,
            string[] scr)
        {
            var comboBox = FindOrInstantiate(comboBoxPrefab, windowRect, prop.GetPrettyName());
            comboBox.title.text = prop.GetPrettyName();

            string propAddress;
            var comboboxPropertyName = $"{prop.Name}ComboBox";

            if (prop.Name == "class")
            {
                propAddress = $"@{prop.Name}";
            }
            else
            {
                propAddress = $"{prop.Name}";
            }

            if (!string.IsNullOrEmpty(prependProperty))
            {
                if (prop.Name == "class")
                {
                    propAddress = $"{prependProperty}.@{prop.Name}";
                }
                else
                {
                    propAddress = $"{prependProperty}.{prop.Name}";
                }
                comboboxPropertyName = $"{prependProperty}{prop.Name.ToCamelCase()}ComboBox";
            }

            scr[0] =
                $"\n\t\t[BoxGroup(\"{groupName}\")] public D90ComboBox {comboboxPropertyName};";

            scr[1] =
                $"\n\t\t\t{comboboxPropertyName}.OnAddedFlag -= On{comboboxPropertyName.ToCamelCase()}AddedFlag;" +
                $"\n\t\t\t{comboboxPropertyName}.OnAddedFlag += On{comboboxPropertyName.ToCamelCase()}AddedFlag;" +
                $"\n\t\t\t{comboboxPropertyName}.OnRemovedFlag -= On{comboboxPropertyName.ToCamelCase()}RemovedFlag;" +
                $"\n\t\t\t{comboboxPropertyName}.OnRemovedFlag += On{comboboxPropertyName.ToCamelCase()}RemovedFlag;";


            switch (customEditor)
            {
                case CustomEditor.AbilityEditor:
                    scr[1] += $"\n\t\t\t{comboboxPropertyName}.SetValueWithoutNotify((int)m_abilities[m_index].{propAddress}, " +
                             $"Enum.GetValues(typeof({prop.FormatFullTypeName()})).Convert(e => new D90ComboBox.OptionValue " +
                             $"{{ name = e.ToString(), value = (int)e }}).ToList());";

                    scr[2] =
                        $"\n\t\tprivate void On{comboboxPropertyName.ToCamelCase()}AddedFlag(int flag)" +
                        $"\n\t\t{{" +
                        $"\n\t\t\tm_abilities[m_index].{propAddress} |= ({prop.FormatFullTypeName()})flag;" +
                        $"\n\t\t\tInit(m_abilities, m_index);" +
                        $"\n\t\t}}" +
                        $"\n\t\tprivate void On{comboboxPropertyName.ToCamelCase()}RemovedFlag(int flag)" +
                        $"\n\t\t{{" +
                        $"\n\t\t\tm_abilities[m_index].{propAddress} &= ~({prop.FormatFullTypeName()})flag;" +
                        $"\n\t\t\tInit(m_abilities, m_index);" +
                        $"\n\t\t}}";

                    break;
                default:
                    scr[1] += $"\n\t\t\t{comboboxPropertyName}.SetValueWithoutNotify(/* todo */, " +
                             $"Enum.GetValues(typeof({prop.FormatFullTypeName()})).Convert(e => new D90ComboBox.OptionValue " +
                             $"{{ name = e.ToString(), value = (int)e }}).ToList());";

                    scr[2] =
                        $"\n\t\tprivate void On{comboboxPropertyName.ToCamelCase()}AddedFlag(int flag)" +
                        $"\n\t\t{{" +
                        $"\n\t\t\t//todo: add code" +
                        $"\n\t\t}}" +
                        $"\n\t\tprivate void On{comboboxPropertyName.ToCamelCase()}RemovedFlag(int flag)" +
                        $"\n\t\t{{" +
                        $"\n\t\t\t//todo: add code" +
                        $"\n\t\t}}";
                    break;
            }
        }

        private void CreateDropdown(FieldInfo prop, RectTransform windowRect, string groupName, string prependProperty,
            string[] scr)
        {
            var dropdown = FindOrInstantiate(dropdownPrefab, windowRect, prop.GetPrettyName());
            if (!dropdown) return;

            dropdown.RectTransform().GetChild(2).GetComponent<Text>().text = prop.GetPrettyName();

            string propAddress;
            var dropdownPropertyName = $"{prop.Name}Dropdown";

            if (prop.Name == "class")
            {
                propAddress = $"@{prop.Name}";
            }
            else
            {
                propAddress = $"{prop.Name}";
            }

            if (!string.IsNullOrEmpty(prependProperty))
            {
                if (prop.Name == "class")
                {
                    propAddress = $"{prependProperty}.@{prop.Name}";
                }
                else
                {
                    propAddress = $"{prependProperty}.{prop.Name}";
                }
                dropdownPropertyName = $"{prependProperty}{prop.Name.ToCamelCase()}Dropdown";
            }

            scr[0] =
                $"\n\t\t[BoxGroup(\"{groupName}\")] public Dropdown {dropdownPropertyName};";

            scr[1] =
                $"\n\t\t\t{dropdownPropertyName}.ClearOptions();" +
                $"\n\t\t\t{dropdownPropertyName}.AddOptions(Enum.GetNames(typeof({prop.FormatFullTypeName()})).ToList());" +
                $"\n\t\t\t{dropdownPropertyName}.onValueChanged.RemoveAllListeners();" +
                $"\n\t\t\t{dropdownPropertyName}.onValueChanged.AddListener(On{dropdownPropertyName.ToCamelCase()}Changed);";

            switch (customEditor)
            {
                case CustomEditor.AbilityEditor:
                    scr[1] += $"\n\t\t\t{dropdownPropertyName}.SetValueWithoutNotify((int)m_abilities[m_index].{propAddress});";

                    if (prop.Name == "customEffectComponent")
                    {
                        scr[1] =
                            $"\n\t\t\tvar q = typeof(GameVfxComponent).Assembly.GetTypes()" +
                            $"\n\t\t\t\t.Where(x => !x.IsAbstract)" +
                            $"\n\t\t\t\t.Where(x => !x.IsGenericTypeDefinition)" +
                            $"\n\t\t\t\t.Where(x => typeof(GameVfxComponent).IsAssignableFrom(x))" +
                            $"\n\t\t\t\t.Convert(t => ((Type)t).AssemblyQualifiedName)" +
                            $"\n\t\t\t\t.ToList();";

                        scr[1] +=
                            $"\n\t\t\t{dropdownPropertyName}.ClearOptions();" +
                            $"\n\t\t\t{dropdownPropertyName}.AddOptions(q);" +
                            $"\n\t\t\t{dropdownPropertyName}.SetValueWithoutNotify(q.IndexOf( m_abilities[m_index].{propAddress}));" +
                            $"\n\t\t\t{dropdownPropertyName}.onValueChanged.RemoveAllListeners();" +
                            $"\n\t\t\t{dropdownPropertyName}.onValueChanged.AddListener( On{dropdownPropertyName.ToCamelCase()}Changed );";

                        scr[2] =
                            $"\n\t\tprivate void On{dropdownPropertyName.ToCamelCase()}Changed(int index)" +
                            $"\n\t\t{{" +
                            $"\n\t\t\tm_abilities[m_index].{propAddress} = {dropdownPropertyName}.options[index].text;" +
                            $"\n\t\t\tInit(m_abilities, m_index);" +
                            $"\n\t\t}}";
                    }
                    else
                    {
                        scr[2] =
                            $"\n\t\tprivate void On{dropdownPropertyName.ToCamelCase()}Changed(int index)" +
                            $"\n\t\t{{" +
                            $"\n\t\t\tm_abilities[m_index].{propAddress} = ({prop.FormatFullTypeName()})index;" +
                            $"\n\t\t\tInit(m_abilities, m_index);" +
                            $"\n\t\t}}";
                    }

                    break;
                default:
                    scr[1] += $"\n\t\t\t{dropdownPropertyName}.SetValueWithoutNotify(/* todo */);";

                    scr[2] =
                        $"\n\t\tprivate void On{dropdownPropertyName.ToCamelCase()}Changed(int index)" +
                        $"\n\t\t{{" +
                        $"\n\t\t\t//todo: add code" +
                        $"\n\t\t}}";
                    break;
            }
        }
    }
}