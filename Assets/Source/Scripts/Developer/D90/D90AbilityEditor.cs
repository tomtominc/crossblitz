using System;
using System.Collections.Generic;
using CrossBlitz.Card.Vfx;
using System.Linq;
using CrossBlitz.Card;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.UI;
namespace CrossBlitz.Developer.D90
{
	public class D90AbilityEditor : MonoBehaviour
	{
		[BoxGroup("Ability Base Values")] public Dropdown keywordDropdown;
		[BoxGroup("Ability Base Values")] public D90ComboBox triggerTypeComboBox;
		[BoxGroup("Ability Base Values")] public D90ComboBox triggerEvaluateConditionComboBox;
		[BoxGroup("Ability Base Values")] public D90ComboBox triggerConditionsComboBox;
		[BoxGroup("Ability Base Values")] public D90ComboBox targetConditionsComboBox;
		[BoxGroup("Ability Base Values")] public Dropdown FilterValuesFactionDropdown;
		[BoxGroup("Ability Base Values")] public Dropdown FilterValuesClassDropdown;
		[BoxGroup("Ability Base Values")] public D90ComboBox FilterValuesStatusEffectComboBox;
		[BoxGroup("Ability Base Values")] public InputField FilterValuesMaxCountInputField;
		[BoxGroup("Ability Base Values")] public Dropdown triggerValuesFactionDropdown;
		[BoxGroup("Ability Base Values")] public Dropdown triggerValuesClassDropdown;
		[BoxGroup("Ability Base Values")] public D90ComboBox triggerValuesStatusEffectComboBox;
		[BoxGroup("Ability Base Values")] public InputField triggerValuesMaxCountInputField;
		[BoxGroup("Ability Properties")] public Toggle usesCustomEffectToggle;
		[BoxGroup("Ability Properties")] public Dropdown customEffectComponentDropdown;
		[BoxGroup("Ability Base Values")] public Toggle usesGenericIconToggle;
		[BoxGroup("Target Values")] public Dropdown triggerTargetDropdown;
		[BoxGroup("Target Values")] public Dropdown effectTargetDropdown;
		[BoxGroup("Target Values")] public Dropdown modifyEquationDropdown;
		[BoxGroup("Ability Properties")] public Toggle damageBasedOnConditionsToggle;
		[BoxGroup("Ability Properties")] public Dropdown damageAmountConditionConditionDropdown;
		[BoxGroup("Ability Properties")] public Toggle damageAmountConditionAppliesOnlyToYourFieldToggle;
		[BoxGroup("Ability Properties")] public Dropdown damageAmountConditionFactionDropdown;
		[BoxGroup("Ability Properties")] public Dropdown damageAmountConditionClassDropdown;
		[BoxGroup("Ability Properties")] public InputField damageValueInputField;
		[BoxGroup("Ability Properties")] public Toggle damageRandomlySplitToggle;
		[BoxGroup("Ability Properties")] public Toggle applyDamageInEffectToggle;
		[BoxGroup("Ability Properties")] public Toggle targetCountBasedOnConditionsToggle;
		[BoxGroup("Ability Properties")] public Toggle repeatDamageBasedOnConditionsToggle;
		[BoxGroup("Ability Properties")] public InputField targetCountInputField;
		[BoxGroup("Ability Properties")] public Dropdown targetCountConditionConditionDropdown;
		[BoxGroup("Ability Properties")] public Toggle targetCountConditionAppliesOnlyToYourFieldToggle;
		[BoxGroup("Ability Properties")] public Dropdown targetCountConditionFactionDropdown;
		[BoxGroup("Ability Properties")] public Dropdown targetCountConditionClassDropdown;
		[BoxGroup("Ability Properties")] public Dropdown repeatCountConditionConditionDropdown;
		[BoxGroup("Ability Properties")] public Toggle repeatCountConditionAppliesOnlyToYourFieldToggle;
		[BoxGroup("Ability Properties")] public Dropdown repeatCountConditionFactionDropdown;
		[BoxGroup("Ability Properties")] public Dropdown repeatCountConditionClassDropdown;
		[BoxGroup("Ability Properties")] public Dropdown auraDataAuraEffectDropdown;
		[BoxGroup("Ability Properties")] public D90ComboBox auraDataTargetsComboBox;
		[BoxGroup("Ability Properties")] public InputField auraDataModifyDrawAmountInputField;
		[BoxGroup("Ability Properties")] public Toggle auraDataRemoveWhenMinionDiesToggle;
		[BoxGroup("Ability Properties")] public InputField manaModifierInputField;
		[BoxGroup("Ability Properties")] public InputField armorModifierInputField;
		[BoxGroup("Ability Properties")] public InputField powerModifierInputField;
		[BoxGroup("Ability Properties")] public Dropdown modifyStatsConditionConditionDropdown;
		[BoxGroup("Ability Properties")] public Dropdown modifyStatsConditionFactionDropdown;
		[BoxGroup("Ability Properties")] public Dropdown modifyStatsConditionClassDropdown;
		[BoxGroup("Ability Properties")] public InputField healthModifierInputField;
		[BoxGroup("Ability Properties")] public InputField healAmountInputField;
		[BoxGroup("Ability Properties")] public InputField multiAttackCountInputField;
		[BoxGroup("Ability Properties")] public InputField armorBodyAmountInputField;
		[BoxGroup("Ability Properties")] public InputField maxDamageTakenDuringHitInputField;
		[BoxGroup("Ability Properties")] public InputField drawCountInputField;
		[BoxGroup("Ability Properties")] public Dropdown drawConditionConditionDropdown;
		[BoxGroup("Ability Properties")] public Dropdown drawConditionFactionDropdown;
		[BoxGroup("Ability Properties")] public Dropdown drawConditionClassDropdown;
		[BoxGroup("Ability Properties")] public InputField summonMinionDataCountInputField;
		[BoxGroup("Ability Properties")] public D90ComboBox summonMinionDataPlacementComboBox;
		[BoxGroup("Ability Properties")] public Dropdown summonMinionDataSummonVisualEffectDropdown;
		[BoxGroup("Ability Properties")] public Toggle cardFilterIsRandomToggle;
		[BoxGroup("Ability Properties")] public Toggle cardFilterDrawCountBasedOnConditionsToggle;
		[BoxGroup("Ability Properties")] public Dropdown cardFilterDrawCountConditionDropdown;
		[BoxGroup("Ability Properties")] public Toggle cardFilterGrantedToOpponentToggle;
		[BoxGroup("Ability Properties")] public Toggle cardFilterGrantACopyFromLocationToggle;
		[BoxGroup("Ability Properties")] public D90ComboBox cardFilterCopyFromLocationComboBox;
		[BoxGroup("Ability Properties")] public Toggle cardFilterFromOpponentsFactionToggle;
		[BoxGroup("Ability Properties")] public Dropdown cardFilterFromClassDropdown;
		[BoxGroup("Ability Properties")] public Dropdown cardFilterFromTypeDropdown;
		[BoxGroup("Ability Properties")] public Dropdown cardFilterFromRarityDropdown;
		[BoxGroup("Ability Properties")] public Toggle cardFilterLimitToSameFactionToggle;
		[BoxGroup("Ability Properties")] public Toggle cardFilterOverrideCopiedCardStatsToggle;
		[BoxGroup("Ability Properties")] public InputField cardFilterCostOverrideInputField;
		[BoxGroup("Ability Properties")] public InputField cardFilterPowerOverrideInputField;
		[BoxGroup("Ability Properties")] public InputField cardFilterHealthOverrideInputField;
		[BoxGroup("Ability Properties")] public Toggle removeAllCardsToggle;
		[BoxGroup("Ability Properties")] public InputField amountOfCardsToRemoveInputField;
		[BoxGroup("Ability Properties")] public D90ComboBox triggeredAbilityComboBox;
		[BoxGroup("Conditional Properties")] public Toggle hasConditionalAbilityToggle;
		private List<Ability> m_abilities;
		private int m_index;
		public void Init(List<Ability> abilities, int index)
		{
			m_abilities = abilities;
			m_index = index;
			// keywordDropdown.ClearOptions();
			// keywordDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.AbilityKeyword)).ToList());
			// keywordDropdown.onValueChanged.RemoveAllListeners();
			// keywordDropdown.onValueChanged.AddListener(OnKeywordDropdownChanged);
			// keywordDropdown.SetValueWithoutNotify((int)m_abilities[m_index].keyword);
			//
			// triggerTypeComboBox.OnAddedFlag -= OnTriggerTypeComboBoxAddedFlag;
			// triggerTypeComboBox.OnAddedFlag += OnTriggerTypeComboBoxAddedFlag;
			// triggerTypeComboBox.OnRemovedFlag -= OnTriggerTypeComboBoxRemovedFlag;
			// triggerTypeComboBox.OnRemovedFlag += OnTriggerTypeComboBoxRemovedFlag;
			// triggerTypeComboBox.SetValueWithoutNotify((int)m_abilities[m_index].triggerType, Enum.GetValues(typeof(CrossBlitz.Card.TriggerType)).Convert(e => new D90ComboBox.OptionValue { name = e.ToString(), value = (int)e }).ToList());
			//
			// triggerEvaluateConditionComboBox.OnAddedFlag -= OnTriggerEvaluateConditionComboBoxAddedFlag;
			// triggerEvaluateConditionComboBox.OnAddedFlag += OnTriggerEvaluateConditionComboBoxAddedFlag;
			// triggerEvaluateConditionComboBox.OnRemovedFlag -= OnTriggerEvaluateConditionComboBoxRemovedFlag;
			// triggerEvaluateConditionComboBox.OnRemovedFlag += OnTriggerEvaluateConditionComboBoxRemovedFlag;
			// triggerEvaluateConditionComboBox.SetValueWithoutNotify((int)m_abilities[m_index].triggerEvaluateCondition, Enum.GetValues(typeof(CrossBlitz.Card.TriggerEvaluateCondition)).Convert(e => new D90ComboBox.OptionValue { name = e.ToString(), value = (int)e }).ToList());
			//
			// triggerConditionsComboBox.OnAddedFlag -= OnTriggerConditionsComboBoxAddedFlag;
			// triggerConditionsComboBox.OnAddedFlag += OnTriggerConditionsComboBoxAddedFlag;
			// triggerConditionsComboBox.OnRemovedFlag -= OnTriggerConditionsComboBoxRemovedFlag;
			// triggerConditionsComboBox.OnRemovedFlag += OnTriggerConditionsComboBoxRemovedFlag;
			// triggerConditionsComboBox.SetValueWithoutNotify((int)m_abilities[m_index].triggerConditions, Enum.GetValues(typeof(CrossBlitz.Card.FilterCondition)).Convert(e => new D90ComboBox.OptionValue { name = e.ToString(), value = (int)e }).ToList());
			//
			// targetConditionsComboBox.OnAddedFlag -= OnTargetConditionsComboBoxAddedFlag;
			// targetConditionsComboBox.OnAddedFlag += OnTargetConditionsComboBoxAddedFlag;
			// targetConditionsComboBox.OnRemovedFlag -= OnTargetConditionsComboBoxRemovedFlag;
			// targetConditionsComboBox.OnRemovedFlag += OnTargetConditionsComboBoxRemovedFlag;
			// targetConditionsComboBox.SetValueWithoutNotify((int)m_abilities[m_index].targetConditions, Enum.GetValues(typeof(CrossBlitz.Card.FilterCondition)).Convert(e => new D90ComboBox.OptionValue { name = e.ToString(), value = (int)e }).ToList());
			//
			// FilterValuesFactionDropdown.ClearOptions();
			// FilterValuesFactionDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Faction)).ToList());
			// FilterValuesFactionDropdown.onValueChanged.RemoveAllListeners();
			// FilterValuesFactionDropdown.onValueChanged.AddListener(OnFilterValuesFactionDropdownChanged);
			// FilterValuesFactionDropdown.SetValueWithoutNotify((int)m_abilities[m_index].filterValues.faction);
			//
			// FilterValuesClassDropdown.ClearOptions();
			// FilterValuesClassDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Class)).ToList());
			// FilterValuesClassDropdown.onValueChanged.RemoveAllListeners();
			// FilterValuesClassDropdown.onValueChanged.AddListener(OnFilterValuesClassDropdownChanged);
			// FilterValuesClassDropdown.SetValueWithoutNotify((int)m_abilities[m_index].filterValues.@class);
			//
			// FilterValuesStatusEffectComboBox.OnAddedFlag -= OnFilterValuesStatusEffectComboBoxAddedFlag;
			// FilterValuesStatusEffectComboBox.OnAddedFlag += OnFilterValuesStatusEffectComboBoxAddedFlag;
			// FilterValuesStatusEffectComboBox.OnRemovedFlag -= OnFilterValuesStatusEffectComboBoxRemovedFlag;
			// FilterValuesStatusEffectComboBox.OnRemovedFlag += OnFilterValuesStatusEffectComboBoxRemovedFlag;
			// FilterValuesStatusEffectComboBox.SetValueWithoutNotify((int)m_abilities[m_index].filterValues.statusEffect, Enum.GetValues(typeof(CrossBlitz.Card.StatusEffect)).Convert(e => new D90ComboBox.OptionValue { name = e.ToString(), value = (int)e }).ToList());
			//
			// FilterValuesMaxCountInputField.onValueChanged.RemoveAllListeners();
			// FilterValuesMaxCountInputField.onValueChanged.AddListener(OnFilterValuesMaxCountInputFieldChanged);
			// FilterValuesMaxCountInputField.SetTextWithoutNotify(m_abilities[m_index].filterValues.maxCount.ToString());
			//
			//
			// triggerValuesFactionDropdown.ClearOptions();
			// triggerValuesFactionDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Faction)).ToList());
			// triggerValuesFactionDropdown.onValueChanged.RemoveAllListeners();
			// triggerValuesFactionDropdown.onValueChanged.AddListener(OnTriggerValuesFactionDropdownChanged);
			// triggerValuesFactionDropdown.SetValueWithoutNotify((int)m_abilities[m_index].triggerValues.faction);
			//
			// triggerValuesClassDropdown.ClearOptions();
			// triggerValuesClassDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Class)).ToList());
			// triggerValuesClassDropdown.onValueChanged.RemoveAllListeners();
			// triggerValuesClassDropdown.onValueChanged.AddListener(OnTriggerValuesClassDropdownChanged);
			// triggerValuesClassDropdown.SetValueWithoutNotify((int)m_abilities[m_index].triggerValues.@class);
			//
			// triggerValuesStatusEffectComboBox.OnAddedFlag -= OnTriggerValuesStatusEffectComboBoxAddedFlag;
			// triggerValuesStatusEffectComboBox.OnAddedFlag += OnTriggerValuesStatusEffectComboBoxAddedFlag;
			// triggerValuesStatusEffectComboBox.OnRemovedFlag -= OnTriggerValuesStatusEffectComboBoxRemovedFlag;
			// triggerValuesStatusEffectComboBox.OnRemovedFlag += OnTriggerValuesStatusEffectComboBoxRemovedFlag;
			// triggerValuesStatusEffectComboBox.SetValueWithoutNotify((int)m_abilities[m_index].triggerValues.statusEffect, Enum.GetValues(typeof(CrossBlitz.Card.StatusEffect)).Convert(e => new D90ComboBox.OptionValue { name = e.ToString(), value = (int)e }).ToList());
			//
			// triggerValuesMaxCountInputField.onValueChanged.RemoveAllListeners();
			// triggerValuesMaxCountInputField.onValueChanged.AddListener(OnTriggerValuesMaxCountInputFieldChanged);
			// triggerValuesMaxCountInputField.SetTextWithoutNotify(m_abilities[m_index].triggerValues.maxCount.ToString());
			//
			//
			// usesCustomEffectToggle.onValueChanged.RemoveAllListeners();
			// usesCustomEffectToggle.onValueChanged.AddListener(OnUsesCustomEffectToggleChanged);
			// usesCustomEffectToggle.SetIsOnWithoutNotify(m_abilities[m_index].usesCustomEffect);
			//
			// var q = typeof(GameVfxComponent).Assembly.GetTypes()
			// 	.Where(x => !x.IsAbstract)
			// 	.Where(x => !x.IsGenericTypeDefinition)
			// 	.Where(x => typeof(GameVfxComponent).IsAssignableFrom(x))
			// 	.Convert(t => ((Type)t).AssemblyQualifiedName)
			// 	.ToList();
			// customEffectComponentDropdown.ClearOptions();
			// customEffectComponentDropdown.AddOptions(q);
			// customEffectComponentDropdown.SetValueWithoutNotify(q.IndexOf( m_abilities[m_index].customEffectComponent));
			// customEffectComponentDropdown.onValueChanged.RemoveAllListeners();
			// customEffectComponentDropdown.onValueChanged.AddListener( OnCustomEffectComponentDropdownChanged );
			//
			// usesGenericIconToggle.onValueChanged.RemoveAllListeners();
			// usesGenericIconToggle.onValueChanged.AddListener(OnUsesGenericIconToggleChanged);
			// usesGenericIconToggle.SetIsOnWithoutNotify(m_abilities[m_index].usesGenericIcon);
			//
			// triggerTargetDropdown.ClearOptions();
			// triggerTargetDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.TriggerTarget)).ToList());
			// triggerTargetDropdown.onValueChanged.RemoveAllListeners();
			// triggerTargetDropdown.onValueChanged.AddListener(OnTriggerTargetDropdownChanged);
			// //triggerTargetDropdown.SetValueWithoutNotify((int)m_abilities[m_index].triggerTarget);
			//
			// effectTargetDropdown.ClearOptions();
			// effectTargetDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.TriggerTarget)).ToList());
			// effectTargetDropdown.onValueChanged.RemoveAllListeners();
			// effectTargetDropdown.onValueChanged.AddListener(OnEffectTargetDropdownChanged);
			// //effectTargetDropdown.SetValueWithoutNotify((int)m_abilities[m_index].effectTarget);
			//
			// modifyEquationDropdown.ClearOptions();
			// modifyEquationDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.ModifyEquation)).ToList());
			// modifyEquationDropdown.onValueChanged.RemoveAllListeners();
			// modifyEquationDropdown.onValueChanged.AddListener(OnModifyEquationDropdownChanged);
			// //modifyEquationDropdown.SetValueWithoutNotify((int)m_abilities[m_index].modifyEquation);
			//
			// damageBasedOnConditionsToggle.onValueChanged.RemoveAllListeners();
			// damageBasedOnConditionsToggle.onValueChanged.AddListener(OnDamageBasedOnConditionsToggleChanged);
			// damageBasedOnConditionsToggle.SetIsOnWithoutNotify(m_abilities[m_index].damageBasedOnConditions);
			//
			// damageAmountConditionConditionDropdown.ClearOptions();
			// damageAmountConditionConditionDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Condition)).ToList());
			// damageAmountConditionConditionDropdown.onValueChanged.RemoveAllListeners();
			// damageAmountConditionConditionDropdown.onValueChanged.AddListener(OnDamageAmountConditionConditionDropdownChanged);
			// damageAmountConditionConditionDropdown.SetValueWithoutNotify((int)m_abilities[m_index].damageAmountCondition.condition);
			//
			// damageAmountConditionAppliesOnlyToYourFieldToggle.onValueChanged.RemoveAllListeners();
			// damageAmountConditionAppliesOnlyToYourFieldToggle.onValueChanged.AddListener(OnDamageAmountConditionAppliesOnlyToYourFieldToggleChanged);
			// damageAmountConditionAppliesOnlyToYourFieldToggle.SetIsOnWithoutNotify(m_abilities[m_index].damageAmountCondition.appliesOnlyToYourField);
			//
			// damageAmountConditionFactionDropdown.ClearOptions();
			// damageAmountConditionFactionDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Faction)).ToList());
			// damageAmountConditionFactionDropdown.onValueChanged.RemoveAllListeners();
			// damageAmountConditionFactionDropdown.onValueChanged.AddListener(OnDamageAmountConditionFactionDropdownChanged);
			// damageAmountConditionFactionDropdown.SetValueWithoutNotify((int)m_abilities[m_index].damageAmountCondition.faction);
			//
			// damageAmountConditionClassDropdown.ClearOptions();
			// damageAmountConditionClassDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Class)).ToList());
			// damageAmountConditionClassDropdown.onValueChanged.RemoveAllListeners();
			// damageAmountConditionClassDropdown.onValueChanged.AddListener(OnDamageAmountConditionClassDropdownChanged);
			// damageAmountConditionClassDropdown.SetValueWithoutNotify((int)m_abilities[m_index].damageAmountCondition.@class);
			//
			//
			// damageValueInputField.onValueChanged.RemoveAllListeners();
			// damageValueInputField.onValueChanged.AddListener(OnDamageValueInputFieldChanged);
			// damageValueInputField.SetTextWithoutNotify(m_abilities[m_index].damageValue.ToString());
			//
			// damageRandomlySplitToggle.onValueChanged.RemoveAllListeners();
			// damageRandomlySplitToggle.onValueChanged.AddListener(OnDamageRandomlySplitToggleChanged);
			// damageRandomlySplitToggle.SetIsOnWithoutNotify(m_abilities[m_index].damageRandomlySplit);
			//
			// applyDamageInEffectToggle.onValueChanged.RemoveAllListeners();
			// applyDamageInEffectToggle.onValueChanged.AddListener(OnApplyDamageInEffectToggleChanged);
			// applyDamageInEffectToggle.SetIsOnWithoutNotify(m_abilities[m_index].applyDamageInEffect);
			//
			// targetCountBasedOnConditionsToggle.onValueChanged.RemoveAllListeners();
			// targetCountBasedOnConditionsToggle.onValueChanged.AddListener(OnTargetCountBasedOnConditionsToggleChanged);
			// targetCountBasedOnConditionsToggle.SetIsOnWithoutNotify(m_abilities[m_index].targetCountBasedOnConditions);
			//
			// repeatDamageBasedOnConditionsToggle.onValueChanged.RemoveAllListeners();
			// repeatDamageBasedOnConditionsToggle.onValueChanged.AddListener(OnRepeatDamageBasedOnConditionsToggleChanged);
			// repeatDamageBasedOnConditionsToggle.SetIsOnWithoutNotify(m_abilities[m_index].repeatDamageBasedOnConditions);
			//
			// targetCountInputField.onValueChanged.RemoveAllListeners();
			// targetCountInputField.onValueChanged.AddListener(OnTargetCountInputFieldChanged);
			// targetCountInputField.SetTextWithoutNotify(m_abilities[m_index].targetCount.ToString());
			//
			// targetCountConditionConditionDropdown.ClearOptions();
			// targetCountConditionConditionDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Condition)).ToList());
			// targetCountConditionConditionDropdown.onValueChanged.RemoveAllListeners();
			// targetCountConditionConditionDropdown.onValueChanged.AddListener(OnTargetCountConditionConditionDropdownChanged);
			// targetCountConditionConditionDropdown.SetValueWithoutNotify((int)m_abilities[m_index].targetCountCondition.condition);
			//
			// targetCountConditionAppliesOnlyToYourFieldToggle.onValueChanged.RemoveAllListeners();
			// targetCountConditionAppliesOnlyToYourFieldToggle.onValueChanged.AddListener(OnTargetCountConditionAppliesOnlyToYourFieldToggleChanged);
			// targetCountConditionAppliesOnlyToYourFieldToggle.SetIsOnWithoutNotify(m_abilities[m_index].targetCountCondition.appliesOnlyToYourField);
			//
			// targetCountConditionFactionDropdown.ClearOptions();
			// targetCountConditionFactionDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Faction)).ToList());
			// targetCountConditionFactionDropdown.onValueChanged.RemoveAllListeners();
			// targetCountConditionFactionDropdown.onValueChanged.AddListener(OnTargetCountConditionFactionDropdownChanged);
			// targetCountConditionFactionDropdown.SetValueWithoutNotify((int)m_abilities[m_index].targetCountCondition.faction);
			//
			// targetCountConditionClassDropdown.ClearOptions();
			// targetCountConditionClassDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Class)).ToList());
			// targetCountConditionClassDropdown.onValueChanged.RemoveAllListeners();
			// targetCountConditionClassDropdown.onValueChanged.AddListener(OnTargetCountConditionClassDropdownChanged);
			// targetCountConditionClassDropdown.SetValueWithoutNotify((int)m_abilities[m_index].targetCountCondition.@class);
			//
			//
			// repeatCountConditionConditionDropdown.ClearOptions();
			// repeatCountConditionConditionDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Condition)).ToList());
			// repeatCountConditionConditionDropdown.onValueChanged.RemoveAllListeners();
			// repeatCountConditionConditionDropdown.onValueChanged.AddListener(OnRepeatCountConditionConditionDropdownChanged);
			// repeatCountConditionConditionDropdown.SetValueWithoutNotify((int)m_abilities[m_index].repeatCountCondition.condition);
			//
			// repeatCountConditionAppliesOnlyToYourFieldToggle.onValueChanged.RemoveAllListeners();
			// repeatCountConditionAppliesOnlyToYourFieldToggle.onValueChanged.AddListener(OnRepeatCountConditionAppliesOnlyToYourFieldToggleChanged);
			// repeatCountConditionAppliesOnlyToYourFieldToggle.SetIsOnWithoutNotify(m_abilities[m_index].repeatCountCondition.appliesOnlyToYourField);
			//
			// repeatCountConditionFactionDropdown.ClearOptions();
			// repeatCountConditionFactionDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Faction)).ToList());
			// repeatCountConditionFactionDropdown.onValueChanged.RemoveAllListeners();
			// repeatCountConditionFactionDropdown.onValueChanged.AddListener(OnRepeatCountConditionFactionDropdownChanged);
			// repeatCountConditionFactionDropdown.SetValueWithoutNotify((int)m_abilities[m_index].repeatCountCondition.faction);
			//
			// repeatCountConditionClassDropdown.ClearOptions();
			// repeatCountConditionClassDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Class)).ToList());
			// repeatCountConditionClassDropdown.onValueChanged.RemoveAllListeners();
			// repeatCountConditionClassDropdown.onValueChanged.AddListener(OnRepeatCountConditionClassDropdownChanged);
			// repeatCountConditionClassDropdown.SetValueWithoutNotify((int)m_abilities[m_index].repeatCountCondition.@class);
			//
			//
			// auraDataAuraEffectDropdown.ClearOptions();
			// auraDataAuraEffectDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.AuraData.AuraEffect)).ToList());
			// auraDataAuraEffectDropdown.onValueChanged.RemoveAllListeners();
			// auraDataAuraEffectDropdown.onValueChanged.AddListener(OnAuraDataAuraEffectDropdownChanged);
			// auraDataAuraEffectDropdown.SetValueWithoutNotify((int)m_abilities[m_index].auraData.auraEffect);
			//
			// auraDataTargetsComboBox.OnAddedFlag -= OnAuraDataTargetsComboBoxAddedFlag;
			// auraDataTargetsComboBox.OnAddedFlag += OnAuraDataTargetsComboBoxAddedFlag;
			// auraDataTargetsComboBox.OnRemovedFlag -= OnAuraDataTargetsComboBoxRemovedFlag;
			// auraDataTargetsComboBox.OnRemovedFlag += OnAuraDataTargetsComboBoxRemovedFlag;
			// auraDataTargetsComboBox.SetValueWithoutNotify((int)m_abilities[m_index].auraData.targets, Enum.GetValues(typeof(CrossBlitz.Card.FilterCondition)).Convert(e => new D90ComboBox.OptionValue { name = e.ToString(), value = (int)e }).ToList());
			//
			// auraDataModifyDrawAmountInputField.onValueChanged.RemoveAllListeners();
			// auraDataModifyDrawAmountInputField.onValueChanged.AddListener(OnAuraDataModifyDrawAmountInputFieldChanged);
			// auraDataModifyDrawAmountInputField.SetTextWithoutNotify(m_abilities[m_index].auraData.modifyDrawAmount.ToString());
			//
			// auraDataRemoveWhenMinionDiesToggle.onValueChanged.RemoveAllListeners();
			// auraDataRemoveWhenMinionDiesToggle.onValueChanged.AddListener(OnAuraDataRemoveWhenMinionDiesToggleChanged);
			// auraDataRemoveWhenMinionDiesToggle.SetIsOnWithoutNotify(m_abilities[m_index].auraData.removeWhenMinionDies);
			//
			//
			// manaModifierInputField.onValueChanged.RemoveAllListeners();
			// manaModifierInputField.onValueChanged.AddListener(OnManaModifierInputFieldChanged);
			// manaModifierInputField.SetTextWithoutNotify(m_abilities[m_index].manaModifier.ToString());
			//
			// armorModifierInputField.onValueChanged.RemoveAllListeners();
			// armorModifierInputField.onValueChanged.AddListener(OnArmorModifierInputFieldChanged);
			// armorModifierInputField.SetTextWithoutNotify(m_abilities[m_index].armorModifier.ToString());
			//
			// powerModifierInputField.onValueChanged.RemoveAllListeners();
			// powerModifierInputField.onValueChanged.AddListener(OnPowerModifierInputFieldChanged);
			// powerModifierInputField.SetTextWithoutNotify(m_abilities[m_index].powerModifier.ToString());
			//
			// // modifyStatsConditionConditionDropdown.ClearOptions();
			// // modifyStatsConditionConditionDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Condition)).ToList());
			// // modifyStatsConditionConditionDropdown.onValueChanged.RemoveAllListeners();
			// // modifyStatsConditionConditionDropdown.onValueChanged.AddListener(OnModifyStatsConditionConditionDropdownChanged);
			// // modifyStatsConditionConditionDropdown.SetValueWithoutNotify((int)m_abilities[m_index].modifyStatsCondition.condition);
			// //
			// // modifyStatsConditionFactionDropdown.ClearOptions();
			// // modifyStatsConditionFactionDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Faction)).ToList());
			// // modifyStatsConditionFactionDropdown.onValueChanged.RemoveAllListeners();
			// // modifyStatsConditionFactionDropdown.onValueChanged.AddListener(OnModifyStatsConditionFactionDropdownChanged);
			// // modifyStatsConditionFactionDropdown.SetValueWithoutNotify((int)m_abilities[m_index].modifyStatsCondition.faction);
			// //
			// // modifyStatsConditionClassDropdown.ClearOptions();
			// // modifyStatsConditionClassDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Class)).ToList());
			// // modifyStatsConditionClassDropdown.onValueChanged.RemoveAllListeners();
			// // modifyStatsConditionClassDropdown.onValueChanged.AddListener(OnModifyStatsConditionClassDropdownChanged);
			// // modifyStatsConditionClassDropdown.SetValueWithoutNotify((int)m_abilities[m_index].modifyStatsCondition.@class);
			//
			//
			// // healthModifierInputField.onValueChanged.RemoveAllListeners();
			// // healthModifierInputField.onValueChanged.AddListener(OnHealthModifierInputFieldChanged);
			// // healthModifierInputField.SetTextWithoutNotify(m_abilities[m_index].healthModifier.ToString());
			// //
			// // healAmountInputField.onValueChanged.RemoveAllListeners();
			// // healAmountInputField.onValueChanged.AddListener(OnHealAmountInputFieldChanged);
			// // healAmountInputField.SetTextWithoutNotify(m_abilities[m_index].healAmount.ToString());
			//
			// // multiAttackCountInputField.onValueChanged.RemoveAllListeners();
			// // multiAttackCountInputField.onValueChanged.AddListener(OnMultiAttackCountInputFieldChanged);
			// // multiAttackCountInputField.SetTextWithoutNotify(m_abilities[m_index].multiAttackCount.ToString());
			//
			// armorBodyAmountInputField.onValueChanged.RemoveAllListeners();
			// armorBodyAmountInputField.onValueChanged.AddListener(OnArmorBodyAmountInputFieldChanged);
			// armorBodyAmountInputField.SetTextWithoutNotify(m_abilities[m_index].armorBodyAmount.ToString());
			//
			// maxDamageTakenDuringHitInputField.onValueChanged.RemoveAllListeners();
			// maxDamageTakenDuringHitInputField.onValueChanged.AddListener(OnMaxDamageTakenDuringHitInputFieldChanged);
			// maxDamageTakenDuringHitInputField.SetTextWithoutNotify(m_abilities[m_index].maxDamageTakenDuringHit.ToString());
			//
			// drawCountInputField.onValueChanged.RemoveAllListeners();
			// drawCountInputField.onValueChanged.AddListener(OnDrawCountInputFieldChanged);
			// drawCountInputField.SetTextWithoutNotify(m_abilities[m_index].drawCount.ToString());
			//
			// drawConditionConditionDropdown.ClearOptions();
			// drawConditionConditionDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Condition)).ToList());
			// drawConditionConditionDropdown.onValueChanged.RemoveAllListeners();
			// drawConditionConditionDropdown.onValueChanged.AddListener(OnDrawConditionConditionDropdownChanged);
			// drawConditionConditionDropdown.SetValueWithoutNotify((int)m_abilities[m_index].drawCondition.condition);
			//
			// drawConditionFactionDropdown.ClearOptions();
			// drawConditionFactionDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Faction)).ToList());
			// drawConditionFactionDropdown.onValueChanged.RemoveAllListeners();
			// drawConditionFactionDropdown.onValueChanged.AddListener(OnDrawConditionFactionDropdownChanged);
			// drawConditionFactionDropdown.SetValueWithoutNotify((int)m_abilities[m_index].drawCondition.faction);
			//
			// drawConditionClassDropdown.ClearOptions();
			// drawConditionClassDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Class)).ToList());
			// drawConditionClassDropdown.onValueChanged.RemoveAllListeners();
			// drawConditionClassDropdown.onValueChanged.AddListener(OnDrawConditionClassDropdownChanged);
			// drawConditionClassDropdown.SetValueWithoutNotify((int)m_abilities[m_index].drawCondition.@class);
			//
			//
			//
			// summonMinionDataCountInputField.onValueChanged.RemoveAllListeners();
			// summonMinionDataCountInputField.onValueChanged.AddListener(OnSummonMinionDataCountInputFieldChanged);
			// summonMinionDataCountInputField.SetTextWithoutNotify(m_abilities[m_index].summonMinionData.count.ToString());
			//
			// summonMinionDataPlacementComboBox.OnAddedFlag -= OnSummonMinionDataPlacementComboBoxAddedFlag;
			// summonMinionDataPlacementComboBox.OnAddedFlag += OnSummonMinionDataPlacementComboBoxAddedFlag;
			// summonMinionDataPlacementComboBox.OnRemovedFlag -= OnSummonMinionDataPlacementComboBoxRemovedFlag;
			// summonMinionDataPlacementComboBox.OnRemovedFlag += OnSummonMinionDataPlacementComboBoxRemovedFlag;
			// summonMinionDataPlacementComboBox.SetValueWithoutNotify((int)m_abilities[m_index].summonMinionData.placement, Enum.GetValues(typeof(CrossBlitz.Card.SummonPlacement)).Convert(e => new D90ComboBox.OptionValue { name = e.ToString(), value = (int)e }).ToList());
			//
			// summonMinionDataSummonVisualEffectDropdown.ClearOptions();
			// summonMinionDataSummonVisualEffectDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.SummonMinionData.SummonVisualEffect)).ToList());
			// summonMinionDataSummonVisualEffectDropdown.onValueChanged.RemoveAllListeners();
			// summonMinionDataSummonVisualEffectDropdown.onValueChanged.AddListener(OnSummonMinionDataSummonVisualEffectDropdownChanged);
			// summonMinionDataSummonVisualEffectDropdown.SetValueWithoutNotify((int)m_abilities[m_index].summonMinionData.summonVisualEffect);
			//
			//
			//
			// // cardFilterIsRandomToggle.onValueChanged.RemoveAllListeners();
			// // cardFilterIsRandomToggle.onValueChanged.AddListener(OnCardFilterIsRandomToggleChanged);
			// // cardFilterIsRandomToggle.SetIsOnWithoutNotify(m_abilities[m_index].cardFilter.isRandom);
			//
			// cardFilterDrawCountBasedOnConditionsToggle.onValueChanged.RemoveAllListeners();
			// cardFilterDrawCountBasedOnConditionsToggle.onValueChanged.AddListener(OnCardFilterDrawCountBasedOnConditionsToggleChanged);
			// cardFilterDrawCountBasedOnConditionsToggle.SetIsOnWithoutNotify(m_abilities[m_index].cardFilter.drawCountBasedOnConditions);
			//
			// // cardFilterDrawCountConditionDropdown.ClearOptions();
			// // cardFilterDrawCountConditionDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.DrawCountCondition)).ToList());
			// // cardFilterDrawCountConditionDropdown.onValueChanged.RemoveAllListeners();
			// // cardFilterDrawCountConditionDropdown.onValueChanged.AddListener(OnCardFilterDrawCountConditionDropdownChanged);
			// // cardFilterDrawCountConditionDropdown.SetValueWithoutNotify((int)m_abilities[m_index].cardFilter.drawCountCondition);
			//
			//
			// cardFilterGrantedToOpponentToggle.onValueChanged.RemoveAllListeners();
			// cardFilterGrantedToOpponentToggle.onValueChanged.AddListener(OnCardFilterGrantedToOpponentToggleChanged);
			// cardFilterGrantedToOpponentToggle.SetIsOnWithoutNotify(m_abilities[m_index].cardFilter.grantedToOpponent);
			//
			// cardFilterGrantACopyFromLocationToggle.onValueChanged.RemoveAllListeners();
			// cardFilterGrantACopyFromLocationToggle.onValueChanged.AddListener(OnCardFilterGrantACopyFromLocationToggleChanged);
			// cardFilterGrantACopyFromLocationToggle.SetIsOnWithoutNotify(m_abilities[m_index].cardFilter.grantACopyFromLocation);
			//
			// cardFilterCopyFromLocationComboBox.OnAddedFlag -= OnCardFilterCopyFromLocationComboBoxAddedFlag;
			// cardFilterCopyFromLocationComboBox.OnAddedFlag += OnCardFilterCopyFromLocationComboBoxAddedFlag;
			// cardFilterCopyFromLocationComboBox.OnRemovedFlag -= OnCardFilterCopyFromLocationComboBoxRemovedFlag;
			// cardFilterCopyFromLocationComboBox.OnRemovedFlag += OnCardFilterCopyFromLocationComboBoxRemovedFlag;
			// cardFilterCopyFromLocationComboBox.SetValueWithoutNotify((int)m_abilities[m_index].cardFilter.copyFromLocation, Enum.GetValues(typeof(CrossBlitz.Card.FilterCondition)).Convert(e => new D90ComboBox.OptionValue { name = e.ToString(), value = (int)e }).ToList());
			//
			// cardFilterFromOpponentsFactionToggle.onValueChanged.RemoveAllListeners();
			// cardFilterFromOpponentsFactionToggle.onValueChanged.AddListener(OnCardFilterFromOpponentsFactionToggleChanged);
			// cardFilterFromOpponentsFactionToggle.SetIsOnWithoutNotify(m_abilities[m_index].cardFilter.fromOpponentsFaction);
			//
			// cardFilterFromClassDropdown.ClearOptions();
			// cardFilterFromClassDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Class)).ToList());
			// cardFilterFromClassDropdown.onValueChanged.RemoveAllListeners();
			// cardFilterFromClassDropdown.onValueChanged.AddListener(OnCardFilterFromClassDropdownChanged);
			// cardFilterFromClassDropdown.SetValueWithoutNotify((int)m_abilities[m_index].cardFilter.fromClass);
			//
			// cardFilterFromTypeDropdown.ClearOptions();
			// cardFilterFromTypeDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.CardType)).ToList());
			// cardFilterFromTypeDropdown.onValueChanged.RemoveAllListeners();
			// cardFilterFromTypeDropdown.onValueChanged.AddListener(OnCardFilterFromTypeDropdownChanged);
			// cardFilterFromTypeDropdown.SetValueWithoutNotify((int)m_abilities[m_index].cardFilter.fromType);
			//
			// cardFilterFromRarityDropdown.ClearOptions();
			// cardFilterFromRarityDropdown.AddOptions(Enum.GetNames(typeof(CrossBlitz.Card.Rarity)).ToList());
			// cardFilterFromRarityDropdown.onValueChanged.RemoveAllListeners();
			// cardFilterFromRarityDropdown.onValueChanged.AddListener(OnCardFilterFromRarityDropdownChanged);
			// cardFilterFromRarityDropdown.SetValueWithoutNotify((int)m_abilities[m_index].cardFilter.fromRarity);
			//
			// cardFilterLimitToSameFactionToggle.onValueChanged.RemoveAllListeners();
			// cardFilterLimitToSameFactionToggle.onValueChanged.AddListener(OnCardFilterLimitToSameFactionToggleChanged);
			// cardFilterLimitToSameFactionToggle.SetIsOnWithoutNotify(m_abilities[m_index].cardFilter.limitToSameFaction);
			//
			// cardFilterOverrideCopiedCardStatsToggle.onValueChanged.RemoveAllListeners();
			// cardFilterOverrideCopiedCardStatsToggle.onValueChanged.AddListener(OnCardFilterOverrideCopiedCardStatsToggleChanged);
			// cardFilterOverrideCopiedCardStatsToggle.SetIsOnWithoutNotify(m_abilities[m_index].cardFilter.overrideCopiedCardStats);
			//
			// cardFilterCostOverrideInputField.onValueChanged.RemoveAllListeners();
			// cardFilterCostOverrideInputField.onValueChanged.AddListener(OnCardFilterCostOverrideInputFieldChanged);
			// cardFilterCostOverrideInputField.SetTextWithoutNotify(m_abilities[m_index].cardFilter.costOverride.ToString());
			//
			// cardFilterPowerOverrideInputField.onValueChanged.RemoveAllListeners();
			// cardFilterPowerOverrideInputField.onValueChanged.AddListener(OnCardFilterPowerOverrideInputFieldChanged);
			// cardFilterPowerOverrideInputField.SetTextWithoutNotify(m_abilities[m_index].cardFilter.powerOverride.ToString());
			//
			// cardFilterHealthOverrideInputField.onValueChanged.RemoveAllListeners();
			// cardFilterHealthOverrideInputField.onValueChanged.AddListener(OnCardFilterHealthOverrideInputFieldChanged);
			// cardFilterHealthOverrideInputField.SetTextWithoutNotify(m_abilities[m_index].cardFilter.healthOverride.ToString());
			//
			//
			//
			// removeAllCardsToggle.onValueChanged.RemoveAllListeners();
			// removeAllCardsToggle.onValueChanged.AddListener(OnRemoveAllCardsToggleChanged);
			// removeAllCardsToggle.SetIsOnWithoutNotify(m_abilities[m_index].removeAllCards);
			//
			// amountOfCardsToRemoveInputField.onValueChanged.RemoveAllListeners();
			// amountOfCardsToRemoveInputField.onValueChanged.AddListener(OnAmountOfCardsToRemoveInputFieldChanged);
			// amountOfCardsToRemoveInputField.SetTextWithoutNotify(m_abilities[m_index].amountOfCardsToRemove.ToString());
			//
			//
			//
			//
			// triggeredAbilityComboBox.OnAddedFlag -= OnTriggeredAbilityComboBoxAddedFlag;
			// triggeredAbilityComboBox.OnAddedFlag += OnTriggeredAbilityComboBoxAddedFlag;
			// triggeredAbilityComboBox.OnRemovedFlag -= OnTriggeredAbilityComboBoxRemovedFlag;
			// triggeredAbilityComboBox.OnRemovedFlag += OnTriggeredAbilityComboBoxRemovedFlag;
			// triggeredAbilityComboBox.SetValueWithoutNotify((int)m_abilities[m_index].triggeredAbility, Enum.GetValues(typeof(CrossBlitz.Card.TriggerType)).Convert(e => new D90ComboBox.OptionValue { name = e.ToString(), value = (int)e }).ToList());
			//
			// hasConditionalAbilityToggle.onValueChanged.RemoveAllListeners();
			// hasConditionalAbilityToggle.onValueChanged.AddListener(OnHasConditionalAbilityToggleChanged);
			// hasConditionalAbilityToggle.SetIsOnWithoutNotify(m_abilities[m_index].hasConditionalAbility);


		}
		// private void OnKeywordDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].keyword = (CrossBlitz.Card.AbilityKeyword)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnTriggerTypeComboBoxAddedFlag(int flag)
		// {
		// 	m_abilities[m_index].triggerType |= (CrossBlitz.Card.TriggerType)flag;
		// 	Init(m_abilities, m_index);
		// }
		// private void OnTriggerTypeComboBoxRemovedFlag(int flag)
		// {
		// 	m_abilities[m_index].triggerType &= ~(CrossBlitz.Card.TriggerType)flag;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnTriggerEvaluateConditionComboBoxAddedFlag(int flag)
		// {
		// 	m_abilities[m_index].triggerEvaluateCondition |= (CrossBlitz.Card.TriggerEvaluateCondition)flag;
		// 	Init(m_abilities, m_index);
		// }
		// private void OnTriggerEvaluateConditionComboBoxRemovedFlag(int flag)
		// {
		// 	m_abilities[m_index].triggerEvaluateCondition &= ~(CrossBlitz.Card.TriggerEvaluateCondition)flag;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnTriggerConditionsComboBoxAddedFlag(int flag)
		// {
		// 	m_abilities[m_index].triggerConditions |= (CrossBlitz.Card.FilterCondition)flag;
		// 	Init(m_abilities, m_index);
		// }
		// private void OnTriggerConditionsComboBoxRemovedFlag(int flag)
		// {
		// 	m_abilities[m_index].triggerConditions &= ~(CrossBlitz.Card.FilterCondition)flag;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnTargetConditionsComboBoxAddedFlag(int flag)
		// {
		// 	m_abilities[m_index].targetConditions |= (CrossBlitz.Card.FilterCondition)flag;
		// 	Init(m_abilities, m_index);
		// }
		// private void OnTargetConditionsComboBoxRemovedFlag(int flag)
		// {
		// 	m_abilities[m_index].targetConditions &= ~(CrossBlitz.Card.FilterCondition)flag;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnFilterValuesFactionDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].filterValues.faction = (CrossBlitz.Card.Faction)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnFilterValuesClassDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].filterValues.@class = (CrossBlitz.Card.Class)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnFilterValuesStatusEffectComboBoxAddedFlag(int flag)
		// {
		// 	m_abilities[m_index].filterValues.statusEffect |= (CrossBlitz.Card.StatusEffect)flag;
		// 	Init(m_abilities, m_index);
		// }
		// private void OnFilterValuesStatusEffectComboBoxRemovedFlag(int flag)
		// {
		// 	m_abilities[m_index].filterValues.statusEffect &= ~(CrossBlitz.Card.StatusEffect)flag;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnFilterValuesMaxCountInputFieldChanged(string input)
		// {
		// 	if (int.TryParse(input, out var value))
		// 	{
		// 		m_abilities[m_index].filterValues.maxCount = value;
		// 		Init(m_abilities, m_index);
		// 	}
		// }
		//
		//
		// private void OnTriggerValuesFactionDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].triggerValues.faction = (CrossBlitz.Card.Faction)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnTriggerValuesClassDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].triggerValues.@class = (CrossBlitz.Card.Class)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnTriggerValuesStatusEffectComboBoxAddedFlag(int flag)
		// {
		// 	m_abilities[m_index].triggerValues.statusEffect |= (CrossBlitz.Card.StatusEffect)flag;
		// 	Init(m_abilities, m_index);
		// }
		// private void OnTriggerValuesStatusEffectComboBoxRemovedFlag(int flag)
		// {
		// 	m_abilities[m_index].triggerValues.statusEffect &= ~(CrossBlitz.Card.StatusEffect)flag;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnTriggerValuesMaxCountInputFieldChanged(string input)
		// {
		// 	if (int.TryParse(input, out var value))
		// 	{
		// 		m_abilities[m_index].triggerValues.maxCount = value;
		// 		Init(m_abilities, m_index);
		// 	}
		// }
		//
		//
		// private void OnUsesCustomEffectToggleChanged(bool isOn)
		// {
		// 	m_abilities[m_index].usesCustomEffect = isOn;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnCustomEffectComponentDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].customEffectComponent = customEffectComponentDropdown.options[index].text;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnUsesGenericIconToggleChanged(bool isOn)
		// {
		// 	m_abilities[m_index].usesGenericIcon = isOn;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnTriggerTargetDropdownChanged(int index)
		// {
		// 	//m_abilities[m_index].triggerTarget = (CrossBlitz.Card.TriggerTarget)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnEffectTargetDropdownChanged(int index)
		// {
		// 	//m_abilities[m_index].effectTarget = (CrossBlitz.Card.TriggerTarget)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnModifyEquationDropdownChanged(int index)
		// {
		// 	//m_abilities[m_index].modifyEquation = (CrossBlitz.Card.ModifyEquation)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnDamageBasedOnConditionsToggleChanged(bool isOn)
		// {
		// 	m_abilities[m_index].damageBasedOnConditions = isOn;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnDamageAmountConditionConditionDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].damageAmountCondition.condition = (CrossBlitz.Card.Condition)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnDamageAmountConditionAppliesOnlyToYourFieldToggleChanged(bool isOn)
		// {
		// 	m_abilities[m_index].damageAmountCondition.appliesOnlyToYourField = isOn;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnDamageAmountConditionFactionDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].damageAmountCondition.faction = (CrossBlitz.Card.Faction)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnDamageAmountConditionClassDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].damageAmountCondition.@class = (CrossBlitz.Card.Class)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		//
		// private void OnDamageValueInputFieldChanged(string input)
		// {
		// 	if (int.TryParse(input, out var value))
		// 	{
		// 		m_abilities[m_index].damageValue = value;
		// 		Init(m_abilities, m_index);
		// 	}
		// }
		//
		// private void OnDamageRandomlySplitToggleChanged(bool isOn)
		// {
		// 	m_abilities[m_index].damageRandomlySplit = isOn;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnApplyDamageInEffectToggleChanged(bool isOn)
		// {
		// 	m_abilities[m_index].applyDamageInEffect = isOn;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnTargetCountBasedOnConditionsToggleChanged(bool isOn)
		// {
		// 	m_abilities[m_index].targetCountBasedOnConditions = isOn;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnRepeatDamageBasedOnConditionsToggleChanged(bool isOn)
		// {
		// 	m_abilities[m_index].repeatDamageBasedOnConditions = isOn;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnTargetCountInputFieldChanged(string input)
		// {
		// 	if (int.TryParse(input, out var value))
		// 	{
		// 		m_abilities[m_index].targetCount = value;
		// 		Init(m_abilities, m_index);
		// 	}
		// }
		//
		// private void OnTargetCountConditionConditionDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].targetCountCondition.condition = (CrossBlitz.Card.Condition)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnTargetCountConditionAppliesOnlyToYourFieldToggleChanged(bool isOn)
		// {
		// 	m_abilities[m_index].targetCountCondition.appliesOnlyToYourField = isOn;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnTargetCountConditionFactionDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].targetCountCondition.faction = (CrossBlitz.Card.Faction)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnTargetCountConditionClassDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].targetCountCondition.@class = (CrossBlitz.Card.Class)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		//
		// private void OnRepeatCountConditionConditionDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].repeatCountCondition.condition = (CrossBlitz.Card.Condition)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnRepeatCountConditionAppliesOnlyToYourFieldToggleChanged(bool isOn)
		// {
		// 	m_abilities[m_index].repeatCountCondition.appliesOnlyToYourField = isOn;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnRepeatCountConditionFactionDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].repeatCountCondition.faction = (CrossBlitz.Card.Faction)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnRepeatCountConditionClassDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].repeatCountCondition.@class = (CrossBlitz.Card.Class)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		//
		// private void OnAuraDataAuraEffectDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].auraData.auraEffect = (CrossBlitz.Card.AuraData.AuraEffect)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnAuraDataTargetsComboBoxAddedFlag(int flag)
		// {
		// 	m_abilities[m_index].auraData.targets |= (CrossBlitz.Card.FilterCondition)flag;
		// 	Init(m_abilities, m_index);
		// }
		// private void OnAuraDataTargetsComboBoxRemovedFlag(int flag)
		// {
		// 	m_abilities[m_index].auraData.targets &= ~(CrossBlitz.Card.FilterCondition)flag;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnAuraDataModifyDrawAmountInputFieldChanged(string input)
		// {
		// 	if (int.TryParse(input, out var value))
		// 	{
		// 		m_abilities[m_index].auraData.modifyDrawAmount = value;
		// 		Init(m_abilities, m_index);
		// 	}
		// }
		//
		// private void OnAuraDataRemoveWhenMinionDiesToggleChanged(bool isOn)
		// {
		// 	m_abilities[m_index].auraData.removeWhenMinionDies = isOn;
		// 	Init(m_abilities, m_index);
		// }
		//
		//
		// private void OnManaModifierInputFieldChanged(string input)
		// {
		// 	if (int.TryParse(input, out var value))
		// 	{
		// 		m_abilities[m_index].manaModifier = value;
		// 		Init(m_abilities, m_index);
		// 	}
		// }
		//
		// private void OnArmorModifierInputFieldChanged(string input)
		// {
		// 	if (int.TryParse(input, out var value))
		// 	{
		// 		m_abilities[m_index].armorModifier = value;
		// 		Init(m_abilities, m_index);
		// 	}
		// }
		//
		// private void OnPowerModifierInputFieldChanged(string input)
		// {
		// 	if (int.TryParse(input, out var value))
		// 	{
		// 		m_abilities[m_index].powerModifier = value;
		// 		Init(m_abilities, m_index);
		// 	}
		// }
		//
		// private void OnModifyStatsConditionConditionDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].modifyStatsCondition.condition = (CrossBlitz.Card.Condition)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnModifyStatsConditionFactionDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].modifyStatsCondition.faction = (CrossBlitz.Card.Faction)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnModifyStatsConditionClassDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].modifyStatsCondition.@class = (CrossBlitz.Card.Class)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		//
		// private void OnHealthModifierInputFieldChanged(string input)
		// {
		// 	if (int.TryParse(input, out var value))
		// 	{
		// 		m_abilities[m_index].healthModifier = value;
		// 		Init(m_abilities, m_index);
		// 	}
		// }
		//
		// private void OnHealAmountInputFieldChanged(string input)
		// {
		// 	if (int.TryParse(input, out var value))
		// 	{
		// 		m_abilities[m_index].healAmount = value;
		// 		Init(m_abilities, m_index);
		// 	}
		// }
		//
		// // private void OnMultiAttackCountInputFieldChanged(string input)
		// // {
		// // 	if (int.TryParse(input, out var value))
		// // 	{
		// // 		m_abilities[m_index].multiAttackCount = value;
		// // 		Init(m_abilities, m_index);
		// // 	}
		// // }
		//
		// private void OnArmorBodyAmountInputFieldChanged(string input)
		// {
		// 	if (int.TryParse(input, out var value))
		// 	{
		// 		m_abilities[m_index].armorBodyAmount = value;
		// 		Init(m_abilities, m_index);
		// 	}
		// }
		//
		// private void OnMaxDamageTakenDuringHitInputFieldChanged(string input)
		// {
		// 	if (int.TryParse(input, out var value))
		// 	{
		// 		m_abilities[m_index].maxDamageTakenDuringHit = value;
		// 		Init(m_abilities, m_index);
		// 	}
		// }
		//
		// private void OnDrawCountInputFieldChanged(string input)
		// {
		// 	if (int.TryParse(input, out var value))
		// 	{
		// 		m_abilities[m_index].drawCount = value;
		// 		Init(m_abilities, m_index);
		// 	}
		// }
		//
		// private void OnDrawConditionConditionDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].drawCondition.condition = (CrossBlitz.Card.Condition)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnDrawConditionFactionDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].drawCondition.faction = (CrossBlitz.Card.Faction)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnDrawConditionClassDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].drawCondition.@class = (CrossBlitz.Card.Class)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		//
		//
		// private void OnSummonMinionDataCountInputFieldChanged(string input)
		// {
		// 	if (int.TryParse(input, out var value))
		// 	{
		// 		m_abilities[m_index].summonMinionData.count = value;
		// 		Init(m_abilities, m_index);
		// 	}
		// }
		//
		// private void OnSummonMinionDataPlacementComboBoxAddedFlag(int flag)
		// {
		// 	m_abilities[m_index].summonMinionData.placement |= (CrossBlitz.Card.SummonPlacement)flag;
		// 	Init(m_abilities, m_index);
		// }
		// private void OnSummonMinionDataPlacementComboBoxRemovedFlag(int flag)
		// {
		// 	m_abilities[m_index].summonMinionData.placement &= ~(CrossBlitz.Card.SummonPlacement)flag;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnSummonMinionDataSummonVisualEffectDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].summonMinionData.summonVisualEffect = (CrossBlitz.Card.SummonMinionData.SummonVisualEffect)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		//
		//
		// private void OnCardFilterIsRandomToggleChanged(bool isOn)
		// {
		// 	// m_abilities[m_index].cardFilter.isRandom = isOn;
		// 	// Init(m_abilities, m_index);
		// }
		//
		// private void OnCardFilterDrawCountBasedOnConditionsToggleChanged(bool isOn)
		// {
		// 	m_abilities[m_index].cardFilter.drawCountBasedOnConditions = isOn;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnCardFilterDrawCountConditionDropdownChanged(int index)
		// {
		// 	// m_abilities[m_index].cardFilter.drawCountCondition = (CrossBlitz.Card.DrawCountCondition)index;
		// 	// Init(m_abilities, m_index);
		// }
		//
		//
		// private void OnCardFilterGrantedToOpponentToggleChanged(bool isOn)
		// {
		// 	m_abilities[m_index].cardFilter.grantedToOpponent = isOn;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnCardFilterGrantACopyFromLocationToggleChanged(bool isOn)
		// {
		// 	m_abilities[m_index].cardFilter.grantACopyFromLocation = isOn;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnCardFilterCopyFromLocationComboBoxAddedFlag(int flag)
		// {
		// 	m_abilities[m_index].cardFilter.copyFromLocation |= (CrossBlitz.Card.FilterCondition)flag;
		// 	Init(m_abilities, m_index);
		// }
		// private void OnCardFilterCopyFromLocationComboBoxRemovedFlag(int flag)
		// {
		// 	m_abilities[m_index].cardFilter.copyFromLocation &= ~(CrossBlitz.Card.FilterCondition)flag;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnCardFilterFromOpponentsFactionToggleChanged(bool isOn)
		// {
		// 	m_abilities[m_index].cardFilter.fromOpponentsFaction = isOn;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnCardFilterFromClassDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].cardFilter.fromClass = (CrossBlitz.Card.Class)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnCardFilterFromTypeDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].cardFilter.fromType = (CrossBlitz.Card.CardType)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnCardFilterFromRarityDropdownChanged(int index)
		// {
		// 	m_abilities[m_index].cardFilter.fromRarity = (CrossBlitz.Card.Rarity)index;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnCardFilterLimitToSameFactionToggleChanged(bool isOn)
		// {
		// 	m_abilities[m_index].cardFilter.limitToSameFaction = isOn;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnCardFilterOverrideCopiedCardStatsToggleChanged(bool isOn)
		// {
		// 	m_abilities[m_index].cardFilter.overrideCopiedCardStats = isOn;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnCardFilterCostOverrideInputFieldChanged(string input)
		// {
		// 	if (int.TryParse(input, out var value))
		// 	{
		// 		m_abilities[m_index].cardFilter.costOverride = value;
		// 		Init(m_abilities, m_index);
		// 	}
		// }
		//
		// private void OnCardFilterPowerOverrideInputFieldChanged(string input)
		// {
		// 	if (int.TryParse(input, out var value))
		// 	{
		// 		m_abilities[m_index].cardFilter.powerOverride = value;
		// 		Init(m_abilities, m_index);
		// 	}
		// }
		//
		// private void OnCardFilterHealthOverrideInputFieldChanged(string input)
		// {
		// 	if (int.TryParse(input, out var value))
		// 	{
		// 		m_abilities[m_index].cardFilter.healthOverride = value;
		// 		Init(m_abilities, m_index);
		// 	}
		// }
		//
		//
		//
		// private void OnRemoveAllCardsToggleChanged(bool isOn)
		// {
		// 	m_abilities[m_index].removeAllCards = isOn;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnAmountOfCardsToRemoveInputFieldChanged(string input)
		// {
		// 	if (int.TryParse(input, out var value))
		// 	{
		// 		m_abilities[m_index].amountOfCardsToRemove = value;
		// 		Init(m_abilities, m_index);
		// 	}
		// }
		//
		//
		//
		//
		// private void OnTriggeredAbilityComboBoxAddedFlag(int flag)
		// {
		// 	m_abilities[m_index].triggeredAbility |= (CrossBlitz.Card.TriggerType)flag;
		// 	Init(m_abilities, m_index);
		// }
		// private void OnTriggeredAbilityComboBoxRemovedFlag(int flag)
		// {
		// 	m_abilities[m_index].triggeredAbility &= ~(CrossBlitz.Card.TriggerType)flag;
		// 	Init(m_abilities, m_index);
		// }
		//
		// private void OnHasConditionalAbilityToggleChanged(bool isOn)
		// {
		// 	m_abilities[m_index].hasConditionalAbility = isOn;
		// 	Init(m_abilities, m_index);
		// }
		//

	}
}