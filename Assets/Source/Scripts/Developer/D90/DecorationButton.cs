using CrossBlitz.Fables.Grid;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz
{
    public class DecorationButton : MonoBehaviour
    {
        public string buttonName;
        public Text buttonText;
        public Button button;
        public GridEditorToolbox editorPanel;

        public void Start()
        {
            button.onClick.AddListener(ClickButton);
        }


        public void SetupButton(string name, GridEditorToolbox toolbox)
        {
            buttonName = name;
            buttonText.text = name;
            editorPanel = toolbox;
        }

        public void ClickButton()
        {
            editorPanel.OnRefreshDecorationList(buttonName);
        }
        
    }
}
