﻿using UnityEngine;

namespace CrossBlitz.Cameras
{

    public class GameCamera : CameraVariant
    {
        public static GameCamera Instance;
        public static Camera Camera;

        public override void Awake()
        {
            base.Awake();
            Instance = this;
            Camera = GetComponent<Camera>();
        }
    }
}
