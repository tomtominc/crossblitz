﻿using UnityEngine;

namespace CrossBlitz.Cameras
{
    [RequireComponent(typeof(Camera))]
    public class LobbyCamera : MonoBehaviour
    {
        public static LobbyCamera Instance;
        public static Camera Camera;

        private void Awake()
        {
            Instance = this;
            Camera = GetComponent<Camera>();
        }
    }
}
