using UnityEngine;

namespace CrossBlitz.Cameras
{
    [RequireComponent(typeof(Camera))]
    public class WorldMapCamera : MonoBehaviour
    {
        public Camera stableCamera;
        public float dragSpeed = 2;

        private Camera _cam;
        private Vector3 _dragOrigin;

        private Vector3 _center;
        private Vector3 _lastCameraPosition;

        public Camera Camera => _cam;

        private void Start()
        {
            _cam = GetComponent<Camera>();
        }
        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                _dragOrigin = Input.mousePosition;
                _center = stableCamera.ScreenToWorldPoint(Input.mousePosition);
                _lastCameraPosition = transform.position;
                return;
            }

            if (!Input.GetMouseButton(0)) return;

            var nextPosition = stableCamera.ScreenToWorldPoint(Input.mousePosition);
            var moveAmount = _center - nextPosition;
            transform.position = _lastCameraPosition + new Vector3( moveAmount.x, moveAmount.y);
        }
    }
}