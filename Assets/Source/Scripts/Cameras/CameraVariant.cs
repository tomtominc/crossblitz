using CrossBlitz.AddressablesAPI;
using UnityEngine;

namespace CrossBlitz.Cameras
{
    [RequireComponent(typeof(Camera))]
    [RequireComponent(typeof(ShakeContainer))]
    public class CameraVariant : MonoBehaviour
    {
        protected ShakeContainer _shakeContainer;
        protected Camera _camera;

        public virtual void Awake()
        {
            _shakeContainer = gameObject.GetComponent<ShakeContainer>();
            _camera = gameObject.GetComponent<Camera>();
        }

        public void Shake(float magnitude, float duration)
        {
            _shakeContainer.Shake(magnitude,duration);
        }

        public static CameraVariant GetCurrentCamera()
        {
            var sceneModel = SceneModel.GetCurrentSceneModel();

            if (sceneModel != null)
            {
                var cam = sceneModel.GetCamera();
                if (cam == null) return null;
                var camVariant = cam.GetComponent<CameraVariant>();
                return camVariant;
            }

            return null;
        }
    }
}