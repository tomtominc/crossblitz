using System.Collections.Generic;
using CrossBlitz.ClientAPI.Visuals;
using Source.Scripts.Card;
using UnityEngine;

namespace CrossBlitz.Card
{
    public static class CardFactory
    {
        public static CardView Create(CreateCardFactorySettings settings, List<CardView> cardList)
        {
            var cardObject = Object.Instantiate(settings.CardPrefab, settings.Layout, false);
            var cardView = cardObject.GetComponent<CardView>();

            cardView.Init(settings.SortingOrder, settings.CardObjectOffsetPosition, settings.StartsDisabled );

            if (!string.IsNullOrEmpty(settings.SortingLayer))
            {
                cardView.SetSortingLayer(settings.SortingLayer);
            }

            if (settings.RemoveCanvas)
            {
                cardView.RemoveCanvas();
            }

            if (settings.RemoveShadow)
            {
                cardView.interactiveShadow.enabled = false;
                cardView.shadow.OffsetDistance = 0;
            }

            var cardComponents = new List<ICardComponent>();

            if (settings.AdditionalComponents != null && settings.AdditionalComponents.Count > 0)
            {
                for (var i = 0; i < settings.AdditionalComponents.Count; i++)
                {
                    var component = (ICardComponent) cardObject.AddComponent(settings.AdditionalComponents[i]);
                    component.Initialize(cardView);
                    cardComponents.Add(component);
                }
            }

            // Setup the cards data; if we're in a game this will be through the
            // card state, otherwise, it's through the card data.
            if (settings.CardState != null)
            {
                var gameCardView = cardView.GetCardComponent<GameCardView>();
                gameCardView.SetCardData(settings.CardState);
            }
            else if (settings.CardData != null)
            {
                cardView.SetCardData(settings.CardData);
            }

            for (var i = 0; i < cardComponents.Count; i++)
            {
                cardComponents[i].OnAfterCreation();
            }

            cardList?.Add(cardView);
            return cardView;
        }
    }
}