using System;
using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AssetManagement;
using CrossBlitz.Audio;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Cursors;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.GameVfx;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.Plugins.TakoBoyStudios.Utilities;
using CrossBlitz.ServerAPI.GameLogic;
using DG.Tweening;
using Source.Scripts.Card;
using UnityEngine;
using UnityEngine.EventSystems;
using Events = CrossBlitz.ClientAPI.GameLogic.EventSystem.Events;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Card
{
    public class HoverCard : MonoBehaviour,
        ICardComponent,
        IPointerEnterHandler,
        IPointerExitHandler,
        IPointerClickHandler,
        IBeginDragHandler,
        IDragHandler,
        IEndDragHandler
    {
        private static bool _isLoadingZoomedCard;
        private static GameObject _zoomedCardPrefab;

        private CardData _cardData;
        private CardView _cardView;
        private CardView _zoomedCard;

        private GameCardView _gameCard;
        private RectTransform _parentRect;

        private int _previousSortOrder;
        private bool _isHovered;
        private float _offsetX;
        private float _zoomedCardDelay;
        private bool _isFocus;
        public bool ForceToolTipsRight { get; set; }
        public bool ForceToolTipsLeft { get; set; }
        public bool ForceToolTipsOpposite { get; set; }

        public bool IsHoverable { get; set; }
        public bool IsLocked { get; set; }

        public bool UsesHoverPunchEffect { get; set; }
        public Vector2 HoverPunchDirection { get; set;  }

        public bool UsesScalingPunchEffect { get; set; }
        public CardView ZoomedCard => _zoomedCard;
        public CardData Data => _cardData;
        public void Initialize(CardView card)
        {
            _cardView = card;
        }

        public void OnAfterCreation()
        {
            if (_cardView.data == null || string.IsNullOrEmpty(_cardView.data.name))
            {
                Destroy(this);
                return;
            }

            _cardData = _cardView.data;
            _parentRect = _cardView.Rect;
            _gameCard = _cardView.GetCardComponent<GameCardView>();

            IsHoverable = true;
            UsesHoverPunchEffect = true;
            HoverPunchDirection = Vector2.down;

            if (_cardView != null)
            {
                CreateZoomedCard(_parentRect);
            }

            if (_gameCard && _gameCard.View)
            {
                _gameCard.View.OnFormattedCardDescription += OnFormattedDescription;
            }
        }

        private void OnFormattedDescription(string description)
        {
            if (_zoomedCard)
            {
                _zoomedCard.descriptionLabel.text = description;
            }
        }

        public void Initialize(CardData cardData, Transform parent, float offsetX)
        {
            _cardData =cardData;
            _parentRect = parent.RectTransform();
            _offsetX = offsetX;

            IsHoverable = true;
            UsesHoverPunchEffect = true;
            HoverPunchDirection = Vector2.down;

            _isFocus = false;

            CreateZoomedCard(_parentRect);
        }

        private async void CreateZoomedCard(Transform parent)
        {
            if (_zoomedCardPrefab == null && !_isLoadingZoomedCard)
            {
                _isLoadingZoomedCard = true;
                _zoomedCardPrefab = await AddressableReferenceLoader.GetAsset("Card_Zoomed",true);
                _isLoadingZoomedCard = false;
            }

            while (_zoomedCardPrefab == null) await new WaitForEndOfFrame();

            var settings = new CreateCardFactorySettings
            {
                CardPrefab = _zoomedCardPrefab,
                Layout = parent.RectTransform(),
                SortingLayer = _cardView == null ? "Default" : _cardView.GetSortingLayer(),
                AdditionalComponents = new List<Type> { typeof(SubCardComponent), typeof(CardControlsComponent) }
            };

            _zoomedCard = CardFactory.Create(settings, null);
            _zoomedCard.SetCardData(_cardData);
            _zoomedCard.CanvasGroup.blocksRaycasts = false;
            _zoomedCard.CanvasGroup.interactable = false;
            _zoomedCard.SetAlpha(0);

            _zoomedCard.SetActive(false);
        }

        private Tweener punchTween;

        public void OnPointerEnter(PointerEventData e)
        {
            if (!IsHoverable)
            {
                return;
            }

            if (GameCardView.Current) return;

            if (GameplayScreen.Instance != null &&
                GameplayScreen.Instance.ScreenState != GameplayScreen.ScreenStateType.NormalUpdate)
            {
                return;
            }

            if (GameServer.IsRunning())
            {
                // fixes a bug with hand cards
                if (_cardView != null &&
                    _cardView.viewType == CardView.CardViewType.STANDARD &&
                    _cardView.transform.localPosition != Vector3.zero)
                {
                    return;
                }
            }

            //if (_cardView != null)
            //{
            //    if (_cardView.data.type == CardType.Resource) return;
            //}

            if (_gameCard != null)
            {
                if (_gameCard.GameState != GameCardView.GameStateType.None) return;
                if (_gameCard.State.Team == Team.Opponent && _gameCard.State.characterData.type == CardType.Trick)
                {
                    return;
                }
            }

            if (_zoomedCard == null || _cardData == null)
            {
                return;
            }

            if (_cardView != null)
            {
                if (_zoomedCard.data.id != _cardView.data.id)
                {
                    _cardData = _cardView.data;
                    _zoomedCard.SetCardData(_cardData);
                }
            }

            KillAllTweens();

            if (punchTween == null || !punchTween.IsActive())
            {
                if (UsesHoverPunchEffect)
                {
                    _parentRect.DOKill(true);
                    punchTween = _parentRect.RectTransform().DOPunchAnchorPos(HoverPunchDirection * 5, 0.2f);
                }
                else if (UsesScalingPunchEffect)
                {
                    _parentRect.DOKill(true);
                    punchTween = _parentRect.RectTransform().DOPunchScale(new Vector3(.1f,-.1f,0), 0.4f);
                }
            }


            AudioController.PlaySound("battle_card_hover", "BARDSFX", true, gameObject);

            if (DeckEditMenu.Instance != null || ManaMeldEditMenu.Instance != null)
            {
                var itemInstance = App.Inventory.GetItem(_cardData.id);

                if (itemInstance != null && _cardView != null)
                {
                    itemInstance.IsNew = false;
                    App.Inventory.SetIsDirty(true);

                    var cardObtainedEffect = _cardView.GetVfx<CardObtainedEffectComponent, CardObtainedEffect>();

                    if (cardObtainedEffect != null)
                    {
                        cardObtainedEffect.SetNewlyObtained(false);
                    }

                    _cardView.EnableUsageGlow(true, _cardView.data.type == CardType.Relic || _cardView.data.type == CardType.Elder_Relic, true);
                }
            }

            //if (_isFocus && GameServer.IsRunning())
            //{
            //    var cardState = GameServer.State.GetCard(_cardData.id);

            //    if (cardState != null && cardState.HasTrigger(TriggerType.FOCUS))
            //    {
            //        var focusAbility = cardState.GetAbility(TriggerType.FOCUS);
            //        var abilityState = cardState.GetAbilityState(focusAbility);

            //        var focusEffect = _cardView.GetVfx<FocusEffectComponent, FocusEffect>();

            //        if (focusEffect != null)
            //        {
            //            focusEffect.SetFocusOn();
            //        }
            //    }
            //}

            if (_cardView)
            {
                var gameCardView = _cardView.GetCardComponent<GameCardView>();

                if (gameCardView != null)
                {
                    if (_zoomedCard.keywordDescriptionPopup)
                    {
                        _zoomedCard.keywordDescriptionPopup.SetCardState(gameCardView.State.uid);
                    }

                    var subCard = _zoomedCard.GetVfx<SubCardComponent, SubCardEffect>();

                    if (subCard)
                    {
                        subCard.SetGameCardUid(gameCardView.State.uid);
                    }
                }
            }

            var canvasRect = this.RectTransform().GetCanvas();
            var canvas = canvasRect.GetComponent<Canvas>();
            var screenSize = CrossBlitzInputModule.Instance.GetNativeScreenSize();
            var zoomedWorldPos = CrossBlitzInputModule.Instance.GetUiPositionAtNativeRes(_parentRect);
            var zoomedSize = _zoomedCard.RectTransform().sizeDelta;
            var previewPosition = new Vector2(zoomedSize.x * 0.66f + _offsetX, 0f);
            var zoomHeightHalved = zoomedSize.y / 2;

            var topMargin = 6;
            var bottomMargin = 24;

            if (zoomedWorldPos.x > screenSize.x/2)
            {
                previewPosition.x = -previewPosition.x;
            }

            if (canvas.renderMode != RenderMode.ScreenSpaceOverlay)
            {
                zoomedWorldPos = canvas.WorldToCanvasPosition(_parentRect.position);

                var cardBottom = zoomedWorldPos.y - zoomHeightHalved;
                var cardTop = zoomedWorldPos.y + zoomHeightHalved;

                var screenUpperBound = screenSize.y * 0.5f - topMargin;
                var screenLowerBound = -screenSize.y * 0.5f + bottomMargin;

                //Debug.LogError($"Preview Pos = {previewPosition} Bottom {cardBottom} Top {cardTop} LBound {screenLowerBound} UBound {screenUpperBound}.");

                if (cardBottom < screenLowerBound)
                {
                    var diff = Mathf.Abs( Mathf.Abs(screenLowerBound) - Mathf.Abs(cardBottom));
                    previewPosition.y += diff;
                }

                if (cardTop > screenUpperBound)
                {
                    var diff = Mathf.Abs(Mathf.Abs(screenUpperBound) - Mathf.Abs(cardTop));
                    previewPosition.y -= diff;
                }

                //Debug.LogError($"Preview Pos After = {previewPosition}");
            }

            if (_cardView)
            {
                _previousSortOrder = _cardView.GetSortOrder();
                _cardView.SetSortOrder(_previousSortOrder + GameCardView.DragSortingOrder);
            }

            // subtracting the margin so the card animates to the preview position
            _zoomedCard.transform.localEulerAngles = -_parentRect.eulerAngles;
            _zoomedCard.RectTransform().anchoredPosition = previewPosition;
            _zoomedCard.SetActive(true);

            _zoomedCard.SetAlpha(0);
            _zoomedCardDelay = GetZoomDelay();

            if (Math.Abs(_zoomedCardDelay - 2f) > float.Epsilon)
            {
                if (_gameCard != null)
                {
                    _zoomedCard.Fade(1, 0.12f, _gameCard.State.IsCardUsableFromHand(), _zoomedCardDelay, _gameCard.State.ConditionalAbilitiesHaveBeenMet(_gameCard.State.uid));
                }
                else
                {
                    _zoomedCard.Fade(1, 0.12f, false, _zoomedCardDelay);
                }
            }

            _zoomedCard.RectTransform().DOAnchorPosY(previewPosition.y, 1f).SetDelay(_zoomedCardDelay).SetEase(Ease.OutBack);

            Events.Publish(this, EventType.OnCardHoveredOver, _zoomedCard.data);

            if (_zoomedCard.keywordDescriptionPopup)
            {
                if (ForceToolTipsRight)
                {
                    _zoomedCard.keywordDescriptionPopup.AnimateIn(1);
                }
                else if (ForceToolTipsLeft)
                {
                    _zoomedCard.keywordDescriptionPopup.AnimateIn(-1);
                }
                else if (ForceToolTipsOpposite)
                {
                    _zoomedCard.keywordDescriptionPopup.AnimateIn(_zoomedCard.RectTransform().position.x > 0 ? 1 : -1);
                }
                else
                {
                    _zoomedCard.keywordDescriptionPopup.AnimateIn(_zoomedCard.RectTransform().position.x > 0 ? -1 : 1);
                }
            }

            _isHovered = true;
        }

        public static float GetZoomDelay()
        {
            return App.Settings.GetZoomedCardPopupDelay() switch
            {
                0 => 0f,
                1 => 0.3f,
                2 => 0.6f,
                3 => 1f,
                4 => 2f,
                _ => 0f
            };
        }

        public void KillMoveTween()
        {
            DOTween.Kill(_zoomedCard.RectTransform());
            _zoomedCard.keywordDescriptionPopup.KillTween();
        }

        public void KillAllTweens()
        {
            if (_zoomedCard != null && _zoomedCard.keywordDescriptionPopup != null)
            {
                DOTween.Kill(_zoomedCard.RectTransform());
                _zoomedCard.keywordDescriptionPopup.KillTween();
            }
        }

        public void OnPointerExit(PointerEventData e)
        {
            if (IsLocked) return;
            if (!_isHovered) return;

            _isHovered = false;

            if (_cardView)
            {
                _cardView.SetSortOrder( _previousSortOrder );

                if (DeckEditMenu.Instance != null || ManaMeldEditMenu.Instance != null)
                {
                    _cardView.EnableUsageGlow(false, false, true);
                }
            }

            KillAllTweens();

            Events.Publish(this, EventType.OnCardHoveredOver, null);

            _zoomedCard.Fade(0, 0.12f, callback: ()=> _zoomedCard.SetActive(false));
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (IsLocked) return;

            if(_zoomedCard !=null)
            {
                _zoomedCard.SetActive(false);
            }
        }

        public void OnDrag(PointerEventData eventData)
        {

        }

        public void OnEndDrag(PointerEventData eventData)
        {

        }

        public void OnDestroy()
        {
            if (_zoomedCard)
            {
                DOTween.Kill(_zoomedCard.front.image.material);
                DOTween.Kill(_zoomedCard.CanvasGroup);
                DOTween.Kill(_zoomedCard.RectTransform());
            }

            if (_zoomedCard && _zoomedCard.gameObject)
            {
                AddressableReferenceLoader.Unload(_zoomedCard.gameObject);
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Right)
            {
                if (_zoomedCard)
                {
                    var subCard = _zoomedCard.GetVfx<SubCardComponent, SubCardEffect>();
                    if (subCard) subCard.MoveToNextCard();
                }

                if (_cardView)
                {
                    var subCard = _cardView.GetVfx<SubCardComponent, SubCardEffect>();
                    if (subCard) subCard.MoveToNextCard();
                }

                if (GameplayScreen.Instance && _gameCard)
                {
                    GameplayScreen.Instance.simpleCardHistory.Open(_gameCard.State);
                }
            }
        }
    }
}
