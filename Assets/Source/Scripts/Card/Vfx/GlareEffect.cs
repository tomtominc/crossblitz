using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class GlareEffect : MonoBehaviour, IGameVfx
    {
        private const int FadeFrames = 3;
        private const float FrameTime = 0.08f;
        public SpriteAnimation glareEyeAnim;
        public SpriteAnimation glareAuraAnim;
        public Color overlayColor;

        private CardView _card;
        private GameCardView _gameCard;

        private RectTransform m_rect;

        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }
        public void Initialize(CardView card)
        {
            _card = card;
            _gameCard = _card.GetCardComponent<GameCardView>();
        }

        /// <summary>
        /// Play is used when spawning the effect on top of the card.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            gameObject.SetActive(true);

            var fadeTime = FadeFrames * FrameTime;

            // Play Eyeball and dark overlay
            _card.TintOverTime(overlayColor, BlendMode.DarkerColor, fadeTime);
            glareEyeAnim.Play("glare");
            while(glareEyeAnim.CurrentFrame < 9) yield return Timing.WaitForOneFrame;

            // Play aura effect
            glareAuraAnim.SetActive(true);
            glareAuraAnim.Play("glare");
            AudioController.PlaySound("vfx_purple_energy", "BARDSFX", true, gameObject);
            while (glareEyeAnim.CurrentFrame < 16) yield return Timing.WaitForOneFrame;

            // Remove the dark overlay while eyeball closes
            yield return Timing.WaitUntilDone(_card.RemoveTintOverTime(fadeTime).WaitForCompletion(true));

            Destroy(gameObject);

        }
    }
}