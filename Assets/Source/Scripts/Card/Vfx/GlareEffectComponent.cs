using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class GlareEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<GlareEffect>("GlareEffect", card.transform);
            effect.Initialize(card);
        }
    }
}