using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Databases;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using DG.Tweening;
using TakoBoyStudios.Events;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.GameVfx
{
    public class OpponentPlayedCardPreview : MonoBehaviour
    {
        public const float PlayedCardFadeAfterSeconds = 3;

        public CanvasGroup canvas;
        public RectTransform cardLayout;
        public RectTransform cardPoint;
        public GameObject cardZoomedPrefab;

        private GameCardView m_gameCardView;
        private CardView m_cardView;
        private float m_fadeTime;
        public bool disabled;

        private bool m_hasNewEffects;

        public event Action<GameCardView> OnCardShown;

        private void Start()
        {
            m_fadeTime = 0;
            canvas.alpha = 0;

            Events.Subscribe(EventType.OnCardPlayed, OnCardPlayed);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnCardPlayed, OnCardPlayed);
        }

        public void SetCanvasAlpha(float alpha)
        {
            canvas.alpha = alpha;
        }

        public void SetCardPreviewAlpha(float alpha)
        {
            // if (m_cardView)
            // {
            //     m_cardView.CanvasGroup.alpha = alpha;
            // }
        }

        public void ActivateFade()
        {
            m_fadeTime = PlayedCardFadeAfterSeconds;
            disabled = false;
        }

        private void Update()
        {
            if (disabled)
            {
                return;
            }

            if (canvas.alpha > 0 && m_fadeTime > 0)
            {
                m_fadeTime -= Time.deltaTime;

                if (m_fadeTime <= 0)
                {
                    canvas.DOFade(0, 0.24f);
                }
            }
        }

        private void OnCardPlayed(IMessage message)
        {
            if (message.Data is OnCardPlayedEventArgs args)
            {
                if (!args.isClientCard)
                {
                    var card = GameServer.State.GetCard(args.cardUid);

                    if (card == null)
                    {
                        return;
                    }

                    if (card.characterData.type == CardType.Spell)
                    {
                        return;
                    }

                    if (card.characterData.type == CardType.Power)
                    {
                        return;
                    }

                    if (m_gameCardView != null && m_gameCardView.State.uid == card.uid)
                    {
                        return;
                    }

                    SpawnCardPreview(card);
                }
            }
        }

        public void SpawnCardPreview(string cardId)
        {
            if (m_gameCardView != null)
            {
                Destroy(m_gameCardView.gameObject);
                m_gameCardView = null;
            }
            var card = Db.CardDatabase.GetCard(cardId);

            if (m_cardView == null)
            {
                if (m_cardView)
                {
                    Destroy(m_cardView.gameObject);
                }

                var createSettings = new CreateCardFactorySettings
                {
                    CardData = card,
                    CardPrefab = cardZoomedPrefab,
                    Layout = cardLayout,
                    StartsDisabled = false,
                    AdditionalComponents = new List<Type> ()
                };

                m_cardView = CardFactory.Create(createSettings, null);
                m_cardView.GetComponent<CanvasGroup>().interactable = false;
                m_cardView.GetComponent<CanvasGroup>().blocksRaycasts = false;
            }

            m_cardView.SetActive(true);
            m_cardView.SetCardData(card);

            canvas.DOKill();
            canvas.alpha = 0;
            canvas.DOFade(1, 0.1f);

            m_cardView.Rect.DOKill();
            m_cardView.Rect.anchoredPosition = new Vector2(40, 0);
            m_cardView.Rect.localScale = Vector3.zero;
            m_cardView.CanvasGroup.alpha = 1;
            m_cardView.Rect.DOScale(1, 0.4f).SetEase(Ease.OutBack);
            m_cardView.Rect.DOAnchorPos(Vector2.zero, 0.24f).SetEase(Ease.OutBack);

            m_fadeTime = PlayedCardFadeAfterSeconds;
        }

        public void SpawnCardPreview(GameCardState card, params Type[] otherEffects)
        {
            if (card.characterData.type == CardType.Trick)
            {
                return;
            }

            if (m_cardView != null)
            {
                Destroy(m_cardView.gameObject);
                m_cardView = null;
            }

            if (m_gameCardView == null || m_hasNewEffects || otherEffects.Length > 0)
            {
                if (m_gameCardView)
                {
                    Destroy(m_gameCardView.gameObject);
                }

                var createSettings = new CreateCardFactorySettings
                {
                    CardState = card,
                    CardPrefab = cardZoomedPrefab,
                    Layout = cardLayout,
                    StartsDisabled = false,
                    AdditionalComponents = new List<Type> { typeof(GameCardView) }
                };

                if (otherEffects.Length > 0)
                {
                    m_hasNewEffects = true;
                    createSettings.AdditionalComponents.AddRange(otherEffects);
                }
                else
                {
                    m_hasNewEffects = false;
                }

                var cardView = CardFactory.Create(createSettings, null);

                m_gameCardView = cardView.GetCardComponent<GameCardView>();
                m_gameCardView.displayRealGameStatsOnZoomedView = true;
                m_gameCardView.GetComponent<CanvasGroup>().interactable = false;
                m_gameCardView.GetComponent<CanvasGroup>().blocksRaycasts = false;
            }

            m_gameCardView.SetActive(true);
            m_gameCardView.SetCardData(card);

            canvas.DOKill();
            canvas.alpha = 0;
            canvas.DOFade(1, 0.1f);

            m_gameCardView.Rect.DOKill();
            m_gameCardView.Rect.anchoredPosition = new Vector2(40, 0);
            m_gameCardView.Rect.localScale = Vector3.zero;
            m_gameCardView.CanvasGroup.alpha = 1;
            m_gameCardView.Rect.DOScale(1, 0.4f).SetEase(Ease.OutBack);
            m_gameCardView.Rect.DOAnchorPos(Vector2.zero, 0.24f).SetEase(Ease.OutBack);

            m_fadeTime = PlayedCardFadeAfterSeconds;

            OnCardShown?.Invoke(m_gameCardView);
        }
    }
}