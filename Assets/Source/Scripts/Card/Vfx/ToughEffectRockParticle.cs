using System;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.GameVfx.Minion
{
    public class ToughEffectRockParticle : MonoBehaviour
    {
        public float gravity = -10;
        public float groundOffset = -32;
        public Vector2 initialVelocityMagnitude ;
        public SpriteAnimation animator;
        public CanvasGroup canvasGroup;

        // the velocity of the rock
        private Vector2 _velocity;
        // cached transform
        private RectTransform _rectTransform;
        // where the "ground" is
        private float _groundY;
        // bool to start fading
        private bool _updatePhysics;
        // the amount the object was pushed
        private float _velocityPush;

        public void Play(float direction)
        {
            // caching the rect transform for optimization reasons.
            _rectTransform = transform as RectTransform;
            // get the ground position of the object (which is its spawn y)
            _groundY = _rectTransform.anchoredPosition.y+groundOffset;
            // get a random amount of "push" force so all the rocks can be slightly different.
             _velocityPush = Random.Range(initialVelocityMagnitude.y, initialVelocityMagnitude.y * 2);
            // sets the velocity of the rock upward and in the direction of the spawn (x velocity should be halved to get a nice arc)
            _velocity = new Vector2(direction*initialVelocityMagnitude.x/2, _velocityPush);
            // play a random particle effect from the list.
            animator.Play($"spawn-{Random.Range(1, 4)}");
            // allow the rock to update its physics
            _updatePhysics = true;
        }

        private void Update()
        {
            if (!_updatePhysics)
            {
                return;
            }
            // add gravity to the rock
            _velocity += Vector2.up * (gravity * Time.deltaTime);
            // move the rect transform by velocity
            _rectTransform.anchoredPosition += _velocity;

            if (_rectTransform.anchoredPosition.y <= _groundY && _velocity.y < 0)
            {
                Bounce();
            }
        }

        private void Bounce()
        {
            _velocityPush = _velocityPush * 0.5f;
            // add another "push" by half the initial amount to simulate a "bounce" effect
            _velocity = new Vector2(_velocity.x*0.5f, _velocityPush);

            // check if the velocity is really small in either the y or x direction, if it is lets just stop it and fade it out.
            if (_velocity.y <= 0.1f)
            {
                _velocity = Vector2.zero;
                _updatePhysics = false;
                canvasGroup.DOFade(0, 0.5f).OnComplete(OnRockFinishedFade);
            }
        }

        private void OnRockFinishedFade()
        {
            Destroy(gameObject);
        }
    }
}