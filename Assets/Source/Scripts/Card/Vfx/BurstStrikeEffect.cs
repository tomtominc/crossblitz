using System;
using System.Collections;
using System.Collections.Generic;
using Asyncoroutine;
using BlendModes;
using CrossBlitz.AssetManagement;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.Transition;
using DG.Tweening;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.GameVfx.Spell
{
    public class BurstStrikeEffect : MonoBehaviour, IGameVfx
    {
        private const float Inset = -50f;
        private const float MaxTime = 5;

        public RectTransform boundsRect;
        public SpriteAnimation baseAnim;
        public SpriteAnimationAsset projectileAnim;
        public CanvasGroup canvas;
        public BurstStrikeProjectileEffect projectilePrefab;

        private RectTransform _rect;

        private DamageCommand _command;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {

        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {

            _command = command as DamageCommand;
            _rect = canvas.RectTransform();

            var historyCard = _command.PendingDamageHistories[0];
            var hurtCard = GameServer.State.GetCard(historyCard.CardUid);

            // Set Position of the Effect
            var getBoardCardTask = GameBoard.GetBoardCard(hurtCard.uid);
            yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
            var boardCardResult = getBoardCardTask.Result;
            _rect.position = new Vector2(0f, boardCardResult.boardCard.transform.position.y);

            // Turn on the effect
            baseAnim.SetActive(true);
            baseAnim.Play("slash");
            AudioController.PlaySound("vfx_dark_energy", "BARDSFX", true, gameObject);
            while (baseAnim.CurrentFrame < 6) yield return Timing.WaitForOneFrame;

            GameplayScreen.Instance.ShakeScreen(4, 2);
            TransitionController.FlashScreen(Color.magenta, BlendMode.Color);

            // Spawn and shoot out the projectile
            var projectile = Instantiate(projectilePrefab, _rect.RectTransform(), false);
            Timing.RunCoroutine(projectile.Play(command, vfxParams));

            AudioController.PlaySound("vfx_wind", "BARDSFX", true, gameObject);

            while (!baseAnim.IsDone) yield return Timing.WaitForOneFrame;
            baseAnim.SetActive(false);

            while (!projectile.isDone) yield return Timing.WaitForOneFrame;
            Destroy(projectile);
            Destroy(gameObject);

        }

        public void Unload()
        {
            AddressableReferenceLoader.Unload(gameObject);
        }
    }
}