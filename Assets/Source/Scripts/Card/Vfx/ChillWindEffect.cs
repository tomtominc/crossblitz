using System;
using System.Collections;
using System.Collections.Generic;
using Asyncoroutine;
using BlendModes;
using CrossBlitz.AssetManagement;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.Transition;
using DG.Tweening;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.GameVfx.Spell
{
    public class ChillWindEffect : MonoBehaviour, IGameVfx
    {
        private const float Inset = -50f;
        private const float MaxTime = 5;

        public RectTransform boundsRect;
        public SpriteAnimationAsset projectileAnim;
        public CanvasGroup canvas;
        public Color overlayColorBlue;
        public GameObject gustParticle;
        public ChillWindPuffParticle puffParticle;
        public ChillWindIceParticle iceParticle;

        private RectTransform _rect;

        private DamageCommand _command;
        private List<GameCardView> _boardCardResults;
        private GameBoard gameboard;

        private RectTransform m_rect;
        private float _particleTimerGust = 0.0f;
        private float _particleTimerPuff = 0.0f;
        private float _particleTimerIce = 0.0f;
        private bool _spawnParticles = false;
        private RectTransform _hurtCardRect;

        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {

        }

        public void Update()
        {

            if (_spawnParticles)
            {
                _particleTimerGust += Time.deltaTime;
                _particleTimerPuff += Time.deltaTime;
                _particleTimerIce += Time.deltaTime;
            }

            // Spawn Gust Particles
            if (_particleTimerGust >= 0.2f)
            {
                var particle = Instantiate(gustParticle, _rect.RectTransform(), false);

                var x = _rect.RectTransform().position.x;
                var y = Random.Range(_rect.RectTransform().position.y - 30f, _rect.RectTransform().position.y + 30f);
                particle.RectTransform().localPosition = new Vector2(x, y);

                _particleTimerGust = 0f;
            }

            // Spawn Puff Particles
            if (_particleTimerPuff >= 0.5f)
            {
                var particle = Instantiate(puffParticle, _rect.RectTransform(), false);

                var x = _rect.RectTransform().position.x-60f;
                var y = Random.Range(_rect.RectTransform().position.y - 30f, _rect.RectTransform().position.y + 30f);
                particle.RectTransform().localPosition = new Vector2(x, y);
                particle.SetupParticle();

                _particleTimerPuff = 0f;
            }

            // Spawn Ice Particles
            if (_particleTimerIce >= 0.25f)
            {
                var particle = Instantiate(iceParticle, _rect.RectTransform(), false);

                var x = _rect.RectTransform().position.x - 60f;
                var y = Random.Range(_rect.RectTransform().position.y - 30f, _rect.RectTransform().position.y + 30f);
                particle.RectTransform().localPosition = new Vector2(x, y);
                particle.SetupParticle();

                _particleTimerIce = 0f;
            }
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {

            //_command = command as DamageCommand;
            _boardCardResults = new List<GameCardView>();
            gameboard = GameplayScreen.Instance.gameBoard;
            _rect = canvas.RectTransform();

            //var historyCard = _command.;
            var hurtCard = GameServer.State.GetCard(command.TargetUids[0]);

            // Set Position of the Effect
            var getBoardCardTask = GameBoard.GetBoardCard(hurtCard.uid);
            yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
            var boardCardResult = getBoardCardTask.Result;
            _rect.position = new Vector2(-2f, boardCardResult.boardCard.transform.position.y);


            yield return Timing.WaitForSeconds(0.5f);
            GameplayScreen.Instance.ShakeScreen(4, 2);
            TransitionController.FlashScreen(Color.cyan, BlendMode.Color);
            AudioController.PlaySound("vfx_wind", "BARDSFX", true, gameObject);

            // Spawn and shoot out the projectiles
            _spawnParticles = true;

            // Get all Damaged Cards
            for (var i = 0; i < command?.TargetUids.Count; i++)
            {
                var getBoardCardTargetTask = GameBoard.GetBoardCard(command.TargetUids[i]);
                yield return Timing.WaitUntilDone(getBoardCardTargetTask.AsCoroutine());
                var getBoardCardTargetResult = getBoardCardTargetTask.Result;

                if (getBoardCardTargetResult.error != ClientErrorCode.None)
                {
                    Server.HandleClientError(getBoardCardTargetResult, false);
                }

                if (getBoardCardTargetResult.boardCard)
                {
                    _boardCardResults.Add(getBoardCardTargetResult.boardCard);

                    // get a reference to the card object (child object of the card so we aren't moving the card directly.
                    var cardRect = getBoardCardTargetResult.boardCard.View.cardObject;

                    // Make the cards shake and slowy turn blue, then when all summoned particles die, do the damage
                    var origAnchorPos = cardRect.anchoredPosition;
                    cardRect.DOShakeAnchorPos(2f, 5, 50, 0).OnComplete(() => cardRect.DOAnchorPos(origAnchorPos, 0.1f));
                    getBoardCardTargetResult.boardCard.View.TintOverTime(overlayColorBlue, BlendMode.LighterColor, 2f);
                }
            };

            //while (!baseAnim.IsDone) yield return Timing.WaitForOneFrame;
            //baseAnim.SetActive(false);

            yield return Timing.WaitForSeconds(1f);
            _spawnParticles = false;

            yield return Timing.WaitForSeconds(1f);

            // Do Damage to cards
            for (var i = 0; i < _boardCardResults.Count; i++)
            {
                //_boardCardResults[i].State.ApplyHistoryByCardGuid(_command.SourceUid);
                //var damageFx = _boardCardResults[i].View.GetVfx<DamageEffectComponent, DamageEffect>();
                //Timing.RunCoroutine(damageFx.GenericDamageAnimation(_command.Amount));

                // Remove Tint
                _boardCardResults[i].View.RemoveTintOverTime(0.5f);

            }

            //yield return Timing.WaitUntilDone(projectileCanvas.DOFade(0, 0.5f).WaitForCompletion(true));

            yield return Timing.WaitForSeconds(0.5f);

            Destroy(gameObject);

        }

        public void Unload()
        {
            AddressableReferenceLoader.Unload(gameObject);
        }
    }
}