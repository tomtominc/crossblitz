using CrossBlitz.GameVfx;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class CardScrappedComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<CardScrappedEffect>("CardScrappedEffect", card.transform);
            effect.Initialize(card);
        }
    }
}