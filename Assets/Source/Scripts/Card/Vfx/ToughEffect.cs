using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class ToughEffect : MonoBehaviour, IGameVfx
    {
        private const int TotalFrames = 18;
        private const float FrameTime = 0.08f;
        public CanvasGroup rockOverlayCanvas;
        public RectTransform rockParticleParent;
        public ToughEffectRockParticle rockParticlePrefab;

        private CardView _card;
        private GameCardView _gameCard;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }
        public void Initialize(CardView card)
        {
            _card = card;
            _gameCard = _card.GetCardComponent<GameCardView>();
            _gameCard.Events.OnSilenced += OnSilenced;
        }

        private void OnSilenced(GameCardView cardView)
        {
            rockOverlayCanvas.alpha = 0;
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Play is used when spawning the effect on top of the card.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            // by default the effects are always inactive, we'll need to make them active when we play them.
            gameObject.SetActive(true);

            // get a reference to the card object (child object of the card so we aren't moving the card directly.
            var cardRect = _card.cardObject;

            // the overlay should fade in over the lifetime of the animation, in this case its TotalFrames * FrameTime
            rockOverlayCanvas.alpha = 0;
            rockOverlayCanvas.DOFade(1, TotalFrames * FrameTime);
            AudioController.PlaySound("vfx_tough", "BARDSFX", true, gameObject);

            // Create a list to hold the rock particles we create
            var rockParticles = new List<ToughEffectRockParticle>();

            // Every "jerk" or offset will fling a particle off the card to give the animation more impact.
            for (var i = 0; i < TotalFrames/2; i++)
            {
                var rockParticle = Instantiate(rockParticlePrefab, rockParticleParent, false);
                rockParticle.SetActive(false);
                rockParticles.Add(rockParticle);
            }

            // the spawn locations of the rock should be dictated by the parents rect (to make it easier to adjust if needed)
            var rockSpawnBounds = rockParticleParent.sizeDelta;

            // loop and move the card back and forth on x
            for (var i = 0; i < TotalFrames; i++)
            {
                var moveDirection = i % 2 == 0 ? 1 : -1;
                cardRect.anchoredPosition = new Vector2(moveDirection * 2, 0);

                if (rockParticles.Count > i)
                {
                    // get the "next" rock
                    var rockParticle = rockParticles[i];
                    rockParticle.SetActive(true);
                    // get the spawn points, the x value min is just a little bit off from the center (so we don't spawn exactly in the center)
                    // while the x value max is the very edge of the card.
                    // and finally, we multiply it by the move direction so its going in the same direction as the card.
                    var spawnX = Random.Range(0, rockSpawnBounds.x / 2) * moveDirection;
                    var spawnY = Random.Range(0, rockSpawnBounds.y);

                    rockParticle.RectTransform().anchoredPosition = new Vector2(spawnX, spawnY);
                    rockParticle.Play(moveDirection);
                }

                // hold the position for FrameTime so the movement is not instant.
                yield return Timing.WaitForSeconds(FrameTime);
            }

            // reset the position of the card
            cardRect.anchoredPosition = Vector2.zero;
        }
    }
}