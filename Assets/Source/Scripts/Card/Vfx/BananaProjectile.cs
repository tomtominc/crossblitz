using System;
using System.Collections.Generic;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.Models;
using DG.Tweening;
using MEC;
using UnityEngine;
using Asyncoroutine;
using Random = UnityEngine.Random;

namespace CrossBlitz.GameVfx.Minion
{
    public class BananaProjectile : MonoBehaviour
    {
        private const float MinJumpHeight = 0.75f;
        private const float MaxJumpHeight = 1f;
        private const float JumpTimePerMeter = 0.25f;

        public SpriteAnimation animator;

        private float _jumpHeight;
        private float _timeToJumpApex;
        private bool _updateRotate;
        private float _timeInAir;
        private float _maxDistance;
        private float _rotateAngle;
        private float _rotateSpeed;
        Vector3 _midPoint;

        public void Update()
        {
            if (_updateRotate)
            {
                _rotateAngle += _rotateSpeed;
                transform.rotation = Quaternion.Euler(new Vector3(0, 0, _rotateAngle));
            }
        }

        public IEnumerator<float> JumpToTile(Vector3 startPos, Vector3 targetPos, GetBoardCardResult getTargetCardResult, ModifyHealthAndPowerCommand powerCommand, int index, Action<BananaProjectile> OnFinished)
        {

            var distance = Vector3.Distance(startPos, targetPos);

            _rotateAngle = Random.Range(0, 360);
            _rotateSpeed = 60f;

            _updateRotate = true;
            animator.paused = true;

            _jumpHeight = Mathf.Clamp(distance, MinJumpHeight, MaxJumpHeight);
            _timeToJumpApex = _jumpHeight * JumpTimePerMeter;
            _timeInAir = 0;
           // _updateJump = true;

            _maxDistance = Vector3.Distance(transform.position, targetPos);

            yield return Timing.WaitUntilDone(transform.DOMove(targetPos, _timeToJumpApex).SetEase(Ease.InSine)
            .SetUpdate(UpdateType.Fixed).WaitForCompletion(true));

            //_updateJump = false;
            transform.localScale = Vector3.one;
            animator.paused = false;
            _rotateSpeed = 0f;


            while (!animator.IsDone) yield return Timing.WaitForOneFrame;
            Destroy(gameObject);

            getTargetCardResult.boardCard.State.ApplyHistory(powerCommand.PendingPowerHistories[index].Uid);
            var getModifyEffectTask = getTargetCardResult.boardCard.GetVfx<ModifyHealthAndPowerEffectComponent>();
            yield return Timing.WaitUntilDone(getModifyEffectTask.AsCoroutine());
            var getModifyEffectResult = getModifyEffectTask.Result;
            //todo figure out if modifier is from own ability
            //Timing.RunCoroutine( getBoardCardResult.boardCard.AnimateEffect<GenericEffectComponent>(null));
            yield return Timing.WaitUntilDone(((ModifyHealthAndPowerEffect)getModifyEffectResult.Effect).PlayModifyAnimation(powerCommand.Power, powerCommand.Health));
            getTargetCardResult.boardCard.UpdateViewFromState();

            OnFinished?.Invoke(this);


        }
    }
}