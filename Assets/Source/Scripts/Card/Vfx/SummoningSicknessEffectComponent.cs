using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class SummoningSicknessEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<SummoningSicknessEffect>("SummoningSicknessEffect", card.transform);
            effect.Initialize(card);
        }
    }
}