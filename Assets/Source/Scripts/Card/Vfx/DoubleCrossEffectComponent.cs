using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class DoubleCrossEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<DoubleCrossEffect>("DoubleCrossEffect", card.cardObject);
            effect.Initialize(card);
        }
    }
}