using System.Collections.Generic;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.GameVfx;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    // Deprecated, not needed!
    public class AddCardToDeckEffect : MonoBehaviour, IGameVfx
    {
        private bool m_animating;
        private Queue<string> m_cardQueue;

        public void Initialize(CardView card)
        {

        }

        public void Initialize(Transform rect)
        {

        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            yield break;
        }

        public void QueueCard( string cardUid  )
        {
            m_cardQueue ??= new Queue<string>();
            m_cardQueue.Enqueue(cardUid);

            if (!m_animating)
            {
                Timing.RunCoroutine(AnimateAdd());
            }
        }

        private IEnumerator<float> AnimateAdd()
        {
            m_animating = true;

            var nextCardUid = m_cardQueue.Peek();
            var nextCardState = GameServer.State.GetCard(nextCardUid);
            var nextCardName = nextCardState.characterData.name;
            var nextCardCount = 0;

            while (m_cardQueue.Count > 0)
            {
                var queueCardUid = m_cardQueue.Peek();
                var queueCardState = GameServer.State.GetCard(queueCardUid);
                var queueCardName = queueCardState.characterData.name;

                if (queueCardName != nextCardName) break;

                m_cardQueue.Dequeue();
                nextCardCount++;
            }

            if (nextCardCount > 0)
            {
                yield break;
            }
        }
    }
}