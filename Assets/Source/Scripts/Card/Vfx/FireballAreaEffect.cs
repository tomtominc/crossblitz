using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.GameVfx;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    /// <summary>
    /// A game board effect, should not be spawned on top of a card. The ability can handle targeting and sources
    /// </summary>
    public class FireballAreaEffect : MonoBehaviour, IGameVfx
    {
        public float multiShotDelay = 0.24f;
        public FireballEffect fireballEffectPrefab;

        public void Initialize(CardView card)
        {

        }

        public void Initialize(Transform rect)
        {

        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            gameObject.SetActive(true);

            var getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(command);
            yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
            var vfxData = getSourceAndTargetData.Result;
            var handles = new List<CoroutineHandle>();

            for (var i = 0; i < vfxData.targetCards.Count; i++)
            {
                var fireballEffect = Instantiate(fireballEffectPrefab, transform, false);
                handles.Add(Timing.RunCoroutine(fireballEffect.Play(vfxData, command, i)));
                yield return Timing.WaitForSeconds(multiShotDelay);
            }

            while (handles.Exists(handle => handle.IsRunning)) yield return Timing.WaitForOneFrame;

            yield return Timing.WaitForOneFrame;

            Destroy(gameObject);
        }
    }
}