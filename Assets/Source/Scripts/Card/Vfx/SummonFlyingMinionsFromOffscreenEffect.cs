using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Spell
{
    public class SummonFlyingMinionsFromOffscreenEffect : MonoBehaviour, IGameVfx
    {
        private CardView _card;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            gameObject.SetActive(true);

            var getBoardCardTask =AddressableReferenceLoader.GetAsset("Card_Board", true);
            yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
            var boardCardPrefab = getBoardCardTask.Result;

            var settings = new CreateCardFactorySettings
            {
                CardPrefab = boardCardPrefab,
                StartsDisabled = true
            };

            var summonCommand = (SummonCommand) command;

            // Randomize summoning so it looks cooler (not needed for every summon method / effect)
            var count = summonCommand.Positions.Count;
            var last = count - 1;

            for (var i = 0; i < last; ++i)
            {
                var r = Random.Range(i, count);
                var tmp = summonCommand.Positions[i];
                var tmp0 = summonCommand.TargetUids[i];
                summonCommand.Positions[i] = summonCommand.Positions[r];
                summonCommand.TargetUids[i] = summonCommand.TargetUids[r];
                summonCommand.Positions[r] = tmp;
                summonCommand.TargetUids[r] = tmp0;
            }

            for (var i = 0; i < summonCommand.Positions.Count; i++)
            {
                var position = summonCommand.Positions[i];
                var cardState = GameServer.State.GetCard( summonCommand.TargetUids[i] );
                var tile = GameplayScreen.Instance.GetBoardTile(position);

                settings.CardState = cardState;
                settings.Layout = tile.RectTransform();
                settings.AdditionalComponents = GameVfxComponent.GetRequiredVfxComponents(cardState);

                // settings.AdditionalComponents.Add(typeof(GreenSummonEffectComponent));

                var boardCard = CardFactory.Create(settings, null);
                var boardCardGameView = boardCard.GetCardComponent<GameCardView>();

                while (!boardCard.AllVfxComponentsLoaded()) yield return Timing.WaitForOneFrame;

                boardCard.GetVfx<AttackEffectComponent, AttackEffect>().SetupDirection(boardCardGameView.State.Team);
                boardCard.SetActive(true);

                yield return Timing.WaitUntilDone(boardCardGameView.AnimateSummonOnTile(tile, cardState, SummonMinionData.SummonVisualEffect.FromOutsideScreen, summonCommand.SummonMethod));

                GameplayScreen.Instance.SetBoardTile(boardCardGameView, position);
                vfxParameters.summons.Add(boardCardGameView);
            }
        }
    }
}