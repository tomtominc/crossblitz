using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class DeathrattleEffect : MonoBehaviour,IGameVfx
    {
        public SpriteAnimation deathrattle;

        private CardView _card;
        private GameCardView _gameCard;
        private GameTile _tile;
        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
            _gameCard = card.GetCardComponent<GameCardView>();
            _tile = _gameCard.TileState?.GetView();
            if (_tile) transform.SetParent(_tile.effectsContainer);
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            gameObject.SetActive(true);
            deathrattle.SetActive(true);
            deathrattle.Play("spawn");
            AudioController.PlaySound("battle_trigger_deathrattle", "BARDSFX", true, gameObject);
            while (!deathrattle.IsDone) yield return Timing.WaitForOneFrame;
            deathrattle.Play("ping");
            while (!deathrattle.IsDone) yield return Timing.WaitForOneFrame;
            deathrattle.Play("despawn");
            while (!deathrattle.IsDone) yield return Timing.WaitForOneFrame;
            gameObject.SetActive(false);
        }
    }
}