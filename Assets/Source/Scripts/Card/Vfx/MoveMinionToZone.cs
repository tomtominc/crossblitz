using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Data;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Commands
{
    public class MoveMinionToZone : Command
    {
        public Team TeamLocation;
        public CardLocation NewLocation;

        public MoveMinionToZone() => Type = CommandType.MOVE_MINION_TO_ZONE;

        public override void OnExecuteCommand()
        {
            base.OnExecuteCommand();

            var player = GameServer.GetPlayerState(TeamLocation);

            for (var i = 0; i < TargetUids.Count; i++)
            {
                var targetUid = TargetUids[i];
                var location = NewLocation;

                var card = GameServer.State.GetCard(targetUid);

                if (card.Team != TeamLocation)
                {
                    card.ChangeOwners(TeamLocation);
                }

                if (location == CardLocation.Hand && player.GetHandCount() >= PlayerBattleState.MaxHandSize)
                {
                    location = CardLocation.Graveyard;
                }

                GameServer.State.Board.RemoveCardFromTile(targetUid, location);

                if (location == CardLocation.Hand)
                {
                    player.AddCardToHand(targetUid, false);
                }
                else if (location == CardLocation.Deck)
                {
                    player.AddBackToDeck(targetUid);
                }
            }
        }

        public override IEnumerator<float> Resolve()
        {
            for (var i = 0; i < TargetUids.Count; i++)
            {
                var targetUid = TargetUids[i];

                if (string.IsNullOrEmpty(targetUid))
                {
                    yield break;
                }

                var cardTask = GameBoard.GetBoardCard(targetUid);
                yield return Timing.WaitUntilDone(cardTask.AsCoroutine());
                var card = cardTask.Result.boardCard;

                if (card == null)
                {
                    yield break;
                }

                var reference = AddressableReferenceLoader.GetAsset("Card_Standard", true);
                yield return Timing.WaitUntilDone(reference.AsCoroutine());

                var settings = new CreateCardFactorySettings
                {
                    CardPrefab = reference.Result,
                    Layout = GameplayScreen.Instance.handView.RectTransform(),
                    AdditionalComponents = CardView.GetRequiredComponentsForStandardHandCard(),
                    StartsDisabled = true,
                    CardState = card.State,
                    SortingOrder = GameCardView.HandSortingOrder + 20
                };

                var standardCardView = CardFactory.Create(settings, null);
                standardCardView.transform.position = card.transform.position;
                standardCardView.SetAlpha(0);
                standardCardView.SetActive(true);

                yield return Timing.WaitUntilDone(card.TransformToStandardCardViewFromBoardCard(standardCardView));

                switch (card.State.location)
                {
                    case CardLocation.Hand:
                        if (card.State.Team == Team.Client)
                        {
                            GameplayScreen.Instance.handView.TrackCard(standardCardView.GetCardComponent<GameCardView>());
                            yield return Timing.WaitForSeconds(0.5f);
                        }
                        else
                        {
                            GameplayScreen.Instance.opponentHandView.TrackCard(standardCardView.GetCardComponent<GameCardView>());
                            yield return Timing.WaitForSeconds(0.5f);
                        }

                        standardCardView.SetInteractable(true);
                        break;
                    case CardLocation.Graveyard:
                        var cardZoomedPrefab = AddressableReferenceLoader.
                            GetAsset("Card_Zoomed", true);
                        yield return Timing.WaitUntilDone(cardZoomedPrefab.AsCoroutine());
                        yield return Timing.WaitUntilDone(DrawCardCommand.
                            Overdraw(reference.Result, targetUid, standardCardView));
                        break;
                    case CardLocation.Deck:
                        yield return Timing.WaitUntilDone(AddCardToZoneCommand.
                                AnimateStandardCardToDeck(standardCardView.
                                    GetCardComponent<GameCardView>(), GetTeam()));
                        break;
                }

                Object.Destroy(card.gameObject);
            }
        }
    }
}