using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI.GameLogic.Data;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class FireballEffect : MonoBehaviour
    {
        public float moveSpeed = 20;
        public SpriteAnimation fireBall;

        private GameCardView m_sourceCard;
        private GameCardView m_targetCard;

        public IEnumerator<float> Play( VfxSourceAndTargetData vfxData, Command command, int targetIndex )
        {
            gameObject.SetActive(true);

            var sourcePosition = vfxData.sourceCardPosition;
            m_sourceCard = vfxData.sourceCardObject;

            var targetPosition = vfxData.targetCardPositions[targetIndex];
            m_targetCard = vfxData.targetCardObjects[targetIndex];

            // fire ball start
            fireBall.SetActive(true);
            fireBall.transform.position = sourcePosition;
            fireBall.Play("spawn");

            // rotate fireball towards target
            var dir = (targetPosition - fireBall.transform.position).normalized;
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            fireBall.transform.eulerAngles = new Vector3(0, 0, angle+270);

            while (!fireBall.IsDone) yield return Timing.WaitForOneFrame;

            GameplayScreen.Instance.ShakeScreen();

            fireBall.Play("idle");

            AudioController.PlaySound("vfx_wind", "BARDSFX", true, gameObject).setPitch(0.75f);

            if (m_sourceCard != null)
            {
                m_sourceCard.Rect.DOKill();
                m_sourceCard.Rect.DOPunchAnchorPos(-dir * 4f, 0.12f);
            }

            while (Vector3.Distance(fireBall.transform.position, targetPosition) > 1)
            {
                fireBall.transform.position = Vector3.MoveTowards(fireBall.transform.position, targetPosition, moveSpeed * Time.deltaTime);
                yield return Timing.WaitForOneFrame;
            }

            GameplayScreen.Instance.ShakeScreen();

            fireBall.transform.position = targetPosition;
            fireBall.Play("explode");

            AudioController.PlaySound("vfx_fiery_explosion", "BARDSFX", true, gameObject);

            if (m_targetCard != null)
            {
                m_targetCard.Rect.DOKill();
                m_targetCard.Rect.DOPunchAnchorPos(dir * 4f, 0.12f);
            }

            // handles damage and applies history
            GameVfxUtilities.HandleDamageCommand(command, vfxData, targetIndex, m_targetCard);

            while (!fireBall.IsDone) yield return Timing.WaitForOneFrame;

            fireBall.SetActive(false);

            yield return Timing.WaitForOneFrame;

            Destroy(gameObject);
        }

    }
}