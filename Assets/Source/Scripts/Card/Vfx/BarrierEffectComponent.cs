using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class BarrierEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<BarrierEffect>("BarrierEffect", card.cardObject.transform);
            effect.Initialize(card);
        }
    }
}