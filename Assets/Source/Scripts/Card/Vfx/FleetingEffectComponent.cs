using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class FleetingEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<FleetingEffect>("FleetingEffect", card.front.transform);
            effect.Initialize(card);
        }
    }
}