using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.GameVfx;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class PuffCannonEffect : MonoBehaviour, IGameVfx
    {
        public PuffProjectile puffProjectilePrefab;

        public void Initialize(CardView card)
        {

        }

        public void Initialize(Transform rect)
        {

        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            var getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(command, false);
            yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
            var vfxData = getSourceAndTargetData.Result;
            var handles = new List<CoroutineHandle>();

            for (var i = 0; i < vfxData.targetCards.Count; i++)
            {
                var puffProjectile = Instantiate(puffProjectilePrefab, transform, false);
                handles.Add(Timing.RunCoroutine(puffProjectile.Play(vfxData, command, i, -1, string.Empty, .5f, true)));

                yield return Timing.WaitForSeconds(Random.Range(0.2f, 0.4f));
            }

            while (handles.Exists(handle => handle.IsRunning)) yield return Timing.WaitForOneFrame;

            Destroy(gameObject, 2f);
        }
    }
}