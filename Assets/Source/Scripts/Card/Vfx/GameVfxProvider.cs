using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI.GameLogic.Data;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx
{
    public class GameVfxProvider : MonoBehaviour
    {
        private static List<IGameVfx> PreVizEffectsShowing;

        public static async Task<IGameVfx> GetVfxForSpell(GameCardState spell, GameCardView parentView)
        {
            var parent = spell.data.parentBehaviour switch
            {
                EffectParentBehaviour.GameBoard => GameplayScreen.Instance.gameBoard.RectTransform(),
                EffectParentBehaviour.TargetCardObject when parentView => parentView.View.cardObject,
                EffectParentBehaviour.TargetCard when parentView => parentView.View.transform,
                EffectParentBehaviour.CastingHero => spell.Team == Team.Client ?
                    GameplayScreen.Instance.heroSideBarContainer.playerHeroView.heroPortraitContainer :
                    GameplayScreen.Instance.heroSideBarContainer.opponentHeroView.heroPortraitContainer,
                _ => GameplayScreen.Instance.gameBoard.RectTransform()
            };

            var spellEffect = await AddressableReferenceLoader.Load(spell.data.effectName, parent);
            return spellEffect.GetComponent<IGameVfx>();
        }

        public static async Task<IGameVfx> GetVfxForSpell(GameCardState spell, RectTransform parentView, bool soICanPutNull)
        {
            var parent = spell.data.parentBehaviour switch
            {
                EffectParentBehaviour.GameBoard => GameplayScreen.Instance.gameBoard.RectTransform(),
                EffectParentBehaviour.TargetCardObject when parentView => parentView,
                EffectParentBehaviour.TargetCard when parentView => parentView,
                EffectParentBehaviour.CastingHero => spell.Team == Team.Client ?
                    GameplayScreen.Instance.heroSideBarContainer.playerHeroView.heroPortraitContainer :
                    GameplayScreen.Instance.heroSideBarContainer.opponentHeroView.heroPortraitContainer,
                _ => GameplayScreen.Instance.gameBoard.RectTransform()
            };

            var spellEffect = await AddressableReferenceLoader.Load(spell.data.effectName, parent);
            return spellEffect.GetComponent<IGameVfx>();
        }

        public static async Task<IGameVfx> GetWorldVfx(string effectName)
        {
            var worldEffect = await AddressableReferenceLoader.Load(effectName, GameplayScreen.Instance.gameBoard.RectTransform());
            return worldEffect.GetComponent<IGameVfx>();
        }

        public static async void ShowVfxForPreViz(GameCardState spell, GameCardView parentView)
        {
            var preViz = await GetVfxForSpell(spell, parentView);

            if (PreVizEffectsShowing == null) PreVizEffectsShowing = new List<IGameVfx>();

            Timing.RunCoroutine(preViz.Play(null, new VfxParameters
            {
                showPreViz = true
            }));

            PreVizEffectsShowing.Add(preViz);
        }

        public static void HideAllPreVizEffects()
        {
            if (PreVizEffectsShowing == null) return;

            for (var i = 0; i < PreVizEffectsShowing.Count; i++)
            {
                Timing.RunCoroutine(PreVizEffectsShowing[i].Play(null, null));
            }

            PreVizEffectsShowing.Clear();
        }
    }
}