using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class BombProjectile : MonoBehaviour
    {
        public SpriteAnimation animator;
        public GameObject trailParticles;

        private GameCardView m_targetCard;
        private bool m_updateExplosion;
        private Color m_explosionColor;
        private bool m_updateTrailParticles;
        private Vector3 m_lastPosition;

        private void FixedUpdate()
        {
            if (m_updateExplosion && m_targetCard)
            {
                switch (animator.CurrentFrame)
                {
                    case 1:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 50f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(-2, -2);
                        break;
                    case 2:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 100f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(3, 3);
                        break;
                    case 3:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 130f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(4, 4);
                        break;
                    case 4:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 150f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(3, 3);
                        break;
                    case 5:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 150f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(-1, -1);
                        break;
                    case 6:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 130f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(-2, -2);
                        break;
                    case 7:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 100f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(-2, -2);
                        break;
                    case 8:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 50f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(-1, -1);
                        break;
                    case 9:
                        m_targetCard.View.RemoveTint();
                        m_targetCard.Rect.anchoredPosition = new Vector2(0, 0);
                        break;
                }
            }

            if (m_updateTrailParticles)
            {
                var direction = (m_lastPosition - transform.position).normalized;

                for (var i = 0; i < trailParticles.transform.childCount; i++)
                {
                    trailParticles.transform.GetChild(i).localPosition = 5 * (i + 1) * direction;
                }

                m_lastPosition = transform.position;
            }
        }

        public IEnumerator<float> Play(VfxSourceAndTargetData vfxData, Command command,
            int targetIndex,
            float delay,
            Vector3 bombOffset,
            bool despawnInsteadOfExplode=false,
            bool parabolaMovement = true)
        {
            animator.SetActive(false);

            if (delay > 0) yield return Timing.WaitForSeconds(delay);

            var startPosition = vfxData.sourceCardPosition;
            var targetPosition = vfxData.targetCardPositions[targetIndex];
            var dir = (targetPosition - startPosition).normalized;
            var distance = Vector3.Distance(startPosition, targetPosition);

            animator.SetActive(true);
            animator.transform.localPosition = bombOffset;

            m_targetCard = vfxData.targetCardObjects[targetIndex];
            transform.position = startPosition;

            var isFlipped = dir.x <= 0;
            const float height = 5;

            animator.Play("spawn");
            animator.transform.localScale = new Vector3(isFlipped ? -1 : 1, 1, 1);

            AudioController.PlaySound("vfx_bomb_fuse", "BARDSFX", false, gameObject);

            while (!animator.IsDone) yield return Timing.WaitForOneFrame;

            m_lastPosition = transform.position;
            trailParticles.SetActive(true);
            m_updateTrailParticles = true;

            animator.Play($"idle");

            AudioController.PlaySound("vfx_lob", "BARDSFX", true, gameObject);

            GameplayScreen.Instance.ShakeScreen();

            if (parabolaMovement)
            {
                yield return Timing.WaitUntilDone(GameVfxUtilities.MoveInParabola(transform, targetPosition, 1, height,
                    false));
            }
            else
            {
                while (Vector3.Distance(transform.position, targetPosition) > 0.1f)
                {
                    var distToGoal = Vector3.Distance(transform.position, targetPosition) / distance;
                    transform.position = Vector3.MoveTowards(transform.position, targetPosition, 30 * Time.deltaTime);
                    animator.transform.localPosition = new Vector3(0, height * distToGoal, 0);
                    yield return Timing.WaitForOneFrame;
                }
            }

            var particleSystems = trailParticles.GetComponentsInChildren<ParticleSystem>();

            for (var i = 0; i < particleSystems.Length; i++)
            {
                particleSystems[i].Stop(true, ParticleSystemStopBehavior.StopEmitting);
            }

            transform.position = targetPosition;

            GameplayScreen.Instance.ShakeScreen();

            if (despawnInsteadOfExplode)
            {
                animator.Play("despawn");

                AudioController.StopSound("vfx_bomb_fuse");
                AudioController.PlaySound("map_land", "BARDSFX", true, gameObject);
            }
            else
            {
                animator.transform.localPosition = Vector3.zero;
                animator.Play($"explosion");

                AudioController.StopSound("vfx_bomb_fuse");
                AudioController.PlaySound("vfx_fiery_explosion", "BARDSFX", true, gameObject);
            }

            m_explosionColor = "#e22712".ToColor();
            m_updateExplosion = true;

            transform.localScale = Vector3.one;

            GameVfxUtilities.HandleDamageCommand( command, vfxData, targetIndex, m_targetCard);

        }
    }
}