using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class ComboSlashEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            if (card == null)
            {
                effect = await LoadEffect<ComboSlashEffect>("ComboSlashEffect", transform);
                effect.Initialize(this.RectTransform());
            }
            else
            {
                effect = await LoadEffect<ComboSlashEffect>("ComboSlashEffect", card.transform);
                effect.Initialize(card);
            }
        }
    }
}