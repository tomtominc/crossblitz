using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class GrantArmorEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<GrantArmorEffect>("GrantArmorEffect", card.cardObject);
            effect.Initialize(card);
        }
    }
}

