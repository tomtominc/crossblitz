using System;
using CrossBlitz.InputAPI;
using TMPro;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class ControlItem : MonoBehaviour
    {
        public SpriteAnimation controlAnimator;
        public SpriteAnimation actionAnimator;
        public TextMeshProUGUI keyboardBinding;

        private string m_controlAnimationName;

        public void SetAction(Controls.Actions action)
        {
            var key = Controls.GetKey(action);

            if (key == KeyCode.Mouse0)
            {
                m_controlAnimationName = "left-click";
                controlAnimator.Play(m_controlAnimationName);
                keyboardBinding.SetActive(false);
            }
            else if (key == KeyCode.Mouse1)
            {
                m_controlAnimationName = "right-click";
                controlAnimator.Play(m_controlAnimationName);
                keyboardBinding.SetActive(false);
            }
            else
            {
                m_controlAnimationName = "key-blank";
                controlAnimator.Play(m_controlAnimationName);
                keyboardBinding.SetActive(true);
                keyboardBinding.text = key.ToString();
            }

            actionAnimator.Play(action.ToString());
        }

        private void OnEnable()
        {
            if (!string.IsNullOrEmpty(m_controlAnimationName))
            {
                controlAnimator.Play(m_controlAnimationName);
            }
        }

        private void Update()
        {
            if (controlAnimator.CurrentAnimationName == m_controlAnimationName)
            {
                if (controlAnimator.CurrentFrame == 0)
                {
                    keyboardBinding.rectTransform.anchoredPosition = new Vector2(0, -1);
                }
                else if (controlAnimator.CurrentFrame == 1)
                {
                    keyboardBinding.rectTransform.anchoredPosition = new Vector2(0, -2);
                }
            }
        }
    }
}