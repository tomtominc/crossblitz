using System;
using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Hero;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using TakoBoyStudios.Events;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.GameVfx.Minion
{
    public class GuardEffect : MonoBehaviour, IGameVfx
    {
        public SpriteAnimation boardAnimator;
        public SpriteAnimation heroAnimator;

        private CardView _card;
        private GameCardView _gameCard;

        private RectTransform m_rect;
        private HeroViewBattle _heroView;

        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
            _gameCard = _card.GetCardComponent<GameCardView>();

            //var childIndex = _card.front.transform.GetSiblingIndex() - 1;
            //transform.SetSiblingIndex(childIndex);

            boardAnimator.SetActive(false);
            heroAnimator.SetActive(false);

            _gameCard.Events.OnSilenced += OnSilenced;

            Events.Subscribe(EventType.OnCharacterDamaged, OnCharacterDamaged);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnCharacterDamaged, OnCharacterDamaged);
        }

        private void OnSilenced(GameCardView cardView)
        {
            //Timing.RunCoroutine(Despawn());
        }

        private void OnCharacterDamaged(IMessage message)
        {
            Debug.Log("ON CHARACTER DAMAGED!");
            if (message.Data is OnCharacterDamagedEventArgs args)
            {
                if (_gameCard != null && _gameCard.State != null && !_gameCard.State.silenced)
                {
                    
                    var guard = _gameCard.State.GetAbility(AbilityKeyword.GUARD);
                    var targetState = GameServer.State.GetCard(args.characterUid);
                    
                    if (targetState.IsHero && !_gameCard.State.IsDead() && ((_gameCard.State.Team == Team.Client && args.isClientCharacter) || (_gameCard.State.Team == Team.Opponent && !args.isClientCharacter)))
                    {
                        if (guard != null)
                        {
                            Timing.RunCoroutine(Ping());
                        }
                    }

                }
            }
        }

        public IEnumerator<float> Spawn()
        {

            var targetState = GameServer.State.GetCard(_gameCard.State.uid);
            var heroView= GameplayScreen.Instance.heroSideBarContainer.GetHeroView(targetState.Team);
            var boardTarget = heroView.boardTarget;

            _heroView = heroView;

            //heroAnimator.transform.SetParent(boardTarget.transform);
            //heroAnimator.RectTransform().anchoredPosition = new Vector3 (0,0,0);

            //heroAnimator.transform.SetParent(boardTarget.transform);
            heroAnimator.RectTransform().position = boardTarget.RectTransform().position + Vector3.down;

            gameObject.SetActive(true);
            boardAnimator.SetActive(true);
            heroAnimator.SetActive(true);

            boardAnimator.Play("activate", ()=> boardAnimator.SetActive(false));

            // Check to see if the guard animation is already playing for the hero
            if (!heroView.guardAnimation) {
                heroAnimator.Play("activate", () => heroAnimator.SetActive(false));
                heroView.guardAnimation = true;
            }

            _gameCard.UpdateViewFromState(true);

            while (boardAnimator.CurrentFrame <= 0)
            {
                yield return Timing.WaitForOneFrame;
            }

            heroView.guardAnimation = false;

            while (boardAnimator.isActiveAndEnabled)
            {
                yield return Timing.WaitForOneFrame;
            } 
        }

        public IEnumerator<float> Ping()
        {
            //yield return Timing.WaitForSeconds(0.5f);

            _gameCard.UpdateViewFromState(true);

            boardAnimator.Play("ping", () => boardAnimator.SetActive(false));
            if (!_heroView.guardAnimation)
            {
                heroAnimator.Play("ping", () => heroAnimator.SetActive(false));
                _heroView.guardAnimation = true;
            }

            while (heroAnimator.CurrentFrame <= 0) yield return Timing.WaitForOneFrame;

            _heroView.guardAnimation = false;

            while (boardAnimator.CurrentFrame <= 2) yield return Timing.WaitForOneFrame;

            //if (_gameCard.State.Team == Team.Opponent) _gameCard.HitFromDirection(Vector2.up, 10);
            //else _gameCard.HitFromDirection(Vector2.down, 10);

            while (boardAnimator.isActiveAndEnabled)
            {
                yield return Timing.WaitForOneFrame;
            }
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            throw new System.NotImplementedException();
        }
    }
}