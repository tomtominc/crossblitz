using CrossBlitz.GameVfx;

namespace CrossBlitz.Card.Vfx
{
    public class MulliganCrossComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<MulliganCrossEffect>("MulliganCrossEffect", card.cardObject);
            effect.Initialize(card);
        }
    }
}