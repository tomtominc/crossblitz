using System;
using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class FleetingEffect : MonoBehaviour, IGameVfx
    {
        public GameObject cracks;
        public SpriteAnimation crackEffects;
        public GameObject spawnBurstParticles;
        public GameObject poofLoopParticles;

        private CardView _card;
        private GameCardView _gameCard;

        public void Initialize(CardView card)
        {
            _card = card;
            _gameCard = _card.GetCardComponent<GameCardView>();
            _gameCard.Events.OnFrozen += OnFrozen;
            _gameCard.Events.OnUnFrozen += OnUnFrozen;
            _gameCard.Events.OnDamaged += OnDamaged;
            _gameCard.Events.OnSilenced += OnSilenced;
            _gameCard.Events.OnTransformed += OnTransformed;
        }

        private void OnDestroy()
        {
            if (_gameCard != null)
            {
                _gameCard.Events.OnFrozen -= OnFrozen;
                _gameCard.Events.OnUnFrozen -= OnUnFrozen;
                _gameCard.Events.OnDamaged -= OnDamaged;
                _gameCard.Events.OnSilenced -= OnSilenced;
                _gameCard.Events.OnTransformed -= OnTransformed;
            }
        }

        public void Initialize(Transform rect) { }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            yield break;
        }

        public IEnumerator<float> Spawn()
        {
            gameObject.SetActive(true);

            cracks.SetActive(false);
            crackEffects.SetActive(true);
            spawnBurstParticles.SetActive(true);

            crackEffects.Play("activate");

            AudioController.PlaySound("vfx_dark_energy", "BARDSFX", true, gameObject);

            while (!crackEffects.IsDone) yield return Timing.WaitForOneFrame;

            cracks.SetActive(true);
            crackEffects.Play("idle");
            spawnBurstParticles.SetActive(false);
            poofLoopParticles.SetActive(true);
        }

        private void OnFrozen(GameCardView card)
        {
            Pause();
        }

        private void OnUnFrozen(GameCardView card)
        {
            Play();
        }

        private void OnDamaged(GameCardView card)
        {

        }

        private void OnSilenced(GameCardView card)
        {
            RemoveFleeting(card);
        }

        private void OnTransformed(GameCardView card)
        {
            if (card.State.HasAbility( AbilityKeyword.FLEETING ))
            {
                return;
            }

            RemoveFleeting(card);
        }

        private void RemoveFleeting(GameCardView card)
        {
            gameObject.SetActive(false);
        }

        private void Pause()
        {
            var particleSystems = poofLoopParticles.GetComponentsInChildren<ParticleSystem>();

            for (var i = 0; i < particleSystems.Length; i++)
            {
                particleSystems[i].Pause(true);
            }
        }

        private void Play()
        {
            var particleSystems = poofLoopParticles.GetComponentsInChildren<ParticleSystem>();

            for (var i = 0; i < particleSystems.Length; i++)
            {
                particleSystems[i].Play(true);
            }
        }

        private void Stop()
        {
            var particleSystems = poofLoopParticles.GetComponentsInChildren<ParticleSystem>();

            for (var i = 0; i < particleSystems.Length; i++)
            {
                particleSystems[i].Stop(true, ParticleSystemStopBehavior.StopEmitting);
            }
        }
    }
}