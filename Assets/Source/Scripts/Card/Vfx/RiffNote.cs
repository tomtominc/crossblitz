using DG.Tweening;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class RiffNote : MonoBehaviour
    {
        public SpriteAnimation animator;

        private RectTransform m_rect;
        private int m_currentNote;

        private void Start()
        {
            m_rect = this.RectTransform();
        }

        public void Play()
        {
            var randomAngle = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
            m_currentNote = Random.Range(1, 4);
            m_rect.anchoredPosition= Vector2.zero;
            m_rect.DOAnchorPos(new Vector2(50, 50) * randomAngle, 0.24f)
                .SetEase(Ease.OutSine);
            animator.image.color = Color.white;
            animator.image.DOFade(0, 0.24f).SetDelay(0.24f);
            //animator.Play($"spawn-{m_currentNote}", OnFinishedPlayingNote);
        }

        // private void OnFinishedPlayingNote()
        // {
        //     animator.Play($"idle-{m_currentNote}", () =>
        //     {
        //         _playingNotes.Remove(note);
        //         _pooledNotes.Add(note);
        //         note.SetActive(false);
        //     });
        // }

    }
}