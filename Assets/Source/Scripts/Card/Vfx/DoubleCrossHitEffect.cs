using System.Collections.Generic;
using Asyncoroutine;
using BlendModes;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Hero;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.Models;
using CrossBlitz.Transition;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class DoubleCrossHitEffect : MonoBehaviour, IGameVfx
    {
        public SpriteAnimation doubleCrossAnim;
        public GameObject DoubleCrossHit;
        public Color overlayRed;
        public RectTransform effectRectTransform;

        private CardView _card;
        private RectTransform m_rect;

        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
        }


        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            gameObject.SetActive(true);

            // get a reference to the card object (child object of the card so we aren't moving the card directly.
            var cardRect = _card.cardObject;

            // Shake Card & Tint 
            _card.Tint(overlayRed, BlendMode.Lighten);
            _card.RemoveTintOverTime(0.6f);
            cardRect.RectTransform().DOPunchAnchorPos(Vector2.right * 10, 0.6f,20);

            // Play Animation
            //doubleCrossAnim.SetActive(true);
            //doubleCrossAnim.Play("hit");

            // End process
            //while (!doubleCrossAnim.IsDone) yield return Timing.WaitForOneFrame;

            yield return Timing.WaitForSeconds(0.6f);
            //yield return Timing.WaitForSeconds(2f);
            Destroy(gameObject);
        }
    }
}