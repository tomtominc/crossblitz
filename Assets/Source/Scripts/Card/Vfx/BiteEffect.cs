using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.GameVfx;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class BiteEffect : MonoBehaviour, IGameVfx
    {
        public BubbleParticle bubbleParticlePrefab;
        public BiteEffectParticle biteEffectParticlePrefab;
        public void Initialize(CardView card)
        {

        }

        public void Initialize(Transform rect)
        {

        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            var getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(command);
            yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
            var vfxData = getSourceAndTargetData.Result;
            var handles = new List<CoroutineHandle>();
            BiteEffectParticle firstBiteEffect = null;

            for (var i = 0; i < vfxData.targetCards.Count; i++)
            {
                var biteEffectParticle = Instantiate(biteEffectParticlePrefab, transform, false);
                handles.Add( Timing.RunCoroutine(biteEffectParticle.Play(vfxData, command, i)));
                if (firstBiteEffect == null) firstBiteEffect = biteEffectParticle;
            }

            while (handles.Exists(handle => handle.IsRunning)) yield return Timing.WaitForOneFrame;

            var commandList = GameServer.State.GetCardHistories(vfxData.sourceCard.uid);
            var modifyCommand = commandList.Find(cmd =>
                cmd.Type == CommandType.MODIFY_POWER || cmd.Type == CommandType.MODIFY_HEALTH);

            if (modifyCommand != null)
            {
                getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(modifyCommand);
                yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
                vfxData = getSourceAndTargetData.Result;
                handles = new List<CoroutineHandle>();

                for (var i = 0; i < vfxData.targetCards.Count; i++)
                {
                    var bubbleParticle = Instantiate(bubbleParticlePrefab, transform, false);
                    handles.Add( Timing.RunCoroutine(bubbleParticle.Play(vfxData, command, i, firstBiteEffect!= null ? firstBiteEffect.transform:null)));
                }

                while (handles.Exists(handle => handle.IsRunning)) yield return Timing.WaitForOneFrame;
            }

            Destroy(gameObject, 2);
        }
    }
}