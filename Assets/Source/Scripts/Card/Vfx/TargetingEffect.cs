using System;
using System.Collections.Generic;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.GameVfx;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Card.Vfx
{
    public class TargetingEffect : MonoBehaviour, IGameVfx
    {
        public Transform targetingCursor;
        public Transform linePipPrefab;
        public Button cancelButton;
        public Canvas canvas;

        private const int Pips = 10;
        private const float MaxRadius = .3f;
        private Transform[] m_linePips;

        private bool m_update;
        private Camera m_cam;
        private Transform m_rect;
        private Vector3 m_offset;
        //private int m_currentTargetCount;

        public event Action OnCancel;

        public void Initialize(CardView card)
        {
            cancelButton.onClick.RemoveAllListeners();
            cancelButton.onClick.AddListener(OnCancelAction);
        }

        public void Initialize(Transform rect)
        {
            cancelButton.onClick.RemoveAllListeners();
            cancelButton.onClick.AddListener(OnCancelAction);
        }

        public void Show(RectTransform rect, bool useCancelButton, Vector3 offset)
        {
            gameObject.SetActive(true);

            cancelButton.onClick.RemoveAllListeners();
            cancelButton.onClick.AddListener(OnCancelAction);

            m_rect = rect;
            m_offset = offset;
            m_cam = Camera.current;
            m_linePips = new Transform[Pips];
            targetingCursor.SetActive(true);
            transform.position = rect.transform.position;

            if (useCancelButton)
            {
                cancelButton.SetActive(true);
                cancelButton.transform.localScale = new Vector3(0, 1, 1);
                cancelButton.transform.DOScale(1, 0.48f).SetEase(Ease.OutBack);
            }
            else
            {
                cancelButton.SetActive(false);
            }

            canvas.sortingOrder = GameplayScreen.BoardTargetSortOrder + 10;

            for (var i = 0; i < Pips; i++)
            {
                var linePip = Instantiate(linePipPrefab, transform, false);
                m_linePips[i] = linePip;
            }

            m_update = true;
        }

        public void Update()
        {
            if (!m_update)
            {
                return;
            }

            if (m_cam == null)
            {
                m_cam = Camera.current;
            }

            if (m_cam == null)
            {
                m_cam = Camera.main;
            }

            if (m_cam == null)
            {
                return;
            }

            Cursor.visible = false;
            targetingCursor.position = m_cam.ScreenToWorldPoint(Input.mousePosition);

            var point = new[] {m_rect.transform.position+m_offset, Vector3.zero, targetingCursor.position};
            var perp = point[0].x > point[2].x ?
                Vector2.Perpendicular(point[0] - point[2]) :
                Vector2.Perpendicular(point[2] - point[0]);
            var diff = Mathf.Abs(point[0].x - point[2].x);

            var radius = Mathf.Clamp( MaxRadius * (diff / 5f), 0, MaxRadius);
            point[1] = (point[0] + (point[2] - point[0]) / 2) + (Vector3)perp * radius;

            for (var i = 0; i < Pips; i++)
            {
                var time = i / (float)Pips;
                var m1 = Vector2.Lerp(point[0], point[1], time);
                var m2 = Vector2.Lerp(point[1], point[2], time);
                var lerp = Vector2.Lerp(m1, m2, time);

                m_linePips[i].position = lerp;
            }

            // if (GameplayScreen.Instance.ShowingTargets != null &&
            //     GameplayScreen.Instance.ShowingTargets.Count > m_currentTargetCount)
            // {
            //     for (var i = m_currentTargetCount; i < GameplayScreen.Instance.ShowingTargets.Count; i++)
            //     {
            //         GameplayScreen.Instance.ShowingTargets[i].OnCardEnterActiveBoardTarget +=
            //             OnHoverTarget();
            //         GameplayScreen.Instance.ShowingTargets[i].OnCardExitActiveBoardTarget +=
            //             OnLeaveTarget();
            //     }
            // }
        }

        public void Hide()
        {
            Cursor.visible = true;

            m_update = false;
            Destroy(gameObject);
        }

        private void OnCancelAction()
        {
            OnCancel?.Invoke();
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            throw new System.NotImplementedException();
        }
    }
}