using System.Collections.Generic;
using BlendModes;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class GenericProjectile : GenericEffectParticle
    {
        public GameObject trailParticles;
        public Color targetCardTintColor;
        public bool moveInParabola;
        public float moveSpeed = 1;
        public float jumpScale = 2;
        public float particleOffsets = 10;
        public float parabolaHeight = 10;

        private bool m_updateExplosion;
        private bool m_updateTrailParticles;
        private Vector3 m_lastPosition;

        private void FixedUpdate()
        {
            if (m_updateExplosion && m_targetCard)
            {
                switch (animator.CurrentFrame)
                {
                    case 1:
                        m_targetCard.View.Tint(targetCardTintColor, BlendMode.Color, true, 50f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(-2, -2);
                        break;
                    case 2:
                        m_targetCard.View.Tint(targetCardTintColor, BlendMode.Color, true, 100f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(3, 3);
                        break;
                    case 3:
                        m_targetCard.View.Tint(targetCardTintColor, BlendMode.Color, true, 130f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(4, 4);
                        break;
                    case 4:
                        m_targetCard.View.Tint(targetCardTintColor, BlendMode.Color, true, 150f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(3, 3);
                        break;
                    case 5:
                        m_targetCard.View.Tint(targetCardTintColor, BlendMode.Color, true, 150f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(-1, -1);
                        break;
                    case 6:
                        m_targetCard.View.Tint(targetCardTintColor, BlendMode.Color, true, 130f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(-2, -2);
                        break;
                    case 7:
                        m_targetCard.View.Tint(targetCardTintColor, BlendMode.Color, true, 100f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(-2, -2);
                        break;
                    case 8:
                        m_targetCard.View.Tint(targetCardTintColor, BlendMode.Color, true, 50f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(-1, -1);
                        break;
                    case 9:
                        m_targetCard.View.RemoveTint();
                        m_targetCard.Rect.anchoredPosition = new Vector2(0, 0);
                        break;
                }
            }

            if (m_updateTrailParticles)
            {
                var direction = (m_lastPosition - transform.position).normalized;

                for (var i = 0; i < trailParticles.transform.childCount; i++)
                {
                    trailParticles.transform.GetChild(i).localPosition = particleOffsets * (i + 1) * direction;
                }

                m_lastPosition = transform.position;
            }
        }


        public override IEnumerator<float> Play(VfxSourceAndTargetData vfxData, Command command, int targetIndex)
        {
            animator.SetActive(false);

            var sourceCardObject = vfxData.sourceCardObject;
            var startPosition = vfxData.sourceCardPosition;
            var targetPosition = vfxData.targetCardPositions[targetIndex];
            var dir = (targetPosition - startPosition).normalized;
            //var distance = Vector3.Distance(startPosition, targetPosition);

            animator.SetActive(true);
            m_targetCard = vfxData.targetCardObjects[targetIndex];

            transform.position = startPosition;

            var isFlipped = dir.x <= 0;
            const float height = 3;

            animator.Play(spawnAnimationName);
            animator.transform.localScale = new Vector3(isFlipped ? -1 : 1, 1, 1);

            if (sourceCardObject != null)
            {
                sourceCardObject.View.StartAnimating();
                sourceCardObject.View.RemoveTint();
                sourceCardObject.View.Tint(Color.white, BlendMode.Lighten);
                sourceCardObject.View.RemoveTintOverTime(0.25f);
                sourceCardObject.View.cardObject.DOPunchAnchorPos(dir * 20, 0.25f, 2, 0).SetEase(Ease.OutBack);
            }

            while (!animator.IsDone) yield return Timing.WaitForOneFrame;

            if (sourceCardObject != null)
            {
                sourceCardObject.View.RemoveTint();
                sourceCardObject.View.Tint(Color.white, BlendMode.Lighten);
                sourceCardObject.View.RemoveTintOverTime(0.25f);
                sourceCardObject.View.cardObject.DOPunchAnchorPos(dir * 20, 0.25f, 2, 0).SetEase(Ease.OutBack);
            }

            m_lastPosition = transform.position;
            m_updateTrailParticles = true;
            trailParticles.SetActive(true);

            animator.Play(idleAnimationName);

            if (moveInParabola)
            {
                yield return Timing.WaitUntilDone(GameVfxUtilities.MoveInParabola(transform, targetPosition, moveSpeed,
                    parabolaHeight, false));
            }
            else
            {
                yield return Timing.WaitUntilDone(GameVfxUtilities.MoveJump(transform,startPosition, targetPosition, moveSpeed, jumpScale));
            }

            if (sourceCardObject != null)
            {
                sourceCardObject.View.cardObject.anchoredPosition = Vector2.zero;
            }

            var particleSystems = trailParticles.GetComponentsInChildren<ParticleSystem>();

            for (var i = 0; i < particleSystems.Length; i++)
            {
                particleSystems[i].Stop(true, ParticleSystemStopBehavior.StopEmitting);
            }

            transform.position = targetPosition;
            animator.transform.localPosition = Vector3.zero;

            GameplayScreen.Instance.ShakeScreen();

            animator.Play(despawnAnimationName);

            m_updateExplosion = true;

            transform.localScale = Vector3.one;

            GameVfxUtilities.HandleDamageCommand(command, vfxData, targetIndex, m_targetCard);
        }
    }
}