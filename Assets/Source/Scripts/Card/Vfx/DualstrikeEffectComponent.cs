using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class DualstrikeEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<DualstrikeEffect>("DualstrikeEffect", card.cardObject.transform);
            effect.Initialize(card);
        }
    }
}