using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.Hero;
using MEC;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace CrossBlitz.GameVfx.Spell
{
    public class FrostShotEffect : MonoBehaviour, IGameVfx
    {
        private const float DelayBetweenParticles=0.18f;
        private const float ShakePortraitDelay = 0.06f;
        private const float DelayBetweenMultipleProjectilesFast = 0.3f;
        private const float DelayBetweenMultipleProjectiles = 0.7f;

        public RectTransform preVizContainer;
        public SpriteAnimation preVizParticlePrefab;
        public SpriteAnimation glyph;
        public FrostShotProjectile projectilePrefab;

        private Queue<SpriteAnimation> preVizParticles;

        private bool _showPreViz;
        private float _particleTimer;
        private float _shakePortraitTimer;
        private RectTransform _heroPortrait;
        private Image _portraitGlow;
        private float _glowAlpha;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            //_card = card;
        }

        private void Update()
        {
            if (_showPreViz)
            {
                UpdatePreViz();
            }
        }

        private void UpdatePreViz()
        {
            _particleTimer -= Time.deltaTime;

            if (_particleTimer <= 0)
            {
                _particleTimer = DelayBetweenParticles;
                SpawnRandomPreVizParticle();
            }

            _shakePortraitTimer -= Time.deltaTime;

            if (_shakePortraitTimer <= 0)
            {
                _shakePortraitTimer = ShakePortraitDelay;

                if (_heroPortrait == null)
                {
                    AssignHeroPortrait();
                }

                int offsetX = Random.Range(-2, 3);
                int offsetY = Random.Range(-2, 3);

                if (_heroPortrait)
                {
                    _heroPortrait.anchoredPosition = new Vector2(offsetX, offsetY);
                }
            }

            if (_portraitGlow == null)
            {
                AssignHeroPortrait();
            }

            if (_portraitGlow)
            {
                _portraitGlow.SetActive(true);
                _glowAlpha = Mathf.PingPong(Time.time, 0.8f);
                _portraitGlow.color = "#c5fcfa".ToColor(_glowAlpha);
            }
        }

        private void SpawnRandomPreVizParticle()
        {
            var sideX = Random.value > 0.5f ? -1 : 1;
            var sideY = Random.value > 0.5f ? -1 : 1;

            var posX = Random.Range(20, 40) * sideX;
            var posY = Random.Range(20, 40) * sideY;

            var particle = Instantiate(preVizParticlePrefab, preVizContainer);
            particle.transform.localScale = new Vector3(sideX, sideY, 1);
            particle.RectTransform().anchoredPosition = new Vector2(posX, posY);
            particle.Play($"particle-absorption-{Random.Range(1, 3)}", OnFinishedParticleAnimation);

            if (preVizParticles == null) preVizParticles = new Queue<SpriteAnimation>();

            preVizParticles.Enqueue(particle);

        }

        private void OnFinishedParticleAnimation()
        {
            if (preVizParticles != null && preVizParticles.Peek() != null)
            {
                var particle = preVizParticles.Dequeue();
                Destroy(particle.gameObject);
            }
        }

        private void AssignHeroPortrait()
        {
            if (transform.parent && transform.parent.parent && transform.parent.parent.parent)
            {
                var heroBattleView = transform.parent.parent.parent.GetComponent<HeroViewBattle>();

                if (heroBattleView)
                {
                    _heroPortrait = heroBattleView.portraitCropped.RectTransform();
                    _portraitGlow = heroBattleView.portraitGlow.image;
                }
            }
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            if (command == null && vfxParameters == null)
            {
                HidePreViz();
                yield break;
            }

            if (command == null || (vfxParameters != null && vfxParameters.showPreViz))
            {
                ShowPreViz();
                yield break;
            }

            var getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(command);
            yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
            var vfxData = getSourceAndTargetData.Result;
            var handles = new List<CoroutineHandle>();

            gameObject.SetActive(true);

            var ability = command.GetAbility();
            var effectType = ability?.effectSubType ?? 0;

            if (effectType == 0)
            {
                glyph.transform.position = vfxData.sourceCardPosition;
                glyph.SetActive(true);
                glyph.Play("spawn-glyph", () => glyph.Play("idle-glyph"));
                while (glyph.CurrentFrame < 17) yield return Timing.WaitForOneFrame;
            }

            for (var i = 0; i < vfxData.targetCards.Count; i++)
            {
                var frostShotProjectile = Instantiate(projectilePrefab, transform, false);
                handles.Add(Timing.RunCoroutine(frostShotProjectile.Play(vfxData, command, i, effectType == 1)));
                yield return Timing.WaitForSeconds(effectType == 0 ? DelayBetweenMultipleProjectiles : DelayBetweenMultipleProjectilesFast);
            }

            if (effectType == 0)
            {
                glyph.Play("despawn-glyph");
                while (!glyph.IsDone) yield return Timing.WaitForOneFrame;
                glyph.SetActive(false);
            }

            while (handles.Exists(handle => handle.IsRunning)) yield return Timing.WaitForOneFrame;

            // wait until the explosion particles in the projectile prefab have dissipated
            Destroy(gameObject, 1.2f);
        }

        private void ShowPreViz()
        {
            _showPreViz = true;
        }

        private void HidePreViz()
        {
            _showPreViz = false;

            if (_heroPortrait)
            {
                _heroPortrait.anchoredPosition = new Vector2(0, 0);
            }

            if (_portraitGlow)
            {
                _portraitGlow.SetActive(false);
            }

            Destroy(gameObject);
        }
    }
}