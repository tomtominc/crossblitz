using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class ModifyHealthAndPowerEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<ModifyHealthAndPowerEffect>("ModifyHealthAndPowerEffect", card.cardObject);
            effect.Initialize(card);
        }
    }
}