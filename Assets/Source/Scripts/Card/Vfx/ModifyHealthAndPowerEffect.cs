using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.GameVfx.Minion
{
    public class ModifyHealthAndPowerEffect : MonoBehaviour, IGameVfx
    {
        private const int FramesBeforeBurst = 20;

        [BoxGroup("Universal")]
        public CanvasGroup valueGroupFadeCanvas;
        [BoxGroup("Universal")]
        public RectTransform valueGroupRect;
        [BoxGroup("Universal")]
        public RectTransform iconsRect;
        [BoxGroup("Universal")]
        public RectTransform labelsRect;
        [BoxGroup("Universal")]
        public GameObject stretchedOverlay;
        [BoxGroup("Universal")]
        public CanvasGroup overlay;
        [BoxGroup("Universal")]
        public Image overlayImage;
        [BoxGroup("Universal")]
        public RectTransform arrowParticles;

        [BoxGroup("Health")]
        public SpriteAnimation healthIcon;
        [BoxGroup("Health")]
        public SpriteAnimation healthBurst;
        [BoxGroup("Health")]
        public SpriteAnimation healthLineBurst;
        [BoxGroup("Health")]
        public TMPSpriteFont healthLabel;

        [BoxGroup("Power")]
        public SpriteAnimation powerIcon;
        [BoxGroup("Power")]
        public SpriteAnimation powerBurst;
        [BoxGroup("Power")]
        public SpriteAnimation powerLineBurst;
        [BoxGroup("Power")]
        public TMPSpriteFont powerLabel;

        private CardView _card;
        private GameCardView _gameCard;
        private RectTransform _cardRect;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
            _gameCard = _card.GetCardComponent<GameCardView>();
            _cardRect = card.cardObject;
        }

        public IEnumerator<float> PlayModifyAnimation(int modifyPowerAmount, int modifyHealthAmount, bool isHealing = false)
        {
            gameObject.SetActive(true);
            stretchedOverlay.SetActive(false);
            arrowParticles.SetActive(false);
            powerBurst.SetActive(false);
            healthBurst.SetActive(false);
            healthLineBurst.SetActive(false);
            powerLineBurst.SetActive(false);
            valueGroupRect.SetActive(false);

            valueGroupFadeCanvas.alpha = 0;
            valueGroupRect.SetParent(_card.transform, false);
            iconsRect.anchoredPosition = Vector2.zero;
            labelsRect.anchoredPosition = Vector2.zero;

            overlay.alpha = 0;
            overlayImage.color  = modifyPowerAmount > 0 ? "#a7bdc3".ToColor(0.6f) : "#d56d6d".ToColor(0.6f);

            var arrows = new List<SpriteAnimation>();

            for (var i = 0; i < arrowParticles.childCount; i++)
            {
                var arrow = arrowParticles.GetChild(i);
                var arrowParticle = arrow.GetComponent<SpriteAnimation>();
                arrows.Add(arrowParticle);
            }

            for (var frame = 0; frame < FramesBeforeBurst; frame++)
            {
                switch (frame)
                {
                    case 0: _cardRect.anchoredPosition = new Vector2(-1, -2);
                        break;
                    case 1: _cardRect.anchoredPosition = new Vector2(1, -3);
                        break;
                    case 2: _cardRect.anchoredPosition = new Vector2(-1, 6);
                        break;
                    case 3: _cardRect.anchoredPosition = new Vector2(0, 7);
                        break;
                    case 4: _cardRect.anchoredPosition = new Vector2(0, 8);
                        break;
                    case 5:
                        valueGroupFadeCanvas.alpha = 1;
                        valueGroupRect.SetActive(true);
                        _cardRect.anchoredPosition = Vector2.zero;

                        arrowParticles.SetActive(true);
                        stretchedOverlay.SetActive(true);
                        iconsRect.SetActive(true);
                        labelsRect.SetActive(true);
                        overlay.alpha = 1;

                        if (modifyHealthAmount > 0)
                        {
                            healthIcon.SetActive(true);
                            healthIcon.Play("idle");
                            healthLabel.SetActive(true);
                            healthLabel.text = modifyHealthAmount.ToString();
                            AudioController.PlaySound("battle_cardbuff_hp", "BARDSFX", true, gameObject);

                            if (modifyPowerAmount <= 0)
                            {
                                healthLineBurst.SetActive(true);
                                healthLineBurst.Play("line-burst");
                            }
                        }
                        else
                        {
                            healthIcon.SetActive(false);
                            healthLabel.SetActive(false);
                        }

                        if (modifyPowerAmount > 0)
                        {
                            powerIcon.SetActive(true);
                            powerIcon.Play($"idle-{_card.data.attackType.ToString().ToLower()}");
                            powerLabel.SetActive(true);
                            powerLabel.text = modifyPowerAmount.ToString();
                            AudioController.PlaySound("battle_cardbuff_attack", "BARDSFX", true, gameObject);

                            if (modifyHealthAmount <= 0)
                            {
                                powerLineBurst.SetActive(true);
                                powerLineBurst.Play("line-burst");
                            }
                        }
                        else
                        {
                            powerIcon.SetActive(false);
                            powerLabel.SetActive(false);
                        }

                        for (var i = 0; i < arrows.Count; i++)
                        {
                            var arrowAnim = modifyPowerAmount > 0 && modifyHealthAmount > 0 ? i % 2 == 0 ?
                                "power" : "health" : modifyPowerAmount > 0 ? "power" : "health";
                            arrows[i].Play(arrowAnim);
                        }

                        iconsRect.DOAnchorPosY(iconsRect.anchoredPosition.y + 11, 0.25f)
                            .SetEase(Ease.OutBack);
                        labelsRect.DOAnchorPosY(labelsRect.anchoredPosition.y + 11, 0.3f)
                            .SetEase(Ease.OutBack);

                        _gameCard.UpdateViewFromState();

                        break;
                    case 6:
                        _cardRect.anchoredPosition = new Vector2(0, 9);
                        stretchedOverlay.SetActive(false);

                        if (modifyHealthAmount > 0)
                        {
                            healthBurst.SetActive(true);
                            healthBurst.Play("number-burst");
                        }
                        else
                        {
                            healthBurst.SetActive(false);
                        }

                        if (modifyPowerAmount > 0)
                        {
                            powerBurst.SetActive(true);
                            powerBurst.Play("number-burst");
                        }
                        else
                        {
                            powerBurst.SetActive(false);
                        }

                        break;
                    case 7:
                        _cardRect.anchoredPosition = new Vector2(0, 10);
                        overlay.DOFade(0, 0.12f);
                        break;
                    case 8:
                        _cardRect.anchoredPosition = new Vector2(0, 11);
                        _cardRect.DOAnchorPosY(0, 0.3f)
                            .SetEase(Ease.OutBack);
                        break;
                    case 15:
                        valueGroupRect.SetParent(transform, true);
                        break;
                }

                if (frame == 3) yield return Timing.WaitForSeconds(0.12f);
                else yield return Timing.WaitForSeconds(0.04f);
            }

            valueGroupFadeCanvas.DOFade(0, 0.5f).OnComplete(OnFinishedFade);
            _gameCard.UpdateViewFromState();
        }

        private void OnFinishedFade()
        {
            gameObject.SetActive(false);
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            yield break;
        }
    }
}