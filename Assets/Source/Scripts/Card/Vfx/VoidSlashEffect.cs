using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Audio;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class VoidSlashEffect : MonoBehaviour
    {
        private const float FrameTime = 0.08f;
        public SpriteAnimation slashAnim;
        public Color overlayPurple;
        public Color overlayWhite;

        public IEnumerator<float> Play( VfxSourceAndTargetData vfxData, Command command, int targetIndex )
        {
            gameObject.SetActive(true);
            slashAnim.Play("slash");

            var targetPosition = vfxData.targetCardPositions[targetIndex];
            var targetCard = vfxData.targetCardObjects[targetIndex];

            transform.position = targetPosition;

            if (targetCard != null)
            {
                var targetCardView = targetCard.View;
                yield return Timing.WaitForSeconds(0.3f);

                targetCardView.Tint(overlayPurple, BlendMode.Darken);
                AudioController.PlaySound("vfx_claw_slash", "BARDSFX", true, gameObject);
                yield return Timing.WaitForSeconds(0.3f);

                targetCardView.Tint(overlayWhite, BlendMode.Lighten);
                yield return Timing.WaitForSeconds(0.3f);

                targetCardView.Tint(overlayPurple, BlendMode.Darken);
                targetCardView.RemoveTintOverTime(1f);
            }

            if (command is DamageCommand damageCommand)
            {
                // do damage here..
                // check if card null, if not apply damage then do generic damage animation.
                // remember to check "Apply Damage in Effect" when setting up the effect in the ability

                var targetState = vfxData.targetCards[targetIndex];

                if (targetState != null)
                {
                    var damageHistory = damageCommand.PendingDamageHistories[targetIndex];
                    targetState.ApplyHistory(damageHistory.Uid);

                    if (targetState.IsHero)
                    {
                        var heroView = GameplayScreen.Instance.heroSideBarContainer;
                        AudioController.PlaySound("vfx_claw_slash", "BARDSFX", true, gameObject);
                        Timing.RunCoroutine(heroView.HitPortraitNonAttack(targetState.Team, damageCommand, damageHistory.Uid));
                    }
                    else if (targetCard != null)
                    {
                        var amount = damageHistory.DamageAmount;
                        var damageFx = targetCard.View.GetVfx<DamageEffectComponent, DamageEffect>();

                        if (damageFx != null)
                        {
                            Timing.RunCoroutine(damageFx.GenericDamageAnimation(amount));
                        }
                    }
                }
            }

            while (!slashAnim.IsDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            Destroy(gameObject);
        }
    }
}