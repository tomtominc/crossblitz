using System;
using System.Collections;
using System.Collections.Generic;
using Asyncoroutine;
using BlendModes;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using DG.Tweening;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.GameVfx.Spell
{
    public class BurstStrikeProjectileEffect : MonoBehaviour, IGameVfx
    {

        public SpriteAnimation projectileAnim;
        public CanvasGroup projectileCanvas;
        public GameObject particleObject;

        public Color overlayPurple;
        public Color overlayWhite;

        public bool isDone;

        private List<GameCardView> _boardCardResults;
        private DamageCommand _command;
        private RectTransform m_rect;
        private float _particleTimer;

        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
            _particleTimer = 0f;
            isDone = false;
        }

        public void Initialize(CardView card)
        {

        }

        public void Update()
        {
            _particleTimer += Time.deltaTime;

            if (_particleTimer >= 0.1f)
            {
                var parentTransform = GetComponent<RectTransform>();
                var particle = Instantiate(particleObject, parentTransform);
                var x = Random.Range(parentTransform.anchoredPosition.x - parentTransform.rect.width / 3,
                    parentTransform.anchoredPosition.x + parentTransform.rect.width / 3);
                var y = Random.Range(parentTransform.anchoredPosition.y+22f - parentTransform.rect.height / 8,
                parentTransform.anchoredPosition.y+22f + parentTransform.rect.height / 8);
                particle.RectTransform().anchoredPosition = new Vector2(x, y);
                _particleTimer = 0f;
            }
        }
        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            _command = command as DamageCommand;
            _boardCardResults = new List<GameCardView>();

            projectileAnim.Play("gash");

            // Get all Damaged Cards
            for (var i = 0; i < _command?.TargetUids.Count; i++)
            {
                var getBoardCardTask = GameBoard.GetBoardCard(_command.TargetUids[i]);
                yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                var getBoardCardResult = getBoardCardTask.Result;

                if (getBoardCardResult.error != ClientErrorCode.None)
                {
                    Server.HandleClientError(getBoardCardResult, false);
                }

                if (getBoardCardResult.boardCard)
                {
                    _boardCardResults.Add(getBoardCardResult.boardCard);

                    // get a reference to the card object (child object of the card so we aren't moving the card directly.
                    var cardRect = getBoardCardResult.boardCard.View.cardObject;

                    // Shake Card, apply tints
                    var origAnchorPos = cardRect.anchoredPosition;
                    cardRect.DOShakeAnchorPos(1f, 5, 50, 0).OnComplete(() => cardRect.DOAnchorPos(origAnchorPos, 0.1f));
                    //getBoardCardResult.boardCard.View.Tint(overlayWhite, BlendMode.Lighten);
                    //getBoardCardResult.boardCard.View.Tint(overlayPurple, BlendMode.Darken);
                }
            }


            while (projectileAnim.CurrentFrame < 6) yield return Timing.WaitForOneFrame;

            yield return Timing.WaitForSeconds(0.5f);

            // Do Damage to cards
            for (var i = 0; i < _boardCardResults.Count; i++)
            {
                _boardCardResults[i].State.ApplyHistoryByCardGuid(_command.SourceUid);
                var damageFx = _boardCardResults[i].View.GetVfx<DamageEffectComponent, DamageEffect>();
                Timing.RunCoroutine(damageFx.GenericDamageAnimation(_command.Amount));

                // Remove Tint
                _boardCardResults[i].View.RemoveTintOverTime(0.5f);
                
            }

            yield return Timing.WaitUntilDone(projectileCanvas.DOFade(0, 0.5f).WaitForCompletion(true));

            yield return Timing.WaitForSeconds(0.4f);

            isDone = true;
        }
      

        public void Unload()
        {
            AddressableReferenceLoader.Unload(gameObject);
        }
    }
}