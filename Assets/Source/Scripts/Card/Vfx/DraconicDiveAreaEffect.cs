using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.GameVfx;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    /// <summary>
    /// A game board effect, should not be spawned on top of a card. The ability can handle targeting and sources
    /// </summary>
    public class DraconicDiveAreaEffect : MonoBehaviour, IGameVfx
    {
        public DraconicDiveEffect draconicDiveEffectPrefab;

        public void Initialize(CardView card)
        {

        }

        public void Initialize(Transform rect)
        {

        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            gameObject.SetActive(true);

            var getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(command, false);
            yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
            var vfxData = getSourceAndTargetData.Result;

            for (var i = 0; i < vfxData.targetCards.Count; i++)
            {
                var draconicDiveEffect = Instantiate(draconicDiveEffectPrefab, transform, false);
                yield return Timing.WaitUntilDone(draconicDiveEffect.Play(vfxData, command, i));
            }

            yield return Timing.WaitForOneFrame;

            Destroy(gameObject);
        }
    }
}