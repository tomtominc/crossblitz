
namespace CrossBlitz.Card.Vfx
{
    public class TargetingEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            if (card == null)
            {
                effect = await LoadEffect<TargetingEffect>("TargetingEffect", transform);
                effect.Initialize(card);
            }
            else
            {
                effect = await LoadEffect<TargetingEffect>("TargetingEffect", card.transform);
                effect.Initialize(card);
            }
        }
    }
}