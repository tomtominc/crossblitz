using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class PyreCannonEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<PyreCannonEffect>("PyreCannonEffect", card.transform);
            effect.Initialize(card);
        }
    }
}