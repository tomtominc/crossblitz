using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class BattlecryEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<BattlecryEffect>("BattlecryEffect", card.cardObject);
            effect.Initialize(card);
        }
    }
}