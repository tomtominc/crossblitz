using CrossBlitz.GameVfx;

namespace CrossBlitz.Card.Vfx
{
    public class SubCardComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            var addressableId = card.viewType == CardView.CardViewType.STANDARD
                ? "SubCardEffect_Standard"
                : "SubCardEffect_Zoomed";
            effect = await LoadEffect<SubCardEffect>(addressableId, card.transform);
            effect.Initialize(card);
        }
    }
}