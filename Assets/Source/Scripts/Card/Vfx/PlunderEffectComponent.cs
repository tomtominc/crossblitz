using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class PlunderEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<PlunderEffect>("PlunderEffect", card.transform);
            effect.Initialize(card);
        }
    }
}