using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx
{
    public class CardTransformEffect : MonoBehaviour, IGameVfx
    {
        public ShakeContainer shakeContainer;
        public SpriteAnimation overlayJellyAnimator;

        private CardView _view;

        public bool IsFinishedTransforming { get; private set; }

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _view = card;
            overlayJellyAnimator.SetActive(false);
        }

        public IEnumerator<float> TransformFromZoomedCard(GameCardView standardCard)
        {
            IsFinishedTransforming = false;

            gameObject.SetActive(true);

            //=======================================================
            // SETS UP OVERLAY COLORS / STARTS THE SHAKE
            //=======================================================

            _view.frontOverlayCanvas.SetActive(true);
            _view.frontOverlayCanvas.alpha = 0;

            if (_view.data.type == CardType.Minion && _view.attackTypeIconOverlayCanvas)
            {
                _view.attackTypeIconOverlayCanvas.SetActive(true);
                _view.attackTypeIconOverlayCanvas.alpha = 0;
            }

            _view.frontOverlayCanvas.DOFade(1, 0.08f);
            _view.frontOverlay.image.color = "8a488e".ToColor();


            if (_view.data.type == CardType.Minion && _view.attackTypeIconOverlayCanvas)
            {
                _view.attackTypeIconOverlayCanvas.DOFade(1, 0.08f);
                _view.attackTypeIconOverlay.color = "8a488e".ToColor();
            }

            shakeContainer.Shake(16, 0.32f);

            yield return Timing. WaitForSeconds(0.08f);

            //=======================================================
            // PLAYS THE JELLY OVERLAY ANIMATION / SCALES THE CARD FOR ANOTHER ADDED EFFECT
            //=======================================================

            overlayJellyAnimator.SetActive(true);
            overlayJellyAnimator.Play(_view.data.type.ToString().ToLower());
            _view.cardObject.SetActive(false);

            AudioController.PlaySound("battle_card_add", "BARDSFX", true, gameObject);

            yield return Timing.WaitForSeconds(0.32f);

            //=======================================================
            // SCALES THE CARD DOWN TO STANDARD SIZE
            //=======================================================

            yield return Timing.WaitUntilDone(_view.RectTransform()
                .DOScale(new Vector3(88f / 168f, 100f / 202f), 0.16f)
                .WaitForCompletion(true));

            //=======================================================
            // FADES THE ZOOMED VIEW AND SETS UP THE STANDARD CARD
            //=======================================================
            var standardCardView = standardCard.View;
            standardCardView.frontOverlay.SetActive(true);
            standardCardView.frontOverlayCanvas.alpha = 1;
            standardCardView.frontOverlay.image.color =  "b455a0".ToColor();

            if (_view.data.type == CardType.Minion && standardCardView.attackTypeIconOverlayCanvas)
            {
                standardCardView.attackTypeIconOverlay.SetActive(true);
                standardCardView.attackTypeIconOverlayCanvas.alpha = 1;
                standardCardView.attackTypeIconOverlay.color = "b455a0".ToColor();
            }

            standardCardView.RectTransform().position = _view.RectTransform().position;
            standardCardView.SetActive(true);

            var drag = standardCardView.GetComponent<DragRotator>();
            drag.Reset();

            yield return Timing.WaitUntilDone(_view.CanvasGroup
                .DOFade(0,0.12f)
                .WaitForCompletion(true));

            _view.SetActive(false);

            //=======================================================
            // FADES THE STANDARD FRONT
            //=======================================================

            standardCardView.frontOverlayCanvas.DOFade(0, 0.24f)
                .OnComplete(()=> _view.frontOverlayCanvas.SetActive(false));

            if (_view.data.type == CardType.Minion)
            {
                standardCardView.attackTypeIconOverlayCanvas.DOFade(0, 0.24f)
                    .OnComplete(() => _view.attackTypeIconOverlayCanvas.SetActive(false));
            }

            yield return Timing. WaitForSeconds(0.04f);

            standardCardView.frontOverlay.image.color = "e698a4".ToColor();

            if (_view.data.type == CardType.Minion && standardCardView.attackTypeIconOverlayCanvas)
            {
                standardCardView.attackTypeIconOverlay.color = "e698a4".ToColor();
            }

            yield return Timing. WaitForSeconds(0.04f);

            standardCardView.frontOverlay.image.color = "f8d3d9".ToColor();

            if (_view.data.type == CardType.Minion && standardCardView.attackTypeIconOverlayCanvas)
            {
                standardCardView.attackTypeIconOverlay.color = "f8d3d9".ToColor();
            }

            yield return Timing. WaitForSeconds(0.04f);

            standardCardView.frontOverlay.image.color = Color.white;

            if (_view.data.type == CardType.Minion && standardCardView.attackTypeIconOverlayCanvas)
            {
                standardCardView.attackTypeIconOverlay.color = Color.white;
            }

            IsFinishedTransforming = true;
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            throw new System.NotImplementedException();
        }
    }
}