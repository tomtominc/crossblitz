using System;
using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI.GameLogic;
using DG.Tweening;
using FMOD.Studio;
using TakoBoyStudios.Events;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;
using Random = UnityEngine.Random;

namespace CrossBlitz.GameVfx.Minion
{
    public class RiffEffect : MonoBehaviour, IGameVfx
    {
        public SpriteAnimation musicNodePrefab;
        public SpriteAnimation animator;

        private CardView _card;
        private GameCardView _gameCard;

        private int _currentBeatColor = 1;
        private bool _updateBeat;

        private RectTransform _rect;
        private static event Action OnBeat;
        private List<SpriteAnimation> _pooledNotes;

        public void Initialize(CardView card)
        {
            _card = card;
            _updateBeat = false;
            _rect = transform as RectTransform;
            _gameCard = _card.GetCardComponent<GameCardView>();
            _currentBeatColor = 1;
            _pooledNotes = new List<SpriteAnimation>();

            Events.Subscribe(EventType.OnCardPlayed, OnCardPlayed);

            var beatCallback = new EVENT_CALLBACK(BeatEventCallback);
            var music = AudioController.GetCurrentSong();
            music.setCallback(beatCallback,EVENT_CALLBACK_TYPE.TIMELINE_BEAT | EVENT_CALLBACK_TYPE.TIMELINE_MARKER);

            _gameCard.Events.OnSilenced += OnSilenced;
            _gameCard.Events.OnTransformed += OnSilenced;
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnCardPlayed, OnCardPlayed);
            OnBeat -= OnPlayBeat;
        }

        [AOT.MonoPInvokeCallback(typeof(EVENT_CALLBACK))]
        private static FMOD.RESULT BeatEventCallback(EVENT_CALLBACK_TYPE type, IntPtr otherPtr, IntPtr parameterPtr)
        {
            switch (type)
            {
                case EVENT_CALLBACK_TYPE.TIMELINE_BEAT:
                {
                    OnBeat?.Invoke();

                    break;
                }
                case EVENT_CALLBACK_TYPE.TIMELINE_MARKER:
                {
                    break;
                }
            }

            return FMOD.RESULT.OK;
        }

        public void Initialize(Transform rect)
        {
            _rect = rect as RectTransform;
        }

        private void OnCardPlayed(IMessage message)
        {
            if (message.Data is OnCardPlayedEventArgs args)
            {
                var card = GameServer.State.GetCard(args.cardUid);
                if (card != null && card.owner == _gameCard.State.owner && card.data.type == CardType.Spell )
                {
                     Activate();
                }
            }
        }

        // private void OnFrozen(GameCardView card) {}
        // private void OnUnFrozen(GameCardView card) {}
        // private void OnDamaged(GameCardView card) {}

        public void Spawn()
        {
            OnBeat += OnPlayBeat;

            gameObject.SetActive(true);
            animator.SetActive(true);
            _updateBeat = true;
        }

        private void OnPlayBeat()
        {
            if (!_updateBeat) return;

            PlayBeat();
        }

        private void PlayBeat()
        {
            animator.Play($"idle-{_currentBeatColor}");
            _currentBeatColor = _currentBeatColor == 1 ? 2 : 1;

            var randomAngle = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
            var randomNote = Random.Range(1, 4);
            var note = GetNote();
            note.RectTransform().anchoredPosition= Vector2.zero;
            note.RectTransform()
                .DOAnchorPos(new Vector2(50, 50) * randomAngle, 0.24f)
                .SetEase(Ease.OutSine);
            note.image.color = Color.white;
            note.image.DOFade(0, 0.24f).SetDelay(0.24f);
            note.Play($"spawn-{randomNote}", () =>
            {
                note.Play($"idle-{randomNote}", () =>
                {
                    _pooledNotes.Add(note);
                    note.SetActive(false);
                });
            });
        }


        private SpriteAnimation GetNote()
        {
            SpriteAnimation note;

            if (_pooledNotes.Count > 0)
            {
                note = _pooledNotes[0];
                note.transform.SetParent(_rect, false);
                _pooledNotes.RemoveAt(0);
            }
            else
            {
                note = Instantiate(musicNodePrefab, _rect, false);
            }

            note.SetActive(true);

            return note;
        }

        private void Activate()
        {
            OnBeat -= OnPlayBeat;

            Events.Unsubscribe(EventType.OnCardPlayed, OnCardPlayed);
            _updateBeat = false;
            animator.Play("activate");
        }

        private void OnSilenced(GameCardView card)
        {
            _updateBeat = false;

            OnBeat -= OnPlayBeat;
            gameObject.SetActive(false);

            Events.Unsubscribe(EventType.OnCardPlayed, OnCardPlayed);
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            yield break;
        }
    }
}