using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.CardCollection;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.Databases;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.InputAPI;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI.GameLogic;
using DG.Tweening;
using UnityEngine;

namespace CrossBlitz.GameVfx
{
    public class CardControlsEffect : MonoBehaviour, IGameVfx
    {
        public RectTransform controlsParent;
        public ControlItem controlItemPrefab;

        private CardView m_cardView;
        private string m_lastCardId;
        private RectTransform m_rect;
        private List<Controls.Actions> m_actions;

        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            m_cardView = card;
            m_rect = m_cardView.RectTransform();
            m_lastCardId = m_cardView.data?.id;

            this.SetActive(true);

            ChangeControls();
        }

        private void Update()
        {
            if (m_lastCardId != m_cardView.data.id)
            {
                m_lastCardId = m_cardView.data.id;
                ChangeControls();
            }
        }

        private void OnEnable()
        {
            ChangeControls();
        }

        public void AddControls(List<Controls.Actions> actions)
        {
            m_actions ??= new List<Controls.Actions>();

            if (actions == null)
            {
                m_actions.Clear();
                controlsParent.DestroyChildren();
                return;
            }

            for (var i = 0; i < actions.Count; i++)
            {
                if (m_actions.Contains(actions[i]))
                {
                    continue;
                }

                var controlItem = Instantiate(controlItemPrefab, controlsParent, false);
                controlItem.SetAction(actions[i]);
                m_actions.Add(actions[i]);
            }
        }

        private void ChangeControls()
        {
            AddControls(null);

            if (DeckEditMenu.Instance)
            {
                if (DeckEditMenu.Instance.Settings.OpenMode == DeckOpenMode.DeckEditor)
                {
                    if (DeckCardList.IsPointerOverCardList)
                    {
                        AddControls(new List<Controls.Actions>
                        {
                            Controls.Actions.RemoveCard
                        });
                    }
                    else
                    {
                        AddControls(new List<Controls.Actions>
                        {
                            Controls.Actions.AddCard
                        });
                    }
                }
            }
            else if (GameServer.IsRunning())
            {
                if (m_cardView.keywordDescriptionPopup != null &&
                    m_cardView.keywordDescriptionPopup.modifierTooltip.IsReady() &&
                    m_cardView.keywordDescriptionPopup.modifierTooltip.pages. GetMaxNumberOfPages() > 1)
                {
                    AddControls( new List<Controls.Actions>
                    {
                        Controls.Actions.TurnPageLeft,
                        Controls.Actions.TurnPageRight,
                    });
                }
            }

            var subCard = m_cardView.GetVfx<SubCardComponent, SubCardEffect>();

            if (subCard && subCard.HasTokens())
            {
                AddControls(new List<Controls.Actions>
                {
                    Controls.Actions.Swap
                });
            }
        }


        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            throw new System.NotImplementedException();
        }
    }
}