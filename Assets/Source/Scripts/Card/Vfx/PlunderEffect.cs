using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class PlunderEffect : MonoBehaviour, IGameVfx
    {
        private const int FadeFrames = 3;
        private const float FrameTime = 0.08f;
        public SpriteAnimation anim;
        public Color overlayColor;

        private CardView _card;
        private GameCardView _gameCard;

        private RectTransform m_rect;

        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }
        public void Initialize(CardView card)
        {
            _card = card;
            _gameCard = _card.GetCardComponent<GameCardView>();
        }

        /// <summary>
        /// Play is used when spawning the effect on top of the card.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            gameObject.SetActive(true);

            // Play Anim
            anim.Play("activate-plunder");

            // Shake Card
            //_card.RectTransform().DOShakeAnchorPos(0.5f, 5, 50, 0);

            while (!anim.IsDone) yield return Timing.WaitForOneFrame;

            yield return Timing.WaitForSeconds(1f);

            if (this == null) yield break;

            gameObject.SetActive(false);
        }
    }
}