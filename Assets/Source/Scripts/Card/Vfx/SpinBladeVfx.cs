using System;
using System.Collections;
using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AssetManagement;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using DG.Tweening;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.GameVfx.Spell
{
    public class SpinBladeVfx : MonoBehaviour, IGameVfx
    {
        private const float Inset = -50f;
        private const float MaxTime = 5;

        public RectTransform boundsRect;
        public SpriteAnimation spinBlade;
        public CanvasGroup spinBladeCanvas;

        private bool _updateCollisions;
        private RectTransform _spinBladeRect;
        private Vector2 _moveDirection;
        private Rect _moveBounds;
        private float _moveSpeed;

        private bool _finished;
        private float _bounceCount;
        private float _currentTime;

        private List<RectTransform> _currentlyColliding;
        private List<GameCardView> _testCollisionsWith;
        private List<GameCardView> _collidedWith;

        private DamageCommand _command;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {

        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            //Debug.Log("Spin Target Count " +  command.TargetUids.Count);
            //todo: erase this! not really how this is supposed to happen
            // yield return Timing.WaitUntilDone(GameplayScreen.Instance.heroPowerCinematic
            //         .PlayCinematic(!Command.IsOpponentCommand(command), command.SourceUid));

            _command = command as DamageCommand;
            _testCollisionsWith = new List<GameCardView>();
            _collidedWith = new List<GameCardView>();
            _currentlyColliding = new List<RectTransform>();
            _spinBladeRect = spinBlade.RectTransform();

            _spinBladeRect.anchoredPosition = Vector2.zero;
            spinBlade.Play("spawn");

            for (var i = 0; i < _command?.TargetUids.Count; i++)
            {
                var getBoardCardTask = GameBoard.GetBoardCard(_command.TargetUids[i]);
                yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                var getBoardCardResult = getBoardCardTask.Result;

                if (getBoardCardResult.error != ClientErrorCode.None)
                {
                    Server.HandleClientError(getBoardCardResult, false);
                }

                if (getBoardCardResult.boardCard)
                {
                    _testCollisionsWith.Add(getBoardCardResult.boardCard);
                }
            }

            while (!spinBlade.IsDone) yield return Timing.WaitForOneFrame;

            spinBlade.Play("spin-loop");
            AudioController.PlaySound("HeroPower-Spinblade_Spinning", "SFX", false, gameObject);

            _moveDirection = Random.insideUnitCircle;

            var size = boundsRect.sizeDelta;
            _moveBounds = new Rect(-size.x/2f,-size.y/2f,
                size.x/2f, size.y/2f);
            _moveSpeed = 16;
            _bounceCount = 0;
            _updateCollisions = true;

            while (!_finished) yield return Timing.WaitForOneFrame;

            AudioController.StopSound("HeroPower-Spinblade_Spinning");
            _updateCollisions = false;

            yield return Timing.WaitUntilDone(spinBladeCanvas.DOFade(0, 0.125f).WaitForCompletion(true));
        }

        private void Update()
        {
            if (_updateCollisions)
            {
                _currentTime += Time.deltaTime;

                MoveSpinBlade();
                UpdateCollisions();
            }
        }

        private void MoveSpinBlade()
        {
            if (Math.Abs(_moveDirection.x) < 0.1f)
            {
                _moveDirection.x = Random.Range(0.1f, 1f);
            }

            if (Math.Abs(_moveDirection.y) < 0.1f)
            {
                _moveDirection.y = Random.Range(0.1f, 1f);
            }

            var pos = _spinBladeRect.anchoredPosition;
            pos += _moveDirection.normalized * _moveSpeed;


            if (pos.x < _moveBounds.x)
            {
                pos.x = _moveBounds.x;
                _moveDirection.x = -_moveDirection.x;
                _bounceCount++;
                GameplayScreen.Instance.ShakeScreen(4, 2);
            }

            if (pos.x > _moveBounds.width)
            {
                pos.x = _moveBounds.width;
                _moveDirection.x = -_moveDirection.x;
                _bounceCount++;
                GameplayScreen.Instance.ShakeScreen(4, 2);
            }

            if (pos.y < _moveBounds.y)
            {
                pos.y = _moveBounds.y;
                _moveDirection.y = -_moveDirection.y;
                _bounceCount++;
                GameplayScreen.Instance.ShakeScreen(4, 2);
            }

            if (pos.y > _moveBounds.height)
            {
                pos.y = _moveBounds.height;
                _moveDirection.y = -_moveDirection.y;
                _bounceCount++;
                GameplayScreen.Instance.ShakeScreen(4, 2);
            }

            _spinBladeRect.anchoredPosition = pos;
        }

        private void UpdateCollisions()
        {
            for (var i = 0; i < _testCollisionsWith.Count; i++)
            {
                if (_testCollisionsWith[i] == null) continue;

                var rect = _testCollisionsWith[i].RectTransform();

                if (Vector2.Distance(_spinBladeRect.position, rect.position) < 2)
                {
                    if (_currentlyColliding.Contains(rect)) continue;

                    _currentlyColliding.Add(rect);

                    if (!_collidedWith.Contains(_testCollisionsWith[i]))
                    {
                        _collidedWith.Add(_testCollisionsWith[i]);
                        _testCollisionsWith[i].State.ApplyHistoryByCardGuid(_command.SourceUid);
                        var damageFx = _testCollisionsWith[i].View.GetVfx<DamageEffectComponent, DamageEffect>();
                        Timing.RunCoroutine(damageFx.GenericDamageAnimation(_command.Amount));
                        GameplayScreen.Instance.ShakeScreen(4, 2);
                        AudioController.PlaySound("vfx_sword_slash", "BARDSFX", true, gameObject);
                        // _hitPause = true;
                        // _hitPauseTimer = 0.1f;
                    }
                }
                else if (_currentlyColliding.Contains(rect))
                {
                    _currentlyColliding.Remove(rect);
                }
            }

            if (_currentTime >= MaxTime)
            {
                for (var i = 0; i < _testCollisionsWith.Count; i++)
                {
                    if (_testCollisionsWith[i] == null) continue;
                    if (_collidedWith.Contains(_testCollisionsWith[i])) continue;

                    _collidedWith.Add(_testCollisionsWith[i]);
                    _testCollisionsWith[i].State.ApplyHistoryByCardGuid(_command.SourceUid);
                    var damageFx = _testCollisionsWith[i].View.GetVfx<DamageEffectComponent, DamageEffect>();
                    Timing.RunCoroutine(damageFx.GenericDamageAnimation(_command.Amount));
                    GameplayScreen.Instance.ShakeScreen(4, 2);
                }

                _finished = true;
                _updateCollisions = false;
                _bounceCount = 1000;
            }

            if (_collidedWith.Count == _testCollisionsWith.Count && _bounceCount > 10)
            {
                // we collided with everything!

                _finished = true;
            }
        }

        public void Unload()
        {
            AddressableReferenceLoader.Unload(gameObject);
        }
    }
}