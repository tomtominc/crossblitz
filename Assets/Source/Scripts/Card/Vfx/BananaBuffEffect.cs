using System.Collections.Generic;
using Asyncoroutine;
using BlendModes;
using CrossBlitz.AssetManagement;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Hero;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.Models;
using CrossBlitz.Transition;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class BananaBuffEffect : MonoBehaviour, IGameVfx
    {
        public BananaProjectile bananaPrefab;
        public Color overlayColor;
        private CardView _card;

        private RectTransform m_rect;
        private GetBoardCardResult m_getBoardCardResult;
        private HeroViewBattle m_heroTarget;
        private GameTile m_tile;
        private Vector3 m_position;

        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            gameObject.SetActive(true);

            // Setup targets and positions
            // var damageCommand = (DamageCommand)command;
            //var damageHistory = damageCommand.PendingDamageHistories[0];
            //var amount = damageHistory.DamageAmount;
            var powerCommand = command as ModifyHealthAndPowerCommand;
            var card = GameServer.State.GetCard(powerCommand.SourceUid);

            var getBoardCardTask = GameBoard.GetBoardCard(card.uid);
            yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
            m_getBoardCardResult = getBoardCardTask.Result;

            if (m_getBoardCardResult.error != ClientErrorCode.None)
            {
                Server.HandleClientError(m_getBoardCardResult, true);
            }

            var tileState = GameServer.State.Board.GetTile(card.uid);

            _card = m_getBoardCardResult.boardCard.View;

            // get a reference to the card object (child object of the card so we aren't moving the card directly.
            var cardRect = _card.cardObject;
            var time = powerCommand.PendingPowerHistories.Count-1 * 0.25f;
            cardRect.DOPunchAnchorPos(Vector2.left*5f, time);

            for (var i = 0; i < powerCommand.PendingPowerHistories.Count; i++)
            {
                var getTargetCardTask = GameBoard.GetBoardCard(powerCommand.PendingPowerHistories[i].CardUid);
                yield return Timing.WaitUntilDone(getTargetCardTask.AsCoroutine());
                var getTargetCardResult = getTargetCardTask.Result;

                if (getTargetCardResult.error != ClientErrorCode.None)
                {
                    Server.HandleClientError(getTargetCardResult, true);
                }

                m_position = getTargetCardResult.boardCard.transform.position;

                // Tint card
                _card.TintOverTime(overlayColor, BlendMode.Darken, 0.25f);
                _card.RemoveTint();
                _card.Tint(Color.white, BlendMode.Lighten);
                _card.RemoveTintOverTime(0.25f);

                // Spawn Banana
                var banana = Instantiate(bananaPrefab, _card.transform);
                //GameplayScreen.Instance.ShakeScreen(4, 2);
                banana.animator.Play("spawn");
                banana.animator.paused = true;
                AudioController.PlaySound("vfx_lob", "BARDSFX", true, gameObject);
                var startPos = _card.transform.position;
                var targetPos = getTargetCardResult.boardCard.transform.position;
                
                if (i == powerCommand.PendingPowerHistories.Count - 1)
                {
                    yield return Timing.WaitUntilDone(banana.JumpToTile(startPos, targetPos, getTargetCardResult, powerCommand, i,  null));
                }
                else
                {
                    Timing.RunCoroutine(banana.JumpToTile(startPos, targetPos, getTargetCardResult, powerCommand, i, null));
                    yield return Timing.WaitForSeconds(.25f);
                }

            }




            //GameplayScreen.Instance.ShakeScreen(4, 2);
            //TransitionController.FlashScreen(Color.yellow, BlendMode.Lighten);

            // Banana Dissolve
            //banana.animator.paused = false;


            //Debug.Log("HURT BOARD CARD");
            //vfxParameters.OnApply?.Invoke();

            //var damageFx = m_getBoardCardResult.boardCard.View.GetVfx<DamageEffectComponent, DamageEffect>();
            //yield return Timing.WaitUntilDone(damageFx.GenericDamageAnimation(amount));
            

            // End process
            gameObject.SetActive(false);
        }
    }
}