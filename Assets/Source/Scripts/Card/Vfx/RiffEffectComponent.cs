using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class RiffEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<RiffEffect>("RiffEffect", card.transform);
            effect.Initialize(card);
        }
    }
}