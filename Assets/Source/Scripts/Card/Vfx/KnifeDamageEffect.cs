using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class KnifeDamageEffect : MonoBehaviour
    {
        public int maxShakeTimes = 6;
        public float knifeFallSpeed = 20;
        public float targetOffsetY = 2;
        public SpriteAnimation knife;
        public SpriteAnimation knifeShadow;

        private GameCardView m_targetCard;

        private bool m_shakeTargetCard;
        private int m_shakeTimes;
        private float m_shakeFrameTime;

        private void Update()
        {
            if (m_shakeTargetCard && m_shakeTimes < maxShakeTimes && m_targetCard != null)
            {
                m_shakeFrameTime += Time.deltaTime;

                if (m_shakeFrameTime >= 0.04f)
                {
                    m_shakeFrameTime = 0;
                    m_shakeTimes++;

                    m_targetCard.View.cardObject.anchoredPosition =
                        new Vector2(2 * (m_shakeTimes % 2 == 0 ? 1 : -1), 0);
                }

                if (m_shakeTimes >= maxShakeTimes)
                {
                    m_targetCard.View.cardObject.anchoredPosition = Vector2.zero;
                    m_shakeTargetCard = false;
                    m_shakeTimes = 0;
                    m_shakeFrameTime = 0;
                }
            }
        }

        public IEnumerator<float> Play(VfxSourceAndTargetData vfxData, Command command, int targetIndex)
        {
            gameObject.SetActive(true);

            var targetPosition = vfxData.targetCardPositions[targetIndex] + new Vector3(0, targetOffsetY, 0);
            m_targetCard = vfxData.targetCardObjects[targetIndex];

            // have the knife start really high
            knife.transform.position = targetPosition + new Vector3(0, 8, 0);
            knife.SetActive(true);

            knifeShadow.transform.position = targetPosition;
            knifeShadow.SetActive(true);

            knife.Play("falling");
            knifeShadow.Play("falling");

            // move knife and wait until it "hits"
            while (Vector3.Distance(knife.transform.position, targetPosition) > 0)
            {
                knife.transform.position = Vector3.MoveTowards(knife.transform.position, targetPosition,
                    knifeFallSpeed * Time.deltaTime);
                yield return Timing.WaitForOneFrame;
            }

            knife.Play("land");
            knifeShadow.Play("land");
            AudioController.PlaySound("vfx_sword_slash", "BARDSFX", true, gameObject);

            yield return Timing.WaitForSeconds(0.08f);

            // shake card for dramatic hit
            if (m_targetCard)
            {
                m_shakeTimes = 0;
                m_shakeFrameTime = 0;
                m_shakeTargetCard = true;
                m_targetCard.View.cardObject.anchoredPosition = new Vector2(2, 0);
            }

            // deal damage to card!
            if (command is DamageCommand damageCommand)
            {
                var targetState = vfxData.targetCards[targetIndex];

                if (targetState != null)
                {
                    var damageHistory = damageCommand.PendingDamageHistories[targetIndex];
                    targetState.ApplyHistory(damageHistory.Uid);

                    if (targetState.IsHero)
                    {
                        var heroView = GameplayScreen.Instance.heroSideBarContainer;
                        Timing.RunCoroutine(heroView.HitPortraitNonAttack(targetState.Team, damageCommand,
                            damageHistory.Uid));
                    }
                    else if (m_targetCard != null)
                    {
                        var amount = damageHistory.DamageAmount;
                        var damageFx = m_targetCard.View.GetVfx<DamageEffectComponent, DamageEffect>();

                        if (damageFx != null)
                        {
                            Timing.RunCoroutine(damageFx.GenericDamageAnimation(amount));
                        }
                    }
                }
                else
                {
                    Debug.LogError("Could not find target card in index!");
                }
            }


            while (!knife.IsDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            knife.Play("despawn");
            knifeShadow.Play("despawn");

            while (!knife.IsDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            Destroy(gameObject);
        }
    }
}