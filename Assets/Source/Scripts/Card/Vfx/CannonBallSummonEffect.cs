using System.Collections.Generic;
using System.Linq;
using Asyncoroutine;
using BlendModes;
using CrossBlitz.AssetManagement;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx.Spell;
using CrossBlitz.Hero;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.ServerAPI.Models;
using CrossBlitz.Transition;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class CannonBallSummonEffect : MonoBehaviour, IGameVfx
    {
        public CannonBall cannonBallPrefab;
        public Color overlayRed;
        private CardView _card;

        private RectTransform m_rect;
        //private GetBoardCardResult m_getBoardCardResult;
        //private HeroViewBattle m_heroTarget;
        private GameTile m_tile;
       //private Vector3 m_position;
       private List<CannonBall> m_destroyCannonballsAfterAnimation;

        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            gameObject.SetActive(true);

            var damageCommand = (DamageCommand)command;
            var targetCards = new List<GameCardView>();
            var routineHandles = new List<CoroutineHandle>();

            for (var i = 0; i < damageCommand.PendingDamageHistories.Count; i++)
            {
                var damageHistory = damageCommand.PendingDamageHistories[i];
                var amount = damageHistory.DamageAmount;

                var sourceCard = GameServer.State.GetCard(damageHistory.AppliedBy);
                var targetCard = GameServer.State.GetCard(damageHistory.CardUid);

                if (i < damageCommand.PendingDamageHistories.Count-1)
                {
                    routineHandles.Add(Timing.RunCoroutine(ShootCannonball(vfxParameters, sourceCard, targetCard, targetCards, damageHistory,damageCommand, amount)));
                    yield return Timing.WaitForSeconds(1f);
                }
                else
                {
                    yield return Timing.WaitUntilDone(ShootCannonball(vfxParameters, sourceCard, targetCard, targetCards,
                        damageHistory, damageCommand, amount));
                }
            }

            if (routineHandles.Any(r => r.IsRunning))
            {
                yield return Timing.WaitForOneFrame;
            }

            yield return Timing.WaitUntilDone(GameVfxUtilities.EvaluateDeath(targetCards));

            // End process
            gameObject.SetActive(false);
        }

        private IEnumerator<float> ShootCannonball(VfxParameters vfxParameters, GameCardState sourceCard, GameCardState targetCard, List<GameCardView> targetCards, CardHistory damageHistory, DamageCommand damageCommand, int amount)
        {
            Vector3 startPosition;
            Vector3 endPosition;
            GameCardView targetCardObject = null;

            if (sourceCard.IsHero || sourceCard.characterData.type == CardType.Spell ||
                sourceCard.characterData.type == CardType.Power || sourceCard.characterData.type == CardType.Relic ||
                sourceCard.characterData.type == CardType.Elder_Relic)
            {
                var sourceHero = GameplayScreen.Instance.heroSideBarContainer.GetHeroView(sourceCard.Team);
                startPosition = sourceHero.boardTarget.transform.position + Vector3.down;
            }
            else
            {
                var getBoardCardTask = GameBoard.GetBoardCard(sourceCard.uid);
                yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                var getBoardCardResult = getBoardCardTask.Result;

                if (getBoardCardResult.error != ClientErrorCode.None)
                {
                    startPosition = getBoardCardResult.worldPositionIfNotFound;
                }
                else
                {
                    startPosition = getBoardCardResult.boardCard.transform.position;
                }
            }

            // get the target card

            if (targetCard.IsHero)
            {
                var targetHero = GameplayScreen.Instance.heroSideBarContainer.GetHeroView(targetCard.Team);
                endPosition = targetHero.boardTarget.transform.position + Vector3.down;
            }
            else
            {
                var getBoardCardTask = GameBoard.GetBoardCard(targetCard.uid);
                yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                var getBoardCardResult = getBoardCardTask.Result;

                if (getBoardCardResult.error != ClientErrorCode.None)
                {
                    Server.HandleClientError(getBoardCardResult, true);
                    yield break;
                }

                endPosition = getBoardCardResult.boardCard.transform.position;
                targetCardObject = getBoardCardResult.boardCard;
                targetCards.AddDistinct(targetCardObject);
            }

            if (sourceCard.IsHero || sourceCard.characterData.type == CardType.Spell ||
                sourceCard.characterData.type == CardType.Power)
            {
                // do nothing?
            }
            else if (_card != null)
            {
                var sourceCardObject = _card;

                var cardRect = sourceCardObject.cardObject;
                var origAnchorPos = cardRect.anchoredPosition;

                // Shake Cannon
                // sourceCardObject.IsAnimating(true);
                // sourceCardObject.TintOverTime(overlayRed, BlendMode.Darken, 0.25f);
                // cardRect.DOShakeAnchorPos(0.25f, 5, 50, 0);
                // yield return Timing.WaitForSeconds(0.25f);

                // ShootFX
                sourceCardObject.StartAnimating();
                sourceCardObject.RemoveTint();
                sourceCardObject.Tint(Color.white, BlendMode.Lighten);
                sourceCardObject.RemoveTintOverTime(0.25f);

                // Spawn Cannonball
                cardRect.DOPunchAnchorPos(Vector2.down * 20, 0.25f, 2, 0).SetEase(Ease.OutBack).OnComplete(() => cardRect.DOAnchorPos(origAnchorPos, 0.1f));
            }

            var cannonBall = Instantiate(cannonBallPrefab, transform);
            cannonBall.transform.position = startPosition;
            cannonBall.m_targetCard = _card;

            GameplayScreen.Instance.ShakeScreen();

            cannonBall.animator.Play("idle");
            AudioController.PlaySound("vfx_cannonshot", "BARDSFX", true, gameObject);

            yield return Timing.WaitUntilDone(cannonBall.JumpToTile(endPosition, null));

            cannonBall.animator.Play("pre-explode");
            yield return Timing.WaitForSeconds(.05f);

            GameplayScreen.Instance.ShakeScreen();
            TransitionController.FlashScreen(Color.yellow, BlendMode.Lighten);
            AudioController.PlaySound("vfx_fiery_explosion", "BARDSFX", true, gameObject);
            cannonBall.Burst();

            if (targetCard.IsHero)
            {
                targetCard.ApplyHistory(damageHistory.Uid);
                Timing.RunCoroutine(GameplayScreen.Instance.heroSideBarContainer.HitPortraitNonAttack(targetCard.Team,damageCommand, damageHistory.Uid));

            }
            else if (targetCardObject && targetCardObject.View != null)
            {
                targetCardObject.State.ApplyHistory( damageHistory.Uid );
                var damageFx = targetCardObject.View.GetVfx<DamageEffectComponent, DamageEffect>();

                if (damageFx != null)
                {
                    Timing.RunCoroutine(damageFx.GenericDamageAnimation(amount, true));
                }
            }

            while (!cannonBall.animatorBurst.IsDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            Destroy(cannonBall.gameObject);
            if (_card)
            {
                _card.EndAnimating();
            }
        }
    }
}