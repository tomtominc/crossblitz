using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class SpellTargetComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<SpellTargetEffect>("SpellTargetEffect", card.RectTransform());
            effect.Initialize(card);
        }
    }
}