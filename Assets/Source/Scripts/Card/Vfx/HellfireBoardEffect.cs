using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Spell
{
    public class HellfireBoardEffect : MonoBehaviour, IGameVfx
    {
        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }
        public void Initialize(CardView card){}

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            var damageCommand = (DamageCommand) command;
            var damagedBoardCards = new List<GameCardView>();

            for (var i = 0; i < damageCommand.TargetUids.Count; i++)
            {
                var getBoardCardTask = GameBoard.GetBoardCard(damageCommand.TargetUids[i]);
                yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                var getBoardCardResult = getBoardCardTask.Result;

                if (getBoardCardResult.error != ClientErrorCode.None)
                {
                    Server.HandleClientError(getBoardCardResult, true);
                    continue;
                }

                damagedBoardCards.AddDistinct(getBoardCardResult.boardCard);
            }

            for (var i = 0; i < damagedBoardCards.Count; i++)
            {
                var hellfireDamageEffectTask = damagedBoardCards[i].GetVfx<HellfireEffectComponent>();

                yield return Timing.WaitUntilDone(hellfireDamageEffectTask.AsCoroutine());

                if (hellfireDamageEffectTask.Result.Effect == null)
                {
                    hellfireDamageEffectTask.Result.Initialize(damagedBoardCards[i].GetComponent<CardView>());
                    hellfireDamageEffectTask.Result.OnAfterCreation();
                    Debug.Log("Needed a backup asset for this");
                    while (hellfireDamageEffectTask.Result.Effect == null) yield return Timing.WaitForOneFrame;
                }

                if (hellfireDamageEffectTask.Result != null && hellfireDamageEffectTask.Result.Effect != null)
                {
                    //var effect = (KnifeDamageEffect) knifeDamageEffectTask.Result.Effect;
                    if (hellfireDamageEffectTask.Result.Effect is HellfireEffect effect)
                    {
                        if (i == damagedBoardCards.Count - 1)
                        {
                            yield return Timing.WaitUntilDone(effect.DealDamage(damageCommand.Amount));
                        }
                        else
                        {
                            Timing.RunCoroutine(effect.DealDamage(damageCommand.Amount));
                            yield return Timing.WaitForSeconds(.125f);
                        }
                    }
                }
            }
        }
    }
}