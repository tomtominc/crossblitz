using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.GameVfx;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class GiveFrostAbilityEffect : MonoBehaviour, IGameVfx
    {
        public CycloneSnowParticles snowParticlePrefab;

        public void Initialize(CardView card)
        {

        }

        public void Initialize(Transform rect)
        {

        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            var getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(command);
            yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
            var vfxData = getSourceAndTargetData.Result;
            var handles = new List<CoroutineHandle>();

            for (var i = 0; i < vfxData.targetCardObjects.Count; i++)
            {
                if (vfxData.targetCardObjects[i] != null)
                {
                    vfxData.targetCardObjects[i].UpdateViewFromState();
                }
            }

            for (var i = 0; i < vfxData.targetCards.Count; i++)
            {
                var snowParticles = Instantiate(snowParticlePrefab, transform, false);
                handles.Add( Timing.RunCoroutine(snowParticles.Play(vfxData, command, i)));
            }

            while (handles.Exists(handle => handle.IsRunning)) yield return Timing.WaitForOneFrame;

            for (var i = 0; i < vfxData.targetCardObjects.Count; i++)
            {
                if (vfxData.targetCardObjects[i] != null)
                {
#pragma warning disable CS4014
                    vfxData.targetCardObjects[i].ShowAbilityEffect();
#pragma warning restore CS4014
                }
            }



            Destroy(gameObject, 2);
        }
    }
}