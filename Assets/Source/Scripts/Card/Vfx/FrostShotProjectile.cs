using System;
using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class FrostShotProjectile : MonoBehaviour
    {
        public float moveSpeed = 20;
        public SpriteAnimation projectile;
        public GameObject trailParticles;
        public GameObject hitEffect;

        public IEnumerator<float> Play(VfxSourceAndTargetData vfxData, Command command, int targetIndex, bool fast=false, bool fromOffscreen=false, bool skipDeathEvaluation = false, float moveSpeedOverride = -1, Action<int> onHit = null)
        {
            gameObject.SetActive(true);

            var sourcePosition = vfxData.sourceCardPosition;
            var sourceRect = vfxData.sourceRect;
            transform.position = sourcePosition;

            var targetPosition = vfxData.targetCardPositions[targetIndex];
            var targetRect = vfxData.targetRects[targetIndex];
            var targetCard = vfxData.targetCardObjects[targetIndex];

            if (fromOffscreen)
            {
                sourcePosition = targetPosition + new Vector3(-20, 20);
                sourceRect = null;
                transform.position = sourcePosition;
            }

            if (moveSpeedOverride > 0)
            {
                moveSpeed = moveSpeedOverride;
            }

            // get direction info
            var dir = (targetPosition - sourcePosition).normalized;
            var dirNeg = (sourcePosition - targetPosition).normalized;
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            var angleNeg = Mathf.Atan2(dirNeg.y, dirNeg.x) * Mathf.Rad2Deg;

            // rotate the particles in the negative direction so they fire away as the projectile is moving.
            trailParticles.transform.eulerAngles = new Vector3(0, 0, angleNeg);

            // rotate the hit effect in the same direction as the projectile.
            hitEffect.transform.eulerAngles = new Vector3(0, 0, angle);

            if (!fast)
            {
                projectile.Play("spawn-projectile");
                while (!projectile.IsDone) yield return Timing.WaitForOneFrame;
            }

            projectile.Play("projectile-idle");

            if (!fast)
            {
                // pause before firing?
                yield return Timing.WaitForSeconds(0.12f);
            }

            trailParticles.SetActive(true);

            if (!fromOffscreen)
            {
                // shake the screen when fired
                GameplayScreen.Instance.ShakeScreen();
            }

            if (sourceRect != null)
            {
                sourceRect.DOKill();
                sourceRect.DOPunchAnchorPos(-dir * 4f, 0.12f);
            }

            AudioController.PlaySound("vfx_wind", "BARDSFX", true, gameObject);
            while (Vector3.Distance(projectile.transform.position, targetPosition) > 1)
            {
                projectile.transform.position = Vector3.MoveTowards(projectile.transform.position, targetPosition, moveSpeed * Time.deltaTime);
                yield return Timing.WaitForOneFrame;
            }

            // shake the screen on hit
            GameplayScreen.Instance.ShakeScreen();

            projectile.transform.position = targetPosition;

            if (targetRect != null)
            {
                targetRect.DOKill();
                targetRect.DOPunchAnchorPos(dir * 4f, 0.12f);
            }

            // handles damage and applies history
            GameVfxUtilities.HandleDamageCommand(command, vfxData, targetIndex, targetCard, skipDeathEvaluation);

            var trailSystems = trailParticles.GetComponentsInChildren<ParticleSystem>();

            for (var i = 0; i < trailSystems.Length; i++)
            {
                trailSystems[i].Stop(true, ParticleSystemStopBehavior.StopEmitting);
            }

            hitEffect.SetActive(true);
            projectile.Play("explode");

            AudioController.PlaySound("vfx_frozen", "BARDSFX", true, gameObject);

            onHit?.Invoke(targetIndex);

            while (!projectile.IsDone) yield return Timing.WaitForOneFrame;

            projectile.image.enabled = false;

            // yield return Timing.WaitForSeconds(1.2f);
            //
            // Destroy(gameObject);
        }
    }
}