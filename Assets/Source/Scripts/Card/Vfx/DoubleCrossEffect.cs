using System.Collections.Generic;
using Asyncoroutine;
using BlendModes;
using CrossBlitz.AssetManagement;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Hero;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.Models;
using CrossBlitz.Transition;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class DoubleCrossEffect : MonoBehaviour, IGameVfx
    {
        public SpriteAnimation doubleCrossAnim;
        public DoubleCrossHitEffect DoubleCrossHit;
        public Color overlayRed;
        public RectTransform effectRectTransform;

        private CardView _card;
        private RectTransform m_rect;
        private GetBoardCardResult m_getBoardCardResult;
        private List<GameCardView> _boardCardResults;
        private HeroViewBattle m_heroTarget;
        private GameTile m_tile;
        private Vector3 m_position;

        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            gameObject.SetActive(true);

            _boardCardResults = new List<GameCardView>();
            var damageCommand = (DamageCommand)command;

            var historyCard = damageCommand.PendingDamageHistories[0];
            var hurtCard = GameServer.State.GetCard(historyCard.CardUid);

            // Set Position of the Effect
            var getBoardCardTask = GameBoard.GetBoardCard(hurtCard.uid);
            yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
            var boardCardResult = getBoardCardTask.Result;
            effectRectTransform.position = new Vector2(boardCardResult.boardCard.transform.position.x, boardCardResult.boardCard.transform.position.y);

            // Play Animation
            doubleCrossAnim.SetActive(true);
            doubleCrossAnim.Play("attack");
            AudioController.PlaySound("vfx_dual_strike", "BARDSFX", true, gameObject);
            yield return Timing.WaitForSeconds(0.3f);

            boardCardResult.boardCard.RectTransform().DOPunchAnchorPos(Vector2.up * 15, 0.5f, 1);
            yield return Timing.WaitForSeconds(0.05f);

            AudioController.PlaySound("vfx_sword_slash", "BARDSFX", true, gameObject);
            for (int i = 0; i < damageCommand.PendingDamageHistories.Count; i++)
            {
                // Setup targets and positions
                var damageHistory = damageCommand.PendingDamageHistories[i];
                var amount = damageHistory.DamageAmount;
                var card = GameServer.State.GetCard(damageHistory.CardUid);

                getBoardCardTask = GameBoard.GetBoardCard(card.uid);
                yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                var getBoardCardResult = getBoardCardTask.Result;

                if (getBoardCardResult.error != ClientErrorCode.None)
                {
                    Server.HandleClientError(getBoardCardResult, true);
                }

                _boardCardResults.Add(getBoardCardResult.boardCard);

                // Spawn the pyre effect
               // var projectile = Instantiate(DoubleCrossHit, getBoardCardResult.boardCard.View.transform, false);
                //projectile.Initialize(getBoardCardResult.boardCard.View);
                //projectile.damageIndex = i;
                //Timing.RunCoroutine(projectile.Play(command, vfxParameters));
            }

            GameplayScreen.Instance.ShakeScreen(4, 2);





            //Debug.Log("HURT BOARD CARD");
            //vfxParameters.OnApply?.Invoke();
            //var damageFx = m_getBoardCardResult.boardCard.View.GetVfx<DamageEffectComponent, DamageEffect>();
            //yield return Timing.WaitUntilDone(damageFx.GenericDamageAnimation(amount));

            // End process
            //yield return Timing.WaitForSeconds(0.25f);

            // Do Damage to cards
            for (var i = 0; i < _boardCardResults.Count; i++)
            {
                _boardCardResults[i].State.ApplyHistoryByCardGuid(damageCommand.SourceUid);
                var damageFx = _boardCardResults[i].View.GetVfx<DamageEffectComponent, DamageEffect>();
                Timing.RunCoroutine(damageFx.GenericDamageAnimation(damageCommand.Amount));
            }

            yield return Timing.WaitForSeconds(2f);

            //yield return Timing.WaitForSeconds(2f);
            Destroy(gameObject);
        }
    }
}