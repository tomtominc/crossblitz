using System;
using System.Collections;
using System.Collections.Generic;
using Asyncoroutine;
using BlendModes;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.Transition;
using DG.Tweening;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.GameVfx.Spell
{
    public class LiquidGoldEffect : MonoBehaviour, IGameVfx
    {
        public RectTransform manaContainerRect;
        public LiquidGoldProjectileEffect projectilePrefab;
        public Team team;

        private ModifyManaCommand _command;
        private RectTransform m_rect;

        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {

        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {

            _command = command as ModifyManaCommand;
            manaContainerRect.anchoredPosition = new Vector2(-0f, 0);

            //GameplayScreen.Instance.ShakeScreen(4, 2);
            //TransitionController.FlashScreen(Color.yellow, BlendMode.Color);
            var player = GameServer.GetPlayerState(_command.PlayerUid);
            var heroView = player.GetTeam() == Team.Client
                        ? GameplayScreen.Instance.heroSideBarContainer.playerHeroView
                        : GameplayScreen.Instance.heroSideBarContainer.opponentHeroView;

            var manaPipTransform = heroView.manaView.activePip.RectTransform();

            var xScale = 1;
            var numProjectiles = 0;

            yield return Timing.WaitForSeconds(0.5f);

            for (var i = 0; i < _command.Amount; i++)
            {
                var goldProjectile = Instantiate(projectilePrefab, manaContainerRect, false);
                goldProjectile.Setup(xScale, manaPipTransform);
                yield return Timing.WaitUntilDone(goldProjectile.Play(command, vfxParams));

                xScale *= -1;
                numProjectiles += 1;
            }

            yield return Timing.WaitForSeconds(0.5f);

            Destroy(gameObject);
        }


        public void Unload()
        {
            AddressableReferenceLoader.Unload(gameObject);
        }
    }
}