using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using CrossBlitz.GameVfx.Minion;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class DraconicDiveEffect : MonoBehaviour
    {
        public float magicBallMoveSpeed = 200;
        public SpriteAnimation magicBallOverlay;
        public SpriteAnimation magicBall;
        public SpriteAnimation swirlUnderlay;
        public SpriteAnimation absorbOverlay;

        private Color m_sourceTintColor;
        private Color m_targetTintColor;

        private bool m_updateMagicBallOverlayTint;
        private bool m_updateAbsorbTint;

        private GameCardView m_sourceCard;
        private GameCardView m_targetCard;

        private void Update()
        {
            if (m_updateMagicBallOverlayTint && magicBallOverlay.IsActive() && m_sourceCard != null)
            {
                switch (magicBallOverlay.CurrentFrame)
                {
                    case 0:
                    {
                        m_sourceTintColor.a = 50/255f;
                        m_sourceCard.View.Tint(m_sourceTintColor, BlendMode.Color, true);
                        break;
                    }
                    case 1:
                    {
                        m_sourceTintColor.a = 120/255f;
                        m_sourceCard.View.Tint(m_sourceTintColor, BlendMode.Color, true);
                        break;
                    }
                    case 11:
                    {
                        m_sourceTintColor.a = 140/255f;
                        m_sourceCard.View.Tint(m_sourceTintColor, BlendMode.Color, true);
                        break;
                    }
                    case 12:
                    {
                        m_sourceTintColor.a = 120/255f;
                        m_sourceCard.View.Tint(m_sourceTintColor, BlendMode.Color, true);
                        break;
                    }
                    case 13:
                    {
                        m_sourceTintColor.a = 100/255f;
                        m_sourceCard.View.Tint(m_sourceTintColor, BlendMode.Color, true);
                        break;
                    }
                    case 14:
                    {
                        m_sourceTintColor.a = 80/255f;
                        m_sourceCard.View.Tint(m_sourceTintColor, BlendMode.Color, true);
                        break;
                    }
                    case 15:
                    {
                        m_sourceTintColor.a = 60/255f;
                        m_sourceCard.View.Tint(m_sourceTintColor, BlendMode.Color, true);
                        break;
                    }
                    case 16:
                    {
                        m_sourceCard.View.RemoveTint();
                        break;
                    }
                    default:
                    {
                        m_sourceTintColor.a = 160/255f;
                        m_sourceCard.View.Tint(m_sourceTintColor, BlendMode.Color, true);
                        break;
                    }
                }
            }

            if (m_updateAbsorbTint && absorbOverlay.IsActive() && m_targetCard != null)
            {
                switch (absorbOverlay.CurrentFrame)
                {
                    case 0:
                    {
                        m_targetTintColor.a = 40/255f;
                        m_targetCard.View.Tint(m_targetTintColor, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(0, -2);
                        break;
                    }
                    case 1:
                    {
                        m_targetTintColor.a = 80/255f;
                        m_targetCard.View.Tint(m_targetTintColor, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(0, 2);
                        break;
                    }
                    case 2:
                    {
                        m_targetTintColor.a = 120/255f;
                        m_targetCard.View.Tint(m_targetTintColor, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(0, 3);
                        break;
                    }
                    case 3:
                    {
                        m_targetTintColor.a = 160/255f;
                        m_targetCard.View.Tint(m_targetTintColor, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(0, 3);
                        break;
                    }
                    case 4:
                    {
                        m_targetTintColor.a = 170/255f;
                        m_targetCard.View.Tint(m_targetTintColor, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(0, 4);
                        break;
                    }
                    case 5:
                    {
                        m_targetTintColor.a = 170/255f;
                        m_targetCard.View.Tint(m_targetTintColor, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(0, 4);
                        break;
                    }
                    case 6:
                    {
                        m_targetTintColor.a = 160/255f;
                        m_targetCard.View.Tint(m_targetTintColor, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(0, 4);
                        break;
                    }
                    case 7:
                    {
                        m_targetTintColor.a = 120/255f;
                        m_targetCard.View.Tint(m_targetTintColor, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(0, 3);
                        break;
                    }
                    case 8:
                    {
                        m_targetTintColor.a = 80/255f;
                        m_targetCard.View.Tint(m_targetTintColor, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(0, 2);
                        break;
                    }
                    case 9:
                    {
                        m_targetTintColor.a = 40/255f;
                        m_targetCard.View.Tint(m_targetTintColor, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(0, -2);
                        break;
                    }
                    case 10:
                    {
                        m_targetCard.View.RemoveTint();
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(0, -2);
                        break;
                    }
                    case 11:
                    {
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(0, -1);
                        break;
                    }
                    case 12:
                    {
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(0, 0);
                        break;
                    }
                }
            }
        }

        public IEnumerator<float> Play( VfxSourceAndTargetData vfxData, Command command, int targetIndex )
        {
            gameObject.SetActive(true);

            m_sourceTintColor = "#701e85".ToColor();
            m_targetTintColor = "#d96cea".ToColor();

            var sourcePosition = vfxData.sourceCardPosition;
            m_sourceCard = vfxData.sourceCardObject;

            var targetPosition = vfxData.targetCardPositions[targetIndex];
            m_targetCard = vfxData.targetCardObjects[targetIndex];

            // target swirl
            swirlUnderlay.SetActive(true);
            swirlUnderlay.transform.position = targetPosition;
            swirlUnderlay.Play("spawn", OnSwirlSpawn);



            // overlay start
            m_updateMagicBallOverlayTint = true;

            magicBallOverlay.SetActive(true);
            magicBallOverlay.transform.position = sourcePosition;
            magicBallOverlay.Play("target", OnFinishedMagicBallOverlay);

            AudioController.PlaySound("vfx_purple_energy", "BARDSFX", true, gameObject);

            while (magicBallOverlay.CurrentFrame < 10) yield return Timing.WaitForOneFrame;

            // magic ball start
            magicBall.SetActive(true);
            magicBall.transform.position = sourcePosition + new Vector3(0, 1.5f, 0);
            magicBall.Play("spawn");

            while (!magicBall.IsDone) yield return Timing.WaitForOneFrame;

            magicBall.Play("idle");

            yield return Timing.WaitForSeconds(0.5f);

            AudioController.PlaySound("vfx_wind", "BARDSFX", true, gameObject).setPitch(0.75f);
            while (Vector3.Distance(magicBall.transform.position, targetPosition) > 1)
            {
                magicBall.transform.position = Vector3.MoveTowards(magicBall.transform.position, targetPosition, magicBallMoveSpeed * Time.deltaTime);
                yield return Timing.WaitForOneFrame;
            }

            m_updateAbsorbTint = true;
            magicBall.transform.position = targetPosition;

            swirlUnderlay.Play("end");

            absorbOverlay.transform.position = targetPosition;
            absorbOverlay.SetActive(true);
            absorbOverlay.Play("absorb");

            AudioController.PlaySound("vfx_dark_energy", "BARDSFX", true, gameObject);

            magicBall.SetActive(false);

            if (command is DamageCommand damageCommand)
            {
                // do damage here..
                // check if card null, if not apply damage then do generic damage animation.
                // remember to check "Apply Damage in Effect" when setting up the effect in the ability

                var targetState = vfxData.targetCards[targetIndex];

                if (targetState != null)
                {
                    var damageHistory = damageCommand.PendingDamageHistories[targetIndex];
                    targetState.ApplyHistory(damageHistory.Uid);

                    if (targetState.IsHero)
                    {
                        var heroView = GameplayScreen.Instance.heroSideBarContainer;
                        Timing.RunCoroutine(heroView.HitPortraitNonAttack(targetState.Team, damageCommand, damageHistory.Uid));
                    }
                    else if (m_targetCard != null)
                    {
                        var amount = damageHistory.DamageAmount;
                        var damageFx = m_targetCard.View.GetVfx<DamageEffectComponent, DamageEffect>();

                        if (damageFx != null)
                        {
                            Timing.RunCoroutine(damageFx.GenericDamageAnimation(amount));
                        }
                    }
                }
            }

            while (!absorbOverlay.IsDone) yield return Timing.WaitForOneFrame;

            m_updateAbsorbTint = false;
            absorbOverlay.SetActive(false);

            yield return Timing.WaitForOneFrame;

            Destroy(gameObject);
        }

        private void OnFinishedMagicBallOverlay()
        {
            magicBallOverlay.SetActive(false);
            m_updateMagicBallOverlayTint = false;
        }

        private void OnSwirlSpawn()
        {
            swirlUnderlay.Play("idle");
        }

    }
}