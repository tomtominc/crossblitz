using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class FreezeEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<FreezeEffect>("FreezeEffect", card.cardObject);
            effect.Initialize(card);
        }
    }
}