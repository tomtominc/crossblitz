using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Commands;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class EruptionEffect : GenericEffectParticle
    {
        public SpriteAnimation explodeAnim;
        public Color overlayPurple;
        public Color overlayWhite;

        public IEnumerator<float> Play(VfxSourceAndTargetData vfxData, Command command, int targetIndex)
        {
            var targetRect = vfxData.targetRects[targetIndex];
            var origAnchorPos = targetRect.anchoredPosition;

            targetRect.DOShakeAnchorPos(1f, 5, 50, 0)
                .OnComplete(() => targetRect.anchoredPosition = origAnchorPos );

            yield return Timing.WaitForSeconds(0.25f);

            // Play Explosion Animation and change color
            explodeAnim.SetActive(true);
            explodeAnim.Play("explode", () => gameObject.SetActive(false));

            AudioController.PlaySound("vfx_fiery_explosion", "BARDSFX", true, gameObject);
            GameVfxUtilities.HandleDamageCommand( command, vfxData, targetIndex, vfxData.targetCardObjects[targetIndex] );

            yield return Timing.WaitForSeconds(0.4f);

            Destroy(gameObject);
        }
    }
}