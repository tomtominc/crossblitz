using System;
using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using DG.Tweening;
using MEC;
using TakoBoyStudios;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class DualstrikeEffect : MonoBehaviour, IGameVfx
    {
        public SpriteAnimation attackTypeIcon;
        public SpriteAnimation x2Icon;
        public RectTransform x2Rect;

        private const float t = 3.3333f;
        private const float start = 35f;
        private const float end = 40f;

        private CardView _card;
        private GameCardView _gameCard;
        private bool m_updateFloat;
        private float m_timer;

        private RectTransform _rect;
        public void Initialize(Transform rect)
        {
            _rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
            m_timer = 0;
            _gameCard = _card.GetCardComponent<GameCardView>();
            _gameCard.Events.OnSilenced += OnSilenced;
            _gameCard.Events.OnTransformed += OnTransformed;
        }

        public void Update()
        {
            if (m_updateFloat)
            {
                if (m_timer <= 0.12f) x2Rect.anchoredPosition = new Vector2(x2Rect.anchoredPosition.x, 35);
                else if (m_timer <= 0.92f) x2Rect.anchoredPosition = new Vector2(x2Rect.anchoredPosition.x, 34);
                else if (m_timer <= 1.06f) x2Rect.anchoredPosition = new Vector2(x2Rect.anchoredPosition.x, 35);
                else if (m_timer <= 1.14f) x2Rect.anchoredPosition = new Vector2(x2Rect.anchoredPosition.x, 36);
                else if (m_timer <= 1.26f) x2Rect.anchoredPosition = new Vector2(x2Rect.anchoredPosition.x, 37);
                else if (m_timer <= 1.44f) x2Rect.anchoredPosition = new Vector2(x2Rect.anchoredPosition.x, 38);
                else if (m_timer <= 1.88f) x2Rect.anchoredPosition = new Vector2(x2Rect.anchoredPosition.x, 39);
                else if (m_timer <= 2.06f) x2Rect.anchoredPosition = new Vector2(x2Rect.anchoredPosition.x, 38);
                else if (m_timer <= 2.18f) x2Rect.anchoredPosition = new Vector2(x2Rect.anchoredPosition.x, 37);
                else if (m_timer <= 2.26f) x2Rect.anchoredPosition = new Vector2(x2Rect.anchoredPosition.x, 36);
                else m_timer = 0;

                m_timer += Time.deltaTime;
            }
        }

        public IEnumerator<float> Spawn()
        {
            m_updateFloat = false;
            m_timer = 0;

            yield return Timing.WaitForSeconds(0.24f);

            if (gameObject == null) yield break;

            gameObject.SetActive(true);

            var attackTypeLower = _card.data.attackType.ToString().ToLower();

            x2Icon.SetActive(true);
            x2Icon.Play($"spawn-{attackTypeLower}");
            AudioController.PlaySound("vfx_dual_strike", "BARDSFX", true, gameObject);
            x2Rect.anchoredPosition = new Vector2(x2Rect.anchoredPosition.x, start);

            attackTypeIcon.transform.SetParent( _card.attackTypeIcon.transform.parent, true );
            attackTypeIcon.transform.SetSiblingIndex(_card.attackTypeIcon.transform.GetSiblingIndex());
            attackTypeIcon.SetActive(true);
            attackTypeIcon.Play($"{attackTypeLower}-change");

            _card.attackTypeIcon.SetActive(false);

            while (!x2Icon.IsDone) yield return Timing.WaitForOneFrame;

            if (gameObject == null) yield break;

            x2Icon.Play($"idle-{attackTypeLower}");
            attackTypeIcon.Play(attackTypeLower);
            m_updateFloat = true;
        }

        private void OnDualstrikeSpawnUpdate(int frame)
        {
            var cardRect = _card.cardObject;

            cardRect.anchoredPosition = frame switch
            {
                0 => new Vector2(0, 4),
                1 => new Vector2(-1, 7),
                2 => new Vector2(0, 10),
                3 => new Vector2(0, 11),
                4 => new Vector2(0, 10),
                5 => new Vector2(0, 8),
                6 => new Vector2(0, 5),
                7 => new Vector2(0, 1),
                8 => new Vector2(0, -1),
                9 => new Vector2(0, 0),
                _ => cardRect.anchoredPosition
            };
        }

        private void OnSilenced(GameCardView card)
        {
            RemoveDualstrike(card);
        }

        private void OnTransformed(GameCardView card)
        {
            if (!card.State.HasAbility(AbilityKeyword.DUAL_STRIKE))
            {
                RemoveDualstrike(card);
            }
        }

        private void RemoveDualstrike(GameCardView card)
        {
            if (gameObject)
            {
                _card.attackTypeIcon.SetActive(true);
                Destroy(attackTypeIcon.gameObject);
                Destroy(gameObject);
            }
        }

        private void OnDestroy()
        {
            _gameCard.Events.OnSilenced -= OnSilenced;
            _gameCard.Events.OnTransformed -= RemoveDualstrike;
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            throw new System.NotImplementedException();
        }
    }
}