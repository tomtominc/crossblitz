using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class SignHereEffect : MonoBehaviour, IGameVfx
    {
        private const int FadeFrames = 3;
        private const float FrameTime = 0.08f;
        public SpriteAnimation faceAnim;
        public SpriteAnimation AuraAnim;
        public Color overlayColor;

        private CardView _card;
        private GameCardView _gameCard;

        private RectTransform m_rect;

        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }
        public void Initialize(CardView card)
        {
            _card = card;
            _gameCard = _card.GetCardComponent<GameCardView>();
        }

        /// <summary>
        /// Play is used when spawning the effect on top of the card.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            gameObject.SetActive(true);

            // Play Face
            faceAnim.Play("spawn");
            while (faceAnim.CurrentFrame < 8) yield return Timing.WaitForOneFrame;

            // Play Aura Effect and Dark Overlay
            AuraAnim.SetActive(true);
            AuraAnim.Play("glare");
            AudioController.PlaySound("vfx_purple_energy", "BARDSFX", true, gameObject);
            _card.TintOverTime(overlayColor, BlendMode.DarkerColor, 0.25f);
            while (AuraAnim.CurrentFrame < 16) yield return Timing.WaitForOneFrame;

            // Despawn Face and remove dark overlay
            faceAnim.Play("despawn", () => faceAnim.SetActive(false));
            yield return Timing.WaitUntilDone(_card.RemoveTintOverTime(0.25f).WaitForCompletion(true));

            Destroy(gameObject);

        }
    }
}