using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Pools;
using DG.Tweening;
using KennethDevelops.ProLibrary.Extensions;
using KennethDevelops.ProLibrary.Managers;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class DeathExplodeEffect : MonoBehaviour, IGameVfx
    {
        public PoolManager sweatPool;
        public SpriteAnimation explosionEffect;
        public SpriteAnimation woodDebris;

        private CardView _card;
        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            gameObject.SetActive(true);

            var cardRect = _card.cardObject;

            var red = "#E22712".ToColor();
            var orange = "#F47133".ToColor();
            var yellow = "#FFC500".ToColor();
            var sweatSide = 1;

            GameplayScreen.Instance.ShakeScreen(2, 4);

            for (var i = 0; i < 10; i++)
            {
                cardRect.anchoredPosition = new Vector2(i % 2 == 0 ? -2 : 2, 0);
                switch (i)
                {
                    case 0: _card.Tint(orange.Alpha(0.4f),BlendMode.SoftLight, true);
                        break;
                    case 1: _card.Tint(yellow.Alpha(0.4f), BlendMode.SoftLight, true);
                        break;
                    case 2: _card.Tint(red.Alpha(0.6f), BlendMode.SoftLight, true);
                        break;
                    case 3: _card.Tint(red.Alpha(0.6f), BlendMode.SoftLight, true);
                        break;
                    case 4: _card.Tint(yellow.Alpha(0.4f), BlendMode.SoftLight, true);
                        break;
                    case 5: _card.Tint(orange.Alpha(0.4f), BlendMode.SoftLight, true);
                        break;
                    case 6: _card.Tint(orange.Alpha(0.4f), BlendMode.SoftLight, true);
                        break;
                    case 7: _card.Tint(yellow.Alpha(0.6f), BlendMode.SoftLight, true);
                        break;
                    case 8: _card.Tint(red.Alpha(0.6f), BlendMode.SoftLight, true);;
                        break;
                    case 9: _card.Tint(red.Alpha(0.6f), BlendMode.SoftLight, true);
                        break;
                }

                if (i % 2 == 0)
                {
                    var sweat = sweatPool.AcquireObject<AnimatorPoolObject>(Vector3.zero, Quaternion.identity);
                    sweat.transform.localScale=Vector3.zero;
                    sweat.RectTransform().anchoredPosition = new Vector2(27 * sweatSide, 19f);
                    sweatSide *= -1;
                }
                yield return Timing.WaitForSeconds(0.04f);
            }

            _card.Tint(orange.Alpha(0.8f), BlendMode.SoftLight);
            cardRect.anchoredPosition = new Vector2(0, 0);

            yield return Timing.WaitForSeconds(0.04f);

            _card.RemoveTint();
            _card.cardObjectCanvas.alpha = 0;

            explosionEffect.SetActive(true);
            explosionEffect.Play("explosion");

            while (explosionEffect.CurrentFrame < 2) yield return Timing.WaitForOneFrame;

            GameplayScreen.Instance.ShakeScreen(18);

            woodDebris.SetActive(true);
            woodDebris.Play("wood-debris");

            while (woodDebris.CurrentFrame < 4) yield return Timing.WaitForOneFrame;

            woodDebris.image.DOFade(0, 0.12f);

            yield return Timing.WaitForSeconds(0.12f);

            var state = _card.GetCardComponent<GameCardView>().State;

            if (state.HasTrigger(TriggerType.DEATHRATTLE))
            {
                yield return Timing.WaitUntilDone(DeathEffect.EvaluateDeath(state,_card,true));
            }
            else
            {
                Timing.RunCoroutine(DeathEffect.EvaluateDeath(state,_card,true));
            }

            Destroy(gameObject);
        }
    }
}