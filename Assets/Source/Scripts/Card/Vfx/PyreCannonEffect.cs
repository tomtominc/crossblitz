using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using MEC;
using UnityEngine;
using Asyncoroutine;
using CrossBlitz.Audio;

namespace CrossBlitz.GameVfx.Minion
{
    public class PyreCannonEffect : MonoBehaviour, IGameVfx
    {
        public float multiShotDelay = 0.12f;
        public PyreCannonEffectProjectile projectilePrefab;

        public void Initialize(Transform rect) { }
        public void Initialize(CardView card) { }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            // by default the effects are always inactive, we'll need to make them active when we play them.
            gameObject.SetActive(true);

            var getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(command);
            yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
            var vfxData = getSourceAndTargetData.Result;
            var handles = new List<CoroutineHandle>();
            var positionInfo = new List<PositionInfo>();

            for (var i = 0; i < vfxData.targetEmptyTilePositions.Count; i++)
            {
                positionInfo.Add(new PositionInfo { index = i, pos = vfxData.targetEmptyTilePositions[i], spawnOnEmpty = true});
            }

            for (var i = 0; i < vfxData.targetCardPositions.Count; i++)
            {
                positionInfo.Add(new PositionInfo { index = i, pos = vfxData.targetCardPositions[i], spawnOnEmpty = false});
            }

            positionInfo = positionInfo.OrderBy(p => Vector3.Distance(p.pos, vfxData.sourceCardPosition)).ToList();

            AudioController.PlaySound("manameld_fire", "BARDSFX", false);

            for (var i = 0; i < positionInfo.Count; i++)
            {
                var fireballEffect = Instantiate(projectilePrefab, transform, false);
                handles.Add(Timing.RunCoroutine(fireballEffect.Play(vfxData, command, positionInfo[i].index, positionInfo[i].spawnOnEmpty)));
                yield return Timing.WaitForSeconds(multiShotDelay);
            }

            while (handles.Exists(handle => handle.IsRunning)) yield return Timing.WaitForOneFrame;

            AudioController.StopSound("manameld_fire");

            yield return Timing.WaitForOneFrame;

            Destroy(gameObject);
        }

        public struct PositionInfo
        {
            public int index;
            public Vector3 pos;
            public bool spawnOnEmpty;
        }
    }
}