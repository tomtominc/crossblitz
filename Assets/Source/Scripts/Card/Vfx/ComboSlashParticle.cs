using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI.GameLogic;
using DG.Tweening;
using MEC;
using UnityEngine;
using Asyncoroutine;
using CrossBlitz.Card.Vfx;
using CrossBlitz.Audio;

namespace CrossBlitz.GameVfx.Minion
{
    public class ComboSlashParticle : MonoBehaviour
    {
        public SpriteAnimation slashAnim;

        private GameCardView m_targetCard;
        private Color m_targetColorTint;
        private bool m_updateTargetSlash;

        private void Update()
        {
            if (m_updateTargetSlash && m_targetCard != null && slashAnim.CurrentAnimationName.Equals("slash"))
            {
                switch (slashAnim.CurrentFrame)
                {
                    case 4:
                        m_targetColorTint.a = 100/255f;
                        m_targetCard.View.Tint(m_targetColorTint, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(2, 0);
                        AudioController.PlaySound("vfx_sword_slash", "BARDSFX", true, gameObject);
                        break;
                    case 5:
                        m_targetColorTint.a = 200/255f;
                        m_targetCard.View.Tint(m_targetColorTint, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(-4, 0);
                        break;
                    case 6:
                        m_targetColorTint.a = 200/255f;
                        m_targetCard.View.Tint(m_targetColorTint, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(-6, 0);
                        break;
                    case 7:
                        m_targetColorTint.a = 100/255f;
                        m_targetCard.View.Tint(m_targetColorTint, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(1, 0);
                        break;
                    case 8:
                        m_targetColorTint.a = 50/255f;
                        m_targetCard.View.Tint(m_targetColorTint, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(2, 0);
                        break;
                    case 9:
                        m_targetColorTint.a = 100/255f;
                        m_targetCard.View.Tint(m_targetColorTint, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(-4, 0);
                        AudioController.PlaySound("vfx_sword_slash", "BARDSFX", true, gameObject);
                        break;
                    case 10:
                        m_targetColorTint.a = 200/255f;
                        m_targetCard.View.Tint(m_targetColorTint, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(-6, 0);
                        break;
                    case 11:
                        m_targetColorTint.a = 200/255f;
                        m_targetCard.View.Tint(m_targetColorTint, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(1, 0);
                        break;
                    case 12:
                        m_targetColorTint.a = 100/255f;
                        m_targetCard.View.Tint(m_targetColorTint, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(2, 0);
                        break;
                    case 13:
                        m_targetColorTint.a = 50/255f;
                        m_targetCard.View.Tint(m_targetColorTint, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(-4, 0);
                        break;
                    case 14:
                        m_targetColorTint.a = 100/255f;
                        m_targetCard.View.Tint(m_targetColorTint, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(-6, 0);
                        AudioController.PlaySound("vfx_sword_slash", "BARDSFX", true, gameObject);
                        break;
                    case 15:
                        m_targetColorTint.a = 200/255f;
                        m_targetCard.View.Tint(m_targetColorTint, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(1, 0);
                        break;
                    case 16:
                        m_targetColorTint.a = 200/255f;
                        m_targetCard.View.Tint(m_targetColorTint, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(2, 0);
                        break;
                    case 17:
                        m_targetColorTint.a = 100/255f;
                        m_targetCard.View.Tint(m_targetColorTint, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(-4, 0);
                        break;
                    case 18:
                        m_targetColorTint.a = 50/255f;
                        m_targetCard.View.Tint(m_targetColorTint, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(-6, 0);
                        break;
                    case 19:
                        m_targetColorTint.a = 0;
                        m_targetCard.View.Tint(m_targetColorTint, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(1, 0);
                        break;
                    case 20:
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(2, 0);
                        break;
                    case 21:
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(0, 0);
                        break;
                    case 22:
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(-1, 0);
                        break;
                    default:
                        m_targetColorTint.a = 0;
                        m_targetCard.View.Tint(m_targetColorTint, BlendMode.Color, true);
                        m_targetCard.View.cardObject.anchoredPosition = new Vector2(0, 0);
                        break;

                }
            }
        }

        public IEnumerator<float> Play( VfxSourceAndTargetData vfxData, Command command, int targetIndex )
        {
            // by default the effects are always inactive, we'll need to make them active when we play them.
            gameObject.SetActive(true);

            var targetPosition = vfxData.targetCardPositions[targetIndex];
            m_targetCard = vfxData.targetCardObjects[targetIndex];
            slashAnim.transform.position = targetPosition;
            slashAnim.Play("slash");

            if (m_targetCard != null)
            {
                m_targetColorTint = "#FC5122".ToColor();
                m_updateTargetSlash = true;
            }

            while (slashAnim.CurrentFrame < 16) yield return Timing.WaitForOneFrame;

            if (command is DamageCommand damageCommand)
            {
                var targetState = vfxData.targetCards[targetIndex];

                if (targetState != null)
                {
                    var damageHistory = damageCommand.PendingDamageHistories[targetIndex];
                    targetState.ApplyHistory(damageHistory.Uid);

                    if (targetState.IsHero)
                    {
                        var heroView = GameplayScreen.Instance.heroSideBarContainer;
                        Timing.RunCoroutine(heroView.HitPortraitNonAttack(targetState.Team, damageCommand, damageHistory.Uid));
                    }
                    else if (m_targetCard != null)
                    {
                        var amount = damageHistory.DamageAmount;
                        var damageFx = m_targetCard.View.GetVfx<DamageEffectComponent, DamageEffect>();

                        if (damageFx != null)
                        {
                            Timing.RunCoroutine(damageFx.GenericDamageAnimation(amount));
                        }
                    }
                }
            }

            while (!slashAnim.IsDone) yield return Timing.WaitForOneFrame;

            m_updateTargetSlash = false;

            if (m_targetCard != null)
            {
                m_targetCard.View.RemoveTint();
                m_targetCard.View.cardObject.anchoredPosition = new Vector2(0, 0);
            }

            yield return Timing.WaitForOneFrame;

            Destroy(gameObject);
        }
    }
}