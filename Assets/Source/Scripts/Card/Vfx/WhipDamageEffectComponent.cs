using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class WhipDamageEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<WhipDamageEffect>("WhipDamageEffect", card.transform);
            effect.Initialize(card);
        }
    }
}