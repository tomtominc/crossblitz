using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.GameVfx;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class BombEffect : MonoBehaviour, IGameVfx
    {
        public Bomb bombPrefab;
        public BombProjectile bombProjectilePrefab;

        private const float MinDropDelay = 0.2f;
        private const float MaxDropDelay = 0.5f;

        public void Initialize(CardView card)
        {

        }

        public void Initialize(Transform rect)
        {

        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            gameObject.SetActive(true);

            var getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(command);
            yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
            var vfxData = getSourceAndTargetData.Result;
            var handles = new List<CoroutineHandle>();

            for (var i = 0; i < vfxData.targetCards.Count; i++)
            {
                var bombProjectile = Instantiate(bombProjectilePrefab, transform, false);
                handles.Add(Timing.RunCoroutine(bombProjectile.Play(vfxData, command, i, -1, new Vector3(0, 14f))));
                yield return Timing.WaitForSeconds(Random.Range(MinDropDelay, MaxDropDelay));
            }

            while (handles.Exists(handle => handle.IsRunning)) yield return Timing.WaitForOneFrame;

            if (command is DestroyCommand destroyCommand)
            {
                yield return Timing.WaitUntilDone(GameVfxUtilities.EvaluateDeath(vfxData.targetCardObjects));
            }

            Destroy(gameObject, 2f);
        }
    }
}