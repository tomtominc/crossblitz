﻿using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class BattlecryEffect : MonoBehaviour, IGameVfx
    {
        public SpriteAnimation battlecryAnimator;
        public CanvasGroup frontOverlay;

        private CardView _card;
        private RectTransform _rect;
        public void Initialize(Transform rect)
        {
            _rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
            _rect = _card.Rect;
        }



        public void OnAfterCreation()
        {

        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            gameObject.SetActive(true);
            frontOverlay.alpha = 0;
            battlecryAnimator.SetActive(false);

            yield return Timing.WaitUntilDone(_rect.DOAnchorPosY(-5, 0.24f)
                .SetEase(Ease.InOutBack).WaitForCompletion(true));

            battlecryAnimator.SetActive(true);
            battlecryAnimator.Play("battlecry-activate");
            frontOverlay.alpha = 1;
            frontOverlay.DOFade(0, 0.16f);

            AudioController.PlaySound("battle_trigger_battlecry", "BARDSFX", true, gameObject);

            for (var i = 0; i < 12; i++)
            {
                _rect.anchoredPosition = new Vector2(0, i%2==0? 7 : 6);
                yield return Timing.WaitForSeconds(0.04f);
            }

            yield return Timing.WaitUntilDone(_rect.DOAnchorPosY(0, 0.24f)
                .SetEase(Ease.InBack).WaitForCompletion(true));

            if (vfxParams != null && vfxParams.destroyAfterCompletion)
            {
                Destroy(gameObject);
            }
            else
            {
                gameObject.SetActive(false);
            }
        }

        public void Unload()
        {

        }
    }
}
