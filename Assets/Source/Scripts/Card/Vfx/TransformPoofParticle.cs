using System.Collections.Generic;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.GameVfx;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class TransformPoofParticle : GenericEffectParticle
    {
        public override IEnumerator<float> Play(VfxSourceAndTargetData vfxData, Command command, int targetIndex)
        {
            var targetCard = vfxData.targetCards[targetIndex];
            var targetCardObject = vfxData.targetCardObjects[targetIndex];
            var targetPosition = vfxData.targetCardPositions[targetIndex];

            transform.position = targetPosition;

            if (targetCard != null && command is TransformCardsCommand transformCardsCommand)
            {
                targetCard.ExecutePendingTransformation( transformCardsCommand.TransformPendingHistories[targetIndex], transformCardsCommand.TransformCardData.keepStatsAndUpgradesOfPreviousMinion );
            }

            animator.Play("poof");

            while (animator.CurrentFrame < 5) yield return Timing.WaitForOneFrame;

            GameplayScreen.Instance.ShakeScreen(4, 1);

            if (targetCardObject != null)
            {
                targetCardObject.View.SetCardData(targetCardObject.State.data);
                targetCardObject.UpdateViewFromState();

                yield return Timing.WaitForOneFrame;
                yield return Timing.WaitUntilDone(targetCardObject.TransformBoardCard());
            }

            while (!animator.IsDone) yield return Timing.WaitForOneFrame;
        }
    }
}