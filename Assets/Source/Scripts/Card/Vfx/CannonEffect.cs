using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.GameVfx;
using CrossBlitz.ServerAPI;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class CannonEffect : MonoBehaviour, IGameVfx
    {
        public FuseEffectParticle fuseEffectPrefab;
        public CannonBallEffectParticle cannonBallPrefab;
        public PuffProjectile puffProjectilePrefab;

        public void Initialize(CardView card) { }

        public void Initialize(Transform rect) { }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            var commandIndexCurr = GameLogic.IndexOfCommand(command);
            var commandsList = GameLogic.GetCommandsAtIndexAndBeyond(commandIndexCurr);
            var cannonCommands = new List<Command>();
            var originAbility = command.GetAbility();

            for (var i = 0; i < commandsList.Count; i++)
            {
                if (commandsList[i] is DamageCommand damageCommand)
                {
                    var ability = damageCommand.GetAbility();
                    if (ability!=null && ability.usesCustomEffect && ability.usesBoardEffect && ability.boardEffectPrefab == "CannonEffect")
                    {
                        cannonCommands.Add(damageCommand);
                        Server.CommandResolver.RemoveCommandFromResolve(damageCommand);
                    }
                }
                else if (commandsList[i] is DestroyCommand destroyCommand)
                {
                    if (destroyCommand.SourceUid == command.SourceUid && originAbility != null &&
                        originAbility.effectSubType == 1)
                    {
                        Server.CommandResolver.RemoveCommandFromResolve(destroyCommand);
                    }
                }
            }

            var fuses = new List<FuseEffectParticle>();
            var vfxDataSet = new List<VfxSourceAndTargetData>();
            var handles = new List<CoroutineHandle>();

            for (var i = 0; i < cannonCommands.Count; i++)
            {
                var getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(cannonCommands[i]);
                yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
                var vfxData = getSourceAndTargetData.Result;
                vfxDataSet.Add( vfxData );

                if (vfxData.sourceCardObject)
                {
                    vfxData.sourceCardObject.View.StartAnimating();
                }

                // apply the fuse
                var fuse = Instantiate(fuseEffectPrefab, transform, false);
                fuse.transform.position = vfxData.sourceCardPosition;
                fuses.Add(fuse);
                handles.Add(Timing.RunCoroutine(fuse.Spawn()));
            }

            while (handles.Exists(handle => handle.IsRunning)) yield return Timing.WaitForOneFrame;

            handles = new List<CoroutineHandle>();

            for (var i = 0; i < cannonCommands.Count; i++)
            {
                var vfxData = vfxDataSet[i];
                var fuse = fuses[i];
                var ability = cannonCommands[i].GetAbility();

                yield return Timing.WaitUntilDone(fuse.Burn());

                for (var j = 0; j < vfxData.targetCards.Count; j++)
                {
                    if (ability.effectSubType == 1)
                    {
                        var puffProjectile = Instantiate(puffProjectilePrefab, transform, false);
                        handles.Add(Timing.RunCoroutine(puffProjectile.Play(vfxData, cannonCommands[i], j, -1, string.Empty, .5f, true)));
                        yield return Timing.WaitForSeconds(Random.Range(0.2f, 0.4f));
                    }
                    else
                    {
                        var cannonball = Instantiate(cannonBallPrefab, transform, false);
                        yield return Timing.WaitUntilDone(cannonball.Play(vfxData, cannonCommands[i], j));
                    }
                }
            }

            while (handles.Exists(handle => handle.IsRunning)) yield return Timing.WaitForOneFrame;

            for (var i = 0; i < vfxDataSet.Count; i++)
            {
                var vfxData = vfxDataSet[i];

                if (vfxData.sourceCardObject != null)
                {
                    vfxData.sourceCardObject.View.EndAnimating();
                }
            }

            Destroy(gameObject,2);
        }
    }
}