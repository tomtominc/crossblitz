using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using DG.Tweening;
using MEC;
using Source.Scripts.Card;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.GameVfx
{
    public class DeathEffect : MonoBehaviour, IGameVfx
    {
        public SpriteAnimation deathEffect;
        private CardView m_view;
        private RectTransform _rectTransform;
        private GameCardView m_gameCard;
        private bool m_isPlaying;
        public bool IsPlaying => m_isPlaying;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            m_view = card;
            _rectTransform = m_view.RectTransform();
            m_gameCard = m_view.GetCardComponent<GameCardView>();
        }

        public static IEnumerator<float> EvaluateDeath(GameCardState state, CardView card, bool skipDeathAnimation = false)
        {
            if (state.IsDead())
            {
                var death = card.GetVfx<DeathEffectComponent, DeathEffect>();
                yield return Timing.WaitUntilDone(death.Play(null,  new VfxParameters { skipDeathAnimation = skipDeathAnimation }));
            }
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            m_isPlaying = true;

            var cardState = m_view.GetCardComponent<GameCardView>().State;

            // //@Rob WARNING: This can return null sometimes!
            // var cardThatDestroyedThisCard = GameServer.State.GetCard(cardState.destroyedByCardUid);
            //
            // //@Rob check for Plunder (check to make sure that the card isn't null, this is just example code)
            // var isPlunder = cardThatDestroyedThisCard.HasTrigger(TriggerType.PLUNDER);
            //
            // //@Rob get the view (check to make sure that the card isn't null, this is just example code)
            // var cardThatDestroyedThisCardView = GameplayScreen.Instance.GetBoardCard(cardThatDestroyedThisCard.uid, out _, out _);

            GameplayScreen.Instance.RemoveMinionFromBoard(cardState.uid, CardLocation.Graveyard);

            if (m_gameCard) m_gameCard.Events.Die();

            Events.Publish(this, EventType.OnMinionDestroyed, new OnMinionDestroyedEventArgs{ cardUid = cardState.uid, isClientCard = cardState.Team == Team.Client});

            // Get KillCard
            var killCardState = GameServer.State.GetCard(cardState.destroyedByCardUid);
            var isPlunder = false;

            if (killCardState != null)
            {
                isPlunder = killCardState.HasTrigger(TriggerType.PLUNDER);
            }

            if (vfxParams == null || !vfxParams.skipDeathAnimation)
            {
                AudioController.PlaySound("battle_minion_death", "BARDSFX", true, gameObject);

                deathEffect.SetActive(true);
                deathEffect.image.color = new Color(1, 1, 1, 0);
                deathEffect.image.DOFade(1, 0.16f);

                m_view.attackTypeIcon.SetActive(false);

                yield return Timing.WaitUntilDone(_rectTransform.DOShakePosition(0.16f)
                    .WaitForCompletion(true));

                if(isPlunder) deathEffect.Play("destroyed-plunder");
                else deathEffect.Play("destroyed");

                m_view.GetCardComponent<GameCardView>().ObjectCanvasGroup.alpha = 0;

                while (deathEffect.CurrentFrame < 19) yield return Timing.WaitForOneFrame;

                if (isPlunder)
                {
                    AudioController.PlaySound("vfx_plunder", "BARDSFX", true, gameObject);
                }

                GameplayScreen.Instance.ShakeScreen(4, 1);
            }

            // Call the Plunder effect if applicable
            if (killCardState != null && killCardState.HasTrigger(TriggerType.PLUNDER))
            {
                while (!deathEffect.IsDone) yield return Timing.WaitForOneFrame;
                var boardKillCard = GameplayScreen.Instance.GetBoardCard(killCardState.uid, out var errorCode, out var errorLocation);

                if (boardKillCard)
                {
                    yield return Timing.WaitUntilDone(boardKillCard.AnimateEffect<PlunderEffectComponent>(command));
                }
            }

            // Call the Death Rattle effect if applicable
            if (m_gameCard && m_gameCard.State.HasTrigger(TriggerType.DEATHRATTLE))
            {
                yield return Timing.WaitUntilDone(m_gameCard.AnimateEffect<DeathrattleEffectComponent>(command));
            }

            if (vfxParams == null || !vfxParams.skipDeathAnimation)
            {
                while (!deathEffect.IsDone) yield return Timing.WaitForOneFrame;
            }

            Destroy(m_view.gameObject);
        }

        public IEnumerator<float> PlayTransform()
        {
            deathEffect.SetActive(true);
            deathEffect.image.color = new Color(1, 1, 1, 0);
            deathEffect.image.DOFade(1, 0.16f);

            m_view.attackTypeIcon.SetActive(false);

            yield return Timing.WaitUntilDone(_rectTransform.DOShakePosition(0.16f)
                .WaitForCompletion(true));

            deathEffect.Play("transform");

            m_view.SetCardData(m_gameCard.State.data);
            m_gameCard.UpdateViewFromState();

            yield return Timing.WaitForOneFrame;

            m_view.Tint("e698a4".ToColor(), BlendMode.Color, true);

            while (deathEffect.CurrentFrame < 5) yield return Timing.WaitForOneFrame;

            GameplayScreen.Instance.ShakeScreen(4, 1);

            deathEffect.image.DOFade(0, 0.16f);

            yield return Timing.WaitForSeconds(0.16f);

            m_gameCard.OnSummonedOnBoard(SummonCommand.Method.PlayedWithAbility);

            var requiredComponents = GameVfxComponent.GetRequiredVfxComponents(m_gameCard.State);
            var createdComponents = new List<ICardComponent>();

            for (var i = 0; i < requiredComponents.Count; i++)
            {
                var requiredComponent = m_gameCard.GetComponent(requiredComponents[i]);

                if (requiredComponent == null)
                {
                    var component = (ICardComponent) m_gameCard.gameObject.AddComponent(requiredComponents[i]);
                    component.Initialize(m_view);
                    component.OnAfterCreation();

                    createdComponents.Add(component);
                }
            }

            // if (m_gameCard != null && m_gameCard.State.IsSlow())
            // {
            //     m_view.TintOverTime("#2E1D26".ToColor(0.4f), BlendMode.Darken, 0.4f);
            // }
            // else
            // {
            //     m_view.RemoveTintOverTime(0.4f);
            // }

            m_view.RemoveTintOverTime(0.4f);

            while (createdComponents.Exists(c =>c is GameVfxComponent vfx && vfx.Effect == null))
            {
                Debug.Log("Waiting for vfx component to load!");
                yield return Timing.WaitForOneFrame;
            }
        }
    }
}