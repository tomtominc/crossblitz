using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class DiscardEffect : MonoBehaviour, IGameVfx
    {
        private CardView _card;
        private GameCardView _gameCard;

        public SpriteAnimation discard;

        public void Initialize(CardView card)
        {
            _card = card;
            _gameCard = _card.GetCardComponent<GameCardView>();
        }

        public void Initialize(Transform rect)
        {

        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            gameObject.SetActive(true);
            discard.SetActive(false);

            const float fade = 0.24f;

            var localPos = _gameCard.Rect.localPosition;

            if (_gameCard.State.Team == Team.Client)
            {
                _gameCard.Rect.DOLocalMoveY(localPos.y + 32f, fade);
            }

            _card.TintOverTime(Color.white, BlendMode.Color, fade, true);
            yield return Timing.WaitForSeconds(fade);
            _card.RemoveTint();
            _card.cardObject.SetActive(false);

            discard.SetActive(true);
            discard.Play("discard");
            AudioController.PlaySound("vfx_teleport", "BARDSFX", true, gameObject);

            while (!discard.IsDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            // still in hand!
            if (_gameCard.State.location == CardLocation.Hand)
            {
                yield return Timing.WaitForSeconds(0.12f);

                if (_gameCard.State.Team == Team.Client)
                {
                    _gameCard.Rect.localPosition = localPos;
                }

                discard.Play("discard-reverse");
                AudioController.PlaySound("vfx_teleport", "BARDSFX", true, gameObject).setPitch(0.8f);

                while (!discard.IsDone)
                {
                    yield return Timing.WaitForOneFrame;
                }

                _card.cardObject.SetActive(true);
                _card.Tint("#df634e".ToColor(), BlendMode.Color, true);
                _card.RemoveTintOverTime(0.24f);
                discard.SetActive(false);

                var historyList = GameServer.State.GetCardHistories(_gameCard.State.uid);
                var commandIndex = historyList.FindLastIndex(c => c.Type == CommandType.DISCARD_CARD);

                for (var i = commandIndex; i < historyList.Count; i++)
                {
                    var history = historyList[i];

                    if (history.Type == CommandType.MODIFY_POWER || history.Type == CommandType.MODIFY_HEALTH)
                    {
                        Server.CommandResolver.RemoveCommandFromResolve(history);
                        // resolves this command here, so the visual looks better
                        yield return Timing.WaitUntilDone(history.Resolve());
                    }
                }

                yield return Timing.WaitForSeconds(0.24f);
                gameObject.SetActive(false);
            }
            else if (_gameCard.State.Team == Team.Client)
            {
                GameplayScreen.Instance.handView.RemoveCard(_gameCard, null);
                Destroy(_gameCard.gameObject);
            }
        }
    }
}