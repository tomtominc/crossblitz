using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class DeathrattleEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<DeathrattleEffect>("DeathrattleEffect", card.transform);
            effect.Initialize(card);
        }
    }
}