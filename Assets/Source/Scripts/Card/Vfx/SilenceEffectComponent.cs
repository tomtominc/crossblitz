using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class SilenceEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<SilenceEffect>("SilenceEffect", card.transform);
            effect.Initialize(card);
        }
    }
}