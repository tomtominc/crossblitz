using System.Collections;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI.GameLogic.Data;
using DG.Tweening;
using GameDataEditor;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.GameVfx.Minion
{
    public class DamageEffect : MonoBehaviour, IGameVfx
    {
        public CanvasGroup damageEffectOverlay;
        public SpriteAnimation damageEffectCross;
        public SpriteAnimation damageBurstEffect;
        public RectTransform overlayParent;
        public ValueEffectDisplay damageValueBubblePrefab;

        private CardView _view;
        private RectTransform _rectTransform;
        public Vector2 AnchoredPosition
        {
            get => _rectTransform.anchoredPosition;
            set => _rectTransform.anchoredPosition = value;
        }

        public Vector2 WorldPosition
        {
            get => _rectTransform.GetPosition(CoordinateSystem.AsChildOfCanvas);
            set => _rectTransform.SetPosition(value, CoordinateSystem.AsChildOfCanvas);
        }

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _view = card;
            _rectTransform = _view.RectTransform();

        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerator<float> GenericDamageAnimation(int damageAmount, bool skipDeathEvaluation=false, bool onlyDisplayDamage = false)
        {
            gameObject.SetActive(true);

            var gameCard = _view.GetCardComponent<GameCardView>();
            gameCard.UpdateViewFromState();
            gameCard.Events.Damaged();

            damageEffectOverlay.transform.SetParent(_view.cardObject, false);
            damageEffectOverlay.RectTransform().anchoredPosition = Vector2.zero;
            damageEffectOverlay.SetActive(true);
            damageEffectOverlay.alpha = 1;

            damageEffectCross.SetActive(true);
            damageEffectCross.Play("idle");

            damageBurstEffect.SetActive(true);
            damageBurstEffect.Play("burst");

            // Check for Ability Specific Bubbles
            var state = gameCard.State;
            var damageBubbleAnim = "spawn";
            if (state.HasAbility(AbilityKeyword.TOUGH)) damageBubbleAnim = "spawn-tough";

            var damageValueBubble = Instantiate(damageValueBubblePrefab, overlayParent, false);
            damageValueBubble.SetActive(true);
            damageValueBubble.rectTransform.anchoredPosition = Vector2.zero;

            if (damageAmount == 0)
            {
                Timing.RunCoroutine(damageValueBubble.DisplayValue($"{damageAmount}", damageBubbleAnim, gameCard.View.Rect));
            }
            else
            {
                Timing.RunCoroutine(damageValueBubble.DisplayValue($"-{damageAmount}", damageBubbleAnim, gameCard.View.Rect));
            }

            if (!onlyDisplayDamage)
            {
                AnchoredPosition = new Vector2(4,-4);

                yield return Timing.WaitForSeconds(0.04f); // 19

                AnchoredPosition = new Vector2(-5, 2);

                yield return Timing.WaitForSeconds(0.04f); // 20

                AnchoredPosition = new Vector2(-3, -5);

                yield return Timing.WaitForSeconds(0.04f); // 21

                AnchoredPosition = new Vector2(3, 5);

                yield return Timing.WaitForSeconds(0.04f); // 22

                AnchoredPosition = new Vector2(-1, -3);

                yield return Timing.WaitForSeconds(0.04f); // 23

                AnchoredPosition = new Vector2(2, 2);

                yield return Timing.WaitForSeconds(0.04f); // 23

                AnchoredPosition = new Vector2(0, 0);
            }

            if (!skipDeathEvaluation && state.IsDead())
            {
                damageBurstEffect.SetActive(false);
                damageEffectCross.SetActive(false);
                damageEffectOverlay.SetActive(false);

                yield return Timing.WaitUntilDone(EvaluateDeath(state));
                yield break;
            }

            yield return Timing.WaitUntilDone( damageEffectOverlay.DOFade(0, 1f)
                .WaitForCompletion(true));

            damageBurstEffect.SetActive(false);
            damageEffectCross.SetActive(false);
            damageEffectOverlay.SetActive(false);

            damageEffectOverlay.transform.SetParent(transform, false);
            damageEffectOverlay.transform.SetAsFirstSibling();
            damageEffectOverlay.RectTransform().anchoredPosition = Vector2.zero;

            yield return Timing.WaitForSeconds(0.25f);
        }

        public IEnumerator<float> EvaluateDeath(GameCardState state)
        {
            if (state.HasTrigger(TriggerType.DEATHRATTLE))
            {
                yield return Timing.WaitUntilDone(DeathEffect.EvaluateDeath(state, _view));
            }
            else
            {
                Timing.RunCoroutine(DeathEffect.EvaluateDeath(state, _view));
            }
        }

        public IEnumerator<float> AnimateDamagedFromOther(int damageAmount)
        {
            yield break;
        }

        public IEnumerator<float> AnimateDamagedFromAttack(int damageAmount)
        {
            gameObject.SetActive(true);

            var gameCard = _view.GetCardComponent<GameCardView>();
            gameCard.UpdateViewFromState();
            gameCard.Events.Damaged();

            damageEffectOverlay.transform.SetParent(_view.cardObject, false);
            damageEffectOverlay.RectTransform().anchoredPosition = Vector2.zero;
            damageEffectOverlay.SetActive(true);
            damageEffectOverlay.alpha = 1;

            AnchoredPosition = new Vector2(0,6);

            damageEffectCross.SetActive(true);
            damageEffectCross.Play("idle");

            damageBurstEffect.SetActive(true);
            damageBurstEffect.Play("burst");

            // Check for Ability Specific Bubbles
            var state = gameCard.State;
            var damageBubbleAnim = "spawn";
            if (state.HasAbility(AbilityKeyword.TOUGH)) damageBubbleAnim = "spawn-tough";

            var damageValueBubble = Instantiate(damageValueBubblePrefab, overlayParent, false);
            damageValueBubble.SetActive(true);
            damageValueBubble.rectTransform.anchoredPosition = Vector2.zero;

            if (damageAmount == 0)
            {
                Timing.RunCoroutine(damageValueBubble.DisplayValue($"{damageAmount}", damageBubbleAnim, gameCard.View.Rect));
            }
            else
            {
                Timing.RunCoroutine(damageValueBubble.DisplayValue($"-{damageAmount}", damageBubbleAnim, gameCard.View.Rect));
            }

            yield return  Timing. WaitForSeconds(0.06f); // 18

            AnchoredPosition = new Vector2(-5,14);

            yield return  Timing. WaitForSeconds(0.06f); // 19

            AnchoredPosition = new Vector2(5,15);

            yield return  Timing.WaitForSeconds(0.06f); // 20

            AnchoredPosition = new Vector2(1,7);

            yield return Timing.WaitForSeconds(0.06f); // 21

            AnchoredPosition = new Vector2(1,-4);

            yield return Timing.WaitForSeconds(0.06f); // 22

            AnchoredPosition = new Vector2(-1,1);

            yield return Timing.WaitForSeconds(0.06f); // 23

            AnchoredPosition = new Vector2(0,0);

            yield return Timing.WaitUntilDone( damageEffectOverlay.DOFade(0, 1f).WaitForCompletion(true));

            damageBurstEffect.SetActive(false);
            damageEffectCross.SetActive(false);
            damageEffectOverlay.SetActive(false);

            damageEffectOverlay.transform.SetParent(transform, false);
            damageEffectOverlay.transform.SetAsFirstSibling();
            damageEffectOverlay.RectTransform().anchoredPosition = Vector2.zero;

            if (state.HasTrigger(TriggerType.DEATHRATTLE))
            {
                yield return Timing.WaitUntilDone(DeathEffect.EvaluateDeath(state,_view));
            }
            else
            {
                Timing.RunCoroutine(DeathEffect.EvaluateDeath(state,_view));
            }
        }
    }
}