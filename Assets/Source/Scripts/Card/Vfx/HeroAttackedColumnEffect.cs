using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Cursors;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx
{
    public class HeroAttackedColumnEffect : MonoBehaviour, IGameVfx
    {
        public SpriteAnimation columnEffect;
        public SpriteAnimation explosionEffect;

        private Team _team;
        public GameCardView Attacker { get; set; }
        public bool IsDone { get; set; }

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {

        }

        public void Initialize(Team team, GameCardView attacker)
        {
            _team = team;
            Attacker = attacker;

            var rot = team == Team.Opponent ? 0 : 180;
            var x = Attacker.transform.position.x;
            var globalY = CrossBlitzInputModule.Instance.GetNativeScreenSize().y / 2;
            var globalPosY = team == Team.Opponent ? globalY:-globalY;
            this.RectTransform().anchoredPosition = new Vector2(0, globalPosY);
            this.RectTransform().position = new Vector3(x, this.RectTransform().position.y, 0);
            this.RectTransform().localEulerAngles = new Vector3(0, 0, rot);
        }

        public IEnumerator<float> Hit()
        {
            IsDone = false;
            gameObject.SetActive(true);
            columnEffect.SetActive(true);
            columnEffect.Play("attacked");

            if (Attacker.State.data.usesExplosionEffectWhenAttacking)
            {
                explosionEffect.SetActive(true);
                explosionEffect.Play("explosion");
            }

            while (!columnEffect.IsDone) yield return Timing.WaitForOneFrame;

            if (Attacker.State.data.usesExplosionEffectWhenAttacking)
            {
                while (!explosionEffect.IsDone) yield return Timing.WaitForOneFrame;
            }

            IsDone = true;
            Destroy(gameObject);
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            throw new System.NotImplementedException();
        }
    }
}