using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Hero;
using CrossBlitz.ServerAPI.GameLogic.Data;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class CustomAttackEffect : MonoBehaviour, IGameVfx
    {
        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public virtual void Initialize(CardView card)
        {
            throw new System.NotImplementedException();
        }

        public virtual IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            throw new System.NotImplementedException();
        }

        public virtual IEnumerator<float> AttackBuildUp(Vector2 attackDirection)
        {
            throw new System.NotImplementedException();
        }

        public virtual IEnumerator<float> AttackMinion(GameCardView target, AttackCommand command, CardHistory damageHistory, CardHistory retaliateHistory)
        {
            throw new System.NotImplementedException();
        }

        public virtual IEnumerator<float> AttackHero(HeroViewBattle heroTarget, AttackCommand command, CardHistory damageHistory, CardHistory retaliateHistory)
        {
            throw new System.NotImplementedException();
        }
    }
}