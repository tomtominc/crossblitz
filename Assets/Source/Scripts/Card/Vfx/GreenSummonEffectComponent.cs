using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class GreenSummonEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<GreenSummonEffect>("GreenSummonEffect", card.transform);
            effect.Initialize(card);
        }
    }
}