using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class WarCryEffect : MonoBehaviour, IGameVfx
    {
        private const int FadeFrames = 3;
        private const float FrameTime = 0.08f;
        public SpriteAnimation cryAnim;
        public Color overlayColor;

        private CardView _card;
        private GameCardView _gameCard;

        private RectTransform m_rect;

        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }
        public void Initialize(CardView card)
        {
            _card = card;
            _gameCard = _card.GetCardComponent<GameCardView>();
        }

        /// <summary>
        /// Play is used when spawning the effect on top of the card.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            gameObject.SetActive(true);
            AudioController.PlaySound("vfx_growl", "BARDSFX", true, gameObject);

            // Play Face
            cryAnim.Play("activate");

            // Shake Card
            var origAnchorPos = _card.RectTransform().anchoredPosition;
            _card.RectTransform().DOShakeAnchorPos(0.5f, 5, 50, 0).OnComplete(() => _card.RectTransform().DOAnchorPos(origAnchorPos, 0.1f));

            while (!cryAnim.IsDone) yield return Timing.WaitForOneFrame;

            gameObject.SetActive(false);
        }
    }
}