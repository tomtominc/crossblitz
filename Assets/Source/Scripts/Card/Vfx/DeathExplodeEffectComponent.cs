using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class DeathExplodeEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<DeathExplodeEffect>("DeathExplodeEffect", card.transform);
            effect.Initialize(card);
        }
    }
}