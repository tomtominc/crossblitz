using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class DisplayCardsEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<DisplayCardsEffect>("DisplayCardsEffect", card.transform);
            effect.Initialize(card);
        }
    }
}