using CrossBlitz.GameVfx;

namespace CrossBlitz.Card.Vfx
{
    public class DeathEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<DeathEffect>("DeathEffect", card.RectTransform());
            effect.Initialize(card);
        }
    }
}