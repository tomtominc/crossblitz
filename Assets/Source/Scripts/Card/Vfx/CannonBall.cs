using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class CannonBall : MonoBehaviour
    {
        private const float MinJumpHeight = 0.5f;
        private const float MaxJumpHeight = 1f;
        private const float JumpTimePerMeter = 0.5f;

        public SpriteAnimation animator;
        public SpriteAnimation animatorBurst;

        private float _jumpHeight;
        private float _timeToJumpApex;
        private bool _updateJump;
        private float _maxDistance;
        Vector3 _midPoint;

        [HideInInspector]
        public CardView m_targetCard;


        private void FixedUpdate()
        {
            if (_updateJump)
            {
                var distance = Vector3.Distance(transform.position, _midPoint);
                var ratio = (1 * (distance / _maxDistance));
                transform.localScale = new Vector3(2 - ratio, 2 - ratio);
            }
        }

        public void Burst()
        {
            animator.SetActive(false);
            animatorBurst.SetActive(true);
            animatorBurst.Play("smoke-burst");
        }

        public IEnumerator<float> JumpToTile(Vector3 position, Action<CannonBall> OnFinished)
        {
            var targetPos = position;
            var startPos = transform.position;
            var distance = Vector3.Distance(startPos, targetPos);

            _jumpHeight = Mathf.Clamp(distance, MinJumpHeight, MaxJumpHeight);
            _timeToJumpApex = _jumpHeight * JumpTimePerMeter;
            _updateJump = true;

            _midPoint = (transform.position + targetPos) / 2f;
            _maxDistance = Vector3.Distance(transform.position, _midPoint);

            yield return Timing.WaitUntilDone(transform.DOMove(_midPoint, _timeToJumpApex).SetEase(Ease.OutSine)
                .SetUpdate(UpdateType.Fixed).WaitForCompletion(true));

            yield return Timing.WaitUntilDone(transform.DOMove(targetPos, _timeToJumpApex).SetEase(Ease.InSine)
            .SetUpdate(UpdateType.Fixed).WaitForCompletion(true));

            _updateJump = false;
            transform.localScale = Vector3.one;

            OnFinished?.Invoke(this);
        }
    }
}