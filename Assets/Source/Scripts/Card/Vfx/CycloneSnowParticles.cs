using System;
using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.GameVfx;
using MEC;
using TakoBoyStudios;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class CycloneSnowParticles : MonoBehaviour
    {
        private float m_maxRadial;
        private float m_currentRadial;
        private float m_time;
        private ParticleSystem[] m_particles;

        private void Update()
        {
            if (m_particles == null) return;

            for (var i = 0; i < m_particles.Length; i++)
            {
                var v0 = m_particles[i].velocityOverLifetime;
                v0.radial = m_currentRadial + i;
            }

            if (m_time >= 1)
            {
                m_time += Time.deltaTime * 2;
            }
            else
            {
                m_time += Time.deltaTime * 4;
            }

            m_currentRadial = EasingFunction.EaseOutQuad(0, m_maxRadial, m_time);
        }

        public IEnumerator<float> Play(VfxSourceAndTargetData vfxData, Command command, int targetIndex)
        {
            var targetPosition = vfxData.targetCardPositions[targetIndex];

            transform.position = targetPosition;
            m_currentRadial = 0;
            m_maxRadial = 3;
            m_particles = gameObject.GetComponentsInChildren<ParticleSystem>();

            AudioController.PlaySound("vfx_wind", "BARDSFX", true, gameObject);

            while (m_time < 1) yield return Timing.WaitForOneFrame;

            for (var i = 0; i < m_particles.Length; i++)
            {
                m_particles[i].Stop(true, ParticleSystemStopBehavior.StopEmitting);
            }
        }
    }
}