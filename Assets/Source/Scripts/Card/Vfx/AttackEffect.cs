using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.GameVfx;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.Hero;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.ClientAPI.Visuals
{
    public class AttackEffect : CustomAttackEffect
    {
        private const float MeleeMoveSpeed = 300;

        public ParticleSystem absorbParticle;
        public SpriteAnimation meleeAttack;
        public SpriteAnimation meleeAttackAfterImage;
        public SpriteAnimation rangedAttack;
        public SpriteAnimation rangedAttackAfterImage;
        public SpriteAnimation arcaneAttack;
        public SpriteAnimation arcaneAttackAfterImage;
        public SpriteAnimation arcaneAttackShooter;
        public SpriteAnimation arcaneAttackShooterAfterImage;
        public SpriteAnimation arcaneAttackExplosion;
        public SpriteAnimation arcaneAttackExplosionAfterImage;
        public SpriteAnimation dashFx;
        public List<SpriteAnimation> attackLines;

        private Vector2 originalRangedAttackPosition;
        private Vector2 originalArcaneAttackPosition;

        private CardView _view;
        private RectTransform _rectTransform;

        public Vector2 AnchoredPosition
        {
            get => _rectTransform.anchoredPosition;
            set => _rectTransform.anchoredPosition = value;
        }

        public Vector2 WorldPosition
        {
            get => _rectTransform.GetPosition(CoordinateSystem.AsChildOfCanvas);
            set => _rectTransform.SetPosition(value, CoordinateSystem.AsChildOfCanvas);
        }

        public override void Initialize(CardView card)
        {
            _view = card;
            _rectTransform = _view.RectTransform();

            originalArcaneAttackPosition = arcaneAttack.RectTransform().anchoredPosition;
            originalRangedAttackPosition = rangedAttack.RectTransform().anchoredPosition;
        }

        public void ResetValues()
        {
            arcaneAttackExplosionAfterImage.transform.SetParent(transform, true);
            arcaneAttackExplosionAfterImage.RectTransform().anchoredPosition = originalArcaneAttackPosition;
            arcaneAttackExplosion.transform.SetParent(transform, true);
            arcaneAttackExplosion.RectTransform().anchoredPosition = originalArcaneAttackPosition;
            arcaneAttackAfterImage.transform.SetParent(transform, true);
            arcaneAttackAfterImage.RectTransform().anchoredPosition = originalArcaneAttackPosition;
            arcaneAttack.transform.SetParent(transform, true);
            arcaneAttack.RectTransform().anchoredPosition = originalArcaneAttackPosition;

            rangedAttackAfterImage.transform.SetParent(transform, true);
            rangedAttackAfterImage.RectTransform().anchoredPosition = originalRangedAttackPosition;
            rangedAttack.transform.SetParent(transform, true);
            rangedAttack.RectTransform().anchoredPosition = originalRangedAttackPosition;
        }

        public void SetupDirection(Team team)
        {
            var direction = team == Team.Opponent ?
                new Vector2(1, -1) : new Vector2(1, 1);

            meleeAttack.RectTransform().anchoredPosition *= direction;
            meleeAttackAfterImage.RectTransform().anchoredPosition *= direction;
            rangedAttack.RectTransform().anchoredPosition *= direction;
            rangedAttackAfterImage.RectTransform().anchoredPosition *= direction;
            arcaneAttack.RectTransform().anchoredPosition *= direction;
            arcaneAttackAfterImage.RectTransform().anchoredPosition *= direction;
            arcaneAttackShooter.RectTransform().anchoredPosition *= direction;
            arcaneAttackShooterAfterImage.RectTransform().anchoredPosition *= direction;
            dashFx.RectTransform().anchoredPosition *= direction;

            meleeAttack.transform.localScale *= direction;
            meleeAttackAfterImage.transform.localScale *= direction;
            rangedAttack.transform.localScale *= direction;
            rangedAttackAfterImage.transform.localScale *= direction;
            arcaneAttack.transform.localScale *= direction;
            arcaneAttackAfterImage.transform.localScale *= direction;
            arcaneAttackShooter.transform.localScale *= direction;
            arcaneAttackShooterAfterImage.transform.localScale *= direction;
            dashFx.transform.localScale *= direction;

            _view.cardDialogue.SetDirection(direction);

        }

        public override IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            // ========= EXAMPLES!!!!

            // Get the target views of a command -- these are the physical cards on the board.
            // var targets = new List<GameCardView>();
            //
            // for (var i = 0; i < command.TargetUids.Count; i++)
            // {
            //     var targetUid = command.TargetUids[i];
            //     var getCardTask = GameBoard.GetBoardCard(targetUid);
            //
            //     // we need to wait for the board card sometimes (in the case of it being summoned etc)
            //     yield return Timing.WaitUntilDone(getCardTask.AsCoroutine());
            //
            //     var boardTaskResult = getCardTask.Result;
            //
            //     // has an error code for you if it fails.
            //     // boardTaskResult.error
            //
            //     // has a location for you if its unable to be found (might be in the graveyard?)
            //     // boardTaskResult.errorLocation
            //
            //     // checks to see if the board card is valid
            //     if (boardTaskResult.boardCard)
            //     {
            //         targets.Add(boardTaskResult.boardCard);
            //     }
            // }
            //
            // // Get the target states of a command -- this is purely just data, sometimes you may not need the view.
            // var targetStates = new List<GameCardState>();
            //
            // for (var i = 0; i < command.TargetUids.Count; i++)
            // {
            //     var targetUid = command.TargetUids[i];
            //     var targetState = GameServer.State.GetCard(targetUid);
            //
            //     // checks to see if the target state is null -- should never be null but we still check.
            //     if (targetState != null)
            //     {
            //         targetStates.Add(targetState);
            //     }
            // }
            //
            //
            // // Getting the source card is the same as targets, just no loop.
            // var getSourceCardTask = GameBoard.GetBoardCard(command.SourceUid);
            // yield return Timing.WaitUntilDone(getSourceCardTask.AsCoroutine());
            //
            // // here's the physical source card.
            // var sourceCard = getSourceCardTask.Result.boardCard;
            //
            // // Getting the "Ability" that was used to create the command.
            //
            // // first we need to get the source card state.
            // var sourceCardState = GameServer.State.GetCard(command.SourceUid);
            //
            // if (sourceCardState != null)
            // {
            //     // get the ability via the ability index property on the command.
            //     var ability = sourceCardState.GetAbilities()[command.AbilityIndex];
            //
            //     if (ability != null)
            //     {
            //         // do something with the ability (get data and do something with that?)
            //     }
            // }

            yield break;
        }

        public override IEnumerator<float> AttackMinion(GameCardView target, AttackCommand command, CardHistory damageHistory, CardHistory retaliateHistory)
        {
            _view.GetCardComponent<HoverCard>().UsesHoverPunchEffect = false;
            _view.GetCardComponent<HoverCard>().IsHoverable = false;

            var attackDirection = command.Team == Team.Client ? new Vector2(1, 1) : new Vector2(1, -1);

            if (!command.alreadyBuiltUpAttack)
            {
                yield return Timing.WaitUntilDone(AttackBuildUp(attackDirection));
            }

            var attackType = _view.data.attackType;

            switch (attackType)
            {
                case AttackType.Melee:
                    meleeAttack.SetActive(true);
                    meleeAttack.Play("attack");
                    meleeAttackAfterImage.SetActive(true);
                    meleeAttackAfterImage.Play("attack");
                    AudioController.PlaySound("battle_attack_melee", "BARDSFX", true, gameObject);
                    break;
                case AttackType.Ranged:
                    rangedAttack.SetActive(true);
                    rangedAttack.Play("pre-attack");
                    rangedAttackAfterImage.SetActive(true);
                    rangedAttackAfterImage.Play("pre-attack");

                    rangedAttackAfterImage.transform.SetParent(target.transform, true);
                    rangedAttack.transform.SetParent(target.transform, true);

                    AudioController.PlaySound("battle_attack_ranged", "BARDSFX", true, gameObject);
                    break;

                case AttackType.Arcane:
                    arcaneAttackShooter.SetActive(true);
                    arcaneAttackShooter.Play("initial-fire");
                    arcaneAttackShooterAfterImage.SetActive(true);
                    arcaneAttackShooterAfterImage.Play("initial-fire");
                    AudioController.PlaySound("battle_attack_arcane", "BARDSFX", true, gameObject);
                    break;
            }

            // scale for attack direction
            AnchoredPosition = new Vector2(0, -6) * attackDirection;

            if (attackType != AttackType.Melee)
            {
                _view.frontOverlayCanvas.alpha = 0;
                yield return Timing. WaitForSeconds(0.03f);
            }

            _view.frontOverlayCanvas.alpha = 1;
            _view.frontOverlay.image.color = Color.white;
            _view.frontOverlayCanvas.DOFade(0, 0.18f);

            dashFx.SetActive(true);
            dashFx.Play("burst");

            switch (attackType)
            {
                case AttackType.Melee:

                    AudioController.PlaySound("battle_impact_melee", "BARDSFX", true, gameObject);

                    // scale for attack direction
                    AnchoredPosition = new Vector2(0,6) * attackDirection;
                    var meleeTarget = target.WorldPosition - (new Vector2(0, 95) * attackDirection);

                    DOTween.To(() => WorldPosition, x => WorldPosition = x, meleeTarget, 0.06f).SetEase(Ease.OutBack);

                    yield return Timing. WaitForSeconds(0.03f);
                    break;
                case AttackType.Ranged:

                    // scale for attack direction
                    var rangeTarget = new Vector2(1, -24) * attackDirection;
                    _rectTransform.DOPunchAnchorPos(Vector2.down * attackDirection * 20f, 0.33f);

                    rangedAttackAfterImage.RectTransform().DOAnchorPos(rangeTarget, 0.03f);
                    yield return Timing.WaitUntilDone(rangedAttack.RectTransform().DOAnchorPos(rangeTarget, 0.03f)
                        .WaitForCompletion(true));

                    rangedAttack.Play("attack");
                    rangedAttackAfterImage.Play("attack");

                    AudioController.PlaySound("battle_impact_ranged", "BARDSFX", true, gameObject);

                    break;
                case AttackType.Arcane: // move the spell to the target
                    var arcaneTarget = new Vector2(2, -24) * attackDirection;

                    arcaneAttack.SetActive(true);
                    arcaneAttack.Play("idle");
                    arcaneAttackAfterImage.SetActive(true);
                    arcaneAttackAfterImage.Play("idle");

                    arcaneAttackAfterImage.transform.SetParent(target.transform, true);
                    arcaneAttack.transform.SetParent(target.transform, true);
                    arcaneAttackExplosionAfterImage.transform.SetParent(target.transform, true);
                    arcaneAttackExplosion.transform.SetParent(target.transform, true);

                    _rectTransform.DOPunchAnchorPos(Vector2.down * attackDirection * 20f, 0.39f);

                    arcaneAttackAfterImage.RectTransform().DOAnchorPos(arcaneTarget, 0.09f);
                    //yield return Timing.WaitUntilDone(arcaneAttack.RectTransform().DOAnchorPos(arcaneTarget, 0.09f).WaitForCompletion(true));

                    arcaneAttack.SetActive(false);
                    arcaneAttackAfterImage.SetActive(false);

                    arcaneAttackExplosion.SetActive(true);
                    arcaneAttackExplosionAfterImage.SetActive(true);
                    arcaneAttackExplosion.RectTransform().anchoredPosition = new Vector2(8, -4);
                    arcaneAttackExplosionAfterImage.RectTransform().anchoredPosition = new Vector2(8, -4);
                    arcaneAttackExplosion.Play("hit");
                    arcaneAttackExplosionAfterImage.Play("hit");

                    AudioController.PlaySound("battle_impact_arcane", "BARDSFX", true, gameObject);

                    break;
            }

            GameplayScreen.Instance.ShakeScreen(4, 2);

            var source = GameServer.State.GetCard(command.SourceUid);
            var sourceDamageFx = _view.GetVfx<DamageEffectComponent, DamageEffect>();

            target.State.ApplyHistory(damageHistory.Uid);
            var targetDamageFx = target.View.GetVfx<DamageEffectComponent, DamageEffect>();
            Timing.RunCoroutine(targetDamageFx.AnimateDamagedFromAttack(damageHistory.DamageAmount));

            if (retaliateHistory != null && sourceDamageFx)
            {
                source.ApplyHistory(retaliateHistory.Uid);
                Timing.RunCoroutine(sourceDamageFx.GenericDamageAnimation(retaliateHistory.DamageAmount, true, true));
            }

            yield return  Timing. WaitForSeconds(0.30f); // 22

            ResetValues();

            var targetPosition = new Vector2(0, 0);

            while (Vector2.Distance(AnchoredPosition, targetPosition) > 0.1f)
            {
                AnchoredPosition = Vector2.MoveTowards(AnchoredPosition, targetPosition, MeleeMoveSpeed * Time.deltaTime);

                yield return Timing.WaitForOneFrame;
            }

            AnchoredPosition = targetPosition;

            yield return Timing. WaitForSeconds(0.06f);

            _view.frontShadowRect.DOAnchorPos(new Vector2(GameCardView.ShadowOffsetBoard, -GameCardView.ShadowOffsetBoard), 0.12f).SetEase(Ease.OutBounce);

            transform.DOScale(Vector3.one, 0.12f).SetEase(Ease.OutBounce);

            yield return  Timing. WaitForSeconds(0.06f); // 23

            AnchoredPosition = new Vector2(0,-1) * attackDirection;

            yield return  Timing. WaitForSeconds(0.06f); // 24

            AnchoredPosition = new Vector2(0,0);

            _view.GetCardComponent<HoverCard>().UsesHoverPunchEffect = true;
            _view.GetCardComponent<HoverCard>().IsHoverable = true;

            if (sourceDamageFx)
            {
                yield return Timing.WaitUntilDone(sourceDamageFx.EvaluateDeath(source));
            }
        }

        public override IEnumerator<float> AttackHero(HeroViewBattle heroTarget, AttackCommand command, CardHistory damageHistory, CardHistory retaliateHistory)
        {
            var attackDirection = command.Team == Team.Client ? new Vector2(1, 1) : new Vector2(1, -1);

            if (!command.alreadyBuiltUpAttack)
            {
                yield return Timing.WaitUntilDone(AttackBuildUp(attackDirection));
            }

            var attackType = _view.data.attackType;
            var targetTransform = GameplayScreen.Instance.shakeContainer;

            switch (attackType)
            {
                case AttackType.Melee:
                    meleeAttack.SetActive(true);
                    meleeAttack.Play("attack");
                    meleeAttackAfterImage.SetActive(true);
                    meleeAttackAfterImage.Play("attack");
                    AudioController.PlaySound("battle_attack_melee", "BARDSFX");
                    break;
                case AttackType.Ranged:
                    rangedAttack.SetActive(true);
                    rangedAttack.Play("pre-attack");
                    rangedAttackAfterImage.SetActive(true);
                    rangedAttackAfterImage.Play("pre-attack");

                    rangedAttackAfterImage.transform.SetParent(targetTransform, true);
                    rangedAttack.transform.SetParent(targetTransform, true);

                    AudioController.PlaySound("battle_attack_ranged", "BARDSFX");
                    break;

                case AttackType.Arcane:
                    arcaneAttackShooter.SetActive(true);
                    arcaneAttackShooter.Play("initial-fire");
                    arcaneAttackShooterAfterImage.SetActive(true);
                    arcaneAttackShooterAfterImage.Play("initial-fire");
                    AudioController.PlaySound("battle_attack_arcane", "BARDSFX");
                    break;
            }

            AnchoredPosition = new Vector2(0, -6)* attackDirection;

            if (attackType != AttackType.Melee)
            {
                _view.frontOverlayCanvas.alpha = 0;

                yield return Timing. WaitForSeconds(0.03f);
            }

            _view.frontOverlayCanvas.alpha = 1;
            _view.frontOverlay.image.color = Color.white;
            _view.frontOverlayCanvas.DOFade(0, 0.18f);

            dashFx.SetActive(true);
            dashFx.Play("burst");

            var targetWorldPointY = attackDirection.y * (targetTransform.rect.height / 2f);

            switch (attackType)
            {
                case AttackType.Melee:

                    AudioController.PlaySound("battle_impact_melee", "BARDSFX", true, gameObject);

                    var meleeTarget = new Vector2(0, targetWorldPointY + ( -95 * attackDirection.y));
                    DOTween.To(() => AnchoredPosition, x => AnchoredPosition = x, meleeTarget, 0.06f).SetEase(Ease.OutBack);
                    yield return Timing. WaitForSeconds(0.03f);

                    break;
                case AttackType.Ranged: // move the arrow to the target
                    var rangeTarget = new Vector2(rangedAttack.RectTransform().anchoredPosition.x, targetWorldPointY+ ( -24 * attackDirection.y));

                    _rectTransform.DOPunchAnchorPos(Vector2.down * attackDirection * 20f, 0.33f);

                    rangedAttackAfterImage.RectTransform().DOAnchorPos(rangeTarget, 0.03f);
                    yield return Timing.WaitUntilDone(rangedAttack.RectTransform().DOAnchorPos(rangeTarget, 0.03f).WaitForCompletion(true));

                    rangedAttack.RectTransform().anchoredPosition = rangeTarget;
                    rangedAttackAfterImage.RectTransform().anchoredPosition = rangeTarget;
                    rangedAttack.Play("attack");
                    rangedAttackAfterImage.Play("attack");

                    AudioController.PlaySound("battle_impact_ranged", "BARDSFX", true, gameObject);
                    break;
                case AttackType.Arcane: // move the spell to the target

                    arcaneAttack.SetActive(true);
                    arcaneAttack.Play("idle");
                    arcaneAttackAfterImage.SetActive(true);
                    arcaneAttackAfterImage.Play("idle");

                    arcaneAttackAfterImage.transform.SetParent(targetTransform, true);
                    arcaneAttack.transform.SetParent(targetTransform, true);
                    arcaneAttackExplosionAfterImage.transform.SetParent(targetTransform, true);
                    arcaneAttackExplosion.transform.SetParent(targetTransform, true);

                    var arcaneTarget =  new Vector2(arcaneAttack
                            .RectTransform().anchoredPosition.x,
                        targetWorldPointY+ ( -24 * attackDirection.y));

                    _rectTransform.DOPunchAnchorPos(Vector2.down * attackDirection * 20f, 0.39f);

                    arcaneAttackAfterImage.RectTransform().DOAnchorPos(arcaneTarget, 0.09f);
                    yield return Timing.WaitUntilDone(arcaneAttack.RectTransform().DOAnchorPos(arcaneTarget, 0.09f)
                        .WaitForCompletion(true));

                    arcaneAttack.RectTransform().anchoredPosition = arcaneTarget;
                    arcaneAttackAfterImage.RectTransform().anchoredPosition = arcaneTarget;

                    arcaneAttack.SetActive(false);
                    arcaneAttackAfterImage.SetActive(false);

                    arcaneAttackExplosion.SetActive(true);
                    arcaneAttackExplosionAfterImage.SetActive(true);
                    arcaneAttackExplosion.RectTransform().anchoredPosition =
                        arcaneTarget + (new Vector2(6, 20)*attackDirection);
                    arcaneAttackExplosionAfterImage.RectTransform().anchoredPosition =
                        arcaneTarget + (new Vector2(6, 20)*attackDirection);
                    arcaneAttackExplosion.Play("hit");
                    arcaneAttackExplosionAfterImage.Play("hit");

                    AudioController.PlaySound("battle_impact_arcane", "BARDSFX", true, gameObject);

                    break;
            }

            GameplayScreen.Instance.ShakeScreen(4, 2);

            var viewCard = _view.GetComponent<GameCardView>();

            if (heroTarget != null && heroTarget.State != null && viewCard != null)
            {
                Timing.RunCoroutine(GameplayScreen.Instance.heroSideBarContainer.HitPortrait(heroTarget.State.Team, viewCard.State,command));
                heroTarget.State.ApplyHistory(damageHistory.Uid);

                var source = GameServer.State.GetCard(command.SourceUid);
                var sourceDamageFx =  viewCard.View.GetVfx<DamageEffectComponent, DamageEffect>();

                if (retaliateHistory != null && sourceDamageFx)
                {
                    source.ApplyHistory(retaliateHistory.Uid);
                    Timing.RunCoroutine(sourceDamageFx.GenericDamageAnimation(retaliateHistory.DamageAmount, true, true));
                }
            }

            yield return  Timing. WaitForSeconds(0.30f); // 22

            ResetValues();

            var targetPosition = new Vector2(0, 0);

            while (Vector2.Distance(AnchoredPosition, targetPosition) > 0.1f)
            {
                AnchoredPosition = Vector2.MoveTowards(AnchoredPosition,
                    targetPosition, MeleeMoveSpeed * Time.deltaTime);

                yield return Timing.WaitForOneFrame;
            }

            AnchoredPosition = targetPosition;

            yield return Timing. WaitForSeconds(0.06f);

            _view.frontShadowRect.DOAnchorPos(
                new Vector2(GameCardView.ShadowOffsetBoard, -GameCardView.ShadowOffsetBoard),
                0.12f).SetEase(Ease.OutBounce);

            transform.DOScale(Vector3.one, 0.12f).SetEase(Ease.OutBounce);

            yield return  Timing. WaitForSeconds(0.06f); // 23

            AnchoredPosition = new Vector2(0,-1) * attackDirection;

            yield return  Timing. WaitForSeconds(0.06f); // 24

            AnchoredPosition = new Vector2(0,0);

            if (viewCard != null)
            {
                var source = GameServer.State.GetCard(command.SourceUid);
                var sourceDamageFx =  viewCard.View.GetVfx<DamageEffectComponent, DamageEffect>();

                if (sourceDamageFx)
                {
                    yield return Timing.WaitUntilDone(sourceDamageFx.EvaluateDeath(source));
                }
            }
        }

        public override IEnumerator<float> AttackBuildUp(Vector2 attackDirection)
        {
            yield return Timing.WaitUntilDone(AttackLiftUp());

            if (_view.data.attackType == AttackType.Melee)
            {
                AudioController.PlaySound("battle_windup_melee", "BARDSFX", true, gameObject);
            }
            else if (_view.data.attackType == AttackType.Ranged)
            {
                AudioController.PlaySound("battle_windup_ranged", "BARDSFX", true, gameObject);
            }
            else if (_view.data.attackType == AttackType.Arcane)
            {
                AudioController.PlaySound("battle_windup_arcane", "BARDSFX", true, gameObject);
            }

            const float frameTime = 0.03f;
            _view.frontOverlay.image.color = Color.white;
            _view.frontOverlayCanvas.SetActive(true);
            _view.frontOverlayCanvas.alpha = 0.5f;

            var glow =  _view.frontOverlayCanvas.
                DOFade(1, 0.12f).SetLoops(-1, LoopType.Yoyo);

            AnchoredPosition = new Vector2(0,-1)* attackDirection;

            this.SetActive(true);
            absorbParticle.SetActive(true);
            attackLines[0].SetActive(true);
            attackLines[0].Play("top-left-idle");

            yield return Timing. WaitForSeconds(frameTime); // 3
            AnchoredPosition = new Vector2(0,-3)* attackDirection;

            yield return Timing. WaitForSeconds(frameTime); // 4
            AnchoredPosition = new Vector2(0,-5)* attackDirection;
            attackLines[1].SetActive(true);
            attackLines[1].Play("top-right-idle");

            yield return Timing. WaitForSeconds(frameTime); // 5
            AnchoredPosition = new Vector2(-1,-5)* attackDirection;

            yield return Timing. WaitForSeconds(frameTime); //6
            AnchoredPosition = new Vector2(1,-6)* attackDirection;
            attackLines[2].SetActive(true);
            attackLines[2].Play("bottom-left-idle");

            yield return Timing. WaitForSeconds(frameTime);//7
            AnchoredPosition = new Vector2(0,-4)* attackDirection;

            yield return Timing. WaitForSeconds(frameTime);//8
            AnchoredPosition = new Vector2(-1,-6)* attackDirection;

            yield return Timing. WaitForSeconds(frameTime);//9
            AnchoredPosition = new Vector2(1,-4)* attackDirection;
            attackLines[3].SetActive(true);
            attackLines[3].Play("bottom-right-idle");

            yield return Timing. WaitForSeconds(frameTime);//10
            AnchoredPosition = new Vector2(0,-5)* attackDirection;

            yield return Timing. WaitForSeconds(frameTime); //11 --
            AnchoredPosition = new Vector2(1, -5)* attackDirection;

            yield return Timing. WaitForSeconds(frameTime); //12
            AnchoredPosition = new Vector2(-1, -6)* attackDirection;

            yield return Timing. WaitForSeconds(frameTime); //13
            AnchoredPosition = new Vector2(1, -4)* attackDirection;

            yield return Timing. WaitForSeconds(frameTime); //14
            AnchoredPosition = new Vector2(-1, -4)* attackDirection;

            yield return Timing. WaitForSeconds(frameTime); //15 --
            AnchoredPosition = new Vector2(0, -5)* attackDirection;

            TweenUtility.Kill(glow, true);

            absorbParticle.SetActive(false);
            attackLines.ForEach(x=>x.SetActive(false));

            if (_view.data.attackType != AttackType.Melee)
                yield return Timing. WaitForSeconds(0.16f);//16
        }

        private IEnumerator<float> AttackLiftUp()
        {
            var liftOffDuration = 0.4f;
            var ease = Ease.OutBack;

            _view.frontShadowRect.DOAnchorPos(new Vector2(GameCardView.ShadowOffsetMin, -GameCardView.ShadowOffsetMin), liftOffDuration).SetEase(ease);

            transform.DOScale(new Vector2(1.16f, 1.16f), liftOffDuration).SetEase(ease);

            yield return Timing.WaitUntilDone(_rectTransform.DOAnchorPosY(2f, liftOffDuration).SetEase(ease).WaitForCompletion(true));
        }

        public void Unload()
        {

        }
    }
}
