using System.Collections.Generic;
using Asyncoroutine;
using BlendModes;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI.GameLogic;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class SlimeSummonEffect : MonoBehaviour, IGameVfx
    {
        public SlimeBall slimeBallPrefab;
        //public Color overlayLime;
        // private CardView _card;
        //
        // private RectTransform m_rect;
        public void Initialize(Transform rect) { }

        public void Initialize(CardView card) { }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            gameObject.SetActive(true);

            var getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(command, false);
            yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
            var vfxData = getSourceAndTargetData.Result;
            var handles = new List<CoroutineHandle>();

            var sourceObject = vfxData.sourceCardObject;

            if (sourceObject != null)
            {
                var overlayColor = "88CC37".ToColor(191f / 255f);
                sourceObject.View.TintOverTime(overlayColor, BlendMode.Darken, 0.25f);
                sourceObject.View.cardObject.DOShakeAnchorPos(0.25f, 5, 50, 0);
                yield return Timing.WaitForSeconds(0.24f);
                sourceObject.View.RemoveTint();
            }

            for (var i = 0; i < vfxData.targetCards.Count; i++)
            {
                var slimeBall = Instantiate(slimeBallPrefab, transform, false);
                handles.Add( Timing.RunCoroutine(slimeBall.Play(vfxData, command, i)));
                yield return Timing.WaitForSeconds(.4f);
            }

            while (handles.Exists(handle => handle.IsRunning)) yield return Timing.WaitForOneFrame;

            if (sourceObject != null)
            {
                sourceObject.View.RemoveTint();
                sourceObject.View.cardObject.anchoredPosition = Vector2.zero;
            }

            // var getBoardCardTask =AddressableReferenceLoader.GetAsset("Card_Board", true);
            // yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
            // var boardCardPrefab = getBoardCardTask.Result;
            //
            // var settings = new CreateCardFactorySettings
            // {
            //     CardPrefab = boardCardPrefab,
            //     StartsDisabled = true
            // };
            //
            // var summonCommand = (SummonCommand) command;
            //
            //
            //
            // for (var i = 0; i < summonCommand.Positions.Count; i++)
            // {
            //     var position = summonCommand.Positions[i];
            //     var cardState = GameServer.State.GetCard( summonCommand.TargetUids[i] );
            //     var tile = GameplayScreen.Instance.GetBoardTile(position);
            //
            //     // get a reference to the card object (child object of the card so we aren't moving the card directly.
            //     var cardRect = _card.cardObject;
            //     var origAnchorPos = cardRect.anchoredPosition;
            //     // Shake Slime
            //     _card.TintOverTime(overlayLime, BlendMode.Darken, 0.25f);
            //     cardRect.DOShakeAnchorPos(0.25f, 5, 50, 0);
            //     yield return Timing.WaitForSeconds(0.25f);
            //
            //     // ShootFX
            //     _card.RemoveTint();
            //     _card.Tint(Color.white, BlendMode.Lighten);
            //     _card.RemoveTintOverTime(0.25f);
            //
            //     // Spawn Slime
            //     cardRect.RectTransform().DOPunchAnchorPos(Vector2.down * 20, 0.25f, 2, 0).SetEase(Ease.OutBack).OnComplete(() => cardRect.DOAnchorPos(origAnchorPos, 0.1f));
            //     var slimeBall = Instantiate(slimeBallPrefab, _card.transform);
            //     GameplayScreen.Instance.ShakeScreen(4, 2);
            //     //slimeBall.animator.Play("glob-spawn");
            //     //while (!slimeBall.animator.IsDone) yield return Timing.WaitForOneFrame;
            //     slimeBall.animator.Play("glob-idle");
            //     yield return Timing.WaitUntilDone(slimeBall.JumpToTile(tile.transform.position, null));
            //
            //     slimeBall.animator.Play("glob-explode");
            //     settings.CardState = cardState;
            //     settings.Layout = tile.RectTransform();
            //     settings.AdditionalComponents = GameVfxComponent.GetRequiredVfxComponents(cardState);
            //
            //     settings.AdditionalComponents.Add(typeof(GreenSummonEffectComponent));
            //
            //     var boardCard = CardFactory.Create(settings, null);
            //     var boardCardGameView = boardCard.GetCardComponent<GameCardView>();
            //
            //     while (!boardCard.AllVfxComponentsLoaded()) yield return Timing.WaitForOneFrame;
            //
            //     boardCard.GetVfx<AttackEffectComponent, AttackEffect>().SetupDirection(boardCardGameView.State.Team);
            //     boardCard.SetActive(true);
            //     Timing.RunCoroutine(boardCardGameView.AnimateSummonOnTile(tile, cardState, SummonMinionData.SummonVisualEffect.Slime, summonCommand.SummonMethod));
            //     GameplayScreen.Instance.SetBoardTile(boardCardGameView, position);
            //     vfxParameters.summons.Add(boardCardGameView);
            // }
        }
    }
}