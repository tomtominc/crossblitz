using System.Collections.Generic;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI.GameLogic.Data;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class UseHandCardEffect : GenericEffect
    {
        public SpriteAnimation animator;
        public void Hide()
        {
            animator.SetActive(false);
        }

        public IEnumerator<float> PlayEffect(GameCardState cardState, Vector3 position)
        {
            transform.position = position;
            gameObject.SetActive(true);
            animator.SetActive(true);

            var animName = $"use-{cardState.data.type.ToString().ToLower()}";

            if (!animator.Play(animName))
            {
                Debug.LogError($"{animName} can't be played.");
            }

            while (!animator.IsDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            Destroy(gameObject);
        }
    }
}