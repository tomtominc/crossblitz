using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class TrapCardEffectComponents : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<TrapCardEffect>("TrapCardEffect", card.portraitMask.transform);
            effect.Initialize(card);
        }
    }
}