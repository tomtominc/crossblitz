using System;
using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.GameVfx;
using CrossBlitz.GameVfx.Spell;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.Settings;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.Card.Vfx
{
    public class ToadstoolToot : MonoBehaviour, IGameVfx
    {
        public SpriteAnimation toadstoolWhistle;
        public PuffProjectile puffProjectilePrefab;

        public void Initialize(CardView card)
        {

        }

        public void Initialize(Transform rect)
        {

        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            toadstoolWhistle.SetActive(true);
            toadstoolWhistle.Play("spawn");

            var getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(command);
            yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
            var vfxData = getSourceAndTargetData.Result;
            var handles = new List<CoroutineHandle>();

            var commandsList = GameServer.State.GetCardHistories(vfxData.sourceCard.uid);

            for (var i = 0; i < commandsList.Count; i++)
            {
                if (commandsList[i] is DestroyCommand destroyCommand)
                {
                    Server.CommandResolver.RemoveCommandFromResolve(destroyCommand);
                }
            }

            while (!toadstoolWhistle.IsDone) yield return Timing.WaitForOneFrame;

            toadstoolWhistle.Play("whistle");

            while (!toadstoolWhistle.IsDone) yield return Timing.WaitForOneFrame;

            for (var i = 0; i < vfxData.targetCards.Count; i++)
            {
                var puffProjectile = Instantiate(puffProjectilePrefab, transform, false);
                handles.Add(Timing.RunCoroutine(puffProjectile.Play(vfxData, command, i, -1, string.Empty, .5f, true, true)));

                yield return Timing.WaitForSeconds(Random.Range(0.2f, 0.4f));
            }

            toadstoolWhistle.Play("despawn");

            while (!toadstoolWhistle.IsDone) yield return Timing.WaitForOneFrame;
            while (handles.Exists(handle => handle.IsRunning)) yield return Timing.WaitForOneFrame;

            Destroy(gameObject, 2f);
        }
    }
}