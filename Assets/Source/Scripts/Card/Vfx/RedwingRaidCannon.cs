using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class RedwingRaidCannon : MonoBehaviour
    {
        public string direction;
        public SpriteAnimation animator;
        public SpriteAnimation burst;

        public enum State
        {
            Spawning,
            Firing,
            Waiting
        }

        private State m_state;

        public State GetState()
        {
            return m_state;
        }

        public void Spawn()
        {
            m_state = State.Spawning;
            animator.SetActive(true);
            animator.Play(direction, ()=> m_state = State.Waiting );
            AudioController.PlaySound("map_land", "BARDSFX", true, gameObject).setPitch(0.75f);
        }

        public IEnumerator<float> Fire()
        {
            var dir = GetPunchDirection();
            animator.RectTransform().anchoredPosition = new Vector2(0, 40 * dir.y);

            yield return Timing.WaitForSeconds(0.1f);

            GameplayScreen.Instance.ShakeScreen();
            AudioController.PlaySound("vfx_cannonshot", "BARDSFX", true, gameObject);

            m_state = State.Firing;

            animator.RectTransform().DOAnchorPosY(0f, 0.24f);

            burst.SetActive(true);
            burst.Play("smoke-burst");

            while (burst.IsDone == false) yield return Timing.WaitForOneFrame;

            burst.SetActive(false);
            m_state = State.Waiting;
        }

        public void Despawn()
        {
            var dir = GetPunchDirection();
            animator.RectTransform().DOAnchorPos(new Vector2(0, 64 * dir.y), 0.4f).SetEase(Ease.InQuad);
        }

        public Vector3 GetPunchDirection()
        {
            if (direction == "bottom") return Vector2.down;
            return Vector2.up;
        }
    }
}