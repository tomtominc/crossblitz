using System.Collections.Generic;
using System.Linq;
using Asyncoroutine;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.GameVfx;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class RedwingRaidEffect : MonoBehaviour, IGameVfx
    {
        public RectTransform topCannonContainer;
        public RectTransform bottomCannonContainer;


        public RectTransform topExplosionContainer;
        public RectTransform bottomExplosionContainer;

        public RedwingRaidCannon bottomCannonPrefab;
        public RedwingRaidCannon topCannonPrefab;
        public GenericEffectParticle explosionPrefab;

        public void Initialize(CardView card)
        {

        }

        public void Initialize(Transform rect)
        {

        }

        public IEnumerator<float> SpawnExplosionsRandomly(RectTransform explosionContainer)
        {
            for (var i = 0; i < 20; i++)
            {
                var explosion = Instantiate(explosionPrefab, explosionContainer, false);

                float x = Random.Range(-explosionContainer.rect.width / 2, explosionContainer.rect.width / 2);
                float y = Random.Range(-explosionContainer.rect.height / 2, explosionContainer.rect.height / 2);

                explosion.RectTransform().anchoredPosition = new Vector3(x, y, 0);
                explosion.animator.Play("explosion");
                AudioController.PlaySound("vfx_fiery_explosion", "BARDSFX", true, gameObject);

                GameplayScreen.Instance.ShakeScreen();

                yield return Timing.WaitForSeconds(0.12f);
            }
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            var getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(command);
            yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
            var vfxData = getSourceAndTargetData.Result;
            var handles = new List<CoroutineHandle>();
            var cannons = new List<RedwingRaidCannon>();
            var cannonPrefab = vfxData.sourceCard.Team == GameServer.LocalPlayerTeam
                ? bottomCannonPrefab
                : topCannonPrefab;
            var cannonContainer = vfxData.sourceCard.Team == GameServer.LocalPlayerTeam
                ? bottomCannonContainer
                : topCannonContainer;
            var explosionContainer = vfxData.sourceCard.Team == GameServer.LocalPlayerTeam
                ? topExplosionContainer
                : bottomCannonContainer;

            for (var i = 0; i < 4; i++)
            {
                var cannon = Instantiate(cannonPrefab, cannonContainer, false);
                cannons.Add(cannon);
            }

            cannons.Shuffle();

            for (var i = 0; i < cannons.Count; i++)
            {
                cannons[i].Spawn();
                yield return Timing.WaitForSeconds(0.12f);
            }

            Timing.RunCoroutine(SpawnExplosionsRandomly(explosionContainer));

            for (var i = 0; i < 3; i++)
            {
                for (var j = 0; j < cannons.Count; j++)
                {
                    Timing.RunCoroutine(cannons[j].Fire());

                    yield return Timing.WaitForSeconds(0.24f);
                }

                cannons.Shuffle();
            }

            for (var i = 0; i < vfxData.targetCardObjects.Count; i++)
            {
                var targetCard = vfxData.targetCardObjects[i];
                GameVfxUtilities.HandleDamageCommand(command, vfxData, i, targetCard);
            }

            while (cannons.Exists(c => c.GetState() != RedwingRaidCannon.State.Waiting))
                yield return Timing.WaitForOneFrame;

            yield return Timing.WaitForSeconds(0.24f);

            for (var i = 0; i < cannons.Count; i++)
            {
                cannons[i].Despawn();
                yield return Timing.WaitForSeconds(0.12f);
            }

            yield return Timing.WaitForSeconds(0.4f);

            Destroy(gameObject);
        }
    }
}