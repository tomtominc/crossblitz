using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class DiscardEffectComponent: GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<DiscardEffect>("DiscardEffect", card.transform);
            effect.Initialize(card);
        }
    }
}