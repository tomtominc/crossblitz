using System;
using CrossBlitz.GameVfx;
using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class GenericVfxComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<IGameVfx>(m_vfxData.vfxPrefab, transform);
            effect.Initialize(transform);
        }
    }
}