using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using DG.Tweening;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class SpellTargetEffect : MonoBehaviour, IGameVfx
    {
        public CanvasGroup canvasGroup;
        public SpriteAnimation animator;
        public SpriteAnimation animator2;

        private CardView _cardView;
        private DragRotator _drag;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _cardView = card;
            _drag = _cardView.GetCardComponent<DragRotator>();
        }

        public void OnAfterCreation()
        {

        }

        public void ShowEffect(bool show)
        {
            gameObject.SetActive(show);
            canvasGroup.alpha = 1;
            //Cursor.visible = !show;
            _drag.Reset();
            _drag.rotationEnabled = !show;

            if (show)
            {
                animator.Play("idle");
                animator2.Play("idle");
            }
        }

        public CustomYieldInstruction Fade(float fade, float duration)
        {
            return canvasGroup.DOFade(fade, duration).WaitForCompletion(true);
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            throw new System.NotImplementedException();
        }

        public void Unload()
        {

        }
    }
}