using CrossBlitz.AssetManagement;
using CrossBlitz.GameVfx;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class ManaMeldRecipeComponent : GameVfxComponent
    {
        public ManaMeldCollectionRecipe manaMeldCollectionRecipe;

        public override async void LoadEffect()
        {
            //effect = await LoadEffect<ManaMeldCollectionRecipe>("ManaMeldCollectionRecipe", card.transform);
           // effect.Initialize(card);
           // manaMeldCollectionRecipe = effect.GetComponent<ManaMeldCollectionRecipe>();

            var manaMeldRecipeComponentObject = await AddressableReferenceLoader.Load("ManaMeldCollectionRecipe", card.cardObject);
            manaMeldCollectionRecipe = manaMeldRecipeComponentObject.GetComponent<ManaMeldCollectionRecipe>();
            manaMeldCollectionRecipe.Initialize(card);
        }
    }
}