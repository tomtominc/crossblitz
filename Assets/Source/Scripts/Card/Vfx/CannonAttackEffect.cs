using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Cursors;
using CrossBlitz.Hero;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class CannonAttackEffect : CustomAttackEffect
    {
        public float cannonballSpeed = 32;
        public SpriteAnimation smokePuff;
        public SpriteAnimation cannonBall;

        private CardView _card;
        private GameCardView _gameCard;

        public override void Initialize(CardView card)
        {
            _card = card;
            _gameCard = _card.GetCardComponent<GameCardView>();
        }

        public override IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            yield break;
        }

        public override IEnumerator<float> AttackBuildUp(Vector2 attackDirection)
        {
            yield break;
        }

        private IEnumerator<float> ShootCannonball(Vector3 targetPosition)
        {
            while (Vector3.Distance(cannonBall.transform.position, targetPosition) > 1)
            {
                cannonBall.transform.position = Vector3.MoveTowards(cannonBall.transform.position, targetPosition, cannonballSpeed * Time.deltaTime);
                yield return Timing.WaitForOneFrame;
            }
        }

        public override IEnumerator<float> AttackMinion(GameCardView target, AttackCommand command, CardHistory damageHistory, CardHistory retaliateHistory)
        {
            gameObject.SetActive(true);
            cannonBall.SetActive(true);
            smokePuff.SetActive(true);

            if (_gameCard.State.HasAbility(AbilityKeyword.FLYING))
            {
                var flyingEffect = _gameCard.View.GetVfx<FlyingEffectComponent,FlyingEffect>();

                if (flyingEffect)
                {
                    flyingEffect.StopFlying();
                }
            }

            var cardRect = _card.cardObject;
            var offsetScale = command.Team == Team.Opponent ? 1 : -1;
            var rot = command.Team == Team.Opponent ? 180 : 0;

            cardRect.anchoredPosition = new Vector2(0, 15f * offsetScale);
            cardRect.localEulerAngles = new Vector3(0, 0, rot);

            _card.Tint(Color.white, BlendMode.Overlay);

            smokePuff.Play("smoke-burst");
            cannonBall.Play("spawn", ()=> cannonBall.Play("idle"));

            Timing.RunCoroutine(UpdateCardPosition(cardRect, offsetScale));

            yield return Timing.WaitUntilDone(ShootCannonball(target.transform.position));

            cannonBall.SetActive(false);

            GameplayScreen.Instance.ShakeScreen(8,4);

            target.State.ApplyHistory(damageHistory.Uid);

            var targetDamageFx = target.View.GetVfx<DamageEffectComponent, DamageEffect>();
            var source = GameServer.State.GetCard(command.SourceUid);
            Timing.RunCoroutine(targetDamageFx.AnimateDamagedFromAttack(source.GetPower()));

            yield return  Timing. WaitForSeconds(0.30f);

            if (_gameCard.State.HasAbility(AbilityKeyword.FLYING))
            {
                var flyingEffect = _gameCard.View.GetVfx<FlyingEffectComponent,FlyingEffect>();
                if (flyingEffect) flyingEffect.StartFlying();
            }
        }

        public override IEnumerator<float> AttackHero(HeroViewBattle heroTarget, AttackCommand command, CardHistory damageHistory, CardHistory retaliateHistory)
        {
            gameObject.SetActive(true);
            cannonBall.SetActive(true);
            smokePuff.SetActive(true);

            if (_gameCard.State.HasAbility(AbilityKeyword.FLYING))
            {
                var flyingEffect = _gameCard.View.GetVfx<FlyingEffectComponent,FlyingEffect>();

                if (flyingEffect)
                {
                    flyingEffect.StopFlying();
                }
            }

            var cardRect = _card.cardObject;
            var offsetScale = command.Team == Team.Opponent ? 1 : -1;
            var rot = command.Team == Team.Opponent ? 180 : 0;

            cardRect.anchoredPosition = new Vector2(0, 15f * offsetScale);
            cardRect.localEulerAngles = new Vector3(0, 0, rot);

            _card.Tint(Color.white, BlendMode.Overlay);

            smokePuff.Play("smoke-burst");
            cannonBall.Play("spawn", ()=> cannonBall.Play("idle"));

            Timing.RunCoroutine(UpdateCardPosition(cardRect, offsetScale));

            var screenSize = CrossBlitzInputModule.Instance.GetNativeScreenSize();
            var targetPosition = command.Team == Team.Opponent
                ? new Vector2(cannonBall.transform.position.x, -(screenSize.y * 0.0312f) / 2)
                : new Vector2(cannonBall.transform.position.x, (screenSize.y * 0.0312f) / 2);

            yield return Timing.WaitUntilDone(ShootCannonball(targetPosition));

            cannonBall.SetActive(false);

            GameplayScreen.Instance.ShakeScreen(8,4);

            if (heroTarget != null && heroTarget.State != null)
            {
                var viewCard = _card.GetComponent<GameCardView>();

                if (viewCard != null)
                {
                    Timing.RunCoroutine(GameplayScreen.Instance.heroSideBarContainer.HitPortrait(heroTarget.State.Team, viewCard.State,command));
                    heroTarget.State.ApplyHistory(damageHistory.Uid);
                }
            }

            yield return  Timing. WaitForSeconds(0.30f);

            if (_gameCard.State.HasAbility(AbilityKeyword.FLYING))
            {
                var flyingEffect = _gameCard.View.GetVfx<FlyingEffectComponent,FlyingEffect>();
                if (flyingEffect) flyingEffect.StartFlying();
            }
        }

        private IEnumerator<float> UpdateCardPosition(RectTransform cardRect, int offsetScale)
        {
            cardRect.DOPunchScale(new Vector3(-0.16f, 0.16f, 0), 0.16f);

            yield return Timing.WaitForSeconds(0.04f);

            _card.RemoveTintOverTime(0.12f);

            yield return Timing.WaitUntilDone(cardRect.DOAnchorPosY(19f * offsetScale, 0.12f)
                .SetEase(Ease.OutBack).WaitForCompletion(true));

            yield return Timing.WaitUntilDone(cardRect.DOAnchorPosY(0f, 0.24f)
                .SetEase(Ease.OutBack).WaitForCompletion(true));
        }
    }
}