using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CrossBlitz.AssetManagement;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using CrossBlitz.ServerAPI.GameLogic.Data;
using Source.Scripts.Card;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class GameVfxComponent : MonoBehaviour, ICardComponent, ICardEffectComponent
    {
        protected bool initialized;
        protected CardView card;
        protected IGameVfx effect;
        protected GameObject effectPrefab;
        protected Transform effectParent;

        public IGameVfx Effect => effect;

        public virtual void Initialize(CardView cardView)
        {
            card = cardView;
        }

        public void OnAfterCreation()
        {
            LoadEffect();
        }

        public virtual async void LoadEffect()
        {

        }

        protected async Task<T> LoadEffect<T> (string effectName, Transform parent) where T : IGameVfx
        {
            initialized = true;
            effectParent = parent;
            effectPrefab = await AddressableReferenceLoader.GetAsset(effectName, true);
            var effectObject = Instantiate(effectPrefab, effectParent, false);
            return effectObject.GetComponent<T>();
        }

        public static List<Type> GetRequiredVfxComponents(GameCardState cardState)
        {
            var requiredComponents = new List<Type>{typeof(GameCardView),
                typeof(HoverCard), typeof(BoardTarget), typeof(AttackEffectComponent), typeof(SummonEffectComponent),
                typeof(DamageEffectComponent), typeof(GenericEffectComponent), typeof(DeathEffectComponent),
                typeof(CardEventsComponent), typeof(SummoningSicknessEffectComponent) };

            if (cardState.data.hasCustomAttackProperties && !string.IsNullOrEmpty(cardState.data.customAttackEffect))
            {
                requiredComponents.AddDistinct(Type.GetType(cardState.data.customAttackEffect));
            }

            if (cardState.characterData.type == CardType.Trick)
            {
                requiredComponents.AddDistinct(typeof(TrapCardEffectComponents));
            }

            var abilities = cardState.GetAbilities();

            if (abilities.Count <= 0) return requiredComponents;

            for (var i = 0; i < abilities.Count; i++)
            {
                var ability = abilities[i];

                if (ability.triggerType.HasFlag(TriggerType.BATTLECRY))
                {
                    requiredComponents.AddDistinct(typeof(BattlecryEffectComponent));
                }

                if (ability.triggerType.HasFlag(TriggerType.DEATHRATTLE))
                {
                    requiredComponents.AddDistinct(typeof(DeathrattleEffectComponent));
                }

                if (ability.IsRiff())
                {
                    requiredComponents.AddDistinct(typeof(RiffEffectComponent));
                }

                if (ability.usesCustomEffect && !ability.usesBoardEffect && !string.IsNullOrEmpty(ability.customEffectComponent))
                {
                    var type = Type.GetType(ability.customEffectComponent);
                    if (type != null ) requiredComponents.AddDistinct(type);
                }

                switch (ability.keyword)
                {
                    case AbilityKeyword.GAIN_ARMOR: requiredComponents.AddDistinct(typeof(GrantArmorEffectComponent));
                        break;
                    case AbilityKeyword.TOUGH: requiredComponents.AddDistinct(typeof(ToughEffectComponent));
                        break;
                    case AbilityKeyword.BARRIER: requiredComponents.AddDistinct(typeof(BarrierEffectComponent));
                        break;
                    case AbilityKeyword.THORNS: requiredComponents.AddDistinct(typeof(ThornsEffectComponent));
                        break;
                    case AbilityKeyword.FLYING: requiredComponents.AddDistinct(typeof(FlyingEffectComponent));
                        break;
                    case AbilityKeyword.LIFESTEAL: requiredComponents.AddDistinct(typeof(LifestealEffectComponent));
                        break;
                    case AbilityKeyword.DUAL_STRIKE: requiredComponents.AddDistinct(typeof(DualstrikeEffectComponent));
                        break;
                    case AbilityKeyword.GUARD: requiredComponents.AddDistinct(typeof(GuardEffectComponent));
                        break;
                    case AbilityKeyword.FLEETING: requiredComponents.AddDistinct(typeof(FleetingEffectComponent));
                        break;
                }
            }

            return requiredComponents;
        }

        protected GameVfxData m_vfxData;

        public void SetGameVfx(GameVfxData vfxData)
        {
            m_vfxData = vfxData;
        }
    }
}
