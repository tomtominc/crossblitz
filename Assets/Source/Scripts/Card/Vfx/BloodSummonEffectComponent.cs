using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class BloodSummonEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<BloodSummonEffect>("BloodSummonEffect", card.transform);
            effect.Initialize(card);
        }
    }
}