using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class GolemBuildEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<GolemBuildEffect>("GolemBuildEffect", card.transform);
            effect.Initialize(card);
        }
    }
}