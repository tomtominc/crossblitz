using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class SacrificialStageEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<SacrificialStageEffect>("SacrificialStageEffect", card.transform);
            effect.Initialize(card);
        }
    }
}