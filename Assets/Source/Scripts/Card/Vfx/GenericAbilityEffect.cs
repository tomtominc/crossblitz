using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class GenericAbilityEffect : MonoBehaviour, IGameVfx
    {
        private CardView _card;
        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }
        public void Initialize(CardView card)
        {
            _card = card;
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            if (vfxParams != null && vfxParams.dimScreen)
            {
                GameplayScreen.Instance.ShowTargets(new List<BoardTarget>{_card.GetCardComponent<BoardTarget>()});
            }

            gameObject.SetActive(true);

            if (_card.genericTriggerIcon.IsActive())
            {
                _card.genericTriggerIcon.Play("active");

                while (!_card.genericTriggerIcon.IsDone)
                {
                    yield return Timing.WaitForOneFrame;
                }

                _card.genericTriggerIcon.Play("non-active");
            }
            else
            {
                yield return Timing.WaitForSeconds(0.72f);
            }

            gameObject.SetActive(false);

            if (vfxParams != null && vfxParams.dimScreen)
            {
                GameplayScreen.Instance.HideTargets();
            }
        }
    }
}