using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class FlyingEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<FlyingEffect>("FlyingEffect", card.cardObject);
            effect.Initialize(card);
        }
    }
}