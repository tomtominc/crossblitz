using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class SilenceEffect : MonoBehaviour, IGameVfx
    {
        public SpriteAnimation animator;

        public void Initialize(CardView card)
        {

        }

        public void Initialize(Transform rect)
        {

        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            gameObject.SetActive(true);
            animator.SetActive(true);
            animator.Play("activate");

            while (!animator.IsDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            gameObject.SetActive(false);
        }
    }
}