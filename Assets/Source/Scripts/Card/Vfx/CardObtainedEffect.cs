using System.Collections.Generic;
using Coffee.UIExtensions;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.PlayFab.Authentication;
using TMPro;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class CardObtainedEffect : MonoBehaviour, IGameVfx
    {
        public GameObject cardNewlyObtainedText;
        public List<ParticleSystem> cardNewlyObtainedSparkles;
        public GameObject newRecipeUnlock;
        public SpriteAnimation mawldersShop;

        private CardView m_card;

        private RectTransform m_rect;
        private float m_newTextOrigPosX;
        private float m_newTextOrigPosY;

        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
            m_newTextOrigPosX = cardNewlyObtainedText.RectTransform().anchoredPosition.x;
            m_newTextOrigPosY = cardNewlyObtainedText.RectTransform().anchoredPosition.y;
        }

        public void Initialize(CardView card)
        {
            m_card = card;
        }

        private void Update()
        {
            transform.localScale = Vector3.one;
        }

        public void SetNewlyObtained(bool isNewlyObtained)
        {
            gameObject.SetActive(true);

            if (isNewlyObtained && App.Settings.GetNewCardNotificationsEnabled())
            {
                m_card.EnableUsageGlow(true, true);
                cardNewlyObtainedText.SetActive(true);

                for (var i = 0; i < cardNewlyObtainedSparkles.Count; i++)
                {
                    cardNewlyObtainedSparkles[i].Play(true);
                }
            }
            else
            {
                m_card.EnableUsageGlow(false, immediate:true);
                cardNewlyObtainedText.SetActive(false);

                for (var i = 0; i < cardNewlyObtainedSparkles.Count; i++)
                {
                    cardNewlyObtainedSparkles[i].Stop(true, ParticleSystemStopBehavior.StopEmitting);
                }
            }
        }

        public void SetupForShop(bool turnOn)
        {

            if (turnOn)
            {
                for (var i = 0; i < cardNewlyObtainedSparkles.Count; i++)
                {
                    cardNewlyObtainedSparkles[i].SetActive(false);
                }

                cardNewlyObtainedText.GetComponent<TextMeshProUGUI>().rectTransform.SetWidth(120);
                cardNewlyObtainedText.GetComponent<TextMeshProUGUI>().text = "NEW CARD IN STOCK!";
                cardNewlyObtainedText.RectTransform().anchoredPosition = new Vector2(m_newTextOrigPosX, m_newTextOrigPosY - 88);
            }

            gameObject.SetActive(turnOn);

        }

        public void SetupForNewRecipe(bool turnOn)
        {
            cardNewlyObtainedText.SetActive(false);
            newRecipeUnlock.SetActive(turnOn);
            gameObject.SetActive(turnOn);

            if (turnOn)
            {
               // mawldersShop.Play("melding-furnace");
            }

            for (var i = 0; i < cardNewlyObtainedSparkles.Count; i++)
            {
                cardNewlyObtainedSparkles[i].GetComponent<UIParticle>().scale = 3f;
            }
        }

        public void SetParticleSize(float size)
        {
            for (var i = 0; i < cardNewlyObtainedSparkles.Count; i++)
            {
                cardNewlyObtainedSparkles[i].GetComponent<UIParticle>().scale = size;
            }
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            throw new System.NotImplementedException();
        }
    }
}