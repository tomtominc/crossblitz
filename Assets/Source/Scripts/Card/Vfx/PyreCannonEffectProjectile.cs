using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using DG.Tweening;
using MEC;
using UnityEngine;
using Asyncoroutine;
using CrossBlitz.ClientAPI;
using CrossBlitz.Audio;

namespace CrossBlitz.GameVfx.Minion
{
    public class PyreCannonEffectProjectile : MonoBehaviour
    {
        public const float FrameTime = 0.04f;

        public SpriteAnimation explodeAnim;
        public Color overlayRed;
        public Color overlayWhite;

        private GameCardView m_targetCard;
        private GameCardView m_sourceCard;

        private bool m_shakeTarget;
        private int m_shakeSide;
        private float m_shakeDelay;

        private void Update()
        {
            if (m_shakeTarget && m_targetCard !=null)
            {
                m_shakeDelay -= Time.deltaTime;

                if (m_shakeDelay <= 0)
                {
                    m_targetCard.AnchoredPosition = new Vector2(m_shakeSide, 0);
                    m_shakeDelay = FrameTime;
                    m_shakeSide *= -1;
                }
            }
        }

        public IEnumerator<float> Play( VfxSourceAndTargetData vfxData, Command command, int targetIndex, bool spawnOnEmptyTile )
        {
            // by default the effects are always inactive, we'll need to make them active when we play them.
            gameObject.SetActive(true);

            // get a reference to the card object (child object of the card so we aren't moving the card directly.
            var sourcePosition = vfxData.sourceCardPosition;
            m_sourceCard = vfxData.sourceCardObject;

            var targetPosition = spawnOnEmptyTile ?
                vfxData.targetEmptyTilePositions[targetIndex] :
                vfxData.targetCardPositions[targetIndex];

            if (!spawnOnEmptyTile)
            {
                m_targetCard = vfxData.targetCardObjects[targetIndex];
            }

            // Shake Card
            if (m_targetCard != null)
            {
                m_shakeSide = 1;
                m_shakeDelay = 0;
                m_shakeTarget = true;
            }

            explodeAnim.transform.position = targetPosition;

            yield return Timing.WaitForSeconds(0.4f);

            // Play Explosion Animation and change color
            explodeAnim.SetActive(true);
            explodeAnim.Play("explode");
            AudioController.PlaySound("vfx_fiery_explosion", "BARDSFX", true, gameObject);

            // if(_card != null) _card.Tint(overlayWhite, BlendMode.Lighten);
            // yield return Timing.WaitForSeconds(0.2f);
            // if (_card != null) _card.Tint(overlayRed, BlendMode.Color);
            // if (_card != null) _card.RemoveTintOverTime(1f);

            while (!explodeAnim.IsDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (command != null && !spawnOnEmptyTile && command is DamageCommand damageCommand)
            {
                var targetState = vfxData.targetCards[targetIndex];

                if (targetState != null)
                {
                    var damageHistory = damageCommand.PendingDamageHistories[targetIndex];
                    targetState.ApplyHistory(damageHistory.Uid);

                    if (targetState.IsHero)
                    {
                        var heroView = GameplayScreen.Instance.heroSideBarContainer;
                        Timing.RunCoroutine(heroView.HitPortraitNonAttack(targetState.Team, damageCommand, damageHistory.Uid));
                    }
                    else if (m_targetCard != null)
                    {
                        var amount = damageHistory.DamageAmount;
                        var damageFx = m_targetCard.View.GetVfx<DamageEffectComponent, DamageEffect>();

                        if (damageFx != null)
                        {
                            Timing.RunCoroutine(damageFx.GenericDamageAnimation(amount));
                        }
                    }
                }
            }

            yield return Timing.WaitForOneFrame;

            Destroy(gameObject);
        }
    }
}