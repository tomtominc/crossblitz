using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using MEC;
using UnityEngine;
using Asyncoroutine;

namespace CrossBlitz.GameVfx.Minion
{
    public class ComboSlashEffect : MonoBehaviour, IGameVfx
    {
        public ComboSlashParticle slashPrefab;
        public void Initialize(Transform rect) { }
        public void Initialize(CardView card) { }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            gameObject.SetActive(true);

            var getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(command);
            yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
            var vfxData = getSourceAndTargetData.Result;

            for (var i = 0; i < vfxData.targetCards.Count; i++)
            {
                var slashEffect = Instantiate(slashPrefab, transform, false);
                yield return Timing.WaitUntilDone(slashEffect.Play(vfxData, command, i));
            }

            yield return Timing.WaitForOneFrame;

            Destroy(gameObject);
        }
    }
}