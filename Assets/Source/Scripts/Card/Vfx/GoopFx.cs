using UnityEngine;

namespace CrossBlitz.GameVfx
{
    public class GoopFx : MonoBehaviour
    {
        public float initialSlideSpeed=10;
        public float minDrag=0.5f;
        public float maxDrag = 2;

        public SpriteAnimation goopBubble;
        public SpriteAnimation goopSplatter;
        public bool kill;

        public float Radius { get; private set; }

        private float _slideSpeed;
        private float _drag;
        private Vector2 _velocity;
        private RectTransform _rect;

        private void Start()
        {
            _rect = this.RectTransform();
        }

        public void Splatter()
        {
            kill = false;
            Radius = 96f;
            goopSplatter.SetActive(true);
            goopSplatter.Play($"splatter-{Random.Range(1, 4)}", OnFinished);
        }

        public void Bubble()
        {
            Radius = 24f;
            goopBubble.SetActive(true);
            goopBubble.Play($"{Random.Range(1, 8)}");
        }

        public void StartSliding()
        {
            _slideSpeed = initialSlideSpeed;
            _velocity = Vector2.down;
            _drag = Random.Range(minDrag, maxDrag);
        }

        private void Update()
        {
            if (_slideSpeed > 0)
            {
                _rect.anchoredPosition += _velocity * (_slideSpeed * Time.deltaTime);
                _slideSpeed -= _drag * Time.deltaTime;
            }
        }

        private void OnFinished()
        {
            //kill = true;
            // Bubble();
            // StartSliding();
        }
    }
}