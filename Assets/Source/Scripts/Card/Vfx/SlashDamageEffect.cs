using System.Collections.Generic;
using System.Linq;
using Asyncoroutine;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI;
using DG.Tweening;
using MEC;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.GameVfx.Minion
{
    public class SlashDamageEffect : MonoBehaviour, IGameVfx
    {
        public Image overlayImage;
        public CanvasGroup cardOverlay;
        public SpriteAnimation slashEffect;

        private CardView _card;
        private GameCardView _gameCard;
        private RectTransform _cardRect;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
            _gameCard = _card.GetCardComponent<GameCardView>();
            _cardRect = _card.cardObject;
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            gameObject.SetActive(true);
            cardOverlay.alpha = 0;
            slashEffect.SetActive(false);
            overlayImage.color = Color.white;

            vfxParameters.OnApply?.Invoke();

            var damageCommand = (DamageCommand) command;
            if (damageCommand.PendingDamageHistories.Count <= 0) yield break;

            var cardList = new List<GameCardView>();

            for (var i = 0; i < damageCommand.PendingDamageHistories.Count; i++)
            {
                var damageHistory = damageCommand.PendingDamageHistories[i];
                var getBoardCardTask = GameBoard.GetBoardCard(damageHistory.CardUid);
                yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                var getBoardCardResult = getBoardCardTask.Result;

                if (getBoardCardResult.error != ClientErrorCode.None)
                {
                    Server.HandleClientError(getBoardCardResult, true);
                }

                if (getBoardCardResult.boardCard)
                {
                    cardList.Add(getBoardCardResult.boardCard);
                }
            }

            var showList = cardList.Select(card => card.View.GetCardComponent<BoardTarget>()).ToList();
            showList.Add(_card.GetCardComponent<BoardTarget>());

            GameplayScreen.Instance.ShowTargets(showList);

            cardOverlay.SetActive(true);

            yield return Timing.WaitUntilDone(cardOverlay.DOFade(0.6f, 0.08f)
                .WaitForCompletion(true));

            slashEffect.SetActive(true);
            slashEffect.Play("slash");
            AudioController.PlaySound("vfx_claw_slash", "BARDSFX", true, gameObject);

            while (slashEffect.CurrentFrame < 1) yield return Timing.WaitForOneFrame;

            overlayImage.color = "#a7bdc3".ToColor();
            cardOverlay.alpha = 0.8f;
            cardOverlay.DOFade(0, 0.5f);

            GameplayScreen.Instance.ShakeScreen();

            for (var i = 0; i < cardList.Count; i++)
            {
                var damageFx = cardList[i].View.GetVfx<DamageEffectComponent, DamageEffect>();
                Timing.RunCoroutine(damageFx.GenericDamageAnimation(damageCommand.Amount));
            }

            while (!slashEffect.IsDone) yield return Timing.WaitForOneFrame;

            GameplayScreen.Instance.HideTargets();

            gameObject.SetActive(false);
            cardOverlay.SetActive(false);
            slashEffect.SetActive(false);
        }
    }
}