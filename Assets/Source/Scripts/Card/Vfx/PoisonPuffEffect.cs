using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.GameVfx;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class PoisonPuffEffect : MonoBehaviour, IGameVfx
    {
        public PuffGrenade puffGrenadePrefab;
        public PuffProjectile puffProjectilePrefab;

        public void Initialize(CardView card) {}

        public void Initialize(Transform rect){}

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            var getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(command);
            yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
            var vfxData = getSourceAndTargetData.Result;
            var handles = new List<CoroutineHandle>();

            var commandList = GameServer.State.GetCardHistories(vfxData.sourceCard.uid);
            var damageCommands = new List<Command>();

            for (var i = 0; i < commandList.Count; i++)
            {
                var damageCommand = commandList[i];

                if (damageCommand != command)
                {
                    damageCommands.Add(damageCommand);
                    Server.CommandResolver.RemoveCommandFromResolve(damageCommand);
                }
            }

            for (var i = 0; i < vfxData.targetCards.Count; i++)
            {
                var puffGrenade = Instantiate(puffGrenadePrefab, transform, false);
                handles.Add( Timing.RunCoroutine(puffGrenade.Play(vfxData, command, i)));
            }

            // the grenades coroutine will run until I need the other puffs to summon
            while (handles.Exists(handle => handle.IsRunning)) yield return Timing.WaitForOneFrame;

            for (var i = 0; i < damageCommands.Count; i++)
            {
                var damageCommand = damageCommands[i];
                getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(damageCommand);
                yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
                var vfxDataDamage = getSourceAndTargetData.Result;
                handles = new List<CoroutineHandle>();

                vfxDataDamage.sourceCard = vfxData.targetCards[0];
                vfxDataDamage.sourceRect = vfxData.targetRects[0];
                vfxDataDamage.sourceCardObject = vfxData.targetCardObjects[0];
                vfxDataDamage.sourceCardPosition = vfxData.targetCardPositions[0];

                for (var j = 0; j < vfxDataDamage.targetCards.Count; j++)
                {
                    var puffProjectile = Instantiate(puffProjectilePrefab, transform, false);
                    handles.Add( Timing.RunCoroutine(puffProjectile.Play(vfxDataDamage,damageCommand,j, Random.Range(-0.04f, 0.08f), "purple")));
                }
            }

            while (handles.Exists(handle => handle.IsRunning)) yield return Timing.WaitForOneFrame;

            Destroy(gameObject, 2f);
        }
    }
}