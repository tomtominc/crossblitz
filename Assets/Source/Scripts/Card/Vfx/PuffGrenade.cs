using System.Collections;
using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class PuffGrenade : MonoBehaviour
    {
        public SpriteAnimation animator;
        private float m_velocity;
        private bool m_updateGravity;
        private bool m_updateExplosion;
        private Color m_explosionColor;
        private GameCardView m_targetCard;
        private Vector3 m_groundPosition;
        private const float m_gravity = 20;

        private void Update()
        {
            if (m_updateGravity)
            {
                m_velocity -= m_gravity * Time.deltaTime;
                transform.position += new Vector3(0, m_velocity) * Time.deltaTime;

                if (transform.position.y <= m_groundPosition.y)
                {
                    m_updateGravity = false;
                }
            }

            if (m_updateExplosion && m_targetCard)
            {
                switch (animator.CurrentFrame)
                {
                    case 5:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 50f/255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(-2, -2);
                        break;
                    case 6:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 100f/255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(3, 3);
                        break;
                    case 7:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 130f/255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(4, 4);
                        break;
                    case 8:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 150f/255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(3, 3);
                        break;
                    case 9:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 150f/255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(-1, -1);
                        break;
                    case 10:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 130f/255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(-2, -2);
                        break;
                    case 11:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 100f/255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(-2, -2);
                        break;
                    case 12:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 50f/255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(-1, -1);
                        break;
                    case 13:
                        m_targetCard.View.RemoveTint();
                        m_targetCard.Rect.anchoredPosition = new Vector2(0, 0);
                        break;
                }
            }
        }


        public IEnumerator<float> Play(VfxSourceAndTargetData vfxData, Command command, int targetIndex)
        {
            m_velocity = 4f;
            var targetPosition = vfxData.targetCardPositions[targetIndex];
            m_targetCard = vfxData.targetCardObjects[targetIndex];

            m_groundPosition = targetPosition + new Vector3(0, 0.5f,0);
            transform.position = m_groundPosition + new Vector3(0, .5f, 0);
            m_updateGravity = true;

            animator.Play("spawn");

            AudioController.PlaySound("vfx_transform_poof", "BARDSFX", true, gameObject).setPitch(1.25f);

            while (m_updateGravity) yield return Timing.WaitForOneFrame;

            transform.position = m_groundPosition + new Vector3(0, 0.12f, 0f);
            transform.DOMove(m_groundPosition, 0.4f).SetEase(Ease.OutBounce);

            yield return Timing.WaitForSeconds(0.4f);

            GameplayScreen.Instance.ShakeScreen();

            animator.Play("explode");

            AudioController.PlaySound("vfx_transform_poof", "BARDSFX", true, gameObject).setPitch(0.75f);

            m_explosionColor = "#d96cea".ToColor();
            m_updateExplosion = true;

            while (animator.CurrentFrame < 6) yield return Timing.WaitForOneFrame;

            GameVfxUtilities.HandleDamageCommand( command, vfxData, targetIndex, m_targetCard);

        }
    }
}