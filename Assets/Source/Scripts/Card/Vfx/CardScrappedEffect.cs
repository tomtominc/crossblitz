using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx
{
    public class CardScrappedEffect : MonoBehaviour, IGameVfx
    {
        public SpriteAnimation overlayJellyAnimator;

        private CardView _card;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            this.SetActive(true);

            AudioController.PlaySound("battle_card_discard", "BARDSFX", true, gameObject);

            _card.frontOverlayCanvas.SetActive(true);
            _card.frontOverlayCanvas.alpha = 0;

            if (_card.data.type == CardType.Minion && _card.attackTypeIconOverlayCanvas)
            {
                _card.attackTypeIconOverlayCanvas.SetActive(true);
                _card.attackTypeIconOverlayCanvas.alpha = 0;
            }

            _card.frontOverlayCanvas.DOFade(1, 0.08f);
            _card.frontOverlay.image.color = "8a488e".ToColor();

            if (_card.data.type == CardType.Minion && _card.attackTypeIconOverlayCanvas)
            {
                _card.attackTypeIconOverlayCanvas.DOFade(1, 0.08f);
                _card.attackTypeIconOverlay.color = "8a488e".ToColor();
            }

            yield return Timing.WaitForSeconds(0.24f);

            _card.cardObject.SetActive(false);

            overlayJellyAnimator.SetActive(true);
            overlayJellyAnimator.Play("scrapped");

            while (overlayJellyAnimator.IsDone == false) yield return Timing.WaitForOneFrame;

            this.SetActive(false);
            // meld screen should handle destroying this card?
        }
    }
}