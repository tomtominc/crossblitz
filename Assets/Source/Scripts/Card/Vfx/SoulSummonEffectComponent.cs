using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class SoulSummonEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<SoulSummonEffect>("SoulSummonEffect", card.transform);
            effect.Initialize(card);
        }
    }
}