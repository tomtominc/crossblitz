using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.PlayFab.Authentication;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx
{
    public class ValueEffectDisplay : MonoBehaviour
    {
        public CanvasGroup canvasGroup;
        public TMPSpriteFont valueLabel;
        public SpriteAnimation container;
        public RectTransform rectTransform;

        public ToughEffectRockParticle rockParticlePrefab;

        public IEnumerator<float> DisplayValue(string valueDisplay, string containerAnimation = "spawn", RectTransform parentRect = null)
        {
            var originalPosition = rectTransform.anchoredPosition;
            rectTransform.anchoredPosition = originalPosition + Vector2.down * 8;

            canvasGroup.alpha = 1;
            valueLabel.text = valueDisplay;
            container.Play(containerAnimation);

            if(containerAnimation == "spawn-tough")
            {
                AudioController.PlaySound("vfx_tough", "BARDSFX", true, gameObject);
                valueLabel.RectTransform().anchoredPosition = new Vector2(valueLabel.RectTransform().anchoredPosition.x, valueLabel.RectTransform().anchoredPosition.y + 4);
                SpawnToughParticles(containerAnimation, parentRect);
            }
            
            yield return Timing.WaitUntilDone( rectTransform.DOAnchorPosY(originalPosition.y, 0.5f).SetEase(Ease.OutBack).WaitForCompletion(true));
            yield return Timing.WaitForSeconds(GetDamageSpeed());
            rectTransform.DOAnchorPosY(originalPosition.y + 16, 0.5f).SetEase(Ease.InQuint);
            canvasGroup.DOFade(0, 0.5f);
            yield return Timing.WaitForSeconds(0.5f);
            Destroy(gameObject);
        }

        private void SpawnToughParticles(string containerAnimation, RectTransform parentRect)
        {

            // the spawn locations of the rock should be dictated by the parents rect (to make it easier to adjust if needed)
            var rockSpawnBounds = parentRect.sizeDelta;

            for (var i = 0; i < 3; i++)
            {
                var rockParticle = Instantiate(rockParticlePrefab, parentRect, false);
                rockParticle.SetActive(true);

                // get the spawn points, the x value min is just a little bit off from the center (so we don't spawn exactly in the center)
                // while the x value max is the very edge of the card.
                // and finally, we multiply it by the move direction so its going in the same direction as the card.
                var moveDirection = i % 2 == 0 ? 1 : -1;
                var spawnX = Random.Range(0, rockSpawnBounds.x / 2) * moveDirection;
                var spawnY = Random.Range(0, rockSpawnBounds.y);

                rockParticle.RectTransform().anchoredPosition = new Vector2(spawnX, spawnY);
                rockParticle.Play(moveDirection);
            }
        }

        public static float GetDamageSpeed()
        {
            return App.Settings.GetDamageDisplaySpeed() switch
            {
                0 => 1f,
                1 => .75f,
                2 => 0.5f,
                3 => 0.25f,
                _ => 1f
            };
        }
    }
}