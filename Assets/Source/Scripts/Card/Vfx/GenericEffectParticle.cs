using System;
using System.Collections;
using System.Collections.Generic;
using BlendModes;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class GenericEffectParticle : MonoBehaviour
    {
        public SpriteAnimation animator;
        [FoldoutGroup("Animations")] public string spawnAnimationName;
        [FoldoutGroup("Animations")] public string idleAnimationName;
        [FoldoutGroup("Animations")] public string despawnAnimationName;
        [FoldoutGroup("Effect Options")] public bool tintTargetCard;
        [FoldoutGroup("Effect Options")] [ShowIf("tintTargetCard")] public Color tintColor;
        [FoldoutGroup("Effect Options")] [ShowIf("tintTargetCard")] public string animationToTint;
        [FoldoutGroup("Effect Options")] [ShowIf("tintTargetCard")] public List<float> animationFrameToAlpha;

        protected GameCardView m_targetCard;

        protected virtual void Update()
        {
            if (tintTargetCard && m_targetCard && animator.CurrentAnimationName == animationToTint)
            {
                if (animationFrameToAlpha.Count > animator.CurrentFrame)
                {
                    var alpha = animationFrameToAlpha[animator.CurrentFrame];

                    if (alpha <= 0)
                    {
                        m_targetCard.View.RemoveTint();
                    }
                    else
                    {
                        m_targetCard.View.Tint(tintColor, BlendMode.Color, true,
                            animationFrameToAlpha[animator.CurrentFrame]);
                    }
                }
                else
                {
                    m_targetCard.View.RemoveTint();
                }
            }
        }

        public virtual IEnumerator<float> Play(VfxSourceAndTargetData vfxData, Command command, int targetIndex)
        {
            m_targetCard = vfxData.targetCardObjects[targetIndex];

            var targetPosition = vfxData.targetCardPositions[targetIndex];

            transform.position = targetPosition;

            if (!string.IsNullOrEmpty(spawnAnimationName))
            {
                animator.Play(spawnAnimationName);
                while (!animator.IsDone) yield return Timing.WaitForOneFrame;
            }

            if (!string.IsNullOrEmpty(idleAnimationName))
            {
                animator.Play(idleAnimationName);
                while (!animator.IsDone) yield return Timing.WaitForOneFrame;
            }

            // in case this is a damage command we call this after the idle is done playing.
            GameVfxUtilities.HandleDamageCommand(command, vfxData, targetIndex, vfxData.targetCardObjects[targetIndex]);

            if (!string.IsNullOrEmpty(despawnAnimationName))
            {
                animator.Play(spawnAnimationName);
                while (!animator.IsDone) yield return Timing.WaitForOneFrame;
            }
        }
    }
}