using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class GrantArmorEffect : MonoBehaviour, IGameVfx
    {
        // A public reference to the shield animator
        public SpriteAnimation shieldAnimator;
        // A reference to the card that's passed in
        private CardView _card;
        // A reference to the cards transform (cached for optimization)
        private RectTransform _cardTransform;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
            _cardTransform = card.cardObject;
        }

        public void OnAfterCreation()
        {

        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            gameObject.SetActive(true);

            shieldAnimator.Play("burst", frame =>
            {
                _cardTransform.anchoredPosition = frame switch
                {
                    0 => new Vector2(0, -1),
                    1 => new Vector2(0, -2),
                    5 => new Vector2(0, 4),
                    6 => new Vector2(0, 5),
                    10 => new Vector2(0, 4),
                    11 => new Vector2(0, 0),
                    _ => _cardTransform.anchoredPosition
                };
            });

            while (shieldAnimator.IsDone == false) yield return Timing.WaitForOneFrame;

            gameObject.SetActive(false);
        }
    }
}
