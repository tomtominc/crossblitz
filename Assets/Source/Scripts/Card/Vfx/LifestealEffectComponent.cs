using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class LifestealEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<LifestealEffect>("LifestealEffect", card.cardObject.transform);
            effect.Initialize(card);
        }
    }
}