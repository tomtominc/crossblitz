using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class WhipDamageEffect : MonoBehaviour, IGameVfx
    {
        private const int FramesBeforeWhipStart = 14;
        public float moveSpeed = 100;
        public SpriteAnimation whip;
        public SpriteAnimation hitEffect;
        private CardView _card;
        private GameCardView _gameCard;
        private GameCardView _targetBoardCard;
        private RectTransform _cardRect;
        private RectTransform _whipRect;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
            _gameCard = _card.GetCardComponent<GameCardView>();
            _cardRect = _card.cardObject;
            _whipRect = whip.RectTransform();
        }

        private void OnWhipUpdate(int frame)
        {
            switch (frame)
            {
                case 3: _cardRect.anchoredPosition = new Vector2(0, 14);
                    _whipRect.anchoredPosition = new Vector2(23, 29);
                    break;
                case 4:
                    _cardRect.anchoredPosition = new Vector2(2, 9);
                    _whipRect.anchoredPosition = new Vector2(23, 29);
                    break;
                case 5:
                    _cardRect.anchoredPosition = new Vector2(1, 10);
                    break;
                case 6:
                    _cardRect.anchoredPosition = new Vector2(0, 12);
                    break;
                case 7:
                    _cardRect.anchoredPosition = new Vector2(-1, 14);
                    break;
                case 8:
                    _cardRect.anchoredPosition = new Vector2(-1, 12);
                    break;
            }
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            gameObject.SetActive(true);
            whip.SetActive(false);
            hitEffect.SetActive(false);

            _card.SetSortingLayer("Particles");
            _gameCard.SetSortingOrder(GameCardView.HandSortingOrder);

            _targetBoardCard = null;

            vfxParams.OnApply?.Invoke();

            var damageCommand = (DamageCommand) command;
            if (damageCommand.PendingDamageHistories.Count <= 0) yield break;

            var damageHistory = damageCommand.PendingDamageHistories[0];
            var getBoardCardTask = GameBoard.GetBoardCard(damageHistory.CardUid);
            yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
            var getBoardCardResult = getBoardCardTask.Result;

            if (getBoardCardResult.error != ClientErrorCode.None)
            {
                Server.HandleClientError(getBoardCardResult, true);
            }

            if (getBoardCardResult.boardCard)
            {
                _targetBoardCard = getBoardCardResult.boardCard;
            }

            if (_targetBoardCard == null) yield break;

            var genericEffect = _card.GetVfx<GenericEffectComponent, GenericAbilityEffect>();
            Timing.RunCoroutine(genericEffect.Play(damageCommand,null));

            GameplayScreen.Instance.ShowTargets(new List<BoardTarget>
            {
                _card.GetCardComponent<BoardTarget>(),
                _targetBoardCard.View.GetCardComponent<BoardTarget>()
            });

            var offsetFromTargetCard = new Vector2(-58, 11);
            var originalParent = _card.transform.parent;

            _card.transform.SetParent(_targetBoardCard.transform.parent, true);
            _card.RectTransform().DOAnchorPos(offsetFromTargetCard, moveSpeed)
                .SetSpeedBased(true);

            for (var frame = 0; frame < FramesBeforeWhipStart; frame++)
            {
                switch (frame)
                {
                    case 0: _cardRect.anchoredPosition = new Vector2(0,-2);
                        break;
                    case 1: _cardRect.anchoredPosition = new Vector2(0,-4);
                        break;
                    case 2:
                    case 3:
                    case 4:
                        _cardRect.anchoredPosition = new Vector2(0,-5);
                        break;
                    case 5: _cardRect.anchoredPosition = new Vector2(0,-8);
                        break;
                    case 6: _cardRect.anchoredPosition = new Vector2(0,2);
                        break;
                    case 7: _cardRect.anchoredPosition = new Vector2(0,6);
                        whip.SetActive(true);
                        whip.Play("whip-idle");
                        _whipRect.anchoredPosition = new Vector2(23, -2f);
                        break;
                    case 8: _cardRect.anchoredPosition = new Vector2(0,8);
                        _whipRect.anchoredPosition = new Vector2(23, 6f);
                        break;
                    case 9: _cardRect.anchoredPosition = new Vector2(0,9);
                        _whipRect.anchoredPosition = new Vector2(23, 17);
                        break;
                    case 10: _cardRect.anchoredPosition = new Vector2(0,11);
                        _whipRect.anchoredPosition = new Vector2(23, 32);
                        break;
                    case 11: _cardRect.anchoredPosition = new Vector2(0,11);
                        _whipRect.anchoredPosition = new Vector2(23, 27);
                        break;
                    case 12: _cardRect.anchoredPosition = new Vector2(0,11);
                        _whipRect.anchoredPosition = new Vector2(23, 30);
                        break;
                    case 13:_cardRect.anchoredPosition = new Vector2(0,12);
                        _whipRect.anchoredPosition = new Vector2(23, 29);
                        break;
                }

                if (frame == 12) yield return Timing.WaitForSeconds(0.3f);
                else yield return Timing.WaitForSeconds(0.04f);
            }

            whip.Play("whip", OnWhipUpdate);

            while (whip.CurrentFrame < 4) yield return Timing.WaitForOneFrame;


            hitEffect.SetActive(true);
            hitEffect.Play("hit-fx");
            AudioController.PlaySound("Battle_WhipAttack_NoWhoosh", "SFX", true, gameObject);

            GameplayScreen.Instance.ShakeScreen();

            var damageFx = _targetBoardCard.View.GetVfx<DamageEffectComponent, DamageEffect>();
            Timing.RunCoroutine(damageFx.GenericDamageAnimation(damageCommand.Amount));

            while (whip.CurrentFrame < 8) yield return Timing.WaitForOneFrame;

            _card.transform.SetParent(originalParent, true);
            GameplayScreen.Instance.HideTargets();

            yield return Timing.WaitUntilDone(_card.RectTransform().DOAnchorPos(Vector2.zero, moveSpeed)
                .SetSpeedBased(true).WaitForCompletion(true));

            _cardRect.DOAnchorPos(Vector2.zero, 0.25f).SetEase(Ease.OutBack);

            while (!whip.IsDone) yield return Timing.WaitForOneFrame;

            whip.SetActive(false);
            hitEffect.SetActive(false);
            gameObject.SetActive(false);

            _card.SetSortingLayer("Default");
            _gameCard.SetSortingOrder(GameCardView.BoardSortingOrder);
        }
    }
}