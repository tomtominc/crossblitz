using System.Collections.Generic;
using Asyncoroutine;
using BlendModes;
using CrossBlitz.AssetManagement;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class BiteEffectParticle : GenericEffectParticle
    {
        public override IEnumerator<float> Play(VfxSourceAndTargetData vfxData, Command command, int targetIndex)
        {
            animator.SetActive(false);

            var targetCard = vfxData.targetCards[targetIndex];
            var location = targetCard.location == CardLocation.Graveyard
                ? targetCard.lastLocation
                : targetCard.location;

            if (location == CardLocation.Deck)
            {
                yield return Timing.WaitUntilDone(BiteCardInDeck(targetCard));
            }
            else if (location == CardLocation.Hand)
            {
                yield return Timing.WaitUntilDone(BiteCardInHand(targetCard));
            }
            else if (location == CardLocation.Board)
            {
                yield return Timing.WaitUntilDone(BiteCardOnBoard(vfxData, command, targetIndex));
            }
        }

        private IEnumerator<float> BiteCardOnBoard(VfxSourceAndTargetData vfxData, Command command, int targetIndex)
        {
            var targetPosition = vfxData.targetCardPositions[targetIndex];
            var targetRect = vfxData.targetRects[targetIndex];
            var targetCard = vfxData.targetCardObjects[targetIndex];
            var targetCardState = vfxData.targetCards[targetIndex];

            transform.position = targetPosition;
            animator.SetActive(true);
            animator.Play("chomp");

            while (animator.CurrentFrame < 6) yield return Timing.WaitForOneFrame;

            AudioController.PlaySound("vfx_claw_slash", "BARDSFX", true, gameObject);

            GameplayScreen.Instance.ShakeScreen();

            if (command is DamageCommand)
            {
                GameVfxUtilities.HandleDamageCommand(command, vfxData, targetIndex, targetCard, false);
            }
            else if (command is DestroyCommand)
            {
                Timing.RunCoroutine(DeathEffect.EvaluateDeath(targetCardState , targetCard.View));
            }
        }

        private IEnumerator<float> BiteCardInHand(GameCardState card)
        {
            if (card.Team == GameServer.LocalPlayerTeam)
            {
                var localHandView = GameplayScreen.Instance.handView;
                var handCard = GameplayScreen.Instance.GetHandCard(card.uid);

                if (handCard == null)
                {
                    Debug.LogError($"Couldn't find hand card. {card.characterData.id}");
                    yield break;
                }

                handCard.UpdateViewFromState();
                handCard.transform.DOLocalMoveY( handCard.transform.localPosition.y + 32f, 0.24f);

                var getWorldVfxTask = GameVfxProvider.GetWorldVfx("UseCardEffect");
                yield return Timing.WaitUntilDone(getWorldVfxTask.AsCoroutine());
                var useCardEffect = (UseHandCardEffect)getWorldVfxTask.Result;
                useCardEffect.Hide();

                yield return Timing.WaitForSeconds(0.32f);

                transform.position = handCard.transform.position;
                animator.SetActive(true);
                animator.Play("chomp");

                while (animator.CurrentFrame < 6) yield return Timing.WaitForOneFrame;

                AudioController.PlaySound("vfx_claw_slash", "BARDSFX", true, gameObject);

                GameplayScreen.Instance.ShakeScreen();
                handCard.View.Tint("e42f2f".ToColor(), BlendMode.Color, true);

                while (animator.CurrentFrame < 7) yield return Timing.WaitForOneFrame;

                Timing.RunCoroutine(useCardEffect.PlayEffect(card, handCard.View.transform.position));

                localHandView.RemoveCard(handCard, null);
                Destroy(handCard.gameObject);
            }
            else
            {
                var remoteHandView = GameplayScreen.Instance.opponentHandView;
                remoteHandView.Show();

                while (remoteHandView.IsTransitioning) yield return Timing.WaitForOneFrame;
                yield return Timing.WaitForSeconds(0.25f);

                var handCard = remoteHandView.GetCard(card.uid);
                yield return Timing.WaitUntilDone(handCard.Discard(true, true));

                var getWorldVfxTask = GameVfxProvider.GetWorldVfx("UseCardEffect");
                yield return Timing.WaitUntilDone(getWorldVfxTask.AsCoroutine());
                var useCardEffect = (UseHandCardEffect)getWorldVfxTask.Result;
                useCardEffect.Hide();

                yield return Timing.WaitForSeconds(0.24f);

                var handCardView = handCard.Card;
                transform.position = handCardView.transform.position;
                animator.SetActive(true);
                animator.Play("chomp");

                while (animator.CurrentFrame < 6) yield return Timing.WaitForOneFrame;

                AudioController.PlaySound("vfx_claw_slash", "BARDSFX", true, gameObject);

                GameplayScreen.Instance.ShakeScreen();
                handCardView.View.Tint("e42f2f".ToColor(), BlendMode.Color, true);

                while (animator.CurrentFrame < 7) yield return Timing.WaitForOneFrame;

                Timing.RunCoroutine(useCardEffect.PlayEffect(card, handCardView.View.transform.position));

                remoteHandView.RemoveCard(card.uid);
            }
        }

        private IEnumerator<float> BiteCardInDeck(GameCardState card)
        {
            var deck = GameplayScreen.Instance.GetDeck(card.Team);
            var reference = AddressableReferenceLoader.GetAsset("Card_Standard", true);

            yield return Timing.WaitUntilDone(reference.AsCoroutine());

            var settings = new CreateCardFactorySettings
            {
                CardPrefab = reference.Result,
                Layout = transform.parent.RectTransform(),
                AdditionalComponents = CardView.GetRequiredComponentsForStandardHandCard(),
                StartsDisabled = true
            };

            var cardState = GameServer.State.GetCard(card.uid);

            settings.CardState = cardState;
            settings.SortingOrder = GameCardView.HandSortingOrder + 20;

            var standardCardView = CardFactory.Create(settings, null);
            var gameCardView = standardCardView.GetCardComponent<GameCardView>();

            standardCardView.SetInteractable(false);
            standardCardView.cardObject.SetActive(false);
            standardCardView.cardBackObject.SetActive(true);

            var dragRotator = standardCardView.GetCardComponent<DragRotator>();
            dragRotator.enabled = false;

            var tc = deck.GetTopCard();

            standardCardView.transform.position = tc.transform.position;
            standardCardView.transform.localEulerAngles = Vector3.zero;
            standardCardView.SetActive(true);
            standardCardView.SetGameState(cardState);

            deck.RefreshDeckList();

            yield return Timing.WaitForSeconds(.5f);

            standardCardView.transform.DOMove(standardCardView.transform.position + new Vector3(0,3,0), .5f).SetEase(Ease.OutBack);

            AudioController.PlaySound("battle_card_draw", "BARDSFX", true, standardCardView.gameObject);

            var getWorldVfxTask = GameVfxProvider.GetWorldVfx("UseCardEffect");
            yield return Timing.WaitUntilDone(getWorldVfxTask.AsCoroutine());
            var useCardEffect = (UseHandCardEffect)getWorldVfxTask.Result;
            useCardEffect.Hide();

            yield return Timing.WaitForSeconds(.5f);

            yield return Timing.WaitUntilDone(gameCardView.Flip());
            gameCardView.UpdateViewFromState();

            yield return Timing.WaitForSeconds(.5f);

            transform.position = standardCardView.transform.position;
            animator.SetActive(true);
            animator.Play("chomp");

            while (animator.CurrentFrame < 6) yield return Timing.WaitForOneFrame;

            AudioController.PlaySound("vfx_claw_slash", "BARDSFX", true, gameObject);
            GameplayScreen.Instance.ShakeScreen();
            standardCardView.Tint("e42f2f".ToColor(), BlendMode.Color, true);

            while (animator.CurrentFrame < 7) yield return Timing.WaitForOneFrame;

            standardCardView.cardObject.SetActive(false);
            Timing.RunCoroutine(useCardEffect.PlayEffect(gameCardView.State, gameCardView.transform.position) );
        }
    }
}