using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    // todo: transfer the summon effect to this, it's still using the old system (CardEffect)
    public class SummonEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<SummonEffect>("SummonEffect", card.transform);
            effect.Initialize(card);
        }
    }
}