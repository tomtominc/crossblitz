using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class OvergrowSummonEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<OvergrowSummonEffect>("OvergrowSummonEffect", card.transform);
            effect.Initialize(card);
        }
    }
}