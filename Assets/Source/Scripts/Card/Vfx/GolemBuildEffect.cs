using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class GolemBuildEffect : MonoBehaviour, IGameVfx
    {
        public SpriteAnimation animator;
        public void Initialize(CardView card)
        {

        }

        public void Initialize(Transform rect)
        {

        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            gameObject.SetActive(true);
            animator.SetActive(true);
            animator.Play("spawn");
            AudioController.PlaySound("vfx_building", "BARDSFX",true, gameObject);

            while (!animator.IsDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            gameObject.SetActive(false);
        }
    }
}