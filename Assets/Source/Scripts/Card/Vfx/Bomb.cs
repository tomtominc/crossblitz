using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class Bomb : MonoBehaviour
    {
        public RectTransform shadow;
        public RectTransform bomb;
        public SpriteAnimation bombAnimator;

        private bool m_drop;
        private bool m_explode;
        private float m_explodeTicks;
        private Vector2 m_targetPosition;

        private const float MaxDist = 400;
        private const float Speed = 800;
        private const float PreExplodeTicks = 0.24f;

        private string m_targetCardId;
        private string m_pendingDamageHistoryUid;
        private DamageCommand m_damageCommand;

        private void Update()
        {
            var distance = bomb.anchoredPosition.y;
            shadow.localScale = new Vector3(1 - (distance / MaxDist), 1 - (distance / MaxDist), 1);

            if (m_drop)
            {
                bomb.anchoredPosition += Vector2.down  * Speed * Time.deltaTime;

                if (Vector2.Distance(bomb.anchoredPosition, m_targetPosition) <= Speed * Time.deltaTime)
                {
                    bomb.anchoredPosition = m_targetPosition + new Vector2(0, 4f);
                    bomb.DOAnchorPosY(m_targetPosition.y, 0.4f).SetEase(Ease.OutBounce);

                    m_drop = false;
                    m_explode = true;
                    m_explodeTicks = 0;
                    bombAnimator.Play("pre-explode");
                }
            }

            if (m_explode)
            {
                m_explodeTicks += Time.deltaTime;

                if (m_explodeTicks >= PreExplodeTicks)
                {
                    m_explode = false;
                    m_explodeTicks = 0;
                    bombAnimator.Play("explosion", OnExplodeFinished);
                    shadow.SetActive(false);

                    AudioController.PlaySound("vfx_fiery_explosion", "BARDSFX", true, gameObject);
                    Timing.RunCoroutine( DealDamage() );
                }
            }
        }

        public void SetBombDropTarget(RectTransform rect, string targetCardId, string pendingDamageHistoryUid, DamageCommand damageCommand)
        {
            transform.SetParent( rect, false);
            bombAnimator.Play("idle");
            const float targetSize = 8;
            m_targetPosition = new Vector2(Random.Range(-targetSize, targetSize), Random.Range(-targetSize, targetSize));
            bomb.anchoredPosition = m_targetPosition + new Vector2(0, 1000);

            m_targetCardId = targetCardId;
            m_pendingDamageHistoryUid = pendingDamageHistoryUid;
            m_damageCommand = damageCommand;
        }

        public void Drop()
        {
            AudioController.PlaySound("vfx_lob", "BARDSFX", true, gameObject);
            m_drop = true;
        }

        private IEnumerator<float> DealDamage()
        {
            var card = GameServer.State.GetCard(m_targetCardId);

            if (card.IsHero)
            {
                card.ApplyHistory(m_pendingDamageHistoryUid);
                Timing.RunCoroutine(GameplayScreen.Instance.heroSideBarContainer.HitPortraitNonAttack(card.Team, m_damageCommand, m_pendingDamageHistoryUid ));
                yield break;
            }

            var getBoardCardTask = GameBoard.GetBoardCard(card.uid);
            yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
            var getBoardCardResult = getBoardCardTask.Result;

            if (getBoardCardResult.error != ClientErrorCode.None)
            {
                Server.HandleClientError(getBoardCardResult, true);
            }

            if (getBoardCardResult.boardCard)
            {
                var damageFx = getBoardCardResult.boardCard.View.GetVfx<DamageEffectComponent, DamageEffect>();
                var history = card.GetHistory(m_pendingDamageHistoryUid);
                Timing.RunCoroutine(damageFx.GenericDamageAnimation(history.DamageAmount));
            }
        }

        private void OnExplodeFinished()
        {
            if (gameObject) Destroy(gameObject);
        }
    }
}