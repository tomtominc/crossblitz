using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class ToughEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<ToughEffect>("ToughEffect", card.front.transform);
            effect.Initialize(card);
        }
    }
}