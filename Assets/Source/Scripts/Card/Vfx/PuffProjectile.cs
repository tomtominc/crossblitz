using System;
using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using DG.Tweening;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.Card.Vfx
{
    public class PuffProjectile : MonoBehaviour
    {
        public SpriteAnimation animator;
        public GameObject trailParticlesColor;
        public GameObject trailParticlesPoison;
        public SpriteAnimation spawnFx;
        public SpriteAnimation transformPoofAnimator;

        private GameCardView m_targetCard;
        private bool m_updateExplosion;
        private Color m_explosionColor;
        private GameObject m_particles;
        private Vector3 m_lastPosition;


        // private const float m_gravity = 10;
        // private const float JumpTimePerMeter = 0.5f;
        // private Vector3 m_midPoint;
        // private float m_maxDistance;
        // private float m_minYPosition;
        // private float m_velocity;
        // private bool m_updateGravity;
        // private bool m_updateJump;

        private void FixedUpdate()
        {
            if (m_updateExplosion && m_targetCard)
            {
                switch (animator.CurrentFrame)
                {
                    case 1:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 50f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(-2, -2);
                        break;
                    case 2:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 100f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(3, 3);
                        break;
                    case 3:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 130f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(4, 4);
                        break;
                    case 4:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 150f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(3, 3);
                        break;
                    case 5:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 150f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(-1, -1);
                        break;
                    case 6:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 130f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(-2, -2);
                        break;
                    case 7:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 100f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(-2, -2);
                        break;
                    case 8:
                        m_targetCard.View.Tint(m_explosionColor, BlendMode.Color, true, 50f / 255f);
                        m_targetCard.Rect.anchoredPosition = new Vector2(-1, -1);
                        break;
                    case 9:
                        m_targetCard.View.RemoveTint();
                        m_targetCard.Rect.anchoredPosition = new Vector2(0, 0);
                        break;
                }
            }

            if (m_particles != null)
            {
                var direction = (m_lastPosition - transform.position).normalized;
                m_particles.transform.localPosition = 10 * direction;
                m_lastPosition = transform.position;
            }
        }

        public IEnumerator<float> Play(VfxSourceAndTargetData vfxData, Command command,
            int targetIndex, float delay, string color, float randomOffset = 0,
            bool parabolaMovement = false, bool transformInsideEffect = false)
        {
            animator.SetActive(false);

            if (delay > 0) yield return Timing.WaitForSeconds(delay);

            var startPosition = vfxData.sourceCardPosition;
            var targetPosition = vfxData.targetCardPositions[targetIndex];
            var dir = (targetPosition - startPosition).normalized;
            var distance = Vector3.Distance(startPosition, targetPosition);

            animator.SetActive(true);
            m_targetCard = vfxData.targetCardObjects[targetIndex];

            var startOffset = parabolaMovement ? new Vector3(Random.Range(-1f,1f), Random.Range(0f,2f)) : dir + Vector3.up;
            transform.position = startPosition + startOffset;

            if (string.IsNullOrEmpty(color))
            {
                var rand = Random.Range(0, 3);
                color = rand == 0 ? "red" : rand == 1 ? "green" : "yellow";
            }

            var purple = color == "purple";
            var appendExplosion = purple ? "-purple" : string.Empty;
            var isFlipped = dir.x <= 0;
            const float height = 3;

            animator.Play($"spawn-{color}");
            animator.transform.localScale = new Vector3(isFlipped ? -1 : 1, 1, 1);

            spawnFx.SetActive(true);
            spawnFx.Play("spawn");
            spawnFx.transform.SetParent( transform.parent, true);

            if (!parabolaMovement)
            {
                animator.transform.localPosition = new Vector3(0, height, 0);
            }

            while (!animator.IsDone) yield return Timing.WaitForOneFrame;

            m_lastPosition = transform.position;
            m_particles = purple ? trailParticlesPoison : trailParticlesColor;
            m_particles.SetActive(true);

            animator.Play($"idle-{color}");

            AudioController.PlaySound("vfx_lob", "BARDSFX", true, gameObject);

            if (parabolaMovement)
            {
                yield return Timing.WaitUntilDone(GameVfxUtilities.MoveInParabola(transform, targetPosition, 1, 10, false));
            }
            else
            {
                while (Vector3.Distance(transform.position, targetPosition) > 0.1f)
                {
                    var distToGoal = Vector3.Distance(transform.position, targetPosition) / distance;
                    transform.position = Vector3.MoveTowards(transform.position, targetPosition, 5 * Time.deltaTime);
                    animator.transform.localPosition = new Vector3(0, height * distToGoal, 0);
                    yield return Timing.WaitForOneFrame;
                }
            }

            var particleSystems = m_particles.GetComponentsInChildren<ParticleSystem>();

            for (var i = 0; i < particleSystems.Length; i++)
            {
                particleSystems[i].Stop(true, ParticleSystemStopBehavior.StopEmitting);
            }

            transform.position = targetPosition;
            animator.transform.localPosition = Vector3.zero;

            GameplayScreen.Instance.ShakeScreen(chromaticAberration: true);

            animator.Play($"explode{appendExplosion}");

            AudioController.PlaySound("vfx_transform_poof", "BARDSFX", true, gameObject);

            // m_updateJump = false;
            m_explosionColor = purple ? "#d96cea".ToColor() : "#54b697".ToColor();
            m_updateExplosion = true;

            transform.localScale = Vector3.one;

            GameVfxUtilities.HandleDamageCommand( command, vfxData, targetIndex, m_targetCard);

            if (transformInsideEffect && vfxData.targetCards[targetIndex] != null && command is TransformCardsCommand transformCardsCommand)
            {
                //Debug.LogError($"Command Target = {command.TargetUids[targetIndex]} Vfx Target = {vfxData.targetCards[targetIndex].uid}");
                vfxData.targetCards[targetIndex].ExecutePendingTransformation( transformCardsCommand.TransformPendingHistories[targetIndex], transformCardsCommand.TransformCardData.keepStatsAndUpgradesOfPreviousMinion );

                transformPoofAnimator.SetActive(true);
                transformPoofAnimator.Play("poof");

                while (transformPoofAnimator.CurrentFrame < 5) yield return Timing.WaitForOneFrame;

                GameplayScreen.Instance.ShakeScreen(4, 1);

                if (m_targetCard != null)
                {
                    m_targetCard.View.SetCardData(vfxData.targetCards[targetIndex].data);
                    m_targetCard.UpdateViewFromState();

                    yield return Timing.WaitForOneFrame;
                    yield return Timing.WaitUntilDone(m_targetCard.TransformBoardCard());
                }

                while (!transformPoofAnimator.IsDone) yield return Timing.WaitForOneFrame;
            }
        }
    }
}