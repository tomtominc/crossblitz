using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class FocusEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            switch (card.viewType)
            {
                case CardView.CardViewType.STANDARD:
                    effect = await LoadEffect<FocusEffect>("FocusEffectStandard", card.transform);
                    effect.Initialize(card);
                    break;
                case CardView.CardViewType.ZOOMED:
                    effect = await LoadEffect<FocusEffect>("FocusEffectZoomed", card.transform);
                    effect.Initialize(card);
                    break;
            }
        }
    }
}