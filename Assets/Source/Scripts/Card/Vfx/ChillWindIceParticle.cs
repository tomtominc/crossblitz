using System;
using System.Collections;
using System.Collections.Generic;
using Asyncoroutine;
using BlendModes;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.Transition;
using DG.Tweening;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.GameVfx.Spell
{
    public class ChillWindIceParticle : MonoBehaviour
    {

        public RectTransform rect;
        public SpriteAnimation anim;

        private bool _move;
        private float _moveAccel;
        private float _moveSpeed;
        private Vector2 _moveDirection;


        public void Update()
        {
            if (_move)
            {
                var pos = rect.anchoredPosition;
                pos += _moveDirection.normalized * _moveSpeed;
                _moveAccel += (0.75f * Time.deltaTime);
                _moveSpeed += _moveAccel;
                rect.anchoredPosition = pos;

                //if (_moveSpeed <= 0.75f)
                //{
                //    _moveSpeed = 0.75f;
                //}
            }
        }

        public void SetupParticle()
        {
            _move = false;
            _moveAccel = 0.0f;
            _moveSpeed = 0.0f;
            _moveDirection = Vector2.right;

            gameObject.SetActive(true);

            Timing.RunCoroutine(Play());
        }

        public IEnumerator<float> Play()
        {
            var randomInt = Random.Range(1, 3);

            _move = true;

            anim.Play("particle-" + randomInt.ToString());
            while (!anim.IsDone) yield return Timing.WaitForOneFrame;

            Destroy(gameObject);
        }
    }
}