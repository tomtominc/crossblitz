using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class WarCryEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            Debug.Log("INSIDE COMPONENET SIGN HERE");
            effect = await LoadEffect<WarCryEffect>("WarCryEffect", card.transform);
            effect.Initialize(card);
        }
    }
}