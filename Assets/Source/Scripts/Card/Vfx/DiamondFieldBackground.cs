using System;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using DG.Tweening;
using TakoBoyStudios.Events;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.GameVfx
{
    public class DiamondFieldBackground : MonoBehaviour
    {
        public bool isClient;
        public Image diamondPattern;

        private void Start()
        {
            Events.Subscribe(EventType.OnTurnStart, OnTurnStart);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnTurnStart, OnTurnStart);
        }

        private void OnTurnStart(IMessage message)
        {
            if (message.Data is OnTurnStartEventArgs args)
            {
                diamondPattern.material.DOFade(args.isClient == isClient ? 0.2f : 0, 0.48f);
            }
        }
    }
}