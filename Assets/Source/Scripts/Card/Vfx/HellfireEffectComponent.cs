using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class HellfireEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<HellfireEffect>("HellfireEffect", card.transform);
            effect.Initialize(card);
        }
    }
}