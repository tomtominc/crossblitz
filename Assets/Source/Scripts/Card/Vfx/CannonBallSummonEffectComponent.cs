using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class CannonBallSummonEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<CannonBallSummonEffect>("CannonBallSummonEffect", card.cardObject);
            effect.Initialize(card);
        }
    }
}