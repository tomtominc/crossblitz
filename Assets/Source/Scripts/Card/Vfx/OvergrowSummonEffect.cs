using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class OvergrowSummonEffect : MonoBehaviour, IGameVfx
    {
        public SpriteAnimation summonEffect;

        private RectTransform _rect;
        public void Initialize(Transform rect)
        {
            _rect = rect as RectTransform;
        }
        public void Initialize(CardView card)
        {

        }

        public void OnAfterCreation()
        {
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            gameObject.SetActive(true);

            summonEffect.Play("spawn", () =>
            {
                gameObject.SetActive(false);
            });

            AudioController.PlaySound("vfx_overgrow_plant", "BARDSFX", true, gameObject);

            while (summonEffect.CurrentFrame < 27) yield return Timing.WaitForOneFrame;

            GameplayScreen.Instance.ShakeScreen();
        }
    }
}