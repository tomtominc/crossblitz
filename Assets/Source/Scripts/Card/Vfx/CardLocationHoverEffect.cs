using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.CardCollection;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.Databases;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.GameVfx;
using CrossBlitz.InputAPI;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI.GameLogic;
using DG.Tweening;
using Source.Scripts.Card;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;


public class CardLocationHoverEffect : MonoBehaviour
{
    public RectTransform toolTipRect;
    public CanvasGroup toolTipCanvas;
    public TextMeshProUGUI chapterLabel;
    public TextMeshProUGUI locationLabel;
    public TextMeshProUGUI hintLabel;
    public SpriteAnimation typeIcon;

    //public CardData cardData;

    public string cardId;

    public void SetupCard(CardData cardData)
    {
        this.SetActive(true);

        cardId = cardData.id;

        var cardLocationIcon = Db.CardPoolsDatabase.GetCardLocationIcon(cardId);
        var cardLocationChapter = Db.CardPoolsDatabase.GetCardLocationChapter(cardId).ToUpper();
        var cardLocation = Db.CardPoolsDatabase.GetCardLocation(cardId).ToUpper();
        var cardLocationHint = Db.CardPoolsDatabase.GetCardLocationHint(cardId).ToUpper();

        typeIcon.Play(cardLocationIcon);
        chapterLabel.text = cardLocationChapter;
        locationLabel.text = cardLocation;
        hintLabel.text = cardLocationHint;
    }


    public void Fade(float alpha, float duration, float delay = 0, TweenCallback callback = null)
    {
        DOTween.Kill(toolTipCanvas);

        toolTipCanvas.DOFade(alpha, duration)
            .SetDelay(delay)
            .OnComplete(callback);
    }
}
