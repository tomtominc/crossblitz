using System.Collections.Generic;
using System.Linq.Expressions;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class FlyingEffect : MonoBehaviour, IGameVfx
    {
        public SpriteAnimation liftOffAnimator;
        public SpriteAnimation windGustAnimator;

        private CardView _card;
        private GameCardView _gameCard;
        private bool _fly;
        private float _timer;
        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
            _gameCard = _card.GetCardComponent<GameCardView>();
            _gameCard.Events.OnSilenced += OnSilenced;
        }

        public void StartFlying()
        {
            _fly = true;
            _timer = 0;
        }

        public void StopFlying()
        {
            _fly = false;
        }

        public void KillFlying()
        {
            _fly = false;
            _card.cardObject.anchoredPosition = Vector2.zero;
            _card.frontShadowRect.anchoredPosition = new Vector2(2, -2);
            gameObject.SetActive(false);
        }

        private void OnSilenced(GameCardView view)
        {
            KillFlying();
        }

        private void Update()
        {
            if (_fly)
            {
                _timer += Time.deltaTime;
                var t = Mathf.PingPong(_timer * .5f, 1);
                var offset = 1 + t * 5f;
                _card.cardObject.anchoredPosition = new Vector2(0,offset);
                _card.frontShadowRect.anchoredPosition = new Vector2(offset/2+2, -offset - offset/2 - 2);
            }
        }

        public IEnumerator<float> Spawn()
        {
            gameObject.SetActive(true);
            var cardRect = _card.cardObject;
            cardRect.DOKill();
            cardRect.DOAnchorPosY(-5, 0.2f);
            yield return Timing.WaitForSeconds(0.2f);
            cardRect.DOAnchorPosY(6, 0.2f);
            AudioController.PlaySound("vfx_wind", "BARDSFX", true, gameObject);
            yield return Timing.WaitForSeconds(0.2f);
            liftOffAnimator.SetActive(true);
            liftOffAnimator.Play("lift-off");
            yield return Timing.WaitForSeconds(0.04f);
            cardRect.DOAnchorPosY(1, 0.24f).SetEase(Ease.OutBack);
            while (!liftOffAnimator.IsDone) yield return Timing.WaitForOneFrame;

            liftOffAnimator.SetActive(false);
            windGustAnimator.SetActive(true);
            windGustAnimator.Play("idle");
            StartFlying();
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            throw new System.NotImplementedException();
        }
    }
}