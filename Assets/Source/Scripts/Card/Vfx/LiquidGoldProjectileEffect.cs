using System;
using System.Collections;
using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AssetManagement;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using DG.Tweening;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.GameVfx.Spell
{
    public class LiquidGoldProjectileEffect : MonoBehaviour, IGameVfx
    {
        private const float Inset = -50f;
        private const float MaxTime = 0.5f;

        public SpriteAnimation projectileAnim;
        public CanvasGroup goldCanvas;

        private RectTransform _projectileRect;
        private BladeBombBarrelEffect _parent;

        private bool _finished;
        private float _bounceCount;
        private float _currentTime;

        private ModifyManaCommand _command;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {

        }

        public void Update()
        {
            if (projectileAnim.IsDone) Destroy(gameObject);
        }

        public void Setup(float xScale, RectTransform manaPipRectTransform)
        {
            gameObject.RectTransform().position = manaPipRectTransform.RectTransform().position + new Vector3 (0f,0.75f,0f);
            gameObject.RectTransform().localScale = new Vector3(xScale, gameObject.RectTransform().localScale.y, gameObject.RectTransform().localScale.z);
            gameObject.SetActive(true);
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            _command = command as ModifyManaCommand;

            _projectileRect = projectileAnim.RectTransform();
            _projectileRect.anchoredPosition = Vector2.zero;

            projectileAnim.Play("gain-mana");

            while (projectileAnim.CurrentFrame < 14) yield return Timing.WaitForOneFrame;
            //_command.UpdateManaValues(true);
            AudioController.PlaySound("TMP-Key_Pickup", "SFX", true, gameObject);

            while (projectileAnim.CurrentFrame < 15) yield return Timing.WaitForOneFrame;
        }


        public void Unload()
        {
            AddressableReferenceLoader.Unload(gameObject);
        }
    }
}