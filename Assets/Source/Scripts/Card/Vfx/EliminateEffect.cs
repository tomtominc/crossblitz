using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Spell
{
    public class EliminateEffect : MonoBehaviour, IGameVfx
    {
        public CanvasGroup canvas;
        public RectTransform gash;
        public SpriteAnimation sliceAnimator;
        public CanvasGroup overlayCanvas;

        private GameCardView _card;
        private RectTransform _cardTransform;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            var destroyCommand = (DamageCommand) command;

            if (destroyCommand.TargetUids.Count == 0 || destroyCommand.TargetUids[0] == null)
            {
                Debug.LogError("There are no targets for the Eliminate Effect!");
                yield break;
            }

            var targetUid = destroyCommand.TargetUids[0];
            var getBoardCardTask = GameBoard.GetBoardCard(targetUid);
            yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
            var getBoardCardResult = getBoardCardTask.Result;

            if (getBoardCardResult.error != ClientErrorCode.None)
            {
                Server.HandleClientError(getBoardCardResult, true);
                yield break;
            }


            _card = getBoardCardResult.boardCard;
            _card.UpdateViewFromState();
            _cardTransform = _card.View.cardObject;

            gash.SetActive(false);
            gash.SetParent(_cardTransform, false);
            gash.anchoredPosition = Vector2.zero;

            overlayCanvas.alpha = 0;
            sliceAnimator.Play("slash", frame =>
            {
                _cardTransform.anchoredPosition = frame switch
                {
                    0 => new Vector2(0, 2),
                    1 => new Vector2(-1, -4),
                    2 => new Vector2(2, 4),
                    3 => new Vector2(-3, -2),
                    4 => new Vector2(1, 2),
                    5 => new Vector2(1, -1),
                    6 => new Vector2(0, 1),
                    7 => new Vector2(0, 0),
                    _ => _cardTransform.anchoredPosition
                };

                if (frame == 1)
                {
                    overlayCanvas.alpha = 1;
                    overlayCanvas.DOFade(0, 0.16f);
                }
                else if (frame == 2)
                {
                    gash.SetActive(true);
                }
            });

            while (sliceAnimator.IsDone == false) yield return Timing.WaitForOneFrame;
            yield return Timing.WaitUntilDone(canvas.DOFade(0, 0.125f).WaitForCompletion(true));

            if (_card.State.HasTrigger(TriggerType.DEATHRATTLE))
            {
                yield return Timing.WaitUntilDone(DeathEffect.EvaluateDeath(_card.State,_card.View));
            }
            else
            {
                Timing.RunCoroutine(DeathEffect.EvaluateDeath(_card.State,_card.View));
            }

            Unload();
        }

        private void Unload()
        {
            Destroy(gameObject);
        }
    }
}