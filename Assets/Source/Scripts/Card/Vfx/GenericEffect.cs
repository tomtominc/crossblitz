using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.GameVfx;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    /// <summary>
    /// A simple effect that can be used for things that just spawn on top of something and disappear
    /// Can use the generic effect particle or override the generic effect particle for additional behaviour
    /// </summary>
    public class GenericEffect : MonoBehaviour, IGameVfx
    {
        public float delayBetweenSpawns;
        public GenericEffectParticle genericEffectParticle;

        public virtual void Initialize(CardView card) { }
        public virtual void Initialize(Transform rect) { }

        public virtual IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            var getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(command);
            yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
            var vfxData = getSourceAndTargetData.Result;
            var handles = new List<CoroutineHandle>();

            for (var i = 0; i < vfxData.targetCards.Count; i++)
            {
                var genericEffect = Instantiate(genericEffectParticle, transform, false);
                handles.Add( Timing.RunCoroutine(genericEffect.Play(vfxData, command, i)));
                if (delayBetweenSpawns > 0) yield return Timing.WaitForSeconds(delayBetweenSpawns);
            }

            while (handles.Exists(handle => handle.IsRunning)) yield return Timing.WaitForOneFrame;

            Destroy(gameObject,2);
        }
    }
}