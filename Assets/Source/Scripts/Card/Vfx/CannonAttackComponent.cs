using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class CannonAttackComponent : CustomAttackComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<CannonAttackEffect>("CannonAttackEffect", card.transform);
            effect.Initialize(card);
        }
    }
}