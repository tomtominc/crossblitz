using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class ThornsEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<ThornsEffect>("ThornsEffect", card.cardObject);
            effect.Initialize(card);
        }
    }
}