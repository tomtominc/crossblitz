using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class BarrierEffect : MonoBehaviour, IGameVfx
    {
        private const int SpawnPreFrames = 6;
        private const float FrameTime = 0.04f;
        private const float ForthFrameHoldTime = 0.12f;

        public SpriteAnimation animator;
        public CanvasGroup frozenVariant;

        private CardView _card;
        private GameCardView _gameCard;
        private bool _isActive;

        private RectTransform _rect;
        public void Initialize(Transform rect)
        {
            _rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
            _isActive = false;
            _gameCard = _card.GetCardComponent<GameCardView>();
            _gameCard.Events.OnFrozen += OnFrozen;
            _gameCard.Events.OnUnFrozen += OnUnFrozen;
            _gameCard.Events.OnDamaged += OnDamaged;
            _gameCard.Events.OnSilenced += OnSilenced;
            _gameCard.Events.OnTransformed += OnTransformed;
        }

        private void Update()
        {
            if (_card != null)
            {
                transform.localPosition = _card.cardObject.localPosition;
            }
        }

        public IEnumerator<float> Spawn()
        {
            _isActive = true;

            gameObject.SetActive(true);
            animator.SetActive(false);
            frozenVariant.SetActive(false);
            var cardRect = _card.cardObject;

            for (var i = 0; i < SpawnPreFrames; i++)
            {
                switch (i)
                {
                    case 0: cardRect.anchoredPosition = new Vector2(-1,-2);
                        break;
                    case 1: cardRect.anchoredPosition = new Vector2(1,-3);
                        break;
                    case 2: cardRect.anchoredPosition = new Vector2(-1,-6);
                        break;
                    case 3: cardRect.anchoredPosition = new Vector2(0,-7);
                        break;
                    case 4: cardRect.anchoredPosition = new Vector2(0,-8);
                        break;
                }
                if (i == 4) yield return Timing.WaitForSeconds(ForthFrameHoldTime);
                else yield return Timing.WaitForSeconds(FrameTime);
            }

            animator.SetActive(true);
            cardRect.anchoredPosition = new Vector2(0,0);

            animator.Play("spawn",OnBarrierSpawnUpdate);
            AudioController.PlaySound("vfx_barrier_created", "BARDSFX", true, gameObject);

            while (animator.CurrentFrame == 0) yield return Timing.WaitForOneFrame;

            if (_gameCard.State.HasStatusEffect(StatusEffect.Frozen))
            {
                frozenVariant.SetActive(true);
            }

            while (!animator.IsDone) yield return Timing.WaitForOneFrame;

            if (_gameCard.State.HasStatusEffect(StatusEffect.Frozen))
            {
                frozenVariant.SetActive(true);
                animator.SetActive(false);
            }
            else
            {
                animator.Play("idle");
            }
        }

        private void OnBarrierSpawnUpdate(int frame)
        {
            // var cardRect = _card.cardObject;
            //
            // cardRect.anchoredPosition = frame switch
            // {
            //     0 => new Vector2(0, 4),
            //     1 => new Vector2(-1, 7),
            //     2 => new Vector2(0, 10),
            //     3 => new Vector2(0, 11),
            //     4 => new Vector2(0, 10),
            //     5 => new Vector2(0, 8),
            //     6 => new Vector2(0, 5),
            //     7 => new Vector2(0, 1),
            //     8 => new Vector2(0, -1),
            //     9 => new Vector2(0, 0),
            //     _ => cardRect.anchoredPosition
            // };
        }

        public IEnumerator<float> DestroyBarrier()
        {
            _isActive = false;
            animator.Play("break");
            AudioController.PlaySound("vfx_barrier_broken", "BARDSFX", true, gameObject);
            frozenVariant.SetActive(false);
            while (!animator.IsDone) yield return Timing.WaitForOneFrame;
            animator.SetActive(false);
        }

        private void OnFrozen(GameCardView card)
        {
            if (_gameCard != card || !_isActive ) return;

            frozenVariant.alpha = 0;
            frozenVariant.SetActive(true);
            frozenVariant.DOFade(1, 0.35f);
        }

        private void OnUnFrozen(GameCardView card)
        {
            if (_gameCard != card || !_isActive) return;
            frozenVariant.SetActive(false);
            animator.SetActive(true);
            animator.Play("idle");
        }

        private void OnDamaged(GameCardView card)
        {
            if (!_isActive) return;

            _gameCard.Events.LostBarrier();
            Timing.RunCoroutine(DestroyBarrier());

            if (_gameCard.State.HasStatusEffect(StatusEffect.Barrier))
            {
                _gameCard.State.RemoveStatusEffect(StatusEffect.Barrier);
            }
        }

        private void OnSilenced(GameCardView card)
        {
            if (!_isActive)
            {
                return;
            }

            _gameCard.Events.LostBarrier();
            animator.SetActive(false);

            if (_gameCard.State.HasStatusEffect(StatusEffect.Barrier))
            {
                _gameCard.State.RemoveStatusEffect(StatusEffect.Barrier);
            }
        }

        private void OnTransformed(GameCardView card)
        {
            if (card.State.HasStatusEffect(StatusEffect.Barrier))
            {
                return;
            }

            RemoveBarrier(card);
        }

        private void RemoveBarrier(GameCardView card)
        {
            if (!_isActive)
            {
                return;
            }

            animator.SetActive(false);

            if (_gameCard.State.HasStatusEffect(StatusEffect.Barrier))
            {
                _gameCard.State.RemoveStatusEffect(StatusEffect.Barrier);
            }
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            throw new System.NotImplementedException();
        }
    }
}