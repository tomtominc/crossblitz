using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.GameVfx;
using CrossBlitz.GameVfx.Minion;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class VoidSlashBoardEffect : MonoBehaviour, IGameVfx
    {
        public VoidSlashEffect voidSlashEffectPrefab;
        public void Initialize(CardView card)
        { }

        public void Initialize(Transform rect)
        { }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            gameObject.SetActive(true);

            var getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(command);
            yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
            var vfxData = getSourceAndTargetData.Result;

            for (var i = 0; i < vfxData.targetCards.Count; i++)
            {
                var draconicDiveEffect = Instantiate(voidSlashEffectPrefab, transform, false);
                yield return Timing.WaitUntilDone(draconicDiveEffect.Play(vfxData, command, i));
            }

            yield return Timing.WaitForOneFrame;

            Destroy(gameObject);
        }
    }
}