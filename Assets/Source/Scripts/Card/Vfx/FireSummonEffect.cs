using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class FireSummonEffect : MonoBehaviour,IGameVfx
    {
        public SpriteAnimation summonEffect;
        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }
        public void Initialize(CardView card)
        {

        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            gameObject.SetActive(true);

            summonEffect.Play("spawn", () =>
            {
                gameObject.SetActive(false);
            });

            while (summonEffect.CurrentFrame < 27) yield return Timing.WaitForOneFrame;

            GameplayScreen.Instance.ShakeScreen();
        }
    }
}