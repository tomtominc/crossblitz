using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class BananaBuffEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<BananaBuffEffect>("BananaBuffEffect", card.cardObject);
            effect.Initialize(card);
        }
    }
}