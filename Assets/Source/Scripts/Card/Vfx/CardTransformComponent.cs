using CrossBlitz.GameVfx;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class CardTransformComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<CardTransformEffect>("CardTransformEffect", card.transform);
            effect.Initialize(card);
        }
    }
}