using CrossBlitz.GameVfx;

namespace CrossBlitz.Card.Vfx
{
    public class CardControlsComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            var addressableId = card.viewType == CardView.CardViewType.STANDARD
                ? "CardControlsEffect_Standard"
                : "CardControlsEffect_Zoomed";
            effect = await LoadEffect<CardControlsEffect>(addressableId, card.transform);
            effect.Initialize(card);
        }
    }
}