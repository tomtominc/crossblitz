using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.GameVfx;
using CrossBlitz.Transition;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class CannonBallEffectParticle : MonoBehaviour
    {
        private const float MinJumpHeight = 0.5f;
        private const float MaxJumpHeight = 1f;
        private const float JumpTimePerMeter = 0.5f;

        public SpriteAnimation animator;
        public SpriteAnimation animatorBurst;
        public SpriteAnimation animatorExplode;

        private float m_jumpHeight;
        private float m_timeToJumpApex;
        private bool m_updateJump;
        private float m_maxDistance;
        private Vector3 m_midPoint;

        private void FixedUpdate()
        {
            if (m_updateJump)
            {
                var distance = Vector3.Distance(animator.transform.position, m_midPoint);
                var ratio = (1 * (distance / m_maxDistance));
                animator.transform.localScale = new Vector3(2 - ratio, 2 - ratio);
            }
        }

        public IEnumerator<float> Play(VfxSourceAndTargetData vfxData, Command command, int targetIndex)
        {
            var sourcePosition = vfxData.sourceCardPosition;
            var targetPosition = vfxData.targetCardPositions[targetIndex];
            var sourceCard = vfxData.sourceCardObject;

            transform.position = sourcePosition;

            animator.Play("idle");
            AudioController.PlaySound("vfx_cannonshot", "BARDSFX", true, gameObject);
            GameplayScreen.Instance.ShakeScreen();

            var distance = Vector3.Distance(sourcePosition, targetPosition);
            var direction = (sourcePosition - targetPosition).normalized;
            var angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

            animatorBurst.SetActive(true);
            animatorBurst.Play("smoke-burst");
            animatorBurst.transform.eulerAngles = new Vector3(0,0,angle + 90);
            animator.transform.eulerAngles = new Vector3(0, 0, angle + 90);

            if (sourceCard != null)
            {
                sourceCard.View.Tint(Color.white, BlendMode.Lighten);
                sourceCard.View.RemoveTintOverTime(0.24f);
                sourceCard.View.cardObject.DOPunchAnchorPos(direction * 20f, 0.24f, 2, 0).SetEase(Ease.OutBack);
            }

            m_jumpHeight = Mathf.Clamp(distance, MinJumpHeight, MaxJumpHeight);
            m_timeToJumpApex = m_jumpHeight * JumpTimePerMeter;
            m_updateJump = true;

            m_midPoint = (animator.transform.position + targetPosition) / 2f;
            m_maxDistance = Vector3.Distance(animator.transform.position, m_midPoint);

            yield return Timing.WaitUntilDone(animator.transform.DOMove(m_midPoint, m_timeToJumpApex).SetEase(Ease.OutSine)
                .SetUpdate(UpdateType.Fixed).WaitForCompletion(true));

            yield return Timing.WaitUntilDone(animator.transform.DOMove(targetPosition, m_timeToJumpApex).SetEase(Ease.InSine)
                .SetUpdate(UpdateType.Fixed).WaitForCompletion(true));

            animator.Play("pre-explode");
            yield return Timing.WaitForSeconds(.05f);

            GameplayScreen.Instance.ShakeScreen();
            TransitionController.FlashScreen(Color.yellow, BlendMode.Lighten);
            AudioController.PlaySound("vfx_fiery_explosion", "BARDSFX", true, gameObject);

            GameVfxUtilities.HandleDamageCommand( command, vfxData, targetIndex, vfxData.targetCardObjects[targetIndex] );

            animator.SetActive(false);

            animatorExplode.transform.position = animator.transform.position;
            animatorExplode.SetActive(true);
            animatorExplode.Play("explosion");

            m_updateJump = false;
            animator.transform.localScale = Vector3.one;
        }
    }
}