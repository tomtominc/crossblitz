
using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using DG.Tweening;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.GameVfx.Minion
{
    public class FreezeEffect : MonoBehaviour, IGameVfx
    {
        private const float ShineDelay = 1f;

        public CanvasGroup canvasGroup;
        public SpriteAnimation freezeAnimator;
        public SpriteAnimation shineParticle;
        private CardView _card;
        private RectTransform _cardTransform;
        private GameCardView _gameCard;

        private float _shineTimer;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
            _cardTransform = card.cardObject;
            _gameCard = _card.GetCardComponent<GameCardView>();
            _gameCard.Events.OnUnFrozen += OnUnFrozen;
            _gameCard.Events.OnTransformed += OnUnFrozen;
            _gameCard.Events.OnSilenced += OnUnFrozen;
        }

        private void OnDestroy()
        {
            _gameCard.Events.OnUnFrozen -= OnUnFrozen;
            _gameCard.Events.OnTransformed -= OnUnFrozen;
            _gameCard.Events.OnSilenced -= OnUnFrozen;
        }

        private void Update()
        {
            _shineTimer -= Time.deltaTime;

            if (_shineTimer <= 0)
            {
                _shineTimer = ShineDelay;
                shineParticle.SetActive(true);
                shineParticle.Play("shine",OnShineFinished);

                int posX = Random.Range(-27, 28);
                int posY = Random.Range(-38, 25);

                shineParticle.RectTransform().anchoredPosition = new Vector2(posX, posY );
            }
        }

        private void OnShineFinished()
        {
            shineParticle.SetActive(false);
        }

        public void OnAfterCreation()
        {

        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            // nothing here, this effect uses custom methods
            Debug.LogError("Freeze Effect should never invoke the generic Play() method - this class uses custom methods: 'Freeze()' or 'UnFreeze()'.");
            yield break;
        }

        public IEnumerator<float> Freeze()
        {
            gameObject.SetActive(true);
            canvasGroup.SetActive(true);
            canvasGroup.alpha = 0;
            canvasGroup.DOFade(1, 0.12f);
            AudioController.PlaySound("vfx_frozen", "BARDSFX", true, gameObject);
            yield break;
        }

        private void OnUnFrozen(GameCardView card)
        {
            Timing.RunCoroutine(UnFreeze());
        }

        private IEnumerator<float> UnFreeze()
        {
            yield return Timing.WaitUntilDone(canvasGroup.DOFade(0, 0.12f).WaitForCompletion(true));
            //Destroy(gameObject);
        }
    }
}