using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class HellfireEffect : MonoBehaviour, IGameVfx
    {
        private const float FrameTime = 0.08f;
        public SpriteAnimation shootAnim;
        public Color overlayPurple;
        public Color overlayWhite;

        private CardView _card;
        private GameCardView _gameCard;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }
        public void Initialize(CardView card)
        {
            _card = card;
            _gameCard = _card.GetCardComponent<GameCardView>();
        }

        /// <summary>
        /// Play is used when spawning the effect on top of the card.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerator<float> DealDamage(int amount)
        {
            // by default the effects are always inactive, we'll need to make them active when we play them.
            gameObject.SetActive(true);


            // get a reference to the card object (child object of the card so we aren't moving the card directly.
            var cardRect = _card.cardObject;

            // Shake Card
            var origAnchorPos = cardRect.anchoredPosition;
            cardRect.DOShakeAnchorPos(0.25f, 5, 50, 0).OnComplete(() => cardRect.DOAnchorPos(origAnchorPos, 0.1f));

            // Play shot Animation and change color
            var randomVector = new Vector2(Random.Range(-20.0f, 20.0f), Random.Range(-20.0f, 20.0f));
            shootAnim.RectTransform().anchoredPosition = randomVector;
            shootAnim.Play("bullet-ping");
            AudioController.PlaySound("vfx_gunshot", "BARDSFX", true, gameObject);
            _card.Tint(overlayWhite, BlendMode.Lighten);
            yield return Timing.WaitForSeconds(0.05f);
            _card.Tint(overlayPurple, BlendMode.Color);
            _card.RemoveTintOverTime(0.2f);

            while (!shootAnim.IsDone) yield return Timing.WaitForOneFrame;

            if (amount > 0)
            {
                var damageFx = _card.GetVfx<DamageEffectComponent, DamageEffect>();
                Timing.RunCoroutine(damageFx.GenericDamageAnimation(amount));
            }

            gameObject.SetActive(false);
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            yield break;
        }
    }
}