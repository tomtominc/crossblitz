using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class FireSummonEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<FireSummonEffect>("FireSummonEffect", card.transform);
            effect.Initialize(card);
        }
    }
}