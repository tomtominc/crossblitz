using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class TrapCardEffect : MonoBehaviour, IGameVfx
    {
        public SpriteAnimation trapDoor;
        public SpriteAnimation trapDoorBubble;

        public void Initialize(CardView card)
        {
            var gameCard = card.GetCardComponent<GameCardView>();
            var tile = gameCard.TileState?.GetView();
            if (tile) trapDoorBubble.transform.SetParent(tile.effectsContainer);

            gameCard.Events.OnTransformed += OnTransformed;
        }

        public void Initialize(Transform rect)
        {

        }

        private void OnTransformed(GameCardView card)
        {
            gameObject.SetActive(false);
        }

        public void Hide()
        {
            gameObject.SetActive(true);
            trapDoor.SetActive(true);
            trapDoor.Play("closed");
        }

        public IEnumerator<float> Reveal()
        {
            trapDoorBubble.SetActive(true);
            trapDoorBubble.Play("spawn");
            AudioController.PlaySound("CutsceneSFX-Determination", "SFX", true, gameObject);
            while (!trapDoorBubble.IsDone) yield return Timing.WaitForOneFrame;
            trapDoorBubble.SetActive(false);
            trapDoor.Play("open");
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            throw new System.NotImplementedException();
        }
    }
}