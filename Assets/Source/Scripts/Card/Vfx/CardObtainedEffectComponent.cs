using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class CardObtainedEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<CardObtainedEffect>("CardObtainedEffect", card.transform);
            effect.Initialize(card);
        }
    }
}