using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.GameVfx;
using MEC;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Card.Vfx
{
    public class AddPuffCapToDeck : MonoBehaviour, IGameVfx
    {
        public PuffProjectile puffProjectilePrefab;
        public BombProjectile bombProjectilePrefab;

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            var getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(command, false);
            yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
            var vfxData = getSourceAndTargetData.Result;
            var handles = new List<CoroutineHandle>();

            for (var i = 0; i < vfxData.targetCards.Count; i++)
            {
                var addedCardState = vfxData.targetCards[i];
                var playerDeck = GameplayScreen.Instance.GetDeck(addedCardState.Team);
                vfxData.targetCardPositions[i] = playerDeck.transform.position;

                AudioController.PlaySound("vfx_lob", "BARDSFX", true, gameObject);

                if (addedCardState.characterData.id == "PuffCap")
                {
                    var puffProjectile = Instantiate(puffProjectilePrefab, transform, false);
                    handles.Add(Timing.RunCoroutine(puffProjectile.Play(vfxData, command, i, -1, string.Empty, .5f, true)));
                }
                else if (addedCardState.characterData.id == "Bomb" || addedCardState.characterData.id == "UpgradedBomb")
                {
                    var bombProjectile = Instantiate(bombProjectilePrefab, transform, false);
                    handles.Add(Timing.RunCoroutine(bombProjectile.Play(vfxData, command, i, -1, new Vector3(0, 64f), true)));
                }

                Events.Publish(null, EventType.OnCardAddedToDeck, new OnCardAddedToDeckEventArgs
                {
                    teamOfDeck = addedCardState.Team,
                    addedByTeam = vfxData.sourceCard.Team,
                    cardUid = addedCardState.uid
                });

                yield return Timing.WaitForSeconds(Random.Range(0.2f, 0.4f));
            }

            while (handles.Exists(handle => handle.IsRunning)) yield return Timing.WaitForOneFrame;

            Destroy(gameObject, 2f);
        }

        public void Initialize(CardView card)
        {

        }

        public void Initialize(Transform rect)
        {

        }
    }
}