using System.Collections.Generic;
using CrossBlitz.Audio;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class FuseEffectParticle : MonoBehaviour
    {
        public SpriteAnimation animator;

        public IEnumerator<float> Spawn()
        {
            animator.Play("spawn");
            AudioController.PlaySound("vfx_transform_poof", "BARDSFX", true, gameObject);
            while (!animator.IsDone) yield return Timing.WaitForOneFrame;
            animator.Play("idle");
        }

        public IEnumerator<float> Burn()
        {
            animator.Play("burn");
            AudioController.PlaySound("vfx_bomb_fuse", "BARDSFX", false, gameObject);
            while (!animator.IsDone) yield return Timing.WaitForOneFrame;
            AudioController.StopSound("vfx_bomb_fuse");
            animator.SetActive(false);
        }
    }
}