using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class DamageEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<DamageEffect>("DamageEffect", card.transform);
            effect.Initialize(card);
        }
    }
}