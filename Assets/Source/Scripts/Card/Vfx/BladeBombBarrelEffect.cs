using System;
using System.Collections;
using System.Collections.Generic;
using Asyncoroutine;
using BlendModes;
using CrossBlitz.AssetManagement;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using CrossBlitz.Transition;
using DG.Tweening;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.GameVfx.Spell
{
    public class BladeBombBarrelEffect : MonoBehaviour, IGameVfx
    {
        private const float Inset = -50f;
        private const float MaxTime = 5;

        public RectTransform boundsRect;
        public SpriteAnimation barrelAnim;
        public SpriteAnimationAsset projectileAnim;
        public CanvasGroup barrelCanvas;
        public BladeBombProjectileEffect projectilePrefab;
        public List<RectTransform> _currentlyColliding;
        public List<GameCardView> collidedWith;

        private RectTransform _barrelRect;
        private Vector2 _moveDirection;
        private float _moveSpeed;
        private float _moveAccel;

        private bool _grounded = false;
        private bool _finished;
        private float _bounceCount;
        private float _currentTime;

        private List<GameCardView> _testCollisionsWith;

        private DamageCommand _command;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {

        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            //Debug.Log("Spin Target Count " +  command.TargetUids.Count);
            //todo: erase this! not really how this is supposed to happen
            // yield return Timing.WaitUntilDone(GameplayScreen.Instance.heroPowerCinematic
            //         .PlayCinematic(!Command.IsOpponentCommand(command), command.SourceUid));

            _command = command as DamageCommand;
            _testCollisionsWith = new List<GameCardView>();
            collidedWith = new List<GameCardView>();
            _currentlyColliding = new List<RectTransform>();
            _barrelRect = barrelCanvas.RectTransform();

            _barrelRect.anchoredPosition = new Vector2(0f, 280);
            _moveDirection = new Vector2(0f, -1f);
            _grounded = false;

            barrelAnim.Play("fall");
            yield return Timing.WaitForSeconds(0.3f);

            AudioController.PlaySound("SpecialTile_Fall", "SFX", true);

            while (!_grounded) yield return Timing.WaitForOneFrame;

            barrelAnim.Play("land");
            while (!barrelAnim.IsDone) yield return Timing.WaitForOneFrame;

            barrelAnim.Play("fuse");
            AudioController.PlaySound("vfx_bomb_fuse", "BARDSFX", false);
            while (!barrelAnim.IsDone) yield return Timing.WaitForOneFrame;
            

            barrelAnim.Play("explode");
            while (barrelAnim.CurrentFrame < 6) yield return Timing.WaitForOneFrame;
            GameplayScreen.Instance.ShakeScreen(4, 2);
            TransitionController.FlashScreen(Color.yellow, BlendMode.Color);
            AudioController.PlaySound("vfx_fiery_explosion", "BARDSFX", true, gameObject);
            AudioController.StopSound("vfx_bomb_fuse");

            // Spawn and shoot out the projectiles
            for (var i = 0; i < 6; i++)
            {
                var animIndex = Random.Range(0,4);
                var projectileAnimation = projectileAnim.animations[animIndex];

                var degrees = i*60;
                var projectileDirection = new Vector2((float)Math.Cos(degrees * Math.PI / 180), (float)Math.Sin(degrees * Math.PI / 180));

                var barrelProjectile = Instantiate(projectilePrefab, _barrelRect.RectTransform(), false);
                barrelProjectile.Setup(projectileDirection, projectileAnimation.name, collidedWith);

                if (i == 5)
                {
                    AudioController.PlaySound("vfx_sword_slash", "BARDSFX", true, gameObject);
                    yield return Timing.WaitUntilDone(barrelProjectile.Play(command, vfxParams));
                }
                else
                {
                    Timing.RunCoroutine(barrelProjectile.Play(command, vfxParams));
                }
            }
        }

        private void Update()
        {
            if (!_grounded)
            {
                FallBarrel();
            }
        }

        private void FallBarrel()
        {
            _moveDirection.x = 0f;
            _moveDirection.y = 1f;

            var pos = _barrelRect.anchoredPosition;
            pos -= _moveDirection.normalized * _moveSpeed;
            _moveAccel += (0.1f * Time.deltaTime);
            _moveSpeed += _moveAccel;

            if (pos.y < 0)
            {
                pos.y = 0;
                _moveDirection.y = 0f;
                GameplayScreen.Instance.ShakeScreen(4, 2);
               _grounded = true;
            }

            _barrelRect.anchoredPosition = pos;
        }

        public void Unload()
        {
            AddressableReferenceLoader.Unload(gameObject);
        }
    }
}