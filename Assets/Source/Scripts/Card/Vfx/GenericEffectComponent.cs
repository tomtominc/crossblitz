using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class GenericEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<GenericAbilityEffect>("GenericAbilityEffect", card.cardObject);
            effect.Initialize(card);
        }
    }
}