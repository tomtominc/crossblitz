using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class SignHereEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            Debug.Log("INSIDE COMPONENET SIGN HERE");
            effect = await LoadEffect<SignHereEffect>("SignHereEffect", card.transform);
            effect.Initialize(card);
        }
    }
}