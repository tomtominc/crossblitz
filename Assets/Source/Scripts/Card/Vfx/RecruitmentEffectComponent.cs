using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class RecruitmentEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<RecruitmentEffect>("RecruitmentEffect", card.transform);
            effect.Initialize(card);
        }
    }
}