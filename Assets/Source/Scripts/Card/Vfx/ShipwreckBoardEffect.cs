using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Spell
{
    public class ShipwreckBoardEffect : MonoBehaviour, IGameVfx
    {
        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }
        public void Initialize(CardView card){}

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            gameObject.SetActive(true);

            var damagedCards = new List<GameCardView>();

            for (var i = 0; i < command.TargetUids.Count; i++)
            {
                var targetUid = command.TargetUids[i];
                var getBoardCardTask = GameBoard.GetBoardCard(targetUid);
                yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                var getBoardCardResult = getBoardCardTask.Result;

                if (getBoardCardResult.error != ClientErrorCode.None)
                {
                    Server.HandleClientError(getBoardCardResult, false);
                }

                if (getBoardCardResult.boardCard)
                {
                    var boardCard = getBoardCardResult.boardCard;
                    var deathExplosion = boardCard.GetVfx<DeathExplodeEffectComponent>();
                    yield return Timing.WaitUntilDone(deathExplosion.AsCoroutine());
                    damagedCards.Add(boardCard);
                }
            }

            for (var i = 0; i < damagedCards.Count; i++)
            {
                var deathExplosion = damagedCards[i].View.GetVfx<DeathExplodeEffectComponent, DeathExplodeEffect>();
                Timing.RunCoroutine(deathExplosion.Play(command, vfxParameters));
            }

            yield return Timing.WaitForSeconds(2);
        }
    }
}