using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asyncoroutine;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.Utils;
using DG.Tweening;
using MEC;
using Source.Scripts.Card;
using Source.Scripts.Server.GameLogic.Data;
using TakoBoyStudios;
using UnityEngine;

namespace CrossBlitz.GameVfx
{
    public interface IGameVfx
    {
        void Initialize(CardView card);
        void Initialize(Transform rect);

        /// <summary>
        /// This is called when the vfx needs to be played for our player. The IEnumerator is waited on so not all effects go off at once
        /// </summary>
        /// <param name="command"></param>
        ///  <param name="vfxParameters"></param>
        /// <returns></returns>
        IEnumerator<float> Play(Command command, VfxParameters vfxParameters);
    }

    [System.Serializable]
    public class VfxParameters
    {
        public string targetUid;
        public string historyUid;
        public bool showPreViz;
        public bool dimScreen;
        public bool skipDeathAnimation;
        public bool destroyAfterCompletion;
        public Action OnApply;
        public List<GameCardView> summons;
    }

    public class VfxSourceAndTargetData
    {
        public GameCardState sourceCard;
        public RectTransform sourceRect;
        public GameCardView sourceCardObject;
        public Vector3 sourceCardPosition;

        public List<GameCardState> targetCards;
        public List<GameCardView> targetCardObjects;
        public List<Vector3> targetCardPositions;
        public List<RectTransform> targetRects;

        public List<Vector3> targetEmptyTilePositions;

        public static async Task<VfxSourceAndTargetData> GetVfxData( Command command, bool waitForBoardPendingBoardCards = true)
        {
            GameCardView sourceCardObject = null;
            var sourceCard = GameServer.State.GetCard(command.SourceUid);
            Vector3 sourcePosition;
            RectTransform sourceRectTransform=null;

            if (sourceCard.IsHero || sourceCard.characterData.type == CardType.Spell ||
                sourceCard.characterData.type == CardType.Power || sourceCard.characterData.type == CardType.Relic ||
                sourceCard.characterData.type == CardType.Elder_Relic)
            {
                var heroTarget = GameplayScreen.Instance.heroSideBarContainer.GetHeroView(sourceCard.Team);
                sourcePosition = heroTarget.boardTarget.transform.position + Vector3.down;
                sourceRectTransform = heroTarget.boardTarget.RectTransform();
            }
            else
            {
                var maxTimeOutFrames = waitForBoardPendingBoardCards ? -1 : 1;
                var getBoardCardTask = GameBoard.GetBoardCard(sourceCard.uid, maxTimeOutFrames);
                await getBoardCardTask;
                var getBoardCardResult = getBoardCardTask.Result;

                if (getBoardCardResult.error != ClientErrorCode.None || getBoardCardResult.boardCard == null)
                {
                    sourcePosition = getBoardCardResult.worldPositionIfNotFound;
                }
                else
                {
                    sourceCardObject = getBoardCardResult.boardCard;
                    sourcePosition = sourceCardObject.transform.position;
                    sourceRectTransform = sourceCardObject.Rect;
                }
            }

            var targetCards = new List<GameCardState>();
            var targetCardObjects = new List<GameCardView>();
            var targetPositions = new List<Vector3>();
            var targetRects = new List<RectTransform>();
            var emptyTiles = new List<Vector3>();
            var ability = command.GetAbility();

            if (ability != null && ability.spawnOnEmptyRow)
            {
                var rowSource = command.TargetUids.Count > 0 ? command.TargetUids[0] : sourceCard.uid;

                if (!string.IsNullOrEmpty(rowSource))
                {
                    var tile = GameServer.State.Board.GetTileWithCardOrHistoryOfCard(rowSource);

                    if (tile == null)
                    {
                        var rowTarget = GameServer.State.GetCard(rowSource);
                        Debug.LogError($"No tile or history of tile found for {rowTarget.characterData.name}. Will not populate rows.");
                    }
                    else
                    {
                        var rowTiles = GameServer.State.Board.GetUnOccupiedTilesInRow(tile.Position.y);

                        for (var i = 0; i < rowTiles.Count; i++)
                        {
                            emptyTiles.Add(rowTiles[i].GetView().transform.position);
                        }
                    }
                }
            }

            if (ability != null && ability.spawnOnEmptyColumn)
            {
                var columnSource = command.TargetUids.Count > 0 ? command.TargetUids[0] : sourceCard.uid;

                if (!string.IsNullOrEmpty(columnSource))
                {
                    var tile = GameServer.State.Board.GetTileWithCardOrHistoryOfCard(columnSource);

                    if (tile == null)
                    {
                        var columnTarget = GameServer.State.GetCard(columnSource);
                        Debug.LogError($"No tile or history of tile found for {columnTarget.characterData.name}. Will not populate columns.");
                    }
                    else
                    {
                        var simplePos = BoardState.BoardPositionToSimpleMap[tile.Position];
                        var columnTiles = GameServer.State.Board.GetUnOccupiedTilesInColumn(simplePos.x);

                        for (var i = 0; i < columnTiles.Count; i++)
                        {
                            emptyTiles.Add(columnTiles[i].GetView().transform.position);
                        }
                    }
                }
            }

            for (var i = 0; i < command.TargetUids.Count; i++)
            {
                var targetCard = GameServer.State.GetCard(command.TargetUids[i]);
                targetCards.Add(targetCard);

                if (targetCard.IsHero)
                {
                    var heroTarget = GameplayScreen.Instance.heroSideBarContainer.GetHeroView(targetCard.Team);
                    targetPositions.Add(heroTarget.boardTarget.transform.position + Vector3.down);
                    targetCardObjects.Add(null); // adding target card object as null so count stays the same between position and card lists
                    targetRects.Add(heroTarget.boardTarget.RectTransform());
                }
                else
                {
                    var maxTimeOutFrames = waitForBoardPendingBoardCards ? -1 : 1;
                    var getBoardCardTask = GameBoard.GetBoardCard(targetCard.uid, maxTimeOutFrames);
                    await getBoardCardTask;
                    var getBoardCardResult = getBoardCardTask.Result;

                    if (getBoardCardResult.error != ClientErrorCode.None || getBoardCardResult.boardCard == null)
                    {
                        targetPositions.Add(getBoardCardResult.worldPositionIfNotFound);
                        targetCardObjects.Add(null);
                        targetRects.Add(null);
                    }
                    else
                    {
                        var targetCardObject = getBoardCardResult.boardCard;
                        targetCardObjects.Add(targetCardObject);
                        targetPositions.Add(targetCardObject.transform.position);
                        targetRects.Add(targetCardObject.Rect);
                    }
                }
            }

            return new VfxSourceAndTargetData
            {
                sourceCard = sourceCard,
                sourceRect = sourceRectTransform,
                sourceCardObject = sourceCardObject,
                sourceCardPosition = sourcePosition,

                targetCards = targetCards,
                targetCardObjects = targetCardObjects,
                targetCardPositions = targetPositions,
                targetRects = targetRects,

                targetEmptyTilePositions = emptyTiles
            };
        }
    }

    [System.Serializable]
    public class GameVfxData : UniqueItem
    {
        // public enum TargetType
        // {
        //     // targets a card view or hero view
        //     Single,
        // }

        public enum ParentType
        {
            CardTransform,
            CardObject,
        }

        public string vfxName;
        public string vfxType;
        public string vfxPrefab;
        public ParentType parentType;
    }

    public static class GameVfxUtilities
    {
        // public static void HandleAddToDeckCommand(Command command, string cardUid)
        // {
        //     if (command is AddCardToZoneCommand zoneCommand && zoneCommand.locationToAddTo == CardLocation.Deck)
        //     {
        //         Timing.RunCoroutine(GameplayScreen.Instance.AddCardToDeck(cardUid));
        //     }
        // }

        public static void HandleDamageCommand(Command command, VfxSourceAndTargetData vfxData, int targetIndex, GameCardView targetCard, bool skipDeathEvaluation = false)
        {
            if (command is DamageCommand damageCommand)
            {
                // do damage here..
                // check if card null, if not apply damage then do generic damage animation.
                // remember to check "Apply Damage in Effect" when setting up the effect in the ability

                var targetState = vfxData.targetCards[targetIndex];

                if (targetState != null)
                {
                    var damageHistory = damageCommand.PendingDamageHistories[targetIndex];
                    targetState.ApplyHistory(damageHistory.Uid);

                    if (targetState.IsHero)
                    {
                        var heroView = GameplayScreen.Instance.heroSideBarContainer;
                        Timing.RunCoroutine(heroView.HitPortraitNonAttack(targetState.Team, damageCommand, damageHistory.Uid));
                    }
                    else if (targetCard != null)
                    {
                        var amount = damageHistory.DamageAmount;
                        var damageFx = targetCard.View.GetVfx<DamageEffectComponent, DamageEffect>();

                        if (damageFx != null)
                        {
                            Timing.RunCoroutine(damageFx.GenericDamageAnimation(amount, skipDeathEvaluation));
                        }
                    }
                }
            }
        }

        public static IEnumerator<float> EvaluateDeath(List<GameCardView> targetCards)
        {
            targetCards.RemoveAll(c => c == null);
            targetCards = targetCards.DistinctBy(c => c.State.uid).ToList();
            targetCards = targetCards.OrderBy(c => c != null && c.State.HasTrigger(TriggerType.DEATHRATTLE)).ToList();

            for (var i = 0; i < targetCards.Count; i++)
            {
                var targetCardObject = targetCards[i];
                if (targetCardObject == null) continue;
                var damageFx = targetCardObject.View.GetVfx<DamageEffectComponent, DamageEffect>();
                yield return Timing.WaitUntilDone(damageFx.EvaluateDeath(targetCardObject.State).CancelWith(targetCardObject.gameObject));
            }
        }

        public static IEnumerator<float> MoveJump(Transform obj, Vector3 startPosition, Vector3 targetPosition, float speed, float scale)
        {
            obj.transform.localScale = new Vector3(1, 1, 1);
            obj.position = startPosition;

            var midPoint = (startPosition + targetPosition) / 2f;
            var timeInAir = 0f;

            while (timeInAir < 1)
            {
                var x = EasingFunction.EaseOutCirc(startPosition.x, midPoint.x, timeInAir);
                var y = EasingFunction.EaseOutCirc(startPosition.y, midPoint.y, timeInAir);
                var s = EasingFunction.EaseOutCirc(1, scale, timeInAir);

                obj.transform.position = new Vector3(x, y);
                obj.transform.localScale = new Vector3(s, s, 1);

                timeInAir += Time.deltaTime * speed;

                yield return Timing.WaitForOneFrame;
            }

            obj.transform.position = midPoint;
            obj.transform.localScale = new Vector3(scale, scale, 1);
            timeInAir = 0;

            while (timeInAir < 1)
            {
                var x = EasingFunction.EaseInCirc(midPoint.x, targetPosition.x, timeInAir);
                var y = EasingFunction.EaseInCirc(midPoint.y, targetPosition.y, timeInAir);
                var s = EasingFunction.EaseInCirc(scale, 1, timeInAir);

                obj.transform.position = new Vector3(x, y);
                obj.transform.localScale = new Vector3(s, s, 1);

                timeInAir += Time.deltaTime * speed;

                yield return Timing.WaitForOneFrame;
            }

            obj.transform.position = targetPosition;
            obj.transform.localScale = new Vector3(1, 1, 1);
        }

        public static IEnumerator<float> MoveInParabola(Transform obj, Vector3 targetPosition, float speed, float height, bool rotate )
        {
            var point = new List<Vector3> { obj.position, Vector3.zero, targetPosition };
            point[1] = (point[0] + (point[2] - point[0]) / 2) + Vector3.up * height;
            var time = 0f;
            var lastPosition = obj.position;

            while (time < 1f)
            {
                var m1 = Vector3.Lerp(point[0], point[1], time);
                var m2 = Vector3.Lerp(point[1], point[2], time);
                obj.position = Vector3.Lerp(m1, m2, time);

                var diff = (lastPosition - obj.position).normalized;
                var angle = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;

                if (rotate)
                {
                    obj.eulerAngles = new Vector3(0, 0, angle > 0 ? angle + 90 : 0);
                }

                time += Time.deltaTime * speed;
                lastPosition = obj.position;

                yield return Timing.WaitForOneFrame;
            }

            obj.position = targetPosition;
        }

        public static int GetCountWithConditions(Ability ability, AbilityCondition condition, Team team, int startingCount)
        {
            int number = startingCount;

            switch (condition.condition)
            {
                case Condition.NUMBER_OF_SPECIFIC_CARDS_IN_DECK:
                {
                    if (condition.filters.HasFlag(FilterCondition.PLAYER))
                    {
                        number += GameServer.GetPlayerState(team).GetNumberOfCardsWithIdInDeck(condition.cardId);
                    }
                    else if (condition.filters.HasFlag(FilterCondition.OPPONENT))
                    {
                        number += GameServer.GetPlayerState(team == Team.Client ? Team.Opponent : Team.Client).GetNumberOfCardsWithIdInDeck(condition.cardId);
                    }
                    break;
                }
            }

            return number;
        }
    }
}