using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI.GameLogic;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class RecruitmentEffect : MonoBehaviour, IGameVfx
    {
        private const float FrameTime = 0.08f;
        public SpriteAnimation flagAnim;

        private CardView _card;
        private GameCardView _gameCard;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }
        public void Initialize(CardView card)
        {
            _card = card;
            _gameCard = _card.GetCardComponent<GameCardView>();
        }

        /// <summary>
        /// Play is used when spawning the effect on top of the card.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            //Update their Position in the hierarchy
            var card = GameServer.State.GetCard(command.SourceUid);
            var tile = GameServer.State.Board.GetTile(card.uid);

            if (tile == null || tile.GetView() == null)
            {
                gameObject.SetActive(false);
                yield break;
            }

            transform.SetParent(tile.GetView().effectsContainer);

            // by default the effects are always inactive, we'll need to make them active when we play them.
            gameObject.SetActive(true);

            // get a reference to the card object (child object of the card so we aren't moving the card directly.
            var cardRect = _card.cardObject;

            // Play Stab
            flagAnim.Play("spawn", () => flagAnim.Play("idle", () => flagAnim.Play("despawn")));
            yield return Timing.WaitForSeconds(0.5f);

            cardRect.DOPunchAnchorPos(Vector2.up * 6f, 0.2f, 1, 0);
            AudioController.PlaySound("vfx_waving", "BARDSFX", true, gameObject);
            yield return Timing.WaitForSeconds(0.4f);

            cardRect.DOPunchAnchorPos(Vector2.up * 6f, 0.2f, 1, 0);
            AudioController.PlaySound("vfx_waving", "BARDSFX", true, gameObject);
            yield return Timing.WaitForSeconds(0.3f);

            gameObject.SetActive(false);

        }
    }
}