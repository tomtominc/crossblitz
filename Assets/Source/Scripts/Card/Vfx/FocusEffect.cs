using System;
using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.Transition;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class FocusEffect : MonoBehaviour, IGameVfx
    {
        [BoxGroup("Generic")] public Color overlayColorBlack;
        [BoxGroup("Generic")] public Color overlayColorBlue;
        [BoxGroup("Generic")] public Color overlayColorGold;

        [BoxGroup("Standard View")] public SpriteAnimation standardSingleEffect;
        [BoxGroup("Standard View")] public SpriteAnimation standardCompleteEffect;
        [BoxGroup("Standard View")] public SpriteAnimation standardCompleteAura;
        [BoxGroup("Standard View")] public SpriteAnimation standardCompleteBigSparkTopLeft;
        [BoxGroup("Standard View")] public SpriteAnimation standardCompleteBigSparkBottomRight;
        [BoxGroup("Standard View")] public SpriteAnimation standardCompleteSmallSparkTopLeft;
        [BoxGroup("Standard View")] public SpriteAnimation standardCompleteSmallSparkTopRight;
        [BoxGroup("Standard View")] public SpriteAnimation standardCompleteSmallSparkBottomRight;
        [BoxGroup("Standard View")] public SpriteAnimation standardCompleteSmallSparkBottomLeft;

        [BoxGroup("Zoomed View")] public SpriteAnimation zoomedSingleEffect;
        [BoxGroup("Zoomed View")] public SpriteAnimation zoomedCompleteEffect;

        private CardView m_cardView;
        private GameCardView m_gameCardView;
        private bool m_poweredUp;
        private bool m_poweredUpLoop;
        private bool m_playingSingleEffect;
        //private bool m_disableZoomOrPreviewActivation;

        public void Initialize(CardView card)
        {
            m_cardView = card;
            m_gameCardView = m_cardView.GetComponent<GameCardView>();
            m_poweredUp = false;
            m_poweredUpLoop = false;
        }

        public void Initialize(Transform rect)
        {
        }

        public void Update()
        {
            if (m_poweredUp && m_poweredUpLoop)
            {
                m_poweredUpLoop = false;
                Timing.RunCoroutine(PoweredUp());
            }
        }

        private void OnEnable()
        {
            if (m_poweredUp && m_cardView != null)
            {
                m_cardView.UpdatePortraitForFocus();
                m_cardView.portraitBackground.Play("focus-activated");
            }
        }

        public IEnumerator<float> SingleEffect(bool skipCharge = false)
        {
            gameObject.SetActive(true);

            

            switch (m_cardView.viewType)
            {
                case CardView.CardViewType.STANDARD:

                    AudioController.PlaySound("vfx_focus_lvl_1", "BARDSFX", true, gameObject);

                    //if (m_gameCardView != null && m_gameCardView.State.Team == Team.Opponent)
                    {
                        m_playingSingleEffect = true;

                        GameplayScreen.Instance.opponentPlayedCardPreview.OnCardShown += OnFocusPreviewCardShown;

                        // have the preview card shown
                        GameplayScreen.Instance.opponentPlayedCardPreview.SpawnCardPreview(
                            m_gameCardView.State,
                            typeof(FocusEffectComponent)
                        );
                    }

                    // ZOOM CARD STUFF
                    var zoomedCard = m_cardView.GetCardComponent<HoverCard>().ZoomedCard;
                    if (zoomedCard != null)
                    {
                        var effectComponent = zoomedCard.GetComponent<FocusEffectComponent>();

                        if (effectComponent == null)
                        {
                            effectComponent = zoomedCard.AddCardComponent<FocusEffectComponent>();
                            effectComponent.Initialize(zoomedCard);
                            effectComponent.OnAfterCreation();
                        }

                        while (effectComponent.Effect == null)
                        {
                            yield return Timing.WaitForOneFrame;
                        }

                        var focusEffect = (FocusEffect)effectComponent.Effect;

                        Timing.RunCoroutine(focusEffect.SingleEffect(true));
                    }

                    standardSingleEffect.SetActive(true);
                    standardSingleEffect.Play("burst-standard", () => standardSingleEffect.SetActive(false));

                    break;

                case CardView.CardViewType.ZOOMED:

                    if (!skipCharge)
                    {
                        zoomedSingleEffect.SetActive(true);
                        zoomedSingleEffect.Play("burst-zoomed", () => zoomedSingleEffect.SetActive(false));
                    }

                    break;
            }

            m_cardView.TintOverTime(overlayColorBlue, BlendMode.LighterColor, 0.1f);
            GameplayScreen.Instance.ShakeScreen(4, 2);
            //TransitionController.FlashScreen(overlayColorBlue, BlendMode.SoftLight);
            yield return Timing.WaitForSeconds(0.1f);

            m_cardView.RemoveTintOverTime(0.9f);
            yield return Timing.WaitForSeconds(0.9f);
        }

        public IEnumerator<float> CompleteEffect(bool skipCharge = false)
        {
            gameObject.SetActive(true);

            

            switch (m_cardView.viewType)
            {
                case CardView.CardViewType.STANDARD:

                    AudioController.PlaySound("vfx_focus_lvl_2", "BARDSFX", true, gameObject);

                    //if (m_gameCardView != null && m_gameCardView.State.Team == Team.Opponent)
                    {
                        m_playingSingleEffect = false;

                        GameplayScreen.Instance.opponentPlayedCardPreview.OnCardShown += OnFocusPreviewCardShown;

                        // have the preview card shown
                        GameplayScreen.Instance.opponentPlayedCardPreview.SpawnCardPreview(
                            m_gameCardView.State,
                            typeof(FocusEffectComponent)
                        );
                    }

                    // ZOOM CARD STUFF
                    var zoomedCard = m_cardView.GetCardComponent<HoverCard>().ZoomedCard;

                    if (zoomedCard != null)
                    {
                        var effectComponent = zoomedCard.GetComponent<FocusEffectComponent>();

                        if (effectComponent == null)
                        {
                            effectComponent = zoomedCard.AddCardComponent<FocusEffectComponent>();
                            effectComponent.Initialize(zoomedCard);
                            effectComponent.OnAfterCreation();
                        }

                        while (effectComponent.Effect == null)
                        {
                            yield return Timing.WaitForOneFrame;
                        }

                        var focusEffect = (FocusEffect)effectComponent.Effect;
                        Timing.RunCoroutine(focusEffect.CompleteEffect(true));
                    }

                    // Charge Up the card
                    m_cardView.TintOverTime(overlayColorBlack, BlendMode.DarkerColor, 0.1f);
                    standardCompleteEffect.SetActive(true);
                    standardCompleteEffect.Play("transform-standard",
                        () => standardCompleteEffect.SetActive(false));
                    while (standardCompleteEffect.CurrentFrame <= 12) yield return Timing.WaitForOneFrame;

                    // Power Up the card
                    //var gameCard = m_cardView.GetCardComponent<GameCardView>();
                    //gameCard.UpdateViewFromState(true);
                    m_cardView.UpdatePortraitForFocus();

                    GameplayScreen.Instance.ShakeScreen(6, 3);
                    //AudioController.PlaySound("HeroAbility-Flash", "SFX", true, gameObject);
                    m_cardView.portraitBackground.Play("focus-activated");
                    m_cardView.Tint(overlayColorGold, BlendMode.LighterColor);
                    m_cardView.RemoveTintOverTime(1.25f);

                    yield return Timing.WaitForSeconds(1f);

                    break;

                case CardView.CardViewType.ZOOMED:

                    // Charge Up the card
                    if (!skipCharge)
                    {
                        m_cardView.TintOverTime(overlayColorBlack, BlendMode.DarkerColor, 0.1f);
                        zoomedCompleteEffect.SetActive(true);
                        zoomedCompleteEffect.Play("transform-zoomed",
                            () => standardCompleteEffect.SetActive(false));
                        while (zoomedCompleteEffect.CurrentFrame <= 12) yield return Timing.WaitForOneFrame;
                    }

                    m_cardView.UpdatePortraitForFocus();
                    m_cardView.portraitBackground.Play("focus-activated");

                    if (!skipCharge)
                    {
                        m_cardView.Tint(overlayColorGold, BlendMode.LighterColor);
                        m_cardView.RemoveTintOverTime(1.25f);
                        yield return Timing.WaitForSeconds(1f);
                    }
                    break;
            }


            yield return Timing.WaitForSeconds(0.5f);

            SetFocusOn();
        }

        public IEnumerator<float> PoweredUp()
        {
            switch (m_cardView.viewType)
            {
                case CardView.CardViewType.STANDARD:
                    standardCompleteSmallSparkTopRight.SetActive(true);
                    standardCompleteSmallSparkTopRight.Play("spark-1",
                        () => standardCompleteSmallSparkTopRight.SetActive(false));
                    while (!standardCompleteSmallSparkTopRight.IsDone) yield return Timing.WaitForOneFrame;

                    standardCompleteAura.SetActive(true);
                    standardCompleteAura.Play("idle", () => standardCompleteAura.SetActive(false));
                    while (standardCompleteAura.CurrentFrame <= 9) yield return Timing.WaitForOneFrame;

                    standardCompleteSmallSparkBottomLeft.SetActive(true);
                    standardCompleteSmallSparkBottomLeft.Play("spark-2",
                        () => standardCompleteSmallSparkBottomLeft.SetActive(false));
                    while (standardCompleteSmallSparkBottomLeft.CurrentFrame <= 3)
                        yield return Timing.WaitForOneFrame;

                    standardCompleteSmallSparkBottomRight.SetActive(true);
                    standardCompleteSmallSparkBottomRight.Play("spark-1",
                        () => standardCompleteSmallSparkBottomRight.SetActive(false));
                    while (!standardCompleteSmallSparkBottomRight.IsDone) yield return Timing.WaitForOneFrame;

                    standardCompleteSmallSparkTopLeft.SetActive(true);
                    standardCompleteSmallSparkTopLeft.Play("spark-1",
                        () => standardCompleteSmallSparkTopLeft.SetActive(false));
                    while (!standardCompleteSmallSparkTopLeft.IsDone) yield return Timing.WaitForOneFrame;

                    standardCompleteAura.SetActive(true);
                    standardCompleteAura.Play("idle", () => standardCompleteAura.SetActive(false));
                    while (!standardCompleteAura.IsDone) yield return Timing.WaitForOneFrame;

                    break;

                case CardView.CardViewType.ZOOMED:

                    break;
            }

            m_poweredUpLoop = true;
        }

        public void SetFocusOn()
        {
            m_poweredUp = true;
            m_poweredUpLoop = true;
        }

        private async void OnFocusPreviewCardShown(GameCardView cardView)
        {
            GameplayScreen.Instance.opponentPlayedCardPreview.OnCardShown -= OnFocusPreviewCardShown;

            var focusEffectComponent = await cardView.GetVfx<FocusEffectComponent>();
            var focusEffect = (FocusEffect)focusEffectComponent.Effect;

            if (m_playingSingleEffect)
            {
                Timing.RunCoroutine(focusEffect.SingleEffect());
            }
            else
            {
                Timing.RunCoroutine(focusEffect.CompleteEffect());
            }

            m_playingSingleEffect = false;
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            yield break;
        }
    }
}