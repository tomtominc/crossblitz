using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class SlimeSummonEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<SlimeSummonEffect>("SlimeSummonEffect", card.cardObject);
            effect.Initialize(card);
        }
    }
}