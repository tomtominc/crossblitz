using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class SlashDamageEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<SlashDamageEffect>("SlashDamageEffect", card.transform);
            effect.Initialize(card);
        }
    }
}