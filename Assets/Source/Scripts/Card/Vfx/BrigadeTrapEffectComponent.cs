using CrossBlitz.GameVfx.Minion;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class BrigadeTrapEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<BrigadeTrapEffect>("BrigadeTrapEffect", card.transform);
            effect.Initialize(card);
        }
    }
}