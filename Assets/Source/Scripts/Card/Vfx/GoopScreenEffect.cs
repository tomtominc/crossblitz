using System.Collections.Generic;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx
{
    public class GoopScreenEffect : MonoBehaviour
    {

        public RectTransform goopSplashParent;
        public RectTransform goopBubbleParent;
        public CanvasGroup goopBubbleCanvas;

        public GoopFx goopPrefab;

        private List<GoopFx> _goopFx;
        private List<GoopFx> _splashes;

        private bool _updateSplashes;
        private float _splashTimer;

        private float m_width;
        private float m_height;

        private void Start()
        {
            goopBubbleCanvas.SetActive(false);
            goopSplashParent.SetActive(false);

            goopBubbleCanvas.alpha = 0;

            _goopFx = new List<GoopFx>();
            _splashes = new List<GoopFx>();

            m_width = goopBubbleParent.rect.width;
            m_height = goopBubbleParent.rect.height;
        }

        public void StartSplash()
        {
            goopBubbleCanvas.SetActive(true);
            goopSplashParent.SetActive(true);

            _updateSplashes = true;

            FillGoopScreen();
        }

        public IEnumerator<float> FadeInGoop()
        {
            goopBubbleParent.SetActive(true);

            for (var i = 0; i < _goopFx.Count; i++)
            {
                _goopFx[i].StartSliding();
            }

            yield return Timing.WaitUntilDone(goopBubbleCanvas.DOFade(1, 0.6f).WaitForCompletion(true));

            _updateSplashes = false;
        }

        private void Update()
        {
            if (_updateSplashes)
            {
                _splashTimer -= Time.deltaTime;
                if (_splashTimer <= 0)
                {
                    _splashTimer = 0.04f;

                    SpawnSplash();
                }
            }
        }

        private void SpawnSplash()
        {
            for (var i = _splashes.Count-1; i > -1; i--)
            {
                if (_splashes[i].kill) Destroy(_splashes[i].gameObject);
            }

            _splashes.RemoveAll(g => g == null || g.kill);

            var goop = Instantiate(goopPrefab, goopSplashParent);

            goop.Splatter();

            var inset = 20;

            var x = Random.Range(-m_width / 2f +inset ,m_width  /2f - inset);
            var y =Random.Range(-m_height/2f + inset, m_height/2f - inset);

            goop.RectTransform().anchoredPosition = new Vector2(x, y);

            var count = 0;

            while (count < 200 && _splashes.Exists(g => IsInside(
                g.RectTransform().anchoredPosition,
                g.Radius,
                goop.RectTransform().anchoredPosition,
                goop.Radius)))
            {
                x = Random.Range(-m_width / 2f + inset,m_width  /2f - inset);
                y  =Random.Range(-m_height/2f + inset, m_height/2f - inset);

                goop.RectTransform().anchoredPosition = new Vector2(x, y);
                count++;
            }

            _splashes.Add(goop);

            if (count >= 200)
            {
                //Debug.LogError("Couldn't find a spot for goop!");
            }

        }

        private void FillGoopScreen()
        {
            for (var i = 0; i < 200; i++)
            {
                var goop = Instantiate(goopPrefab, goopBubbleParent);
                goop.Bubble();

                var x = Random.Range(-m_width / 2f,m_width  /2f);
                var y = Random.Range(-m_height/2f, m_height/2f);

                goop.RectTransform().anchoredPosition = new Vector2(x, y);

                var count = 0;

                while (count < 200 && _goopFx.Exists(g => IsInside(
                    g.RectTransform().anchoredPosition,
                    g.Radius,
                    goop.RectTransform().anchoredPosition,
                    goop.Radius)))
                {
                    x =Random.Range(-m_width / 2f,m_width  /2f);
                    y = Random.Range(-m_height/2f, m_height/2f);

                    goop.RectTransform().anchoredPosition = new Vector2(x, y);
                    count++;
                }

                _goopFx.Add(goop);

                if (count >= 200)
                {
                    //Debug.LogError("Couldn't find a spot for goop!");
                }
            }
        }
        private bool IsInside(Vector2 midPoint0, float radius0, Vector2 midPoint1, float radius1)
        {
            var rect0 = new Rect(midPoint0.x, midPoint0.y, radius0, radius0);
            var rect1 = new Rect(midPoint1.x, midPoint1.y, radius1, radius1);

            return rect0.Overlaps(rect1);
        }
    }
}