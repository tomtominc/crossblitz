using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.Models;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class BrigadeTrapEffect : MonoBehaviour,IGameVfx
    {
        public SpriteAnimation summonEffect;
        private CardView _card;
        private GameCardView _gameCard;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }
        public void Initialize(CardView card)
        {
            _card = card;
            _gameCard = card.GetCardComponent<GameCardView>();
        }

        public void OnAfterCreation()
        {
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            var tile = _gameCard.TileState?.GetView();
            if (tile) transform.SetParent(tile.effectsContainer);
            gameObject.SetActive(true);

            var cardRect = _card.cardObject;
            cardRect.localScale = Vector3.zero;
            _card.cardObjectCanvas.alpha = 1;

            summonEffect.Play("appear");
            AudioController.PlaySound("vfx_transform_poof", "BARDSFX", true, gameObject);
            while (!summonEffect.IsDone) yield return Timing.WaitForOneFrame;

            summonEffect.Play("open");
            AudioController.PlaySound("vfx_brigade_trap_open", "BARDSFX", true, gameObject);
            while (!summonEffect.IsDone) yield return Timing.WaitForOneFrame;

            cardRect.localScale = new Vector3(0.24f, 0.24f, 1f);

            yield return Timing.WaitForSeconds(0.04f);

            cardRect.DOAnchorPosY(20f, 0.04f);
            AudioController.PlaySound("map_jump", "BARDSFX", true, gameObject);
            yield return Timing.WaitForSeconds(0.04f);

            cardRect.DOScale(Vector3.one, 0.2f).SetEase(Ease.OutBack);

            yield return Timing.WaitForSeconds(0.16f);

            cardRect.DOAnchorPosY(0, 0.28f).SetEase(Ease.OutBack);

            GameplayScreen.Instance.ShakeScreen();
            summonEffect.Play("despawn", () => gameObject.SetActive(false));
            //AudioController.PlaySound("vfx_brigade_trap_close", "BARDSFX", true, gameObject);
        }
    }
}