using System.Collections.Generic;
using System.Linq;
using Asyncoroutine;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class BlizzardEffect : MonoBehaviour, IGameVfx
    {
        public FrostShotProjectile projectilePrefab;
        public GameObject snowParticles;
        public CanvasGroup overlay;

        public void Initialize(CardView card){}

        public void Initialize(Transform rect) {}

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            overlay.DOFade(1, .5f);

            // slight delay so we can see the cool snow for longer
            yield return Timing.WaitForSeconds(0.5f);

            // get your initial data, this should always happen
            var getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(command);
            yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
            var vfxData = getSourceAndTargetData.Result;
            var handles = new List<CoroutineHandle>();

            // // get all effects associated with this card (Bitter Winter)
            var commandList = GameServer.State.GetCardHistories(vfxData.sourceCard.uid);
            // create a list of summons commands to store our summons to resolve later
            var summonCommands = new List<Command>();

            // cancel summons from view resolver, we will resolve them here (special).
            for (var i = 0; i < commandList.Count; i++)
            {
                var cmd = commandList[i];
                if (cmd.Type == CommandType.SUMMON)
                {
                    summonCommands.Add(cmd);
                    Server.CommandResolver.RemoveCommandFromResolve(cmd);
                }
            }

            // shuffling the order around to make it look better
            var order = new List<int>();

            for (var i = 0; i < vfxData.targetCards.Count; i++)
            {
                order.Add(i);
            }

            order.Shuffle();

            for (var i = 0; i < order.Count; i++)
            {
                var frostShotProjectile = Instantiate(projectilePrefab, transform, false);
                handles.Add(Timing.RunCoroutine(frostShotProjectile.Play(vfxData, command, order[i], true, true, false, 40)));
                yield return Timing.WaitForSeconds(Random.Range(.24f, .4f));
            }

            //yield return Timing.WaitUntilDone(GameVfxUtilities.EvaluateDeath(vfxData.targetCardObjects));
            yield return Timing.WaitForSeconds(0.5f);

            var summons = new List<GameCardView>();

            // simple summon, I'd like to do something more extravagant for this but for now this works!
            for (var i = 0; i < summonCommands.Count; i++)
            {
                var summonCommand = (SummonCommand)summonCommands[i];
#pragma warning disable CS4014
                yield return Timing.WaitUntilDone( SummonCommand.SummonBoardCards(summonCommand, summonCommand.GetAbility(), summons).AsCoroutine());
#pragma warning restore CS4014
            }

            if (summons.Count > 0)
            {
                var lastSummon = summons[summons.Count - 1];
                while (lastSummon.GameState != GameCardView.GameStateType.None) yield return Timing.WaitForOneFrame;
            }

            while (handles.Exists(handle => handle.IsRunning)) yield return Timing.WaitForOneFrame;



            overlay.DOFade(0, 1f);

            yield return Timing.WaitForSeconds(0.5f);

            var snowParticleSystems = snowParticles.GetComponentsInChildren<ParticleSystem>();

            for (var i = 0; i < snowParticleSystems.Length; i++)
            {
                snowParticleSystems[i].Stop(true, ParticleSystemStopBehavior.StopEmitting);
            }

            // wait until the explosion particles in the projectile prefab have dissipated
            Destroy(gameObject, 3f);
        }
    }
}