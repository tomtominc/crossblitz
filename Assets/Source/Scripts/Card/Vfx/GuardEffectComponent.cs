using CrossBlitz.GameVfx.Minion;

namespace CrossBlitz.Card.Vfx
{
    public class GuardEffectComponent : GameVfxComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<GuardEffect>("GuardEffect", card.cardObject);
            effect.Initialize(card);
        }
    }
}