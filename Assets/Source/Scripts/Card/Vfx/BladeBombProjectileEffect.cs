using System;
using System.Collections;
using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AssetManagement;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using DG.Tweening;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.GameVfx.Spell
{
    public class BladeBombProjectileEffect : MonoBehaviour, IGameVfx
    {
        private const float Inset = -50f;
        private const float MaxTime = 0.5f;

        public SpriteAnimation projectileAnim;
        public CanvasGroup spinBladeCanvas;

        private bool _updateCollisions;
        private RectTransform _projectileRect;
        private Vector2 _moveDirection;
        private Rect _moveBounds;
        private float _moveSpeed;
        private float _moveAccel;
        private float _rotateAngle;
        private float _rotateSpeed;
        private string _animName;
        private BladeBombBarrelEffect _parent;

        private bool _finished;
        private float _bounceCount;
        private float _currentTime;

        private List<RectTransform> _currentlyColliding;
        private List<GameCardView> _testCollisionsWith;
        private List<GameCardView> _collidedWith;

        private DamageCommand _command;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {

        }

        public void Setup(Vector2 moveDirection, string animName, List<GameCardView> collidedWith)
        {
            _collidedWith = collidedWith;
            _moveDirection = moveDirection;
            _rotateAngle = Random.Range(0, 360);
            _rotateSpeed = 10f;
            _animName = animName;
            gameObject.SetActive(true);
            projectileAnim.Play(_animName);
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            _command = command as DamageCommand;
            _testCollisionsWith = new List<GameCardView>();
            //_collidedWith = new List<GameCardView>();
            _currentlyColliding = new List<RectTransform>();
            _projectileRect = projectileAnim.RectTransform();
            _projectileRect.anchoredPosition = Vector2.zero;
            
            for (var i = 0; i < _command?.TargetUids.Count; i++)
            {
                var getBoardCardTask = GameBoard.GetBoardCard(_command.TargetUids[i]);
                yield return Timing.WaitUntilDone(getBoardCardTask.AsCoroutine());
                var getBoardCardResult = getBoardCardTask.Result;

                if (getBoardCardResult.error != ClientErrorCode.None)
                {
                    Server.HandleClientError(getBoardCardResult, false);
                }

                if (getBoardCardResult.boardCard)
                {
                    _testCollisionsWith.Add(getBoardCardResult.boardCard);
                }
            }

            _moveSpeed = 8;
            _updateCollisions = true;

            while (!_finished) yield return Timing.WaitForOneFrame;
            _updateCollisions = false;

            yield return Timing.WaitUntilDone(spinBladeCanvas.DOFade(0, 0.5f).WaitForCompletion(true));
        }

        private void Update()
        {
            if (_updateCollisions)
            {
                _currentTime += Time.deltaTime;
                UpdateCollisions();
            }
            MoveProjectile();
        }

        private void MoveProjectile()
        {
            var pos = _projectileRect.anchoredPosition;
            pos += _moveDirection.normalized * _moveSpeed;
            _moveAccel += (0.35f * Time.deltaTime);
            _moveSpeed -= _moveAccel;

            if(_moveSpeed <= 0.75f)
            {
                _moveSpeed = 0.75f;
            }

            _rotateSpeed -= _moveAccel;

            _rotateAngle += _rotateSpeed;

            _projectileRect.anchoredPosition = pos;
            _projectileRect.transform.rotation = Quaternion.Euler(new Vector3(0, 0, _rotateAngle));
        }

        private void UpdateCollisions()
        {
            for (var i = 0; i < _testCollisionsWith.Count; i++)
            {
                if (_testCollisionsWith[i] == null) continue;

                var rect = _testCollisionsWith[i].RectTransform();

                if (Vector2.Distance(_projectileRect.position, rect.position) < 2)
                {
                    if (_currentlyColliding.Contains(rect)) continue;

                    _currentlyColliding.Add(rect);

                    if (!_collidedWith.Contains(_testCollisionsWith[i]))
                    {
                        _collidedWith.Add(_testCollisionsWith[i]);
                        _testCollisionsWith[i].State.ApplyHistoryByCardGuid(_command.SourceUid);
                        var damageFx = _testCollisionsWith[i].View.GetVfx<DamageEffectComponent, DamageEffect>();
                        Timing.RunCoroutine(damageFx.GenericDamageAnimation(_command.Amount));
                        GameplayScreen.Instance.ShakeScreen(4, 2);
                        //AudioController.PlaySound("vfx_sword_slash", "BARDSFX", true, gameObject);
                    }
                }
                else if (_currentlyColliding.Contains(rect))
                {
                    _currentlyColliding.Remove(rect);
                }
            }

            // we collided with everything!
            if (_collidedWith.Count == _testCollisionsWith.Count )
            {
                _finished = true;
            }

        }

        public void Unload()
        {
            AddressableReferenceLoader.Unload(gameObject);
        }
    }
}