using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using DG.Tweening;
using UnityEngine;

namespace CrossBlitz.GameVfx
{
    public class MulliganCrossEffect : MonoBehaviour, IGameVfx
    {
        public SpriteAnimation crossAnimator;

        private CardView _view;

        public void Initialize(CardView card)
        {
            _view = card;
        }

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Show(bool show)
        {
            crossAnimator.SetActive(show);
            DOTween.Kill(_view.cardObject);
            DOTween.Kill(_view.frontShadowRect);

            if (show)
            {
                crossAnimator.Play("spawn");
                _view.TintOverTime(new Color(0f, 0f, 0f, 0.6f), BlendMode.Multiply, 0.25f);
                _view.cardObject.DOAnchorPosY(-5f, 0.25f);
                _view.frontShadowRect.DOAnchorPos(new Vector2(4, -4), 0.25f);
            }
            else
            {
                _view.Tint(Color.white, BlendMode.Overlay);
                _view.RemoveTintOverTime(0.125f);
                _view.cardObject.DOPunchScale(Vector3.one * 0.16f, 0.25f);
                _view.cardObject.DOAnchorPosY(0, 0.125f);
                _view.frontShadowRect.DOAnchorPos(new Vector2(10,-10), 0.125f);
            }
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            throw new System.NotImplementedException();
        }

        public void Unload()
        {
           //throw new System.NotImplementedException();
        }
    }
}