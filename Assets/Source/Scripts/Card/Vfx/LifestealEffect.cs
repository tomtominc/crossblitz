using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class LifestealEffect : MonoBehaviour, IGameVfx
    {
        private const int SpawnPreFrames = 6;
        private const float FrameTime = 0.04f;
        private const float ForthFrameHoldTime = 0.12f;

        public SpriteAnimation animatorFront;
        public CanvasGroup animatorFrontCanvas;

        public SpriteAnimation animatorBack;
        public CanvasGroup animatorBackCanvas;
        public bool onTop;

        private CardView _card;
        private GameCardView _gameCard;
        private bool _isActive;

        private RectTransform _rect;
        public void Initialize(Transform rect)
        {
            _rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
            _isActive = false;
            _gameCard = _card.GetCardComponent<GameCardView>();
            _gameCard.Events.OnSilenced += OnSilenced;
            _gameCard.Events.OnTransformed += OnTransformed;

            animatorBack.transform.SetParent(_card.cardObject, false);
            animatorBack.transform.SetSiblingIndex(0);
        }

        public IEnumerator<float> Spawn()
        {
            _isActive = true;

            gameObject.SetActive(true);
            animatorFront.SetActive(false);
            animatorBack.SetActive(false);

            var cardRect = _card.cardObject;

            for (var i = 0; i < SpawnPreFrames; i++)
            {
                switch (i)
                {
                    case 0: cardRect.anchoredPosition = new Vector2(-1,-2);
                        break;
                    case 1: cardRect.anchoredPosition = new Vector2(1,-3);
                        break;
                    case 2: cardRect.anchoredPosition = new Vector2(-1,-6);
                        break;
                    case 3: cardRect.anchoredPosition = new Vector2(0,-7);
                        break;
                    case 4: cardRect.anchoredPosition = new Vector2(0,-8);
                        break;
                }
                if (i == 4) yield return Timing.WaitForSeconds(ForthFrameHoldTime);
                else yield return Timing.WaitForSeconds(FrameTime);
            }

            animatorFront.SetActive(true);
            animatorBack.SetActive(true);

            cardRect.anchoredPosition = new Vector2(0,0);

            AudioController.PlaySound("vfx_lifesteal", "BARDSFX", true, gameObject);
            animatorFront.Play("spawn", OnLifestealSpawnUpdate);
            animatorBack.Play("spawn");

            while (animatorFront.CurrentFrame == 0) yield return Timing.WaitForOneFrame;

            while (!animatorBack.IsDone) yield return Timing.WaitForOneFrame;
            animatorBack.Play("idle");

            while (!animatorFront.IsDone) yield return Timing.WaitForOneFrame;
            animatorFront.Play("idle");
        }

        private void OnLifestealSpawnUpdate(int frame)
        {
            var cardRect = _card.cardObject;

            cardRect.anchoredPosition = frame switch
            {
                0 => new Vector2(0, 4),
                1 => new Vector2(-1, 7),
                2 => new Vector2(0, 10),
                3 => new Vector2(0, 11),
                4 => new Vector2(0, 10),
                5 => new Vector2(0, 8),
                6 => new Vector2(0, 5),
                7 => new Vector2(0, 1),
                8 => new Vector2(0, -1),
                9 => new Vector2(0, 0),
                _ => cardRect.anchoredPosition
            };
        }

        private void OnSilenced(GameCardView card)
        {
            RemoveLifesteal(card);
        }

        private void OnTransformed(GameCardView card)
        {
            if (card.State.HasAbility(AbilityKeyword.LIFESTEAL))
            {
                return;
            }

            RemoveLifesteal(card);
        }

        private void RemoveLifesteal(GameCardView card)
        {
            if (!_isActive)
            {
                return;
            }

            animatorFrontCanvas.DOFade(0f,.25f).OnComplete(() => animatorFront.SetActive(false));
            animatorBackCanvas.DOFade(0f, .25f).OnComplete(() => animatorBack.SetActive(false));
            //animator.SetActive(false);
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            throw new System.NotImplementedException();
        }
    }
}