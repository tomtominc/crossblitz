using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;

namespace CrossBlitz.Card.Vfx
{
    public class AttackEffectComponent : CustomAttackComponent
    {
        public override async void LoadEffect()
        {
            effect = await LoadEffect<AttackEffect>("AttackEffect", card.cardObject);
            effect.Initialize(card);
        }
    }
}