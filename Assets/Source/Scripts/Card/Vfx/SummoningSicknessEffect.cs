using System;
using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using TakoBoyStudios.Events;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.GameVfx.Minion
{
    public class SummoningSicknessEffect : MonoBehaviour, IGameVfx
    {
        public SpriteAnimation waitEffect;
        public SpriteAnimation tint;
        public float tintSpeed = 10;

        private CardView m_cardView;
        private GameCardView m_gameCardView;
        private bool m_alive;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            m_cardView = card;
            m_gameCardView = m_cardView.GetCardComponent<GameCardView>();
            tint.image.color = "#2E1D26".ToColor(0f);

            if (m_cardView.data.type == CardType.Minion)
            {
                if (!tint.Play($"overlay-{m_cardView.data.type.ToString().ToLower()}-{m_cardView.data.attackType.ToString().ToLower()}"))
                {
                    tint.Play($"overlay-{m_cardView.data.type.ToString().ToLower()}");
                }
            }
            else
            {
                tint.Play($"overlay-{m_cardView.data.type.ToString().ToLower()}");
            }

            m_cardView.OnStartAnimating += OnStartAnimating;
            m_cardView.OnEndAnimating += OnEndAnimating;

            m_gameCardView.Events.OnDeath += OnDeath;

            var boardTarget = m_cardView.GetCardComponent<BoardTarget>();
            boardTarget.OnShowTarget += OnShowTarget;
            boardTarget.OnHideTarget += OnHideTarget;

            Events.Subscribe( EventType.OnTurnStart, OnTurnStarted);
        }

        private void OnStartAnimating()
        {
            gameObject.SetActive(false);
            tint.image.color =  "#2E1D26".ToColor(0f);
        }

        private void OnEndAnimating()
        {
            gameObject.SetActive(true);
        }

        private void OnDeath(GameCardView gameCard)
        {
            if (m_alive)
            {
                gameObject.SetActive(false);
            }
        }

        private void OnShowTarget(BoardTarget boardTarget)
        {
            if (m_alive)
            {
                gameObject.SetActive(false);
            }
        }

        private void OnHideTarget(BoardTarget boardTarget)
        {
            if (m_alive)
            {
                gameObject.SetActive(true);
            }
        }

        private void OnDestroy()
        {
            Events.Unsubscribe( EventType.OnTurnStart, OnTurnStarted);
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            yield break;
        }

        private void Update()
        {
            if (m_alive && tint.image.color.a < 0.4f)
            {
                var color = tint.image.color;
                color.a += tintSpeed * Time.deltaTime;
                tint.image.color = color;
            }

            if (!m_alive && tint.image.color.a > 0)
            {
                var color = tint.image.color;
                color.a -= tintSpeed * Time.deltaTime;
                tint.image.color = color;
            }

            if (m_alive)
            {

            }
        }

        public void StartWait()
        {
            m_alive = true;
            gameObject.SetActive(true);
            //Timing.RunCoroutine(StartWaitRoutine().CancelWith(gameObject));
        }

        private IEnumerator<float> StartWaitRoutine()
        {
            yield return Timing.WaitForSeconds(0.4f);
            gameObject.SetActive(true);
        }

        private void OnEnable()
        {
            if (m_alive)
            {
                waitEffect.SetActive(true);
                waitEffect.Play("spawn", () => { waitEffect.Play("idle"); });
            }
        }

        private void OnTurnStarted( IMessage message )
        {
            if (!m_alive) return;

            if (message.Data is OnTurnStartEventArgs args && m_gameCardView != null && m_gameCardView.State != null)
            {
                var currentPlayer = GameServer.GetPlayerState(GameServer.State.CurrentPlayer);
                if (currentPlayer.GetTeam() != m_gameCardView.State.Team)
                {
                    EndWait();
                }
            }
        }

        public void EndWait()
        {
            m_alive = false;

            m_cardView.OnStartAnimating -= OnStartAnimating;
            m_cardView.OnEndAnimating -= OnEndAnimating;
            m_gameCardView.Events.OnDeath -= OnDeath;
            var boardTarget = m_cardView.GetCardComponent<BoardTarget>();
            boardTarget.OnShowTarget -= OnShowTarget;
            boardTarget.OnHideTarget -= OnHideTarget;

            waitEffect.Play("despawn", () =>
            {
                Destroy(gameObject);
            });
        }
    }
}