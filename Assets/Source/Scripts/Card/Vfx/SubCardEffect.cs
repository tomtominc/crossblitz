using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.Databases;
using CrossBlitz.InputAPI;
using DG.Tweening;
using Source.Scripts.Card;
using UnityEngine;

namespace CrossBlitz.GameVfx
{
    public class SubCardEffect : MonoBehaviour, IGameVfx
    {
        public Canvas subCardInstructionCanvas;
        public RectTransform subCardParent;

        private CardView _card;
        private int _targetCardIndex;
        private int _currentCardIndex;
        private int _maxCardIndex;
        private List<CardView> _cards;
        private string _lastCardId;
        private bool _animating;
        private int _originalSortingOrder;
        private bool _nextCard;
        private bool _hasTokens;
        private bool _init;
        private HoverCard _hoverCard;
        private string _gameCardUid;

        private const float OffsetX = 10;
        private const float OffsetY = -8;

        private RectTransform m_rect;

        const float moveDuration = 0.24f;
        const float delayBetweenCards = moveDuration - (moveDuration * 0.3f);
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
            _hoverCard = _card.transform.parent != null ? _card.transform.parent.GetComponent<HoverCard>() : null;
            _lastCardId = _card.data?.id;
            _originalSortingOrder = _card.GetSortOrder();

            this.SetActive(true);

            RefreshSubCards();

           // Events.Subscribe(EventType.OnCardHoveredOver, OnCardHovered);
        }

        public void SetGameCardUid(string gameCardUid)
        {
            _gameCardUid = gameCardUid;
        }

        private void OnEnable()
        {
            _currentCardIndex = 0;
            _lastCardId = _card.data.id;
            RefreshSubCards();
        }

        private void Update()
        {
            if (!gameObject.activeSelf)
            {
                return;
            }

            if (_nextCard)
            {
                _nextCard = false;
                NextCard();
            }



            if (_targetCardIndex != _currentCardIndex && !_animating)
            {
                _animating = true;

                var i = 0;
                var currIndex = _currentCardIndex;

                while (i < _cards.Count)
                {
                    // do movement
                    var tokenCard = _cards[currIndex];
                    tokenCard.cardBack3D.SetActive(false);
                    tokenCard.frontMeldAnimator.SetActive(false);

                    if (tokenCard.keywordDescriptionPopup != null)
                    {
                        tokenCard.keywordDescriptionPopup.KillAnimation();
                        tokenCard.keywordDescriptionPopup.canvasGroup.alpha = 0;
                        tokenCard.keywordDescriptionPopup.modifierTooltip.canvasGroup.alpha = 0;
                    }

                    if (currIndex == _targetCardIndex) // the display card!
                    {
                        var seq = DOTween.Sequence();
                        // rotate to -6 and move to the right -101
                        // rotate to 6 and move to center
                        // rotate to 0

                        var dur = moveDuration * 0.8f;
                        var ease = Ease.OutBack;

                        seq.Append(tokenCard.cardObject.DOLocalRotate(new Vector3(0, 0, -6), dur).SetEase(ease));
                        seq.Join(tokenCard.cardObject.DOAnchorPos(new Vector3(121f, 0, 0), dur).SetEase(ease));
                        seq.Append(tokenCard.cardObject.DOLocalRotate(new Vector3(0, 0, 0), dur).SetEase(ease));
                        seq.Join(tokenCard.cardObject.DOAnchorPos(new Vector3(0, 0, 0), dur).SetEase(ease));
                        seq.Join(tokenCard.cardObject.DOScale(1, dur).SetEase(Ease.OutBack));
                        seq.InsertCallback(dur-0.08f, () => { tokenCard.SetSortOrder(_originalSortingOrder-1); });
                        //seq.Append(tokenCard.cardObject.DOLocalRotate(new Vector3(0, 0, 0), moveDuration).SetEase(Ease.OutBack));
                        var index = currIndex;
                        seq.AppendCallback(() =>
                        {
                            _animating = false;
                            tokenCard.keywordDescriptionPopup.AnimateIn(0f, _hoverCard);
                        });
                    }
                    else
                    {
                        var rotateTo = new Vector3(0, 0, 6 + (2 * i-1));
                        var moveTo = new Vector2(-14f + (-7 * i-1), 0f);
                        var scaleTo = .95f;
                        var i0 = i;
                        tokenCard.cardObject.DOAnchorPos(moveTo, moveDuration).SetEase(Ease.OutBack);
                        tokenCard.cardObject.DOScale(scaleTo, moveDuration);
                        tokenCard.cardObject.DOLocalRotate(rotateTo, moveDuration).SetEase(Ease.OutBack)
                            .OnComplete(() =>
                            {
                                tokenCard.SetSortOrder(_originalSortingOrder - i0 - 2);
                            });
                    }

                    i++;

                    currIndex++;
                    if (currIndex >= _cards.Count) currIndex = 0;
                }

                _currentCardIndex = _targetCardIndex;

                // var animateOut = _cards[_currentCardIndex];
                // var nextCard = _currentCardIndex + 1;
                // if (nextCard >= _maxCardIndex) nextCard = 0;
                // var i = 0;
                //
                // var seq = DOTween.Sequence();
                // seq.Append(animateOut.cardObject.DOAnchorPos(new Vector2(-101, 0), 0.12f));
                // while (nextCard != _currentCardIndex)
                // {
                //     var card = _cards[nextCard];
                //     var i0 = i;
                //     seq.Join(card.cardObject.DOAnchorPos(new Vector2(OffsetX * i, OffsetY * i), 0.12f)
                //         .OnComplete(()=> card.SetSortOrder(_originalSortingOrder-i0)));
                //     nextCard++;
                //     i++;
                //     if (nextCard >= _maxCardIndex) nextCard = 0;
                // }
                //
                // seq.AppendCallback(() =>
                // {
                //     animateOut.SetSortOrder(_originalSortingOrder - _maxCardIndex);
                // });
                //
                // seq.Append(animateOut.cardObject.DOAnchorPos(new Vector2(OffsetX * (_maxCardIndex-1), OffsetY*(_maxCardIndex-1)), 0.12f));
                // seq.AppendCallback(() =>
                // {
                //     _animating = false;
                //     _currentCardIndex++;
                //     if (_currentCardIndex >= _maxCardIndex) _currentCardIndex = 0;
                // });
            }
        }

        private void RefreshSubCards()
        {
            if (_cards != null)
            {
                for (var i = 0; i < _cards.Count; i++)
                {
                    _cards[i].cardObject.DOKill();
                }
            }

            subCardParent.DestroyChildren();

            _cards = new List<CardView> { _card };
            LoadSubCardsIfApplicable();
        }

        private async void LoadSubCardsIfApplicable()
        {
            if (_card.data == null)
            {
                return;
            }

            if (_card != null)
            {
                _card.cardObject.anchoredPosition = Vector2.zero;
                _card.cardObject.localScale = Vector3.one;
                _card.cardObject.eulerAngles = Vector3.zero;

                // if (!string.IsNullOrEmpty(_gameCardUid))
                // {
                //     _card.keywordDescriptionPopup.SetCardState(_gameCardUid);
                // }
                //
                // if (_card.keywordDescriptionPopup.HasKeywordsToDisplay())
                // {
                //     _card.keywordDescriptionPopup.AnimateIn(0f, _hoverCard);
                // }
                // else
                // {
                //     _card.keywordDescriptionPopup.ShowModifierTooltip(0);
                // }
            }

            var tokens = _card.data.GetTokens();

            tokens = tokens.Distinct().ToList();

            var addressableId = _card.viewType == CardView.CardViewType.STANDARD ? "Card_Standard" : "Card_Zoomed";

            var delay = HoverCard.GetZoomDelay() + delayBetweenCards;

            _maxCardIndex = tokens.Count + 1;
            _currentCardIndex = 0;
            _targetCardIndex = 0;
            _hasTokens = false;

            for (var i = 0; i < tokens.Count; i++)
            {
                var cardData = Db.CardDatabase.GetCard(tokens[i]);

                if (cardData == null) continue;

                var obj = await AddressableReferenceLoader.GetAsset(addressableId);
                var settings = new CreateCardFactorySettings
                {
                    CardPrefab = obj,
                    CardData = cardData,
                    SortingOrder =_card.GetSortOrder()-(i+1),
                    SortingLayer = _card.GetSortingLayer(),
                    Layout = subCardParent,
                    // CardObjectOffsetPosition = new Vector2(OffsetX * (i+1), OffsetY * (i+1))
                };

                var tokenCard = CardFactory.Create(settings, _cards);
                tokenCard.CanvasGroup.interactable = false;
                tokenCard.CanvasGroup.blocksRaycasts = false;
                tokenCard.SetOutlineAlpha(0);
                tokenCard.cardBack3D.SetActive(false);
                tokenCard.frontMeldAnimator.SetActive(false);

                // tokenCard.Tint(Color.black, BlendMode.Multiply);

                var rotateTo = new Vector3(0, 0, 6 + (2 * i));
                var moveTo = new Vector2(-14f + (-7 * i), 0f);
                var scaleTo = .95f;


                // tokenCard.cardObject.anchoredPosition = new Vector2(-14f, 0f);
                // tokenCard.cardObject.eulerAngles = new Vector3(0, 0, 6);
                // tokenCard.cardObject.localScale = new Vector3(.95f, .95f, .95f);

                tokenCard.cardObject.DOAnchorPos(moveTo, moveDuration).SetEase(Ease.OutBack).SetDelay(delay + delayBetweenCards * i);
                tokenCard.cardObject.DOScale(scaleTo, moveDuration).SetDelay(delay + delayBetweenCards * i);
                tokenCard.cardObject.DOLocalRotate(rotateTo, moveDuration).SetEase(Ease.OutBack).SetDelay(delay + delayBetweenCards * i);

                //x=-14, zrot = 6, scale = .95
                // if (_card.data != null)
                // {
                //     // var itemAmount = App.Inventory.GetItemAmount(_card.data.id);
                //     //
                //     // if (itemAmount <= 0 && tokenCard.frontMeldAnimator)
                //     // {
                //     //     tokenCard.frontMeldAnimator.SetActive(true);
                //     //     if (DeckEditMenu.Instance) tokenCard.UpdateFrontMeldAnimator();
                //     // }
                //     // else
                //     // {
                //     //     tokenCard.frontMeldAnimator.SetActive(false);
                //     // }
                //
                //
                // }
            }

            if (tokens.Count > 0 && subCardInstructionCanvas)
            {
                subCardInstructionCanvas.SetActive(true);
                subCardInstructionCanvas.sortingLayerName = _card.GetSortingLayer();
                subCardInstructionCanvas.sortingOrder = _card.GetSortOrder() + 1;

                _hasTokens = true;
            }
            else if (subCardInstructionCanvas)
            {
                subCardInstructionCanvas.SetActive(false);
            }

            _init = true;
        }

        private void NextCard()
        {
            _targetCardIndex--;
            if (_targetCardIndex < 0) _targetCardIndex = _cards.Count-1;
        }

        public void MoveToNextCard()
        {
            _nextCard = true;
        }

        public bool HasTokens()
        {
            return _hasTokens;
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            throw new System.NotImplementedException();
        }
    }
}