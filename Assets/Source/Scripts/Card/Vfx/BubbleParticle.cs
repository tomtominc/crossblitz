using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.GameVfx;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card.Vfx
{
    public class BubbleParticle : MonoBehaviour
    {
        public IEnumerator<float> Play(VfxSourceAndTargetData vfxData, Command command, int targetIndex, Transform overrideStartFrom)
        {
            transform.position = overrideStartFrom.position;
            var targetPosition = vfxData.targetCardPositions[targetIndex];
            yield return Timing.WaitForSeconds(.5f);
            AudioController.PlaySound("vfx_boiling", "BARDSFX", true, gameObject);
            yield return Timing.WaitUntilDone(transform.DOMove(targetPosition, 20).SetSpeedBased(true).WaitForCompletion(true));
            yield return Timing.WaitForSeconds(.5f);

            var trailSystems = gameObject.GetComponentsInChildren<ParticleSystem>();

            for (var i = 0; i < trailSystems.Length; i++)
            {
                trailSystems[i].Stop(true, ParticleSystemStopBehavior.StopEmitting);
            }
        }
    }
}