using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.Visuals;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class SacrificialStageEffect : MonoBehaviour, IGameVfx
    {
        private const float FrameTime = 0.08f;
        public SpriteAnimation stabAnim;
        public Color overlayRed;
        public Color overlayBlack;

        private CardView _card;
        private GameCardView _gameCard;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }
        public void Initialize(CardView card)
        {
            _card = card;
            _gameCard = _card.GetCardComponent<GameCardView>();
        }

        /// <summary>
        /// Play is used when spawning the effect on top of the card.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerator<float> Play(Command command, VfxParameters vfxParams)
        {
            // by default the effects are always inactive, we'll need to make them active when we play them.
            gameObject.SetActive(true);

            // get a reference to the card object (child object of the card so we aren't moving the card directly.
            var cardRect = _card.cardObject;
            var fadeTime = 4 * FrameTime;

            // Play Stab
            stabAnim.Play("stab");

            yield return Timing.WaitForSeconds(0.35f);

            _card.Tint(overlayBlack, BlendMode.Darken);
            cardRect.DOPunchAnchorPos(Vector2.down * 5f, 0.35f);
            AudioController.PlaySound("vfx_sword_slash", "BARDSFX", true, gameObject);
            yield return Timing.WaitForSeconds(0.15f);

            _card.Tint(overlayRed, BlendMode.Darken);
            yield return Timing.WaitForSeconds(0.15f);


            _card.RemoveTintOverTime(0.20f);
            yield return Timing.WaitForSeconds(0.35f);

            Destroy(gameObject);

        }
    }
}