using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Card;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.ServerAPI;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Spell
{
    public class RainingKnivesBoardEffect : MonoBehaviour, IGameVfx
    {
        public KnifeDamageEffect knifeEffectPrefab;

        private RectTransform m_rect;
        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }
        public void Initialize(CardView card){}

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            gameObject.SetActive(true);

            var getSourceAndTargetData = VfxSourceAndTargetData.GetVfxData(command);
            yield return Timing.WaitUntilDone(getSourceAndTargetData.AsCoroutine());
            var vfxData = getSourceAndTargetData.Result;

            for (var i = 0; i < vfxData.targetCards.Count; i++)
            {
                var knifeEffect = Instantiate(knifeEffectPrefab, transform, false);

                if (i == vfxData.targetCards.Count - 1)
                {
                    yield return Timing.WaitUntilDone(knifeEffect.Play(vfxData, command, i));
                }
                else
                {
                    Timing.RunCoroutine(knifeEffect.Play(vfxData, command, i));
                    yield return Timing.WaitForSeconds(.075f);
                }
            }

            yield return Timing.WaitForOneFrame;

            Destroy(gameObject);
        }
    }
}