using System;
using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using TakoBoyStudios.Events;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.GameVfx.Minion
{
    public class ThornsEffect : MonoBehaviour, IGameVfx
    {
        public SpriteAnimation animator;
        public SpriteAnimation thornsPowerContainer;

        private CardView _card;
        private GameCardView _gameCard;

        private RectTransform m_rect;

        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
        }

        public void Initialize(CardView card)
        {
            _card = card;
            _gameCard = _card.GetCardComponent<GameCardView>();

            var childIndex = _card.front.transform.GetSiblingIndex()-1;
            transform.SetSiblingIndex(childIndex);

            // thornsPowerContainer.transform.SetParent(_card.attackTypeContainer.transform,false);
            // thornsPowerContainer.RectTransform().anchoredPosition = Vector2.zero;
            thornsPowerContainer.SetActive(false);

            _gameCard.Events.OnSilenced += OnSilenced;

            Events.Subscribe(EventType.OnTurnStart, OnTurnStart);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnTurnStart, OnTurnStart);

            //Destroy(thornsPowerContainer.gameObject);

        }

        private void OnSilenced(GameCardView cardView)
        {
            Timing.RunCoroutine(Despawn());
        }

        private void OnTurnStart(IMessage message)
        {
            if (message.Data is OnTurnStartEventArgs args)
            {
                if (_gameCard != null && _gameCard.State != null && !_gameCard.State.silenced)
                {
                    var thorns = _gameCard.State.GetAbility(AbilityKeyword.THORNS);

                    if (thorns != null)
                    {     
                        if (_gameCard.State.Team == GameServer.State.CurrentTeamTurn())
                        {
                            Timing.RunCoroutine(Despawn());
                        }
                        else
                        {
                            Timing.RunCoroutine(Spawn());
                        }
                    }
                }
            }
        }

        public IEnumerator<float> Spawn()
        {
            gameObject.SetActive(true);
            animator.SetActive(true);

            animator.Play("spawn");
            AudioController.PlaySound("vfx_thorns", "BARDSFX", true, gameObject);
            //thornsPowerContainer.Play("spawn");

            while (animator.CurrentFrame <= 0) yield return Timing.WaitForOneFrame;

            _gameCard.HitFromDirection(Vector2.up, 4);

            _card.Tint(Color.white, BlendMode.Screen);

            // if (_gameCard != null && _gameCard.State.IsSlow())
            // {
            //     _card.TintOverTime("#2E1D26".ToColor(0.4f), BlendMode.Darken, 0.4f);
            // }
            // else
            // {
            //     _card.RemoveTintOverTime(0.4f);
            // }

            _card.RemoveTintOverTime(0.4f);
            _gameCard.UpdateViewFromState(true);

            while (!animator.IsDone) yield return Timing.WaitForOneFrame;

            animator.Play("idle");

            if (_gameCard.State != null && _gameCard.State.Team == GameServer.State.CurrentTeamTurn())
            {
                Timing.RunCoroutine(Despawn());
            }
        }

        public IEnumerator<float> Despawn()
        {
            yield return Timing.WaitForSeconds(0.5f);

            _gameCard.UpdateViewFromState(true);

            animator.Play("despawn");
            //thornsPowerContainer.Play("despawn");

            while (!animator.IsDone) yield return Timing.WaitForOneFrame;

            animator.SetActive(false);
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            throw new System.NotImplementedException();
        }
    }
}