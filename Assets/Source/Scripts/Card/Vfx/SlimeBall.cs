using System;
using System.Collections.Generic;
using BlendModes;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Commands;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.GameVfx.Minion
{
    public class SlimeBall : MonoBehaviour
    {
        private const float MinJumpHeight = 0.5f;
        private const float MaxJumpHeight = 1f;
        private const float JumpTimePerMeter = 0.5f;

        public SpriteAnimation animator;

        private float _jumpHeight;
        private float _timeToJumpApex;
        private bool _updateJump;
        private float _timeInAir;
        private float _maxDistance;
        Vector3 _midPoint;

        private void FixedUpdate()
        {
            if (_updateJump)
            {
                _timeInAir += Time.deltaTime;

                var distance = Vector3.Distance(transform.position, _midPoint);
                var ratio = (1 * (distance / _maxDistance));
                transform.localScale = new Vector3(2 - ratio, 2 - ratio);
            }
        }

        public IEnumerator<float> Play(VfxSourceAndTargetData vfxData, Command command, int targetIndex)
        {
            var sourcePosition = vfxData.sourceCardPosition;
            var targetPosition = vfxData.targetCardPositions[targetIndex];
            var sourceCard = vfxData.sourceCardObject;

            transform.position = sourcePosition;

            animator.Play("glob-spawn");

            while (!animator.IsDone) yield return Timing.WaitForOneFrame;

            animator.Play("glob-idle");
            GameplayScreen.Instance.ShakeScreen();
            AudioController.PlaySound("vfx_cannonshot", "BARDSFX", true, gameObject);

            var distance = Vector3.Distance(sourcePosition, targetPosition);
            var direction = (sourcePosition - targetPosition).normalized;

            if (sourceCard != null)
            {
                sourceCard.View.Tint(Color.white, BlendMode.Lighten);
                sourceCard.View.RemoveTintOverTime(0.24f);
                sourceCard.View.cardObject.DOPunchAnchorPos(direction * 20f, 0.24f, 2, 0).SetEase(Ease.OutBack);
            }

            _jumpHeight = Mathf.Clamp(distance, MinJumpHeight, MaxJumpHeight);
            _timeToJumpApex = _jumpHeight * JumpTimePerMeter;
            _timeInAir = 0;
            _updateJump = true;

            _midPoint = (transform.position + targetPosition) / 2f;
            _maxDistance = Vector3.Distance(transform.position, _midPoint);

            yield return Timing.WaitUntilDone(transform.DOMove(_midPoint, _timeToJumpApex).SetEase(Ease.OutSine)
                .SetUpdate(UpdateType.Fixed).WaitForCompletion(true));

            yield return Timing.WaitUntilDone(transform.DOMove(targetPosition, _timeToJumpApex).SetEase(Ease.InSine)
            .SetUpdate(UpdateType.Fixed).WaitForCompletion(true));

            animator.Play("glob-explode");

            _updateJump = false;
            transform.localScale = Vector3.one;
        }
    }
}