using CrossBlitz.AssetManagement;
using Source.Scripts.Card;
using UnityEngine;

namespace CrossBlitz.Card
{
    public class CardCollectionComponentsLoader : MonoBehaviour, ICardComponent
    {
        public CardCollectionComponents cardCollectionComponent;
        public async void Initialize(CardView card)
        {
            var cardCollectionComponentObject = await AddressableReferenceLoader.Load("CardCollectionComponents", card.cardObject, false);
            cardCollectionComponent = cardCollectionComponentObject.GetComponent<CardCollectionComponents>();
            cardCollectionComponent.SetCardView(card);
        }

        public void OnAfterCreation()
        {
        }
    }
}