﻿using System;
using System.Collections;
using System.Collections.Generic;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Utils;
using GameDataEditor;
using Sirenix.OdinInspector;

namespace CrossBlitz.Card
{
    /// <summary>
    /// This holds values for a heroes level tree. This API should be expanded as we do more with the tree.
    /// </summary>
    [System.Serializable]
    public class LevelTree
    {
        public string archetype;
        public List<LevelReward> levels;

        public string GetLevelReport()
        {
            var details = "";

            var totalHealthBuffs = 0;
            var maxHealth = 20;

            var cardsOffered = 0;
            var cardsInOrder = new List<string>();
            var cardsDistinct = new List<string>();
            var cardsPairing = new Dictionary<string, int>();

            var totalManaShardsOffered = 0;
            var totalManaShards = 0;

            var totalIngredientsOffered = 0;
            const string completeStatus = "COMPLETE";
            const string notEnoughStatus = "NOT ENOUGH CARDS";
            const string tooManyStatus = "TOO MANY CARDS";

            var cardPairingStatus = "Card Status:";

            for (var i = 0; i < levels.Count; i++)
            {
                switch (levels[i].type)
                {
                    case LevelReward.EType.Card:
                        cardsOffered++;
                        if (!string.IsNullOrEmpty(levels[i].cardId))
                        {
                            cardsInOrder.Add(levels[i].cardId);
                            cardsDistinct.AddDistinct(levels[i].cardId);
                            var card = Db.CardDatabase.GetCard(levels[i].cardId);
                            if (cardsPairing.ContainsKey(card.id))
                            {
                                cardsPairing[card.id]++;
                            }
                            else
                            {
                                cardsPairing.Add(card.id, 1);
                            }
                        }
                        break;
                    case LevelReward.EType.HeathUp:
                        totalHealthBuffs++;
                        maxHealth += levels[i].healthPoints;
                        break;
                    case LevelReward.EType.ManaShards:
                        totalManaShardsOffered++;
                        totalManaShards += levels[i].manaShards;
                        break;
                    case LevelReward.EType.IngredientPouch:
                        totalIngredientsOffered++;
                        break;
                }
            }

            // var completedCards = 0;
            // var notEnoughCards = 0;
            // var tooManyCards = 0;
            //
            // foreach (var pair in cardsPairing)
            // {
            //     if (pair.Value == 2)
            //     {
            //         completedCards++;
            //     }
            //     else if (pair.Value < 2)
            //     {
            //         notEnoughCards++;
            //     }
            //     else
            //     {
            //         tooManyCards++;
            //     }
            // }
            //
            // if (notEnoughCards > 0)
            // {
            //     cardPairingStatus = $"{cardPairingStatus} {notEnoughStatus}";
            // }
            // else  if (tooManyCards > 0)
            // {
            //     cardPairingStatus = $"{cardPairingStatus} {tooManyStatus}";
            // }

            details += $"Max Health: {maxHealth}\n";
            details += $"Mana Shards: {totalManaShards}\n";
            details += $"Health Cards Offered: {totalHealthBuffs}\n";
            details += $"Cards Offered: {cardsOffered}\n";
            details += $"Distinct Cards Offered: {cardsDistinct.Count}\n";
            details += cardPairingStatus + "\n";
            details += $"Mana Shards Offered: {totalManaShardsOffered}\n";
            details += $"Ingredients Offered: {totalIngredientsOffered}\n";
            details += $"CARDS:\n";

            foreach (var pair in cardsPairing)
            {
                var card = Db.CardDatabase.GetCard(pair.Key);
                if (card != null)
                {
                    var count = 1;

                    if (card.rarity == Rarity.Common || card.rarity == Rarity.Rare)
                    {
                        count = 2;
                    }

                    details += $"{card.name} ({pair.Value}/{count})\n";
                }
            }
            return details;
        }
    }

    [System.Serializable]
    public class LevelReward
    {
        public enum EType
        {
            Card,
            HeathUp,
            ManaShards,
            IngredientPouch,
            TreasureChest,
            RelicSlot,
            ElderRelicSlot
        }

        [HorizontalGroup(0.8f)][ReadOnly]
        public int uid;
        [HorizontalGroup(0.2f)][Button] public void ResetUid() { uid = GUID.CreateUniqueInt(); }
        public EType type;
        [ShowIf("@this.type == EType.Card")]
        public string cardId;
        [ShowIf("@this.type == EType.HeathUp")]
        public int healthPoints;
        [ShowIf("@this.type == EType.ManaShards")]
        public int manaShards;
        [ShowIf("@this.type == EType.IngredientPouch")]
        public string ingredient;

        public int GetCardAmount()
        {
            if (string.IsNullOrEmpty(cardId)) return 0;
            var card = Db.CardDatabase.GetCard(cardId);
            if (card == null) return 0;

            switch (card.rarity)
            {
                case Rarity.Common:
                    return 8;
                case Rarity.Rare:
                    return 4;
                case Rarity.Mythic:
                    return 2;
                case Rarity.Legendary:
                    return 1;
            }

            return 1;
        }

// #if UNITY_EDITOR
//         public static ValueDropdownList<string> cards;
//
//         private static IEnumerable GetAllCards()
//         {
//             if (cards != null) return cards;
//
//             cards = new ValueDropdownList<string>();
//
//             if (!Db.Initialized)
//             {
//                 Db.ForceInit();
//             }
//
//             for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
//             {
//                 var cardData = Db.CardDatabase.Cards[i];
//                 cards.Add($"{cardData.faction}/{cardData.name}", cardData.id);
//             }
//
//             return cards;
//         }
// #endif
    }

    /// <summary>
    /// A card that is presented to the player for leveling up,
    /// heroes when leveled up will always present these as choices but can also
    /// be used in other context.
    /// </summary>
    [System.Serializable]
    public class LevelCard
    {
        public enum EType
        {
            Stat,
            Reward,
            Card,
            Feature,
            Hero
        }

        public EType type;

        [ShowIf("type", EType.Stat)]
        public string statId;

        [ShowIf("type", Value = EType.Reward)]
        public Reward reward;

        [ShowIf("type", Value = EType.Card)]
        [ValueDropdown("GetAllCards")]
        public string cardId;

        private static ValueDropdownList<string> cards;

        private static IEnumerable GetAllCards()
        {
            if (cards != null) return cards;

            cards = new ValueDropdownList<string>();

            for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
            {
                var cardData = Db.CardDatabase.Cards[i];
                cards.Add($"{cardData.faction}/{cardData.name}", cardData.id);
            }


            return cards;
        }
    }

    [System.Serializable]
    public class Reward
    {
        public RewardType type;
        public string reward;
        public int amount;
    }

    public enum RewardType
    {
        None,
        Card,
        Hero,
        Keystones,
        Gold,
        Power,
        Chest,
    }
}