using System;
using CrossBlitz.Card.Vfx;
using CrossBlitz.CardCollection;
using CrossBlitz.Cursors;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.GameVfx;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.Utils;
using CrossBlitz.ViewAPI.Popups;
using DG.Tweening;
using Source.Scripts.Card;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace CrossBlitz.Card
{
    /// <summary>
    /// This component is added if the card can be "dragged" on. This card is not dragged itself but
    /// creates a card that can be dragged - use this anytime you want to get "drag events"
    /// </summary>
    public class DragCard : MonoBehaviour,
        ICardComponent,
        IDragHandler,
        IBeginDragHandler,
        IEndDragHandler,
        IPointerEnterHandler,
        IPointerExitHandler,
        IPointerClickHandler,
      //  IPointerDownHandler,
      //  IPointerUpHandler,
        ICursorSelectable
    {
        private CardView _cardView;
        private DragCardDeckEditor _dragCard;
        private CardData _cardData;
        private ScrollRect _scrollRect;
        private bool _isScrolling;
        private bool _isInDeck;
        private bool _isDragging;
        private bool _isClicking;
        private bool _canClick;

        public virtual void Initialize(CardView card)
        {
            _cardView = card;
            _canClick = true;
        }

        public void OnAfterCreation()
        {

        }

        public virtual void SetCardData(CardData cardData)
        {
            _cardData = cardData;

            if (_cardView)
            {
                _cardView.SetCardData(_cardData);
            }
        }

        public virtual void SetDragCardDeckEditor(DragCardDeckEditor dragCard)
        {
            _dragCard = dragCard;
        }

        public virtual void SetScrollRect(ScrollRect scrollRect)
        {
            _scrollRect = scrollRect;
        }

        public virtual void SetIsInDeck(bool isInDeck)
        {
            _isInDeck = isInDeck;
        }

        public virtual void OnBeginDrag(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                var delta = Mathf.Abs(eventData.delta.y) - Mathf.Abs(eventData.delta.x);

                if (_scrollRect != null && (delta > 0 || DeckEditMenu.Instance == null))
                {
                    _scrollRect.OnBeginDrag(eventData);
                    _isScrolling = true;

                    return;
                }

                if (DeckEditMenu.Instance.Settings.OpenMode != DeckOpenMode.DeckEditor)
                {
                    if(_cardView != null) _cardView.Rect.DOPunchAnchorPos(eventData.delta, 0.24f);
                    return;
                }

                if (!_isInDeck)
                {
                    var canBeDragged = true;
                    var count = DeckEditMenu.Instance.Settings.DeckSettings.deckData.GetAmountOfSingleCard(_cardData.id);
                    var itemAmount = App.Inventory.GetItemAmount(_cardData.id);
                    var amountLeft = itemAmount - count;

                    if (amountLeft <= 0)
                    {
                        canBeDragged = false;
                    }

                    switch (_cardData.rarity)
                    {
                        case Rarity.Common:
                            if (count >= DeckData.MaxCopiesOfCommonCards) canBeDragged = false;
                            break;
                        case Rarity.Rare:
                            if (count >= DeckData.MaxCopiesOfRareCards) canBeDragged = false;
                            break;
                        case Rarity.Mythic:
                            if (count >= DeckData.MaxCopiesOfMysticCards) canBeDragged = false;
                            break;
                        case Rarity.Legendary:
                            if (count >= DeckData.MaxCopiesOfLegendaryCards) canBeDragged = false;
                            break;
                    }

                    if (!canBeDragged)
                    {
                        _cardView.Rect.DOPunchAnchorPos(eventData.delta, 0.24f);
                        return;
                    }
                }

                _isDragging = true;

                if (_dragCard)
                {
                    _dragCard.SetCardData(_cardData);
                    _dragCard.SetIsInDeck(_isInDeck);
                    _dragCard.OnBeginDrag(eventData);
                }
            }

        }

        public virtual void OnPointerClick(PointerEventData eventData)
        {
            if (!_isScrolling && !_isDragging) { 
                //Main Action
                if (eventData.button == PointerEventData.InputButton.Left)
                {
                    if (DeckCardList.IsPointerOverCardList)
                    {
                        ProcessClick("remove");
                    }
                    else
                    {
                        ProcessClick("add", eventData);
                        ProcessClick("craft");
                    }
                }

                //Sub Action
                if (eventData.button == PointerEventData.InputButton.Right)
                {
                    if (DeckCardList.IsPointerOverCardList)
                    {

                    }
                    else
                    {

                    }
                }
            }
        }

        public virtual void SetClickable(bool clickable)
        {
            _canClick = clickable;
        }

        public void OnAdd(PointerEventData eventData)
        {
            if (!_isInDeck && _cardData != null && DeckEditMenu.Instance.Settings.DeckSettings != null)
            {
                if (DeckEditMenu.Instance.Settings.DeckSettings.deckData != null)
                {
                    var canBeDragged = true;
                    var count = DeckEditMenu.Instance.Settings.DeckSettings.deckData.GetAmountOfSingleCard(_cardData.id);
                    var itemAmount = App.Inventory.GetItemAmount(_cardData.id);
                    var amountLeft = itemAmount - count;

                    if (amountLeft <= 0)
                    {
                        canBeDragged = false;
                    }

                    switch (_cardData.rarity)
                    {
                        case Rarity.Common:
                            if (count >= DeckData.MaxCopiesOfCommonCards) canBeDragged = false;
                            break;
                        case Rarity.Rare:
                            if (count >= DeckData.MaxCopiesOfRareCards) canBeDragged = false;
                            break;
                        case Rarity.Mythic:
                            if (count >= DeckData.MaxCopiesOfMysticCards) canBeDragged = false;
                            break;
                        case Rarity.Legendary:
                            if (count >= DeckData.MaxCopiesOfLegendaryCards) canBeDragged = false;
                            break;
                    }

                    if (!canBeDragged)
                    {
                        _cardView.Rect.DOPunchAnchorPos(eventData.delta, 0.24f);
                        return;
                    }

                    // ADD CARD TO DECK
                    DragCardDeckEditor.Current = _dragCard;
                    _dragCard._list.AddToDeckCardList(_cardView);
                    DragCardDeckEditor.Current = null;
                }
            }
        }

        public virtual void OnDrag(PointerEventData eventData)
        {
            if (_scrollRect != null && _isScrolling)
            {
                _scrollRect.OnDrag(eventData);
                return;
            }

            if (_dragCard)
            {
                _dragCard.OnDrag(eventData);
            }
        }

        public virtual void OnEndDrag(PointerEventData eventData)
        {
            if (_isScrolling)
            {
                _scrollRect.OnEndDrag(eventData);
                _isScrolling = false;
                return;
            }

            _isDragging = false;

            if (_dragCard)
            {
                _dragCard.OnEndDrag(eventData);
            }
        }



        public event Action PointerEnterEvent;
        public event Action PointerExitEvent;

        public virtual void OnPointerEnter(PointerEventData eventData)
        {
            PointerEnterEvent?.Invoke();

        }

        public virtual void OnPointerExit(PointerEventData eventData)
        {
            PointerExitEvent?.Invoke();
        }

        protected virtual void OpenRightClickError()
        {
            PopupController.Open(new PopupInfo
            {
                style = PopupWindowStyle.Dialogue,
                body = "Right-click to view card details."
            });
        }

        protected virtual void OpenSimpleCraftingMenu()
        {

            if (DeckEditMenu.Instance == null && ManaMeldEditMenu.Instance == null)
            {
                return;
            }

            else if (DeckEditMenu.Instance == null && ManaMeldEditMenu.Instance != null)
            {
                if (ManaMeldEditMenu.Instance.meldOverlay == null)
                {
                    return;
                }

                ManaMeldEditMenu.Instance.meldOverlay.Open(GetComponent<HoverCard>());
            }
        }

        protected virtual void ProcessClick(string type, PointerEventData eventData = null )
        {

            if (DeckEditMenu.Instance == null && ManaMeldEditMenu.Instance == null)
            {
                return;
            }
            else if (DeckEditMenu.Instance != null && ManaMeldEditMenu.Instance == null && DeckEditMenu.Instance.Settings.OpenMode != DeckOpenMode.DeckCreator_SelectDeck)
            {
                if(type == "remove") _dragCard._list.RemoveCard(_cardData, true);
                if(type == "add" && _canClick) OnAdd(eventData);
            }
            else if (DeckEditMenu.Instance == null && ManaMeldEditMenu.Instance != null)
            {
                if (ManaMeldEditMenu.Instance.meldOverlay != null)
                {
                    if (type == "craft")
                    {
                        ManaMeldEditMenu.Instance.UpdatePreview(_cardView);
                    }
                }
            }
        }

        // public bool MoveToNextSubCard()
        // {
        //     if (_cardView == null) return false;
        //     var hover = _cardView.GetComponent<HoverCard>();
        //     if (hover == null) return false;
        //     if (hover.ZoomedCard == null) return false;
        //     var subCardEffect = hover.ZoomedCard.GetVfx<SubCardComponent, SubCardEffect>();
        //     if (subCardEffect == null) return false;
        //     subCardEffect.NextCard();
        //     return true;
        // }

        public bool Interactable => false;
        public bool Grabbable => DeckEditMenu.Instance != null && DeckEditMenu.Instance.Settings.OpenMode == DeckOpenMode.DeckEditor;
        public bool InfoOnly => false;
        public bool Loading => false;
    }
}