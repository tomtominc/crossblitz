﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using CrossBlitz.Card;

public class ImportCard : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public CardView cardView;
    public CardView cardViewZoomed;

    public event Action<ImportCard> Clicked;

    public bool LoadingData
    {
        get { return cardView.loadingData || cardViewZoomed.loadingData; }
    }

    public void Awake()
    {
        cardView.SetActive(true);
        cardViewZoomed.SetActive(false);
      var cgroup =  cardViewZoomed.GetComponent<CanvasGroup>();
      cgroup.interactable = false;
      cgroup.blocksRaycasts = false;
    }

    public void SetCardData(CardData data)
    {
        cardView.SetCardData(data);
        cardViewZoomed.SetCardData(data);
    }

    public void OnPointerEnter(PointerEventData e)
    {
        Vector2 zoomedSize = cardViewZoomed.RectTransform().sizeDelta;
        Vector2 previewPosition = new Vector2(zoomedSize.x * 0.66f, 0f);

        if (e.position.x > (Screen.width / 2))
        {
            previewPosition.x = -previewPosition.x;
        }

        cardViewZoomed.RectTransform().anchoredPosition = previewPosition;
        cardViewZoomed.SetActive(true);
    }

    public void OnPointerExit(PointerEventData e)
    {
        cardViewZoomed.SetActive(false);
    }

    public void OnPointerClick(PointerEventData e)
    {
        Clicked(this);
    }
}
