using CrossBlitz.CardCollection;
using CrossBlitz.DeckEditAPI;
using DG.Tweening;
using Source.Scripts.Card;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CrossBlitz.Card
{
    /// <summary>
    /// This card is used for dragging between the players deck/card collection
    /// It knows when to turn into a standard card and a deck view card (small version)
    /// You only drag this card around.
    /// </summary>
    public class DragCardDeckEditor : MonoBehaviour, ICardComponent
    {
        public static DragCardDeckEditor Current;

        private CardView _cardView;
        private CardDeckViewItem _cardDeckViewItem;
        private DragRotator _drag;
        private CanvasGroup _canvasGroup;
        private bool _isInDeck;
        private int _setIsActiveAfterFrames;
        public DeckCardList _list;

        public bool IsInDeck => _isInDeck;
        public CardData CardData => _cardView.data;

        public void Initialize(CardView card)
        {
            _cardView = card;
            _drag = _cardView.GetCardComponent<DragRotator>();
            _canvasGroup = _cardView.CanvasGroup;
            _canvasGroup.blocksRaycasts = false;
            _canvasGroup.interactable = false;
        }

        public void OnAfterCreation()
        {

        }

        public void SetDeckCardList(DeckCardList list)
        {
            _list = list;
        }

        public void SetDeckViewItemPrefab(GameObject cardDeckViewPrefab)
        {
            var cardDeckViewObject = Instantiate(cardDeckViewPrefab, _cardView.RectTransform(), false);
            _cardDeckViewItem = cardDeckViewObject.GetComponent<CardDeckViewItem>();
        }

        public void SetCardData(CardData cardData)
        {
            Debug.Log($"Card Data = {cardData.id}");
            _cardView.SetCardData(cardData);
            _cardDeckViewItem.Load(new CardValue { count = 1, id = cardData.id });
        }

        public void SetIsInDeck(bool isInDeck)
        {
            _isInDeck = isInDeck;
        }

        public void OnBeginDrag(PointerEventData e)
        {
            Debug.Log("On Begin Drag happened!");
            Current = this;
            _drag.ResetAndTeleportToMouse();
            _drag.followMouse = true;
            _setIsActiveAfterFrames = 5;
            _cardView.CanvasGroup.alpha = 0;
            _cardView.SetActive(true);

           // _list.RectTransform().DOPunchAnchorPos(Vector2.right * 5, 0.15f, 0, .25f);

           // if (_list) _list.RectTransform().DOPunchAnchorPos(Vector2.right * 5, 0.15f, 0, .25f);
           // DeckCardList.GetComponent<layout>().RectTransform().DOPunchAnchorPos(Vector2.right * 5, 0.15f, 0, .25f);

            UpdateView();
        }

        public void OnDrag(PointerEventData e)
        {
            UpdateView();
        }

        public void OnEndDrag(PointerEventData e)
        {
            Current = null;
            _drag.followMouse = false;
            _cardView.SetActive(false);
        }

        private void UpdateView()
        {
            if (DeckCardList.IsPointerOverCardList)
            {
                if (_cardView.cardObject.IsActive())
                {
                    _cardView.cardObject.SetActive(false);
                }

                if (!_cardDeckViewItem.IsActive())
                {
                    _cardDeckViewItem.SetActive(true);
                }
            }
            else
            {
                if (!_cardView.cardObject.IsActive())
                {
                    _cardView.cardObject.SetActive(true);
                }

                if (_cardDeckViewItem.IsActive())
                {
                    _cardDeckViewItem.SetActive(false);
                }

            }

            if (_cardView.CanvasGroup.alpha <= 1)
            {
                _setIsActiveAfterFrames--;

                if (_setIsActiveAfterFrames <= 0)
                {
                    _cardView.CanvasGroup.alpha = 1;
                }
            }
        }
    }
}