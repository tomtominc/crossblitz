using System;
using System.Collections;
using System.Collections.Generic;
using CrossBlitz.Fables.Battle;
using GameDataEditor;
using Sirenix.OdinInspector;

namespace CrossBlitz.Card
{
    [System.Serializable]
    public class CardCraftingData
    {
        public bool craftable;
        [ShowIf("craftable")]
        public int cost;
        [ShowIf("craftable")]
        public List<ItemValue> ingredients;
        [ShowIf("craftable")]
        public List<ItemValue> scraps;
    }
}