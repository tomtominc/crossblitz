﻿using System;
using System.Collections.Generic;
using BlendModes;
using Source.Scripts.Card;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using CrossBlitz.AssetManagement;
using CrossBlitz.Card.Utilities;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Databases;
using CrossBlitz.Developer.Tools;
using CrossBlitz.GameVfx;
using CrossBlitz.Items.Data;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.UI.Widgets.TMP;
using DG.Tweening;
using GameDataEditor;
using LeTai.TrueShadow;
using Sirenix.OdinInspector;
using UnityEngine.U2D;
using BlendMode = BlendModes.BlendMode;

namespace CrossBlitz.Card
{
    public class CardView : MonoBehaviour
    {
        private static readonly int OutlineAlpha = Shader.PropertyToID("_OutlineAlpha");
        public static int MaxAbilityIconsPerCard = 3;

        [BoxGroup("General Properties")]
        public CardViewType viewType;
        [BoxGroup("General Properties")]
        public RectTransform cardObject;
        [BoxGroup("General Properties")]
        public CanvasGroup cardObjectCanvas;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.ZOOMED || this.viewType == CardViewType.STANDARD")]
        public TextMeshProUGUI nameLabel;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.ZOOMED")]
        public TMP_MaterialAnimator nameMaterialAnimator;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.STANDARD")]
        public TMP_FontAssetAnimator nameFontAssetAnimator;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.ZOOMED")]
        public TextMeshProUGUI descriptionLabel;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.ZOOMED")]
        public TextMeshProUGUI burstDescriptionLabel;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.ZOOMED")]
        public SpriteAnimation burstBanner;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.ZOOMED")]
        public TextMeshProUGUI burstRewardLabel;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.ZOOMED")]
        public GameObject burstBarContainer;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.ZOOMED")]
        public Image burstBar;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.ZOOMED")]
        public TextMeshProUGUI burstBarValue;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.ZOOMED")]
        public TMP_FontAssetAnimator descriptionFontAssetAnimator;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.ZOOMED")]
        public TMPSpriteFont powerValueSF;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.ZOOMED")]
        public TMPSpriteFont healthValueSF;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.ZOOMED")]
        public TMPSpriteFont costValueSF;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.STANDARD || this.viewType == CardViewType.BOARD")]
        public TextMeshProUGUI powerValue;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.STANDARD || this.viewType == CardViewType.BOARD")]
        public TextMeshProUGUI healthValue;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.STANDARD")]
        public TextMeshProUGUI costValue;
        [BoxGroup("Visual Components")]
        public TrueShadow shadow;
        [BoxGroup("Visual Components")]
        public InteractiveShadow interactiveShadow;
        [BoxGroup("Visual Components")]
        public Image portrait;
        [BoxGroup("Visual Components")]
        public SpriteAnimation portraitMask;
        [BoxGroup("Visual Components")]
        public SpriteAnimation portraitBackground;
        [BoxGroup("Ingredient Visual Components")][ShowIf("@this.viewType == CardViewType.STANDARD || this.viewType == CardViewType.ZOOMED")]
        public SpriteAnimation resourcePortrait;
        [BoxGroup("Ingredient Visual Components")][ShowIf("@this.viewType == CardViewType.STANDARD || this.viewType == CardViewType.ZOOMED")]
        public TextMeshProUGUI resourceAmountLabel;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.BOARD || this.viewType == CardViewType.ZOOMED")]
        public CardOutline cardOutline;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.STANDARD")]
        public CardGlowEffect cardGlowEffect;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.ZOOMED")]
        public SpriteAnimation setIcon;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.ZOOMED")]
        public GameObject abilityNotSetupCross;
        [BoxGroup("Visual Components")]
        [ShowIf("@this.viewType == CardViewType.STANDARD || this.viewType == CardViewType.BOARD")]
        public AbilityIconDisplay abilityIconDisplay;
        [BoxGroup("Visual Components")][ShowIf("@this.viewType == CardViewType.ZOOMED")]
        public KeywordDescriptionPopup keywordDescriptionPopup;
        [BoxGroup("Front Components")]
        public SpriteAnimation front;
        [BoxGroup("Front Components")]
        public Texture2D outlinePurpleTexture;
        [BoxGroup("Front Components")]
        public Texture2D outlineOrangeTexture;
        [BoxGroup("Front Components")]
        public Image front3D;
        [BoxGroup("Front Components")]
        public Image frontOutline;
        [BoxGroup("Front Components")]
        public SpriteAnimation frontOverlay;
        [BoxGroup("Front Components")]
        public Image frontShadow;
        [BoxGroup("Front Components")]
        public RectTransform frontShadowRect;
        [BoxGroup("Front Components")]
        public CanvasGroup frontOverlayCanvas;
        [BoxGroup("Front Components")]
        public BlendModeEffect frontOverlayBlend;
        [BoxGroup("Front Components")]
        public SpriteAnimation frontMeldAnimator;
        [BoxGroup("Icon Components")]
        public SpriteAnimation classIcon;
        [BoxGroup("Icon Components")][ShowIf("@this.viewType == CardViewType.ZOOMED")]
        public SpriteAnimation classBar;
        [BoxGroup("Icon Components")][ShowIf("@this.viewType == CardViewType.ZOOMED")]
        public TextMeshProUGUI className;
        [BoxGroup("Icon Components")][ShowIf("@this.viewType == CardViewType.ZOOMED || this.viewType == CardViewType.STANDARD")]
        public SpriteAnimation rarityIcon;
        [BoxGroup("Icon Components")]
        public SpriteAnimation attackTypeIcon;
        [BoxGroup("Icon Components")]
        public SpriteAnimation attackTypeContainer;
        [BoxGroup("Icon Components")]
        public Image attackTypeIconOverlay;
        [BoxGroup("Icon Components")]
        public CanvasGroup attackTypeIconOverlayCanvas;
        [BoxGroup("Icon Components")]
        public BlendModeEffect attackTypeIconOverlayBlend;
        [BoxGroup("Card Back Components")][ShowIf("@this.viewType == CardViewType.STANDARD")]
        public RectTransform cardBackObject;
        [BoxGroup("Card Back Components")][ShowIf("@this.viewType == CardViewType.STANDARD")]
        public SpriteAnimation cardBack;
        [BoxGroup("Card Back Components")][ShowIf("@this.viewType == CardViewType.STANDARD")]
        public Image cardBack3D;
        [BoxGroup("Card Back Components")][ShowIf("@this.viewType == CardViewType.STANDARD")]
        public Image cardBackShadow;

        [BoxGroup("Board Card Components")][ShowIf("@this.viewType == CardViewType.BOARD")]
        public RectTransform abilityIconHolder;
        [BoxGroup("Board Card Components")][ShowIf("@this.viewType == CardViewType.BOARD")]
        public SpriteAnimation genericTriggerIcon;
        [BoxGroup("Board Card Components")][ShowIf("@this.viewType == CardViewType.BOARD")]
        public RectTransform attackBlockedIcon;
        [BoxGroup("Board Card Components")][ShowIf("@this.viewType == CardViewType.BOARD")]
        public CardDialogue cardDialogue;
        [BoxGroup("Board Card Components")][ShowIf("@this.viewType == CardViewType.BOARD")]
        public AbilityIcon abilityIconPrefab;

        [NonSerialized]
        public CardData data;
        [NonSerialized]
        public bool loadingData;
        [NonSerialized]
        public bool isDirty;

        private Canvas _canvas;
        private CanvasGroup _canvasGroup;
        private RectTransform _rect;
        private int _glowOutlineTexturePropertyNameId;
        private int _glowOutlineAlphaPropertyNameId;

        public Canvas Canvas => _canvas;
        public CanvasGroup CanvasGroup => _canvasGroup;
        public RectTransform Rect => _rect;

        private Dictionary<Type, ICardComponent> _components;
        private Dictionary<Type, IGameVfx> _vfx;

        private List<AbilityIcon> _abilityIcons;

        public void Init(int sortingOrder, Vector2 offsetPosition, bool startsInactive)
        {
            _canvas = GetComponent<Canvas>();
            _canvasGroup = GetComponent<CanvasGroup>();
            _rect = this.RectTransform();
            _components = new Dictionary<Type, ICardComponent>();
            _vfx=new Dictionary<Type, IGameVfx>();

            cardObject.anchoredPosition = offsetPosition;
            if (sortingOrder > 0) _canvas.sortingOrder = sortingOrder;

            if (keywordDescriptionPopup == null)
            {
                keywordDescriptionPopup = GetComponentInChildren<KeywordDescriptionPopup>();
            }

            // have something here so we don't get errors!
            data = new CardData();

            if (front3D != null)
            {
                front3D.alphaHitTestMinimumThreshold = 1;
            }

            if (viewType == CardViewType.ZOOMED)
            {
                _glowOutlineAlphaPropertyNameId = m_frontMaterial.shader.GetPropertyNameId(m_frontMaterial.shader.FindPropertyIndex("_OutlineAlpha"));
                _glowOutlineTexturePropertyNameId = m_frontMaterial.shader.GetPropertyNameId(m_frontMaterial.shader.FindPropertyIndex("_OutlineTex"));
            }

            this.SetActive(!startsInactive);
        }

        public int GetSortOrder()
        {
            if (!_canvas) return 0;
            return _canvas.sortingOrder;
        }

        public void SetSortOrder(int sortingOrder)
        {
            if (_canvas)
                _canvas.sortingOrder = sortingOrder;
        }

        public void SetSortingLayer(string sortingLayer)
        {
            if (_canvas)
                _canvas.sortingLayerName = sortingLayer;
        }

        public string GetSortingLayer()
        {
            if (_canvas)
                return _canvas.sortingLayerName;

            return "Default";
        }

        public void SetInteractable(bool interact)
        {
            _canvasGroup.interactable = interact;
            _canvasGroup.blocksRaycasts = interact;
        }

        public void RemoveCanvas()
        {
            var graphicRaycaster = GetComponent<GraphicRaycaster>();
            if (graphicRaycaster) Destroy(graphicRaycaster);
            Destroy(_canvas);
            _canvas = null;
        }

        public T AddCardComponent<T>() where T : Component, ICardComponent
        {
            var component = GetComponent<T>();

            if (component == null)
            {
                component = gameObject.AddComponent<T>();
            }

            if (_components.ContainsKey(typeof(T)))
            {
                _components.Remove(typeof(T));
            }

            _components.Add(typeof(T), component);

            return component;
        }

        public T AddCardComponent<T>(Type t) where T : Component, ICardComponent
        {
            var component = GetComponent(t);

            if (component == null)
            {
                component = gameObject.AddComponent(t);
            }

            if (_components.ContainsKey(typeof(T)))
            {
                _components.Remove(typeof(T));
            }

            _components.Add(typeof(T), (T)component);

            return (T)component;
        }

        public T GetCardComponent<T>() where T : ICardComponent
        {
            if (!_components.ContainsKey(typeof(T)))
            {
                var component = GetComponent<T>();

                if (component == null)
                {
                    component = GetComponentInChildren<T>();
                }

                if (component == null)
                {
                    return default;
                }

                _components.Add(typeof(T), component);
            }

            return (T)_components[typeof(T)];
        }

        public U GetVfx<T, U>() where T : GameVfxComponent where U : IGameVfx
        {
            if (!_vfx.ContainsKey(typeof(U)))
            {
                var component = GetCardComponent<T>();

                if (component == null)
                {
                    return default;
                }

                if (component.Effect is U u)
                {
                    _vfx.Add(typeof(U), u);
                }
                else
                {
                    return default;
                }
            }

            return (U)_vfx[typeof(U)];
        }

        public bool AllVfxComponentsLoaded()
        {
            var components = GetComponents<ICardEffectComponent>();

            for (var i = 0; i < components.Length; i++)
            {
                if (components[i].Effect == null) return false;
            }

            return true;
        }

        private void OnDisable()
        {
            if (portrait != null && portrait.sprite != null)
            {
                // BundlesManager.Instance.UnloadAssetBundle(data.portraitUrl);
                // DestroyImmediate(portrait.sprite, true);
            }
        }

        public void EnableUsageGlow(bool e, bool conditionalGlow = false, bool immediate = false)
        {
            if (cardGlowEffect)
            {
                if (!e && immediate)
                {
                    cardGlowEffect.SetActive(false);
                }
                else
                {
                    cardGlowEffect.EnableOutline(e, data.type.ToString().ToLower(), conditionalGlow);
                }
            }
            else
            {
                Debug.LogError("Card Glow Effect not availalbe!");
            }
        }

        private async void LoadBuild()
        {
            if (portrait)
            {
                switch (data.type)
                {
                    case CardType.Resource:
                    {
                        portrait.SetActive(false);

                        if (resourcePortrait != null)
                        {
                            resourcePortrait.SetActive(true);

                            if (!resourcePortrait.Play(data.portraitUrl))
                            {
                                Debug.LogError($"Could not play {data.portraitUrl} in resource portrait!");
                            }
                        }

                        break;
                    }
                    default:
                        if (!string.IsNullOrEmpty(data.portraitUrl))
                        {
                            var portraitUrl = $"{data.portraitUrl}-outlined";

                            if (portrait.sprite != null && portrait.sprite.name != portraitUrl)
                            {
                                var atlas = CardGenerationTool.Instance != null ? CardGenerationTool.Instance.cardAtlas : await AddressableReferenceLoader.Load<SpriteAtlas>("cards-atlas");
                                portrait.sprite = atlas.GetSprite(portraitUrl);
                            }

                            if (portrait.sprite == null)
                            {
                                var atlas = CardGenerationTool.Instance != null ? CardGenerationTool.Instance.cardAtlas : await AddressableReferenceLoader.Load<SpriteAtlas>("cards-atlas");
                                portrait.sprite = atlas.GetSprite(portraitUrl);
                            }

                            if (portrait.sprite == null)
                            {
                                portraitUrl = data.portraitUrl;
                                var atlas = CardGenerationTool.Instance != null ? CardGenerationTool.Instance.cardAtlas : await AddressableReferenceLoader.Load<SpriteAtlas>("cards-atlas");
                                portrait.sprite = atlas.GetSprite(portraitUrl);
                            }

                            portrait.SetActive(portrait.sprite != null);
                        }
                        else
                        {
                            portrait.SetActive(false);
                        }
                        break;
                }

                OnOffsetChanged();
            }

            if (viewType == CardViewType.BOARD)
            {
                UpdateAbilitiesForBoardCardDisplay(data.GetAbilities());
            }

            loadingData = false;
        }

        public void UpdateAbilitiesForBoardCardDisplay(List<Ability> abilities)
        {
            if (!abilityIconHolder)
            {
                return;
            }

            if (_abilityIcons == null || _abilityIcons.Count < MaxAbilityIconsPerCard)
            {
                _abilityIcons = new List<AbilityIcon>();

                for (var i = 0; i < MaxAbilityIconsPerCard; i++)
                {
                    var abilityIcon = Instantiate(abilityIconPrefab, abilityIconHolder, false);
                    abilityIcon.SetActive(false);
                    _abilityIcons.Add(abilityIcon);
                }
            }

            _abilityIcons.ForEach( icon => icon.SetActive(false));
            genericTriggerIcon.SetActive(false);

            var i0 = 0;

            for (var i = 0; i < abilities.Count; i++)
            {
                if (i0 >= _abilityIcons.Count) break;

                var ability = abilities[i];

                if ( Ability.UsesCustomIcon(ability) )
                {
                    var abilityIcon = _abilityIcons[i0];
                    abilityIcon.SetAbility(ability);
                    abilityIcon.SetActive(true);
                    i0++;
                }
                else if (Ability.UsesGenericTriggerIcon(ability))
                {
                    genericTriggerIcon.SetActive(true);
                    genericTriggerIcon.Play("non-active");
                }
            }
        }

        public void OnOffsetChanged()
        {
            switch (viewType)
            {
                case CardViewType.STANDARD:
                    portrait.rectTransform.anchoredPosition = data.standardOffset;
                    break;
                case CardViewType.ZOOMED:
                    portrait.rectTransform.anchoredPosition = data.zoomedOffset;
                    break;
                case CardViewType.BOARD:
                    portrait.rectTransform.anchoredPosition = data.boardOffset;
                    break;
            }
        }

        public void OnBurstOffsetChanged()
        {
            if (burstDescriptionLabel)
            {
                burstDescriptionLabel.text = FormatDescription(data.description);
                burstDescriptionLabel.rectTransform.sizeDelta =
                    new Vector2(data.topDescriptionWidth, data.topDescriptionHeight);
                burstDescriptionLabel.rectTransform.anchoredPosition = new Vector2(-1, data.topDescriptionOffset);
                burstDescriptionLabel.SetActive(data.type == CardType.Power);
            }

            if (burstRewardLabel)
            {
                burstRewardLabel.text = FormatDescription(data.burstRewardDescription);
                burstRewardLabel.rectTransform.sizeDelta =
                    new Vector2(data.bottomDescriptionWidth, data.bottomDescriptionHeight);
                burstRewardLabel.rectTransform.anchoredPosition = new Vector2(-1, data.bottomDescriptionOffset);
                burstRewardLabel.SetActive(data.type == CardType.Power);
            }

            if (burstBanner)
            {
                burstBanner.Play($"{data.faction.ToString().ToLower()}");
                burstBanner.RectTransform().anchoredPosition = new Vector2(-1, data.burstBannerOffsetY);
                burstBanner.SetActive(data.type == CardType.Power);
            }

            if (burstBarContainer)
            {
                burstBarContainer.SetActive(data.type == CardType.Power);
                if (data.limitBreak!=null) burstBarValue.text = $"0/{data.limitBreak.value}";
                burstBar.fillAmount = 0;
            }

            if (descriptionLabel)
            {
                descriptionLabel.rectTransform.anchoredPosition = data.descriptionOffset;
                descriptionLabel.rectTransform.sizeDelta = data.descriptionSize;
            }
        }

        public void SetCardData(CardData _data)
        {
            data = _data;

            if (data.type != CardType.Resource)
            {
                if (nameLabel)
                {
                    if (viewType == CardViewType.STANDARD)
                    {
                        nameLabel.text = data.nameAbbrev;
                        nameLabel.fontSize = 16;
                    }
                    else
                    {
                        nameLabel.text = data.name;
                    }
                }

                if (nameMaterialAnimator)
                {
                    if (data.type == CardType.Elder_Relic)
                    {
                        nameMaterialAnimator.SetPreset("relic");
                    }
                    else
                    {
                        nameMaterialAnimator.SetPreset(data.type.ToString().ToLower());
                    }
                }

                if (nameFontAssetAnimator)
                {
                    nameFontAssetAnimator.SetPreset(data.type.ToString().ToLower());
                }
            }
            else
            {
                if (nameLabel)
                {
                    nameLabel.text = data.name;
                }

                if (resourceAmountLabel)
                {
                    if (data.resourceType == "relic-slot")
                    {
                        resourceAmountLabel.SetActive(false);
                    }
                    else
                    {
                        resourceAmountLabel.SetActive(true);
                    }

                    if (data.resourceType == "hp-up")
                    {
                        resourceAmountLabel.text = $"+{data.resourceAmount} HP";
                    }
                    else
                    {
                        resourceAmountLabel.text = $"<color=#b29675>x</color>{data.resourceAmount}";
                    }
                }
            }

            if (descriptionLabel)
            {
                descriptionLabel.text = FormatDescription(data.description);
                descriptionLabel.SetActive(data.type != CardType.Power);
            }

            OnBurstOffsetChanged();

            if (descriptionFontAssetAnimator)
            {
                //descriptionFontAssetAnimator.SetPreset(data.type.ToString().ToLower());
            }

            if (powerValue)
            {
                powerValue.text = data.power.ToString();
            }

            if (healthValue)
            {
                healthValue.text = data.health.ToString();
            }

            if (costValue)
            {
                costValue.text = data.cost.ToString();
            }

            if (powerValueSF)
            {
                powerValueSF.text = data.power.ToString();
            }

            if (healthValueSF)
            {
                healthValueSF.text = data.health.ToString();
            }

            if (costValueSF)
            {
                costValueSF.text = data.cost.ToString();
            }

            if (setIcon)
            {
                if (data.type == CardType.Minion || data.type == CardType.Power || data.type == CardType.Spell || data.type == CardType.Trick)
                {
                    setIcon.Play($"{data.type.ToString().ToLower()}-{CardData.CardSetToIcon[data.set]}");
                    setIcon.SetActive(true);
                }
                else
                {
                    setIcon.SetActive(false);
                    //setIcon.Play($"{data.type.ToString().ToLower()}-{CardData.CardSetToIcon[data.set]}");
                }
            }

            if (className)
            {
                if (data.@class == Class.None || data.@class == Class.All)
                {
                    className.SetActive(false);
                }
                // hacky because splitting a string costs too much.
                else if (data.@class == Class.SproutElf)
                {
                    className.SetActive(true);
                    className.text = "Sprout Elf";
                }
                else
                {
                    className.SetActive(true);
                    className.text = data.@class.ToString();
                }
            }

            if (data.type == CardType.Minion)
            {
                if (powerValue)
                {
                    powerValue.SetActive(true);
                }

                if (healthValue)
                {
                    healthValue.SetActive(true);
                }

                if (powerValueSF)
                {
                    powerValueSF.SetActive(true);
                }

                if (healthValueSF)
                {
                    healthValueSF.SetActive(true);
                }

                if (attackTypeIcon)
                {
                    attackTypeIcon.SetActive(true);
                }

                if (attackTypeContainer)
                {
                    attackTypeContainer.SetActive(true);
                }

                if (costValue)
                {
                    costValue.SetActive(true);
                }

                if (costValueSF)
                {
                    costValueSF.SetActive(true);
                }

                if (nameLabel && viewType == CardViewType.ZOOMED)
                {
                    nameLabel.rectTransform.anchoredPosition3D = new Vector3(-1, -16, -8);
                }
            }
            else if (data.type == CardType.Power)
            {
                if (powerValue)
                {
                    powerValue.SetActive(false);
                }

                if (healthValue)
                {
                    healthValue.SetActive(false);
                }

                if (powerValueSF)
                {
                    powerValueSF.SetActive(false);
                }

                if (healthValueSF)
                {
                    healthValueSF.SetActive(false);
                }

                if (attackTypeIcon)
                {
                    attackTypeIcon.SetActive(false);
                }

                if (attackTypeContainer)
                {
                    attackTypeContainer.SetActive(false);
                }

                if (costValue)
                {
                    costValue.SetActive(false);
                }

                if (costValueSF)
                {
                    costValueSF.SetActive(false);
                }

                if (nameLabel && viewType == CardViewType.ZOOMED)
                {
                    nameLabel.rectTransform.anchoredPosition3D = new Vector3(-1, 11, -8);
                }
            }
            else
            {
                if (powerValue)
                {
                    powerValue.SetActive(false);
                }

                if (healthValue)
                {
                    healthValue.SetActive(false);
                }

                if (powerValueSF)
                {
                    powerValueSF.SetActive(false);
                }

                if (healthValueSF)
                {
                    healthValueSF.SetActive(false);
                }

                if (attackTypeIcon)
                {
                    attackTypeIcon.SetActive(false);
                }

                if (attackTypeContainer)
                {
                    attackTypeContainer.SetActive(false);
                }

                if (costValue && (data.type == CardType.Resource || data.type == CardType.Relic || data.type == CardType.Elder_Relic))
                {
                    costValue.SetActive(false);
                }
                else if (costValue)
                {
                    costValue.SetActive(true);
                }

                if (costValueSF && (data.type == CardType.Resource || data.type == CardType.Relic || data.type == CardType.Elder_Relic))
                {
                    costValueSF.SetActive(false);
                }
                else if (costValueSF)
                {
                    costValueSF.SetActive(true);
                }

                if (nameLabel && viewType == CardViewType.ZOOMED)
                {
                    nameLabel.rectTransform.anchoredPosition3D = new Vector3(-1, -16, -8);
                }
            }

            if (portraitMask)
            {
                var maskName = string.Empty;

                switch (data.type)
                {
                    case CardType.Minion:
                        maskName = $"minion-mask-{viewType.ToString().ToLower()}";
                        break;
                    case CardType.Trick:
                        maskName = $"trick-mask-{viewType.ToString().ToLower()}";
                        break;
                    case CardType.Spell:
                        maskName = $"spell-mask-{viewType.ToString().ToLower()}";
                        break;
                    case CardType.Power:
                        maskName = $"power-mask-{viewType.ToString().ToLower()}";
                        break;
                    case CardType.Relic:
                    case CardType.Elder_Relic:
                        maskName = $"relic-mask-{viewType.ToString().ToLower()}";
                        break;
                    case CardType.Resource:
                        maskName = $"resource-mask-{viewType.ToString().ToLower()}";
                        break;
                }

                if (!string.IsNullOrEmpty(maskName))
                {
                    portraitMask.Play(maskName);
                }

                if (data.type == CardType.Power && viewType == CardViewType.ZOOMED)
                {
                    portraitMask.RectTransform().anchoredPosition = new Vector2(0, 10);
                }
                else
                {
                    portraitMask.RectTransform().anchoredPosition = new Vector2(0, 0);
                }
            }

            if (front)
            {
                if (data.type != CardType.Resource)
                {
                    var frontName = $"{data.faction.ToString().ToLower()}-{data.type.ToString().ToLower()}";

                    if (data.type == CardType.Relic)
                    {
                        frontName = "relic";
                    }
                    else if (data.type == CardType.Elder_Relic)
                    {
                        frontName = "elder-relic";
                    }

                    if (!front.Play(frontName))
                    {
                       // Debug.LogError($"Could not play the front animation on {data.name}. @{frontName} ({data.type})");
                    }
                }
                else
                {
                    var frontName = $"{data.resourceType}-{data.faction.ToString().ToLower()}";

                    if (data.resourceType != "ingredient")
                    {
                        frontName = $"{data.resourceType}";
                    }

                    if (data.resourceType == "mana-shard")
                    {
                        frontName = "currency-manashard";
                    }

                    if (data.resourceType == "dawn-dollars")
                    {
                        frontName = "currency-dawn-dollars";
                    }

                    if (data.resourceType == "relic-slot")
                    {
                        frontName = "currency-relic-slot";
                    }

                    if (!front.Play(frontName))
                    {
                        //Debug.LogError($"Could not play the front animation on {data.name}. @{frontName}");
                    }
                }

                if (frontOutline)
                {
                    if (viewType != CardViewType.STANDARD)
                    {
                        frontOutline.sprite = front.image.sprite;
                    }
                }

                if (frontOverlay)
                {
                    if (_data.type == CardType.Minion)
                    {
                        if (!frontOverlay.Play(
                            $"overlay-{_data.type.ToString().ToLower()}-{_data.attackType.ToString().ToLower()}"))
                        {
                            frontOverlay.Play($"overlay-{_data.type.ToString().ToLower()}");
                        }
                    }
                    else
                    {
                        frontOverlay.Play($"overlay-{_data.type.ToString().ToLower()}");
                    }

                    frontOverlay.SetActive(false);
                }

                if (front3D)
                {
                    front3D.sprite = front.image.sprite;
                }

                if (frontShadow)
                {
                    frontShadow.sprite = front.image.sprite;
                }
            }

            if (cardBack)
            {
                if (cardBack3D)
                {
                    cardBack3D.sprite = cardBack.image.sprite;
                }

                if (cardBackShadow)
                {
                    cardBackShadow.sprite = cardBack.image.sprite;
                }
            }

            if (classIcon)
            {
                if (data.@class == Class.None || data.@class == Class.All)
                {
                    classIcon.SetActive(false);
                }
                else
                {
                    classIcon.SetActive(true);
                    var classIconName = data.@class.ToString().ToLower();
                    classIcon.Play(classIconName);
                }
            }

            if (classBar)
            {
                if (data.@class == Class.None || data.@class == Class.All)
                {
                    classBar.SetActive(false);
                }
                else
                {
                    classBar.SetActive(true);
                    var classBarName = data.@class.ToString().ToLower();
                    classBar.Play(classBarName);
                }
            }

            if (rarityIcon)
            {
                if (data.type != CardType.Resource && data.type != CardType.Relic && data.type != CardType.Elder_Relic)
                {
                    var rarityName = data.rarity.ToString().ToLower();
                    rarityIcon.Play(rarityName);
                }
                else
                {
                    rarityIcon.SetActive(false);
                }
            }

            if (abilityIconDisplay)
            {
                abilityIconDisplay.SetupIconDisplay(data);
            }

            if (keywordDescriptionPopup)
            {
                keywordDescriptionPopup.SetCardData(data.id);
            }

            if (attackTypeIcon)
            {
                if (data.type == CardType.Minion)
                {
                    attackTypeIcon.Play(data.attackType.ToString().ToLower());

                    if (attackTypeIconOverlay)
                    {
                        attackTypeIconOverlay.sprite = attackTypeIcon.image.sprite;
                    }

                    if (attackTypeContainer)
                    {
                        attackTypeContainer.Play(data.attackType.ToString().ToLower());
                    }
                }
                else
                {
                    attackTypeIcon.SetActive(false);

                    if (attackTypeContainer)
                    {
                        attackTypeContainer.SetActive(false);
                    }
                }
            }

            if (abilityNotSetupCross)
            {
                // if (data != null && !string.IsNullOrEmpty(data.description) && data.abilities?.Count <= 0)
                // {
                //     abilityNotSetupCross.SetActive(true);
                // }
                // else
                // {
                //     abilityNotSetupCross.SetActive(false);
                // }
            }

            if (portraitBackground)
            {
                if (data.type == CardType.Resource)
                {
                    if (data.resourceType == "ingredient")
                    {
                        portraitBackground.Play($"{data.resourceType}-{data.faction.ToString().ToLower()}");
                    }
                    else
                    {
                        portraitBackground.Play($"{data.resourceType}");
                    }
                }
                else if (data.type == CardType.Relic)
                {
                    portraitBackground.Play("relic");
                }
                else if (data.type == CardType.Elder_Relic)
                {
                    portraitBackground.Play("elder-relic");
                }
                else
                {
                    portraitBackground.Play($"{data.faction.ToString().ToLower()}-{data.type.ToString().ToLower()}");
                }
            }

            if (viewType == CardViewType.ZOOMED && data.set != CardSet.Token && data.set != CardSet.Deprecated)
            {
                // var itemAmount = App.Inventory.GetItemAmount(data.id);
                //
                // if (itemAmount <= 0 && frontMeldAnimator)
                // {
                //     frontMeldAnimator.SetActive(true);
                //     UpdateFrontMeldAnimator();
                // }
                // else
                // {
                //     frontMeldAnimator.SetActive(false);
                // }
            }

            isDirty = true;
        }

        private void Update()
        {
            // if (Server.ConnectionState != Server.State.CONNECTED)
            //     return;

            if (!loadingData && isDirty)
            {
                loadingData = true;
                isDirty = false;

                LoadBuild();
            }

            //Debug.Log($"{nameLabel.text} = {nameLabel.bounds} = {nameLabel.textBounds}");

            // var charactersWidth = 0;
            // var charactersWithWidthOver5 = 0;
            //
            // for (var i = 0; i < nameLabel.textInfo.characterInfo.Length; i++)
            // {
            //     var width = nameLabel.textInfo.characterInfo[i].textElement.glyph.glyphRect.width;
            //     charactersWidth += width;
            //     if (width > 5) charactersWithWidthOver5++;
            //
            //     //Debug.Log($"{nameLabel.text} ({i+1}/{nameLabel.textInfo.characterInfo.Length}) = {nameLabel.textInfo.characterInfo[i].textElement.glyph.glyphRect.width}" );
            // }
            //
            // Debug.Log($"{nameLabel.text} width = {charactersWidth} characters > 5 = {charactersWithWidthOver5}");

        }

        public void UpdateFrontMeldAnimator()
        {
            if (frontMeldAnimator)
            {
                if (data.type == CardType.Minion)
                {
                    if (!frontMeldAnimator.Play(
                        $"overlay-{data.type.ToString().ToLower()}-{data.attackType.ToString().ToLower()}"))
                    {
                        Debug.LogError($"Couldn't find anim overlay-{data.type.ToString().ToLower()}-{data.attackType.ToString().ToLower()}");
                    }
                    else if (viewType == CardViewType.ZOOMED)
                    {
                        //Debug.Log($"anim successful! overlay-{data.type.ToString().ToLower()}-{data.attackType.ToString().ToLower()} {data.name}");
                    }
                }
                else
                {
                    if (!frontMeldAnimator.Play($"overlay-{data.type.ToString().ToLower()}"))
                    {
                        Debug.LogError($"Couldn't find anim overlay-{data.type.ToString().ToLower()}");
                    }
                    else if (viewType == CardViewType.ZOOMED)
                    {
                        //Debug.Log($"success overlay-{data.type.ToString().ToLower()} {data.name}");
                    }
                }
            }
        }

        [HideInInspector] public bool m_isTinted;

        public event Action OnStartAnimating;
        public event Action OnEndAnimating;

        public void StartAnimating()
        {
            OnStartAnimating?.Invoke();
        }

        public void EndAnimating()
        {
            OnEndAnimating?.Invoke();
        }

        public void Tint(Color color, BlendMode blendMode, bool removeBlend = false, float alpha = 1)
        {
            m_isTinted = true;

            if (!frontOverlayCanvas.IsActive())
            {
                frontOverlayCanvas.SetActive(true);
            }

            frontOverlayCanvas.alpha = alpha;
            frontOverlay.image.color = color;

            if (frontOverlayBlend)
            {
                if (removeBlend)
                {
                    frontOverlayBlend.enabled = false;
                }
                else
                {
                    frontOverlayBlend.BlendMode = blendMode;
                }
            }

            if (data.type == CardType.Minion && attackTypeIconOverlayCanvas)
            {
                attackTypeIconOverlayCanvas.alpha = alpha;
                attackTypeIconOverlay.color = color;

                if (attackTypeIconOverlayBlend)
                {
                    attackTypeIconOverlayBlend.BlendMode = blendMode;
                }
            }
        }

        public void RemoveTint()
        {
            m_isTinted = false;

            if (frontOverlayCanvas)
            {
                frontOverlayCanvas.DOKill();
                frontOverlayCanvas.alpha = 0;
                frontOverlayCanvas.SetActive(false);
            }

            if (attackTypeIconOverlayCanvas)
            {
                attackTypeIconOverlayCanvas.DOKill();
                attackTypeIconOverlayCanvas.alpha = 0;
                attackTypeIconOverlayCanvas.SetActive(false);
            }
        }

        public void TintThenUntintOverTime(Color color, BlendMode blendMode, float durationTo, float delayBetweenUnTint, float durationFrom)
        {
            TintOverTime(color, blendMode, durationTo).OnComplete(() =>
            {
                Sequence sequence = DOTween.Sequence();
                sequence.AppendInterval(delayBetweenUnTint);
                sequence.AppendCallback(() =>
                {
                    RemoveTintOverTime(durationFrom);
                });
            });
        }

        public Tweener TintOverTime(Color color, BlendMode blendMode, float duration, bool removeBlendMode = false)
        {
            m_isTinted = true;

            if (!frontOverlayCanvas.IsActive())
                frontOverlayCanvas.SetActive(true);

            frontOverlayCanvas.alpha = 0;
            frontOverlay.image.color = color;

            if (frontOverlayBlend)
            {
                if (removeBlendMode)
                {
                    frontOverlayBlend.enabled = false;
                }
                else
                {
                    frontOverlayBlend.enabled = true;
                    frontOverlayBlend.BlendMode = blendMode;
                }

            }

            if(data.type == CardType.Minion && attackTypeIconOverlayCanvas)
            {
                if (!attackTypeIconOverlayCanvas.IsActive())
                    attackTypeIconOverlayCanvas.SetActive(true);

                attackTypeIconOverlayCanvas.alpha = 0;
                attackTypeIconOverlay.color = color;
                attackTypeIconOverlayCanvas.DOFade(1, duration);

                if (attackTypeIconOverlayBlend)
                {
                    attackTypeIconOverlayBlend.BlendMode = blendMode;
                }
            }

            return frontOverlayCanvas.DOFade(1, duration);
        }

        public Tweener RemoveTintOverTime(float duration)
        {
            m_isTinted = false;

            if (data.type == CardType.Minion && attackTypeIconOverlayCanvas)
            {
                attackTypeIconOverlayCanvas.DOFade(0, duration).OnComplete(() =>
                {
                    attackTypeIconOverlayCanvas.alpha = 0;
                    attackTypeIconOverlayCanvas.SetActive(false);
                });
            }

            return frontOverlayCanvas.DOFade(0, duration).OnComplete(() =>
            {
                frontOverlayCanvas.alpha = 0;
                frontOverlayCanvas.SetActive(false);
            });;
        }

        private Material m__frontMaterial;

        private Material m_frontMaterial
        {
            get
            {
                if (m__frontMaterial == null)
                {
                    m__frontMaterial = Instantiate(front.image.material);
                    front.image.material = m__frontMaterial;
                }

                return m__frontMaterial;
            }
        }


        public void SetAlpha(float alpha)
        {
            m_frontMaterial.SetFloat(OutlineAlpha, alpha);
            _canvasGroup.alpha = alpha;
        }
        public void Fade(float alpha, float duration, bool alsoFadeOutline = false, float delay = 0, bool usesOrangeGlow=false, TweenCallback callback = null)
        {
            DOTween.Kill(m_frontMaterial);
            DOTween.Kill(_canvasGroup);

            if (alpha < 1 || alsoFadeOutline)
            {
                m_frontMaterial.DOFloat(alpha, _glowOutlineAlphaPropertyNameId, duration).SetDelay(delay);
                m_frontMaterial.SetTexture(_glowOutlineTexturePropertyNameId,usesOrangeGlow ? outlineOrangeTexture : outlinePurpleTexture );
            }

            _canvasGroup.DOFade(alpha, duration)
                .SetDelay(delay)
                .OnComplete(callback);
        }

        public void SetOutlineAlpha(float alpha)
        {
            m_frontMaterial.SetFloat(OutlineAlpha, alpha);
        }

        public void SetGameState(GameCardState cardState)
        {
            if (!GameServer.IsRunning())
            {
                return;
            }

            if (cardBack)
            {
                var player = GameServer.GetPlayerState(cardState.owner);
                var cardBackData = Db.CardBackDatabase.GetCardBackData(player.cardBackUid);
                cardBack.Play(cardBackData.portraitUrl);
            }

            if (data.type == CardType.Power && burstBarContainer && burstBarValue && burstBar)
            {
                burstBarContainer.SetActive(true);
                burstBarValue.text = $"{cardState.limitBreakValue}/{data.limitBreak.value}";
                burstBar.fillAmount =cardState.limitBreakValue / (float) data.limitBreak.value;
            }

            if (abilityNotSetupCross && cardState.silenced)
            {
                abilityNotSetupCross.SetActive(true);
            }
            else if (abilityNotSetupCross)
            {
                abilityNotSetupCross.SetActive(false);
            }

            if (keywordDescriptionPopup)
            {
                keywordDescriptionPopup.SetCardState(cardState.uid);
            }


            if (cardState != null && cardState.HasAbility(AbilityKeyword.FOCUS))
            {
                var focus = cardState.GetAbility(AbilityKeyword.FOCUS);
                var focusState = cardState.GetAbilityState(focus);


                if (focusState.focusValue >= focus.focusCount)
                {
                    UpdatePortraitForFocus();
                }
            }

            if (descriptionLabel)
            {
                descriptionLabel.text = FormatDescription(data.description);
            }
            else
            {
                FormatDescription(data.description, cardState);
            }
        }

        public async void UpdatePortraitForFocus()
        {
            if (!string.IsNullOrEmpty(data.portraitUrl))
            {
                var portraitUrl = data.portraitUrl;

                if (portrait.sprite != null && portrait.sprite.name != portraitUrl)
                {
                    var atlas = CardGenerationTool.Instance != null ? CardGenerationTool.Instance.cardAtlas : await AddressableReferenceLoader.Load<SpriteAtlas>("cards-atlas");
                    portrait.sprite = atlas.GetSprite(portraitUrl);
                }

                if (portrait.sprite == null)
                {
                    var atlas = CardGenerationTool.Instance != null ? CardGenerationTool.Instance.cardAtlas : await AddressableReferenceLoader.Load<SpriteAtlas>("cards-atlas");
                    portrait.sprite = atlas.GetSprite(portraitUrl);
                }

                if (portrait.sprite == null)
                {
                    portraitUrl = data.portraitUrl;
                    var atlas = CardGenerationTool.Instance != null ? CardGenerationTool.Instance.cardAtlas : await AddressableReferenceLoader.Load<SpriteAtlas>("cards-atlas");
                    portrait.sprite = atlas.GetSprite(portraitUrl);
                }

                portrait.SetActive(portrait.sprite != null);
            }
            else
            {
                portrait.SetActive(false);
            }
        }

        public event Action<string> OnFormattedCardDescription;

        public string FormatDescription(string description, GameCardState state = null)
        {
            if (string.IsNullOrEmpty(description))
            {
                return string.Empty;
            }

            var formattedDescription =CardDescriptionFormatter.Format(CardDescriptionFormatter.FormatNumbers(description, data, state));
            OnFormattedCardDescription?.Invoke(formattedDescription);

            return formattedDescription;
        }

        public static List<Type> GetRequiredComponentsForStandardHandCard()
        {
            return new List<Type> {typeof(GameCardView), typeof(DragRotator), typeof(HoverCard), typeof(SpellTargetComponent), typeof(CardEventsComponent) };
        }

        [Flags]
        public enum CardViewType
        {
            STANDARD,
            ZOOMED,
            BOARD
        }
    }
}
