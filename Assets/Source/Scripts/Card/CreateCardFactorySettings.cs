using System;
using System.Collections.Generic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using UnityEngine;

namespace CrossBlitz.Card
{
    public class CreateCardFactorySettings
    {
        public GameObject CardPrefab;
        public CardData CardData;
        public GameCardState CardState;
        public RectTransform Layout;
        public Vector2 CardObjectOffsetPosition;
        public string SortingLayer;
        public int SortingOrder;
        public bool RemoveCanvas;
        public bool StartsDisabled;
        public bool RemoveShadow;
        public List<Type> AdditionalComponents;
    }
}