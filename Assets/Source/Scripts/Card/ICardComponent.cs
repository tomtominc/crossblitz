using CrossBlitz.Card;
using CrossBlitz.GameVfx;

namespace Source.Scripts.Card
{
    public interface ICardComponent
    {
        void Initialize(CardView card);
        void OnAfterCreation();
    }

    public interface ICardEffectComponent
    {
        IGameVfx Effect { get;  }
        void LoadEffect();
    }
}
