using System;
using CrossBlitz.Card.Utilities;
using TMPro;
using UnityEngine;

namespace CrossBlitz.Card
{
    public class KeywordDescription : MonoBehaviour
    {
        public TextMeshProUGUI m_Keyword;
        public TextMeshProUGUI m_Description;
        public GameObject modifierIcon;

        public void SetKeyword(string keyword, string description, bool fromModifier)
        {
            m_Keyword.text = CardDescriptionFormatter.Format(keyword);
            m_Description.text = description;
            modifierIcon.SetActive(fromModifier);
        }
    }
}