﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card.Data;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.Databases;
using CrossBlitz.Hero;
using CrossBlitz.Items.Data;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.ServerAPI.GameLogic.Intelligence;
using CrossBlitz.Utils;
using GameDataEditor;
using Newtonsoft.Json;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.Serialization;

namespace CrossBlitz.Card
{
    [Serializable]
    public class CardData : CharacterData
    {
        // x DescriptionFormattingDatabase
        // BlitzBurstDatabase
        // AiPropertiesDatabase
        // CraftingDatabase

        [ShowIf("ShowFormattingOptions")] [FoldoutGroup("Basic Information")] public bool hasDescriptionFormatting;
        [ShowIf("ShowFormattingOptions")] [FoldoutGroup("Basic Information")] public AbilityCondition formatCondition;
        [FoldoutGroup("Basic Information", true)] public string nameAbbrev;
        [FoldoutGroup("Basic Information")] public int health;
        [FoldoutGroup("Basic Information")] public int power;
        [FoldoutGroup("Basic Information")] public int cost;
        [FoldoutGroup("Basic Information")] public Vector2Int descriptionOffset = new Vector2Int(-2, -61);
        [FoldoutGroup("Basic Information")] public Vector2Int descriptionSize = new Vector2Int(116,66);
        [FoldoutGroup("Basic Information")] public CardSet set;
        [FoldoutGroup("Basic Information")] public AttackType attackType;
        [FoldoutGroup("Basic Information")] public Rarity rarity;
        [FoldoutGroup("Basic Information")] public Class @class;

        [ShowIf("IsFocus")] [FoldoutGroup("Focus")] [TextArea]public string focusedDescription;

        [ShowIf("@this.type == CardType.Power")] [FoldoutGroup("Blitz Burst")]public int topDescriptionOffset=-36;
        [ShowIf("@this.type == CardType.Power")] [FoldoutGroup("Blitz Burst")]public int topDescriptionWidth=116;
        [ShowIf("@this.type == CardType.Power")] [FoldoutGroup("Blitz Burst")]public int topDescriptionHeight=29;
        [ShowIf("@this.type == CardType.Power")] [FoldoutGroup("Blitz Burst")]public int bottomDescriptionOffset=-70;
        [ShowIf("@this.type == CardType.Power")] [FoldoutGroup("Blitz Burst")]public int bottomDescriptionWidth=116;
        [ShowIf("@this.type == CardType.Power")] [FoldoutGroup("Blitz Burst")]public int bottomDescriptionHeight=28;
        [ShowIf("@this.type == CardType.Power")] [FoldoutGroup("Blitz Burst")]public int burstBannerOffsetY=-52;
        [ShowIf("@this.type == CardType.Power")] [FoldoutGroup("Blitz Burst")][TextArea] public string burstRewardDescription;
        [ShowIf("@this.type == CardType.Power")] [FoldoutGroup("Blitz Burst")] public LimitBreakData limitBreak;

        [ShowIf("@this.type == CardType.Spell")] [FoldoutGroup("Effect Properties")] public bool usesTargetingEffect;
        [FoldoutGroup("Effect Properties")] public bool usesCustomEffect;
        [FoldoutGroup("Effect Properties")] public bool usesPreViz;

        [ShowIf("@this.type == CardType.Minion")] [FoldoutGroup("Effect Properties")]public bool hasCustomAttackProperties;
        [ShowIf("@this.type == CardType.Minion && this.hasCustomAttackProperties == true")] [FoldoutGroup("Effect Properties")] public bool usesExplosionEffectWhenAttacking;

        [FoldoutGroup("Effect Properties")]
        [ValueDropdown("GetAllCustomAttacks")]
        [ShowIf("@this.type == CardType.Minion && this.hasCustomAttackProperties == true")]
        public string customAttackEffect;

        [FoldoutGroup("Effect Properties")] [ShowIf("@this.usesCustomEffect == true")]
        public string effectName;
        [FoldoutGroup("Effect Properties")] [ShowIf("@this.usesCustomEffect == true")]
        public EffectParentBehaviour parentBehaviour;

        // [FoldoutGroup("Ability Editor")] public List<Ability> abilities;
        // [FoldoutGroup("Given Abilities")] [ShowIf("GivesCardsAbilities")] public List<Ability> givenAbilities;
        // [FoldoutGroup("Conditional Abilities")] [ShowIf("HasConditionalAbility")] public List<Ability> conditionalAbilities;
        [FoldoutGroup("AI")] public AiProperties aiProperties;
        [FoldoutGroup("Crafting")] public CardCraftingData craftingData;

        [FoldoutGroup("Offset Properties")] public Vector2Int boardOffset;
        [FoldoutGroup("Offset Properties")] public Vector2Int deckOffset;

        [HideInInspector] public string resourceType;
        [HideInInspector] public int resourceAmount;
        [HideInInspector] public int levelRewardUid;

        public bool ShowFormattingOptions()
        {
            return description.Contains("{0}");
        }
        public override int GetCost()
        {
            return cost;
        }

        public override int GetPower()
        {
            return power;
        }

        public override int GetHealth()
        {
            return health;
        }

        public List<Ability> GetAbilities()
        {
            if (string.IsNullOrEmpty(id)) return new List<Ability>();
            return Db.AbilityDatabase.GetAbilities(id);
        }

        public List<Ability> GetConditionalAbilities()
        {
            if (string.IsNullOrEmpty(id)) return new List<Ability>();
            return Db.AbilityDatabase.GetConditionalAbilities(id);
        }


        public List<Ability> GetGivenAbilities()
        {
            if (string.IsNullOrEmpty(id)) return new List<Ability>();
            return Db.AbilityDatabase.GetGivenAbilities(id);
        }

        public bool IsBattlecry()
        {
            var abilities = GetAbilities();
            if (abilities == null || abilities.Count <= 0) return false;
            return abilities.Exists(ability => ability.triggerType == TriggerType.BATTLECRY);
        }

        public bool GivesCardsAbilities()
        {
            var abilities = GetAbilities();
            if (abilities == null || abilities.Count <= 0)
            {
                return false;
            }

            var givenAbility = abilities.Exists(ability => ability.keyword == AbilityKeyword.GIVE_ABILITY);

            if (givenAbility)
            {
                return true;
            }

            // var focus = abilities.FindAll(ability => ability.keyword == AbilityKeyword.FOCUS);
            //
            // for (var i = 0; i < focus.Count; i++)
            // {
            //     if (focus[i].focusType == FocusType.GrantAbility) return true;
            // }

            var auras = abilities.FindAll(ability => ability.keyword == AbilityKeyword.AURA);

            for (var i = 0; i < auras.Count; i++)
            {
                if (auras[i].auraData == null) continue;
                if (auras[i].auraData.auraEffect == AuraData.AuraEffect.Give_Ability) return true;
            }

            if (abilities.Exists(a => a.summonMinionData != null && a.summonMinionData.grantAbilities))
            {
                return true;
            }

            return false;
        }

        public bool HasConditionalAbility()
        {
            if (string.IsNullOrEmpty(id)) return false;

            var abilities = GetAbilities();
            if (abilities == null || abilities.Count <= 0) return false;
            return abilities.Exists(ability => ability.hasConditionalAbility);
        }

        public List<string> GetTokens()
        {
            var abilities = GetAbilities();
            var tokens = new List<string>();

            if (abilities != null)
            {
                for (var i = 0; i < abilities.Count; i++)
                {
                    if (abilities[i].summonMinionData != null &&
                        !string.IsNullOrEmpty(abilities[i].summonMinionData.cardId) &&
                        id != abilities[i].summonMinionData.cardId)
                    {
                        tokens.Add(abilities[i].summonMinionData.cardId);
                    }

                    if (abilities[i].cardFilter != null &&
                        !string.IsNullOrEmpty(abilities[i].cardFilter.cardId)&&
                        id != abilities[i].cardFilter.cardId)
                    {
                        tokens.Add(abilities[i].cardFilter.cardId);
                    }

                    if (abilities[i].transformCardData != null &&
                        !string.IsNullOrEmpty(abilities[i].transformCardData.cardId) &&
                        id != abilities[i].transformCardData.cardId)
                    {
                        tokens.Add(abilities[i].transformCardData.cardId);
                    }
                }
            }

            return tokens;
        }

        public bool IsFocus()
        {
            var abilities = GetAbilities();
            return abilities != null && abilities.Find(a => a.triggerType == TriggerType.FOCUS) != null;
        }

        public static CardData FromItem(ItemDataInstance itemData)
        {
            return FromItem(Db.ItemDatabase.GetItem(itemData.ItemId), itemData.RemainingUses);
        }

        public static CardData FromItem(ItemData itemData, int amount, string heroName = default)
        {
            switch (itemData.ItemClass)
            {
                case ItemClassType.Card:
                    return Db.CardDatabase.GetCard(itemData.ItemId);
                case ItemClassType.Ingredient:
                    var ingData = new CardData();
                    ingData.type = CardType.Resource;
                    ingData.id = itemData.ItemId;
                    ingData.name = itemData.DisplayName;
                    ingData.portraitUrl = itemData.ItemImageUrl;
                    ingData.resourceType = itemData.ItemClass?.ToLower();
                    ingData.resourceAmount = amount;
                    ingData.description =
                        $"{new GDEItemData(itemData.ItemId).Description?.ToUpper()} USED FOR <color=#c24ca4>MANA MELDING.</color>";
                    ingData.faction = Crafting.GetItemFaction(itemData.ItemId);

                    return ingData;
                case ItemClassType.HpUp:
                    return new CardData
                    {
                        type = CardType.Resource,
                        id = itemData.ItemId,
                        name = itemData.DisplayName,
                        portraitUrl = itemData.ItemImageUrl,
                        resourceType = itemData.ItemClass.ToLower(),
                        resourceAmount = amount,
                        description = $"INCREASES {heroName}'S <color=#e56569>MAXIMUM HEALTH.</color>",
                        faction = Crafting.GetItemFaction(itemData.ItemId)
                    };
            }

            return null;
        }

        public static CardData FromLevelReward( LevelReward levelReward )
        {
            var cardData = new CardData();

            switch (levelReward.type)
            {
                case LevelReward.EType.Card:
                    cardData = Db.CardDatabase.GetCard(levelReward.cardId);
                    cardData.levelRewardUid = levelReward.uid;
                    break;
                case LevelReward.EType.HeathUp:
                    cardData.type = CardType.Resource;
                    cardData.levelRewardUid = levelReward.uid;
                    cardData.name = "HP UP";
                    cardData.resourceType = "hp-up";
                    cardData.portraitUrl = "hp-up";
                    cardData.resourceAmount = levelReward.healthPoints;
                    cardData.description = $"INCREASES <color=#e56569>MAXIMUM HEALTH.</color>";
                    break;
                case LevelReward.EType.RelicSlot:
                    cardData.type = CardType.Resource;
                    cardData.levelRewardUid = levelReward.uid;
                    cardData.name = "UNLOCK RELIC SLOT";
                    cardData.resourceType = "relic-slot";
                    cardData.portraitUrl = "relic-slot";
                    cardData.description = $"UNLOCKS A <color=#3D4D57>RELIC SLOT</color> FOR THIS HERO, ALLOWING YOU TO EQUIP MORE <color=#3D4D57>RELICS.</color>";
                    break;
                case LevelReward.EType.IngredientPouch:
                    var ingredientName = levelReward.ingredient;
                    cardData = FromItem(Db.ItemDatabase.GetItem(ingredientName), Crafting.GetLevelRewardIngredientAmountForIngredient(ingredientName));
                    cardData.levelRewardUid = levelReward.uid;
                    break;
                case LevelReward.EType.ManaShards:
                    cardData.type = CardType.Resource;
                    cardData.levelRewardUid = levelReward.uid;
                    cardData.resourceType = "mana-shard";
                    cardData.portraitUrl = "mana-shard";
                    cardData.name = "MANA SHARDS";
                    cardData.resourceAmount = levelReward.manaShards;
                    cardData.cost = levelReward.uid;
                    cardData.description = "A SPECIAL FORM OF CURRENCY USED FOR <color=#c24ca4>MANA MELDING.</color>";
                    break;
            }

            return cardData;
        }

        public static CardData FromCurrency(string currencyCode, int amount)
        {
            switch (currencyCode)
            {
                case Currency.MANA_SHARDS:
                    return new CardData
                    {
                        type = CardType.Resource,
                        id = currencyCode,
                        name = "Mana Shards",
                        portraitUrl = "mana-shard",
                        resourceType = "mana-shard",
                        description =  "USED FOR <color=#c24ca4>MANA MELDING.</color>",
                        resourceAmount = amount,
                    };
                case Currency.GOLD:
                    return new CardData
                    {
                        type = CardType.Resource,
                        id = currencyCode,
                        name = "Mana Shards",
                        portraitUrl = "cross-coins",
                        resourceType = "cross-coins",
                        resourceAmount = amount,
                    };
                case Currency.DAWN_DOLLARS:
                    return new CardData
                    {
                        type = CardType.Resource,
                        id = currencyCode,
                        name = "Dawn Dollars",
                        portraitUrl = "dawn-dollars",
                        resourceType = "dawn-dollars",
                        resourceAmount = amount,
                    };
            }

            return null;
        }

        public static readonly Dictionary<CardSet, string> CardSetToIcon = new Dictionary<CardSet, string>
        {
            { CardSet.All, "core"},
            { CardSet.Core, "core"},
            { CardSet.Deprecated, "core"},
            { CardSet.Token, "core"},
            { CardSet.PiratesAhoy, "pirates-play"},
            { CardSet.StarscapeTwilight, "musical-meddling"},
            { CardSet.MonksMantra, "monks-mantra"},
            { CardSet.PerilousPilfering, "perilous-pilfering"},
            { CardSet.SproutwoodGuardian, "regents-resolve"},
        };

        [System.Serializable]
        public class Changelog
        {
            public long time;
            public int oldPowerValue = -1;
            public int oldHealthValue = -1;
            public int oldManaValue = -1;
            public string oldDescription;
            public string comment;
        }

#if UNITY_EDITOR

        public static ValueDropdownList<string> gameVfxComponentNames;
        public IEnumerable GetAllCustomAttacks()
        {
            if (gameVfxComponentNames != null) return gameVfxComponentNames;

            gameVfxComponentNames = new ValueDropdownList<string>();

            var q = typeof(CustomAttackComponent).Assembly.GetTypes()
                .Where(x => !x.IsAbstract)
                .Where(x => !x.IsGenericTypeDefinition)
                .Where(x => typeof(CustomAttackComponent).IsAssignableFrom(x))
                .ToList();

            for (var i = 0; i < q.Count; i++)
            {
                gameVfxComponentNames.Add(q[i].GetNiceName(), q[i].AssemblyQualifiedName);
            }

            return gameVfxComponentNames;
        }

        [OnInspectorGUI]
        [PropertyOrder(-10), HorizontalGroup("Split", 100)]
        private void ShowImage()
        {
            if (!string.IsNullOrEmpty(portraitUrl))
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label(
                    UnityEditor.AssetDatabase.LoadAssetAtPath<Texture>(
                        $"Assets/Source/Addressables/card-artwork/Card Artwork/{portraitUrl}.png"));

                GUILayout.Label(
                    UnityEditor.AssetDatabase.LoadAssetAtPath<Texture>(
                        $"Assets/Source/Addressables/card-artwork/Color-Outlines/{portraitUrl}-outlined.png"));
                GUILayout.EndHorizontal();
            }
        }
#endif
    }

    [Serializable]
    public class Ability
    {
        // need this link so we can have it be in a separate database
        [HideInInspector]
        public string linkedToCardId;

        // [FoldoutGroup("Ability Base Values")]
        // public bool canTriggerInHand;
        [FoldoutGroup("Ability Base Values")] public CardLocation triggerLocation = CardLocation.Board;

        [InfoBox("Taunt minions should have a Target Condition of type PLAYER_SELECTED.", InfoMessageType.Warning,
            "ShowTauntError")]
        [FoldoutGroup("Ability Base Values")]
        public AbilityKeyword keyword;

        [FoldoutGroup("Ability Base Values")] public TriggerType triggerType;

        [DetailedInfoBox("What should be evaluated when this trigger happens?",
            "This just checks to see which card needs to be evaluated when the trigger goes off. Selecting both will check the target and if that does not work it will check the source card.\nEXAMPLE: If the trigger type is \"DAMAGED\" you probably always want to check the TARGET and not the source of the damage.")]
        [FoldoutGroup("Ability Base Values")]
        public TriggerEvaluateCondition triggerEvaluateCondition;

        [DetailedInfoBox("Conditions that activate an ability.",
            "After an ability is 'Triggered' through it's TriggerType parameter it will need to validate the conditions of what was triggered.\nExample: TriggerType 'Damaged', will sometimes need to verify if it was one of your minions or hero etc to activate.\nExample 2: Whenever a friendly minion takes damage, gain +2 armor. So the trigger type is \"damaged\" and the trigger conditions should be *PLAYER* + *MINION*")]
        [FoldoutGroup("Ability Base Values")]
        public FilterCondition triggerConditions;
        [FoldoutGroup("Ability Base Values")] public FilterConditionValues triggerValues;

        [DetailedInfoBox("Filter \"valid\" targets using this.",
            "Ability target works with the ability keyword, you should apply as many as fits the need of the ability.\nEXAMPLE: \"Whenever a friendly minion takes damage, gain +2 armor\". Don't worry about the first part if we're here we've already satisfied the trigger conditions. What we're looking at is \"gain +2 armor\", the Ability Keyword should be \"Gain Armor\" with the Ability Target being *PLAYER* + *HERO*")]
        [FormerlySerializedAs("filterConditions")]
        [FoldoutGroup("Ability Base Values")]
        public FilterCondition targetConditions;

        //[ShowIf("@this.targetConditions.HasFlag(FilterCondition.IS_FACTION) || this.targetConditions.HasFlag(FilterCondition.IS_CLASS) || this.targetConditions.HasFlag(FilterCondition.ADVANCED_FILTERS) || this.targetConditions.HasFlag(FilterCondition.MAX_TARGET_COUNT) || this.targetConditions.HasFlag(FilterCondition.THIS_GAME_EFFECT)")]
        [FormerlySerializedAs("FilterValues")] [FoldoutGroup("Ability Base Values")]
        public FilterConditionValues filterValues;

        //[ShowIf("@this.triggerConditions.HasFlag(FilterCondition.IS_FACTION) || this.triggerConditions.HasFlag(FilterCondition.IS_CLASS) || this.triggerConditions.HasFlag(FilterCondition.ADVANCED_FILTERS) || this.triggerConditions.HasFlag(FilterCondition.MAX_TARGET_COUNT) || this.targetConditions.HasFlag(FilterCondition.THIS_GAME_EFFECT)")]

        [FoldoutGroup("Ability Base Values")] [TextArea] public string abilityDescription;

        [FoldoutGroup("Ability Properties")]
        public bool exhaust;
        [FoldoutGroup("Ability Properties")]
        public bool oncePerTurn;
        [FoldoutGroup("Effect Properties")]
        public bool usesCustomEffect;
        [FoldoutGroup("Effect Properties")] [ShowIf("usesCustomEffect")]
        public bool exitResolveAfterCustomEffect;
        [FoldoutGroup("Effect Properties")] [ShowIf("usesCustomEffect")]
        public bool spawnEffectOnEachTarget;
        [FoldoutGroup("Effect Properties")] [ShowIf("@this.keyword == AbilityKeyword.DAMAGE || this.keyword == AbilityKeyword.DESTROY")] [ShowIf("usesCustomEffect")]
        public bool applyDamageInEffect;
        [FoldoutGroup("Effect Properties")] [ShowIf("usesCustomEffect")]
        public bool usesBoardEffect;
        [FoldoutGroup("Effect Properties")] [ShowIf("@this.usesCustomEffect == true && this.usesBoardEffect == false")] [ValueDropdown("GetAllGameVfxTypes")]
        public string customEffectComponent;
        [FoldoutGroup("Effect Properties")] [ShowIf("usesBoardEffect")]
        public string boardEffectPrefab;
        [FoldoutGroup("Effect Properties/Custom")]  [ShowIf("usesBoardEffect")]
        public bool spawnOnEmptyColumn;
        [FoldoutGroup("Effect Properties/Custom")] [ShowIf("usesBoardEffect")]
        public bool spawnOnEmptyRow;
        [FoldoutGroup("Effect Properties/Custom")] [ShowIf("usesBoardEffect")]
        public int effectSubType;
        [FoldoutGroup("Ability Base Values")]
        public bool usesGenericIcon;
        [FoldoutGroup("Ability Properties")] [ShowIf("@this.keyword == AbilityKeyword.DAMAGE")]
        public bool damageBasedOnConditions;
        [FoldoutGroup("Ability Properties")]
        [ShowIf("@this.keyword == AbilityKeyword.DAMAGE && this.damageBasedOnConditions == true")]
        public AbilityCondition damageAmountCondition;
        [FoldoutGroup("Ability Properties")]
        [ShowIf("@this.damageBasedOnConditions == false && (this.keyword == AbilityKeyword.DAMAGE || this.keyword == AbilityKeyword.THORNS)")]
        public Class damageSchool;
        [FoldoutGroup("Ability Properties")]
        [ShowIf("@this.damageBasedOnConditions == false && (this.keyword == AbilityKeyword.DAMAGE || this.keyword == AbilityKeyword.THORNS)")]
        public int damageValue;
        [FoldoutGroup("Ability Properties")] [ShowIf("@this.keyword == AbilityKeyword.DAMAGE")]
        public bool damageRandomlySplit;
        [FoldoutGroup("Ability Properties")] [ShowIf("@this.keyword == AbilityKeyword.DAMAGE")]
        public bool targetCountBasedOnConditions;
        [FoldoutGroup("Ability Properties")] [ShowIf("@this.keyword == AbilityKeyword.DAMAGE || this.keyword == AbilityKeyword.TRANSFORM_CARDS")]
        public bool repeatDamageBasedOnConditions;
        [FoldoutGroup("Ability Properties")]
        [ShowIf("@this.keyword == AbilityKeyword.DAMAGE && (this.targetCountBasedOnConditions == false || this.repeatDamageBasedOnConditions == false)")]
        public int targetCount;
        [FoldoutGroup("Ability Properties")]
        [ShowIf("@this.keyword == AbilityKeyword.DAMAGE && this.targetCountBasedOnConditions == true")]
        public AbilityCondition targetCountCondition;
        [FoldoutGroup("Ability Properties")]
        [ShowIf("@this.keyword == AbilityKeyword.DAMAGE || this.keyword == AbilityKeyword.TRANSFORM_CARDS && this.repeatDamageBasedOnConditions == true")]
        public AbilityCondition repeatCountCondition;
        [FoldoutGroup("Ability Properties")] [ShowIf("@this.keyword == AbilityKeyword.AURA")]
        public AuraData auraData;
        [FoldoutGroup("Ability Properties")] [ShowIf("@this.keyword == AbilityKeyword.MODIFY_MANA_CRYSTALS")]
        public bool modifyMaximumManaOnly;
        [FoldoutGroup("Ability Properties")]
        [ShowIf("@this.keyword == AbilityKeyword.MODIFY_MANA_CRYSTALS || this.keyword == AbilityKeyword.MODIFY_STARTING_MANA || this.keyword == AbilityKeyword.MODIFY_COST")]
        public int manaModifier;
        [FoldoutGroup("Ability Properties")] [ShowIf("@this.keyword == AbilityKeyword.GAIN_ARMOR")]
        public bool armorGainBasedOnConditions;
        [FoldoutGroup("Ability Properties")]
        [ShowIf("@this.keyword == AbilityKeyword.GAIN_ARMOR && this.armorGainBasedOnConditions == true")]
        public AbilityCondition armorCondition;
        [FoldoutGroup("Ability Properties")] [ShowIf("@this.keyword == AbilityKeyword.GAIN_ARMOR")]
        public int armorModifier;

        /// <summary>
        /// Focus count in the context of "Trigger" is a regular focus card with the focus keyword Focus (X): Do something.
        /// In the context of an ability, it "Focuses" other cards by this amount.
        /// </summary>
        [FoldoutGroup("Ability Properties")]
        [ShowIf("@this.triggerType == TriggerType.FOCUS || this.keyword == AbilityKeyword.FOCUS")]
        public int focusCount;

        [FoldoutGroup("Ability Properties")]
        [ShowIf("@this.keyword == AbilityKeyword.GIVE_ABILITY && this.targetConditions == FilterCondition.SELF")]
        public bool replaceAbility;

        [FoldoutGroup("Ability Properties")]
        [ShowIf("@this.keyword == AbilityKeyword.GIVE_ABILITY && this.targetConditions == FilterCondition.SELF")]
        public int replacementIndex;

        [FoldoutGroup("Ability Properties")]
        [ShowIf(
            "@this.keyword == AbilityKeyword.MODIFY_POWER || this.keyword == AbilityKeyword.MODIFY_HEALTH || this.keyword == AbilityKeyword.MODIFY_HEALTH_AND_POWER")]
        public bool setInsteadOfModify;

        [FoldoutGroup("Ability Properties")]
        [ShowIf(
            "@this.keyword == AbilityKeyword.MODIFY_POWER || this.keyword == AbilityKeyword.MODIFY_HEALTH_AND_POWER")]
        public int powerModifier;

        [FoldoutGroup("Ability Properties")]
        [ShowIf(
            "@this.keyword == AbilityKeyword.MODIFY_HEALTH || this.keyword == AbilityKeyword.MODIFY_HEALTH_AND_POWER || this.keyword == AbilityKeyword.MODIFY_STARTING_HEALTH")]
        public int healthModifier;

        [FoldoutGroup("Ability Properties")] [ShowIf("@this.keyword == AbilityKeyword.HEAL")]
        public int healAmount;

        [FoldoutGroup("Ability Properties")] [ShowIf("@this.keyword == AbilityKeyword.ARMOR_BODY")]
        public int armorBodyAmount;

        [FoldoutGroup("Ability Properties")] [ShowIf("@this.keyword == AbilityKeyword.MODIFY_DAMAGE")]
        public int maxDamageTakenDuringHit;

        [FoldoutGroup("Ability Properties")] [ShowIf("@this.keyword == AbilityKeyword.DRAW")]
        public bool fillHand;

        [FoldoutGroup("Ability Properties")]
        [ShowIf(
            "@this.fillHand == false && (this.keyword == AbilityKeyword.DRAW || this.keyword == AbilityKeyword.DRAW_FROM_BOTTOM || this.keyword == AbilityKeyword.GET_CARD_FROM_DECK || this.keyword == AbilityKeyword.ADD_CARD_TO_HAND || this.keyword == AbilityKeyword.ADD_CARD_TO_DECK || this.keyword == AbilityKeyword.MODIFY_STARTING_DRAW)")]
        public int drawCount;

        [FoldoutGroup("Ability Properties")]
        [ShowIf(
            "@this.keyword == AbilityKeyword.DRAW || this.keyword == AbilityKeyword.DRAW_FROM_BOTTOM || this.keyword == AbilityKeyword.GET_CARD_FROM_DECK")]
        public DrawCondition drawCondition;

        [FoldoutGroup("Ability Properties")]
        [ShowIf(
            "@this.keyword == AbilityKeyword.SUMMON || this.keyword == AbilityKeyword.SUMMON_COPIES || this.keyword == AbilityKeyword.OVERGROW")]
        public SummonMinionData summonMinionData;

        [FoldoutGroup("Ability Properties")] [ShowIf("@this.keyword == AbilityKeyword.CAST_CARD")]
        public bool repeats;

        [FoldoutGroup("Ability Properties")] [ShowIf("repeats")]
        public int repeatMin;

        [FoldoutGroup("Ability Properties")] [ShowIf("repeats")]
        public int repeatMax;

        [FoldoutGroup("Ability Properties")]
        [ShowIf("@this.keyword == AbilityKeyword.GIVE_ABILITY")]
        public bool usesRandomGivenAbility;
        [FoldoutGroup("Ability Properties")]
        [ShowIf("@this.keyword == AbilityKeyword.GIVE_ABILITY && this.usesRandomGivenAbility")]
        public bool getLinkedAbilities;
        [FoldoutGroup("Ability Properties")]
        [ShowIf("@this.keyword == AbilityKeyword.GIVE_ABILITY && this.usesRandomGivenAbility")]
        public bool checkTriggerInsteadOfKeyword;

        [FoldoutGroup("Ability Properties")]
        [ShowIf(
            "@this.keyword == AbilityKeyword.GET_CARD_FROM_DECK || this.keyword == AbilityKeyword.ADD_CARD_TO_HAND || this.keyword == AbilityKeyword.CAST_CARD || this.keyword == AbilityKeyword.ADD_CARD_TO_DECK || this.keyword == AbilityKeyword.REDEEM || (this.keyword == AbilityKeyword.GIVE_ABILITY && this.usesRandomGivenAbility)")]
        public CardFilter cardFilter;

        [FoldoutGroup("Ability Properties")] [ShowIf("@this.keyword == AbilityKeyword.DISCARD")]
        public bool removeAllCards;

        [FoldoutGroup("Ability Properties")]
        [ShowIf("@this.keyword == AbilityKeyword.DISCARD && this.removeAllCards == false")]
        public int amountOfCardsToRemove;

        [FoldoutGroup("Ability Properties")] [ShowIf("@this.keyword == AbilityKeyword.TRANSFORM_CARDS")]
        public TransformCardData transformCardData;

        [FoldoutGroup("Ability Properties")] [ShowIf("@this.keyword == AbilityKeyword.MOVE_MINION_TO_ZONE")]
        public FilterCondition ownerOfZone;
        [FoldoutGroup("Ability Properties")] [ShowIf("@this.keyword == AbilityKeyword.MOVE_MINION_TO_ZONE")]
        public CardLocation zoneToMoveTo;

        [FoldoutGroup("Ability Properties")] [ShowIf("@this.keyword == AbilityKeyword.TRIGGER_ABILITY")]
        public int triggerTimes;
        [FoldoutGroup("Ability Properties")] [ShowIf("@this.keyword == AbilityKeyword.TRIGGER_ABILITY")]
        public TriggerType triggeredAbility;

        [FoldoutGroup("Conditional Properties")]
        public bool hasConditionalAbility;

        [FoldoutGroup("Conditional Properties")] [ShowIf("@this.hasConditionalAbility == true")]
        public List<AbilityCondition> conditions;

        [FoldoutGroup("Ability Properties")]
        public AbilityCondition abilityCondition;

        public int GetValue()
        {
            return Mathf.Max(damageValue, armorModifier, powerModifier, healthModifier, healAmount,
                armorBodyAmount);
        }

        public void ResetValues()
        {
            damageValue = armorModifier = powerModifier = 0;
            healthModifier = healAmount;
            armorBodyAmount = 0;
        }

        public static bool UsesCustomIcon(Ability ability)
        {
            return ability.keyword == AbilityKeyword.ARMOR_BODY ||
                    //ability.keyword == AbilityKeyword.LIFESTEAL ||
                    //ability.keyword == AbilityKeyword.DUAL_STRIKE ||
                    ability.keyword == AbilityKeyword.PIERCE ||
                    ability.keyword == AbilityKeyword.POISON ||
                    ability.keyword == AbilityKeyword.SPLASH ||
                    ability.triggerType == TriggerType.DEATHRATTLE ||
                    ability.triggerType == TriggerType.PLUNDER ||
                    //ability.keyword == AbilityKeyword.RUSH ||
                    ability.keyword == AbilityKeyword.SWIFT_STRIKE;
        }

        public bool IsRiff()
        {
            return triggerType == TriggerType.PLAYED_SPELL &&
                   triggerEvaluateCondition == TriggerEvaluateCondition.SOURCE &&
                   triggerConditions.HasFlag(FilterCondition.PLAYER) &&
                   triggerConditions.HasFlag(FilterCondition.SPELL) && exhaust;
        }

        public static bool UsesGenericTriggerIcon(Ability ability)
        {
            return ability.usesGenericIcon;
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Server.JsonSettings);
        }

        public static Ability FromJson(string json)
        {
            return JsonConvert.DeserializeObject<Ability>(json, Server.JsonSettings);
        }

#if UNITY_EDITOR

        public static bool ShowTauntError(AbilityKeyword keyword)
        {
            return keyword == AbilityKeyword.TAUNT;
        }

        public static ValueDropdownList<string> cards;

        private static IEnumerable GetAllCards()
        {
            if (cards != null) return cards;

            cards = new ValueDropdownList<string>();

            if (!Db.Initialized)
            {
                Db.ForceInit();
            }

            for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
            {
                var cardData = Db.CardDatabase.Cards[i];
                cards.Add($"{cardData.faction}/{cardData.name}", cardData.id);
            }

            return cards;
        }

        public static ValueDropdownList<string> gameVfxComponentNames;
        public IEnumerable GetAllGameVfxTypes()
        {
            if (gameVfxComponentNames != null) return gameVfxComponentNames;

            gameVfxComponentNames = new ValueDropdownList<string>();

            var q = typeof(GameVfxComponent).Assembly.GetTypes()
                .Where(x => !x.IsAbstract)
                .Where(x => !x.IsGenericTypeDefinition)
                .Where(x => typeof(GameVfxComponent).IsAssignableFrom(x))
                .ToList();

            for (var i = 0; i < q.Count; i++)
            {
                gameVfxComponentNames.Add(q[i].GetNiceName(), q[i].AssemblyQualifiedName);
            }

            return gameVfxComponentNames;
        }
#endif
    }

    [Serializable]
    public class FilterConditionValues
    {

        public enum StatFormula
        {
            None,
            LessThanOrEqualTo,
            LessThan,
            GreaterThanOrEqualTo,
            GreaterThan,
            Equal
        }

        public enum Stat
        {
            None,
            Health,
            Power,
            ManaCost,
            Armor,
            Damage,
        }

        public Faction faction;
        public Class @class;
        public bool lastRedeemedCard;
        public bool notFromSameFaction;
        public bool doesNotHaveStatusEffect;
        public StatusEffect statusEffect;
        public bool maxCountBasedOnPreviousCommandTargetCount;
        public int maxCount;
        public bool turnRestricted;
        [ShowIf("turnRestricted")]
        public Team turnItNeedsToBe;
        public CardLocation fromLocation;
        public CardLocation toLocation;
        public Stat stat;
        [ShowIf("@this.stat != Stat.None")]
        public StatFormula statFormula;
        [ShowIf("@this.statFormula != StatFormula.None && this.stat != Stat.None")]
        public int statScore;
        public bool doesNotHaveKeyword;
        public AbilityKeyword keyword;
        public bool doesNotHaveTrigger;
        public TriggerType trigger;
        public bool checkAbilityConditionToTrigger;
        public bool useExactTilePositions;
        [ShowIf("useExactTilePositions")]
        public List<Vector2Int> tilePositions;

        public bool allowMultipleSpecificCards;
        [ValueDropdown("GetAllCards")]
        public string specificTargetCard;



#if UNITY_EDITOR
        public static ValueDropdownList<string> cards;

        private static IEnumerable GetAllCards()
        {
            if (cards != null) return cards;

            cards = new ValueDropdownList<string>();

            if (!Db.Initialized)
            {
                Db.ForceInit();
            }

            for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
            {
                var cardData = Db.CardDatabase.Cards[i];
                cards.Add($"{cardData.faction}/{cardData.name}", cardData.id);
            }


            return cards;
        }
#endif
    }

    [Serializable]
    public class CardFilter
    {
        public bool drawCountBasedOnConditions;
        [ShowIf("drawCountBasedOnConditions")] public Condition drawCountCondition;
        [ShowIf("@this.drawCountCondition == Condition.SPELL_DAMAGE")]
        public Class spellSchool;
        [ValueDropdown("GetAllCards")] public string cardId;
        public bool addAllRandomCards;
        [ValueDropdown("GetAllCards")] public List<string> randomCardIds;
        public bool grantedToOpponent;
        public bool fromDiscardedCardsThisGame;
        public bool fromSameSpellSchoolAsLastPlayedSpell;
        public bool copiesFromAbilityTargets;
        public bool grantACopyFromLocation;
        [ShowIf("grantACopyFromLocation")] public FilterCondition copyFromLocation;
        public bool fromOpponentsFaction;
        public bool fromAnotherFaction;
        public bool limitToSameFaction;
        public bool lastRedeemedCard;
        public Class fromClass = Class.None;
        public CardType fromType = CardType.None;
        public Rarity fromRarity = Rarity.All;
        public CardLocation fromLocation = CardLocation.Null;
        public AbilityKeyword keyword = AbilityKeyword.NONE;
        public TriggerType trigger = TriggerType.NONE;
        public FilterConditionValues.Stat withStat;
        [ShowIf("@this.withStat != FilterConditionValues.Stat.None")]
        public FilterConditionValues.StatFormula formula;
        [ShowIf("@this.withStat != FilterConditionValues.Stat.None")]
        public int statAmount;

        [BoxGroup("Stat Overrides")] public bool overrideCopiedCardStats;
        [ShowIf("overrideCopiedCardStats")] [BoxGroup("Stat Overrides")] public int costOverride = -1;
        [ShowIf("overrideCopiedCardStats")] [BoxGroup("Stat Overrides")] public int powerOverride = -1;
        [ShowIf("overrideCopiedCardStats")] [BoxGroup("Stat Overrides")] public int healthOverride = -1;

        public static List<CardData> GetCardsWithFilter(List<CardData> input, CardFilter filter)
        {
            var output = new List<CardData>( input );

            if (filter.fromClass != Class.All && filter.fromClass != Class.None)
            {
                output.RemoveAll( c => c.@class != filter.fromClass);
            }

            if (filter.keyword != AbilityKeyword.NONE)
            {
                output.RemoveAll( c => !c.GetAbilities().Exists( a => a.keyword == filter.keyword));
            }

            if (filter.trigger != TriggerType.NONE)
            {
                // 1
                //output.RemoveAll( card => !card.GetAbilities().Exists( ability => ability != null && ability.triggerType == filter.trigger));

                // 2
                for (var i = output.Count-1; i > -1; i--)
                {
                    var card = output[i];
                    var abilities = card.GetAbilities();
                    var passes = false;

                    for (var j = 0; j < abilities.Count; j++)
                    {
                        var ability = abilities[j];
                        passes = ability.triggerType == filter.trigger;

                        if (passes) break;
                    }

                    if (!passes)
                    {
                        output.RemoveAt(i);
                    }
                }
            }

            if (!string.IsNullOrEmpty(filter.cardId))
            {
                output.RemoveAll( c => c.id != filter.cardId);
            }

            if (filter.fromType != CardType.None)
            {
                output.RemoveAll( c => c.type != filter.fromType);

                if (filter.fromType == CardType.Spell)
                {
                    output.RemoveAll(c => c.GetAbilities().Count <= 0);
                }
            }

            if (filter.randomCardIds != null && filter.randomCardIds.Count > 0)
            {
                output.Clear();

                for (var i = 0; i < filter.randomCardIds.Count; i++)
                {
                    output.Add(Db.CardDatabase.GetCard(filter.randomCardIds[i]));
                }
            }

            if (filter.withStat != FilterConditionValues.Stat.None &&
                filter.formula != FilterConditionValues.StatFormula.None)
            {
                output = Db.CardDatabase.GetCardsByStatFormula(output,filter.withStat,filter.formula, filter.statAmount, Faction.All);
            }

            // add more as needed :)

            return output;
        }

        public static List<CardData> GetCardsWithFilter(CardFilter filter)
        {
            var output = new List<CardData>();
            var temp = new List<GameCardState>(GameServer.State.Cards);

            if (filter.fromLocation != CardLocation.Null)
            {
                temp.RemoveAll(c => c.location != filter.fromLocation);
            }

            if (filter.lastRedeemedCard)
            {
                var history = GameServer.State.GetLastHistoryOf(CommandType.REDEEM);
                if (history == null) return new List<CardData>();
                var redeemCommand = (RedeemCommand)history;

                temp.RemoveAll(c => c.characterData.id != redeemCommand.RedeemedCard);
            }

            for (var i = 0; i < temp.Count; i++)
            {
                output.Add( Db.CardDatabase.GetCard( temp[i].characterData.id ));
            }

            return GetCardsWithFilter(output, filter);
        }

#if UNITY_EDITOR
        public static ValueDropdownList<string> cards;

        private static IEnumerable GetAllCards()
        {
            if (cards != null) return cards;

            cards = new ValueDropdownList<string>();

            if (!Db.Initialized)
            {
                Db.ForceInit();
            }

            for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
            {
                var cardData = Db.CardDatabase.Cards[i];
                cards.Add($"{cardData.faction}/{cardData.name}", cardData.id);
            }


            return cards;
        }
#endif
    }

    [Serializable]
    public class TransformCardData
    {
        public enum TransformType
        {
            SpecificCard,
            Evolve,
            LastSummonedMinion,
            CostBased,
            ByRarity,
        }

        public bool keepStatsAndUpgradesOfPreviousMinion;
        public TransformType transformType;

        [ShowIf("@this.transformType == TransformType.SpecificCard")]
        [ValueDropdown("GetAllCards")]
        public string cardId;

        // [ShowIf("@this.transformType == TransformType.Evolve")]
        // public bool devolveIfEnemy;
        [ShowIf("@this.transformType == TransformType.Evolve")]
        public int evolveCostChange;

        [ShowIf("@this.transformType == TransformType.CostBased")]
        public FilterConditionValues.StatFormula costFormula;
        [ShowIf("@this.transformType == TransformType.CostBased")]
        public int cost;

        [ShowIf("@this.transformType == TransformType.ByRarity")]
        public Rarity rarity;

#if UNITY_EDITOR
        public static ValueDropdownList<string> cards;

        private static IEnumerable GetAllCards()
        {
            if (cards != null) return cards;

            cards = new ValueDropdownList<string>();

            if (!Db.Initialized)
            {
                Db.ForceInit();
            }

            for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
            {
                var cardData = Db.CardDatabase.Cards[i];
                cards.Add($"{cardData.faction}/{cardData.name}", cardData.id);
            }


            return cards;
        }
#endif
    }

    [System.Serializable]
    public class CardLocationData : UniqueItem
    {
        public enum Location
        {
            None,
            StartingDeck, // meldable
            CardPrizePool, // meldable
            Battle,
            Shop, // exclusive
            Cutscene, // exclusive
            LevelReward, // exclusive, meldable
            Chest // exclusive
        }

        public Location location;

        // inside starting deck
        public string startingDeckHeroId;

        // in chapter prize pool
        public string chapterNamePrizePool;

        // battle
        public string battleUid;

        // Shop Buy-able
        public int shopTileUid;

        // Cutscene Reward
        public string cutsceneUid;

        // Level Reward
        public string levelRewardHeroId;
        public int levelRewardUid;

        // Chest Reward
        public int chestTileUid;

        // unused data but this is what tile exactly - could be useful someday
        public string mapUid;
        public string roomUid;
        public int tileUid;
    }

    public enum LimitBreakType
    {
        SUMMON_CLASS,
        PLAY_CARD_BY_NAME,
        ADD_CARD_BY_NAME_TO_OPPONENTS_DECK,
        MINIONS_ON_YOUR_SIDE_OF_THE_FIELD,
        MINIONS_ARE_FROZEN,
        DRAW_CARDS,
        PLAY_MINION_WITH_TRIGGER,
        DISCARD_CARDS,
        GAIN_ARMOR,
        DAMAGE_WITH_SPELLS,
        SUMMON_CARD_BY_NAME,
        GIVE_OR_RESTORE_HEALTH,
        PLAY_CARD_FROM_CLASS,
        SUMMON_ANY_MINION,
        PLAY_CARD_FROM_ANOTHER_FACTION,
        TRAPS_TRIGGERED_BY_PLAYER,
        TARGET_MINION_WITH_SPELL,
        SET_TRAPS,
        YOUR_MINIONS_HAVE_BEEN_DESTROYED,
        FRIENDLY_MINIONS_DAMAGED_BY_YOU,
    }

    [Serializable]
    public class LimitBreakData
    {
        public LimitBreakType type;
        public int value;

        [ShowIf("@this.type == LimitBreakType.SUMMON_CLASS || this.type == LimitBreakType.PLAY_CARD_FROM_CLASS || this.type == LimitBreakType.TARGET_MINION_WITH_SPELL")]
        public Class @class;
        [ShowIf("@this.type == LimitBreakType.PLAY_MINION_WITH_TRIGGER")]
        public TriggerType triggerType;

        [ShowIf(
            "@this.type == LimitBreakType.PLAY_CARD_BY_NAME || this.type == LimitBreakType.SUMMON_CARD_BY_NAME || this.type == LimitBreakType.ADD_CARD_BY_NAME_TO_OPPONENTS_DECK")]
        [ValueDropdown("GetAllCards")]
        public string cardId;

        [ShowIf("@this.type == LimitBreakType.MINIONS_ON_YOUR_SIDE_OF_THE_FIELD")] [ValueDropdown("GetAllCards")]
        public List<string> cardIds;

#if UNITY_EDITOR
        public static ValueDropdownList<string> cards;

        private static IEnumerable GetAllCards()
        {
            if (cards != null) return cards;

            if (!Db.Initialized)
            {
                Db.ForceInit();
            }

            cards = new ValueDropdownList<string>();

            for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
            {
                var cardData = Db.CardDatabase.Cards[i];
                cards.Add($"{cardData.faction}/{cardData.name}", cardData.id);
            }


            return cards;
        }
#endif
    }

    [System.Serializable]
    public class AbilityCondition
    {
        public Condition condition;
        public FilterCondition filters;

        [ShowIf("@this.filters.HasFlag(FilterCondition.ADVANCED_FILTERS)")]
        public FilterConditionValues filterValues;

        [ShowIf("@this.condition == Condition.FACTION_ON_FIELD")]
        public Faction faction;

        [ShowIf("@this.condition == Condition.CLASS_ON_FIELD")]
        public Class @class;

        [ShowIf("@this.condition == Condition.TRAPS_SET")]
        public int conditionCount;

        [ShowIf("@this.condition == Condition.NUMBER_OF_SPECIFIC_CARDS_IN_DECK || this.condition == Condition.NUMBER_OF_SPECIFIC_CARDS_ON_FIELD || this.condition == Condition.SPECIFIC_MINION_DIED_THIS_GAME || this.condition == Condition.NUMBER_OF_SPECIFIC_CARDS_PLAYED")]
        [ValueDropdown("GetAllCards")]
        public string cardId;

        [ShowIf("@this.condition == Condition.PREVIOUS_COMMAND_TARGETS")]
        public bool usePreviousLinkedCommandNotParent;

        [ShowIf("@this.condition == Condition.SOURCE_CARD_FROM_RESOLVE_TRIGGER || this.condition == Condition.TARGET_CARD_FROM_RESOLVE_TRIGGER")]
        public FilterConditionValues.Stat stat;

#if UNITY_EDITOR
        public static ValueDropdownList<string> cards;

        private static IEnumerable GetAllCards()
        {
            if (cards != null) return cards;

            if (!Db.Initialized)
            {
                Db.ForceInit();
            }

            cards = new ValueDropdownList<string>();

            for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
            {
                var cardData = Db.CardDatabase.Cards[i];
                cards.Add($"{cardData.faction}/{cardData.name}", cardData.id);
            }


            return cards;
        }
#endif

    }

    [Serializable]
    public class DrawCondition
    {
        [InfoBox("Draws based on how many of these conditions are met.")]
        public Condition condition;
        public FilterCondition filter;

        [ShowIf("@this.condition == Condition.FACTION_ON_FIELD")]
        public Faction faction;

        [ShowIf("@this.condition == Condition.CLASS_ON_FIELD")]
        public Class @class;
    }


    [Serializable]
    public class SummonMinionData
    {
        public enum SummonVisualEffect
        {
            FromHand,
            FromAbility,
            Blood,
            Slime,
            FromOutsideScreen,
            Fire,
            Soul,
            Overgrow,
            BrigadeTrap
        }

        public enum SummonFrom
        {
            SpecificCard,
            Filter,
            Discarded_From_Last_Ability
        }

        public enum PotentialCardTargets
        {
            Card_Database_Faction_Restricted,
            GameCards,
            Card_Database_No_Faction_Restrictions
        }

        public int count;
        public bool disableSquash;
        public bool disableIfMinionExists;
        public SummonPlacement placement;
        public SummonFrom summonFrom;
        [ShowIf("@this.summonFrom == SummonFrom.Filter")]  public PotentialCardTargets potentialCardTargets;
        [ShowIf("@this.summonFrom == SummonFrom.Filter")] public CardFilter cardFilter;
        [ShowIf("@this.summonFrom == SummonFrom.SpecificCard")] [ValueDropdown("GetAllCards")] public string cardId;
        [ShowIf("@this.placement == SummonPlacement.EXACT_POSITION")] public Vector2Int tilePosition;
        public bool grantAbilities;
        public SummonVisualEffect summonVisualEffect;
        [HideInInspector]
        public bool relentlessSummon;



#if UNITY_EDITOR
        public static ValueDropdownList<string> cards;

        private static IEnumerable GetAllCards()
        {
            if (cards != null && cards.Count > 0)
            {
                return cards;
            }

            if (!Db.Initialized)
            {
                Db.ForceInit();
            }

            cards = new ValueDropdownList<string>();

            for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
            {
                var cardData = Db.CardDatabase.Cards[i];
                cards.Add($"{cardData.faction}/{cardData.name}", cardData.id);
            }


            return cards;
        }
#endif
    }

    [System.Serializable]
    public class AuraData
    {
        public enum AuraEffect
        {
            None,
            Double_Battlecry,
            Double_Deathrattle,
            DrawModifier,
            StatModifier,
            Double_Plunder,
            SpellDamage,
            Transform_Summoned_Minions,
            Damage_Spells_Heal_Instead,
            Give_Ability,
            Use_Conditional_Ability,
            Cards_Cost_Health_Instead_Of_Mana,
        }

        [System.Flags]
        public enum RelativeBoardSpace
        {
            Up = 1 << 1,
            Down = 1 << 2,
            Left = 1 << 3,
            Right = 1 << 4
        }

        public AuraEffect auraEffect;
        public FilterCondition targets;
        public FilterConditionValues advancedFilters;

        [InfoBox("This will use 'advancedFilters' to identify the minion next to, front or back of this minion. If the advanced filters match then this aura will be valid. Usually used for SELF powering. E.g. Ironbark Soldier")]
        public bool checkRelativeBoardSpot;
        [ShowIf("checkRelativeBoardSpot")]
        public RelativeBoardSpace relativeBoardSpot;

        public bool turnRestricted;
        [ShowIf("turnRestricted")]
        public FilterCondition turnAllowed;

        [ShowIf("@this.auraEffect == AuraEffect.DrawModifier")]
        public int modifyDrawAmount;
        public bool removeWhenMinionDies;
        public bool removeWhenTurnEnds;

        [ShowIf("@this.auraEffect == AuraEffect.Transform_Summoned_Minions")]
        [ValueDropdown("GetAllCards")]
        public string minionToTransform;
        [ShowIf("@this.auraEffect == AuraEffect.Transform_Summoned_Minions")]
        [ValueDropdown("GetAllCards")]
        public string minionToTransformInto;

        [ShowIf("@this.auraEffect == AuraEffect.StatModifier")]
        public FilterConditionValues.Stat stat;
        [ShowIf("@this.auraEffect == AuraEffect.SpellDamage")]
        public Class spellSchool;
        [ShowIf("@this.auraEffect == AuraEffect.StatModifier || this.auraEffect == AuraEffect.SpellDamage")]
        public int modifyStatAmount;
        [ShowIf("@this.auraEffect == AuraEffect.SpellDamage")]
        public bool showTotalSpellDamageFromThisMinion;



#if UNITY_EDITOR
        public static ValueDropdownList<string> cards;

        private static IEnumerable GetAllCards()
        {
            if (cards != null && cards.Count > 0)
            {
                return cards;
            }

            if (!Db.Initialized)
            {
                Db.ForceInit();
            }

            cards = new ValueDropdownList<string>();

            for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
            {
                var cardData = Db.CardDatabase.Cards[i];
                cards.Add($"{cardData.faction}/{cardData.name}", cardData.id);
            }


            return cards;
        }
#endif
    }

    /// <summary>
    /// Card sets determine which chests cards come from
    /// </summary>
    public enum CardSet
    {
        /// <summary>
        /// When set to deprecated, the card wont show up in the game.
        /// </summary>
        Deprecated,

        /// <summary>
        /// A token is not part of any set, you cannot acquire this card and is only summoned through effects.
        /// </summary>
        Token,

        /// <summary>
        /// The classic set is the main chest in the franchise, players start out with cards from the classic set
        /// </summary>
        Core,

        /// <summary>
        /// A set the introduces pirates to other factions besides War - Redcroft Book 1
        /// </summary>
        PiratesAhoy,

        /// <summary>
        /// A set that introduces music themes into other decks besides Chaos - Violet Book 1
        /// </summary>
        StarscapeTwilight,

        /// <summary>
        /// A set that introduces theft mechanics in other decks besides Fortune - Quill Book 1
        /// </summary>
        PerilousPilfering,

        /// <summary>
        /// A set that introduces fighting and warriors to other decks besides Balance - Seto Book 1
        /// </summary>
        MonksMantra,

        /// <summary>
        /// Sprout wood stuff - Mereena Book 1
        /// </summary>
        SproutwoodGuardian,

        /// <summary>
        /// All sets
        /// </summary>
        All
    }

    /// <summary>
    /// The attack type of a minion determines which rows they can attack from
    /// </summary>
    public enum AttackType
    {
        /// <summary>
        /// Attacks from only the front row
        /// </summary>
        Melee,

        /// <summary>
        /// Can attack in all rows
        /// </summary>
        Arcane,

        /// <summary>
        /// Can only attack from the back row
        /// </summary>
        Ranged,

        /// <summary>
        /// Can't attack or gain power.
        /// </summary>
        Immobile
    }

    /// <summary>
    /// How rare a card is - this also determines how much mana dust you can get from it
    /// </summary>
    public enum Rarity
    {
        Common,
        Rare,
        Mythic,
        Legendary,
        All
    }

    /// <summary>
    /// Factions separate cards from heroes, you can only put a faction card with a faction hero.
    /// </summary>
    public enum Faction
    {
        /// <summary>
        /// Neutral cards can go in any deck
        /// </summary>
        All,
        Neutral,
        War,
        Nature,
        Fortune,
        Balance,
        Chaos,
        None,
    }

    /// <summary>
    /// Classes have no effect on the card except for the can get class dependant buffs
    /// </summary>
    public enum Class
    {
        All,
        None,
        Demon,
        Mage,
        Pirate,
        Dragon,
        Champion,
        Brawler,
        SproutElf,
        Golem,

        Pyro,
        Frost,
        Terra,
        Magitek,
        Chakra,
        Draconic,

        Cannon,
        Shrubasaur
    }

    public enum Race
    {
        None,
        Human,
        Riverclan,
        Sprout_Elf,
        Beastkin,
        Seafin,
        Wingblood,
        Littlefolk,
        Scaleskin,
        Demonborne,
        Bokin,
        Fairy,
        Mushfellow,
        Mawpaw,
        Undead,
        Shrubasaur
    }

    [Flags]
    public enum Archetype
    {
        PirateParty,
        BombsAway,
        RagingRevelry,

        DraconicDeath,
        Discard,
        BattleBandsBattleCry,

        FreezeMill,
        Focus,
        FocusSchool,

        DoubloonOtk,
        RansackRogue,
        TinkerTech,

        PuffPile,
        VileThicket,
        IronbarkMilitia
    }

    /// <summary>
    /// The card type determines what card type of card  this is, similar to how a class would work
    /// </summary>
    public enum CardType
    {
        /// <summary>
        /// minion cards are summoned onto the board
        /// </summary>
        Minion,

        /// <summary>
        /// Spell cards activate their effects and go away
        /// </summary>
        Spell,

        /// <summary>
        /// Trick cards are placed by the hero waiting for certain conditions to activate
        /// </summary>
        Trick,

        /// <summary>
        /// Relics activate when the game starts and give your hero some buff
        /// </summary>
        Relic,

        /// <summary>
        /// Heros are the main health source if your hero loses all of it's health the game is over
        /// </summary>
        Hero,

        /// <summary>
        /// Powers are similar to spells, you can use a power on a certain turn
        /// </summary>
        Power,

        /// <summary>
        /// Resource cards that are used outside of play
        /// </summary>
        Resource,

        /// <summary>
        /// used for abilities to we can declare there's none
        /// </summary>
        None,

        /// <summary>
        /// A character in game, usually an npc or a special character used for other things.
        /// </summary>
        Character,

        /// <summary>
        /// An elder relic is special type of relic, only 1 elder relic can be added to each deck.
        /// </summary>
        Elder_Relic,
    }

// used for sorting in the collection manager.
    public enum TypeFilter
    {
        All,
        Minion,
        Melee,
        Arcane,
        Ranged,
        Spell,
        Trick,
        Power,
    }

    public enum KeywordFilter
    {
        All,
        None,
        Turn_End,
        Turn_Start,
        // Ally,
        // Enemy,
        Battlecry,
        Deathrattle,
        Damaged,
        Barrier,
        Silence,
        Dual_Strike,
        Flying,
        Pierce,
        Redeem,
        //Slow,
        Tough,
        Lifesteal,
        Freeze,
        Focus,
        Plunder,
        Thorns,
        Overgrow,
        Flux,
        Rush,
        Swift_Strike
    }

    public enum RelicFilter
    {
        All,
        ElderRelics,
        Relics
    }

    public enum CardBackFilter
    {
        All,
        Unlocked,
        Locked
    }

    /// <summary>
    /// Team is only used in Matches, it determines which card belongs to which player
    /// </summary>
    public enum Team
    {
        Client,
        Opponent,
    }

    /// <summary>
    /// Only used in matches, just tells us where the card is
    /// </summary>
    public enum CardLocation
    {
        /// <summary>
        /// will be null for opponent cards that you can't see
        /// </summary>
        Null = -1,

        /// <summary>
        /// The card is inside the deck
        /// </summary>
        Deck,

        /// <summary>
        /// The card is in your hand, can only be called when drawing cards
        /// </summary>
        Hand,

        /// <summary>
        /// The card is on the board.
        /// </summary>
        Board,

        /// <summary>
        /// If it's been destroyed or used.
        /// </summary>
        Graveyard,
    }


    public static class AbilityUtilities
    {
        public static List<AbilityKeyword> triggeredAbilities = new List<AbilityKeyword>
        {
            AbilityKeyword.HEAL, AbilityKeyword.DAMAGE, AbilityKeyword.MODIFY_HEALTH,
            AbilityKeyword.MODIFY_POWER, AbilityKeyword.GIVE_ABILITY, AbilityKeyword.SUMMON,
            AbilityKeyword.GAIN_ARMOR,
        };

        public static List<AbilityKeyword> keywordsWithValues = new List<AbilityKeyword>
        {
            AbilityKeyword.HEAL, AbilityKeyword.DAMAGE, AbilityKeyword.MODIFY_HEALTH,
            AbilityKeyword.MODIFY_POWER, AbilityKeyword.GAIN_ARMOR,
            AbilityKeyword.DUAL_STRIKE, AbilityKeyword.ARMOR_BODY
        };

        public static List<AbilityKeyword> cardSelectorAbilities = new List<AbilityKeyword>
        {
            AbilityKeyword.SUMMON,
        };

        public static Dictionary<AbilityKeyword, TriggerType> fixedTriggerTypes =
            new Dictionary<AbilityKeyword, TriggerType>
            {
                {AbilityKeyword.MODIFY_DAMAGE, TriggerType.DAMAGED}
            };

        public static List<AbilityKeyword> keywordsThatUseEquation = new List<AbilityKeyword>
        {
            AbilityKeyword.MODIFY_DAMAGE
        };

        public static List<AbilityKeyword> keywordsWithTriggerTargets = new List<AbilityKeyword>
        {
            AbilityKeyword.HEAL, AbilityKeyword.DAMAGE, AbilityKeyword.MODIFY_HEALTH,
            AbilityKeyword.MODIFY_POWER, AbilityKeyword.GIVE_ABILITY, AbilityKeyword.SUMMON,
            AbilityKeyword.MODIFY_DAMAGE, AbilityKeyword.GAIN_ARMOR, AbilityKeyword.DESTROY,
        };


        public static bool IsTriggerType(AbilityKeyword keyword)
        {
            return triggeredAbilities.Contains(keyword);
        }

        public static string GetTriggerTitle(AbilityKeyword keyword)
        {
            return string.Format("{0} TRIGGERED BY?", keyword);
        }

        public static bool KeywordHasValue(AbilityKeyword keyword)
        {
            return keywordsWithValues.Contains(keyword);
        }

        public static string GetValueTitle(AbilityKeyword keyword)
        {
            return keyword.ToString();
        }

        public static bool UsesCardSelector(AbilityKeyword keyword)
        {
            return cardSelectorAbilities.Contains(keyword);
        }

        public static bool HasFixedTriggerType(AbilityKeyword keyword)
        {
            return fixedTriggerTypes.ContainsKey(keyword);
        }

        public static TriggerType GetFixedTriggerType(AbilityKeyword keyword)
        {
            return fixedTriggerTypes[keyword];
        }

        public static bool UsesEquation(AbilityKeyword keyword)
        {
            return keywordsThatUseEquation.Contains(keyword);
        }

        public static bool HasTriggerTarget(AbilityKeyword keyword)
        {
            return keywordsWithTriggerTargets.Contains(keyword);
        }

        public static bool HasEffectTarget(AbilityKeyword keyword)
        {
            return keywordsWithTriggerTargets.Contains(keyword);
        }

        public static string GetTriggerTargetTitle(AbilityKeyword keyword, TriggerType trigger)
        {
            return "TRIGGER TARGET CONDITION";
        }

        public static string GetEffectTargetTitle(AbilityKeyword keyword, TriggerType trigger)
        {
            return "EFFECT TARGET CONDITION";
        }
    }

    public enum EffectParentBehaviour
    {
        TargetCard,
        TargetCardObject,
        GameBoard,
        CastingHero,
    }

    /// <summary>
    /// The ability keyword indicates what ability this card has, using the Trigger Type and Trigger Target
    /// it will modify the ability targets and when it's triggered.
    /// </summary>
    public enum AbilityKeyword
    {
        /// <summary>
        /// No ability
        /// </summary>
        NONE,

        /// <summary>
        /// Heals a target(s) or self
        /// </summary>
        HEAL,

        /// <summary>
        /// Creates a shield around the minion that allows them to not take damage on first strike
        /// </summary>
        BARRIER,

        /// <summary>
        /// This minion attacks multiple times in a row dependant on the multi-attack amount.
        /// </summary>
        DUAL_STRIKE,

        /// <summary>
        /// A multi-attack that is conditional on game stuff
        /// </summary>
        MULTI_ATTACK_CONDITIONAL,

        /// <summary>
        /// Upon successful hit, deals damage to the row behind the damaged minion
        /// </summary>
        PIERCE,

        /// <summary>
        /// Removes all abilities from the stated minion or minions.
        /// </summary>
        SILENCE,

        /// <summary>
        /// Damages minions that do successful melee damage by the stated amount.
        /// </summary>
        THORNS,

        /// <summary>
        /// Upon successful hit, deals damage to all minions around the target minion. (including ally minions)
        /// </summary>
        SPLASH,

        /// <summary>
        /// Upon successful hit, deals stated damage and also heals that much.
        /// </summary>
        LIFESTEAL,

        /// <summary>
        /// Reduces damage dealt to the minion by the indicated block value.
        /// </summary>
        ARMOR_BODY,

        /// <summary>
        /// Deals indicated damage amount.
        /// </summary>
        DAMAGE,

        /// <summary>
        /// Increases or decreases health by amount to indicated minion(s).
        /// </summary>
        MODIFY_HEALTH,

        /// <summary>
        /// Increases or decreases power by amount to indicated minion(s).
        /// </summary>
        MODIFY_POWER,

        /// <summary>
        /// Freezes a minion until the next turn start phase.
        /// A frozen minion cannot attack but can still defend.
        /// Abilities are activated as normal.
        /// </summary>
        FREEZE,

        /// <summary>
        ///  Melee units cannot hit flying minions.
        /// </summary>
        FLYING,

        /// <summary>
        /// Every turn poisoned minions take 1 damage.
        /// </summary>
        POISON,

        /// <summary>
        /// Give a character(s) the indicated ability
        /// </summary>
        GIVE_ABILITY,

        /// <summary>
        /// Summon a minion(s)
        /// </summary>
        SUMMON,

        /// <summary>
        /// Summons copies of the target minion(s)
        /// </summary>
        SUMMON_COPIES,

        /// <summary>
        /// Modifies damage received
        /// </summary>
        MODIFY_DAMAGE,

        /// <summary>
        /// Gain indicated amount of armor
        /// </summary>
        GAIN_ARMOR,

        /// <summary>
        /// Destroy a character(s)
        /// </summary>
        DESTROY,

        /// <summary>
        /// modify the cost of a card (should be in the hand or deck)
        /// </summary>
        MODIFY_COST,

        /// <summary>
        /// draw a card
        /// </summary>
        DRAW,

        /// <summary>
        /// draw a card from the bottom of your deck
        /// </summary>
        DRAW_FROM_BOTTOM,

        /// <summary>
        /// Add a card to your hand
        /// </summary>
        ADD_CARD_TO_HAND,

        /// <summary>
        /// modify health and power at the same time.
        /// </summary>
        MODIFY_HEALTH_AND_POWER,

        /// <summary>
        /// When a card is tough it takes 1 less damage
        /// </summary>
        TOUGH,

        /// <summary>
        /// Counter minions will have a 50% chance to deal 2x damage back at a minion.
        /// </summary>
        COUNTER,

        /// <summary>
        /// Elusive minions can't be attacked.
        /// </summary>
        ELUSIVE,

        /// <summary>
        /// Taunt minions can move another card to in front of them.
        /// </summary>
        TAUNT,

        /// <summary>
        /// Modifies the mana crystal amount a player has.
        /// </summary>
        MODIFY_MANA_CRYSTALS,

        /// <summary>
        /// Gets a card from your deck by name, class or any other modifier
        /// </summary>
        GET_CARD_FROM_DECK,

        /// <summary>
        /// Adds a card to your deck
        /// </summary>
        ADD_CARD_TO_DECK,

        /// <summary>
        /// This card attacks right after being summoned (summoned != played) played minions need to be played from
        /// hand and would be a Battlecry!
        /// </summary>
        ATTACK_IMMEDIATELY_WHEN_SUMMONED,

        /// <summary>
        /// Removes cards from hand
        /// </summary>
        DISCARD,

        /// <summary>
        /// Able to attack the turn they are summoned, during the Battle Phase.
        /// </summary>
        RUSH,

        /// <summary>
        /// Redeem a card
        /// </summary>
        REDEEM,

        /// <summary>
        /// Create a predefined aura that effects the game.
        /// </summary>
        AURA,

        /// <summary>
        /// Transform targets into whatever you want!
        /// </summary>
        TRANSFORM_CARDS,

        /// <summary>
        /// Triggers deathrattles based on targets
        /// </summary>
        TRIGGER_ABILITY,

        /// <summary>
        /// This minion needs to wait 1 turn before attacking
        /// </summary>
        BIDE,

        /// <summary>
        /// Modifies your starting mana
        /// </summary>
        MODIFY_STARTING_MANA,

        /// <summary>
        /// modifies your starting draw
        /// </summary>
        MODIFY_STARTING_DRAW,

        /// <summary>
        /// modifies your starting health
        /// </summary>
        MODIFY_STARTING_HEALTH,

        /// <summary>
        /// Only used in the Leviafin fight
        /// </summary>
        RELEASE_BRASS,

        /// <summary>
        /// Spawns a copy of the minion to the right of the original
        /// </summary>
        OVERGROW,

        /// <summary>
        /// Can't be discarded
        /// </summary>
        DISCARD_PROOF,

        /// <summary>
        /// Gains control of an enemy minion
        /// </summary>
        GAIN_CONTROL,

        /// <summary>
        /// Can't attack (immobile attack type now)
        /// </summary>
        CANT_ATTACK,

        /// <summary>
        /// Forces a target to attack0
        /// </summary>
        FORCE_TARGETS_TO_ATTACK,

        /// <summary>
        ///
        /// </summary>
        FOCUS,
        GUARD,
        BLITZ_BURST_INSTANT_FILL,
        COST_REDUCED_BY_CONDITION,
        FLUX,
        GAIN_DEATHRATTLES_THAT_TRIGGER_WHILE_IN_HAND,
        PLAY_FLUX,
        CAST_CARD,
        TRIGGER_ALL_DEATHRATTLES,
        NEGATE_LAST_DAMAGE,
        SWIFT_STRIKE,
        MOVE_MINION_TO_ZONE,
        REFILL_MANA,
        BOOST_SPELL_DAMAGE,
        FLEETING,
        RELENTLESS,
    }


    /// <summary>
    /// When is the ability triggered, abilities only need a trigger type if it's triggered
    /// normally through the flow of the game, sometimes abilities need to know which targets
    /// this needs to happen to like 'DAMAGED' type
    /// However, things like Battlecry don't need trigger conditions just because the condition is met if
    /// they are summoned by the player.
    /// </summary>
    [System.Flags]
    public enum TriggerType
    {
        /// <summary>
        /// No trigger type, this ability is invalid if it has this type
        /// </summary>
        NONE = 0,

        /// <summary>
        /// Triggers when the card is played on the field
        /// </summary>
        BATTLECRY = 1 << 1,

        /// <summary>
        /// Triggers when 'Trigger Target' is damaged
        /// </summary>
        DAMAGED = 1 << 2,

        /// <summary>
        /// Triggers when the character is destroyed.
        /// </summary>
        DEATHRATTLE = 1 << 3,

        /// <summary>
        /// Triggers when the turn starts
        /// </summary>
        TURN_START = 1 << 4,

        /// <summary>
        /// Triggers when the turn ends
        /// </summary>
        TURN_END = 1 << 5,

        /// <summary>
        /// Triggers only when a minion is summoned from the players hand (played for its mana cost)
        /// </summary>
        MINION_PLAYED = 1 << 6,

        /// <summary>
        /// Triggers when 'Trigger Target' summons a minion
        /// </summary>
        MINION_SUMMONED = 1 << 7,

        /// <summary>
        /// A linked ability just plays out normally with the first ability.
        /// Example: Battlecry: Deal 1 damage to a minion and give it +3 power.
        /// The "Linked" ability is just give it +3 power.
        /// So we should in the first step deal 1 damage to that minion and in the second
        /// give it +3 power.
        /// </summary>
        LINKED_ABILITY = 1 << 8,

        /// <summary>
        /// Triggers when a player discards a card.
        /// </summary>
        DISCARD = 1 << 9,

        /// <summary>
        /// Activates when attacked
        /// </summary>
        ATTACKED = 1 << 10,

        /// <summary>
        /// activates when someone plays a spell
        /// </summary>
        PLAYED_SPELL = 1 << 11,

        /// <summary>
        /// when a player draws a card
        /// </summary>
        DRAW_CARD = 1 << 12,

        /// <summary>
        /// Minion has been destroyed
        /// </summary>
        MINION_DESTROYED = 1 << 13,

        /// <summary>
        /// activates when any card is played
        /// </summary>
        PLAYED_CARD = 1 << 14,

        /// <summary>
        /// Trigger this effect when this minion destroys another minion by striking.
        /// </summary>
        PLUNDER = 1 << 15,

        /// <summary>
        /// NOT USED CAN REPLACE WITH ANOTHER MECHANIC!!!
        /// </summary>
        CHARACTER_STATUS_EFFECT_CHANGED = 1 << 16,

        /// <summary>
        /// NOT USED CAN REPLACE WITH ANOTHER MECHANIC!!!
        /// </summary>
        [Obsolete("Use PLAYED_SPELL Trigger, Trigger Evaluate Condition: SOURCE, Trigger Conditions: PLAYER, SPELL, Exhaust: true")]
        RIFF = 1 << 17,

        /// <summary>
        /// After focused this will trigger something.
        /// </summary>
        FOCUS = 1 << 18,

        /// <summary>
        /// activates when any player redeems a card,
        /// </summary>
        REDEEMED_CARD = 1 << 19,

        /// <summary>
        /// activates when this card deals damage to something - filter using trigger conditions
        /// </summary>
        DEALT_DAMAGE_THROUGH_ATTACKING = 1 << 20,

        /// <summary>
        /// cast this card when drawn and draw another card
        /// </summary>
        CAST_WHEN_DRAWN = 1 << 21,

        // /// <summary>
        // /// triggers when a card is discarded.
        // /// </summary>
        // DISCARD_CARD = 1 << 22,

        /// <summary>
        /// When a card is added to a certain zone
        /// </summary>
        CARD_CHANGED_LOCATIONS = 1 << 22,

        /// <summary>
        /// this happens BEFORE the played card executes commands, good for counters or transformations from playing a minion etc.
        /// </summary>
        BEFORE_PLAYED_CARD = 1 << 23,

        /// <summary>
        /// triggers when a trap is set by the player and card effects.
        /// </summary>
        TRAP_SET = 1 << 24,

        /// <summary>
        /// triggers when a trap is revealed
        /// </summary>
        TRAP_REVEALED = 1 << 25,

        GAME_START = 1 << 26,

        /// <summary>
        /// when a minion is transformed
        /// </summary>
        MINION_TRANSFORMED = 1 << 27
    }

    /// <summary>
    /// Using with Trigger Targets:
    /// After an ability is 'Triggered' through it's TriggerType parameter it will need to validate the conditions
    /// of what was triggered.
    /// Example: TriggerType 'Damaged', will sometimes need to verify if it was one of your minions or hero etc to activate.
    /// Example 2: Whenever a friendly minion takes damage, gain +2 armor.
    /// So the trigger type is "damaged" and the trigger conditions should be *PLAYER* + *MINION*
    ///
    /// Using with Ability Targets:
    /// Ability target works with the ability keyword, you should apply as many as fits the need of the ability.
    /// EXAMPLE: "Whenever a friendly minion takes damage, gain +2 armor"
    /// Don't worry about the first part if we're here we've already satisfied the trigger conditions.
    /// What we're looking at is "gain +2 armor", the Ability Keyword should be "Gain Armor" with
    /// the Ability Target being *PLAYER* + *HERO*
    /// </summary>
    [System.Flags]
    public enum FilterCondition
    {
        /// <summary>
        /// Use any when the selection does not matter.
        /// </summary>
        NONE = 0,

        // filtering specifics, all is used to get a big list, while self is just this minion/hero etc and last is
        // the last played minion, usually

        /// <summary>
        /// When using ALL you're saying that all of these things should be affected not just one of them.
        /// </summary>
        ALL = 1 << 1, //*

        /// <summary>
        /// Force conditions to be based on myself
        /// Example: If this minion takes damage, do something
        /// The trigger would be damage and the condition would be *Self* + *Minion*
        /// </summary>
        SELF = 1 << 2,

        /// <summary>
        /// Force the target to be the *Last* (probably minion?)
        /// </summary>
        LAST = 1 << 3, //*

        /// <summary>
        /// This is a targeted effect where the player selects it
        /// </summary>
        PLAYER_SELECTED = 1 << 4,

        /// <summary>
        /// Force conditions to be based on if the player variables
        /// </summary>
        PLAYER = 1 << 5,

        /// <summary>
        /// Force conditions to be based on opponent variables
        /// </summary>
        OPPONENT = 1 << 6,

        /// <summary>
        /// The target will be a hero, there should be another identifier with this.
        /// </summary>
        HERO = 1 << 7,

        /// <summary>
        /// The condition is now it has to be a minion, activate things like *Any*, *Player* or *Opponent* to change logic.
        /// Example: Player Minion
        /// Example 2: (Whenever a minion is summoned on your side of the board) use *ANY*,*PLAYERS_BOARD_SIDE*,*MINION*
        /// </summary>
        MINION = 1 << 8,

        /// <summary>
        /// effects only spells
        /// </summary>
        SPELL = 1 << 9,

        /// <summary>
        /// effects only trick cards
        /// </summary>
        TRICK = 1 << 10,

        /// <summary>
        /// The condition only affects things on the board
        /// </summary>
        BOARD = 1 << 11,

        /// <summary>
        /// The condtiion only affects things in the deck
        /// </summary>
        DECK = 1 << 12,

        /// <summary>
        /// The condition only affects things in the hand
        /// </summary>
        HAND = 1 << 13,

        /// <summary>
        /// Can only be triggered if the target or self is damaged?
        /// </summary>
        IS_DAMAGED = 1 << 14,

        /// <summary>
        /// Target is an entire row
        /// </summary>
        ROW = 1 << 15,

        /// <summary>
        /// Target is an entire column
        /// </summary>
        COLUMN = 1 << 16,

        /// <summary>
        /// Target all but one final target
        /// </summary>
        ALL_BUT_ONE = 1 << 17,

        /// <summary>
        /// Adjacent spaces are effected, usually used with targeted effects
        /// </summary>
        ADJACENT_SPACES = 1 << 18,

        /// <summary>
        /// Disable any linked abilities - usually used when you add a
        /// given effect to a card so it doesn't trigger its over abilities
        /// </summary>
        DISABLE_LINKED = 1 << 19,

        /// <summary>
        /// Exclude this character from being able to trigger the condition.
        /// </summary>
        EXCLUDE_SELF = 1 << 20,

        /// <summary>
        /// Used when you want to do something on the same team
        /// </summary>
        SAME_TEAM = 1 << 21,

        /// <summary>
        /// This player has played a card this turn!
        /// </summary>
        PLAYED_CARD_THIS_TURN = 1 << 22,

        /// <summary>
        /// Cards that were targets in the previous command will be the target for this command as well.
        /// </summary>
        PREVIOUS_COMMAND_TARGETS = 1 << 23,

        /// <summary>
        /// Is in the graveyard
        /// </summary>
        GRAVEYARD = 1 << 24,

        /// <summary>
        /// Once all targets are confirmed, the game will select one at random to send back.
        /// </summary>
        MAX_TARGET_COUNT = 1 << 25,

        /// <summary>
        /// Uses the "source" card input into the "Resolve Triggered Cards"
        /// </summary>
        SOURCE_CARD_FROM_RESOLVE_TRIGGERED_CARDS = 1 << 26,

        /// <summary>
        /// Uses the "target" card input into the "Resolve Triggered Cards"
        /// </summary>
        TARGET_CARD_FROM_RESOLVE_TRIGGERED_CARDS = 1 << 27,

        /// <summary>
        /// Set this if you don't want it to target something that's about to die. some things this is actually a benefit, like when
        /// something damages something and buffs something in the same step!
        /// </summary>
        NOT_PENDING_DEAD = 1 << 28,

        DIAGONAL_SPACES = 1 << 29,

        ADVANCED_FILTERS = 1 << 30,
    }

    [Flags]
    public enum TriggerEvaluateCondition
    {
        TARGET = 1 << 0,
        SOURCE = 1 << 1,
        TRIGGERED = 1 << 2
    }

    public enum TriggerTarget
    {
        NONE,
        SELF,
        ANY,

        PLAYER_HERO,
        OPPONENT_HERO,
        RANDOM_HERO,
        ANY_HERO,

        RANDOM_PLAYER_MINON,
        RANDOM_OPPONENT_MINION,
        RANDOM_MINON,
        LAST_MINION,
        ANY_MINION,

        ALL,

        PLAYER_MINION,
        OPPONENT_MINION,

        ANY_DAMAGED_MINION
    }

    public enum ModifyEquation
    {
        NONE,
        EQUAL_TO,
        BY_AMOUNT,
    }

    public enum Condition
    {
        NONE,
        FACTION_ON_FIELD,
        CLASS_ON_FIELD,
        DAMAGED,
        TARGET_FROZEN,
        DAMAGED_MINIONS,
        DAMAGED_MINIONS_OWNED_BY_PLAYER,
        DAMAGED_MINIONS_OWNED_BY_OPPONENT,
        DAMAGED_THIS_GAME,
        [Obsolete("Use DAMAGED_MINIONS with a filter of PLAYER.")]
        DAMAGED_FRIENDLY,
        DAMAGED_OPPONENTS,
        CARDS_PLAYED_THIS_TURN,
        HIGHEST_ATTACK_MINION,
        FROZEN_THIS_GAME,
        NUMBER_OF_SPECIFIC_CARDS_IN_DECK,
        DISCARDED_THIS_GAME,
        TARGETS_HEALTH,
        TARGETS_POWER,
        PREVIOUS_COMMAND_TARGETS,
        SPELL_DAMAGE,
        ARMOR_AMOUNT,
        COST_OF_LAST_SPELL_USED,
        COST_OF_LAST_DRAWN_CARD,
        SPELLS_PLAYED_THIS_TURN,
        NUMBER_OF_SPECIFIC_CARDS_ON_FIELD,
        COST_OF_LAST_COMMAND_TARGET,
        SPECIFIC_MINION_DIED_THIS_GAME,
        TRAPS_SET,
        THIS_MINIONS_POWER,
        NUMBER_OF_SPECIFIC_CARDS_PLAYED,
        NUMBER_OF_CARDS_PLAYED_FROM_DIFFERENT_FACTIONS,
        NUMBER_OF_MINIONS_BY_FILTER_DIED_THIS_GAME,
        SOURCE_CARD_FROM_RESOLVE_TRIGGER,
        TARGET_CARD_FROM_RESOLVE_TRIGGER
    }


    /// <summary>
    /// This is only valid for the Summon Ability, it gives placement info
    /// </summary>
    [Flags]
    public enum SummonPlacement
    {
        NONE = 1 << 0,

        /// <summary>
        /// Summon only on the players side of the field
        /// </summary>
        PLAYER = 1 << 1,

        /// <summary>
        /// Summon only on the opponents side of the field
        /// </summary>
        OPPONENT = 1 << 2,

        /// <summary>
        /// Summon something randomly
        /// </summary>
        RANDOM = 1 << 3,

        /// <summary>
        /// Summon an entire row of this minion
        /// </summary>
        ROW = 1 << 4,

        /// <summary>
        /// Summon an entire column of this minion
        /// </summary>
        COLUMN = 1 << 5,

        /// <summary>
        /// Summon in the front of the effect minion
        /// </summary>
        FRONT = 1 << 6,

        /// <summary>
        /// Summon in the back of the effect minion
        /// </summary>
        BACK = 1 << 7,

        /// <summary>
        /// Summon on the right side of the effect minion
        /// </summary>
        RIGHT_SIDE = 1 << 8,

        /// <summary>
        /// Summon on the left side of the effect minion
        /// </summary>
        LEFT_SIDE = 1 << 9,

        /// <summary>
        /// Summon in the same spot as the source card (usually from a deathrattle)
        /// </summary>
        IN_PLACE = 1 << 10,
        /// <summary>
        /// Summon on an exact spot
        /// </summary>
        EXACT_POSITION = 1 << 11,

        /// <summary>
        /// Used for Front and Back placement, only works if you summon 1 thing.
        /// When combined with FRONT or BACK this will only summon in FRONT of the minion or in BACK of the minion doing the summoning
        /// if the board spot is filled the minion will not be summoned.
        /// </summary>
        STRICT = 1 << 12
    }

    [System.Flags]
    public enum StatusEffect
    {
        Frozen = 1 << 1,
        Barrier = 1 << 2,
        Tough = 1 << 3
    }

    public enum TodoStatus
    {
        NotStarted,
        Testing,
        Finished
    }
}