using System.Collections.Generic;
using CrossBlitz.InputAPI;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.Utils;
using TakoBoyStudios;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Card
{
    public class ModifierTooltip : MonoBehaviour
    {
        public ModifierTooltipItem tooltipItem;
        public CanvasGroup canvasGroup;
        public PageBehaviour pages;
        public RectTransform containerRect;
        public GameObject arrow;

        private string m_gameCardUid;
        private float m_startingY;
        private float m_currY;
        private bool m_animateIn;
        private float m_animateInDelay;
        private RectTransform m_rect;
        private RectTransform m_attachmentRect;
        private int m_side;
        private float m_originalX;
        private float m_punchCurrX;
        private bool m_punchRight;
        private bool m_punchLeft;
        private const float PunchOffset = 10;


        private const float StartingYNoAttachments = -48;

        private void Awake()
        {
            m_rect = this.RectTransform();
            m_originalX = m_rect.anchoredPosition.x;
        }

        private void Update()
        {
            if (m_animateIn)
            {
                if (m_animateInDelay > 0)
                {
                    m_animateInDelay -= Time.deltaTime;

                    if (m_animateInDelay <= 0)
                    {
                        Resize();
                    }
                }

                if (m_animateInDelay <= 0)
                {
                    const float distance = 1f;
                    const float duration = 0.24f;
                    const float speed = distance / duration;

                    canvasGroup.alpha += speed *  Time.deltaTime;

                    m_currY += 1 / .4f * Time.deltaTime;

                    var pointY = EasingFunction.EaseOutBack(m_startingY + 10, m_startingY, m_currY);
                    m_rect.anchoredPosition = new Vector2(m_originalX * m_side, pointY);

                    if (m_currY >= 1)
                    {
                        m_animateIn = false;
                    }
                }
            }

            if (canvasGroup.alpha >= 1)
            {
                if (Controls.GetAction(Controls.Actions.TurnPageLeft))
                {
                    OnTurnPageLeft();
                }

                if (Controls.GetAction(Controls.Actions.TurnPageRight))
                {
                    OnTurnPageRight();
                }
            }

            if (m_punchRight)
            {
                m_punchCurrX += 1 / .24f * Time.deltaTime;

                var pointX = EasingFunction.EaseOutBack(m_originalX * m_side + PunchOffset, m_originalX * m_side, m_punchCurrX);
                m_rect.anchoredPosition = new Vector2(pointX, m_rect.anchoredPosition.y);

                if (m_punchCurrX >= 1) m_punchRight = false;
            }

            if (m_punchLeft)
            {
                m_punchCurrX += 1 / .24f * Time.deltaTime;

                var pointX = EasingFunction.EaseOutBack(m_originalX * m_side - PunchOffset, m_originalX * m_side, m_punchCurrX);
                m_rect.anchoredPosition = new Vector2(pointX, m_rect.anchoredPosition.y);

                if (m_punchCurrX >= 1) m_punchLeft = false;
            }
        }

        public bool IsReady()
        {
            if (!GameServer.IsRunning()) return false;

            var histories = GameServer.MainState.GetCardHistories(m_gameCardUid);
            var displayingHistories = 0;

            for (var i = 0; i < histories.Count; i++)
            {
                if (!ModifierTooltipItem.ModifierSupported(histories[i].Type))
                {
                    continue;
                }

                displayingHistories++;
            }

            return !string.IsNullOrEmpty(m_gameCardUid) && displayingHistories > 0;
        }

        public void SetGameCardUid(string gameCardUid)
        {
            m_gameCardUid = gameCardUid;
        }

        private void RefreshPages()
        {
            var cardHistories = GameServer.MainState.GetCardHistories(m_gameCardUid);
            var modifierData = new List<ModifierTooltipData>();
            var duplicate = false;

            for (var i = 0; i < cardHistories.Count; i++)
            {
                var history = cardHistories[i];

                if (!ModifierTooltipItem.ModifierSupported(history.Type))
                {
                    continue;
                }

                duplicate = false;

                for (var j = 0; j < modifierData.Count; j++)
                {
                    var data = modifierData[j];
                    var command = GameServer.MainState.GetHistory(data.stateHistoryGuid);

                    if (history.IsEquivalent(command))
                    {
                        data.duplicateCount++;
                        duplicate = true;
                        break;
                    }
                }

                if (duplicate) continue;

                var newData = new ModifierTooltipData();
                newData.duplicateCount = 1;
                newData.stateHistoryGuid = history.Guid;
                newData.sourceUid = m_gameCardUid;

                modifierData.Add(newData);
            }

            if (modifierData.Count > 0)
            {
                pages.InitializePages(tooltipItem, 1, modifierData.ConvertAll(m => (IPageItemData)m));
            }
        }

        private void Resize()
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(containerRect);

            var size = containerRect.rect.height;
            m_rect.sizeDelta = new Vector2(m_rect.sizeDelta.x, size + 57);

            if (m_attachmentRect)
            {
                LayoutRebuilder.ForceRebuildLayoutImmediate(m_attachmentRect);
                m_startingY = -m_attachmentRect.rect.height - 36;
            }

            m_rect.anchoredPosition = new Vector2(m_originalX * m_side, m_startingY + 10);
        }

        public void Open(int side, float delay, RectTransform attachmentRect, bool showArrow)
        {
            RefreshPages();

            m_animateIn = true;
            m_animateInDelay = delay <= 0 ? 0.24f : delay;
            m_startingY = StartingYNoAttachments;
            m_attachmentRect = attachmentRect;
            m_currY = 0;
            m_side = side;

            canvasGroup.alpha = 0;
            arrow.SetActive(showArrow);
        }

        public void Close()
        {
            m_animateIn = false;
            m_animateInDelay = 0.24f;
            m_startingY = StartingYNoAttachments;

            canvasGroup.alpha = 0;
        }

        private void OnTurnPageRight()
        {
            if (canvasGroup.alpha >= 1)
            {
                m_punchRight = true;
                m_punchLeft = false;
                m_punchCurrX = 0;
                m_rect.anchoredPosition = new Vector2(m_originalX * m_side + PunchOffset, m_rect.anchoredPosition.y);
                pages.OnNextButton();
            }
        }

        private void OnTurnPageLeft()
        {
            if (canvasGroup.alpha >= 1)
            {
                m_punchRight = false;
                m_punchLeft = true;
                m_punchCurrX = 0;
                m_rect.anchoredPosition = new Vector2(m_originalX * m_side - PunchOffset, m_rect.anchoredPosition.y);
                pages.OnPreviousButton();
            }
        }
    }
}