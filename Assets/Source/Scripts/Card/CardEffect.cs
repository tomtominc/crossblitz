using System.Collections.Generic;
using CrossBlitz.ClientAPI.Visuals;
using UnityEngine;
using UnityEngine.UI;

namespace Source.Scripts.Card
{
    public class CardEffect : MonoBehaviour
    {
        public GameEffectsManager.VFX type;
        public Canvas canvas;
        public List<SpriteAnimation> animations;
        public List<Image> images;

        public bool destroyAfterPlaying { get; set; }

        public void Update()
        {
            if (destroyAfterPlaying)
            {
                if (animations[0].IsDone)
                {
                    GameEffectsManager.Instance.RemoveEffect(this);
                }
            }
        }
    }
}
