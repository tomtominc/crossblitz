using System;

namespace CrossBlitz.Card.Data
{
    [Flags]
    public enum DropLocations
    {
        None = 1 << 0,
        Chests = 1 << 1,
        Story = 1 << 2,
        StartingCard = 1 << 3
    }
    [Serializable]
    public class DropTableData
    {
        public DropLocations dropLocations;
        public int dropPercentage;
    }
}