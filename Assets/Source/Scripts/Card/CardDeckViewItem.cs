using System;
using System.Collections.Generic;
using CrossBlitz.AssetManagement;
using CrossBlitz.CardCollection;
using CrossBlitz.Databases;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.U2D;
using UnityEngine.UI;

namespace CrossBlitz.Card
{
    public class CardDeckViewItem : MonoBehaviour,
        IPointerClickHandler
    {
        public RectTransform rect;
        public SpriteAnimation factionSlot;
        public Image portrait;
        public TextMeshProUGUI cardName;
        public RectTransform manaCostContainer;
        public TextMeshProUGUI manaCostLabel;
        public SpriteAnimation cardTypeIcon;
        public SpriteAnimation relicIcon;

        [BoxGroup("Disabled Overlay")] public GameObject disabledOverlay;
        [BoxGroup("Disabled Overlay")] public Material ghostMaterial;
        [BoxGroup("Disabled Overlay")] public Material uiDefaultMaterial;
        [BoxGroup("Disabled Overlay")] public Image manaContainer;
        [BoxGroup("Disabled Overlay")] public CanvasGroup blackOverlay;

        [BoxGroup("Multi Card")] public CanvasGroup multiCardCanvas;
        [BoxGroup("Multi Card")] public RectTransform multiCardRectContainer;
        [BoxGroup("Multi Card")] public SpriteAnimation multiCardFactionIconDeckEditView;
        [BoxGroup("Multi Card")] public SpriteAnimation multiCardFactionIconGameView;
        [BoxGroup("Multi Card")] public TMPSpriteFont multiCardLabel;
        [BoxGroup("Multi Card")] public SpriteAnimation multiCardLabelAnimator;
        [BoxGroup("Multi Card")] public TMP_SpriteAsset greenFontSpriteAsset;
        [BoxGroup("Multi Card")] public TMP_SpriteAsset redFontSpriteAsset;
        [BoxGroup("Multi Card")] public GameObject multiCardActionDrips;
        [BoxGroup("Multi Card")] public List<SpriteAnimation> doubleCardDripAnimators;
        [BoxGroup("Layout")] public LayoutElement layoutElement;
        [BoxGroup("Animate In")] public ShakeContainer shake;
        [BoxGroup("Animate In")] public CanvasGroup plopOverlay;
        [BoxGroup("Animate In")] public GameObject actionDrips;
        [BoxGroup("Animate In")] public List<SpriteAnimation> dripAnimators;

        [BoxGroup("Animate Parameters")] public bool rotateOnHover=true;
        [BoxGroup("Animate Parameters")] public float rotatePower = 20;
        [BoxGroup("Animate Parameters")] public int rotateVibrateTimes = 4;
        [BoxGroup("Animate Parameters")] public float rotateElasticity = 1;
        [BoxGroup("Animate Parameters")] public float rotateHoverDuration = 1f;

        private CardValue m_cardValue;
        private CardData m_cardData;
        private Tweener _rotateTween;
        private DeckCardList _deckCardList;

        public CardData CardData
        {
            get
            {
                if (m_cardData == null)
                {
                    m_cardData = Db.CardDatabase.GetCard(m_cardValue.id);
                }

                return m_cardData;
            }
        }

        public int CardCount => m_cardValue.count;

        public async void Load(CardValue cardValue, bool showOwnership = true, int deckRecipeCount = 0, bool forceDisabledOverlay = false)
        {
            m_cardValue = cardValue;
            m_cardData = null;

            cardName.text = CardData.name;
            //cardName.color = new Color(229, 237, 177, 1f);
            manaCostLabel.text = CardData.cost.ToString();
            factionSlot.Play($"{CardData.faction.ToString().ToLower()}-{CardData.type.ToString().ToLower()}");
            multiCardFactionIconDeckEditView.Play(CardData.faction.ToString().ToLower());
            multiCardFactionIconGameView.Play(CardData.faction.ToString().ToLower());
            multiCardLabel.text = CardCount.ToString();
            multiCardLabelAnimator.Play($"{CardData.rarity.ToString().ToLower()}-{CardCount}");
            plopOverlay.GetComponent<Image>().maskable = true;
            manaCostContainer.SetActive(true);
            blackOverlay.SetActive(false);
            relicIcon.SetActive(false);
            cardName.rectTransform.SetLeft(70);
            cardName.horizontalAlignment = HorizontalAlignmentOptions.Center;

            if (DeckEditMenu.Instance)
            {
                multiCardFactionIconDeckEditView.SetActive(true);
                multiCardFactionIconGameView.SetActive(false);
            }
            else
            {
                multiCardFactionIconDeckEditView.SetActive(false);
                multiCardFactionIconGameView.SetActive(true);
            }

            if (deckRecipeCount != 0)
            {
                multiCardLabel.text = deckRecipeCount.ToString();
                multiCardLabelAnimator.Play($"{CardData.rarity.ToString().ToLower()}-{deckRecipeCount}");
                multiCardFactionIconDeckEditView.SetActive(false);
                multiCardFactionIconGameView.SetActive(true);

                if (forceDisabledOverlay) ChangeToRed();
                else ChangeToGreen();
            }

            var dragCard = GetComponent<DragCard>();

            if (dragCard != null)
            {
                dragCard.PointerEnterEvent -= DragCardPointerEnterEvent;
                dragCard.PointerExitEvent -= DragCardPointerExitEvent;

                dragCard.PointerEnterEvent += DragCardPointerEnterEvent;
                dragCard.PointerExitEvent += DragCardPointerExitEvent;
            }

            var hoverCard = GetComponent<HoverCard>();

            if (hoverCard)
            {
                hoverCard.UsesHoverPunchEffect = false;

                if (hoverCard.ZoomedCard)
                {
                    hoverCard.ZoomedCard.SetSortingLayer("Collection");
                }
            }

            if (CardData.type == CardType.Minion)
            {
                cardTypeIcon.Play(CardData.attackType.ToString().ToLower());
            }
            else if (CardData.type == CardType.Spell || CardData.type == CardType.Trick)
            {
                cardTypeIcon.Play(CardData.type.ToString().ToLower());
            }
            else if (CardData.type == CardType.Power)
            {
                manaCostContainer.SetActive(false);
                cardTypeIcon.SetActive(false);
                //cardTypeIcon.Play($"{_cardData.type.ToString().ToLower()}-{_cardData.heroPowerRank}");
            }
            else if (CardData.type == CardType.Relic || CardData.type == CardType.Elder_Relic)
            {
                relicIcon.SetActive(true);
                manaCostContainer.SetActive(false);
                cardTypeIcon.SetActive(false);
                multiCardCanvas.SetActive(false);
                cardName.color = new Color(255, 255, 255, 1f);
                cardName.rectTransform.SetLeft(40);
                cardName.horizontalAlignment = HorizontalAlignmentOptions.Left;

                if (CardData.type == CardType.Relic)
                {
                    factionSlot.Play($"relic");
                }
                else
                {
                    factionSlot.Play($"elder-relic");
                }

                relicIcon.Play(CardData.portraitUrl);
            }
            else
            {
                cardTypeIcon.SetActive(false);
            }

            if (showOwnership)
            {
                if ((!App.Inventory.OwnsCard(CardData.id) && DeckEditMenu.Instance) || forceDisabledOverlay)
                {
                    portrait.material = uiDefaultMaterial;
                    portrait.color = new Color(1, 1, 1, 0.8f);
                    disabledOverlay.SetActive(true);
                }
                else
                {
                    portrait.material = uiDefaultMaterial;
                    portrait.color = new Color(1, 1, 1, 1);
                    disabledOverlay.SetActive(false);
                }
            }

            if (!string.IsNullOrEmpty(CardData.portraitUrl) && portrait && (CardData.type != CardType.Relic && CardData.type != CardType.Elder_Relic))
            {
                if (portrait.sprite != null && portrait.sprite.name != CardData.portraitUrl)
                {
                    var atlas = await AddressableReferenceLoader.Load<SpriteAtlas>("cards-atlas");
                    portrait.sprite = atlas.GetSprite(CardData.portraitUrl);
                }
                else if (portrait.sprite == null)
                {
                    var atlas = await AddressableReferenceLoader.Load<SpriteAtlas>("cards-atlas");
                    portrait.sprite = atlas.GetSprite(CardData.portraitUrl);
                }

                portrait.SetActive(portrait.sprite != null);
                portrait.rectTransform.anchoredPosition = CardData.deckOffset + new Vector2Int(-4, -4);
            }
            else
            {
                portrait.SetActive(false);
            }
        }
        public void SetDeckCardList(DeckCardList deckCardList)
        {
            _deckCardList = deckCardList;
        }

        public void OnOffsetChanged()
        {
            portrait.rectTransform.anchoredPosition = CardData.deckOffset + new Vector2Int(-4, -4);
        }

        private void DragCardPointerEnterEvent()
        {
            plopOverlay.SetActive(true);
            plopOverlay.alpha = 0;
            plopOverlay.DOFade(.5f, 0.2f);

            if (rotateOnHover)
            {
                if (_rotateTween == null || !_rotateTween.IsActive())
                {
                    rect.RectTransform().DOKill();
                    rect.RectTransform().localScale = Vector3.one;
                    rect.RectTransform().localEulerAngles = Vector3.zero;

                    _rotateTween = rect.RectTransform().DOPunchRotation(Vector3.right * rotatePower, rotateHoverDuration,
                        rotateVibrateTimes,
                        rotateElasticity);
                }
            }
        }

        private void DragCardPointerExitEvent()
        {
            plopOverlay.SetActive(true);
            plopOverlay.alpha = .5f;
            plopOverlay.DOFade(0, 0.2f);
        }

        public void AnimatePlop()
        {
            rect.DOKill(true);
            rect.DOPunchScale(new Vector3(-0.2f, 1, 0) * 0.2f, 0.2f);

            actionDrips.SetActive(true);
            dripAnimators.ForEach( drip => drip.Play("spawn-particle"));

            plopOverlay.SetActive(true);
            plopOverlay.alpha = 1;
            plopOverlay.DOFade(0, 0.5f)
                .OnComplete(() =>
                {
                    actionDrips.SetActive(false);
                    plopOverlay.SetActive(false);
                });
        }

        public void AnimatePlopDie()
        {
            rect.DOKill(true);
            rect.DOPunchScale(new Vector3(-0.2f, 1, 0) * 0.2f, 0.2f);
           // actionDrips.SetActive(true);
           // dripAnimators.ForEach(drip => drip.Play("spawn-particle"));
            plopOverlay.SetActive(true);
            plopOverlay.alpha = 0;
            plopOverlay.DOFade(1, 0.5f)
                .OnComplete(() =>
                {
                    actionDrips.SetActive(false);
                    plopOverlay.SetActive(false);
                });
        }


        public void AnimateRemove()
        {
            rect.SetActive(false);
            layoutElement.DOPreferredSize(new Vector2(170, 0), 0.5f)
                .SetEase(Ease.OutBounce)
                .OnComplete(() =>
                {

                    Destroy(gameObject);
                });
        }

        public void AnimateDoubleCard(CardValue cardValue, bool spawnEffect = true, int deckRecipeCount = 0)
        {
            m_cardValue = cardValue;

            rect.DOKill(true);
            rect.DOPunchScale(new Vector3(-0.2f, 1, 0) * 0.2f, 0.2f);

            if (spawnEffect)
            {
                actionDrips.SetActive(true);
                dripAnimators.ForEach(drip => drip.Play("spawn-particle"));
            }

            multiCardCanvas.SetActive(true);
            multiCardCanvas.alpha = 1;

            multiCardRectContainer.DOKill(true);
            multiCardRectContainer.DOPunchScale(-Vector3.up, 0.25f);
            multiCardRectContainer.DOJumpAnchorPos(multiCardRectContainer.anchoredPosition, 8, 1, 0.25f)
                .SetEase(Ease.Linear);

            multiCardActionDrips.SetActive(true);
            doubleCardDripAnimators.ForEach( drip => drip.Play("spawn-particle"));

            multiCardLabel.text = CardCount.ToString();
            multiCardLabelAnimator.Play($"{CardData.rarity.ToString().ToLower()}-{CardCount}");


            if (deckRecipeCount != 0)
            {
                multiCardLabel.text = deckRecipeCount.ToString();
                multiCardLabelAnimator.Play($"{CardData.rarity.ToString().ToLower()}-{deckRecipeCount}");
                multiCardFactionIconDeckEditView.SetActive(false);
                multiCardFactionIconGameView.SetActive(true);
            }

            if (spawnEffect)
            {
                plopOverlay.SetActive(true);
                plopOverlay.alpha = 1;
                plopOverlay.DOFade(0, 0.5f)
                    .OnComplete(() => {
                        actionDrips.SetActive(false);
                        multiCardActionDrips.SetActive(false);
                        plopOverlay.SetActive(false);
                    });
            }
        }

        private void ChangeToGreen()
        {
            multiCardLabel.resourceName = "multi-card-icon-numbers-green_8x8";
            multiCardLabel.ForceRefresh(greenFontSpriteAsset);
        }

        private void ChangeToRed()
        {
            multiCardLabel.resourceName = "multi-card-icon-numbers-red_8x8";
            multiCardLabel.ForceRefresh(redFontSpriteAsset);
        }

        public void AnimateRemoveDoubleCard()
        {
            multiCardCanvas.DOFade(0, 0.25f)
                .OnComplete(() => multiCardCanvas.SetActive(false));
        }

        public void OnPointerClick(PointerEventData eventData)
        {
          //  if(eventData.button == PointerEventData.InputButton.Right) _deckCardList.RemoveCard(this.CardData);
        }

        public void OnDestroy()
        {
            DOTween.Kill(plopOverlay);
            DOTween.Kill(multiCardCanvas);
            //DOTween.Kill(doubleCardOverlay);
            DOTween.Kill(multiCardRectContainer);
            DOTween.Kill(layoutElement);
            DOTween.Kill(rect);
        }
    }
}