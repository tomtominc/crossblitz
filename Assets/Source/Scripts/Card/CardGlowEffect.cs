using System.Collections.Generic;
using CrossBlitz.ServerAPI.GameLogic;
using UnityEngine;

namespace Source.Scripts.Card
{
    public class CardGlowEffect : MonoBehaviour
    {
        public SpriteAnimation outlineAnimator;
        public SpriteAnimation actionDripPrefab;
        public SpriteAnimation topBubblePrefab;
        public SpriteAnimation topDripPrefab;
        public SpriteAnimation bottomDripPrefab;
        public RectTransform topDrips;
        public RectTransform bottomDrips;
        public RectTransform actionDrips;

        private List<SpriteAnimation> _actionDrips;
        private List<SpriteAnimation> _bubblesTop;
        private List<SpriteAnimation> _dripsTop;
        private List<SpriteAnimation> _dripsBottom;

        private Queue<SpriteAnimation> _actionDripQueue;
        private Queue<SpriteAnimation> _bubblesTopQueue;
        private Queue<SpriteAnimation> _dripsTopQueue;
        private Queue<SpriteAnimation> _dripsBottomQueue;

        private float _topBubbleSpawnDelayMin = 0.32f;
        private float _topBubbleSpawnDelayMax = 1f;
        private float _topDripSpawnDelayMin = 0.4f;
        private float _topDripSpawnDelayMax = 0.6f;
        private float _bottomSpawnDelayMin = 0.4f;
        private float _bottomSpawnDelayMax = 0.8f;
        private float _topLastX;
        private float _topLastDripX;
        private float _bottomLastX;
        private float _minDistanceX = 6;
        private int _minActionDrips = 3;
        private int _maxActionDrips = 6;

        private float _bubbleTimer;
        private float _topDripTimer;
        private float _bottomDripTimer;

        private string _appendedAnimationName;
        private string _cardType;

        private void Start()
        {
            _bubblesTop = new List<SpriteAnimation>();
            _dripsTop = new List<SpriteAnimation>();
            _dripsBottom = new List<SpriteAnimation>();
            _actionDrips = new List<SpriteAnimation>();

            _bubblesTopQueue = new Queue<SpriteAnimation>();
            _dripsTopQueue = new Queue<SpriteAnimation>();
            _dripsBottomQueue = new Queue<SpriteAnimation>();
            _actionDripQueue = new Queue<SpriteAnimation>();

            _bubbleTimer = Random.Range(_topBubbleSpawnDelayMin, _topBubbleSpawnDelayMax);
            _topDripTimer = Random.Range(_topDripSpawnDelayMin, _topDripSpawnDelayMax);
            _bottomDripTimer = Random.Range(_bottomSpawnDelayMin, _bottomSpawnDelayMax);

            SpawnBubbles();

            gameObject.SetActive(false);
        }

        public void EnableOutline(bool e, string cardType, bool conditionalGlow)
        {
            _appendedAnimationName = conditionalGlow ? "-orange" : string.Empty;
            _cardType = cardType == "elder_relic" ? "relic" : cardType;

            switch (_cardType)
            {
                case "spell": topDrips.anchoredPosition = new Vector2(0, 36);
                    if (!GameServer.IsRunning())
                    {
                       // _appendedAnimationName += "-none";
                    }
                    break;
                case "minion": topDrips.anchoredPosition = new Vector2(0, 40);
                    break;
                case "trick": topDrips.anchoredPosition = new Vector2(0, 36);
                    break;
                case "power": _appendedAnimationName = "-orange";
                    break;
            }

            if (e)
            {
                gameObject.SetActive(true);

                if (GameServer.IsRunning())
                {
                    outlineAnimator.Play($"spawn-{_cardType}{_appendedAnimationName}", frame =>
                    {
                        if (frame == 1)
                        {
                            SpawnActionDrips();
                        }
                    });
                }
                else
                {
                    outlineAnimator.Play($"spawn-{_cardType}{_appendedAnimationName}");
                }


            }
            else if (gameObject.activeSelf)
            {
                outlineAnimator.Play($"despawn-{_cardType}", () =>
                {
                    gameObject.SetActive(false);
                });
            }
        }

        public void Click(string cardType)
        {
            _cardType = cardType;

            if (gameObject.activeSelf)
            {
                outlineAnimator.Play($"click-{_cardType}{_appendedAnimationName}");
                SpawnActionDrips();
            }
        }

        public void SpawnActionDrips()
        {
            var count = Random.Range(_minActionDrips, _maxActionDrips);

            for (var i = 0; i < count; i++)
            {
                if (Random.value < 0.5)
                {
                    var x = Random.Range(-32, 32);
                    var y = 44;
                    var scaleX = x < 0 ? -1 : 1;

                    if (_actionDripQueue.Count > 0)
                    {
                        var actionDrip = _actionDripQueue.Dequeue();
                        actionDrip.SetActive(true);
                        actionDrip.transform.localScale = new Vector3(scaleX, 1, 1);
                        actionDrip.RectTransform().anchoredPosition = new Vector2(x, y);
                        actionDrip.Play($"spawn-particle{_appendedAnimationName}");

                        _actionDrips.Add(actionDrip);
                    }
                }
                else
                {
                    var x = Random.value > 0.5 ? -42 : 42;
                    var y = Random.Range(-30, 30);
                    var scaleX = x < 0 ? -1 : 1;

                    if (_actionDripQueue.Count > 0)
                    {
                        var actionDrip = _actionDripQueue.Dequeue();
                        actionDrip.SetActive(true);
                        actionDrip.transform.localScale = new Vector3(scaleX, 1, 1);
                        actionDrip.RectTransform().anchoredPosition = new Vector2(x, y);
                        actionDrip.Play($"spawn-particle{_appendedAnimationName}");

                        _actionDrips.Add(actionDrip);
                    }
                }
            }
        }

        private void SpawnBubbles()
        {
            for (var i = 0; i < 8; i++)
            {
                var topBubble = Instantiate(topBubblePrefab, topDrips, false);
                var topDrip = Instantiate(topDripPrefab, topDrips, false);
                var bottomDrip = Instantiate(bottomDripPrefab, bottomDrips, false);

                topBubble.RectTransform().anchoredPosition = Vector2.zero;
                topDrip.RectTransform().anchoredPosition = Vector2.zero;
                bottomDrip.RectTransform().anchoredPosition = Vector2.zero;

                topBubble.SetActive(false);
                topDrip.SetActive(false);
                bottomDrip.SetActive(false);

                _bubblesTopQueue.Enqueue(topBubble);
                _dripsTopQueue.Enqueue(topDrip);
                _dripsBottomQueue.Enqueue(bottomDrip);
            }

            for (var i = 0; i < _maxActionDrips * 2; i++)
            {
                var actionDrip = Instantiate(actionDripPrefab, actionDrips, false);
                actionDrip.RectTransform().anchoredPosition = Vector2.zero;
                actionDrip.SetActive(false);

                _actionDripQueue.Enqueue(actionDrip);
            }
        }

        private void Update()
        {
            _bubbleTimer -= Time.deltaTime;

            if (_bubbleTimer <= 0 && _bubblesTopQueue.Count > 0)
            {
                _bubbleTimer = Random.Range(_topBubbleSpawnDelayMin, _topBubbleSpawnDelayMax);

                if (_topLastX < 15 && _topLastX > -15)
                {
                    if (Random.value > 0.5)
                    {
                        _topLastX = Random.Range(15, 28);
                    }
                    else
                    {
                        _topLastX = Random.Range(-28, -15);
                    }
                }
                else if (_topLastX >= 15)
                {
                    _topLastX = Random.Range(-28, -15);
                }
                else
                {
                    _topLastX = Random.Range(15, 28);
                }


                var topBubble = _bubblesTopQueue.Dequeue();

                topBubble.RectTransform().anchoredPosition = new Vector2(_topLastX, 0);
                topBubble.SetActive(true);
                topBubble.Play($"idle-particle-2{_appendedAnimationName}");

                _bubblesTop.Add(topBubble);
            }

            _topDripTimer -= Time.deltaTime;

            if (_topDripTimer <= 0&& _dripsTopQueue.Count > 0)
            {
                _topDripTimer = Random.Range(_topDripSpawnDelayMin, _topDripSpawnDelayMax);

                if (_topLastDripX < 15 && _topLastDripX > -15)
                {
                    if (Random.value > 0.5)
                    {
                        _topLastDripX = Random.Range(15, 28);
                    }
                    else
                    {
                        _topLastDripX = Random.Range(-28, -15);
                    }
                }
                else if (_topLastDripX >= 15)
                {
                    _topLastDripX = Random.Range(-28, -15);
                }
                else
                {
                    _topLastDripX = Random.Range(15, 28);
                }


                var topDrip = _dripsTopQueue.Dequeue();

                topDrip.RectTransform().anchoredPosition = new Vector2(_topLastX, 0);
                topDrip.SetActive(true);
                topDrip.Play($"idle-particle-1{_appendedAnimationName}");

                _dripsTop.Add(topDrip);
            }

            _bottomDripTimer -= Time.deltaTime;

            if (_bottomDripTimer <= 0 && _dripsBottomQueue.Count > 0)
            {
                _bottomDripTimer = Random.Range(_bottomSpawnDelayMin, _bottomSpawnDelayMax);

                var x = _bottomLastX;

                while (Mathf.Abs(x - _bottomLastX) < _minDistanceX)
                {
                    _bottomLastX = Random.Range(-28, 28);
                }


                var bottomDrip = _dripsBottomQueue.Dequeue();

                bottomDrip.RectTransform().anchoredPosition = new Vector2(_bottomLastX, 0);
                bottomDrip.SetActive(true);
                bottomDrip.Play($"idle-particle-3{_appendedAnimationName}");

                _dripsBottom.Add(bottomDrip);
            }

            for (var i = _bubblesTop.Count - 1; i > -1; i--)
            {
                var topBubble = _bubblesTop[i];

                if (topBubble.IsDone)
                {
                    topBubble.SetActive(false);

                    _bubblesTop.Remove(topBubble);
                    _bubblesTopQueue.Enqueue(topBubble);
                }
            }

            for (var i = _dripsTop.Count - 1; i > -1; i--)
            {
                var topDrip = _dripsTop[i];

                if (topDrip.IsDone)
                {
                    topDrip.SetActive(false);

                    _dripsTop.Remove(topDrip);
                    _dripsTopQueue.Enqueue(topDrip);
                }
            }

            for (var i = _dripsBottom.Count - 1; i > -1; i--)
            {
                var bottomDrip = _dripsBottom[i];

                if (bottomDrip.IsDone)
                {
                    bottomDrip.SetActive(false);

                    _dripsBottom.Remove(bottomDrip);
                    _dripsBottomQueue.Enqueue(bottomDrip);
                }
            }

            for (var i = _actionDrips.Count - 1; i > -1; i--)
            {
                var actionDrip = _actionDrips[i];

                if (actionDrip.IsDone)
                {
                    actionDrip.SetActive(false);

                    _actionDrips.Remove(actionDrip);
                    _actionDripQueue.Enqueue(actionDrip);
                }
            }
        }
    }
}
