using System.Collections.Generic;
using UnityEngine;

namespace CrossBlitz.Card
{
    public class AbilityIconDisplay : MonoBehaviour
    {
        public List<SpriteAnimation> abilityIcons;

        public void SetupIconDisplay(CardData cardData)
        {
            for (var i = 0; i < abilityIcons.Count; i++)
            {
                abilityIcons[i].SetActive(false);
            }

            if (cardData.GetAbilities().Count <= 0)
            {
                return;
            }

            var addedKeywords = new List<string>();
            var abilities = cardData.GetAbilities();

            for (var i = 0; i < abilities.Count; i++)
            {
                if (i > 2) break;

                var ability = abilities[i];
                var added = false;

                switch (ability.keyword)
                {
                    case AbilityKeyword.BARRIER:
                        addedKeywords.AddDistinct("barrier");
                        added = true;
                        break;
                    case AbilityKeyword.ELUSIVE:
                        addedKeywords.AddDistinct("elusive");
                        added = true;
                        break;
                    case AbilityKeyword.FLYING:
                        addedKeywords.AddDistinct("flying");
                        added = true;
                        break;
                    case AbilityKeyword.FREEZE:
                        addedKeywords.AddDistinct("freeze");
                        added = true;
                        break;
                    case AbilityKeyword.LIFESTEAL:
                        addedKeywords.AddDistinct("lifesteal");
                        added = true;
                        break;
                    case AbilityKeyword.DUAL_STRIKE:
                        addedKeywords.AddDistinct("dual-strike");
                        added = true;
                        break;
                    case AbilityKeyword.PIERCE:
                        addedKeywords.AddDistinct("pierce");
                        added = true;
                        break;
                    case AbilityKeyword.POISON:
                        addedKeywords.AddDistinct("poisonous");
                        added = true;
                        break;
                    case AbilityKeyword.SILENCE:
                        addedKeywords.AddDistinct("silence");
                        added = true;
                        break;
                    case AbilityKeyword.TAUNT:
                        addedKeywords.AddDistinct("taunt");
                        added = true;
                        break;
                    case AbilityKeyword.THORNS:
                        addedKeywords.AddDistinct("thorns");
                        added = true;
                        break;
                    case AbilityKeyword.TOUGH:
                        addedKeywords.AddDistinct("tough");
                        added = true;
                        break;
                    case AbilityKeyword.RUSH:
                        addedKeywords.AddDistinct("rush");
                        added = true;
                        break;
                    // case AbilityKeyword.BIDE:
                    //     addedKeywords.AddDistinct("slow");
                    //     added = true;
                    //     break;
                    case AbilityKeyword.OVERGROW:
                        addedKeywords.AddDistinct("overgrow");
                        added = true;
                        break;
                    case AbilityKeyword.SWIFT_STRIKE:
                        addedKeywords.AddDistinct("swift-strike");
                        added = true;
                        break;
                    case AbilityKeyword.FLUX:
                        addedKeywords.AddDistinct("flux");
                        added = true;
                        break;
                }

                // the ability didn't add an icon, lets see if we can add one with a trigger type
                if (!added)
                {
                    switch (ability.triggerType)
                    {
                        case TriggerType.BATTLECRY:
                            addedKeywords.AddDistinct("battlecry");
                            break;
                        case TriggerType.DAMAGED:
                            addedKeywords.AddDistinct("damaged");
                            break;
                        case TriggerType.DEATHRATTLE:
                            addedKeywords.AddDistinct("deathrattle");
                            break;
                        case TriggerType.TURN_START:
                            addedKeywords.AddDistinct("turn-start");
                            break;
                        case TriggerType.TURN_END:
                            addedKeywords.AddDistinct("turn-end");
                            break;
                        case TriggerType.PLUNDER:
                            addedKeywords.AddDistinct("plunder");
                            break;
                        default:
                            if (i == 0 && cardData.type == CardType.Minion)
                            {
                                addedKeywords.AddDistinct("generic-minion");
                            }
                            break;
                    }
                }
            }

            for (var i = 0; i < addedKeywords.Count; i++)
            {
                abilityIcons[i].SetActive(true);
                abilityIcons[i].Play(addedKeywords[i]);
            }
        }
    }
}