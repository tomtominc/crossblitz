using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card.Utilities;
using CrossBlitz.Databases;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.Card
{
    public class KeywordDescriptionPopup : MonoBehaviour
    {
        public CanvasGroup canvasGroup;
        public RectTransform layout;
        public KeywordDescription keywordDescriptionPrefab;
        public ModifierTooltip modifierTooltip;
        private Dictionary<string,KeywordDescription> _keywords;
        private Vector2 _originalPos;
        private CardView m_view;
        private string m_cardId;
        private string m_gameCardId;

        private int _side;

        private void Awake()
        {
           _originalPos = this.RectTransform().anchoredPosition;
           m_view = transform.parent != null ? transform.parent.GetComponent<CardView>() : null;
        }

        private bool HasKeywordsToDisplay()
        {
            return _keywords != null && _keywords.Count > 0;
        }

        public void KillAnimation()
        {
            canvasGroup.DOKill();
            this.RectTransform().DOKill();
        }

        public void SetCardData(string cardId)
        {
            m_cardId = cardId;
            var cardData = Db.CardDatabase.GetCard(m_cardId);

            ResetKeywords();

            if (cardData != null)
            {
                ParseTextForKeywords(cardData.description);
            }
        }

        public void SetCardState(string gameCardUid)
        {
            m_gameCardId = gameCardUid;
            modifierTooltip.SetGameCardUid(gameCardUid);

            var gameCardState = GameServer.State.GetCard(m_gameCardId);
            var abilities = gameCardState.GetAbilities();

            for (var i = 0; i < abilities.Count; i++)
            {
                var ability = abilities[i];
                ParseTextForKeywords(ability.abilityDescription, true);
            }
        }

        private void ResetKeywords()
        {
            if (_keywords == null)
            {
                _keywords = new Dictionary<string, KeywordDescription>();
            }

            foreach (var keywordPair in _keywords)
            {
                Destroy(keywordPair.Value.gameObject);
            }

            _keywords.Clear();
        }
        public void ParseTextForKeywords(string text, bool fromModifier = false)
        {
            if (string.IsNullOrEmpty(text)) return;

            var upper = text.ToUpper();

            foreach (var keywordPair in CardDescriptionFormatter.KeywordDescriptions)
            {
                if (upper.Contains(keywordPair.Key) && !_keywords.ContainsKey(keywordPair.Key))
                {
                    var keywordDescription = Instantiate(keywordDescriptionPrefab, layout, false);
                    keywordDescription.SetKeyword(keywordPair.Key, keywordPair.Value, fromModifier);
                    _keywords.Add(keywordPair.Key, keywordDescription);
                }
            }
        }

        public void ShowModifierTooltip(int side, float delay, bool showArrow)
        {
            if (modifierTooltip && modifierTooltip.IsReady())
            {
                modifierTooltip.Open(side, delay, showArrow ? this.RectTransform() : null, showArrow);
            }
        }
        public void AnimateIn(int side, float delay=0.4f)
        {
            var keywordsDisplayed = HasKeywordsToDisplay();
            _side = side;

            if (keywordsDisplayed)
            {
                gameObject.SetActive(true);

                canvasGroup.DOKill();
                canvasGroup.alpha = 0;
                canvasGroup.DOFade(1, 0.24f).SetDelay(delay);

                this.RectTransform().DOKill();
                this.RectTransform().anchoredPosition =
                    new Vector2(_originalPos.x * _side + 16 * -_side, _originalPos.y);
                this.RectTransform().DOAnchorPosX(_originalPos.x * _side, 0.24f).SetDelay(delay).SetEase(Ease.OutBack);
            }

            ShowModifierTooltip(side, keywordsDisplayed ? delay + 0.24f : 0.24f, keywordsDisplayed);
        }

        private void OnDestroy()
        {
           KillAnimation();
        }

        public void AnimateIn(float delay=0.4f, HoverCard parent = null)
        {
            if (_side == 0 & m_view != null)
            {
                var hoverCard = parent;

                if (hoverCard)
                {
                    if (hoverCard.ForceToolTipsRight)
                    {
                        _side = 1;
                    }
                    else if (hoverCard.ForceToolTipsLeft)
                    {
                        _side = -1;
                    }
                    else
                    {
                        _side = m_view.RectTransform().position.x > 30f ? -1 : 1;
                    }
                }
                else
                {
                    _side = m_view.RectTransform().position.x > 30f ? -1 : 1;
                }
            }
            else if (_side == 0)
            {
                _side = 1;
            }

            // canvasGroup.DOKill();
            // canvasGroup.alpha = 0;
            // canvasGroup.DOFade(1, 0.24f).SetDelay(delay);
            //
            // this.RectTransform().DOKill();
            // this.RectTransform().anchoredPosition = new Vector2(_originalPos.x * _side + 16 * -_side, _originalPos.y);
            // this.RectTransform().DOAnchorPosX(_originalPos.x * _side, 0.24f).SetDelay(delay).SetEase(Ease.OutBack);

            AnimateIn(_side, delay);
        }

        public void KillTween()
        {
            DOTween.Kill(canvasGroup);
            DOTween.Kill(this.RectTransform());
            canvasGroup.alpha = 0;
            modifierTooltip.Close();
        }
    }
}