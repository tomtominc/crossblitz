using CrossBlitz.DeckEditAPI;
using CrossBlitz.PlayFab.Authentication;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Card
{
    public class CardCollectionComponents : MonoBehaviour
    {
        public SpriteAnimation cardInDeckArrow;
        public SpriteAnimation cardInDeckCountAnimator;
        public GameObject countBraces;
        public TextMeshProUGUI cardCount;
        public SpriteAnimation disabledFromCardCountOverlay;
        public GameObject cardEquipped;
        public GameObject cardEquippedRelic;

        private CardView _cardView;

        public void SetCardView(CardView cardView)
        {
            _cardView = cardView;
            _cardView.interactiveShadow.selectedDistance = 4;
            _cardView.interactiveShadow.hoverDistance = 6;
            _cardView.interactiveShadow.clickedDistance = 5;
            _cardView.interactiveShadow.SetNormalSettings(2, 4);

            this.SetActive(true);
        }

        public void Refresh(DeckEditSettings settings, CardData cardData)
        {
            var itemAmount = App.Inventory.GetItemAmount(cardData.id);
            var cardType = cardData.type;

            if (settings.OpenMode == DeckOpenMode.DeckEditor)
            {
                if (disabledFromCardCountOverlay)
                {
                    if (cardData.type == CardType.Minion)
                    {
                        if (!disabledFromCardCountOverlay.Play($"overlay-{cardData.type.ToString().ToLower()}-{cardData.attackType.ToString().ToLower()}"))
                        {
                            disabledFromCardCountOverlay.Play($"overlay-{cardData.type.ToString().ToLower()}");
                        }
                    }
                    else
                    {
                        disabledFromCardCountOverlay.Play($"overlay-{cardData.type.ToString().ToLower()}");
                    }

                    disabledFromCardCountOverlay.SetActive(false);
                }

                var isAtMaxCount = false;
                var maxValue = 0;
                var amountOfCardInDeck = settings.DeckSettings.deckData.GetAmountOfSingleCard(cardData.id);
                var itemsLeft = itemAmount - amountOfCardInDeck;

                countBraces.SetActive(true);
                cardCount.text = $"<color=#b29678>x</color>{Mathf.Clamp(itemsLeft, 0, itemAmount)}";

                switch (cardData.rarity)
                {
                    case Rarity.Common:
                        maxValue = DeckData.MaxCopiesOfCommonCards;
                        break;
                    case Rarity.Rare:
                        maxValue = DeckData.MaxCopiesOfRareCards;
                        break;
                    case Rarity.Mythic:
                        maxValue = DeckData.MaxCopiesOfMysticCards;
                        break;
                    case Rarity.Legendary:
                        maxValue = DeckData.MaxCopiesOfLegendaryCards;
                        break;
                }

                isAtMaxCount = amountOfCardInDeck >= maxValue || itemsLeft <= 0;

                if (isAtMaxCount)
                {
                    cardInDeckArrow.SetActive(true);
                    cardInDeckArrow.Play($"{cardData.faction.ToString().ToLower()}");
                    cardInDeckCountAnimator.Play($"{cardData.rarity.ToString().ToLower()}-{amountOfCardInDeck}");
                    disabledFromCardCountOverlay.image.color = new Color(0, 0, 0, 0.4f);
                    disabledFromCardCountOverlay.SetActive(true);
                    _cardView.shadow.enabled = false;
                }
                else if (amountOfCardInDeck > 0)
                {
                    cardInDeckArrow.SetActive(true);
                    cardInDeckArrow.Play($"{cardData.faction.ToString().ToLower()}");
                    cardInDeckCountAnimator.Play($"{cardData.rarity.ToString().ToLower()}-{amountOfCardInDeck}");
                    disabledFromCardCountOverlay.SetActive(false);
                    _cardView.shadow.enabled = true;
                }
                else
                {
                    cardInDeckArrow.SetActive(false);
                    disabledFromCardCountOverlay.SetActive(false);
                    _cardView.shadow.enabled = true;
                }

                if (cardType == CardType.Relic || cardType == CardType.Elder_Relic)
                {
                    if (amountOfCardInDeck > 0)
                    {
                        cardEquippedRelic.SetActive(true);
                        _cardView.GetCardComponent<DragCard>().SetClickable(false);
                        //_cardView.SetInteractable(false);
                    }
                    else
                    {
                        cardEquippedRelic.SetActive(false);
                        //_cardView.SetInteractable(true);
                        _cardView.GetCardComponent<DragCard>().SetClickable(true);
                    }
                }
            }
            else
            {
                cardInDeckArrow.SetActive(false);
                disabledFromCardCountOverlay.SetActive(false);
                _cardView.shadow.enabled = true;
            }

            if (itemAmount <= 0 && _cardView.frontMeldAnimator)
            {
                _cardView.frontMeldAnimator.SetActive(true);
                _cardView.UpdateFrontMeldAnimator();
            }
            else
            {
                _cardView.frontMeldAnimator.SetActive(false);
            }

            if (settings.OpenMode != DeckOpenMode.DeckEditor)
            {
                if (itemAmount > 0)
                {
                    countBraces.SetActive(true);
                    cardCount.text = $"<color=#b29678>x</color>{itemAmount}";
                }
                else
                {
                    countBraces.SetActive(false);
                    cardCount.text = $"{itemAmount}";
                }
            }

            if (cardType == CardType.Relic || cardType == CardType.Elder_Relic)
            {
                cardInDeckArrow.SetActive(false);
                countBraces.SetActive(false);
                disabledFromCardCountOverlay.SetActive(false);
            }
        }
    }
}