using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using UnityEngine;

namespace CrossBlitz.Card.Utilities
{
    public static class CardDescriptionFormatter
    {
        public static readonly Dictionary<string,int> SpriteIndices = new Dictionary<string, int>
        {
            {"PIRATE", 0},
            {"DEMONBORNE", 1},
            {"BEAST", 2},
            {"MAGE", 3},
            {"CHAMPION", 4},
            {"GOLEM", 5},
            {"DRAGON", 6},
            {"SPROUT ELF", 7},
            {"BRAWLER", 8},
            {"BARRIER", 9},
            {"SILENCE", 10},
            {"LIFESTEAL", 11},
            {"DUAL STRIKE", 12},
            {"TOUGH", 13},
            {"FREEZE", 14},
            {"BATTLECRY", 15},
            {"FLYING", 16},
            {"THORNS", 17},
            {"DEATHRATTLE", 18},
            {"POISONOUS", 19},
            {"FOCUS", 20},
            {"PIERCE", 21},
            {"TAUNT", 22},
            {"ELUSIVE", 23},
            {"MELEE", 24},
            {"ARCANE", 25},
            {"RANGED", 26},
            {"REDEEM", 27},
            {"RUSH", 28},
            {"SLOW", 29},
            {"PLUNDER", 23},
            {"PYRO", 30},
            {"FROST", 33},
            {"TERRA", 34},
            {"MAGITEK", 35},
            {"CHAKRA", 36},
            {"DRACONIC", 32},
            {"OVERGROW", 31},
            {"RIFF", 37},
            {"FLUX", 38},
            {"GUARD", 39},
            {"SWIFT STRIKE", 40},
            {"CANNON", 41},
            {"SHRUBASAUR", 42},
            {"CAST WHEN DRAWN", 43},
            {"IMMOBILE", 44},
            //{"CANNONS", 45},
            {"FROZEN", 46},
            {"FLEETING", 47},
            {"RELENTLESS", 48},
            {"EVOLVE", 49}
        };

        public static readonly Dictionary<string,string> KeywordDescriptions = new Dictionary<string, string>
        {
            {"FLEETING", "Destroy this minion when your turn starts."},
            {"FLUX", "After this card is discarded add it back to your hand."},
            {"BARRIER", "Negates the next damage this minion would take."},
            {"GUARD", "Damage to your hero is halved, rounded down, from all sources."},
            {"SILENCE", "Removes all abilities & enchantments."},
            {"LIFESTEAL", "Damage this minion deals also heals the controlling hero by that amount."},
            {"DUAL STRIKE", "This minion strikes twice."},
            {"TOUGH", "Takes 1 less damage from all sources."},
            {"RELENTLESS", "Resummon this minion the first time it dies."},
            {"BATTLECRY", "Triggers when played from your hand."},
            {"FLYING", "Can only be blocked by other Flying minions."},
            {"THORNS", "Deals X damage to melee minions that successfully strike this minion."},
            {"DEATHRATTLE", "Triggers when destroyed."},
            {"EVOLVE", "Transforms a friendly minion into one that costs 1 more or an enemy minion into one that costs 1 less."},
            {"FOCUS", "Upgrades in hand after X spells are cast."},
            {"PIERCE", "Excess strike damage is dealt directly to the enemy hero."},
            {"TAUNT", "Force an enemy minion to move on an unoccupied space in front of it."},
            {"ELUSIVE", "Can only be blocked by another Elusive minion."},
            {"RIFF", "A one-time effect, triggered after you cast a spell card."},
            {"REDEEM", "Choose one of three random cards to add to your hand."},
            {"RUSH", "This minion can attack the first turn its summoned."},
            {"SLOW", "This minion can't attack the turn it's summoned."},
            {"PLUNDER", "Triggers after this card destroys a minion."},
            {"OVERGROW", "When played, summon a copy to the right of this."},
            {"SWIFT STRIKE", "Strikes minions without taking return damage."},
            {"CAST WHEN DRAWN", "When drawn this card is cast automatically with random targets, if not specified."},
            {"FREEZE", "Frozen minions lose their next attack. While frozen, this minion can't retaliate or activate any abilities."},
            // {"PYRO", "Fire magic, mainly encompasses direct damage and devastating area effects."},
            // {"FROST", "Ice magic, based around the Freeze keyword, stalling enemy minions."},
            // {"TERRA", "Earth magic, allows control over many different elements, including, vegetation, water, electricity, and celestial elements."},
            // {"MAGITEK", "Golem magic, allows for powering & upgrading machines, devices."},
            // {"CHAKRA", "Used to create barriers, destructive energy techniques & enhance physical attributes. Requires a deep understanding of the body and mind."},
            // {"DRACONIC", "Dark magic, used to inflict pain and suffering. Can also impart feelings of confusion, dread, and misery."},
            {"IMMOBILE", "Can't attack or gain Power."},

        };

        public static string Format(string description)
        {
            description = description.ToUpper();

            foreach (var keyValueSprite in SpriteIndices)
            {
                description = Regex.Replace(description, $@"\b{keyValueSprite.Key}\b",
                    $"<sprite name=\"card-keyword-tags_256x256_{SpriteIndices[keyValueSprite.Key]}\">");
            }

            return description;
        }

        /// <summary>
        /// Used for formatting numbers like ({0} left!) usually found in Blitz Break cards.
        /// </summary>
        /// <param name="description">the unformatted description</param>
        /// <param name="cardData">The limit break data, I'll need this to pull values.</param>
        /// <param name="cardState">The card state if in a game, this can be null.</param>
        /// <returns>the formatted description</returns>
        public static string FormatNumbers(string description, CardData cardData, GameCardState cardState)
        {
            var limitBreakData = cardData.limitBreak;

            if (cardData.type == CardType.Power && limitBreakData != null)
            {
                switch (limitBreakData.type)
                {
                    case LimitBreakType.SUMMON_CLASS:
                        var value = cardState != null ? limitBreakData.value - cardState.limitBreakValue : limitBreakData.value;
                        description = string.Format(description, value);
                        break;
                }
            }

            if (cardState != null)
            {
                var focus = cardState.GetAbility(TriggerType.FOCUS);

                if (focus != null)
                {
                    var focusState = cardState.GetAbilityState(focus);

                    if (focusState != null)
                    {
                        if (focusState.focusValue >= focus.focusCount)
                        {
                            description = cardState.data.focusedDescription;
                        }
                        else
                        {
                            description = string.Format(description, focus.focusCount - focusState.focusValue);
                        }
                    }
                }

                if (cardState.data.hasDescriptionFormatting)
                {
                    var number = 0;

                    switch (cardState.data.formatCondition.condition)
                    {
                        case Condition.NUMBER_OF_SPECIFIC_CARDS_PLAYED:
                        {
                            var playCommands = GameServer.State.GetHistories(CommandType.PLAY);

                            for (var i = 0; i < playCommands.Count; i++)
                            {
                                var command = playCommands[i];
                                if (cardState.data.formatCondition.filters.HasFlag(FilterCondition.PLAYER) &&
                                    cardState.owner != command.PlayerUid)
                                {
                                    continue;
                                }

                                if (cardState.data.formatCondition.filters.HasFlag(FilterCondition.OPPONENT) &&
                                    cardState.owner == command.PlayerUid)
                                {
                                    continue;
                                }

                                var source = GameServer.State.GetCard(command.SourceUid);

                                if (source.characterData != null && source.characterData.id == cardState.data.formatCondition.cardId)
                                {
                                    number++;
                                }
                            }

                            break;
                        }
                        case Condition.NUMBER_OF_CARDS_PLAYED_FROM_DIFFERENT_FACTIONS:
                        {
                            var playCommands = GameServer.State.GetHistories(CommandType.PLAY);

                            for (var i = 0; i < playCommands.Count; i++)
                            {
                                var command = playCommands[i];

                                var source = GameServer.State.GetCard(command.SourceUid);
                                var player = GameServer.GetPlayerState(command.PlayerUid);

                                if (cardState.data.formatCondition.filters.HasFlag(FilterCondition.PLAYER) && cardState.owner != command.PlayerUid)
                                {
                                    continue;
                                }

                                if (cardState.data.formatCondition.filters.HasFlag(FilterCondition.OPPONENT) && cardState.owner == command.PlayerUid)
                                {
                                    continue;
                                }

                                if (source.characterData.faction != Faction.Neutral && source.characterData.faction != player.faction)
                                {
                                    number++;
                                }
                            }

                            var minionSummons = GameServer.State.GetHistories(CommandType.SUMMON);

                            for (var i = 0; i < minionSummons.Count; i++)
                            {
                                var command = (SummonCommand)minionSummons[i];

                                if (command.SummonMethod != SummonCommand.Method.PlayedFromHand) continue;

                                var source = GameServer.State.GetCard(command.SourceUid);
                                var player = GameServer.GetPlayerState(command.PlayerUid);

                                if (cardState.data.formatCondition.filters.HasFlag(FilterCondition.PLAYER) && cardState.owner != command.PlayerUid)
                                {
                                    continue;
                                }

                                if (cardState.data.formatCondition.filters.HasFlag(FilterCondition.OPPONENT) && cardState.owner == command.PlayerUid)
                                {
                                    continue;
                                }

                                if (source.characterData.faction != Faction.Neutral && source.characterData.faction != player.faction)
                                {
                                    number++;
                                }

                            }
                            break;
                        }
                        case Condition.FROZEN_THIS_GAME:
                        {
                            number = GameServer.State.gameStats.TotalMinionsFrozen;
                            break;
                        }
                        case Condition.NUMBER_OF_SPECIFIC_CARDS_IN_DECK:
                        {
                            if (cardState.data.formatCondition.filters.HasFlag(FilterCondition.PLAYER))
                            {
                                number = GameServer.GetPlayerState(cardState.owner)
                                    .GetNumberOfCardsWithIdInDeck(cardState.data.formatCondition.cardId);
                            }
                            else if (cardState.data.formatCondition.filters.HasFlag(FilterCondition.OPPONENT))
                            {
                                number = GameServer.GetPlayerState(cardState.Team == Team.Client ? Team.Opponent : Team.Client)
                                    .GetNumberOfCardsWithIdInDeck(cardState.data.formatCondition.cardId);
                            }
                            break;
                        }
                    }

                    description = string.Format( description, number );
                }

                var damageAbilities = cardState.GetAbilities( AbilityKeyword.DAMAGE );
                var additionalDamage = GameServer.State.AuraUpdater.GetAdditionalDamage(cardState.uid);

                for (var i = 0; i < damageAbilities.Count; i++)
                {
                    if (additionalDamage <= 0 && cardState.modifierIncrease <= 0 && ( damageAbilities[i].damageSchool == Class.All || damageAbilities[i].damageSchool == Class.None ))
                    {
                        continue;
                    }

                    var damage = damageAbilities[i].damageValue;
                    var spellSchoolIncrease = GameServer.State.AuraUpdater.GetSpellDamageForSchool(cardState.owner,  damageAbilities[i].damageSchool);
                    var totalDamage = damage + spellSchoolIncrease + cardState.modifierIncrease + additionalDamage;
                    //Debug.LogError($"total damage = {totalDamage} / {damage} ({cardState.characterData.name})");

                    if (totalDamage == damage)
                    {
                        continue;
                    }

                    var damageSchoolString = damageAbilities[i].damageSchool.ToString();
                    var font = totalDamage >= damage ? "HopeGoldOutlined_SpellDamageIncrease" : "HopeGoldOutlined_SpellDamageDecrease";
                    var replacement = $"<color=#ffffff><font=\"{font}\">{totalDamage}</font></color> {damageSchoolString}";

                    description = Regex.Replace(description,$"\\d+ {damageSchoolString}", replacement);
                }

                var spellDamageAuras = cardState.GetAbilities(AbilityKeyword.AURA);

                for (var i = 0; i < spellDamageAuras.Count; i++)
                {
                    var spellDamageAura = spellDamageAuras[i];
                    if (spellDamageAura.auraData.auraEffect != AuraData.AuraEffect.SpellDamage) continue;
                    var increasedDamage = cardState.GetIncreasedSpellDamageForAbility(spellDamageAura);
                    if (increasedDamage == 0) continue;
                    var damageSchoolString = spellDamageAura.auraData.spellSchool.ToString();
                    var totalDamage = spellDamageAura.auraData.modifyStatAmount + increasedDamage;
                    var font = increasedDamage > 0 ?  "HopeGoldOutlined_SpellDamageIncrease" : "HopeGoldOutlined_SpellDamageDecrease";
                    var replacement = $"{damageSchoolString} damage <color=#ffffff><font=\"{font}\">+{totalDamage}</font></color>";
                    description = Regex.Replace(description, $"{damageSchoolString} Damage \\+\\d+\\.", replacement);
                }


                if (cardState.location == CardLocation.Board)
                {
                    // var auras = GameServer.State.AuraUpdater.GetAurasCreatedByCard(cardState.uid);
                    // var increase = auras.Sum(a => a.auraAbility.auraData.modifyStatAmount);
                    // var auraAbilities = cardState.GetAbilities(AbilityKeyword.AURA);
                    //
                    // for (var i = 0; i < auraAbilities.Count; i++)
                    // {
                    //     var auraAbility = auraAbilities[i];
                    //
                    //     if (auraAbility.auraData.auraEffect != AuraData.AuraEffect.SpellDamage || !auraAbility.auraData.showTotalSpellDamageFromThisMinion)
                    //     {
                    //         continue;
                    //     }
                    //
                    //     var spellDamage = auraAbility.auraData.modifyStatAmount;
                    //     var totalDamage = increase;
                    //
                    //     if (totalDamage == spellDamage)
                    //     {
                    //         continue;
                    //     }
                    //
                    //     var damageSchoolString = auraAbilities[i].auraData.spellSchool.ToString();
                    //     var font = totalDamage > spellDamage ? "HopeGoldOutlined_SpellDamageIncrease" : "HopeGoldOutlined_SpellDamageDecrease";
                    //     var replacement = $"{damageSchoolString} damage <color=#ffffff><font=\"{font}\">+{totalDamage}</font></color>";
                    //
                    //     description = Regex.Replace(description, $"{damageSchoolString} Damage \\+\\d+\\.", replacement);
                    // }
                }
            }
            else
            {
                if (cardData.IsFocus())
                {
                    var focusAbility = cardData.GetAbilities().Find(a => a.triggerType == TriggerType.FOCUS);

                    if (focusAbility != null)
                    {
                        description = string.Format(description, focusAbility.focusCount);
                    }
                }

                if (cardData.hasDescriptionFormatting)
                {
                    description = string.Format(description, 0);
                }
            }


            return description;
        }
    }
}