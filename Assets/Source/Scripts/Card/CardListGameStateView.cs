using Source.Scripts.Card;
using UnityEngine;

namespace CrossBlitz.Card
{
    public class CardListGameStateView : MonoBehaviour, ICardComponent
    {
        public CanvasGroup canvasGroup;

        public void Initialize(CardView card)
        {

        }

        public void OnAfterCreation()
        {

        }

        public void Init(CardDeckViewItem deckViewItem, CardValue value)
        {
            deckViewItem.blackOverlay.SetActive(value == null || value.count == 0);
        }
    }
}