using CrossBlitz.Card.Utilities;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.Data;
using CrossBlitz.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Card
{
    public class ModifierTooltipItem : PageItem
    {
        public TextMeshProUGUI cardCount;
        public TextMeshProUGUI cardName;
        public SpriteAnimation cardTypeAnimator;
        public TextMeshProUGUI smallModifier;
        public TextMeshProUGUI largeModifier;
        public VerticalLayoutGroup layoutGroup;

        public override void SetAsEmpty(int itemIndex)
        {
            gameObject.SetActive(false);
        }

        private void AsSmallView()
        {
            smallModifier.SetActive(true);
            largeModifier.SetActive(false);

            layoutGroup.padding.top = 33;
            layoutGroup.padding.bottom = 0;
        }

        private void AsLargeView()
        {
            smallModifier.SetActive(false);
            largeModifier.SetActive(true);

            layoutGroup.padding.top = 36;
            layoutGroup.padding.bottom = 10;
        }

        public static bool ModifierSupported(CommandType type)
        {
            return type == CommandType.MODIFY_POWER || type == CommandType.MODIFY_HEALTH ||
                   type == CommandType.MODIFY_COST || type == CommandType.GIVE_ABILITY;
        }

        public void SetData(ModifierTooltipData modifierData, GameCardState source,  Command command)
        {
            var appliedBy = GameServer.State.GetCard(command.SourceUid);

            cardName.text = appliedBy.characterData.name;

            switch (appliedBy.characterData.type)
            {
                case CardType.Minion: cardTypeAnimator.Play("minion");
                    break;
                case CardType.Power: cardTypeAnimator.Play("power");
                    break;
                case CardType.Spell: cardTypeAnimator.Play("spell");
                    break;
                case CardType.Trick: cardTypeAnimator.Play("trick");
                    break;
                case CardType.Relic: cardTypeAnimator.Play("relic");
                    break;
                case CardType.Elder_Relic: cardTypeAnimator.Play("elder-relic");
                    break;
            }

            switch (command.Type)
            {
                case CommandType.MODIFY_POWER:
                case CommandType.MODIFY_HEALTH:
                {
                    var power = 0;
                    var health = 0;

                    if (command is ModifyHealthAndPowerCommand healthAndPowerCommand)
                    {
                        power = healthAndPowerCommand.Power;
                        health = healthAndPowerCommand.Health;
                    }
                    else if (command is ModifyPowerCommand powerCommand)
                    {
                        power = powerCommand.Amount;
                    }
                    else if (command is ModifyHealthCommand healthCommand)
                    {
                        health = healthCommand.Amount;
                    }

                    if (power > 0 && health > 0)
                    {
                        smallModifier.text = $"+{power}/+{health}";
                        AsSmallView();
                    }
                    else if (power < 0 && health < 0)
                    {
                        smallModifier.text = $"{power}/{health}";
                        AsSmallView();
                    }
                    else if (power > 0)
                    {
                        smallModifier.text = $"+{power} Power";
                        AsSmallView();
                    }
                    else if (power < 0)
                    {
                        smallModifier.text = $"{power} Power";
                        AsSmallView();
                    }
                    else if (health > 0)
                    {
                        smallModifier.text = $"+{health} Health";
                        AsSmallView();
                    }
                    else if (health < 0)
                    {
                        smallModifier.text = $"{health} Health";
                        AsSmallView();
                    }

                    break;
                }
                case CommandType.MODIFY_COST:
                {
                    var cost = ((ModifyCostCommand)command).ModifyAmount;

                    if (cost > 0)
                    {
                        smallModifier.text = $"Increased Cost +{cost}";
                        AsSmallView();
                    }
                    else if (cost < 0)
                    {
                        smallModifier.text = $"Decreased Cost {cost}";
                        AsSmallView();
                    }

                    break;
                }
                case CommandType.GIVE_ABILITY:
                {
                    if (command is GiveAbilityCommand giveAbility)
                    {
                        if (giveAbility.AbilitiesToGive.Count > 0)
                        {
                            var firstAbility = giveAbility.AbilitiesToGive[0];
                            var description = firstAbility.abilityDescription;

                            if (string.IsNullOrEmpty(description))
                            {
                                Debug.LogError("A Given ability does not have an ability description!!");
                            }

                            if (!string.IsNullOrEmpty(description) && description.Length > 12)
                            {
                                largeModifier.text = CardDescriptionFormatter.Format( description );
                                AsLargeView();
                            }
                            else
                            {
                                smallModifier.text = CardDescriptionFormatter.Format( description );
                                AsSmallView();
                            }
                        }
                    }
                    break;
                }
            }
        }

        public void SetData(ModifierTooltipData modifierData, GameCardState source,  CardHistory history)
        {
            var appliedBy = GameServer.State.GetCard(history.AppliedBy);

            cardName.text = appliedBy.characterData.name;

            switch (appliedBy.characterData.type)
            {
                case CardType.Minion: cardTypeAnimator.Play("minion");
                    break;
                case CardType.Power: cardTypeAnimator.Play("power");
                    break;
                case CardType.Spell: cardTypeAnimator.Play("spell");
                    break;
                case CardType.Trick: cardTypeAnimator.Play("trick");
                    break;
                case CardType.Relic: cardTypeAnimator.Play("relic");
                    break;
                case CardType.Elder_Relic: cardTypeAnimator.Play("elder-relic");
                    break;
            }

            if (history.PowerModifier > 0 && history.HealthModifier > 0)
            {
                smallModifier.text = $"+{history.PowerModifier}/+{history.HealthModifier}";
                AsSmallView();
            }
            else if (history.PowerModifier < 0 && history.HealthModifier < 0)
            {
                smallModifier.text = $"{history.PowerModifier}/{history.HealthModifier}";
                AsSmallView();
            }
            else if (history.PowerModifier > 0)
            {
                smallModifier.text = $"+{history.PowerModifier} Power";
                AsSmallView();
            }
            else if (history.PowerModifier < 0)
            {
                smallModifier.text = $"{history.PowerModifier} Power";
                AsSmallView();
            }
            else if (history.HealthModifier > 0)
            {
                smallModifier.text = $"+{history.HealthModifier} Health";
                AsSmallView();
            }
            else if (history.HealthModifier < 0)
            {
                smallModifier.text = $"{history.HealthModifier} Health";
                AsSmallView();
            }
            else if (history.CostModifier > 0)
            {
                smallModifier.text = $"+{history.CostModifier} Cost Increase";
                AsSmallView();
            }
            else if (history.CostModifier < 0)
            {
                smallModifier.text = $"+{history.CostModifier} Cost Decrease";
                AsSmallView();
            }
        }

        public override void SetData(IPageItemData data, int itemIndex)
        {
            gameObject.SetActive(true);

            var modifierData = (ModifierTooltipData)data;
            var source = GameServer.MainState.GetCard(modifierData.sourceUid);

            if (!string.IsNullOrEmpty(modifierData.cardHistoryGuid))
            {
                var history = source.GetHistory(modifierData.cardHistoryGuid);
                SetData(modifierData, source, history);
            }
            else
            {
                var command = GameServer.MainState.GetHistory(modifierData.stateHistoryGuid);

                if (command != null)
                {
                    SetData(modifierData, source, command);
                }
                else
                {
                    Debug.LogError($"Command is null!!! {modifierData.stateHistoryGuid}");
                }
            }

            cardCount.text = $"x{modifierData.duplicateCount}";
            cardCount.SetActive(modifierData.duplicateCount > 1);
        }
    }

    public class ModifierTooltipData : IPageItemData
    {
        public string sourceUid;
        public int duplicateCount;
        public string cardHistoryGuid;
        public int stateHistoryGuid;
    }
}