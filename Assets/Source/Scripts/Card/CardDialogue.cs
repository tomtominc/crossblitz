using System.Collections.Generic;
using System.Threading.Tasks;
using CrossBlitz.Audio;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.GameVfx;
using CrossBlitz.Transition;
using DG.Tweening;
using Febucci.UI;
using MEC;
using Source.Scripts.Card;
using TakoBoyStudios;
using TMPro;
using UnityAsync;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Card
{
    public class CardDialogue : MonoBehaviour
    {
        public const float CloseDelay = 3f;

        public CanvasGroup canvas;
        public RectTransform container;
        public SpriteAnimation emote;
        public TextMeshProUGUI dialogue;
        public RectTransform effectRectTransform;
        public Sprite upBoxSprite;
        public Sprite downBoxSprite;
        public RectTransform nextArrowRect;
        public CanvasGroup nextArrowCanvas;

        private Dictionary<string, GenericVfxComponent> loadedEffects;
        private float closeDelay;
        private bool closeInputEnabled;
        private bool dialogueShowed;
        private bool _userInput;
        private bool closeDialogue;
        private TextAnimator textAnimator;
        private float _arrowSine;

        private void Awake()
        {
            textAnimator = dialogue.GetComponent<TextAnimator>();
            textAnimator.onEvent -= OnEvent;
            textAnimator.onEvent += OnEvent;

            loadedEffects = new Dictionary<string, GenericVfxComponent>();

            dialogueShowed = false;
            closeInputEnabled = false;
            closeDialogue = false;
            _userInput = false;
        }

        public async Task<T> GetVfx<T>(GameVfxData vfxData) where T : GameVfxComponent, ICardEffectComponent
        {
            var effectComponent = effectRectTransform.gameObject.GetComponent<T>();

            if (effectComponent == null || effectComponent.Effect == null)
            {
                effectComponent = effectRectTransform.gameObject.AddComponent<T>();
                effectComponent.SetGameVfx(vfxData);
                effectComponent.Initialize(null);
                effectComponent.OnAfterCreation();
            }

            while (effectComponent.Effect == null)
            {
                await Await.NextUpdate();
            }

            return effectComponent;
        }

        private async void OnEvent(string message)
        {
            if (message.Contains("load-effect"))
            {
                var parameters = message.Split(':');

                if (parameters.Length > 1)
                {
                    var effectName = parameters[1];
                    var effect = Db.BattleDatabase.GetBattleVfxByName(effectName);
                    loadedEffects ??= new Dictionary<string, GenericVfxComponent>();

                    if (effect != null)
                    {
                        if (loadedEffects.ContainsKey(effect.Uid))
                        {
                            if (loadedEffects[effect.Uid].Effect == null)
                            {
                                loadedEffects.Remove(effect.Uid);
                            }
                        }

                        if (loadedEffects.ContainsKey(effect.Uid))
                        {
                            var vfxComponent = await GetVfx<GenericVfxComponent>(effect);
                            ((MonoBehaviour)vfxComponent.Effect).SetActive(false);
                            loadedEffects.Add(effect.Uid, vfxComponent);
                        }
                    }
                    else
                    {
                        Debug.LogError($"No Effect with name: {effectName}");
                    }
                }
            }
            if (message.Contains("battle-effect"))
            {
                var parameters = message.Split(':');

                if (parameters.Length > 1)
                {
                    var effectName = parameters[1];
                    var effect = Db.BattleDatabase.GetBattleVfxByName(effectName);

                    if (effect != null)
                    {
                        if (loadedEffects.ContainsKey(effect.Uid))
                        {
                            if (loadedEffects[effect.Uid].Effect == null)
                            {
                                loadedEffects.Remove(effect.Uid);
                            }
                        }

                        if (loadedEffects.ContainsKey(effect.Uid))
                        {
                            var vfxComponent = loadedEffects[effect.Uid];
                            ((MonoBehaviour)vfxComponent.Effect).SetActive(true);
                            Timing.RunCoroutine(vfxComponent.Effect.Play(null,
                                new VfxParameters { destroyAfterCompletion = true }));
                        }
                        else
                        {
                            var vfxComponent = await GetVfx<GenericVfxComponent>(effect);
                            ((MonoBehaviour)vfxComponent.Effect).SetActive(true);
                            Timing.RunCoroutine(vfxComponent.Effect.Play(null,
                                new VfxParameters { destroyAfterCompletion = true }));
                        }
                    }
                    else
                    {
                        Debug.LogError($"No Effect with name: {effectName}");
                    }
                }
            }
            if (message.Contains("screen-shake"))
            {
                var parameters = message.Split(':');
                var magnitude = 4;
                var duration = 0.5f;

                if (parameters.Length > 1)
                {
                    magnitude = int.Parse(parameters[1]);
                }

                if (parameters.Length > 2)
                {
                    duration = float.Parse(parameters[2]);
                }

                GameplayScreen.Instance.ShakeScreen(magnitude,duration);
            }
            else if (message.Contains("flash"))
            {
                var parameters = message.Split(':');

                if (parameters.Length > 2)
                {
                    var color = parameters[1].ToColor();
                    var blendMode = GameUtilities.ParseEnum<BlendModes.BlendMode>(parameters[2]);
                    TransitionController.FlashScreen(color, blendMode);
                }
                else
                {
                    TransitionController.FlashScreen();
                }
            }
            else if (message.Contains("stop-ambient"))
            {
                AmbientController.Stop();
            }
            else if (message.Contains("ambient"))
            {
                var audioId = message.Split(':');

                if (audioId.Length > 1)
                {
                    AmbientController.PlayAmbient(audioId[1]);
                }
            }
            else if (message.Contains("fade-music"))
            {
                var parameters = message.Split(':');
                var seconds = 1;

                if (parameters.Length > 1)
                {
                    seconds = int.Parse(parameters[1]);
                }

                AudioController.FadePlaylistVolume(0f, seconds);
            }
            else if (message.Contains("sfx"))
            {
                var audioId = message.Split(':');

                if (audioId.Length > 1)
                {
                    AudioController.PlaySound(audioId[1]);
                }
            }
            else if (message.Contains("music"))
            {
                var audioId = message.Split(':');

                if (audioId.Length > 1)
                {
                    AudioController.PlaySong(audioId[1]);
                   // AudioController.FadePlaylistVolume(1f, 0.5f);
                }
            }
            else if (message.Contains("shake-char"))
            {
                var parameters = message.Split(':');

                var characterId = string.Empty;
                var magnitude = 4;
                var duration = 0.16f;

                if (parameters.Length > 1)
                {
                    characterId = parameters[1];
                }

                if (parameters.Length > 2)
                {
                    magnitude = int.Parse(parameters[2]);
                }

                if (parameters.Length > 3)
                {
                    duration = float.Parse(parameters[3]);
                }

                Events.Publish(this, EventType.OnShakeCharacter, new OnShakeCharacterEventArgs
                {
                    characterId = characterId,
                    magnitude = magnitude,
                    duration = duration
                });
            }
            else if (message.Contains("bounce-char"))
            {
                var characterId = message.Split(':');

                if (characterId.Length > 1)
                {
                    Events.Publish(this, EventType.OnBounceCharacter, new OnBounceCharacterEventArgs { characterId = characterId[1]});
                }
                else
                {
                    Events.Publish(this, EventType.OnBounceCharacter, new OnBounceCharacterEventArgs());
                }
            }


        }

        public void SetDirection(Vector2 direction)
        {
            if (direction.y < 0)
            {
                this.RectTransform().anchoredPosition = new Vector2(20f, -36.5f);
                var dialogueLayout = container.GetComponent<HorizontalLayoutGroup>();
                dialogueLayout.padding = new RectOffset(2, 4, 8, 6);
                emote.RectTransform().anchoredPosition = new Vector2(15, 7);
                container.GetComponent<Image>().sprite = downBoxSprite;
            }
            else if (direction.y > 0)
            {
                // don't need this yet, but I might to able to switch team board spots.
            }
        }

        public void SetDialogue(DialogueSnippetData snippetData, bool userInput = false)
        {
            if (snippetData == null || string.IsNullOrEmpty(snippetData.Dialogue))
            {
                return;
            }

            DOTween.Kill(container);
            DOTween.Kill(canvas);

            gameObject.SetActive(true);
            canvas.alpha = 1;
            textAnimator = dialogue.GetComponent<TextAnimator>();
            _userInput = false;
            closeDialogue = false;


            if (_userInput) container.DOPunchScale(Vector2.one * 0.25f, 0.5f);
            else container.DOPunchScale(Vector2.one * 0.25f, 0.5f).OnComplete(CloseDialogue);

            if (string.IsNullOrEmpty(snippetData.Emotion) || snippetData.Emotion == "talking")
            {
                emote.gameObject.SetActive(false);
            }
            else
            {
                emote.gameObject.SetActive(true);
                emote.Play(snippetData.Emotion);
            }

            //emote.Play(snippetData.Emotion);
            dialogue.text = snippetData.Dialogue;

            if (!string.IsNullOrEmpty(snippetData.SoundFx))
            {
                AudioController.PlaySound(snippetData.SoundFx);
            }
        }


        private void CloseDialogue()
        {
            Debug.Log("CLOSE DIALOGUE");
            if (_userInput)
            {
                canvas.DOFade(0, 0.25f).OnComplete(() => gameObject.SetActive(false));
                dialogueShowed = false;
                closeInputEnabled = false;
                closeDialogue = true;
                ArrowFadeOut();
            }
            else
            {
                canvas.DOFade(0, 0.25f).SetDelay(dialogue.text.Length * 0.04f).OnComplete(() => gameObject.SetActive(false));
            }
        }

        public void ArrowFadeIn()
        {
            nextArrowCanvas.alpha = 0;
            nextArrowRect.SetActive(true);
            nextArrowCanvas.DOFade(1, .1f).OnComplete(()=> closeInputEnabled = true);
        }

        public void ArrowFadeOut()
        {
            nextArrowCanvas.DOFade(0, .1f).OnComplete(() => nextArrowCanvas.SetActive(false));
        }



        private void Update()
        {
            //var textAnimator = dialogue.GetComponent<TextAnimator>();

            if (_userInput)
            {
                if (!closeDialogue && !dialogueShowed && textAnimator.allLettersShown)
                {
                    dialogueShowed = true;
                    ArrowFadeIn();
                }

                if (closeInputEnabled && Input.GetMouseButtonDown(0))
                {
                    CloseDialogue();
                }

                // Oscillate Arrow
                if (nextArrowRect.IsActive())
                {
                    _arrowSine = Mathf.PingPong(Time.time*.5f, 0.25f);
                    var y = EasingFunction.EaseOutQuad(18, 24, _arrowSine);
                    nextArrowRect.anchoredPosition = new Vector2(nextArrowRect.anchoredPosition.x, y-8f);
                }

            }
        }
    }
}