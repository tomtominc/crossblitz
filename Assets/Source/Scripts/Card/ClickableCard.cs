using System;
using CrossBlitz.ClientAPI.GameLogic.Visuals;
using CrossBlitz.ClientAPI.Visuals;
using Source.Scripts.Card;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CrossBlitz.Card
{
    public class ClickableCard : MonoBehaviour, ICardComponent, IPointerClickHandler
    {
        public event Action<ClickableCard> OnLeftClick;
        public event Action<ClickableCard> OnRightClick;
        public event Action<ClickableCard> OnMiddleClick;

        private GameCardView _gameCard;
        private CardView _cardView;

        public void Initialize(CardView card)
        {
            _cardView = card;
            _gameCard = _cardView.GetComponent<GameCardView>();
        }

        public void OnAfterCreation()
        {

        }

        public void OnPointerClick(PointerEventData eventData)
        {
            switch (eventData.button)
            {
                case PointerEventData.InputButton.Right:
                    OnRightClick?.Invoke(this);
                    break;
                case PointerEventData.InputButton.Left:
                    OnLeftClick?.Invoke(this);
                    break;
                case PointerEventData.InputButton.Middle:
                    OnMiddleClick?.Invoke(this);
                    break;
            }
        }
    }
}