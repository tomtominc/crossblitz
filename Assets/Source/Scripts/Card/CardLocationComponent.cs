using System;
using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.AssetManagement;
using CrossBlitz.Audio;
using CrossBlitz.Card.Vfx;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Cursors;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.GameVfx;
using CrossBlitz.GameVfx.Minion;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.Plugins.TakoBoyStudios.Utilities;
using CrossBlitz.ServerAPI.GameLogic;
using DG.Tweening;
using Source.Scripts.Card;
using UnityEngine;
using UnityEngine.EventSystems;
using Events = CrossBlitz.ClientAPI.GameLogic.EventSystem.Events;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Card
{
    public class CardLocationComponent : MonoBehaviour,
        ICardComponent,
        IPointerEnterHandler,
        IPointerExitHandler,
        IBeginDragHandler
    {
        private bool _isLoadingCardLocation;
        private GameObject _cardLocationHoverPrefab;

        private CardData _cardData;
        private CardView _cardView;
        private CardLocationHoverEffect _cardLocationEffect;

        private GameCardView _gameCard;
        private RectTransform _parentRect;

        private int _previousSortOrder;
        private bool _isHovered;
        private float _offsetX;
        private float _zoomedCardDelay;
        private bool _isFocus;
        public bool ForceToolTipsRight { get; set; }
        public bool ForceToolTipsLeft { get; set; }
        public bool ForceToolTipsOpposite { get; set; }

        public bool IsHoverable { get; set; }
        public bool IsLocked { get; set; }

        public bool UsesHoverPunchEffect { get; set; }
        public Vector2 HoverPunchDirection { get; set;  }

        public bool UsesScalingPunchEffect { get; set; }
        public CardData Data => _cardData;
        public void Initialize(CardView card)
        {
            _cardView = card;
        }

        public void OnAfterCreation()
        {
            if (_cardView.data == null || string.IsNullOrEmpty(_cardView.data.name))
            {
                Destroy(this);
                return;
            }

            _cardData = _cardView.data;
            _parentRect = _cardView.Rect;
            _gameCard = _cardView.GetCardComponent<GameCardView>();

            IsHoverable = true;
            UsesHoverPunchEffect = true;
            HoverPunchDirection = Vector2.down;

            if (_cardView != null)
            {
                CreateCardLocation(_parentRect);
            }

            if (_gameCard && _gameCard.View)
            {
                _gameCard.View.OnFormattedCardDescription += OnFormattedDescription;
            }
        }

        private void OnFormattedDescription(string description)
        {
            //if (_cardLocationEffect)
            //{
            //    _cardLocationEffect.descriptionLabel.text = description;
            //}
        }

        public void Initialize(CardData cardData, Transform parent)
        {
            _cardData =cardData;
            _parentRect = parent.RectTransform();

            IsHoverable = true;
            UsesHoverPunchEffect = false;
            HoverPunchDirection = Vector2.down;

            _isFocus = false;

            CreateCardLocation(_parentRect);
        }

        private async void CreateCardLocation(Transform parent)
        {
            if (_cardLocationHoverPrefab == null && !_isLoadingCardLocation)
            {
                _isLoadingCardLocation = true;
                _cardLocationHoverPrefab = await AddressableReferenceLoader.GetAsset("CardLocationHoverEffect", true);
                _isLoadingCardLocation = false;
            }

            while (_cardLocationHoverPrefab == null) await new WaitForEndOfFrame();

            var cardLocation = Instantiate(_cardLocationHoverPrefab, parent.RectTransform());

            _cardLocationEffect = cardLocation.GetComponent<CardLocationHoverEffect>();
            _cardLocationEffect.SetupCard(_cardData);
            _cardLocationEffect.toolTipCanvas.blocksRaycasts = false;
            _cardLocationEffect.toolTipCanvas.interactable = false;
            _cardLocationEffect.toolTipCanvas.alpha = 0;

            _cardLocationEffect.SetActive(false);
        }

        private Tweener punchTween;

        public void OnPointerEnter(PointerEventData e)
        {
            if (!IsHoverable)
            {
                return;
            }

            if (GameCardView.Current) return;


             if (_cardLocationEffect == null || _cardData == null)
            {
                return;
            }

            if (_cardView != null)
            {
                if (_cardLocationEffect.cardId != _cardView.data.id)
                {
                    _cardData = _cardView.data;
                    _cardLocationEffect.SetupCard(_cardData);
                }
            }

            KillAllTweens();

            if (punchTween == null || !punchTween.IsActive())
            {
                if (UsesHoverPunchEffect)
                {
                    _parentRect.DOKill(true);
                    punchTween = _parentRect.RectTransform().DOPunchAnchorPos(HoverPunchDirection * 5, 0.2f);
                }
                else if (UsesScalingPunchEffect)
                {
                    _parentRect.DOKill(true);
                    punchTween = _parentRect.RectTransform().DOPunchScale(new Vector3(.1f,-.1f,0), 0.4f);
                }
            }


            AudioController.PlaySound("battle_card_hover", "BARDSFX", true, gameObject);

            var canvasRect = this.RectTransform().GetCanvas();
            var canvas = canvasRect.GetComponent<Canvas>();
            var screenSize = CrossBlitzInputModule.Instance.GetNativeScreenSize();
            var zoomedWorldPos = CrossBlitzInputModule.Instance.GetUiPositionAtNativeRes(_parentRect);
            var zoomedSize = _cardLocationEffect.RectTransform().sizeDelta;
            var previewPosition = new Vector2(zoomedSize.x * 0.66f + _offsetX, 0f);
            var zoomHeightHalved = zoomedSize.y / 2;

            if (zoomedWorldPos.x > screenSize.x/2)
            {
                previewPosition.x = -previewPosition.x;
            }

            if (_cardView)
            {
                _previousSortOrder = _cardView.GetSortOrder();
                _cardView.SetSortOrder(_previousSortOrder + GameCardView.DragSortingOrder);
            }

            // subtracting the margin so the card animates to the preview position
            _cardLocationEffect.transform.localEulerAngles = -_parentRect.eulerAngles;
            _cardLocationEffect.RectTransform().anchoredPosition = previewPosition;
            _cardLocationEffect.SetActive(true);

            _cardLocationEffect.toolTipCanvas.alpha = 0f;
            _zoomedCardDelay = GetZoomDelay();

            if (Math.Abs(_zoomedCardDelay - 2f) > float.Epsilon)
            {
                _cardLocationEffect.Fade(1, 0.12f, _zoomedCardDelay);
            }

            _cardLocationEffect.RectTransform().DOAnchorPosY(previewPosition.y, 1f).SetDelay(_zoomedCardDelay).SetEase(Ease.OutBack);
            _cardLocationEffect.RectTransform().DOAnchorPosX(previewPosition.x-8, 0.25f).SetDelay(_zoomedCardDelay).SetEase(Ease.OutBack);

            //Events.Publish(this, EventType.OnCardHoveredOver, _zoomedCard.data);

            _isHovered = true;
        }

        public static float GetZoomDelay()
        {
            return App.Settings.GetZoomedCardPopupDelay() switch
            {
                0 => 0f,
                1 => 0.3f,
                2 => 0.6f,
                3 => 1f,
                4 => 2f,
                _ => 0f
            };
        }

        public void KillMoveTween()
        {
            DOTween.Kill(_cardLocationEffect.RectTransform());
        }

        public void KillAllTweens()
        {
            if (_cardLocationEffect != null)
            {
                DOTween.Kill(_cardLocationEffect.RectTransform());
            }
        }

        public void OnPointerExit(PointerEventData e)
        {
            if (IsLocked) return;
            if (!_isHovered) return;

            _isHovered = false;

            KillAllTweens();

            //Events.Publish(this, EventType.OnCardHoveredOver, null);

            _cardLocationEffect.Fade(0, 0.12f, callback: ()=> _cardLocationEffect.SetActive(false));
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (IsLocked) return;
            if (!_isHovered) return;

            _isHovered = false;

            KillAllTweens();

            //Events.Publish(this, EventType.OnCardHoveredOver, null);

            _cardLocationEffect.Fade(0, 0.12f, callback: () => _cardLocationEffect.SetActive(false));
        }

        public void OnDestroy()
        {
            if (_cardLocationEffect)
            {
                DOTween.Kill(_cardLocationEffect.toolTipCanvas);
                DOTween.Kill(_cardLocationEffect.RectTransform());
            }

            if (_cardLocationEffect && _cardLocationEffect.gameObject)
            {
                AddressableReferenceLoader.Unload(_cardLocationEffect.gameObject);
            }
        }
    }
}
