using CrossBlitz.Card;
using UnityEngine;

namespace Source.Scripts.Card
{
    public class AbilityIcon : MonoBehaviour
    {
        public SpriteAnimation abilityIcon;
        public TMPSpriteFont abilityLabel;

        public AbilityIcon SetAbility(Ability ability)
        {
            switch (ability.keyword)
            {
                case AbilityKeyword.ARMOR_BODY:
                    abilityLabel.text = ability.armorBodyAmount.ToString();
                    abilityIcon.Play("armor-body");
                    break;
                case AbilityKeyword.LIFESTEAL:
                    abilityIcon.Play("lifesteal");
                    break;
                case AbilityKeyword.DUAL_STRIKE:
                    abilityLabel.SetActive(false);
                    abilityIcon.Play("dual-strike");
                    break;
                case AbilityKeyword.PIERCE:
                    abilityIcon.Play("pierce");
                    break;
                case AbilityKeyword.POISON:
                    abilityIcon.Play("poisonous");
                    break;
                case AbilityKeyword.SPLASH:
                    abilityIcon.Play("splash-damage");
                    break;
                case AbilityKeyword.RUSH:
                    abilityIcon.Play("rush");
                    break;
                case AbilityKeyword.SWIFT_STRIKE:
                    abilityIcon.Play("swift-strike");
                    break;
            }

            switch (ability.triggerType)
            {
                case TriggerType.DEATHRATTLE:
                    abilityIcon.Play("deathrattle");
                    break;
                case TriggerType.PLUNDER:
                    abilityIcon.Play("plunder");
                    break;
            }

            return this;
        }
    }
}
