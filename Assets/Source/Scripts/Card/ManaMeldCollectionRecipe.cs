using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Commands;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.Fables.Battle;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.Universal;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.GameVfx
{
    public class ManaMeldCollectionRecipe : MonoBehaviour, IGameVfx
    {
        public GameObject recipeSelectedContainer;
        private bool m_recipeSelected;

        public GameObject cardViewContainer;
        private CardView _cardView;

        public GameObject completedRecipeContainer;
        private bool m_recipeCompleted;
       
        public IngredientDisplayManaRecipe recipeIngredientPrefab;
        public RectTransform ingredientsLayout;

        public SpriteAnimation cardInDeckCountAnimator;
        public SpriteAnimation cardFactionAnimator;

        public GameObject readyStatus;

        private RectTransform m_rect;
        private float m_recipeCardOrigPosX;
        private float m_recipeCardOrigPosY;


        public void Start()
        {
            gameObject.SetActive(true);
            GetComponent<Transform>().SetAsFirstSibling();

            m_recipeCardOrigPosX = GetComponent<RectTransform>().anchoredPosition.x;
            m_recipeCardOrigPosY = GetComponent<RectTransform>().anchoredPosition.y;

            gameObject.RectTransform().anchoredPosition = new Vector2(m_recipeCardOrigPosX + 43, m_recipeCardOrigPosY - 56);
        }

        public void SetCardView(CardView cardView)
        {
            _cardView = cardView;
            _cardView.interactiveShadow.selectedDistance = 4;
            _cardView.interactiveShadow.hoverDistance = 6;
            _cardView.interactiveShadow.clickedDistance = 5;
            _cardView.interactiveShadow.SetNormalSettings(2, 4);

            this.SetActive(true);
        }

        public void Refresh(DeckEditSettings settings, CardData cardData)
        {

            ingredientsLayout.DestroyChildren();

            // Set READY status
            readyStatus.SetActive(Crafting.CanCraftCard(cardData, out _));

            // Set Selected status
            recipeSelectedContainer.SetActive(false);

            var itemAmount = App.Inventory.GetItemAmount(cardData.id);
            var isAtMaxCount = false;
            var maxValue = 0;
            var amountOfCardInDeck = settings.DeckSettings.deckData.GetAmountOfSingleCard(cardData.id);

            switch (cardData.rarity)
            {
                case Rarity.Common:
                    maxValue = DeckData.MaxCopiesOfCommonCards;
                    break;
                case Rarity.Rare:
                    maxValue = DeckData.MaxCopiesOfRareCards;
                    break;
                case Rarity.Mythic:
                    maxValue = DeckData.MaxCopiesOfMysticCards;
                    break;
                case Rarity.Legendary:
                    maxValue = DeckData.MaxCopiesOfLegendaryCards;
                    break;
            }

            isAtMaxCount = itemAmount >= maxValue;

            cardInDeckCountAnimator.Play($"{cardData.rarity.ToString().ToLower()}-{itemAmount}");
            cardFactionAnimator.Play($"{cardData.faction.ToString().ToLower()}");

            if (isAtMaxCount)
            {
                ingredientsLayout.SetActive(false);
                completedRecipeContainer.SetActive(true);
                readyStatus.SetActive(false);
            }
            else
            {
                ingredientsLayout.SetActive(true);
                completedRecipeContainer.SetActive(false);
            }


            // Set Mana Shard Cost
            var manaShard = Instantiate(recipeIngredientPrefab, ingredientsLayout);
            var costToCraft = cardData.craftingData.cost;
            manaShard.SetItemId(null, costToCraft, true);

            // Set Ingredients
            for (var i = 0; i < cardData.craftingData.ingredients.Count; i++)
            {
                var display = Instantiate(recipeIngredientPrefab, ingredientsLayout);
                display.SetItemId(cardData.craftingData.ingredients[i].ItemUid, cardData.craftingData.ingredients[i].Count);
            }

            if (itemAmount <= 0 && _cardView.frontMeldAnimator)
            {
                _cardView.frontMeldAnimator.SetActive(false);
                //_cardView.frontMeldAnimator.SetActive(true);
                //_cardView.UpdateFrontMeldAnimator();
            }
            else
            {
                _cardView.frontMeldAnimator.SetActive(false);
            }

            ////Select the preview card if it has been selected
            //if (ManaMeldEditMenu.Instance.previewCard != null)
            //{
            //    Debug.Log("PREVIEW CARD NAME: " + ManaMeldEditMenu.Instance.previewCard.data.name);
            //    Debug.Log("LOADED CARD NAME: " + _cardView.data.name);

            //    if(ManaMeldEditMenu.Instance.previewCard.data.name == _cardView.data.name)
            //    {
            //        ManaMeldEditMenu.Instance.UpdatePreview(_cardView);
            //    }
            //}
        }

        public void Initialize(CardView card)
        {
            _cardView = card;
            gameObject.SetActive(true);
        }

        public void Initialize(Transform rect)
        {
            m_rect = rect as RectTransform;
            //m_recipeCardOrigPosX = GetComponent<RectTransform>().anchoredPosition.x;
            //m_recipeCardOrigPosY = GetComponent<RectTransform>().anchoredPosition.y;

            //gameObject.RectTransform().anchoredPosition = new Vector2(m_recipeCardOrigPosX+50, m_recipeCardOrigPosY - 40);
        }

        public IEnumerator<float> Play(Command command, VfxParameters vfxParameters)
        {
            throw new System.NotImplementedException();
        }
    }
}