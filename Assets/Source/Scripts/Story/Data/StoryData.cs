using CrossBlitz.Card;

namespace CrossBlitz.Story.Data
{
    /// <summary>
    /// REMOVE THIS!!! USE FableChapterData instead.
    /// </summary>
    [System.Serializable]
    public class StoryData
    {
        public string title;
        public string description;
        public string prologue;
        public string character;
        public Faction faction;
        public string path;
    }
}