using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.Hero;
using CrossBlitz.PlayFab.Authentication;

namespace CrossBlitz.Fables
{
    public static class FableController
    {
        //public static string CurrentChapter = "Redcroft-1-1"; // HERO-BOOK-CHAPTER (FORMAT)
        public static string CurrentBookName;
        public static int CurrentBookNumber = -1;
        public static int CurrentChapterNumber = -1;

        public static void Init()
        {
            CurrentBookName = string.Empty;
            CurrentBookNumber = -1;
            CurrentChapterNumber = -1;
        }

        public static void SetChapter(string bookName, int bookNumber, int chapterNumber)
        {
            CurrentBookName = bookName;
            CurrentBookNumber = bookNumber;
            CurrentChapterNumber = chapterNumber;

            if (!string.IsNullOrEmpty(CurrentBookName))
            {
                App.FableData.OnOpenFable();
                // add an event!
            }
            else
            {
                //!!! don't remove this save, it saves so it can store timer information before I stop it in OnClosedFable!
                App.SaveAll();

                App.FableData.OnClosedFable();
                // add an event!
            }
        }

        public static string GetCurrentChapterId()
        {
            return $"{CurrentBookName}-{CurrentBookNumber}-{CurrentChapterNumber}";
        }

        public static bool PlayerIsInAFable()
        {
            return !string.IsNullOrEmpty(CurrentBookName);
        }

        public static string GetCurrentHeroName()
        {
            return CurrentBookName;
        }

        public static Faction GetCurrentFaction()
        {
            return HeroData.GetFactionForHero(GetCurrentHeroName());
        }
    }
}