
using CrossBlitz.Audio;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.UI.Widgets;
using UnityEngine;

namespace CrossBlitz.Fables
{
    public class SkipBattlesButton : MonoBehaviour
    {
        public ToggleButton skipBattlesButton;


        private void Start()
        {
            skipBattlesButton.Toggle.isOn = App.Settings.GetSkipBattles();
            skipBattlesButton.OnValueChanged += OnSkipBattlesToggled;
        }

        private void OnSkipBattlesToggled(bool isOn, string context)
        {
            if (isOn)
            {
                App.Settings.SetSkipBattles(true);
                AudioController.PlaySound("TMP-UI-ClickGrab");
            }
            else
            {
                App.Settings.SetSkipBattles(false);
                AudioController.PlaySound("TMP-UI-ClickDrop");
            }
        }
    }
}