using System;
using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Grid;
using CrossBlitz.PlayFab.Authentication;
using UnityEngine;

namespace CrossBlitz.Fables.PathfindingAPI
{
    public static class Pathfinding
    {
        public static List<FableTileData> FindPath(FableTileData startTile, FableTileData targetTile, bool checkForNonCompletedTiles=false, bool checkForVisualPaths=false)
        {
            if (App.Settings.GetUnlockAllNodes())
            {
                checkForNonCompletedTiles = false;
                checkForVisualPaths = false;
            }

            if (checkForNonCompletedTiles)
            {
                var startTileClientData = FablesMapController.Instance.GetClientTile(startTile);
                var targetTileClientData = FablesMapController.Instance.GetClientTile(targetTile);

                if (startTileClientData != null && !startTileClientData.Complete && targetTileClientData != null && !targetTileClientData.Complete)
                {
                    return null;
                }

                var chapterData = FablesMapController.Instance.ClientChapterData;

                if (!string.IsNullOrEmpty(targetTile.questId) && chapterData != null && targetTileClientData != null)
                {
                    if (!chapterData.CanShowQuestNode(targetTile.questId, targetTile.questStep) || !targetTileClientData.QuestNodeRevealed)
                    {
                        return null;
                    }
                }
            }

            var openSet = new List<FableTileData>{startTile};
            var closedSet = new HashSet<FableTileData>();

            while (openSet.Count > 0)
            {
                var currentNode = openSet[0];

                for (var i = 1; i < openSet.Count; i++)
                {
                    if (openSet[i].FCost < currentNode.FCost ||
                        openSet[i].FCost==currentNode.FCost &&
                        openSet[i].hCost < currentNode.hCost)
                    {
                        currentNode = openSet[i];
                    }
                }

                openSet.Remove(currentNode);
                closedSet.Add(currentNode);

                if (currentNode == targetTile)
                {
                    // found path!
                    return RetracePath(startTile,targetTile);
                }

                var neighbors = HexGrid2D.GetNeighbors(currentNode.hex);

                for (var i = 0; i < neighbors.Count; i++)
                {
                    var neighbor = neighbors[i];

                    if (neighbor.NodeType == NodeType.Path)
                    {
                        if (!neighbor.connectingPathRooms.Contains(currentNode.uid))
                        {
                            continue;
                        }
                    }

                    if (checkForNonCompletedTiles && neighbor != targetTile && neighbor.NodeType != NodeType.Path)
                    {
                        var clientTileData = FablesMapController.Instance.GetClientTile(neighbor);

                        if (clientTileData != null && !clientTileData.Complete)
                        {
                            continue;
                        }
                    }

                    if (checkForVisualPaths)
                    {
                        if (neighbor != targetTile && neighbor.NodeType != NodeType.Path)
                        {
                            var clientTileData = FablesMapController.Instance.GetClientTile(neighbor);

                            if (clientTileData != null && !clientTileData.Complete)
                            {
                                continue;
                            }
                        }
                        else if (neighbor.NodeType == NodeType.Path)
                        {
                            var clientTileData = FablesMapController.Instance.GetClientTile(neighbor);

                            if (clientTileData != null && !clientTileData.Complete)
                            {
                                continue;
                            }
                        }
                    }

                    if (!neighbor.IsWalkable() || closedSet.Contains(neighbor))
                    {
                        continue;
                    }

                    var newMovementCostToNeighbor = currentNode.gCost + HexGrid2D.GetDistance(currentNode.hex,neighbor.hex);

                    if (newMovementCostToNeighbor < neighbor.gCost || !openSet.Contains(neighbor))
                    {
                        neighbor.gCost = newMovementCostToNeighbor;
                        neighbor.hCost = HexGrid2D.GetDistance(neighbor.hex, targetTile.hex);
                        neighbor.parentUid = currentNode.uid;

                        if (!openSet.Contains(neighbor))
                        {
                            openSet.Add(neighbor);
                        }
                    }
                }
            }

            return null;
        }

        private static List<FableTileData> RetracePath(FableTileData startTile, FableTileData targetTile)
        {
            var path = new List<FableTileData>();
            var currentNode = targetTile;

            while (currentNode != null && currentNode != startTile)
            {
                path.Add(currentNode);
                currentNode = HexGrid2D.GetTile( currentNode.parentUid )?.TileData;
            }

            path.Reverse();
            return path;
        }
    }
}