using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Cutscene;
using CrossBlitz.Fables.Cutscene.Data;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.Fables.EventDialogueAPI
{
    public class EventDialogueContainer : MonoBehaviour
    {
        // public CanvasGroup dialogueCanvas;
        // public CutsceneDialogueBox dialogueBox;
        //
        // private CutsceneData m_cutsceneData;
        // private CutsceneDialogueInfo _currentDialogueInfo;
        // private EventStage _stage;
        //
        // public IEnumerator<float> AnimateEventStart(CutsceneData cutsceneData, EventStage stage)
        // {
        //     m_cutsceneData = cutsceneData;
        //     _stage = stage;
        //
        //     // dialogueCanvas.alpha = 0;
        //     // dialogueCanvas.DOFade(1, 0.25f);
        //     //
        //     // dialogueBox.SetActive(true);
        //     // dialogueBox.InitDialogueBox(cutsceneData);
        //
        //     yield return Timing. WaitForSeconds(0.5f);
        // }
        //
        // public IEnumerator<float> AnimateEventEnd()
        // {
        //     // dialogueCanvas.DOFade(0, 0.5f);
        //     //
        //     // yield return Timing.WaitUntilDone(dialogueBox.AnimateOut().CancelWith(gameObject));
        //     //
        //     // dialogueBox.SetActive(false);
        //     yield break;
        // }
        //
        // public IEnumerator<float> PlayDialogue()
        // {
        //     Timing.RunCoroutine(dialogueBox.AnimateIn(null).CancelWith(gameObject));
        //
        //     while (dialogueBox.IsReady == false) yield return Timing.WaitForOneFrame;
        //
        //     for (var i = 0; i < m_cutsceneData.DialogueInfo.Count; i++)
        //     {
        //         var dialogueInfo = m_cutsceneData.DialogueInfo[i];
        //         var sameCharacterAsLast = _currentDialogueInfo != null && dialogueInfo.CharacterIndex == _currentDialogueInfo.CharacterIndex;
        //
        //         _currentDialogueInfo = dialogueInfo;
        //         _currentDialogueInfo.SameCharacterFromLast = sameCharacterAsLast;
        //         _currentDialogueInfo.IsFirst = i == 0;
        //
        //         var nextDialogue = i < m_cutsceneData.DialogueInfo.Count - 1 ? m_cutsceneData.DialogueInfo[i + 1] : null;
        //         _currentDialogueInfo.NextIsSameCharacter = nextDialogue != null && nextDialogue.CharacterIndex == _currentDialogueInfo.CharacterIndex;
        //
        //         if (_currentDialogueInfo.EmoteIds != null && _currentDialogueInfo.EmoteIds.Count > 0)
        //         {
        //             for (var j = 0; j < _currentDialogueInfo.EmoteIds.Count; j++)
        //             {
        //                 if (_currentDialogueInfo.EmoteIds[j] == "talking")
        //                 {
        //                     _stage.characters[j].RemoveEmote();
        //                 }
        //                 else
        //                 {
        //                     _stage.characters[j].SetEmote(_currentDialogueInfo.EmoteIds[j]);
        //                 }
        //             }
        //         }
        //
        //         for (var j = 0; j < _stage.characters.Count; j++)
        //         {
        //             if (_currentDialogueInfo.CharacterIndex == j)
        //             {
        //                 _stage.characters[j].AnimateStartTalking(_currentDialogueInfo);
        //             }
        //             else
        //             {
        //                 _stage.characters[j].AnimateEndTalking();
        //             }
        //         }
        //
        //         yield return Timing.WaitUntilDone(dialogueBox.ShowText(_currentDialogueInfo).CancelWith(gameObject));
        //
        //         if (_currentDialogueInfo.HasChoices && _currentDialogueInfo.Choices.Count > 1)
        //         {
        //             var characterIndex = 0;
        //
        //             if (_currentDialogueInfo.CharacterIndex <= 1) characterIndex = 3;
        //
        //             if (_currentDialogueInfo.EmoteIds != null && _currentDialogueInfo.EmoteIds.Count > 0)
        //             {
        //                 for (var j = 0; j < _currentDialogueInfo.EmoteIds.Count; j++)
        //                 {
        //                     if (_currentDialogueInfo.EmoteIds[j] == "talking")
        //                     {
        //                         _stage.characters[j].RemoveEmote();
        //                     }
        //                     else if (j == characterIndex)
        //                     {
        //                         _stage.characters[j].SetEmote("confused");
        //                     }
        //                     else
        //                     {
        //                         _stage.characters[j].SetEmote(_currentDialogueInfo.EmoteIds[j]);
        //                     }
        //                 }
        //             }
        //
        //             for (var j = 0; j < _stage.characters.Count; j++)
        //             {
        //                 if (characterIndex == j)
        //                 {
        //                     _stage.characters[j].AnimateStartTalking(_currentDialogueInfo,true);
        //                 }
        //                 else
        //                 {
        //                     _stage.characters[j].AnimateEndTalking();
        //                 }
        //             }
        //
        //             yield return Timing.WaitUntilDone(dialogueBox.ShowChoices(m_cutsceneData, _currentDialogueInfo, characterIndex));
        //
        //             if (dialogueBox.currentChoice > -1)
        //             {
        //                 var choiceDialogue = _currentDialogueInfo.Choices[dialogueBox.currentChoice];
        //
        //                 if (choiceDialogue != null)
        //                 {
        //                     var cutsceneData = Db.CutsceneDatabase.GetCutscene(choiceDialogue.CutsceneUid);
        //                     if(cutsceneData != null)
        //                     {
        //                         m_cutsceneData = cutsceneData;
        //                         yield return Timing.WaitUntilDone(PlayDialogue().CancelWith(gameObject));
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }
        //
        // public void SkipDialogue()
        // {
        //
        // }
    }
}