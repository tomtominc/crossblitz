using System;
using System.Collections.Generic;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.Hero;
using DG.Tweening;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CrossBlitz.Fables.EventDialogueAPI
{
    public class DialogueChoiceContainer : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
    {
        public CanvasGroup canvasGroup;
        public SpriteAnimation playerEmblem;
        public SpriteAnimation highlight;
        public TextMeshProUGUI text;

        private CharacterData _character;
        private CutsceneChoice _choice;
        private int _index;
        private bool _highlightAllowed;
        private RectTransform _rect;

        private Tweener _punchRot;
        private Tweener _punchPos;

        public event Action<int> OnChoiceSelected;

        public void SetChoice(CharacterData characterData, CutsceneChoice choice, int index)
        {
            _character = characterData;
            _choice = choice;
            _index = index;
            _highlightAllowed = false;
            _rect=transform as RectTransform;

            OnDeselect();
        }

        public IEnumerator<float> AnimateIn()
        {
            canvasGroup.alpha = 0;
            var pos = this.RectTransform().anchoredPosition;
            pos = new Vector2(-10f, pos.y);
            this.RectTransform().anchoredPosition = pos;
            this.RectTransform().DOAnchorPosX(0, 0.24f);
            canvasGroup.DOFade(1, 0.24f);
            yield return Timing.WaitForSeconds(0.24f);
            _highlightAllowed = true;
        }

        private void OnDeselect()
        {
            highlight.Play("unselected");
            playerEmblem.SetActive(false);
            text.text = _choice.Choice;
            text.color = "#b29678".ToColor();
        }

        private void OnSelect()
        {
            if (_character == null)
            {
                highlight.Play("fortune");
            }
            else
            {
                playerEmblem.SetActive(true);
                highlight.Play(_character.faction.ToString().ToLower());
                playerEmblem.Play(_character.id.ToLower());
            }

            text.text = _choice.Choice;
            text.color = "#ffffff".ToColor();

            if (_punchRot == null || !_punchRot.IsActive())
            {
                _rect.DOKill();
                _punchRot = _rect.DOPunchRotation(Vector3.back * (UnityEngine.Random.value > 0.5f? 4f:-4), 0.16f);
            }

            if (_punchPos == null || !_punchPos.IsActive())
            {
                playerEmblem.RectTransform().DOKill();
                _punchPos = playerEmblem.RectTransform().DOPunchAnchorPos(Vector3.up * 8f, 0.24f);
            }

            playerEmblem.image.color = new Color(1, 1, 1, 0);
            playerEmblem.image.DOFade(1, 0.16f);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!_highlightAllowed) return;
            OnSelect();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (!_highlightAllowed) return;
            OnDeselect();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (!_highlightAllowed) return;
            OnChoiceSelected?.Invoke(_index);
        }
    }
}