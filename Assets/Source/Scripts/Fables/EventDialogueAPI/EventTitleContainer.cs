using System;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace CrossBlitz.Fables.EventDialogueAPI
{
    public class EventTitleContainer : MonoBehaviour
    {
        public CanvasGroup titleContainerCanvas;
        public RectTransform titleContainerRect;
        public TextMeshProUGUI titleLabel;

        private Vector2 _originalPos;

        private void OnDestroy()
        {
            titleContainerCanvas.DOKill();
            titleContainerRect.DOKill();
        }

        public void AnimateIn(string title)
        {
            gameObject.SetActive(true);

            titleContainerCanvas.alpha = 0;
            titleContainerCanvas.DOFade(1, 0.25f);

            _originalPos = titleContainerRect.anchoredPosition;
            titleContainerRect.anchoredPosition = new Vector2(_originalPos.x, _originalPos.y + 26f);
            titleContainerRect.DOAnchorPosY(_originalPos.y, 0.5f);

            titleLabel.text = title;
        }

        public void AnimateOut()
        {
            titleContainerRect.DOAnchorPosY(_originalPos.y+ 26f, 0.5f).SetEase(Ease.InBack);
            titleContainerCanvas.DOFade(0, 0.5f)
                .OnComplete(() =>
                {
                    gameObject.SetActive(false);
                });
        }
    }
}