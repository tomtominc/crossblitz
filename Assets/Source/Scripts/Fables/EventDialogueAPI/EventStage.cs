using System;
using System.Collections;
using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.Dialogue;
using CrossBlitz.Fables.Cutscene;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.Fables.Grid;
using CrossBlitz.Items.Data;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ViewAPI.Popups;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.Fables.EventDialogueAPI
{
    public class EventStage : DialogueStage
    {
        public CanvasGroup canvas;
        public CanvasGroup dialogueCanvas;
        public CutsceneDialogueBox dialogueBox;
        public NarratorDialogueBox narratorDialogueBox;
        public IllustrationDialogueBox illustrationDialogueBox;
        public RectTransform stageRect;
        public SpriteAnimation stageAnimator;
        public EventCharacter rightCharacter;
        public EventCharacter leftCharacter;
        public EventCharacter emptyCharacter;

        private int m_currentDialogueIndex;
        private List<CutsceneDialogueInfo> m_currentDialogues;
        private string m_selectedChoiceDialogueUid;
        private bool m_skippable;
        

        public int dialogueIndex = 0;
        public int choiceDialogueIndex = 0;
        public int skipToDialogueIndex = 0;
        public bool madeChoice = false;

        public string SelectedChoiceDialogueUid => m_selectedChoiceDialogueUid;

        public virtual IEnumerator<float> AnimateEventStart(CutsceneData cutsceneData)
        {
            m_cutsceneData = cutsceneData;

            UpdateQuest();
            SetupCharacters();

            canvas.alpha = 0;
            canvas.DOFade(1, 0.25f);

            var stagePos = stageRect.anchoredPosition;
            stageRect.anchoredPosition = new Vector2(stagePos.x, stagePos.y - 26f);

            if (!stageAnimator.Play(string.IsNullOrEmpty(cutsceneData.EventStageType) ? "sand" : cutsceneData.EventStageType))
            {
                stageAnimator.Play("sand");
            }

            dialogueCanvas.SetActive(true);
            dialogueCanvas.alpha = 0;
            dialogueCanvas.DOFade(1, 0.25f);

            dialogueBox.SetActive(true);
            dialogueBox.InitDialogueBox(m_cutsceneData);

            narratorDialogueBox.SetActive(false);
            narratorDialogueBox.InitDialogueBox(m_cutsceneData);

            illustrationDialogueBox.SetActive(false);
            illustrationDialogueBox.InitDialogueBox(m_cutsceneData);

            stageRect.DOAnchorPosY(stagePos.y, 0.5f);

            yield return Timing.WaitForSeconds( 0.5f);
            yield return Timing.WaitUntilDone(AnimateInitialCharactersIn().CancelWith(gameObject), "cutscene");
        }

        public bool CanSkip()
        {
            return m_skippable;
        }

        public override void OnEnd()
        {
            if (narratorDialogueBox) narratorDialogueBox.SetActive(false);
            if (illustrationDialogueBox) illustrationDialogueBox.SetActive(false);
        }

        private void UpdateQuest()
        {
            if (FablesMapController.Instance && FablesMapController.Instance.ClientChapterData != null)
            {
                var chapter = FablesMapController.Instance.ClientChapterData;
                if (chapter == null) chapter = App.FableData.GetCurrentChapter();
                if (chapter != null) chapter.ResolveQuestIfApplicable(m_cutsceneData);
            }
        }

        private void OnDestroy()
        {
            if (canvas) canvas.DOKill();
            if (dialogueCanvas) dialogueCanvas.DOKill();
            if (stageRect) stageRect.DOKill();
        }

        public bool IsOnChoiceDialogue()
        {
            if (m_currentDialogues != null && m_currentDialogueIndex < m_currentDialogues.Count)
            {
                return m_currentDialogues[m_currentDialogueIndex].HasChoices;
            }

            return false;
        }

        public bool TryGetChoiceDialogue(out List<CutsceneDialogueInfo> choiceDialogue)
        {
            var hasChoices = false;
            choiceDialogue = new List<CutsceneDialogueInfo>();

            if (m_currentDialogues != null)
            {
                for (var i = m_currentDialogueIndex; i < m_currentDialogues.Count; i++)
                {
                    var dialogue = m_currentDialogues[i];

                    if (dialogue.HasChoices)
                    {
                        hasChoices = true;
                        choiceDialogue.Add(dialogue);
                        choiceDialogueIndex = i;
                        break;
                    }
                }
            }

            return hasChoices;
        }

        public override void OnSkipToChoices()
        {
            m_skippable = false;

            dialogueIndex = choiceDialogueIndex - 1;
            m_currentDialogueIndex = dialogueIndex;
            skipToDialogueIndex = 0;
 
            dialogueBox.IsReady = true;
            narratorDialogueBox.IsReady = true;
            illustrationDialogueBox.IsReady = true;

            dialogueBox.OnSkippedToChoice();
            narratorDialogueBox.OnSkippedToChoice();
            illustrationDialogueBox.OnSkippedToChoice();

            Timing.RunCoroutine(illustrationDialogueBox.SimplyFadeOut().CancelWith(illustrationDialogueBox), "cutscene");
            Timing.RunCoroutine(narratorDialogueBox.SimplyFadeOut().CancelWith(narratorDialogueBox), "cutscene");
        }

        public void OnSkipped()
        {
            m_skippable = false;

            dialogueIndex = m_currentDialogues.Count;
            skipToDialogueIndex = dialogueIndex;

            dialogueBox.IsReady = true;
            narratorDialogueBox.IsReady = true;
            illustrationDialogueBox.IsReady = true;

            dialogueBox.OnSkipped();
            narratorDialogueBox.OnSkipped();
            illustrationDialogueBox.OnSkipped();

            Timing.RunCoroutine(illustrationDialogueBox.SimplyFadeOut().CancelWith(illustrationDialogueBox), "cutscene");
            Timing.RunCoroutine(narratorDialogueBox.SimplyFadeOut().CancelWith(narratorDialogueBox), "cutscene");
        }

        protected override void SetupCharacters()
        {
            if (m_cutsceneData.RightSideCharacters?.Count > 0 && m_cutsceneData?.DialogueInfo?.Count > 0)
            {
                CutsceneDialogueInfo initialRightCharacterDialogue=null;

                for (var i = 0; i < m_cutsceneData.DialogueInfo.Count; i++)
                {
                    if (m_cutsceneData.DialogueInfo[i].CharacterSide == CharacterSide.Right  &&
                        m_cutsceneData.RightSideCharacters[m_cutsceneData.DialogueInfo[i].CharacterIndex] != DialogueInfo.Narrator &&
                        m_cutsceneData.RightSideCharacters[m_cutsceneData.DialogueInfo[i].CharacterIndex] != DialogueInfo.Illustration)
                    {
                        initialRightCharacterDialogue = m_cutsceneData.DialogueInfo[i];
                        break;
                    }
                }

                if (initialRightCharacterDialogue!=null)
                {
                    rightCharacter.Init();
                    rightCharacter.SetCharacterData(
                        m_cutsceneData.RightSideCharacters[initialRightCharacterDialogue.CharacterIndex],
                        m_cutsceneData.RightCharacterOverrideNames[initialRightCharacterDialogue.CharacterIndex],
                        initialRightCharacterDialogue.CharacterSide);

                    rightCharacter.canvas.alpha = 0;
                }
                else
                {
                    rightCharacter.SetActive(false);
                }
            }

            if (m_cutsceneData.LeftSideCharacters?.Count > 0 && m_cutsceneData?.DialogueInfo?.Count > 0)
            {
                CutsceneDialogueInfo initialLeftCharacterDialogue=null;

                for (var i = 0; i < m_cutsceneData.DialogueInfo.Count; i++)
                {
                    if (m_cutsceneData.DialogueInfo[i].CharacterSide == CharacterSide.Left &&
                        m_cutsceneData.LeftSideCharacters[m_cutsceneData.DialogueInfo[i].CharacterIndex] != DialogueInfo.Narrator &&
                        m_cutsceneData.LeftSideCharacters[m_cutsceneData.DialogueInfo[i].CharacterIndex] != DialogueInfo.Illustration)
                    {
                        initialLeftCharacterDialogue = m_cutsceneData.DialogueInfo[i];
                        break;
                    }
                }

                if (initialLeftCharacterDialogue!=null)
                {
                    leftCharacter.Init();
                    leftCharacter.SetCharacterData(
                        m_cutsceneData.LeftSideCharacters[initialLeftCharacterDialogue.CharacterIndex],
                        m_cutsceneData.LeftCharacterOverrideNames[initialLeftCharacterDialogue.CharacterIndex],
                        initialLeftCharacterDialogue.CharacterSide);

                    leftCharacter.canvas.alpha = 0;
                }
                else
                {
                    leftCharacter.SetActive(false);
                }
            }
        }

        private IEnumerator<float> AnimateInitialCharactersIn()
        {
            if (leftCharacter.IsValid())
            {
                Timing.RunCoroutine(leftCharacter.AnimateIn().CancelWith(gameObject), "cutscene");
                yield return Timing.WaitForSeconds(0.12f);
            }

            if (rightCharacter.IsValid())
            {
                Timing.RunCoroutine(rightCharacter.AnimateIn().CancelWith(gameObject), "cutscene");
                yield return Timing.WaitForSeconds(0.12f);
            }
        }

        public IEnumerator<float> PlayDialogue(List<CutsceneDialogueInfo> dialogues)
        {
            Timing.RunCoroutine(dialogueBox.AnimateIn(null).CancelWith(gameObject), "cutscene");

            while (dialogueBox.IsReady == false)
            {
                yield return Timing.WaitForOneFrame;
            }

            m_currentDialogues = dialogues;
            m_skippable = true;
            //m_currentDialogueIndex = 0;

            m_currentDialogueIndex = skipToDialogueIndex;

            for (dialogueIndex = skipToDialogueIndex; dialogueIndex < m_currentDialogues.Count; dialogueIndex++)
            {
                m_currentDialogueIndex = dialogueIndex;
                var curr = m_currentDialogues[m_currentDialogueIndex];
                var currentCharacterValue = curr.GetCharacterValues(m_cutsceneData);

                if (dialogues.Count > dialogueIndex + 1)
                {
                    curr.NextIsSameCharacter = dialogues[dialogueIndex + 1].GetCharacterValues(m_cutsceneData).id == currentCharacterValue.id;
                }

                if (currentCharacterValue.id == DialogueInfo.Empty)
                {
                    if (currentCharacterValue.side == CharacterSide.Right)
                    {
                        emptyCharacter = rightCharacter;
                    }
                    else if (currentCharacterValue.side == CharacterSide.Left)
                    {
                        emptyCharacter = leftCharacter;
                    }

                    if (emptyCharacter)
                    {
                        if (dialogues[dialogueIndex].Text.Contains("slow-exit"))
                        {
                            yield return Timing.WaitUntilDone(emptyCharacter.AnimateOutRoutineSlow().CancelWith(emptyCharacter), "cutscene");
                        }
                        else
                        {
                            yield return Timing.WaitUntilDone(emptyCharacter.AnimateOutRoutine().CancelWith(emptyCharacter), "cutscene");
                        }


                        emptyCharacter.SetCharacterData(currentCharacterValue.id, currentCharacterValue.overrideName, currentCharacterValue.side);

                        if (emptyCharacter.nameContainer && emptyCharacter.nameContainer.GetCurrentAnimatorStateInfo(0).IsName("Open"))
                        {
                            emptyCharacter.nameContainer.Play("Close");
                        }

                        if (emptyCharacter.speechTail)
                        {
                            emptyCharacter.speechTail.SetActive(false);
                        }
                    }

                    continue;
                }

                if (currentCharacterValue.id == DialogueInfo.Narrator)
                {
                    dialogueBox.boxArrow.SetActive(false);
                    dialogueBox.dialogue.ShowText(string.Empty);
                    dialogueBox.IsReady = false;

                    narratorDialogueBox.SetActive(true);
                    yield return Timing.WaitUntilDone(narratorDialogueBox.ShowText(curr).CancelWith(narratorDialogueBox), "cutscene");

                    if (!curr.NextIsSameCharacter)
                    {
                        yield return Timing.WaitUntilDone(narratorDialogueBox.AnimateOut().CancelWith(narratorDialogueBox), "cutscene");
                        narratorDialogueBox.SetActive(false);

                    }
                    continue;
                }

                if (currentCharacterValue.id == DialogueInfo.Illustration)
                {
                    dialogueBox.boxArrow.SetActive(false);
                    dialogueBox.dialogue.ShowText(string.Empty);
                    dialogueBox.IsReady = false;

                    illustrationDialogueBox.SetActive(true);
                    yield return Timing.WaitUntilDone(illustrationDialogueBox.ShowText(curr).CancelWith(illustrationDialogueBox), "cutscene");

                    if (!curr.NextIsSameCharacter)
                    {
                        yield return Timing.WaitUntilDone(illustrationDialogueBox.AnimateOut().CancelWith(illustrationDialogueBox), "cutscene");
                        illustrationDialogueBox.SetActive(false);

                    }
                    continue;
                }

                
                // Slide the Player Character onto the scene if they currently are not present for a choice
                if (curr.HasChoices && curr.Choices.Count > 1 && m_currentDialogueIndex > 0 && !madeChoice)
                {
                    var prevDialogue = m_currentDialogues[m_currentDialogueIndex - 1];
                    var prevCharacterValue = prevDialogue.GetCharacterValues(m_cutsceneData);

                    for (var i = 1; prevCharacterValue.id == currentCharacterValue.id; i++)
                    {
                        prevDialogue = m_currentDialogues[m_currentDialogueIndex - i];
                        prevCharacterValue = prevDialogue.GetCharacterValues(m_cutsceneData);
                    }

                    if (leftCharacter.CharacterData.id != prevCharacterValue.id)
                    {
                        yield return Timing.WaitUntilDone(leftCharacter.AnimateOutRoutine().CancelWith(leftCharacter), "cutscene");
                        leftCharacter.SetCharacterData(prevCharacterValue.id, prevCharacterValue.overrideName, CharacterSide.Left);

                        var characterIndex = m_cutsceneData.LeftSideCharacters.IndexOf(leftCharacter.CharacterData.id);
                        var leftSideEmote = curr.LeftSideEmotes[characterIndex];
                        leftCharacter.SetEmote(leftSideEmote);

                        yield return Timing.WaitUntilDone(leftCharacter.AnimateIn().CancelWith(leftCharacter), "cutscene");
                    }
                }

                curr.IsFirst = dialogueIndex == 0;
                curr.SameCharacterFromLast = false;

                if (currentCharacterValue.side == CharacterSide.Right && rightCharacter.IsValid() && rightCharacter.CharacterData.id != currentCharacterValue.id)
                {
                    yield return Timing.WaitUntilDone(rightCharacter.AnimateOutRoutine().CancelWith(rightCharacter), "cutscene");
                    rightCharacter.SetCharacterData(currentCharacterValue.id, currentCharacterValue.overrideName, currentCharacterValue.side);
                    yield return Timing.WaitUntilDone(rightCharacter.AnimateIn().CancelWith(rightCharacter), "cutscene");
                }
                else if (currentCharacterValue.side == CharacterSide.Left && leftCharacter.IsValid() && leftCharacter.CharacterData.id != currentCharacterValue.id)
                {
                    yield return Timing.WaitUntilDone(leftCharacter.AnimateOutRoutine().CancelWith(leftCharacter), "cutscene");
                    leftCharacter.SetCharacterData(currentCharacterValue.id, currentCharacterValue.overrideName, currentCharacterValue.side);
                    yield return Timing.WaitUntilDone(leftCharacter.AnimateIn().CancelWith(leftCharacter), "cutscene");
                }
                else
                {
                    curr.SameCharacterFromLast = true;
                }

                if (rightCharacter.IsValid())
                {
                    var characterIndex = m_cutsceneData.RightSideCharacters.IndexOf(rightCharacter.CharacterData.id);
                    var rightSideEmote = curr.RightSideEmotes[characterIndex];
                    if (rightSideEmote == "talking") rightCharacter.RemoveEmote();
                    else rightCharacter.SetEmote(rightSideEmote);
                }

                if (leftCharacter.IsValid())
                {
                    var characterIndex = m_cutsceneData.LeftSideCharacters.IndexOf(leftCharacter.CharacterData.id);
                    var leftSideEmote = curr.LeftSideEmotes[characterIndex];
                    if (leftSideEmote == "talking") leftCharacter.RemoveEmote();
                    else leftCharacter.SetEmote(leftSideEmote);
                }

                if (currentCharacterValue.side == CharacterSide.Right)
                {
                    rightCharacter.AnimateStartTalking(curr, curr.UsesThoughtBubble);
                    leftCharacter.AnimateEndTalking();
                }
                else if (currentCharacterValue.side == CharacterSide.Left)
                {
                    rightCharacter.AnimateEndTalking();
                    leftCharacter.AnimateStartTalking(curr, curr.UsesThoughtBubble);
                }

                yield return Timing.WaitUntilDone(dialogueBox.ShowText(curr).CancelWith(gameObject), "cutscene");

                if (curr.HasChoices && curr.Choices.Count > 1 && !madeChoice)
                {
                    m_skippable = false;
                    madeChoice = true;

                    var characterToAnswerId = currentCharacterValue.side == CharacterSide.Right
                        ? leftCharacter.CharacterData?.id
                        : rightCharacter.CharacterData?.id;

                        // ? m_cutsceneData.LeftSideCharacters
                        //     [m_cutsceneData.LeftSideCharacters.IndexOf(leftCharacter.CharacterData.id)]
                        // : m_cutsceneData.RightSideCharacters[
                        //     m_cutsceneData.RightSideCharacters.IndexOf(rightCharacter.CharacterData.id)];

                    if (currentCharacterValue.side == CharacterSide.Left)
                    {
                        rightCharacter.SetEmote("confused");
                        leftCharacter.RemoveEmote();
                    }
                    else if (currentCharacterValue.side == CharacterSide.Right)
                    {
                        leftCharacter.SetEmote("confused");
                        rightCharacter.RemoveEmote();
                    }

                    if (currentCharacterValue.side == CharacterSide.Left)
                    {
                        rightCharacter.AnimateStartTalking(curr,true);
                        leftCharacter.AnimateEndTalking();
                    }
                    else if (currentCharacterValue.side == CharacterSide.Right)
                    {
                        leftCharacter.AnimateStartTalking(curr,true);
                        rightCharacter.AnimateEndTalking();
                    }

                    yield return Timing.WaitUntilDone(dialogueBox.ShowChoices(m_cutsceneData, curr, characterToAnswerId), "cutscene");

                    if (dialogueBox.currentChoice > -1)
                    {
                        var choiceDialogue = curr.Choices[dialogueBox.currentChoice];

                        if (choiceDialogue != null)
                        {
                            var cutsceneData = Db.CutsceneDatabase.GetCutscene(choiceDialogue.CutsceneUid);

                            if (cutsceneData != null)
                            {
                                m_skippable = true;

                                m_cutsceneData = cutsceneData;
                                m_selectedChoiceDialogueUid = m_cutsceneData.Uid;
                                skipToDialogueIndex = 0;
                                UpdateQuest();

                                yield return Timing.WaitUntilDone(PlayDialogue(cutsceneData.DialogueInfo).CancelWith(gameObject), "cutscene");
                            }
                        }
                    }
                }
            }
        }

        public virtual IEnumerator<float> AnimateEventEnd()
        {
            canvas.DOFade(0, 0.5f);

            dialogueCanvas.DOFade(0, 0.5f);
            Timing.RunCoroutine(dialogueBox.AnimateOut().CancelWith(gameObject), "cutscene");

            var stagePos = stageRect.anchoredPosition;
            stageRect.DOAnchorPosY(stagePos.y - 26f, 0.5f);
            yield return Timing.WaitForSeconds(0.5f);

            dialogueBox.SetActive(false);
        }
    }
}