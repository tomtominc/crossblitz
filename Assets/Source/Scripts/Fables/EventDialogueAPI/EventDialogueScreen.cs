using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Audio;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.InputAPI;
using CrossBlitz.Items.Data;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ViewAPI.Popups;
using DG.Tweening;
using MEC;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Fables.EventDialogueAPI
{
    public class EventDialogueScreen : InputMode, ISceneConfiguration
    {
        public CanvasGroup background;
        public EventTitleContainer titleContainer;
        public Button skipButton;

        public EventStage stage;
        private CutsceneData _cutsceneData;
        private bool _skipped;
        private CoroutineHandle _cutsceneHandle;
        private bool _cutsceneFinished;
        public bool CutsceneFinished => _cutsceneFinished;

        private List<CutsceneDialogueInfo> m_skipToChoicesDialogue;
        public event Action OnFinished;

        private void Start()
        {
            skipButton.onClick.AddListener(OnSkipped);
        }

        public override void StartMode()
        {

        }

        public override void UpdateMode()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                OnSkipped();
            }

            if (stage != null && stage.CanSkip())
            {
                skipButton.SetActive(true);
            }
            else
            {
                skipButton.SetActive(false);
            }
        }

        public override void EndMode()
        {
            stage.OnEnd();
        }

        public void OnSkipped()
        {
            if (!_skipped && _cutsceneHandle.IsRunning && stage.CanSkip())
            {
                if (stage.IsOnChoiceDialogue())
                {
                    return;
                }

                if (stage.TryGetChoiceDialogue(out var choiceDialogue))
                {
                    //Timing.KillCoroutines("cutscene");
                    m_skipToChoicesDialogue = choiceDialogue;
                    stage.OnSkipToChoices();
                    return;
                }

                _skipped = true;
                //_cutsceneFinished = true;
                //Timing.KillCoroutines("cutscene");

                skipButton.SetActive(false);
                stage.OnSkipped();
            }
        }

        public void StartEvent(CutsceneData cutsceneData)
        {
            _cutsceneData = cutsceneData;
            skipButton.SetActive(false);

            if (!string.IsNullOrEmpty(_cutsceneData.StartingTrack))
            {
                AudioController.PlaySong(_cutsceneData.StartingTrack);
            }

            if (!string.IsNullOrEmpty(_cutsceneData.StartingAmbient))
            {
                AmbientController.PlayAmbient(_cutsceneData.StartingAmbient);
            }

            _cutsceneHandle=Timing.RunCoroutine(AnimateEventStart().CancelWith(gameObject));
            skipButton.SetActive(true);
        }

        private IEnumerator<float> AnimateEventStart()
        {
            stage.SetActive(true);

            background.SetActive(true);
            background.alpha = 0;
            background.DOFade(1, 0.25f);

            titleContainer.AnimateIn(_cutsceneData.EventTitle);

            yield return Timing.WaitUntilDone(stage.AnimateEventStart(_cutsceneData).CancelWith(gameObject),"cutscene");

            if (!_skipped)
            {
                skipButton.SetActive(true);
                yield return Timing.WaitUntilDone(stage.PlayDialogue(_cutsceneData.DialogueInfo).CancelWith(gameObject), "cutscene");

                if (m_skipToChoicesDialogue != null && m_skipToChoicesDialogue.Count > 0 && !stage.madeChoice)
                {
                    yield return Timing.WaitUntilDone(stage.PlayDialogue(m_skipToChoicesDialogue).CancelWith(gameObject),"cutscene");
                }
            }

            // var itemGrants = new List<ItemValue>();
            //
            // for (var i = 0; i < _cutsceneData.DialogueInfo.Count; i++)
            // {
            //     var dialogue = _cutsceneData.DialogueInfo[i];
            //
            //     if (dialogue.RewardItems != null && dialogue.RewardItems.Count > 0)
            //     {
            //         itemGrants.AddRange(dialogue.RewardItems);
            //     }
            //
            //     if (dialogue.HasChoices)
            //     {
            //         var choice = dialogue.Choices.Find(c => c.CutsceneUid == stage.SelectedChoiceDialogueUid);
            //
            //         if (choice != null)
            //         {
            //             var choiceCutscene = Db.CutsceneDatabase.GetCutscene(choice.CutsceneUid);
            //
            //             for (var j = 0; j < choiceCutscene.DialogueInfo.Count; j++)
            //             {
            //                 var choiceDialogue = choiceCutscene.DialogueInfo[j];
            //
            //                 if (choiceDialogue.RewardItems != null && choiceDialogue.RewardItems.Count > 0)
            //                 {
            //                     itemGrants.AddRange(choiceDialogue.RewardItems);
            //                 }
            //             }
            //         }
            //     }
            // }

            if (_cutsceneData.RewardItems?.Count > 0)
            {
                var result = App.Inventory.GrantItems(_cutsceneData.RewardItems);
                App.HandleItemGrant(result,null, PurchaseErrorCode.NONE, false);
                while (PopupController.IsOpen) yield return Timing.WaitForOneFrame;
            }

            //if (!_skipped)
           // {
                background.DOFade(0, 0.5f);
                titleContainer.AnimateOut();

                yield return Timing.WaitUntilDone(stage.AnimateEventEnd().CancelWith(gameObject), "cutscene");
           // }


            yield return Timing.WaitForSeconds(0.1f);

            background.SetActive(false);
            stage.SetActive(false);

            if (FablesInput.Instance)
            {
                FablesInput.Instance.GoToMode(FablesInput.Mode.Explore);
            }

            _cutsceneFinished = true;
            OnFinished?.Invoke();
        }

        public Task OnCreated(SceneModel sceneModel)
        {
            return Task.CompletedTask;
        }

        public void OnFocused(bool hasFocus)
        {

        }

        public void OnSceneLoaded(SceneModel model)
        {

        }

        public void OnSceneUnLoaded(SceneModel model)
        {

        }
    }
}