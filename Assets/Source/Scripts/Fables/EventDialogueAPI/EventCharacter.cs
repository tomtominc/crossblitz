using System.Collections.Generic;
using CrossBlitz.AssetManagement;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Dialogue;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.Hero;
using DG.Tweening;
using MEC;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Fables.EventDialogueAPI
{
    public class EventCharacter : MonoBehaviour
    {
        public ShakeContainer shake;
        public CanvasGroup canvas;
        public Image characterPortrait;
        public RectTransform characterPortraitRect;
        public SpriteAnimation shadow;

        public CanvasGroup emoteBubble;
        public SpriteAnimation emoteAnimator;

        public Animator nameContainer;
        public SpriteAnimation speechTail;
        public TextMeshProUGUI nameLabel;

        protected CharacterData _characterData;
        public CharacterData CharacterData => _characterData;

        protected float _originalEmoteWindowPositionY;
        protected string m_characterName;

        protected RectTransform m_cachedRect;
        protected Vector2 m_originalPosition;
        protected Vector2 m_outLocation;
        protected Vector2 m_sizeDelta;

        protected CharacterSide m_characterSide;

        private void Start()
        {
            Events.Subscribe(EventType.OnShakeCharacter, OnShakeCharacter);
            Events.Subscribe(EventType.OnBounceCharacter, OnBounceCharacter);
        }

        protected virtual void OnDestroy()
        {
            if (m_cachedRect) m_cachedRect.DOKill();
            if (canvas) canvas.DOKill();
            if (characterPortraitRect) characterPortraitRect.DOKill();
            if (emoteBubble)
            {
                emoteBubble.DOKill();
                emoteBubble.RectTransform().DOKill();
            }

            Events.Unsubscribe(EventType.OnShakeCharacter, OnShakeCharacter);
            Events.Unsubscribe(EventType.OnBounceCharacter, OnBounceCharacter);
        }

        public virtual bool IsValid()
        {
            return this.IsActive() && _characterData != null;
        }

        public virtual void Init()
        {
            m_cachedRect = (RectTransform)transform;
            m_originalPosition = m_cachedRect.anchoredPosition;
            m_sizeDelta = m_cachedRect.sizeDelta;
        }

        public virtual void SetCharacterData(string characterId, string overrideName, CharacterSide characterSide)
        {
            if (string.IsNullOrEmpty(characterId))
            {
                gameObject.SetActive(false);
                return;
            }

            m_characterSide = characterSide;
            _characterData = Db.CharacterDatabase.GetCharacter(characterId);

            if (_characterData == null)
            {
                Debug.LogError($"Could not find character with Id {characterId}.");
                gameObject.SetActive(false);
                return;
            }

            if (shadow)
            {
                if (m_characterSide == CharacterSide.Left)
                {
                    shadow.RectTransform().anchoredPosition =
                        _characterData.eventDialogueShadowOffset;
                }
                else
                {
                    shadow.RectTransform().anchoredPosition =
                        new Vector2(-_characterData.eventDialogueShadowOffset.x, _characterData.eventDialogueShadowOffset.y);
                }

                shadow.Play(_characterData.eventDialogueShadowType);
            }

            if (_characterData.name != "empty")
            {
                m_characterName = !string.IsNullOrEmpty(overrideName) ? overrideName : _characterData.name;
                if (nameLabel) nameLabel.text = m_characterName;
            }

            gameObject.SetActive(true);

            if (emoteBubble)
            {
                emoteBubble.alpha = 0;
                _originalEmoteWindowPositionY = emoteBubble.RectTransform().anchoredPosition.y;
            }

            LoadCharacterPortrait();
        }

        public virtual IEnumerator<float> AnimateIn()
        {
            canvas.alpha = 0;
            canvas.DOFade(1, 0.24f);

            m_cachedRect.anchoredPosition = m_outLocation;
            m_cachedRect.DOAnchorPos(m_originalPosition, 0.24f).SetEase(Ease.OutBack);
            yield return Timing.WaitForSeconds(0.24f);
        }

        public virtual IEnumerator<float> AnimateOutRoutine()
        {
            m_cachedRect.DOAnchorPos(m_outLocation, 0.24f).SetEase(Ease.InBack);
            canvas.DOFade(0, 0.24f);
            yield return Timing.WaitForSeconds(0.24f);
        }

        public IEnumerator<float> AnimateOutRoutineSlow()
        {
            canvas.DOFade(0, 0.75f);
            yield return Timing.WaitForSeconds(0.75f);

            m_cachedRect.DOAnchorPos(m_outLocation, 0.24f).SetEase(Ease.InBack);
            yield return Timing.WaitForSeconds(0.5f);
        }

        protected virtual async void LoadCharacterPortrait()
        {
            var atlas = await AddressableReferenceLoader.Load<SpriteAtlas>("cards-atlas");

            characterPortrait.sprite = atlas.GetSprite(_characterData.portraitUrl);

            characterPortraitRect.anchoredPosition = m_characterSide == CharacterSide.Right ?
                new Vector2(-_characterData.eventDialogueOffset.x, _characterData.eventDialogueOffset.y) :
                _characterData.eventDialogueOffset;

            characterPortraitRect.localScale = new Vector3(GetFacingDir(), 1,1);

            m_outLocation = m_characterSide == CharacterSide.Right
                ? new Vector2(m_cachedRect.sizeDelta.x, m_originalPosition.y)
                : new Vector2(-m_cachedRect.sizeDelta.x, m_originalPosition.y);
        }

        public virtual void AnimateStartTalking(DialogueInfo dialogue, bool questionTail)
        {
            nameLabel.text = m_characterName;

            Sequence punchSeq = DOTween.Sequence();
            punchSeq.Append(characterPortraitRect.DOScale(new Vector3(1.06f * GetFacingDir(), 0.97f), 0.08f));
            punchSeq.AppendCallback(() => { OnIsTalking(dialogue,questionTail); });
            punchSeq.Append(characterPortraitRect.DOScale(new Vector3(0.97f * GetFacingDir(), 1.06f), 0.04f));
            punchSeq.Append(characterPortraitRect.DOScale(new Vector3(1f * GetFacingDir(), 1f), 0.04f));

        }

        protected int GetFacingDir()
        {
            if (_characterData == null) return 1;
            if (m_characterSide == CharacterSide.Right) return _characterData.rightFacing ? 1 : -1;
            return _characterData.rightFacing ? -1 : 1;
        }

        private void OnIsTalking(DialogueInfo dialogue, bool questionTail=false)
        {
            if (!nameContainer.GetCurrentAnimatorStateInfo(0).IsName("Open"))
            {
                nameContainer.Play("Open");
            }

            speechTail.SetActive(true);
            speechTail.Play(questionTail ? "question" : "default");
        }

        public virtual void AnimateEndTalking()
        {
            if (nameContainer.GetCurrentAnimatorStateInfo(0).IsName("Open"))
            {
                nameContainer.Play("Close");
            }

            speechTail.SetActive(false);
        }


        public virtual void SetEmote(string emoteAnim)
        {
            if (!emoteAnimator.Play(emoteAnim))
            {
                RemoveEmote();
                return;
            }

            if (emoteBubble.alpha <= 0)
            {
                emoteBubble.RectTransform().anchoredPosition = new Vector2(emoteBubble.RectTransform().anchoredPosition.x,_originalEmoteWindowPositionY-2f);
                emoteBubble.DOFade(1, 0.25f);
                emoteBubble.RectTransform().DOAnchorPosY(_originalEmoteWindowPositionY, 0.25f);
            }
        }

        public virtual void RemoveEmote()
        {
            emoteBubble.DOFade(0, 0.25f);
            emoteBubble.RectTransform().DOAnchorPosY(_originalEmoteWindowPositionY-2f, 0.25f);
        }

        public virtual void OnShakeCharacter(IMessage message)
        {
            if (CharacterData == null) return;

            if (message.Data is OnShakeCharacterEventArgs args)
            {
                if (string.IsNullOrWhiteSpace(args.characterId))
                {
                    Timing.RunCoroutine(ShakeEffect(args.magnitude, args.duration).CancelWith(gameObject), "cutscene");
                    shake.Shake(12, 0.8f);
                    return;
                }

                var idLowered = CharacterData.id.ToLower();
                var shakeIdLowered = args.characterId.ToLower();

                if (idLowered == shakeIdLowered)
                {
                    Timing.RunCoroutine(ShakeEffect(args.magnitude, args.duration).CancelWith(gameObject), "cutscene");
                }
            }
        }

        private IEnumerator<float> ShakeEffect(int maxPixelOffset, float duration)
        {
            if (maxPixelOffset < 4) maxPixelOffset = 4;
            if (duration < 0.16f) duration = 0.16f;

            var durationBetweenShakes = duration / maxPixelOffset;
            var originalPos = m_cachedRect.anchoredPosition;
            var loops = maxPixelOffset;
            var dir = m_characterSide == CharacterSide.Right ? 1 : -1;

            for (var i = 0; i < loops; i++)
            {
                m_cachedRect.anchoredPosition = new Vector2(originalPos.x + (maxPixelOffset*dir), originalPos.y);
                yield return Timing.WaitForSeconds(durationBetweenShakes);
                dir *= -1;
                maxPixelOffset--;
            }

            m_cachedRect.anchoredPosition = originalPos;
        }

        public virtual void OnBounceCharacter(IMessage message)
        {
            if (CharacterData == null) return;

            if (message.Data is OnBounceCharacterEventArgs args)
            {
                if (string.IsNullOrWhiteSpace(args.characterId))
                {
                    Timing.RunCoroutine(BounceEffect().CancelWith(gameObject), "cutscene");
                    return;
                }

                var idLowered = CharacterData.id.ToLower();
                var shakeIdLowered = args.characterId.ToLower();

                if (idLowered == shakeIdLowered)
                {
                    Timing.RunCoroutine(BounceEffect().CancelWith(gameObject), "cutscene");
                }
            }
        }

        private IEnumerator<float> BounceEffect()
        {
            const int numJumps = 3;
            const float frameTime = 0.04f;
            var cardSize = new Vector2(88, 100);
            var originalPos = characterPortraitRect.anchoredPosition;

            for (var i = 0; i < numJumps; i++)
            {
                characterPortraitRect.sizeDelta = new Vector2(cardSize.x + 3, cardSize.y - 5);
                characterPortraitRect.anchoredPosition = new Vector2(originalPos.x, originalPos.y - 1);
                shadow.transform.localScale = new Vector3(1.04f, 1.04f, 1);
                yield return Timing.WaitForSeconds(frameTime);
                characterPortraitRect.sizeDelta = new Vector2(cardSize.x + 4, cardSize.y - 8);
                shadow.transform.localScale = new Vector3(1.1f, 1.1f, 1);
                yield return Timing.WaitForSeconds(frameTime);
                characterPortraitRect.sizeDelta = new Vector2(cardSize.x - 6, cardSize.y + 3);
                characterPortraitRect.anchoredPosition = new Vector2(originalPos.x, originalPos.y + 4);
                shadow.transform.localScale = new Vector3(0.96f, 0.96f, 1);
                yield return Timing.WaitForSeconds(frameTime);
                characterPortraitRect.sizeDelta = new Vector2(cardSize.x, cardSize.y);
                characterPortraitRect.anchoredPosition = new Vector2(originalPos.x, originalPos.y + 10);
                shadow.transform.localScale = new Vector3(0.9f, 0.9f, 1);
                yield return Timing.WaitForSeconds(frameTime);
                characterPortraitRect.anchoredPosition = new Vector2(originalPos.x, originalPos.y + 12);
                shadow.transform.localScale = new Vector3(0.84f, 0.84f, 1);
                yield return Timing.WaitForSeconds(frameTime);
                characterPortraitRect.anchoredPosition = new Vector2(originalPos.x, originalPos.y + 11);
                shadow.transform.localScale = new Vector3(0.9f, 0.9f, 1);
                yield return Timing.WaitForSeconds(frameTime);
                characterPortraitRect.anchoredPosition = new Vector2(originalPos.x, originalPos.y + 7);
                shadow.transform.localScale = new Vector3(0.94f, 0.94f, 1);
                yield return Timing.WaitForSeconds(frameTime);
                characterPortraitRect.sizeDelta = new Vector2(cardSize.x + 3, cardSize.y - 5);
                characterPortraitRect.anchoredPosition = new Vector2(originalPos.x, originalPos.y - 1);
                shadow.transform.localScale = new Vector3(1.04f, 1.04f, 1);
                yield return Timing.WaitForSeconds(frameTime);
                characterPortraitRect.sizeDelta = new Vector2(cardSize.x, cardSize.y);
                characterPortraitRect.anchoredPosition = new Vector2(originalPos.x, originalPos.y);
                shadow.transform.localScale = new Vector3(1, 1, 1);
                yield return Timing.WaitForSeconds(frameTime);
            }
        }
    }
}