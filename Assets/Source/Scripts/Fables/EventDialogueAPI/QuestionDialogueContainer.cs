using System;
using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Cutscene.Data;
using MEC;
using TMPro;
using UnityEngine;

namespace CrossBlitz.Fables.EventDialogueAPI
{
    public class QuestionDialogueContainer : MonoBehaviour
    {
        public TextMeshProUGUI lastQuestion;
        public List<DialogueChoiceContainer> choices;

        public event Action<int> OnChoiceConfirmed;

        public IEnumerator<float> Open(CutsceneData cutsceneData, CutsceneDialogueInfo info, string characterId)
        {
            lastQuestion.text = info.Text;

            for (var i = 0; i < choices.Count; i++)
            {
                choices[i].canvasGroup.alpha = 0;
            }

            yield return Timing.WaitForSeconds(0.24f);

            var characterData = Db.CharacterDatabase.GetCharacter(characterId);

            for (var i = 0; i < info.Choices.Count; i++)
            {
                choices[i].OnChoiceSelected -= ChoiceSelected;
                choices[i].OnChoiceSelected += ChoiceSelected;
                choices[i].SetChoice(characterData,info.Choices[i], i );
                Timing.RunCoroutine(choices[i].AnimateIn().CancelWith(gameObject), "cutscene");
                yield return Timing.WaitForSeconds(0.1f);
            }
        }

        private void ChoiceSelected(int index)
        {
            OnChoiceConfirmed?.Invoke(index);
        }
    }
}