
using System.Collections.Generic;
using CrossBlitz.Audio;
using DG.Tweening;
using Febucci.UI;
using MEC;
using UnityEngine;

namespace CrossBlitz.Fables.EventDialogueAPI
{
    public class BattleBanner : MonoBehaviour
    {
        public static BattleBanner Instance;
        public CanvasGroup backgroundCanvas;
        public CanvasGroup swordsCanvas;

        public RectTransform swordRight;
        public RectTransform swordLeft;
        public List<CanvasGroup> swordOverlays;
        public SpriteAnimation hitEffect;

        public RectTransform headerBanner;
        public CanvasGroup headerGlow;
        public GameObject dustParticles;
        public Canvas titleCanvas;

        public TextAnimatorPlayer titleLabel;
        public TextAnimatorPlayer titleShadowLabel;

        public void Awake()
        {
            Instance = this;
        }

        public IEnumerator<float> AnimateIn()
        {
            swordsCanvas.alpha = 0;
            backgroundCanvas.alpha = 0;
            headerBanner.localScale = new Vector2(1, 0);
            swordLeft.eulerAngles = new Vector3(0,0, 90);
            swordRight.eulerAngles = new Vector3(0, 0, -90);
            titleCanvas.overrideSorting = false;
            headerGlow.alpha = 1;

            headerBanner.SetActive(true);
            swordsCanvas.SetActive(true);
            hitEffect.SetActive(false);
            backgroundCanvas.SetActive(true);
            swordOverlays.ForEach(overlay=> overlay.alpha = 0);

            yield return Timing.WaitForOneFrame;

            AudioController.PlaySound("Battle_Whoosh-Physical 01");

            titleLabel.ShowText(string.Empty);
            titleShadowLabel.ShowText(string.Empty);

            backgroundCanvas.DOFade(1, 0.25f);
            swordsCanvas.DOFade(1, 0.25f);

            const float swordDuration = 0.25f;

            swordLeft.DORotate(Vector3.zero, swordDuration)
                .SetEase(Ease.InQuad);
            swordRight.DORotate(Vector3.zero, swordDuration)
                .SetEase(Ease.InQuad);

            yield return Timing. WaitForSeconds(swordDuration);

            AudioController.PlaySound("Battle_SlashingAttack01");

            swordLeft.DOPunchRotation(new Vector3(0, 0, -10), 0.25f);
            swordRight.DOPunchRotation(new Vector3(0, 0, 10), 0.25f);

            headerBanner.DOScaleY(1, 0.5f)
                .SetEase(Ease.OutElastic, 2);

            hitEffect.SetActive(true);
            hitEffect.Play("slash");

            swordOverlays.ForEach(overlay=> overlay.alpha = 1);
            yield return Timing. WaitForSeconds(0.1f);
            swordOverlays.ForEach(overlay=> overlay.alpha = 0);

            yield return Timing. WaitForSeconds(0.25f);
            headerGlow.DOFade(0, 0.25f);
            dustParticles.SetActive(true);

            yield return Timing. WaitForSeconds(0.25f);

            titleCanvas.overrideSorting = true;
            titleCanvas.sortingOrder = 7100;
            titleLabel.ShowText("BATTLE COMMENCE!");
            titleShadowLabel.ShowText("BATTLE COMMENCE!");

            yield return Timing.WaitForSeconds(2);
        }
    }
}