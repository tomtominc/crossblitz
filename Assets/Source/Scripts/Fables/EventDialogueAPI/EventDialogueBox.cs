using System.Collections.Generic;
using CrossBlitz.Dialogue;
using CrossBlitz.Fables.Cutscene;
using CrossBlitz.Fables.Cutscene.Data;
using DG.Tweening;
using MEC;
using TMPro;
using UnityEngine;

namespace CrossBlitz.Fables.EventDialogueAPI
{
    public class EventDialogueBox : DialogueBox
    {
        [Header("Event Only")]
        public Canvas dialogueContainerCanvas;
        public SpriteAnimation portraitBackground;
        public RectTransform portraitContainer;
        public CanvasGroup disableOverlay;
        public TextMeshProUGUI characterNameLabel;

        [HideInInspector]
        public string characterId;

        private CutsceneData _cutsceneData;
        private float _originalPortraitY;
        private float _originalContainerPositionY;
        private float _originalEmoteWindowPositionY;
        private bool _isActive;
        private RectTransform _speechBox;
        private float _portraitDeltaTimer;
        private float _dialogueDeltaTimer;

        const float MoveHeight = 4;
        const float MoveSpeed = 1;

        public override void InitDialogueBox(object customInfo)
        {
            _cutsceneData = (CutsceneData) customInfo;
            _originalPortraitY = portraitContainer.anchoredPosition.y;
            _speechBox = box;
            box = boxCanvas.RectTransform();
            _originalContainerPositionY = box.anchoredPosition.y;

            base.InitDialogueBox(_cutsceneData);
        }

        protected override void SetupStartingPosition()
        {

        }

        public override IEnumerator<float> AnimateIn(DialogueInfo dialogueInfo)
        {

            boxCanvas.DOFade(1, 0.25f);
            _speechBox.sizeDelta = new Vector2(84f, 72f);
            portraitContainer.DOAnchorPosY(_originalPortraitY + 4f, 0.5f);
            disableOverlay.DOFade(0, 0.25f);
            dialogueContainerCanvas.sortingOrder = 6100;
            box.anchoredPosition=new Vector2(box.anchoredPosition.x, _originalContainerPositionY + 4);
            yield return Timing.WaitUntilDone( _speechBox.DOSizeDelta(new Vector2(284f, 72f), 0.5f)
                .SetEase(Ease.OutBack).WaitForCompletion(true));
        }

        protected virtual IEnumerator<float> AnimateInFromSecond(DialogueInfo dialogueInfo)
        {
            boxCanvas.DOFade(1, 0.25f);
            portraitContainer.DOAnchorPosY(_originalPortraitY + 4f + Mathf.Sin(_portraitDeltaTimer * MoveSpeed) * MoveHeight, 0.5f);
            disableOverlay.DOFade(0, 0.25f);
            dialogueContainerCanvas.sortingOrder = 6100;
            yield return Timing.WaitUntilDone(box.DOAnchorPosY(_originalContainerPositionY + 4f, 0.5f)
                .SetEase(Ease.OutBack).WaitForCompletion(true));
        }

        public override IEnumerator<float> AnimateOut()
        {
            disableOverlay.DOFade(1, 0.25f);
            portraitContainer.DOAnchorPosY(_originalPortraitY, 0.25f);
            boxCanvas.DOFade(0, 0.25f);
            //box.DOAnchorPosY(_originalContainerPositionY - 25f, 0.25f);
            dialogueContainerCanvas.sortingOrder = 6001;
            yield break;
        }

        protected override void Update()
        {
            if (!_isActive) return;

            BoxDelta = Mathf.Sin(_dialogueDeltaTimer * MoveSpeed) * MoveHeight;
            DialogueDelta = Mathf.Sin(_portraitDeltaTimer * MoveSpeed) * MoveHeight;

            if (box)
            {
                box.anchoredPosition = new Vector2(box.anchoredPosition.x, _originalContainerPositionY+ 4f + BoxDelta);
            }

            if (portraitFrame)
            {
                portraitFrame.anchoredPosition = new Vector2(portraitFrame.anchoredPosition.x, _originalPortraitY + 4f + DialogueDelta);
            }

            _dialogueDeltaTimer += Time.deltaTime;
            _portraitDeltaTimer += Time.deltaTime;
        }

        public override IEnumerator<float> ShowText(DialogueInfo dialogueBaseInfo)
        {
            dialogue.ShowText(string.Empty);

            if (!_isActive)
            {
                _portraitDeltaTimer = 90;
                _dialogueDeltaTimer = 0;
            }

            var dialogueInfo = (CutsceneDialogueInfo) dialogueBaseInfo;

            if (dialogueInfo.IsFirst)
            {
                yield return Timing.WaitUntilDone(AnimateIn(dialogueInfo));
            }
            else if (!dialogueInfo.SameCharacterFromLast)
            {
                yield return Timing.WaitUntilDone(AnimateInFromSecond(dialogueInfo));
            }

            _isActive = true;
            isReady = true;
            DialogueShowed = false;
            NextButtonPressed = dialogueInfo.SkipPressedActions;

            boxArrow.SetActive(false);

            if (portrait != null && portrait.CurrentAnimationName != dialogueInfo.PortraitAnimation)
            {
                portrait.Play( dialogueInfo.PortraitAnimation );
            }

            dialogue.ShowText(dialogueInfo.Text);

            while (!DialogueShowed)
            {
                yield return Timing.WaitForOneFrame;
            }

            DialogueShowed = false;
            NextButtonPressed = dialogueInfo.SkipPressedActions;

            if (!dialogueInfo.SkipPressedActions)
            {
                boxArrow.SetActive(true);
            }

            while (!NextButtonPressed) yield return Timing.WaitForOneFrame;

            _isActive = dialogueInfo.NextIsSameCharacter;

            NextButtonPressed = false;
        }
    }
}