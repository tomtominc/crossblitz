using System.Collections.Generic;
using CrossBlitz.Fables.Battle;

namespace CrossBlitz.Fables.Treasure
{
    [System.Serializable]
    public class TreasureData
    {
        public int ManaShards=3000;
        public List<ItemValue> ItemRewards;
    }
}