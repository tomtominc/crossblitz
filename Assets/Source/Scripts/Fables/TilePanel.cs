using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Accolades;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Grid;
using CrossBlitz.InputAPI;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using DG.Tweening;
using System.Collections.Generic;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Fables
{
    public class TilePanel : MonoBehaviour
    {
        public RectTransform container;
        public SpriteAnimation banner;
        public RectTransform checkmark;
        public TextMeshProUGUI tileTypeTitle;
        public SpriteAnimation factionEmblem;
        public Button startButton;
        public TextMeshProUGUI startButtonLabel;
        public Button endChapterButton;
        public GameObject checkDeckButton;
        public RectTransform infoContainer;
        public Button infoButton;
        public RectTransform levelContainer;
        public TextMeshProUGUI levelLabel;
        public RectTransform buttonPointer;
        public CanvasGroup buttonPointerCanvas;
        public CanvasGroup buttonGlow;
        public SpriteAnimation buttonReadyBurst;

        public HexTileDisplayView tileDisplay;

        public Transform accoladePipParent;
        public AccoladePipDisplay accoladePipDisplayPrefab;

        public Transform sideQuestIcon;

        private bool m_isOpen;
        private HexTile2D m_tile;
        private bool invalidCardNum = false;
        private bool invalidDeck = false;

        private void Start()
        {
            m_isOpen = false;
            container.anchoredPosition = new Vector2(148, 0);
            startButton.onClick.AddListener(InvokeCurrentTile);
            endChapterButton.onClick.AddListener(InvokeCurrentTile);
            infoButton.onClick.AddListener(OnInfoButton);
            infoContainer.SetActive(true);
            levelContainer.SetActive(true);

            Events.Subscribe(EventType.OnPlayerStartedWalkingFromTile, OnPlayerExitTile);
            Events.Subscribe(EventType.OnPlayerArrivedAtTile, OnPlayerArrivedOnTile);
        }

        private void OnDestroy()
        {
            buttonPointer.DOKill();
            buttonGlow.DOKill();

            Events.Unsubscribe(EventType.OnPlayerStartedWalkingFromTile, OnPlayerExitTile);
            Events.Unsubscribe(EventType.OnPlayerArrivedAtTile, OnPlayerArrivedOnTile);
        }

        private void Open()
        {
            if (!m_isOpen)
            {
                m_isOpen = true;
                container.DOKill();
                container.DOAnchorPosX(12, 0.44f).SetEase(Ease.InOutQuint)
                    .OnComplete(() =>
                    {
                        buttonPointer.DOKill();
                        buttonGlow.DOKill();

                        buttonPointer.SetActive(false);
                        buttonPointer.anchoredPosition = new Vector2(-46.5f, 0);
                        buttonPointerCanvas.alpha = 0;

                        buttonGlow.SetActive(false);
                        buttonGlow.alpha = 0;

                        if (!checkDeckButton.activeSelf)
                        {
                            PlayButtonBurst();
                        }
                        else
                        {
                            buttonPointer.SetActive(true);
                            buttonPointer.DOAnchorPosX(-50.5f, 0.4f).SetLoops(-1, LoopType.Yoyo);
                            buttonPointerCanvas.DOFade(1, 0.4f);
                        }

                    });
            }
        }

        private void Update()
        {
            if (Time.frameCount % 60 == 0)
            {

                if(m_tile != null && m_tile.TileData.NodeType == NodeType.Battle)
                {
                    var currentDeck = App.PlayerDecks.GetDeckBasedOnGameState();
                    invalidDeck = !currentDeck.IsValid();
                    invalidCardNum = currentDeck.GetCardCount() < 30;

                    if (invalidDeck || invalidCardNum)
                    {
                        checkDeckButton.SetActive(true);
                    }
                    else if(checkDeckButton.activeSelf)
                    {
                        checkDeckButton.SetActive(false);
                        PlayButtonBurst();
                    }
                }
                //else
                //{
                //    if (checkDeckButton.activeSelf)
                //    {
                //        checkDeckButton.SetActive(false);
                //        PlayButtonBurst();
                //    }
                //}

            }
        }

        private void Close()
        {
            if (m_isOpen)
            {
                m_isOpen = false;
                container.DOKill();
                container.DOAnchorPosX(148, 0.24f).SetEase(Ease.InOutQuint, 1.2f);
            }

            buttonPointer.DOKill();
            buttonGlow.DOKill();

            buttonPointer.SetActive(false);
            buttonGlow.SetActive(false);
            buttonReadyBurst.SetActive(false);

            m_tile = null;
        }

        private void OnPlayerExitTile(IMessage message)
        {
            Close();
        }

        private void InvokeCurrentTile()
        {
            if (m_tile != null)
            {


                if (FablesInput.Instance == null || FablesInput.Instance.currentMode != FablesInput.Mode.Explore)
                {
                    return;
                }

                if (!ExploreMenu.IsAbleToSelectTile())
                {
                    return;
                }

                AudioController.PlaySound("UI-Selection");

                // Only press shop and mana meld buttons more than once
                var m_nodeType = m_tile.TileData.NodeType;

                if (m_nodeType != NodeType.Shop && m_nodeType != NodeType.ManaMeld)
                {
                    startButton.interactable = false;
                    endChapterButton.interactable = false;
                }

                Events.Publish(this, EventType.OnPlayerInvokedTile, new OnPlayerInvokedTileEventArgs{tile=m_tile});
                if (m_tile.TileData.IsExit() ||
                    m_tile.TileData.NodeType == NodeType.EnterNewMap ||
                    m_tile.TileData.NodeType == NodeType.ExitNewMap ||
                    m_tile.TileData.NodeType == NodeType.Chest ||
                    m_tile.TileData.NodeType == NodeType.Event1 ||
                   m_tile.TileData.NodeType == NodeType.Event2 ) Close();
            }
        }

        private void OnInfoButton()
        {
            //m_tile.Select();

            Events.Publish(this, EventType.OpenTileInfo, new OpenTileInfoEventArgs
            {
                tileData = m_tile.TileData,
                clientTileData = m_tile.ClientTileData
            });
        }

        private void OnPlayerArrivedOnTile(IMessage message)
        {
            if (message.Data is OnPlayerArrivedAtTileEventArgs args)
            {
                m_tile = args.tile;

                var isValidTile = true;
                var usesStartButton = true;
                var buttonIsInteractable = true;
                infoContainer.SetActive(false);
                levelContainer.SetActive(false);

                switch (args.tile.TileData.NodeType)
                {
                    case NodeType.MultiBattle:
                    case NodeType.Battle:
                        tileTypeTitle.text = "BATTLE";
                        startButtonLabel.text = "Play";

                        infoContainer.SetActive(true);
                        levelContainer.SetActive(true);

                        break;
                    case NodeType.Event1:
                    case NodeType.Event2:
                        tileTypeTitle.text = "EVENT";
                        startButtonLabel.text = "Chat";

                        if (FablesMapController.Instance != null && FablesMapController.Instance.ClientChapterData != null)
                        {
                            // for (var i = 0; i < FablesMapController.Instance.ClientChapterData.SeenCutscenes.Count; i++)
                            // {
                            //     var cutscene = FablesMapController.Instance.ClientChapterData.SeenCutscenes[i];
                            //     var cutsceneData = Db.CutsceneDatabase.GetCutscene(cutscene);
                            //     Debug.Log($"Seen={cutscene} {cutsceneData.EventTitle}");
                            // }

                            buttonIsInteractable = !FablesMapController.Instance.ClientChapterData.HasSeenCutscene(args.tile.TileData.introCutsceneUid);
                        }
                        break;
                    case NodeType.Chest when args.tile.ClientTileData == null || !args.tile.ClientTileData.Complete:
                        tileTypeTitle.text = "TREASURE";
                        startButtonLabel.text = "Open";
                        break;
                    case NodeType.ExitLeft:
                    case NodeType.ExitRight:
                    case NodeType.ExitUp:
                    case NodeType.ExitDown:
                        tileTypeTitle.text = "EXIT";
                        startButtonLabel.text = "Leave";
                        break;
                    case NodeType.ExitChapterFinished:
                        tileTypeTitle.text = "FINISH";
                        startButton.SetActive(false);
                        endChapterButton.SetActive(true);
                        usesStartButton = false;
                        break;
                    case NodeType.EnterNewMap:
                        tileTypeTitle.text = "ENTRANCE";
                        startButtonLabel.text = "Enter";
                        break;
                    case NodeType.ExitNewMap:
                        tileTypeTitle.text = "EXIT";
                        startButtonLabel.text = "Leave";
                        break;
                    case NodeType.Shop:
                        var shopType = args.tile.TileData.ShopType;
                        if(shopType == ShopType.Card) tileTypeTitle.text = "CARD SHOP";
                        else tileTypeTitle.text = "RELIC SHOP";
                        startButtonLabel.text = "Enter";
                        break;
                    case NodeType.ManaMeld:
                        tileTypeTitle.text = "CRAFT CARDS";
                        startButtonLabel.text = "Enter";
                        break;
                    default:
                        isValidTile = false;
                        Close();
                        break;
                }

                if (isValidTile)
                {
                    startButton.SetActive(usesStartButton && buttonIsInteractable);
                    endChapterButton.SetActive(!usesStartButton);

                    tileDisplay.SetData(args.tile.TileData, args.tile.ClientTileData);

                    if (args.tile.ClientTileData != null && args.tile.ClientTileData.Complete)
                    {
                        checkmark.SetActive(true);
                        checkmark.anchoredPosition = new Vector2(-20, 58f + 7);
                        checkmark.DOAnchorPosY(58f, 0.24f).SetEase(Ease.OutBounce);
                    }
                    else
                    {
                        checkmark.SetActive(false);
                    }

                    var battleData = Db.BattleDatabase.GetBattleData(args.tile.TileData.battleUid);

                    accoladePipParent.DestroyChildren();

                    if (battleData?.Accolades != null && battleData.Accolades.Count > 0 && m_tile.TileData.NodeType == NodeType.Battle && args.tile.ClientTileData != null)
                    {

                        var pips = new List<Transform>();

                        for (var i = 0; i < battleData.Accolades.Count; i++)
                        {
                            var accoladePip = Instantiate(accoladePipDisplayPrefab, accoladePipParent, false);

                            if (args.tile.ClientTileData == null)
                            {
                                continue;
                            }

                            if (args.tile.ClientTileData.BattleData == null)
                            {
                                Debug.LogError("Battle Data was null and was reset!");
                                args.tile.ClientTileData.Init(m_tile.TileData);
                            }

                            if (args.tile.ClientTileData.BattleData.Accolades == null || args.tile.ClientTileData.BattleData.Accolades.Count <= i)
                            {
                                Debug.LogError("No Accolade data found! Resetting data.");
                                args.tile.ClientTileData.BattleData.FixDependencies(battleData);
                            }

                            accoladePip.SetAccolade(battleData.Accolades[i], args.tile.ClientTileData.BattleData.Accolades[i], battleData.Faction);

                            pips.Add(accoladePip.transform);
                        }
                    }

                    if (!string.IsNullOrEmpty(m_tile.TileData.questId))
                    {
                        var questShowing = IsQuestShowing(out var isSideQuest, args.tile.ClientTileData);
                        sideQuestIcon.SetActive(isSideQuest);
                    }
                    else
                    {
                        sideQuestIcon.SetActive(false);
                    }

                    //startButton.SetActive(true);
                    //endChapterButton.SetActive(false);
                    factionEmblem.SetActive(battleData != null && !string.IsNullOrEmpty( battleData.Name));

                    if(battleData != null) levelLabel.text = battleData.Level.ToString();

                    //startButton.SetActive(buttonIsInteractable);
                    startButton.interactable = buttonIsInteractable;
                    endChapterButton.interactable = buttonIsInteractable;


                    banner.Play(FableTileData.GetFactionStringLower(args.tile.TileData));

                    if (factionEmblem.IsActive())
                    {
                        factionEmblem.Play(FableTileData.GetFactionStringLower(args.tile.TileData));
                    }


                    if(m_tile.TileData.NodeType == NodeType.Battle)
                    {
                        var currentDeck = App.PlayerDecks.GetDeckBasedOnGameState();
                        invalidDeck = !currentDeck.IsValid();
                        invalidCardNum = currentDeck.GetCardCount() < 30;
                        checkDeckButton.SetActive(invalidDeck || invalidCardNum);
                    }
                    else
                    {
                        checkDeckButton.SetActive(false);
                    }


                    Open();
                }
            }
        }

        private void PlayButtonBurst()
        {
            buttonReadyBurst.SetActive(true);
            buttonReadyBurst.Play("burst", () =>
            {
                buttonGlow.SetActive(true);
                buttonGlow.DOFade(0.5f, 0.4f).SetLoops(-1, LoopType.Yoyo);

                buttonPointer.SetActive(true);
                buttonPointer.DOAnchorPosX(-50.5f, 0.4f).SetLoops(-1, LoopType.Yoyo);

                buttonPointerCanvas.DOFade(1, 0.4f);
            });
        }

        public bool IsQuestShowing(out bool isSideQuest, ClientTileData clientTileData)
        {
            var quest = Db.FableQuestDatabase.GetQuest(m_tile.TileData.questId);
            var chapterData = FablesMapController.Instance.ClientChapterData;
            isSideQuest = quest?.IsSideQuest ?? false;

            // in gameplay
            if (quest != null && chapterData != null && clientTileData != null)
            {
                // the quest is active and you're able to show it
                if (chapterData.CanShowQuestNode(m_tile.TileData.questId, m_tile.TileData.questStep))
                {
                    // the quest hasn't been revealed, you'll reveal it later so just turn off the node parent
                    return true;
                }

                return false;
            }

            return true;
        }
    }
}