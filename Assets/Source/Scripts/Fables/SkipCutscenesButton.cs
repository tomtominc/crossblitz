using CrossBlitz.Audio;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.UI.Widgets;
using UnityEngine;

namespace CrossBlitz.Fables
{
    public class SkipCutscenesButton: MonoBehaviour
    {
        public ToggleButton skipCutscenesButton;


        private void Start()
        {
            skipCutscenesButton.Toggle.isOn = App.Settings.GetSkipCutscenes();
            skipCutscenesButton.OnValueChanged += OnSkipCutscenesToggled;
        }

        private void OnSkipCutscenesToggled(bool isOn, string context)
        {
            if (isOn)
            {
                App.Settings.SetSkipCutscenes(true);
                AudioController.PlaySound("TMP-UI-ClickGrab");
            }
            else
            {
                App.Settings.SetSkipCutscenes(false);
                AudioController.PlaySound("TMP-UI-ClickDrop");
            }
        }
    }
}