using CrossBlitz.Audio;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.UI.Widgets;
using UnityEngine;

namespace CrossBlitz.Fables
{
    public class SkipRewardsButton: MonoBehaviour
    {
        public ToggleButton skipRewardsButton;


        private void Start()
        {
            skipRewardsButton.Toggle.isOn = App.Settings.GetNoBattleRewards();
            skipRewardsButton.OnValueChanged += OnNoBattleRewardsToggled;
        }

        private void OnNoBattleRewardsToggled(bool isOn, string context)
        {
            if (isOn)
            {
                App.Settings.SetNoBattleRewards(true);
                AudioController.PlaySound("TMP-UI-ClickGrab");
            }
            else
            {
                App.Settings.SetNoBattleRewards(false);
                AudioController.PlaySound("TMP-UI-ClickDrop");
            }
        }
    }
}