using CrossBlitz.Fables.Grid;
using UnityEngine;

namespace CrossBlitz.Fables
{
    public class ExploreActionWindowContent : MonoBehaviour
    {
        private HexTile2D tile;

        public virtual void SetTileData(HexTile2D tileData)
        {
            tile = tileData;
        }
    }
}