using System;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Fables.Grid;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.UI.Widgets;
using DG.Tweening;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Fables
{
    public class QuestSideContainer : MonoBehaviour
    {
        public CanvasGroup canvasGroup;
        public ToggleButton mainQuestToggleButton;
        public Image mainQuestContainerMask;
        public TextMeshProUGUI mainQuestLabel;
        public RectTransform sideQuestContainer;
        public TextMeshProUGUI sideQuestLabel;
        public CanvasGroup sideQuestOverlay;

        private Boolean _skipSFXOnce;

        private void Start()
        {
            mainQuestContainerMask.enabled = false;
            sideQuestContainer.SetActive(false);

            _skipSFXOnce = true;

            mainQuestToggleButton.OnValueChanged -= OnMainQuestToggled;
            mainQuestToggleButton.OnValueChanged += OnMainQuestToggled;

            Events.Subscribe(EventType.OnPlayerQuestsChanged, OnPlayerQuestsChanged);
            Events.Subscribe(EventType.OnPlayerEnteredMap, OnPlayerEnteredMap);
        }

        public void DisplayOn()
        {
            canvasGroup.DOKill();
            canvasGroup.DOFade(1, 0.24f);
        }

        public void DisplayOff()
        {
            canvasGroup.DOKill();
            canvasGroup.DOFade(0, 0.24f);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnPlayerQuestsChanged, OnPlayerQuestsChanged);
            Events.Unsubscribe(EventType.OnPlayerEnteredMap, OnPlayerEnteredMap);
        }

        private void OnPlayerEnteredMap(IMessage message)
        {
            if (FablesMapController.Instance && FablesMapController.Instance.ClientChapterData != null)
            {
                RefreshView(FablesMapController.Instance.ClientChapterData);
            }
        }

        private void OnPlayerQuestsChanged(IMessage message)
        {
            if (message.Data is OnPlayerQuestsChangedEventArgs args)
            {
                RefreshView(args.readOnlyChapterData);
            }
        }

        private void OnMainQuestToggled(bool isOn, string context)
        {
            if (isOn)
            {
                if(!_skipSFXOnce) AudioController.PlaySound("TMP-UI-ClickGrab");
                _skipSFXOnce = false;
            }
            else
            {
                AudioController.PlaySound("TMP-UI-ClickDrop");
            }
            if (FablesMapController.Instance && FablesMapController.Instance.ClientChapterData != null)
            {
                RefreshView(FablesMapController.Instance.ClientChapterData);
            }
        }

        private void RefreshView(ClientChapterData chapterData)
        {
            if (FablesMapController.Instance && chapterData != null)
            {
                if (!mainQuestToggleButton.Toggle.isOn)
                {
                    if (!string.Equals(mainQuestLabel.text, chapterData.GetMainQuestObjective(),
                        StringComparison.CurrentCultureIgnoreCase))
                    {
                        mainQuestToggleButton.Toggle.isOn = true;
                    }
                }

                mainQuestLabel.text = chapterData.GetMainQuestObjective();

                var sideQuestObjective = chapterData.GetSideQuestObjective();

                Debug.Log($"Side Quest? {sideQuestObjective}, Toggle: {mainQuestToggleButton.Toggle.isOn}");

                if (!string.IsNullOrEmpty(sideQuestObjective) && mainQuestToggleButton.Toggle.isOn)
                {
                    mainQuestContainerMask.enabled = true;

                    if (!sideQuestContainer.IsActive())
                    {
                        sideQuestOverlay.SetActive(true);
                        sideQuestOverlay.alpha = 0;
                        sideQuestOverlay.DOFade(1, 0.24f).OnComplete(() =>
                        {
                            sideQuestContainer.SetActive(true);
                            sideQuestLabel.text = sideQuestObjective;

                            sideQuestOverlay.RectTransform().DOScale(1.2f, 0.24f);
                            sideQuestOverlay.DOFade(0, 0.24f).OnComplete(() =>
                            {
                                sideQuestOverlay.RectTransform().localScale = Vector3.one;
                                sideQuestOverlay.SetActive(false);
                            });
                        });
                    }
                    else
                    {
                        sideQuestContainer.SetActive(true);
                        sideQuestLabel.text = sideQuestObjective;
                    }

                }
                else
                {
                    mainQuestContainerMask.enabled = false;
                    sideQuestContainer.SetActive(false);
                }
            }
            else
            {
                Debug.LogError("Something went wrong with Quests.");
            }
        }
    }
}