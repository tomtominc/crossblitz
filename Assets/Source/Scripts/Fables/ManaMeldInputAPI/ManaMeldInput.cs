
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.Fables.Grid;
using CrossBlitz.InputAPI;
using CrossBlitz.PlayFab.Authentication;
using UnityEngine.SceneManagement;

namespace CrossBlitz.Fables.DeckViewInputApi
{
    public class ManaMeldInput : InputMode
    {

        private void Awake()
        {

        }

        private void OnDestroy()
        {

        }

        private void Start()
        {


        }

        public override void StartMode()
        {
            if (FableController.PlayerIsInAFable())
            {
                App.FableData.OnEnterMeld();
            }
        }

        public override void UpdateMode()
        {

        }

        public override void EndMode()
        {
            if (FableController.PlayerIsInAFable())
            {
                App.FableData.OnExitMeld();
            }

            if (SceneManager.GetSceneByName("ManaMeld").isLoaded)
            {
                SceneController.Instance.UnloadScene("ManaMeld");
            }
        }

        public async void OpenManaMeld()
        {
            if (FablesMapController.Instance == null)
            {
                return;
            }

            var chapterData = FablesMapController.Instance.ChapterData;

            if (chapterData == null)
            {
                return;
            }

            if (SceneManager.GetSceneByName("ManaMeld").isLoaded)
            {
                return;
            }

            AudioController.PlaySound("UI-StartingHand");
            //MasterAudio.PlaySound("UI-StartingHand");

            var sceneModel = await SceneController.Instance.LoadScene(new SceneLoadParams
            {
                SceneToLoad = "ManaMeld"
            });

            var collectionManager = sceneModel.GetView<ManaMeldEditMenu>("CollectionManager");

            collectionManager.OnCollectionClosed += OnCollectionManagerClosed;
            collectionManager.OpenCollectionManagerWithSettings(new DeckEditSettings
            {
                Mode = DeckEditMode.Edit,
                OpenMode = DeckOpenMode.Collection,
                MenuMode = CollectionMenuMode.ManaMeld,
                DeckSettings = new DeckSettings { deckData = App.PlayerDecks.GetDeckBasedOnGameState() },
                EquipContext = App.PlayerDecks.GetContextBasedOnGameState(),
                FilterCardSettings = new FilterCardSettings { ownedCardsOnly = false, forcedFaction = chapterData.ChapterFaction, faction = Faction.All, @class = Class.All, rarity = Rarity.All },
            });
        }

        private async void OnCollectionManagerClosed()
        {
            await System.Threading.Tasks.Task.Delay(400);

            if (FablesInput.Instance)
            {
                FablesInput.Instance.GoToMode(FablesInput.Mode.Explore);
            }
        }

    }
}