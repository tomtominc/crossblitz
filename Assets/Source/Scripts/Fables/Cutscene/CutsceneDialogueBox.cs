using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.Databases;
using CrossBlitz.Dialogue;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.Fables.EventDialogueAPI;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace CrossBlitz.Fables.Cutscene
{
    public class CutsceneDialogueBox : DialogueBox
    {
        private CutsceneData _cutsceneData;
        public QuestionDialogueContainer questionDialogueContainer;

        [HideInInspector] public int currentChoice=-1;

        public override void InitDialogueBox(object customInfo)
        {
            _cutsceneData = (CutsceneData) customInfo;

            questionDialogueContainer.OnChoiceConfirmed -= ChoiceConfirmed;
            questionDialogueContainer.OnChoiceConfirmed += ChoiceConfirmed;

            base.InitDialogueBox(_cutsceneData);
        }

        protected override void OnDestroy()
        {
            if (container) container.DOKill();
        }

        protected override void SetupStartingPosition()
        {
            container.anchoredPosition = new Vector2(OriginalContainerPos.x, -90f);
        }

        protected override void Update()
        {
        }

        public override IEnumerator<float> AnimateIn(DialogueInfo dialogueInfo)
        {
            AudioController.PlaySound("cutscene_text_slide_in", "BARDSFX", true);
            container.DOAnchorPosY(OriginalContainerPos.y, 0.5f).SetEase(Ease.OutBack);

            yield return Timing.WaitForSeconds(0.5f);

            nextButton.SetActive(true);
            isReady = true;
        }

        public override IEnumerator<float> AnimateOut()
        {
            AudioController.PlaySound("cutscene_text_slide_out", "BARDSFX", true);
            isReady = false;
            nextButton.SetActive(false);
            container.DOAnchorPosY(-90f, 0.5f).SetEase(Ease.InBack);

            yield return Timing.WaitForSeconds(0.5f);
        }

        public override IEnumerator<float> ShowText(DialogueInfo dialogueInfo)
        {
            dialogue.SetActive(true);
            nextButton.SetActive(true);
            questionDialogueContainer.SetActive(false);

            yield return Timing.WaitUntilDone(base.ShowText(dialogueInfo));
        }

        public IEnumerator<float> ShowChoices(CutsceneData cutsceneData, CutsceneDialogueInfo info, string otherCharacterId)
        {
            dialogue.SetActive(false);
            questionDialogueContainer.SetActive(true);
            currentChoice = -1;
            boxArrow.SetActive(false);
            nextButton.SetActive(false);

            yield return Timing.WaitUntilDone(questionDialogueContainer.Open(cutsceneData, info, otherCharacterId).CancelWith(gameObject), "cutscene");

            while (currentChoice < 0)
            {
                yield return Timing.WaitForOneFrame;
            }
        }

        private void ChoiceConfirmed(int index)
        {
            currentChoice = index;
        }

        public void setReady(bool ready)
        {
            nextButton.SetActive(true);
            isReady = ready;
        }

        protected override int GetTypewriterPitch(DialogueInfo dialogueInfo)
        {
            if (_cutsceneData != null && dialogueInfo is CutsceneDialogueInfo info)
            {
                if (info.CharacterSide == CharacterSide.Left &&
                    _cutsceneData.LeftSideCharacters.Count > info.CharacterIndex)
                {
                    var character =
                        Db.CharacterDatabase.GetCharacter(_cutsceneData.LeftSideCharacters[info.CharacterIndex]);
                    if (character != null)
                    {
                        return 1;
                    }
                }
                else if (info.CharacterSide == CharacterSide.Right &&
                         _cutsceneData.RightSideCharacters.Count > info.CharacterIndex)
                {
                    var character =
                        Db.CharacterDatabase.GetCharacter(_cutsceneData.RightSideCharacters[info.CharacterIndex]);
                    if (character != null)
                    {
                        return 1;
                    }
                }
            }

            return base.GetTypewriterPitch(dialogueInfo);
        }

        //protected override string GetTypewriterBleepSound(DialogueInfo dialogueInfo)
        //{
        //    if (_cutsceneData != null && dialogueInfo is CutsceneDialogueInfo info)
        //    {
        //        if (info.CharacterSide == CharacterSide.Left &&
        //            _cutsceneData.LeftSideCharacters.Count > info.CharacterIndex)
        //        {
        //            var character =
        //                Db.CharacterDatabase.GetCharacter(_cutsceneData.LeftSideCharacters[info.CharacterIndex]);
        //            if (character != null && !string.IsNullOrEmpty(character.speechBleep))
        //            {
        //                return character.speechBleep;
        //            }
        //        }
        //        else if (info.CharacterSide == CharacterSide.Right &&
        //                 _cutsceneData.RightSideCharacters.Count > info.CharacterIndex)
        //        {
        //            var character =
        //                Db.CharacterDatabase.GetCharacter(_cutsceneData.RightSideCharacters[info.CharacterIndex]);
        //            if (character != null && !string.IsNullOrEmpty(character.speechBleep))
        //            {
        //                return character.speechBleep;
        //            }
        //        }
        //    }

        //    return base.GetTypewriterBleepSound(dialogueInfo);
        //}
    }
}