using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asyncoroutine;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Audio;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.Fables.EventDialogueAPI;
using CrossBlitz.InputAPI;
using CrossBlitz.Items.Data;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ViewAPI.Popups;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Fables.Cutscene
{
    public class EventOptions
    {
        public bool dimBackground;
        public bool usesSkipButton;
        public bool usesPortraitBackground;
        public bool usesTitleContainer;
        public bool usesDialogueBox;
        public bool centerIllustration;
    }
    public class CutsceneDialogueScreen : InputMode, ISceneConfiguration
    {
        public bool dimBackground =true;
        [ShowIf("dimBackground")]
        public CanvasGroup background;
        public bool useTitleContainer=true;
        [ShowIf("useTitleContainer")]
        public EventTitleContainer titleContainer;
        public bool usePortraitBackground = true;
        [ShowIf("usePortraitBackground")]
        public bool useDialogueBox = true;
        [ShowIf("useDialogueBox")]
        public CutscenePortraitBackground portraitBackground;
        public bool usesSkipButton;
        [ShowIf("usesSkipButton")]
        public Button skipButton;
        public bool centerIllustration = false;
        public CutsceneStage stage;

        private bool _cutsceneFinished;
        private bool _skipped;

        private CutsceneData _cutsceneData;
        private CoroutineHandle _cutsceneHandle;
        public bool CutsceneFinished => _cutsceneFinished;

        private List<CutsceneDialogueInfo> m_skipToChoicesDialogue;

        public event Action OnFinishedCutscene;

        public void OnSkipped()
        {
            if (!_skipped && _cutsceneHandle.IsRunning && stage.CanSkip())
            {
                if (stage.IsOnChoiceDialogue())
                {
                    return;
                }

                if (stage.TryGetChoiceDialogue(out var choiceDialogue))
                {
                    //Timing.KillCoroutines("cutscene");
                    m_skipToChoicesDialogue = choiceDialogue;
                    stage.OnSkipToChoices();
                    return;
                }

                _skipped = true;
                //_cutsceneFinished = true;
                //Timing.KillCoroutines("cutscene");

                skipButton.SetActive(false);
                stage.OnSkipped();
            }
        }

        public override void StartMode()
        {

        }

        public override void UpdateMode()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                OnSkipped();
            }

            if (stage != null && stage.CanSkip() && usesSkipButton)
            {
                skipButton.SetActive(true);
            }
            else
            {
                skipButton.SetActive(false);
            }
        }

        public override void EndMode()
        {
            stage.OnEnd();
        }

        public void StartEvent(CutsceneData cutsceneData, EventOptions options)
        {
            _cutsceneData = cutsceneData;
            dimBackground = options.dimBackground;
            usesSkipButton = options.usesSkipButton;
            usePortraitBackground = options.usesPortraitBackground;
            useTitleContainer = options.usesTitleContainer;
            useDialogueBox = options.usesDialogueBox;
            centerIllustration = options.centerIllustration;

            skipButton.SetActive(false);

            if (usesSkipButton && skipButton)
            {
                skipButton.onClick.AddListener(OnSkipped);
            }

            if (!string.IsNullOrEmpty(_cutsceneData.StartingTrack))
            {
                AudioController.PlaySong(_cutsceneData.StartingTrack);
            }

            if (!string.IsNullOrEmpty(_cutsceneData.StartingAmbient))
            {
                AmbientController.PlayAmbient(_cutsceneData.StartingAmbient);
            }

            _cutsceneHandle = Timing.RunCoroutine(AnimateEvent().CancelWith(gameObject));
        }

        private IEnumerator<float> AnimateEvent()
        {
            _cutsceneFinished = false;

            if (background && dimBackground)
            {
                background.SetActive(true);
                background.alpha = 0;
                background.DOFade(1, 0.25f);
            }

            if (centerIllustration)
            {
                var illustrationRectTransform = stage.illustrationDialogueBox.RectTransform();
                illustrationRectTransform.SetLeft(-120);
                illustrationRectTransform.SetTop(-90);
            }

            if (portraitBackground && usePortraitBackground)
            {
                portraitBackground.SetActive(true);

                yield return Timing.WaitUntilDone(portraitBackground
                    .LoadPortraitBackground(_cutsceneData.EventLocation)
                    .AsCoroutine());

                yield return Timing.WaitUntilDone(portraitBackground.AnimateIn().CancelWith(gameObject),"cutscene");
            }

            if (titleContainer && useTitleContainer)
            {
                titleContainer.AnimateIn(_cutsceneData.EventTitle);
            }

            if(usesSkipButton) skipButton.SetActive(true);

            if (!useDialogueBox)
            {
                stage.useCutsceneDialogueBox = false;
            }

            yield return Timing.WaitUntilDone(stage.PlayDialogue(_cutsceneData).CancelWith(gameObject),"cutscene");

            if (m_skipToChoicesDialogue != null && m_skipToChoicesDialogue.Count > 0 && !stage.madeChoice)
            {
                stage.cutsceneDialogueBox.IsReady = true;
                yield return Timing.WaitUntilDone(stage.PlayDialogue(m_skipToChoicesDialogue).CancelWith(gameObject),"cutscene");
            }

            if (_cutsceneData.RewardItems?.Count > 0)
            {
                var result = App.Inventory.GrantItems(_cutsceneData.RewardItems);
                App.HandleItemGrant(result,null, PurchaseErrorCode.NONE, false);
                while (PopupController.IsOpen) yield return Timing.WaitForOneFrame;
            }

            Timing.RunCoroutine(portraitBackground.AnimateOut().CancelWith(gameObject),"cutscene");

            if (background && dimBackground)
            {
                background.DOFade(0, 0.5f).OnComplete(() => background.SetActive(false));
            }

            if (titleContainer && useTitleContainer)
            {
                titleContainer.AnimateOut();
            }

            if (useDialogueBox)
            {
                Timing.RunCoroutine(stage.AnimateOut().CancelWith(gameObject), "cutscene");
                yield return Timing.WaitForSeconds(1);
            }

            _cutsceneFinished = true;

            OnFinishedCutscene?.Invoke();
        }

        public bool GetSkipped()
        {
            return _skipped;
        }


        public Task OnCreated(SceneModel sceneModel)
        {
            return Task.CompletedTask;
        }

        public void OnFocused(bool hasFocus)
        {

        }

        public void OnSceneLoaded(SceneModel model)
        {

        }

        public void OnSceneUnLoaded(SceneModel model)
        {

        }
    }
}