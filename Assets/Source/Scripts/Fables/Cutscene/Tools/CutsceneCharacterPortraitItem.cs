using TMPro;
using UnityEngine;

namespace CrossBlitz.Fables.Cutscene.Tools
{
    public class CutsceneCharacterPortraitItem : MonoBehaviour
    {
        public TextMeshProUGUI characterName;
        public SpriteAnimation characterPortrait;

        public void SetCharacter(string character)
        {
            var anim = character.ToLower();
            characterPortrait.SetActive(false);
            characterName.SetActive(false);

            if (characterPortrait.HasAnimation(anim))
            {
                characterPortrait.SetActive(true);
                characterPortrait.Play(anim);
            }
            else
            {
                characterName.SetActive(true);
                characterName.text = character;
            }
        }
    }
}