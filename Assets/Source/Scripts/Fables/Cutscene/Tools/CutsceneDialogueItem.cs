using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Developer.Tools;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.Shop.Data;
using CrossBlitz.UI.Widgets;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Fables.Cutscene.Tools
{
    public class CutsceneDialogueItem : MonoBehaviour
    {
        public RectTransform rectTransform;
        public RectTransform dialogueBoxRect;
        public RectTransform arrow;

        public TextMeshProUGUI dialogue;

        public CutsceneCharacterPortraitItem characterPortrait;
        public ToggleButton characterPortraitSelectToggle;
        public MultiSelectToggleButtonLayout characterToggleLayout;

        public Transform emotionGroupLayout;
        public CutsceneEmotionItem emotionItemPrefab;
        public SpriteAnimationAsset emoteAnimationAsset;

        public Button removeButton;
        public Button addChoiceButton;
        public Button addItemButton;

        public GameObject itemsGroup;
        public RectTransform itemLayout;
        public LayoutItem rewardItemPrefab;

        public GameObject choicesGroup;
        public RectTransform choicesLayout;
        public LayoutItem choiceItemPrefab;

        private string _dialogueInfoUid;
        private CutsceneDataInspector _inspector;
        private string _currentRewardItemToReplace;

        public event Action<string, string> OnRewardItemDeleted;
        public event Action<string,string, string> OnRewardItemReplaced;
        public event Action<string, string> OnRewardItemAdded;
        public event Action<string, string, string> OnEmoteChanged;
        public event Action<string> OnDialogueDeleted;
        public event Action<string, int, string> OnChoiceTextChanged;
        public event Action<string> OnChoiceAdded;
        public event Action OnUpdateHeight;


        public CutsceneDialogueInfo _dialogueInfo => _inspector.GetDialogueInfo(_dialogueInfoUid);

        private void Start()
        {
            characterPortraitSelectToggle.OnValueChanged += CharacterPortraitToggled;
            removeButton.onClick.AddListener(DialogueDeleted);
            addChoiceButton.onClick.AddListener(ChoiceAdded);
            addItemButton.onClick.AddListener(RewardItemAdded);
        }

        public void RefreshLayout(CutsceneDataInspector inspector, string dialogueInfoUid)
        {
            _inspector = inspector;
            _dialogueInfoUid = dialogueInfoUid;

            dialogue.text = _dialogueInfo.Text;

            SetupEmoteOptions();
            SetupRewardItems();
            SetupChoices();
            SetupCharacter();
            SetupItemPosition();
        }

        //============================================================================================================
        // POSITIONING
        //============================================================================================================

        private void SetupItemPosition()
        {
            // if (string.IsNullOrEmpty(_dialogueInfo.Obsolete_Character_Id_Property))
            // {
            //     Debug.LogError("Character is null!");
            //     return;
            // }

            // var characterData = _inspector.CurrentData._Obsolete_Characters_Property
            //         .Find(character => _dialogueInfo.Obsolete_Character_Id_Property == character.CharacterId);
            //
            // if (characterData==null)
            // {
            //     Debug.LogError("Character Data is null!");
            //     return;
            // }

            //if (characterData.Side == -1) // left side
            {
                dialogueBoxRect.anchoredPosition = Vector2.zero;
                characterPortrait.RectTransform().anchoredPosition = new Vector2(-420, 0);
                arrow.anchoredPosition = new Vector2(-345,0);
                arrow.eulerAngles = new Vector3(0,0,-90);
            }
            //else
            {
                dialogueBoxRect.anchoredPosition = new Vector2(-100, 0);
                characterPortrait.RectTransform().anchoredPosition = new Vector2(320, 0);
                arrow.anchoredPosition = new Vector2(345,0);
                arrow.eulerAngles = new Vector3(0,0,90);
            }
        }

        private void Update()
        {
            var oldSize = rectTransform.sizeDelta;

            rectTransform.sizeDelta = new Vector2(670, dialogueBoxRect.rect.height);

            var newSize = rectTransform.sizeDelta;

            if (Math.Abs(oldSize.y - newSize.y) > Mathf.Epsilon) OnUpdateHeight?.Invoke();
        }

        //============================================================================================================
        // ITEM REWARDS
        //============================================================================================================

        private void SetupRewardItems()
        {
            //Debug.Log($"dialogue items = {_dialogueInfo.RewardItems.Count}");
            // if (_dialogueInfo.RewardItems.Count > 0)
            // {
            //     itemsGroup.SetActive(true);
            //     itemLayout.DestroyChildren(false, 1);
            //
            //     for (var i = 0; i < _dialogueInfo.RewardItems.Count; i++)
            //     {
            //         var rewardItem = Instantiate(rewardItemPrefab, itemLayout);
            //         rewardItem.SetLayoutItem( new LayoutItemData
            //         {
            //             //addressableIcon = _dialogueInfo.RewardItems[i],
            //             data = _dialogueInfo.RewardItems[i]
            //         });
            //         rewardItem.OnDeleteButtonClicked += RewardDeleted;
            //         rewardItem.OnButtonPressed += RewardReplaceButtonPressed;
            //     }
            // }
            // else
            // {
            //     itemsGroup.SetActive(false);
            // }
        }

        private void RewardItemAdded()
        {
            // add a random item, the player can swap this out.
            OnRewardItemAdded?.Invoke(_dialogueInfo.Uid, "AncientScale");
            SetupRewardItems();
        }

        private void RewardDeleted(LayoutItem rewardItem)
        {
            var reward = (string)rewardItem.Data.data;
            OnRewardItemDeleted?.Invoke(_dialogueInfo.Uid, reward);
            SetupRewardItems();
        }

        private void RewardReplaceButtonPressed(ContentButton button, LayoutItem rewardItem)
        {
            _currentRewardItemToReplace = (string)rewardItem.Data.data;
        }

        private void RewardItemReplaced(ShopCategory category, string item)
        {
            OnRewardItemReplaced?.Invoke(_dialogueInfo.Uid, _currentRewardItemToReplace, item);
            SetupRewardItems();
        }

        //============================================================================================================
        // EMOTES
        //============================================================================================================

        private void SetupEmoteOptions()
        {
            emotionGroupLayout.DestroyChildren(false, 1);

            var currentEmotions = emoteAnimationAsset.animations.Select(anim => anim.name).ToList();

            // for (var i = 0; i < _dialogueInfo._Obsolete_Emote_Info_Property.Count; i++)
            // {
            //     var emotionItem = Instantiate(emotionItemPrefab, emotionGroupLayout, false);
            //     emotionItem.SetupEmotionItem(_dialogueInfo._Obsolete_Emote_Info_Property[i].CharacterId, currentEmotions, _dialogueInfo._Obsolete_Emote_Info_Property[i].EmoteId);
            //     emotionItem.OnEmoteChanged += EmoteChanged;
            // }

        }

        private void EmoteChanged(string character, string emotion)
        {
            OnEmoteChanged?.Invoke(_dialogueInfo.Uid, character, emotion);
        }

        private void CharacterPortraitToggled(bool isOn, string content)
        {
            if (isOn)
            {
                SetupCharacterToggleLayout();
            }
            else
            {
                characterToggleLayout.SetActive(false);
            }
        }

        //============================================================================================================
        // CHOICES
        //============================================================================================================

        private void SetupChoices()
        {
            if (_dialogueInfo.Choices.Count > 0)
            {
                choicesGroup.SetActive(true);
                choicesLayout.DestroyChildren(false, 1);
                for (var i = 0; i < _dialogueInfo.Choices.Count; i++)
                {
                    var choiceData = _dialogueInfo.Choices[i];
                    var choice = Instantiate(choiceItemPrefab, choicesLayout);
                    choice.SetLayoutItem( new LayoutItemData
                    {
                        title = choiceData.Choice,
                        data = i
                    });
                    choice.OnTitleChanged += ChoiceTextChanged;
                }
            }
            else
            {
                choicesGroup.SetActive(false);
            }
        }

        private void ChoiceTextChanged(string choiceNewText, LayoutItem item)
        {
            OnChoiceTextChanged?.Invoke(_dialogueInfo.Uid, (int)item.Data.data, choiceNewText);
        }

        private void ChoiceAdded()
        {
            OnChoiceAdded?.Invoke(_dialogueInfo.Uid);
            SetupChoices();
            OnUpdateHeight?.Invoke();
        }

        //============================================================================================================
        // CHARACTER
        //============================================================================================================

        private void SetupCharacter()
        {
            // characterPortrait.SetCharacter(_dialogueInfo.Obsolete_Character_Id_Property);
            // characterPortraitSelectToggle.Toggle.isOn = false;
        }
        private void SetupCharacterToggleLayout()
        {
            var toggleValues = new List<ToggleSelectValue>();

            // for (var i = 0; i < _inspector.CurrentData._Obsolete_Characters_Property.Count; i++)
            // {
            //     toggleValues.Add(new ToggleSelectValue
            //     {
            //         content = _inspector.CurrentData._Obsolete_Characters_Property[i].CharacterId,
            //         isOn = _inspector.CurrentData._Obsolete_Characters_Property[i].CharacterId == _dialogueInfo.Obsolete_Character_Id_Property
            //     });
            // }

            characterToggleLayout.SetActive(true);
            characterToggleLayout.Rebuild(toggleValues);
        }

        private void DialogueDeleted()
        {
            OnDialogueDeleted?.Invoke(_dialogueInfo.Uid);
            Destroy(gameObject);
        }
    }
}