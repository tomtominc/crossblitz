using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace CrossBlitz.Fables.Cutscene.Tools
{
    public class CutsceneEmotionItem : MonoBehaviour
    {
        public CutsceneCharacterPortraitItem avatar;
        public TMP_Dropdown emotionDropdown;

        private string _characterId;

        public event Action<string, string> OnEmoteChanged;

        private void Start()
        {
            emotionDropdown.onValueChanged.AddListener(EmoteChanged);
        }
        public void SetupEmotionItem(string character, List<string> emotionChoices, string defaultEmotion = "talking")
        {
            _characterId = character;

            avatar.SetCharacter(_characterId);
            emotionDropdown.ClearOptions();
            emotionDropdown.AddOptions(emotionChoices);
            emotionDropdown.SetValueWithoutNotify(emotionChoices.IndexOf(defaultEmotion));
        }

        private void EmoteChanged(int emoteIndex)
        {
            OnEmoteChanged?.Invoke(_characterId, emotionDropdown.options[emoteIndex].text);
        }
    }
}