using System;
using CrossBlitz.Databases;
using CrossBlitz.Developer.Tools;
//using UltimateClean;

namespace CrossBlitz.Fables.Cutscene.Tools
{
    public class CutsceneEditor : DatabaseEditorView
    {
        public CutsceneDatabaseTable cutsceneDatabaseTable;
        public event Action<string> OnLinkedCutscene;

        public override void Open()
        {
            //popup.Open();
            cutsceneDatabaseTable.RefreshDatabaseTable(string.Empty, _options.linkOptionEnabled);
            cutsceneDatabaseTable.OnLinkedCutscene -= LinkedCutscene;
            cutsceneDatabaseTable.OnLinkedCutscene += LinkedCutscene;
        }

        private void OnDisable()
        {
            Db.CutsceneDatabase.Save();
        }

        private void LinkedCutscene(string cutsceneUid)
        {
            OnLinkedCutscene?.Invoke(cutsceneUid);
        }
    }
}