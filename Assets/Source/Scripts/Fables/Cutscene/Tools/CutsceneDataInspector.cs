using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.UI.Widgets;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Fables.Cutscene.Tools
{
    public class CutsceneDataInspector : MonoBehaviour
    {
        [BoxGroup("View Group")] public TextMeshProUGUI cutsceneNameLabel;
        [BoxGroup("View Group")] public TMP_Dropdown cutsceneTypeDropdown;

        [BoxGroup("View Group")] public TMP_InputField cutsceneNameEdit;
        [BoxGroup("View Group")] public ToggleButton editNameToggle;

        [BoxGroup("Navigation Controls")]
        public Button settingsButton;
        [BoxGroup("Navigation Controls")]
        public Button chatButton;
        [BoxGroup("Navigation Controls")]
        public Button backButton;

        [BoxGroup("Dialogue Controls")]
        public TMP_InputField dialogueInputField;
        [BoxGroup("Dialogue Controls")]
        public Button sendDialogueButton;


        [BoxGroup("Character Select Controls")]
        public ToggleButton addCharacterPopupToggle;
        [BoxGroup("Character Select Controls")]
        public MultiSelectToggleButtonLayout characterToggleLayout;
        [BoxGroup("Character Select Controls")]
        public RectTransform characterButtonLayout;
        [BoxGroup("Character Select Controls")]
        public MultiToggleButton characterTogglePrefab;
        [BoxGroup("Character Select Controls")]
        public ToggleGroup characterToggleGroup;

        [BoxGroup("Messages")]
        public CutsceneDialogueItem dialogueItemPrefab;
        [BoxGroup("Messages")]
        public RectTransform dialogueLayout;

        [BoxGroup("Misc")]
        public GameObject dialogueControlsGroup;
        [BoxGroup("Misc")]
        public GameObject dialogueThreadGroup;
        [BoxGroup("Misc")]
        public GameObject noCharacterSelectedWarning;

        private CutsceneData currentData;
        public CutsceneData CurrentData => currentData;

        private string _currentCharacterSelected;
        private List<CutsceneDialogueInfo> _currentList;

        public event Action<string,string> OnNameChanged;

        private void Start()
        {
            sendDialogueButton.onClick.AddListener(OnSendDialogue);
            addCharacterPopupToggle.OnValueChanged += ShowCharacterPopup;
            characterToggleLayout.OnContentValueChanged += OnCharacterPresenceChanged;
            noCharacterSelectedWarning.SetActive(true);
            sendDialogueButton.interactable = false;
            editNameToggle.OnValueChanged += OnEditNameToggled;
            cutsceneTypeDropdown.onValueChanged.AddListener(OnCutsceneTypeChanged);
        }

        private void OnEditNameToggled(bool isOn, string content)
        {
            if (!isOn)
            {
                cutsceneNameLabel.text = cutsceneNameEdit.text;
                cutsceneNameEdit.SetActive(false);

                if (!string.IsNullOrEmpty(cutsceneNameEdit.text) && cutsceneNameEdit.text != currentData.EventTitle)
                {
                    currentData.EventTitle = cutsceneNameEdit.text;
                    OnNameChanged?.Invoke(currentData.Uid, currentData.EventTitle);
                    Db.CutsceneDatabase.Save();
                }
            }
            else
            {
                cutsceneNameEdit.text = currentData.EventTitle;
                cutsceneNameEdit.SetActive(true);
            }
        }

        private void OnCutsceneTypeChanged(int content)
        {
            currentData.CutsceneType = (CutsceneType) content;
            Db.CutsceneDatabase.Save();
        }

        public void ShowEmpty()
        {
            dialogueThreadGroup.SetActive(false);
            dialogueControlsGroup.SetActive(false);
            cutsceneNameLabel.text = "No Cutscene Selected";
            editNameToggle.SetActive(false);
            cutsceneNameEdit.SetActive(false);
        }

        public void OpenCutscene(CutsceneData data)
        {
            currentData = data;
            cutsceneNameLabel.text = currentData.EventTitle;
            cutsceneTypeDropdown.SetValueWithoutNotify((int)data.CutsceneType);

            dialogueThreadGroup.SetActive(true);
            dialogueControlsGroup.SetActive(true);
            editNameToggle.SetActive(true);

            SetupCharacters();
            FillDialogue(currentData.DialogueInfo);
        }

        private void OnSendDialogue()
        {
            if (string.IsNullOrEmpty(dialogueInputField.text) || string.IsNullOrEmpty(_currentCharacterSelected))
            {
                Debug.LogError("No character selected or there's no dialogue in the text box!");
                return;
            }

            var dialogue = new CutsceneDialogueInfo();
            dialogue.UpdateIdentifier();
            dialogue.Text = dialogueInputField.text;
            //dialogue.Obsolete_Character_Id_Property = _currentCharacterSelected;
            //dialogue._Obsolete_Emote_Info_Property = new List<CutsceneEmoteData>();
            dialogue.Choices = new List<CutsceneChoice>();
           // dialogue.RewardItems = new List<string>();

            // for (var i = 0; i < currentData._Obsolete_Characters_Property.Count; i++)
            // {
            //     dialogue._Obsolete_Emote_Info_Property.Add(new CutsceneEmoteData
            //     {
            //         CharacterId = currentData._Obsolete_Characters_Property[i].CharacterId,
            //         EmoteId = "talking"
            //     });
            // }

            _currentList.Add(dialogue);

            AddDialogoue(dialogue);
        }

        private void ShowCharacterPopup(bool show, string content)
        {
            characterToggleLayout.SetActive(show);

            if (show)
            {
                var toggleValues = new List<ToggleSelectValue>();
                var heroes = Db.HeroDatabase.Heroes;
                var minions = Db.CardDatabase.Cards.FindAll(card => card.type == CardType.Minion);

                // for (var i = 0; i < heroes.Count; i++)
                // {
                //     toggleValues.Add( new ToggleSelectValue
                //     {
                //         content = heroes[i].id,
                //         isOn = currentData._Obsolete_Characters_Property.Exists( character => character.CharacterId == heroes[i].id)
                //     });
                // }
                //
                // for (var i = 0; i < minions.Count; i++)
                // {
                //     toggleValues.Add( new ToggleSelectValue
                //     {
                //         content = minions[i].id,
                //         isOn = currentData._Obsolete_Characters_Property.Exists( character => character.CharacterId == minions[i].id)
                //     });
                // }

                characterToggleLayout.Rebuild(toggleValues);
            }
        }

        private void OnCharacterPresenceChanged(bool isOn, string content)
        {
            Debug.LogWarning($"Turning On? {isOn} Content = {content}");

            if (isOn)
            {
                // if (!currentData._Obsolete_Characters_Property.Exists(character => character.CharacterId == content))
                // {
                //     var heroData = Db.HeroDatabase.GetHero(content);
                //     var cardData = Db.CardDatabase.GetCard(content);
                //
                //     currentData._Obsolete_Characters_Property.Add(new CutsceneCharacterData
                //     {
                //         CharacterId = content,
                //         CharacterName = heroData != null? heroData.name : cardData.name,
                //         PortraitUri = heroData != null ? heroData.portraitUrl : cardData.portraitUrl,
                //         DialoguePortrait = heroData != null ? heroData.id : "none",
                //         DialoguePortraitBackground = "none"
                //     });
                //
                //     Db.CutsceneDatabase.Save();
                //
                //     SetupCharacters();
                // }
                // else
                // {
                //     Debug.LogError("Character already exists in this context!");
                // }
            }
            else
            {
                // currentData._Obsolete_Characters_Property.RemoveAll(character => character.CharacterId == content);
                // Db.CutsceneDatabase.Save();
                //
                // SetupCharacters();
            }
        }

        private void SetupCharacters()
        {
            characterButtonLayout.DestroyChildren();

            // for (var i = 0; i < CurrentData._Obsolete_Characters_Property.Count; i++)
            // {
            //     var characterToggle = Instantiate(characterTogglePrefab, characterButtonLayout);
            //
            //     characterToggle.name = CurrentData._Obsolete_Characters_Property[i].CharacterId;
            //
            //     characterToggle.toggleButtons[0].Toggle.isOn =
            //         _currentCharacterSelected == CurrentData._Obsolete_Characters_Property[i].CharacterId;
            //     characterToggle.toggleButtons[0].Toggle.group = characterToggleGroup;
            //     characterToggle.toggleButtons[0].content = CurrentData._Obsolete_Characters_Property[i].CharacterId;
            //
            //     // characterToggle.toggleButtons[1].Toggle.isOn = CurrentData._Obsolete_Characters_Property[i].Side == -1;
            //     // characterToggle.toggleButtons[2].Toggle.isOn = CurrentData._Obsolete_Characters_Property[i].Side == 1;
            //
            //     characterToggle.OnValueChanged  += OnCharacterOptionSelected;
            //
            //     var portrait = characterToggle.GetComponent<CutsceneCharacterPortraitItem>();
            //     portrait.SetCharacter(CurrentData._Obsolete_Characters_Property[i].CharacterId);
            // }
            //
            // noCharacterSelectedWarning.SetActive(CurrentData._Obsolete_Characters_Property.Count <= 0);
        }

        private void OnCharacterOptionSelected(bool isOn, string content, MultiToggleButton button)
        {
            if (!isOn) return;

            // if (content == "Left")
            // {
            //     var characterData = CurrentData._Obsolete_Characters_Property.Find(character => character.CharacterId == button.name);
            //     if (characterData != null)
            //     {
            //         //characterData.Side = -1;
            //     }
            // }
            // else if (content == "Right")
            // {
            //     var characterData = CurrentData._Obsolete_Characters_Property.Find(character => character.CharacterId == button.name);
            //     if (characterData != null)
            //     {
            //         //characterData.Side = 1;
            //     }
            // }
            // else
            // {
            //     _currentCharacterSelected = content;
            //     noCharacterSelectedWarning.SetActive(string.IsNullOrEmpty(_currentCharacterSelected));
            //     sendDialogueButton.interactable = !string.IsNullOrEmpty(_currentCharacterSelected);
            // }
        }

        public void AddDialogoue(CutsceneDialogueInfo dialogue)
        {
            var dialogueItem = Instantiate(dialogueItemPrefab, dialogueLayout);
            dialogueItem.RefreshLayout(this, dialogue.Uid);
            dialogueItem.OnRewardItemDeleted += OnRewardItemDeleted;
            dialogueItem.OnRewardItemReplaced += OnRewardItemReplaced;
            dialogueItem.OnRewardItemAdded += OnRewardItemAdded;
            dialogueItem.OnEmoteChanged += OnEmoteChanged;
            dialogueItem.OnDialogueDeleted += OnDialogueDeleted;
            dialogueItem.OnChoiceTextChanged += OnChoiceTextChanged;
            dialogueItem.OnChoiceAdded += OnChoiceAdded;
            dialogueItem.OnUpdateHeight += OnUpdateHeight;
        }

        public void FillDialogue(List<CutsceneDialogueInfo> dialogue)
        {
            _currentList = dialogue;

            dialogueLayout.DestroyChildren();

            for (var i = 0; i < _currentList.Count; i++)
            {
                var dialogueItem = Instantiate(dialogueItemPrefab, dialogueLayout);
                dialogueItem.RefreshLayout(this, _currentList[i].Uid);
                dialogueItem.OnRewardItemDeleted += OnRewardItemDeleted;
                dialogueItem.OnRewardItemReplaced += OnRewardItemReplaced;
                dialogueItem.OnRewardItemAdded += OnRewardItemAdded;
                dialogueItem.OnEmoteChanged += OnEmoteChanged;
                dialogueItem.OnDialogueDeleted += OnDialogueDeleted;
                dialogueItem.OnChoiceTextChanged += OnChoiceTextChanged;
                dialogueItem.OnChoiceAdded += OnChoiceAdded;
                dialogueItem.OnUpdateHeight += OnUpdateHeight;
            }
        }

        private void OnUpdateHeight()
        {
            LayoutRebuilder.MarkLayoutForRebuild(dialogueLayout);
        }

        private void OnEmoteChanged(string dialogueUid, string characterId, string emotion)
        {
            // var dialogue = GetDialogueInfo(dialogueUid);
            // var emote = dialogue._Obsolete_Emote_Info_Property.Find(emote => emote.CharacterId == characterId);
            // emote.EmoteId = emotion;
            // Db.CutsceneDatabase.Save();
        }

        private void OnRewardItemDeleted(string dialogueUid, string deletedItem)
        {
            var dialogue = GetDialogueInfo(dialogueUid);
           // dialogue.RewardItems.Remove(deletedItem);
            Db.CutsceneDatabase.Save();
        }

        private void OnRewardItemReplaced(string dialogueUid, string originalItem, string replacementItem)
        {
            var dialogue = GetDialogueInfo(dialogueUid);
           // var indexOfOriginalItem = dialogue.RewardItems.IndexOf(originalItem);
           // dialogue.RewardItems[indexOfOriginalItem] = replacementItem;
            Db.CutsceneDatabase.Save();
        }

        private void OnRewardItemAdded(string dialogueUid, string addedItem)
        {
            var dialogue = GetDialogueInfo(dialogueUid);
           // dialogue.RewardItems.Add(addedItem);
            Db.CutsceneDatabase.Save();
        }

        private void OnDialogueDeleted(string dialogueUid)
        {
            _currentList.RemoveAll(dialogue => dialogue.Uid == dialogueUid);
            Db.CutsceneDatabase.Save();
            FillDialogue(_currentList);
            //Db.CutsceneDatabase.Delete(dialogueUid);
        }

        private void OnChoiceTextChanged(string dialogueUid, int choiceIndex, string choiceNewText)
        {
            var dialogue = GetDialogueInfo(dialogueUid);
            dialogue.Choices[choiceIndex].Choice = choiceNewText;
            Db.CutsceneDatabase.Save();
        }

        private void OnChoiceAdded(string dialogueUid)
        {
            // var dialogue = GetDialogueInfo(dialogueUid);
            //
            // if (dialogue.Choices.Count <= 0)
            // {
            //     dialogue.Choices.Add(new CutsceneChoice { Choice = "Yes!", _Obsolete_Dialogue_Json_Property = new List<string>() } );
            //     dialogue.Choices.Add(new CutsceneChoice { Choice = "No", _Obsolete_Dialogue_Json_Property = new List<string>() } );
            // }
            // else
            // {
            //     dialogue.Choices.Add(new CutsceneChoice { Choice = $"Choice {dialogue.Choices.Count+1}", _Obsolete_Dialogue_Json_Property = new List<string>() } );
            // }
            //
            // Db.CutsceneDatabase.Save();
        }

        public CutsceneDialogueInfo GetDialogueInfo(string dialogueUid)
        {
            return _currentList.Find(dialogue => dialogue.Uid == dialogueUid);
        }
    }
}