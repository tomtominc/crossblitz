using System;
using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.UI.Widgets;
using CrossBlitz.ViewAPI.Popups;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Fables.Cutscene.Tools
{
    public class CutsceneDatabaseTable : MonoBehaviour
    {
        public Button addCutsceneDatabaseItemButton;
        public RectTransform cutsceneTableLayout;
        public LayoutItem cutsceneDatabaseItemPrefab;
        public CutsceneDataInspector cutsceneInspector;

        private string _selectedTableItem;
        private bool _showLinkOption;
        private Dictionary<string, LayoutItem> _cutsceneDatabaseItems;

        public event Action<string> OnLinkedCutscene;

        private void Start()
        {
            addCutsceneDatabaseItemButton.onClick.AddListener(AddCutsceneButtonClicked);
            cutsceneInspector.OnNameChanged += OnCutsceneNameChanged;
        }

        public void RefreshDatabaseTable(string selectedTableItem, bool showLinkOption)
        {
            _showLinkOption = showLinkOption;
            _selectedTableItem = selectedTableItem;

            cutsceneTableLayout.DestroyChildren();
            _cutsceneDatabaseItems = new Dictionary<string, LayoutItem>();

            if (Db.CutsceneDatabase.Cutscenes.Count <= 0) return;

            if (_selectedTableItem == string.Empty)
            {
                cutsceneInspector.ShowEmpty();
            }

            foreach (var tableItem in Db.CutsceneDatabase.Cutscenes)
            {
                var cutsceneData = tableItem;
                var layoutItem = Instantiate(cutsceneDatabaseItemPrefab, cutsceneTableLayout);
                layoutItem.SetLayoutItem( new LayoutItemData
                {
                    title = cutsceneData.EventTitle,
                    subTitle = cutsceneData.DialogueInfo.Count > 0 ? cutsceneData.DialogueInfo[cutsceneData.DialogueInfo.Count-1].Text : string.Empty,
                    data = cutsceneData
                });

                layoutItem.OnMainButtonValueChanged += OnCutsceneDatabaseItemSelected;
                layoutItem.OnDeleteButtonClicked += OnCutsceneDatabaseItemDeleted;
                layoutItem.OnButtonPressed += OnOtherOptionsClicked;

                if (layoutItem.buttons.Count > 0)
                {
                    layoutItem.buttons[0].SetActive(_showLinkOption);
                }

                if (layoutItem.labels.Count > 0)
                {
                    layoutItem.labels[0].text = cutsceneData.DialogueInfo.Count.ToString();
                }

                if (_selectedTableItem == cutsceneData.Uid)
                {
                    cutsceneInspector.OpenCutscene(cutsceneData);
                }

                _cutsceneDatabaseItems.Add(cutsceneData.Uid, layoutItem);
            }
        }

        private void AddCutsceneButtonClicked()
        {
            var newCutsceneData = Db.CutsceneDatabase.CreateNew(string.Empty);
            RefreshDatabaseTable(newCutsceneData.Uid, _showLinkOption);
        }

        private void OnCutsceneDatabaseItemSelected(bool isOn, LayoutItem item)
        {
            if (!isOn) return;

            var cutsceneData = (CutsceneData) item.Data.data;
            cutsceneInspector.OpenCutscene(cutsceneData);
            //RefreshDatabaseTable(cutsceneData.Uid, _showLinkOption);
        }

        private void OnOtherOptionsClicked(ContentButton clickedButton, LayoutItem item)
        {
            if (clickedButton == null) return;

            Debug.Log($"Clicked {clickedButton.name}");

            if (clickedButton.name.Equals("LinkButton"))
            {
                var cutsceneData = (CutsceneData) item.Data.data;
                OnLinkedCutscene?.Invoke(cutsceneData.Uid);
            }
        }

        private void OnCutsceneDatabaseItemDeleted(LayoutItem item)
        {
            var cutsceneData = (CutsceneData) item.Data.data;
            _selectedTableItem = cutsceneData.Uid;

            PopupController.Open(new PopupInfo
            {
                title = "Delete Cutscene?",
                body = "Deleting this cutscene cannot be undone.",
                deny = "Keep Cutscene",
                confirm = "Delete Cutscene"
            });

            PopupController.OnPopupChoice += OnDeleteActionResponse;
        }

        private void OnDeleteActionResponse(bool delete)
        {
            if (delete)
            {
                Db.CutsceneDatabase.Delete(_selectedTableItem);
                RefreshDatabaseTable(string.Empty, _showLinkOption);
            }
        }

        private void OnCutsceneNameChanged(string cutsceneGuid, string newName)
        {
            if (_cutsceneDatabaseItems.ContainsKey(cutsceneGuid))
            {
                _cutsceneDatabaseItems[cutsceneGuid].titleLabel.text = newName;
            }
        }
    }
}