using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Dialogue;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.Fables.EventDialogueAPI;
using CrossBlitz.Hero;
using MEC;
using TakoBoyStudios.Events;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Fables.Cutscene
{
    public class MatchCutsceneCharacter : EventCharacter
    {
        private CardView m_cardView;
        private HeroViewBattle m_heroView;
        private string m_lastEmote;

        public RectTransform RectTransform
        {
            get
            {
                if (m_cardView != null) return m_cardView.Rect;
                if (m_heroView != null) return m_heroView.portraitContainerParent;

                return this.RectTransform();
            }
        }

        public CardDialogue CardDialogue
        {
            get
            {
                if (m_cardView != null) return m_cardView.cardDialogue;
                if (m_heroView != null) return m_heroView.cardDialogue;

                return null;
            }
        }

        public string LastEmote => m_lastEmote;

        private void Start()
        {
            Events.Subscribe(EventType.OnShakeCharacter, OnShakeCharacter);
            Events.Subscribe(EventType.OnBounceCharacter, OnBounceCharacter);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            Events.Unsubscribe(EventType.OnShakeCharacter, OnShakeCharacter);
            Events.Unsubscribe(EventType.OnBounceCharacter, OnBounceCharacter);
        }

        public void SetCardView(HeroViewBattle heroView, CardView cardView)
        {
            m_heroView = heroView;
            m_cardView = cardView;
        }

        public override void AnimateStartTalking(DialogueInfo dialogue, bool questionTail)
        {

        }

        protected override void LoadCharacterPortrait()
        {

        }

        public override void AnimateEndTalking()
        {

        }

        public override void SetEmote(string emoteAnim)
        {
            m_lastEmote = emoteAnim;
        }

        public IEnumerator<float> ShowText(DialogueInfo dialogueInfo, bool userInput)
        {
            CardDialogue.SetDialogue(new DialogueSnippetData
            {
                Emotion = LastEmote,
                Dialogue = dialogueInfo.Text

            }, userInput);

            yield return Timing.WaitUntilTrue(() => !CardDialogue.IsActive());
        }

        public override void OnShakeCharacter(IMessage message)
        {
            m_cachedRect = RectTransform;
            base.OnShakeCharacter(message);
        }

        public override void OnBounceCharacter(IMessage message)
        {
            characterPortraitRect = RectTransform;
            base.OnBounceCharacter(message);
        }
    }
}