using System.Collections.Generic;
using CrossBlitz.Dialogue;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.Fables.EventDialogueAPI;
using DG.Tweening;
using LeTai.TrueShadow;
using MEC;
using TakoBoyStudios.Events;
using UnityEngine;
using UnityEngine.Serialization;

namespace CrossBlitz.Fables.Cutscene
{
    public class CutsceneCharacter : EventCharacter
    {
        public SpriteAnimation characterAnimator;
        [FormerlySerializedAs("shadow")] public TrueShadow trueShadow;

        public override void SetCharacterData(string characterId, string overrideName, CharacterSide characterSide)
        {
            base.SetCharacterData(characterId, overrideName, characterSide);

            m_cachedRect.anchoredPosition = m_characterSide==CharacterSide.Right?
                new Vector2(base.m_sizeDelta.x, base.m_originalPosition.y):
                new Vector2(-base.m_sizeDelta.x, base.m_originalPosition.y);
        }

        protected override void LoadCharacterPortrait()
        {
            SetEmote("talking");
        }

        public override IEnumerator<float> AnimateIn()
        {
            yield return Timing.WaitUntilDone(m_cachedRect.DOAnchorPos(m_originalPosition, 0.5f)
                .SetEase(Ease.OutExpo)
                .WaitForCompletion(true));
        }

        public void AnimateOut()
        {
            var outLocation = m_characterSide == CharacterSide.Right?
                new Vector2(m_sizeDelta.x, m_originalPosition.y):
                new Vector2(-m_sizeDelta.x, m_originalPosition.y);
            m_cachedRect.DOAnchorPos(outLocation, 0.5f).SetEase(Ease.InBack);
        }

        public override IEnumerator<float> AnimateOutRoutine()
        {
            var outLocation =m_characterSide ==CharacterSide.Right? new Vector2(m_sizeDelta.x, m_originalPosition.y): new Vector2(-m_sizeDelta.x, m_originalPosition.y);
            m_cachedRect.DOAnchorPos(outLocation, 0.5f).SetEase(Ease.InBack);
            yield return Timing.WaitForSeconds(0.5f);
        }

        public IEnumerator<float> AnimateOutRoutineSlow()
        {
            var canvasGroup = m_cachedRect.GetComponent<CanvasGroup>();
            canvasGroup.DOFade(0, 0.75f);
            yield return Timing.WaitForSeconds(0.75f);

            var outLocation = m_characterSide == CharacterSide.Right ? new Vector2(m_sizeDelta.x, m_originalPosition.y) : new Vector2(-m_sizeDelta.x, m_originalPosition.y);
            m_cachedRect.DOAnchorPos(outLocation, 0.5f);
            yield return Timing.WaitForSeconds(0.5f);
            canvasGroup.alpha = 1;
        }

        public override void AnimateStartTalking(DialogueInfo dialogue, bool questionTail)
        {
            if (_characterData == null || _characterData.id == DialogueInfo.Empty)
            {
                if (m_cachedRect == null) return;

                var outLocation = m_characterSide == CharacterSide.Right?
                    new Vector2(m_sizeDelta.x, m_originalPosition.y):
                    new Vector2(-m_sizeDelta.x, m_originalPosition.y);

                m_cachedRect.anchoredPosition = outLocation;

                return;
            }

            Sequence punchSeq = DOTween.Sequence();
            punchSeq.Append(characterPortraitRect.DOScale(new Vector3(1.06f * (m_characterSide==CharacterSide.Right?-1:1), 0.97f), 0.08f));
            punchSeq.AppendCallback(() =>
            {
                OnIsTalking(dialogue,questionTail);
            });
            punchSeq.Append(characterPortraitRect.DOScale(new Vector3(0.97f * (m_characterSide==CharacterSide.Right?-1:1), 1.06f), 0.04f));
            punchSeq.Append(characterPortraitRect.DOScale(new Vector3(1f * (m_characterSide==CharacterSide.Right?-1:1), 1f), 0.04f));
        }

        private void OnIsTalking(DialogueInfo dialogue, bool questionTail = false)
        {
            if (_characterData == null || _characterData.id == DialogueInfo.Empty)
            {
                if (m_cachedRect == null) return;

                var outLocation = m_characterSide == CharacterSide.Right?
                    new Vector2(m_sizeDelta.x, m_originalPosition.y):
                    new Vector2(-m_sizeDelta.x, m_originalPosition.y);

                m_cachedRect.anchoredPosition = outLocation;

                return;
            }

            if (characterAnimator.image)
            {
                characterAnimator.image.DOColor(Color.white, 0.16f);
            }
            if (trueShadow)
            {
                DOTween.To(() => trueShadow.OffsetDistance, offset => trueShadow.OffsetDistance = offset, 16, 0.16f);
            }

            m_cachedRect.DOAnchorPosX(m_originalPosition.x, 0.08f);

            if (!nameContainer.GetCurrentAnimatorStateInfo(0).IsName("Open"))
            {
                nameContainer.Play("Open");
            }

            speechTail.SetActive(true);
            speechTail.Play(questionTail ? "question" : "default");
        }

        public override void AnimateEndTalking()
        {
            if (_characterData == null || _characterData.id == DialogueInfo.Empty)
            {
                if (m_cachedRect == null) return;

                var outLocation = m_characterSide == CharacterSide.Right?
                    new Vector2(m_sizeDelta.x, m_originalPosition.y):
                    new Vector2(-m_sizeDelta.x, m_originalPosition.y);

                m_cachedRect.anchoredPosition = outLocation;

                return;
            }

            const float duration = 0.16f;

            if (characterAnimator)
            {
                characterAnimator.image.DOColor("#B4B4B4".ToColor(), duration);
            }

            if (trueShadow)
            {
                DOTween.To(() => trueShadow.OffsetDistance, offset => trueShadow.OffsetDistance = offset, 3, duration);
            }

            if (m_cachedRect)
            {
                m_cachedRect.DOAnchorPosX(m_originalPosition.x + (m_characterSide == CharacterSide.Right ? 4 : -4),
                    duration);
            }

            if (nameContainer && nameContainer.GetCurrentAnimatorStateInfo(0).IsName("Open"))
            {
                nameContainer.Play("Close");
            }

            if (speechTail)
            {
                speechTail.SetActive(false);
            }
        }

        public override void SetEmote(string emoteAnim)
        {
            if (string.IsNullOrEmpty(emoteAnim) || emoteAnim.Equals("talking"))
            {
                emoteAnim = "idle";
            }

            if (emoteAnim.Equals("confused") && m_characterSide == CharacterSide.Right)
            {
                emoteAnim = "confused-right";
            }

            if (emoteAnim.Equals("embarassed"))
            {
                emoteAnim = "embarrassed";
            }

            if (characterAnimator && CharacterData!=null && CharacterData.id != DialogueInfo.Empty)
            {
                var portrait = CharacterData.portraitUrl;

                if (string.IsNullOrEmpty(portrait))
                {
                    portrait = CharacterData.id.ToLower();
                }

                if (!characterAnimator.Play($"{portrait}-{emoteAnim}"))
                {
                    Debug.LogError($"Could not find animation {portrait}-{emoteAnim}");
                }
            }
        }

        public override void OnBounceCharacter(IMessage message)
        {

        }

        public override void RemoveEmote()
        {
            // can't remove emotes on a cut scene character
        }
    }
}