using System.Collections.Generic;
using Asyncoroutine;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI;
using CrossBlitz.Databases;
using CrossBlitz.Dialogue;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.ServerAPI;
using CrossBlitz.ServerAPI.GameLogic;
using MEC;
using UnityEngine;

namespace CrossBlitz.Fables.Cutscene
{
    public class MatchDialogueStage : DialogueStage
    {
        public GameplayScreen gameplayScreen;

        private bool m_isPlaying;
        private int m_currentDialogueIndex;
        private MatchCutsceneCharacter m_currentCharacter;
        private List<CutsceneDialogueInfo> m_currentDialogues;
        private Dictionary<string, MatchCutsceneCharacter> m_characters;

        public bool Playing => m_isPlaying;

        public void Init()
        {

        }

        public void PlayCutscene(CutsceneData cutsceneData, bool userInput = false)
        {
            if (this == null)
            {
                return;
            }

            if (Playing)
            {
                return;
            }

            if (cutsceneData == null)
            {
                return;
            }

            m_characters = new Dictionary<string, MatchCutsceneCharacter>();
            m_cutsceneData = cutsceneData;
            Timing.RunCoroutine( PlayDialogue(m_cutsceneData.DialogueInfo, userInput).CancelWith(gameObject), CommandResolver.ResolvingTag );

        }

        private MatchCutsceneCharacter GetCharacter(string cardId)
        {
            if (m_characters.TryGetValue(cardId, out var character))
            {
                return character;
            }

            var cardsWithId = GameServer.State.GetCardsWithId(cardId);

            if (cardsWithId.Count <= 0)
            {
                Debug.LogError($"Could find card {cardId}.");
                return null;
            }

            cardsWithId.RemoveAll(c => c.location != CardLocation.Board);

            if (cardsWithId.Count <= 0)
            {
                Debug.LogError($"Could find card {cardId} on the board!");
                return null;
            }

            var heroCard = cardsWithId.Find(c =>
                c.uid == GameServer.ClientPlayerState.heroUid || c.uid == GameServer.OpponentPlayerState.heroUid);

            var randomCard = heroCard == null ? cardsWithId[Random.Range(0, cardsWithId.Count)] : heroCard;

            if (randomCard.IsHero || randomCard.uid == GameServer.ClientPlayerState.heroUid || randomCard.uid == GameServer.OpponentPlayerState.heroUid)
            {
                var heroView = randomCard.Team == Team.Client ?
                    GameplayScreen.Instance.heroSideBarContainer.playerHeroView :
                    GameplayScreen.Instance.heroSideBarContainer.opponentHeroView;

                var matchCutsceneCharacter = heroView.GetOrAddComponent<MatchCutsceneCharacter>();
                matchCutsceneCharacter.Init();
                matchCutsceneCharacter.SetCardView(heroView, null);
                m_characters.Add(cardId, matchCutsceneCharacter);
                return matchCutsceneCharacter;
            }

            var boardCard = GameplayScreen.Instance.GetBoardCard(randomCard.uid, out ClientErrorCode errorCode,
                out CardLocation errorLocation);

            if (boardCard != null)
            {
                var matchCutsceneCharacter = boardCard.GetOrAddComponent<MatchCutsceneCharacter>();
                matchCutsceneCharacter.Init();
                matchCutsceneCharacter.SetCardView(null, boardCard.View);
                m_characters.Add(cardId, matchCutsceneCharacter);
                return matchCutsceneCharacter;
            }

            Debug.LogError($"Error: {errorCode} : Location: {errorLocation}");

            return null;
        }

        public IEnumerator<float> PlayDialogue(List<CutsceneDialogueInfo> dialogues, bool userInput)
        {
            m_isPlaying = true;
            m_currentDialogues = dialogues;
            m_currentDialogueIndex = 0;

            for (var i = 0; i < m_currentDialogues.Count; i++)
            {
                m_currentDialogueIndex = i;
                var curr = m_currentDialogues[m_currentDialogueIndex];
                var currentCharacterValue = curr.GetCharacterValues(m_cutsceneData);

                if (dialogues.Count > i + 1)
                {
                    curr.NextIsSameCharacter = dialogues[i + 1].GetCharacterValues(m_cutsceneData).id.Equals(currentCharacterValue.id);
                }

                var lastCharacter = m_currentCharacter;

                m_currentCharacter = GetCharacter(currentCharacterValue.id);

                if (m_currentCharacter == null)
                {
                    Debug.LogError($"Cutscene character null: {currentCharacterValue.id}");
                    continue;
                }

                // var task = m_currentCharacter.GetOrCreateDialogueBox();
                // yield return Timing.WaitUntilDone(task.AsCoroutine());
                // m_cutsceneDialogueBox = task.Result;

                curr.IsFirst = i == 0;
                curr.SameCharacterFromLast = m_currentCharacter == lastCharacter;

                AudioController.PlaySound("battle_card_dialogue", "BARDSFX", true);

                if (!curr.SameCharacterFromLast)
                {

                    m_currentCharacter.AnimateStartTalking(curr, curr.UsesThoughtBubble);
                    m_currentCharacter.SetCharacterData(currentCharacterValue.id, currentCharacterValue.overrideName, currentCharacterValue.side);

                    if (lastCharacter) lastCharacter.AnimateEndTalking();
                }

                yield return Timing.WaitForSeconds(0.04f);

                if (lastCharacter != null && lastCharacter.IsValid())
                {
                    var characterIndex = m_cutsceneData.RightSideCharacters.IndexOf(lastCharacter.CharacterData.id);

                    if (characterIndex < 0)
                    {
                        characterIndex = m_cutsceneData.LeftSideCharacters.IndexOf(lastCharacter.CharacterData.id);
                        lastCharacter.SetEmote(curr.LeftSideEmotes[characterIndex]);
                    }
                    else
                    {
                        lastCharacter.SetEmote(curr.RightSideEmotes[characterIndex]);
                    }
                }

                if (m_currentCharacter != null && m_currentCharacter.IsValid())
                {
                    var characterIndex = m_cutsceneData.RightSideCharacters.IndexOf(m_currentCharacter.CharacterData.id);

                    if (characterIndex < 0)
                    {
                        characterIndex = m_cutsceneData.LeftSideCharacters.IndexOf(m_currentCharacter.CharacterData.id);
                        m_currentCharacter.SetEmote(curr.LeftSideEmotes[characterIndex]);
                    }
                    else
                    {
                        m_currentCharacter.SetEmote(curr.RightSideEmotes[characterIndex]);
                    }
                }

                curr.SkipPressedActions = true;
                yield return Timing.WaitUntilDone(m_currentCharacter.ShowText(curr, userInput).CancelWith(gameObject), "cutscene");
            }

            m_isPlaying = false;
        }
    }
}