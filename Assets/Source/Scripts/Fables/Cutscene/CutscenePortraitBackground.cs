using System.Collections.Generic;
using System.Threading.Tasks;
using CrossBlitz.AssetManagement;
using CrossBlitz.Audio;
using MEC;
using UnityEngine;

namespace CrossBlitz.Fables.Cutscene
{
    public class CutscenePortraitBackground : MonoBehaviour
    {
        public Animator animator;
        public RectTransform maskParent;

        private GameObject _loadedBackground;

        public async Task LoadPortraitBackground(string locationId)
        {
            if (string.IsNullOrEmpty(locationId) || locationId.Contains("CutsceneLocation-"))
            {
                locationId = "SunsetValley";
            }

            _loadedBackground = await AddressableReferenceLoader.Load( "CutsceneLocation-" + locationId, maskParent);
            _loadedBackground.RectTransform().anchoredPosition = Vector2.zero;
        }

        public IEnumerator<float> AnimateIn()
        {        
            AudioController.PlaySound("cutscene_bgd_open", "BARDSFX", true);
            animator.Play("Open");

            while (animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1)
            {
                yield return Timing.WaitForOneFrame;
            }
        }

        public IEnumerator<float> AnimateOut()
        {
            animator.Play("Close");

            while (animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1)
            {
                yield return Timing.WaitForOneFrame;
            }

            gameObject.SetActive(false);
        }
    }
}