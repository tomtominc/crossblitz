using System.Collections.Generic;
using CrossBlitz.Dialogue;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Utils;
using Newtonsoft.Json;
using UnityEngine;

namespace CrossBlitz.Fables.Cutscene.Data
{
    [System.Serializable]
    public class DialogueSnippetData : UniqueItem
    {
        public string Option;
        public string Emotion = "talking";
        public string Dialogue;
        public string RoomUid;
        public string SoundFx;
    }

    public enum CutsceneType
    {
        HexMapDialogue,
        EventScene,
        Cutscene,
    }

    public enum CharacterSide
    {
        Right,
        Left
    }

    [System.Serializable]
    public class CutsceneData : UniqueItem
    {
        public CutsceneType CutsceneType;
        public string EventTitle;
        public string EventLocation;
        public string EventStageType;
        public string StartingTrack;
        public string StartingAmbient;
        public string QuestUid;
        public bool IncrementQuestStepInstead;
        public int QuestStepChange;
        public List<string> RightSideCharacters;
        public List<string> LeftSideCharacters;
        public List<string> RightCharacterOverrideNames;
        public List<string> LeftCharacterOverrideNames;
        public List<CutsceneDialogueInfo> DialogueInfo;
        public List<ItemValue> RewardItems;
        public string Folder;

        public static bool IsValid(CutsceneData cutsceneData)
        {
            return cutsceneData != null && cutsceneData.DialogueInfo != null && cutsceneData.DialogueInfo.Count > 0;
        }

        public static List<string> GetCutsceneEmotions()
        {
            return new List<string>
            {
                "talking", "angry", "bored", "confused",
                "determined", "embarrassed", "flirt", "joy",
                "nervous", "sad", "shocked"
            };
        }
    }

    [System.Serializable]
    public class CutsceneDialogueInfo : DialogueInfo
    {
        public CharacterSide CharacterSide;
        public int CharacterIndex;
        public List<string> RightSideEmotes;
        public List<string> LeftSideEmotes;
        public bool HasChoices;
        public bool UsesThoughtBubble;
        public List<CutsceneChoice> Choices;

        // used in game for state checks
        [JsonIgnore][HideInInspector] public bool IsFirst;
        [JsonIgnore][HideInInspector] public bool NextIsSameCharacter;
        [JsonIgnore][HideInInspector] public bool SameCharacterFromLast;

        public CharacterValues GetCharacterValues(CutsceneData cutsceneData)
        {
            var characterId = CharacterSide == CharacterSide.Right
                ? cutsceneData.RightSideCharacters[CharacterIndex]
                : cutsceneData.LeftSideCharacters[CharacterIndex];

            var overrideName = CharacterSide == CharacterSide.Right
                ? cutsceneData.RightCharacterOverrideNames[CharacterIndex]
                : cutsceneData.LeftCharacterOverrideNames[CharacterIndex];

            return new CharacterValues {id = characterId, overrideName = overrideName, side = CharacterSide};
        }
    }

    [System.Serializable]
    public class CutsceneChoice
    {
        public string Choice;
        public string CutsceneUid;
    }

    [System.Serializable]
    public class CutsceneEmoteData
    {
        public string EmoteId;
        public string CharacterId;
    }

    public struct CharacterValues
    {
        public string id;
        public string overrideName;
        public CharacterSide side;
    }
}