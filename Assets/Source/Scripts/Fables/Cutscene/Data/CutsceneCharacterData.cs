using UnityEngine;

namespace CrossBlitz.Fables.Cutscene.Data
{
    // [System.Serializable]
    // public class CutsceneCharacterData
    // {
    //     /// <summary>
    //     /// The characters Id, this is the GDE Data id
    //     /// </summary>
    //     public string CharacterId;
    //     /// <summary>
    //     /// The name of the character, takes formatting for colors
    //     /// </summary>
    //     public string CharacterName;
    //     /// <summary>
    //     /// The portrait of the character, for small cutscenes this is the cards portrait Id
    //     /// for large cutscenes this is the bigger portrait
    //     /// </summary>
    //     public string PortraitUri;
    //     /// <summary>
    //     /// The face of the character in the small cutscenes, not used in the big ones.
    //     /// </summary>
    //     public string DialoguePortrait;
    //     /// <summary>
    //     /// The background of the portrait, just shades of different color for heroes (probably can remove this)
    //     /// </summary>
    //     public string DialoguePortraitBackground;
    //     /// <summary>
    //     /// The initial emotion for the character so both characters aren't just idle when the game starts.
    //     /// </summary>
    //     public string InitialEmotion;
    //     /// <summary>
    //     /// The offset of the card portrait for small views, not used in the big ones.
    //     /// </summary>
    //     public Vector2 Offset;
    //     /// <summary>
    //     /// The scale this should be at, can use side now.
    //     /// </summary>
    //     public int ScaleX;
    // }
}