using System;
using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.Hero;
using DG.Tweening;
using GameDataEditor;
using MEC;
using Sirenix.OdinInspector;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Fables.Cutscene
{
    public class HeroCutscenePortraitAnimator : MonoBehaviour
    {
        public CanvasGroup screenOverlay;
        public SpriteAnimation cutscenePortrait;
        public SpriteAnimation cutscenePortraitShadow;
        public SpriteAnimation cutscenePortraitOverlay;
        public SpriteAnimation levelUpEffect;

        [BoxGroup("Dialogue")] public CanvasGroup dialogueBoxFader;
        [BoxGroup("Dialogue")] public RectTransform dialogueBox;
        [BoxGroup("Dialogue")] public TextMeshProUGUI dialogue;

        public List<SpriteAnimation> levelUpStarsRight;
        public List<SpriteAnimation> levelUpStarsLeft;

        private string _heroId;
        private string _factionLower;
        private float _moveBackToIdleAfterSeconds;
        private bool m_fadeOutDialogue;

        private void Start()
        {
            Events.Subscribe(EventType.OnHeroLevelChanged, OnHeroLevelChanged);
            Events.Subscribe(EventType.OnHeroVisuallyLeveledUp, OnHeroVisuallyLeveledUp);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnHeroLevelChanged, OnHeroLevelChanged);
            Events.Unsubscribe(EventType.OnHeroVisuallyLeveledUp, OnHeroVisuallyLeveledUp);
        }

        private void Update()
        {
            if (_moveBackToIdleAfterSeconds > 0)
            {
                _moveBackToIdleAfterSeconds -= Time.deltaTime;

                if (_moveBackToIdleAfterSeconds <= 0)
                {
                    _moveBackToIdleAfterSeconds = 0;
                    AnimateBackToIdle();
                }
            }
        }

        public void SetDialogue(string dialogueText)
        {
            dialogueBox.DOKill();
            dialogueBoxFader.DOKill();

            dialogueBox.SetActive(true);
            dialogueBoxFader.alpha = 1;
            dialogueBox.localScale = Vector3.zero;
            dialogue.text = dialogueText;

            dialogueBox.DOScale(Vector3.one, 0.2f).SetEase(Ease.OutBack);
            dialogueBoxFader.DOFade(0, 0.4f).SetDelay(2);
        }

        public void SetHeroId(string heroId)
        {
            _heroId = heroId;
            _factionLower = HeroData.GetFactionForHero(_heroId).ToString().ToLower();
            cutscenePortrait.Play($"{_heroId.ToLower()}-idle");
            cutscenePortraitShadow.Play($"{_heroId.ToLower()}-idle");
        }

        private void AnimateBackToIdle()
        {
            AnimateToEmotion("idle");
        }

        public void AnimateToEmotion(string emotion, bool shouldMoveBackToIdle = false)
        {
            if (string.IsNullOrEmpty(_heroId))
            {
                _heroId = GDEItemKeys.Hero_Redcroft;
            }

            if (emotion == "embarassed") emotion = "embarrassed";

            _moveBackToIdleAfterSeconds = -1;

            var characterPortraitRect = cutscenePortrait.RectTransform();
            var characterPortraitRectShadow = cutscenePortraitShadow.RectTransform();

            var currentScaleX = characterPortraitRect.transform.localScale.x;

            Sequence punchSeq = DOTween.Sequence();
            punchSeq.Append(characterPortraitRect.DOScale(new Vector3(1.06f * currentScaleX, 0.97f), 0.08f));
            punchSeq.AppendCallback(() =>
            {
                cutscenePortrait.Play($"{_heroId.ToLower()}-{emotion}");
            });
            punchSeq.Append(characterPortraitRect.DOScale(new Vector3(0.97f * currentScaleX, 1.06f), 0.08f));
            punchSeq.Append(characterPortraitRect.DOScale(new Vector3(currentScaleX, 1f), 0.08f));

            Sequence punchSeq2 = DOTween.Sequence();
            punchSeq2.Append(characterPortraitRectShadow.DOScale(new Vector3(1.06f * currentScaleX, 0.97f), 0.08f));
            punchSeq2.AppendCallback(() =>
            {
                cutscenePortraitShadow.Play($"{_heroId.ToLower()}-{emotion}");
            });
            punchSeq2.Append(characterPortraitRectShadow.DOScale(new Vector3(0.97f * currentScaleX, 1.06f), 0.08f));
            punchSeq2.Append(characterPortraitRectShadow.DOScale(new Vector3(currentScaleX, 1f), 0.08f));

            if (shouldMoveBackToIdle)
            {
                _moveBackToIdleAfterSeconds = 2;
            }
        }

        public IEnumerator<float> AnimateLevelUp(DialogueSnippetData dialogueSnippet = null)
        {
            _moveBackToIdleAfterSeconds = -1;

            var characterPortraitRect = cutscenePortrait.RectTransform();
            var characterPortraitRectShadow = cutscenePortraitShadow.RectTransform();
            var characterPortraitRectOverlay = cutscenePortraitOverlay.RectTransform();

            screenOverlay.SetActive(true);
            screenOverlay.GetComponent<Image>().color = Color.white;
            screenOverlay.alpha = 0.5f;

            cutscenePortraitOverlay.SetActive(true);
            cutscenePortraitOverlay.image.color = Color.white;

            var emotion = "determined";

            if (dialogueSnippet != null)
            {
                if (!string.IsNullOrEmpty(dialogueSnippet.Dialogue))
                {
                    SetDialogue(dialogueSnippet.Dialogue);
                }

                if (!string.IsNullOrEmpty(dialogueSnippet.SoundFx))
                {
                    AudioController.PlaySound(dialogueSnippet.SoundFx);
                }

                if (!string.IsNullOrEmpty(dialogueSnippet.Emotion))
                {
                    emotion = dialogueSnippet.Emotion;
                }
            }

            var currentScaleX = characterPortraitRect.transform.localScale.x;

            Sequence punchSeq = DOTween.Sequence();
            punchSeq.Append(characterPortraitRect.DOScale(new Vector3(1.06f * currentScaleX, 0.97f), 0.08f));
            punchSeq.AppendCallback(() =>
            {
                cutscenePortrait.Play($"{_heroId.ToLower()}-{emotion}");
            });
            punchSeq.Append(characterPortraitRect.DOScale(new Vector3(0.97f * currentScaleX, 1.06f), 0.04f));
            punchSeq.Append(characterPortraitRect.DOScale(new Vector3(currentScaleX, 1f), 0.04f));

            Sequence punchSeq2 = DOTween.Sequence();
            punchSeq2.Append(characterPortraitRectShadow.DOScale(new Vector3(1.06f * currentScaleX, 0.97f), 0.08f));
            punchSeq2.AppendCallback(() =>
            {
                cutscenePortraitShadow.Play($"{_heroId.ToLower()}-{emotion}");
            });
            punchSeq2.Append(characterPortraitRectShadow.DOScale(new Vector3(0.97f * currentScaleX, 1.06f), 0.04f));
            punchSeq2.Append(characterPortraitRectShadow.DOScale(new Vector3(currentScaleX, 1f), 0.04f));

            Sequence punchSeq3 = DOTween.Sequence();
            punchSeq3.Append(characterPortraitRectOverlay.DOScale(new Vector3(1.06f * currentScaleX, 0.97f), 0.08f));
            punchSeq3.AppendCallback(() =>
            {
                var factionColor =ColorPalette.CrossBlitz.GetFactionColor(HeroData.GetFactionForHero(_heroId)).ToColor();
                cutscenePortraitOverlay.Play($"{_heroId.ToLower()}-{emotion}");
                screenOverlay.GetComponent<Image>().color = factionColor;
                screenOverlay.alpha = 0.5f;
                cutscenePortraitOverlay.image.color = factionColor;
            });
            punchSeq3.Append(characterPortraitRectOverlay.DOScale(new Vector3(0.97f * currentScaleX, 1.06f), 0.04f));
            punchSeq3.Append(characterPortraitRectOverlay.DOScale(new Vector3(currentScaleX, 1f), 0.04f));
            punchSeq3.AppendCallback(() =>
            {
                cutscenePortraitOverlay.SetActive(false);
                screenOverlay.DOFade(0, 0.5f);
            });

            levelUpEffect.SetActive(true);
            levelUpEffect.Play(_factionLower);

            var originalPositionsLeft = new List<Vector2>();
            var originalPositionsRight = new List<Vector2>();

            for (var i = 0; i < levelUpStarsLeft.Count; i++)
            {
                levelUpStarsLeft[i].SetActive(true);
                levelUpStarsLeft[i].Play(_factionLower);
                originalPositionsLeft.Add(levelUpStarsLeft[i].transform.position);
                levelUpStarsLeft[i].GetComponent<Rigidbody2D>().AddForce(new Vector2(-1, 1) * 4, ForceMode2D.Impulse);
            }

            for (var i = 0; i < levelUpStarsRight.Count; i++)
            {
                levelUpStarsRight[i].SetActive(true);
                levelUpStarsRight[i].Play(_factionLower);
                originalPositionsRight.Add(levelUpStarsRight[i].transform.position);
                levelUpStarsRight[i].GetComponent<Rigidbody2D>().AddForce(new Vector2(1, 1) * 4, ForceMode2D.Impulse);
            }

            while (!levelUpStarsLeft[0].IsDone) yield return Timing.WaitForOneFrame;

            for (var i = 0; i < levelUpStarsLeft.Count; i++)
            {
                levelUpStarsLeft[i].SetActive(false);
                levelUpStarsLeft[i].transform.position = originalPositionsLeft[i];
            }

            for (var i = 0; i < levelUpStarsRight.Count; i++)
            {
                levelUpStarsRight[i].SetActive(false);
                levelUpStarsRight[i].transform.position = originalPositionsRight[i];
            }

            while (!levelUpEffect.IsDone) yield return Timing.WaitForOneFrame;

            levelUpEffect.SetActive(false);

            _moveBackToIdleAfterSeconds = 2;
        }

        private void OnHeroLevelChanged(IMessage message)
        {
            var eventArgs = (OnHeroLevelChangedEventArgs) message.Data;

            if (eventArgs.heroId == _heroId)
            {
                Timing.RunCoroutine(AnimateLevelUp());
            }
        }

        private void OnHeroVisuallyLeveledUp(IMessage message)
        {
            var eventArgs = (OnHeroVisuallyLeveledUpEventArgs) message.Data;

            if (eventArgs.heroId == _heroId)
            {
                Timing.RunCoroutine(AnimateLevelUp());
            }
        }

    }
}