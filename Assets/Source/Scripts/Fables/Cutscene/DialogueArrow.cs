using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossBlitz
{
    public class DialogueArrow : MonoBehaviour
    {
        public CanvasGroup arrowCanvas;

        private void Start()
        {
            //arrowCanvas = GetComponent<CanvasGroup>();
        }

        public void FadeOut()
        {
            arrowCanvas.DOFade(0, .1f);
        }

        public void FadeIn()
        {
            arrowCanvas.DOFade(1, .1f);
        }
    }
}
