using CrossBlitz.Fables.Cutscene.Data;
using UnityEngine;

namespace CrossBlitz.Fables.Cutscene
{
    public class DialogueStage : MonoBehaviour
    {
        protected CutsceneData m_cutsceneData;
        protected int m_currentRightSidedCharacter;
        protected int m_currentLeftSidedCharacter;

        protected virtual void SetupCharacters()
        {

        }

        public virtual void OnEnd()
        {

        }

        public virtual void OnSkipToChoices()
        {

        }

    }
}