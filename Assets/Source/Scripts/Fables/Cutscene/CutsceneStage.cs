using System;
using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.Dialogue;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.Fables.Grid;
using CrossBlitz.Items.Data;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ViewAPI.Popups;
using MEC;
using UnityEngine;

namespace CrossBlitz.Fables.Cutscene
{
    public class CutsceneStage : DialogueStage
    {
        public GameObject portraitsContainer;
        public CutsceneCharacter rightCharacter;
        public CutsceneCharacter leftCharacter;
        public CutsceneCharacter emptyCharacter;
        public CutsceneDialogueBox cutsceneDialogueBox;
        public NarratorDialogueBox narratorDialogueBox;
        public IllustrationDialogueBox illustrationDialogueBox;

        private int m_currentDialogueIndex;
        private List<CutsceneDialogueInfo> m_currentDialogues;
        private string m_selectedChoiceDialogueUid;
        private bool m_skippable;
  
        public int dialogueIndex = 0;
        public int choiceDialogueIndex = 0;
        public int skipToDialogueIndex = 0;
        public bool useCutsceneDialogueBox = true;
        public bool madeChoice = false;

        public string SelectedChoiceDialogueUid => m_selectedChoiceDialogueUid;

        public IEnumerator<float> PlayDialogue(CutsceneData cutsceneData)
        {
            m_cutsceneData = cutsceneData;

            portraitsContainer.SetActive(true);

            cutsceneDialogueBox.SetActive(true);
            cutsceneDialogueBox.InitDialogueBox(cutsceneData);

            narratorDialogueBox.SetActive(false);
            narratorDialogueBox.InitDialogueBox(cutsceneData);

            illustrationDialogueBox.SetActive(false);
            illustrationDialogueBox.InitDialogueBox(cutsceneData);

            UpdateQuest();
            SetupCharacters();

            yield return Timing.WaitUntilDone(PlayUntilEnd().CancelWith(gameObject), "cutscene");
        }

        public bool CanSkip()
        {
            return m_skippable;
        }

        public override void OnEnd()
        {
            if (narratorDialogueBox) narratorDialogueBox.SetActive(false);
            if (illustrationDialogueBox) illustrationDialogueBox.SetActive(false);
        }

        private void UpdateQuest()
        {
            if (FablesMapController.Instance && FablesMapController.Instance.ClientChapterData!= null)
            {
                var chapter = FablesMapController.Instance.ClientChapterData;
                if (chapter == null) chapter = App.FableData.GetCurrentChapter();
                if (chapter != null) chapter.ResolveQuestIfApplicable(m_cutsceneData);
            }
        }

        public bool IsOnChoiceDialogue()
        {
            if (m_currentDialogues != null && m_currentDialogueIndex < m_currentDialogues.Count)
            {
                return m_currentDialogues[m_currentDialogueIndex].HasChoices;
            }

            return false;
        }

        public bool TryGetChoiceDialogue(out List<CutsceneDialogueInfo> choiceDialogue)
        {
            var hasChoices = false;
            choiceDialogue = new List<CutsceneDialogueInfo>();

            if (m_currentDialogues != null)
            {
                for (var i = m_currentDialogueIndex; i < m_currentDialogues.Count; i++)
                {
                    var dialogue = m_currentDialogues[i];

                    if (dialogue.HasChoices)
                    {
                        hasChoices = true;
                        choiceDialogue.Add(dialogue);
                        choiceDialogueIndex = i;
                        break;
                    }
                }
            }

            return hasChoices;
        }

        public override void OnSkipToChoices()
        {
            m_skippable = false;

            dialogueIndex = choiceDialogueIndex -1;
            skipToDialogueIndex = dialogueIndex;

            cutsceneDialogueBox.IsReady = true;
            narratorDialogueBox.IsReady = true;
            illustrationDialogueBox.IsReady = true;

            cutsceneDialogueBox.OnSkippedToChoice();
            narratorDialogueBox.OnSkippedToChoice();
            illustrationDialogueBox.OnSkippedToChoice();

            Timing.RunCoroutine(illustrationDialogueBox.SimplyFadeOut().CancelWith(illustrationDialogueBox), "cutscene");
            Timing.RunCoroutine(narratorDialogueBox.SimplyFadeOut().CancelWith(narratorDialogueBox), "cutscene");
        }

        public void OnSkipped()
        {
            m_skippable = false;

            dialogueIndex = m_currentDialogues.Count;
            skipToDialogueIndex = dialogueIndex;

            cutsceneDialogueBox.IsReady = true;
            narratorDialogueBox.IsReady = true;
            illustrationDialogueBox.IsReady = true;

            cutsceneDialogueBox.OnSkipped();
            narratorDialogueBox.OnSkipped();
            illustrationDialogueBox.OnSkipped();

            Timing.RunCoroutine(illustrationDialogueBox.SimplyFadeOut().CancelWith(illustrationDialogueBox), "cutscene");
            Timing.RunCoroutine(narratorDialogueBox.SimplyFadeOut().CancelWith(narratorDialogueBox), "cutscene");
        }

        protected override void SetupCharacters()
        {
            CutsceneDialogueInfo initialCharacterDialogue=null;
            CutsceneDialogueInfo initialRightCharacterDialogue=null;
            CutsceneDialogueInfo initialLeftCharacterDialogue=null;

            if (m_cutsceneData?.DialogueInfo?.Count > 0)
            {
                for (var i = 0; i < m_cutsceneData.DialogueInfo.Count; i++)
                {
                    if (m_cutsceneData.DialogueInfo[i].CharacterSide == CharacterSide.Right &&
                        m_cutsceneData.RightSideCharacters[m_cutsceneData.DialogueInfo[i].CharacterIndex] != DialogueInfo.Narrator &&
                        m_cutsceneData.RightSideCharacters[m_cutsceneData.DialogueInfo[i].CharacterIndex] != DialogueInfo.Illustration)
                    {
                        initialCharacterDialogue = m_cutsceneData.DialogueInfo[i];
                        break;
                    }

                    if (m_cutsceneData.DialogueInfo[i].CharacterSide == CharacterSide.Left &&
                        m_cutsceneData.LeftSideCharacters[m_cutsceneData.DialogueInfo[i].CharacterIndex] != DialogueInfo.Narrator &&
                        m_cutsceneData.LeftSideCharacters[m_cutsceneData.DialogueInfo[i].CharacterIndex] != DialogueInfo.Illustration)
                    {
                        initialCharacterDialogue = m_cutsceneData.DialogueInfo[i];
                        break;
                    }
                }
            }

            if (m_cutsceneData?.RightSideCharacters?.Count > 0 && m_cutsceneData?.DialogueInfo?.Count > 0)
            {
                for (var i = 0; i < m_cutsceneData.DialogueInfo.Count; i++)
                {
                    if (m_cutsceneData.DialogueInfo[i].CharacterSide == CharacterSide.Right &&
                        m_cutsceneData.RightSideCharacters[m_cutsceneData.DialogueInfo[i].CharacterIndex] != DialogueInfo.Narrator &&
                        m_cutsceneData.RightSideCharacters[m_cutsceneData.DialogueInfo[i].CharacterIndex] != DialogueInfo.Illustration)
                    {
                        initialRightCharacterDialogue = m_cutsceneData.DialogueInfo[i];
                        break;
                    }
                }

                if (initialRightCharacterDialogue!=null)
                {
                    rightCharacter.Init();
                    rightCharacter.SetCharacterData(
                        m_cutsceneData.RightSideCharacters[initialRightCharacterDialogue.CharacterIndex],
                        m_cutsceneData.RightCharacterOverrideNames[initialRightCharacterDialogue.CharacterIndex],
                        initialRightCharacterDialogue.CharacterSide);
                }
                else
                {
                    rightCharacter.SetActive(false);
                }
            }

            if (m_cutsceneData?.LeftSideCharacters?.Count > 0 && m_cutsceneData?.DialogueInfo?.Count > 0)
            {
                for (var i = 0; i < m_cutsceneData.DialogueInfo.Count; i++)
                {
                    if (m_cutsceneData.DialogueInfo[i].CharacterSide == CharacterSide.Left &&
                        m_cutsceneData.LeftSideCharacters[m_cutsceneData.DialogueInfo[i].CharacterIndex] != DialogueInfo.Narrator &&
                        m_cutsceneData.LeftSideCharacters[m_cutsceneData.DialogueInfo[i].CharacterIndex] != DialogueInfo.Illustration)
                    {
                        initialLeftCharacterDialogue = m_cutsceneData.DialogueInfo[i];
                        break;
                    }
                }

                if (initialLeftCharacterDialogue!=null)
                {
                    leftCharacter.Init();
                    leftCharacter.SetCharacterData(
                        m_cutsceneData.LeftSideCharacters[initialLeftCharacterDialogue.CharacterIndex],
                        m_cutsceneData.LeftCharacterOverrideNames[initialLeftCharacterDialogue.CharacterIndex],
                        initialLeftCharacterDialogue.CharacterSide);
                }
                else
                {
                    leftCharacter.SetActive(false);
                }
            }

            if (initialCharacterDialogue!=null)
            {
                if (initialRightCharacterDialogue != null)
                {
                    rightCharacter.SetEmote(initialCharacterDialogue.RightSideEmotes[initialRightCharacterDialogue.CharacterIndex]);
                }

                if (initialLeftCharacterDialogue != null)
                {
                    leftCharacter.SetEmote(initialCharacterDialogue.LeftSideEmotes[initialLeftCharacterDialogue.CharacterIndex]);
                }
            }
        }

        public IEnumerator<float> AnimateOut()
        {
            if (rightCharacter.IsValid()) rightCharacter.AnimateOut();
            if (leftCharacter.IsValid()) leftCharacter.AnimateOut();

            yield return Timing.WaitForSeconds(0.25f);
            yield return Timing.WaitUntilDone(cutsceneDialogueBox.AnimateOut().CancelWith(gameObject), "cutscene");

            portraitsContainer.SetActive(false);
            cutsceneDialogueBox.SetActive(false);
        }

        private IEnumerator<float> PlayUntilEnd()
        {
            if (useCutsceneDialogueBox)
            {
                Timing.RunCoroutine(cutsceneDialogueBox.AnimateIn(null).CancelWith(gameObject), "cutscene");
            }
            else
            {
                cutsceneDialogueBox.setReady(true);
            }

            yield return Timing.WaitForSeconds(0.24f);

            if (leftCharacter.IsValid() && leftCharacter.CharacterData.id != DialogueInfo.Empty)
            {
                Timing.RunCoroutine(leftCharacter.AnimateIn().CancelWith(gameObject), "cutscene");
                yield return Timing.WaitForSeconds(0.15f);
            }

            if (rightCharacter.IsValid() && rightCharacter.CharacterData.id != DialogueInfo.Empty)
            {
                Timing.RunCoroutine(rightCharacter.AnimateIn().CancelWith(gameObject), "cutscene");
                yield return Timing.WaitForSeconds(0.15f);
            }

            while (cutsceneDialogueBox.IsReady == false) yield return Timing.WaitForOneFrame;
            yield return Timing.WaitForSeconds(0.15f);

            m_skippable = true;

            yield return Timing.WaitUntilDone(PlayDialogue(m_cutsceneData.DialogueInfo).CancelWith(gameObject), "cutscene" );

        }

        public IEnumerator<float> PlayDialogue(List<CutsceneDialogueInfo> dialogues)
        {
            //Debug.LogError($"==PLAY DIALOGUE== ({dialogues.Count})");

            m_currentDialogues = dialogues;

            m_currentDialogueIndex = skipToDialogueIndex;

            for (dialogueIndex = skipToDialogueIndex; dialogueIndex < m_currentDialogues.Count; dialogueIndex++)
            {
                //Debug.LogError($"==ITERATE OVER== ({i+1}/{m_currentDialogues.Count}) {GetInstanceID()}");

                m_currentDialogueIndex = dialogueIndex;

                var curr = m_currentDialogues[m_currentDialogueIndex];
                var currentCharacterValue = curr.GetCharacterValues(m_cutsceneData);

                if (dialogues.Count > dialogueIndex + 1)
                {
                    curr.NextIsSameCharacter = dialogues[dialogueIndex + 1].GetCharacterValues(m_cutsceneData).id.Equals(currentCharacterValue.id);
                }

                if (currentCharacterValue.id == DialogueInfo.Empty)
                {
                    if (currentCharacterValue.side == CharacterSide.Right)
                    {
                        emptyCharacter = rightCharacter;
                    }
                    else if (currentCharacterValue.side == CharacterSide.Left)
                    {
                        emptyCharacter = leftCharacter;
                    }

                    if (emptyCharacter)
                    {
                        if (dialogues[dialogueIndex].Text.Contains("slow-exit"))
                        {
                            yield return Timing.WaitUntilDone(emptyCharacter.AnimateOutRoutineSlow().CancelWith(emptyCharacter), "cutscene");
                        }
                        else
                        {
                            yield return Timing.WaitUntilDone(emptyCharacter.AnimateOutRoutine().CancelWith(emptyCharacter), "cutscene");
                        }


                        emptyCharacter.SetCharacterData(currentCharacterValue.id, currentCharacterValue.overrideName, currentCharacterValue.side);

                        if (emptyCharacter.nameContainer && emptyCharacter.nameContainer.GetCurrentAnimatorStateInfo(0).IsName("Open"))
                        {
                            emptyCharacter.nameContainer.Play("Close");
                        }

                        if (emptyCharacter.speechTail)
                        {
                            emptyCharacter.speechTail.SetActive(false);
                        }
                    }

                    continue;
                }

                if (currentCharacterValue.id == DialogueInfo.Narrator)
                {
                    cutsceneDialogueBox.boxArrow.SetActive(false);
                    cutsceneDialogueBox.dialogue.ShowText(string.Empty);

                    narratorDialogueBox.SetActive(true);
                    yield return Timing.WaitUntilDone(narratorDialogueBox.ShowText(curr).CancelWith(narratorDialogueBox),"cutscene");

                    if (!curr.NextIsSameCharacter)
                    {
                        yield return Timing.WaitUntilDone(narratorDialogueBox.AnimateOut()
                            .CancelWith(narratorDialogueBox),"cutscene");
                        narratorDialogueBox.SetActive(false);
                    }

                    continue;
                }

                if (currentCharacterValue.id == DialogueInfo.Illustration)
                {
                    //Debug.Log("TURN OFF THE CUTSCENEDIALOGUEBOX");
                    cutsceneDialogueBox.boxArrow.SetActive(false);
                    cutsceneDialogueBox.dialogue.ShowText(string.Empty);
                    //Timing.RunCoroutine(cutsceneDialogueBox.AnimateOut());

                    illustrationDialogueBox.SetActive(true);
                    illustrationDialogueBox.isIllustration = true;
                    //illustrationDialogueBox.GetTypewriterBleepSoundPitchIllustration(currentCharacterValue.id);
                    yield return Timing.WaitUntilDone(illustrationDialogueBox.ShowText(curr).CancelWith(illustrationDialogueBox), "cutscene");

                    if (!curr.NextIsSameCharacter)
                    {
                        yield return Timing.WaitUntilDone(illustrationDialogueBox.AnimateOut()
                            .CancelWith(illustrationDialogueBox), "cutscene");
                        illustrationDialogueBox.SetActive(false);
                    }

                    continue;
                }

                // Slide the Player Character onto the scene if they currently are not present for a choice
                if (curr.HasChoices && curr.Choices.Count > 1 && m_currentDialogueIndex > 0 && !madeChoice)
                {
                    var prevDialogue = m_currentDialogues[m_currentDialogueIndex - 1];
                    var prevCharacterValue = prevDialogue.GetCharacterValues(m_cutsceneData);

                    for (var i = 1; prevCharacterValue.id == currentCharacterValue.id; i++)
                    {
                        prevDialogue = m_currentDialogues[m_currentDialogueIndex - i];
                        prevCharacterValue = prevDialogue.GetCharacterValues(m_cutsceneData);
                    }

                    if (leftCharacter.CharacterData.id != prevCharacterValue.id)
                    {
                        yield return Timing.WaitUntilDone(leftCharacter.AnimateOutRoutine().CancelWith(leftCharacter), "cutscene");
                        leftCharacter.SetCharacterData(prevCharacterValue.id, prevCharacterValue.overrideName, CharacterSide.Left);

                        var characterIndex = m_cutsceneData.LeftSideCharacters.IndexOf(leftCharacter.CharacterData.id);
                        var leftSideEmote = curr.LeftSideEmotes[characterIndex];
                        leftCharacter.SetEmote(leftSideEmote);

                        yield return Timing.WaitUntilDone(leftCharacter.AnimateIn().CancelWith(leftCharacter), "cutscene");
                    }
                }

                curr.IsFirst = dialogueIndex == 0;
                curr.SameCharacterFromLast = false;

                var updateLeftCharEmote = true;
                var updateRightCharEmote = true;

                if (currentCharacterValue.side == CharacterSide.Right && rightCharacter.IsValid() && rightCharacter.CharacterData.id != currentCharacterValue.id)
                {
                    yield return Timing.WaitUntilDone(rightCharacter.AnimateOutRoutine().CancelWith(rightCharacter),"cutscene");
                    rightCharacter.SetCharacterData(currentCharacterValue.id, currentCharacterValue.overrideName, currentCharacterValue.side);

                    var characterIndex = m_cutsceneData.RightSideCharacters.IndexOf(rightCharacter.CharacterData.id);
                    var rightSideEmote = curr.RightSideEmotes[characterIndex];
                    rightCharacter.SetEmote(rightSideEmote);

                    yield return Timing.WaitUntilDone(rightCharacter.AnimateIn().CancelWith(rightCharacter),"cutscene");

                    updateLeftCharEmote = false;
                }
                else if (currentCharacterValue.side == CharacterSide.Left && leftCharacter.IsValid() && leftCharacter.CharacterData.id != currentCharacterValue.id)
                {
                    yield return Timing.WaitUntilDone(leftCharacter.AnimateOutRoutine().CancelWith(leftCharacter),"cutscene");
                    leftCharacter.SetCharacterData(currentCharacterValue.id, currentCharacterValue.overrideName, currentCharacterValue.side);

                    var characterIndex = m_cutsceneData.LeftSideCharacters.IndexOf(leftCharacter.CharacterData.id);
                    var leftSideEmote = curr.LeftSideEmotes[characterIndex];
                    leftCharacter.SetEmote(leftSideEmote);

                    yield return Timing.WaitUntilDone(leftCharacter.AnimateIn().CancelWith(leftCharacter),"cutscene");

                    updateRightCharEmote = false;
                }
                else
                {
                    curr.SameCharacterFromLast = true;
                }

                if (currentCharacterValue.side == CharacterSide.Right)
                {
                    rightCharacter.AnimateStartTalking(curr, curr.UsesThoughtBubble);
                    leftCharacter.AnimateEndTalking();
                }
                else if (currentCharacterValue.side == CharacterSide.Left)
                {
                    rightCharacter.AnimateEndTalking();
                    leftCharacter.AnimateStartTalking(curr, curr.UsesThoughtBubble);
                }

                yield return Timing.WaitForSeconds(0.04f);

                if (updateRightCharEmote && rightCharacter.IsValid())
                {
                    var characterIndex = m_cutsceneData.RightSideCharacters.IndexOf(rightCharacter.CharacterData.id);
                    var rightSideEmote = curr.RightSideEmotes[characterIndex];
                    rightCharacter.SetEmote(rightSideEmote);
                }

                if (updateLeftCharEmote && leftCharacter.IsValid())
                {
                    var characterIndex = m_cutsceneData.LeftSideCharacters.IndexOf(leftCharacter.CharacterData.id);
                    var leftSideEmote = curr.LeftSideEmotes[characterIndex];
                    leftCharacter.SetEmote(leftSideEmote);
                }

                //in here I'd setup choice buttons which the player can press...
                yield return Timing.WaitUntilDone(cutsceneDialogueBox.ShowText(curr).CancelWith(gameObject), "cutscene");

                if (curr.HasChoices && curr.Choices.Count > 1 && !madeChoice)
                {
                    m_skippable = false;
                    madeChoice = true;

                    var characterToAnswerId = currentCharacterValue.side == CharacterSide.Right
                    ? m_cutsceneData.LeftSideCharacters
                        [m_cutsceneData.LeftSideCharacters.IndexOf(leftCharacter.CharacterData.id)]
                    : m_cutsceneData.RightSideCharacters[
                        m_cutsceneData.RightSideCharacters.IndexOf(rightCharacter.CharacterData.id)];

                    if (currentCharacterValue.side == CharacterSide.Left)
                    {
                        rightCharacter.AnimateStartTalking(curr,true);
                        leftCharacter.AnimateEndTalking();
                    }
                    else if (currentCharacterValue.side == CharacterSide.Right)
                    {
                        leftCharacter.AnimateStartTalking(curr,true);
                        rightCharacter.AnimateEndTalking();
                    }

                    yield return Timing.WaitForSeconds(0.04f);

                    if (currentCharacterValue.side == CharacterSide.Left)
                    {
                        rightCharacter.SetEmote("confused");
                    }
                    else if (currentCharacterValue.side == CharacterSide.Right)
                    {
                        leftCharacter.SetEmote("confused");
                    }

                    yield return Timing.WaitUntilDone(cutsceneDialogueBox.ShowChoices(m_cutsceneData, curr, characterToAnswerId).CancelWith(gameObject),"cutscene");


                    if (cutsceneDialogueBox.currentChoice > -1)
                    {
                        var choiceDialogue = curr.Choices[cutsceneDialogueBox.currentChoice];

                        if (choiceDialogue != null)
                        {
                            var cutsceneData = Db.CutsceneDatabase.GetCutscene(choiceDialogue.CutsceneUid);

                            if (cutsceneData != null)
                            {
                                m_skippable = true;

                                m_cutsceneData = cutsceneData;
                                m_selectedChoiceDialogueUid = m_cutsceneData.Uid;
                                skipToDialogueIndex = 0;
                                UpdateQuest();

                                yield return Timing.WaitUntilDone(PlayDialogue(m_cutsceneData.DialogueInfo).CancelWith(gameObject),"cutscene");

                                if (m_cutsceneData.RewardItems?.Count > 0)
                                {
                                    var result = App.Inventory.GrantItems(m_cutsceneData.RewardItems);
                                    App.HandleItemGrant(result,null, PurchaseErrorCode.NONE, false);
                                    while (PopupController.IsOpen) yield return Timing.WaitForOneFrame;
                                }

                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}