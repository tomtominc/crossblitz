using CrossBlitz.Fables.Grid;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Fables
{
    public class ExploreActionWindowBattleContent : ExploreActionWindowContent
    {
        public RectTransform difficultyLayout;

        public override void SetTileData(HexTile2D tileData)
        {
            base.SetTileData(tileData);

            for (var i = 0; i < 3; i++)
            {
                var d = difficultyLayout.GetChild(i).GetComponent<Toggle>();
                d.isOn = true;
            }
        }

    }
}