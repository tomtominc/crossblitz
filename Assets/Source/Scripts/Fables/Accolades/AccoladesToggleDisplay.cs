using System.Collections;
using System.Collections.Generic;
using CrossBlitz.Databases;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.ServerAPI.GameLogic;
using DG.Tweening;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Fables.Accolades
{
    public class AccoladesToggleDisplay : MonoBehaviour
    {
        public RectTransform accoladeLayout;
        public AccoladeToggle accoladePrefab;
        public TextMeshProUGUI description;
        public RectTransform completeCheckmark;
        public RectTransform containerTail;
        public RectTransform container;
        public SpriteAnimation accoladeFlash;

        private void Start()
        {
            Timing.RunCoroutine( OnCreatedGame() );
        }

        private IEnumerator<float> OnCreatedGame()
        {
            var tileData = Db.MapDatabase.GetTileByBattle(GameServer.Mode.BattleUid);
            var battleData = Db.BattleDatabase.GetBattleData(GameServer.Mode.BattleUid);

            if (tileData == null) yield break;

            var clientTile = App.FableData.GetTile(tileData.uid);
            var clientBattleData = clientTile.BattleData;

            AccoladeToggle initialSelected =null;

            clientTile.BattleData?.FixDependencies(battleData);

            for (var i = 0; i < battleData.Accolades.Count; i++)
            {
                var accoladeObject = Instantiate(accoladePrefab, accoladeLayout);
                accoladeObject.OnShowAccolade += OnShowAccolade;
                accoladeObject.SetData(battleData.Accolades[i], clientBattleData.Accolades[i], this);
                if (i == 0) initialSelected = accoladeObject;
            }

            yield return Timing.WaitForOneFrame;

            accoladeLayout.GetComponent<HorizontalLayoutGroup>().enabled = false;

            yield return Timing.WaitForOneFrame;

            if (initialSelected != null) OnShowAccolade(initialSelected);
        }

        private void OnShowAccolade(AccoladeToggle toggle)
        {
            container.DOPunchAnchorPos(Vector2.up * 4, 0.24f);
            description.text = toggle.Data.Description;
            completeCheckmark.SetActive(toggle.ClientData.IsComplete);
            containerTail.anchoredPosition = new Vector2(toggle.RectTransform().anchoredPosition.x - 4, 7);
        }
    }
}