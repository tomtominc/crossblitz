using CrossBlitz.Fables.Battle;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.Universal;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Results
{
    public class BattleStatsDisplay : MonoBehaviour
    {
        public TextMeshProUGUI description;
        public TextMeshProUGUI stat;
        public RectTransform container;

        public void SetData(string statDescription, string statNumber,Color color, bool showContainer)
        {
            description.text = statDescription;
            stat.text = statNumber;
            stat.color = color;
            description.color = color;
            container.SetActive(showContainer);
        }
    }


    //public void FadeIn()
    //    {
    //        fadeAlpha = GetComponent<CanvasGroup>();
    //        fadeAlpha.alpha = 0;
    //        fadeAlpha.DOFade(1.0f, .25f);
    //        dawnBonusContainer.DOPunchAnchorPos(Vector2.up * 4f, .25f);
    //    }
    //}
}
