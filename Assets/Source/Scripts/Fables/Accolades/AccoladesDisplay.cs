using CrossBlitz.Audio;
using CrossBlitz.Fables.Battle;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.Universal;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Fables.Accolades
{
    public class AccoladesDisplay : MonoBehaviour
    {
        public Toggle toggle;
        public TextMeshProUGUI description;
        public RectTransform dawnBonusContainer;
        public NumberLabelFormatter dawnBonusLabel;
        private CanvasGroup fadeAlpha;

        public void SetData(AccoladeData data, ClientAccoladeData clientData, bool showAsCrossedOff)
        {
            if (toggle)
            {
                toggle.isOn = clientData != null && clientData.IsComplete;
            }

            if (description )
            {
                var prefix = clientData != null && clientData.IsComplete ? "<color=#718a4c>" : string.Empty;
                description.text = $"{prefix}{data.Description}";
                if (showAsCrossedOff) description.text = "<s>" + description.text;
            }

            if ( dawnBonusContainer && dawnBonusLabel)
            {
                if (clientData != null && clientData.IsComplete && !showAsCrossedOff)
                {
                    dawnBonusContainer.SetActive(true);
                    var o = dawnBonusContainer.anchoredPosition;
                    dawnBonusContainer.anchoredPosition = new Vector2(o.x + 10, o.y);
                    dawnBonusContainer.DOAnchorPosX(o.x, 0.12f).SetEase(Ease.OutBack);
                    dawnBonusLabel.label.text = "+0";
                    dawnBonusLabel.UpdateLabel(data.DawnDollarReward);
                    AudioController.PlaySound("ui_buy", "BARDSFX");
                }
                else
                {
                    dawnBonusContainer.SetActive(false);
                }
            }
        }

        public void FadeIn()
        {
            fadeAlpha = GetComponent<CanvasGroup>();
            fadeAlpha.alpha = 0;
            fadeAlpha.DOFade(1.0f, .25f);

            if (dawnBonusContainer.IsActive())
            {
                dawnBonusContainer.GetComponent<CanvasGroup>().alpha = 0;
                dawnBonusContainer.GetComponent<CanvasGroup>().DOFade(1, 0.24f).SetDelay(0.2f);
                var o = dawnBonusContainer.anchoredPosition;
                dawnBonusContainer.anchoredPosition = new Vector2(o.x - 10, o.y);
                dawnBonusContainer.DOAnchorPosX(o.x, 0.24f).SetEase(Ease.OutBack).SetDelay(0.2f);
            }
        }
    }
}