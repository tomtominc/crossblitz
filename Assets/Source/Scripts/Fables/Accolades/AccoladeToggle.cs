using System;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Fables.Battle;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.UI.Widgets;
using UnityEngine;
using UnityEngine.UI;
using TakoBoyStudios.Events;
using Events = CrossBlitz.ClientAPI.GameLogic.EventSystem.Events;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;
using System.Collections.Generic;
using MEC;
using CrossBlitz.Audio;

namespace CrossBlitz.Fables.Accolades
{
    public class AccoladeToggle : MonoBehaviour
    {
        public Button accoladeShowButton;
        public ToggleButton accoladeToggle;
        public SpriteAnimation accoladeFlash;

        public event Action<AccoladeToggle> OnShowAccolade;

        private AccoladeData _data;
        private ClientAccoladeData _clientData;
        private AccoladesToggleDisplay _parent;

        public AccoladeData Data => _data;
        public ClientAccoladeData ClientData => _clientData;

        private void Start()
        {

            if (accoladeShowButton)
            {
                accoladeShowButton.onClick.AddListener(OnShow);
            }
        }

        public void SetData(AccoladeData data, ClientAccoladeData clientData, AccoladesToggleDisplay parent)
        {
            _data = data;
            _clientData = clientData;
            _parent = parent;

            Events.Subscribe(EventType.OnAccoladeComplete, OnAccoladeComplete);

            if (accoladeToggle)
            {
                accoladeToggle.Toggle.isOn = clientData.IsComplete;
            }
        }

        private void OnShow()
        {
            OnShowAccolade?.Invoke(this);
        }

        private void OnAccoladeComplete(IMessage message)
        {
            Debug.Log("ACCOLADE COMPLETED");
            if (message.Data is OnAccoladeCompleteArgs args)
            {
                if(args.accoladeID == _data.Uid)
                {
                    if (accoladeToggle)
                    {
                        accoladeToggle.Toggle.isOn = true;
                        OnShow();
                        Timing.RunCoroutine(FlashContainer().CancelWith(gameObject));
                    }
                }
            }
        }

        private IEnumerator<float> FlashContainer()
        {
            AudioController.PlaySound("Key_Pickup", "SFX", false);
            while(accoladeFlash.CurrentFrame != 8)
            {
                yield return Timing.WaitForOneFrame;
            }
            _parent.accoladeFlash.SetActive(true);
        }
    }
}