using CrossBlitz.Card;
using CrossBlitz.Fables.Battle;
using CrossBlitz.PlayFab.Data;
using UnityEngine;

namespace CrossBlitz.Fables.Accolades
{
    public class AccoladePipDisplay : MonoBehaviour
    {
        public Transform pip;
        public SpriteAnimation slotAnimator;
        public void SetAccolade(AccoladeData accolade, ClientAccoladeData clientAccolade, Faction faction)
        {
            pip.SetActive(clientAccolade?.IsComplete ?? false);
            slotAnimator.Play(faction.ToString().ToLower());
        }
    }
}