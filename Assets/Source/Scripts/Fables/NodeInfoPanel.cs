using CrossBlitz.AssetManagement;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Accolades;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Data;
using CrossBlitz.Hero;
using CrossBlitz.PlayFab.Data;
using DG.Tweening;
using System.Collections.Generic;
using CrossBlitz.Fables;
using CrossBlitz.Items.Data;
using CrossBlitz.Results;
using MEC;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz
{
    public class NodeInfoPanel : MonoBehaviour
    {
        public Button exitButton;
        public RectTransform contentRect;
        public SpriteAnimation infoHeader;
        public Image enemyPortrait;
        public SpriteAnimation enemyPortraitAnimator;
        public TextMeshProUGUI enemyLevel;
        public TextMeshProUGUI enemyHealth;
        public TextMeshProUGUI enemyName;
        public TextMeshProUGUI enemyDeck;
        public HeroLimitBreakViewSimple heroLimitBreak;
        public AccoladesDisplay accoladesDisplayPrefab;
        public RectTransform accoladesLayout;
        public RectTransform lootIconsLayout;
        public ResultsLootIcon lootIconPrefab;
        public RectTransform lootPreviewTarget;
        public List<RelicSlot> relicSlots;


        private void Start()
        {
            exitButton.onClick.AddListener(Close);
            Events.Subscribe(EventType.OpenTileInfo, OnOpenTileInfo);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OpenTileInfo, OnOpenTileInfo);
        }

        private void OnOpenTileInfo(IMessage message)
        {
            if (message.Data is OpenTileInfoEventArgs args)
            {
                if (args.tileData.NodeType == NodeType.Battle)
                {
                    SetInfo(args.tileData, args.clientTileData);
                    Open();
                }
            }
        }

        private void Open()
        {
            exitButton.GetComponent<CanvasGroup>().interactable = true;
            exitButton.GetComponent<CanvasGroup>().blocksRaycasts = true;
            //exitButton.GetComponent<CanvasGroup>().DOFade(1, 0.12f);

            AudioController.PlaySound("battle_end_win_1", "BARDSFX");
            contentRect.DOAnchorPosX(0, 0.5f).SetEase(Ease.OutQuart);
        }

        private void Close()
        {
            exitButton.GetComponent<CanvasGroup>().interactable = false;
            exitButton.GetComponent<CanvasGroup>().blocksRaycasts = false;
            //exitButton.GetComponent<CanvasGroup>().DOFade(0, 0.12f);
            contentRect.DOAnchorPosX(-contentRect.sizeDelta.x, 0.5f).SetEase(Ease.InQuart);
        }

        public void SetInfo(FableTileData tileData, ClientTileData clientTileData)
        {
            var enemyOrBossNode = tileData.NodeType == NodeType.Battle ? "enemy" : "boss";
            infoHeader.Play(enemyOrBossNode);
            enemyPortraitAnimator.Play(enemyOrBossNode);

            var battleData = Db.BattleDatabase.GetBattleData(tileData.battleUid);
            var cardData = Db.CardDatabase.GetCard(battleData.CardId);

            enemyLevel.text = battleData.Level.ToString();
            enemyHealth.text = battleData.Health.ToString();
            enemyName.text = battleData.Name;
            enemyDeck.text = Db.DeckRecipeDatabase.GetArchetypeName(battleData.DeckArchetype);

            SetupAccolades(battleData, clientTileData);
            SetupRelicsAndBlitzBurst(battleData);
            SetupLootIcons(battleData);
            LoadPortrait(cardData);
        }

        private async void LoadPortrait(CardData cardData)
        {
            if (cardData == null || string.IsNullOrEmpty(cardData.portraitUrl))
            {
                enemyPortrait.SetActive(false);
                return;
            }

            var atlas = await AddressableReferenceLoader.Load<SpriteAtlas>("cards-atlas");
            enemyPortrait.sprite = atlas.GetSprite(cardData.portraitUrl);
            enemyPortrait.RectTransform().anchoredPosition = cardData.boardOffset;
        }

        private void SetupAccolades(BattleData battleData, ClientTileData clientData)
        {
            accoladesLayout.DestroyChildren();

            clientData.BattleData?.FixDependencies(battleData);

            for (var i = 0; i < battleData.Accolades.Count; i++)
            {
                if (clientData.BattleData == null) continue;
                var accoladeDisplay = Instantiate(accoladesDisplayPrefab, accoladesLayout);
                accoladeDisplay.SetData(battleData.Accolades[i],clientData.BattleData.Accolades[i], clientData.BattleData.Accolades[i].IsComplete);
            }
        }

        private void SetupLootIcons(BattleData battleData)
        {
            lootIconsLayout.DestroyChildren();
            lootPreviewTarget.DestroyChildren();

            var faction = FableController.GetCurrentFaction();
            if (faction == Faction.Neutral) return;
            var manaShards = battleData.GetManaShardsFromWin(true);
            var loot = battleData.GetIngredientsFromWin(faction, false);

            if (manaShards > 0)
            {
                var manaShardIcon = Instantiate(lootIconPrefab, lootIconsLayout, false);
                manaShardIcon.SetLoot(ItemClassType.ManaShard, Currency.MANA_SHARDS, manaShards, lootPreviewTarget, disableCount:true);
                manaShardIcon.AnimateIn();
            }

            for (var i = 0; i < loot.Count; i++)
            {
                var lootInfo = loot[i];
                var lootIcon = Instantiate(lootIconPrefab, lootIconsLayout, false);
                lootIcon.SetLoot(ItemClassType.Ingredient, lootInfo.ItemUid, lootInfo.Count, lootPreviewTarget, disableCount:true);
                lootIcon.AnimateIn();
            }
        }

        private void SetupRelicsAndBlitzBurst(BattleData battleData)
        {
            //Initialize Relics from deck
            for (var i = 0; i < relicSlots.Count; i++)
            {
                relicSlots[i].SetAsEmpty();
                relicSlots[i].slot.SetActive(false);
            }

            heroLimitBreak.zoomedCardParent.DestroyChildren();
            heroLimitBreak.SetActive(false);

            //Find relics and blitz burst from deck
            var deckData = battleData.Deck;
            var k = 1;

            for (var i = 0; i < deckData.cards.Count; i++)
            {
                var card = Db.CardDatabase.GetCard(deckData.cards[i].id);

                if(card != null)
                {
                    if (card.type == CardType.Elder_Relic)
                    {
                        relicSlots[0].zoomedCardParent.DestroyChildren();
                        relicSlots[0].SetRelic(card.id, deckData.uid);
                    }
                    else if (card.type == CardType.Relic)
                    {
                        relicSlots[k].zoomedCardParent.DestroyChildren();
                        relicSlots[k].SetRelic(card.id, deckData.uid);
                        k += 1;
                    }

                    if (card.type == CardType.Power)
                    {
                        heroLimitBreak.SetActive(true);
                        heroLimitBreak.SetCard(card.id);
                    }
                }
            }
        }
    }
}