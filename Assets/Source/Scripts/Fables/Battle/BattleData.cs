using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Data;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.ServerAPI.GameLogic;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using CrossBlitz.Utils;
using GameDataEditor;
using Selectionator;
using UnityEngine;
using UnityEngine.Serialization;

namespace CrossBlitz.Fables.Battle
{
    [System.Serializable]
    public class CustomBattleOptions
    {
        public bool Enabled;
        public int PlayerStartingMana;
        public int OpponentStartingMana;
        public bool ForcePlayerStartsFirst;
        public bool ForceOpponentStartsFirst;
        public List<string> RemoveCardsFromPlayersDeck;
        public List<string> ForcedPrizeCards;
        public CustomDeckOverride PlayerDeckOverride;
        public CustomDeckOverride OpponentDeckOverride;
        public CustomDeckOverride PlayerAdditionalCards;
        public CustomDeckOverride OpponentAdditionalCards;
        public CustomGameBoardOptions GameBoardOptions;
    }

    [System.Serializable]
    public class CustomDeckOverride
    {
        public bool Enabled;
        public bool DrawInOrder;
        public DeckData OverrideDeck;
    }

    [System.Serializable]
    public class CustomGameBoardOptions
    {
        public List<CustomGameCardState> Board;
    }

    [System.Serializable]
    public class CustomGameCardState
    {
        public string CardId;

        public bool AddBarrier;
        public bool AddLifeSteal;
        public bool AddTough;
        public bool AddElusive;
        public bool AddPierce;
        public bool AddThorns;
        public bool AddFreeze;
        public bool AddFlying;
        public bool AddPoison;
        public bool AddCounter;
        public bool AddRush;
    }

    // to add battle events
    // 1. Add Go to EventType.cs add an event to the end of: Game Events --
    // 2. Add to the Enum, Actions and both to the dictionary
    // 3. Add the enum of the same name to this

    [System.Serializable]
    public class BattleEvent : UniqueItem
    {
        public enum BattleEventListener
        {
            OnStartMatch,
            OnMatchWon,
            OnMatchLost,
            OnMatchDraw,
            OnTurnStart,
            OnTurnEnd,
            OnMatchDisposed,
            OnCardDrawn,
            OnMinionSummoned,
            OnCardChangedLocation,
            OnCharacterDamaged,
            OnCharacterStatsModified,
            OnMinionDestroyed,
            OnCardPlayed,
            OnModifiedMana,
            OnCardAddedToDeck,
            OnMinionFrozen,
            OnCardTrackedAndAddedToHand
        }

        public enum EventFrequency
        {
            Once,
            Random,
            Always,
            Never
        }

        public string displayName;

        public EventFrequency frequency;

        // skip the first few times this should be called.
        // e.g. Add OnTurnStart event, skip 3 times to say something turn 4.
        public bool skipUntil;
        public int skips;

        // Event Listener Type
        public BattleEventListener battleEventListener;

        // Used if it makes sense
        public string cardId;

        // only if "random" is checked, this is a percentage (0-1)
        public float randomFrequency;

        // takes the cutscene data and uses it for various ways to show things.
        // map type cutscenes are usual, as they'll display text and do events with hero portraits.
        // other types will stop the game and probably will not be used.
        // plays a cutscene at random...
        public List<string> cutsceneUids;

    }
    public enum BattleType
    {
        None,
        Lackey,
        Boss,
        Special,
    }
    public enum AIStrategy
    {
        None,
        DoubloonOtk,
        DoubloonCheat,
    }

    public enum PlayStyle
    {
        Aggressive,
        Defensive,
    }

    [System.Serializable]
    public class BattleData : UniqueItem
    {
        public string BattleName;
        public BattleType BattleType;
        public string CardId;
        public string Name;
        public Faction Faction;
        public DeckData Deck;
        public string DeckArchetype;
        public int Level;
        public int Health;
        public AIStrategy Strategy;
        public PlayStyle PlayStyle;
        public FableRoomData.RoomWeather Weather;
        public string Music;
        public CustomBattleOptions CustomBattleOptions;
        public RewardTierType RewardTier;
        public string RecommendedDeck;
        public TodoStatus TodoStatus;
        public List<AccoladeData> Accolades;
        //public List<string> ExcludedCards;
        public List<BattleEvent> BattleEvents;
        public List<string> PrizeCards;

        public enum RewardTierType
        {
            Low,
            Medium,
            High,
            Boss
        }

        public AccoladeData GetAccolade(string uid)
        {
            return Accolades.Find(a => a.Uid == uid);
        }

        public string GetDatabaseDisplayName(out bool invalid)
        {
            var prefix = "<color=black>";
            var battleName = BattleName;
            var enemyName = Name;

            switch (TodoStatus)
            {
                case TodoStatus.NotStarted: prefix = "<color=black>";
                    break;
                case TodoStatus.Testing: prefix = "<color=orange>";
                    break;
                case TodoStatus.Finished: prefix = "<color=green>";
                    break;
            }

            if (string.IsNullOrEmpty(BattleName) || BattleName.StartsWith("*") || Deck.cards == null || Deck.cards.Count <= 0)
            {
                prefix = "<color=red>";
            }

            if (string.IsNullOrEmpty(battleName))
            {
                battleName = "No Name";
                invalid = true;
            }
            else
            {
                invalid = false;
            }

            if (string.IsNullOrEmpty(enemyName))
            {
                enemyName = "None";
            }

            return $"{prefix}{battleName} (Lv.{Level} {enemyName} \u2665{Health})</color>";
        }

        public AccoladeData CreateNewAccolade()
        {
            var accolade = new AccoladeData
            {
                Score = 0,
                Description = string.Empty,
                ItemRewards = new List<ItemValue>(),
            };

            accolade.UpdateIdentifier();
            Accolades.Add(accolade);

            return accolade;

        }

        public class RewardSelection : ISelectable
        {
            public string Data { get; set; }
            public float GetSelectionWeight()
            {
                return SelectionWeight;
            }

            public float AdjustedSelectionWeight { get; set; }

            public float SelectionWeight { get; set; }

        }

        public FableChapterData GetChapter()
        {
            var nameSplit = BattleName.Split(':');
            var heroName = nameSplit[0];
            var chapterSplit = nameSplit[1].Split('.')[1][0].ToString();
            return Db.FablesDatabase.GetChapterByChapterId($"{heroName}-1-{chapterSplit}");
        }

        public int GetManaShardsFromWin(bool firstWin)
        {
            switch (RewardTier)
            {
                case RewardTierType.Medium:
                    return 1;
                case RewardTierType.High:
                    return 3;
                case RewardTierType.Boss:
                    return 4;
            }

             return 0;
        }

        public List<ItemValue> GetIngredientsFromWin(Faction currentFaction, bool firstWin)
        {
            if (!Db.Initialized)
            {
                Db.Initialize();
            }

            if (!Crafting.Initialized)
            {
                Crafting.Init();
            }

            var ingredients = new List<ItemValue>();

            switch (RewardTier)
            {
                case RewardTierType.Low:
                    ingredients.Add(new ItemValue { ItemUid = Crafting.Ingredients[currentFaction][1].Key, Count = 1 });
                    ingredients.Add(new ItemValue { ItemUid = Crafting.Ingredients[currentFaction][2].Key, Count = 1 });
                    ingredients.Add(new ItemValue { ItemUid = Crafting.Ingredients[currentFaction][0].Key, Count = 2 });
                    ingredients.Add(new ItemValue { ItemUid = Crafting.Ingredients[Faction.Neutral][3].Key, Count = 1 });
                    ingredients.Add(new ItemValue { ItemUid = Crafting.Ingredients[Faction.Neutral][0].Key, Count = 1 });
                    break;
                case RewardTierType.Medium:
                    ingredients.Add(new ItemValue { ItemUid = Crafting.Ingredients[currentFaction][1].Key, Count = 2 });
                    ingredients.Add(new ItemValue { ItemUid = Crafting.Ingredients[currentFaction][2].Key, Count = 2 });
                    ingredients.Add(new ItemValue { ItemUid = Crafting.Ingredients[currentFaction][0].Key, Count = 1 });
                    ingredients.Add(new ItemValue { ItemUid = Crafting.Ingredients[Faction.Neutral][3].Key, Count = 2 });
                    ingredients.Add(new ItemValue { ItemUid = Crafting.Ingredients[Faction.Neutral][0].Key, Count = 2 });
                    break;
                case RewardTierType.High:
                    ingredients.Add(new ItemValue { ItemUid = Crafting.Ingredients[currentFaction][1].Key, Count = 2 });
                    ingredients.Add(new ItemValue { ItemUid = Crafting.Ingredients[currentFaction][2].Key, Count = 3 });
                    ingredients.Add(new ItemValue { ItemUid = Crafting.Ingredients[currentFaction][0].Key, Count = 2 });
                    ingredients.Add(new ItemValue { ItemUid = Crafting.Ingredients[Faction.Neutral][3].Key, Count = 2 });
                    ingredients.Add(new ItemValue { ItemUid = Crafting.Ingredients[Faction.Neutral][0].Key, Count = 1 });
                    break;
                case RewardTierType.Boss:
                    ingredients.Add(new ItemValue { ItemUid = Crafting.Ingredients[currentFaction][1].Key, Count = 4 });
                    ingredients.Add(new ItemValue { ItemUid = Crafting.Ingredients[currentFaction][2].Key, Count = 3 });
                    ingredients.Add(new ItemValue { ItemUid = Crafting.Ingredients[currentFaction][0].Key, Count = 2 });
                    ingredients.Add(new ItemValue { ItemUid = Crafting.Ingredients[Faction.Neutral][3].Key, Count = 2 });
                    ingredients.Add(new ItemValue { ItemUid = Crafting.Ingredients[Faction.Neutral][0].Key, Count = 1 });
                    break;
            }

            return ingredients;
        }

        public List<string> GetCardRewardsForWin(Faction currentFaction, string chapterUid, bool firstWin, Dictionary<string, ItemValue> customInventory = null)
        {
            if (!Db.Initialized)
            {
                Db.Initialize();
            }

            var rewards = new List<string>();
            var book = App.FableData.GetCurrentBook();

            if (book != null && PrizeCards != null && PrizeCards.Count > 0)
            {
                for (var i = 0; i < PrizeCards.Count; i++)
                {
                    if (!book.Recipes.Contains(PrizeCards[i]))
                    {
                        rewards.Add(PrizeCards[i]);
                    }
                }
            }

            rewards = rewards.OrderBy(r => r).ToList();

            return rewards;
        }

        public void Init()
        {
            UpdateIdentifier();

            Accolades = new List<AccoladeData>();
            BattleEvents = new List<BattleEvent>();
            Deck = new DeckData();

            for (var i = 0; i < 4; i++)
            {
                var accolade = new AccoladeData
                {
                    BattleUid = Uid
                };

                accolade.UpdateIdentifier();

                if ( i == 0)
                {
                    accolade.Type = AccoladeType.Win;
                    accolade.DawnDollarReward = 400;
                    accolade.RegenerateDescription();
                }
                else if ( i == 1)
                {
                    accolade.Type = AccoladeType.UnderTurns;
                    accolade.Score = 10;
                    accolade.DawnDollarReward = 100;
                    accolade.RegenerateDescription();
                }
                else if ( i == 2)
                {
                    accolade.Type = AccoladeType.DestroyedOpponentMinions;
                    accolade.Score = 5;
                    accolade.DawnDollarReward = 100;
                    accolade.RegenerateDescription();
                }
                else if ( i == 3)
                {
                    accolade.Type = AccoladeType.TakeLessThanXDamage;
                    accolade.Score = 20;
                    accolade.DawnDollarReward = 100;
                    accolade.RegenerateDescription();
                }

                Accolades.Add(accolade);
            }
        }
    }

    public enum AccoladeType
    {
        None,
        Win,
        PlayMinions,
        PlaySpells,
        UnderTurns,
        OverTurns,
        BeatWithSpellDamage,
        BeatWithMinionDamage,
        DestroyedOpponentMinions,
        TakeLessThanXDamage,
        DamageYourOwnMinions,
        WinWithAtLeastXNumberOfCardCopiesInYourDeck,
        DealDamageToEnemiesThroughEffects,
        GetAdmiralBrassBack,
        DestroySpecificMinionInASpecificNumberOfTurns,
        TriggerTraps,
        WinUsingBlitzBurst,
    }

    [System.Serializable]
    public class AccoladeData : UniqueItem
    {
        public string BattleUid;
        public AccoladeType Type;
        public string Description;

        public int DawnDollarReward;
        public List<ItemValue> ItemRewards;
        public int Score;

        //[ValueDropdown("GetAllCards")]
        public string CardId;

        public ItemValue GetItemValue(string uid)
        {
            return ItemRewards.Find(item => item.Uid == uid);
        }

        public void DeleteItemReward(string uid)
        {
            var itemValue = GetItemValue(uid);

            if (itemValue != null)
            {
                ItemRewards.Remove(itemValue);
            }
        }

        public void RegenerateDescription()
        {
            switch (Type)
            {
                case AccoladeType.None:
                    Description = string.Empty;
                    break;
                case AccoladeType.Win:
                    Description = "Defeat the enemy.";
                    break;
                case AccoladeType.OverTurns:
                    Description = $"Survive for {Score} turns or longer.";
                    break;
                case AccoladeType.PlayMinions:
                    Description = $"Play {Score} or more minions.";
                    break;
                case AccoladeType.PlaySpells:
                    Description = $"Play {Score} or more spells.";
                    break;
                case AccoladeType.UnderTurns:
                    Description = $"Win in {Score} or less turns.";
                    break;
                case AccoladeType.DestroyedOpponentMinions:
                    Description = $"Destroy at least {Score} of your opponent's minions.";
                    break;
                case AccoladeType.BeatWithMinionDamage:
                    Description = $"Defeat the enemy hero using a minion.";
                    break;
                case AccoladeType.BeatWithSpellDamage:
                    Description = $"Defeat the enemy hero using a spell.";
                    break;
                case AccoladeType.TakeLessThanXDamage:
                    Description = $"Take {Score} or less damage during the entire battle.";
                    break;
                case AccoladeType.DamageYourOwnMinions:
                    Description = $"Damage {Score} or more of your own minions.";
                    break;
                case AccoladeType.WinWithAtLeastXNumberOfCardCopiesInYourDeck: // for bombs
                {
                    var card = Db.CardDatabase.GetCard(CardId);
                    if (card != null)
                    {
                        Description = $"Win with {Score} or more copies of {card.name} in your deck.";
                    }
                    else
                    {
                        Description = $"error could not find {CardId}";
                    }

                    break;
                }
                case AccoladeType.DealDamageToEnemiesThroughEffects:
                    Description = $"Deal {Score} or more damage using minion effects.";
                    break;
                case AccoladeType.GetAdmiralBrassBack:
                    Description = $"<color={ColorPalette.CrossBlitz.FactionColor.WAR}>Admiral Brass</color> was taken, get him back!";
                    break;
                case AccoladeType.DestroySpecificMinionInASpecificNumberOfTurns:
                {
                    var card = Db.CardDatabase.GetCard(CardId);

                    if (card != null)
                    {
                        Description =
                            $"Destroy {card.name} before turn {Score}.";
                    }
                    else
                    {
                        Description =
                            $"Destroy <color={ColorPalette.CrossBlitz.FactionColor.WAR}>NULL</color> before turn {Score}.";
                    }
                    break;
                }
                case AccoladeType.TriggerTraps:
                {
                    Description = $"Trigger {Score} traps.";
                    break;
                }
                case AccoladeType.WinUsingBlitzBurst:
                {
                    Description = $"Defeat the enemy hero using your Blitz Burst.";
                    break;
                }
            }
        }

// #if UNITY_EDITOR
//
//         public static ValueDropdownList<string> cards;
//
//         private static IEnumerable GetAllCards()
//         {
//             if (cards != null) return cards;
//
//             cards = new ValueDropdownList<string>();
//
//             if (!Db.Initialized)
//             {
//                 Db.ForceInit();
//             }
//
//             for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
//             {
//                 var cardData = Db.CardDatabase.Cards[i];
//                 cards.Add($"{cardData.faction}/{cardData.name}", cardData.id);
//             }
//
//             return cards;
//         }
// #endif
    }

    public enum CutsceneTriggerType
    {
        None,
        BattleStart,
        BattleEnd,
    }

    [System.Serializable]
    public class CutsceneTrigger : UniqueItem
    {
        public CutsceneTriggerType TriggerType;
        public string CutsceneUid;
    }

    [System.Serializable]
    public class ItemValue : UniqueItem
    {
        public string ItemUid;
        public int Count;
    }

    [System.Serializable]
    public class BattleResults : ClientData
    {
        public enum Result
        {
            None,
            Won,
            Lost,
            Draw,
        }

        public bool Initialized;
        public Result MatchResult;
        public bool PendingMapVisuals;
        public string DeckId;
        public int HeroLevelBeforeBattleEnd;
        public int HeroLevelsGained;
        public int ExperienceBeforeBattleEnd;
        public int ExperienceGained;
        public string BattleUid;
        public string ChapterUid;
        public int DawnDollarsAcquired;
        public int ManaShardsAcquired;
        public GameStats Stats;
        public GameMode LastGameMode;

        public List<string> CompletedAccoladeIds;
        public List<string> CardsRewarded;
        public List<ItemValue> ChestsObtained;
        public List<ItemValue> IngredientsEarned;

        public override void Init()
        {
            Initialized = App.Load("BattleResults.Initialized", false);
            MatchResult = App.Load("BattleResults.MatchResult", Result.None);
            PendingMapVisuals = App.Load("BattleResults.PendingMapVisuals", false);
            DeckId = App.Load("BattleResults.DeckId", string.Empty);
            HeroLevelBeforeBattleEnd = App.Load("BattleResults.HeroLevelBeforeBattleEnd", 0);
            HeroLevelsGained = App.Load("BattleResults.HeroLevelsGained", 0);
            ExperienceBeforeBattleEnd = App.Load("BattleResults.ExperienceBeforeBattleEnd", 0);
            ExperienceGained = App.Load("BattleResults.ExperienceGained", 0);
            BattleUid = App.Load("BattleResults.BattleUid", string.Empty);
            ChapterUid = App.Load("BattleResults.ChapterUid", string.Empty);
            DawnDollarsAcquired = App.Load("BattleResults.DawnDollarsAcquired", 0);
            ManaShardsAcquired = App.Load("BattleResults.ManaShardsAcquired", 0);
            CompletedAccoladeIds = App.Load("BattleResults.CompletedAccoladeIds", new List<string>());
            CardsRewarded = App.Load("BattleResults.CardsRewarded", new List<string>());
            ChestsObtained = App.Load("BattleResults.ChestsObtained", new List<ItemValue>());
            IngredientsEarned = App.Load("BattleResults.IngredientsEarned", new List<ItemValue>());
            Stats = App.Load("BattleResults.Stats", new GameStats());
            LastGameMode = App.Load("BattleResults.LastGameMode", new GameMode());
        }

        public bool GetInitialized()
        {
            return Initialized;
        }
        public void SetInitialized(bool initialized)
        {
            if(Initialized != initialized)
            {
                Initialized = initialized;
                dirty = true;
            }
        }

        public Result GetMatchResult()
        {
            return MatchResult;
        }
        public void SetMatchResult(Result matchResult)
        {
            if (MatchResult != matchResult)
            {
                MatchResult = matchResult;
                dirty = true;
            }
        }

        public bool GetPendingMapVisuals()
        {
            return PendingMapVisuals;
        }
        public void SetPendingMapVisuals(bool pendingMapVisuals)
        {
            if (PendingMapVisuals != pendingMapVisuals)
            {
                PendingMapVisuals = pendingMapVisuals;
                dirty = true;
            }
        }

        public string GetDeckId()
        {
            return DeckId;
        }
        public void SetDeckId(string deckId)
        {
            if (DeckId != deckId)
            {
                DeckId = deckId;
                dirty = true;
            }
        }

        public int GetHeroLevelBeforeBattleEnd()
        {
            return HeroLevelBeforeBattleEnd;
        }
        public void SetHeroLevelBeforeBattleEnd(int heroLevelBeforeBattleEnd)
        {
            if (HeroLevelBeforeBattleEnd != heroLevelBeforeBattleEnd)
            {
                HeroLevelBeforeBattleEnd = heroLevelBeforeBattleEnd;
                dirty = true;
            }
        }

        public int GetHeroLevelsGained()
        {
            return HeroLevelsGained;
        }
        public void SetHeroLevelsGained(int heroLevelsGained)
        {
            if (HeroLevelsGained != heroLevelsGained)
            {
                HeroLevelsGained = heroLevelsGained;
                dirty = true;
            }
        }

        public int GetExperienceBeforeBattleEnd()
        {
            return ExperienceBeforeBattleEnd;
        }
        public void SetExperienceBeforeBattleEnd(int experienceBeforeBattleEnd)
        {
            if (ExperienceBeforeBattleEnd != experienceBeforeBattleEnd)
            {
                ExperienceBeforeBattleEnd = experienceBeforeBattleEnd;
                dirty = true;
            }
        }

        public int GetExperienceGained()
        {
            return ExperienceGained;
        }
        public void SetExperienceGained(int experienceGained)
        {
            if (ExperienceGained != experienceGained)
            {
                ExperienceGained = experienceGained;
                dirty = true;
            }
        }

        public string GetBattleUid()
        {
            return BattleUid;
        }
        public void SetBattleUid(string battleUid)
        {
            if (BattleUid != battleUid)
            {
                BattleUid = battleUid;
                dirty = true;
            }
        }

        public string GetChapterUid()
        {
            return ChapterUid;
        }
        public void SetChapterUid(string chapterUid)
        {
            if (ChapterUid != chapterUid)
            {
                ChapterUid = chapterUid;
                dirty = true;
            }
        }

        public int GetDawnDollarsAcquired()
        {
            return DawnDollarsAcquired;
        }
        public void SetDawnDollarsAcquired(int dawnDollarsAcquired)
        {
            if (DawnDollarsAcquired != dawnDollarsAcquired)
            {
                DawnDollarsAcquired = dawnDollarsAcquired;
                dirty = true;
            }
        }

        public int GetManaShardsAcquired()
        {
            return ManaShardsAcquired;
        }
        public void SetManaShardsAcquired(int manaShardsAcquired)
        {
            if (ManaShardsAcquired != manaShardsAcquired)
            {
                ManaShardsAcquired = manaShardsAcquired;
                dirty = true;
            }
        }

        public List<string> GetCompletedAccoladeIds()
        {
            return CompletedAccoladeIds;
        }
        public void SetCompletedAccoladeIds(List<string> completedAccoladeIds)
        {
            if (CompletedAccoladeIds != completedAccoladeIds)
            {
                CompletedAccoladeIds = completedAccoladeIds;
                dirty = true;
            }
        }

        public List<string> GetCardsRewarded()
        {
            return CardsRewarded;
        }
        public void SetCardsRewarded(List<string> cardsRewarded)
        {
            if (CardsRewarded != cardsRewarded)
            {
                CardsRewarded = cardsRewarded;
                dirty = true;
            }
        }

        public List<ItemValue> GetChestsObtained()
        {
            return ChestsObtained;
        }
        public void SetChestsObtained(List<ItemValue> chestsObtained)
        {
            if (ChestsObtained != chestsObtained)
            {
                ChestsObtained = chestsObtained;
                dirty = true;
            }
        }

        public List<ItemValue> GetIngredientsEarned()
        {
            return IngredientsEarned;
        }
        public void SetIngredientsEarned(List<ItemValue> ingredientsEarned)
        {
            if (IngredientsEarned != ingredientsEarned)
            {
                IngredientsEarned = ingredientsEarned;
                dirty = true;
            }
        }

        public GameStats GetStats()
        {
            return Stats;
        }
        public void SetStats(GameStats stats)
        {
            if (Stats != stats)
            {
                Stats = stats;
                dirty = true;
            }
        }

        public GameMode GetLastGameMode()
        {
            return LastGameMode;
        }
        public void SetLastGameMode(GameMode lastGameMode)
        {
            if (LastGameMode != lastGameMode)
            {
                LastGameMode = lastGameMode;
                dirty = true;
            }
        }

        public override void ResetData()
        {
            base.ResetData();

            Initialized = false;
            MatchResult = Result.None;
            PendingMapVisuals = false;
            DeckId = string.Empty;
            HeroLevelBeforeBattleEnd = 0;
            HeroLevelsGained = 0;
            ExperienceBeforeBattleEnd = 0;
            ExperienceGained = 0;
            BattleUid = string.Empty;
            ChapterUid = string.Empty;
            DawnDollarsAcquired = 0;
            ManaShardsAcquired = 0;
            CompletedAccoladeIds = new List<string>();
            CardsRewarded = new List<string>();
            ChestsObtained = new List<ItemValue>();
            IngredientsEarned = new List<ItemValue>();
            Stats = new GameStats();
            LastGameMode = new GameMode();

            dirty = true;
        }

        public override void Save()
        {
            App.Save("BattleResults.Initialized", Initialized);
            App.Save("BattleResults.MatchResult", MatchResult);
            App.Save("BattleResults.PendingMapVisuals", PendingMapVisuals);
            App.Save("BattleResults.DeckId", DeckId);
            App.Save("BattleResults.HeroLevelBeforeBattleEnd", HeroLevelBeforeBattleEnd);
            App.Save("BattleResults.HeroLevelsGained", HeroLevelsGained);
            App.Save("BattleResults.ExperienceBeforeBattleEnd", ExperienceBeforeBattleEnd);
            App.Save("BattleResults.ExperienceGained", ExperienceGained);
            App.Save("BattleResults.BattleUid", BattleUid);
            App.Save("BattleResults.ChapterUid", ChapterUid);
            App.Save("BattleResults.DawnDollarsAcquired", DawnDollarsAcquired);
            App.Save("BattleResults.ManaShardsAcquired", ManaShardsAcquired);
            App.Save("BattleResults.CompletedAccoladeIds", CompletedAccoladeIds);
            App.Save("BattleResults.CardsRewarded", CardsRewarded);
            App.Save("BattleResults.ChestsObtained", ChestsObtained);
            App.Save("BattleResults.IngredientsEarned", IngredientsEarned);
            App.Save("BattleResults.Stats", Stats);
            App.Save("BattleResults.LastGameMode", LastGameMode);
        }
    }
}