
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Audio;
using CrossBlitz.Card;
using CrossBlitz.CardCollection;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.DeckEditAPI;
using CrossBlitz.Fables.Grid;
using CrossBlitz.InputAPI;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.UI.Widgets;
using TakoBoyStudios.Events;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Fables.DeckViewInputApi
{
    public class DeckViewInput : InputMode
    {
        /// <summary>
        /// This will be the deck viewer
        /// </summary>
        public DeckViewerMenu deckViewer;
        /// <summary>
        /// The open button for this view, usually on another UI element or menu.
        /// </summary>
        public ToggleButton openButton;
        /// <summary>
        /// opens the collection manager into the deck editor
        /// </summary>
        public Button openDeckEditButton;
        /// <summary>
        /// opens the collection manager so the player can equip decks.
        /// </summary>
        public Button openCollectionButton;
        /// <summary>
        /// If this is not null, it will display when there are new cards.
        /// </summary>
        public TextMeshProUGUI newCardsAvailableLabel;

        public Button closeButton;


        private bool m_isInMode;
        private bool m_inputEnabled;

        private void Awake()
        {
            Events.Subscribe(EventType.OnPlayerEnteredMap, OnPlayerEnteredMap);
            Events.Subscribe(EventType.OnHeroLevelChanged, OnPlayerEnteredMap);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnPlayerEnteredMap, OnPlayerEnteredMap);
            Events.Unsubscribe(EventType.OnHeroLevelChanged, OnPlayerEnteredMap);
        }

        private void OnPlayerEnteredMap(IMessage message)
        {
            RefreshNewCardsLabel();
        }

        private void Start()
        {
            openButton.OnValueChanged += OnToggledOpen;
            deckViewer.Init();

            if (openDeckEditButton) openDeckEditButton.onClick.AddListener(OpenDeckEdit);
            if (openCollectionButton) openCollectionButton.onClick.AddListener(OpenCollection);
            if (closeButton) closeButton.onClick.AddListener(OnClose);

            //RefreshNewCardsLabel();
        }

        public override void StartMode()
        {
            if (FableController.PlayerIsInAFable())
            {
                App.FableData.OnEnterDeckEdit();
            }

            m_inputEnabled = true;
            m_isInMode = true;

        }

        public override void UpdateMode()
        {

        }

        private void Update()
        {
            if (FablesInput.Instance && FablesMapController.Instance)
            {
                openButton.Toggle.interactable = (FablesInput.Instance.currentMode == FablesInput.Mode.Explore ||
                                                  FablesInput.Instance.currentMode == FablesInput.Mode.DeckView) &&
                                                 ExploreMenu.IsAbleToSelectTile();
            }
        }

        public override void EndMode()
        {
            m_isInMode = false;
            m_inputEnabled = false;

            if (FableController.PlayerIsInAFable())
            {
                App.FableData.OnExitDeckEdit();
            }

            if (SceneManager.GetSceneByName("Collection").isLoaded)
            {
                SceneController.Instance.UnloadScene("Collection");
            }
        }

        private async void OpenDeckEdit()
        {
            if (!m_isInMode)
            {
                return;
            }

            if (!m_inputEnabled)
            {
                return;
            }

            if (FablesMapController.Instance == null)
            {
                return;
            }

            var chapterData = FablesMapController.Instance.ChapterData;

            if (chapterData == null)
            {
                return;
            }

            if (SceneManager.GetSceneByName("Collection").isLoaded)
            {
                return;
            }

            AudioController.PlaySound("UI-StartingHand");

            m_inputEnabled = false;
            openButton.ChangeToggleStateWithoutNotify(false);
            deckViewer.Close();

            var sceneModel = await SceneController.Instance.LoadScene(new SceneLoadParams
            {
                SceneToLoad = "Collection"
            });

            var collectionManager = sceneModel.GetView<DeckEditMenu>("CollectionManager");

            collectionManager.OnCollectionClosed += OnCollectionManagerClosed;
            collectionManager.OpenCollectionManagerWithSettings(new DeckEditSettings
            {
                Mode = DeckEditMode.Edit,
                OpenMode = DeckOpenMode.DeckEditor,
                DeckSettings = new DeckSettings { deckData = App.PlayerDecks.GetDeckBasedOnGameState() },
                EquipContext = App.PlayerDecks.GetContextBasedOnGameState(),
                FilterCardSettings = new FilterCardSettings { ownedCardsOnly = true, forcedFaction = chapterData.ChapterFaction, faction = Faction.All, @class = Class.All, rarity = Rarity.All },
            });
        }

        private async void OpenCollection()
        {
            if (!m_isInMode)
            {
                return;
            }

            if (!m_inputEnabled)
            {
                return;
            }

            if (FablesMapController.Instance == null)
            {
                return;
            }

            var chapterData = FablesMapController.Instance.ChapterData;

            if (chapterData == null)
            {
                return;
            }

            if (SceneManager.GetSceneByName("Collection").isLoaded)
            {
                return;
            }

            AudioController.PlaySound("UI-StartingHand");

            m_inputEnabled = false;
            openButton.ChangeToggleStateWithoutNotify(false);
            deckViewer.Close();

            var sceneModel = await SceneController.Instance.LoadScene(new SceneLoadParams
            {
                SceneToLoad = "Collection"
            });

            var collectionManager = sceneModel.GetView<DeckEditMenu>("CollectionManager");

            collectionManager.OnCollectionClosed += OnCollectionManagerClosed;
            collectionManager.OpenCollectionManagerWithSettings(new DeckEditSettings
            {
                Mode = DeckEditMode.Edit,
                OpenMode = DeckOpenMode.Collection,
                DeckSettings = new DeckSettings { deckData = App.PlayerDecks.GetDeckBasedOnGameState() },
                EquipContext = App.PlayerDecks.GetContextBasedOnGameState(),
                FilterCardSettings = new FilterCardSettings { ownedCardsOnly = true, forcedFaction = chapterData.ChapterFaction, faction = Faction.All, @class = Class.All, rarity = Rarity.All },
            });
        }

        private async void OnCollectionManagerClosed()
        {
            RefreshNewCardsLabel();

            await System.Threading.Tasks.Task.Delay(400);

            if (FablesInput.Instance)
            {
                FablesMapController.Instance.heroPanel.heroView.UpdateRelics();
                FablesInput.Instance.GoToMode(FablesInput.Mode.Explore);
            }
        }

        public void RefreshNewCardsLabel()
        {
            if (newCardsAvailableLabel)
            {
                var chapterData = FablesMapController.Instance.ChapterData;

                if (chapterData != null)
                {
                    newCardsAvailableLabel.SetActive( App.Inventory.HasAnyNewCards(chapterData.ChapterFaction) );
                }
                else
                {
                    newCardsAvailableLabel.SetActive( false );
                }
            }
        }

        private void OnToggledOpen(bool isOn, string content)
        {
            if (isOn)
            {
                deckViewer.Open();

                if (FablesInput.Instance)
                {
                    FablesInput.Instance.GoToMode(FablesInput.Mode.DeckView);
                }
            }
            else
            {
                deckViewer.Close();

                if (FablesInput.Instance)
                {
                    FablesInput.Instance.GoToMode(FablesInput.Mode.Explore);
                }
            }
        }

        private void OnClose()
        {
            if (openButton != null)
            {
                openButton.Toggle.isOn=false;
            }
        }
    }
}