using System;
using System.Collections;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Grid;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Fables
{
    public class ExploreActionWindow : MonoBehaviour
    {
        public RectTransform rectTransform;
        public CanvasGroup canvas;
        public TextMeshProUGUI titleLabel;
        public TextMeshProUGUI subTitleLabel;
        public Button moveButton;

        public ExploreActionWindowBattleContent battleContent;

        public event Action<HexTile2D> OnMovePressed;
        private HexTile2D _tile;
        private bool _isOpen;

        private void Start()
        {
            moveButton.onClick.AddListener(MovePressed);
            canvas.alpha = 0;
        }

        public void Open(HexTile2D tileData)
        {
            _tile = tileData;

            canvas.interactable = true;
            canvas.blocksRaycasts = true;

            battleContent.SetActive(false);
            moveButton.SetActive(true);

            if (_tile.TileData.NodeType == NodeType.Battle)
            {
                titleLabel.text = "BATTLE";
                subTitleLabel.text = "DIFFICULTY";

                battleContent.SetActive(true);
                battleContent.SetTileData(_tile);
            }
            else if (_tile.TileData.NodeType == NodeType.ExitDown ||
                     _tile.TileData.NodeType == NodeType.ExitUp ||
                     _tile.TileData.NodeType == NodeType.ExitRight ||
                     _tile.TileData.NodeType == NodeType.ExitLeft)
            {
                titleLabel.text = "SWAP LANDS ENTRANCE";
                subTitleLabel.text = "HORRORS AWAIT";
            }
            else if (_tile.TileData.NodeType == NodeType.Shop)
            {
                titleLabel.text = "SHOP";
                subTitleLabel.text = "PURCHASE CARDS";
            }
            else if (_tile.TileData.NodeType == NodeType.Chest)
            {
                titleLabel.text = "GIANT CHEST";
                subTitleLabel.text = "TREASURES";
            }
            else if (_tile.TileData.NodeType == NodeType.None)
            {
                titleLabel.text = "GRASSLANDS";
                subTitleLabel.text = "GRASSY GREENS";
            }
            else if (_tile.TileData.NodeType == NodeType.Event1 || _tile.TileData.NodeType == NodeType.Event2)
            {
                titleLabel.text = "EVENT";
                subTitleLabel.text = "???";
            }
            else
            {
                titleLabel.text = "UNKNOWN";
                subTitleLabel.text = _tile.TileData.NodeType.ToString();
            }

            if (!_isOpen)
            {
                DOTween.Kill(rectTransform);
                DOTween.Kill(canvas);

                rectTransform.anchoredPosition = new Vector2(0, -12f);

                rectTransform.DOAnchorPosY(0, 0.5f)
                    .SetEase(Ease.OutBack);

                canvas.DOFade(1, 0.25f);

                _isOpen = true;
            }
        }

        public void Close()
        {
            if (!_isOpen) return;

            DOTween.Kill(rectTransform);
            DOTween.Kill(canvas);

            rectTransform.DOAnchorPosY(-12f, 0.5f)
                .SetEase(Ease.OutBack);

            canvas.DOFade(0, 0.25f);

            _isOpen = false;
        }

        private void MovePressed()
        {
            Close();
            moveButton.SetActive(false);
            OnMovePressed?.Invoke(_tile);
        }
    }
}