using System;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Grid;
using CrossBlitz.PlayFab.Data;
using TakoBoyStudios.Events;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Fables
{
    public class NodeToolTips : MonoBehaviour
    {
        public NodeInfoTooltip battleAvailable;
        public NodeInfoTooltip battleCompleted;
        public NodeInfoTooltip battleLocked;
        public NodeInfoTooltip bossAvailable;
        public NodeInfoTooltip bossComplete;
        public NodeInfoTooltip bossLocked;
        public NodeInfoTooltip eventAvailable;
        public NodeInfoTooltip eventLocked;
        public NodeInfoTooltip eventViewed;
        public NodeInfoTooltip exitAvailable;
        public NodeInfoTooltip exitLocked;
        public NodeInfoTooltip multiBattleComplete;
        public NodeInfoTooltip multiBattleLocked;
        public NodeInfoTooltip multiBattleAvailable;
        public NodeInfoTooltip passageAvailable;
        public NodeInfoTooltip passageLocked;
        public NodeInfoTooltip shopAvailable;
        public NodeInfoTooltip shopLocked;
        public NodeInfoTooltip treasureAvailable;
        public NodeInfoTooltip treasureLocked;
        public NodeInfoTooltip treasureOpened;
        public NodeInfoTooltip cardShopAvailable;
        public NodeInfoTooltip cardShopLocked;
        public NodeInfoTooltip relicShopAvailable;
        public NodeInfoTooltip relicShopLocked;
        public NodeInfoTooltip manaForgeAvailable;
        public NodeInfoTooltip manaForgeLocked;

        private NodeInfoTooltip m_currentTooltip;

        private void Start()
        {
            Events.Subscribe(EventType.OnHexTilePointerEnter, OnPointerEnterTile);
            Events.Subscribe(EventType.OnHexTilePointerExit, OnPointerExitTile);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnHexTilePointerEnter, OnPointerEnterTile);
            Events.Unsubscribe(EventType.OnHexTilePointerExit, OnPointerExitTile);
        }

        public void OnPointerEnterTile(IMessage message)
        {
            if (message.Data is OnHexTilePointerEnterEventArgs args)
            {
                var tile = args.tile;

                if (!tile.IsRevealed)
                {
                    return;
                }

                m_currentTooltip  = GetTooltip(tile.TileData, tile.ClientTileData);

                if (m_currentTooltip != null)
                {
                    m_currentTooltip.Open(tile);
                }
            }
        }

        public void OnPointerExitTile(IMessage message)
        {
            // if (message.Data is OnHexTilePointerExitEventArgs args)
            // {
            //     var tile = args.tile;
            //     var toolTip = GetTooltip(tile.TileData, tile.ClientTileData);
            //
            //     if (toolTip != null)
            //     {
            //         toolTip.Close();
            //     }
            // }

            if (m_currentTooltip)
            {
                m_currentTooltip.Close();
            }
        }

        public NodeInfoTooltip GetTooltip(FableTileData tileData, ClientTileData clientTileData)
        {
            if (clientTileData == null)
            {
                clientTileData = new ClientTileData();
                clientTileData.Init(tileData);
            }

            var battleData = Db.BattleDatabase.GetBattleData(tileData.battleUid);

            switch (tileData.NodeType)
            {
                case NodeType.Battle when battleData != null && battleData.BattleType == BattleType.Boss && clientTileData.Complete:
                    return bossComplete;
                case NodeType.MultiBattle when battleData != null && clientTileData.Complete:
                    return multiBattleComplete;
                case NodeType.Battle when battleData != null && clientTileData.Complete:
                    return battleCompleted;
                case NodeType.Battle when battleData != null && battleData.BattleType == BattleType.Boss:
                {
                    if (clientTileData.Complete) return bossComplete;
                    return bossAvailable;
                }
                case NodeType.Battle:
                {
                    if (clientTileData.Complete) return battleCompleted;
                    return battleAvailable;
                }
                case NodeType.Chest:
                {
                    if (clientTileData.Complete) return treasureOpened;
                    return treasureAvailable;
                }
                
                case NodeType.MultiBattle:
                {
                    if (clientTileData.Complete) return multiBattleComplete;
                    return multiBattleAvailable;
                }
                case NodeType.Event1:
                case NodeType.Event2:
                {
                    if (clientTileData.Complete) return eventViewed;
                    return eventAvailable;
                }
                case NodeType.Shop:
                {
                    if (tileData.ShopType == ShopType.Card)
                    {
                        return cardShopAvailable;
                    }
                    else
                    {
                        return relicShopAvailable;
                    }
                }
                case NodeType.ManaMeld:
                    return manaForgeAvailable;
                case NodeType.ExitUp:
                case NodeType.ExitDown:
                case NodeType.ExitRight:
                case NodeType.ExitLeft:
                    return passageAvailable;
                case NodeType.ExitChapterFinished:
                    return exitAvailable;
            }

            return null;
        }
    }
}