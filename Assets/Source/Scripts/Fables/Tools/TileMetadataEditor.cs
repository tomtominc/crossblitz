using CrossBlitz.Databases;
using CrossBlitz.Developer.Tools;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Grid;
using CrossBlitz.Shop;
using CrossBlitz.UI.Widgets;
using UnityEngine;

namespace CrossBlitz.Fables.Tools
{
    public class TileMetadataEditor : DatabaseEditorView
    {
        public LayoutItem tileNameLayout;
        public LayoutItem shopLinkLayout;
        public LayoutItem dialogueLinkLayout;

        public GameObject tileInspectorWindow;
        public GameObject noTileToInspectWindow;
        public GameObject noSelectionWindow;
        public ShopDataInspector shopDataInspector;

        private FableTileData _tile;

        private void OnDisable()
        {
            if (FablesMapController.Instance && _tile != null)
            {
                FablesMapController.Instance.SaveChapter();
            }
        }

        public override void Open()
        {
            _tile = _options.tile;

            if (_tile == null)
            {
                tileInspectorWindow.SetActive(false);
                noTileToInspectWindow.SetActive(true);
                return;
            }

            tileInspectorWindow.SetActive(true);
            noTileToInspectWindow.SetActive(false);

            tileNameLayout.SetLayoutItem(new LayoutItemData
            {
                title = _tile.displayName,
                subTitle = _tile.uid.ToString()
            });

            noSelectionWindow.SetActive(true);
            shopDataInspector.SetActive(false);

            dialogueLinkLayout.OnButtonPressed -= OnDialogueLinkPressed;
            dialogueLinkLayout.OnButtonPressed += OnDialogueLinkPressed;

            var cutsceneData=Db.CutsceneDatabase.GetCutscene(_tile.cutsceneUid);

            if (cutsceneData != null)
            {
                dialogueLinkLayout.buttons[1].SetActive(true);
            }
            else
            {
                dialogueLinkLayout.buttons[1].SetActive(false);
            }

            dialogueLinkLayout.SetLayoutItem(new LayoutItemData
            {
                title = cutsceneData != null ? cutsceneData.EventTitle : "No Cutscene Linked",
                subTitle = cutsceneData != null ? cutsceneData.Uid : "0"
            });

            switch (_tile.NodeType)
            {
                case NodeType.None:
                    Debug.LogError("Cannot edit a tile with no node!");
                    break;
                case NodeType.Start:
                    break;
                case NodeType.Chest:
                    break;
                case NodeType.Battle:
                    break;
                case NodeType.Event1:
                    break;
                case NodeType.Event2:
                    break;
                case NodeType.Shop:
                {
                    // var linkedShop = Db.ShopsDatabase.GetShopByUid(_tile.shopUid);
                    //
                    // shopLinkLayout.SetLayoutItem(new LayoutItemData
                    // {
                    //     title = linkedShop == null ? "No Shop Linked" : linkedShop.displayName,
                    //     subTitle = linkedShop == null ? "0" : linkedShop.Uid,
                    //     data = linkedShop?.Uid
                    // });
                    //
                    // shopLinkLayout.OnMainButtonValueChanged -= OnShopToggled;
                    // shopLinkLayout.OnMainButtonValueChanged += OnShopToggled;
                    // shopLinkLayout.OnRightButtonClicked -= OnLinkNewShopClicked;
                    // shopLinkLayout.OnRightButtonClicked += OnLinkNewShopClicked;
                    // shopLinkLayout.SetActive(true);
                    // shopLinkLayout.mainButton.Toggle.isOn = false;
                    break;
                }
                case NodeType.ExitUp:
                    break;
                case NodeType.ExitDown:
                    break;
                case NodeType.ExitRight:
                    break;
                case NodeType.ExitLeft:
                    break;
            }
        }

        private void OnTileNameChanged(string newTileName, LayoutItem item)
        {
            _tile.displayName = newTileName;
        }

        /// <summary>
        /// Triggers when the player wants to link to the dialogue,
        /// this opens up the dialogue menu where they can create/edit dialogue
        /// then link it back to the tile.
        /// </summary>
        /// <param name="item"></param>
        private void OnDialogueLinkPressed(ContentButton button, LayoutItem item)
        {
            if (button == null) return;
            if (button.name.Equals("LinkCutscene"))
            {
                ToolsNavigator.Instance.OpenCutsceneEditMenu(new DatabaseEditorOpenOptions
                    {viewOnly = false, linkOptionEnabled = true});
                ToolsNavigator.Instance.OnLinked += OnDialogueLinkConfirmed;
            }
            else if (button.name.Equals("UnLinkCutscene"))
            {
                OnDialogueLinkConfirmed(string.Empty);
            }
        }

        private async void OnDialogueLinkConfirmed(string cutsceneLink)
        {
            ToolsNavigator.Instance.OnLinked -= OnDialogueLinkConfirmed;
            _tile.cutsceneUid = cutsceneLink;
            _options.tile = _tile;
            if (!ToolsNavigator.Instance.OpenTileMetadata(_options))
            {
                Open(_options);
            }
            Db.FablesDatabase.Save();
        }

        private void OnShopToggled(bool isOn, LayoutItem item)
        {
            if (isOn)
            {
                noSelectionWindow.SetActive(false);
                shopDataInspector.SetActive(true);
                // var linkedShop = Db.ShopsDatabase.GetShopByUid(_tile.shopUid);
                // shopDataInspector.SetupShop(linkedShop, true, true);
            }
            else
            {
                noSelectionWindow.SetActive(true);
                shopDataInspector.SetActive(false);
            }
        }

        private void OnLinkNewShopClicked(LayoutItem item)
        {
            ToolsNavigator.Instance.OpenShopEditMenu(new DatabaseEditorOpenOptions { viewOnly = false, linkOptionEnabled = true });
            ToolsNavigator.Instance.OnLinked += OnLinkShopConfirmed;
        }

        private async void OnLinkShopConfirmed(string shopUid)
        {
            ToolsNavigator.Instance.OnLinked -= OnLinkShopConfirmed;
            // _tile.shopUid = shopUid;
            // _options.tile = _tile;
            // ToolsNavigator.Instance.OpenTileMetadata(_options);

            Db.FablesDatabase.Save();
        }
    }
}