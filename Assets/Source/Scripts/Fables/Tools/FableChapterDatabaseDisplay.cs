using System;
using System.Linq;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Data;
using CrossBlitz.UI.Widgets;
using CrossBlitz.ViewAPI.Popups;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Fables.Tools
{
    public class FableChapterDatabaseDisplay : MonoBehaviour
    {
        public GameObject errorMessage;
        public RectTransform chapterLayout;
        public LayoutItem chapterDatabaseItemPrefab;
        public Button addChapterButton;
        public GameObject noSelectionWindow;
        public FableChapterDetailsWindow chapterDetailsWindow;
        public FableChapterCreateNewPopup createNewChapterPopup;

        private FableChapterData _chapterToDelete;

        public event Action<FableChapterData, int> OnEditMap;

        private void Start()
        {
            //chapterDetailsWindow.OnEditMap += EditMap;
            addChapterButton.onClick.AddListener(OnAddedChapter);
            createNewChapterPopup.OnSubmit += OnCreateNewChapterPopupSubmit;
        }

        public void SetupDatabaseDisplay(string chapterUid)
        {
            // if (Db.FablesDatabase == null || Db.FablesDatabase.Chapters == null || Db.FablesDatabase.Chapters.Count <= 0)
            // {
            //     errorMessage.SetActive(true);
            //     noSelectionWindow.SetActive(true);
            //     return;
            // }

            noSelectionWindow.SetActive(string.IsNullOrEmpty(chapterUid));
            chapterLayout.DestroyChildren();

            var foundSelection = false;
            // var fables = Db.FablesDatabase.Chapters;
            //
            // for (var i = 0; i < fables.Count; i++)
            // {
            //     var fable = fables[i];
            //     var chapterItem = Instantiate(chapterDatabaseItemPrefab, chapterLayout);
            //
            //     chapterItem.SetLayoutItem(new LayoutItemData
            //     {
            //         title = fable.ChapterName,
            //         subTitle = $"{fable.ChapterHero} Book {fable.BookNumber} Chapter {fable.ChapterNumber}",
            //         sideIconAnimation = fable.ChapterHero.ToLower(),
            //         data = fable
            //     });
            //
            //     if (chapterUid == fable.Uid)
            //     {
            //         foundSelection = true;
            //         chapterItem.mainButton.Toggle.isOn = true;
            //         chapterDetailsWindow.SetActive(true);
            //         chapterDetailsWindow.SetupChapter(fable);
            //     }
            //
            //     chapterItem.OnMainButtonValueChanged += OnChapterSelected;
            //     chapterItem.OnDeleteButtonClicked += OnChapterDeleted;
            // }

            chapterDetailsWindow.SetActive(foundSelection);
            noSelectionWindow.SetActive(!foundSelection);
        }

        private void OnAddedChapter()
        {
            ShowCreateNewPopup();
        }

        private void ShowCreateNewPopup()
        {
            createNewChapterPopup.Open();
        }

        private void OnCreateNewChapterPopupSubmit(CreateNewFableParameters createParams)
        {
            var fable = Db.FablesDatabase.AddChapter(createParams);
            SetupDatabaseDisplay(fable.Uid);
        }

        private void OnChapterSelected(bool isOn, LayoutItem item)
        {
            var fable = (FableChapterData) item.Data.data;
            noSelectionWindow.SetActive(false);
            chapterDetailsWindow.SetActive(true);
            chapterDetailsWindow.SetupChapter(fable);

            LayoutRebuilder.MarkLayoutForRebuild(chapterLayout);
        }

        private void OnChapterDeleted(LayoutItem item)
        {
            _chapterToDelete = (FableChapterData) item.Data.data;

            PopupController.Open(new PopupInfo
            {
                title = "Delete Chapter?",
                body = "Are you sure you want to delete this chapter? This action will be permanent after you save.",
                deny = "Keep Chapter",
                confirm = "Delete Chapter"
            });

            PopupController.OnPopupChoice += OnDeleteActionResponse;
        }

        private void EditMap(int index)
        {
            OnEditMap?.Invoke(chapterDetailsWindow.ChapterData, index);
        }

        private void OnDeleteActionResponse(bool delete)
        {
            if (delete)
            {
                Db.FablesDatabase.DeleteChapter(_chapterToDelete.Uid);
                SetupDatabaseDisplay(string.Empty);
            }
        }
    }
}