using System;
using CrossBlitz.Databases;
using CrossBlitz.Developer.Tools;
using CrossBlitz.Fables.Data;
using UnityAsync;
using UnityEngine.UI;

namespace CrossBlitz.Fables.Tools
{
    public class FableChapterEditor : DatabaseEditorView
    {
        public FableChapterDatabaseDisplay chapterDatabaseDisplay;
        public Button undoButton;
        public Button saveButton;
        public event Action<FableChapterData, int> OnEditMap;

        private void Start()
        {
            chapterDatabaseDisplay.OnEditMap += EditMap;
            undoButton.onClick.AddListener(OnUndo);
            saveButton.onClick.AddListener(OnSave);
        }

        public override void Open()
        {
            chapterDatabaseDisplay.SetupDatabaseDisplay(string.Empty);
            OnUndo();
        }

        private async void OnUndo()
        {
            ToolsNavigator.Instance.loadingOverlay.SetActive(false);
            chapterDatabaseDisplay.SetupDatabaseDisplay(string.Empty);
        }

        private void OnSave()
        {
            Db.FablesDatabase.Save();
        }

        private void EditMap(FableChapterData chapterData, int index)
        {
            OnEditMap?.Invoke(chapterData, index);
        }
    }
}