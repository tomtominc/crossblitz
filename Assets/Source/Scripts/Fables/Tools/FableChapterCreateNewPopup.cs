using System;
using System.Collections.Generic;
using CrossBlitz.PlayFab;
using GameDataEditor;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Fables.Tools
{
    public class CreateNewFableParameters
    {
        public string hero;
        public int bookNumber;
        public int chapterNumber;
    }

    public class FableChapterCreateNewPopup : MonoBehaviour
    {
        public TMP_Dropdown heroDropdown;
        public TMP_InputField bookNumberInput;
        public TMP_InputField chapterNumberInput;
        public TMP_InputField versionNumber;
        public Button submitButton;
        public Button cancelButton;

        private CreateNewFableParameters _createParams;
        public event Action<CreateNewFableParameters> OnSubmit;
        private void Start()
        {
            heroDropdown.onValueChanged.AddListener(OnHeroChanged);
            bookNumberInput.onValueChanged.AddListener(OnBookNumberChanged);
            chapterNumberInput.onValueChanged.AddListener(OnChapterNumberChanged);
            versionNumber.onValueChanged.AddListener(OnVersionNumberChanged);
            submitButton.onClick.AddListener(OnSubmitButton);
            cancelButton.onClick.AddListener(OnCancelButton);
        }
        public void Open()
        {
            _createParams = new CreateNewFableParameters();
            _createParams.hero = "Redcroft";
            _createParams.bookNumber = 1;
            _createParams.chapterNumber = 1;

            heroDropdown.SetValueWithoutNotify(0);
            bookNumberInput.SetTextWithoutNotify(_createParams.bookNumber.ToString());
            chapterNumberInput.SetTextWithoutNotify(_createParams.chapterNumber.ToString());

            gameObject.SetActive(true);
        }

        public void Close()
        {
            gameObject.SetActive(false);
        }

        private void OnHeroChanged(int value)
        {
            _createParams.hero = heroDropdown.options[value].text;
        }

        private void OnBookNumberChanged(string value)
        {
            _createParams.bookNumber = int.Parse(value);
        }

        private void OnChapterNumberChanged(string value)
        {
            _createParams.chapterNumber = int.Parse(value);
        }

        private void OnVersionNumberChanged(string value)
        {
            //_createParams.versionNumber = value;
        }

        private void OnSubmitButton()
        {
            OnSubmit?.Invoke(_createParams);
            Close();
        }

        private void OnCancelButton()
        {
            Close();
        }
    }
}