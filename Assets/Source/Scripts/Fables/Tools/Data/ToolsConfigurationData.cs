using CrossBlitz.Fables.Data;

namespace CrossBlitz.Fables.Tools.Data
{
    /// <summary>
    /// This is used in the scene loading params to setup the tools screen appropriately
    /// </summary>
    public class ToolsConfigurationData
    {
        public FableTileData CurrentTile;
    }
}