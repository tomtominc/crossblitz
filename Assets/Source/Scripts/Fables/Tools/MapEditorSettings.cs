using System;
using System.Linq;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Grid;
using TakoBoyStudios.Events;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Fables.Tools
{
    public class MapEditorSettings : MonoBehaviour
    {
        public InputField nameField;
        public Dropdown weatherField;
        public Button topRoomButton;
        public Text topRoomButtonLabel;
        public Button rightRoomButton;
        public Text rightRoomButtonLabel;
        public Button leftRoomButton;
        public Text leftRoomButtonLabel;
        public Button bottomRoomButton;
        public Text bottomRoomButtonLabel;
        public Button editorButton;
        public Button saveButton;

        private void Start()
        {
            weatherField.options.Clear();
            weatherField.AddOptions(Enum.GetNames(typeof(FableRoomData.RoomWeather)).ToList());
            weatherField.onValueChanged.AddListener(OnWeatherChanged);

            topRoomButton.onClick.AddListener(OnGoToTopRoom);
            rightRoomButton.onClick.AddListener(OnGoToRightRoom);
            leftRoomButton.onClick.AddListener(OnGoToLeftRoom);
            bottomRoomButton.onClick.AddListener(OnGoToBottomRoom);

            saveButton.onClick.AddListener(OnSaveButton);
            editorButton.onClick.AddListener(OnEditorButton);

            Events.Subscribe(EventType.OnEnteredFableMap, OnRoomChanged);

            OnRoomChanged(null);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnEnteredFableMap, OnRoomChanged);
        }

        private void OnRoomChanged(IMessage message)
        {
            var currentRoom = FablesMapController.Instance.GetCurrentRoom();
            nameField.SetTextWithoutNotify(currentRoom.DisplayName);
            weatherField.SetValueWithoutNotify((int)currentRoom.Weather);
            topRoomButtonLabel.text =
                currentRoom.GetRoomInDirection(FablesMapController.Instance.MapData,
                    FableRoomData.RoomDirection.Top) == null
                    ? "Create"
                    : "Open";
            rightRoomButtonLabel.text =
                currentRoom.GetRoomInDirection(FablesMapController.Instance.MapData,
                    FableRoomData.RoomDirection.Right) == null
                    ? "Create"
                    : "Open";
            leftRoomButtonLabel.text =
                currentRoom.GetRoomInDirection(FablesMapController.Instance.MapData,
                    FableRoomData.RoomDirection.Left) == null
                    ? "Create"
                    : "Open";
            bottomRoomButtonLabel.text =
                currentRoom.GetRoomInDirection(FablesMapController.Instance.MapData,
                    FableRoomData.RoomDirection.Bottom) == null
                    ? "Create"
                    : "Open";
        }

        private void OnGoToTopRoom()
        {
            FablesMapController.Instance.AddRoom(FableRoomData.RoomDirection.Top);
        }

        private void OnGoToRightRoom()
        {
            FablesMapController.Instance.AddRoom(FableRoomData.RoomDirection.Right);
        }

        private void OnGoToLeftRoom()
        {
            FablesMapController.Instance.AddRoom(FableRoomData.RoomDirection.Left);
        }

        private void OnGoToBottomRoom()
        {
            FablesMapController.Instance.AddRoom(FableRoomData.RoomDirection.Bottom);
        }

        private void OnEditorButton()
        {
            Events.Publish(this, EventType.OpenNodeMetadataEditor, new OpenNodeMetadataEditorEventArgs());
        }

        private void OnWeatherChanged(int value)
        {
            var room = FablesMapController.Instance.GetCurrentRoom();
            room.Weather = (FableRoomData.RoomWeather) value;
            Events.Publish(this, EventType.OnFablesWeatherChanged, null);
        }

        private void OnSaveButton()
        {
            var room = FablesMapController.Instance.GetCurrentRoom();
            room.DisplayName = nameField.text;
            room.Weather = (FableRoomData.RoomWeather) weatherField.value;
            FablesMapController.Instance.SaveChapter();
        }
    }
}