using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CrossBlitz.Databases;
using CrossBlitz.Developer.Tools;
using CrossBlitz.Fables.Data;
using CrossBlitz.UI.Widgets;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Fables.Tools
{
    public class FableChapterDetailsWindow : MonoBehaviour
    {
        public TMP_InputField nameInputField;
        public TMP_InputField bookNumberInput;
        public TMP_InputField chapterNumberInput;
        public LayoutItem dialogueLinkLayout;
        public TMP_InputField prologueInputField;
        public TMP_Dropdown heroDropdown;
        public GameObject noMapErrorMessage;
        public RectTransform mapLayout;
        public LayoutItem mapLayoutItemPrefab;
        public Button addMapButton;

        private FableChapterData _chapterData;
        public FableChapterData ChapterData => _chapterData;

        //public event Action<int> OnEditMap;

        private void Start()
        {
            nameInputField.onValueChanged.AddListener(ChangedName);
            bookNumberInput.onValueChanged.AddListener(ChangedBookNumber);
            chapterNumberInput.onValueChanged.AddListener(ChangedChapterNumber);
            prologueInputField.onValueChanged.AddListener(ChangedPrologue);
            heroDropdown.onValueChanged.AddListener(OnHeroChanged);
            addMapButton.onClick.AddListener(AddMap);
        }

        public void SetupChapter( FableChapterData chapterData )
        {
            _chapterData = chapterData;

            nameInputField.SetTextWithoutNotify(_chapterData.ChapterName);
            bookNumberInput.SetTextWithoutNotify(_chapterData.BookNumber);
            chapterNumberInput.SetTextWithoutNotify(_chapterData.ChapterNumber.ToString());
            prologueInputField.SetTextWithoutNotify(_chapterData.ChapterPrologue);
            heroDropdown.SetValueWithoutNotify(heroDropdown.options.FindIndex(option => option.text == _chapterData.ChapterHero));

            dialogueLinkLayout.OnButtonPressed -= OnDialogueLinkPressed;
            dialogueLinkLayout.OnButtonPressed += OnDialogueLinkPressed;

            var cutsceneData=Db.CutsceneDatabase.GetCutscene(_chapterData.IntroCutsceneUid);

            if (cutsceneData != null)
            {
                dialogueLinkLayout.buttons[1].SetActive(true);
            }
            else
            {
                dialogueLinkLayout.buttons[1].SetActive(false);
            }

            dialogueLinkLayout.SetLayoutItem(new LayoutItemData
            {
                title = cutsceneData != null ? cutsceneData.EventTitle : "No Cutscene Linked",
                subTitle = cutsceneData != null ? cutsceneData.Uid : "0"
            });

            UpdateMapList();
        }

        private void OnDialogueLinkPressed(ContentButton button, LayoutItem item)
        {
            if (button == null) return;
            if (button.name.Equals("LinkCutscene"))
            {
                ToolsNavigator.Instance.OpenCutsceneEditMenu(new DatabaseEditorOpenOptions
                    {viewOnly = false, linkOptionEnabled = true});
                ToolsNavigator.Instance.OnLinked += OnDialogueLinkConfirmed;
            }
            else if (button.name.Equals("UnLinkCutscene"))
            {
                OnDialogueLinkConfirmed(string.Empty);
            }
        }

        private async void OnDialogueLinkConfirmed(string cutsceneLink)
        {
            ToolsNavigator.Instance.OnLinked -= OnDialogueLinkConfirmed;
            _chapterData.IntroCutsceneUid = cutsceneLink;
            ToolsNavigator.Instance.OpenChapterEditMenu(new DatabaseEditorOpenOptions());
            Db.FablesDatabase.Save();
        }

        public async void UpdateMapList()
        {
            // noMapErrorMessage.SetActive(_chapterData.Rooms.Count <= 0);
            //
            // mapLayout.DestroyChildren();
            //
            // for (var i = 0; i < _chapterData.Maps.Count; i++)
            // {
            //     var mapItem = Instantiate(mapLayoutItemPrefab, mapLayout);
            //     mapItem.SetLayoutItem(new LayoutItemData
            //     {
            //         title = $"{_chapterData.ChapterName} - Map{i+1}",
            //         indexInList = i,
            //         listCount = _chapterData.Maps.Count,
            //         data = _chapterData.Maps[i]
            //     });
            //     mapItem.OnRightButtonClicked += OnEditMapButton;
            //     mapItem.OnDeleteButtonClicked += OnDeleteMapButton;
            //     mapItem.OnMoveUpList += OnMapMoveUpList;
            //     mapItem.OnMoveDownList += OnMapMoveDownList;
            // }
            //
            // await Task.Delay(100);
            //
            // LayoutRebuilder.MarkLayoutForRebuild(mapLayout.parent.RectTransform());
        }

        private void ChangedName(string chapterName)
        {
            _chapterData.ChapterName = chapterName;
            //Db.FablesDatabase.SetFableChapterData(_chapterData);
            Debug.Log("On Fable Name Changed!");
        }

        private void ChangedBookNumber(string bookNumber)
        {
            _chapterData.BookNumber = bookNumber;
            //Db.FablesDatabase.SetFableChapterData(_chapterData);
            Debug.Log("On Fable Book Changed!");
        }

        private void ChangedChapterNumber(string chapterNumber)
        {
            _chapterData.ChapterNumber = int.Parse( chapterNumber );
            //Db.FablesDatabase.SetFableChapterData(_chapterData);
            Debug.Log("On Fable Chapter Changed!");
        }

        private void ChangedPrologue(string chapterPrologue)
        {
            _chapterData.ChapterPrologue = chapterPrologue;
           // Db.FablesDatabase.SetFableChapterData(_chapterData);
            Debug.Log("On Fable Prologue Changed!");
        }

        private void OnHeroChanged( int heroIndex )
        {
            _chapterData.ChapterHero = heroDropdown.options[heroIndex].text;
           // Db.FablesDatabase.SetFableChapterData(_chapterData);
            Debug.Log($"On Fable Hero Changed! {_chapterData.ChapterHero}");
        }

        private void AddMap()
        {
            var map = new FableMapData();
            map.UpdateIdentifier();
            map.Rooms = new List<FableRoomData>();
            //_chapterData.Maps.Add(map);
           // Db.FablesDatabase.SetFableChapterData(_chapterData);
            UpdateMapList();
        }

        private void OnEditMapButton(LayoutItem mapItem)
        {
            Debug.Log("edit button is presssed!!!");
            var map = (FableMapData) mapItem.Data.data;
            //var index = _chapterData.Maps.IndexOf(map);
            //OnEditMap?.Invoke(index);
        }

        private void OnDeleteMapButton(LayoutItem mapItem)
        {
            var mapData = (FableMapData) mapItem.Data.data;
            //_chapterData.Maps.RemoveAll(map => map.Uid == mapData.Uid);
           // Db.FablesDatabase.SetFableChapterData(_chapterData);
            UpdateMapList();
        }

        private void OnMapMoveUpList(LayoutItem mapItem)
        {
            // var mapData = (FableMapData) mapItem.Data.data;
            // var mapInChapter = _chapterData.Maps.Find(map => map.Uid == mapData.Uid);
            // if (mapInChapter != null)
            // {
            //     var currentIndex = _chapterData.Maps.IndexOf(mapInChapter);
            //     if (currentIndex == 0)
            //     {
            //         Debug.LogError("This map is already at index 0");
            //         return;
            //     }
            //     //_chapterData.Maps.RemoveAll(map => map.Uid == mapInChapter.Uid);
            //     //_chapterData.Maps.Insert(currentIndex-1, mapInChapter);
            //    // Db.FablesDatabase.SetFableChapterData(_chapterData);
            //
            //     UpdateMapList();
            // }
        }

        private void OnMapMoveDownList(LayoutItem mapItem)
        {
            // var mapData = (FableMapData) mapItem.Data.data;
            // var mapInChapter = _chapterData.Maps.Find(map => map.Uid == mapData.Uid);
            // if (mapInChapter != null)
            // {
            //     var currentIndex = _chapterData.Maps.IndexOf(mapInChapter);
            //     if (currentIndex >= _chapterData.Maps.Count-1)
            //     {
            //         Debug.LogError("This map is already at the end of the list.");
            //         return;
            //     }
            //     _chapterData.Maps.RemoveAll(map => map.Uid == mapInChapter.Uid);
            //     _chapterData.Maps.Insert(currentIndex+1, mapInChapter);
            //    // Db.FablesDatabase.SetFableChapterData(_chapterData);
            //
            //     UpdateMapList();
            // }
        }
    }
}