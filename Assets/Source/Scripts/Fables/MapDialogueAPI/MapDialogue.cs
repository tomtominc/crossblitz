using CrossBlitz.Dialogue;
using CrossBlitz.InputAPI;
using UnityEngine;

namespace CrossBlitz.Fables.MapDialogueAPI
{
    public class MapDialogue : InputMode
    {
        public CanvasGroup canvas;
        public MapDialogueBox dialogueBox;

        public override void StartMode()
        {
            canvas.interactable = true;
            canvas.blocksRaycasts = true;
        }

        public override void UpdateMode()
        {
            if (InputProcessor.IsLeftMouseDown())
            {
                dialogueBox.OnNextButton();
            }
        }

        public override void EndMode()
        {
            canvas.interactable = false;
            canvas.blocksRaycasts = false;
        }

        public void OpenMapDialogue(MapDialogueCustomProperties properties)
        {
            dialogueBox.InitDialogueBox(properties);
        }
    }
}