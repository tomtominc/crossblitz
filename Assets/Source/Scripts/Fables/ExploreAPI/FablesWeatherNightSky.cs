using CrossBlitz.Pools;
using DG.Tweening;
using KennethDevelops.ProLibrary.Managers;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.Fables.ExploreAPI
{
    public class FablesWeatherNightSky: FablesWeather
    {
        public SpriteAnimation[] twinkleObjects;

        private const float MinTwinkleDelay = 0.5f;
        private const float MaxTwinkleDelay = 1.5f;

        private float m_twinkleDelay;
        private bool m_twinklePlay;

        private void Start()
        {
            m_twinkleDelay = Random.Range(MinTwinkleDelay, MaxTwinkleDelay);
            m_twinklePlay = false;
        }

        public override void Update()
        {
            m_twinkleDelay -= Time.deltaTime;

            if (m_twinkleDelay <= 0)
            {
                m_twinkleDelay = Random.Range(MinTwinkleDelay, MaxTwinkleDelay);

                var x = Random.Range(-175, 320);
                var y = Random.Range(-175, 175);
                var index = Random.Range(0, 3);

                twinkleObjects[index].RectTransform().anchoredPosition = new Vector3(x, y);
                twinkleObjects[index].SetActive(true);
                twinkleObjects[index].Play("twinkle");
            }
        }
    }
}