using System;
using BlendModes;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Grid;
using Sirenix.OdinInspector;
using TakoBoyStudios.Events;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Fables.ExploreAPI
{
    public class FablesBackground : MonoBehaviour
    {
        [BoxGroup("Background Preview")] public BlendModeEffect foregroundBlend;
        [BoxGroup("Background Preview")] public Image foreground1;
        [BoxGroup("Background Preview")] public Image backgroundGradient1;
        [BoxGroup("Background Preview")] public Image backgroundGradient2;
        [BoxGroup("Background Preview")] public Image backgroundGradient3;
        [BoxGroup("Background Preview")] public Image backgroundGradient4;
        [BoxGroup("Background Preview")] public Image backgroundGradient5;
        [BoxGroup("Background Preview")] public Image backgroundGradient6;
        [BoxGroup("Background Preview")] public Image cornerLayer1a;
        [BoxGroup("Background Preview")] public Image cornerLayer2a;
        [BoxGroup("Background Preview")] public Image cornerLayer1b;
        [BoxGroup("Background Preview")] public Image cornerLayer2b;
        [BoxGroup("Background Preview")] public Image diamondPattern;
        [BoxGroup("Background Preview")] public UIBackground diamondScroller;

        [BoxGroup("Weather")] public FablesWeatherRain rainHeavy;
        [BoxGroup("Weather")] public FablesWeatherRain rainMedium;
        [BoxGroup("Weather")] public GameObject orbs;
        [BoxGroup("Weather")] public FablesWeatherNightSky nightSky;

        private float m_time;

        private void Awake()
        {
            Events.Subscribe(EventType.OnEnteredFableMap, SetupBackground);
            Events.Subscribe(EventType.OnFablesWeatherChanged, WeatherChanged);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnEnteredFableMap, SetupBackground);
            Events.Unsubscribe(EventType.OnFablesWeatherChanged, WeatherChanged);
        }

        private void WeatherChanged(IMessage message)
        {
            SetupPalette(FablesMapController.Instance.GetCurrentRoom());
        }

        private Color m_foreground1Target;
        private Color m_backgroundGradient1Target;
        private Color m_backgroundGradient2Target;
        private Color m_backgroundGradient3Target;
        private Color m_backgroundGradient4Target;
        private Color m_backgroundGradient5Target;
        private Color m_backgroundGradient6Target;
        private Color m_cornerLayer1aTarget;
        private Color m_cornerLayer2aTarget;
        private Color m_cornerLayer1bTarget;
        private Color m_cornerLayer2bTarget;
        private Color m_diamondPatternTarget;
        
        private void SetupPalette(FableRoomData room)
        {
            var paletteName = string.IsNullOrEmpty(room.BackgroundPalette) ? "blue-sky" : room.BackgroundPalette;
            var palette = Db.FableBackgroundPalettePresetsDatabase.GetPalette(paletteName);

            m_foreground1Target = palette.foregroundColor1.ToColor(palette.foregroundAlpha1 / 255f);
            m_backgroundGradient1Target = palette.backgroundColor1.ToColor(palette.backgroundAlpha1 / 255f);
            m_backgroundGradient2Target = palette.backgroundColor2.ToColor(palette.backgroundAlpha2 / 255f);
            m_backgroundGradient3Target = palette.backgroundColor3.ToColor(palette.backgroundAlpha3 / 255f);
            m_backgroundGradient4Target = palette.backgroundColor4.ToColor(palette.backgroundAlpha4 / 255f);
            m_backgroundGradient5Target = palette.backgroundColor5.ToColor(palette.backgroundAlpha5 / 255f);
            m_backgroundGradient6Target = palette.backgroundColor6.ToColor(palette.backgroundAlpha6 / 255f);
            m_cornerLayer1aTarget = palette.cornerLayerColor1.ToColor(palette.cornerLayerAlpha1 / 255f);
            m_cornerLayer2aTarget = palette.cornerLayerColor2.ToColor(palette.cornerLayerAlpha2 / 255f);
            m_cornerLayer1bTarget = palette.cornerLayerColor1.ToColor(palette.cornerLayerAlpha1 / 255f);
            m_cornerLayer2bTarget = palette.cornerLayerColor2.ToColor(palette.cornerLayerAlpha2 / 255f);
            m_diamondPatternTarget = palette.diamondPatternColor.ToColor(palette.diamondPatternAlpha / 255f);
            diamondScroller.m_speed = palette.scrollSpeed * 0.01f;
            foregroundBlend.BlendMode = palette.foregroundBlend;
            m_time = 0;

            switch (room.Weather)
            {
                case FableRoomData.RoomWeather.RainMedium:
                    rainMedium.SetActive(true);
                    rainHeavy.SetActive(false);
                    orbs.SetActive(false);
                    nightSky.SetActive(false);
                    break;
                case FableRoomData.RoomWeather.RainHeavy:
                    rainMedium.SetActive(false);
                    rainHeavy.SetActive(true);
                    orbs.SetActive(false);
                    nightSky.SetActive(false);
                    break;
                case FableRoomData.RoomWeather.Orbs:
                    rainMedium.SetActive(false);
                    rainHeavy.SetActive(false);
                    orbs.SetActive(true);
                    nightSky.SetActive(false);
                    break;
                case FableRoomData.RoomWeather.NightSky:
                    rainMedium.SetActive(false);
                    rainHeavy.SetActive(false);
                    orbs.SetActive(false);
                    nightSky.SetActive(true);
                    break;
                default:
                    rainMedium.SetActive(false);
                    rainHeavy.SetActive(false);
                    orbs.SetActive(false);
                    nightSky.SetActive(false);
                    break;
            }
        }

        private void Update()
        {
            if (m_time < 1)
            {
                const float duration = 0.24f;
                m_time += Time.deltaTime;// duration;
                foreground1.color = Color.Lerp(foreground1.color, m_foreground1Target, m_time);
                backgroundGradient1.color = Color.Lerp(backgroundGradient1.color, m_backgroundGradient1Target, m_time);
                backgroundGradient2.color = Color.Lerp(backgroundGradient2.color, m_backgroundGradient2Target, m_time);
                backgroundGradient3.color = Color.Lerp(backgroundGradient3.color, m_backgroundGradient3Target, m_time);
                backgroundGradient4.color = Color.Lerp(backgroundGradient4.color, m_backgroundGradient4Target, m_time);
                backgroundGradient5.color = Color.Lerp(backgroundGradient5.color, m_backgroundGradient5Target, m_time);
                backgroundGradient6.color = Color.Lerp(backgroundGradient6.color, m_backgroundGradient6Target, m_time);
                cornerLayer1a.color = Color.Lerp(cornerLayer1a.color, m_cornerLayer1aTarget, m_time);
                cornerLayer2a.color = Color.Lerp(cornerLayer2a.color, m_cornerLayer2aTarget, m_time);
                cornerLayer1b.color = Color.Lerp(cornerLayer1b.color, m_cornerLayer1bTarget, m_time);
                cornerLayer2b.color = Color.Lerp(cornerLayer2b.color, m_cornerLayer2bTarget, m_time);
                diamondPattern.color = Color.Lerp(diamondPattern.color, m_diamondPatternTarget, m_time);
            }
        }

        public void SetupBackground(IMessage message)
        {
            if (message.Data is OnEnteredFableMapEventArgs args)
            {
                var mapData = args.mapData;
                var currentRoom = mapData.Rooms[args.currentRoomIndex];
                SetupPalette(currentRoom);
            }
        }
    }
}