using CrossBlitz.Pools;
using DG.Tweening;
using KennethDevelops.ProLibrary.Managers;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.Fables.ExploreAPI
{
    public class FablesWeatherRain : FablesWeather
    {
        public CanvasGroup flash;
        public bool useSplashes;
        [ShowIf("useSplashes")] public Camera splashCamera;
        [ShowIf("useSplashes")] public PoolManager splashPool;

        private const float MinFlashDelay = 4;
        private const float MaxFlashDelay = 10;

        private const float MinSplashDelay = 0.04f;
        private const float MaxSplashDelay = 0.16f;

        private float _flashDelay;
        private float m_splashDelay;

        private void Start()
        {
            flash.alpha = 0;
            _flashDelay = Random.Range(MinFlashDelay, MaxFlashDelay);
            m_splashDelay = Random.Range(MinSplashDelay, MaxSplashDelay);
        }

        public override void Update()
        {
            if (useSplashes)
            {
                m_splashDelay -= Time.deltaTime;

                if (m_splashDelay <= 0)
                {
                    m_splashDelay = Random.Range(MinSplashDelay, MaxSplashDelay);

                    var x = Random.Range(0, Screen.width);
                    var y = Random.Range(0, Screen.height);
                    splashPool.AcquireObject<AnimatorPoolObject>(splashCamera.ScreenToWorldPoint(new Vector3(x, y)),
                        Quaternion.identity);

                }
            }
            
            _flashDelay -= Time.deltaTime;

            if (_flashDelay <= 0)
            {
                _flashDelay = Random.Range(MinFlashDelay, MaxFlashDelay);

                var seq = DOTween.Sequence();
                seq.Append(flash.DOFade(1, 0.08f));
                seq.Append(flash.DOFade(0, 0.16f));
            }
        }
    }
}