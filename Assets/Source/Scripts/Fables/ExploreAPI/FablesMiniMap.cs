using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Grid;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using DG.Tweening;
using MEC;
using TakoBoyStudios;
using TakoBoyStudios.Events;
using UnityEngine;
using UnityEngine.UI;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Fables.ExploreAPI
{
    public class FablesMiniMap : MonoBehaviour
    {
        public static Vector2 MiniMapRoomSize = new Vector2(18f, 18f);
        public static Vector2 CenterPosition = new Vector2(-0.5f, 0.5f);

        public RectTransform expandRect;
        public CanvasGroup canvas;
        public RectTransform roomLayout;
        public RectTransform mask;
        public RectTransform mapRect;
        public FablesMiniMapRoom roomPrefab;
        public Button expandAndMinimizeButton;
        public SpriteAnimation expandAndMinimizeAnimator;
        public float miniMapMoveSpeed = 200;

        private Vector2 mapCenter;

        private List<FablesMiniMapRoom> _rooms;
        private bool _expanded;
        private bool _movingMap;
        private Vector2 _startMapPosition;
        private Vector2 _targetMapPosition;
        private float _moveEaseValue;
        private Vector2 _expandSize;
        private FableRoomData m_currentRoom;

        private void Awake()
        {
            expandAndMinimizeButton.onClick.RemoveAllListeners();
            expandAndMinimizeButton.onClick.AddListener(OnExpandOrMinimize);
            Events.Subscribe(EventType.OnEnteredFableMap, BuildMap);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnEnteredFableMap, BuildMap);
        }

        private void OnExpandOrMinimize()
        {
            expandRect.DOKill();

            AudioController.PlaySound("UI-Selection");

            if (_expanded)
            {
                _expanded = false;
                expandRect.DOSizeDelta(new Vector2(62,62), 0.5f).SetEase(Ease.OutBack);
            }
            else
            {
                _expanded = true;
                expandRect.DOSizeDelta(_expandSize, 0.5f).SetEase(Ease.OutBack);
            }
        }

        private void Update()
        {
            if (_expanded && expandAndMinimizeAnimator.CurrentAnimationName != "expand")
            {
                expandAndMinimizeAnimator.Play("expand");
            }
            else if (!_expanded && expandAndMinimizeAnimator.CurrentAnimationName != "minimize")
            {
                expandAndMinimizeAnimator.Play("minimize");
            }

            if (!_movingMap)
            {
                _targetMapPosition = mapCenter;

                if (_expanded)
                {
                    _targetMapPosition = mapCenter;
                }
                else
                {
                    if (m_currentRoom != null && _rooms != null)
                    {
                        var roomNode = _rooms.Find(room => room.RoomUid == m_currentRoom.Uid);

                        if (roomNode)
                        {
                            _targetMapPosition = -roomNode.RectTransform().anchoredPosition;
                        }
                    }
                }

                if (_targetMapPosition != mapRect.anchoredPosition)
                {
                    _startMapPosition = mapRect.anchoredPosition;
                    _movingMap = true;
                    _moveEaseValue = 0;
                }
            }
            else
            {
                _moveEaseValue += miniMapMoveSpeed * Time.deltaTime;

                var current = Vector2.zero;

                current.x = EasingFunction.EaseOutBack(_startMapPosition.x, _targetMapPosition.x, _moveEaseValue);
                current.y = EasingFunction.EaseOutBack(_startMapPosition.y, _targetMapPosition.y, _moveEaseValue);

                mapRect.anchoredPosition = current;

                if (_moveEaseValue >= 1)
                {
                    _movingMap = false;
                    _moveEaseValue = 0;
                    mapRect.anchoredPosition = _targetMapPosition;
                }
            }
        }

        public void DisplayOn()
        {
            canvas.DOFade(1, 0.25f);
        }

        public void DisplayOff()
        {
            canvas.DOFade(0, 0.25f);
        }

        private void BuildMap(IMessage message)
        {
            if (message.Data is OnEnteredFableMapEventArgs args)
            {
                RebuildMap(args.mapData, App.FableData.GetCurrentChapter());
            }
        }

        public void RebuildMap(FableMapData mapData, ClientChapterData clientData)
        {
            Timing.RunCoroutine( BuildMap(mapData,clientData) );
        }

        private IEnumerator<float> BuildMap(FableMapData mapData, ClientChapterData clientData)
        {
            while (FablesMapController.Instance.GetCurrentRoom() == null) yield return Timing.WaitForOneFrame;

            roomLayout.DestroyChildren();
            _rooms = new List<FablesMiniMapRoom>();

            for (var i = 0; i < mapData.Rooms.Count; i++)
            {
                var roomData = mapData.Rooms[i];
                var roomNode = Instantiate(roomPrefab, roomLayout, false);
                roomNode.RectTransform().anchoredPosition = CenterPosition + (MiniMapRoomSize * roomData.Position);
                roomNode.SetRoom(roomData, clientData, mask);
                _rooms.Add(roomNode);
            }

            var minNodeX = mapData.Rooms.Min(room => room.Position.x);
            var maxNodeX = mapData.Rooms.Max(room => room.Position.x);
            var minNodeY = mapData.Rooms.Min(room => room.Position.y);
            var maxNodeY = mapData.Rooms.Max(room => room.Position.y);

            var distanceX = Mathf.Abs(minNodeX) + Mathf.Abs(maxNodeX) + 1;
            var distanceY = Mathf.Abs(minNodeY) + Mathf.Abs(maxNodeY) + 1;

            var distanceXUnits = distanceX * MiniMapRoomSize.x;
            var distanceYUnits = distanceY * MiniMapRoomSize.y;

            var size =new Vector2(distanceXUnits ,distanceYUnits);
            var outsideNodesX = Mathf.Abs(minNodeX) + 0.5f;
            var outsideNodesY = Mathf.Abs(maxNodeY) + 0.5f;
            var nodeOffsetDelta = new Vector2(
                outsideNodesX * MiniMapRoomSize.x,
                outsideNodesY * MiniMapRoomSize.y);

            mapCenter = new Vector2((-size.x * 0.5f) + nodeOffsetDelta.x, (size.y * 0.5f) - nodeOffsetDelta.y);
            mapRect.sizeDelta = Vector2.zero;

            var biggestSide = Mathf.Max(size.x+20, size.y+22);
            var expandSize = Mathf.Max(biggestSide, 122f);
            _expandSize = new Vector2(expandSize, expandSize);

            m_currentRoom = FablesMapController.Instance.GetCurrentRoom();
        }
    }
}