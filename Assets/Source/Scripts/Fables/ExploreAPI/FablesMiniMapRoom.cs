using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Grid;
using CrossBlitz.PlayFab.Data;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace CrossBlitz.Fables.ExploreAPI
{
    public class FablesMiniMapRoom : MonoBehaviour
    {
        public SpriteAnimation room;
        public SpriteAnimation node;
        public SpriteAnimation rightPath;
        public SpriteAnimation topPath;
        public ClipRectToTarget rightPathClipRect;
        public ClipRectToTarget topPathClipRect;

        private List<FableTileData> tileList;
        private List<String> animList = new List<String>();

        private bool _blink;
        private bool _cycle;
        private int _cycle_index = 0;

        private const float BlinkDelayOff = 0.125f;
        private const float BlinkDelayOn = 1f;

        private float _delayTime;
        private string _roomUid;
        public string RoomUid => _roomUid;

        public void SetRoom(FableRoomData roomData, ClientChapterData chapterData, RectTransform mask)
        {
            _roomUid = roomData.Uid;
            _blink = false;
            _cycle = false;

            rightPathClipRect.target = mask;
            topPathClipRect.target = mask;

            if (chapterData != null)
            {
                //var dbRoomData = Db.MapDatabase.GetRoom(roomData.Uid);
                var debugDisplay = roomData.DisplayName == "B1_C1_6";
                var accoladesFinishedInRoom = chapterData.FinishedAllAccoladesInRoom(roomData.Uid,debugDisplay);
                var clientRoomData = chapterData.GetRoomData(roomData.Uid);

                if (!clientRoomData.Explored)
                {
                    room.Play("unexplored");
                }
                else if (accoladesFinishedInRoom)
                {
                    room.Play("all-accolades");
                }
                else
                {
                    room.Play("normal");
                }
            }
            else
            {
                room.Play("normal");
            }

            node.SetActive(true);
            _blink = false;
            _cycle = false;

            var currentRoom = FablesMapController.Instance.GetCurrentRoom();
            var has_right_exit = roomData.HasTileOfType(NodeType.ExitRight);
            var has_up_exit = roomData.HasTileOfType(NodeType.ExitUp);

            if ((currentRoom != null && currentRoom.Uid == roomData.Uid) || (chapterData != null && chapterData.CurrentRoomUid == roomData.Uid))
            {
                node.Play("hero");
                _blink = true;
            }
            else
            {
                if (roomData.HasTileOfType(NodeType.Battle, fableTileData =>
                {
                    var battleData = Db.BattleDatabase.GetBattleData(fableTileData.battleUid);
                    return battleData != null && battleData.BattleType == BattleType.Boss;
                }))
                {
                    node.Play("boss");
                    animList.Add("boss");
                    _cycle = true;
                }

                if (roomData.HasTileOfType(NodeType.Shop))
                {
                    node.Play("shop");
                    //animList.Add("shop");
                    _cycle = true;
                    var tileList = new List<FableTileData>();
                    tileList = roomData.GetTilesOfNodeType(NodeType.Shop);
                    for (int i = 0; i < tileList.Count; i++)
                    {
                        if (tileList[i].ShopType == ShopType.Card)
                        {
                            node.Play("cardShop");
                            animList.Add("cardShop");
                            _cycle = true;
                        }
                        else
                        {
                            node.Play("relicShop");
                            animList.Add("relicShop");
                            _cycle = true;
                        }
                    }
                }

                if (roomData.HasTileOfType(NodeType.ManaMeld))
                {
                    node.Play("manaMeld");
                    animList.Add("manaMeld");
                    _cycle = true;
                }

                if (!_cycle)
                {
                    node.SetActive(false);
                }

            }


            var rightRoom = roomData.GetRoomInDirection(FablesMapController.Instance.MapData, FableRoomData.RoomDirection.Right);
            if (has_right_exit && rightRoom != null)
            {
                if (chapterData != null)
                {
                    var rightRoomData = chapterData.GetRoomData(rightRoom.Uid);
                    rightPath.SetActive(true);
                    rightPath.Play(rightRoomData != null && rightRoomData.Explored ? "normal" : "unexplored");
                }
                else
                {
                    rightPath.SetActive(true);
                    rightPath.Play("normal" );
                }
            }
            else
            {
                rightPath.SetActive(false);
            }

            var topRoom = roomData.GetRoomInDirection(FablesMapController.Instance.MapData, FableRoomData.RoomDirection.Top);
            if (has_up_exit && topRoom != null)
            {
                if (chapterData != null)
                {
                    var topRoomData = chapterData.GetRoomData(topRoom.Uid);
                    topPath.SetActive(true);
                    topPath.Play(topRoomData != null && topRoomData.Explored ? "normal" : "unexplored");
                }
                else
                {
                    topPath.SetActive(true);
                    topPath.Play("normal");
                }
            }
            else
            {
                topPath.SetActive(false);
            }
        }

        private void Update()
        {
            if (_blink)
            {

                var delayMax = node.gameObject.activeSelf ? BlinkDelayOn : BlinkDelayOff;

                _delayTime += Time.deltaTime;

                if (_delayTime >= delayMax)
                {
                    _delayTime = 0;
                    node.SetActive(!node.gameObject.activeSelf);
                }
            }
            if(_cycle && animList.Count > 1)
            {
                var delayMax = node.gameObject.activeSelf ? BlinkDelayOn : BlinkDelayOff;

                _delayTime += Time.deltaTime;

                if (_delayTime >= delayMax)
                {
                    _delayTime = 0;
                    if (node.IsActive())
                    {
                        node.Play(animList[_cycle_index]);
                        _cycle_index += 1;
                        if (_cycle_index > animList.Count - 1) _cycle_index = 0;
                    }

                    node.SetActive(!node.gameObject.activeSelf);
                }
            }
        }
    }
}