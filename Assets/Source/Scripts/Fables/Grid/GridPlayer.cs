using System;
using System.Collections.Generic;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Dialogue;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.PathfindingAPI;
using CrossBlitz.Hero;
using CrossBlitz.InputAPI;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.ServerAPI;
using DG.Tweening;
using MEC;
using TakoBoyStudios.Events;
using UnityEngine;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Fables.Grid
{
    public class GridPlayer : MonoBehaviour
    {
        public static GridPlayer Instance;
        public Transform movementContainer;
        public SpriteAnimation heroAnimator;
        public SpriteAnimation redwingAnimator;
        public SpriteAnimation minecartAnimator;
        public SpriteAnimation jumpFx;
        public SpriteAnimation landFx;
        public SpriteAnimation shadow;

        public HexTile2D CurrentTile { get; set; }

        private HeroData _heroData;
        private string _heroIdLower;

        private float walkSpeedCurr = 3f;
        private const float walkSpeedMin = 3f;
        private const float walkSpeedMax = 6f;
        private const float walkSpeedDecel = 4f;
        private float walkSpeedAccel = 8f;
        private bool walkSpeedDecelerate = false;
        private const float MinJumpHeight = 2;
        private const float MaxJumpHeight = 4;
        private const float JumpTimePerMeter = 0.1f;
        private const float WalkDuration = 0.5f;
        public const float LandingPower = -0.2f;

        private Vector2 movementContainerOriginalPosition;

        // Jump test
        private float jumpHeight = MinJumpHeight;
        private float timeToJumpApex = MinJumpHeight * JumpTimePerMeter;

        private Vector2 velocity;
        private float gravity;
        private float jumpVelocity;
        private bool updateJump;
        private bool isGrounded;
        private HexTile2D jumpTarget;
        private Vector3 jumpFxTargetPosition;
        private bool cancelJump;
        private bool swappingAnimators;
        private string m_jumpingCharacterId;
        private SpriteAnimation m_jumpingCharacterAnimator;
        private SpriteAnimation m_currentAnimator;
        private Vector2 m_lastWalkDirection;
        private bool m_isLeavingMap;

        private bool _updateWalk;
        private bool _toggleWalk;
        private int _inputWalk;
        private bool _playStepSFX;
        private bool _tookStep;
        private float _stepPitch;

        private List<HexTile2D> _walkPath;

        private void Awake()
        {
            Instance = this;
            movementContainerOriginalPosition = movementContainer.localPosition;
        }

        public void  SuperAwake()
        {
            Instance = this;
            movementContainerOriginalPosition = movementContainer.localPosition;
        }

        public string GetHeroId(FableTileData tile)
        {
            var heroId = _heroIdLower;

            if (tile != null)
            {
                var tileTypeData = Db.FablesDatabase.GetTileTypeData(tile.tileType);

                if (!string.IsNullOrEmpty(tileTypeData.HeroSpriteOverride))
                {
                    heroId = tileTypeData.HeroSpriteOverride;
                }
            }

            return heroId;
        }

        public bool IsLeavingMap()
        {
            return m_isLeavingMap;
        }


        private void Start()
        {
            isGrounded = true;
            walkSpeedDecelerate = false;
            _toggleWalk = false;
            _inputWalk = 0;

            var chapterHero = FablesMapController.Instance.ChapterData.ChapterHero;
            var heroData = Db.HeroDatabase.GetHero(chapterHero);
            SetHero(heroData);

            PlayIdleAnimation(Vector2.zero);

            shadow.Play("idle");

            Events.Subscribe(EventType.OnHeroChanged, OnHeroChanged);
            Events.Subscribe(EventType.OnNodeSelected, OnNodeSelected);

            gravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
            jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnHeroChanged, OnHeroChanged);
            Events.Unsubscribe(EventType.OnNodeSelected, OnNodeSelected);

            Instance = null;
        }

        private void SetHero(HeroData heroData)
        {
            _heroData = heroData;
            _heroIdLower = _heroData.id.ToLower();
        }

        private void OnHeroChanged(IMessage message)
        {
            if (!(message.Data is HeroData heroData))
            {
                Debug.LogError("Hero wasn't changed properly!");
                return;
            }

            SetHero(heroData);
            PlayIdleAnimation(Vector2.zero);

            //Debug.Log("On Hero Changed!!");

            //heroAnimator.Play($"{_heroIdLower}-march");
            shadow.Play("idle");
        }

        private void FixedUpdate()
        {
            if (updateJump)
            {
                velocity.y += gravity * Time.deltaTime;
                movementContainer.Translate(velocity * Time.deltaTime, Space.Self);

                if (m_jumpingCharacterId != "quill")
                {
                    if (velocity.y < 0)
                    {
                        if (!m_jumpingCharacterAnimator.CurrentAnimationName.Equals(
                                $"{m_jumpingCharacterId}-jump-fall-loop"))
                        {
                            m_jumpingCharacterAnimator.Play($"{m_jumpingCharacterId}-jump-fall-loop");
                        }
                    }
                    else if (Math.Abs(velocity.y) < float.Epsilon)
                    {
                        m_jumpingCharacterAnimator.Play($"{m_jumpingCharacterId}-jump-peak");
                    }
                }
                jumpFx.transform.position = jumpFxTargetPosition;
            }
        }

        private void Update()
        {
            if(InputWalk())
            {
                IncreaseWalkSpeed();
            }
            else
            {
                DecreaseWalkSpeed();
            }

            if (_playStepSFX)
            {
                if (m_currentAnimator.CurrentFrame == 4 || m_currentAnimator.CurrentFrame == 9)
                {
                    if (!_tookStep)
                    {
                        AudioController.PlaySound("map_footstep", "BARDSFX", true, gameObject).setPitch(_stepPitch);
                        _tookStep = true;
                    }
                }
                else
                {
                    _tookStep = false;
                }
            }
        }

        public bool InputWalk()
        {
            // Pause Walk Speed
            if (walkSpeedDecelerate || updateJump || m_isLeavingMap)
            {
                return false;
            }
            // Hold
            else if (App.Settings.GetFablesSpeedUpCharacter() == 0)
            {
                return Input.GetMouseButton(0);
            }
            //Always On
            else if (App.Settings.GetFablesSpeedUpCharacter() == 1)
            {
                return true;
            }
            //Click Once
            else if (App.Settings.GetFablesSpeedUpCharacter() == 2)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    _toggleWalk = true;
                }
                return _toggleWalk;
            }
            else
            {
                return false;
            }
        }

        public void IncreaseWalkSpeed()
        {
            if (walkSpeedCurr > walkSpeedMax)
            {
                walkSpeedCurr = walkSpeedMax;
            }
            else if (walkSpeedCurr < walkSpeedMax)
            {
                walkSpeedCurr += walkSpeedAccel * Time.deltaTime;
            }
        }

        public void DecreaseWalkSpeed()
        {
            if (walkSpeedCurr > walkSpeedMin)
            {
                walkSpeedCurr -= walkSpeedDecel * Time.deltaTime;
            }
            else if (walkSpeedCurr < walkSpeedMin)
            {
                walkSpeedCurr = walkSpeedMin;
            }
        }

        public void EnterMap(Vector2 direction, HexTile2D target)
        {
            CurrentTile = target;

            if (CurrentTile) CurrentTile.OnHeroStartWalkingTowards();

            var heroId = GetHeroId(CurrentTile != null ? CurrentTile.TileData : null);
            GetAnimator(heroId, false).renderer.color = new Color(1, 1, 1, 0);
            shadow.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);

           // Debug.Log($"On Player Entered Map.");

            Timing.RunCoroutine(EnterMapWait(target, direction).CancelWith(gameObject));
        }

        public void EnterMapWonBattle(Vector2 direction, HexTile2D target)
        {
            CurrentTile = target;

            if (CurrentTile) CurrentTile.OnHeroStartWalkingTowards();

            var heroId = GetHeroId(CurrentTile != null ? CurrentTile.TileData : null);
            GetAnimator(heroId, false).renderer.color = new Color(1, 1, 1, 0);
            shadow.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);

            Timing.RunCoroutine(EnterMapWonBattleWait(target, direction).CancelWith(gameObject));
        }

        private IEnumerator<float> EnterMapWait(HexTile2D tile, Vector2 direction)
        {
            if (!AudioController.IsSoundPlaying("TMP-Footstep"))
            {
                AudioController.PlaySound("TMP-Footstep");
            }

            yield return Timing.WaitForSeconds(.4f);

            PlayWalkAnimation(tile, direction);

            var heroId = GetHeroId(CurrentTile != null ? CurrentTile.TileData : null);
            GetAnimator(heroId, false).renderer.DOFade(1, 0.24f);

            shadow.SetActive(true);
            shadow.GetComponent<SpriteRenderer>().DOFade(0.5f, 0.24f);

            movementContainer.transform.localPosition = movementContainerOriginalPosition - direction;

            yield return Timing.WaitUntilDone(movementContainer.DOLocalMove(movementContainerOriginalPosition, .5f)
                .WaitForCompletion(true));

            m_isLeavingMap = false;

            CurrentTile.PlayerEnterMap(this);

            PlayIdleAnimation(Vector2.zero);

            AudioController.StopSound("TMP-Footstep");
        }


        private IEnumerator<float> EnterMapWonBattleWait(HexTile2D tile, Vector2 direction)
        {
            yield return Timing.WaitForSeconds(.4f);

            PlayWalkAnimation(tile, direction);

            var heroId = GetHeroId(CurrentTile != null ? CurrentTile.TileData : null);
            GetAnimator(heroId, false).renderer.DOFade(1, 0.24f);

            shadow.SetActive(true);
            shadow.GetComponent<SpriteRenderer>().DOFade(0.5f, 0.24f);

            movementContainer.transform.localPosition = movementContainerOriginalPosition - direction;

            yield return Timing.WaitUntilDone(movementContainer.DOLocalMove(movementContainerOriginalPosition, .5f)
                .WaitForCompletion(true));

            _playStepSFX = false;

            GetAnimator(heroId).Play($"{heroId}-victory");

            yield return Timing.WaitForSeconds(0.24f);

            CurrentTile.PlayerEnterMap(this);

            while (!GetAnimator(heroId).IsDone) yield return Timing.WaitForOneFrame;

            m_isLeavingMap = false;

            PlayIdleAnimation(Vector2.zero);
        }

        public IEnumerator<float> ExitMap()
        {
            m_isLeavingMap = true;

            if (!AudioController.IsSoundPlaying("TMP-Footstep"))
            {
                AudioController.PlaySound("TMP-Footstep");
            }

            var direction = -CurrentTile.TileData.GetEnterDirectionAsVector();
            var targetPosition = (Vector2)movementContainer.localPosition + direction;
            PlayWalkAnimation(CurrentTile, direction);

            var heroId = GetHeroId(CurrentTile != null ? CurrentTile.TileData : null);
            GetAnimator(heroId, false).renderer.DOFade(0, 0.5f);

            shadow.SetActive(true);
            shadow.GetComponent<SpriteRenderer>().DOFade(0, 0.5f);

            yield return Timing.WaitUntilDone(movementContainer.DOLocalMove(targetPosition, .5f)
                .WaitForCompletion(true));

            PlayIdleAnimation(Vector2.zero);
        }

        public void PlayAnimation_OnlyUseForEditor(string prefix, bool backToIdle)
        {
            if (backToIdle)
            {
                var heroId = GetHeroId(CurrentTile != null ? CurrentTile.TileData : null);
                GetAnimator(heroId).Play($"{heroId}-{prefix}", () => { GetAnimator(heroId).Play($"{heroId}-march"); });
            }
            else
            {
                var heroId = GetHeroId(CurrentTile != null ? CurrentTile.TileData : null);
                GetAnimator(heroId).Play($"{heroId}-{prefix}");
            }
        }

        public IEnumerator<float> PlayDialogue(List<CutsceneDialogueInfo> dialogue)
        {
            FablesInput.Instance.mapDialogueInput.OpenMapDialogue(new MapDialogueCustomProperties
            {
                DialogueTile = CurrentTile
            });

            var dialogueBox = FablesInput.Instance.mapDialogueInput.dialogueBox;

            for (var i = 0; i < dialogue.Count; i++)
            {
                yield return Timing.WaitUntilDone(dialogueBox.ShowText(dialogue[i]));
            }

            yield return Timing.WaitUntilDone(dialogueBox.AnimateOut());

            FablesInput.Instance.GoToMode(FablesInput.Mode.Explore);
        }

        private void OnNodeSelected(IMessage message)
        {
            walkSpeedCurr = walkSpeedMin;

            if (message.Data is OnNodeSelectedEventArgs args)
            {
                if (args.tileUid != 0)
                {
                    GetReadyToJump();
                }
                else
                {
                    CancelJump();
                }
            }
        }

        public void GetReadyToJump()
        {
            // heroAnimator.Play($"{_heroIdLower}-jump-ready");
            // shadow.Play("jump-ready");
        }

        public void CancelJump()
        {
            // heroAnimator.Play($"{_heroIdLower}-cancel-jump");
            // shadow.Play("cancel-jump");
            // cancelJump = true;
        }

        public SpriteAnimation GetAnimator(string heroId, bool forceOn = true)
        {
            if (!string.IsNullOrEmpty(heroId) && heroId.Contains("redwing"))
            {
                if (forceOn)
                {
                    redwingAnimator.renderer.enabled = true;
                    minecartAnimator.renderer.enabled = false;
                    heroAnimator.renderer.enabled = false;
                }

                m_currentAnimator = redwingAnimator;
                return redwingAnimator;
            }
            else if (!string.IsNullOrEmpty(heroId) && (heroId.Contains("violetcart") || heroId.Contains("setocart")))
            {
                if (forceOn)
                {
                    redwingAnimator.renderer.enabled = false;
                    minecartAnimator.renderer.enabled = true;
                    heroAnimator.renderer.enabled = false;
                }

                m_currentAnimator = minecartAnimator;
                return minecartAnimator;
            }

            if (forceOn)
            {
                redwingAnimator.renderer.enabled = false;
                minecartAnimator.renderer.enabled = false;
                heroAnimator.renderer.enabled = true;
            }

            SetStepPitch(heroId);

            m_currentAnimator = heroAnimator;
            return heroAnimator;
        }

        public void PlayWalkAnimation(HexTile2D tile, Vector2 direction, SpriteAnimation overrideAnimator = default, bool saveWalkFrame = false)
        {

            var heroId = GetHeroId(tile.TileData);
            var animator = overrideAnimator ? overrideAnimator : GetAnimator(heroId, false);
            var currentAnimFrame = animator.CurrentFrame;
            var isVehicle = false;

            animator.speedRatio = Math.Max((walkSpeedCurr*.4f) / walkSpeedMin, 1);

            if (heroId.Contains("redwing") || heroId.Contains("violetcart") || heroId.Contains("setocart")) {
                _playStepSFX = false;
                isVehicle = true;
            }
            else
            {
                _playStepSFX = true;
            }

            if (isVehicle && Mathf.Abs(direction.x) > 0.1f && Mathf.Abs(direction.y) > 0)
            {
                // back right
                if (direction.x > 0 && direction.y > 0)
                {
                    if(animator.CurrentAnimationName!= $"{heroId}-walk-back-diagonal") animator.Play($"{heroId}-walk-back-diagonal");
                    animator.transform.localScale = new Vector3(-1, 1, 1);
                }
                // front right
                else if (direction.x > 0 && direction.y < 0)
                {
                    if (animator.CurrentAnimationName != $"{heroId}-walk-front-diagonal") animator.Play($"{heroId}-walk-front-diagonal");
                    animator.transform.localScale = new Vector3(-1, 1, 1);
                }

                // back left
                else if (direction.x < 0 && direction.y > 0)
                {
                    if (animator.CurrentAnimationName != $"{heroId}-walk-back-diagonal") animator.Play($"{heroId}-walk-back-diagonal");
                    animator.transform.localScale = new Vector3(1, 1, 1);
                }
                // front left
                else if (direction.x < 0 && direction.y < 0)
                {
                    if (animator.CurrentAnimationName != $"{heroId}-walk-front-diagonal") animator.Play($"{heroId}-walk-front-diagonal");
                    animator.transform.localScale = new Vector3(1, 1, 1);
                }
            }
            else
            {
                if (direction.x > 0.1f)
                {
                    if (animator.CurrentAnimationName != $"{heroId}-walk-side") animator.Play($"{heroId}-walk-side");
                    animator.transform.localScale = new Vector3(-1, 1, 1);
                }
                else if (direction.x < -0.1f)
                {
                    if (animator.CurrentAnimationName != $"{heroId}-walk-side") animator.Play($"{heroId}-walk-side");
                    animator.transform.localScale = new Vector3(1, 1, 1);
                }
                else if (direction.y > 0)
                {
                    if (animator.CurrentAnimationName != $"{heroId}-walk-back") animator.Play($"{heroId}-walk-back");
                    animator.transform.localScale = new Vector3(1, 1, 1);
                }
                else if (direction.y < 0)
                {
                    if (animator.CurrentAnimationName != $"{heroId}-walk-front") animator.Play($"{heroId}-walk-front");
                    animator.transform.localScale = new Vector3(1, 1, 1);
                }
            }

            if (saveWalkFrame && _playStepSFX && !overrideAnimator)
            {
                if (animator.GetFrameCount() > currentAnimFrame) animator.CurrentFrame = currentAnimFrame;
            }

        }

        public void PlayIdleAnimation(Vector2 direction)
        {
            var heroId = GetHeroId(CurrentTile != null ? CurrentTile.TileData : null);
            var animator = GetAnimator(heroId);

            animator.speedRatio = 1;

            if ((heroId.Contains("redwing") || heroId.Contains("violetcart") || heroId.Contains("setocart")) && Mathf.Abs(direction.x) > 0.1f && Mathf.Abs(direction.y) > 0)
            {
                // back right
                if (direction.x > 0 && direction.y > 0)
                {
                    animator.Play($"{heroId}-idle-back-diagonal");
                    animator.transform.localScale = new Vector3(-1, 1, 1);
                }
                // front right
                else if (direction.x > 0 && direction.y < 0)
                {
                    animator.Play($"{heroId}-idle-front-diagonal");
                    animator.transform.localScale = new Vector3(-1, 1, 1);
                }

                // back left
                else if (direction.x < 0 && direction.y > 0)
                {
                    animator.Play($"{heroId}-idle-back-diagonal");
                    animator.transform.localScale = new Vector3(1, 1, 1);
                }
                // front left
                else if (direction.x < 0 && direction.y < 0)
                {
                    animator.Play($"{heroId}-idle-front-diagonal");
                    animator.transform.localScale = new Vector3(1, 1, 1);
                }
            }
            else
            {
                animator.Play($"{heroId}-march");
            }

            _playStepSFX = false;
        }

        private enum WalkDirection
        {
            Up=0,
            UpRight=1,
            Right=2,
            DownRight=3,
            Down=4,
            DownLeft=5,
            Left=6,
            UpLeft=7,
        }

        private WalkDirection Vector2ToWalkingDirection(Vector2 direction)
        {
            if (Mathf.Abs(direction.x) > 0.1f && Mathf.Abs(direction.y) > 0)
            {
                // back right
                if (direction.x > 0 && direction.y > 0)
                {
                    return WalkDirection.UpRight;
                }
                // front right
                if (direction.x > 0 && direction.y < 0)
                {
                    return WalkDirection.DownRight;
                }
                // back left
                 if (direction.x < 0 && direction.y > 0)
                 {
                     return WalkDirection.UpLeft;
                 }
                // front left
                if (direction.x < 0 && direction.y < 0)
                {
                    return WalkDirection.DownLeft;
                }
            }
            else
            {
                if (direction.x > 0.1f)
                {
                    return WalkDirection.Right;
                }

                if (direction.x < -0.1f)
                {
                    return WalkDirection.Left;
                }

                if (direction.y > 0)
                {
                    return WalkDirection.Up;
                }

                if (direction.y < 0)
                {
                    return WalkDirection.Down;
                }
            }

            return WalkDirection.Down;
        }

        private Vector2 WalkingDirectionToVector2(WalkDirection walkDirection)
        {
            switch (walkDirection)
            {
                case WalkDirection.Up: return new Vector2(0, 1);
                case WalkDirection.UpRight:return new Vector2(1, 1);
                case WalkDirection.Right:return new Vector2(1, 0);
                case WalkDirection.DownRight:return new Vector2(1, -1);
                case WalkDirection.Down:return new Vector2(0, -1);
                case WalkDirection.DownLeft:return new Vector2(-1, -1);
                case WalkDirection.Left:return new Vector2(-1, 0);
                case WalkDirection.UpLeft:return new Vector2(-1, 1);
            }

            return Vector2.zero;
        }

        public IEnumerator<float> TurnShip(HexTile2D tile, Vector2 direction, Vector2 lastDirection)
        {
            var walkDirection = Vector2ToWalkingDirection(direction);
            var lastWalkDirection = Vector2ToWalkingDirection(lastDirection);
            var realDistance = (int)lastWalkDirection > (int)walkDirection ?
                (int)lastWalkDirection - (int)walkDirection:
                (int)walkDirection - (int)lastWalkDirection;
            var loopedDistance = (int)lastWalkDirection > (int)walkDirection ?
                7 - ((int)lastWalkDirection - (int)walkDirection - 1):
                7 - ((int)walkDirection - (int)lastWalkDirection - 1);
            var distanceToDirection = Mathf.Min(realDistance,loopedDistance);

            //Debug.Log($"Walk Dir: {walkDirection} Last Dir: {lastWalkDirection} Real Dist {realDistance} Looped {loopedDistance}");

            if (distanceToDirection < 2)
            {
                // the ship only needs to turn to the next direction or not at all.
                yield break;
            }

            var nextDirection = lastWalkDirection;
            int stepDirection;

            if (realDistance < loopedDistance)
            {
                if ((int)lastWalkDirection > (int)walkDirection)
                {
                    stepDirection = -1;
                }
                else
                {
                    stepDirection = 1;
                }
            }
            else
            {
                if ((int)lastWalkDirection > (int)walkDirection)
                {
                    stepDirection = 1;
                }
                else
                {
                    stepDirection = -1;
                }
            }

            var targetDirection = (WalkDirection)(int)walkDirection - stepDirection;

            while (nextDirection != targetDirection)
            {
                nextDirection = (WalkDirection) (int)nextDirection + stepDirection;
                PlayWalkAnimation(tile, WalkingDirectionToVector2(nextDirection));

                yield return Timing.WaitForSeconds(0.04f);
            }
        }

        public IEnumerator<float> WalkToTile(HexTile2D targetTile)
        {
            if (targetTile == CurrentTile || !IsGrounded())
            {
                yield break;
            }

            for (var i = 0; i < HexGrid2D.Map.Count; i++)
            {
                HexGrid2D.Map[i].tileAnimator.renderer.color = Color.white;
            }

            var path = Pathfinding.FindPath(CurrentTile.TileData, targetTile.TileData, false, true);

            if (path == null)
            {
                Debug.LogError($"This tile cannot be walked to! {CurrentTile.TileData.NodeType} -> {targetTile.TileData.NodeType}");
                yield break;
            }

            //if (!AudioController.IsSoundPlaying("TMP-Footsteps2"))
            //{
            //    AudioController.PlaySound("TMP-Footsteps2");
            //}

            Events.Publish(this, EventType.OnPlayerStartedWalkingFromTile, null);

            _walkPath = new List<HexTile2D>();

            for (var i = 0; i < path.Count; i++)
            {
                var tile = HexGrid2D.GetTile(path[i].uid);
                if (tile == null)
                {
                    Debug.LogError($"Could not find tile {path[i].uid}");
                    continue;
                }

                _walkPath.Add(tile);
                //tile.tileAnimator.renderer.color = Color.green;
            }

            _toggleWalk = false;
            var justJumped = false;

            var lastTile = CurrentTile;
            var lastDirection = Vector2.zero;

            for (var i = 0; i < _walkPath.Count; i++)
            {
                var tile = _walkPath[i];
                var nextTile = i <=_walkPath.Count-2 ? _walkPath[i+1] : null;
                var targetPos = tile.markerAnimator.transform.position;
                var direction = (targetPos - transform.position).normalized;
                var punchTile = true;

                // Acceleration adjustments
                if (i >= _walkPath.Count - 1) { walkSpeedDecelerate = true; }
                if (nextTile != null && nextTile.TileData.Height != tile.TileData.Height) { walkSpeedDecelerate = true; }
                if (Math.Abs(direction.y) > 0.9  && walkSpeedDecelerate) walkSpeedAccel = 20f;
                else walkSpeedAccel = 10f;

                //Debug.Log("Math.Abs(direction.y): " + Math.Abs(direction.y));

                if (lastTile.TileData.Height != tile.TileData.Height)
                {
                    PlayWalkAnimation(lastTile, direction);

                    tile.OnHeroStartWalkingTowards();
                    yield return Timing.WaitUntilDone(JumpToTile(tile, lastTile));
                    lastTile.OnHeroStartWalkingAway();

                    walkSpeedDecelerate = false;
                    justJumped = true;
                }
                else
                {
                    var lastTileHeroId = GetHeroId(lastTile.TileData);
                    var heroId = GetHeroId(tile.TileData);

                    if (Server.Instance.customGameOptions.turnShip && ((heroId.Contains("redwing") && lastTileHeroId.Contains("redwing")) || (heroId.Contains("violetcart") && lastTileHeroId.Contains("violetcart")) || (heroId.Contains("setocart") && lastTileHeroId.Contains("setocart"))))
                    {
                        yield return Timing.WaitUntilDone(TurnShip(tile, direction, lastDirection));
                    }

                    PlayWalkAnimation(tile, direction, null, !justJumped);
                    justJumped = false;

                    lastTile.OnHeroStartWalkingAway();
                    tile.OnHeroStartWalkingTowards();

                    swappingAnimators = GetAnimator(lastTileHeroId, false) !=
                                        GetAnimator(heroId, false);

                    if (swappingAnimators)
                    {
                        PlayWalkAnimation(lastTile, direction);
                        TransitionAnimators(lastTile, tile);
                    }

                    if (lastTile.sortingGroup.sortingOrder < tile.sortingGroup.sortingOrder)
                    {
                        transform.SetParent(tile.playerContainer, true);
                    }

                    while (Vector3.Distance(targetPos, transform.position) > 0.01f)
                    {
                        targetPos = tile.markerAnimator.transform.position;
                        transform.position = Vector3.MoveTowards(transform.position, targetPos, walkSpeedCurr * Time.deltaTime);

                        if (punchTile)
                        {
                            var nextDistance = Vector3.Distance(targetPos, transform.position);

                            if (nextDistance <= 0.5f)
                            {
                                tile.Push(-.02f);
                                punchTile = false;
                            }
                        }

                        yield return Timing.WaitForOneFrame;
                    }
                }

                lastTile = tile;
                lastDirection = direction;
            }

            CurrentTile.PlayerExitTile();
            CurrentTile = targetTile;
            CurrentTile.OnPointerExit();
            CurrentTile.PlayerEnterTile(this, false);

            walkSpeedDecelerate = false;

            PlayIdleAnimation(lastDirection);
            //AudioController.StopSound("TMP-Footsteps2");
        }

        public void TransitionAnimators(HexTile2D currentTile, HexTile2D targetTile)
        {
            var lastAnimator = GetAnimator(GetHeroId(currentTile.TileData), false);
            var animator = GetAnimator(GetHeroId(targetTile.TileData), false);

            animator.speedRatio = 1;

            lastAnimator.renderer.enabled = true;
            animator.renderer.enabled = true;

            lastAnimator.renderer.color = Color.white;
            animator.renderer.color = new Color(1, 1, 1, 0);

            lastAnimator.renderer.DOFade(0, 0.5f);
            animator.renderer.DOFade(1, 0.5f);
        }

        public IEnumerator<float> JumpToTile(HexTile2D targetTile, HexTile2D currentTile)
        {
            if (targetTile == CurrentTile || !IsGrounded())
            {
                Debug.Log("BREAK JUMP TO TILE");
                yield break;
            }

            var targetPos = targetTile.markerAnimator.transform.position;
            var ease = Ease.Linear;
            var setChildBefore = targetTile.SortingOrder > currentTile.SortingOrder;

            if (setChildBefore)
            {
                transform.SetParent(targetTile.playerContainer, true);
            }

            var startPos = transform.position;
            var distance = Vector2.Distance(startPos, targetPos);
            var heightDifference = Mathf.Clamp(targetTile.TileData.Height - currentTile.TileData.Height, 0, 10);

            jumpTarget = targetTile;
            jumpHeight = Mathf.Clamp(distance + heightDifference, MinJumpHeight, MaxJumpHeight);
            timeToJumpApex = jumpHeight * JumpTimePerMeter;

            gravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
            jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;

            isGrounded = false;
            velocity.y = jumpVelocity;

            _playStepSFX = false;

            DOTween.Kill(transform);

            swappingAnimators = GetAnimator(GetHeroId(currentTile.TileData), false) !=
                                GetAnimator(GetHeroId(jumpTarget.TileData), false);

            var jumpingFromShip = false;

            if (swappingAnimators)
            {
                if (GetHeroId(currentTile.TileData).Contains("redwing") || GetHeroId(currentTile.TileData).Contains("violetcart") || GetHeroId(currentTile.TileData).Contains("setocart"))
                {
                    jumpingFromShip = true;
                    m_jumpingCharacterId = GetHeroId(jumpTarget.TileData);
                    m_jumpingCharacterAnimator = GetAnimator(m_jumpingCharacterId, false);
                    // GetAnimator(GetHeroId(currentTile.TileData), false)
                    //     .Play($"{GetHeroId(currentTile.TileData)}-march");
                }
                else
                {
                    m_jumpingCharacterId = GetHeroId(currentTile.TileData);
                    m_jumpingCharacterAnimator = GetAnimator(m_jumpingCharacterId, false);
                    GetAnimator(GetHeroId(targetTile.TileData), false)
                        .Play($"{GetHeroId(targetTile.TileData)}-march");
                }
            }
            else
            {
                m_jumpingCharacterId = GetHeroId(currentTile.TileData);
                m_jumpingCharacterAnimator = GetAnimator(m_jumpingCharacterId, false);
            }

            if (swappingAnimators && !jumpingFromShip)
            {
                m_jumpingCharacterAnimator.Play($"{m_jumpingCharacterId}-jump-launch");
                shadow.Play("jump-launch");

                jumpFxTargetPosition = m_jumpingCharacterAnimator.transform.position;

                while (!m_jumpingCharacterAnimator.IsDone) yield return Timing.WaitForOneFrame;

                jumpFx.SetActive(true);
                jumpFx.Play("launch");
                jumpFx.transform.position = jumpFxTargetPosition;
            }
            else if (!swappingAnimators)
            {
                m_jumpingCharacterAnimator.Play($"{m_jumpingCharacterId}-jump-launch");
                shadow.Play("jump-launch");

                jumpFxTargetPosition = m_jumpingCharacterAnimator.transform.position;

                while (!m_jumpingCharacterAnimator.IsDone) yield return Timing.WaitForOneFrame;

                jumpFx.SetActive(true);
                jumpFx.Play("launch");
                jumpFx.transform.position = jumpFxTargetPosition;
            }

            AudioController.PlaySound("map_jump", "BARDSFX", true, gameObject);
            //AudioController.PlaySound("TMP-Jump");

            updateJump = true;
            shadow.Play("in-air");
            m_jumpingCharacterAnimator.Play($"{m_jumpingCharacterId}-jump-rise-loop");

            if (swappingAnimators)
            {
                TransitionAnimators(currentTile, targetTile);
            }

            yield return Timing.WaitUntilDone(transform.DOMove(targetPos, timeToJumpApex * 2).SetEase(ease)
                .SetUpdate(UpdateType.Fixed).WaitForCompletion(true));

            velocity.y = 0;
            movementContainer.localPosition = movementContainerOriginalPosition;

            AudioController.PlaySound("map_land", "BARDSFX", true, gameObject);
            //AudioController.PlaySound("TMP-JumpLand2");

            targetTile.Push(LandingPower, true);
            AudioController.PlaySound("map_tile_wobble", "BARDSFX", true, gameObject);

            updateJump = false;

            jumpFxTargetPosition = targetPos;
            jumpFx.Play("land");
            jumpFx.transform.localPosition = Vector3.zero;
            shadow.Play("jump-land");
            m_jumpingCharacterAnimator.Play($"{m_jumpingCharacterId}-jump-land");

            if (!setChildBefore)
            {
                transform.SetParent(targetTile.playerContainer, true);
            }

            while (!m_jumpingCharacterAnimator.IsDone) yield return Timing.WaitForOneFrame;

            jumpFx.SetActive(false);
            // shadow.Play("idle");
            // PlayIdleAnimation();
            isGrounded = true;
            walkSpeedCurr = walkSpeedMin;
        }

        public bool IsGrounded()
        {
            return isGrounded;
        }

        public void SetStepPitch(string heroId)
        {
            switch (heroId)
            {
                case "redcroft":
                    _stepPitch = 0.5f;
                    break;
                case "violet":
                    _stepPitch = 0.75f;
                    break;
                case "quill":
                    _stepPitch = 1.0f;
                    break;
                case "seto":
                    _stepPitch = 0.75f;
                    break;
                case "mereena":
                    _stepPitch = 0.75f;
                    break;
            }
        }
    }
}