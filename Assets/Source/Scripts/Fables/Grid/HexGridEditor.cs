using CrossBlitz.AddressablesAPI;
using CrossBlitz.Databases;
using CrossBlitz.Developer.D90;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Tools;
using CrossBlitz.InputAPI;
using UnityEngine;

namespace CrossBlitz.Fables.Grid
{
    public enum GridEditorTool
    {
        TilePlacement, // Add/Remove tiles of any type
        TileHeightModifier, // Increase/Decrease height of a tile
        NodePlacement, // Add/Remove nodes of any type (double click to pull up the node settings)
        NodeEditor, // edit node metadata by clicking on a node.
        DecorationEditor, // edit decorations
        NpcEditor,
        PathEditor,
        Settings
    }

    public enum PathMode
    {
        Select,
        Add,
        Delete
    }

    public class GridEditorToolSettings
    {
        public GridEditorTool Tool;
        public string TileType;
        public NodeType NodeType;
        public string DecorationType;
        public string NpcType;
        public PathMode PathMode;
        public int CurrentPathTile;
    }
    public class HexGridEditor : InputMode
    {
        public GameObject tools;
        public HexGrid2D grid;
        public GridEditorToolbox toolbox;
        public CanvasGroup canvasGroup;
        public MapEditorSettings settings;
        public D90TileEditorWindow tileEditorWindow;


        private static GridEditorToolSettings _currentSettings;

        public static GridEditorToolSettings CurrentSettings
        {
            get
            {
                if (_currentSettings == null)
                {
                    _currentSettings= new GridEditorToolSettings
                    {
                        Tool = GridEditorTool.TilePlacement,
                        TileType = TileTypeData.Default,
                        NodeType = NodeType.Battle
                    };
                }

                return _currentSettings;
            }
        }
        public static HexTile2D CurrentTile;
        public static HexTile2D TileEditorTileView;

        private Camera _cam;

        protected void Start()
        {
            SceneController.OnSceneLoaded += OnSceneLoaded;

            //grid.GenerateMap(3);

            _currentSettings = new GridEditorToolSettings
            {
                Tool = GridEditorTool.TilePlacement,
                TileType = TileTypeData.Default,
                NodeType = NodeType.Battle
            };

            toolbox.OnToolChanged += OnToolChanged;
            toolbox.OnExploreMode += OnExploreMode;

            tileEditorWindow.OnTileChanged += OnTileDataChanged;

            //canvasGroup.blocksRaycasts = false;
            //canvasGroup.interactable = false;
        }

        private void OnTileDataChanged(int uid)
        {
            if (TileEditorTileView == null)
            {
                return;
            }

            if (TileEditorTileView.TileData == null || TileEditorTileView.TileData.uid == uid )
            {
                var tileData = Db.MapDatabase.GetTile(uid);
                TileEditorTileView.SetHexData(tileData, false);
            }
            else
            {
                Debug.LogError($"Current Tile is not being changed. {uid} != {TileEditorTileView.TileData.uid}");
            }
        }

        private void OnSceneLoaded(SceneModel model)
        {
            SceneController.OnSceneLoaded -= OnSceneLoaded;
            _cam = model.sceneCamera;
        }

        private void OnExploreMode()
        {
            InputProcessor.GoToMode(FablesInput.Mode.Explore);
        }

        private void OnToolChanged(GridEditorToolSettings settings)
        {
            _currentSettings = settings;
            UpdateRaycast(true);
        }

        public override void StartMode()
        {
            tools.SetActive(true);
            canvasGroup.blocksRaycasts = true;
            canvasGroup.interactable = true;

            toolbox.OpenMenu();
        }

        public override void UpdateMode()
        {


            if (settings.IsActive())
            {
                if (CurrentTile)
                {
                    CurrentTile.OnPointerExit();
                    CurrentTile = null;
                }

                return;
            }

            UpdateRaycast(false);
            UpdateToolActions();
        }

        public override void EndMode()
        {
            if (CurrentTile)
            {
                CurrentTile.OnPointerExit();
                CurrentTile = null;
            }

            toolbox.CloseMenu();
        }

        private void UpdateRaycast(bool forceUpdate)
        {
            if (_cam == null)
            {
                var sceneModel = SceneModel.GetCurrentSceneModel();

                if (sceneModel)
                {
                    _cam = sceneModel.GetCamera();
                }

                if (_cam == null)
                {
                    Debug.LogError("Fatal: cannot get camera!");
                    return;
                }
            }

            var hit = Physics2D.Raycast(_cam.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit && hit.transform)
            {
                var tile = hit.transform.GetComponent<HexTile2DReference>();
                if (!forceUpdate && tile.hexTile == CurrentTile) return;

                if (tile)
                {
                    if (CurrentTile && CurrentTile != tile.hexTile)
                    {
                        CurrentTile.OnPointerExit();
                    }

                    CurrentTile = tile.hexTile;

                    if (CurrentTile)
                    {
                        CurrentTile.OnPointerEnterEditorMode(CurrentSettings);
                    }
                }
                else if (CurrentTile)
                {
                    CurrentTile.OnPointerExit();
                    CurrentTile = null;
                }
            }
            else
            {
                if (CurrentTile)
                {
                    CurrentTile.OnPointerExit();
                }

                CurrentTile = null;
            }
        }

        private void UpdateToolActions()
        {
            if (CurrentTile == null)
            {
                return;
            }

            if (Input.GetMouseButtonDown(0))
            {
                CurrentTile.OnPointerClickEditorMode();



                // while holding the shift key
                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                {
                    switch (CurrentSettings.Tool)
                    {
                        case GridEditorTool.TilePlacement:
                            CurrentTile.TileData.Decoration = string.Empty;
                            CurrentTile.TileData.displayName = string.Empty;
                            CurrentTile.TileData.tileType = string.Empty;
                            CurrentTile.TileData.Npc = string.Empty;
                            CurrentTile.TileData.NodeType = NodeType.None;
                            CurrentTile.TileData.Height = 0;
                            CurrentTile.SetTileType(string.Empty);
                            break;
                        case GridEditorTool.TileHeightModifier:
                            CurrentTile.TileData.Height++;
                            CurrentTile.GenerateHeight();
                            break;
                        case GridEditorTool.NodePlacement:
                            CurrentTile.SetNodeType(NodeType.None);
                            break;
                        case GridEditorTool.DecorationEditor:
                            CurrentTile.SetDecorationType(string.Empty);
                            break;
                        case GridEditorTool.NpcEditor:
                            CurrentTile.SetNpcType(string.Empty);
                            break;
                    }
                }
                else
                {
                    switch (CurrentSettings.Tool)
                    {
                        case GridEditorTool.TilePlacement:
                            CurrentTile.SetTileType(CurrentSettings.TileType);
                            break;
                        case GridEditorTool.TileHeightModifier:
                            if (string.IsNullOrEmpty(CurrentTile.TileData.tileType))
                            {
                                break;
                            }
                            CurrentTile.TileData.Height++;
                            CurrentTile.GenerateHeight();
                            break;
                        case GridEditorTool.NodePlacement:
                            if (string.IsNullOrEmpty(CurrentTile.TileData.tileType))
                            {
                                break;
                            }
                            CurrentTile.SetNodeType(CurrentSettings.NodeType);
                            break;
                        case GridEditorTool.NodeEditor:

                            if (string.IsNullOrEmpty(CurrentTile.TileData.tileType))
                            {
                                break;
                            }

                            TileEditorTileView = CurrentTile;
                            tileEditorWindow.Open(CurrentTile.TileData.uid);
                            break;
                        case GridEditorTool.DecorationEditor:
                            if (string.IsNullOrEmpty(CurrentTile.TileData.tileType))
                            {
                                break;
                            }
                            CurrentTile.SetDecorationType(CurrentSettings.DecorationType);
                            break;
                        case GridEditorTool.NpcEditor:
                            if (string.IsNullOrEmpty(CurrentTile.TileData.tileType))
                            {
                                break;
                            }
                            CurrentTile.SetNpcType(CurrentSettings.NpcType);
                            break;
                        case GridEditorTool.PathEditor:
                        {
                            if (string.IsNullOrEmpty(CurrentTile.TileData.tileType))
                            {
                                break;
                            }
                            switch (CurrentSettings.PathMode)
                            {
                                case PathMode.Select:
                                    if (CurrentSettings.CurrentPathTile != 0)
                                    {
                                        var lastTile = HexGrid2D.GetTile(CurrentSettings.CurrentPathTile);
                                        lastTile.DeselectEditorMode();
                                    }
                                    CurrentSettings.PathMode = PathMode.Add;
                                    CurrentSettings.CurrentPathTile = CurrentTile.TileData.uid;
                                    CurrentTile.SelectEditorMode();
                                    break;
                                case PathMode.Add:
                                    if (CurrentSettings.CurrentPathTile == 0)
                                    {
                                        CurrentSettings.CurrentPathTile = CurrentTile.TileData.uid;
                                    }

                                    var selectedPathTile = HexGrid2D.GetTile(CurrentSettings.CurrentPathTile);

                                    if (selectedPathTile.TileData.IsNeighbour(CurrentTile.TileData.uid) && CurrentSettings.CurrentPathTile != CurrentTile.TileData.uid)
                                    {

                                        CurrentTile.TileData.connectingPathRooms.AddDistinct(CurrentSettings.CurrentPathTile);
                                        selectedPathTile.TileData.connectingPathRooms.AddDistinct(CurrentTile.TileData.uid);
                                        CurrentSettings.CurrentPathTile = CurrentTile.TileData.uid;

                                        selectedPathTile.DeselectEditorMode();
                                        CurrentTile.SelectEditorMode();

                                        if (CurrentTile.TileData.NodeType == NodeType.None)
                                        {
                                            CurrentTile.TileData.NodeType = NodeType.Path;
                                        }

                                        CurrentTile.SetTileType(CurrentTile.TileData.tileType);

                                        if (selectedPathTile.TileData.NodeType == NodeType.None)
                                        {
                                            selectedPathTile.TileData.NodeType = NodeType.Path;
                                        }

                                        selectedPathTile.SetTileType(selectedPathTile.TileData.tileType);
                                    }
                                    break;
                                case PathMode.Delete:

                                    var neighbors = HexGrid2D.GetNeighborsTile2D(CurrentTile.TileData.hex);

                                    for (var i = 0; i < neighbors.Count; i++)
                                    {
                                        neighbors[i].TileData.connectingPathRooms.Remove(CurrentTile.TileData.uid);
                                        if (neighbors[i].TileData.connectingPathRooms.Count <= 0 &&
                                            neighbors[i].TileData.NodeType == NodeType.Path)
                                        {
                                            neighbors[i].TileData.NodeType = NodeType.None;
                                        }

                                        neighbors[i].SetTileType(neighbors[i].TileData.tileType);
                                    }

                                    CurrentTile.TileData.connectingPathRooms.Clear();
                                    CurrentTile.TileData.NodeType = NodeType.None;
                                    CurrentTile.SetTileType(CurrentTile.TileData.tileType);
                                    break;
                            }
                            break;
                        }
                    }
                }

                UpdateRaycast(true);
            }

            if (Input.GetMouseButtonDown(1))
            {
                switch (CurrentSettings.Tool)
                {
                    case GridEditorTool.TilePlacement:

                        break;
                    case GridEditorTool.TileHeightModifier:
                        CurrentTile.TileData.Height--;
                        CurrentTile.GenerateHeight();
                        break;
                    case GridEditorTool.NodePlacement:
                        break;
                }

                UpdateRaycast(true);
            }
        }
    }
}