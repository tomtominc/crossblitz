﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Accolades;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.PathfindingAPI;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Rendering;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;
using Random = UnityEngine.Random;

namespace CrossBlitz.Fables.Grid
{
    public class HexTile2D : MonoBehaviour
    {
        private static class NeighborDirection
        {
            public const int TopRight = 0;
            public const int BottomRight = 1;
            public const int Bottom = 2;
            public const int BottomLeft = 3;
            public const int TopLeft = 4;
            public const int Top = 5;
        }

        [System.Serializable]
        public class LadderHeights
        {
            public GameObject left;
            public GameObject middle;
            public GameObject right;
        }

        public const float PixelSize = 1f / 32f;
        public const float Width = 31.5f * PixelSize;
        public const float Height = 19.5f * PixelSize;
        public const float HeightOffset = 15f * PixelSize;
        public const float CenterOffset = 8f * PixelSize;
        public const float AnimationHeightOffset = 20;
        public const int MinimumHeightForLadder = 2;
        public const float RevealDelay = 0.3f;

        public SortingGroup sortingGroup;
        public Transform tileObjectContainer;
        public Transform tileContainer;
        public Transform playerContainer;
        public Transform nodeParentTransform;
        public SpriteRenderer tileOverlay;

        [BoxGroup("Animators")] public SpriteAnimation tileAnimator;
        [BoxGroup("Animators")] public SpriteAnimation tileShadowAnimator;
        [BoxGroup("Animators")] public SpriteAnimation nodeShadowAnimator;
        [BoxGroup("Animators")] public SpriteAnimation nodeAnimator;
        [BoxGroup("Animators")] public SpriteAnimation markerShadowAnimator;
        [BoxGroup("Animators")] public SpriteAnimation markerAnimator;
        [BoxGroup("Animators")] public SpriteAnimation selectedOverlayAnimator;
        [BoxGroup("Animators")] public SpriteAnimation pathAnimator;
        [BoxGroup("Animators")] public SpriteAnimation decorationAnimator;
        [BoxGroup("Animators")] public SpriteAnimation npcAnimator;
        [BoxGroup("Animators")] public SpriteAnimation pathPoofAnimator;
        [BoxGroup("Animators")] public SpriteAnimation fallingTilePoofAnimator;
        [BoxGroup("Animators")] public SpriteAnimation notificationAnimator;
        [BoxGroup("VFX")] public SpriteAnimation hitDustEffectPrefab;
        [BoxGroup("Outline Settings")] public SpriteOutline nodeOutline;
        [BoxGroup("Outline Settings")] public SpriteOutline markerOutline;
        [BoxGroup("Outline Settings")] public SpriteOutline npcOutline;
        [BoxGroup("Outline Settings")] public Transform selectionArrow;
        [BoxGroup("Outline Settings")] public SpriteAnimation selectionArrowAnimator;
        [BoxGroup("Event Effect")] public SpriteRenderer eventEffectOverlay;
        [BoxGroup("Event Effect")] public SpriteAnimation eventEffectBeam;
        [BoxGroup("Height Settings")] public List<SpriteAnimation> tileHeightAdditions;
        [BoxGroup("Map Editor")] public PolygonCollider2D editorHitBox;
        [BoxGroup("Side Quest")] public Transform sideQuestIcon;
        [BoxGroup("Accolades")] public Transform accoladePipParent;
        [BoxGroup("Accolades")] public AccoladePipDisplay accoladePipDisplayPrefab;

        private FableTileData m_tileData;
        public List<LadderHeights> ladderHeights;

        private float _offsetHover;
        private GridPlayer _player;
        private int _tileIndex = -1;
        private float _targetSelectOverlayAlpha;
        private bool m_locked;
        private bool _hasNotification = false;

        public FableTileData TileData => m_tileData;
        public bool IsRevealed => true; //ClientTileData == null || (ClientTileData.Revealed && ClientTileData.HasBeenShown);

        public ClientTileData ClientTileData => FablesMapController.Instance.GetClientTile(TileData);

        public Vector2 Position
        {
            get => transform.localPosition;
            set => transform.localPosition = value;
        }

        public Vector2 WorldPosition
        {
            get => transform.position;
            set => transform.position = value;
        }

        public Vector2 Center
        {
            get
            {
                if (TileData != null)
                {
                    return WorldPosition + new Vector2(0, CenterOffset + TileData.Height * HeightOffset);
                }

                return WorldPosition + new Vector2(0, CenterOffset);
            }
        }

        public int originalSortingOrder { get; set; }

        public GridPlayer Player
        {
            get => _player;
            set => _player = value;
        }

        public int SortingOrder
        {
            get => sortingGroup.sortingOrder;
            set => sortingGroup.sortingOrder = value;
        }

        private bool _animatingIn;
        private SpringTileEffect _springEffect;
        private float _springTension;
        private float _springDampening;

        private void OnEnable()
        {
            _offsetHover = Random.Range(0, 10);
            _springEffect = new SpringTileEffect(tileObjectContainer, 0);
            _springTension = 0.025f;
            _springDampening = 0.025f;
        }

        private void Update()
        {
            if (m_tileData == null) return;

            if (selectionArrow.IsActive())
            {
                var pos = selectionArrow.localPosition;

                if (!string.IsNullOrEmpty(TileData.Npc))
                {
                    pos.y = FableTileData.SelectArrowHeight[NodeType.Shop] + Mathf.PingPong(Time.time, 0.125f);
                }
                else if (FableTileData.SelectArrowHeight.TryGetValue(m_tileData.NodeType, out var height))
                {
                    pos.y = height + Mathf.PingPong(Time.time, 0.125f);
                }
                else // fallback height is average
                {
                    pos.y = FableTileData.SelectArrowHeight[NodeType.Battle] + Mathf.PingPong(Time.time, 0.125f);
                }

                selectionArrow.localPosition = pos;
            }

            if (FableTileData.MarkerAnimationHeight.TryGetValue(m_tileData.NodeType, out var animHeight))
            {
                AnimateMarker(animHeight);
            } // no fallback because we don't want ALL markers animating (like chests and the shop etc)

            // if (FableTileData.TileBehaviourTypes.ContainsKey(fableData.TileType))
            // {
            //     switch (FableTileData.TileBehaviourTypes[fableData.TileType])
            //     {
            //         case TileBehaviourType.Float:
            //             if (Mathf.Abs(_springEffect.Speed) <= 0.0001f)
            //             {
            //                 _springEffect.Speed = -0.001f;
            //             }
            //             //_springEffect.Speed =  0.02f - Mathf.PingPong(Time.time, 0.04f);
            //             break;
            //     }
            // }

            if (Mathf.Abs(_targetSelectOverlayAlpha - selectedOverlayAnimator.renderer.color.a) > float.Epsilon)
            {
                var alpha = selectedOverlayAnimator.renderer.color.a;
                alpha = Mathf.MoveTowards(alpha, _targetSelectOverlayAlpha, Time.deltaTime * 2f);
                selectedOverlayAnimator.renderer.color = new Color(1, 1, 1, alpha);
            }

            if (_hasNotification)
            {
                AnimateNotification(0.2f);
            }
        }

        private void FixedUpdate()
        {
            _springEffect.Update(_springDampening, _springTension, 0);
        }

        private void AnimateMarker(float animHeight)
        {
            const float speed = 0.1f;
            var pos = markerAnimator.transform.localPosition;
            pos.y = Mathf.PingPong((Time.time + _offsetHover) * speed,animHeight);
            markerAnimator.transform.localPosition = pos;

            var shadowTransform = markerShadowAnimator.transform;
            var scale = shadowTransform.localScale;
            scale.x = 1 - Mathf.PingPong((Time.time + _offsetHover) * speed,animHeight);
            scale.y = 1 - Mathf.PingPong((Time.time + _offsetHover) * speed,animHeight);
            shadowTransform.localScale = scale;

            pos = shadowTransform.localPosition;
            pos.y = -(Mathf.PingPong((Time.time + _offsetHover) * speed,animHeight) * 0.5f);
            shadowTransform.localPosition = pos;
        }

        private void AnimateNotification(float animHeight)
        {
            const float speed = 0.1f;
            var pos = notificationAnimator.transform.localPosition;
            pos.y = Mathf.PingPong((Time.time + _offsetHover) * speed, animHeight);
            notificationAnimator.transform.localPosition = pos + new Vector3(0f, 0.5f);
        }

        public void SetTileType(string tileType)
        {
            m_tileData.tileType = tileType;
            SetHexData(m_tileData, false);
        }

        public void SetNodeType(NodeType nodeType)
        {
            m_tileData.NodeType = nodeType;
            SetHexData(m_tileData, false);
        }

        public void SetDecorationType(string decoration)
        {
            m_tileData.Decoration = decoration;
            SetHexData(m_tileData, false);
        }

        public void SetNpcType(string npc)
        {
            m_tileData.Npc = npc;
            SetHexData(m_tileData, false);
        }

        public void SetTileIndex(int index)
        {
            _tileIndex = index;
        }

        public void SetHexData(FableTileData fData, bool animateIn, bool generatePaths=true)
        {
            m_tileData = fData;
            _animatingIn = animateIn;

            tileAnimator.UpdateAnimations();
            nodeShadowAnimator.UpdateAnimations();
            nodeAnimator.UpdateAnimations();
            markerShadowAnimator.UpdateAnimations();
            markerAnimator.UpdateAnimations();

            if (m_tileData.IsEmptyTile())
            {
                tileAnimator.SetActive(false);
                tileShadowAnimator.SetActive(false);
                nodeShadowAnimator.SetActive(false);
                nodeAnimator.SetActive(false);
                npcAnimator.SetActive(false);
                pathAnimator.SetActive(true);
                markerShadowAnimator.SetActive(false);
                markerAnimator.renderer.enabled = false;
                decorationAnimator.SetActive(false);
                notificationAnimator.SetActive(false);
                notificationAnimator.renderer.enabled = false;
                // if (generatePaths) GenerateHeight();
                return;
            }

            var battleData = Db.BattleDatabase.GetBattleData(m_tileData.battleUid);

            if (battleData?.Accolades != null && battleData.Accolades.Count > 0 && m_tileData.NodeType == NodeType.Battle && ClientTileData != null)
            {
                accoladePipParent.DestroyChildren();
                accoladePipParent.localPosition = new Vector3(0, -0.8125f, 0);

                var pips = new List<Transform>();

                //ClientTileData?.BattleData?.FixDependencies(battleData);

                //Debug.LogError($"BattleData: {battleData.Uid} == {ClientTileData.BattleData.BattleUid}");

                for (var i = 0; i < battleData.Accolades.Count; i++)
                {
                    var accoladePip = Instantiate(accoladePipDisplayPrefab, accoladePipParent, false);
                    accoladePip.transform.localPosition = Vector3.zero;

                    if (ClientTileData == null)
                    {
                        continue;
                    }

                    if (ClientTileData.BattleData == null)
                    {
                        Debug.LogError("Battle Data was null and was reset!");
                        ClientTileData.Init(TileData);
                    }

                    if (ClientTileData.BattleData.Accolades == null || ClientTileData.BattleData.Accolades.Count <= i)
                    {
                        Debug.LogError("No Accolade data found! Resetting data.");
                        ClientTileData.BattleData.FixDependencies(battleData);
                    }

                    accoladePip.SetAccolade(battleData.Accolades[i], ClientTileData.BattleData.Accolades[i], battleData.Faction);

                    pips.Add( accoladePip.transform );
                }

                if (pips.Count > 0)
                {
                    GameUtilities.CenterTransformsHorizontal(accoladePipParent, pips, 4 * (1 / 32f));
                }
            }

            if(m_tileData.NodeType == NodeType.Shop)
            {
                var bookData = App.FableData.GetCurrentBook();
                var items = m_tileData.items;
                var setActive = true;

                for (var i = items.Count - 1; i >= 0; i--)
                {
                    if (bookData == null || bookData.StoreData == null || bookData.StoreData.Items == null)
                    {
                        continue;
                    }

                    for (var k = bookData.StoreData.Items.Count - 1; k >= 0; k--)
                    {
                        var bookItem = bookData.StoreData.Items[k];

                        if (bookItem.ItemId == items[i].ItemUid)
                        {
                            setActive = false;
                        }
                    }
                }

                if (setActive)
                {
                    notificationAnimator.SetActive(true);
                    notificationAnimator.renderer.enabled = true;
                    _hasNotification = true;
                }

            }

            tileAnimator.SetActive(true);
            //tileShadowAnimator.SetActive(true);

            var tileTypeData = Db.FablesDatabase.GetTileTypeData(m_tileData.tileType);

            m_tileData.PlayTileAnimation(tileAnimator);

            if (m_tileData.NodeType == NodeType.Path && !string.IsNullOrEmpty(tileTypeData.PathType) && IsRevealed)
            {
                nodeShadowAnimator.SetActive(false);
                nodeAnimator.SetActive(false);
                markerShadowAnimator.SetActive(false);
                markerAnimator.renderer.enabled = false;
                npcAnimator.SetActive(false);

                if (generatePaths) pathAnimator.SetActive(GeneratePath());
            }
            else if (!string.IsNullOrEmpty(m_tileData.Decoration))
            {
                nodeShadowAnimator.SetActive(false);
                nodeAnimator.SetActive(false);
                markerShadowAnimator.SetActive(false);
                markerAnimator.renderer.enabled = false;
                pathAnimator.SetActive(false);
                npcAnimator.SetActive(false);
                decorationAnimator.SetActive(true);
                decorationAnimator.Play(m_tileData.Decoration);
                UpdateDecoration();
            }
            else if (!string.IsNullOrEmpty(TileData.Npc))
            {
                nodeShadowAnimator.SetActive(false);
                nodeAnimator.SetActive(false);
                markerAnimator.renderer.enabled = false;
                pathAnimator.SetActive(false);
                decorationAnimator.SetActive(false);
                npcAnimator.SetActive(true);
                markerShadowAnimator.SetActive(true);

                var markerShadowAnimation = FableTileData.MarkerShadowAnimations[NodeType.Battle];

                markerShadowAnimator.Play(markerShadowAnimation);
                npcAnimator.Play($"{TileData.Npc}-march");
            }
            else if (m_tileData.NodeType == NodeType.Start || IsRevealed)
            {
                // set the path stuff to false if its not a path
                pathAnimator.SetActive(false);
                decorationAnimator.SetActive(false);
                npcAnimator.SetActive(false);

                var nodeAnim = FableTileData.GetNodeAnimation(m_tileData);
                var nodeShadowAnimation = "generic";

                if (FableTileData.NodeShadowAnimations.TryGetValue(m_tileData.NodeType, out var shadowAnim))
                {
                    nodeShadowAnimation = shadowAnim;
                }

                if (string.IsNullOrEmpty(nodeAnim))
                {
                    nodeShadowAnimator.SetActive(false);
                    nodeAnimator.SetActive(false);
                }
                else
                {
                    nodeShadowAnimator.SetActive(true);
                    nodeAnimator.SetActive(true);

                    if (!nodeShadowAnimator.Play(nodeShadowAnimation))
                    {
                        nodeShadowAnimator.SetActive(false);
                    }

                    nodeAnimator.Play(nodeAnim);
                }

                markerAnimator.transform.localPosition = Vector2.zero;

                var shadowTransform = markerShadowAnimator.transform;
                shadowTransform.localScale = Vector3.one;
                shadowTransform.localPosition = Vector2.zero;

                var markerAnim = FableTileData.GetMarkerAnimation(m_tileData);
                var markerShadowAnim = "peddlers-wagon";

                if (FableTileData.MarkerShadowAnimations.TryGetValue(m_tileData.NodeType, out var markerShadow))
                {
                    markerShadowAnim = markerShadow;
                }

                switch (m_tileData.NodeType)
                {
                    case NodeType.Battle:
                    case NodeType.Chest:
                    case NodeType.Event1:
                    case NodeType.Event2:
                        if (string.IsNullOrEmpty(markerAnim) || (ClientTileData != null && ClientTileData.Complete))
                        {
                            markerShadowAnimator.SetActive(false);
                            markerAnimator.renderer.enabled = false;
                        }
                        else
                        {
                            markerShadowAnimator.SetActive(true);
                            markerAnimator.renderer.enabled = true;

                            if (!markerShadowAnimator.Play(markerShadowAnim))
                            {
                                markerShadowAnimator.SetActive(false);
                            }

                            markerAnimator.Play(markerAnim);
                        }
                        break;
                    case NodeType.None:
                        markerShadowAnimator.SetActive(false);
                        markerAnimator.renderer.enabled = false;
                        break;
                    default:
                        markerShadowAnimator.SetActive(true);
                        markerAnimator.renderer.enabled = true;

                        if (!markerShadowAnimator.Play(markerShadowAnim))
                        {
                            markerShadowAnimator.SetActive(false);
                        }

                        markerAnimator.Play(markerAnim);
                        break;
                }
            }

            LockBoundingArea();

            if (generatePaths)
            {
                GenerateHeight();

                if (!string.IsNullOrEmpty(m_tileData.questId))
                {
                    var questShowing = IsQuestShowing(out var isSideQuest);
                    sideQuestIcon.SetActive(isSideQuest);
                    nodeParentTransform.SetActive(questShowing);
                }
            }
        }

        public bool IsQuestShowing(out bool isSideQuest)
        {
            var quest = Db.FableQuestDatabase.GetQuest(m_tileData.questId);
            var chapterData = FablesMapController.Instance.ClientChapterData;
            isSideQuest = quest?.IsSideQuest ?? false;

            // in gameplay
            if (quest != null && chapterData != null && ClientTileData != null)
            {
                // the quest is active and you're able to show it
                if (chapterData.CanShowQuestNode(m_tileData.questId, m_tileData.questStep))
                {
                    // the quest hasn't been revealed, you'll reveal it later so just turn off the node parent
                    return true;
                }

                return false;
            }

            return true;
        }

        public void ShowQuestNode()
        {

        }

        public void UpdateDecoration()
        {
            if (TileData != null && !string.IsNullOrEmpty(TileData.Decoration))
            {
                var decorationData = Db.FablesDatabase.GetDecorationData(TileData.Decoration);
                decorationAnimator.transform.localPosition = decorationData.GetOffsetInWorldSpace();
            }
        }

        public void RefreshHexData(bool generatePaths = true)
        {
            SetHexData(m_tileData, false, generatePaths);
        }

        [Button(ButtonSizes.Medium, ButtonStyle.Box, Expanded = true)]
        public void GenerateHeight()
        {
            if (m_tileData.Height > 10)
            {
                m_tileData.Height = 10;
            }

            if (m_tileData.Height < 0)
            {
                m_tileData.Height = 0;
            }

            var animationOffset = 0f;

            if (_animatingIn /*&& _hexData.Height > 0*/)
            {
                animationOffset = AnimationHeightOffset;
            }

            tileContainer.localPosition = new Vector3(0, m_tileData.Height * HeightOffset + animationOffset, 0);
            tileHeightAdditions.ForEach(tile => tile.SetActive(false));

            for (var i = 0; i < m_tileData.Height; i++)
            {
                tileHeightAdditions[i].Play(tileAnimator.CurrentAnimationName);
                tileHeightAdditions[i].transform.localPosition = new Vector3(0,
                    i * HeightOffset + animationOffset /* + (i == 0? 0: animationOffset)*/, 0);
                tileHeightAdditions[i].renderer.sortingOrder = -tileHeightAdditions.Count + i;
            }

            //heightAdditionsContainer.localPosition = new Vector3(0, -(_hexData.Height * HeightOffset + animationOffset), 0);

            var points = editorHitBox.GetPath(0);

            points[2].y = 0.25f - (m_tileData.Height * 0.5f);
            points[3].y = -0.25f - (m_tileData.Height * 0.5f);
            points[4].y = -0.25f - (m_tileData.Height * 0.5f);
            points[5].y = 0.25f - (m_tileData.Height * 0.5f);

            editorHitBox.SetPath(0, points);

            GenerateShadow();
            RefreshNeighborShadows();
        }

        public void GenerateShadow()
        {
            if (m_tileData.IsEmptyTile()) return;

            var topNeighbor = HexGrid2D.GetNeighborTile2D(m_tileData.hex, NeighborDirection.Top);
            var topLeftNeighbor = HexGrid2D.GetNeighborTile2D(m_tileData.hex, NeighborDirection.TopLeft);

            if (topNeighbor && topLeftNeighbor)
            {
                if (topNeighbor.GetHeight() > GetHeight() && topLeftNeighbor.GetHeight() > GetHeight())
                {
                    tileShadowAnimator.SetActive(true);
                    tileShadowAnimator.Play("top-right-shadow");
                }
                else if (topNeighbor.GetHeight() > GetHeight())
                {
                    tileShadowAnimator.SetActive(true);
                    tileShadowAnimator.Play("top-shadow");
                }
                else if (topLeftNeighbor.GetHeight() > GetHeight())
                {
                    tileShadowAnimator.SetActive(true);
                    tileShadowAnimator.Play("right-shadow");
                }
                else
                {
                    tileShadowAnimator.SetActive(false);
                }
            }
            else if (topNeighbor && topNeighbor.GetHeight() > GetHeight())
            {
                tileShadowAnimator.SetActive(true);
                tileShadowAnimator.Play("top-shadow");
            }
            else if (topLeftNeighbor && topLeftNeighbor.GetHeight() > GetHeight())
            {
                tileShadowAnimator.SetActive(true);
                tileShadowAnimator.Play("right-shadow");
            }
            else
            {
                tileShadowAnimator.SetActive(false);
            }
        }

        private void RefreshNeighborShadows()
        {
            var bottomNeighbor = HexGrid2D.GetNeighborTile2D(m_tileData.hex, NeighborDirection.Bottom);
            var bottomRightNeighbor = HexGrid2D.GetNeighborTile2D(m_tileData.hex, NeighborDirection.BottomRight);

            if (bottomNeighbor)
            {
                bottomNeighbor.GenerateShadow();
            }

            if (bottomRightNeighbor)
            {
                bottomRightNeighbor.GenerateShadow();
            }
        }

        private bool GeneratePath()
        {
            if (m_tileData.IsEmptyTile())
            {
                return false;
            }

            if (ClientTileData != null && !ClientTileData.Complete)
            {
                //Debug.LogError($"Tile {Db.MapDatabase.GetDirectoryNameForTile(m_tileData.uid)} is not complete!");
                return false;
            }

            TileData.connectingPathRooms ??= new List<int>();

            var tileTypeData = Db.FablesDatabase.GetTileTypeData(m_tileData.tileType);
            var pathEnvironment = tileTypeData.PathType;
            var pathType = "-1";
            var flipX = false;

            var topNeighbor = HexGrid2D.GetNeighborTile2D(m_tileData.hex, NeighborDirection.Top);
            var topRightNeighbor = HexGrid2D.GetNeighborTile2D(m_tileData.hex, NeighborDirection.TopRight);
            var bottomRightNeighbor = HexGrid2D.GetNeighborTile2D(m_tileData.hex, NeighborDirection.BottomRight);
            var bottomNeighbor = HexGrid2D.GetNeighborTile2D(m_tileData.hex, NeighborDirection.Bottom);
            var bottomLeftNeighbor = HexGrid2D.GetNeighborTile2D(m_tileData.hex, NeighborDirection.BottomLeft);
            var topLeftNeighbor = HexGrid2D.GetNeighborTile2D(m_tileData.hex, NeighborDirection.TopLeft);

            if (NeedsPathAccess(this, topRightNeighbor) &&
                NeedsPathAccess(this, bottomNeighbor) &&
                NeedsPathAccess(this, topLeftNeighbor))
            {
                pathType = "7";
            }
            else if (NeedsPathAccess(this, topNeighbor) &&
                     NeedsPathAccess(this, bottomRightNeighbor) &&
                     NeedsPathAccess(this, bottomLeftNeighbor))
            {
                pathType = "8";
            }
            else if (NeedsPathAccess(this, topNeighbor) &&
                     NeedsPathAccess(this, bottomNeighbor))
            {
                pathType = "1";
            }
            else if (NeedsPathAccess(this, topLeftNeighbor) &&
                     NeedsPathAccess(this, bottomRightNeighbor))
            {
                pathType = "2";
            }
            else if (NeedsPathAccess(this, topRightNeighbor) &&
                     NeedsPathAccess(this, bottomLeftNeighbor))
            {
                pathType = "2";
                flipX = true;
            }
            else if (NeedsPathAccess(this, topLeftNeighbor) &&
                     NeedsPathAccess(this, bottomNeighbor))
            {
                pathType = "3a";
            }
            else if (NeedsPathAccess(this, topRightNeighbor) &&
                     NeedsPathAccess(this, bottomNeighbor))
            {
                pathType = "3b";
            }
            else if (NeedsPathAccess(this, topNeighbor) &&
                     NeedsPathAccess(this, bottomLeftNeighbor))
            {
                pathType = "4a";
            }
            else if (NeedsPathAccess(this, topNeighbor) &&
                     NeedsPathAccess(this, bottomRightNeighbor))
            {
                pathType = "4b";
            }
            else if (NeedsPathAccess(this, topLeftNeighbor) &&
                     NeedsPathAccess(this, topRightNeighbor))
            {
                pathType = "5";
            }
            else if (NeedsPathAccess(this, bottomLeftNeighbor) &&
                     NeedsPathAccess(this, bottomRightNeighbor))
            {
                pathType = "6";
            }
            // else if (NeedsPathAccess(this, topRightNeighbor) ||
            //          NeedsPathAccess(this, bottomLeftNeighbor))
            // {
            //     pathType = "2";
            //     flipX = true;
            //
            // }
            // else if (NeedsPathAccess(this, bottomRightNeighbor) ||
            //          NeedsPathAccess(this, topLeftNeighbor))
            // {
            //     pathType = "2";
            // }
            else
            {
                pathType = "-1";
            }

            if (pathType == "-1")
            {
                pathAnimator.SetActive(false);
                GenerateLadders(topNeighbor,topLeftNeighbor,topRightNeighbor);
                return false;
            }

            pathAnimator.SetActive(true);
            pathAnimator.Play($"{pathEnvironment}-{pathType}");
            pathAnimator.renderer.flipX = flipX;

            GenerateLadders(topNeighbor,topLeftNeighbor,topRightNeighbor);

            return true;
        }

        private void GenerateLadders(HexTile2D topNeighbor, HexTile2D topLeftNeighbor, HexTile2D topRightNeighbor)
        {
            for (var i = 0; i < ladderHeights.Count; i++)
            {
                ladderHeights[i].left.SetActive(false);
                ladderHeights[i].middle.SetActive(false);
                ladderHeights[i].right.SetActive(false);
            }

            int heightDiff;

            if (NeedsLadder(this, topNeighbor, out heightDiff))
            {
                var index = heightDiff - MinimumHeightForLadder;

                if (index >= ladderHeights.Count|| index < 0)
                {
                    Debug.LogError($"Ladders for heights above {heightDiff} are not supported");
                }
                else
                {
                    var ladderHeight = ladderHeights[index];
                    ladderHeight.middle.SetActive(true);
                }
            }

            if (NeedsLadder(this, topLeftNeighbor, out heightDiff))
            {
                var index = heightDiff - MinimumHeightForLadder;

                if (index >= ladderHeights.Count|| index < 0)
                {
                    Debug.LogError($"Ladders for heights above {heightDiff} are not supported");
                }
                else
                {
                    var ladderHeight = ladderHeights[index];
                    ladderHeight.left.SetActive(true);
                }
            }

            if (NeedsLadder(this, topRightNeighbor, out heightDiff))
            {
                var index = heightDiff - MinimumHeightForLadder;

                if (index >= ladderHeights.Count || index < 0)
                {
                    Debug.LogError($"Ladders for heights above {heightDiff} are not supported");
                }
                else
                {
                    var ladderHeight = ladderHeights[index];
                    ladderHeight.right.SetActive(true);
                }
            }
        }

        private void LockBoundingArea()
        {
            if (m_locked) return;

            if (Position.x >  4.40625f)
            {
                Position = new Vector2(Position.x - PixelSize, Position.y);
            }

            if (Position.x < -4.40625f)
            {
                Position = new Vector2(Position.x + PixelSize, Position.y);
            }

            if (Position.y < -2.2f)
            {
                Position = new Vector2(Position.x, Position.y - PixelSize);
            }

            if (Position.y > 2.2f)
            {
                Position = new Vector2(Position.x, Position.y + PixelSize);
            }

            m_locked = true;
        }

        public void OnPointerEnterEditorMode(GridEditorToolSettings settings)
        {
            if (m_tileData.IsEmptyTile()) return;

            if (settings.Tool == GridEditorTool.TilePlacement || settings.Tool == GridEditorTool.TileHeightModifier ||
                m_tileData.NodeType == NodeType.None || m_tileData.NodeType == NodeType.Path)
            {
                markerOutline.Clear();
                npcOutline.Clear();
                nodeOutline.Clear();
            }
            else if (settings.Tool == GridEditorTool.NodePlacement || settings.Tool == GridEditorTool.NodeEditor || settings.Tool == GridEditorTool.DecorationEditor)
            {
                if (markerOutline.IsActive())
                {
                    markerOutline.color = Color.white;
                    markerOutline.Regenerate();
                }

                if (npcAnimator.IsActive())
                {
                    npcOutline.color = Color.white;
                    npcOutline.Regenerate();
                }

                if (nodeOutline.IsActive())
                {
                    nodeOutline.color = Color.white;
                    nodeOutline.Regenerate();
                }
            }

            _targetSelectOverlayAlpha = 0.5f;
            selectedOverlayAnimator.SetActive(true);
            selectedOverlayAnimator.Play("animate");
            selectionArrow.SetActive(true);
        }

        public void OnPointerClickEditorMode()
        {
            //Push(-0.1f);
        }

        public void OnPointerHoverExploreMode()
        {

        }

        public void OnPointerEnterExploreMode()
        {
            if (!HexGrid2D.inputEnabled || !m_tileData.IsValidNodeType() || m_tileData.IsEmptyTile())
            {
                return;
            }

            if (!string.IsNullOrEmpty(m_tileData.questId))
            {
                if (!IsQuestShowing(out var isSideQuest))
                {
                    return;
                }
            }

            //Debug.Log($"Hovered = {m_tileData.NodeType}");

            AudioController.PlaySound("Text-LetterB");

            Events.Publish(this,EventType.OnHexTilePointerEnter, new OnHexTilePointerEnterEventArgs{tile = this} );

            if (m_tileData.NodeType == NodeType.None || m_tileData.NodeType == NodeType.Path )
            {
                //markerOutline.Clear();
                //npcOutline.Clear();
                nodeOutline.Clear();
            }
            else
            {
                if (_player == null)
                {
                    // if (markerOutline.IsActive())
                    // {
                    //     markerOutline.color = Color.white;
                    //     markerOutline.Regenerate();
                    // }
                    //
                    // if (npcAnimator.IsActive())
                    // {
                    //     npcOutline.color = Color.white;
                    //     npcOutline.Regenerate();
                    // }
                }

                // if (nodeOutline.IsActive())
                // {
                //     nodeOutline.color = Color.white;
                //     nodeOutline.Regenerate();
                // }
            }

            _targetSelectOverlayAlpha = 0.5f;
            selectedOverlayAnimator.SetActive(true);
            selectedOverlayAnimator.Play("animate");
            selectionArrow.SetActive(true);
            selectionArrowAnimator.Play("idle");
        }

        public void OnLeftClick_ExploreMode()
        {
            // if (!HexGrid2D.inputEnabled || !m_tileData.IsValidNodeType() || m_tileData.IsEmptyTile())
            // {
            //     return;
            // }

            Push(-0.1f);
            Events.Publish(this,EventType.OnHexTilePointerExit, new OnHexTilePointerExitEventArgs{tile = this} );
        }

        public void OnRightClick_ExploreMode()
        {
            // if (!HexGrid2D.inputEnabled || !m_tileData.IsValidNodeType() || m_tileData.IsEmptyTile())
            // {
            //     return;
            // }

            Push(-0.1f);

            Events.Publish(this, EventType.OpenTileInfo, new OpenTileInfoEventArgs
            {
                tileData = TileData,
                clientTileData = ClientTileData
            });
        }

        public void Push(float power, bool withNeighbors = false)
        {
            _springEffect.Speed = power;

            if (withNeighbors)
            {
                StartCoroutine(PushNeighbors(power));
            }
        }

        private IEnumerator PushNeighbors(float power)
        {
            for (var i = 0; i < 5; i++)
            {
                yield return new WaitForSeconds(0.06f);

                var tiles = HexGrid2D.GetTilesAtDistance(TileData.hex, i + 1);

                for (var j = 0; j < tiles.Count; j++)
                {
                    power *= 0.8f;
                    tiles[j].Push(power);
                }
            }
        }

        public void SelectEditorMode()
        {
            selectedOverlayAnimator.SetActive(true);
            selectedOverlayAnimator.Play("animate");
            selectionArrow.SetActive(true);
        }

        public void DeselectEditorMode()
        {
            selectedOverlayAnimator.SetActive(false);
            selectionArrow.SetActive(false);
        }

        public void Select()
        {
            selectedOverlayAnimator.SetActive(true);
            selectedOverlayAnimator.Play("animate");
            selectionArrow.SetActive(true);

            if (m_tileData.IsValidNodeType())
            {
                Events.Publish(this, EventType.OnNodeSelected, new OnNodeSelectedEventArgs {tileUid = m_tileData.uid});
            }
        }

        public void Deselect()
        {
            if (m_tileData.IsValidNodeType())
            {
                Events.Publish(this, EventType.OnNodeSelected, new OnNodeSelectedEventArgs {tileUid = 0});
            }
        }

        public void OnPointerExit(bool clearOutline = true)
        {
            Events.Publish(this,EventType.OnHexTilePointerExit, new OnHexTilePointerExitEventArgs{tile = this} );

            if (clearOutline)
            {
                markerOutline.Clear();
                npcOutline.Clear();
                nodeOutline.Clear();

                selectionArrow.SetActive(false);
                _targetSelectOverlayAlpha = 0;

                SortingOrder = originalSortingOrder;
            }
        }

        public int GetHeight()
        {
            if (_animatingIn) return _visualBlockCount;

            return TileData.Height;
        }

        private int _visualBlockCount;
        private int _animateBlockCount;

        public bool NextBlockIn()
        {
            AnimateBlockIn(_animateBlockCount);
            _animateBlockCount++;
            return _animateBlockCount > TileData.Height;
        }

        public void AnimateBlockIn(int block)
        {
            const float dur = 0.25f;
            const float punch = 0.125f;

            if (block == TileData.Height)
            {
                tileContainer.DOLocalMoveY(TileData.Height * HeightOffset, dur)
                    .OnComplete(() =>
                    {
                        _springTension = 0.05f;
                        _springDampening = 0.1f;

                        _springEffect.Speed = -punch;

                        _animatingIn = false;

                        RefreshHexData();
                    });
            }
            else
            {
                var i = block;
                tileHeightAdditions[i].transform.DOLocalMoveY(i * HeightOffset, dur)
                    .OnComplete(() =>
                    {
                        _springTension = 0.05f;
                        _springDampening = 0.1f;

                        _springEffect.Speed = -punch;
                        _visualBlockCount++;
                        GenerateShadow();
                        RefreshNeighborShadows();
                    });
            }
        }

        public void AnimateBlockOut()
        {
            const float dur = 0.5f;

            transform.DOLocalMoveY(AnimationHeightOffset, dur).SetEase(Ease.InBack);
        }

        public void PlayerEnterMap(GridPlayer player)
        {
            Events.Publish(this, EventType.OnPlayerEnteredMap, new OnPlayerEnteredMapEventArgs { player = player, tile = this });
            PlayerEnterTile(player,true);
        }

        public void PlayerEnterTile(GridPlayer player, bool fromEnter)
        {
            _player = player;
            _player.CurrentTile = this;

            var playerTrans = _player.transform;
            playerTrans.SetParent(playerContainer, true);
            playerTrans.localPosition = Vector3.zero;

            markerAnimator.renderer.DOKill();
            markerShadowAnimator.renderer.DOKill();

            markerAnimator.renderer.color = new Color(1, 1, 1, 0);
            markerShadowAnimator.renderer.color = new Color(1, 1, 1, 0);

            // these nodes need no actions to be "complete"
            switch (m_tileData.NodeType)
            {
                case NodeType.Shop:
                case NodeType.ManaMeld:
                case NodeType.Start:
                case NodeType.ExitDown:
                case NodeType.ExitLeft:
                case NodeType.ExitRight:
                case NodeType.ExitUp:
                case NodeType.EnterNewMap:
                case NodeType.ExitNewMap:
                    {
                    if (ClientTileData != null)
                    {
                        FablesMapController.Instance.ClientChapterData.SetTileAsComplete(ClientTileData.TileUid, false);
                        RefreshHexData();
                    }

                    break;
                }
            }

            Events.Publish(this,EventType.OnPlayerArrivedAtTile, new OnPlayerArrivedAtTileEventArgs { player = _player, tile = this, fromEnter = fromEnter} );

            HexGrid2D.OnPlayerEntered();
            //RecursePathGlowOverlay(TileData, new List<FableTileData> {TileData});
        }

        private void DrawGlowOverlay(FableTileData playerTile)
        {
            if (ClientTileData == null || ClientTileData.Complete)
            {
                return;
            }

            if (Pathfinding.FindPath(playerTile, TileData, false, true) != null)
            {
                tileOverlay.SetActive(true);
                tileOverlay.DOColor(new Color(1, 1, 1, 0f), 0.4f).SetLoops(-1, LoopType.Yoyo);
            }
            else
            {
                tileOverlay.DOKill();
                tileOverlay.color = new Color(1, 1, 1, 0.5f);
                tileOverlay.SetActive(false);
            }
        }

        private void RecursePathGlowOverlay(FableTileData playerTile, List<FableTileData> checkedTiles)
        {
            for (var i = 0; i < TileData.connectingPathRooms.Count; i++)
            {
                var tile = HexGrid2D.GetTile(TileData.connectingPathRooms[i]);

                if (tile == null) continue;
                if (checkedTiles.Contains(tile.TileData)) continue;

                if (tile != null && tile.TileData != null && tile.TileData.IsValidNodeType())
                {
                    tile.DrawGlowOverlay(playerTile);
                    checkedTiles.Add(tile.TileData);
                }
                else if (tile != null && tile.TileData != null && tile.TileData.NodeType == NodeType.Path)
                {
                    checkedTiles.Add(tile.TileData);
                    tile.RecursePathGlowOverlay(playerTile, checkedTiles);
                }
            }
        }

        public void PlayerExitTile()
        {
            _player = null;

            Events.Publish(this,EventType.OnPlayerExitTile, new OnPlayerExitTileEventArgs { player = _player, tile = this } );
        }

        public Vector2 GetDialogueTargetWorldPosition()
        {
            if (_player != null)
            {
                return _player.transform.position;
            }

            Debug.LogError($"Could not get a target world position for dialogue {name}");

            return Vector2.zero;
        }

        public IEnumerator<float> PlayEventEffect()
        {
            eventEffectOverlay.SetActive(true);
            eventEffectBeam.SetActive(true);

            eventEffectOverlay.color = Color.white;
            eventEffectBeam.Play("beam");
            yield return Timing.WaitUntilDone(eventEffectOverlay
                .DOFade(0, eventEffectBeam.GetCurrentAnimationLength())
                .WaitForCompletion(true));

            eventEffectOverlay.SetActive(false);
            eventEffectBeam.SetActive(false);
        }

        public void TurnOffNotification()
        {
            if (_hasNotification) _hasNotification = false;
        }

        public bool IsPathRevealed()
        {
            return pathAnimator.IsActive() || pathPoofAnimator.IsActive();
        }

        public IEnumerator<float> RevealPathWithPoof()
        {
            if (TileData.NodeType == NodeType.Path)
            {
                if (ClientTileData.Complete && !IsPathRevealed())
                {
                    AudioController.PlaySound("TerrainTile-Poof");
                    pathPoofAnimator.SetActive(true);
                    pathPoofAnimator.Play("spawn", () => pathPoofAnimator.SetActive(false));

                    while (pathPoofAnimator.CurrentFrame < 3)
                    {
                        yield return Timing.WaitForOneFrame;
                    }

                    RefreshHexData();
                }

                GeneratePath();
            }
        }

        public IEnumerator<float> FallFromSky()
        {
            var originalMarkerPosition = nodeParentTransform.localPosition;

            nodeParentTransform.localPosition = new Vector3(originalMarkerPosition.x, originalMarkerPosition.y + 11f, originalMarkerPosition.z);
            nodeParentTransform.SetActive(true);

            RefreshHexData(false);

            yield return Timing.WaitUntilDone( nodeParentTransform.DOLocalMove(originalMarkerPosition, 1.5f)
                .SetEase(Ease.InQuart)
                .OnComplete(() =>
                {
                    RefreshHexData(false);
                    Push(-0.1f);
                    AudioController.PlaySound("map_tile_wobble", "BARDSFX", true, gameObject);
                    AudioController.PlaySound("map_land", "BARDSFX", true, gameObject);
                }).WaitForCompletion(true));

            fallingTilePoofAnimator.SetActive(true);
            fallingTilePoofAnimator.Play("animate", () => fallingTilePoofAnimator.SetActive(false));
        }

        public IEnumerator<float> RevealTileRecursive(bool nodeFallsFromSky)
        {
            //ClientTileData.HasBeenShown = true;

            if (TileData.NodeType == NodeType.Path)
            {
                //Debug.Log($"path is getting revealed?? {name} = {ClientTileData.Complete}, {pathAnimator.IsActive()}");

                if (ClientTileData.Complete && !pathAnimator.IsActive())
                {
                    pathPoofAnimator.SetActive(true);
                    pathPoofAnimator.Play("spawn", () => pathPoofAnimator.SetActive(false));

                    while (pathPoofAnimator.CurrentFrame < 3) yield return Timing.WaitForOneFrame;

                    RefreshHexData(false);
                }
            }
            else if (TileData.NodeType != NodeType.None && nodeFallsFromSky)
            {
                // var originalMarkerPosition = nodeParentTransform.localPosition;
                // nodeParentTransform.localPosition = new Vector3(originalMarkerPosition.x, originalMarkerPosition.y + 20f, originalMarkerPosition.z);
                //
                // RefreshHexData(false);
                //
                // nodeParentTransform.DOLocalMove(originalMarkerPosition, 0.5f)
                //     .SetEase(Ease.Linear)
                //     .OnComplete(() =>
                //     {
                //         Push(-0.1f);
                //     });
            }

            if (TileData.NodeType != NodeType.None )
            {
                yield return Timing.WaitForSeconds(RevealDelay);

                var neighbors = HexGrid2D.GetNeighborsTile2D(TileData.hex);

                for (var i = 0; i < neighbors.Count; i++)
                {
                    if (neighbors[i].TileData.NodeType != NodeType.None && neighbors[i].ClientTileData != null)
                    {
                        Timing.RunCoroutine(neighbors[i].RevealTileRecursive(true));
                    }
                }
            }
        }

        public void OnHeroStartWalkingTowards()
        {
            if (TileData.NodeType != NodeType.None && markerAnimator.IsActive())
            {
                markerAnimator.renderer.DOKill();
                markerShadowAnimator.renderer.DOKill();
                notificationAnimator.renderer.DOKill();

                markerAnimator.renderer.DOFade(0, 0.12f);
                markerShadowAnimator.renderer.DOFade(0, 0.12f);
                if(_hasNotification) notificationAnimator.renderer.DOFade(0, 0.12f);
            }
        }

        public void OnHeroStartWalkingAway()
        {
            if (TileData.NodeType != NodeType.None && markerAnimator.IsActive())
            {
                markerAnimator.renderer.DOKill();
                markerShadowAnimator.renderer.DOKill();
                notificationAnimator.renderer.DOKill();

                markerAnimator.renderer.DOFade(1, 0.12f);
                markerShadowAnimator.renderer.DOFade(.4f, 0.12f);
                if (_hasNotification) notificationAnimator.renderer.DOFade(1, 0.12f);
            }
        }

        public static bool NeedsPathAccess(HexTile2D from, HexTile2D to)
        {
            if (to == null || to.m_tileData == null || from == null || from.m_tileData == null)
            {
                return false;
            }

            if (from.TileData.connectingPathRooms.Contains(to.TileData.uid))
            {
                if (!string.IsNullOrEmpty(to.TileData.questId) && !to.IsQuestShowing(out var sd) )
                {
                    return false;
                }

                if (to.TileData.IsValidNodeType())
                {
                    if (string.IsNullOrEmpty(to.TileData.questId))
                    {
                        return true;
                    }

                    if (to.IsQuestShowing(out var isSideQuest))
                    {
                        return true;
                    }

                    return false;
                }

                var trackingNodes = new Queue<FableTileData>();
                trackingNodes.Enqueue(to.TileData);
                var alreadyChecked = new List<int> { from.TileData.uid, to.TileData.uid };
                var questNodes = new List<HexTile2D>();

                while (trackingNodes.Count > 0)
                {
                    var trackingNode = trackingNodes.Dequeue();

                    for (var j = 0; j < trackingNode.connectingPathRooms.Count; j++)
                    {
                        if (alreadyChecked.Contains(trackingNode.connectingPathRooms[j]))
                        {
                            continue;
                        }

                        var node = Db.MapDatabase.GetTile(trackingNode.connectingPathRooms[j]);

                        if (node.IsValidNodeType())
                        {
                            questNodes.Add( HexGrid2D.GetTile(node.uid) );
                        }
                        else
                        {
                            // var clientNode = App.FableData.GetTile(node.uid);
                            //
                            // if (clientNode.Complete)
                            // {
                            //     trackingNodes.Enqueue(node);
                            // }

                            trackingNodes.Enqueue(node);
                        }
                    }

                    alreadyChecked.Add(trackingNode.uid);
                }

                var numValidNodes = questNodes.Count <= 0 ? 1 : 0;

                for (var j = 0; j < questNodes.Count; j++)
                {
                    if (string.IsNullOrEmpty(questNodes[j].TileData.questId))
                    {
                        numValidNodes++;
                        continue;
                    }

                    if (questNodes[j].IsQuestShowing(out var isSideQuest))
                    {
                        numValidNodes++;
                    }
                }

                return numValidNodes >= 1;
            }

            return false;
        }

        private static bool NeedsLadder(HexTile2D from, HexTile2D to, out int heightDiff)
        {
            heightDiff = 0;
            if (!NeedsPathAccess(from, to)) return false;
            heightDiff = to.m_tileData.Height - from.m_tileData.Height;
            return heightDiff >= MinimumHeightForLadder;
        }
    }
}