using UnityEngine;

namespace CrossBlitz.Fables.Grid
{
    public class SpringTileEffect
    {
        public float TargetHeight;
        public float Height;
        public float Speed;

        private Transform _transform;

        public SpringTileEffect(Transform transform, float targetHeight)
        {
            _transform = transform;
            TargetHeight = targetHeight;
        }

        public void Update(float dampening, float tension, float height)
        {
            float x = height - Height;
            Speed += tension * x - Speed * dampening;
            Height += Speed;

            _transform.localPosition = new Vector3(0, Height, 0);
        }
    }
}