using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Databases;
using CrossBlitz.Developer.D90;
using CrossBlitz.Fables.Data;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using CrossBlitz.UI.Widgets;
using CrossBlitz.UI.Widgets.Dropdown;
using DG.Tweening;
using float_oat.Desktop90;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Fables.Grid
{
    public class GridEditorToolbox : MonoBehaviour
    {
        public DropdownContainer universalToolItemContainer;

        public DropdownBox tilePlacementDropBox;
        public DropdownBox nodePlacementDropBox;
        public DropdownBox decorationPlacementDropBox;
        public DropdownBox npcPlacementDropBox;

        public ToggleButton tileHeightButton;
        public Button nodeEditorButton;

        public Button pathButton;
        public Button addPathButton;
        public Button removePathButton;

        public CanvasGroup instructionsCanvas;
        public Text currentToolTitleLabel;
        public Text currentToolSubTitleLabel;
        public Text controlsLabel;

        public Button settingsButton;

        public D90TileEditorWindow tileEditorWindow;
        public WindowController mapSettingsWindow;
        public WindowController pathSettingsWindow;

        public GameObject decorationEditor;
        public RectTransform decorationButtonContainer;
        public DecorationButton decorationButtonPrefab;
        public D90IntegerInput decorationXOffset;
        public D90IntegerInput decorationYOffset;
        public Button saveDecorationData;

        [BoxGroup("Tile Metadata")] public GameObject tileMetadata;
        [BoxGroup("Tile Metadata")] public SpriteAnimationAsset pathAnimator;
        [BoxGroup("Tile Metadata")] public SpriteAnimationAsset npcAnimator;
        [BoxGroup("Tile Metadata")] public Dropdown sceneEnvironment;
        [BoxGroup("Tile Metadata")] public Dropdown pathType;
        [BoxGroup("Tile Metadata")] public Dropdown heroSpriteOverride;
        [BoxGroup("Tile Metadata")] public Button saveTileMetadata;

        public event Action<GridEditorToolSettings> OnToolChanged;
        public event Action OnExploreMode;

        public Dictionary<string, List<string>> decorationDictionary;


        public void Start()
        {
            universalToolItemContainer.ItemSelected += OnToolItemSelected;
            tilePlacementDropBox.OnClickedDropdown += OnTilePlacementClicked;
            nodePlacementDropBox.OnClickedDropdown += OnNodePlacementClicked;
            decorationPlacementDropBox.OnClickedDropdown += OnDecorationPlacementClicked;
            npcPlacementDropBox.OnClickedDropdown += OnNpcPlacementClicked;
            tileHeightButton.OnValueChanged += OnTileHeight;

            pathButton.onClick.AddListener(OnPathButton);
            addPathButton.onClick.AddListener(OnAddPathButton);
            removePathButton.onClick.AddListener(OnDeletePathButton);

            nodeEditorButton.onClick.RemoveAllListeners();
            nodeEditorButton.onClick.AddListener(OnNodeEditorButton);

            settingsButton.onClick.RemoveAllListeners();
            settingsButton.onClick.AddListener(OnSettingsButton);

            saveDecorationData.onClick.RemoveAllListeners();
            saveDecorationData.onClick.AddListener(OnSaveDecorationData);

            saveTileMetadata.onClick.RemoveAllListeners();
            saveTileMetadata.onClick.AddListener(OnSaveDecorationData);

            decorationXOffset.OnChanged -= OnDecorationOffsetXChanged;
            decorationYOffset.OnChanged -= OnDecorationOffsetYChanged;

            decorationXOffset.OnChanged += OnDecorationOffsetXChanged;
            decorationYOffset.OnChanged += OnDecorationOffsetYChanged;

            sceneEnvironment.ClearOptions();
            sceneEnvironment.AddOptions(Enum.GetNames(typeof(SceneEnvironment)).ToList());
            sceneEnvironment.onValueChanged.RemoveAllListeners();
            sceneEnvironment.onValueChanged.AddListener(OnTileSceneEnvironmentChanged);

            var uniquePathNames = new List<string>();
            var pathValues = pathAnimator.animations.Select(anim => anim.name).ToList();

            for (var i = 0; i < pathValues.Count; i++)
            {
                var lastIndexOfSplit = pathValues[i].LastIndexOf('-');
                var currName = pathValues[i].Remove(lastIndexOfSplit, pathValues[i].Length-lastIndexOfSplit);

                if (!uniquePathNames.Contains(currName))
                {
                    uniquePathNames.Add(currName);
                }
            }

            pathValues = uniquePathNames;

            pathType.ClearOptions();
            pathType.AddOptions(pathValues);
            pathType.onValueChanged.RemoveAllListeners();
            pathType.onValueChanged.AddListener(OnPathTypeChanged);

            var uniqueNames = new List<string> { "None" };
            var values = npcAnimator.animations.Select(anim => anim.name).ToList();

            for (var i = 0; i < values.Count; i++)
            {
                var currName = values[i].Split('-')[0];

                if (!uniqueNames.Contains(currName))
                {
                    uniqueNames.Add(currName);
                }
            }

            values = uniqueNames;

            heroSpriteOverride.ClearOptions();
            heroSpriteOverride.AddOptions(values);
            heroSpriteOverride.onValueChanged.RemoveAllListeners();
            heroSpriteOverride.onValueChanged.AddListener(OnHeroSpriteOverrideChanged);


            decorationDictionary = new Dictionary<string, List<String>>();

            var uniqueDecorationPrefix = new List<string>();
            var decorationNames = new List<string>();
            var decorationValues = decorationPlacementDropBox.assetProperty.animations.Select(anim => anim.name).ToList();
            var endOfPrefixSet = false;
            var prevPrefix =  decorationValues[0].Split('-')[0];
            var currPrefix =  decorationValues[0].Split('-')[0];

            for (var i = 0; i < decorationValues.Count; i++)
            {
                // set the previous and current prefixes
                prevPrefix = currPrefix;
                currPrefix = decorationValues[i].Split('-')[0];

                // check to see if at a new prefix, and if so, add the built up list of decorations that use the prefix to the dictionary of decorations
                if (prevPrefix != currPrefix) endOfPrefixSet = true;
                else endOfPrefixSet = false;
                
                // If at the end of a prefix set, add it to the dictionary
                if (endOfPrefixSet)
                {
                    // Check for duplicate Key
                    if (decorationDictionary.ContainsKey(prevPrefix))
                    {
                        Debug.Log("DUPLICATE PREFIX: " + prevPrefix);
                        var newList = decorationDictionary[prevPrefix];
                        newList.AddRange(decorationNames);
                    }
                    else
                    {
                        Debug.Log("NEW PREFIX: " + prevPrefix);
                        var newList = new List<string>(decorationNames);
                        decorationDictionary.Add(prevPrefix, newList);
                    }

                    // reset the decoration Name list for the next prefix
                    decorationNames.Clear();
                }

                // Start adding new names to the decoration list
                decorationNames.Add(decorationValues[i]);

                // Add the current prefix to the unique prefix list if applicable
                if (!uniqueDecorationPrefix.Contains(currPrefix))
                {
                    uniqueDecorationPrefix.Add(currPrefix);
                }
            }

            // Add the Final List
            if (decorationDictionary.ContainsKey(currPrefix))
            {
                Debug.Log("DUPLICATE PREFIX: " + currPrefix);
                var lastList = decorationDictionary[currPrefix];
                lastList.AddRange(decorationNames);
            }
            else
            {
                Debug.Log("NEW PREFIX: " + currPrefix);
                var lastList = new List<string>(decorationNames);
                decorationDictionary.Add(currPrefix, lastList);
            }

            decorationNames.Clear();

            var decorationToggleGroup = decorationButtonContainer.GetComponent<ToggleGroup>();
            foreach (var decorationname in uniqueDecorationPrefix)
            {
                var newButton = Instantiate(decorationButtonPrefab, decorationButtonContainer);
                newButton.SetupButton(decorationname, this);
            }

            instructionsCanvas.alpha = 0;
            this.RectTransform().anchoredPosition = new Vector2(this.RectTransform().sizeDelta.x,0);
        }



        private void Update()
        {
            decorationEditor.SetActive(HexGridEditor.CurrentSettings.Tool == GridEditorTool.DecorationEditor);
            decorationButtonContainer.SetActive(HexGridEditor.CurrentSettings.Tool == GridEditorTool.DecorationEditor);
            pathSettingsWindow.SetActive(HexGridEditor.CurrentSettings.Tool == GridEditorTool.PathEditor);
            mapSettingsWindow.SetActive(HexGridEditor.CurrentSettings.Tool == GridEditorTool.Settings);
            tileEditorWindow.SetActive(HexGridEditor.CurrentSettings.Tool == GridEditorTool.NodeEditor);
            tileMetadata.SetActive(HexGridEditor.CurrentSettings.Tool == GridEditorTool.TilePlacement);

            pathButton.interactable = !pathSettingsWindow.IsActive();
            nodeEditorButton.interactable = !tileEditorWindow.IsActive();
        }

        private void OnSettingsButton()
        {
            universalToolItemContainer.RemoveContents();

            HexGridEditor.CurrentSettings.Tool = GridEditorTool.Settings;
            OnToolChanged?.Invoke(HexGridEditor.CurrentSettings);

            UpdateInstructions();
        }

        private void OnTilePlacementClicked(DropdownBox box)
        {
            tilePlacementDropBox.SetContents();

            HexGridEditor.CurrentSettings.Tool = GridEditorTool.TilePlacement;
            OnToolChanged?.Invoke(HexGridEditor.CurrentSettings);

            UpdateInstructions();
        }

        private void OnNodePlacementClicked(DropdownBox box)
        {
            nodePlacementDropBox.SetContents();

            HexGridEditor.CurrentSettings.Tool = GridEditorTool.NodePlacement;
            OnToolChanged?.Invoke(HexGridEditor.CurrentSettings);

            UpdateInstructions();
        }

        private void OnDecorationPlacementClicked(DropdownBox box)
        {
            decorationPlacementDropBox.itemsToLeaveOut.Clear();
            decorationPlacementDropBox.SetContents();

            HexGridEditor.CurrentSettings.Tool = GridEditorTool.DecorationEditor;

            if (string.IsNullOrEmpty(HexGridEditor.CurrentSettings.DecorationType))
            {
                HexGridEditor.CurrentSettings.DecorationType = "grassland-bush-1";
            }

            var decorationData = Db.FablesDatabase.GetDecorationData(HexGridEditor.CurrentSettings.DecorationType);
            decorationXOffset.SetTextWithoutNotify(decorationData.PixelOffset.x.ToString());
            decorationYOffset.SetTextWithoutNotify(decorationData.PixelOffset.y.ToString());

            OnToolChanged?.Invoke(HexGridEditor.CurrentSettings);

            UpdateInstructions();
        }

        public void OnRefreshDecorationList(string buttonName)
        {
            Debug.Log(buttonName);

            decorationPlacementDropBox.itemsToLeaveOut.Clear();

            var allOtherKeyValues = decorationDictionary.Where(kv => kv.Key != buttonName);
            var allOtherDecorations = new List<string>();

            foreach (var keyValues in allOtherKeyValues)
            {
                allOtherDecorations.AddRange(decorationDictionary[keyValues.Key]);
            }
            foreach (var decorations in allOtherDecorations)
            {
                decorationPlacementDropBox.itemsToLeaveOut.Add(decorations);
            }
            decorationPlacementDropBox.RefreshDropBox();
            decorationPlacementDropBox.SetContents();

            HexGridEditor.CurrentSettings.DecorationType = decorationDictionary[buttonName][0];

            var decorationData = Db.FablesDatabase.GetDecorationData(HexGridEditor.CurrentSettings.DecorationType);
            decorationXOffset.SetTextWithoutNotify(decorationData.PixelOffset.x.ToString());
            decorationYOffset.SetTextWithoutNotify(decorationData.PixelOffset.y.ToString());

        }

        private void OnDecorationOffsetXChanged(int value)
        {
            var decorationData = Db.FablesDatabase.GetDecorationData(HexGridEditor.CurrentSettings.DecorationType);
            decorationData.PixelOffset.x = decorationXOffset.Value;

            for (var i = 0; i < HexGrid2D.Map.Count; i++)
            {
                var tile = HexGrid2D.Map[i].TileData;

                if (tile != null && !string.IsNullOrEmpty(tile.Decoration) && tile.Decoration == HexGridEditor.CurrentSettings.DecorationType)
                {
                    HexGrid2D.Map[i].UpdateDecoration();
                }
            }
        }

        private void OnDecorationOffsetYChanged(int value)
        {
            var decorationData = Db.FablesDatabase.GetDecorationData(HexGridEditor.CurrentSettings.DecorationType);
            decorationData.PixelOffset.y = decorationYOffset.Value;

            for (var i = 0; i < HexGrid2D.Map.Count; i++)
            {
                var tile = HexGrid2D.Map[i].TileData;

                if (tile != null && !string.IsNullOrEmpty(tile.Decoration) && tile.Decoration == HexGridEditor.CurrentSettings.DecorationType)
                {
                    HexGrid2D.Map[i].UpdateDecoration();
                }
            }
        }

        private void OnSaveDecorationData()
        {
            if (!string.IsNullOrEmpty(HexGridEditor.CurrentSettings.DecorationType))
            {
                var decorationData = Db.FablesDatabase.GetDecorationData(HexGridEditor.CurrentSettings.DecorationType);
                decorationData.PixelOffset.x = decorationXOffset.Value;
                decorationData.PixelOffset.y = decorationYOffset.Value;
            }

            Db.MapDatabase.Save();
            Db.FablesDatabase.Save();

        }

        private void OnNpcPlacementClicked(DropdownBox box)
        {
            npcPlacementDropBox.SetContents();

            HexGridEditor.CurrentSettings.Tool = GridEditorTool.NpcEditor;

            if (string.IsNullOrEmpty(HexGridEditor.CurrentSettings.NpcType))
            {
                HexGridEditor.CurrentSettings.NpcType = "redcroft";
            }

            OnToolChanged?.Invoke(HexGridEditor.CurrentSettings);

            UpdateInstructions();
        }

        private void OnToolItemSelected(DropdownItemData data)
        {
            switch (HexGridEditor.CurrentSettings.Tool)
            {
                case GridEditorTool.TilePlacement:
                    OnTilePlacementSelected(data);
                    break;
                case GridEditorTool.NodePlacement:
                    OnNodePlacementSelected(data);
                    break;
                case GridEditorTool.DecorationEditor:
                    OnDecorationSelected(data);
                    break;
                case GridEditorTool.NpcEditor:
                    OnNpcSelected(data);
                    break;
            }
        }

        private void OnTilePlacementSelected(DropdownItemData data)
        {
            var tileType = data.name;
            HexGridEditor.CurrentSettings.Tool = GridEditorTool.TilePlacement;
            HexGridEditor.CurrentSettings.TileType = tileType;
            OnToolChanged?.Invoke(HexGridEditor.CurrentSettings);

            Debug.Log($"Getting tile type data of {tileType}");

            var tileTypeData = Db.FablesDatabase.GetTileTypeData(tileType);

            var index = sceneEnvironment.options.FindIndex(op => op.text == tileTypeData.SceneEnvironment.ToString());
            if (index < 0) index = 0;
            sceneEnvironment.SetValueWithoutNotify(index);

            index = pathType.options.FindIndex(op => op.text == tileTypeData.PathType);
            if (index < 0) index = 0;
            pathType.SetValueWithoutNotify(index);

            index = heroSpriteOverride.options.FindIndex(op => op.text == tileTypeData.HeroSpriteOverride);
            if (index < 0) index = 0;
            heroSpriteOverride.SetValueWithoutNotify(index);

            UpdateInstructions();
        }

        private void OnTileSceneEnvironmentChanged(int index)
        {
            var tileTypeData = Db.FablesDatabase.GetTileTypeData(HexGridEditor.CurrentSettings.TileType);

            if (tileTypeData != null)
            {
                tileTypeData.SceneEnvironment = GameUtilities.ParseEnum(sceneEnvironment.options[index].text, SceneEnvironment.Grass);
            }
        }

        private void OnPathTypeChanged(int index)
        {
            var tileTypeData = Db.FablesDatabase.GetTileTypeData(HexGridEditor.CurrentSettings.TileType);

            if (tileTypeData != null)
            {
                tileTypeData.PathType = pathType.options[index].text;
            }
            else
            {
                Debug.LogError($"Tile Type {HexGridEditor.CurrentSettings.TileType} does not exist.");
            }
        }

        private void OnHeroSpriteOverrideChanged(int index)
        {
            var tileTypeData = Db.FablesDatabase.GetTileTypeData(HexGridEditor.CurrentSettings.TileType);

            if (tileTypeData != null)
            {
                if (index == 0) tileTypeData.HeroSpriteOverride = string.Empty;
                else tileTypeData.HeroSpriteOverride = heroSpriteOverride.options[index].text;
            }
            else
            {
                Debug.LogError($"Tile Type {HexGridEditor.CurrentSettings.TileType} does not exist.");
            }
        }

        private void OnNodePlacementSelected(DropdownItemData data)
        {
            var nodeType = GameUtilities.ParseEnum(data.name, NodeType.Battle);
            HexGridEditor.CurrentSettings.Tool = GridEditorTool.NodePlacement;
            HexGridEditor.CurrentSettings.NodeType = nodeType;
            OnToolChanged?.Invoke(HexGridEditor.CurrentSettings);

            UpdateInstructions();
        }

        private void OnDecorationSelected(DropdownItemData data)
        {
            HexGridEditor.CurrentSettings.Tool = GridEditorTool.DecorationEditor;
            HexGridEditor.CurrentSettings.DecorationType = data.name;

            var decorationData = Db.FablesDatabase.GetDecorationData(HexGridEditor.CurrentSettings.DecorationType);
            decorationXOffset.SetTextWithoutNotify(decorationData.PixelOffset.x.ToString());
            decorationYOffset.SetTextWithoutNotify(decorationData.PixelOffset.y.ToString());

            OnToolChanged?.Invoke(HexGridEditor.CurrentSettings);

            UpdateInstructions();
        }

        private void OnNpcSelected(DropdownItemData data)
        {
            HexGridEditor.CurrentSettings.Tool = GridEditorTool.NpcEditor;
            HexGridEditor.CurrentSettings.NpcType = data.name;
            OnToolChanged?.Invoke(HexGridEditor.CurrentSettings);

            UpdateInstructions();
        }

        private void OnTileHeight(bool isOn, string content)
        {
            if (!isOn) return;

            universalToolItemContainer.RemoveContents();

            HexGridEditor.CurrentSettings.Tool = GridEditorTool.TileHeightModifier;
            OnToolChanged?.Invoke(HexGridEditor.CurrentSettings);
            UpdateInstructions();
        }

        private void OnNodeEditorButton()
        {
            tileEditorWindow.Open(0);
            universalToolItemContainer.RemoveContents();
            HexGridEditor.CurrentSettings.Tool = GridEditorTool.NodeEditor;
            OnToolChanged?.Invoke(HexGridEditor.CurrentSettings);
            UpdateInstructions();
        }

        private void OnPathButton()
        {
            if (HexGridEditor.CurrentSettings.Tool == GridEditorTool.PathEditor) return;

            pathSettingsWindow.Open();
            universalToolItemContainer.RemoveContents();
            OnAddPathButton();
        }

        private void OnAddPathButton()
        {
            addPathButton.interactable = false;
            removePathButton.interactable = true;

            HexGridEditor.CurrentSettings.Tool = GridEditorTool.PathEditor;
            HexGridEditor.CurrentSettings.PathMode = PathMode.Select;
            HexGridEditor.CurrentSettings.CurrentPathTile = 0;

            OnToolChanged?.Invoke(HexGridEditor.CurrentSettings);
            UpdateInstructions();
        }

        private void OnDeletePathButton()
        {
            addPathButton.interactable = true;
            removePathButton.interactable = false;

            HexGridEditor.CurrentSettings.Tool = GridEditorTool.PathEditor;
            HexGridEditor.CurrentSettings.PathMode = PathMode.Delete;
            HexGridEditor.CurrentSettings.CurrentPathTile = 0;

            OnToolChanged?.Invoke(HexGridEditor.CurrentSettings);
            UpdateInstructions();
        }

        private void ExploreMode()
        {
            OnExploreMode?.Invoke();
        }

        public void OpenMenu()
        {
            instructionsCanvas.DOFade(1, 0.25f);
            universalToolItemContainer.SetActive(true);
            this.RectTransform().DOAnchorPosX(0, 0.25f).SetEase(Ease.OutBack);
            OnTilePlacementClicked(null);
        }

        public void CloseMenu()
        {
            instructionsCanvas.DOFade(0, 0.25f);
            universalToolItemContainer.SetActive(false);
            this.RectTransform().DOAnchorPosX(this.RectTransform().sizeDelta.x, 0.25f)
               .SetEase(Ease.InBack);
        }

        private void Save()
        {
            FablesMapController.Instance.SaveChapter();
        }

        public void UpdateInstructions()
        {
            if (HexGridEditor.CurrentSettings.Tool == GridEditorTool.TilePlacement)
            {
                currentToolTitleLabel.text = "Terrain Stamp";
                currentToolSubTitleLabel.text = "Place different terrain types on the map.";
                controlsLabel.text = "Place Tile: <color=#F6D77B>Left-Click</color>\n" +
                                     "Remove Tile: <color=#F6D77B>Shift + Left-Click</color>\n";
            }
            else if (HexGridEditor.CurrentSettings.Tool == GridEditorTool.TileHeightModifier)
            {
                currentToolTitleLabel.text = "Tile Height Editor";
                currentToolSubTitleLabel.text = "Increase or decrease the height of a tile.";
                controlsLabel.text = "Raise: <color=#F6D77B>Left-Click</color>\n" +
                                     "Lower: <color=#F6D77B>Right-Click</color>\n";
            }
            else if (HexGridEditor.CurrentSettings.Tool == GridEditorTool.NodePlacement)
            {
                currentToolTitleLabel.text = "Node Stamp";
                currentToolSubTitleLabel.text = "Add nodes to the tiles, these will need to be edited to take actions.";
                controlsLabel.text = "Place Node: <color=#F6D77B>Left-Click</color>\n" +
                                     "Remove Node: <color=#F6D77B>Shift + Left-Click</color>\n";
            }
            else if (HexGridEditor.CurrentSettings.Tool == GridEditorTool.NodeEditor)
            {
                currentToolTitleLabel.text = "Node Editor";
                currentToolSubTitleLabel.text = "Edit node properties to add or remove various things that happen when on the node.";
                controlsLabel.text = "Open Node Settings: <color=#F6D77B>Left-Click Node</color>";
            }
            else if (HexGridEditor.CurrentSettings.Tool == GridEditorTool.DecorationEditor)
            {
                currentToolTitleLabel.text = "Decoration Stamp";
                currentToolSubTitleLabel.text = "Add decorations to the map.";
                controlsLabel.text = "Place Decoration: <color=#F6D77B>Left-Click</color>\n" +
                                     "Remove Decoration: <color=#F6D77B>Shift + Left-Click</color>\n";
            }
            else if (HexGridEditor.CurrentSettings.Tool == GridEditorTool.NpcEditor)
            {
                currentToolTitleLabel.text = "Npc Stamp";
                currentToolSubTitleLabel.text = "Add npcs to the map.";
                controlsLabel.text = "Place Npc: <color=#F6D77B>Left-Click</color>\n" +
                                     "Remove Npc: <color=#F6D77B>Shift + Left-Click</color>\n";
            }
            else if (HexGridEditor.CurrentSettings.Tool == GridEditorTool.PathEditor)
            {
                currentToolTitleLabel.text = "Path Editor";
                currentToolSubTitleLabel.text = "Add and remove paths.";
                controlsLabel.text = "Select a tile, then tiles you want to path to.\n" +
                                     "Delete paths by selecting a tile with a path.";
            }
            else if (HexGridEditor.CurrentSettings.Tool == GridEditorTool.PathEditor)
            {
                currentToolTitleLabel.text = "Settings";
                currentToolSubTitleLabel.text = "Edit & save the settings of the map.";
                controlsLabel.text = "Edit & save the settings of the map.";
            }
        }
    }
}