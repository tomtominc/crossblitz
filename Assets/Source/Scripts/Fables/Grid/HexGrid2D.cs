using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Audio;
using CrossBlitz.Fables.Data;
using CrossBlitz.InputAPI;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.Transition;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.Fables.Grid
{
    public class HexGrid2D : MonoBehaviour
    {
        public const int Columns = 8;
        public const int Rows = 8;
        public static HexGrid2D Instance;
        public HexTile2D tilePrefab;
        public static Layout TileLayout;
        public static List<HexTile2D> Map;
        public Camera main;
        public static bool inputEnabled=true;

        public event Action OnMapFinishedBuilding;
        public event Action OnMapFinishedExiting;
        public event Action OnMapPreGeneration;

        public enum GenerateMapTransition
        {
            Instant,
            Animate_Blocks,
            Fade
        }


        private void Awake()
        {
            Instance = this;
        }

        private void OnDestroy()
        {
            Instance = null;
            Map.Clear();
            Map = null;
            main = null;
        }

        private void Start()
        {
            SceneController.OnSceneLoaded += OnSceneLoaded;
        }

        private void OnSceneLoaded(SceneModel model)
        {
            SceneController.OnSceneLoaded -= OnSceneLoaded;
            main = model.sceneCamera;
        }

        public static void OnPlayerEntered()
        {
            inputEnabled = true;
        }

        public IEnumerator<float> GenerateMap(FableRoomData roomData, GenerateMapTransition transition, bool saveInBetween)
        {
            //Debug.LogError("Generating map!!!");

            //if (!saveInBetween) yield return Timing.WaitUntilDone(TransitionController.TransitionOut(TransitionController.TransitionType.Hex));
            if (!saveInBetween) yield return Timing.WaitUntilDone(TransitionController.TransitionOut(TransitionController.TransitionType.Fade));
            const int mapRadius = 5;

            if (FablesInput.Instance.currentMode != FablesInput.Mode.Editor)
            {
                inputEnabled = false;
            }

            if (transition == GenerateMapTransition.Animate_Blocks)
            {
                if (Map != null && Map.Count > 0)
                {
                    yield return Timing.WaitUntilDone(AnimateMapOut());
                    yield return Timing.WaitForSeconds(0.24f);

                    OnMapFinishedExiting?.Invoke();
                }
            }
            else if (Map != null && Map.Count > 0)
            {
                OnMapFinishedExiting?.Invoke();
            }

            if (Map != null && Map.Count > 0)
            {
                yield return Timing.WaitForSeconds(.6f);
            }

            transform.DestroyChildren(true);

            var size = new Point(HexTile2D.Width, HexTile2D.Height);

            TileLayout = new Layout(Layout.flat, size, new Point(0,0));

            Map = new List<HexTile2D>();

            var map = roomData.Tiles ?? new List<FableTileData>();

            for (var q = -mapRadius; q <= mapRadius; q++)
            {
                var r1 = Mathf.Max(-mapRadius, -q - mapRadius);
                var r2 = Mathf.Min(mapRadius, -q + mapRadius);

                for (var r = r1; r <= r2; r++)
                {
                    var s = -q - r;
                    var hexData = map.Find(tile => tile.hex.q == q && tile.hex.r == r && tile.hex.s == s);
                    if (hexData == null)
                    {
                        hexData = new FableTileData();
                        hexData.Init();
                        hexData.tileType = TileTypeData.Default;
                        hexData.hex = new Hex(q, r, s);
                        map.Add(hexData);
                    }
                }
            }

            roomData.Tiles = map;

            for (var i = 0; i < roomData.Tiles.Count; i++)
            {
                var point = TileLayout.HexToPixel(roomData.Tiles[i].hex);
                var tile = Instantiate(tilePrefab, transform);
                var x = RoundToPixel((float) point.x);
                var y = RoundToPixel((float) point.y);
                tile.name = $"Tile[{i}]";
                tile.Position = new Vector2(x, y);
                tile.originalSortingOrder =(int) (-point.y*100f);
                tile.SortingOrder = tile.originalSortingOrder;
                tile.SetTileIndex(i);
                tile.SetHexData(roomData.Tiles[i], false, false);
                Map.Add(tile);
            }

            // generate map rules

            OnMapPreGeneration?.Invoke();

            // set things before

            for (var i = 0; i < Map.Count; i++)
            {
                Map[i].SetHexData(roomData.Tiles[i], transition == GenerateMapTransition.Animate_Blocks);
            }

            if(saveInBetween)
            {
                HexTile2D enterNode = FablesMapController.Instance.GetStartingNode();
                FablesMapController.Instance.ClientChapterData.SetPlayerTile(enterNode.TileData.uid);
                //Debug.Log("SAVEALL()");
                App.SaveAll();
            }

            if (transition == GenerateMapTransition.Animate_Blocks)
            {
                yield return Timing.WaitUntilDone(AnimateMapIn());
            }
            else
            {
                for (var i = 0; i < Map.Count; i++)
                {
                    Map[i].RefreshHexData();
                }
            }

            OnMapFinishedBuilding?.Invoke();
        }

        private IEnumerator<float> AnimateMapIn()
        {
            // var tilesWithHeight = new List<HexTile2D>(Map);
            // tilesWithHeight.Shuffle();
            //
            // for (var i = 0; i < tilesWithHeight.Count; i++)
            // {
            //     tilesWithHeight[i].NextBlockIn();
            //
            //     if (tilesWithHeight[i].TileData != null && tilesWithHeight[i].TileData.NodeType != NodeType.None)
            //     {
            //         yield return Timing.WaitForSeconds(0.01f);
            //     }
            // }

            AudioController.PlaySound("MapTiles-Fall");

            var tilesWithHeight = new List<HexTile2D>(Map);

            while (tilesWithHeight.Count > 0)
            {
                var randCount = Random.Range(1, 5);

                for (var i = 0; i < randCount; i++)
                {
                    var rand = Random.Range(0, tilesWithHeight.Count);

                    if (rand >= tilesWithHeight.Count) break;

                    if (tilesWithHeight[rand].NextBlockIn())
                    {
                        tilesWithHeight.RemoveAt(rand);
                    }
                }

                yield return Timing.WaitForSeconds(0.01f);
            }
        }

        private IEnumerator<float> AnimateMapOut()
        {
            var tilesWithHeight = new List<HexTile2D>(Map);
            tilesWithHeight.Shuffle();

            for (var i = 0; i < tilesWithHeight.Count; i++)
            {
                tilesWithHeight[i].AnimateBlockOut();

                if (tilesWithHeight[i].TileData != null && tilesWithHeight[i].TileData.NodeType != NodeType.None)
                {
                    yield return Timing.WaitForSeconds(0.01f);
                }
            }
        }

        [Button(ButtonSizes.Medium, ButtonStyle.Box, Expanded = true)]
        public void GenerateTileShadows()
        {
            if (Map == null)
            {
                Map = GetComponentsInChildren<HexTile2D>().ToList();
            }

            for (var i = 0; i < Map.Count; i++)
            {
                Map[i].GenerateShadow();
            }
        }

        public static HexTile2D GetNeighborTile2D(Hex hex, int direction)
        {
            var neighborHex = hex.Neighbor(direction);
            return Map.Find(tile =>
                tile.TileData.hex.q == neighborHex.q &&
                tile.TileData.hex.r == neighborHex.r &&
                tile.TileData.hex.s == neighborHex.s);
        }

        public static List<FableTileData> GetNeighbors(Hex hex)
        {
            if (Map == null) return null;

            var neighbors = new List<FableTileData>();
            for (var i = 0; i < 6; i++)
            {
                var neighborHex = hex.Neighbor(i);
                var neighborTile = Map.Find(tile =>
                    tile.TileData.hex.q == neighborHex.q &&
                    tile.TileData.hex.r == neighborHex.r &&
                    tile.TileData.hex.s == neighborHex.s);

                if (neighborTile)
                {
                    neighbors.Add(neighborTile.TileData);
                }
            }

            return neighbors;
        }

        public static List<HexTile2D> GetNeighborsTile2D(Hex hex)
        {
            var neighbors = new List<HexTile2D>();
            for (var i = 0; i < 6; i++)
            {
                var neighborHex = hex.Neighbor(i);
                var neighborTile = Map.Find(tile =>
                    tile.TileData.hex.q == neighborHex.q &&
                    tile.TileData.hex.r == neighborHex.r &&
                    tile.TileData.hex.s == neighborHex.s);

                if (neighborTile)
                {
                    neighbors.Add(neighborTile);
                }
            }

            return neighbors;
        }

        private static float RoundToPixel(float n)
        {
            return RoundToNearest(n, HexTile2D.PixelSize);
        }

        public static List<HexTile2D> GetTilesAtDistance(Hex hex, int distance)
        {
            return Map.FindAll(tile => tile.TileData.hex.Distance(hex) == distance);
        }

        public static HexTile2D GetTile(int uid)
        {
            return Map.Find(tile => tile.TileData.uid == uid);
        }

        public static int GetDistance(Hex hex1, Hex hex2)
        {
            return hex1.Distance(hex2);
        }

        public static Vector2 GetDirection(HexTile2D hex1, HexTile2D hex2)
        {
            return (hex2.Position - hex1.Position).normalized;
        }

        private static float RoundToNearest(float n, float x)
        {
            return Mathf.Round(n / x) * x;
        }

        public static List<HexTile2D> GetTilesOfNodeType(NodeType nodeType)
        {
            if (Map == null) return new List<HexTile2D>();
            return Map.FindAll(tile => tile.TileData.NodeType == nodeType);
        }

        public static List<HexTile2D> GetTilesOfNodeTypes(params NodeType[] nodeTypes)
        {
            var tilesFound = new List<HexTile2D>();

            for (var i = 0; i < nodeTypes.Length; i++)
            {
                tilesFound.AddRange(GetTilesOfNodeType(nodeTypes[i]));
            }

            return tilesFound;
        }

        public static HexTile2D GetTileFromMousePosition()
        {
            // if (Instance.main == null)
            // {
            //     var sceneModel = SceneModel.GetCurrentSceneModel();
            //
            //     if (sceneModel)
            //     {
            //         Instance.main = sceneModel.sceneCamera;
            //     }
            //
            //     return null;
            // }

            var results = new List<RaycastHit2D>();
            var contactFilter = new ContactFilter2D();
            var hits =  Physics2D.Raycast(
                Instance.main.ScreenToWorldPoint(Input.mousePosition),
                Vector2.zero,contactFilter.NoFilter(), results);
            var sortOrder = -100000;
            HexTile2D returnTile=null;

            for (var i = 0; i < hits; i++)
            {
                var hit = results[i];

                if (hit && hit.transform)
                {
                    var tile = hit.transform.GetComponent<HexTile2DReference>();

                    if (tile.hexTile.sortingGroup.sortingOrder > sortOrder)
                    {
                        sortOrder = tile.hexTile.sortingGroup.sortingOrder;
                        returnTile = tile.hexTile;
                    }
                }
            }

            return returnTile;
        }
    }
}