using CrossBlitz.Databases;
using CrossBlitz.Fables.Data;
using CrossBlitz.PlayFab.Data;
using UnityEngine;

namespace CrossBlitz.Fables.Grid
{
    public class HexTileDisplayView : MonoBehaviour
    {
        public SpriteAnimation tileAnimator;
        public SpriteAnimation nodeAnimator;
        public SpriteAnimation markerAnimator;
        public SpriteAnimation npcAnimator;

        public void SetData(FableTileData tileData)
        {
            FableTileData.PlayTileAnimation(tileData.tileType,tileAnimator);

            nodeAnimator.SetActive(true);

            if (!nodeAnimator.Play(FableTileData.GetNodeAnimation(tileData)))
            {
                nodeAnimator.SetActive(false);
            }

            markerAnimator.SetActive(true);


            var markerAnim = FableTileData.GetMarkerAnimation(tileData,true);
            var useMarker = true;

            if (tileData.NodeType == NodeType.Battle)
            {
                npcAnimator.SetActive(true);
                markerAnimator.SetActive(false);
                useMarker = false;

                if (string.IsNullOrEmpty(markerAnim) || !npcAnimator.Play(markerAnim))
                {
                    npcAnimator.SetActive(false);
                    nodeAnimator.SetActive(false);
                    useMarker = true;
                }
            }

            if (useMarker)
            {
                markerAnim = FableTileData.GetMarkerAnimation(tileData);

                npcAnimator.SetActive(false);
                markerAnimator.SetActive(true);

                if (string.IsNullOrEmpty(markerAnim) || !markerAnimator.Play(markerAnim))
                {
                    markerAnimator.SetActive(false);
                    nodeAnimator.SetActive(false);
                }
            }
        }

        public void SetData(FableTileData tileData, ClientTileData clientTileData)
        {
            SetData(tileData);

            switch (tileData.NodeType)
            {
                case NodeType.Battle:
                case NodeType.Chest:
                case NodeType.Event1:
                case NodeType.Event2:
                    if (clientTileData != null && clientTileData.Complete)
                    {
                        markerAnimator.SetActive(false);
                    }
                    break;

            }

            // var battleData = Db.BattleDatabase.GetBattleData(tileData.battleUid);
            //
            // switch (tileData.NodeType)
            // {
            //     case NodeType.BossBattle when battleData != null && clientTileData != null && clientTileData.Complete:
            //     case NodeType.MultiBattle when battleData != null && clientTileData != null && clientTileData.Complete:
            //     case NodeType.Battle when battleData != null && clientTileData != null && clientTileData.Complete:
            //
            //         break;
            //     case NodeType.BossBattle:
            //     case NodeType.MultiBattle:
            //     case NodeType.Battle:
            //
            //         break;
            // }
        }
    }
}