using System;
using System.Collections.Generic;
using CrossBlitz.AddressablesAPI;
using Asyncoroutine;
using CrossBlitz.Audio;
using CrossBlitz.ClientAPI;
using CrossBlitz.ClientAPI.GameLogic.EventSystem;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Cutscene;
using CrossBlitz.Fables.Cutscene.Data;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.EventDialogueAPI;
using CrossBlitz.Fables.PathfindingAPI;
using CrossBlitz.Hero;
using CrossBlitz.InputAPI;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using CrossBlitz.Settings;
using CrossBlitz.ViewAPI.Popups;
using CrossBlitz.Transition;
using MEC;
using DG.Tweening;
using TakoBoyStudios.Events;
using UnityEngine;
using Events = CrossBlitz.ClientAPI.GameLogic.EventSystem.Events;
using EventType = CrossBlitz.ClientAPI.GameLogic.EventSystem.EventType;

namespace CrossBlitz.Fables.Grid
{
    public class FablesMapController : MonoBehaviour
    {
        public static FablesMapController Instance;

        public HexGrid2D grid;
        public FablesInput input;
        public GridPlayer playerPrefab;
        public HeroPanel heroPanel;
        public CanvasGroup epilogueBooks;

        [NonSerialized] public FableChapterData ChapterData;
        [NonSerialized] public int LastRoomIndex;
        [NonSerialized] public int CurrentRoomIndex;

        public ClientChapterData ClientChapterData => App.FableData.GetCurrentChapter();

        public FableMapData MapData => ClientChapterData != null
            ?
            Db.MapDatabase.GetMap(ClientChapterData.CurrentMapUid)
            : ChapterData != null
                ? Db.MapDatabase.GetMap(ChapterData.mapId)
                : null;

        private void Awake()
        {
            Instance = this;

            Events.Subscribe(EventType.OnPlayerEnteredMap, OnPlayerEnteredMap);
            Events.Subscribe(EventType.OnPlayerArrivedAtTile, OnPlayerArrivedAtTile);
            Events.Subscribe(EventType.OnPlayerInvokedTile, OnPlayerInvokedTile);
        }

        private void OnDestroy()
        {
            Events.Unsubscribe(EventType.OnPlayerEnteredMap, OnPlayerEnteredMap);
            Events.Unsubscribe(EventType.OnPlayerArrivedAtTile, OnPlayerArrivedAtTile);
            Events.Unsubscribe(EventType.OnPlayerInvokedTile, OnPlayerInvokedTile);
        }

        public void InitializeFable(FablesInput.Mode mode, FableChapterData chapterData)
        {
            // if (ClientChapterData == null)
            // {
            //     ClientChapterData = new ClientChapterData
            //         {ChapterUid = chapterData.Uid, ChapterNumber = 1, CurrentRoomIndex = 1};
            //
            // }
            // else
            if (string.IsNullOrEmpty(ClientChapterData.ChapterUid))
            {
                ClientChapterData.SetChapterUid( chapterData.Uid );
            }

            if (string.IsNullOrEmpty(ClientChapterData.CurrentMapUid))
            {
                var map = Db.MapDatabase.GetMap(chapterData.mapId);

                if (map?.Rooms == null || map.Rooms.Count <= 0)
                {
                    Debug.LogError("Something went wrong!");
                    return;
                }

                var room = map.Rooms[0];
                ClientChapterData.SetCurrentRoomAndSave(map.Uid, room.Uid, 0);
            }

            InitializeFable(mode, chapterData, ClientChapterData.CurrentRoomIndex);
        }

        public void InitializeFable(FablesInput.Mode mode, FableChapterData chapterData, int roomIndex)
        {
            ChapterData = chapterData;
            CurrentRoomIndex = roomIndex;
            LastRoomIndex = CurrentRoomIndex;

            input.GoToMode(mode);

            var heroData = Db.HeroDatabase.GetHero(ChapterData.ChapterHero);
            if (heroData != null)
            {
                heroPanel.SetHero(heroData);
                heroPanel.heroView.UpdateRelics();
            }
            else
            {
                Debug.LogError($"Hero is null! {ChapterData.ChapterHero}");
            }

            if (MapData.Rooms.Count <= CurrentRoomIndex)
            {
                for (var i = MapData.Rooms.Count; i <= CurrentRoomIndex; i++)
                {
                    var roomData = new FableRoomData();
                    roomData.UpdateIdentifier();
                    roomData.DisplayName = $"new room #{roomData.Uid}";
                    roomData.Tiles = new List<FableTileData>();

                    MapData.Rooms.Add(roomData);
                }
            }

            if (ClientChapterData != null)
            {
                ClientChapterData.Init(ChapterData);

                if (ClientChapterData.TileOccupiedByPlayerUid == 0)
                {
                    ClientChapterData.SetPlayerTile( GetSavedOrFirstTileUid() );
                }
            }

            App.PlayerDecks.EquipPvpDeckBasedOnGameState();
            OpenRoom(CurrentRoomIndex, HexGrid2D.GenerateMapTransition.Animate_Blocks, false);
        }

        public void OpenMap(string overrideMapUid, string roomUid, int tileUid)
        {
            if (ClientChapterData == null)
            {
                return;
            }

            ClientChapterData.SetCurrentMap( overrideMapUid );
            ClientChapterData.SetPlayerTile(tileUid);

            CurrentRoomIndex = MapData.GetRoomIndex(roomUid);

            OpenRoom(CurrentRoomIndex, HexGrid2D.GenerateMapTransition.Animate_Blocks);
        }

        public void UpdateOpenPaths()
        {
            grid.OnMapFinishedBuilding -= UpdateOpenPaths;

            if (ClientChapterData != null)
            {
                var playerTile = MapData.GetTile(ClientChapterData.TileOccupiedByPlayerUid);
                var targets = MapData.Rooms[CurrentRoomIndex].GetTilesOfNotOfNodeType(NodeType.None, NodeType.Path);

                for (var i = 0; i < targets.Count; i++)
                {
                    var path = Pathfinding.FindPath(playerTile, targets[i], true);

                    if (path == null)
                    {
                        continue;
                    }

                    for (var j = 0; j < path.Count; j++)
                    {
                        if (path[j].NodeType == NodeType.Path)
                        {
                            ClientChapterData.SetTileAsComplete(path[j].uid, false);
                        }
                    }
                }
            }
        }

        public FableRoomData GetCurrentRoom()
        {
            if (MapData?.Rooms?.Count <= 0) return null;
            if (CurrentRoomIndex < 0 || CurrentRoomIndex >= MapData?.Rooms?.Count) return null;

            return MapData?.Rooms?[CurrentRoomIndex];
        }

        private void OpenRoom(int roomIndex, HexGrid2D.GenerateMapTransition transition, bool saveOnOpen = true)
        {
            LastRoomIndex = CurrentRoomIndex;
            CurrentRoomIndex = roomIndex;

            ClientChapterData?.SetCurrentRoomAndSave(MapData.Uid, MapData.Rooms[CurrentRoomIndex].Uid,
                CurrentRoomIndex);

            grid.OnMapFinishedBuilding -= OnMapEntered;
            grid.OnMapFinishedBuilding += OnMapEntered;

            grid.OnMapPreGeneration -= UpdateOpenPaths;
            grid.OnMapPreGeneration += UpdateOpenPaths;

            Events.Publish(this, EventType.OnEnteredFableMap, new OnEnteredFableMapEventArgs
            {
                mapData = MapData,
                currentRoomIndex = CurrentRoomIndex,
                mode = input.currentMode
            });

            PlayMusicAndAmbient();

            Timing.RunCoroutine(grid.GenerateMap(MapData.Rooms[CurrentRoomIndex], transition, saveOnOpen));
        }

        private void OnMapEntered()
        {
            grid.OnMapFinishedBuilding -= OnMapEntered;

            // update the paths again--
            UpdateOpenPaths();

            switch (input.currentMode)
            {
                case FablesInput.Mode.Explore:
                    CreatePlayerAndEnter();
                    break;
            }

            if (App.FableData.CurrentChapterData != null)
            {
                App.FableData.CurrentChapterData.MarkAllPendingTilesAsComplete(GetCurrentRoom().Uid);
            }
        }

        public int GetSavedOrFirstTileUid()
        {
            // the logic below will figure out which node to enter on
            int savedOrFirstTileUid;

            // Debug.LogError($"Last = {LastRoomIndex} Current = {CurrentRoomIndex}");
            //Debug.LogError($"Current Map = {ClientChapterData.CurrentMapUid} - Room Id = {ClientChapterData.CurrentRoomUid} - Room Index = {CurrentRoomIndex}");

            if (LastRoomIndex == CurrentRoomIndex)
            {
                //1. get saved node first
                savedOrFirstTileUid = ClientChapterData.TileOccupiedByPlayerUid;

                //2. check if the tile that is saved is in the room we're in.
                if (savedOrFirstTileUid != 0)
                {
                    var room = MapData.GetRoomWithTile(savedOrFirstTileUid);

                    if (room.Uid != ClientChapterData.CurrentRoomUid)
                    {
                        savedOrFirstTileUid = 0;
                    }
                }

                //2. get the start node
                if (savedOrFirstTileUid == 0)
                {
                    var startNodes = MapData.GetTilesOfNodeType(NodeType.Start);

                    if (startNodes.Count >= 1)
                    {
                        savedOrFirstTileUid = startNodes[0].uid;
                    }
                }

                //3. get an exit node closest to the (0,0) origin room
                if (savedOrFirstTileUid == 0)
                {
                    savedOrFirstTileUid = GetEntranceNode(MapData.GetRoomAtPosition(Vector2Int.zero), GetCurrentRoom());
                }

                //4. PANIC
                if (savedOrFirstTileUid == 0)
                {
                    Debug.LogError("No Saved or first node found!");
                }
            }
            // entering from another room
            // (1) grab the direction you're entering from and find the node that I'm supposed to be on
            else
            {
                var lastRoom = MapData.Rooms[LastRoomIndex];
                var currentRoom = MapData.Rooms[CurrentRoomIndex];
                savedOrFirstTileUid = GetEntranceNode(lastRoom, currentRoom);
            }

            if (savedOrFirstTileUid == 0)
            {
                PopupController.Open(new PopupInfo
                {
                    style = PopupWindowStyle.Generic, title = "NO ENTER NODE",
                    body =
                        "Could not find a node to enter onto, no saved node found, no start node found and no enter/exit node found!",
                    confirm = "Oh Shit"
                });
            }

            return savedOrFirstTileUid;
        }

        public HexTile2D GetStartingNode()
        {
            // the logic below will figure out which node to enter on
            var enterNode = HexGrid2D.GetTile(GetSavedOrFirstTileUid());

            if (enterNode == null)
            {
                PopupController.Open(new PopupInfo
                {
                    style = PopupWindowStyle.Generic, title = "NO ENTER NODE",
                    body =
                        "Could not find a node to enter onto, no saved node found, no start node found and no enter/exit node found!",
                    confirm = "Oh Shit"
                });
                return null;
            }

            return enterNode;
        }

        private void CreatePlayerAndEnter()
        {
            if (GridPlayer.Instance)
            {
                Destroy(GridPlayer.Instance.gameObject);
            }

            // the logic below will figure out which node to enter on
            HexTile2D enterNode = GetStartingNode();
            ClientChapterData.SetPlayerTile(enterNode.TileData.uid);

            var currentPlayer = Instantiate(playerPrefab, enterNode.playerContainer, false);
            currentPlayer.SuperAwake();

            //Debug.LogError($"Enter Map: Tile ID = {enterNode.ClientTileData.TileUid}");

            if (ClientChapterData != null && App.BattleResults != null && App.BattleResults.GetPendingMapVisuals() && !string.IsNullOrEmpty(enterNode.ClientTileData.BattleData.BattleUid))
            {
                App.BattleResults.SetPendingMapVisuals(false);

               // Debug.LogError($"Battle Results = {App.BattleResults.MatchResult} - Tile ID = {enterNode.ClientTileData.TileUid}");

                if (App.BattleResults.GetMatchResult() == BattleResults.Result.Won)
                {
                    currentPlayer.EnterMapWonBattle(enterNode.TileData.GetEnterDirectionAsVector(), enterNode);

                    if (!string.IsNullOrEmpty(enterNode.TileData.outroCutsceneUid) && !ClientChapterData.HasSeenCutscene(enterNode.TileData.outroCutsceneUid))
                    {
                        ClientChapterData.SetTileAsComplete(enterNode.TileData.uid, true);
                        Timing.RunCoroutine(RunOutroCutsceneForBattle(enterNode.TileData));
                        enterNode.RefreshHexData(true);
                    }
                    else
                    {
                        ClientChapterData.SetTileAsComplete(enterNode.TileData.uid, true);
                        enterNode.RefreshHexData(true);
                        App.BattleResults.ResetData();
                        App.SaveAll();
                    }
                }
                else
                {
                    currentPlayer.EnterMap(enterNode.TileData.GetEnterDirectionAsVector(), enterNode);
                    App.BattleResults.ResetData();
                }


            }
            else
            {
                currentPlayer.EnterMap(enterNode.TileData.GetEnterDirectionAsVector(), enterNode);
            }
        }

        private IEnumerator<float> RunOutroCutsceneForBattle(FableTileData tileData)
        {
            var cutscene = Db.CutsceneDatabase.GetCutscene(tileData.outroCutsceneUid);

            if (!App.Settings.GetSkipCutscenes())
            {
                yield return Timing.WaitUntilDone(StartCutscene(cutscene));
                yield return Timing.WaitUntilTrue(() => ClientChapterData.HasSeenCutscene(cutscene.Uid));
                yield return Timing.WaitForSeconds(0.5f);
                PlayMusicAndAmbient();
                input.GoToMode(FablesInput.Mode.Explore);
                App.BattleResults.ResetData();
                App.SaveAll();
            }
            else
            {
                var chapter = App.FableData.GetCurrentChapter();
                chapter.ResolveQuestIfApplicable(cutscene);
            }

            RevealPathsSingleton(tileData);
        }

        private void RevealPathsSingleton(FableTileData tileData)
        {
            if (!m_revealPathRoutine.IsRunning)
            {
                m_revealPathRoutine = Timing.RunCoroutine(RevealPaths(tileData).CancelWith(gameObject));
            }
            else
            {
                Debug.LogError($"Trying to reveal paths but its already running? {m_revealPathRoutine.IsRunning}");
            }
        }

        private CoroutineHandle m_revealPathRoutine;

        public int GetEntranceNode(FableRoomData last, FableRoomData current)
        {
            if (last == null || last.Uid == current.Uid)
            {
                var startNodes = current.GetTilesOfNodeType(NodeType.Start);

                if (startNodes.Count >= 1)
                {
                    return startNodes[0].uid;
                }

                return 0; // could not find any nodes
            }

            int enterNode = 0;

            var direction = current.Position - last.Position;

            if (direction.x > 0) // right
            {
                var node = current.GetTilesOfNodeType(NodeType.ExitLeft);

                if (node.Count > 0)
                {
                    enterNode = node[0].uid;
                }
            }
            else if (direction.x < 0) // left
            {
                var node = current.GetTilesOfNodeType(NodeType.ExitRight);

                if (node.Count > 0)
                {
                    enterNode = node[0].uid;
                }
            }
            else if (direction.y > 0) // up
            {
                var node = current.GetTilesOfNodeType(NodeType.ExitDown);

                if (node.Count > 0)
                {
                    enterNode = node[0].uid;
                }
            }
            else if (direction.y < 0) // down
            {
                var node = current.GetTilesOfNodeType(NodeType.ExitUp);

                if (node.Count > 0)
                {
                    enterNode = node[0].uid;
                }
            }

            return enterNode;
        }

        public void AddRoom(FableRoomData.RoomDirection roomDirection)
        {
            var currentRoom = GetCurrentRoom();
            var nextRoom = currentRoom.GetRoomInDirection(MapData, roomDirection);

            if (nextRoom == null)
            {
                var roomData = new FableRoomData();
                roomData.UpdateIdentifier();
                roomData.Tiles = new List<FableTileData>();

                switch (roomDirection)
                {
                    case FableRoomData.RoomDirection.Top:
                        roomData.Position = new Vector2Int(currentRoom.Position.x, currentRoom.Position.y + 1);
                        break;
                    case FableRoomData.RoomDirection.Right:
                        roomData.Position = new Vector2Int(currentRoom.Position.x + 1, currentRoom.Position.y);
                        break;
                    case FableRoomData.RoomDirection.Left:
                        roomData.Position = new Vector2Int(currentRoom.Position.x - 1, currentRoom.Position.y);
                        break;
                    case FableRoomData.RoomDirection.Bottom:
                        roomData.Position = new Vector2Int(currentRoom.Position.x, currentRoom.Position.y - 1);
                        break;
                }

                MapData.Rooms.Add(roomData);
                SaveChapter();
            }

            OpenRoom(currentRoom.GetRoomIndexInDirection(MapData, roomDirection), HexGrid2D.GenerateMapTransition.Instant);
        }

        private void OnPlayerEnteredMap(IMessage message)
        {
            if (message.Data is OnPlayerEnteredMapEventArgs args)
            {
                if (ClientChapterData != null && args.tile && args.tile.TileData != null)
                {
                    var roomData = Db.MapDatabase.GetRoomByTileUid(args.tile.TileData.uid);
                    if (roomData != null)
                    {
                        var clientRoomData = ClientChapterData.GetRoomData(roomData.Uid);
                        clientRoomData.Explored = true;

                        var mapData = Db.MapDatabase.GetMapByRoomUid(roomData.Uid);
                        input.exploreInput.miniMap.RebuildMap(mapData, ClientChapterData);

                        App.FableData.SetIsDirty(true);
                    }
                }
            }
        }

        private IEnumerator<float> RevealPaths(FableTileData playerTile)
        {
            var targets = MapData.Rooms[CurrentRoomIndex].GetTilesOfNotOfNodeType(NodeType.None, NodeType.Path);
            var makeTileFallFromSky = false;
            List<CoroutineHandle> pathReveals = new List<CoroutineHandle>();

            //Debug.LogError($"Trying to reveal tile. {playerTile.uid} Targets = {targets.Count}");

            for (var i = 0; i < targets.Count; i++)
            {
                if (ClientChapterData != null && !string.IsNullOrEmpty(targets[i].questId) &&
                    ClientChapterData.CanShowQuestNode(targets[i].questId, targets[i].questStep))
                {
                    if (ClientChapterData.SetQuestNodeAsRevealed(targets[i].uid))
                    {
                        makeTileFallFromSky = true;
                    }
                }

                var path = Pathfinding.FindPath(playerTile, targets[i], true);

                if (path == null)
                {
                    //Debug.LogError($"Can't find path between {playerTile.displayName} -> {targets[i].displayName} [{i}]");
                    continue;
                }

                var targetTile = HexGrid2D.GetTile(targets[i].uid);

                pathReveals.Add(Timing.RunCoroutine(RevealSinglePath(path, targetTile)));
            }

            //Debug.LogError($"Revealing {pathReveals.Count} paths!");
            while (pathReveals.Exists(c => c.IsRunning)) yield return Timing.WaitForOneFrame;
        }

        private IEnumerator<float> RevealSinglePath(List<FableTileData> path, HexTile2D targetTile)
        {
            for (var j = 0; j < path.Count; j++)
            {
                if (path[j].NodeType == NodeType.Path)
                {
                    ClientChapterData.SetTileAsComplete(path[j].uid, false);
                    var tile = HexGrid2D.GetTile(path[j].uid);

                    if (tile && !tile.IsPathRevealed())
                    {
                        Timing.RunCoroutine(tile.RevealPathWithPoof());
                        yield return Timing.WaitForSeconds(HexTile2D.RevealDelay);
                    }
                }
            }

            if (targetTile != null && !targetTile.nodeParentTransform.IsActive())
            {
                yield return Timing.WaitUntilDone(targetTile.FallFromSky());
            }
        }

        private void OnPlayerArrivedAtTile(IMessage message)
        {
            if (message.Data is OnPlayerArrivedAtTileEventArgs args)
            {
                if (args.tile == null)
                {
                    return;
                }

                ClientChapterData.SetPlayerTile(args.tile.TileData.uid);

                if (!App.Settings.GetSkipCutscenes())
                {
                    if (string.IsNullOrEmpty(args.tile.TileData.outroCutsceneUid))
                    {
                        RevealPathsSingleton(args.tile.TileData);
                    }
                }
                else
                {
                    RevealPathsSingleton(args.tile.TileData);
                }
            }

            // check for any events that might trigger, these include dialogue / battles / shops etc
        }

        private CoroutineHandle m_invokingTileBehaviour;

        private void OnPlayerInvokedTile(IMessage message)
        {
            if (message.Data is OnPlayerInvokedTileEventArgs args)
            {
                if (args.tile == null) return;
                if (!HexGrid2D.inputEnabled) return;
                if (m_invokingTileBehaviour.IsRunning) return;

                m_invokingTileBehaviour = Timing.RunCoroutine(InvokeTileBehaviour(args).CancelWith(gameObject));
            }
        }

        private void OnFinishedDemoChoice(bool choice)
        {
            PopupController.OnPopupChoice -= OnFinishedDemoChoice;

            if (choice)
            {
                SettingsManager.ReturnToMainMenu();
            }
            else
            {
                SettingsManager.QuitGame();
            }
        }

        private IEnumerator<float> InvokeTileBehaviour(OnPlayerInvokedTileEventArgs args)
        {
            var saveGame = true;
            // ======================================================================================
            // INTRO CUTSCENE
            // ======================================================================================

            if (!string.IsNullOrEmpty(args.tile.TileData.introCutsceneUid) && (ClientChapterData == null || !ClientChapterData.HasSeenCutscene(args.tile.TileData.introCutsceneUid)))
            {
                var cutscene = Db.CutsceneDatabase.GetCutscene(args.tile.TileData.introCutsceneUid);

                if (!App.Settings.GetSkipCutscenes())
                {
                    yield return Timing.WaitUntilDone(StartCutscene(cutscene).CancelWith(gameObject));
                }

                if (args.tile.ClientTileData != null)
                {
                    if (args.tile.TileData.NodeType == NodeType.Event1 || args.tile.TileData.NodeType == NodeType.Event2)
                    {
                        ClientChapterData?.SetTileAsComplete(args.tile.ClientTileData.TileUid, true);
                    }
                    else
                    {
                        ClientChapterData?.MarkCutsceneAsSeen(args.tile.TileData.introCutsceneUid);
                    }

                    args.tile.RefreshHexData();

                    // if (!string.IsNullOrEmpty(args.tile.TileData.questId))
                    // {
                    //     ClientChapterData?.IncrementQuestStep(args.tile.TileData.questId, 1);
                    // }

                    RevealPathsSingleton(args.tile.TileData);
                }

                input.GoToMode(FablesInput.Mode.Explore);

                while (input.currentMode != FablesInput.Mode.Explore)
                {
                    yield return Timing.WaitForOneFrame;
                }
            }

            // ======================================================================================
            // CHAPTER END
            // ======================================================================================

            if (args.tile.TileData.NodeType == NodeType.ExitChapterFinished)
            {
                // Load the Chapter Complete Banner
                yield return Timing.WaitUntilDone(LoadChapterCompleteBanner().CancelWith(gameObject));



                // Load the Epilogue Scene if applicable
                var cutscene = Db.CutsceneDatabase.GetCutscene(args.tile.TileData.introCutsceneUid);
                if (cutscene != null)
                {
                    // Fade in the Book BG
                    yield return Timing.WaitUntilDone(SetupEpilogue().CancelWith(gameObject));

                    // Play the Cutscene
                    yield return Timing.WaitUntilDone(StartCutscene(cutscene, true).CancelWith(gameObject));
                }

                // Load the Chapter Results Screen
                Timing.RunCoroutine(LoadChapterComplete());

                //yield return Timing.WaitUntilDone(LoadChapterComplete().CancelWith(gameObject));
                yield break;
            }

            // ======================================================================================
            // SHOP
            // ======================================================================================

            if (args.tile.TileData.NodeType == NodeType.Shop)
            {
                if (!OpenShop(args.tile.TileData.items, args.tile.TileData.ShopType))
                {
                    Debug.LogError($"Shop is not valid and could not be opened.");
                }

                while (input.currentMode != FablesInput.Mode.Explore)
                {
                    yield return Timing.WaitForOneFrame;
                }

                if (!string.IsNullOrEmpty(args.tile.TileData.outroCutsceneUid))
                {
                    var cutscene = Db.CutsceneDatabase.GetCutscene(args.tile.TileData.outroCutsceneUid);

                    yield return Timing.WaitUntilDone(StartCutscene(cutscene).CancelWith(gameObject));

                    while (input.currentMode != FablesInput.Mode.Explore)
                    {
                        yield return Timing.WaitForOneFrame;
                    }
                }

                args.tile.TurnOffNotification();
            }

            // ======================================================================================
            // MANA MELD
            // ======================================================================================

            if (args.tile.TileData.NodeType == NodeType.ManaMeld)
            {
                if (!string.IsNullOrEmpty(args.tile.TileData.cardPool))
                {
                    var cardPool = Db.CardPoolsDatabase.GetCardPool(args.tile.TileData.cardPool);

                    if (cardPool != null)
                    {
                        var book = App.FableData.GetCurrentBook();

                        for (var i = 0; i < cardPool.cards.Count; i++)
                        {
                            book.AddRecipe( cardPool.cards[i] );
                        }
                    }
                }

                if (!OpenManaMeld())
                {
                    Debug.LogError($"ManaMeld is not valid and could not be opened.");
                }

                while (input.currentMode != FablesInput.Mode.Explore)
                {
                    yield return Timing.WaitForOneFrame;
                }

                if (!string.IsNullOrEmpty(args.tile.TileData.outroCutsceneUid))
                {
                    var cutscene = Db.CutsceneDatabase.GetCutscene(args.tile.TileData.outroCutsceneUid);

                    yield return Timing.WaitUntilDone(StartCutscene(cutscene).CancelWith(gameObject));

                    while (input.currentMode != FablesInput.Mode.Explore)
                    {
                        yield return Timing.WaitForOneFrame;
                    }
                }
            }

            // ======================================================================================
            // CHESTS
            // ======================================================================================

            if (args.tile.TileData.NodeType == NodeType.Chest &&
                args.tile.TileData.items?.Count > 0 &&
                args.tile.ClientTileData != null &&
                !args.tile.ClientTileData.Complete)
            {
                AudioController.PlaySound("TMP-Key_Pickup");

                yield return Timing.WaitForSeconds(0.1f);

                ClientChapterData?.SetTileAsComplete(args.tile.ClientTileData.TileUid, true);
                args.tile.RefreshHexData();

                var items = new List<ItemValue>( args.tile.TileData.items );

                for (var i = 0; i < items.Count; i++)
                {
                    items[i].Count = Crafting.GetIngredientAmountForAChest(items[i].ItemUid);
                }

                ClientInventory.GrantAndDisplayItems(items, new List<ItemValue> { new ItemValue
                {
                    ItemUid = Currency.MANA_SHARDS,
                    Count = Crafting.ManaShardsPerChest
                }});

            }

            // ======================================================================================
            // BATTLES
            // ======================================================================================

            if (!string.IsNullOrEmpty(args.tile.TileData.battleUid))
            {
                if (!App.Settings.GetSkipBattles())
                {
                    //Debug.LogError($"Loading Battle: Tile ID = {args.tile.TileData.uid}");
                    yield return Timing.WaitUntilDone(LoadBattle(args.tile.TileData).CancelWith(gameObject));
                    yield break;
                }

                if (App.Settings.GetSkipBattles() && args.tile.ClientTileData != null && ClientChapterData!=null)
                {
                    App.BattleResults = new BattleResults();
                    App.BattleResults.SetInitialized(true);
                    App.BattleResults.SetPendingMapVisuals(true);
                    App.BattleResults.SetMatchResult(BattleResults.Result.Won);

                    var battleData = Db.BattleDatabase.GetBattleData(args.tile.TileData.battleUid);
                    var faction = HeroData.GetFactionForHero(FableController.GetCurrentHeroName());

                    BattleController.OnBattleSkipped(battleData, faction, ChapterData, args.tile.TileData.uid, !App.FableData.GetCurrentChapter().GetTile( args.tile.TileData.uid).Complete );

                    ClientChapterData.SetTileAsComplete(args.tile.ClientTileData.TileUid,true );

                    GridPlayer.Instance.PlayAnimation_OnlyUseForEditor("victory", true);

                    UpdateOpenPaths();

                    if (!string.IsNullOrEmpty(args.tile.TileData.outroCutsceneUid))
                    {
                        yield return Timing.WaitForSeconds(1);
                        yield return Timing.WaitUntilDone(RunOutroCutsceneForBattle(args.tile.TileData).CancelWith(gameObject));

                        args.tile.RefreshHexData(false);
                    }
                    else
                    {
                        RevealPathsSingleton(args.tile.TileData);
                        args.tile.RefreshHexData(false );
                    }

                    App.BattleResults.ResetData();

                    input.GoToMode(FablesInput.Mode.Explore);
                }

                while (input.currentMode != FablesInput.Mode.Explore)
                {
                    yield return Timing.WaitForOneFrame;
                }
            }

            // ======================================================================================
            // EXITS
            // ======================================================================================

            if ((args.tile.TileData.IsExit() || args.tile.TileData.NodeType == NodeType.EnterNewMap || args.tile.TileData.NodeType == NodeType.ExitNewMap) && args.tile.TileData.entranceTileUid != 0)
            {
                saveGame = false;
                var mapData = Db.MapDatabase.GetMapByTileUid(args.tile.TileData.entranceTileUid);
                var room = mapData.GetRoomWithTile(args.tile.TileData.entranceTileUid);

                yield return Timing.WaitUntilDone(GridPlayer.Instance.ExitMap().CancelWith(gameObject));

                OpenMap(mapData.Uid, room.Uid, args.tile.TileData.entranceTileUid);
            }

            // ======================================================================================
            // PASSAGES
            // ======================================================================================

            else if (args.tile.TileData.IsExit())
            {
                saveGame = false;
                var roomDirection = args.tile.TileData.GetExitDirection();
                var currentRoom = GetCurrentRoom();
                var nextRoom = currentRoom.GetRoomInDirection(MapData, roomDirection);

                if (nextRoom == null)
                {
                    Debug.LogError("There is no room in this direction!!");
                    yield break;
                }

                yield return Timing.WaitUntilDone(GridPlayer.Instance.ExitMap().CancelWith(gameObject));

                OpenRoom(currentRoom.GetRoomIndexInDirection(MapData, roomDirection), HexGrid2D.GenerateMapTransition.Animate_Blocks);
            }

            PlayMusicAndAmbient();

            if (saveGame)
            {
                App.SaveAll();
            }
        }

        private void PlayMusicAndAmbient()
        {
            var room = GetCurrentRoom();

            if (room == null)
            {
                return;
            }


            if (!string.IsNullOrEmpty(room.Music) && room.Music != "Keep Same")
            {
                if (!AudioController.IsPlaying(room.Music))
                {
                    AudioController.PlaySong(room.Music, true, true);
                }
            }

            if (!string.IsNullOrEmpty(room.Ambient) && room.Ambient != "Keep Same")
            {
                AmbientController.PlayAmbient(room.Ambient);
            }
            else
            {
                AmbientController.Stop();
            }
        }

        public IEnumerator<float> StartCutscene(CutsceneData cutscene, bool epilogue = false)
        {
            if (CutsceneData.IsValid(cutscene))
            {
                switch (cutscene.CutsceneType)
                {
                    case CutsceneType.HexMapDialogue:
                        if (GridPlayer.Instance)
                        {
                            input.GoToMode(FablesInput.Mode.MapDialogue);
                            yield return Timing.WaitUntilDone(GridPlayer.Instance.PlayDialogue(cutscene.DialogueInfo));
                        }

                        break;
                    case CutsceneType.EventScene:
                        FablesInput.Instance.GoToMode(FablesInput.Mode.Dialogue);
                        while (FablesInput.Instance.eventSceneInput.EventSceneInput == null)
                        {
                            yield return Timing.WaitForOneFrame;
                        }
                        FablesInput.Instance.eventSceneInput.EventSceneInput.StartEvent(cutscene);
                        while (!FablesInput.Instance.eventSceneInput.EventSceneInput.CutsceneFinished)
                        {
                            yield return Timing.WaitForOneFrame;
                        }

                        break;
                    case CutsceneType.Cutscene:
                        FablesInput.Instance.GoToMode(FablesInput.Mode.Cutscene);
                        while (FablesInput.Instance.cutsceneInput.CutsceneInput == null)
                        {
                            yield return Timing.WaitForOneFrame;
                        }

                        if (epilogue)
                        {
                            var eventOptions = new EventOptions
                            {
                                dimBackground = false,
                                usesSkipButton = true,
                                usesDialogueBox = false,
                                centerIllustration = true
                            };

                            FablesInput.Instance.cutsceneInput.CutsceneInput.StartEvent(cutscene, eventOptions);
                        }
                        else
                        {
                            var eventOptions = new EventOptions
                            {
                                dimBackground = true,
                                usesSkipButton = true,
                                usesPortraitBackground = true,
                                usesTitleContainer = true,
                                usesDialogueBox = true
                            };

                            FablesInput.Instance.cutsceneInput.CutsceneInput.StartEvent(cutscene, eventOptions);
                        }

                        while (!FablesInput.Instance.cutsceneInput.CutsceneInput.CutsceneFinished)
                        {
                            yield return Timing.WaitForOneFrame;
                        }

                        break;
                }
            }
        }

        public bool OpenShop(List<ItemValue> items, ShopType shopType)
        {
            if (shopType == ShopType.Card)
            {
                FablesInput.Instance.GoToMode(FablesInput.Mode.CardShop);
            }
            else
            {
                FablesInput.Instance.GoToMode(FablesInput.Mode.RelicShop);
            }

            Timing.RunCoroutine(FablesInput.Instance.shopInput.Open(items, shopType));
            return true;
        }

        public bool OpenManaMeld()
        {
            FablesInput.Instance.GoToMode(FablesInput.Mode.ManaMeld);
            FablesInput.Instance.manaMeldInput.OpenManaMeld();
            return true;
        }

        public IEnumerator<float> LoadBattle(FableTileData tile)
        {
            FablesInput.Instance.GoToMode(FablesInput.Mode.None);

            AmbientController.Stop();
            AudioController.StopSong();
            AudioController.PlaySound("jingle-battle-start", "MUSIC/JINGLES");

            yield return Timing.WaitUntilDone(BattleBanner.Instance.AnimateIn());

            var sceneEnvironment = tile.GetSceneEnvironment();

            Events.Publish(this, EventType.StartBattle, new StartBattleEventArgs
            {
                gameMode = new GameMode
                {
                    GameType = GameType.STORY,
                    ConnectionMode = ConnectionMode.LOCAL,
                    SceneEnvironment = sceneEnvironment,
                    SceneToReturnTo = "FablesMap",
                    ShowResultsAfterMatch = true,
                    BattleUid = tile.battleUid,
                    DeckUid = App.PlayerDecks.GetDeckBasedOnGameState().uid,
                }
            });
        }

        // private void OnPlayerExitTile(IMessage message)
        // {
        // }

        public IEnumerator<float> LoadChapterCompleteBanner()
        {
            FablesInput.Instance.GoToMode(FablesInput.Mode.None);

            AmbientController.Stop();
            AudioController.StopSong();
            AudioController.PlaySound("UI-ColiseumStart-NoFX");
            GridPlayer.Instance.PlayAnimation_OnlyUseForEditor("victory", true);

            yield return Timing.WaitForSeconds(.25f);

            AudioController.PlaySound("jingle-select-fable", "MUSIC/JINGLES");

            yield return Timing.WaitUntilDone(ChapterCompleteBanner.Instance.AnimateIn());
        }

        public IEnumerator<float> LoadChapterComplete()
        {
            FablesInput.Instance.GoToMode(FablesInput.Mode.None);

            AmbientController.Stop();
            AudioController.StopSong();
            AudioController.PlaySong("chapter-results");

            App.ChapterResults = new Results.ChapterResults();
            App.ChapterResults.Initialized = true;
            App.ChapterResults.ChapterNumber = ChapterData.ChapterNumber;
            App.ChapterResults.CharacterName = ChapterData.ChapterHero;

            var chapter = App.FableData.GetCurrentChapter();
            chapter.GetAccoladeCounts(out var completeCount, out var maxCount);

            App.ChapterResults.AccoladesComplete = completeCount;
            App.ChapterResults.AccoladesMax = maxCount;
            App.ChapterResults.PlayTime = chapter.GetChapterPlaytime();

            Debug.Log(App.ChapterResults.ChapterNumber);
            Debug.Log(App.ChapterResults.CharacterName);

            //yield return Timing.WaitUntilDone(TransitionController.TransitionIn(TransitionController.TransitionType.Hex));
            yield return Timing.WaitUntilDone(TransitionController.TransitionIn(TransitionController.TransitionType.Fade));
            yield return Timing.WaitUntilDone(SceneController.Instance.UnloadAllScenes().AsCoroutine());

            var sceneToLoad = new SceneLoadParams
            {
                SceneToLoad = "ChapterFinished"
            };
            var sceneLoadTask = SceneController.Instance.LoadScene(sceneToLoad);
        }

        public ClientTileData GetClientTile(FableTileData tile)
        {
            if (ClientChapterData == null) return null;
            if (tile == null) return null;
            if (input.currentMode == FablesInput.Mode.Editor) return null;
            var clientTileData = ClientChapterData.GetTile(tile.uid);

            if (clientTileData == null)
            {
                var roomData = GetCurrentRoom();
                var clientRoomData = ClientChapterData.GetRoomData(roomData.Uid);

                if (clientRoomData == null)
                {
                    Debug.LogError($"Tile is null and cannot be found! {roomData.Uid} - {tile.uid}");
                    return null;
                }

                clientTileData = clientRoomData.GetTile(tile, true);

                if (clientTileData != null)
                {

                    return clientTileData;
                }

                return null;
            }

            return clientTileData;
        }

        public void SaveChapter()
        {
            Db.MapDatabase.Save();
            Db.FablesDatabase.Save();
            //Db.FablesDatabase.Save(ChapterData);
        }

        public IEnumerator<float> SetupEpilogue()
        {
            epilogueBooks.SetActive(true);
            epilogueBooks.DOFade(1f, 1f);
            yield return Timing.WaitForSeconds(1f);
        }
    }
}