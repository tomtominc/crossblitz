using System;
using CrossBlitz.ServerAPI.GameLogic.GameModes;

namespace CrossBlitz.Fables.Data
{
    [Serializable]
    public class TileTypeData
    {
        public string TileId;
        public SceneEnvironment SceneEnvironment;
        public string PathType;
        public TileBehaviourType IdleBehaviourType;
        public string HeroSpriteOverride;

        public const string Default = "grassland";
    }
}