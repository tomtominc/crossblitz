using System;
using System.Collections.Generic;
using CrossBlitz.Utils;
using UnityEngine;

namespace CrossBlitz.Fables.Data
{
    [CreateAssetMenu]
    public class FableQuestDatabase : ScriptableObject
    {
        public List<FableQuestData> Quests;

        public void Init()
        {
            for (var i = 0; i < Quests.Count; i++)
            {
                if (string.IsNullOrEmpty(Quests[i].Uid))
                {
                    Quests[i].UpdateIdentifier();
                }
            }
        }

        public void Save()
        {
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }

        public FableQuestData CreateNewQuest()
        {
            var newQuest = new FableQuestData();
            newQuest.UpdateIdentifier();
            newQuest.Name = "New Quest";
            newQuest.QuestObjectives = new List<string>();

            if (Quests == null) Quests = new List<FableQuestData>();
            Quests.Add(newQuest);

            return newQuest;
        }

        public FableQuestData GetQuest(string uid)
        {
            return Quests.Find(q => q.Uid == uid);
        }

        public string GetQuestUidFromName(string questName)
        {
            var quest = Quests.Find(q => string.Equals(q.Name, questName, StringComparison.CurrentCultureIgnoreCase));

            if (quest != null)
            {
                return quest.Uid;
            }

            return string.Empty;
        }
    }

    [System.Serializable]
    public class FableQuestData : UniqueItem
    {
        public string Name;
        public int QuestSteps;
        public string QuestDescription;
        public List<string> QuestObjectives;
        public bool IsSideQuest;
    }
}