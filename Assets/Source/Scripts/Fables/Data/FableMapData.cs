using System;
using System.Collections.Generic;
using CrossBlitz.Utils;
using UnityEngine;

namespace CrossBlitz.Fables.Data
{

    /// <summary>
    /// All grids that are used in a specific chapter walkthrough
    /// All grids should be connected by entrances/exits to get to all maps.
    /// </summary>
    [Serializable]
    public class FableMapData : UniqueItem
    {
        public string Folder;
        public string DisplayName;
        public string BookLinkUid;
        /// <summary>
        /// All grids/rooms that are connected with this chapters map
        /// </summary>
        public List<FableRoomData> Rooms;

        public FableRoomData GetRoomAtPosition(Vector2Int position)
        {
            return Rooms.Find(r => r.Position == position);
        }

        public FableRoomData GetRoom(string uid)
        {
            return Rooms.Find(r => r.Uid == uid);
        }

        public int GetRoomIndex(string uid)
        {
            return Rooms.IndexOf(GetRoom(uid));
        }

        public bool MoveRoomUp(string roomUid)
        {
            var roomIndex = GetRoomIndex(roomUid);

            if (roomIndex > 0)
            {
                (Rooms[roomIndex - 1], Rooms[roomIndex]) = (Rooms[roomIndex], Rooms[roomIndex - 1]);
                return true;
            }

            return false;
        }

        public bool MoveRoomDown(string roomUid)
        {
            var roomIndex = GetRoomIndex(roomUid);

            if (roomIndex < Rooms.Count-1)
            {
                (Rooms[roomIndex + 1], Rooms[roomIndex]) = (Rooms[roomIndex], Rooms[roomIndex + 1]);
                return true;
            }

            return false;
        }

        public FableRoomData GetRoomWithTile(int uid)
        {
            if (Rooms == null) return null;

            for (var i = 0; i < Rooms.Count; i++)
            {
                for (var j = 0; j < Rooms[i].Tiles.Count; j++)
                {
                    if (Rooms[i].Tiles[j].uid == uid) return Rooms[i];
                }
            }

            return null;
        }

        public FableTileData GetTile(int uid)
        {
            if (Rooms == null) return null;

            for (var i = 0; i < Rooms.Count; i++)
            {
                for (var j = 0; j < Rooms[i].Tiles.Count; j++)
                {
                    if (Rooms[i].Tiles[j].uid == uid) return Rooms[i].Tiles[j];
                }
            }

            return null;
        }

        public List<FableTileData> GetTilesOfNotOfNodeType(params NodeType[] nodeTypes)
        {
            if (Rooms == null) return new List<FableTileData>();
            var nodes = new List<FableTileData>();
            for (var i = 0; i < Rooms.Count; i++)
            {
                for (var j = 0; j < Rooms[i].Tiles.Count; j++)
                {
                    if (!nodeTypes.Contains(Rooms[i].Tiles[j].NodeType))
                    {
                        nodes.Add(Rooms[i].Tiles[j]);
                    }
                }
            }

            return nodes;
        }

        public List<FableTileData> GetTilesOfNodeType(params NodeType[] nodeTypes)
        {
            if (Rooms == null) return new List<FableTileData>();
            var nodes = new List<FableTileData>();
            for (var i = 0; i < Rooms.Count; i++)
            {
                for (var j = 0; j < Rooms[i].Tiles.Count; j++)
                {
                    if (nodeTypes.Contains(Rooms[i].Tiles[j].NodeType))
                    {
                        nodes.Add(Rooms[i].Tiles[j]);
                    }
                }
            }

            return nodes;
        }
    }
}