using System;
using System.Collections.Generic;
using CrossBlitz.Utils;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Fables.Data
{

    /// <summary>
    /// The entire grid of the map, this holds all the tiles
    /// </summary>
    [Serializable]
    public class FableRoomData : UniqueItem
    {
        public enum RoomDirection
        {
            Top,
            Right,
            Bottom,
            Left
        }

        public enum RoomWeather
        {
            Sunny,
            RainHeavy,
            Orbs,
            RainMedium,
            NightSky
        }

        public string DisplayName;
        /// <summary>
        /// True if this is the grid the player starts on.
        /// </summary>
        public bool IsStartingRoom;
        /// <summary>
        /// All the tiles that make up the grid
        /// </summary>
        [Searchable] public List<FableTileData> Tiles;
        /// <summary>
        /// The position of the room within the world grid
        /// </summary>
        public Vector2Int Position;
        /// <summary>
        /// The weather of the room.
        /// </summary>
        public RoomWeather Weather;
        /// <summary>
        /// the palette for the background of the room
        /// </summary>
        public string BackgroundPalette;
        /// <summary>
        /// Music that plays on this map.
        /// </summary>
        public string Music;
        /// <summary>
        /// Ambient this room plays
        /// </summary>
        public string Ambient;

        public FableRoomData GetRoomInDirection(FableMapData mapData, RoomDirection direction)
        {
            var position = Position.InDirection(direction);
            return mapData.GetRoomAtPosition(position);
        }

        public int GetRoomIndexInDirection(FableMapData mapData, RoomDirection direction)
        {
            var position = Position.InDirection(direction);
            var room = mapData.GetRoomAtPosition(position);
            return mapData.Rooms.IndexOf(room);
        }

        public bool HasTileOfType(NodeType nodeType, Func<FableTileData, bool> extraParams=null)
        {
            for (var i = 0; i < Tiles.Count; i++)
            {
                if (Tiles[i].NodeType == nodeType && (extraParams == null || extraParams.Invoke(Tiles[i])))
                {
                    return true;
                }
            }

            return false;
        }

        public FableTileData GetTile(int uid)
        {
            return Tiles.Find(t => t.uid == uid);
        }

        public List<FableTileData> GetTilesOfNotOfNodeType(params NodeType[] nodeTypes)
        {
            var nodes = new List<FableTileData>();

            for (var j = 0; j < Tiles.Count; j++)
            {
                if (!nodeTypes.Contains(Tiles[j].NodeType))
                {
                    nodes.Add(Tiles[j]);
                }
            }

            return nodes;
        }

        public List<FableTileData> GetTilesOfNodeType(params NodeType[] nodeTypes)
        {
            var nodes = new List<FableTileData>();

            for (var j = 0; j < Tiles.Count; j++)
            {
                if (nodeTypes.Contains(Tiles[j].NodeType))
                {
                    nodes.Add(Tiles[j]);
                }
            }

            return nodes;
        }
    }
}