using System;
using System.Collections;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Utils;
using GameDataEditor;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CrossBlitz.Fables.Data
{
    [Serializable]
    public class FableChapterData : UniqueItem
    {
        public bool Enabled;
        /// <summary>
        /// This should not change as it will fuck up the save data.
        /// Will be ChapterHero-BookNumber-ChapterNumber
        /// </summary>
        public string ChapterId;
        /// <summary>
        /// Used for chapter id
        /// </summary>
        public string BookNumber = "1";
        /// <summary>
        /// Used for chapter id
        /// </summary>
        public int ChapterNumber = 1;
        /// <summary>
        /// The name of the chapter
        /// </summary>
        public string ChapterName;
        /// <summary>
        /// The description of the chapter, seen on the book menu.
        /// </summary>
        public string ChapterDescription;
        /// <summary>
        /// The prologue will be shown at the beginning of every chapter.
        /// </summary>
        public string ChapterPrologue;
        /// <summary>
        /// The hero that will be in control of this chapter.
        /// </summary>
        public string ChapterHero;
        /// <summary>
        /// The faction of this chapter, used for various UI elements.
        /// </summary>
        public Faction ChapterFaction;
        /// <summary>
        /// A list of all path ids that are connected to this chapter. Created in the WorldMap scene.
        /// </summary>
        public List<string> Paths;
        /// <summary>
        /// Available set
        /// </summary>
        public List<CardSet> AvailableSets;
        /// <summary>
        /// The card prize pool for this chapter, all battles have chances to gain these cards.
        /// </summary>
        //[ValueDropdown("GetAllCards")]
        public List<string> PrizeCardPool;
        /// <summary>
        /// cards that are excluded from being a prize that you can win from a battle.
        /// </summary>
        //[ValueDropdown("GetAllCards")]
        public List<string> ExcludedPrizeCards;
        /// <summary>
        /// Cards in this chapter that can be crafted at a Mawlder tile.
        /// </summary>
        //[ValueDropdown("GetAllCards")]
        public List<string> CraftableCards;
        /// <summary>
        /// The map id linked to this chapter
        /// </summary>
        public string mapId;
        /// <summary>
        /// Dialogues used for the world map screen, these will play as soon as
        /// The pointer on the map starts moving.
        /// </summary>
        public string IntroCutsceneUid;
        /// <summary>
        /// Dialogues used for the world map screen, these will play as soon as
        /// The pointer on the map starts moving.
        /// </summary>
        public string IntroIllustrationUid;
        /// <summary>
        /// Objectives shown to the player, swapping objectives should be done as an
        /// </summary>
        public string MainQuest;

        public static FableChapterData Fake(string mapId)
        {
            return new FableChapterData
            {
                Enabled = true,
                ChapterId = "Fake-1-1",
                ChapterName = "Fake Chapter",
                ChapterHero = GDEItemKeys.Hero_Redcroft,
                ChapterFaction = Faction.War,
                mapId = mapId,
                IntroCutsceneUid = string.Empty,
                IntroIllustrationUid = string.Empty,
                MainQuest = string.Empty
            };
        }

        // #if UNITY_EDITOR
        // public static ValueDropdownList<string> cards;
        //
        // private static IEnumerable GetAllCards()
        // {
        //     if (cards != null) return cards;
        //
        //     cards = new ValueDropdownList<string>();
        //
        //     if (!Db.Initialized)
        //     {
        //         Db.ForceInit();
        //     }
        //
        //     for (var i = 0; i < Db.CardDatabase.Cards.Count; i++)
        //     {
        //         var cardData = Db.CardDatabase.Cards[i];
        //         cards.Add($"{cardData.faction}/{cardData.name}", cardData.id);
        //     }
        //
        //     return cards;
        // }
        // #endif
    }

    public static class FablesUtils
    {
        public static Vector2Int InDirection(this Vector2Int pos, FableRoomData.RoomDirection direction)
        {
            var newPos = pos;

            switch (direction)
            {
                case FableRoomData.RoomDirection.Top:
                    newPos = new Vector2Int(pos.x, pos.y + 1);
                    break;
                case FableRoomData.RoomDirection.Right:
                    newPos = new Vector2Int(pos.x + 1, pos.y);
                    break;
                case FableRoomData.RoomDirection.Left:
                    newPos = new Vector2Int(pos.x - 1, pos.y);
                    break;
                case FableRoomData.RoomDirection.Bottom:
                    newPos = new Vector2Int(pos.x, pos.y - 1);
                    break;
            }

            return newPos;
        }
    }
}