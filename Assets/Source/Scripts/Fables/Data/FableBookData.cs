using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Shop.Data;
using CrossBlitz.Utils;

namespace CrossBlitz.Fables.Data
{
    [System.Serializable]
    public class FableBookData : UniqueItem
    {
        /// <summary>
        /// Is this book enabled? if its not, it won't show up as an option in the book menu
        /// </summary>
        public bool Enabled;
        /// <summary>
        /// The books Id
        /// </summary>
        public string BookId;
        /// <summary>
        /// The name of this book, if we want to change it from X Chapter X
        /// </summary>
        public string BookName;
        /// <summary>
        /// The books number in the series
        /// </summary>
        public int BookNumber;
        /// <summary>
        /// The hero this book belongs to
        /// </summary>
        public string BookHero;
        /// <summary>
        /// The books prologue TODO: CHANGE THIS TO BE A CUTSCENE ID
        /// </summary>
        public string BookPrologue;
        /// <summary>
        /// The chapters of the book, each holding various data
        /// </summary>
        public List<FableChapterData> Chapters;

        public FableChapterData GetChapter(string uid)
        {
            return Chapters.Find(c => c.Uid == uid);
        }
    }
}