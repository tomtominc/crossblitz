using System;
using System.Collections.Generic;
using System.Linq;
using CrossBlitz.Card;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Grid;
using CrossBlitz.Fables.Treasure;
using CrossBlitz.PlayFab.Data;
using CrossBlitz.ServerAPI.GameLogic.GameModes;
using Newtonsoft.Json;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CrossBlitz.Fables.Data
{
    /// <summary>
    /// Adding a Tile Requirements:
    /// - Add the tile animation to the hex-tile animator (variants can append a -1/-2/-3 etc)
    /// - Add the tile type as a key to SceneEnvironments, TileAnimations & PathAnimations
    ///
    /// </summary>
    // public enum TileType
    // {
    //     None,
    //     Grasslands,
    //     Sand,
    //     WoodA,
    //     WoodB,
    //     River,
    //     Ocean,
    //     OceanShallows
    // }

    public enum NodeType
    {
        None,
        Start,
        Chest,
        Battle,
        MultiBattle,
        [Obsolete("A boss battle should have the tag Battle and in the battle settings be set to Boss.")]
        BossBattle,
        [Obsolete("The battle tile should be checked using the client info, if the tile is 'Complete', then the battle is won.")]
        BattleWon,
        [Obsolete("The boss tile should be checked using the client info, if the tile is 'Complete', then the battle is won.")]
        BossWon,
        [Obsolete("The multi-battle tile should be checked using the client info, if the tile is 'Complete', then the battle is won.")]
        MultiBattleWon,
        Event1,
        Event2,
        Shop,
        ExitUp,
        ExitDown,
        ExitRight,
        ExitLeft,
        ExitChapterFinished,
        Path,
        [Obsolete("Un-walkable tiles dont make sense in this game since only paths are walkable.")]
        UnWalkable,
        EnterNewMap,
        ManaMeld,
        ExitNewMap
    }

    public enum TileBehaviourType
    {
        None,
        Float,
    }

    public enum ShopType
    {
        Card,
        Relic
    }

    public enum ChestType
    {
        Ingredient,
        Recipe,
        Relic,
    }

    /// <summary>
    /// This is 1 hex on the big grid of tiles.
    /// This holds info about many different elements of the tile some properties do not
    /// apply to all tile types.
    /// </summary>
    [System.Serializable]
    public class FableTileData : DataModel
    {


        public static readonly Dictionary<NodeType, string> NodeAnimations = new Dictionary<NodeType, string>
        {
            {NodeType.None, string.Empty},
            {NodeType.Start, "start"},
            {NodeType.Chest, "generic"}
        };

        public static string GetFactionStringLower(FableTileData tile)
        {
            if (string.IsNullOrEmpty(tile.battleUid)) return "neutral";

            var battleData = Db.BattleDatabase.GetBattleData(tile.battleUid);

            return battleData == null ? "neutral" :
                battleData.Faction == Faction.All ? "neutral" :
                battleData.Faction.ToString().ToLower();
        }

        public static string GetNodeAnimation(FableTileData tile)
        {
            var faction = GetFactionStringLower(tile);

            ClientTileData clientData=null;
            if (FablesMapController.Instance)
            {
                clientData = FablesMapController.Instance.GetClientTile(tile);
            }

            var battleData = !string.IsNullOrEmpty(tile.battleUid) ?
                Db.BattleDatabase.GetBattleData(tile.battleUid) : null;

            switch (tile.NodeType)
            {
                case NodeType.MultiBattle when clientData != null && clientData.Complete:
                    return $"multi-battle-2x-won-{faction}";
                case NodeType.Battle when battleData != null && battleData.BattleType == BattleType.Boss && clientData != null && clientData.Complete:
                    return $"boss-won-{faction}";
                case NodeType.Battle when battleData != null && battleData.BattleType == BattleType.Boss:
                    return $"battle-{faction}";
                case NodeType.Battle when clientData != null && clientData.Complete:
                    return $"battle-won-{faction}";
                case NodeType.Battle:
                    return $"battle-{faction}";
                case NodeType.Chest when clientData != null && clientData.Complete:
                    return "chest-opened";
                case NodeType.MultiBattle:
                    return $"multi-battle-2x-{faction}";
                case NodeType.Event1 when clientData != null && clientData.Complete:
                case NodeType.Event2 when clientData != null && clientData.Complete:
                    return "event-viewed";
                default:
                    return NodeAnimations.TryGetValue(tile.NodeType, out var nodeAnimation) ? nodeAnimation : "generic";
            }
        }

        public static string GetMarkerAnimation(FableTileData tile, bool getBattleCharacterInsteadOfIcon = false)
        {
            var markerAnim = string.Empty;

            var battleData = Db.BattleDatabase.GetBattleData(tile.battleUid);

            switch (tile.NodeType)
            {
                case NodeType.Battle when getBattleCharacterInsteadOfIcon:
                    var enemy = battleData.Deck.GetHeroData();
                    markerAnim = $"npc-{enemy.portraitUrl}";
                    break;
                case NodeType.Battle when battleData != null && battleData.BattleType == BattleType.Boss:
                    markerAnim = "boss";
                    break;
                case NodeType.Chest when tile.ChestType == ChestType.Ingredient:
                    markerAnim = "chest-melding";
                    break;
                case NodeType.Chest when tile.ChestType == ChestType.Relic:
                    markerAnim = "chest-relic";
                    break;
                case NodeType.Chest when tile.ChestType == ChestType.Recipe:
                    markerAnim = "chest";
                    break;
                case NodeType.Shop when tile.ShopType == ShopType.Card:
                    markerAnim = "card-shop";
                    break;
                case NodeType.Shop when tile.ShopType == ShopType.Relic:
                    markerAnim = "relic-shop";
                    break;
                default:
                    if (MarkerAnimations.TryGetValue(tile.NodeType, out var marker))
                    {
                        markerAnim = marker;
                    }
                    break;
            }

            return markerAnim;
        }

        public static readonly Dictionary<NodeType, string> NodeShadowAnimations = new Dictionary<NodeType, string>
        {
            {NodeType.None, string.Empty},
            {NodeType.Start, "start"}
        };

        public static readonly Dictionary<NodeType, string> MarkerShadowAnimations = new Dictionary<NodeType, string>
        {
            {NodeType.None, string.Empty},
            {NodeType.Start, string.Empty},
            {NodeType.Chest, "chest"},
            {NodeType.Event1, "event-1"},
            {NodeType.Event2, "event-2"},
            {NodeType.ExitUp, "up-exit"},
            {NodeType.ExitDown, "down-exit"},
            {NodeType.ExitRight, "right-exit"},
            {NodeType.ExitLeft, "left-exit"}
        };

        public static readonly Dictionary<NodeType, string> MarkerAnimations = new Dictionary<NodeType, string>
        {
            {NodeType.None, string.Empty},
            {NodeType.Start, string.Empty},
            {NodeType.Chest, "chest"},
            {NodeType.Battle, "battle"},
            {NodeType.Event1, "event-1"},
            {NodeType.Event2, "event-2"},
            {NodeType.Shop, "peddlers-wagon"},
            {NodeType.ExitUp, "up-exit"},
            {NodeType.ExitDown, "down-exit"},
            {NodeType.ExitRight, "right-exit"},
            {NodeType.ExitLeft, "left-exit"},
            {NodeType.MultiBattle, "multi-battle-2x"},
            {NodeType.ExitChapterFinished, "exit-chapter"},
            {NodeType.EnterNewMap, "enter"},
            {NodeType.ManaMeld, "melding-furnace"},
            {NodeType.ExitNewMap, "enter-reverse"},
        };

        public static readonly Dictionary<NodeType, float> SelectArrowHeight = new Dictionary<NodeType, float>
        {
            {NodeType.None, 0f},
            {NodeType.Start, 1.0625f},
            {NodeType.Chest, 0.5f},
            {NodeType.Battle, 0.59375f},
            {NodeType.Event1, 0.59375f},
            {NodeType.Event2, 0.40625f},
            {NodeType.Shop, 1.0625f},
            {NodeType.ExitUp, 0.4375f},
            {NodeType.ExitDown, 0.4375f},
            {NodeType.ExitRight, 0.40625f},
            {NodeType.ExitLeft, 0.40625f},
            {NodeType.Path, 0},
            {NodeType.EnterNewMap, 0.59375f},
            {NodeType.ManaMeld, 1.0625f},
            {NodeType.ExitNewMap, 0.59375f},
        };

        public static readonly Dictionary<NodeType, float> MarkerAnimationHeight = new Dictionary<NodeType, float>
        {
            {NodeType.Battle, 0.125f},
            {NodeType.Event1, 0.125f},
            {NodeType.Event2, 0.125f},
            {NodeType.ExitUp, 0.125f},
            {NodeType.ExitDown, 0.125f},
            {NodeType.ExitRight, 0.125f},
            {NodeType.ExitLeft, 0.125f},
            {NodeType.MultiBattle, 0.125f},
            {NodeType.ExitChapterFinished, 0.125f},
        };

        /// <summary>
        /// unique identifier for the tile.
        /// </summary>
        [ReadOnly]
        public int uid;
        public string displayName;
        public string tileType;
        public NodeType NodeType;
        public ShopType ShopType;
        public ChestType ChestType;
        public string Decoration;
        public string Npc;
        public int Height;
        public Hex hex;
        public string cutsceneUid;
        public string introCutsceneUid;
        public string outroCutsceneUid;
        public int entranceTileUid; // used for going into and out of interiors or different maps that dont follow the normal flow of going up/down/left/right
        public FableRoomData.RoomDirection entranceDirection; // the direction you'll be moving when entering into the interior
        public string linkedMapUid;
        public string battleUid;
        public bool hiddenAtStart;
        public string showOnCompletedTileUid;
        public int objectiveChangeIndex;
        public List<ItemValue> items;
        public string cardPool;
        public List<int> connectingPathRooms;
        public string questId;
        public int questStep;


        // for quests that have a start/pending/complete state


        [NonSerialized] [JsonIgnore] public int gCost;
        [NonSerialized] [JsonIgnore] public int hCost;
        [JsonIgnore] public int FCost => gCost + hCost;
        [NonSerialized] [JsonIgnore] public int parentUid;

        public void Init()
        {
            uid = GUID.CreateUniqueInt();
            displayName = $"NewTile#{uid}";
            items = new List<ItemValue>();
            connectingPathRooms = new List<int>();
        }

        public void Init(int _uid)
        {
            uid = _uid;
            displayName = $"NewTile#{uid}";
            items = new List<ItemValue>();
            connectingPathRooms = new List<int>();
        }

        // public bool IsWater()
        // {
        //     return !string.IsNullOrEmpty(tileType) && tileType.
        // }

        public bool IsWalkable()
        {
            if (string.IsNullOrEmpty(tileType) || NodeType == NodeType.None) return false;
            return true;
        }

        public bool IsValidNodeType()
        {
            return NodeType != NodeType.None && NodeType != NodeType.Path;
        }

        public bool IsEmptyTile()
        {
            return string.IsNullOrEmpty(tileType);
        }

        public void PlayTileAnimation(SpriteAnimation tileAnimator)
        {
           PlayTileAnimation(tileType, tileAnimator);
        }

        public static void PlayTileAnimation(string tileType, SpriteAnimation tileAnimator)
        {
            if (!string.IsNullOrEmpty(tileType))
            {
                var variantCount = 0;

                foreach (var anim in tileAnimator.animationsByName)
                {
                    if (anim.Key.Contains(tileType) && anim.Key.Length-2 <= tileType.Length)
                    {
                        variantCount++;
                    }
                }

                var randRange = Random.Range(1, variantCount+1);
                var animationName = $"{tileType}-{randRange}";
                var animationData = tileAnimator.GetAnimationData(animationName);

                //Debug.Log($"Tile Type = {tileType} ({variantCount}) = {animationName}");

                if (animationData != null && animationData.frameDatas.Count > 1)
                {
                    var frame = Random.Range(0, animationData.frameDatas.Count);

                    if (!tileAnimator.Play(animationName, frame ))
                    {
                        Debug.LogError($"Could not play {animationName} at frame {frame}");
                    }
                }
                else if (!tileAnimator.Play(animationName))
                {
                    Debug.LogError($"Could not play {animationName}.");
                }
            }
            else
            {
                Debug.LogError($"Could not find animation for tile type {tileType}");
            }
        }

        public SceneEnvironment GetSceneEnvironment()
        {
            var tileTypeData = Db.FablesDatabase.GetTileTypeData(tileType);
            return tileTypeData.SceneEnvironment;
        }

        public bool IsExit()
        {
            return NodeType == NodeType.ExitDown || NodeType == NodeType.ExitUp ||
                   NodeType == NodeType.ExitRight || NodeType == NodeType.ExitLeft;
        }

        public FableRoomData.RoomDirection GetExitDirection()
        {
            switch (NodeType)
            {
                case NodeType.ExitDown: return FableRoomData.RoomDirection.Bottom;
                case NodeType.ExitUp: return FableRoomData.RoomDirection.Top;
                case NodeType.ExitLeft: return FableRoomData.RoomDirection.Left;
                case NodeType.ExitRight: return FableRoomData.RoomDirection.Right;
            }

            return FableRoomData.RoomDirection.Top;
        }

        public Vector2Int GetEnterDirectionAsVector()
        {
            return NodeType switch
            {
                NodeType.ExitDown => Vector2Int.up,
                NodeType.ExitUp => Vector2Int.down,
                NodeType.ExitLeft => Vector2Int.right,
                NodeType.ExitRight => Vector2Int.left,
                NodeType.Start => Vector2Int.down,
                _ => entranceDirection switch
                {
                    FableRoomData.RoomDirection.Top => Vector2Int.down,
                    FableRoomData.RoomDirection.Right => Vector2Int.left,
                    FableRoomData.RoomDirection.Bottom => Vector2Int.up,
                    FableRoomData.RoomDirection.Left => Vector2Int.right,
                    _ => Vector2Int.zero
                }
            };
        }

        public bool IsNeighbour(int neighbourId)
        {
            var neighbours = HexGrid2D.GetNeighbors(hex);
            return neighbours.Exists(tile => tile.uid == neighbourId);
        }
    }
}