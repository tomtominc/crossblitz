using System;
using UnityEngine;

namespace CrossBlitz.Fables.Data
{
    [Serializable]
    public class TileDecorationData
    {
        public string Id;
        public Vector2Int PixelOffset;

        public Vector2 GetOffsetInWorldSpace()
        {
            return new Vector2(PixelOffset.x * (1f / 32f), PixelOffset.y * (1f / 32f));
        }
    }
}