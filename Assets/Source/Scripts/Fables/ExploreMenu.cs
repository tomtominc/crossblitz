using System.Collections.Generic;
using CrossBlitz.AddressablesAPI;
using CrossBlitz.Audio;
using CrossBlitz.Cursors;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.ExploreAPI;
using CrossBlitz.Fables.Grid;
using CrossBlitz.Fables.PathfindingAPI;
using CrossBlitz.InputAPI;
using CrossBlitz.Utils;
using CrossBlitz.ViewAPI.Popups;
using MEC;
using QFSW.QC;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CrossBlitz.Fables
{
    public class ExploreInputCursor : ICursorSelectable
    {
        public bool interactable;
        public bool grab;
        public bool info;
        public bool loading;
        public int lastTileCheckedForPath;

        public bool Interactable => interactable;
        public bool Grabbable => grab;
        public bool InfoOnly => info;
        public bool Loading => loading;
    }
    public class ExploreMenu : InputMode
    {
        public QuestSideContainer questContainer;
        public FablesMiniMap miniMap;

        public static HexTile2D CurrentTile;
        public static HexTile2D SelectedTile;
        //public Camera _cam;

        private bool _inputDisabled;
        private ExploreInputCursor m_cursor;

        private static ExploreMenu m_instance;

        public void Start()
        {
            if (m_instance != null && m_instance != this)
            {
                Destroy(m_instance.gameObject);
            }

            _inputDisabled = false;
            m_cursor = new ExploreInputCursor { interactable = true };
            m_instance = this;
        }

        public IEnumerator<float> MovePlayerToTile(HexTile2D tile)
        {
            if (GridPlayer.Instance == null)
            {
                yield break;
            }

            if (FablesInput.Instance == null || FablesInput.Instance.currentMode != FablesInput.Mode.Explore)
            {
                yield break;
            }

            if (tile.TileData.NodeType != NodeType.None)
            {
                _inputDisabled = true;
            }

            AudioController.PlaySound("TMP-UI-SelectLockedItem");
            yield return Timing.WaitUntilDone(GridPlayer.Instance.WalkToTile(tile));

            _inputDisabled = false;
        }

        public override void StartMode()
        {
            questContainer.DisplayOn();
            miniMap.DisplayOn();
        }

        public override void EndMode()
        {
            questContainer.DisplayOff();
            miniMap.DisplayOff();
        }

        private void OnDestroy()
        {
            CrossBlitzInputModule.Instance.SetOverrideCursor(null);
        }

        public override void UpdateMode()
        {
            UpdateRaycast();
            UpdateAction();

            if (CurrentTile != null && CurrentTile.TileData.IsValidNodeType() && GridPlayer.Instance)
            {
                if (GridPlayer.Instance.CurrentTile != null && GridPlayer.Instance.CurrentTile.TileData?.uid == CurrentTile.TileData.uid)
                    // I'm on this tile, we just pull up the magnifying glass
                {
                    m_cursor.interactable = false;
                    m_cursor.info = !m_cursor.interactable;
                }
                else if (m_cursor.lastTileCheckedForPath != CurrentTile.TileData.uid)
                {
                    var path =
                        Pathfinding.FindPath(GridPlayer.Instance.CurrentTile.TileData, CurrentTile.TileData, false, true);
                    m_cursor.interactable = path != null;
                    m_cursor.info = !m_cursor.interactable;
                }

                m_cursor.lastTileCheckedForPath = CurrentTile.TileData.uid;

                CrossBlitzInputModule.Instance.SetOverrideCursor(m_cursor);
            }
            else
            {
                CrossBlitzInputModule.Instance.SetOverrideCursor(null);
            }



            CrossBlitzInputModule.Instance.SetOverrideCursor(
                CurrentTile != null && CurrentTile.TileData.IsValidNodeType() ? m_cursor : null);
        }

        private void UpdateRaycast()
        {
            var tile = HexGrid2D.GetTileFromMousePosition();

            if (tile != null && tile != CurrentTile)
            {
                if (CurrentTile && !_inputDisabled) CurrentTile.OnPointerExit(CurrentTile != SelectedTile);

                CurrentTile = tile;

                if (CurrentTile && !_inputDisabled ) CurrentTile.OnPointerEnterExploreMode();
            }
            else if (tile == null)
            {
                if (CurrentTile && CurrentTile != SelectedTile && !_inputDisabled) CurrentTile.OnPointerExit();
                CurrentTile = null;
            }

            if (CurrentTile && !_inputDisabled) CurrentTile.OnPointerHoverExploreMode();
        }

        public static bool IsPointerOverUIObject()
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }

        public static bool IsPointerOverUIObject2()
        {
            return EventSystem.current.IsPointerOverGameObject();
        }

        public static bool IsAbleToSelectTile()
        {
            if (PopupController.IsOpen)
            {
                return false;
            }

            if (SceneController.IsLoaded("RecipeViewer"))
            {
                return false;
            }

            if (m_instance == null)
            {
                return false;
            }

            if (m_instance._inputDisabled)
            {
                return false;
            }

            if (GridPlayer.Instance == null)
            {
                return false;
            }

            if (GridPlayer.Instance != null && GridPlayer.Instance.IsLeavingMap())
            {
                return false;
            }

            return true;
        }

        private void UpdateAction()
        {
            var leftMouseClick = InputProcessor.IsLeftMouseDown();
            var rightMouseClick = InputProcessor.IsRightMouseDown();

            if(leftMouseClick && IsAbleToSelectTile())
            {
                if (CurrentTile != null)
                {
                    CurrentTile.OnLeftClick_ExploreMode();
                }

                if (_inputDisabled)
                {
                    return;
                }

                if (SelectedTile != null && CurrentTile != SelectedTile)
                {
                    SelectedTile.OnPointerExit();
                    SelectedTile.Deselect();
                    SelectedTile = null;
                }

                SelectedTile = CurrentTile;

                if (SelectedTile == null) return;

                if (SelectedTile.TileData.IsValidNodeType() && SelectedTile.IsRevealed)
                {
                    if (!string.IsNullOrEmpty(SelectedTile.TileData.questId))
                    {
                        if (!SelectedTile.IsQuestShowing(out var isSideQuest))
                        {
                            return;
                        }
                    }

                    SelectedTile.OnPointerExit();
                    SelectedTile.Deselect();
                    Timing.RunCoroutine(MovePlayerToTile(SelectedTile));
                    SelectedTile = null;
                }
            }
            else if (rightMouseClick && IsAbleToSelectTile())
            {
                if (_inputDisabled) return;

                if (CurrentTile != null)
                {
                    CurrentTile.OnRightClick_ExploreMode();
                }
            }
        }



        [Command("ToolsMode")]
        private void OnToolsButton()
        {
            InputProcessor.GoToMode(FablesInput.Mode.Editor);
        }
    }
}