using System;
using System.Collections.Generic;
using CrossBlitz.Card;
using CrossBlitz.ClientAPI.Visuals;
using CrossBlitz.Databases;
using CrossBlitz.Fables.Accolades;
using CrossBlitz.Fables.Battle;
using CrossBlitz.Fables.Data;
using CrossBlitz.Fables.Grid;
using CrossBlitz.PlayFab.Authentication;
using CrossBlitz.PlayFab.Data;
using DG.Tweening;
using GameDataEditor;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace CrossBlitz.Fables
{
    public class NodeInfoTooltip : MonoBehaviour
    {
        public TextMeshProUGUI nodeName;
        public TextMeshProUGUI deckName;
        public CanvasGroup canvas;

        [BoxGroup("Battles")]
        public TextMeshProUGUI enemyLevel;
        [BoxGroup("Battles")]
        public RectTransform accoladeDisplayContainer;
        [BoxGroup("Battles")]
        public AccoladesDisplay accoladeDisplayPrefab;
        [BoxGroup("Battles")]
        public List<RelicSlot> relicSlots;

        [BoxGroup("Multi-Battles")] public CanvasGroup levelContainerCanvas;
        [BoxGroup("Multi-Battles")] public CanvasGroup nameContainerCanvas;
        [BoxGroup("Multi-Battles")] public SpriteAnimation battleCountTimes;
        [BoxGroup("Multi-Battles")] public TextMeshProUGUI battleCounter;

        public void Open(HexTile2D tile)
        {
            this.SetActive(true);

            var tileData = tile.TileData;
            var clientTileData = tile.ClientTileData;
            var battleData = Db.BattleDatabase.GetBattleData(tile.TileData.battleUid);

            switch (tileData.NodeType)
            {
                case NodeType.Battle:
                case NodeType.MultiBattle:
                    nodeName.text = battleData.Name;
                    deckName.text = Db.DeckRecipeDatabase.GetArchetypeName(battleData.DeckArchetype);
                    enemyLevel.text = battleData.Level.ToString();
                    CreateAccolades(battleData, clientTileData);
                    CreateRelics(battleData);
                    break;
                case NodeType.Event1:
                case NodeType.Event2:
                    var cutsceneData = Db.CutsceneDatabase.GetCutscene(tileData.introCutsceneUid);

                    if (cutsceneData !=null)
                    {
                        nodeName.text = cutsceneData.EventTitle;
                    }
                    else
                    {
                        nodeName.text = "???";
                    }
                    break;
            }

            DOTween.Kill(canvas);
            DOTween.Kill(this.RectTransform());

            canvas.DOFade(1, 0.12f);

            var parentCanvas = transform.GetComponentInParent<Canvas>();
            var canvasRect = transform.parent.RectTransform();
            var viewportPosition = parentCanvas.worldCamera.WorldToViewportPoint(tile.transform.position);
            var targetPosition = new Vector2(
                ((viewportPosition.x*canvasRect.rect.width)-(canvasRect.rect.width*0.5f)),
                ((viewportPosition.y*canvasRect.rect.height)-(canvasRect.rect.height*0.5f)));
            targetPosition.y += (16 * tileData.Height) + 32f;

            var xOffset =(transform.RectTransform().sizeDelta.x / 2) + 32;
            if (targetPosition.x > 100) xOffset = -xOffset;
            targetPosition.x += xOffset;

            //Debug.Log("viewportPosition.x:" + viewportPosition.x);
            //Debug.Log("viewportPosition.y:" + viewportPosition.y);
            //Debug.Log("canvasRect.rect.width:" + canvasRect.rect.width);
            //Debug.Log("canvasRect.rect.height:" + canvasRect.rect.height);
            //Debug.Log("targetPosition.x:" + targetPosition.x);
            //Debug.Log("targetPosition.y:" + targetPosition.y);
            //Debug.Log("rect.width:" + this.RectTransform().rect.width);
            //Debug.Log("rect.height:" + this.RectTransform().rect.height);

            // adjust height so tooltip does not extend off screen
            var rectHeight = this.RectTransform().rect.height / 2;
            if (targetPosition.y + rectHeight > 180)
            {
                var newTargetDiff = (targetPosition.y + rectHeight - 180 + 5);
                targetPosition.y -= newTargetDiff;
            }

            //Debug.Log("targetPosition.y:" + targetPosition.y);
            this.RectTransform().anchoredPosition = new Vector2(targetPosition.x, targetPosition.y - 8f);
            this.RectTransform().DOAnchorPosY(targetPosition.y, 0.24f)
                .SetEase(Ease.OutBack);

            // this.RectTransform().DOAnchorPosY(targetPosition.y - 4f, 1f).SetDelay(0.24f)
            //     .SetLoops(-1, LoopType.Yoyo);
        }

        public void Close()
        {
            DOTween.Kill(canvas);
            DOTween.Kill(this.RectTransform());

            canvas.DOFade(0, 0.12f);
            this.RectTransform().DOAnchorPosY(this.RectTransform().anchoredPosition.y - 8f, 0.12f)
                .SetEase(Ease.InBack);
        }

        private void CreateAccolades(BattleData battleData, ClientTileData clientData)
        {
            accoladeDisplayContainer.DestroyChildren();

            if (clientData == null) return;

            clientData.BattleData?.FixDependencies(battleData);

            for (var i = 0; i < battleData.Accolades.Count; i++)
            {
                if (clientData.BattleData == null) continue;
                var accolade = Instantiate(accoladeDisplayPrefab, accoladeDisplayContainer);
                accolade.SetData(battleData.Accolades[i], clientData.BattleData?.Accolades[i],clientData.BattleData.Accolades[i].IsComplete);
            }
        }

        private void CreateRelics(BattleData battleData)
        {
            //Initialize Relics from deck
            for (var i = 0; i < relicSlots.Count; i++)
            {
                relicSlots[i].SetAsEmpty();
                relicSlots[i].slot.SetActive(false);
            }

            //Find relics and blitz burst from deck
            var deckData = battleData.Deck;
            var k = 1;

            for (var i = 0; i < deckData.cards.Count; i++)
            {
                var card = Db.CardDatabase.GetCard(deckData.cards[i].id);
                if(card != null)
                {
                    if (card.type == CardType.Elder_Relic)
                    {
                        relicSlots[0].zoomedCardParent.DestroyChildren();
                        relicSlots[0].SetRelic(card.id, deckData.uid);
                    }
                    else if (card.type == CardType.Relic)
                    {
                        relicSlots[k].zoomedCardParent.DestroyChildren();
                        relicSlots[k].SetRelic(card.id, deckData.uid);
                        k += 1;
                    }
                }
            }
        }
    }
}