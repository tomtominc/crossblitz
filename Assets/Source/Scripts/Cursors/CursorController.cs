using Sirenix.OdinInspector;

namespace CrossBlitz.Cursors
{
    public class CursorController : SerializedMonoBehaviour
    {
        // public enum CursorState
        // {
        //     Idle,//0
        //     Clickable,//1
        //     Clicked,//1--when mouse down
        //     HoverGrab,//2--
        //     Grab,
        //     Loading,
        //     Magnify
        // }
        //
        //
        // public bool debugCursor;
        // public Transform cursorDebugTransform;
        // public PoolManager clickEffectPool;
        // public EventSystem eventSystem;
        // public Dictionary<CursorState, Texture2D> cursorMap;
        //
        // public static bool ShouldUpdateCursor;
        // private static CursorController _instance;
        // private static CursorState _cursorState;
        // private Camera _camera;
        //
        // private void Awake()
        // {
        //     _instance = this;
        //     _camera = Camera.main;
        //     ShouldUpdateCursor = true;
        //     //SetCursorState(CursorState.Idle);
        // }
        //
        // public static void SetEffectCamera(Camera cam)
        // {
        //     if (_instance == null) return;
        //     _instance._camera = cam;
        // }
        //
        // public static void SetEventSystemActive(bool active)
        // {
        //     _instance.eventSystem.SetActive(active);
        // }
        //
        // public void Update()
        // {
        //     if (debugCursor)
        //     {
        //         if (!cursorDebugTransform.IsActive())
        //         {
        //             cursorDebugTransform.SetActive(true);
        //         }
        //
        //         if (_camera)
        //         {
        //             var mousePosition =DragRotator.GetWorldPositionOnPlane( _camera,Input.mousePosition,0);
        //             mousePosition.z = 0;
        //             cursorDebugTransform.position = mousePosition;
        //         }
        //     }
        //     else if (cursorDebugTransform.IsActive())
        //     {
        //         cursorDebugTransform.SetActive(false);
        //     }
        //
        //     if (!ShouldUpdateCursor) return;
        //
        //     if (_cursorState == CursorState.Idle ||
        //         _cursorState == CursorState.Clickable ||
        //         _cursorState == CursorState.Clicked)
        //     {
        //         if (Input.GetMouseButton(0))
        //         {
        //             SetCursorState(CursorState.Clicked);
        //         }
        //         else if (_cursorState != CursorState.Clickable)
        //         {
        //             SetCursorState(CursorState.Idle);
        //         }
        //     }
        // }
        //
        // public static void SetCursorState(CursorState state)
        // {
        //     _cursorState = state;
        //
        //     Cursor.SetCursor(GetCursorTexture(_cursorState), new Vector2(9,14), CursorMode.Auto);
        //     Cursor.lockState = CursorLockMode.Confined;
        //
        //     if (state == CursorState.Clicked && Input.GetMouseButtonDown(0))
        //     {
        //         if (_instance._camera == null)
        //         {
        //             _instance._camera = Camera.main;
        //         }
        //
        //         if (_instance._camera != null)
        //         {
        //             var pos = DragRotator.GetWorldPositionOnPlane(_instance._camera, Input.mousePosition, 0);
        //             var e = _instance.clickEffectPool.AcquireObject<AnimatorPoolObject>(pos, Quaternion.identity);
        //             e.transform.localScale = _instance._camera.orthographic ? Vector3.one : Vector3.one * 5;
        //         }
        //     }
        // }
        //
        // private static Texture2D GetCursorTexture(CursorState state)
        // {
        //     return _instance.cursorMap[state];
        // }
        //
        // [Button("Set Cursor")]
        // public void SetCursor(CursorState cursorState)
        // {
        //     Cursor.SetCursor(cursorMap[cursorState], Vector2.zero, CursorMode.Auto);
        //     Cursor.lockState = CursorLockMode.Confined;
        // }
    }
}