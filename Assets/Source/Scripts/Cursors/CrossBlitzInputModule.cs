using System;
using CrossBlitz.Plugins.TakoBoyStudios.Utilities;
using CrossBlitz.Pools;
using CrossBlitz.Utils;
using KennethDevelops.ProLibrary.Managers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace CrossBlitz.Cursors
{
    [RequireComponent(typeof(EventSystem))]
    public class CrossBlitzInputModule : StandaloneInputModule
    {
        // How many pixels fit into 1 unit of the game
        public const int PixelsPerUnit = 32;

        public static CrossBlitzInputModule Instance;

        public string clickSound;
        public Canvas cursorCanvas;
        public Camera _cursorCamera;
        public Texture2D NormalCursor;
        public Texture2D HoverClickableCursor;
        public Texture2D ClickingNormalCursor;
        public Texture2D ClickingHandCursor;
        public Texture2D HoverGrabCursor;
        public Texture2D GrabbingCursor;
        public Texture2D LoadingCursor;
        public Texture2D InfoCursor;

        public PoolManager clickEffectPool;

        private EventSystem _eventSystem;
        private Camera _renderCamera;
        private Texture2D _currentCursor;
        private Vector2 _screenMultiplier;
        private ICursorSelectable m_overrideCursor;

        public Vector2 ScreenMultiplier => _screenMultiplier;

        public event Action OnClickedNothing;

        protected override void Awake()
        {
            Instance = this;
            _eventSystem = GetComponent<EventSystem>();
            _renderCamera = _cursorCamera;
            ChangeCursor(NormalCursor);

            base.Awake();
        }

        protected override void Start()
        {
            base.Start();
            OnResolutionChanged();
        }

        protected override void OnDestroy()
        {
            Instance = this;
            base.OnDestroy();
        }

        public void OnResolutionChanged()
        {
            _screenMultiplier = new Vector2(Screen.width / 640f, Screen.height / 360f);
        }

        public void SetSystemActive(bool active)
        {
            if(gameObject)
            {
                gameObject.SetActive(active);
            }
            if (active)
            {
                clickEffectPool.Clear();
                ChangeCursor(NormalCursor);
            }
        }

        public void SetRenderCamera(Camera c)
        {
            _renderCamera = c;
            ChangeCursor(NormalCursor);
        }

        public Vector3 GetMousePosition(bool screenSpace = false)
        {
            if (screenSpace) return Input.mousePosition;
            return _renderCamera.ScreenToWorldPoint(Input.mousePosition);
        }

        public Vector3 GetMousePositionInNativeSpace()
        {
            return ConvertToNativeRes(GetMousePosition(true));
        }

        public Vector2 GetNativeScreenSize()
        {
            return ConvertToNativeRes(new Vector2(Screen.width, Screen.height));
        }

        public Vector2 GetUiPositionAtNativeRes(RectTransform rectTransform)
        {
            var screenPoint = _renderCamera.WorldToScreenPoint(rectTransform.position);
            return ConvertToNativeRes(screenPoint);
        }

        public Vector2 ConvertToNativeRes(Vector2 point)
        {
            var screenMultiplier = ScreenMultiplier;
            return new Vector2(point.x / screenMultiplier.x, point.y / screenMultiplier.y);
        }

        public void SetOverrideCursor(ICursorSelectable overrideCursor)
        {
            m_overrideCursor = overrideCursor;
        }

        protected void Update()
        {
            var mouseState = GetMousePointerEventData(kMouseLeftId);
            var pressedState = mouseState.GetButtonState(PointerEventData.InputButton.Left).eventData.buttonState;

            if (_eventSystem.IsPointerOverGameObject(kMouseLeftId) || m_overrideCursor!=null)
            {
                var hovered = GetObjectUnderPointer(kMouseLeftId);

                if (hovered != null || m_overrideCursor !=null)
                {
                    var cursor = m_overrideCursor ?? hovered.GetComponentInParent<ICursorSelectable>();

                    if (cursor != null)
                    {
                        if (cursor.Interactable)
                        {
                            if (pressedState == PointerEventData.FramePressState.Pressed)
                            {
                                CreateClickEffect();
                                ChangeCursor(ClickingHandCursor);
                            }
                            else if (pressedState == PointerEventData.FramePressState.Released)
                            {
                                ChangeCursor(HoverClickableCursor);
                            }
                            else if (pressedState == PointerEventData.FramePressState.PressedAndReleased)
                            {
                                CreateClickEffect();
                                ChangeCursor(HoverClickableCursor);
                            }
                            else if (_currentCursor == NormalCursor || _currentCursor == HoverGrabCursor ||
                                     _currentCursor == InfoCursor || _currentCursor == LoadingCursor)
                            {
                                ChangeCursor(HoverClickableCursor);
                            }

                            return;
                        }

                        if (cursor.Grabbable)
                        {
                            if (pressedState == PointerEventData.FramePressState.Pressed)
                            {
                                ChangeCursor(GrabbingCursor);
                            }
                            else if (pressedState == PointerEventData.FramePressState.Released)
                            {
                                ChangeCursor(HoverGrabCursor);
                            }
                            else if (pressedState == PointerEventData.FramePressState.PressedAndReleased)
                            {
                                ChangeCursor(HoverGrabCursor);
                            }
                            else if (_currentCursor == NormalCursor || _currentCursor == HoverClickableCursor ||
                                     _currentCursor == InfoCursor || _currentCursor == LoadingCursor)
                            {
                                ChangeCursor(HoverGrabCursor);
                            }

                            return;
                        }

                        if (cursor.InfoOnly)
                        {
                            ChangeCursor(InfoCursor);
                            return;
                        }

                        if (cursor.Loading)
                        {
                            ChangeCursor(LoadingCursor);
                            return;
                        }
                    }

                    var selectable = hovered.GetComponentInParent<Selectable>();

                    if (selectable != null)
                    {
                        if (pressedState == PointerEventData.FramePressState.Pressed)
                        {
                            CreateClickEffect();
                            ChangeCursor(ClickingHandCursor);
                        }
                        else if (pressedState == PointerEventData.FramePressState.Released)
                        {
                            ChangeCursor(HoverClickableCursor);
                        }
                        else if (pressedState == PointerEventData.FramePressState.PressedAndReleased)
                        {
                            CreateClickEffect();
                            ChangeCursor(HoverClickableCursor);
                        }
                        else if (_currentCursor == NormalCursor || _currentCursor == HoverGrabCursor ||
                                 _currentCursor == InfoCursor || _currentCursor == LoadingCursor)
                        {
                            ChangeCursor(HoverClickableCursor);
                        }

                        return;
                    }
                }
            }

            if (pressedState == PointerEventData.FramePressState.Pressed)
            {
                OnClickedNothing?.Invoke();
                CreateClickEffect();
                ChangeCursor(ClickingNormalCursor);
                //MasterAudio.PlaySound(clickSound);
            }
            else if (pressedState == PointerEventData.FramePressState.Released)
            {
                ChangeCursor(NormalCursor);
            }
            else if (pressedState == PointerEventData.FramePressState.PressedAndReleased)
            {
                CreateClickEffect();
                ChangeCursor(NormalCursor);
            }
            else if (_currentCursor == HoverClickableCursor || _currentCursor == HoverGrabCursor|| _currentCursor == InfoCursor || _currentCursor == LoadingCursor)
            {
                ChangeCursor(NormalCursor);
            }
        }

        private void ChangeCursor(Texture2D cursorTexture)
        {
            _currentCursor = cursorTexture;
            Cursor.SetCursor(cursorTexture, new Vector2(cursorTexture.width*0.2f, cursorTexture.height*0.2f), CursorMode.Auto);
        }

        private GameObject GetObjectUnderPointer(int pointerId)
        {
            var lastPointer = GetLastPointerEventData(pointerId);
            return lastPointer?.pointerCurrentRaycast.gameObject;
        }

        private void CreateClickEffect()
        {
            clickEffectPool.AcquireUiObject<AnimatorPoolObject>(cursorCanvas.ScreenToCanvasPosition(Input.mousePosition));
        }
    }
}