Spawn:
cob-trap-card-fx@spawn

Spawn timings:
Frame 5 - spawn card, with tint #2f192f at full opacity.
Frame 18 - slowly fade out tint. Mock-up goes down to 0 opacity over about 400ms.

Despawn:
despawn-smoke@smoke
cob-trap-card@despawn

Despawn timing (timed to despawn-smoke@smoke):
-Frames 0-5 - tint:
    colour: #2f192f
    opacity: [0,60,100,140,180,220]
-Frame 6 - switch trap card out for cob-trap-card@despawn

Destroy:
cob-trap-card-fx@destroy

Destroy timings:
Frame 0-2 - x-offsets: [-1,2,-1]
Frame 0-12 - tint:
    colour: #ffffff
    opacity: [40,80,90,90,90,80,40,0,0,0,40,100,200]
Frame 13 - stop drawing card
