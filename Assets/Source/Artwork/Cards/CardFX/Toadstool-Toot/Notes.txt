Spawn some musical notes on frames 7-9, and frames 17-19. Can use same method as riff.

Mushroom puffs:

A short pause after the whistle sfx finishes, spawn them in random positions all over the game field in quick succession. The mock launches 2 or 3 every 120ms until all 10 are launched.
mushroom-puff-spawn-fx@spawn fires at the same time as spawning the puff.

Start the despawn animation of the whistle either during this moment or after all the puffs have exploded on the target. Either would work well.

Hitting the hero:
Use same explosion FX & tint as for mushroom cannon.