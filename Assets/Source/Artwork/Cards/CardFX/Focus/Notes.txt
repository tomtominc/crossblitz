Burst:

focus@burst-standard, focus@burst-zoomed

Tint colour: #76d3f4
Tint opacity: [30,50,80,100,110,110,110,110,110,110,100,95,90,80,60,30]

------------------------
Transform:

focus@transform-standard, focus@transform-standard, card-bg@focus-activated

-Sparks are baked into this animation and not needed for this part.
-The card bg will likely need some manual positioning to be centered correctly on both the standard and zoomed views.

Tint 1 colour: #2f192f
Tint 1 opacity: (timed to either transform) [20,40,60,80,90,90,90,90,90,90,90,90,90,90]
Tint 2 colour: #fcd66b
Tint 2 opacity: (continue after tint 1)[30,170,180,180,180,180,180,180,180,180,180,180,180,180,170,160,140,120,80,20,0]

-Switch to new bg somewhere during frames 18-21 (about 800 ms in)

------------------------
Activated Idle:

focus-aura@idle,
focus-spark-large & focus-spark-small

-The 'aura' is layered under the card frame on standard view. Sparks go on top of the card.
-Aura just loops. Positon should be correct for standard view, but will need to be positioned manually for the zoomed view.


-Sparks can fire randomly (both position and timing)
-They can be flipped/rotated too
-Maybe limit the large versions to just the zoomed view.
-Positioned anywhere on the standard view I think, but maybe limit it roughly to the area around the character art section on the zoomed view.
