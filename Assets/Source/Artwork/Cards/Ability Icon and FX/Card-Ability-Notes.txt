Brave Shield Overlay - R:244 G:150 B:72 - 30% opacity, subtle glow
Brave Shield Shield Graphic - 70% opacity, subtle glow

Enraged Char Art Overlay - R:223 G:42 B:42 - 45% opacity, subtle glow

Frozen Overlay - R:141 G:235 B:246 - 50% opacity
Frozen Char Art Overlay - R:141 G:235 B246 - 30% opacity
Ice Bits - 40% opacity
Ice Streaks - 40% opacity
--For the snowflakes - just hold frame 2 for as long as we want the snowflake to fall, then play the remaining frames for it to despawn.

Poisoned Overlay - R:208 G:103 B:244 - 30% opacity, subtle glow
Poisoned Char Art Overlay - R:208 G:103 B:244 - 30% opacity, subtle glow
-- For poison particles - hold frame 4 for when they float upwards, then just fade out when they're ready to disappear.

Battlecry (or any generic trigger) - R:255 G:255 B:255 - opacity varies, see animation previews

Silence Overlay - R: 195 G:216 B:233
-- For the chains: hold the last frame for as long as needed, then fade them out
-- For silence bubble: start animation at frame 9 of chain activate



