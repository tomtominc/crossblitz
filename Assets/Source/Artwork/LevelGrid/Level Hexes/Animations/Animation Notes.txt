Hex shine that plays after a Hex is filled in/leveled up:

Frame 1: 40%

Frame 2: 40%

Frame 3: 80%

Frame 4: 100%

Frame 5: 80%

Frame 6: 80%

Frame 7-9: 40%



For teeny lvl numbers that appear on the star node, indicating your current level going up after a Level Up/LvP is spent:

This sequence starts on frame 10 of "hex@level-up-burst"

X,Y Coordinates, with Frame 10 being the 0,0 location. The level up numbers sit at the base of the Star Hex, as opposed to the lvl icons (hp up, new card, etc) that sit in the center of the hex.

Frame 1: (0,-1)

Frame 2: (0,0)

Frame 3: (0,1)

Frame 4: (0,2)

Frame 5: (0,3)

Frame 6: (0,3)

Frame 7: (0,3)

Frame 8: (0,2)

Frame 9: (0,-1)

Frame 10: (0,0)


For lvl icons (like HP UP, new card, chests, etc):

This sequence starts on frame 10 of "hex@level-up-burst"

X,Y Coordinates, with Frame 10 being the 0,0 location. The icons sit in the center of the hex.

Frame 1: (0,0)

Frame 2: (0,2)

Frame 3: (0,3)

Frame 4: (0,4)

Frame 5: (0,3)

Frame 6: (0,2)

Frame 7: (0,-1)

Frame 8: (0,1)

Frame 9: (0,-1)

Frame 10: (0,0)

