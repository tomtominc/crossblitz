*Overlay for Storm Colors

R: 85
G: 117
B: 140

Blending Mode: Multiply

Opacity: 80%


*Overlay for Lightning (sits on top of Storm overlay)

R: 204
G: 251
B: 230

Blending Mode: Hard Mix

Opacity:

Frame1: 20%
Frame2: 80%
Frame3: 70%
Frame4: 50%
Frame5: 20%
Frame6: 10%


*Rain Drop 1
Front Rain Layer
Opacity: 60%


*Rain Drop 2
Back Rain Layer
Opacity: 40%


*Rain Hit FX
Opacity: 60%

Can play over any HEX tiles.

