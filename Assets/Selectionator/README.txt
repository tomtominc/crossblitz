﻿Hello, and thank you for choosing Selectionator!
If you don't need them, you can safely delete the "Demo" and "Examples" folders. Keep the "Scripts" folder, though!

To get started, please check out the example script in the "Examples" folder.

Further documentation, as well as useful reference graphs, are located at http://www.evilwizardstudios.com/selectionator/

Report bugs (and request assistance) at @evilwizards on Twitter, or support@evilwizardstudios.com