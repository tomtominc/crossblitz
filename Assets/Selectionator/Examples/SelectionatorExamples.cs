﻿using System.Collections.Generic;
using System.Linq;
using Selectionator;

public class SelectionatorExamples
{
    public class ExampleSelectableItem : ISelectable
    {
        public string Data { get; set; }
        public float GetSelectionWeight()
        {
            return SelectionWeight;
        }

        public float AdjustedSelectionWeight { get; set; }

        public float SelectionWeight { get; set; }

    }

    public static class SelectionTests
    {
        public static List<ExampleSelectableItem> SelectableItems;

        public static void Init()
        {
            SelectableItems = new List<ExampleSelectableItem>
            {
                new ExampleSelectableItem {Data = "Item A", SelectionWeight = 1},       // Selection Weight Notes:
                new ExampleSelectableItem {Data = "Item B", SelectionWeight = 2},       // Overlapping weights...
                new ExampleSelectableItem {Data = "Item C", SelectionWeight = 2},       // ...are valid.
                new ExampleSelectableItem {Data = "Item E", SelectionWeight = 3.3f},    // Decimals are valid as well.
                new ExampleSelectableItem {Data = "Item F", SelectionWeight = 0},       // Values of zero, like this, will never be selected
                new ExampleSelectableItem {Data = "Item H", SelectionWeight = (5*2)},   // Anything that can be parsed as a float, like this...
                new ExampleSelectableItem {Data = "Item I", SelectionWeight = (1/9f)},   // ...or like this, is valid.
            };
        }

        /// <summary>
        ///     Select an item based on weight.
        /// </summary>
        /// <returns>A single item.</returns>
        public static string SimpleSingleSelection()
        {
            var mySelector = new Selector();
            ExampleSelectableItem mySelection = mySelector.Single(SelectableItems);

            return mySelection.Data;
        }

        /// <summary>
        ///     Select a number of items, based on weight.
        /// </summary>
        /// <returns>An array of size (in this case) 10, filled with items.</returns>
        public static string[] SimpleMultipleSelection()
        {
            int mySelectionSize = 10;
            var mySelector = new Selector();
            IEnumerable<ExampleSelectableItem> mySelection = mySelector.Multiple(SelectableItems, mySelectionSize);

            string[] dataArray = mySelection.Select(item => item.Data).ToArray();
            return dataArray;
        }

        /// <summary>
        ///     Select an item based on weight, but apply a bonus selection "sweetener" to shift selection probability.
        /// </summary>
        /// <returns>A single item.</returns>
        public static string SingleSelectionWithBonus()
        {
            int myBonusValue = 25;
            var mySelector = new Selector();
            ExampleSelectableItem mySelection = mySelector.Single(SelectableItems, myBonusValue);

            return mySelection.Data;
        }

        /// <summary>
        ///     Select an item based on weight, but apply a bonus selection "sweetener" to shift selection probability.
        /// </summary>
        /// <returns>An array of size (in this case) 10, filled with items.</returns>
        public static string[] MultipleSelectionWithBonus()
        {
            int myBonusValue = 25;
            int mySelectionSize = 10;
            var mySelector = new Selector();
            ExampleSelectableItem[] mySelection = mySelector.Multiple(SelectableItems, mySelectionSize, myBonusValue);

            string[] dataArray = mySelection.Select(item => item.Data).ToArray();
            return dataArray;
        }

        /// <summary>
        ///     Select an item based on weight, using a selector that pushes weights towards the median (instead of the mean) and
        ///     only operates on weights below the median pivot.
        /// </summary>
        /// <returns>A single item.</returns>
        public static string SingleSelectionWithCustomParameters()
        {
            int myBonusValue = 25;
            var mySelector = new Selector(SelectionPivot.Median, SelectionRange.BelowPivot);
            ExampleSelectableItem mySelection = mySelector.Single(SelectableItems, myBonusValue);

            return mySelection.Data;
        }
    }
}