﻿using UnityEngine;

namespace Febucci.UI.Core
{
    /// <summary>
    /// Represents bars in the text, like underlines and strikethrough
    /// </summary>
    public struct Bar
    {
        public int charIndex;

        public Vector3[] vertCopies;
        public Vector3[] vertSources;
    }

}