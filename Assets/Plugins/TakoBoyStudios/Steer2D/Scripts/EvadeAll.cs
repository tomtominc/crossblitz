using UnityEngine;

namespace Steer2D
{
    public class EvadeAll : SteeringBehaviour
    {
        public float FleeRadius = 1.0f;
        public bool DrawGizmos = false;
        Vector2 currentPosition;

        public override Vector2 GetVelocity()
        {
            currentPosition = transform.position;
            return Seperation();
        }

        Vector2 Seperation()
        {
            var closestAgent = GetClosestAgent();
            if (!closestAgent) return Vector2.zero;
            var moveDirection = (Vector2)closestAgent.transform.position - currentPosition;
            return new Vector2(-moveDirection.x, 0);
        }

        SteeringAgent GetClosestAgent()
        {
             SteeringAgent closestAgent=null;

            var distanceAway = FleeRadius;

            foreach (var _agent in SteeringAgent.AgentList)
            {
                if (_agent == agent) continue;
                if (!_agent.CompareTag("Cloud")) continue;

                var dist = Vector3.Distance(_agent.transform.position, currentPosition);

                if (dist < distanceAway)
                {
                    distanceAway = dist;
                    closestAgent = _agent;
                }
            }

            return closestAgent;
        }

        void OnDrawGizmos()
        {
            if (DrawGizmos)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawWireSphere(transform.position, FleeRadius/2);
            }
        }
    }
}