﻿using System;
using UnityEngine;

namespace Steer2D
{
    public class Seek : SteeringBehaviour
    {
        public Vector2 TargetPoint = Vector2.zero;
        public Transform target;

        public override Vector2 GetVelocity()
        {
            if (!target)
            {
                return ((TargetPoint - (Vector2) transform.position).normalized * agent.MaxVelocity) -
                       agent.CurrentVelocity;
            }

            return (Vector2)(target.position - transform.position).normalized * agent.MaxVelocity - agent.CurrentVelocity;
        }
    }
}
