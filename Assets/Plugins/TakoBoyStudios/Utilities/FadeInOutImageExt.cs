using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Plugins.TakoBoyStudios.Utilities
{
    public class FadeInOutImageExt : MonoBehaviour
    {
        public float speed;
        public Color fadeFrom;
        public Color fadeTo;

        private Image image;

        private void Awake()
        {
            image = GetComponent<Image>();
            image.color = fadeFrom;
        }

        private void Update()
        {
            var t = Mathf.PingPong(Time.time * speed, 1);
            image.color = Color.Lerp(fadeFrom, fadeTo, t);
        }

    }
}