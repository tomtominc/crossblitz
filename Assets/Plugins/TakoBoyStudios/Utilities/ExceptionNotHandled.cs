using UnityEngine;

namespace CrossBlitz.Plugins.TakoBoyStudios.Utilities
{
    /// <summary>
    /// This class is a substitute for actually handling exceptions because they can't be handled at the time of coding something
    /// Should be no references to this class when the product is shipped!
    /// </summary>
    public static class ExceptionNotHandled
    {
        public static void LogError(string message)
        {
            Debug.LogError(message);
        }
    }
}