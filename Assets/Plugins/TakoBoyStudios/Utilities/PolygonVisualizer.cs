using UnityEngine;

namespace CrossBlitz.Plugins.TakoBoyStudios.Utilities
{
    [RequireComponent(typeof(PolygonCollider2D))]
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    [ExecuteInEditMode]
    public class PolygonVisualizer : MonoBehaviour
    {
        private PolygonCollider2D _pc2;

        public PolygonCollider2D pc2
        {
            get
            {
                if (_pc2 == null) _pc2 = GetComponent<PolygonCollider2D>();
                return _pc2;
            }
        }

        private void Start()
        {
            UpdateMesh();
        }

        private void Update()
        {
            if (Application.isPlaying) return;
            UpdateMesh();
        }

        private void UpdateMesh()
        {
            int pointCount = 0;
            pointCount = pc2.GetTotalPointCount();
            MeshFilter mf = GetComponent<MeshFilter>();
            Mesh mesh = new Mesh();
            Vector2[] points = pc2.points;
            Vector3[] vertices = new Vector3[pointCount];
            Vector2[] uv = new Vector2[pointCount];
            for(int j=0; j<pointCount; j++){
                Vector2 actual = points[j];
                vertices[j] = new Vector3(actual.x, actual.y, 0);
                uv[j] = actual;
            }
            Triangulator tr = new Triangulator(points);
            int [] triangles = tr.Triangulate();
            mesh.vertices = vertices;
            mesh.triangles = triangles;
            mesh.uv = uv;
            mf.mesh = mesh;
        }
    }
}