using UnityEngine;
using UnityEngine.UI;

namespace CrossBlitz.Plugins.TakoBoyStudios.Utilities
{
    public class PingPongImageAlpha : MonoBehaviour
    {
        public float speed;

        public CanvasGroup image;

        private void Awake()
        {
            image.alpha = 1;
        }

        private void Update()
        {
            var t = Mathf.PingPong(Time.time * speed, 1);
            image.alpha = Mathf.PingPong(t, 1);
        }

    }
}