﻿/*
 * Copyright 2015-2017 Evil Wizard Studios
 * Unauthorized copying and distribution of this file, via any medium, is strictly prohibited
 */

namespace Selectionator
{
    /// <summary>
    /// Objects must implement this interface to be selectable. Any collection of ISelectable-implementing objects can be selected from as long as the collection implements IEnumerable. 
    /// </summary>
    public interface ISelectable
    {
        /// <summary>
        /// Returns a float that represents an item's selection weight. Higher values will get selected more often than lower values.
        /// </summary>
        float SelectionWeight {get; set;}
    }
}
