﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TakoBoyStudios.Events
{
    public class EventManager
    {
        private static bool _initialized;
        private static Dictionary<System.Enum, MessageHandler> _messageHandlers;

        private static void Initialize()
        {
            _messageHandlers = new Dictionary<System.Enum, MessageHandler>();
            _initialized = true;
        }

        public static void ClearListeners()
        {
            if (!_initialized) Initialize();
            _messageHandlers.Clear();
        }

        public static void Subscribe(System.Enum pMessage, MessageHandler pHandler)
        {
            if (!_initialized) Initialize();

            MessageListenerDefinition lListener = MessageListenerDefinition.allocate();
            lListener.messageType = pMessage;
            lListener.handler = pHandler;
            Subscribe(lListener);
        }

        private static void Subscribe(MessageListenerDefinition pListener)
        {
            if (!_initialized) Initialize();

            Debug.Log($"Added: {pListener.messageType} {pListener.handler.Target}");

            if (_messageHandlers.ContainsKey(pListener.messageType))
            {
                _messageHandlers[pListener.messageType] += pListener.handler;
            }
            else
            {
                _messageHandlers.Add(pListener.messageType, pListener.handler);
            }

            MessageListenerDefinition.release(pListener);
        }

        public static void Unsubscribe(System.Enum pMessage, MessageHandler pHandler)
        {
            if (!_initialized) Initialize();

            MessageListenerDefinition lListener = MessageListenerDefinition.allocate();
            lListener.messageType = pMessage;
            lListener.handler = pHandler;
            Unsubscribe(lListener);
        }

        private static void Unsubscribe(MessageListenerDefinition pListener)
        {
            if (!_initialized) Initialize();

            if (_messageHandlers.ContainsKey(pListener.messageType))
            {
                MessageHandler lHandler = _messageHandlers[pListener.messageType];
                lHandler -= pListener.handler;

                Debug.Log($"Removed: {pListener.messageType} {pListener.handler.Target}");

                if (null == lHandler || lHandler.GetInvocationList().All( invoke => invoke.Target == null))
                {
                    Debug.Log($"Cleared: {pListener.messageType} {pListener.handler.Target}");
                    _messageHandlers.Remove(pListener.messageType);
                }
                else
                {
                    var invokes = $"Targets ({pListener.messageType}):\n";
                    var invokeList = lHandler.GetInvocationList();
                    for (var i = 0; i < invokeList.Length; i++)
                    {
                        invokes += $"{invokeList[i].Target}\n";
                    }
                    Debug.Log(invokes);
                }
            }
            else
            {
                Debug.LogError($"Could not release {pListener.messageType} {pListener.handler.Target}");
            }

            MessageListenerDefinition.release(pListener);
        }

        public static void Publish(object pSender, System.Enum pMessage, object pData)
        {
            if (!_initialized) Initialize();

            Message lMessage = Message.allocate();
            lMessage.Sender = pSender;
            lMessage.MessageType = pMessage;
            lMessage.Data = pData;

            Publish(lMessage);

            lMessage.Release();
        }

        private static void Publish(IMessage pMessage)
        {
            if (!_initialized) Initialize();

            if (_messageHandlers.ContainsKey(pMessage.MessageType))
            {
                MessageHandler lHandler = _messageHandlers[pMessage.MessageType];
                lHandler?.Invoke(pMessage);
            }
        }
    }

    public delegate void MessageHandler(IMessage rMessage);
}