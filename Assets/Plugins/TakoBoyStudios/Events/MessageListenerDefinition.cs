namespace TakoBoyStudios.Events
{
    public class MessageListenerDefinition
    {
        public System.Enum messageType;
        public MessageHandler handler;

        private static EventObjectPool<MessageListenerDefinition> m_pool = new EventObjectPool<MessageListenerDefinition>(40, 10);

        public static MessageListenerDefinition allocate()
        {
            MessageListenerDefinition l_instance = m_pool.allocate();

            l_instance.handler = null;

            if (l_instance == null)
            {
                l_instance = new MessageListenerDefinition();
            }

            return l_instance;
        }

        public static void release(MessageListenerDefinition p_instance)
        {
            if (p_instance == null)
            {
                return;
            }

            p_instance.handler = null;
            m_pool.release(p_instance);
        }
    }
}
