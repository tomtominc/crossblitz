// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by the Game Data Editor.
//
//      Changes to this file will be lost if the code is regenerated.
//
//      This file was generated from this data file:
//      /Users/thomasferrer/Documents/Development/CreaturesOfAether/Assets/Source/Bundles/data/gde_data.txt
//  </autogenerated>
// ------------------------------------------------------------------------------
using UnityEngine;
using UnityEngine.Video;
using System;
using System.Collections.Generic;

using GameDataEditor;

namespace GameDataEditor
{
    public class GDEDungeonAvatarData : IGDEData
    {
        static string AvatarNameKey = "AvatarName";
		string _AvatarName;
        public string AvatarName
        {
            get { return _AvatarName; }
            set {
                if (_AvatarName != value)
                {
                    _AvatarName = value;
					GDEDataManager.SetString(_key, AvatarNameKey, _AvatarName);
                }
            }
        }

        static string AnimationKeyKey = "AnimationKey";
		string _AnimationKey;
        public string AnimationKey
        {
            get { return _AnimationKey; }
            set {
                if (_AnimationKey != value)
                {
                    _AnimationKey = value;
					GDEDataManager.SetString(_key, AnimationKeyKey, _AnimationKey);
                }
            }
        }

        static string RarityKey = "Rarity";
		string _Rarity;
        public string Rarity
        {
            get { return _Rarity; }
            set {
                if (_Rarity != value)
                {
                    _Rarity = value;
					GDEDataManager.SetString(_key, RarityKey, _Rarity);
                }
            }
        }

        static string ElementKey = "Element";
		string _Element;
        public string Element
        {
            get { return _Element; }
            set {
                if (_Element != value)
                {
                    _Element = value;
					GDEDataManager.SetString(_key, ElementKey, _Element);
                }
            }
        }

        public GDEDungeonAvatarData(string key) : base(key)
        {
            GDEDataManager.RegisterItem(this.SchemaName(), key);
        }
        public override Dictionary<string, object> SaveToDict()
		{
			var dict = new Dictionary<string, object>();
			dict.Add(GDMConstants.SchemaKey, "DungeonAvatar");
			
            dict.Merge(true, AvatarName.ToGDEDict(AvatarNameKey));
            dict.Merge(true, AnimationKey.ToGDEDict(AnimationKeyKey));
            dict.Merge(true, Rarity.ToGDEDict(RarityKey));
            dict.Merge(true, Element.ToGDEDict(ElementKey));
            return dict;
		}

        public override void UpdateCustomItems(bool rebuildKeyList)
        {
        }

        public override void LoadFromDict(string dataKey, Dictionary<string, object> dict)
        {
            _key = dataKey;

			if (dict == null)
				LoadFromSavedData(dataKey);
			else
			{
                dict.TryGetString(AvatarNameKey, out _AvatarName);
                dict.TryGetString(AnimationKeyKey, out _AnimationKey);
                dict.TryGetString(RarityKey, out _Rarity);
                dict.TryGetString(ElementKey, out _Element);
                LoadFromSavedData(dataKey);
			}
		}

        public override void LoadFromSavedData(string dataKey)
		{
			_key = dataKey;
			
            _AvatarName = GDEDataManager.GetString(_key, AvatarNameKey, _AvatarName);
            _AnimationKey = GDEDataManager.GetString(_key, AnimationKeyKey, _AnimationKey);
            _Rarity = GDEDataManager.GetString(_key, RarityKey, _Rarity);
            _Element = GDEDataManager.GetString(_key, ElementKey, _Element);
        }

        public GDEDungeonAvatarData ShallowClone()
		{
			string newKey = Guid.NewGuid().ToString();
			GDEDungeonAvatarData newClone = new GDEDungeonAvatarData(newKey);

            newClone.AvatarName = AvatarName;
            newClone.AnimationKey = AnimationKey;
            newClone.Rarity = Rarity;
            newClone.Element = Element;

            return newClone;
		}

        public GDEDungeonAvatarData DeepClone()
		{
			GDEDungeonAvatarData newClone = ShallowClone();
            return newClone;
		}

        public void Reset_AvatarName()
        {
            GDEDataManager.ResetToDefault(_key, AvatarNameKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(AvatarNameKey, out _AvatarName);
        }

        public void Reset_AnimationKey()
        {
            GDEDataManager.ResetToDefault(_key, AnimationKeyKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(AnimationKeyKey, out _AnimationKey);
        }

        public void Reset_Rarity()
        {
            GDEDataManager.ResetToDefault(_key, RarityKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(RarityKey, out _Rarity);
        }

        public void Reset_Element()
        {
            GDEDataManager.ResetToDefault(_key, ElementKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(ElementKey, out _Element);
        }

        public void ResetAll()
        {
             #if !UNITY_WEBPLAYER
             GDEDataManager.DeregisterItem(this.SchemaName(), _key);
             #else

            GDEDataManager.ResetToDefault(_key, AvatarNameKey);
            GDEDataManager.ResetToDefault(_key, AnimationKeyKey);
            GDEDataManager.ResetToDefault(_key, RarityKey);
            GDEDataManager.ResetToDefault(_key, ElementKey);


            #endif

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            LoadFromDict(_key, dict);
        }
    }
}
