// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by the Game Data Editor.
//
//      Changes to this file will be lost if the code is regenerated.
//
//      This file was generated from this data file:
//      /Users/thomasferrer/Documents/Development/CreaturesOfAether/Assets/Source/Bundles/data/gde_data.txt
//  </autogenerated>
// ------------------------------------------------------------------------------
using UnityEngine;
using UnityEngine.Video;
using System;
using System.Collections.Generic;

using GameDataEditor;

namespace GameDataEditor
{
    public class GDEAIBehaviorData : IGDEData
    {
        static string NameKey = "Name";
		string _Name;
        public string Name
        {
            get { return _Name; }
            set {
                if (_Name != value)
                {
                    _Name = value;
					GDEDataManager.SetString(_key, NameKey, _Name);
                }
            }
        }

        static string AIStrategiesKey = "AIStrategies";
		public List<string>      AIStrategies;
		public void Set_AIStrategies()
        {
	        GDEDataManager.SetStringList(_key, AIStrategiesKey, AIStrategies);
		}
		
        static string DeckKey = "Deck";
		public List<string>      Deck;
		public void Set_Deck()
        {
	        GDEDataManager.SetStringList(_key, DeckKey, Deck);
		}
		

        public GDEAIBehaviorData(string key) : base(key)
        {
            GDEDataManager.RegisterItem(this.SchemaName(), key);
        }
        public override Dictionary<string, object> SaveToDict()
		{
			var dict = new Dictionary<string, object>();
			dict.Add(GDMConstants.SchemaKey, "AIBehavior");
			
            dict.Merge(true, Name.ToGDEDict(NameKey));

            dict.Merge(true, AIStrategies.ToGDEDict(AIStrategiesKey));
            dict.Merge(true, Deck.ToGDEDict(DeckKey));
            return dict;
		}

        public override void UpdateCustomItems(bool rebuildKeyList)
        {
        }

        public override void LoadFromDict(string dataKey, Dictionary<string, object> dict)
        {
            _key = dataKey;

			if (dict == null)
				LoadFromSavedData(dataKey);
			else
			{
                dict.TryGetString(NameKey, out _Name);

                dict.TryGetStringList(AIStrategiesKey, out AIStrategies);
                dict.TryGetStringList(DeckKey, out Deck);
                LoadFromSavedData(dataKey);
			}
		}

        public override void LoadFromSavedData(string dataKey)
		{
			_key = dataKey;
			
            _Name = GDEDataManager.GetString(_key, NameKey, _Name);

            AIStrategies = GDEDataManager.GetStringList(_key, AIStrategiesKey, AIStrategies);
            Deck = GDEDataManager.GetStringList(_key, DeckKey, Deck);
        }

        public GDEAIBehaviorData ShallowClone()
		{
			string newKey = Guid.NewGuid().ToString();
			GDEAIBehaviorData newClone = new GDEAIBehaviorData(newKey);

            newClone.Name = Name;

            newClone.AIStrategies = new List<string>(AIStrategies);
			newClone.Set_AIStrategies();
            newClone.Deck = new List<string>(Deck);
			newClone.Set_Deck();

            return newClone;
		}

        public GDEAIBehaviorData DeepClone()
		{
			GDEAIBehaviorData newClone = ShallowClone();
            return newClone;
		}

        public void Reset_Name()
        {
            GDEDataManager.ResetToDefault(_key, NameKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(NameKey, out _Name);
        }

        public void Reset_AIStrategies()
        {
	        GDEDataManager.ResetToDefault(_key, AIStrategiesKey);

	        Dictionary<string, object> dict;
	        GDEDataManager.Get(_key, out dict);
	        dict.TryGetStringList(AIStrategiesKey, out AIStrategies);
        }
		
        public void Reset_Deck()
        {
	        GDEDataManager.ResetToDefault(_key, DeckKey);

	        Dictionary<string, object> dict;
	        GDEDataManager.Get(_key, out dict);
	        dict.TryGetStringList(DeckKey, out Deck);
        }
		

        public void ResetAll()
        {
             #if !UNITY_WEBPLAYER
             GDEDataManager.DeregisterItem(this.SchemaName(), _key);
             #else

            GDEDataManager.ResetToDefault(_key, NameKey);
            GDEDataManager.ResetToDefault(_key, AIStrategiesKey);
            GDEDataManager.ResetToDefault(_key, DeckKey);


            #endif

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            LoadFromDict(_key, dict);
        }
    }
}
