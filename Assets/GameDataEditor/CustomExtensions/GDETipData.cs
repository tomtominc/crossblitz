// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by the Game Data Editor.
//
//      Changes to this file will be lost if the code is regenerated.
//
//      This file was generated from this data file:
//      /Users/thomasferrer/Documents/Development/CreaturesOfAether/Assets/Source/Bundles/data/gde_data.txt
//  </autogenerated>
// ------------------------------------------------------------------------------
using UnityEngine;
using UnityEngine.Video;
using System;
using System.Collections.Generic;

using GameDataEditor;

namespace GameDataEditor
{
    public class GDETipData : IGDEData
    {
        static string TextKey = "Text";
		string _Text;
        public string Text
        {
            get { return _Text; }
            set {
                if (_Text != value)
                {
                    _Text = value;
					GDEDataManager.SetString(_key, TextKey, _Text);
                }
            }
        }

        public GDETipData(string key) : base(key)
        {
            GDEDataManager.RegisterItem(this.SchemaName(), key);
        }
        public override Dictionary<string, object> SaveToDict()
		{
			var dict = new Dictionary<string, object>();
			dict.Add(GDMConstants.SchemaKey, "Tip");
			
            dict.Merge(true, Text.ToGDEDict(TextKey));
            return dict;
		}

        public override void UpdateCustomItems(bool rebuildKeyList)
        {
        }

        public override void LoadFromDict(string dataKey, Dictionary<string, object> dict)
        {
            _key = dataKey;

			if (dict == null)
				LoadFromSavedData(dataKey);
			else
			{
                dict.TryGetString(TextKey, out _Text);
                LoadFromSavedData(dataKey);
			}
		}

        public override void LoadFromSavedData(string dataKey)
		{
			_key = dataKey;
			
            _Text = GDEDataManager.GetString(_key, TextKey, _Text);
        }

        public GDETipData ShallowClone()
		{
			string newKey = Guid.NewGuid().ToString();
			GDETipData newClone = new GDETipData(newKey);

            newClone.Text = Text;

            return newClone;
		}

        public GDETipData DeepClone()
		{
			GDETipData newClone = ShallowClone();
            return newClone;
		}

        public void Reset_Text()
        {
            GDEDataManager.ResetToDefault(_key, TextKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(TextKey, out _Text);
        }

        public void ResetAll()
        {
             #if !UNITY_WEBPLAYER
             GDEDataManager.DeregisterItem(this.SchemaName(), _key);
             #else

            GDEDataManager.ResetToDefault(_key, TextKey);


            #endif

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            LoadFromDict(_key, dict);
        }
    }
}
